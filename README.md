# Summary

A generic platform for fast and flexible development and delivery of a wide variety of immersive scenario-based games that enable professional skills acquisition. For further information see https://www.ou.nl/en/-/emergo-serious-games-for-professional-competences. The platform is based on [ZK framework](http://www.zkoss.org) with [ZK ZOL license](https://www.zkoss.org/license#peee), and [Spring framework](http://www.springsource.org/spring-framework).

# Building the project

This project has dependencies that are not publicly deployed. These are located in `src/main/lib` and should be installed to your local Maven repo by doing

```
cd src/main/lib
mvn install
```

After this, you can do `mvn clean package` etc. from the root of the project.

# Runtime environment

To run Emergo you need a server running

- Windows server 2019 standard
- Oracle Java 8
- Tomcat 9
- MySQL 8
- ClamAV 1.4+
- ffmpeg.exe and ffprobe.exe

Other OSes and versions might or might not work.

## Tomcat settings

- before starting the server, you must add the file `aspectjweaver-1.9.7.jar` to the tomcat/lib directory, and add this setting `CATALINA_OPTS="$CATALINA_OPTS -javaagent:/opt/tomcat/lib/aspectjweaver-1.9.7.jar"`, e.g. by exporting it from setenv
- (only) when using Tomcat on Linux, you must add `<Resources allowLinking="true" />` to the tomcat `<Context>` in context.xml. Do **not** do this on Windows since it will create a security issue 

# Emergo configuration

To run an Emergo instance properly, it needs a configuration file. By default, the instance will determine the properties file name by looking at the directory in which it is deployed in webapps. E.g. if the instance lives in `emergo-1.0.0` the preferred properties file is called `emergo-1.0.0.properties` and must be located in the tomcat directory catalina.base.

If this derived properties file does not exist, any Emergo instance will fallback to `emergo.properties`, also in the tomcat directory catalina.base. If none of these two exist, the instance will not run properly, or at all.

> Please note that this mechanism will allow you to deploy multiple instances of Emergo on the same tomcat server, each with their own properties. However, **never run two concurrent Emergo instances that use the same properties!**

As a starting point for making the properties file, you can use `emergo-sample.properties` in the root of this project. Make sure to replace any `${PLACEHOLDER}` values with a proper value. Explanations of the settings are in the sample file.

# Gamebrics toolkit (only Dutch)

A working docker container example, featuring the [Gamebrics toolkit](https://gamebrics.nl), can be composed according to https://gitlab.com/ou-teli/gamebrics-docker-compose.