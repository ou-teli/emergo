/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.zkspring;

import java.util.ArrayList;
import java.util.List;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;

public class SSpringHelper {

	protected VView vView = CDesktopComponents.vView();
	protected SSpring sSpring = CDesktopComponents.sSpring();

	public String getTagAttribute(IXMLTag aTag, String aKey) {
		return aTag.getDefAttribute(aKey);
	}

	public String getTagChildValue(IXMLTag aTag, String aChildName, String aDefaultValue) {
		String lValue = sSpring.unescapeXML(aTag.getChildValue(aChildName));
		return lValue.equals("") ? aDefaultValue : lValue;
	}

	public String getTagStatusChildAttribute(IXMLTag aTag, String aKey, String aDefaultValue) {
		String lValue = aTag.getChild(AppConstants.statusElement).getDefAttribute(aKey);
		return lValue.equals("") ? aDefaultValue : lValue;
	}

	public String getCurrentStatusTagStatusChildAttribute(IXMLTag aTag, String aKey, String aDefaultValue) {
		//NOTE replace crlf string constants by \n.
		String lValue = sSpring.unescapeXML(aTag.getCurrentStatusAttribute(aKey)).replace(AppConstants.statusCrLfReplace, "\n");
		return lValue.equals("") ? aDefaultValue : lValue;
	}
	
	public String getCurrentStatusTagStatusChildAttribute(IECaseComponent aCaseComponent, String aTagName, String aTagKey, String aKey, String aDefaultValue) {
		List<String> lErrorMessages = new ArrayList<String>();
		//NOTE replace crlf string constants by \n.
		String lValue = sSpring.unescapeXML(sSpring.getCurrentRunTagStatus(aCaseComponent.getName(), aTagName, aTagKey, aKey, AppConstants.statusTypeRunGroup, lErrorMessages)).replace(AppConstants.statusCrLfReplace, "\n");
		return lValue.equals("") ? aDefaultValue : lValue;
	}
	
	public String getCurrentStatusComponentStatusChildAttribute(CRunComponent aEmergoRunComponent, String aKey, String aDefaultValue) {
		//NOTE replace crlf string constants by \n.
		String lValue = sSpring.unescapeXML(sSpring.getCurrentRunComponentStatus(aEmergoRunComponent.getCaseComponent(), aKey, AppConstants.statusTypeRunGroup));
		return lValue.equals("") ? aDefaultValue : lValue;
	}
	
	public Integer getStatusTagStatusChildAttributeCount(IXMLTag aTag, String aKey, String aValue) {
		return aTag.getStatusAttributeCount(aKey, aValue);
	}
	
	public IXMLTag getReferencedTag(IECaseComponent aCaseComponent, IXMLTag aTag, String aRefChildName) {
		return sSpring.getSReferencedDataTagHelper().getReferencedTag(aCaseComponent, aTag, aRefChildName);
	}
	
	public String getAbsoluteUrl(IXMLTag aTag) {
		String lUrl = sSpring.getSBlobHelper().getUrl(aTag);
		if (!vView.isAbsoluteUrl(lUrl)) {
			if (!lUrl.equals("") && lUrl.indexOf("/") != 0) {
				lUrl = "/" + lUrl;
			}
			lUrl = vView.getEmergoRootUrl() + vView.getEmergoWebappsRoot() + lUrl;
		}
		if (!lUrl.equals("")) {
			// avoid javascript unterminated string error
			lUrl = lUrl.replace("'", "\\'");
		}
		return lUrl;
	}
	
	public String stripEmergoPath(String aUrl) {
		if (aUrl.equals("")) {
			return aUrl;
		}
		String lWebappsRoot = vView.getEmergoWebappsRoot();
		if (aUrl.indexOf(lWebappsRoot) == 0) {
			aUrl = aUrl.substring(lWebappsRoot.length());
		}
		return aUrl;
	}
	
	public IXMLTag setRunTagStatus(CRunComponent aEmergoRunComponent, IXMLTag tag, String statusKey, String statusValue, boolean checkScript) { 
		return aEmergoRunComponent.setRunTagStatus(aEmergoRunComponent.getCaseComponent(), tag, statusKey, statusValue, checkScript, true);
	}

	public void setRunComponentStatus(CRunComponent aEmergoRunComponent, String statusKey, String statusValue, boolean checkScript) { 
		aEmergoRunComponent.setRunComponentStatus(aEmergoRunComponent.getCaseComponent(), statusKey, statusValue, true);
	}

}
