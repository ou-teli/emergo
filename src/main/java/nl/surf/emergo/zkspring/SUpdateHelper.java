/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.zkspring;

import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import org.zkoss.zk.ui.event.Event;

import nl.surf.emergo.business.IRunGroupCaseComponentUpdateManager;
import nl.surf.emergo.business.IRunTeamRunGroupManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.domain.IERunGroupCaseComponentUpdate;
import nl.surf.emergo.domain.IERunTeam;
import nl.surf.emergo.domain.IERunTeamRunGroup;

public class SUpdateHelper {
	
	protected SSpring sSpring;
	
	/** the run group case component update manager. */
	protected IRunGroupCaseComponentUpdateManager runGroupCaseComponentUpdateManager = null;
	
	/**
	 * The update run group tag. Is used to save tags in memory that must update
	 * other users status. For instance if one pc sends an e-message to another pc.
	 * These tags are periodically saved in the rungroupcasecomponentupdate table.
	 * And this table is read by the player of the other pc to update his status,
	 * setting the processed field in the db record to true.
	 */
	protected Hashtable<String,IXMLTag> updateRunGroupTag = null;

	/**
	 * The processed update run group tag. If the Emergo player is started in
	 * readonly modus rungroupcasecomponentupdate records cannot be set to processed,
	 * so the processed status is saved in memory. This is done to prevent processing
	 * the same record again and again.
	 */
	protected Hashtable<String,IERunGroupCaseComponentUpdate> processedUpdateRunGroupTag = null;

	public SUpdateHelper(SSpring sSpring) {
		this.sSpring = sSpring;
	}

	/**
	 * Gets the run group case component update manager.
	 *
	 * @return the run group case component update manager
	 */
	public IRunGroupCaseComponentUpdateManager getRunGroupCaseComponentUpdateManager() {
		if (runGroupCaseComponentUpdateManager == null)
			runGroupCaseComponentUpdateManager = (IRunGroupCaseComponentUpdateManager) (sSpring.getBean("runGroupCaseComponentUpdateManager"));
		return runGroupCaseComponentUpdateManager;
	}

	/**
	 * Handle run group case component update event.
	 *
	 * @param aRunGroupCaseComponentUpdate the run group case component update
	 */
	public void handleRunGroupCaseComponentUpdateEvent(IERunGroupCaseComponentUpdate aRunGroupCaseComponentUpdate) {
		IERunGroup lRunGroup = sSpring.getRunGroup();
		if (lRunGroup != null && aRunGroupCaseComponentUpdate.getRugRugId() == lRunGroup.getRugId()) {
			processUpdateRunTag(aRunGroupCaseComponentUpdate, AppConstants.statusTypeRunGroup);
		}
	}

	/**
	 * Gets the prop update run group tag.
	 *
	 * @return the prop update run group tag
	 */
	protected Hashtable<String,IXMLTag> getPropUpdateRunGroupTag() {
		if (updateRunGroupTag == null)
			setPropUpdateRunGroupTag(new Hashtable<String,IXMLTag>(0));
		return updateRunGroupTag;
	}

	/**
	 * Sets the prop update run group tag.
	 *
	 * @param aUpdateRunGroupTag the new prop update run group tag
	 */
	protected void setPropUpdateRunGroupTag(Hashtable<String,IXMLTag> aUpdateRunGroupTag) {
		updateRunGroupTag = aUpdateRunGroupTag;
	}

	/**
	 * Gets the prop processed update run group tag.
	 *
	 * @return the prop processed update run group tag
	 */
	protected Hashtable<String,IERunGroupCaseComponentUpdate> getPropProcessedUpdateRunGroupTag() {
		if (processedUpdateRunGroupTag == null)
			processedUpdateRunGroupTag = new Hashtable<String,IERunGroupCaseComponentUpdate>(0);
		return processedUpdateRunGroupTag;
	}

	/**
	 * Adds update run tag.
	 * Stores a status tag that is meant for another Emergo run group given by aRugId.
	 * aCacIdForRug is the id of the casecomponent of the other user which status should
	 * be updated. Because casecomponent ids of sender and receiver do not have to be equal.
	 * 
	 * If the addressed run group is active, meaning he has the EMERGO player opened,
	 * and this method is called from within the player, the run tag is processed using
	 * an event queue with scope 'application'.
	 * Otherwise the run tag is saved within memory to be saved periodically by method
	 * saveRunGroupCaseComponentUpdates, that uses database table rungroupcasecomponentupdates
	 * for updating.
	 * 
	 * An usage example is sending an e-message to another Emergo user.
	 *
	 * @param aRugId the a rug id
	 * @param aCacIdForRug the a cac id for rug
	 * @param aStatusTag the a status tag
	 * @param aStatusType the a status type, see IAppManager for possible types
	 * @param aSynchronous the a synchronous
	 */
	public void addUpdateRunTag(int aRugId, int aCacIdForRug, IXMLTag aStatusTag, String aStatusType, boolean aSynchronous) {
		if (!aStatusType.equals(AppConstants.statusTypeRunGroup))
			//no updates allowed in run status or run team status
			return;
		List<IERunGroupAccount> lActiveRunGroupAccounts = null;
		if (sSpring.getPropRunTeam() == null) {
			lActiveRunGroupAccounts = sSpring.getActiveRunGroupAccountsWithinRun();
		}
		else {
			lActiveRunGroupAccounts = sSpring.getActiveRunGroupAccountsWithinRunTeam();
		}
		boolean lRGAisActive = false;
		for (IERunGroupAccount lRunGroupAccount : lActiveRunGroupAccounts) {
			if (lRunGroupAccount.getERunGroup().getRugId() == aRugId) {
				lRGAisActive = true;
			}
		}
		boolean lUseEventQueueProcessing = (lRGAisActive && sSpring.getPropRunWnd() != null);
		boolean lUseDatabaseProcessing = !lUseEventQueueProcessing;
		if (lUseEventQueueProcessing) {
			IERunGroupCaseComponentUpdate lRGCCupdate = createRunGroupCaseComponentUpdate(aStatusTag, aRugId, aCacIdForRug);
			if (lRGCCupdate != null) {
				try {
					if (aSynchronous) {
						sSpring.getPropRunWnd().eventQueueRunGroupCaseComponentUpdateSync.publish(
								new Event(sSpring.getPropRunWnd().eventRunGroupCaseComponentUpdate, null, lRGCCupdate));
					}
					else {
						sSpring.getPropRunWnd().eventQueueRunGroupCaseComponentUpdateAsync.publish(
								new Event(sSpring.getPropRunWnd().eventRunGroupCaseComponentUpdate, null, lRGCCupdate));
					}
				} catch (Exception e) {
					//if error use db processing
					lUseDatabaseProcessing = true;
				}
			}
			else {
				//if error use db processing
				lUseDatabaseProcessing = true;
			}
		}
		if (lUseDatabaseProcessing) {
			//add size to make it unique, so every change in run statustag is saved.
			String lIds = "" + aRugId + "," + aCacIdForRug + "," + getPropUpdateRunGroupTag().size();
			getPropUpdateRunGroupTag().put(lIds, aStatusTag);
		}
	}

	/**
	 * Checks update run tags.
	 * Checks if other Emergo users have done something to update the status of the
	 * current user. Updates are available within table rungroupcasecomponentupdates.
	 * Updates are also carried out by updating the user status, except for read only
	 * runs.
	 *
	 * @param aStatusType the a status type
	 */
	public void checkUpdateRunTags(String aStatusType) {
		if (!aStatusType.equals(AppConstants.statusTypeRunGroup)) return;
		IERunGroup lRunGroup = sSpring.getRunGroup();
		if (lRunGroup == null)
			return;
//		get not processed records
		IRunGroupCaseComponentUpdateManager rguManager = getRunGroupCaseComponentUpdateManager();
		List<IERunGroupCaseComponentUpdate> rgus = rguManager.getAllRunGroupCaseComponentUpdatesByRugId(lRunGroup.getRugId(), false);
		if (rgus.size() == 0)
//			no not processed records
			return;
		if (sSpring.isReadOnlyRun()) {
//			for read only  get first not processed and not processed in memory
			for (IERunGroupCaseComponentUpdate rgu : rgus) {
				if (!getPropProcessedUpdateRunGroupTag().containsKey(""+rgu.getRguId())) {
					if (processUpdateRunTag(rgu, aStatusType)) {
//						for read only don't set status of record but save in memory which records are processed
						getPropProcessedUpdateRunGroupTag().put(""+rgu.getRguId(), rgu);
					}
				}
			}
		}
		else {
//			for student get first not processed record
			for (IERunGroupCaseComponentUpdate rgu : rgus) {
				if (processUpdateRunTag(rgu, aStatusType)) {
//					for student set status of record to processed
					rgu.setProcessed(true);
					rguManager.updateRunGroupCaseComponentUpdate(rgu);
				}
			}
		}
	}

	/**
	 * Processes update run tag.
	 * Checks if other Emergo users have done something to update the status of the
	 *
	 * @param rgu the rgu
	 * @param aStatusType the a status type
	 */
	public boolean processUpdateRunTag(IERunGroupCaseComponentUpdate rgu, String aStatusType) {
		IXMLTag lStatusUpdateRootTag = getXmlRunGroupStatusUpdateTree(rgu);
		if (lStatusUpdateRootTag != null) {
			int lCacId = rgu.getCacCacId();
			IXMLTag lContentTag = lStatusUpdateRootTag.getChild(AppConstants.contentElement);
			if (lContentTag != null) {
				List<IXMLTag> lChildTags = lContentTag.getChildTags();
				if (lChildTags.size() == 1) {
					IXMLTag lStatusUpdateTag = lChildTags.get(0);
					boolean lProcessLocally = lStatusUpdateTag.getAttribute(AppConstants.processLocally).equals(AppConstants.statusValueTrue);
					if (lProcessLocally) {
						// NOTE if processLocally is set a component itself takes care of updating run tags
						return false;
					}
					IECaseComponent lCaseComponent = sSpring.getCaseComponentInRun(lCacId);
					String lTagName = lStatusUpdateTag.getName();
					List<String> lDataTagIds = new ArrayList<String>();
					String lDataTagId = lStatusUpdateTag.getAttribute(AppConstants.keyRefdataid);
					if (lDataTagId.equals("-1")) {
						String lTagTemplate = lStatusUpdateTag.getAttribute(AppConstants.keyRefdatatemplate);
						lDataTagIds.addAll(sSpring.getTagIdsFromTagTemplate(lTagTemplate, lCaseComponent, lTagName));
						if (lDataTagIds.size() == 0)
							// NOTE template doesn't match any tag node name (yet; could be depending on changing state parameter)
							// NOTE consider update as treated
							return true;
					} else {
						lDataTagIds.add(lDataTagId);
					}
					String lStatusKey = lStatusUpdateTag.getAttribute(AppConstants.updateStatusKey);
					String lStatusValue = lStatusUpdateTag.getAttribute(AppConstants.updateStatusValue);
					boolean lCheckScript = (lStatusUpdateTag.getAttribute(AppConstants.updateCheckScript).equals(AppConstants.statusValueTrue));
					IXMLTag lStatusUpdateStatusTag = lStatusUpdateTag.getChild(AppConstants.statusElement);
					SStatustagEnrichment lStatustagEnrichment = null;
					if (lStatusUpdateStatusTag != null) {
						lStatustagEnrichment = new SStatustagEnrichment(); 
						lStatustagEnrichment.setStatusChildTags(lStatusUpdateStatusTag.getChildTags());
					}
					for (String lDataTId : lDataTagIds) {
						//NOTE set last parameter aFromScript to true, although it not originates from script.
						//Otherwise for instance beaming to location does not work
						IXMLTag lStatusTag = sSpring.setRunTagStatus(lCaseComponent, lDataTId, lTagName, lStatusKey, lStatusValue, lStatustagEnrichment, lCheckScript, null, aStatusType, true, true);
						sSpring.setRunTag(lCaseComponent, lStatusTag, aStatusType);
					}
				}
			}
			IXMLTag lComponentTag = lStatusUpdateRootTag.getChild(AppConstants.componentElement);
			if (lComponentTag != null) {
				List<IXMLTag> lChildTags = lComponentTag.getChildTags();
				if (lChildTags.size() == 1) {
//					only component status tag attributes are updated
					IXMLTag lStatusUpdateTag = lChildTags.get(0);
					boolean lProcessLocally = lStatusUpdateTag.getAttribute(AppConstants.processLocally).equals(AppConstants.statusValueTrue);
					if (lProcessLocally) {
						// NOTE if processLocally is set a component itself takes care of updating run tags
						return false;
					}
					IECaseComponent lCaseComponent = sSpring.getCaseComponentInRun(lCacId);
					String lStatusKey = lStatusUpdateTag.getAttribute(AppConstants.updateStatusKey);
					String lStatusValue = lStatusUpdateTag.getAttribute(AppConstants.updateStatusValue);
					boolean lCheckScript = (lStatusUpdateTag.getAttribute(AppConstants.updateCheckScript).equals(AppConstants.statusValueTrue));
					STriggeredReference lTriggeredReference = new STriggeredReference();
					lTriggeredReference.setCaseComponent(lCaseComponent);
					lTriggeredReference.setStatusKey(lStatusKey);
					lTriggeredReference.setStatusValue(lStatusValue);
					//NOTE set last parameter aFromScript to true, although it not originates from script.
					//Otherwise for instance beaming to location does not work
					sSpring.setRunComponentStatus(lTriggeredReference, lCheckScript, aStatusType, true, true);
				}
			}
		}
		return true;
	}

	/**
	 * Saves run group case component updates.
	 */
	protected void saveRunGroupCaseComponentUpdates() {
		if (getPropUpdateRunGroupTag().size() == 0)
			return;
		IRunGroupCaseComponentUpdateManager lBean = getRunGroupCaseComponentUpdateManager();
		for (Enumeration<String> keys = getPropUpdateRunGroupTag().keys(); keys.hasMoreElements();) {
			String lIds = (String) keys.nextElement();
			String[] lIdArr = lIds.split(",");
			IERunGroupCaseComponentUpdate lRunGroupCaseComponentUpdate = createRunGroupCaseComponentUpdate(
					(IXMLTag)getPropUpdateRunGroupTag().get(lIds), Integer.parseInt(lIdArr[0]), Integer.parseInt(lIdArr[1]));
			if (lRunGroupCaseComponentUpdate != null) {
				lBean.newRunGroupCaseComponentUpdate(lRunGroupCaseComponentUpdate);
			}
		}
		setPropUpdateRunGroupTag(new Hashtable<String,IXMLTag>(0));
	}

	/**
	 * Creates run group case component update.
	 * 
	 * @param aStatusTag the a status tag
	 * @param aRugId the a rug id
	 * @param aCacIdForRug the a cac id for rug
	 *
	 * @return the run group case component update
	 */
	private IERunGroupCaseComponentUpdate createRunGroupCaseComponentUpdate(IXMLTag aStatusTag, int aToRugId, int aCacIdForRug) {
		IERunGroup lRunGroup = sSpring.getRunGroup();
		if (lRunGroup == null)
			return null;
		if (aStatusTag == null) {
			return null;
		}
		int lFromRugId = lRunGroup.getRugId();
		IRunGroupCaseComponentUpdateManager lBean = getRunGroupCaseComponentUpdateManager();
//		create new rungroupcasecomponentupdate
		IERunGroupCaseComponentUpdate lComp = lBean.getNewRunGroupCaseComponentUpdate();
		if (lComp != null) {
			lComp.setRugRugId(aToRugId);
			lComp.setCacCacId(aCacIdForRug);
			lComp.setRugRugFromId(lFromRugId);
//			get statustree
			IXMLTag lStatusUpdateRootTag = getXmlRunGroupStatusUpdateTree(lComp);
			if (lStatusUpdateRootTag != null) {
				if (aStatusTag.getName().equals("componentstatus")) {
					IXMLTag lComponentTag = lStatusUpdateRootTag.getChild(AppConstants.componentElement);
					if (lComponentTag != null)
						lComponentTag.getChildTags().add(aStatusTag);
				}
				else {
					sSpring.getXmlManager().newChildNodeSimple(lStatusUpdateRootTag, aStatusTag, null);
				}
//				convert statustree to xmldata
				String lXmlStatus = sSpring.getXmlManager().xmlTreeToDoc(lStatusUpdateRootTag);
//				save it in database
				lComp.setXmldata(lXmlStatus);
			}
		}
		return lComp;
	}

	/**
	 * Gets xml data for component status update.
	 * 
	 * @param aStatusKey the a status key
	 * @param aStatusValue the a status value
	 *
	 * @return xml data
	 */
	public String getXmlDataTagForComponentStatusUpdate(String aStatusKey, String aStatusValue) {
		String lXmlData = CDesktopComponents.sSpring().getEmptyXml();
		IXMLTag lRootTag = CDesktopComponents.sSpring().getXmlManager().getXmlTree(null, lXmlData, AppConstants.other_tag);
		IXMLTag lComponentTag = lRootTag.getChild(AppConstants.componentElement);
		IXMLTag lStatusTag = lComponentTag.getChild(AppConstants.statusElement);
		if (lStatusTag == null) {
			lStatusTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag(AppConstants.statusElement, "");
			lComponentTag.getChildTags().add(lStatusTag);
		}
		lStatusTag.setAttribute("update_statuskey", aStatusKey);
		lStatusTag.setAttribute("update_statusvalue", aStatusValue);
		lStatusTag.setAttribute("update_checkscript", "true");
		lXmlData = CDesktopComponents.sSpring().getXmlManager().xmlTreeToDoc(lRootTag);
		return lXmlData;
	}
	
	/**
	 * Gets xml data for tag status update.
	 * 
	 * @param aTagName the a tag name
	 * @param aTagId the a tag id
	 * @param aStatusKey the a status key
	 * @param aStatusValue the a status value
	 * @param aStatusChildTags the a status child tags
	 * @param aIsTemplateTag the a is template tag
	 *
	 * @return xml data
	 */
	public String getXmlDataTagForTagStatusUpdate(String aTagName, String aTagId, String aStatusKey, String aStatusValue, List<IXMLTag> aStatusChildTags, boolean aIsTemplateTag) {
		String lXmlData = CDesktopComponents.sSpring().getEmptyXml();
		IXMLTag lRootTag = CDesktopComponents.sSpring().getXmlManager().getXmlTree(null, lXmlData, AppConstants.status_tag);
		IXMLTag lContentTag = lRootTag.getChild(AppConstants.contentElement);
		lContentTag.setAttribute("max_id", "2");
		IXMLTag lTag = sSpring.getXmlManager().newXMLTag(aTagName, "");
		lContentTag.getChildTags().add(lTag);
		lTag.setAttribute(AppConstants.updateStatusKey, aStatusKey);
		lTag.setAttribute(AppConstants.updateStatusValue, aStatusValue);
		lTag.setAttribute(AppConstants.updateCheckScript, "true");
		if (aIsTemplateTag) {
			lTag.setAttribute(AppConstants.keyRefdataid, "-1");
			lTag.setAttribute(AppConstants.keyRefdatatemplate, aTagId);
		}
		else {
			lTag.setAttribute(AppConstants.keyRefdataid, aTagId);
		}
		lTag.setAttribute(AppConstants.keyId, "2");
		if (aStatusChildTags != null) {
			IXMLTag lStatusTag = sSpring.getXmlManager().newXMLTag(AppConstants.statusElement, "");
			lTag.getChildTags().add(lStatusTag);
			lStatusTag.setParentTag(lTag);
			for (IXMLTag lStatusChildTag : aStatusChildTags) {
				lStatusTag.getChildTags().add(lStatusChildTag);
				lStatusChildTag.setParentTag(lStatusTag);
			}
		}
		lXmlData = CDesktopComponents.sSpring().getXmlManager().xmlTreeToDoc(lRootTag);
		return lXmlData;
	}
	
	/**
	 * Gets xml data for tag status update.
	 * 
	 * @param aTagName the a tag name
	 * @param aTagId the a tag id
	 * @param aStatusKey the a status key
	 * @param aStatusValue the a status value
	 * @param aIsTemplateTag the a is template tag
	 *
	 * @return xml data
	 */
	public String getXmlDataTagForTagStatusUpdate(String aTagName, String aTagId, String aStatusKey, String aStatusValue, boolean aIsTemplateTag) {
		return getXmlDataTagForTagStatusUpdate(aTagName, aTagId, aStatusKey, aStatusValue, null, aIsTemplateTag);
	}
	
	/**
	 * Saves xml data status update.
	 * 
	 * @param aRunGroup the a run group
	 * @param aCacId the a cac id
	 * @param aXmldata the a xmldata
	 */
	public void saveXmlDataComponentOrTagStatusUpdate(IERunGroup aRunGroup, int aCacId, String aXmldata) {
		IERunGroupCaseComponentUpdate lRunGroupCaseComponentUpdate = getRunGroupCaseComponentUpdateManager().getNewRunGroupCaseComponentUpdate();
		int lRugId = aRunGroup.getRugId();
		lRunGroupCaseComponentUpdate.setRugRugId(lRugId);
		lRunGroupCaseComponentUpdate.setCacCacId(aCacId);
		lRunGroupCaseComponentUpdate.setRugRugFromId(lRugId);
		lRunGroupCaseComponentUpdate.setXmldata(aXmldata);
		lRunGroupCaseComponentUpdate.setProcessed(false);
		lRunGroupCaseComponentUpdate.setCreationdate(new Date());
		lRunGroupCaseComponentUpdate.setLastupdatedate(new Date());
		getRunGroupCaseComponentUpdateManager().saveRunGroupCaseComponentUpdate(lRunGroupCaseComponentUpdate);
	}

	/**
	 * Gets the xml run group status update tree. A tree which contains data to update a run groups status.
	 *
	 * @param aRunGroupCaseComponentUpdate the a run group case component update
	 *
	 * @return the xml status update tree
	 */
	public IXMLTag getXmlRunGroupStatusUpdateTree(IERunGroupCaseComponentUpdate aRunGroupCaseComponentUpdate) {
		IECaseComponent lCaseComponent = sSpring.getCaseComponent(aRunGroupCaseComponentUpdate.getCacCacId(), false);
		if (lCaseComponent == null)
			return null;
		IXMLTag lXmlDefRootTag = sSpring.getXmlDefTree(lCaseComponent.getEComponent());
		if (lXmlDefRootTag == null)
			return null;
		String lXmlStatus = aRunGroupCaseComponentUpdate.getXmldata();
		if (lXmlStatus == null || lXmlStatus.equals(""))
			lXmlStatus = sSpring.getEmptyXml();
		IXMLTag lRootTag = sSpring.getXmlManager().getXmlTree(lXmlDefRootTag, lXmlStatus, AppConstants.status_tag);
		return lRootTag;
	}

	/**
	 * Gets the all external data updates for all run group accounts.
	 * When data has to be exchanged between run group accounts for whom the Emergo player is playing, this data
	 * is exchanged through application memory.
	 * This update mechanism is used for case components which are shared within a run or a run team. Or for run
	 * groups who are composite, so a number of run group accounts forms one run group.
	 *
	 * @return the all external data updates
	 */
	public Hashtable<String,List<Hashtable<String,Object>>> getAppPropAllExternalDataUpdates() {
		if (sSpring.getAppManager().getAppContainer().get("externalDataUpdates") == null)
			sSpring.getAppManager().getAppContainer().put("externalDataUpdates", new Hashtable<String,List<Hashtable<String,Object>>>(0));
		return (Hashtable)sSpring.getAppManager().getAppContainer().get("externalDataUpdates");
	}

	/**
	 * Gets the external data updates for the current run group account.
	 *
	 * @param aStatusType the a status type, see IAppManager
	 *
	 * @return the external data updates
	 */
	public List<Hashtable<String,Object>> getExternalDataUpdates(String aStatusType) {
		boolean compositeRunGroup = sSpring.getRunGroup().getComposite();
		if ((aStatusType.equals(AppConstants.statusTypeRunGroup)) && (!compositeRunGroup))
			return new ArrayList<Hashtable<String,Object>>(0);
		IERunGroupAccount lRunGroupAccount = sSpring.getRunGroupAccount();
		if (lRunGroupAccount == null)
			return new ArrayList<Hashtable<String,Object>>(0);
		Hashtable<String,List<Hashtable<String,Object>>> lExternalDataUpdates = getAppPropAllExternalDataUpdates();
		if (!lExternalDataUpdates.containsKey(""+lRunGroupAccount.getRgaId()))
			return new ArrayList<Hashtable<String,Object>>(0);
		return (List<Hashtable<String,Object>>)lExternalDataUpdates.get(""+lRunGroupAccount.getRgaId());
	}

	/**
	 * Put an external data update into the list of external data updates for all run group accounts within run or run team,
	 * except for the current run group account.
	 *
	 * @param aCaseComponent the a case component
	 * @param aStatusType the a status type, see IAppManager
	 * @param aDataUpdate the a update data
	 */
	public void putExternalDataUpdate(IECaseComponent aCaseComponent, String aStatusType, Hashtable<String,Object> aDataUpdate) {
		boolean lStRunGroup = aStatusType.equals(AppConstants.statusTypeRunGroup);
		boolean lStRun = aStatusType.equals(AppConstants.statusTypeRun);
		boolean lStRunTeam = aStatusType.equals(AppConstants.statusTypeRunTeam);
		IERunGroupAccount lCurrentRunGroupAccount = sSpring.getRunGroupAccount();
		if (lCurrentRunGroupAccount == null)
			return;
		List<IERunGroupAccount> lRunGroupAccounts = sSpring.getActiveRunGroupAccountsWithinRun();
		Hashtable<String,List<Hashtable<String,Object>>> lExternalDataUpdates = getAppPropAllExternalDataUpdates();
		for (IERunGroupAccount lRunGroupAccount : lRunGroupAccounts) {
			// check rungroupaccount is not current rungroupaccount
			boolean lRgaNotCurrentRga = (lRunGroupAccount.getRgaId() != lCurrentRunGroupAccount.getRgaId());
			// check if casecomponent is used by rungroupaccount
			boolean lCacUsedByRga = (sSpring.getCaseComponentRole(lRunGroupAccount.getERunGroup().getECaseRole().getCarId(), aCaseComponent.getCacId()) != null);
			if (lRgaNotCurrentRga && lCacUsedByRga) {
				// check if rungroupaccount is in team of current rungroupaccount if team exists
				boolean lPut = false;
				if (lStRunGroup) {
					// if composite rug and rga's are in same rug
					lPut = ((lRunGroupAccount.getERunGroup().getComposite()) && (lRunGroupAccount.getERunGroup().getRugId() ==
						lCurrentRunGroupAccount.getERunGroup().getRugId()));
				}
				if (lStRun)
					lPut = true;
				if (lStRunTeam) {
					IERunTeam lRunCurrentTeam = sSpring.getRunTeam();
					if (lRunCurrentTeam != null) {
						// current rungroupaccount is member of current team
						// check if rungroupaccount is member of current team
						List<IERunTeamRunGroup> lRunTeamRunGroups = ((IRunTeamRunGroupManager)sSpring.getBean("runTeamRunGroupManager")).getAllRunTeamRunGroupsByRutIdRugId(lRunCurrentTeam.getRutId(), lRunGroupAccount.getERunGroup().getRugId());
						// rungroupaccount is in team or not
						lPut = (lRunTeamRunGroups.size() > 0);
					}
				}
				if (lPut) {
					List<Hashtable<String,Object>> lDataUpdates = null;
					if (lExternalDataUpdates.containsKey(""+lRunGroupAccount.getRgaId()))
						lDataUpdates = lExternalDataUpdates.get(""+lRunGroupAccount.getRgaId());
					else {
						lDataUpdates = new ArrayList<Hashtable<String,Object>>(0);
						lExternalDataUpdates.put(""+lRunGroupAccount.getRgaId(), lDataUpdates);
					}
					lDataUpdates.add(aDataUpdate);
				}
			}
		}
	}

	/**
	 * Executes action tag for other case roles than current one.
	 *
	 * @param aCarIds the car ids for case roles other than the current one
	 * @param aCacId the cac id
	 * @param aTagId the tag id
	 * @param aTagName the tag name
	 * @param aStatusKey the status key
	 * @param aStatusValue the status value
	 * @param aStatusType the a status type, see IAppManager
	 *
	 * @return true, if successful
	 */
	protected boolean executeActionTagForOtherCaseRoles(List<Integer> aCarIds, int aCacId, String aTagId, String aTagName, String aStatusKey, String aStatusValue, String aStatusType) {
		if (aCarIds.size() == 0) {
			return false;
		}
		IXMLTag lStatusTag = sSpring.getXmlManager().newXMLTag(aTagName, "");
		lStatusTag.setAttribute("update_statuskey", aStatusKey);
		lStatusTag.setAttribute("update_statusvalue", aStatusValue);
		lStatusTag.setAttribute("update_checkscript", "true");
		lStatusTag.setAttribute(AppConstants.keyRefdataid, aTagId);
		lStatusTag.setAttribute(AppConstants.keyId, "2");
		List<IERunGroupAccount> lRunGroupAccounts = null;
		if (sSpring.getPropRunTeam() == null) {
			lRunGroupAccounts = sSpring.getRunGroupAccountsWithinRun();
		}
		else {
			lRunGroupAccounts = sSpring.getRunGroupAccountsWithinRunTeam();
		}
		List<Integer> lRugIds = new ArrayList<Integer>();
		for (Integer lCarId : aCarIds) {
			for (IERunGroupAccount lRunGroupAccount : lRunGroupAccounts) {
				if (!lRugIds.contains(lRunGroupAccount.getERunGroup().getRugId())) {
					if (lRunGroupAccount.getERunGroup().getECaseRole().getCarId() == lCarId) {
						addUpdateRunTag(lRunGroupAccount.getERunGroup().getRugId(), aCacId, lStatusTag, aStatusType, false);
						lRugIds.add(lRunGroupAccount.getERunGroup().getRugId());
					}
				}
			}
		}
		return true;
	}

}
