/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.zkspring;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.util.media.AMedia;
import org.zkoss.util.media.Media;

import nl.surf.emergo.business.IBlobManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IEBlob;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.utilities.FtpHelper;
import nl.surf.emergo.utilities.PropsValues;
import nl.surf.emergo.view.VView;

public class SBlobHelper {
	private static final Logger _log = LogManager.getLogger(SBlobHelper.class);
	protected VView vView;
	protected CControl cControl;
	protected SSpring sSpring;

	public SBlobHelper(SSpring sSpring) {
		this.sSpring = sSpring;
		this.vView = sSpring.getPropVView();
		this.cControl = sSpring.getPropCControl();
	}

	/**
	 * Sets the blob file name or url in a session variable, to be checked by the
	 * content files filter servlet.
	 *
	 * @param aFileName the file name or url
	 * @param aBlobId   the blo id
	 *
	 */
	public void setBlobInSessionVar(String aFileName, String aBlobId) {
		if ((aFileName != null) && (!aFileName.isEmpty())) {
			ArrayList<String> lMediaFiles = new ArrayList<String>();
			ArrayList<ArrayList<String>> lBlobIds = new ArrayList<ArrayList<String>>();
			Object lSessVar = cControl.getAccSessAttr(AppConstants.sessKeyContentFiles);
			if (!(lSessVar == null)) {
				lMediaFiles = (ArrayList<String>) lSessVar;
				lBlobIds = (ArrayList<ArrayList<String>>) cControl
						.getAccSessAttr(AppConstants.sessKeyContentFileBlobIds);
			}
			aFileName = aFileName.substring(aFileName.lastIndexOf("/") + 1);
			int lRefPos = lMediaFiles.indexOf(aFileName);
			if (lRefPos < 0) {
				lMediaFiles.add(aFileName);
				ArrayList<String> lTemp = new ArrayList<String>();
				lTemp.add(aBlobId);
				lBlobIds.add(lTemp);
				cControl.setAccSessAttr(AppConstants.sessKeyContentFiles, lMediaFiles);
				cControl.setAccSessAttr(AppConstants.sessKeyContentFileBlobIds, lBlobIds);
			} else {
				ArrayList<String> lTemp = lBlobIds.get(lRefPos);
				if (!lTemp.contains(aBlobId)) {
					lTemp.add(aBlobId);
					lBlobIds.set(lRefPos, lTemp);
					cControl.setAccSessAttr(AppConstants.sessKeyContentFileBlobIds, lBlobIds);
				}
			}
		}
	}

	/**
	 * Gets the blob.
	 *
	 * @param bloId the blo id
	 *
	 * @return the blob
	 */
	public IEBlob getBlob(String bloId) {
		if (bloId.equals(""))
			return null;
		IBlobManager lBean = sSpring.getBlobManager();
		IEBlob lBlob = lBean.getBlob(Integer.parseInt(bloId));
		if (lBlob != null) {
			String lFileName = lBlob.getFilename();
			String lUrl = lBlob.getUrl();
			// add file name or url to session variable, so they can pass the content files
			// filter servlet
			if (StringUtils.isEmpty(lFileName) && StringUtils.isEmpty(lUrl)) {
				// blob must have filename or url filled in
				return null;
			} else {
				setBlobInSessionVar(lFileName, bloId);
				setBlobInSessionVar(lUrl, bloId);
			}
		}
		return lBlob;
	}

	/**
	 * Gets the blob src, the url of the blob.
	 *
	 * @param bloId the blo id
	 *
	 * @return the blob src
	 */
	public String getBlobSrc(String bloId) {
		if (bloId.equals(""))
			return "";
		IEBlob lBlob = getBlob(bloId);
		if (lBlob != null) {
			return getBlobUrl(sSpring.getCase(), "" + lBlob.getBloId(), lBlob.getFilename());
		}
		return "";
	}

	/**
	 * Gets the media src, the url of the media.
	 *
	 * @param bloId the blo id
	 *
	 * @return the media src
	 */
	public String getMediaSrc(String bloId) {
		return getBlobSrc(bloId);
	}

	/**
	 * Gets the image src, the url of the image.
	 *
	 * @param bloId the blo id
	 *
	 * @return the image src
	 */
	public String getImageSrc(String bloId) {
		return getBlobSrc(bloId);
	}

	/**
	 * Gets the audio src, the url of the audio.
	 *
	 * @param bloId the blo id
	 *
	 * @return the audio src
	 */
	public String getAudioSrc(String bloId) {
		return getBlobSrc(bloId);
	}

	/**
	 * Gets blob media.
	 *
	 * @param bloId the blo id
	 *
	 * @return the media
	 */
	public AMedia getBlobMedia(String bloId) {
		IEBlob lBlob = getBlob(bloId);
		if (lBlob == null) {
			return null;
		}
		byte[] lContent = getBlobContent(bloId);
		if (lContent == null) {
			return null;
		}
		return new AMedia(lBlob.getFilename(), lBlob.getFormat(), lBlob.getContenttype(), lContent);
	}

	/**
	 * Sets blob media. If blob with bloId does not exist, it is created.
	 *
	 * @param bloId  the blo id
	 * @param aMedia the a media
	 *
	 * @return the (new) blob id
	 */
	public String setBlobMedia(String bloId, Media aMedia) {
		return setBlobMedia(bloId, aMedia, null, AppConstants.blobtypeDatabase);
	}

	/**
	 * Sets blob media. If blob with bloId does not exist, it is created.
	 *
	 * @param bloId     the blo id
	 * @param aMedia    the a media
	 * @param aName     the a name
	 * @param aBlobtype the a blob type
	 *
	 * @return the (new) blob id
	 */
	public String setBlobMedia(String bloId, Media aMedia, String aName, String aBlobtype) {
		IBlobManager lBean = sSpring.getBlobManager();
		IEBlob lBlob = null;
		if (bloId.equals(""))
			lBlob = lBean.getNewBlob();
		else
			lBlob = getBlob(bloId);
//			lBlob = lBean.getBlob(Integer.parseInt(bloId));
		if (lBlob == null) {
			bloId = "";
			lBlob = lBean.getNewBlob();
		}
		byte[] lContent = null;
		if (aMedia == null || aMedia.getName() == null || aMedia.getContentType() == null
				|| aMedia.getFormat() == null) {
			lBlob.setName("");
			lBlob.setFilename("");
			lBlob.setUrl("");
			lBlob.setContenttype("");
			lBlob.setFormat("");
		} else {
			if (StringUtils.isEmpty(aName)) {
				aName = aMedia.getName();
			}
			lBlob.setName(aName);
			lBlob.setFilename("");
			lBlob.setUrl("");
			if (aBlobtype.equals(AppConstants.blobtypeDatabase)) {
				lBlob.setFilename(aName);
			} else {
				lBlob.setUrl(aName);
			}
			lBlob.setContenttype(aMedia.getContentType());
			lBlob.setFormat(aMedia.getFormat());
			lContent = mediaToByteArray(aMedia);
		}
		if (bloId.equals(""))
			lBean.newFileBlob(lBlob, lContent);
		else
			lBean.updateFileBlob(lBlob, lContent);
		return ("" + lBlob.getBloId());
	}

	/**
	 * Gets the blob file.
	 *
	 * @param bloId the blo id
	 *
	 * @return the file
	 */
	public File getBlobFile(String bloId) {
		IBlobManager lBean = sSpring.getBlobManager();
		IEBlob lBlob = null;
		if ((bloId == null) || (bloId.equals("")))
			return null;
//		lBlob = lBean.getBlob(Integer.parseInt(bloId));
		lBlob = getBlob(bloId);
		if (lBlob == null)
			return null;
		return lBean.getFile(lBlob.getBloId(), lBlob.getFilename());
	}

	/**
	 * Replaces file blob. aFileName is located outside blob folder. File is copied
	 * to blob folder.
	 *
	 * @param aBloId      the a blo id
	 * @param aFileName   the a file name
	 * @param aRemoveFile the a remove file
	 *
	 * @return the blo id
	 */
	public String replaceFileBlob(String aBloId, String aFileName, boolean aRemoveFile) {
		if (!aBloId.equals("")) {
			// remove old blob
			deleteBlob(aBloId);
		}
		IBlobManager lBean = sSpring.getBlobManager();
		IEBlob lBlob = lBean.getNewBlob();
		lBlob.setName("");
		lBlob.setFilename("");
		lBlob.setContenttype("");
		lBlob.setFormat("");
		lBlob.setUrl("");
		lBean.newBlob(lBlob);
		String lDiskPath = sSpring.getAppManager().getAbsoluteBlobPath();
		// create file in sub folder bloId
		String lSubPath = "" + lBlob.getBloId() + "/";
		lDiskPath = lDiskPath + lSubPath;
		sSpring.getFileManager().createDir(lDiskPath);
		String lFileNameWithoutPath = aFileName;
		int lPos = lFileNameWithoutPath.lastIndexOf("/");
		if (lPos > 0) {
			lFileNameWithoutPath = lFileNameWithoutPath.substring(lPos + 1);
		}
		if (!sSpring.getFileManager().copyFile(aFileName, lDiskPath + lFileNameWithoutPath)) {
			return "";
		}
		String lExtension = "";
		lPos = aFileName.lastIndexOf(".");
		if (lPos > 0) {
			lExtension = aFileName.substring(lPos + 1);
		}
		lBlob.setName(lFileNameWithoutPath);
		lBlob.setFilename(lFileNameWithoutPath);
		// TODO add content types for other extensions
		if (lExtension.equals("mp4")) {
			lBlob.setContenttype("video/mp4");
		}
		lBlob.setFormat(lExtension);
		lBean.saveBlob(lBlob);
		if (aRemoveFile) {
			sSpring.getFileManager().deleteFile(aFileName);
		}
		return "" + lBlob.getBloId();
	}

	/**
	 * Replaces url blob.
	 *
	 * @param aBloId   the blob id
	 * @param aUrlName the name of the url
	 *
	 * @return the blob id
	 */
	public String replaceUrlBlob(String aBloId, String aUrlName) {
		if (!aBloId.equals("")) {
			// remove old blob
			deleteBlob(aBloId);
		}
		IBlobManager lBean = sSpring.getBlobManager();
		IEBlob lBlob = lBean.getNewBlob();
		lBlob.setName("");
		lBlob.setFilename("");
		lBlob.setContenttype("");
		lBlob.setFormat("");
		lBlob.setUrl("");
		lBean.newBlob(lBlob);
		String lExtension = "";
		int lPos = aUrlName.lastIndexOf(".");
		if (lPos > 0) {
			lExtension = aUrlName.substring(lPos + 1);
		}
		lBlob.setName(aUrlName);
		lBlob.setUrl(aUrlName);
		lBlob.setContenttype(sSpring.getAppManager().getContentType(aUrlName));
		// TODO add content types for other extensions
		/*
		 * if (lExtension.equals("mp4")) { lBlob.setContenttype("video/mp4"); }
		 */
		lBlob.setFormat(lExtension);
		lBean.saveBlob(lBlob);
		return "" + lBlob.getBloId();
	}

	/**
	 * Gets the blob content.
	 *
	 * @param bloId the blo id
	 *
	 * @return the content
	 */
	public byte[] getBlobContent(String bloId) {
		IBlobManager lBean = sSpring.getBlobManager();
		IEBlob lBlob = null;
		if ((bloId == null) || (bloId.equals("")))
			return null;
//		lBlob = lBean.getBlob(Integer.parseInt(bloId));
		lBlob = getBlob(bloId);
		if (lBlob == null)
			return null;
		return lBean.readFile(lBlob.getBloId(), lBlob.getFilename());
	}

	/**
	 * Sets blob content. If blob with bloId does not exist, it is created.
	 *
	 * @param bloId    the blo id
	 * @param aContent the a content
	 *
	 * @return the (new) blob id
	 */
	public String setBlobContent(String bloId, byte[] aContent) {
		if (aContent == null)
			return "";
		if (aContent.length == 0)
			return "";
		IBlobManager lBean = sSpring.getBlobManager();
		IEBlob lBlob = null;
		if (bloId.equals(""))
			lBlob = lBean.getNewBlob();
		else
//			lBlob = lBean.getBlob(Integer.parseInt(bloId));
			lBlob = getBlob(bloId);
		if (lBlob == null) {
			bloId = "";
			lBlob = lBean.getNewBlob();
		}
		lBlob.setName("content");
		lBlob.setFilename("content");
		lBlob.setContenttype("content");
		lBlob.setFormat("content");
		lBlob.setUrl("");
		if (bloId.equals(""))
			lBean.newFileBlob(lBlob, aContent);
		else
			lBean.updateFileBlob(lBlob, aContent);
		return ("" + lBlob.getBloId());
	}

	/**
	 * Media to byte array. Converts the stream within aMedia to a byte array.
	 *
	 * @param aMedia the a media
	 *
	 * @return the byte[]
	 */
	public byte[] mediaToByteArray(Media aMedia) {
		return sSpring.getFileHelper().mediaToByteArray(aMedia);
	}

	/**
	 * Sets blob url. If blob with bloId does not exist, it is created.
	 *
	 * @param bloId the blo id
	 * @param aUrl  the a url
	 *
	 * @return the (new) blob id
	 */
	public String setBlobUrl(String bloId, String aUrl) {
		if (aUrl != null) {
			IBlobManager lBean = sSpring.getBlobManager();
			IEBlob lBlob = null;
			if (!bloId.equals(""))
				lBlob = getBlob(bloId);
//				lBlob = lBean.getBlob(Integer.parseInt(bloId));
			if ((lBlob == null) || (bloId.equals(""))) {
				bloId = "";
				lBlob = lBean.getNewBlob();
			} else {
//				if existing blob and filename is set then file on disk has to be deleted
				String lFilename = lBlob.getFilename();
				if ((lFilename != null) && (!lFilename.equals(""))) {
					lBean.deleteFileAndFolder(lBlob);
				}
			}
			lBlob.setName(aUrl);
			lBlob.setFilename("");
			lBlob.setContenttype("");
			lBlob.setFormat("");
			lBlob.setUrl(aUrl);
			if (bloId.equals(""))
				lBean.newBlob(lBlob);
			else
				lBean.updateBlob(lBlob);
			return ("" + lBlob.getBloId());
		}
		return "";
	}

	/**
	 * Deletes blob.
	 *
	 * @param bloId the blo id
	 */
	public void deleteBlob(String bloId) {
		if (bloId.equals(""))
			return;
		sSpring.getBlobManager().deleteBlob(Integer.parseInt(bloId));
	}

	/**
	 * Gets the tag background blob id.
	 *
	 * @param aParentTag the a parent tag
	 *
	 * @return the tag background blob id
	 */
	public String getTagBackgroundBlobId(IXMLTag aParentTag) {
		IXMLTag lBackgroundTag = getBackgroundTag(aParentTag);
		if (lBackgroundTag == null)
			return "";
		return lBackgroundTag.getChildValue("picture");
	}

	/**
	 * Gets the background tag.
	 *
	 * @param aParentTag the a parent tag
	 *
	 * @return the background tag
	 */
	public IXMLTag getBackgroundTag(IXMLTag aParentTag) {
		if (aParentTag == null)
			return null;
		List<IXMLTag> lTags = aParentTag.getChilds("background");
		for (IXMLTag lTag : lTags) {
			String lStatus = lTag.getCurrentStatusAttribute(AppConstants.statusKeyOpened);
			boolean lOpened = (lStatus.equals(AppConstants.statusValueTrue));
			if (lOpened)
				// get first opened
				return lTag;
		}
		return null;
	}

	/**
	 * Checks if aTag has url. For instance a background tag.
	 *
	 * @param aTag the a tag
	 *
	 * @return true, if successful
	 */
	public boolean hasUrl(IXMLTag aTag) {
		return (!getUrl(aTag).equals(""));
	}

	/**
	 * Gets the url of aTag. For instance a background tag.
	 *
	 * @param aTag the a tag
	 *
	 * @return the url
	 */
	public String getUrl(IXMLTag aTag) {
		return getUrl(sSpring.getCase(), aTag);
	}

	/**
	 * Gets the url of aTag. For instance a background tag.
	 *
	 * @param aTag          the a tag
	 * @param aChildTagName the a child tag name
	 *
	 * @return the url
	 */
	public String getUrl(IXMLTag aTag, String aChildTagName) {
		return getUrl(sSpring.getCase(), aTag, aChildTagName);
	}

	/**
	 * Gets the url of aTag within aCase. For instance a background tag.
	 *
	 * @param aCase the a case
	 * @param aTag  the a tag
	 *
	 * @return the url
	 */
	public String getUrl(IECase aCase, IXMLTag aTag) {
		return getUrl(aCase, aTag, "");
	}

	/**
	 * Gets the url of aTag within aCase. For instance a background tag. If
	 * aChildTagName is not empty a specific child tag of aTag is picked.
	 *
	 * @param aCase         the a case
	 * @param aTag          the a tag
	 * @param aChildTagName the a child tag name
	 *
	 * @return the url
	 */
	public String getUrl(IECase aCase, IXMLTag aTag, String aChildTagName) {
		if (aCase == null)
			return "";
		if (aTag == null)
			return "";
		// NOTE status url can be set as status attribute for aTag. If so, return it
		String lStatusUrl = aTag.getCurrentStatusAttribute(AppConstants.statusKeyUrl);
		if (!lStatusUrl.equals("")) {
			return lStatusUrl;
		}
		/* ????
		// NOTE status url can be set for aTag. If so, return it
		lStatusUrl = aTag.getCurrentStatusAttribute(AppConstants.statusKeyUrl);
		if (!lStatusUrl.equals("")) {
			return lStatusUrl;
		}
		*/
		boolean lChildTagNameIsEmpty = StringUtils.isEmpty(aChildTagName);
		IXMLTag lBlobChild = null;
		String lBlobid = "";
		// NOTE for user generated content the blob tag can be a child of the status
		// child tag of the status tag
		IXMLTag lStatusTagStatusChildTag = sSpring.getXmlManager().getStatusTagStatusChildTag(aTag);
		for (String childTagName : AppConstants.nodeChildTagsWithBlob) {
			if (lChildTagNameIsEmpty || childTagName.equals(aChildTagName)) {
				if (lStatusTagStatusChildTag != null) {
					lBlobChild = lStatusTagStatusChildTag.getChild(childTagName);
					if (lBlobChild != null) {
						lBlobid = lBlobChild.getValue();
					}
				}
				if (lBlobid.equals("")) {
					lBlobChild = aTag.getChild(childTagName);
					if (lBlobChild != null) {
						lBlobid = lBlobChild.getValue();
					}
				}
				if (!lBlobid.equals("")) {
					break;
				}
			}
		}
		if (lBlobChild == null || lBlobid.equals(""))
			return "";
		String lBlobtype = lBlobChild.getAttribute(AppConstants.keyBlobtype);
		if (lBlobtype.equals("")) {
			lBlobtype = AppConstants.blobtypeDatabase;
		}
		return getUrl(aCase, lBlobid, lBlobtype);
	}

	/**
	 * Gets the url of aTag within current case. For instance a background tag. If
	 * aChildTagName is not empty a specific child tag of aTag is picked.
	 *
	 * @param aBlobid   the a blob id
	 * @param aBlobtype the a blob type
	 *
	 * @return the url
	 */
	public String getUrl(String aBlobid, String aBlobtype) {
		return getUrl(sSpring.getCase(), aBlobid, aBlobtype);
	}

	/**
	 * 
	 * Gets the url of aTag within current. For instance a background tag. If
	 * aChildTagName is not empty a specific child tag of aTag is picked.
	 *
	 * @param aBlobid   the a blob id
	 * @param aBlobtype the a blob type
	 *
	 * @return the url
	 */
	public String getUrl(IECase aCase, String aBlobid, String aBlobtype) {
		IEBlob lBlob = getBlob(aBlobid);
		if (lBlob == null)
			return "";
		if (aBlobtype.equals("")) {
			aBlobtype = AppConstants.blobtypeDatabase;
		}
		String lUrl = "";
		if (aBlobtype.equals(AppConstants.blobtypeDatabase)) {
			// blob on disk in folder blob
			if (!StringUtils.isEmpty(lBlob.getFilename())) {
				lUrl = getBlobUrl(aCase, aBlobid, lBlob.getFilename());
			}
		} else if (aBlobtype.equals(AppConstants.blobtypeIntUrl)) {
			if (!StringUtils.isEmpty(lBlob.getUrl())) {
				lUrl = getIntUrl(aCase, lBlob.getUrl(), false);
			}
		} else if (aBlobtype.equals(AppConstants.blobtypeExtUrl)) {
			if (!StringUtils.isEmpty(lBlob.getUrl())) {
				lUrl = getExtUrl(lBlob.getUrl());
			}
		}
		return lUrl;
	}

	/**
	 * Gets blob url. Url plus path to blob folder.
	 *
	 * @param aCase                the a case
	 * @param aBlobId              the a blob id
	 * @param aFileNameWithoutPath the a file name without path
	 *
	 * @return the url
	 */
	public String getBlobUrl(IECase aCase, String aBlobId, String aFileNameWithoutPath) {
		// get case blob url
		String lBlobDefUrl = VView.getInitParameter("emergo.blob.path") + aBlobId + "/" + aFileNameWithoutPath;
		if (!blobFileExists(aBlobId, aFileNameWithoutPath)) {
			// NOTE if blob file does not exist it might be a (recorded) video on the
			// streaming server
			lBlobDefUrl = getCaseBlobUrlDedicated(aCase, aBlobId, aFileNameWithoutPath);
		}

		// NOTE if case supports multiple languages
		if (sSpring.inRun() && aCase.getMultilingual()) {
			// NOTE 'translated' blob file may exist in streaming folder in sub folder given
			// by getPropLocaleString() and within sub folder 'blob'.
			// So not within a sub folder given by aBlobId.
			String lUrl = getIntUrlOrEmptyString(aCase,
					VView.getInitParameter("emergo.translatedblob.path").substring(1) + aFileNameWithoutPath, true,
					false);
			if (!StringUtils.isEmpty(lUrl)) {
				return lUrl;
			}
			if (sSpring.getFileManager()
					.fileExists(sSpring.getAppManager().getAbsoluteBlobPath() + aBlobId + "/" + aFileNameWithoutPath)) {
				return lBlobDefUrl;
			}
			// NOTE if not on this server, search on external server if defined:
			lUrl = getIntUrlOrEmptyString(aCase,
					VView.getInitParameter("emergo.translatedblob.path").substring(1) + aFileNameWithoutPath, true,
					true);
			if (!StringUtils.isEmpty(lUrl)) {
				return lUrl;
			}
		}
		return lBlobDefUrl;
	}

	/**
	 * Exists blob file?
	 *
	 * @param aBlobId         the a blob id
	 * @param aUrlWithoutPath the a url without path
	 *
	 * @return boolean
	 */
	protected boolean blobFileExists(String aBlobId, String aUrlWithoutPath) {
		String lSubPath = aBlobId + "/";
		String lDiskPath = sSpring.getAppManager().getAbsoluteBlobPath() + lSubPath;
		String lUrlWithoutPars = vView.getUrlWithoutPars(aUrlWithoutPath);
		return sSpring.getFileManager().fileExists(lDiskPath + lUrlWithoutPars);
	}

	/**
	 * Gets case int url from dedicated streaming server. Url plus path to streaming
	 * folder.
	 *
	 * @param aCase           the a case, possibly null
	 * @param aBlobId         the a blob id
	 * @param aUrlWithoutPath the a url without path
	 *
	 * @return the url
	 */
	protected String getCaseBlobUrlDedicated(IECase aCase, String aBlobId, String aUrlWithoutPath) {
		// NOTE streaming server is only used for (recorded) videos
		FtpHelper lFtpHelper = new FtpHelper();
		// NOTE if FTP server defined in emergo.properties, a dedicated streaming server
		// is used
		if (lFtpHelper.ftpServerDefined()) {
			String lServerId = sSpring.getPropEmergoServerId();
			// if video recorded or video uploaded by student, stored url already contains
			// server id and case id sub paths
			boolean lIsStudentRecordingOrUpload = aUrlWithoutPath.startsWith(lServerId + "/" + aCase.getCasId() + "/");
			if (lIsStudentRecordingOrUpload) {
				// video recording by student
				String lSubPath = PropsValues.STREAMING_SERVER_FTP_RUNVIDEOS_SUBPATH;
				if (lFtpHelper.ftpFileExists(lSubPath, aUrlWithoutPath)) {
					String lRelativePath = (lSubPath + aUrlWithoutPath).replaceAll("/",
							PropsValues.STREAMING_PLAYER_PATH_SEPARATOR);
					return CDesktopComponents.vView().getStreamingPlayerApplication() + "/" + lRelativePath;
				}
			} else {
				// blob video
				String lUrl = aCase.getCasId() + VView.getInitParameter("emergo.blob.path") + aBlobId + "/"
						+ aUrlWithoutPath;
				String lSubPath = PropsValues.STREAMING_SERVER_FTP_CASEVIDEOS_SUBPATH + lServerId + "/";
				if (lFtpHelper.ftpFileExists(lSubPath, lUrl)) {
					String lRelativePath = (lSubPath + lUrl).replaceAll("/",
							PropsValues.STREAMING_PLAYER_PATH_SEPARATOR);
					return CDesktopComponents.vView().getStreamingPlayerApplication() + "/" + lRelativePath;
				}
			}
		}
		return "";
	}

	/**
	 * Gets int url. Url plus path to streaming folder.
	 *
	 * @param aCase                         the a case
	 * @param aUrlWithoutPath               the a url without path
	 * @param aOnlySearchForTranslatedFiles the a only search for translated files
	 *
	 * @return the url or empty string if not found
	 */
	protected String getIntUrlOrEmptyString(IECase aCase, String aUrlWithoutPath, boolean aOnlySearchForTranslatedFiles,
			boolean aSearchExt) {
		// get case int url

		List<String> lSubFolders = new ArrayList<String>();
		// NOTE if case supports multiple languages
		if (sSpring.inRun() && aCase.getMultilingual()) {
			// NOTE 'translated' file may exist in sub folder given by getPropLocaleString()
			if (!StringUtils.isEmpty(sSpring.getPropLocaleString())) {
				// first search translated file
				lSubFolders.add(sSpring.getPropLocaleString() + "/");
			}
		}
		if (!aOnlySearchForTranslatedFiles) {
			// then search for original file
			lSubFolders.add("");
		}

		String lUrl = "";
		for (String lSubFolder : lSubFolders) {
			lUrl = getCaseIntUrl(aCase, lSubFolder + aUrlWithoutPath);
			if (!StringUtils.isEmpty(lUrl)) {
				// if found return it (cascading parent cases on internal steaming folder)
				return lUrl;
			}
		}

		for (String lSubFolder : lSubFolders) {
			if (intFileExists(0, lSubFolder + aUrlWithoutPath)) {
				// found in general streaming folder
				return getIntUrl(0, lSubFolder + aUrlWithoutPath);
			}
		}

		for (String lSubFolder : lSubFolders) {
			lUrl = getCaseIntUrlDedicated(aCase, aCase.getCasId(), lSubFolder + aUrlWithoutPath);
			if (!StringUtils.isEmpty(lUrl)) {
				return lUrl;
			}
		}

		if (aSearchExt) {
			// search on external server not reliable, so only do this as last option
			String lExtStreamingPath = PropsValues.STREAMING_PATH_EXT;
			if (vView.isAbsoluteUrl(lExtStreamingPath)) {
				for (String lSubFolder : lSubFolders) {
					// check if file is present on an external server
					lUrl = getCaseIntUrlExt(aCase, aCase.getCasId(), lExtStreamingPath, lSubFolder + aUrlWithoutPath);
					if (!StringUtils.isEmpty(lUrl)) {
						return lUrl;
					}
				}
			}
		}

		return "";
	}

	/**
	 * Gets int url. Url plus path to streaming folder.
	 *
	 * @param aCase                         the a case
	 * @param aUrlWithoutPath               the a url without path
	 * @param aOnlySearchForTranslatedFiles the a only search for translated files
	 *
	 * @return the url or getIntUrl(aCase.getCasId(), aUrlWithoutPath) if not found
	 */
	public String getIntUrl(IECase aCase, String aUrlWithoutPath, boolean aOnlySearchForTranslatedFiles) {
		// get case int url
		String lUrl = getIntUrlOrEmptyString(aCase, aUrlWithoutPath, aOnlySearchForTranslatedFiles, false);
		if (!StringUtils.isEmpty(lUrl)) {
			return lUrl;
		}
		lUrl = getIntUrl(aCase.getCasId(), aUrlWithoutPath);
		if (!intFileExists(0, lUrl)) {
			String lTmpUrl = getIntUrlOrEmptyString(aCase, aUrlWithoutPath, aOnlySearchForTranslatedFiles, true);
			if (!StringUtils.isEmpty(lTmpUrl))
				return lTmpUrl;
		}
		return lUrl;
	}

	/**
	 * Gets ext url, possibly converted, for instance for youtube.
	 *
	 * @param aUrl the a url
	 *
	 * @return the url
	 */
	public String getExtUrl(String aUrl) {
		if (aUrl.indexOf("http://youtu.be/") == 0) {
			// NOTE converts shared youtube url to embedded url
			aUrl = aUrl.replace("youtu.be", "www.youtube.com/embed");
		} else if (aUrl.indexOf("http://www.youtube.com/watch?v=") == 0) {
			// NOTE converts shared youtube url to embedded url
			aUrl = aUrl.replace("http://www.youtube.com/watch?v=", "http://www.youtube.com/embed/");
			aUrl = aUrl.replaceFirst("&", "?");
		} else if (aUrl.indexOf("<iframe") == 0) {
			// NOTE converts shared youtube url to embedded url
			int lIndex = aUrl.indexOf("src=\"");
			if (lIndex > 0) {
				String lUrl = aUrl.substring(lIndex + "src=\"".length());
				lIndex = lUrl.indexOf("\"");
				if (lIndex > 0) {
					aUrl = lUrl.substring(0, lIndex);
				}
			}
		}
		return aUrl;
	}

	/**
	 * Gets case int url. Url plus path to streaming folder.
	 *
	 * @param aCase           the a case
	 * @param aUrlWithoutPath the a url without path
	 *
	 * @return the url
	 */
	protected String getCaseIntUrl(IECase aCase, String aUrlWithoutPath) {
		// local url to file on streaming server so put streaming path before url.
		if (intFileExists(aCase.getCasId(), aUrlWithoutPath)) {
			// found in streaming folder for case
			return getIntUrl(aCase.getCasId(), aUrlWithoutPath);
		}
		if (aCase.getCasCasId() != 0) {
			// if streaming folder of other case is used
			IECase lCase = sSpring.getCaseManager().getCase(aCase.getCasCasId());
			if (lCase != null) {
				// cascading
				return getCaseIntUrl(lCase, aUrlWithoutPath);
			}
		}
		return "";
	}

	/**
	 * Exists int file?
	 *
	 * @param aCasId          the a cas id
	 * @param aUrlWithoutPath the a url without path
	 *
	 * @return boolean
	 */
	protected boolean intFileExists(int aCasId, String aUrlWithoutPath) {
		String lSubPath = "";
		if (aCasId > 0) {
			lSubPath = "" + aCasId + "/";
		}
		String lDiskPath = sSpring.getAppManager().getAbsoluteStreamingPath() + lSubPath;
		String lUrlWithoutPars = vView.getUrlWithoutPars(aUrlWithoutPath);
		return sSpring.getFileManager().fileExists(lDiskPath + lUrlWithoutPars);
	}

	/**
	 * Gets int url. Url plus path to streaming folder.
	 *
	 * @param aCasId          the a cas id
	 * @param aUrlWithoutPath the a url without path
	 *
	 * @return the url
	 */
	protected String getIntUrl(int aCasId, String aUrlWithoutPath) {
		String lSubPath = "";
		if (aCasId > 0) {
			lSubPath = "" + aCasId + "/";
		}
		return VView.getInitParameter("emergo.streaming.path") + lSubPath + aUrlWithoutPath;
	}

	/**
	 * Gets case int url from dedicated streaming server. Url plus path to streaming
	 * folder.
	 *
	 * @param aCase           the a case, possibly null
	 * @param aCasId          the case id of the external case
	 * @param aUrlWithoutPath the a url without path
	 *
	 * @return the url
	 */
	protected String getCaseIntUrlDedicated(IECase aCase, int aCasId, String aUrlWithoutPath) {
		FtpHelper lFtpHelper = new FtpHelper();
		// NOTE if FTP server defined in emergo.properties, a dedicated streaming server
		// is used
		if (lFtpHelper.ftpServerDefined()) {
			String lServerId = sSpring.getPropEmergoServerId();
			// if video recorded by student, stored url already contains server id and case
			// id sub paths
			boolean lIsStudentRecording = aUrlWithoutPath.startsWith(lServerId + "/" + aCasId + "/");
			if (lIsStudentRecording) {
				// video recording by student
				String lSubPath = PropsValues.STREAMING_SERVER_FTP_RUNVIDEOS_SUBPATH;
				if (lFtpHelper.ftpFileExists(lSubPath, aUrlWithoutPath)) {
					String lRelativePath = (lSubPath + aUrlWithoutPath).replaceAll("/",
							PropsValues.STREAMING_PLAYER_PATH_SEPARATOR);
					return CDesktopComponents.vView().getStreamingPlayerApplication() + "/" + lRelativePath;
				}
			} else {
				// case video
				String lUrl = aCasId + "/" + aUrlWithoutPath;
				String lSubPath = PropsValues.STREAMING_SERVER_FTP_CASEVIDEOS_SUBPATH + lServerId + "/";
				if (lFtpHelper.ftpFileExists(lSubPath, lUrl)) {
					String lRelativePath = (lSubPath + lUrl).replaceAll("/",
							PropsValues.STREAMING_PLAYER_PATH_SEPARATOR);
					return CDesktopComponents.vView().getStreamingPlayerApplication() + "/" + lRelativePath;
				}
				// if url not found possibly video belongs to referenced case
				if (aCase != null) {
					int lCasId = aCase.getCasCasId();
					if (lCasId != 0) {
						IECase lCase = sSpring.getCaseManager().getCase(lCasId);
						String lTest = getCaseIntUrlDedicated(lCase, lCasId, aUrlWithoutPath);
						if (!lTest.equals(""))
							return lTest;
					}
				}
			}
		}
		return "";
	}

	/**
	 * Gets case int url from external streaming server. Url plus path to streaming
	 * folder.
	 *
	 * @param aCase             the a case, possibly null
	 * @param aCasId            the case id of the external case
	 * @param aExtStreamingPath the a url path to the external streaming server
	 * @param aUrlWithoutPath   the a url without path
	 *
	 * @return the url
	 */
	protected String getCaseIntUrlExt(IECase aCase, int aCasId, String aExtStreamingPath, String aUrlWithoutPath) {
		// Check if file is present on an external server
		// NOTE methods are not reliable because secured external server might return
		// default 'Access denied' file, with HTTP 200 response code
		String lUrl = toUTFUrl(aExtStreamingPath + aCasId + "/" + aUrlWithoutPath);
		Boolean lExists = false;
		HttpURLConnection con = null;
		try {
			HttpURLConnection.setFollowRedirects(false);
			con = (HttpURLConnection) new URL(lUrl).openConnection();
			con.setInstanceFollowRedirects(false);
			con.setRequestMethod("HEAD");
			lExists = (con.getResponseCode() == HttpURLConnection.HTTP_OK);
		} catch (Exception e) {
			lExists = false;
		}

		if (lExists) {
			return lUrl;
		}
		if (aCase != null) {
			int lCasId = aCase.getCasCasId();
			if (lCasId != 0) {
				IECase lCase = sSpring.getCaseManager().getCase(lCasId);
				return getCaseIntUrlExt(lCase, lCasId, aExtStreamingPath, aUrlWithoutPath);
			}
		}
		return "";
	}

	/**
	 * Get UTF-encoded url.
	 *
	 * @param aUrl the ISO-encoded url
	 */
	public String toUTFUrl(String aUrl) {
		boolean lIsRelativePath = aUrl != null && aUrl.indexOf("../") == 0;
		if (lIsRelativePath) {
			// TODO make ISO-encoded url
			return aUrl;
		}
		URL url;
		URI uri;
		String lUTFUrl = aUrl;
		boolean lIsAbsolutePath = vView.isAbsoluteUrl(lUTFUrl);
		if (!lIsAbsolutePath) {
			// NOTE add root url, because try/catch code needs absolute path
			lUTFUrl = vView.getEmergoRootUrl() + lUTFUrl;
		}
		try {
			url = new URL(lUTFUrl);
			uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(),
					url.getQuery(), url.getRef());
			lUTFUrl = uri.toASCIIString();
		} catch (URISyntaxException | MalformedURLException e) {
			_log.error(e);
		}
		if (!lIsAbsolutePath) {
			// NOTE remove root url
			lUTFUrl = lUTFUrl.substring(vView.getEmergoRootUrl().length(), lUTFUrl.length());
		}
		return lUTFUrl;
	}

	/**
	 * Shows href in popup.
	 *
	 * @param aHref the a href
	 */
	public void showHref(String aHref) {
		if (aHref.equals(""))
			return;
		vView.redirectToViewNotUnique(convertHrefForCertainMediaTypes(aHref), "null");
	}

	/**
	 * Convert href for certain media types. If user has other player coupled to wmv
	 * or flv enforce correct player by using zul files to show file, because other
	 * players may not support streaming video.
	 *
	 * @param aHref the a href
	 *
	 * @return (new) href
	 */
	public String convertHrefForCertainMediaTypes(String aHref) {
		if (aHref.equals("")) {
			return aHref;
		}
		String lFileExtension = "";
		int lPos = aHref.lastIndexOf(".");
		if (lPos > 0) {
			lFileExtension = (aHref.substring(lPos + 1, aHref.length())).toLowerCase(Locale.ENGLISH);
		}
		// urls can have parameters
		lPos = lFileExtension.indexOf("?");
		if (lPos > 0) {
			// if a file is needed remove parameters
			lFileExtension = lFileExtension.substring(0, lPos);
		}
		// NOTE if user has other player coupled to file extensions enforce correct
		// player by using zul files
		String lSkinPath = "";
		if (sSpring.getCase() != null) {
			lSkinPath = sSpring.getSCaseSkinHelper().getCaseSkinPath(sSpring.getCase());
		}
		String lRunMediaView = "";
		if (lFileExtension.matches("wmv|wma|wav"))
			lRunMediaView = VView.v_run_windowsmedia_fr;
		else if (lFileExtension.equals("mpg"))
			lRunMediaView = VView.v_run_windowsmedia_mpg_fr;
		else if (lFileExtension.matches("flv|mp4|webm|ogv|fla|mp3"))
			lRunMediaView = VView.v_run_flash_ref_fr;
		else if (lFileExtension.equals("swf"))
			lRunMediaView = VView.v_run_flash_swf_fr;
		else if (lFileExtension.equals("unity3d"))
			lRunMediaView = VView.v_run_unity_fr;
		else {
			if (CDesktopComponents.vView().isRtmpUrl(aHref))
				lRunMediaView = VView.v_run_flash_ref_fr;
			else
				lRunMediaView = VView.v_run_website_fr;
		}
		String lConverted = aHref;
		if (!lRunMediaView.equals("")) {
			if (!vView.isAbsoluteUrl(aHref)) {
				aHref = vView.getEmergoRootUrl() + vView.getEmergoWebappsRoot() + aHref;
			}
			String lRefStore = "";
			// NOTE URL encode url parameters
			int lParamCount = 0;
			lPos = aHref.indexOf("?");
			ArrayList<String> lParamList = new ArrayList<String>();
			if (lPos > 0 && lPos < (aHref.length() - 1)) {
				// has parameters
				String lParams = aHref.substring(lPos + 1);
				aHref = aHref.substring(0, lPos);
				while (lPos > 0) {
					lPos = lParams.indexOf("&");
					String lStr = "";
					if (lPos < 0) {
						lStr = lParams;
					} else {
						if (lPos > 0) {
							lStr = lParams.substring(0, lPos);
							lParams = lParams.substring(lPos + 1);
						} // else error in aHRef parameters
					}
					if (!(lStr.equals(""))) {
						try {
							lParamList.add(URLEncoder.encode(lStr, "UTF-8"));
							lParamCount++;
						} catch (UnsupportedEncodingException e) {
							_log.error(e);
						}
					}
				}
			}
			try {
				lRefStore = aHref;
				// also encode aHref, because it will be passed as parameter in an url string
				// but when collecting that request parameter it is decoded, so we need to
				// compare it to the original value
				aHref = URLEncoder.encode(aHref, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				_log.error(e);
			}
			ArrayList<String> lSessRefs = new ArrayList<String>();
			Object lSessVar = cControl.getRunSessAttr(AppConstants.sessKeyRunMediaRef);
			if (lSessVar != null)
				lSessRefs = (ArrayList<String>) lSessVar;
			// check if href is in the allowed list
			int lRefPos = lSessRefs.indexOf(lRefStore);
			if (lRefPos < 0) {
				// if not, add it
				lSessRefs.add(lRefStore);
				ArrayList<String> lSessViewers = new ArrayList<String>();
				lSessVar = cControl.getRunSessAttr(AppConstants.sessKeyRunMediaViewer);
				if (!(lSessVar == null))
					lSessViewers = (ArrayList<String>) lSessVar;
				lSessViewers.add(lRunMediaView);
				cControl.setRunSessAttr(AppConstants.sessKeyRunMediaViewer, lSessViewers);
				cControl.setRunSessAttr(AppConstants.sessKeyRunMediaRef, lSessRefs);
				lRefPos = lSessRefs.size() - 1;
			}
			lConverted = CDesktopComponents.vView().uniqueView(lSkinPath + lRunMediaView) + "&sessmediaind=" + lRefPos
					+ "&url=" + aHref;
			for (int i = 0; i < lParamCount; i++) {
				lConverted = lConverted + "&param" + i + "=" + lParamList.get(i);
			}
		}
		return lConverted;
	}

}
