/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.zkspring;

import java.util.List;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IECaseRole;

public class SReferencedDataTagHelper {

	protected SSpring sSpring;
	
	public SReferencedDataTagHelper(SSpring sSpring) {
		this.sSpring = sSpring;
	}

	/**
	 * Gets referenced data tag info within a ref child of a tag.
	 *
	 * @param aCaseComponent the a case componenent
	 * @param aTag the a tag
	 * @param aRefChildName the a ref child name
	 *
	 * @return the referenced data tag info
	 */
	public String[] getReferencedDatatagInfo(IECaseComponent aCaseComponent, IXMLTag aTag, String aRefChildName) {
		if (aCaseComponent == null || aTag == null) {
			return null;
		}
	//	get info from associated casecomponent
		IXMLTag lRefChildTag = aTag.getDefChild(aRefChildName);
		if (lRefChildTag == null) {
			return null;
		}
		String lReftype = lRefChildTag.getDefAttribute(AppConstants.defKeyReftype);
		IECaseRole lCaseRole = sSpring.getSCaseRoleHelper().getCaseRole();
		List<String> lRefIds = sSpring.getCaseHelper().getRefTagIds(lReftype, "" + AppConstants.statusKeySelectedIndex, lCaseRole, aCaseComponent, aTag);
		if (lRefIds == null || lRefIds.size() == 0) {
			return null;
		}
	//	only one ref
		String lRefId = (String)lRefIds.get(0);
		if (lRefId != null && !lRefId.equals("")) {
			String[] lIdArr = lRefId.split(",");
			if (lIdArr.length == 3) {
				return lIdArr;
			}
		}
		return null;
	}

	/**
	 * Gets referenced tag within a ref child of a tag.
	 *
	 * @param aCaseComponent the a case componenent
	 * @param aTag the a tag
	 * @param aRefChildName the a ref child name
	 *
	 * @return the referenced tag
	 */
	public IXMLTag getReferencedTag(IECaseComponent aCaseComponent, IXMLTag aTag, String aRefChildName) {
		String[] lDatatagInfo =  getReferencedDatatagInfo(aCaseComponent, aTag, aRefChildName);
		if (lDatatagInfo == null) {
			return null;
		}
		String lCacId = lDatatagInfo[1];
		String lTagId = lDatatagInfo[2];
		return sSpring.getXmlDataPlusRunStatusTag(sSpring.getCaseComponent(Integer.parseInt(lCacId)), AppConstants.statusTypeRunGroup, lTagId);
	}

	/**
	 * Gets data tag reference within a ref child of a tag.
	 *
	 * @param aCaseComponent the a case componenent
	 * @param aTag the a tag
	 * @param aRefChildName the a ref child name
	 *
	 * @return the data tag reference
	 */
	public SDatatagReference getDatatagReference(IECaseComponent aCaseComponent, IXMLTag aTag, String aRefChildName) {
		String[] lDatatagInfo =  getReferencedDatatagInfo(aCaseComponent, aTag, aRefChildName);
		if (lDatatagInfo == null) {
			return null;
		}
		String lCarId = lDatatagInfo[0];
		String lCacId = lDatatagInfo[1];
		String lTagId = lDatatagInfo[2];
		SDatatagReference lReference = new SDatatagReference();
		lReference.setCaseRole(sSpring.getSCaseRoleHelper().getCaseRole(Integer.parseInt(lCarId)));
		IECaseComponent lCaseComponent = sSpring.getCaseComponent(Integer.parseInt(lCacId));
		lReference.setCaseComponent(lCaseComponent);
		lReference.setDataTag(sSpring.getXmlDataPlusRunStatusTag(lCaseComponent, AppConstants.statusTypeRunGroup, lTagId));
		return lReference;
	}

	
	/**
	 * Determine referenced case component, e.g. notes component for logbook component
	 * 
	 * @param aCaseComponent the a case componenent
	 * @param aRunStatusType the a run status type
	 * @param aCaseComponentRefTagName the a case componenent ref tag name
	 * 
	 * @return the case component
	 */
	public IECaseComponent determineReferencedCaseComponent(IECaseComponent aCaseComponent,
			String aRunStatusType, String aCaseComponentRefTagName) {
		if (aCaseComponent == null) {
			return null;
		}
		String lCacId = "" + aCaseComponent.getCacId();
		IXMLTag lRootTag = sSpring.getXmlDataPlusRunStatusTree(lCacId, aRunStatusType);
		if (lRootTag == null) {
			return null;
		}
		// get cacid of referenced casecomponent
		IXMLTag lContentTag = lRootTag.getChild(AppConstants.contentElement);
		String[] lReferencedDatatagInfo = getReferencedDatatagInfo(aCaseComponent, lContentTag, aCaseComponentRefTagName);
		if (lReferencedDatatagInfo == null) {
			return null;
		}
		lCacId = lReferencedDatatagInfo[1];
		return sSpring.getCaseComponent(lCacId);
	}

	/**
	 * Determine referenced root tag of case component, e.g. root tag of notes notes component for logbook component
	 * 
	 * @param aCaseComponent the a case componenent
	 * @param aRunStatusType the a run status type
	 * @param aCaseComponentRefTagName the a case componenent ref tag name
	 *
	 * @return the XML tag
	 */
	public IXMLTag determineReferencedRootTag(IECaseComponent aCaseComponent,
			String aRunStatusType, String aCaseComponentRefTagName) {
		IECaseComponent lReferencedCaseComponent = determineReferencedCaseComponent(aCaseComponent, aRunStatusType, aCaseComponentRefTagName);
		if (lReferencedCaseComponent == null) {
			return null;
		}
		boolean lReferencedContentShared = (sSpring.getCurrentRunComponentStatus(
				lReferencedCaseComponent, AppConstants.statusKeyShared, AppConstants.statusTypeRunGroup)
				.equals(AppConstants.statusValueTrue));
		String lReferencedRunStatusType = "";
		if (lReferencedContentShared)
			lReferencedRunStatusType = AppConstants.statusTypeRun;
		else
			lReferencedRunStatusType = AppConstants.statusTypeRunGroup;
		return sSpring.getXmlRunStatusTree(lReferencedCaseComponent, lReferencedRunStatusType);
	}
	
}
