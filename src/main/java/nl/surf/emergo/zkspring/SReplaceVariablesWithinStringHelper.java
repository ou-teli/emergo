/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.zkspring;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * <p>
 * The Class SReplaceVariablesWithinStringHelper ...
 * </p>
 */
public class SReplaceVariablesWithinStringHelper implements Serializable {

	private static final long serialVersionUID = -5210662620900359974L;

	protected Hashtable<String, List<String>> temporaryVariables = new Hashtable<String, List<String>>();

	protected Hashtable<String, Integer> numberOfAddedTemporaryVariables = new Hashtable<String, Integer>();

	private SSpring sSpring = null;

	/**
	 * Instantiates a new SReplaceVariablesWithinStringHelper.
	 *
	 * @param sSpring the spring object to use
	 */
	public SReplaceVariablesWithinStringHelper(SSpring aSpring) {
		sSpring = aSpring;
	}

	private SSpring getSSpring() {
		if (sSpring == null)
			return CDesktopComponents.sSpring();
		return sSpring;
	}

	/**
	 * Gets all node tags per case component.
	 *
	 * @param aCaseComponents            the case component
	 * @param aOnlyPresentCaseComponents if only present case components
	 *
	 * @return the list
	 */
	protected List<List<IXMLTag>> getAllPresentNodeTagsPerCaseComponent(List<IECaseComponent> aCaseComponents,
			boolean aOnlyPresentCaseComponents) {
		List<List<IXMLTag>> lTagsForCacs = new ArrayList<List<IXMLTag>>();
		for (IECaseComponent lCaseComponent : aCaseComponents) {
			IXMLTag lRootTag = getSSpring().getXmlDataPlusRunStatusTree(lCaseComponent,
					AppConstants.statusTypeRunGroup);
			if (lRootTag != null) {
				IXMLTag lComponentTag = lRootTag.getChild(AppConstants.componentElement);
				if (lComponentTag != null) {
					boolean lComponentIsPresent = true;
					if (aOnlyPresentCaseComponents) {
						// NOTE is case component present?
						lComponentIsPresent = (!getSSpring()
								.getCurrentTagStatus(lComponentTag, AppConstants.statusKeyPresent)
								.equals(AppConstants.statusValueFalse));
					}
					if (lComponentIsPresent) {
						List<IXMLTag> lPresentNodeTags = new ArrayList<IXMLTag>();
						for (IXMLTag lNodeTag : CDesktopComponents.cScript().getNodeTags(lRootTag)) {
							if (!getSSpring().getCurrentTagStatus(lNodeTag, AppConstants.statusKeyPresent)
									.equals(AppConstants.statusValueFalse)) {
								lPresentNodeTags.add(lNodeTag);
							}
						}
						lTagsForCacs.add(lPresentNodeTags);
					}
				}
			}
		}
		return lTagsForCacs;
	}

	/**
	 * Gets run group value. The value for a certain run member.
	 *
	 * @param aStateKey the state key
	 *
	 * @return the state value
	 */
	protected String getRunGroupValue(String aStateKey) {
		if (aStateKey.equals(AppConstants.stringVarUserNameKey)) {
			if (getSSpring().getRunGroup() != null) {
				return getSSpring().getRunGroup().getName();
			}
		} else if (aStateKey.equals(AppConstants.stringVarUserRoleKey)) {
			if (getSSpring().getSCaseRoleHelper().getCaseRole() != null) {
				return getSSpring().getSCaseRoleHelper().getCaseRole().getName();
			}
		} else if (aStateKey.equals(AppConstants.stringVarCaseTimeKey)) {
			// Case time in minutes
			return Math.round(getSSpring().getCaseTime() / 60) + " " + CDesktopComponents.vView().getLabel("minutes");
		}
		return "";
	}

	/**
	 * Gets state value.
	 *
	 * @param aStateTag the state tag
	 *
	 * @return the value
	 */
	protected String getStateValue(IXMLTag aStateTag) {
		String lStateValue = getSSpring().getCurrentTagStatus(aStateTag, AppConstants.statusKeyValue);
		lStateValue = getSSpring().unescapeXML(lStateValue);
		lStateValue = lStateValue.replace(AppConstants.statusCommaReplace, ",");
		return lStateValue;
	}

	/**
	 * Gets content key value.
	 *
	 * @param aCaseComponent the a case component
	 * @param aStateTag      the state tag
	 *
	 * @return the value
	 */
	protected String getContentKeyValue(IECaseComponent aCaseComponent, IXMLTag aStateTag) {
		// get needed tag content
		IXMLTag lRefChildTag = aStateTag.getChild(AppConstants.reftagElement);
		if (lRefChildTag != null) {
			String lSelectedTagContent = lRefChildTag.getAttribute(AppConstants.nodeChildKeyTagContentName);
			if (!lSelectedTagContent.equals("")) {
				IXMLTag lReferencedTag = getSSpring().getSReferencedDataTagHelper().getReferencedTag(aCaseComponent,
						aStateTag, AppConstants.reftagElement);
				if (lReferencedTag != null) {
					String lStateValue = lReferencedTag.getChildValue(lSelectedTagContent);
					lStateValue = getSSpring().unescapeXML(lStateValue);
					// if string starts with <p> and ends with </p> these tags are removed
					return getSSpring().getCaseHelper().stripCKeditorString(lStateValue);
				}
			}
		}
		return "";
	}

	/**
	 * Gets HTML anchor tag for reference.
	 *
	 * @param aPieceTag the piece tag
	 *
	 * @return the HTML anchor tag
	 */
	protected String getHtmlAnchorTag(IXMLTag aPieceTag) {
		return getHtmlAnchorTag(aPieceTag, aPieceTag.getChildValue("name"));
	}

	/**
	 * Gets HTML anchor tag for reference.
	 *
	 * @param aPieceTag the piece tag
	 * @param aPieceTitle the piece title
	 *
	 * @return the HTML anchor tag
	 */
	protected String getHtmlAnchorTag(IXMLTag aPieceTag, String aPieceTitle) {
		String lPieceTitle = getSSpring().unescapeXML(aPieceTitle);
		String lPieceUrl = CDesktopComponents.vView().getAbsoluteUrl(getSSpring().getSBlobHelper().getUrl(aPieceTag));
		if (lPieceUrl.equals("")) {
			return lPieceTitle;
		}
		return "<a href=\"" + lPieceUrl + "\" target=\"_blank\">" + lPieceTitle + "</a>";
	}

	/**
	 * Replaces variables within a string by their current values. Also replaces
	 * references to user name, user role and case time. In Script actions,
	 * temporary variables references may be used. They will be replaced first.
	 *
	 * @param aComponentCode             the component code
	 * @param aOnlyPresentCaseComponents if only present case components
	 * @param aPrefix                    the prefix
	 * @param aPostfix                   the postfix
	 * @param aString                    the string
	 *
	 * @return the string
	 */
	protected String replaceVariablesWithinString(String aComponentCode, boolean aOnlyPresentCaseComponents,
			String aPrefix, String aPostfix, String aString) {
		boolean areStatesComponents = aComponentCode.equals("states");
		boolean areReferencesComponents = aComponentCode.equals("references");
		// get components and their tags
		List<IECaseComponent> lCaseComponents = getSSpring().getCaseComponentsByComponentCode(aComponentCode);
		List<List<IXMLTag>> lTagsForCacs = getAllPresentNodeTagsPerCaseComponent(lCaseComponents,
				aOnlyPresentCaseComponents);

		// find first key
		int lStart = aString.indexOf(aPrefix);
		int lEnd = aString.lastIndexOf(aPostfix);
		if (!(lStart >= 0 && lEnd > lStart)) {
			// no keys within string
			return aString;
		}
		lEnd = aString.indexOf(aPostfix);
		while (lStart >= 0 && lEnd > 0) {
			if (lEnd <= lStart) {
				// aPostfix is used as literal characters in string (or prefix has been
				// forgotten...)
				return aString.substring(0, lEnd + aPostfix.length()) + replaceVariablesWithinString(aComponentCode,
						aOnlyPresentCaseComponents, aPrefix, aPostfix, aString.substring(lEnd + aPostfix.length()));
			} else {
				// enable state pointers within state pointers, e.g. for indexed states
				lStart = aString.substring(0, lEnd).lastIndexOf(aPrefix);
				String lKey = aString.substring(lStart + aPrefix.length(), lEnd);
				// check for presence of reference to stored temporary variable:
				lKey = replaceTemporaryVarsWithinString(lKey, "");
				// find run group value
				String lValue = getRunGroupValue(lKey);
				boolean lValueFound = !lValue.equals("");
				if (!lValueFound) {
					// if no run group value found
					int lCaseComponentCounter = 0;
					for (List<IXMLTag> lTagsForCac : lTagsForCacs) {
						for (IXMLTag lTag : lTagsForCac) {
							if (areStatesComponents) {
								// check all state components
								if (lTag.getName().equals("state") || lTag.getName().equals("formula")) {
									// state or formula
									if (lTag.getChildValue(AppConstants.defKeyKey).equals(lKey)) {
										lValueFound = true;
										lValue = getStateValue(lTag);
										if (lValue.equals("") && (lTag.getChildValue("keytype").equals("number")
												|| lTag.getChildValue("keytype").equals("time"))) {
											lValue = "0";
										}
										break;
									}
								} else if (lTag.getName().equals("contentkey")) {
									// contentkey
									if (lTag.getChildValue(AppConstants.defKeyKey).equals(lKey)) {
										lValueFound = true;
										lValue = getContentKeyValue(lCaseComponents.get(lCaseComponentCounter), lTag);
										break;
									}
								}
							} else if (areReferencesComponents) {
								// check all reference components
								if (lTag.getName().equals("piece")) {
									// piece
									if (lTag.getChildValue("name").equals(lKey) || lTag.getChildValue("explanation").equals(lKey)) {
										lValueFound = true;
										lValue = getHtmlAnchorTag(lTag, lKey);
										break;
									}
								}
							}
						}
						if (lValueFound)
							break;
						lCaseComponentCounter++;
					}
				}
				if (!lValueFound) {
					if (areStatesComponents) {
						lValue = "STATE " + lKey + " NOT FOUND!";
						getSSpring().getSLogHelper().logAction("replaceVariablesWithinString: error: " + lValue);
					} else if (areReferencesComponents) {
						lValue = "REFERENCE " + lKey + " NOT FOUND!";
						getSSpring().getSLogHelper().logAction("replaceVariablesWithinString: error: " + lValue);
					}

					lValue = "";

				}
				aString = aString.substring(0, lStart) + lValue + aString.substring(lEnd + aPostfix.length());
				lStart = aString.indexOf(aPrefix);
				lEnd = aString.indexOf(aPostfix);
			}
		}

		return aString;
	}

	/**
	 * Replaces variables within a string by their current values. Also replaces
	 * references to user name, user role and case time. In Script actions,
	 * temporary variables references may be used. They will be replaced first.
	 *
	 * @param aString the string
	 *
	 * @return the string
	 */
	public String replaceVariablesWithinString(String aString) {
		// NOTE first handle references, because reference titles may contain states
		// NOTE look in all references components (parameter false)
		aString = replaceVariablesWithinString("references", false, AppConstants.stringLinkPrefix,
				AppConstants.stringLinkPostfix, aString);
		// NOTE only look in present states components (parameter true). States
		// components may be switched off in older games?
		aString = replaceVariablesWithinString("states", true, AppConstants.stringVarPrefix,
				AppConstants.stringVarPostfix, aString);
		return aString;
	}

	/**
	 * Inits replacing temporary variables within a number of strings.
	 */
	public void initReplaceTemporaryVarsWithinStrings() {
		if (numberOfAddedTemporaryVariables.size() > 0) {
			numberOfAddedTemporaryVariables = new Hashtable<String, Integer>();
		}
	}

	/**
	 * If replacing temporary variables within a number of strings is done. If
	 * aRollback, remove the last added values from the variables.
	 * 
	 * @param aRollback the a roll back
	 */
	public void doneReplaceTemporaryVarsWithinStrings(boolean aRollback) {
		if (aRollback && numberOfAddedTemporaryVariables.size() > 0) {
			for (Enumeration<String> keys = numberOfAddedTemporaryVariables.keys(); keys.hasMoreElements();) {
				String tempvarkey = keys.nextElement();
				Integer number = numberOfAddedTemporaryVariables.get(tempvarkey);
				if (number > 0) {
					if (temporaryVariables.containsKey(tempvarkey)) {
						List<String> tempvars = temporaryVariables.get(tempvarkey);
						while (tempvars.size() > 0 && number > 0) {
							tempvars.remove(tempvars.size() - 1);
							number--;
						}
					}
				}
			}
		}
		if (numberOfAddedTemporaryVariables.size() > 0) {
			numberOfAddedTemporaryVariables = new Hashtable<String, Integer>();
		}
	}

	/**
	 * Replaces temporary variables within a number of strings.
	 *
	 * @param aStrings         the a strings
	 * @param aTriggeredTagKey the triggered tag key
	 *
	 * @return list of string
	 */
	public List<String> replaceTemporaryVarsWithinStrings(List<String> aStrings, String aTriggeredTagKey) {
		List<String> lStrings = new ArrayList<String>();
		for (String lString : aStrings) {
			lStrings.add(replaceTemporaryVarsWithinString(lString, aTriggeredTagKey));
		}
		return lStrings;
	}

	/**
	 * Returns value of temporary variable, or empty string if not found.
	 *
	 * @param aTempVarKey the key of the temporary variable
	 *
	 * @return String the value of the temporary variable
	 */
	protected String getTempVarValue(String aTempVarKey) {
		String lTempVarValue = "";
		// NOTE aTempVarKey is determined by regular expression, length at least 1
		// character
		if (temporaryVariables.size() > 0) {
			String lTempVarKey = aTempVarKey;
			// lTempVarKey can contain index e.g. (1) or (-1)
			int lStartIndex = lTempVarKey.indexOf(AppConstants.tempVarIndexPrefix);
			int lEndIndex = lTempVarKey.indexOf(AppConstants.tempVarIndexPostfix);
			int lIndex = 0;
			boolean lUseIndex = false;
			if (lStartIndex >= 0 && lEndIndex > lStartIndex) {
				String lIndexStr = lTempVarKey.substring(lStartIndex + 1, lEndIndex);
				lTempVarKey = lTempVarKey.substring(0, lStartIndex);
				try {
					// get index
					lIndex = Integer.parseInt(lIndexStr);
					lUseIndex = true;
				} catch (NumberFormatException e) {
				}
			}
			if (temporaryVariables.containsKey(lTempVarKey)) {
				int lSize = temporaryVariables.get(lTempVarKey).size();
				if (lUseIndex) {
					if (lIndex < 0) {
						// if lIndex < 0 get n-1-lIndex element
						// if lIndex >= 0 do nothing and get lIndex element
						lIndex = lSize - 1 + lIndex;
					}
				} else {
					// if no index found, get n-1 element
					lIndex = lSize - 1;
				}

				if (lIndex >= 0 && lIndex < lSize) {
					lTempVarValue = temporaryVariables.get(lTempVarKey).get(lIndex);
				}
			}
		}
		return lTempVarValue;
	}

	/**
	 * Adds a value to a temporary variable in the list of temporary variables. If
	 * the temporary variable does not yet exist, it is added to the list. For each
	 * temporary variable, the number of added values in the last traced condition
	 * is kept in the numberOfAddedTemporaryVariables hash table. After tracing the
	 * condition, this hash table is cleared. If the condition evaluates to false,
	 * the corresponding added values are removed from the list of temporary
	 * variables.
	 *
	 * @param aTempVarKey the key of the temporary variable
	 * @param aTempVarKey the key of the temporary variable
	 *
	 * @return String the value of the temporary variable
	 */
	protected String addTempVarValue(String aTempVarKey, String aTempVarValue) {
		if (!temporaryVariables.containsKey(aTempVarKey)) {
			temporaryVariables.put(aTempVarKey, new ArrayList<String>());
		}
		if (!numberOfAddedTemporaryVariables.containsKey(aTempVarKey)) {
			numberOfAddedTemporaryVariables.put(aTempVarKey, new Integer(0));
		}
		temporaryVariables.get(aTempVarKey).add(aTempVarValue);
		Integer lInt = numberOfAddedTemporaryVariables.get(aTempVarKey);
		lInt++;
		numberOfAddedTemporaryVariables.put(aTempVarKey, lInt);
		return aTempVarValue;
	}

	/**
	 * Replaces temporary variables within a string.
	 * 
	 * Temporary variables can be added in script component conditions by code:
	 * [?+tempvar?], which leads to search with regular expression:
	 * \[\?\+(\w[\w\s]*)\?\]
	 * 
	 * Temporary variables can be retrieved in script component conditions and
	 * actions by code: [?tempvar?], or indexed, like [?tempvar(3)?], or with
	 * negative index, like [?tempvar(-3)?]. Which leads to search with regular
	 * expression: \[\?(\w[\w\s]*(?>\(\-?\d+\))?)\?\]
	 * 
	 * @param aString          the string
	 * @param aTriggeredTagKey the triggered tag key
	 *
	 * @return the string
	 */
	public String replaceTemporaryVarsWithinString(String aString, String aTriggeredTagKey) {
		// replace all existing temp vars
		String lRegEx = AppConstants.tempVarRegEx;
		Pattern lPattern = Pattern.compile(lRegEx, Pattern.MULTILINE);
		// first search for temp vars at lowest level
		String lSearchString = aString;
		Matcher lMatcher = lPattern.matcher(lSearchString);

		SSpring lSpring = getSSpring();
		while (lMatcher.find()) {
			List<String> lTempVars = new ArrayList<String>();
			lTempVars.add(lMatcher.group(1));
			while (lMatcher.find()) {
				// NOTE group(0) is complete pattern; group(1) is the group part of the pattern
				lTempVars.add(lMatcher.group(1));
			}
			// now replace all temp vars at this level
			for (String lTVKey : lTempVars) {
				String lTVValue = getTempVarValue(lTVKey);
				lSearchString = lSearchString.replaceFirst(
						Pattern.quote(AppConstants.tempVarPrefix + lTVKey + AppConstants.tempVarPostfix), lTVValue);
				if (lTVValue.isEmpty())
					lSpring.getSLogHelper().logAction("replaceTemporaryVarsWithinString: ERROR: TEMPORARY VARIABLE "
							+ lTVKey + " COULD NOT BE DETERMINED!");
			}
			// now search for temp vars at higher level:
			lMatcher = lPattern.matcher(lSearchString);
		}

		// now look for temp vars we should add in case of matching with triggered tag
		// key
		lRegEx = AppConstants.tempVarRegExAdd;
		lPattern = Pattern.compile(lRegEx, Pattern.MULTILINE);
		lMatcher = lPattern.matcher(lSearchString);
		List<String> lTempVars = new ArrayList<String>();
		while (lMatcher.find()) {
			// NOTE group(0) is complete pattern; group(1) is the group part of the pattern
			lTempVars.add(lMatcher.group(1));
			// NOTE adding temporary variables cannot be done nested
		}
		if (!lTempVars.isEmpty()) {
			// replace the temp var code in the search string by group code, so that we can
			// match the temp vars with the target
			// NOTE search lazy, by adding '?'
			String lMatchString = lMatcher.replaceAll("(.+?)");
			if (!lMatchString.startsWith("(.+?)"))
				lMatchString = ".*" + lMatchString;
			if (lMatchString.endsWith("(.+?)"))
				lMatchString = lMatchString.substring(0, lMatchString.lastIndexOf("(.+?)")) + "(.+)";
			else
				lMatchString = lMatchString + ".*";
			lPattern = Pattern.compile(lMatchString, Pattern.MULTILINE);
			lMatcher = lPattern.matcher(aTriggeredTagKey);
			if (lMatcher.matches()) {
				// NOTE pattern matches the triggered target, so we can add the temporary
				// variables
				int lInd = 1;
				// NOTE group(0) is the complete aTriggeredTargetKey string, temp vars are in
				// groups starting from group(1)
				for (String lTVKey : lTempVars) {
					String lTVValue = addTempVarValue(lTVKey, lMatcher.group(lInd));
					lSearchString = lSearchString.replaceFirst(Pattern.quote(AppConstants.tempVarPrefix
							+ AppConstants.tempVarAdd + lTVKey + AppConstants.tempVarPostfix), lTVValue);
					lSpring.getSLogHelper().logAction("replaceTemporaryVarsWithinString: INFO: added value " + lTVValue
							+ " to temporary variable " + lTVKey + ".");
					lInd++;
				}
			} else {
				// just replace all temp vars by their values if they already do exist
				for (String lTVKey : lTempVars) {
					String lTVValue = getTempVarValue(lTVKey);
					lSearchString = lSearchString.replaceFirst(Pattern.quote(AppConstants.tempVarPrefix
							+ AppConstants.tempVarAdd + lTVKey + AppConstants.tempVarPostfix), lTVValue);
					// NOTE String does not match aTriggeredTagKey. No problem. It occurs in case of
					// template conditions (or actions?) that should not be triggered.
					if (!aTriggeredTagKey.isEmpty()) {
						lSpring.getSLogHelper()
								.logAction("replaceTemporaryVarsWithinString: INFO: temporary variable " + lTVKey
										+ " could not be added, unequal to triggered tag key " + aTriggeredTagKey
										+ ". No problem.");
					} else {
						lSpring.getSLogHelper().logAction("replaceTemporaryVarsWithinString: INFO: temporary variable "
								+ lTVKey + " could not be added, no triggered tag key. No problem.");
					}
				}
			}
		}
		return lSearchString;
	}

	/**
	 * Replaces matches within a number of strings.
	 *
	 * @param aStrings         the a strings
	 * @param aTriggeredTagKey the triggered tag key
	 *
	 * @return list of string
	 */
	public List<String> replaceMatchesWithinStrings(List<String> aStrings, String aTriggeredTagKey) {
		List<String> lStrings = new ArrayList<String>();
		for (String lString : aStrings) {
			lStrings.add(replaceMatchesWithinString(lString, aTriggeredTagKey));
		}
		return lStrings;
	}

	/**
	 * Replaces matches within a string.
	 *
	 * @param aString          the string
	 * @param aTriggeredTagKey the triggered tag key
	 *
	 * @return the string
	 */
	public String replaceMatchesWithinString(String aString, String aTriggeredTagKey) {
		if (StringUtils.isEmpty(aTriggeredTagKey)) {
			return aString;
		}
		if (aTriggeredTagKey.matches(aString)) {
			return aTriggeredTagKey;
		}
		return aString;
	}

}