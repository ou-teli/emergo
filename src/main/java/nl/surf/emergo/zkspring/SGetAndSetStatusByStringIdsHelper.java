/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.zkspring;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * <p>The Class SGetAndSetStatusByStringIdsHelper ...</p>
 */
public class SGetAndSetStatusByStringIdsHelper implements Serializable {

	private static final long serialVersionUID = -636597079239556687L;

	private SSpring sSpring = null;
	
	/**
	 * Instantiates a new SReplaceVariablesWithinStringHelper.
	 *
	 * @param sSpring the spring object to use
	 */
	public SGetAndSetStatusByStringIdsHelper(SSpring aSpring) {
		sSpring = aSpring;
	}

	private SSpring getSSpring() {
		if (sSpring == null)
			return CDesktopComponents.sSpring();
		return sSpring;
	}

	/**
	 * Gets the current run component status. The last value saved.
	 *
	 * @param aCaseComponentName the a case component name
	 * @param aStatusKey the a status key
	 * @param aStatusType the a status type
	 * @param aErrors the a errors
	 *
	 * @return the current run tag status
	 */
	public String getCurrentRunComponentStatus(String aCaseComponentName, String aStatusKey, String aStatusType, List<String> aErrors) {
		IECaseComponent lCaseComponent = getSSpring().getCaseComponent(getSSpring().getCase(), "", aCaseComponentName);
		if (lCaseComponent == null) {
			addCaseComponentNotFoundError(aErrors, aCaseComponentName);
			return "";
		}
		return getSSpring().getCurrentRunComponentStatus(lCaseComponent, aStatusKey, aStatusType);
	}

	/**
	 * Gets the current run tag status. The last value saved.
	 *
	 * @param aCaseComponentName the a case component name
	 * @param aTagName the a tag name
	 * @param aTagKey the a tag key
	 * @param aStatusKey the a status key
	 * @param aStatusType the a status type
	 * @param aErrors the a errors
	 *
	 * @return the current run tag status
	 */
	public String getCurrentRunTagStatus(String aCaseComponentName, String aTagName, String aTagKey, String aStatusKey, String aStatusType, List<String> aErrors) {
		IECaseComponent lCaseComponent = getSSpring().getCaseComponent(getSSpring().getCase(), "", aCaseComponentName);
		if (lCaseComponent == null) {
			addCaseComponentNotFoundError(aErrors, aCaseComponentName);
			return "";
		}
		return getCurrentRunTagStatus(lCaseComponent, aTagName, aTagKey, aStatusKey, aStatusType, aErrors);
	}

	/**
	 * Gets the current run tag status. The last value saved.
	 *
	 * @param aCaseComponent the a case component
	 * @param aTagName the a tag name
	 * @param aTagKey the a tag key
	 * @param aStatusKey the a status key
	 * @param aStatusType the a status type
	 * @param aErrors the a errors
	 *
	 * @return the current run tag status
	 */
	public String getCurrentRunTagStatus(IECaseComponent aCaseComponent, String aTagName, String aTagKey, String aStatusKey, String aStatusType, List<String> aErrors) {
		IXMLTag lTag = getRunTagByNameAndKey(aCaseComponent, aTagName, aTagKey, aStatusType);
		if (lTag == null) {
			addTagNotFoundError(aErrors, aCaseComponent.getName(), aTagName, aTagKey);
			return "";
		}
		return getSSpring().getCurrentTagStatus(lTag, aStatusKey);
	}

	/**
	 * Gets the current run tag status time. The last value saved time.
	 *
	 * @param aCaseComponentName the a case component name
	 * @param aTagName the a tag name
	 * @param aTagKey the a tag key
	 * @param aStatusKey the a status key
	 * @param aStatusType the a status type
	 * @param aErrors the a errors
	 *
	 * @return the current run tag status time
	 */
	public double getCurrentRunTagStatusTime(String aCaseComponentName, String aTagName, String aTagKey, String aStatusKey, String aStatusType, List<String> aErrors) {
		IECaseComponent lCaseComponent = getSSpring().getCaseComponent(getSSpring().getCase(), "", aCaseComponentName);
		if (lCaseComponent == null) {
			addCaseComponentNotFoundError(aErrors, aCaseComponentName);
			return -1;
		}
		IXMLTag lTag = getRunTagByNameAndKey(lCaseComponent, aTagName, aTagKey, aStatusType);
		if (lTag == null) {
			addTagNotFoundError(aErrors, aCaseComponentName, aTagName, aTagKey);
			return -1;
		}
		return getSSpring().getCurrentTagStatusTime(lTag, aStatusKey);
	}

	/**
	 * Gets multiple current run tag status. The last values saved.
	 *
	 * @param aStateData the a state date
	 * @param aStatusType the a status type
	 * @param aErrors the a errors
	 *
	 * @return the current run tag status values
	 */
	public String[] getMultipleCurrentRunTagStatus(List<String[]> aStateData, String aStatusType, List<String> aErrors) {
		String[] lStatusValues = new String[aStateData.size()];
		Hashtable<String,IXMLTag> lHRootTags = new Hashtable<String,IXMLTag>();
		for (String[] lStateData : aStateData) {
			IECaseComponent lCaseComponent = getSSpring().getCaseComponent(getSSpring().getCase(), "", lStateData[0]);
			if (lCaseComponent != null) {
				IXMLTag lRootTag = getSSpring().getXmlDataPlusRunStatusTree(lCaseComponent, aStatusType);
				if (lRootTag != null) { 
					lHRootTags.put(lStateData[0], lRootTag);
				}
			}
			else {
				addCaseComponentNotFoundError(aErrors, lStateData[0]);
			}
		}
		int lCounter = 0;
		for (String[] lStateData : aStateData) {
			String lStateValue = "";
			if (lHRootTags.containsKey(lStateData[0])) {
				IXMLTag lTag = getRunTagByNameAndKey(lHRootTags.get(lStateData[0]), lStateData[1], lStateData[2]);
				if (lTag != null) {
					lStateValue = getSSpring().getCurrentTagStatus(lTag, lStateData[3]);
				}
				else {
					addTagNotFoundError(aErrors, lStateData[0], lStateData[1], lStateData[2]);
				}
			}
			lStatusValues[lCounter] = lStateValue; 
			lCounter ++;
		}
		return lStatusValues;
	}

	/**
	 * Gets multiple current run tag status time. The last values saved time.
	 *
	 * @param aStateData the a state date
	 * @param aStatusType the a status type
	 * @param aErrors the a errors
	 *
	 * @return the current run tag status values time
	 */
	public double[] getMultipleCurrentRunTagStatusTime(List<String[]> aStateData, String aStatusType, List<String> aErrors) {
		double[] lStatusTimes = new double[aStateData.size()];
		Hashtable<String,IXMLTag> lHRootTags = new Hashtable<String,IXMLTag>();
		for (String[] lStateData : aStateData) {
			IECaseComponent lCaseComponent = getSSpring().getCaseComponent(getSSpring().getCase(), "", lStateData[0]);
			if (lCaseComponent != null) {
				IXMLTag lRootTag = getSSpring().getXmlDataPlusRunStatusTree(lCaseComponent, aStatusType);
				if (lRootTag != null) { 
					lHRootTags.put(lStateData[0], lRootTag);
				}
			}
			else {
				addCaseComponentNotFoundError(aErrors, lStateData[0]);
			}
		}
		int lCounter = 0;
		for (String[] lStateData : aStateData) {
			double lStateTime = -1;
			if (lHRootTags.containsKey(lStateData[0])) {
				IXMLTag lTag = getRunTagByNameAndKey(lHRootTags.get(lStateData[0]), lStateData[1], lStateData[2]);
				if (lTag != null) {
					lStateTime = getSSpring().getCurrentTagStatusTime(lTag, lStateData[3]);
				}
				else {
					addTagNotFoundError(aErrors, lStateData[0], lStateData[1], lStateData[2]);
				}
			}
			lStatusTimes[lCounter] = lStateTime; 
			lCounter ++;
		}
		return lStatusTimes;
	}

	/**
	 * Sets the current run component status.
	 *
	 * @param aCaseComponentName the a case component name
	 * @param aStatusKey the a status key
	 * @param aStatusValue the a status value
	 * @param aStatusType the a status type
	 * @param aCheckScript the a check script
	 * @param aErrors the a errors
	 */
	public void setCurrentRunComponentStatus(String aCaseComponentName, String aStatusKey, String aStatusValue, String aStatusType, boolean aCheckScript, List<String> aErrors) {
		IECaseComponent lCaseComponent = getSSpring().getCaseComponent(getSSpring().getCase(), "", aCaseComponentName);
		if (lCaseComponent == null) {
			addCaseComponentNotFoundError(aErrors, aCaseComponentName);
			return;
		}
		STriggeredReference lTriggeredReference = new STriggeredReference();
		lTriggeredReference.setCaseComponent(lCaseComponent);
		lTriggeredReference.setStatusKey(aStatusKey);
		lTriggeredReference.setStatusValue(aStatusValue);
		getSSpring().setRunComponentStatus(lTriggeredReference, aCheckScript, aStatusType, true);
	}
	
	/**
	 * Sets the current run tag status.
	 *
	 * @param aCaseComponentName the a case component name
	 * @param aTagName the a tag name
	 * @param aTagKey the a tag key
	 * @param aStatusKey the a status key
	 * @param aStatusValue the a status value
	 * @param aStatusType the a status type
	 * @param aCheckScript the a check script
	 * @param aErrors the a errors
	 */
	public void setCurrentRunTagStatus(String aCaseComponentName, String aTagName, String aTagKey, String aStatusKey, String aStatusValue, String aStatusType, boolean aCheckScript, List<String> aErrors) {
		IECaseComponent lCaseComponent = getSSpring().getCaseComponent(getSSpring().getCase(), "", aCaseComponentName);
		if (lCaseComponent == null) {
			addCaseComponentNotFoundError(aErrors, aCaseComponentName);
			return;
		}
		setCurrentRunTagStatus(lCaseComponent, aTagName, aTagKey, aStatusKey, aStatusValue, aStatusType, aCheckScript, aErrors);
	}

	/**
	 * Sets the current run tag status.
	 *
	 * @param aCaseComponent the a case component
	 * @param aTagName the a tag name
	 * @param aTagKey the a tag key
	 * @param aStatusKey the a status key
	 * @param aStatusValue the a status value
	 * @param aStatusType the a status type
	 * @param aCheckScript the a check script
	 * @param aErrors the a errors
	 */
	public void setCurrentRunTagStatus(IECaseComponent aCaseComponent, String aTagName, String aTagKey, String aStatusKey, String aStatusValue, String aStatusType, boolean aCheckScript, List<String> aErrors) {
		IXMLTag lTag = getRunTagByNameAndKey(aCaseComponent, aTagName, aTagKey, aStatusType);
		if (lTag == null) {
			addTagNotFoundError(aErrors, aCaseComponent.getName(), aTagName, aTagKey);
			return;
		}
		getSSpring().setRunTagStatus(aCaseComponent, lTag, aStatusKey, aStatusValue, aCheckScript, aStatusType, true, false);
	}

	/**
	 * Add case component not found error.
	 *
	 * @param aErrors the a errors
	 * @param aCaseComponentName the a case component name
	 */
	protected void addCaseComponentNotFoundError(List<String> aErrors, String aCaseComponentName) {
		if (aErrors == null) {
			aErrors = new ArrayList<String>();
		}
		aErrors.add(CDesktopComponents.vView().getLabel("error.casecomponent.not.found").
				replace("%1", aCaseComponentName));
	}

	/**
	 * Gets the run tag by name and key.
	 *
	 * @param aCaseComponent the a case component
	 * @param aTagName the a tag name
	 * @param aTagKey the a tag key
	 * @param aStatusType the a status type, see IAppManager
	 *
	 * @return the xml data tag
	 */
	protected IXMLTag getRunTagByNameAndKey(IECaseComponent aCaseComponent, String aTagName, String aTagKey, String aStatusType) {
		IXMLTag lRootTag = getSSpring().getXmlDataPlusRunStatusTree(aCaseComponent, aStatusType);
		return getRunTagByNameAndKey(lRootTag, aTagName, aTagKey);
	}
	
	/**
	 * Gets the run tag by name and key.
	 *
	 * @param aRootTag the a root tag
	 * @param aTagName the a tag name
	 * @param aTagKey the a tag key
	 *
	 * @return the xml data tag
	 */
	protected IXMLTag getRunTagByNameAndKey(IXMLTag aRootTag, String aTagName, String aTagKey) {
		List<IXMLTag> lNodeTags = CDesktopComponents.cScript().getNodeTags(aRootTag);
		for (IXMLTag lNodeTag : lNodeTags) {
			if (lNodeTag.getName().equals(aTagName) && lNodeTag.getChildValue(lNodeTag.getDefAttribute(AppConstants.defKeyKey)).equals(aTagKey)) {
				return lNodeTag;
			}
		}
		return null;
	}
	
	/**
	 * Add tag not found error.
	 *
	 * @param aErrors the a errors
	 * @param aCaseComponentName the a case component name
	 * @param aTagName the a tag name
	 * @param aTagKey the a tag key
	 */
	protected void addTagNotFoundError(List<String> aErrors, String aCaseComponentName, String aTagName, String aTagKey) {
		if (aErrors == null) {
			aErrors = new ArrayList<String>();
		}
		aErrors.add(CDesktopComponents.vView().getLabel("error.tag.not.found").
				replace("%1", aTagName).
				replace("%2", aTagKey).
				replace("%3", aCaseComponentName));
	}

}