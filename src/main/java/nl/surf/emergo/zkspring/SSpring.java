/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.zkspring;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.PatternSyntaxException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;
import org.zkoss.zk.ui.Desktop;

import bsh.EvalError;
import bsh.Interpreter;
import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.IAppManager;
import nl.surf.emergo.business.IBlobManager;
import nl.surf.emergo.business.ICaseComponentManager;
import nl.surf.emergo.business.ICaseComponentRoleManager;
import nl.surf.emergo.business.ICaseManager;
import nl.surf.emergo.business.ICaseRoleManager;
import nl.surf.emergo.business.IComponentManager;
import nl.surf.emergo.business.IFileManager;
import nl.surf.emergo.business.IRunCaseComponentManager;
import nl.surf.emergo.business.IRunGroupAccountManager;
import nl.surf.emergo.business.IRunGroupCaseComponentManager;
import nl.surf.emergo.business.IRunGroupManager;
import nl.surf.emergo.business.IRunManager;
import nl.surf.emergo.business.IRunTeamCaseComponentManager;
import nl.surf.emergo.business.IRunTeamManager;
import nl.surf.emergo.business.IRunTeamRunGroupManager;
import nl.surf.emergo.business.IXMLAttributeValueTime;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.IXmlManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.business.impl.AppManager;
import nl.surf.emergo.control.CCaseHelper;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.CRunMainTimer;
import nl.surf.emergo.control.run.CRunStatusChange;
import nl.surf.emergo.control.run.CRunWnd;
import nl.surf.emergo.control.script.CScript;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IECaseComponentRole;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.domain.IEComponent;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunCaseComponent;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.domain.IERunGroupCaseComponent;
import nl.surf.emergo.domain.IERunTeam;
import nl.surf.emergo.domain.IERunTeamCaseComponent;
import nl.surf.emergo.domain.IERunTeamRunGroup;
import nl.surf.emergo.utilities.FileHelper;
import nl.surf.emergo.utilities.PropsValues;
import nl.surf.emergo.view.VView;

/**
 * <p>
 * The Class SSpring is called that way, because it functions as an interface
 * between the control layer, which uses ZK components, and the business layer,
 * which uses Spring. So classes within the control packages never directly
 * address the business layer. The latter is done by this SSpring class.
 * </p>
 *
 * <p>
 * This class buffers a lot of Emergo player data in memory, to achieve a better
 * performance.
 * </p>
 *
 * <p>
 * This class has a lot of methods. Some of them should be better positioned in
 * the XmlManager class. And maybe the SSpring class should be divided into
 * smaller classes to keep a better overview.
 * </p>
 */
public class SSpring implements Serializable {

	private static final long serialVersionUID = -2935308124137793774L;

	/** The application context. */
	protected ApplicationContext applicationContext;

	/** The app manager. */
	protected IAppManager appManager = null;

	/** The case manager. */
	protected ICaseManager caseManager = null;

	/** The run manager. */
	protected IRunManager runManager = null;

	/** The xml manager. */
	public IXmlManager xmlManager = null;

	/** the blob manager. */
	protected IBlobManager blobManager = null;

	/** the file manager. */
	protected IFileManager fileManager = null;

	/** the file helper. */
	protected FileHelper fileHelper = new FileHelper();

	/** the run group case component manager. */
	protected IRunGroupCaseComponentManager runGroupCaseComponentManager = null;

	/** The case helper. */
	protected CCaseHelper caseHelper = null;

	/** The bsh, a Java interpreter. */
	protected Interpreter bsh = null;

	/** The CScript. */
	protected CScript cScript = null;

	/** The run wnd, that is the Emergo player window. */
	protected CRunWnd runWnd = null;

	/** The run main timer, that timer of the Emergo player window. */
	protected CRunMainTimer runMainTimer = null;

	/**
	 * The run status, the status of the Emergo player. See IAppManager for possible
	 * values.
	 */
	protected String runStatus = "";

	/** The locale string, e.g. en_UK or nl_NL. */
	protected String localeString = null;

	/** The current run group account, the one who started the Emergo player. */
	protected IERunGroupAccount runGroupAccount = null;

	/**
	 * The current run team, the team, if applicable, of the one who started the
	 * Emergo player.
	 */
	protected IERunTeam runTeam = null;

	/** The current run. */
	protected IERun run = null;

	/**
	 * The Emergo server id. Every server has its own id, e.g., 'emergo' or
	 * 'emergotest'
	 */
	protected String emergoServerId = PropsValues.SERVER_ID;

	/** ---------- */
	/**
	 * Properties used for better performance. Stored in SSpring instance. Search
	 * for 'getProp'.
	 */
	/**
	 * NOTE Properties are stored in application memory as well. Search for
	 * 'getApp'.
	 */

	/**
	 * The xml data plus run status tree. Used to store xml data plus run status
	 * data in memory, for performance.
	 */
	protected Hashtable<String, IXMLTag> xmlDataPlusRunStatusTree = null;

	/**
	 * The xml data plus status tags by id. Used to store in memory, for
	 * performance.
	 */
	protected Hashtable<String, Hashtable<String, IXMLTag>> xmlDataPlusStatusTagsById = null;

	/**
	 * The xml run group status tree. Used to store pc data in memory, for
	 * performance.
	 */
	protected Hashtable<String, IXMLTag> xmlRunGroupStatusTree = null;

	/**
	 * The xml run status tree. Used to store run data in memory, for performance.
	 */
	protected Hashtable<String, IXMLTag> xmlRunStatusTree = null;

	/**
	 * The xml run team status tree. Used to store run team data in memory, for
	 * performance.
	 */
	protected Hashtable<String, IXMLTag> xmlRunTeamStatusTree = null;

	/** The xml status tags by id. Used to store in memory, for performance. */
	protected Hashtable<String, Hashtable<String, IXMLTag>> xmlStatusTagsById = null;

	/**
	 * The save run group case component status root tags. Used to store pc data in
	 * memory, which is saved periodically.
	 */
	protected Hashtable<String, IXMLTag> saveRunGroupCaseComponentStatusRootTags = null;

	/**
	 * The save run case component status root tags. Used to store run data in
	 * memory, which is saved periodically.
	 */
	protected Hashtable<String, IXMLTag> saveRunCaseComponentStatusRootTags = null;

	/**
	 * The save run team case component status root tags. Used to store run team
	 * data in memory, which is saved periodically.
	 */
	protected Hashtable<String, IXMLTag> saveRunTeamCaseComponentStatusRootTags = null;

	/**
	 * The case components in run. Used to store Emergo player data in memory, for
	 * performance.
	 */
	protected List<IECaseComponent> caseComponentsInRun = null;

	/**
	 * The run group case components. Used to store pc data in memory, for
	 * performance.
	 */
	protected Hashtable<String, IERunGroupCaseComponent> runGroupCaseComponents = null;

	/**
	 * The run case components. Used to store run data in memory, for performance.
	 */
	protected Hashtable<String, IERunCaseComponent> runCaseComponents = null;

	/**
	 * The run team case components. Used to store run team data in memory, for
	 * performance.
	 */
	protected Hashtable<String, IERunTeamCaseComponent> runTeamCaseComponents = null;

	/**
	 * The case component roles. Used to store Emergo player data in memory, for
	 * performance.
	 */
	protected Hashtable<String, List<IECaseComponentRole>> caseComponentRolesByCarId = null;

	/** ---------- */

	/** The replace variables within script helper. */
	protected SReplaceVariablesWithinStringHelper replaceVariablesWithinStringHelper;

	/** The replace variables within script helper. */
	protected SGetAndSetStatusByStringIdsHelper getAndSetStatusByStringIdsHelper;

	/** The log helper. */
	protected SLogHelper sLogHelper;

	/** The blob helper. */
	protected SBlobHelper sBlobHelper;

	/** The update helper. */
	protected SUpdateHelper sUpdateHelper;

	/** The case role helper. */
	protected SCaseRoleHelper sCaseRoleHelper;

	/** The script helper. */
	protected SScriptHelper sScriptHelper;

	/** The referenced data tag helper. */
	protected SReferencedDataTagHelper sReferencedDataTagHelper;

	/** The case skin helper. */
	protected SCaseSkinHelper sCaseSkinHelper;

	/**
	 * The case time. It is determined by CRunMainTimer, but also saved within this
	 * class. So it can be accessed by web services.
	 */
	public double caseTime = 0;

	/** ---------- */

	/**
	 * Constructor for SSpring class.
	 */
	public SSpring() {
	}

	/**
	 * Constructor for SSpring class.
	 *
	 * @param aApplicationContext the a application context
	 */
	public SSpring(ApplicationContext aApplicationContext) {
		applicationContext = aApplicationContext;
	}

	/**
	 * Gets the prop c control.
	 *
	 * @return the prop c control
	 */
	public CControl getPropCControl() {
		return CDesktopComponents.cControl();
	}

	/**
	 * Gets the prop v view.
	 *
	 * @return the prop v view
	 */
	public VView getPropVView() {
		return CDesktopComponents.vView();
	}

	/**
	 * Gets the prop cScript. Overruled by AxisSSpring!!
	 *
	 * @return the prop cScript
	 */
	public CScript getPropCScript() {
		if (cScript == null) {
			if (applicationContext == null) {
				cScript = CDesktopComponents.cScript();
			} else {
				cScript = new CScript(this);
				CDesktopComponents.setCScript(cScript);
			}
		}
		return cScript;
	}

	/**
	 * Gets the prop replaceVariablesWithinStringHelper. Overruled by AxisSSpring!!
	 *
	 * @return the prop replaceVariablesWithinStringHelper
	 */
	protected SReplaceVariablesWithinStringHelper getSReplaceVariablesWithinStringHelper() {
		if (replaceVariablesWithinStringHelper == null) {
			replaceVariablesWithinStringHelper = new SReplaceVariablesWithinStringHelper(this);
		}
		return replaceVariablesWithinStringHelper;
	}

	/**
	 * Gets the prop replaceVariablesWithinStringHelper. Overruled by AxisSSpring!!
	 *
	 * @return the prop replaceVariablesWithinStringHelper
	 */
	protected SGetAndSetStatusByStringIdsHelper getSGetAndSetStatusByStringIdsHelper() {
		if (getAndSetStatusByStringIdsHelper == null) {
			getAndSetStatusByStringIdsHelper = new SGetAndSetStatusByStringIdsHelper(this);
		}
		return getAndSetStatusByStringIdsHelper;
	}

	/**
	 * Gets the log helper.
	 *
	 * @return the log helper
	 */
	public SLogHelper getSLogHelper() {
		if (sLogHelper == null) {
			sLogHelper = new SLogHelper(this);
		}
		return sLogHelper;
	}

	/**
	 * Gets the blob helper.
	 *
	 * @return the blob helper
	 */
	public SBlobHelper getSBlobHelper() {
		if (sBlobHelper == null) {
			sBlobHelper = new SBlobHelper(this);
		}
		return sBlobHelper;
	}

	/**
	 * Gets the update helper.
	 *
	 * @return the update helper
	 */
	public SUpdateHelper getSUpdateHelper() {
		if (sUpdateHelper == null) {
			sUpdateHelper = new SUpdateHelper(this);
		}
		return sUpdateHelper;
	}

	/**
	 * Gets the case role helper.
	 *
	 * @return the case role helper
	 */
	public SCaseRoleHelper getSCaseRoleHelper() {
		if (sCaseRoleHelper == null) {
			sCaseRoleHelper = new SCaseRoleHelper(this);
		}
		return sCaseRoleHelper;
	}

	/**
	 * Gets the script helper.
	 *
	 * @return the script helper
	 */
	public SScriptHelper getSScriptHelper() {
		if (sScriptHelper == null) {
			sScriptHelper = new SScriptHelper(this);
		}
		return sScriptHelper;
	}

	/**
	 * Gets the referenced data tag helper.
	 *
	 * @return the referenced data helper
	 */
	public SReferencedDataTagHelper getSReferencedDataTagHelper() {
		if (sReferencedDataTagHelper == null) {
			sReferencedDataTagHelper = new SReferencedDataTagHelper(this);
		}
		return sReferencedDataTagHelper;
	}

	/**
	 * Gets the case skin helper.
	 *
	 * @return the case skin helper
	 */
	public SCaseSkinHelper getSCaseSkinHelper() {
		if (sCaseSkinHelper == null) {
			sCaseSkinHelper = new SCaseSkinHelper(this);
		}
		return sCaseSkinHelper;
	}

	/**
	 * Gets the app manager.
	 *
	 * @return the app manager
	 */
	public IAppManager getAppManager() {
		if (appManager == null)
			appManager = (IAppManager) (getBean("appManager"));
		return appManager;
	}

	/**
	 * Gets the case manager.
	 *
	 * @return the case manager
	 */
	public ICaseManager getCaseManager() {
		if (caseManager == null)
			caseManager = (ICaseManager) (getBean("caseManager"));
		return caseManager;
	}

	/**
	 * Gets the run manager.
	 *
	 * @return the run manager
	 */
	public IRunManager getRunManager() {
		if (runManager == null)
			runManager = (IRunManager) (getBean("runManager"));
		return runManager;
	}

	/**
	 * Gets the xml manager.
	 *
	 * @return the xml manager
	 */
	public IXmlManager getXmlManager() {
		if (xmlManager == null)
			xmlManager = (IXmlManager) (getBean("xmlManager"));
		return xmlManager;
	}

	/**
	 * Gets the blob manager.
	 *
	 * @return the blob manager
	 */
	public IBlobManager getBlobManager() {
		if (blobManager == null)
			blobManager = (IBlobManager) (getBean("blobManager"));
		return blobManager;
	}

	/**
	 * Gets the file manager.
	 *
	 * @return the file manager
	 */
	public IFileManager getFileManager() {
		if (fileManager == null)
			fileManager = (IFileManager) (getBean("fileManager"));
		return fileManager;
	}

	/**
	 * Gets the file helper.
	 *
	 * @return the file helper
	 */
	public FileHelper getFileHelper() {
		return fileHelper;
	}

	/**
	 * Gets the run group case component manager.
	 *
	 * @return the run group case component manager
	 */
	public IRunGroupCaseComponentManager getRunGroupCaseComponentManager() {
		if (runGroupCaseComponentManager == null)
			runGroupCaseComponentManager = (IRunGroupCaseComponentManager) (getBean("runGroupCaseComponentManager"));
		return runGroupCaseComponentManager;
	}

	/**
	 * Gets the case helper.
	 *
	 * @return the case helper
	 */
	protected CCaseHelper getCaseHelper() {
		if (caseHelper == null)
			caseHelper = new CCaseHelper(this);
		return caseHelper;
	}

	/**
	 * Gets the prop bsh.
	 *
	 * @return the prop bsh
	 */
	protected Interpreter getPropBsh() {
		if (bsh == null)
			bsh = new Interpreter();
		return bsh;
	}

	/**
	 * Gets the prop run wnd, the window of the Emergo player.
	 *
	 * @return the prop run wnd
	 */
	protected CRunWnd getPropRunWnd() {
		return runWnd;
	}

	/**
	 * Sets the prop run wnd, the window of the Emergo player.
	 * 
	 * @param aRunWnd the a run wnd
	 */
	public void setPropRunWnd(CRunWnd aRunWnd) {
		runWnd = aRunWnd;
	}

	/**
	 * Gets the prop run main timer, the timer used in the run wnd.
	 *
	 * @return the prop run main timer.
	 */
	public CRunMainTimer getPropRunMainTimer() {
		if (runMainTimer == null)
			if (getPropRunWnd() != null)
				runMainTimer = getPropRunWnd().runMainTimer;
		return runMainTimer;
	}

	/**
	 * Gets the prop run status.
	 *
	 * @return the prop run status
	 */
	protected String getPropRunStatus() {
		return runStatus;
	}

	/**
	 * Sets the prop run status.
	 *
	 * @param aRunStatus the new prop run status
	 */
	protected void setPropRunStatus(String aRunStatus) {
		runStatus = aRunStatus;
	}

	/**
	 * returns Locale string.
	 *
	 * @return the Locale string
	 */
	public String getPropLocaleString() {
		if (localeString == null) {
			localeString = getPropCControl().getLocaleString();
		}
		return localeString;
	}

	/**
	 * Gets the prop run status.
	 *
	 * @return the prop run status
	 */
	public String getPropEmergoServerId() {
		return emergoServerId;
	}

	/**
	 * Gets the prop run group account.
	 *
	 * @return the prop run group account
	 */
	protected IERunGroupAccount getPropRunGroupAccount() {
		return runGroupAccount;
	}

	/**
	 * Sets the prop run group account.
	 *
	 * @param aRunGroupAccount the new prop run group account
	 */
	protected void setPropRunGroupAccount(IERunGroupAccount aRunGroupAccount) {
		runGroupAccount = aRunGroupAccount;
	}

	/**
	 * Gets the prop run team.
	 *
	 * @return the prop run team
	 */
	protected IERunTeam getPropRunTeam() {
		return runTeam;
	}

	/**
	 * Sets the prop run team.
	 *
	 * @param aRunTeam the new prop run team
	 */
	protected void setPropRunTeam(IERunTeam aRunTeam) {
		runTeam = aRunTeam;
	}

	/**
	 * Gets the prop run.
	 *
	 * @return the prop run
	 */
	protected IERun getPropRun() {
		return run;
	}

	/**
	 * Sets the prop run.
	 *
	 * @param aRun the new prop run
	 */
	protected void setPropRun(IERun aRun) {
		run = aRun;
	}

	/** ---------- */
	/**
	 * Application properties used for better performance. Stored in application
	 * memory.
	 */

	/**
	 * Gets the app container.
	 *
	 * @return the app container
	 */
	public Hashtable<String, Object> getAppContainer() {
		return ((IAppManager) getBean("appManager")).getAppContainer();
	}

	/**
	 * Gets the prop xml def tree.
	 *
	 * @return the prop xml def tree
	 */
	protected Hashtable<String, IXMLTag> getAppPropXmlDefTreeByComId() {
		if (getAppContainer().get(AppConstants.xmlDefTreeByComId) == null)
			getAppContainer().put(AppConstants.xmlDefTreeByComId, new Hashtable<String, IXMLTag>(0));
		return (Hashtable) getAppContainer().get(AppConstants.xmlDefTreeByComId);
	}

	/**
	 * Gets the prop xml data tree.
	 *
	 * @return the prop xml data tree
	 */
	protected Hashtable<String, IXMLTag> getAppPropXmlDataTreeByCacId() {
		if (getAppContainer().get(AppConstants.xmlDataTreeByCacId) == null)
			getAppContainer().put(AppConstants.xmlDataTreeByCacId, new Hashtable<String, IXMLTag>(0));
		return (Hashtable) getAppContainer().get(AppConstants.xmlDataTreeByCacId);
	}

	/**
	 * Gets the prop case components in run by cas id.
	 *
	 * @return the prop case components in run by cas id
	 */
	protected Hashtable<String, List<IECaseComponent>> getAppPropCaseComponentsInRunByCasId() {
		if (getAppContainer().get(AppConstants.caseComponentsInRunByCasId) == null)
			getAppContainer().put(AppConstants.caseComponentsInRunByCasId,
					new Hashtable<String, List<IECaseComponent>>(0));
		return (Hashtable) getAppContainer().get(AppConstants.caseComponentsInRunByCasId);
	}

	/** ---------- */
	/** Properties used for better performance. Stored in SSpring instance. */

	/**
	 * Gets the prop xml data plus run status tree.
	 *
	 * @return the prop xml data plus run status tree
	 */
	public Hashtable<String, IXMLTag> getPropXmlDataPlusRunStatusTree() {
		if (xmlDataPlusRunStatusTree == null)
			xmlDataPlusRunStatusTree = new Hashtable<String, IXMLTag>(0);
		return xmlDataPlusRunStatusTree;
	}

	/**
	 * Gets the prop xml data plus status tags by id.
	 *
	 * @return the prop xml data plus status tags by id
	 */
	public Hashtable<String, Hashtable<String, IXMLTag>> getPropXmlDataPlusStatusTagsById() {
		if (xmlDataPlusStatusTagsById == null)
			xmlDataPlusStatusTagsById = new Hashtable<String, Hashtable<String, IXMLTag>>(0);
		return xmlDataPlusStatusTagsById;
	}

	/**
	 * Gets xml data plus status tag by id.
	 *
	 * @param aCaseComponent the a case component
	 * @param aStatusType    the a status type
	 * @param aTagId         the a tag id
	 *
	 * @return the status tag
	 */
	public IXMLTag getXmlDataPlusStatusTagById(IECaseComponent aCaseComponent, String aStatusType, String aTagId) {
		String lId = "" + aCaseComponent.getCacId() + "," + aStatusType;
		if (getPropXmlDataPlusStatusTagsById().containsKey(lId)) {
			return getPropXmlDataPlusStatusTagsById().get(lId).get(aTagId);
		}
		return null;
	}

	/**
	 * Sets xml data plus status tag by id.
	 *
	 * @param aCaseComponent the a case component
	 * @param aStatusTag     the a status tag
	 * @param aStatusType    the a status type
	 */
	public void setXmlDataPlusStatusTagById(IECaseComponent aCaseComponent, IXMLTag aDataPlusStatusTag,
			String aStatusType) {
		String lId = "" + aCaseComponent.getCacId() + "," + aStatusType;
		if (!getPropXmlDataPlusStatusTagsById().containsKey(lId)) {
			getPropXmlDataPlusStatusTagsById().put(lId, new Hashtable<String, IXMLTag>());
		}
		getPropXmlDataPlusStatusTagsById().get(lId).put(aDataPlusStatusTag.getAttribute(AppConstants.keyId),
				aDataPlusStatusTag);
	}

	/**
	 * Gets the prop xml run group status tree.
	 *
	 * @return the prop xml run group status tree
	 */
	public Hashtable<String, IXMLTag> getPropXmlRunGroupStatusTree() {
		if (xmlRunGroupStatusTree == null)
			xmlRunGroupStatusTree = new Hashtable<String, IXMLTag>(0);
		return xmlRunGroupStatusTree;
	}

	/**
	 * Gets the prop xml run status tree.
	 *
	 * @return the prop xml run status tree
	 */
	protected Hashtable<String, IXMLTag> getPropXmlRunStatusTree() {
		if (xmlRunStatusTree == null)
			xmlRunStatusTree = new Hashtable<String, IXMLTag>(0);
		return xmlRunStatusTree;
	}

	/**
	 * Gets the prop xml run team status tree.
	 *
	 * @return the prop xml run team status tree
	 */
	protected Hashtable<String, IXMLTag> getPropXmlRunTeamStatusTree() {
		if (xmlRunTeamStatusTree == null)
			xmlRunTeamStatusTree = new Hashtable<String, IXMLTag>(0);
		return xmlRunTeamStatusTree;
	}

	/**
	 * Gets the prop status tags by id.
	 *
	 * @return the prop status tags by id
	 */
	public Hashtable<String, Hashtable<String, IXMLTag>> getPropXmlStatusTagsById() {
		if (xmlStatusTagsById == null)
			xmlStatusTagsById = new Hashtable<String, Hashtable<String, IXMLTag>>(0);
		return xmlStatusTagsById;
	}

	/**
	 * Gets xml status tag by id.
	 *
	 * @param aCaseComponent the a case component
	 * @param aTagId         the a tag id
	 * @param aStatusType    the a status type
	 *
	 * @return the status tag
	 */
	public IXMLTag getXmlStatusTagById(IECaseComponent aCaseComponent, String aTagId, String aStatusType) {
		String lId = "" + getRunGroup().getRugId() + "," + aCaseComponent.getCacId() + "," + aStatusType;
		if (getPropXmlStatusTagsById().containsKey(lId)) {
			return getPropXmlStatusTagsById().get(lId).get(aTagId);
		}
		return null;
	}

	/**
	 * Sets xml status tag by id.
	 *
	 * @param aCaseComponent the a case component
	 * @param aStatusTag     the a status tag
	 * @param aStatusType    the a status type
	 */
	public void setXmlStatusTagById(IECaseComponent aCaseComponent, IXMLTag aStatusTag, String aStatusType) {
		String lId = "" + getRunGroup().getRugId() + "," + aCaseComponent.getCacId() + "," + aStatusType;
		if (!getPropXmlStatusTagsById().containsKey(lId)) {
			getPropXmlStatusTagsById().put(lId, new Hashtable<String, IXMLTag>());
		}
		getPropXmlStatusTagsById().get(lId).put(aStatusTag.getAttribute(AppConstants.keyId), aStatusTag);
	}

	/**
	 * Gets the prop save run group case component status root tags.
	 *
	 * @return the prop save run group case component status root tags
	 */
	public Hashtable<String, IXMLTag> getPropSaveRunGroupCaseComponentStatusRootTags() {
		if (saveRunGroupCaseComponentStatusRootTags == null)
			setPropSaveXmlRunGroupStatusTree(new Hashtable<String, IXMLTag>(0));
		return saveRunGroupCaseComponentStatusRootTags;
	}

	/**
	 * Sets the prop save xml run group status tree.
	 *
	 * @param aSaveXmlRunGroupStatusTree the new prop save xml run group status tree
	 */
	protected void setPropSaveXmlRunGroupStatusTree(Hashtable<String, IXMLTag> aSaveXmlRunGroupStatusTree) {
		saveRunGroupCaseComponentStatusRootTags = aSaveXmlRunGroupStatusTree;
	}

	/**
	 * Gets the prop save run case component status root tags.
	 *
	 * @return the prop save run case component status root tags
	 */
	protected Hashtable<String, IXMLTag> getPropSaveRunCaseComponentStatusRootTags() {
		if (saveRunCaseComponentStatusRootTags == null)
			setPropSaveXmlRunStatusTree(new Hashtable<String, IXMLTag>(0));
		return saveRunCaseComponentStatusRootTags;
	}

	/**
	 * Sets the prop save xml run status tree.
	 *
	 * @param aSaveXmlRunStatusTree the new prop save xml run status tree
	 */
	protected void setPropSaveXmlRunStatusTree(Hashtable<String, IXMLTag> aSaveXmlRunStatusTree) {
		saveRunCaseComponentStatusRootTags = aSaveXmlRunStatusTree;
	}

	/**
	 * Gets the prop save run team case component status root tags.
	 *
	 * @return the prop save run team case component status root tags
	 */
	protected Hashtable<String, IXMLTag> getPropSaveRunTeamCaseComponentStatusRootTags() {
		if (saveRunTeamCaseComponentStatusRootTags == null)
			setPropSaveXmlRunTeamStatusTree(new Hashtable<String, IXMLTag>(0));
		return saveRunTeamCaseComponentStatusRootTags;
	}

	/**
	 * Sets the prop save xml run team status tree.
	 *
	 * @param aSaveXmlRunTeamStatusTree the new prop save xml run team status tree
	 */
	protected void setPropSaveXmlRunTeamStatusTree(Hashtable<String, IXMLTag> aSaveXmlRunTeamStatusTree) {
		saveRunTeamCaseComponentStatusRootTags = aSaveXmlRunTeamStatusTree;
	}

	/**
	 * Gets the prop case components in run.
	 *
	 * @return the prop case components in run
	 */
	protected List<IECaseComponent> getPropCaseComponentsInRun() {
		return caseComponentsInRun;
	}

	/**
	 * Sets the prop case components in run.
	 *
	 * @param aCaseComponentsInRun the new prop case components in run
	 */
	protected void setPropCaseComponentsInRun(List<IECaseComponent> aCaseComponentsInRun) {
		caseComponentsInRun = aCaseComponentsInRun;
	}

	/**
	 * Gets the prop run group case components.
	 *
	 * @return the prop run group case components
	 */
	protected Hashtable<String, IERunGroupCaseComponent> getPropRunGroupCaseComponents() {
		if (runGroupCaseComponents == null)
			runGroupCaseComponents = new Hashtable<String, IERunGroupCaseComponent>(0);
		return runGroupCaseComponents;
	}

	/**
	 * Gets the prop run case components.
	 *
	 * @return the prop run case components
	 */
	protected Hashtable<String, IERunCaseComponent> getPropRunCaseComponents() {
		if (runCaseComponents == null)
			runCaseComponents = new Hashtable<String, IERunCaseComponent>(0);
		return runCaseComponents;
	}

	/**
	 * Gets the prop run team case components.
	 *
	 * @return the prop run team case components
	 */
	protected Hashtable<String, IERunTeamCaseComponent> getPropRunTeamCaseComponents() {
		if (runTeamCaseComponents == null)
			runTeamCaseComponents = new Hashtable<String, IERunTeamCaseComponent>(0);
		return runTeamCaseComponents;
	}

	/**
	 * Gets the prop case component roles by car id cac id.
	 *
	 * @return the prop case component roles by car id cac id
	 */
	protected Hashtable<String, List<IECaseComponentRole>> getPropCaseComponentRolesByCarId() {
		if (caseComponentRolesByCarId == null)
			caseComponentRolesByCarId = new Hashtable<String, List<IECaseComponentRole>>(0);
		return caseComponentRolesByCarId;
	}

	/**
	 * /** Clears all Spring run speed up properties
	 */
	public void clearRunBoostProps() {
		// NOTE don't clear application properties stored in application memory. They
		// are also managed within ComponentManager, CaseManager and
		// CaseComponentManager classes.
		getPropXmlDataPlusRunStatusTree().clear();
		getPropXmlDataPlusStatusTagsById().clear();
		getPropXmlRunGroupStatusTree().clear();
		getPropXmlRunStatusTree().clear();
		getPropXmlRunTeamStatusTree().clear();
		getPropXmlStatusTagsById().clear();
		getPropSaveRunGroupCaseComponentStatusRootTags().clear();
		getPropSaveRunCaseComponentStatusRootTags().clear();
		getPropSaveRunTeamCaseComponentStatusRootTags().clear();
		getSCaseRoleHelper().setPropPcCaseRoles(null);
		getSCaseRoleHelper().setPropNpcCaseRoles(null);
		getSCaseRoleHelper().getPropCaseRoleInRunByCarId().clear();
		setPropCaseComponentsInRun(null);
		getPropRunGroupCaseComponents().clear();
		getPropRunCaseComponents().clear();
		getPropRunTeamCaseComponents().clear();
		getPropCaseComponentRolesByCarId().clear();
		getSScriptHelper().getPropTagScriptIdsInRun().clear();
		getSScriptHelper().getPropTagScriptTemplateIdsInRun().clear();
		getSScriptHelper().getPropComponentScriptIdsInRun().clear();
		getSScriptHelper().getPropTemplateTagScriptIdsInRun().clear();
		getSScriptHelper().getPropTemplateComponentScriptIdsInRun().clear();
		getSUpdateHelper().getPropUpdateRunGroupTag().clear();
		getSUpdateHelper().getPropProcessedUpdateRunGroupTag().clear();
	}

	/**
	 * Gets the bean, given by aBeanId. A bean is an object defined within the
	 * Spring configuration file, that is injected by Spring at runtime.
	 *
	 * @param aBeanId the a bean id
	 *
	 * @return the bean
	 */
	public Object getBean(String aBeanId) {
		if (applicationContext != null)
			return applicationContext.getBean(aBeanId);
		return AppManager.getBean(aBeanId);
	}

	/**
	 * Gets the empty xml.
	 *
	 * @return the empty xml
	 */
	public String getEmptyXml() {
		return AppConstants.emptyXml;
	}

	/**
	 * Gets the component given by aComId. If called from within Emergo player
	 * component saved in memory is returned.
	 *
	 * @param aComId the a com id
	 *
	 * @return the component
	 */
	public IEComponent getComponent(String aComId) {
		if (inRun())
			return getComponentInRun(aComId);
		return ((IComponentManager) getBean("componentManager")).getComponent(Integer.parseInt(aComId));
	}

	/**
	 * Gets the component in run given by aComId. Component saved in case components
	 * in memory is returned.
	 *
	 * @param aComId the a com id
	 *
	 * @return the component
	 */
	protected IEComponent getComponentInRun(String aComId) {
		List<IECaseComponent> lCaseComponents = getCaseComponentsInRun(getCase().getCasId());
		for (IECaseComponent lCaseComponent : lCaseComponents) {
			IEComponent lComponent = lCaseComponent.getEComponent();
			if (lComponent.getComId() == Integer.parseInt(aComId)) {
				return lComponent;
			}
		}
		return null;
	}

	/**
	 * Gets the component xml definition given by aComId.
	 *
	 * @param aComId the a com id
	 *
	 * @return the component xml definition
	 */
	public String getComponentDefinition(String aComId) {
		IEComponent lComp = getComponent(aComId);
		String lXmlDef = "";
		if (lComp != null)
			lXmlDef = lComp.getXmldefinition();
		if (lXmlDef == null)
			lXmlDef = "";
		// lXmlDef must be valid xml.
		if (lXmlDef.equals(""))
			lXmlDef = getEmptyXml();
		return lXmlDef;
	}

	/**
	 * Gets the case components in run by cas id.
	 *
	 * @return the case components in run by cas id
	 */
	protected Hashtable<String, List<IECaseComponent>> getCaseComponentsInRunByCasId() {
		return getAppPropCaseComponentsInRunByCasId();
	}

	/**
	 * Gets the case components given by aCase. If called from within Emergo player
	 * case components saved in memory are returned.
	 *
	 * @param aCase the a case
	 *
	 * @return the case components
	 */
	public List<IECaseComponent> getCaseComponents(IECase aCase) {
		if (inRun())
			return getCaseComponentsInRun(aCase.getCasId());
		return getCaseComponents(aCase.getCasId());
	}

	/**
	 * Gets all case components given by aCases
	 *
	 * @param aCases the a cases
	 *
	 * @return the case components
	 */
	public List<IECaseComponent> getCaseComponents(List<IECase> aCases) {
		List<Integer> lCasIds = new ArrayList<Integer>();
		for (IECase lCase : aCases) {
			lCasIds.add(lCase.getCasId());
		}
		return ((ICaseComponentManager) (getBean("caseComponentManager"))).getAllCaseComponentsByCasIds(lCasIds);
	}

	/**
	 * Gets the case components given by aCasId. If called from within Emergo player
	 * case components saved in memory are returned.
	 *
	 * @param aCasId the a cas id
	 *
	 * @return the case components
	 */
	public List<IECaseComponent> getCaseComponents(int aCasId) {
		if (inRun())
			return getCaseComponentsInRun(aCasId);
		return ((ICaseComponentManager) (getBean("caseComponentManager"))).getAllCaseComponentsByCasId(aCasId);
	}

	/**
	 * Gets the case components in run given by cas id.
	 *
	 * @param aCasId the a cas id
	 *
	 * @return the case components in run by cas id
	 */
	protected List<IECaseComponent> getCaseComponentsInRunByCasId(String aCasId) {
		String lId = "" + aCasId;
		List<IECaseComponent> lCaseComponents = null;
		if (getCaseComponentsInRunByCasId().containsKey(lId))
			lCaseComponents = getCaseComponentsInRunByCasId().get(lId);
		else {
			lCaseComponents = ((ICaseComponentManager) getBean("caseComponentManager"))
					.getAllCaseComponentsByCasId(Integer.parseInt(aCasId));
			if ((lCaseComponents == null) || (lCaseComponents.size() == 0))
				return null;
			getCaseComponentsInRunByCasId().put(lId, lCaseComponents);
		}
		// copy so original list stays same if calling method removes items
		List<IECaseComponent> lNewCaseComponents = new ArrayList<IECaseComponent>(0);
		for (IECaseComponent lCaseComponent : lCaseComponents) {
			lNewCaseComponents.add(lCaseComponent);
		}
		return lNewCaseComponents;
	}

	/**
	 * Removes the case components in run given by cas id.
	 *
	 * @param aCasId the a cas id
	 */
	protected void removeCaseComponentsInRunByCasId(String aCasId) {
		String lId = "" + aCasId;
		if (getCaseComponentsInRunByCasId().containsKey(lId)) {
			getCaseComponentsInRunByCasId().remove(lId);
		}
	}

	/**
	 * Gets the case components in run given by aCasId.
	 *
	 * @param aCasId the a cas id
	 *
	 * @return the case components in run
	 */
	protected List<IECaseComponent> getCaseComponentsInRun(int aCasId) {
		// NOTE case role maybe null but run may be set if case run manager sets status
		// for all run members
		if (getSCaseRoleHelper().getCaseRole() == null && getRun() == null) {
			return new ArrayList<IECaseComponent>(0);
		}
		List<IECaseComponent> lCaseComponentsInRunByCasId = getCaseComponentsInRunByCasId("" + aCasId);
		if (getPropCaseComponentsInRun() == null) {
			setPropCaseComponentsInRun(lCaseComponentsInRunByCasId);
			for (int i = (getPropCaseComponentsInRun().size() - 1); i >= 0; i--) {
				IECaseComponent lCaseComponent = getPropCaseComponentsInRun().get(i);
				if (!(lCaseComponent.getEComponent().getType() == AppConstants.system_component)) {
					// if system component then always in run and only one instance
					// NOTE case role maybe null if case run manager sets status for all run members
					if (getSCaseRoleHelper().getCaseRole() != null
							&& getCaseComponentRole(getSCaseRoleHelper().getCaseRole().getCarId(),
									lCaseComponent.getCacId()) == null) {
						getPropCaseComponentsInRun().remove(i);
					}
				}
			}
		}
		// copy so original list stays same if calling method removes items
		List<IECaseComponent> lCaseComponents = new ArrayList<IECaseComponent>(0);
		for (IECaseComponent lCaseComponent : getPropCaseComponentsInRun()) {
			lCaseComponents.add(lCaseComponent);
		}
		return lCaseComponents;
	}

	/**
	 * Gets the case components by component code.
	 *
	 * @param aCaseComponents the a case components
	 * @param aComponentCodes the a component codes, comma separated
	 *
	 * @return the case components by component codes
	 */
	public List<IECaseComponent> getCaseComponentsByComponentCodes(List<IECaseComponent> aCaseComponents,
			String aComponentCodes) {
		if (aCaseComponents == null)
			return null;
		if (aComponentCodes.equals(""))
			return aCaseComponents;
		String[] lComponentCodes = aComponentCodes.split(",");
		for (int i = (aCaseComponents.size() - 1); i >= 0; i--) {
			IECaseComponent lCaseComponent = aCaseComponents.get(i);
			boolean lFound = false;
			for (int j = 0; j < lComponentCodes.length; j++) {
				if (lCaseComponent.getEComponent().getCode().equals(lComponentCodes[j]))
					lFound = true;
			}
			if (!lFound)
				aCaseComponents.remove(i);
		}
		return aCaseComponents;
	}

	/**
	 * Gets the case components given by aCase and aComponentCodes.
	 *
	 * @param aCase           the a case
	 * @param aComponentCodes the a component codes, comma separated
	 *
	 * @return the case components
	 */
	public List<IECaseComponent> getCaseComponents(IECase aCase, String aComponentCodes) {
		List<IECaseComponent> lCaseComponents = getCaseComponents(aCase);
		return getCaseComponentsByComponentCodes(lCaseComponents, aComponentCodes);
	}

	/**
	 * Gets the case components given by aComponentCodes in current case.
	 *
	 * @param aComponentCodes the a component codes, comma separated
	 *
	 * @return the case components
	 */
	public List<IECaseComponent> getCaseComponents(String aComponentCodes) {
		return getCaseComponents(getCase(), aComponentCodes);
	}

	/**
	 * Returns all case components given by aComponentCodes which have one or more
	 * case roles in common with casecomponent given by aMCacId.
	 *
	 * @param aMCacId         the a m cac id
	 * @param aComponentCodes the a component codes, comma separated
	 *
	 * @return the case components by master cac id
	 */
	public List<IECaseComponent> getCaseComponentsByMasterCacId(String aMCacId, String aComponentCodes) {
		List<IECaseComponent> lCaseComponents = getCaseComponents(aComponentCodes);
		for (int i = (lCaseComponents.size() - 1); i >= 0; i--) {
			IECaseComponent lCaseComponent = lCaseComponents.get(i);
			List<IECaseRole> lCaseRoles = getSCaseRoleHelper().getCaseRolesByMasterSlaveCacIds(aMCacId,
					"" + lCaseComponent.getCacId());
			if ((lCaseRoles == null) || (lCaseRoles.size() == 0))
				lCaseComponents.remove(i);
		}
		return lCaseComponents;
	}

	/**
	 * Gets the case component given by aCacId. If called from within Emergo player
	 * case component saved in memory is returned.
	 *
	 * @param aCacId the a cac id
	 *
	 * @return the case component
	 */
	public IECaseComponent getCaseComponent(int aCacId) {
		return getCaseComponent(aCacId, true);
	}

	/**
	 * Gets the case component given by aCacId. If called from within Emergo player
	 * case component saved in memory is returned. If not found in memory, boolean
	 * determines if database is searched.
	 *
	 * @param aCacId   the a cac id
	 * @param aOnlyRun true if case component must be present in run, when player is
	 *                 started
	 *
	 * @return the case component
	 */
	public IECaseComponent getCaseComponent(int aCacId, boolean aOnlyRun) {
		IECaseComponent lReturn = null;
		if (inRun()) {
			lReturn = getCaseComponentInRun(aCacId);
			if (aOnlyRun)
				return lReturn;
		}
		if (lReturn == null)
			lReturn = ((ICaseComponentManager) (getBean("caseComponentManager"))).getCaseComponent(aCacId);
		return lReturn;
	}

	/**
	 * Gets the case component in run given by aCacId.
	 *
	 * @param aCacId the a cac id
	 *
	 * @return the case component
	 */
	public IECaseComponent getCaseComponentInRun(int aCacId) {
		List<IECaseComponent> lCaseComponents = getCaseComponentsInRun(getCase().getCasId());
		if (lCaseComponents == null) {
			return null;
		}
		for (IECaseComponent lCaseComponent : lCaseComponents) {
			if (lCaseComponent.getCacId() == aCacId) {
				return lCaseComponent;
			}
		}
		return null;
	}

	/**
	 * Gets the case component given by aCacId.
	 *
	 * @param aCacId the a cac id
	 *
	 * @return the case component
	 */
	public IECaseComponent getCaseComponent(String aCacId) {
		return getCaseComponent(Integer.parseInt(aCacId));
	}

	/**
	 * Gets the case component given by aComponentCode or aCaseComponentName. If not
	 * called from within Emergo player null is returned.
	 *
	 * @param aComponentCode     the a component code
	 * @param aCaseComponentName the a case component name
	 *
	 * @return the case component
	 */
	public IECaseComponent getCaseComponent(String aComponentCode, String aCaseComponentName) {
		IERun lRun = getRun();
		if (lRun == null)
			return null;
		return getCaseComponent(lRun.getECase(), aComponentCode, aCaseComponentName);
	}

	/**
	 * Gets the case components given by aComponentCode. If not called from within
	 * Emergo player null is returned.
	 *
	 * @param aComponentCode the a component code
	 *
	 * @return the case components
	 */
	public List<IECaseComponent> getCaseComponentsByComponentCode(String aComponentCode) {
		IERun lRun = getRun();
		if (lRun == null)
			return null;
		return getCaseComponentsByComponentCode(lRun.getECase(), aComponentCode);
	}

	/**
	 * Gets the case component role name given by aCaseComponent and current case
	 * role. If not called from within Emergo player an empty string is returned.
	 *
	 * @param aCaseComponent the case component
	 *
	 * @return the case component role name
	 */
	public String getCaseComponentRoleName(IECaseComponent aCaseComponent) {
		IERun lRun = getRun();
		if (lRun == null)
			return "";
		IECaseRole lCaseRole = getSCaseRoleHelper().getCaseRole();
		if ((aCaseComponent == null) || (lCaseRole == null))
			return "";
		IECaseComponentRole lCaseComponentRole = getCaseComponentRole(lCaseRole.getCarId(), aCaseComponent.getCacId());
		if (lCaseComponentRole == null)
			return null;
		String lName = lCaseComponentRole.getName();
		// NOTE if case supports multiple languages
		if (inRun() && aCaseComponent.getECase().getMultilingual()) {
			// NOTE lCaseComponentRole.getName() may contain name in other languages so get
			// correct one
			String lLocaleStr = getPropLocaleString();
			String lStartLocale = AppConstants.labelVarPrefix + lLocaleStr + AppConstants.labelVarPostfix;
			String lEndLocale = AppConstants.labelVarPrefix + "/" + lLocaleStr + AppConstants.labelVarPostfix;
			int lStart = lName.indexOf(lStartLocale);
			int lEnd = lName.indexOf(lEndLocale);
			if (lStart >= 0 && lEnd >= (lStart + lStartLocale.length())) {
				lName = lName.substring(lStart + lStartLocale.length(), lEnd);
			} else {
				lStartLocale = AppConstants.labelVarPrefix;
				lStart = lName.indexOf(lStartLocale);
				if (lStart >= 0) {
					lName = lName.substring(0, lStart);
				}
			}
		}
		return lName;
	}

	/**
	 * Gets the case component role name given by aComponentCode or
	 * aCaseComponentName, and current case role. If not called from within Emergo
	 * player an empty string is returned.
	 *
	 * @param aComponentCode     the a component code
	 * @param aCaseComponentName the a case component name
	 *
	 * @return the case component role name
	 */
	public String getCaseComponentRoleName(String aComponentCode, String aCaseComponentName) {
		IERun lRun = getRun();
		if (lRun == null)
			return "";
		IECaseComponent lCaseComponent = getCaseComponent(lRun.getECase(), aComponentCode, aCaseComponentName);
		return getCaseComponentRoleName(lCaseComponent);
	}

	/**
	 * Gets the case component by component code or case component name.
	 *
	 * @param aCaseComponents    the a case components
	 * @param aComponentCode     the a component code
	 * @param aCaseComponentName the a case component name
	 *
	 * @return the case component by component code or case component name
	 */
	public IECaseComponent getCaseComponentByComponentCodeOrCaseComponentName(List<IECaseComponent> aCaseComponents,
			String aComponentCode, String aCaseComponentName) {
		if ((aCaseComponents == null) || (aCaseComponents.size() == 0))
			return null;
		for (IECaseComponent lCaseComponent : aCaseComponents) {
			if (aComponentCode.equals("")) {
				if (lCaseComponent.getName().equals(aCaseComponentName))
					return lCaseComponent;
			} else {
				if (lCaseComponent.getEComponent().getCode().equals(aComponentCode))
					return lCaseComponent;
			}
		}
		return null;
	}

	/**
	 * Gets the case components by component code.
	 *
	 * @param aCaseComponents the a case components
	 * @param aComponentCode  the a component code
	 *
	 * @return the case components by component code
	 */
	public List<IECaseComponent> getCaseComponentsByComponentCode(List<IECaseComponent> aCaseComponents,
			String aComponentCode) {
		List<IECaseComponent> lCaseComponents = new ArrayList<IECaseComponent>();
		if (aCaseComponents != null) {
			for (IECaseComponent lCaseComponent : aCaseComponents) {
				if (lCaseComponent.getEComponent().getCode().equals(aComponentCode)) {
					lCaseComponents.add(lCaseComponent);
				}
			}
		}
		return lCaseComponents;
	}

	/**
	 * Gets the case component by aCase and component code or case component name.
	 *
	 * @param aCase              the a case
	 * @param aComponentCode     the a component code
	 * @param aCaseComponentName the a case component name
	 *
	 * @return the case component
	 */
	public IECaseComponent getCaseComponent(IECase aCase, String aComponentCode, String aCaseComponentName) {
		if (aCase == null)
			return null;
		List<IECaseComponent> lCaseComponents = getCaseComponents(aCase);
		return getCaseComponentByComponentCodeOrCaseComponentName(lCaseComponents, aComponentCode, aCaseComponentName);
	}

	/**
	 * Gets the case components by aCase and component code.
	 *
	 * @param aCase          the a case
	 * @param aComponentCode the a component code
	 *
	 * @return the case components
	 */
	public List<IECaseComponent> getCaseComponentsByComponentCode(IECase aCase, String aComponentCode) {
		if (aCase == null)
			return null;
		List<IECaseComponent> lCaseComponents = getCaseComponents(aCase);
		return getCaseComponentsByComponentCode(lCaseComponents, aComponentCode);
	}

	/**
	 * Gets the case component name.
	 *
	 * @param aCacId the a cac id
	 *
	 * @return the case component name
	 */
	public String getCaseComponentName(String aCacId) {
		IECaseComponent lItem = getCaseComponent(aCacId);
		if (lItem == null)
			return getPropVView().getLabel("cde_s_script.invalid");
		return lItem.getName();
	}

	/**
	 * Gets the case components by status type. See IAppManager for status type
	 * definitions.
	 *
	 * @param aCaseComponents the a case components
	 * @param aStatusType     the a status type
	 *
	 * @return the case components by status type
	 */
	private List<IECaseComponent> pGetCaseComponentsByStatusType(List<IECaseComponent> aCaseComponents,
			String aStatusType) {
		List<IECaseComponent> lNewCaseComponents = new ArrayList<IECaseComponent>(0);
		for (IECaseComponent lCaseComponent : aCaseComponents) {
			IXMLTag lRootTag = getNonCachedXmlDataTree(lCaseComponent);
			if (lRootTag != null) {
				// status type is determined using component definition
				IXMLTag lComponentTag = lRootTag.getChild(AppConstants.componentElement);
				boolean lShared = false;
				boolean lSharedbyteam = false;
				boolean lAdd = false;
				if (lComponentTag != null) {
					lShared = (lComponentTag
							.getDefChildAttribute(AppConstants.statusElement, AppConstants.statusKeyShared)
							.equals(AppConstants.statusValueTrue));
					lSharedbyteam = (lComponentTag
							.getDefChildAttribute(AppConstants.statusElement, AppConstants.statusKeySharedbyteam)
							.equals(AppConstants.statusValueTrue));
				}
				if (aStatusType.equals(AppConstants.statusTypeRunGroup) && (!lShared) && (!lSharedbyteam))
					lAdd = true;
				if (aStatusType.equals(AppConstants.statusTypeRun) && (lShared) && (!lSharedbyteam))
					lAdd = true;
				if (aStatusType.equals(AppConstants.statusTypeRunTeam) && (!lShared) && (lSharedbyteam))
					lAdd = true;
				if (lAdd)
					lNewCaseComponents.add(lCaseComponent);
			}
		}
		return lNewCaseComponents;
	}

	/**
	 * Gets the case components by status type. See IAppManager for status type
	 * definitions.
	 *
	 * @param aCase       the a case
	 * @param aStatusType the a status type
	 *
	 * @return the case components by status type
	 */
	public List<IECaseComponent> getCaseComponentsByStatusType(IECase aCase, String aStatusType) {
		List<IECaseComponent> lCaseComponents = getCaseComponents(aCase);
		return pGetCaseComponentsByStatusType(lCaseComponents, aStatusType);
	}

	/**
	 * Gets the case components by status type. See IAppManager for status type
	 * definitions.
	 *
	 * @param aCases      the a cases
	 * @param aStatusType the a status type
	 *
	 * @return the case components by status type
	 */
	public List<IECaseComponent> getCaseComponentsByStatusType(List<IECase> aCases, String aStatusType) {
		List<IECaseComponent> lCaseComponents = getCaseComponents(aCases);
		return pGetCaseComponentsByStatusType(lCaseComponents, aStatusType);
	}

	/**
	 * Gets the case component content tag of its xml data.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return the case component content tag
	 */
	public IXMLTag getCaseComponentContentTag(IECaseComponent aCaseComponent) {
		if (aCaseComponent == null)
			return null;
		IXMLTag lRootTag = getXmlDataTree(aCaseComponent);
		if (lRootTag == null)
			return null;
		return lRootTag.getChild(AppConstants.contentElement);
	}

	/**
	 * Gets the tag given by aTagId within the xml data of aCaseComponent.
	 *
	 * @param aCaseComponent the a case component
	 * @param aTagId         the a tag id
	 *
	 * @return the tag
	 */
	public IXMLTag getTag(IECaseComponent aCaseComponent, String aTagId) {
		if (aCaseComponent == null)
			return null;
		return getCaseComponentXmlDataTagById(aCaseComponent, aTagId);
	}

	/**
	 * Gets the tag given by aTagId within the xml data of the case component given
	 * by aCacId.
	 *
	 * @param aCacId the a cac id
	 * @param aTagId the a tag id
	 *
	 * @return the tag
	 */
	public IXMLTag getTag(String aCacId, String aTagId) {
		IECaseComponent lCaseComponent = getCaseComponent(aCacId);
		return getTag(lCaseComponent, aTagId);
	}

	/**
	 * Gets the tag name of the parameter tag.
	 *
	 * @param aTag the a tag
	 *
	 * @return the tag name
	 */
	public String getTagName(IXMLTag aTag) {
		if (aTag == null)
			return getPropVView().getLabel("cde_s_script.invalid");
		return getXmlManager().getTagKeyValues(aTag, aTag.getDefAttribute(AppConstants.defKeyKey));
	}

	/**
	 * Gets the tag name of the tag given by aTagId within the xml data of the case
	 * component given by aCacId.
	 *
	 * @param aCacId the a cac id
	 * @param aTagId the a tag id
	 *
	 * @return the tag name
	 */
	public String getTagName(String aCacId, String aTagId) {
		return getTagName(getTag(aCacId, aTagId));
	}

	/**
	 * Gets the case component data. The xml data string of the case component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return the case component data
	 */
	public String getCaseComponentData(IECaseComponent aCaseComponent) {
		return getCaseComponentData(aCaseComponent.getCacId());
	}

	/**
	 * Gets the case component data. The xml data string of the case component given
	 * by aCacId.
	 *
	 * @param aCacId the a cac id
	 *
	 * @return the case component data
	 */
	public String getCaseComponentData(String aCacId) {
		return getCaseComponentData(Integer.parseInt(aCacId));
	}

	/**
	 * Gets the case component data. The xml data string of the case component given
	 * by cacId.
	 *
	 * @param cacId the cac id
	 *
	 * @return the case component data
	 */
	public String getCaseComponentData(int cacId) {
		ICaseComponentManager lBean = (ICaseComponentManager) getBean("caseComponentManager");
		IECaseComponent lComp = lBean.getCaseComponent(cacId);
		String lXmlData = "";
		if (lComp != null)
			lXmlData = lComp.getXmldata();
		// lXmlData must be valid xml.
		if ((lXmlData == null) || (lXmlData.equals("")))
			lXmlData = getEmptyXml();
		return lXmlData;
	}

	/**
	 * Sets case component data. The xml data string of the case component given by
	 * aCacId.
	 *
	 * @param aCacId   the a cac id
	 * @param aXmlData the a xml data
	 */
	public void setCaseComponentData(String aCacId, String aXmlData) {
		ICaseComponentManager lBean = (ICaseComponentManager) getBean("caseComponentManager");
		IECaseComponent lComp = lBean.getCaseComponent(Integer.parseInt(aCacId));
		if (lComp == null)
			return;
		if ((aXmlData == null) || (aXmlData.equals("")))
			aXmlData = getEmptyXml();
		lComp.setXmldata(aXmlData);
		lBean.updateCaseComponent(lComp);
	}

	/**
	 * Gets the current account saved as session variable within the author
	 * environment.
	 *
	 * @return the account
	 */
	public IEAccount getAccount() {
		return ((IAccountManager) getBean("accountManager")).getAccount(getPropCControl().getAccId());
	}

	/**
	 * Has the current account a specific EMERGO role.
	 *
	 * @param aRolCode the role code
	 *
	 * @return boolean
	 */
	public boolean hasRole(String aRolCode) {
		IAccountManager lAccountManager = (IAccountManager) getBean("accountManager");
		if (lAccountManager != null) {
			IEAccount lAccount = lAccountManager.getAccount(getPropCControl().getAccId());
			if (lAccount != null) {
				return lAccountManager.hasRole(lAccount, aRolCode);
			}
		}
		return false;
	}

	/**
	 * Gets the current account in the run, that is the account who started the
	 * Emergo player. If not called from within player null is returned.
	 *
	 * @return the account in run
	 */
	public IEAccount getAccountInRun() {
		if (getRunGroupAccount() == null)
			return null;
		return getRunGroupAccount().getEAccount();
	}

	/**
	 * Gets the current case. If called from within Emergo player case is determined
	 * using run. Otherwise case save as session variable within the author
	 * environment is returned.
	 *
	 * @return the case
	 */
	public IECase getCase() {
		if (inRun())
			return getCaseInRun();
		return ((IECase) getPropCControl().getAccSessAttr("case"));
	}

	/**
	 * Gets the current case in the run associated with the Emergo player. If not
	 * called from within player null is returned.
	 *
	 * @return the case in run
	 */
	protected IECase getCaseInRun() {
		if (getRunGroupAccount() != null)
			return getRunGroupAccount().getERunGroup().getERun().getECase();
		if (getRun() != null)
			return getRun().getECase();
		return null;
	}

	/**
	 * Gets the run associated with the Emergo player. If not called from within
	 * player prop run is returned.
	 *
	 * @return the run
	 */
	public IERun getRun() {
		if (getRunGroupAccount() == null)
			return getPropRun();
		return getRunGroupAccount().getERunGroup().getERun();
	}

	/**
	 * Gets the run given by aRunId.
	 *
	 * @param aRunId the a run id
	 *
	 * @return the run
	 */
	public IERun getRun(int aRunId) {
		if (aRunId == 0)
			return null;
		if (getRun() != null && getRun().getRunId() == aRunId)
			return getRun();
		return ((IRunManager) getBean("runManager")).getRun(aRunId);
	}

	/**
	 * Gets the run group associated with the Emergo player. If not called from
	 * within player null is returned.
	 *
	 * @return the run group
	 */
	public IERunGroup getRunGroup() {
		if (getRunGroupAccount() == null)
			return null;
		return getRunGroupAccount().getERunGroup();
	}

	/**
	 * Gets the run group given by aRugId.
	 *
	 * @param aRugId the a rug id
	 *
	 * @return the run group
	 */
	public IERunGroup getRunGroup(int aRugId) {
		if (aRugId == 0)
			return null;
		if (getRunGroup() != null && getRunGroup().getRugId() == aRugId)
			return getRunGroup();
		return ((IRunGroupManager) getBean("runGroupManager")).getRunGroup(aRugId);
	}

	/**
	 * Gets the run status, the status of the Emergo player. See IAppManager for run
	 * status definitions.
	 *
	 * @return the run status
	 */
	public String getRunStatus() {
		return getPropRunStatus();
	}

	/**
	 * Is run status equal to tutor run.
	 *
	 * @return the run status
	 */
	public boolean isTutorRun() {
		return getPropRunStatus() != null && getPropRunStatus().equals(AppConstants.runStatusTutorRun);
	}

	/**
	 * Checks if run is read only run. If so, Emergo player status is not saved.
	 *
	 * @return true, if is read only run
	 */
	public boolean isReadOnlyRun() {
		return getPropRunStatus() == null || (!getPropRunStatus().equals(AppConstants.runStatusRun)
				&& !getPropRunStatus().equals(AppConstants.runStatusPreview));
	}

	/**
	 * Checks if run is preview run.
	 *
	 * @return true, if is read only run
	 */
	public boolean isPreviewRun() {
		return getPropRunStatus() != null && (getPropRunStatus().equals(AppConstants.runStatusTutorRun)
				|| getPropRunStatus().equals(AppConstants.runStatusPreview)
				|| getPropRunStatus().equals(AppConstants.runStatusPreviewReadOnly));
	}

	/**
	 * Checks if run is test run.
	 *
	 * @return true, if is test run
	 */
	public boolean isTestRun() {
		return getRun() != null && (getRun().getStatus() == AppConstants.run_status_test);
	}

	/**
	 * Sets the run status. See IAppManager for run status definitions.
	 *
	 * @param aRunStatus the new run status
	 */
	public void setRunStatus(String aRunStatus) {
		setPropRunStatus(aRunStatus);
	}

	/**
	 * In run. Checks if Emergo player is started.
	 *
	 * @return true, if successful
	 */
	public boolean inRun() {
		String lStatus = getRunStatus();
		return ((lStatus != null) && (!lStatus.equals("")));
	}

	/**
	 * Gets the current run group account.
	 *
	 * @return the run group account
	 */
	public IERunGroupAccount getRunGroupAccount() {
		return getPropRunGroupAccount();
	}

	/**
	 * Gets the current run team.
	 *
	 * @return the run team
	 */
	public IERunTeam getRunTeam() {
		return getPropRunTeam();
	}

	/**
	 * Gets the run team given by aRutId.
	 *
	 * @param aRutId the a rut id
	 *
	 * @return the run team
	 */
	public IERunTeam getRunTeam(int aRutId) {
		if (aRutId == 0)
			return null;
		if (getRunTeam() != null && getRunTeam().getRutId() == aRutId)
			return getRunTeam();
		return ((IRunTeamManager) getBean("runTeamManager")).getRunTeam(aRutId);
	}

	/**
	 * Gets the run group account given by aRgaId.
	 *
	 * @param aRgaId the a rga id
	 *
	 * @return the run group account
	 */
	public IERunGroupAccount getRunGroupAccount(int aRgaId) {
		if (aRgaId == 0)
			return null;
		if (getRunGroupAccount() != null && getRunGroupAccount().getRgaId() == aRgaId)
			return getRunGroupAccount();
		return ((IRunGroupAccountManager) getBean("runGroupAccountManager")).getRunGroupAccount(aRgaId);
	}

	/**
	 * Gets all run groups within current run.
	 *
	 * @return the run groups
	 */
	public List<IERunGroup> getRunGroups() {
		return ((IRunGroupManager) getBean("runGroupManager")).getAllRunGroupsByRunId(getRun().getRunId());
	}

	/**
	 * Sets the current run group account.
	 *
	 * @param aRunGroupAccount the new run group account
	 */
	public void setRunGroupAccount(IERunGroupAccount aRunGroupAccount) {
		// NOTE clear data cached in SSpring properties for possible previous
		// rungroupaccount.
		// SSpring class is meant to be used for within the player environment for one
		// rungroupaccount
		clearRunBoostProps();
		setPropRunGroupAccount(aRunGroupAccount);
	}

	/**
	 * Sets the current run team.
	 *
	 * @param aRunTeam the new run team
	 */
	public void setRunTeam(IERunTeam aRunTeam) {
		// NOTE clear data cached in SSpring properties for possible previous
		// rungroupaccount
		// SSpring class is meant to be used for within the player environment for one
		// rungroupaccount
		clearRunBoostProps();
		setPropRunTeam(aRunTeam);
	}

	/**
	 * Sets the current run.
	 *
	 * @param aRun the new run
	 */
	public void setRun(IERun aRun) {
		setPropRun(aRun);
	}

	/**
	 * Sets the current run group account.
	 *
	 * @param aRgaId the new run group account
	 */
	public void setRunGroupAccount(int aRgaId) {
		setRunGroupAccount(((IRunGroupAccountManager) getBean("runGroupAccountManager")).getRunGroupAccount(aRgaId));
	}

	/**
	 * Sets the current run team.
	 *
	 * @param aRutId the new run team
	 */
	public void setRunTeam(int aRutId) {
		setRunTeam(((IRunTeamManager) getBean("runTeamManager")).getRunTeam(aRutId));
	}

	/**
	 * Converts a tag template string to a node id if a node for that string exists
	 * in aTagName tags in casecomponent with id aCacId.
	 *
	 * @param aTagTemplate   the tag template string
	 * @param aCaseComponent the a corresponding case component
	 * @param aTagName       the name of the tag nodes
	 *
	 * @return the id of the corresponding node tag
	 */
	public List<String> getTagIdsFromTagTemplate(String aTagTemplate, IECaseComponent aCaseComponent, String aTagName) {
		String lTagName = replaceVariablesWithinString(aTagTemplate);
		List<IXMLTag> lNodeTags = getPropCScript().getNodeTags(aCaseComponent, aTagName);
		List<String> lIds = new ArrayList<String>();
		for (IXMLTag lNodeTag : lNodeTags) {
			String lNodeTagName = getTagName(lNodeTag);
			boolean lFound = false;
			if (lNodeTagName.equals(lTagName)) {
				lFound = true;
			} else {
				try {
					if (lNodeTagName.matches(lTagName)) {
						lFound = true;
					}
				} catch (PatternSyntaxException pError) {
				}
			}
			if (lFound)
				lIds.add(lNodeTag.getAttribute(AppConstants.keyId));
		}
		return lIds;
	}

	/**
	 * Gets the run group case component status.
	 *
	 * @param aRunGroup      the a run group
	 * @param aCaseComponent the a case component
	 *
	 * @return the run group case component
	 */
	public String getRunGroupCaseComponentStatus(IERunGroup aRunGroup, IECaseComponent aCaseComponent) {
		return getRunGroupCaseComponentStatus(aRunGroup.getRugId(), aCaseComponent.getCacId());
	}

	/**
	 * Gets the run case component status.
	 *
	 * @param aRun           the a run
	 * @param aCaseComponent the a case component
	 *
	 * @return the run case component
	 */
	public String getRunCaseComponentStatus(IERun aRun, IECaseComponent aCaseComponent) {
		return getRunCaseComponentStatus(aRun.getRunId(), aCaseComponent.getCacId());
	}

	/**
	 * Gets the run team case component status.
	 *
	 * @param aRunTeam       the a run team
	 * @param aCaseComponent the a case component
	 *
	 * @return the run team case component
	 */
	public String getRunTeamCaseComponentStatus(IERunTeam aRunTeam, IECaseComponent aCaseComponent) {
		return getRunTeamCaseComponentStatus(aRunTeam.getRutId(), aCaseComponent.getCacId());
	}

	/**
	 * Gets the run group case component status.
	 *
	 * @param aRugId the a rug id
	 * @param aCacId the a cac id
	 *
	 * @return the run group case component
	 */
	public String getRunGroupCaseComponentStatus(String aRugId, String aCacId) {
		return getRunGroupCaseComponentStatus(Integer.parseInt(aRugId), Integer.parseInt(aCacId));
	}

	/**
	 * Gets the run case component status.
	 *
	 * @param aRunId the a run id
	 * @param aCacId the a cac id
	 *
	 * @return the run case component
	 */
	public String getRunCaseComponentStatus(String aRunId, String aCacId) {
		return getRunCaseComponentStatus(Integer.parseInt(aRunId), Integer.parseInt(aCacId));
	}

	/**
	 * Gets the run team case component status.
	 *
	 * @param aRutId the a rut id
	 * @param aCacId the a cac id
	 *
	 * @return the run team case component
	 */
	public String getRunTeamCaseComponentStatus(String aRutId, String aCacId) {
		return getRunTeamCaseComponentStatus(Integer.parseInt(aRutId), Integer.parseInt(aCacId));
	}

	/**
	 * Gets current run group case components in run.
	 *
	 * @return the run group case components
	 */
	protected Hashtable<String, IERunGroupCaseComponent> getRunGroupCaseComponents() {
		return getPropRunGroupCaseComponents();
	}

	/**
	 * Gets current run case components in run.
	 *
	 * @return the run case components
	 */
	protected Hashtable<String, IERunCaseComponent> getRunCaseComponents() {
		return getPropRunCaseComponents();
	}

	/**
	 * Gets current run team case components.
	 *
	 * @return the run team case components
	 */
	protected Hashtable<String, IERunTeamCaseComponent> getRunTeamCaseComponents() {
		return getPropRunTeamCaseComponents();
	}

	/**
	 * Gets the run group case component. It is kept in memory for better
	 * performance.
	 *
	 * @param rugId the rug id
	 * @param cacId the cac id
	 *
	 * @return the run group case component
	 */
	public synchronized IERunGroupCaseComponent getRunGroupCaseComponent(int rugId, int cacId) {
		String lIds = "" + rugId + "," + cacId;
		IERunGroupCaseComponent lComp = null;
		IRunGroupCaseComponentManager lBean = getRunGroupCaseComponentManager();
		IERunGroup lRunGroup = getRunGroup(rugId);
		if (getRunGroupCaseComponents().containsKey(lIds))
			lComp = getRunGroupCaseComponents().get(lIds);
		else {
			// NOTE get all for rugid to boost performance
			List<IERunGroupCaseComponent> lComps = lBean.getAllRunGroupCaseComponentsByRugId(rugId);
			for (IERunGroupCaseComponent lCompTemp : lComps) {
				String lIdsTemp = "" + rugId + "," + lCompTemp.getCacCacId();
				if (lRunGroup == getRunGroup()) {
					// NOTE Only buffer in memory if current rungroup, so sSpring only contains
					// current rungroup data
					if (!getRunGroupCaseComponents().containsKey(lIdsTemp)) {
						getRunGroupCaseComponents().put(lIdsTemp, lCompTemp);
					}
				}
				if (lCompTemp.getCacCacId() == cacId) {
					lComp = lCompTemp;
					break;
				}
			}
		}
		if (lComp == null) {
			IECaseComponent lCaseComponent = getCaseComponent(cacId);
			if (lRunGroup != null && lCaseComponent != null) {
				String lXmlData = getEmptyXml();
				lComp = lBean.getNewRunGroupCaseComponent();
				lComp.setRugRugId(lRunGroup.getRugId());
				lComp.setCacCacId(lCaseComponent.getCacId());
				lComp.setXmldata(lXmlData);
				if (lRunGroup == getRunGroup()) {
					lBean.updateRunGroupCaseComponent(lComp);
				} else {
					// NOTE Set creation date otherwise it is empty when rgc is saved
					lComp.setCreationdate(new Date());
				}
				if (lRunGroup == getRunGroup()) {
					// NOTE Only buffer in memory if current rungroup, so sSpring only contains
					// current rungroup data
					getRunGroupCaseComponents().put(lIds, lComp);
				}
			}
		}
		return lComp;
	}

	/**
	 * Gets the run case component. It is kept in memory for better performance.
	 *
	 * @param runId the run id
	 * @param cacId the cac id
	 *
	 * @return the run case component
	 */
	public synchronized IERunCaseComponent getRunCaseComponent(int runId, int cacId) {
		String lIds = "" + runId + "," + cacId;
		IERunCaseComponent lComp = null;
		IRunCaseComponentManager lBean = (IRunCaseComponentManager) getBean("runCaseComponentManager");
		if (getRunCaseComponents().containsKey(lIds))
			lComp = getRunCaseComponents().get(lIds);
		else {
			lComp = lBean.getRunCaseComponent(runId, cacId);
			if (lComp != null)
				getRunCaseComponents().put(lIds, lComp);
		}
		if (lComp == null) {
			IERun lRun = getRun(runId);
			IECaseComponent lCaseComponent = getCaseComponent(cacId);
			if ((lRun != null) && (lCaseComponent != null)) {
				String lXmlData = getEmptyXml();
				// add new
				lComp = lBean.getNewRunCaseComponent();
				lComp.setRunRunId(lRun.getRunId());
				lComp.setCacCacId(lCaseComponent.getCacId());
				lComp.setXmldata(lXmlData);
				if (lRun == getRun()) {
					// NOTE Only save component if current run
					lBean.newRunCaseComponent(lComp);
				}
				getRunCaseComponents().put(lIds, lComp);
			}
		}
		return lComp;
	}

	/**
	 * Gets the run team case component. It is kept in memory for better
	 * performance.
	 *
	 * @param rutId the rut id
	 * @param cacId the cac id
	 *
	 * @return the run team case component
	 */
	public synchronized IERunTeamCaseComponent getRunTeamCaseComponent(int rutId, int cacId) {
		String lIds = "" + rutId + "," + cacId;
		IERunTeamCaseComponent lComp = null;
		IRunTeamCaseComponentManager lBean = (IRunTeamCaseComponentManager) getBean("runTeamCaseComponentManager");
		if (getRunTeamCaseComponents().containsKey(lIds))
			lComp = getRunTeamCaseComponents().get(lIds);
		else {
			lComp = lBean.getRunTeamCaseComponent(rutId, cacId);
			if (lComp != null)
				getRunTeamCaseComponents().put(lIds, lComp);
		}
		if (lComp == null) {
			IERunTeam lRunTeam = getRunTeam(rutId);
			IECaseComponent lCaseComponent = getCaseComponent(cacId);
			if ((lRunTeam != null) && (lCaseComponent != null)) {
				String lXmlData = getEmptyXml();
				// add new
				lComp = lBean.getNewRunTeamCaseComponent();
				lComp.setRutRutId(lRunTeam.getRutId());
				lComp.setCacCacId(lCaseComponent.getCacId());
				lComp.setXmldata(lXmlData);
				if (lRunTeam == getRunTeam()) {
					// NOTE Only save component if current runteam
					lBean.newRunTeamCaseComponent(lComp);
				}
				getRunTeamCaseComponents().put(lIds, lComp);
			}
		}
		return lComp;
	}

	/**
	 * Gets the run group case component xml status.
	 *
	 * @param rugId the rug id
	 * @param cacId the cac id
	 *
	 * @return the run group case component xml status
	 */
	public String getRunGroupCaseComponentStatus(int rugId, int cacId) {
		IERunGroupCaseComponent lComp = getRunGroupCaseComponent(rugId, cacId);
		String lXmlStatus = "";
		if (lComp != null)
			lXmlStatus = lComp.getXmldata();
		if (lXmlStatus == null)
			lXmlStatus = "";
		// lXmlData must be valid xml.
		if (lXmlStatus.equals(""))
			lXmlStatus = getEmptyXml();
		return lXmlStatus;
	}

	/**
	 * Gets the run case component xml status.
	 *
	 * @param runId the run id
	 * @param cacId the cac id
	 *
	 * @return the run case component xml status
	 */
	public String getRunCaseComponentStatus(int runId, int cacId) {
		IERunCaseComponent lComp = getRunCaseComponent(runId, cacId);
		String lXmlStatus = "";
		if (lComp != null)
			lXmlStatus = lComp.getXmldata();
		if (lXmlStatus == null)
			lXmlStatus = "";
		// lXmlStatus must be valid xml.
		if (lXmlStatus.equals(""))
			lXmlStatus = getEmptyXml();
		return lXmlStatus;
	}

	/**
	 * Gets the run team case component xml status.
	 *
	 * @param rutId the rut id
	 * @param cacId the cac id
	 *
	 * @return the run team case component xml status
	 */
	public String getRunTeamCaseComponentStatus(int rutId, int cacId) {
		IERunTeamCaseComponent lComp = getRunTeamCaseComponent(rutId, cacId);
		String lXmlStatus = "";
		if (lComp != null)
			lXmlStatus = lComp.getXmldata();
		if (lXmlStatus == null)
			lXmlStatus = "";
		// lXmlStatus must be valid xml.
		if (lXmlStatus.equals(""))
			lXmlStatus = getEmptyXml();
		return lXmlStatus;
	}

	/**
	 * Sets run group case component xml status. Method is called from control
	 * package by admin and preview options.
	 *
	 * @param rugId      the rug id
	 * @param cacId      the cac id
	 * @param aXmlStatus the a xml status
	 */
	public void setRunGroupCaseComponentStatus(int rugId, int cacId, String aXmlStatus) {
		IERunGroupCaseComponent lComp = getRunGroupCaseComponent(rugId, cacId);
		if (lComp != null) {
			IRunGroupCaseComponentManager lBean = getRunGroupCaseComponentManager();
			if ((aXmlStatus == null) || (aXmlStatus.equals(""))) {
				// if new status is empty first delete blobs
				lBean.deleteBlobs(lComp);
				aXmlStatus = getEmptyXml();
			}
			lComp.setXmldata(aXmlStatus);
			lBean.updateRunGroupCaseComponent(lComp);
		}
	}

	/**
	 * Sets run case component xml status. Method is called from control package by
	 * admin and preview options.
	 *
	 * @param runId      the run id
	 * @param cacId      the cac id
	 * @param aXmlStatus the a xml status
	 */
	public void setRunCaseComponentStatus(int runId, int cacId, String aXmlStatus) {
		IERunCaseComponent lComp = getRunCaseComponent(runId, cacId);
		if (lComp != null) {
			IRunCaseComponentManager lBean = (IRunCaseComponentManager) getBean("runCaseComponentManager");
			IERunCaseComponent lDbComp = lBean.getRunCaseComponent(lComp.getRccId());
			String lXmlStatus = handleRunMultiUser(lComp, lDbComp, aXmlStatus);
			if (lXmlStatus.equals(""))
				return;
			else
				aXmlStatus = lXmlStatus;
			if ((aXmlStatus == null) || (aXmlStatus.equals(""))) {
				// if new status is empty first delete blobs
				lBean.deleteBlobs(lComp);
				aXmlStatus = getEmptyXml();
			}
			lComp.setXmldata(aXmlStatus);
			lBean.updateRunCaseComponent(lComp);
		}
	}

	/**
	 * Sets run team case component xml status. Method is called from control
	 * package by admin and preview options.
	 *
	 * @param rutId      the rut id
	 * @param cacId      the cac id
	 * @param aXmlStatus the a xml status
	 */
	public void setRunTeamCaseComponentStatus(int rutId, int cacId, String aXmlStatus) {
		IERunTeamCaseComponent lComp = getRunTeamCaseComponent(rutId, cacId);
		if (lComp != null) {
			IRunTeamCaseComponentManager lBean = (IRunTeamCaseComponentManager) getBean("runTeamCaseComponentManager");
			IERunTeamCaseComponent lDbComp = lBean.getRunTeamCaseComponent(lComp.getRtcId());
			String lXmlStatus = handleRunTeamMultiUser(lComp, lDbComp, aXmlStatus);
			if (lXmlStatus.equals(""))
				return;
			else
				aXmlStatus = lXmlStatus;
			if ((aXmlStatus == null) || (aXmlStatus.equals(""))) {
				// if new status is empty first delete blobs
				lBean.deleteBlobs(lComp);
				aXmlStatus = getEmptyXml();
			}
			lComp.setXmldata(aXmlStatus);
			lBean.updateRunTeamCaseComponent(lComp);
		}
	}

	/**
	 * Handles run multi user. Checks if one user doesn't overwrite shared content
	 * of other by checking lastupdatedate field of database record. If no
	 * overwrite, the original aXmlStatus is returned. Otherwise an empty string.
	 *
	 * @param aComp      the a comp
	 * @param aDbComp    the a db comp
	 * @param aXmlStatus the a xml status
	 *
	 * @return the string
	 */
	public String handleRunMultiUser(IERunCaseComponent aComp, IERunCaseComponent aDbComp, String aXmlStatus) {
		if (!aDbComp.getLastupdatedate().after(aComp.getLastupdatedate()))
			return aXmlStatus;
		return "";
	}

	/**
	 * Handles run team multi user. Checks if one user doesn't overwrite shared
	 * content of other by checking lastupdatedate field of database record. If no
	 * overwrite, the original aXmlStatus is returned. Otherwise an empty string.
	 *
	 * @param aComp      the a comp
	 * @param aDbComp    the a db comp
	 * @param aXmlStatus the a xml status
	 *
	 * @return the string
	 */
	public String handleRunTeamMultiUser(IERunTeamCaseComponent aComp, IERunTeamCaseComponent aDbComp,
			String aXmlStatus) {
		if (!aDbComp.getLastupdatedate().after(aComp.getLastupdatedate()))
			return aXmlStatus;
		return "";
	}

	/**
	 * Gets save run group case component status using root tag. Was saved in
	 * memory.
	 *
	 * @param rugId the rug id
	 * @param cacId the cac id
	 */
	public IXMLTag getSaveRunGroupCaseComponentStatusRootTag(int rugId, int cacId) {
		String lIds = "" + rugId + "," + cacId;
		if (getPropSaveRunGroupCaseComponentStatusRootTags().containsKey(lIds)) {
			return getPropSaveRunGroupCaseComponentStatusRootTags().get(lIds);
		}
		return null;
	}

	/**
	 * Sets save run group case component status root tag.
	 *
	 * @param aRunGroup      the a run group
	 * @param aCaseComponent the a case component
	 * @param aRootTag       the a root tag
	 */
	public void setSaveRunGroupCaseComponentStatusRootTag(IERunGroup aRunGroup, IECaseComponent aCaseComponent,
			IXMLTag aRootTag) {
		setSaveRunGroupCaseComponentStatusRootTag(aRunGroup.getRugId(), aCaseComponent.getCacId(), aRootTag);
	}

	/**
	 * Gets save run case component status using root tag. Was saved in memory.
	 *
	 * @param runId the run id
	 * @param cacId the cac id
	 */
	public IXMLTag getSaveRunCaseComponentStatusRootTag(int runId, int cacId) {
		String lIds = "" + runId + "," + cacId;
		if (getPropSaveRunCaseComponentStatusRootTags().containsKey(lIds)) {
			return getPropSaveRunCaseComponentStatusRootTags().get(lIds);
		}
		return null;
	}

	/**
	 * Sets run case component status root tag.
	 *
	 * @param aRun           the a run
	 * @param aCaseComponent the a case component
	 * @param aRootTag       the a root tag
	 */
	public void setRunCaseComponentStatusRootTag(IERun aRun, IECaseComponent aCaseComponent, IXMLTag aRootTag) {
		setRunCaseComponentStatusRootTag(aRun.getRunId(), aCaseComponent.getCacId(), aRootTag);
	}

	/**
	 * Gets save run team case component status using root tag. Was saved in memory.
	 *
	 * @param rutId the rut id
	 * @param cacId the cac id
	 */
	public IXMLTag getSaveRunTeamCaseComponentStatusRootTag(int rutId, int cacId) {
		String lIds = "" + rutId + "," + cacId;
		if (getPropSaveRunTeamCaseComponentStatusRootTags().containsKey(lIds)) {
			return getPropSaveRunTeamCaseComponentStatusRootTags().get(lIds);
		}
		return null;
	}

	/**
	 * Sets run team case component status root tag.
	 *
	 * @param aRunTeam       the a run team
	 * @param aCaseComponent the a case component
	 * @param aRootTag       the a root tag
	 */
	public void setRunTeamCaseComponentStatusRootTag(IERunTeam aRunTeam, IECaseComponent aCaseComponent,
			IXMLTag aRootTag) {
		setRunTeamCaseComponentStatusRootTag(aRunTeam.getRutId(), aCaseComponent.getCacId(), aRootTag);
	}

	/**
	 * Sets save run group case component status root tag. Saves changed status in
	 * memory.
	 *
	 * @param rugId    the rug id
	 * @param cacId    the cac id
	 * @param aRootTag the a root tag
	 */
	public void setSaveRunGroupCaseComponentStatusRootTag(int rugId, int cacId, IXMLTag aRootTag) {
		String lIds = "" + rugId + "," + cacId;
		if (!getPropSaveRunGroupCaseComponentStatusRootTags().containsKey(lIds)) {
			if (aRootTag != null) {
				// NOTE This is the only place where
				// getPropSaveRunGroupCaseComponentStatusRootTags() is set!!
				// It is cleared in pSaveRunGroupCaseComponentsStatus, but never overwritten.
				getPropSaveRunGroupCaseComponentStatusRootTags().put(lIds, aRootTag);
				if (!PropsValues.SSPRING_CACHE_RUNGROUP_STATUS) {
					// NOTE if no caching save run group status right away
					pSaveRunGroupCaseComponentsStatus();
				}
			}
		}
	}

	/**
	 * Sets run case component status root tag. Saves changed status directly in
	 * database.
	 *
	 * @param runId    the run id
	 * @param cacId    the cac id
	 * @param aRootTag the a root tag
	 */
	public void setRunCaseComponentStatusRootTag(int runId, int cacId, IXMLTag aRootTag) {
		IRunCaseComponentManager lBean = (IRunCaseComponentManager) getBean("runCaseComponentManager");
		String lXmlStatus = getXmlManager().xmlTreeToDoc(aRootTag);
		IERunCaseComponent lComp = getRunCaseComponent(runId, cacId);
		if (lComp != null) {
			if ((lXmlStatus == null) || (lXmlStatus.equals("")))
				lXmlStatus = getEmptyXml();
			lComp.setXmldata(lXmlStatus);
			if (!isReadOnlyRun())
				// only save as not read only run
				lBean.updateRunCaseComponent(lComp);
		}
	}

	/**
	 * Sets run team case component status root tag. Saves changed status directly
	 * in database.
	 *
	 * @param rutId    the rut id
	 * @param cacId    the cac id
	 * @param aRootTag the a root tag
	 */
	public void setRunTeamCaseComponentStatusRootTag(int rutId, int cacId, IXMLTag aRootTag) {
		IRunTeamCaseComponentManager lBean = (IRunTeamCaseComponentManager) getBean("runTeamCaseComponentManager");
		String lXmlStatus = getXmlManager().xmlTreeToDoc(aRootTag);
		IERunTeamCaseComponent lComp = getRunTeamCaseComponent(rutId, cacId);
		if (lComp != null) {
			if ((lXmlStatus == null) || (lXmlStatus.equals("")))
				lXmlStatus = getEmptyXml();
			lComp.setXmldata(lXmlStatus);
			if (!isReadOnlyRun())
				// only save as not read only run
				lBean.updateRunTeamCaseComponent(lComp);
		}
	}

	/**
	 * Sets save run group case component status root tag.
	 *
	 * @param aRugId   the a rug id
	 * @param aCacId   the a cac id
	 * @param aRootTag the a root tag
	 */
	public void setSaveRunGroupCaseComponentStatusRootTag(String aRugId, String aCacId, IXMLTag aRootTag) {
		setSaveRunGroupCaseComponentStatusRootTag(Integer.parseInt(aRugId), Integer.parseInt(aCacId), aRootTag);
	}

	/**
	 * Sets run case component status root tag.
	 *
	 * @param aRunId   the a run id
	 * @param aCacId   the a cac id
	 * @param aRootTag the a root tag
	 */
	public void setRunCaseComponentStatusRootTag(String aRunId, String aCacId, IXMLTag aRootTag) {
		setRunCaseComponentStatusRootTag(Integer.parseInt(aRunId), Integer.parseInt(aCacId), aRootTag);
	}

	/**
	 * Sets run team case component status root tag.
	 *
	 * @param aRutId   the a rut id
	 * @param aCacId   the a cac id
	 * @param aRootTag the a root tag
	 */
	public void setRunTeamCaseComponentStatusRootTag(String aRutId, String aCacId, IXMLTag aRootTag) {
		setRunTeamCaseComponentStatusRootTag(Integer.parseInt(aRutId), Integer.parseInt(aCacId), aRootTag);
	}

	/**
	 * Saves all changed status of run group case components, run case components
	 * and run team case components, in db.
	 */
	public void saveRunCaseComponentsStatus() {
		pSaveRunGroupCaseComponentsStatus();
		pSaveRunCaseComponentsStatus();
		pSaveRunTeamCaseComponentsStatus();
	}

	public void copyRunCaseComponentsStatus(SSpring aSpring) {
		pCopyRunGroupCaseComponentsStatus(aSpring);
		pCopyRunCaseComponentsStatus(aSpring);
		pCopyRunTeamCaseComponentsStatus(aSpring);
	}

	/**
	 * P save changed run group case components status and clears them from memory.
	 */
	private void pSaveRunGroupCaseComponentsStatus() {
		IRunGroupCaseComponentManager lBean = getRunGroupCaseComponentManager();
		for (Enumeration<String> keys = getPropSaveRunGroupCaseComponentStatusRootTags().keys(); keys
				.hasMoreElements();) {
			String lIds = keys.nextElement();
			String[] lIdArr = lIds.split(",");
			int rugId = Integer.parseInt(lIdArr[0]);
			int cacId = Integer.parseInt(lIdArr[1]);
			IXMLTag lRootTag = getPropSaveRunGroupCaseComponentStatusRootTags().get(lIds);
			String lXmlStatus = getXmlManager().xmlTreeToDoc(lRootTag);
			IERunGroupCaseComponent lComp = getRunGroupCaseComponent(rugId, cacId);
			if (lComp != null) {
				if ((lXmlStatus == null) || (lXmlStatus.equals("")))
					lXmlStatus = getEmptyXml();
				lComp.setXmldata(lXmlStatus);
				if (!isReadOnlyRun())
					// only save if not read only run
					lBean.updateRunGroupCaseComponent(lComp);
			}
			// NOTE Added following statement otherwise status saved in memory is not
			// updated!
			getPropXmlRunGroupStatusTree().put(lIds, lRootTag);
		}
		// clear memory
		setPropSaveXmlRunGroupStatusTree(new Hashtable<String, IXMLTag>(0));
		// also save updates in update table
		getSUpdateHelper().saveRunGroupCaseComponentUpdates();
	}

	private void pCopyRunGroupCaseComponentsStatus(SSpring aSpring) {
		for (Enumeration<String> keys = getPropSaveRunGroupCaseComponentStatusRootTags().keys(); keys
				.hasMoreElements();) {
			String lIds = keys.nextElement();
			IXMLTag lRootTag = getPropSaveRunGroupCaseComponentStatusRootTags().get(lIds);
			aSpring.getPropSaveRunGroupCaseComponentStatusRootTags().put(lIds, lRootTag);
		}
	}

	/**
	 * P save changed run case components status and clears them from memory.
	 */
	private void pSaveRunCaseComponentsStatus() {
		IRunCaseComponentManager lBean = (IRunCaseComponentManager) getBean("runCaseComponentManager");
		for (Enumeration<String> keys = getPropSaveRunCaseComponentStatusRootTags().keys(); keys.hasMoreElements();) {
			String lIds = keys.nextElement();
			String[] lIdArr = lIds.split(",");
			int runId = Integer.parseInt(lIdArr[0]);
			int cacId = Integer.parseInt(lIdArr[1]);
			IXMLTag lRootTag = getPropSaveRunCaseComponentStatusRootTags().get(lIds);
			String lXmlStatus = getXmlManager().xmlTreeToDoc(lRootTag);
			IERunCaseComponent lComp = getRunCaseComponent(runId, cacId);
			if (lComp != null) {
				if ((lXmlStatus == null) || (lXmlStatus.equals("")))
					lXmlStatus = getEmptyXml();
				lComp.setXmldata(lXmlStatus);
				if (!isReadOnlyRun())
					// only save if not read only run
					lBean.updateRunCaseComponent(lComp);
			}
			// NOTE Added following statement otherwise status saved in memory is not
			// updated!
			getPropXmlRunStatusTree().put(lIds, lRootTag);
		}
		// clear memory
		setPropSaveXmlRunStatusTree(new Hashtable<String, IXMLTag>(0));
	}

	private void pCopyRunCaseComponentsStatus(SSpring aSpring) {
		for (Enumeration<String> keys = getPropSaveRunCaseComponentStatusRootTags().keys(); keys.hasMoreElements();) {
			String lIds = keys.nextElement();
			IXMLTag lRootTag = getPropSaveRunCaseComponentStatusRootTags().get(lIds);
			aSpring.getPropSaveRunCaseComponentStatusRootTags().put(lIds, lRootTag);
		}
	}

	/**
	 * P save changed run team case components status and clears them from memory.
	 */
	private void pSaveRunTeamCaseComponentsStatus() {
		IRunTeamCaseComponentManager lBean = (IRunTeamCaseComponentManager) getBean("runTeamCaseComponentManager");
		for (Enumeration<String> keys = getPropSaveRunTeamCaseComponentStatusRootTags().keys(); keys
				.hasMoreElements();) {
			String lIds = keys.nextElement();
			String[] lIdArr = lIds.split(",");
			int rugId = Integer.parseInt(lIdArr[0]);
			int cacId = Integer.parseInt(lIdArr[1]);
			IXMLTag lRootTag = getPropSaveRunTeamCaseComponentStatusRootTags().get(lIds);
			String lXmlStatus = getXmlManager().xmlTreeToDoc(lRootTag);
			IERunTeamCaseComponent lComp = getRunTeamCaseComponent(rugId, cacId);
			if (lComp != null) {
				if ((lXmlStatus == null) || (lXmlStatus.equals("")))
					lXmlStatus = getEmptyXml();
				lComp.setXmldata(lXmlStatus);
				if (!isReadOnlyRun())
					// only save if not read only run
					lBean.updateRunTeamCaseComponent(lComp);
			}
			// NOTE Added following statement otherwise status saved in memory is not
			// updated!
			getPropXmlRunTeamStatusTree().put(lIds, lRootTag);
		}
		// clear memory
		setPropSaveXmlRunTeamStatusTree(new Hashtable<String, IXMLTag>(0));
	}

	private void pCopyRunTeamCaseComponentsStatus(SSpring aSpring) {
		for (Enumeration<String> keys = getPropSaveRunTeamCaseComponentStatusRootTags().keys(); keys
				.hasMoreElements();) {
			String lIds = keys.nextElement();
			IXMLTag lRootTag = getPropSaveRunTeamCaseComponentStatusRootTags().get(lIds);
			aSpring.getPropSaveRunTeamCaseComponentStatusRootTags().put(lIds, lRootTag);
		}
	}

	/**
	 * Resets all status of run group case components, run case components and run
	 * team case components, in db.
	 */
	public void resetRunCaseComponentsStatus() {
		// If in run clear status, also in database
		getPropSaveRunGroupCaseComponentStatusRootTags().clear();
		setPropSaveXmlRunGroupStatusTree(new Hashtable<String, IXMLTag>(0));

		IERunGroupAccount lRunGroupAccount = getRunGroupAccount();
		if (lRunGroupAccount != null) {
			IECase lCase = lRunGroupAccount.getERunGroup().getERun().getECase();
			List<IECaseComponent> lCaseComponents = ((ICaseComponentManager) (getBean("caseComponentManager")))
					.getAllCaseComponentsByCasId(lCase.getCasId());
			for (IECaseComponent lCaseComponent : lCaseComponents) {
				setRunGroupCaseComponentStatus(lRunGroupAccount.getERunGroup().getRugId(), lCaseComponent.getCacId(),
						"");
			}
		}
		// TODO reset run and runteam status
		saveRunCaseComponentsStatus();
	}

	/**
	 * Gets the run group case component data plus status.
	 *
	 * @param aRugId the a rug id
	 * @param aCacId the a cac id
	 *
	 * @return the run group case component data plus status
	 */
	public String getRunGroupCaseComponentDataPlusStatus(String aRugId, String aCacId) {
		IXMLTag lRootTag = getXmlDataPlusRunStatusTree(aCacId, AppConstants.statusTypeRunGroup);
		return getXmlManager().xmlTreeToDoc(lRootTag);
	}

	/**
	 * Gets the run case component data plus status.
	 *
	 * @param aRunId the a run id
	 * @param aCacId the a cac id
	 *
	 * @return the run case component data plus status
	 */
	public String getRunCaseComponentDataPlusStatus(String aRunId, String aCacId) {
		IXMLTag lRootTag = getXmlDataPlusRunStatusTree(aCacId, AppConstants.statusTypeRun);
		return getXmlManager().xmlTreeToDoc(lRootTag);
	}

	/**
	 * Gets the run team case component data plus status.
	 *
	 * @param aRutId the a rut id
	 * @param aCacId the a cac id
	 *
	 * @return the run team case component data plus status
	 */
	public String getRunTeamCaseComponentDataPlusStatus(String aRutId, String aCacId) {
		IXMLTag lRootTag = getXmlDataPlusRunStatusTree(aCacId, AppConstants.statusTypeRunTeam);
		return getXmlManager().xmlTreeToDoc(lRootTag);
	}

	/**
	 * Gets the xml definition tree. It is kept in memory for better performance.
	 *
	 * @param aComId the a com id
	 *
	 * @return the xml def tree
	 */
	public IXMLTag getXmlDefTree(String aComId) {
		return getXmlDefTree(getComponent(aComId));
	}

	/**
	 * Gets the xml definition tree. It is kept in memory for better performance.
	 *
	 * @param aComponent the a component
	 *
	 * @return the xml def tree
	 */
	public IXMLTag getXmlDefTree(IEComponent aComponent) {
		if (aComponent == null) {
			return null;
		}
		String lIds = "" + aComponent.getComId();
		if (getAppPropXmlDefTreeByComId().containsKey(lIds)) {
			return getAppPropXmlDefTreeByComId().get(lIds);
		}
		String lXmlDef = aComponent.getXmldefinition();
		IXMLTag lRootTag = getXmlManager().getXmlTree(null, lXmlDef, AppConstants.definition_tag);
		if (lRootTag != null) {
			getAppPropXmlDefTreeByComId().put(lIds, lRootTag);
		}
		return lRootTag;
	}

	/**
	 * Gets the xml data tree. It is kept in memory for better performance.
	 *
	 * @param aCacId the a cac id
	 *
	 * @return the xml data tree
	 */
	public IXMLTag getXmlDataTree(String aCacId) {
		if (inRun()) {
			String lId = "" + aCacId;
			if (getAppPropXmlDataTreeByCacId().containsKey(lId))
				return getAppPropXmlDataTreeByCacId().get(lId);
		}
		IECaseComponent lCaseComponent = getCaseComponent(aCacId);
		return getXmlDataTree(lCaseComponent);
	}

	/**
	 * Gets the xml data tree. It is kept in memory for better performance.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return the xml data tree
	 */
	public IXMLTag getXmlDataTree(IECaseComponent aCaseComponent) {
		if (aCaseComponent == null)
			return null;
		if (!inRun()) {
			return getNonCachedXmlDataTree(aCaseComponent);
		}
		Hashtable<String, IXMLTag> lDataTrees = getAppPropXmlDataTreeByCacId();
		String lId = "" + aCaseComponent.getCacId();
		if (lDataTrees.containsKey(lId)) {
			return lDataTrees.get(lId);
		}
		// case component in run maybe outdated, so check if it should be replaced by
		// the version in the db.
		IECaseComponent lCaseComponentInRun = getCaseComponentInRun(aCaseComponent.getCacId());
		IECaseComponent lCaseComponent = ((ICaseComponentManager) (getBean("caseComponentManager")))
				.getCaseComponent(aCaseComponent.getCacId());
		if (lCaseComponentInRun == null) {
			getPropCaseComponentsInRun().add(lCaseComponent);
			aCaseComponent = lCaseComponent;
		} else {
			if (lCaseComponentInRun.getLastupdatedate().getTime() != lCaseComponent.getLastupdatedate().getTime()) {
				getPropCaseComponentsInRun().remove(aCaseComponent);
				getPropCaseComponentsInRun().add(lCaseComponent);
				aCaseComponent = lCaseComponent;
			}
		}
		IXMLTag lRootTag = getNonCachedXmlDataTree(aCaseComponent);
		if (lRootTag != null) {
			lDataTrees.put(lId, lRootTag);
		}
		return lRootTag;
	}

	/**
	 * Gets non cached xml data tree.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return the xml data tree
	 */
	public IXMLTag getNonCachedXmlDataTree(IECaseComponent aCaseComponent) {
		if (aCaseComponent == null)
			return null;
		IXMLTag lXmlDefRootTag = getXmlDefTree(aCaseComponent.getEComponent());
		String lXmlData = aCaseComponent.getXmldata();
		if ((lXmlData == null) || (lXmlData.equals("")))
			lXmlData = getEmptyXml();
		return getXmlManager().getXmlTree(lXmlDefRootTag, lXmlData, AppConstants.data_tag);
	}

	/**
	 * Gets the xml run data tree.
	 *
	 * @param aRun the a run
	 * 
	 * @return the xml run data tree
	 */
	public IXMLTag getXmlRunDataTree(IERun aRun) {
		if (aRun == null) {
			return null;
		}
		String lXmlRunData = aRun.getXmldata();
		if (lXmlRunData.equals("")) {
			lXmlRunData = CDesktopComponents.sSpring().getEmptyXml();
		}
		return CDesktopComponents.sSpring().getXmlManager().getXmlTree(null, lXmlRunData, AppConstants.data_tag);
	}

	/**
	 * Sets xml run data tree.
	 *
	 * @param aRun     the a run
	 * @param aRootTag the a root tag
	 */
	public void setXmlRunDataTree(IERun aRun, IXMLTag aRootTag) {
		if (aRun == null) {
			return;
		}
		String lXmlRunData = CDesktopComponents.sSpring().getXmlManager().xmlTreeToDoc(aRootTag);
		if (lXmlRunData == null || lXmlRunData.equals("")) {
			lXmlRunData = CDesktopComponents.sSpring().getEmptyXml();
		}
		aRun.setXmldata(lXmlRunData);
		IRunManager lBean = (IRunManager) CDesktopComponents.sSpring().getBean("runManager");
		lBean.updateRun(aRun);
	}

	/**
	 * Gets the case component xml data tag by id. It is kept in memory for better
	 * performance.
	 *
	 * @param aCaseComponent the a case component
	 * @param aTagId         the a tag id
	 *
	 * @return the xml data tag
	 */
	public IXMLTag getCaseComponentXmlDataTagById(IECaseComponent aCaseComponent, String aTagId) {
		IXMLTag lRootTag = null;
		if (inRun()) {
			// NOTE get xml data plus run status tree before calling
			// getXmlDataPlusStatusTagById, so tags are stored in memory
			lRootTag = getXmlDataPlusRunStatusTree(aCaseComponent, AppConstants.statusTypeRunGroup);
			// NOTE getXmlDataPlusStatusTagById only contains node tags
			IXMLTag lNodeTag = getXmlDataPlusStatusTagById(aCaseComponent, AppConstants.statusTypeRunGroup, aTagId);
			if (lNodeTag != null) {
				return lNodeTag;
			}
			// NOTE To be sure have old method to get tag by id as fallback
			// It is necessary for the content root tag, because it isn't a node tag, but
			// has id 1
			IXMLTag lTag = getXmlManager().getTagById(lRootTag, aTagId);
			if (lTag != null) {
				return lTag;
			}
		} else {
			lRootTag = getXmlDataTree(aCaseComponent);
		}
		List<IXMLTag> lNodeTags = getPropCScript().getNodeTags(lRootTag);
		for (IXMLTag lNodeTag : lNodeTags) {
			if (lNodeTag.getAttribute(AppConstants.keyId).equals(aTagId)) {
				return lNodeTag;
			}
		}
		// NOTE return content tag if tag id = 1
		if (lRootTag != null && aTagId.equals("1")) {
			return lRootTag.getChild(AppConstants.contentElement);
		}
		return null;
	}

	/**
	 * Gets the xml run status tree depending on status type, see AppConstants.
	 *
	 * @param aCacId      the a cac id
	 * @param aStatusType the a status type
	 *
	 * @return the xml status tree
	 */
	public IXMLTag getXmlRunStatusTree(String aCacId, String aStatusType) {
		IECaseComponent lCaseComponent = getCaseComponent(aCacId);
		return getXmlRunStatusTree(lCaseComponent, aStatusType);
	}

	/**
	 * Gets the xml run status tree depending on status type, see AppConstants.
	 *
	 * @param aCaseComponent the a case component
	 * @param aStatusType    the a status type
	 *
	 * @return the xml status tree
	 */
	public IXMLTag getXmlRunStatusTree(IECaseComponent aCaseComponent, String aStatusType) {
		if (aStatusType.equals(AppConstants.statusTypeRun))
			return pGetXmlRunStatusTree(aCaseComponent);
		if (aStatusType.equals(AppConstants.statusTypeRunGroup))
			return pGetXmlRunGroupStatusTree(aCaseComponent);
		if (aStatusType.equals(AppConstants.statusTypeRunTeam))
			return pGetXmlRunTeamStatusTree(aCaseComponent);
		return null;
	}

	/**
	 * Populates status tag hashtables used to find xml tags faster.
	 *
	 * @param aStatusRootTag         the a status root tag
	 * @param aStatusTagsById        the status tags by id
	 * @param aStatusTagsByRefdataid the status tags by ref data id
	 */
	private void populateStatusTagsContainers(IXMLTag aStatusRootTag, Hashtable<String, IXMLTag> aStatusTagsById,
			Hashtable<String, IXMLTag> aStatusTagsByRefdataid) {
		List<IXMLTag> lNodeTags = getPropCScript().getNodeTags(aStatusRootTag);
		if (lNodeTags == null) {
			return;
		}
		for (IXMLTag lNodeTag : lNodeTags) {
			if (!lNodeTag.getAttribute(AppConstants.keyId).equals("")) {
				aStatusTagsById.put(lNodeTag.getAttribute(AppConstants.keyId), lNodeTag);
			}
			if (!lNodeTag.getAttribute(AppConstants.keyRefdataid).equals("")) {
				aStatusTagsByRefdataid.put(lNodeTag.getAttribute(AppConstants.keyRefdataid), lNodeTag);
			}
		}
	}

	/**
	 * P get xml run group status tree. It is kept in memory for better performance.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return the xml status tree
	 */
	private IXMLTag pGetXmlRunGroupStatusTree(IECaseComponent aCaseComponent) {
		IERunGroup lRunGroup = getRunGroup();

		// NOTE If root tag is saved in memory by save action use it because it contains
		// the last changes
		IXMLTag lStatusRootTag = getSaveRunGroupCaseComponentStatusRootTag(lRunGroup.getRugId(),
				aCaseComponent.getCacId());
		if (lStatusRootTag != null) {
			return lStatusRootTag;
		}

		// NOTE Otherwise get the buffered read read root tag
		// It is overwritten when status is saved in database in
		// pSaveRunGroupCaseComponentsStatus
		String lIds = "" + lRunGroup.getRugId() + "," + aCaseComponent.getCacId();
		if (getPropXmlRunGroupStatusTree().containsKey(lIds)) {
			return getPropXmlRunGroupStatusTree().get(lIds);
		}

		// NOTE Or read it from db
		IXMLTag lXmlDefRootTag = getXmlDefTree(aCaseComponent.getEComponent());
		String lXmlStatus = getRunGroupCaseComponentStatus(lRunGroup, aCaseComponent);
		IXMLTag lRootTag = getXmlManager().getXmlTree(lXmlDefRootTag, lXmlStatus, AppConstants.status_tag);
		if (lRootTag != null) {
			getPropXmlRunGroupStatusTree().put(lIds, lRootTag);
			Hashtable<String, IXMLTag> lStatusTagsById = new Hashtable<String, IXMLTag>();
			Hashtable<String, IXMLTag> lStatusTagsByRefdataid = new Hashtable<String, IXMLTag>();
			populateStatusTagsContainers(lRootTag, lStatusTagsById, lStatusTagsByRefdataid);
			lIds += "," + AppConstants.statusTypeRunGroup;
			getPropXmlStatusTagsById().put(lIds, lStatusTagsById);
		}
		return lRootTag;
	}

	/**
	 * Get xml run group status tree. Not kept in memory, because not used for
	 * current rungroup
	 *
	 * @param aRugId         the a rug id
	 * @param aCaseComponent the a case component
	 *
	 * @return the xml status tree
	 */
	public IXMLTag getXmlRunGroupStatusTree(int aRugId, IECaseComponent aCaseComponent) {
		if (aRugId == getRunGroup().getRugId()) {
			// NOTE Only if current rungroup
			// NOTE If root tag is saved in memory by save action use it because it contains
			// the last changes
			IXMLTag lStatusRootTag = getSaveRunGroupCaseComponentStatusRootTag(aRugId, aCaseComponent.getCacId());
			if (lStatusRootTag != null) {
				return lStatusRootTag;
			}

			// NOTE Otherwise get the buffered read read root tag
			String lIds = "" + aRugId + "," + aCaseComponent.getCacId();
			if (getPropXmlRunGroupStatusTree().containsKey(lIds))
				return getPropXmlRunGroupStatusTree().get(lIds);
		}

		IXMLTag lXmlDefRootTag = getXmlDefTree(aCaseComponent.getEComponent());
		String lXmlStatus = getRunGroupCaseComponentStatus(aRugId, aCaseComponent.getCacId());
		return getXmlManager().getXmlTree(lXmlDefRootTag, lXmlStatus, AppConstants.status_tag);
	}

	/**
	 * P get xml run status tree. It is kept in memory for better performance.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return the xml status tree
	 */
	private IXMLTag pGetXmlRunStatusTree(IECaseComponent aCaseComponent) {
		IERun lRun = getRun();

		// TODO Have to test this. Due to shared status this probably will not work. So
		// commented out code.
		/*
		 * // NOTE If root tag is saved in memory by save action use it because it
		 * contains the last changes IXMLTag lStatusRootTag =
		 * getSaveRunCaseComponentStatusRootTag(lRun.getRunId(),
		 * aCaseComponent.getCacId()); if (lStatusRootTag != null) { return
		 * lStatusRootTag; }
		 */

		// NOTE Otherwise get the buffered read read root tag
		String lIds = "" + aCaseComponent.getCacId() + "," + lRun.getRunId();
		if (getPropXmlRunStatusTree().containsKey(lIds))
			return getPropXmlRunStatusTree().get(lIds);

		// NOTE Or read it from db
		IXMLTag lXmlDefRootTag = getXmlDefTree(aCaseComponent.getEComponent());
		String lXmlStatus = getRunCaseComponentStatus(lRun, aCaseComponent);
		IXMLTag lRootTag = getXmlManager().getXmlTree(lXmlDefRootTag, lXmlStatus, AppConstants.status_tag);
		if (lRootTag != null) {
			getPropXmlRunStatusTree().put(lIds, lRootTag);
			Hashtable<String, IXMLTag> lStatusTagsById = new Hashtable<String, IXMLTag>();
			Hashtable<String, IXMLTag> lStatusTagsByRefdataid = new Hashtable<String, IXMLTag>();
			populateStatusTagsContainers(lRootTag, lStatusTagsById, lStatusTagsByRefdataid);
			lIds += "," + AppConstants.statusTypeRun;
			getPropXmlStatusTagsById().put(lIds, lStatusTagsById);
		}
		return lRootTag;
	}

	/**
	 * Get xml run status tree. Not kept in memory, because not used for current run
	 *
	 * @param aRunId         the a run id
	 * @param aCaseComponent the a case component
	 *
	 * @return the xml status tree
	 */
	public IXMLTag getXmlRunStatusTree(int aRunId, IECaseComponent aCaseComponent) {
		IERun lRun = getRun(aRunId);

		if (lRun.getRunId() == getRun().getRunId()) {
			// NOTE Only if current run
			// NOTE If root tag is saved in memory by save action use it because it contains
			// the last changes
			IXMLTag lStatusRootTag = getSaveRunCaseComponentStatusRootTag(lRun.getRunId(), aCaseComponent.getCacId());
			if (lStatusRootTag != null) {
				return lStatusRootTag;
			}

			// NOTE Otherwise get the buffered read read root tag
			String lIds = "" + lRun.getRunId() + "," + aCaseComponent.getCacId();
			if (getPropXmlRunGroupStatusTree().containsKey(lIds))
				return getPropXmlRunStatusTree().get(lIds);
		}

		IXMLTag lXmlDefRootTag = getXmlDefTree(aCaseComponent.getEComponent());
		String lXmlStatus = getRunCaseComponentStatus(lRun, aCaseComponent);
		return getXmlManager().getXmlTree(lXmlDefRootTag, lXmlStatus, AppConstants.status_tag);
	}

	/**
	 * P get xml run team status tree. It is kept in memory for better performance.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return the xml status tree
	 */
	private IXMLTag pGetXmlRunTeamStatusTree(IECaseComponent aCaseComponent) {
		IERunTeam lRunTeam = getRunTeam();
		if (lRunTeam == null)
			return null;

		// TODO Have to test this. Due to shared status this probably will not work. So
		// commented out code.
		/*
		 * // NOTE If root tag is saved in memory by save action use it because it
		 * contains the last changes IXMLTag lStatusRootTag =
		 * getSaveRunTeamCaseComponentStatusRootTag(lRunTeam.getRutId(),
		 * aCaseComponent.getCacId()); if (lStatusRootTag != null) { return
		 * lStatusRootTag; }
		 */

		// NOTE Otherwise get the buffered read read root tag
		String lIds = "" + aCaseComponent.getCacId() + "," + lRunTeam.getRutId();
		if (getPropXmlRunTeamStatusTree().containsKey(lIds))
			return getPropXmlRunTeamStatusTree().get(lIds);

		// NOTE Or read it from db
		IXMLTag lXmlDefRootTag = getXmlDefTree(aCaseComponent.getEComponent());
		String lXmlStatus = getRunTeamCaseComponentStatus(lRunTeam, aCaseComponent);
		IXMLTag lRootTag = getXmlManager().getXmlTree(lXmlDefRootTag, lXmlStatus, AppConstants.status_tag);
		if (lRootTag != null) {
			getPropXmlRunTeamStatusTree().put(lIds, lRootTag);
			Hashtable<String, IXMLTag> lStatusTagsById = new Hashtable<String, IXMLTag>();
			Hashtable<String, IXMLTag> lStatusTagsByRefdataid = new Hashtable<String, IXMLTag>();
			populateStatusTagsContainers(lRootTag, lStatusTagsById, lStatusTagsByRefdataid);
			lIds += "," + AppConstants.statusTypeRunTeam;
			getPropXmlStatusTagsById().put(lIds, lStatusTagsById);
		}
		return lRootTag;
	}

	/**
	 * Get xml run team status tree. Not kept in memory, because not used for
	 * current runteam
	 *
	 * @param aRugId         the a rug id
	 * @param aCaseComponent the a case component
	 *
	 * @return the xml status tree
	 */
	public IXMLTag getXmlRunTeamStatusTree(int aRutId, IECaseComponent aCaseComponent) {
		IERunTeam lRunTeam = getRunTeam(aRutId);

		if (lRunTeam.getRutId() == getRunTeam().getRutId()) {
			// NOTE Only if current rungroup
			// NOTE If root tag is saved in memory by save action use it because it contains
			// the last changes
			IXMLTag lStatusRootTag = getSaveRunTeamCaseComponentStatusRootTag(lRunTeam.getRutId(),
					aCaseComponent.getCacId());
			if (lStatusRootTag != null) {
				return lStatusRootTag;
			}

			// NOTE Otherwise get the buffered read read root tag
			String lIds = "" + lRunTeam.getRutId() + "," + aCaseComponent.getCacId();
			if (getPropXmlRunTeamStatusTree().containsKey(lIds))
				return getPropXmlRunTeamStatusTree().get(lIds);
		}

		IXMLTag lXmlDefRootTag = getXmlDefTree(aCaseComponent.getEComponent());
		String lXmlStatus = getRunTeamCaseComponentStatus(lRunTeam, aCaseComponent);
		return getXmlManager().getXmlTree(lXmlDefRootTag, lXmlStatus, AppConstants.status_tag);
	}

	/**
	 * Gets the xml run group status tree.
	 *
	 * @param aRunGroupCaseComponent the a run group case component
	 *
	 * @return the xml status tree
	 */
	public IXMLTag getXmlRunGroupStatusTree(IERunGroupCaseComponent aRunGroupCaseComponent) {
		IXMLTag lXmlDefRootTag = getXmlDefTree(getCaseComponent(aRunGroupCaseComponent.getCacCacId()).getEComponent());
		String lXmlStatus = aRunGroupCaseComponent.getXmldata();
		if (lXmlDefRootTag == null || lXmlStatus.equals(""))
			return null;
		IXMLTag lRootTag = getXmlManager().getXmlTree(lXmlDefRootTag, lXmlStatus, AppConstants.status_tag);
		return lRootTag;
	}

	/**
	 * Gets the xml run status tree.
	 *
	 * @param aRunCaseComponent the a run case component
	 *
	 * @return the xml run status tree
	 */
	public IXMLTag getXmlRunStatusTree(IERunCaseComponent aRunCaseComponent) {
		IXMLTag lXmlDefRootTag = getXmlDefTree(getCaseComponent(aRunCaseComponent.getCacCacId()).getEComponent());
		String lXmlStatus = aRunCaseComponent.getXmldata();
		if (lXmlDefRootTag == null || lXmlStatus.equals(""))
			return null;
		IXMLTag lRootTag = getXmlManager().getXmlTree(lXmlDefRootTag, lXmlStatus, AppConstants.status_tag);
		return lRootTag;
	}

	/**
	 * Gets the xml run team status tree.
	 *
	 * @param aRunTeamCaseComponent the a run team case component
	 *
	 * @return the xml run team status tree
	 */
	public IXMLTag getXmlRunTeamStatusTree(IERunTeamCaseComponent aRunTeamCaseComponent) {
		IXMLTag lXmlDefRootTag = getXmlDefTree(getCaseComponent(aRunTeamCaseComponent.getCacCacId()).getEComponent());
		String lXmlStatus = aRunTeamCaseComponent.getXmldata();
		if (lXmlDefRootTag == null || lXmlStatus.equals(""))
			return null;
		IXMLTag lRootTag = getXmlManager().getXmlTree(lXmlDefRootTag, lXmlStatus, AppConstants.status_tag);
		return lRootTag;
	}

	/**
	 * Gets the xml data plus run status tree depending on status type, see
	 * AppConstants.
	 *
	 * @param aCacId      the a cac id
	 * @param aStatusType the a status type
	 *
	 * @return the xml data plus status tree
	 */
	public IXMLTag getXmlDataPlusRunStatusTree(String aCacId, String aStatusType) {
		IECaseComponent lCaseComponent = getCaseComponent(aCacId);
		return getXmlDataPlusRunStatusTree(lCaseComponent, aStatusType);
	}

	/**
	 * Gets all case component roles saved in memory by car id cac id.
	 *
	 * @return the case component roles by car id cac id
	 */
	protected Hashtable<String, List<IECaseComponentRole>> getCaseComponentRolesByCarId() {
		return getPropCaseComponentRolesByCarId();
	}

	/**
	 * Gets the case component roles by car id.
	 *
	 * @param aCarId the a car id
	 *
	 * @return the case component roles by car id
	 */
	public List<IECaseComponentRole> getCaseComponentRolesByCarId(int aCarId) {
		String lId = "" + aCarId;
		List<IECaseComponentRole> lCaseComponentRoles = null;
		if (getCaseComponentRolesByCarId().containsKey(lId))
			lCaseComponentRoles = getCaseComponentRolesByCarId().get(lId);
		else {
			ICaseComponentRoleManager lBean = (ICaseComponentRoleManager) getBean("caseComponentRoleManager");
			lCaseComponentRoles = lBean.getAllCaseComponentRolesByCarId(aCarId);
			if (lCaseComponentRoles != null) {
				getCaseComponentRolesByCarId().put(lId, lCaseComponentRoles);
			}
		}
		return lCaseComponentRoles;
	}

	/**
	 * Gets the case component role by car id cac id.
	 *
	 * @param aCarId the a car id
	 * @param aCacId the a cac id
	 *
	 * @return the case component role by car id cac id
	 */
	protected IECaseComponentRole getCaseComponentRole(int aCarId, int aCacId) {
		List<IECaseComponentRole> lCaseComponentRoles = getCaseComponentRolesByCarId(aCarId);
		if (lCaseComponentRoles != null) {
			for (IECaseComponentRole lCaseComponentRole : lCaseComponentRoles) {
				if (lCaseComponentRole.getCacCacId() == aCacId) {
					return lCaseComponentRole;
				}
			}
		}
		return null;
	}

	/**
	 * Gets the xml data plus run status tag, depending on status type, see
	 * AppConstants.
	 *
	 * @param aCaseComponent the a case component
	 * @param aStatusType    the a status type
	 * @param aTagId         the a tag id
	 *
	 * @return the tag
	 */
	public IXMLTag getXmlDataPlusRunStatusTag(IECaseComponent aCaseComponent, String aStatusType, String aTagId) {
		if (aCaseComponent == null)
			return null;
		IXMLTag lXmlTag = getXmlDataPlusStatusTagById(aCaseComponent, aStatusType, aTagId);
		if (lXmlTag != null) {
			return lXmlTag;
		}
		IXMLTag lRootTag = getXmlDataPlusRunStatusTree(aCaseComponent, aStatusType);
		if (lRootTag == null)
			return null;
		IXMLTag lContentTag = lRootTag.getChild(AppConstants.contentElement);
		if (lContentTag == null)
			return null;
		return getXmlManager().getTagById(lContentTag, aTagId);
	}

	/**
	 * Gets the xml data plus run status tag, depending on status type, see
	 * AppConstants.
	 *
	 * @param aCacId      the a cac id
	 * @param aStatusType the a status type
	 * @param aTagId      the a tag id
	 *
	 * @return the tag
	 */
	public IXMLTag getXmlDataPlusRunStatusTag(String aCacId, String aStatusType, String aTagId) {
		IECaseComponent lCaseComponent = getCaseComponent(aCacId);
		return getXmlDataPlusRunStatusTag(lCaseComponent, aStatusType, aTagId);
	}

	/**
	 * Gets the xml data plus run status tag, depending on status type, see
	 * AppConstants.
	 *
	 * @param aRugId         the a rug id
	 * @param aCaseComponent the a case component
	 * @param aTagId         the a tag id
	 *
	 * @return the tag
	 */
	public IXMLTag getXmlDataPlusRunStatusTag(int aRugId, IECaseComponent aCaseComponent, String aTagId) {
		if (aCaseComponent == null)
			return null;
		if (aRugId == getRunGroup().getRugId()) {
			IXMLTag lXmlTag = getXmlDataPlusStatusTagById(aCaseComponent, AppConstants.statusTypeRunGroup, aTagId);
			if (lXmlTag != null) {
				return lXmlTag;
			}
		}
		IXMLTag lRootTag = getXmlDataPlusRunGroupStatusTree(aRugId, aCaseComponent);
		if (lRootTag == null)
			return null;
		IXMLTag lContentTag = lRootTag.getChild(AppConstants.contentElement);
		if (lContentTag == null)
			return null;
		IXMLTag lXmlTag = getXmlManager().getTagById(lContentTag, aTagId);
		return lXmlTag;
	}

	/**
	 * Gets the xml data plus run status tag, depending on status type, see
	 * AppConstants.
	 *
	 * @param aRugId the a rug id
	 * @param aCacId the a cac id
	 * @param aTagId the a tag id
	 *
	 * @return the tag
	 */
	public IXMLTag getXmlDataPlusRunStatusTag(int aRugId, String aCacId, String aTagId) {
		IECaseComponent lCaseComponent = getCaseComponent(aCacId);
		return getXmlDataPlusRunStatusTag(aRugId, lCaseComponent, aTagId);
	}

	/**
	 * Gets the xml data plus run status tree, depending on status type, see
	 * AppConstants.
	 *
	 * @param aCaseComponent the a case component
	 * @param aStatusType    the a status type
	 *
	 * @return the xml data plus status tree
	 */
	public IXMLTag getXmlDataPlusRunStatusTree(IECaseComponent aCaseComponent, String aStatusType) {
		String lId = "" + aCaseComponent.getCacId() + aStatusType;
		if (getPropXmlDataPlusRunStatusTree().containsKey(lId)) {
			return getPropXmlDataPlusRunStatusTree().get(lId);
		}
		IXMLTag lTag = null;
		if (aStatusType.equals(AppConstants.statusTypeRun))
			lTag = pGetXmlDataPlusRunStatusTree(aCaseComponent, aStatusType);
		if (aStatusType.equals(AppConstants.statusTypeRunGroup))
			lTag = pGetXmlDataPlusRunGroupStatusTree(aCaseComponent, aStatusType);
		if (aStatusType.equals(AppConstants.statusTypeRunTeam))
			lTag = pGetXmlDataPlusRunTeamStatusTree(aCaseComponent, aStatusType);
		if (lTag != null) {
			getPropXmlDataPlusRunStatusTree().put(lId, lTag);
		}
		return lTag;
	}

	/**
	 * Populates data plus status tag hashtables used to find xml tags faster and
	 * handle multilingualism of case, if applicable
	 *
	 * @param aDataRootTag           the a data root tag
	 * @param aDataTagsById          the data tags by id
	 * @param aDataTagsByRefstatusid the data tags by ref status id
	 * @param aLocaleStr             the locale string
	 */
	public void populateDataPlusStatusTagsContainersAndHandleMultiLanguage(IXMLTag aDataRootTag,
			Hashtable<String, IXMLTag> aDataTagsById, Hashtable<String, IXMLTag> aDataTagsByRefstatusid,
			String aLocaleStr) {
		boolean lUseLocale = !StringUtils.isEmpty(aLocaleStr);
		List<IXMLTag> lNodeTags = getPropCScript().getNodeTags(aDataRootTag);
		for (IXMLTag lNodeTag : lNodeTags) {
			if (!lNodeTag.getAttribute(AppConstants.keyId).equals("")) {
				aDataTagsById.put(lNodeTag.getAttribute(AppConstants.keyId), lNodeTag);
				if (lUseLocale) {
					handleMultiLanguage(lNodeTag, aLocaleStr);
				}
			}
			if (!lNodeTag.getAttribute(AppConstants.keyRefstatusid).equals("")) {
				aDataTagsByRefstatusid.put(lNodeTag.getAttribute(AppConstants.keyRefstatusid), lNodeTag);
			}
		}
	}

	/**
	 * Handles multi language case content.
	 *
	 * @param aNodeTag   the a node tag
	 * @param aLocaleStr the locale string
	 */
	private void handleMultiLanguage(IXMLTag aNodeTag, String aLocaleStr) {
		for (IXMLTag lChildTag : aNodeTag.getChildTags()) {
			if (lChildTag.getName().equals(AppConstants.statusElement)) {
				// NOTE handle attributes
				IXMLTag lLanguageChildTag = lChildTag.getChild(aLocaleStr);
				if (lLanguageChildTag != null) {
					// loop through all setable attributes of node tag
					String lDefAttributeNames = "";
					IXMLTag lDefChildTag = aNodeTag.getDefChild(AppConstants.initialstatusElement);
					if (lDefChildTag != null) {
						lDefAttributeNames = lDefChildTag.getAttribute(AppConstants.defKeyAttributes);
					}
					if (!lDefAttributeNames.equals("")) {
						String[] lDefAttributeNameArr = lDefAttributeNames.split(",");
						for (int i = 0; i < lDefAttributeNameArr.length; i++) {
							if (lLanguageChildTag.getAttributes().containsKey(lDefAttributeNameArr[i])) {
								// replace attribute by attribute in other language
								lChildTag.setAttribute(lDefAttributeNameArr[i],
										lLanguageChildTag.getAttribute(lDefAttributeNameArr[i]));
							}
						}
					}
				}
			} else {
				// NOTE handle child tags
				IXMLTag lLanguageChildTag = lChildTag.getChild(aLocaleStr);
				if (lLanguageChildTag != null) {
					// replace value by value in other language
					lChildTag.setValue(lLanguageChildTag.getValue());
				}
			}
		}
	}

	/**
	 * P get xml data plus run group status tree, depending on status type, see
	 * AppConstants.
	 *
	 * @param aRunGroup      the a run group
	 * @param aCaseComponent the a case component
	 * @param aStatusRootTag the a status root tag
	 *
	 * @return the xml data plus status tree
	 */
	private IXMLTag pGetXmlDataPlusRunGroupStatusTree(IERunGroup aRunGroup, IECaseComponent aCaseComponent,
			IXMLTag aStatusRootTag) {
		if (aRunGroup == null || aCaseComponent == null)
			return null;
		// check if casecomponent is available for role
		if (getCaseComponentRole(aRunGroup.getECaseRole().getCarId(), aCaseComponent.getCacId()) == null)
			return null;
		IXMLTag lDataRootTag = null;
		lDataRootTag = getXmlDataTree(aCaseComponent);
//		make a copy of data so data is not polluted with status
		lDataRootTag = getXmlManager().copyTag(lDataRootTag, null);
		if (lDataRootTag == null)
			return null;
//		merge data and status
		Hashtable<String, IXMLTag> lDataTagsById = new Hashtable<String, IXMLTag>();
		Hashtable<String, IXMLTag> lDataTagsByRefstatusid = new Hashtable<String, IXMLTag>();
		String lLocaleString = inRun() && aCaseComponent.getECase().getMultilingual() ? getPropLocaleString() : "";
		populateDataPlusStatusTagsContainersAndHandleMultiLanguage(lDataRootTag, lDataTagsById, lDataTagsByRefstatusid,
				lLocaleString);
		IXMLTag lDataPlusStatusRootTag = getXmlManager().getXmlTree(lDataRootTag, aStatusRootTag, lDataTagsById,
				lDataTagsByRefstatusid);
		// if current rungroup store data tags by id in SSpring property
		if (aRunGroup.getRugId() == getRunGroup().getRugId()) {
			// data tags can be created when data and status are merged, e.g., for mails, so
			// populate tag containers again
			// NOTE no multi language within status so language parameter is ""
			populateDataPlusStatusTagsContainersAndHandleMultiLanguage(lDataPlusStatusRootTag, lDataTagsById,
					lDataTagsByRefstatusid, "");
			String lId = "" + aCaseComponent.getCacId() + "," + AppConstants.statusTypeRunGroup;
			getPropXmlDataPlusStatusTagsById().put(lId, lDataTagsById);
		}
		return lDataPlusStatusRootTag;
	}

	/**
	 * P get xml data plus run group status tree, depending on status type, see
	 * AppConstants.
	 *
	 * @param aCaseComponent the a case component
	 * @param aStatusType    the a status type
	 *
	 * @return the xml data plus status tree
	 */
	private IXMLTag pGetXmlDataPlusRunGroupStatusTree(IECaseComponent aCaseComponent, String aStatusType) {
		if (aCaseComponent == null)
			return null;
		IERunGroup lRunGroup = getRunGroup();
		if (lRunGroup == null)
			return null;
		IXMLTag lStatusRootTag = getXmlRunStatusTree(aCaseComponent, aStatusType);
		return pGetXmlDataPlusRunGroupStatusTree(lRunGroup, aCaseComponent, lStatusRootTag);
	}

	/**
	 * Get xml data plus run group status tree, depending on rug id.
	 *
	 * @param aRugId         the a rug id
	 * @param aCaseComponent the a case component
	 *
	 * @return the xml data plus status tree
	 */
	public IXMLTag getXmlDataPlusRunGroupStatusTree(int aRugId, IECaseComponent aCaseComponent) {
		if (aCaseComponent == null)
			return null;
		IERunGroup lRunGroup = getRunGroup(aRugId);
		if (lRunGroup == null)
			return null;
		IXMLTag lStatusRootTag = getXmlRunGroupStatusTree(aRugId, aCaseComponent);
		return pGetXmlDataPlusRunGroupStatusTree(lRunGroup, aCaseComponent, lStatusRootTag);
	}

	/**
	 * P get xml data plus run status tree, depending on status type, see
	 * AppConstants.
	 *
	 * @param aCaseComponent the a case component
	 * @param aStatusType    the a status type
	 *
	 * @return the xml data plus status tree
	 */
	private IXMLTag pGetXmlDataPlusRunStatusTree(IECaseComponent aCaseComponent, String aStatusType) {
		IECaseRole lCaseRole = getSCaseRoleHelper().getCaseRole();
		if (lCaseRole == null)
			return null;
		// check if casecomponent is available for role
		IECaseComponentRole lCaseComponentRole = getCaseComponentRole(lCaseRole.getCarId(), aCaseComponent.getCacId());
		if (lCaseComponentRole == null)
			return null;
		IERun lRun = getRun();
		if (lRun == null)
			return null;
		IXMLTag lDataRootTag = getXmlDataTree(aCaseComponent);
//		make a copy of data so data is not polluted with status
		lDataRootTag = getXmlManager().copyTag(lDataRootTag, null);
		if (lDataRootTag == null)
			return null;
		IXMLTag lStatusRootTag = getXmlRunStatusTree(aCaseComponent, aStatusType);
//		merge data and status
		Hashtable<String, IXMLTag> lDataTagsById = new Hashtable<String, IXMLTag>();
		Hashtable<String, IXMLTag> lDataTagsByRefstatusid = new Hashtable<String, IXMLTag>();
		String lLocaleString = inRun() && aCaseComponent.getECase().getMultilingual() ? getPropLocaleString() : "";
		populateDataPlusStatusTagsContainersAndHandleMultiLanguage(lDataRootTag, lDataTagsById, lDataTagsByRefstatusid,
				lLocaleString);
		IXMLTag lDataPlusStatusRootTag = getXmlManager().getXmlTree(lDataRootTag, lStatusRootTag, lDataTagsById,
				lDataTagsByRefstatusid);
		// data tags can be created when data and status are merged, e.g., for mails, so
		// populate tag containers again
		// NOTE no multi language within status so language parameter is ""
		populateDataPlusStatusTagsContainersAndHandleMultiLanguage(lDataPlusStatusRootTag, lDataTagsById,
				lDataTagsByRefstatusid, "");
		String lId = "" + aCaseComponent.getCacId() + "," + AppConstants.statusTypeRun;
		getPropXmlDataPlusStatusTagsById().put(lId, lDataTagsById);
		return lDataPlusStatusRootTag;
	}

	/**
	 * P get xml data plus run team status tree, depending on status type, see
	 * AppConstants.
	 *
	 * @param aCaseComponent the a case component
	 * @param aStatusType    the a status type
	 *
	 * @return the xml data plus status tree
	 */
	private IXMLTag pGetXmlDataPlusRunTeamStatusTree(IECaseComponent aCaseComponent, String aStatusType) {
		IECaseRole lCaseRole = getSCaseRoleHelper().getCaseRole();
		if (lCaseRole == null)
			return null;
		// check if casecomponent is available for role
		IECaseComponentRole lCaseComponentRole = getCaseComponentRole(lCaseRole.getCarId(), aCaseComponent.getCacId());
		if (lCaseComponentRole == null)
			return null;
		IXMLTag lDataRootTag = null;
		lDataRootTag = getXmlDataTree(aCaseComponent);
//		make a copy of data so data is not polluted with status
		lDataRootTag = getXmlManager().copyTag(lDataRootTag, null);
		if (lDataRootTag == null)
			return null;
		IXMLTag lStatusRootTag = getXmlRunStatusTree(aCaseComponent, aStatusType);
//		merge data and status
		Hashtable<String, IXMLTag> lDataTagsById = new Hashtable<String, IXMLTag>();
		Hashtable<String, IXMLTag> lDataTagsByRefstatusid = new Hashtable<String, IXMLTag>();
		String lLocaleString = inRun() && aCaseComponent.getECase().getMultilingual() ? getPropLocaleString() : "";
		populateDataPlusStatusTagsContainersAndHandleMultiLanguage(lDataRootTag, lDataTagsById, lDataTagsByRefstatusid,
				lLocaleString);
		IXMLTag lDataPlusStatusRootTag = getXmlManager().getXmlTree(lDataRootTag, lStatusRootTag, lDataTagsById,
				lDataTagsByRefstatusid);
		// data tags can be created when data and status are merged, e.g., for mails, so
		// populate tag containers again
		// NOTE no multi language within status so language parameter is ""
		populateDataPlusStatusTagsContainersAndHandleMultiLanguage(lDataPlusStatusRootTag, lDataTagsById,
				lDataTagsByRefstatusid, "");
		String lId = "" + aCaseComponent.getCacId() + "," + AppConstants.statusTypeRunTeam;
		getPropXmlDataPlusStatusTagsById().put(lId, lDataTagsById);
		return lDataPlusStatusRootTag;
	}

	/**
	 * Gets the current run tag status. The last value saved.
	 *
	 * @param aCaseComponent the a case component
	 * @param aTag           the a tag
	 * @param aStatusKey     the a status key
	 * @param aStatusType    the a status type
	 *
	 * @return the current run tag status
	 */
	public String getCurrentRunTagStatus(IECaseComponent aCaseComponent, IXMLTag aTag, String aStatusKey,
			String aStatusType) {
		if (aTag == null)
			return null;
		return getCurrentRunTagStatus(aCaseComponent, aTag.getAttribute(AppConstants.keyId), aStatusKey, aStatusType);
	}

	/**
	 * Gets the current run tag status time. The time the last value was set.
	 *
	 * @param aCaseComponent the a case component
	 * @param aTag           the a tag
	 * @param aStatusKey     the a status key
	 * @param aStatusType    the a status type
	 *
	 * @return the current run tag status time
	 */
	public String getCurrentRunTagStatusTime(IECaseComponent aCaseComponent, IXMLTag aTag, String aStatusKey,
			String aStatusType) {
		if (aTag == null)
			return null;
		return getCurrentRunTagStatusTime(aCaseComponent, aTag.getAttribute(AppConstants.keyId), aStatusKey,
				aStatusType);
	}

	/**
	 * Gets the current run tag status time. The time the last value was set.
	 *
	 * @param aRugId         the a rug id
	 * @param aCaseComponent the a case component
	 * @param aTag           the a tag
	 * @param aStatusKey     the a status key
	 *
	 * @return the current run tag status time
	 */
	public String getCurrentRunTagStatusTime(int aRugId, IECaseComponent aCaseComponent, IXMLTag aTag,
			String aStatusKey) {
		if (aTag == null)
			return null;
		return getCurrentRunTagStatusTime(aRugId, aCaseComponent, aTag.getAttribute(AppConstants.keyId), aStatusKey);
	}

	/**
	 * Gets the current run tag status. The last value saved.
	 *
	 * @param aCaseComponent the a case component
	 * @param aTagId         the a tag id
	 * @param aStatusKey     the a status key
	 * @param aStatusType    the a status type
	 *
	 * @return the current run tag status
	 */
	public String getCurrentRunTagStatus(IECaseComponent aCaseComponent, String aTagId, String aStatusKey,
			String aStatusType) {
		IXMLTag lDataStatusTag = getDataStatusTag(aCaseComponent, aTagId, aStatusType);
		if (lDataStatusTag == null) {
			return "";
		}
		return lDataStatusTag.getCurrentStatusAttribute(aStatusKey);
	}

	/**
	 * Gets the current run tag status. The last value saved.
	 *
	 * @param aRugId         the a rug id
	 * @param aCaseComponent the a case component
	 * @param aTagId         the a tag id
	 * @param aStatusKey     the a status key
	 *
	 * @return the current run tag status
	 */
	public String getCurrentRunTagStatus(int aRugId, IECaseComponent aCaseComponent, String aTagId, String aStatusKey) {
		IXMLTag lDataStatusTag = getDataStatusTag(aRugId, aCaseComponent, aTagId);
		if (lDataStatusTag == null) {
			return "";
		}
		return lDataStatusTag.getCurrentStatusAttribute(aStatusKey);
	}

	/**
	 * Gets the current run tag status time. The time the last value was set.
	 *
	 * @param aCaseComponent the a case component
	 * @param aTagId         the a tag id
	 * @param aStatusKey     the a status key
	 * @param aStatusType    the a status type
	 *
	 * @return the current run tag status time
	 */
	public String getCurrentRunTagStatusTime(IECaseComponent aCaseComponent, String aTagId, String aStatusKey,
			String aStatusType) {
		IXMLTag lDataStatusTag = getDataStatusTag(aCaseComponent, aTagId, aStatusType);
		if (lDataStatusTag == null) {
			return "";
		}
		return "" + lDataStatusTag.getCurrentStatusAttributeTime(aStatusKey);
	}

	/**
	 * Gets the current run tag status time. The time the last value was set.
	 *
	 * @param aRugId         the a rug id
	 * @param aCaseComponent the a case component
	 * @param aTagId         the a tag id
	 * @param aStatusKey     the a status key
	 *
	 * @return the current run tag status time
	 */
	public String getCurrentRunTagStatusTime(int aRugId, IECaseComponent aCaseComponent, String aTagId,
			String aStatusKey) {
		IXMLTag lDataStatusTag = getDataStatusTag(aRugId, aCaseComponent, aTagId);
		if (lDataStatusTag == null) {
			return "";
		}
		return "" + lDataStatusTag.getCurrentStatusAttributeTime(aStatusKey);
	}

	/**
	 * Gets the current tag status. The last value saved.
	 *
	 * @param aTag       the a tag
	 * @param aStatusKey the a status key
	 *
	 * @return the current tag status
	 */
	public String getCurrentTagStatus(IXMLTag aTag, String aStatusKey) {
		if (aTag == null)
			return null;
		return aTag.getCurrentStatusAttribute(aStatusKey);
	}

	/**
	 * Gets the current tag status time. Time of the last value saved.
	 *
	 * @param aTag       the a tag
	 * @param aStatusKey the a status key
	 *
	 * @return the current tag status time
	 */
	public double getCurrentTagStatusTime(IXMLTag aTag, String aStatusKey) {
		if (aTag == null)
			return -1;
		return aTag.getCurrentStatusAttributeTime(aStatusKey);
	}

	/**
	 * Gets the previous run tag status. The value given by aIndex.
	 *
	 * @param aCaseComponent the a case component
	 * @param aTag           the a tag
	 * @param aStatusKey     the a status key
	 * @param aIndex         the a index
	 * @param aStatusType    the a status type
	 *
	 * @return the previous run tag status
	 */
	public String getPreviousRunTagStatus(IECaseComponent aCaseComponent, IXMLTag aTag, String aStatusKey, int aIndex,
			String aStatusType) {
		if (aTag == null)
			return null;
		return getPreviousRunTagStatus(aCaseComponent, aTag.getAttribute(AppConstants.keyId), aStatusKey, aIndex,
				aStatusType);
	}

	/**
	 * Gets the previous run tag status time. The time the value given by aIndex was
	 * set.
	 *
	 * @param aCaseComponent the a case component
	 * @param aTag           the a tag
	 * @param aStatusKey     the a status key
	 * @param aIndex         the a index
	 * @param aStatusType    the a status type
	 *
	 * @return the previous run tag status time
	 */
	public String getPreviousRunTagStatusTime(IECaseComponent aCaseComponent, IXMLTag aTag, String aStatusKey,
			int aIndex, String aStatusType) {
		if (aTag == null)
			return null;
		return getPreviousRunTagStatusTime(aCaseComponent, aTag.getAttribute(AppConstants.keyId), aStatusKey, aIndex,
				aStatusType);
	}

	/**
	 * Gets the previous run tag status. The value given by aIndex.
	 *
	 * @param aCaseComponent the a case component
	 * @param aTagId         the a tag id
	 * @param aStatusKey     the a status key
	 * @param aIndex         the a index
	 * @param aStatusType    the a status type
	 *
	 * @return the previous run tag status
	 */
	public String getPreviousRunTagStatus(IECaseComponent aCaseComponent, String aTagId, String aStatusKey, int aIndex,
			String aStatusType) {
		IXMLTag lDataStatusTag = getDataStatusTag(aCaseComponent, aTagId, aStatusType);
		if (lDataStatusTag == null) {
			return "";
		}
		return lDataStatusTag.getPreviousStatusAttribute(aStatusKey, aIndex);
	}

	/**
	 * Gets the previous run tag status time. The time the value given by aIndex was
	 * set.
	 *
	 * @param aCaseComponent the a case component
	 * @param aTagId         the a tag id
	 * @param aStatusKey     the a status key
	 * @param aIndex         the a index
	 * @param aStatusType    the a status type
	 *
	 * @return the previous run tag status time
	 */
	public String getPreviousRunTagStatusTime(IECaseComponent aCaseComponent, String aTagId, String aStatusKey,
			int aIndex, String aStatusType) {
		IXMLTag lDataStatusTag = getDataStatusTag(aCaseComponent, aTagId, aStatusType);
		if (lDataStatusTag == null) {
			return "";
		}
		return "" + lDataStatusTag.getPreviousStatusAttributeTime(aStatusKey, aIndex);
	}

	/**
	 * Gets the data status tag.
	 *
	 * @param aCaseComponent the a case component
	 * @param aDataTag       the a data tag
	 * @param aStatusType    the a status type
	 *
	 * @return the data status tag
	 */
	public IXMLTag getDataStatusTag(IECaseComponent aCaseComponent, IXMLTag aDataTag, String aStatusType) {
		if (aDataTag == null)
			return null;
		return getDataStatusTag(aCaseComponent, aDataTag.getAttribute(AppConstants.keyId), aStatusType);
	}

	/**
	 * Gets the data status tag.
	 *
	 * @param aRugId         the a rug id
	 * @param aCaseComponent the a case component
	 * @param aDataTag       the a tag
	 *
	 * @return the data status tag
	 */
	public IXMLTag getDataStatusTag(int aRugId, IECaseComponent aCaseComponent, IXMLTag aDataTag) {
		if (aDataTag == null)
			return null;
		return getDataStatusTag(aRugId, aCaseComponent, aDataTag.getAttribute(AppConstants.keyId));
	}

	/**
	 * Gets the data status tag.
	 *
	 * @param aCaseComponent the a case component
	 * @param aTagId         the a tag id
	 * @param aStatusType    the a status type
	 *
	 * @return the data status tag
	 */
	public IXMLTag getDataStatusTag(IECaseComponent aCaseComponent, String aTagId, String aStatusType) {
		if ((aCaseComponent == null) || (aTagId == null))
			return null;
		IXMLTag lXmlTag = getXmlDataPlusStatusTagById(aCaseComponent, aStatusType, aTagId);
		if (lXmlTag != null) {
			return lXmlTag;
		}
		IXMLTag lRootTag = null;
		lRootTag = getXmlDataPlusRunStatusTree(aCaseComponent, aStatusType);
		if (lRootTag == null)
			return null;
		IXMLTag lContentTag = lRootTag.getChild(AppConstants.contentElement);
		if (lContentTag == null)
			return null;
		return getDataStatusTag(lContentTag, aTagId, aStatusType);
	}

	/**
	 * Gets the data status tag.
	 *
	 * @param aContentTag the a content tag
	 * @param aTagId      the a tag id
	 * @param aStatusType the a status type
	 *
	 * @return the data status tag
	 */
	public IXMLTag getDataStatusTag(IXMLTag aContentTag, String aTagId, String aStatusType) {
		if (aTagId == null)
			return null;
		if (aContentTag == null)
			return null;
		return getXmlManager().getTagById(aContentTag, aTagId);
	}

	/**
	 * Gets the data status tag.
	 *
	 * @param aRugId         the a rug id
	 * @param aCaseComponent the a case component
	 * @param aTagId         the a tag id
	 *
	 * @return the data status tag
	 */
	public IXMLTag getDataStatusTag(int aRugId, IECaseComponent aCaseComponent, String aTagId) {
		if ((aCaseComponent == null) || (aTagId == null))
			return null;
		if (aRugId == getRunGroup().getRugId()) {
			IXMLTag lXmlTag = getXmlDataPlusStatusTagById(aCaseComponent, AppConstants.statusTypeRunGroup, aTagId);
			if (lXmlTag != null) {
				return lXmlTag;
			}
		}
		IXMLTag lRootTag = null;
		lRootTag = getXmlDataPlusRunGroupStatusTree(aRugId, aCaseComponent);
		if (lRootTag == null)
			return null;
		IXMLTag lContentTag = lRootTag.getChild(AppConstants.contentElement);
		if (lContentTag == null)
			return null;
		return getXmlManager().getTagById(lContentTag, aTagId);
	}

	/**
	 * Sets run tag.
	 *
	 * @param aCaseComponent the a case component
	 * @param aRunTag        the a run tag
	 * @param aStatusType    the a status type
	 */
	public void setRunTag(IECaseComponent aCaseComponent, IXMLTag aRunTag, String aStatusType) {
		if (aStatusType.equals(AppConstants.statusTypeRunGroup))
			pSetRunGroupTag(aCaseComponent, aRunTag, aStatusType);
		if (aStatusType.equals(AppConstants.statusTypeRun))
			pSetRunTag(aCaseComponent, aRunTag, aStatusType);
		if (aStatusType.equals(AppConstants.statusTypeRunTeam))
			pSetRunTeamTag(aCaseComponent, aRunTag, aStatusType);
	}

	/**
	 * P set run group tag.
	 *
	 * @param aCaseComponent the a case component
	 * @param aRunGroupTag   the a run group tag
	 * @param aStatusType    the a status type
	 */
	private void pSetRunGroupTag(IECaseComponent aCaseComponent, IXMLTag aRunGroupTag, String aStatusType) {
		if ((aCaseComponent == null) || (aRunGroupTag == null))
			return;
		IXMLTag lStatusRootTag = getXmlRunStatusTree(aCaseComponent, aStatusType);
		getXmlManager().replaceNodeSimple(lStatusRootTag, aRunGroupTag);
		IERunGroup lRunGroup = getRunGroup();
		setSaveRunGroupCaseComponentStatusRootTag(lRunGroup, aCaseComponent, lStatusRootTag);
	}

	/**
	 * P set run tag.
	 *
	 * @param aCaseComponent the a case component
	 * @param aRunTag        the a run tag
	 * @param aStatusType    the a status type
	 */
	private void pSetRunTag(IECaseComponent aCaseComponent, IXMLTag aRunTag, String aStatusType) {
		if ((aCaseComponent == null) || (aRunTag == null))
			return;
		IXMLTag lStatusRootTag = getXmlRunStatusTree(aCaseComponent, aStatusType);
		getXmlManager().replaceNodeSimple(lStatusRootTag, aRunTag);
		IERun lRun = getRun();
		setRunCaseComponentStatusRootTag(lRun, aCaseComponent, lStatusRootTag);
	}

	/**
	 * P set run team tag.
	 *
	 * @param aCaseComponent the a case component
	 * @param aRunTeamTag    the a run team tag
	 * @param aStatusType    the a status type
	 */
	private void pSetRunTeamTag(IECaseComponent aCaseComponent, IXMLTag aRunTeamTag, String aStatusType) {
		if ((aCaseComponent == null) || (aRunTeamTag == null))
			return;
		IXMLTag lStatusRootTag = getXmlRunStatusTree(aCaseComponent, aStatusType);
		getXmlManager().replaceNodeSimple(lStatusRootTag, aRunTeamTag);
		IERunTeam lRunTeam = getRunTeam();
		if (lRunTeam == null)
			return;
		setRunTeamCaseComponentStatusRootTag(lRunTeam, aCaseComponent, lStatusRootTag);
	}

	/**
	 * Sets run tag status. Sets a value of status key aStatusKey within tag aTag
	 * for case component aCaseComponent. Normally these value is added to the
	 * already existing status. For some status keys it is replaced.
	 *
	 * @param aCaseComponent the a case component
	 * @param aTag           the a tag
	 * @param aStatusKey     the a status key
	 * @param aStatusValue   the a status value
	 * @param aCheckScript   the a check script, whether to call script to make it
	 *                       possible to react to the status change
	 * @param aStatusType    the a status type, see IAppManager
	 * @param aSaveInDb      the a save in db, whether status change is saved in
	 *                       database, or only in memory
	 * @param aFromScript    the a from script, whether this method is called from
	 *                       within script, so is a result of a script action
	 *
	 * @return the changed or newly created XML status tag
	 */
	public IXMLTag setRunTagStatus(IECaseComponent aCaseComponent, IXMLTag aTag, String aStatusKey, String aStatusValue,
			boolean aCheckScript, String aStatusType, boolean aSaveInDb, boolean aFromScript) {
		return setRunTagStatus(aCaseComponent, aTag, aStatusKey, aStatusValue, null, aCheckScript, aStatusType,
				aSaveInDb, aFromScript);
	}

	/**
	 * Sets run tag status. Sets a value of status key aStatusKey within tag aTag
	 * for case component aCaseComponent. Normally these value is added to the
	 * already existing status. For some status keys it is replaced.
	 *
	 * @param aCaseComponent        the a case component
	 * @param aTag                  the a tag
	 * @param aStatusKey            the a status key
	 * @param aStatusValue          the a status value
	 * @param aStatustagEnrichtment the a status tag enrichment: attributes, child
	 *                              tags or status child tags of the status tag, for
	 *                              instance when an e-message is sent
	 * @param aCheckScript          the a check script, whether to call script to
	 *                              make it possible to react to the status change
	 * @param aStatusType           the a status type, see IAppManager
	 * @param aSaveInDb             the a save in db, whether status change is saved
	 *                              in database, or only in memory
	 * @param aFromScript           the a from script, whether this method is called
	 *                              from within script, so is a result of a script
	 *                              action
	 *
	 * @return the changed or newly created XML status tag
	 */
	public IXMLTag setRunTagStatus(IECaseComponent aCaseComponent, IXMLTag aTag, String aStatusKey, String aStatusValue,
			SStatustagEnrichment aStatustagEnrichtment, boolean aCheckScript, String aStatusType, boolean aSaveInDb,
			boolean aFromScript) {
		return setRunTagStatus(aCaseComponent, aTag, aStatusKey, aStatusValue, aStatustagEnrichtment, aCheckScript,
				aStatusType, aSaveInDb, aFromScript, true);
	}

	/**
	 * Sets run tag status. Sets a value of status key aStatusKey within tag aTag
	 * for case component aCaseComponent. Normally these value is added to the
	 * already existing status. For some status keys it is replaced.
	 *
	 * @param aCaseComponent the a case component
	 * @param aTag           the a tag
	 * @param aStatusKey     the a status key
	 * @param aStatusValue   the a status value
	 * @param aCheckScript   the a check script, whether to call script to make it
	 *                       possible to react to the status change
	 * @param aStatusType    the a status type, see IAppManager
	 * @param aSaveInDb      the a save in db, whether status change is saved in
	 *                       database, or only in memory
	 * @param aFromScript    the a from script, whether this method is called from
	 *                       within script, so is a result of a script action
	 * @param aNotifyPlayer  the a notify player, whether the player should be
	 *                       notified of the tag status change
	 *
	 * @return the changed or newly created XML status tag
	 */
	public IXMLTag setRunTagStatus(IECaseComponent aCaseComponent, IXMLTag aTag, String aStatusKey, String aStatusValue,
			boolean aCheckScript, String aStatusType, boolean aSaveInDb, boolean aFromScript, boolean aNotifyPlayer) {
		return setRunTagStatus(aCaseComponent, aTag, aStatusKey, aStatusValue, null, aCheckScript, aStatusType,
				aSaveInDb, aFromScript, aNotifyPlayer);
	}

	/**
	 * Sets run tag status. Sets a value of status key aStatusKey within tag aTag
	 * for case component aCaseComponent. Normally these value is added to the
	 * already existing status. For some status keys it is replaced.
	 *
	 * @param aCaseComponent        the a case component
	 * @param aTag                  the a tag
	 * @param aStatusKey            the a status key
	 * @param aStatusValue          the a status value
	 * @param aStatustagEnrichtment the a status tag enrichment: attributes, child
	 *                              tags or status child tags of the status tag, for
	 *                              instance when an e-message is sent
	 * @param aCheckScript          the a check script, whether to call script to
	 *                              make it possible to react to the status change
	 * @param aStatusType           the a status type, see IAppManager
	 * @param aSaveInDb             the a save in db, whether status change is saved
	 *                              in database, or only in memory
	 * @param aFromScript           the a from script, whether this method is called
	 *                              from within script, so is a result of a script
	 *                              action
	 * @param aNotifyPlayer         the a notify player, whether the player should
	 *                              be notified of the tag status change
	 *
	 * @return the changed or newly created XML status tag
	 */
	public IXMLTag setRunTagStatus(IECaseComponent aCaseComponent, IXMLTag aTag, String aStatusKey, String aStatusValue,
			SStatustagEnrichment aStatustagEnrichtment, boolean aCheckScript, String aStatusType, boolean aSaveInDb,
			boolean aFromScript, boolean aNotifyPlayer) {
		// NOTE sometimes aTag is a non node tag, so exclude it
		if (aCaseComponent == null || aTag == null
				|| !aTag.getAttribute(AppConstants.defKeyType).equals(AppConstants.defValueNode))
			return null;
		IXMLTag lDataTag = getCaseComponentXmlDataTagById(aCaseComponent, aTag.getAttribute(AppConstants.keyId));
		// lDataTag can be null if more status tags for one data tag in case of
		// e-messages.
		// Then these status tags are added as extra datatags, so they get an id higher
		// than the max id within the original datatags. So it will not be found.
		// if aTag has extended true also don't get data tag by ref data id, but use
		// status tag
		if (lDataTag == null && !aTag.getAttribute(AppConstants.keyExtended).equals(AppConstants.statusValueTrue)) {
			// NOTE code below is not used for mails, but maybe for user generated content
			// so keep it
			lDataTag = getCaseComponentXmlDataTagById(aCaseComponent, aTag.getAttribute(AppConstants.keyRefdataid));
		}
		if (lDataTag == null) {
			lDataTag = aTag;
		}
		return setRunTagStatus(aCaseComponent, "", lDataTag.getName(), aStatusKey, aStatusValue, aStatustagEnrichtment,
				aCheckScript, lDataTag, aStatusType, aSaveInDb, aFromScript, aNotifyPlayer);
	}

	/**
	 * <p>
	 * Sets run tag status. Sets a value of status key aStatusKey within tag given
	 * by aTag or aTagId for case component aCaseComponent. Normally these value is
	 * added to the already existing status. For some status keys it is replaced.
	 * </p>
	 *
	 * <p>
	 * The status change is logged and saved depending on aSaveInDb. The player is
	 * notified of the status change. And script called depending on aCheckScript.
	 * </p>
	 *
	 * @param aCaseComponent        the a case component
	 * @param aTagId                the a tag id, is used as aTag is null
	 * @param aTagName              the a tag name
	 * @param aStatusKey            the a status key
	 * @param aStatusValue          the a status value
	 * @param aStatustagEnrichtment the a status tag enrichment: attributes, child
	 *                              tags or status child tags of the status tag, for
	 *                              instance when an e-message is sent
	 * @param aCheckScript          the a check script, whether to call script to
	 *                              make it possible to react to the status change
	 * @param aTag                  the a tag, if null aTagId is used to get the tag
	 * @param aStatusType           the a status type, see IAppManager
	 * @param aSaveInDb             the a save in db, whether status change is saved
	 *                              in database, or only in memory
	 * @param aFromScript           the a from script, whether this method is called
	 *                              from within script, so is a result of a script
	 *                              action
	 *
	 * @return the changed or newly created XML status tag
	 */
	public IXMLTag setRunTagStatus(IECaseComponent aCaseComponent, String aTagId, String aTagName, String aStatusKey,
			String aStatusValue, SStatustagEnrichment aStatustagEnrichtment, boolean aCheckScript, IXMLTag aTag,
			String aStatusType, boolean aSaveInDb, boolean aFromScript) {
		return setRunTagStatus(aCaseComponent, aTagId, aTagName, aStatusKey, aStatusValue, aStatustagEnrichtment,
				aCheckScript, aTag, aStatusType, aSaveInDb, aFromScript, true);
	}

	/**
	 * <p>
	 * Sets run tag status. Sets a value of status key aStatusKey within tag given
	 * by aTag or aTagId for case component aCaseComponent. Normally these value is
	 * added to the already existing status. For some status keys it is replaced.
	 * </p>
	 *
	 * <p>
	 * The status change is logged and saved depending on aSaveInDb. The player is
	 * notified of the status change. And script called depending on aCheckScript.
	 * </p>
	 *
	 * @param aCaseComponent        the a case component
	 * @param aTagId                the a tag id, is used as aTag is null
	 * @param aTagName              the a tag name
	 * @param aStatusKey            the a status key
	 * @param aStatusValue          the a status value
	 * @param aStatustagEnrichtment the a status tag enrichment: attributes, child
	 *                              tags or status child tags of the status tag, for
	 *                              instance when an e-message is sent
	 * @param aCheckScript          the a check script, whether to call script to
	 *                              make it possible to react to the status change
	 * @param aTag                  the a tag, if null aTagId is used to get the tag
	 * @param aStatusType           the a status type, see IAppManager
	 * @param aSaveInDb             the a save in db, whether status change is saved
	 *                              in database, or only in memory
	 * @param aFromScript           the a from script, whether this method is called
	 *                              from within script, so is a result of a script
	 *                              action
	 * @param aNotifyPlayer         the a notify player, whether the player should
	 *                              be notified of the tag status change
	 *
	 * @return the changed or newly created XML status tag
	 */
	public IXMLTag setRunTagStatus(IECaseComponent aCaseComponent, String aTagId, String aTagName, String aStatusKey,
			String aStatusValue, SStatustagEnrichment aStatustagEnrichtment, boolean aCheckScript, IXMLTag aTag,
			String aStatusType, boolean aSaveInDb, boolean aFromScript, boolean aNotifyPlayer) {
		// NOTE sometimes aTag is a non node tag, so exclude it
		if (aTag != null && !aTag.getAttribute(AppConstants.defKeyType).equals(AppConstants.defValueNode))
			return null;
		if (aStatusKey == null || aStatusKey.equals(""))
			return null;
		if (aCaseComponent == null || aTagId == null)
			return null;
		IXMLTag lStatusRootTag = getXmlRunStatusTree(aCaseComponent, aStatusType);
		if (lStatusRootTag == null)
			return null;
		IXMLTag lStatusContentTag = lStatusRootTag.getChild(AppConstants.contentElement);
		if (lStatusContentTag == null)
			return null;
		// Determine for which datatag status is set. Data tag will be null for user
		// generated content, for instance a source added
		// by a student within the Emergo player.
		// For e-messages data tag is a template to be used for an e-message. More
		// e-messages using the same template can be sent.
		// For most data tags there will be only one corresponding status tag at most.
		IXMLTag lDataTag = null;
		if (aTag == null && !aTagId.equals("")) {
			lDataTag = getCaseComponentXmlDataTagById(aCaseComponent, aTagId);
		} else {
			lDataTag = aTag;
		}

		// if status key has possible values true or false it can be that only
		// one out of all sibling tags may have status key value to true,
		// so status keys of siblings have to be set to false
		handleSingleStatusValueTrue(aCaseComponent, lDataTag, lStatusRootTag, aStatusKey, aStatusValue, aStatusType);

		// if status key is equal to sent a new statustag has to be added always.
		// otherwise an existing statustag is changed.
		// if sent there can be multiple instances of a statustag, like for e-messages
		// or alerts.
		// a version number is added to distinguish the multiple instances.
		boolean lTagSent = ((aStatusKey.equals(AppConstants.statusKeySent))
				&& (aStatusValue.equals(AppConstants.statusValueTrue)));
		// Determine if status key should be saved within a rga child tag of the status
		// tag.
		// This can be true if status is shared between different users.
		boolean lStatusInRgaTag = getAppManager().statusInRgaTag(aStatusKey, aStatusType);

		// NOTE adding childs to status tag of status tag is handled by called methods
		List<IXMLTag> lStatusStatusChilds = null;
		if (aStatustagEnrichtment != null) {
			lStatusStatusChilds = aStatustagEnrichtment.getStatusChildTags();
		}
		IXMLTag lStatusTag = null;
		if (lTagSent) {
			// always add new statustag
			// for e-messages or alerts
			lStatusTag = addNewSentStatusTag(aCaseComponent, lStatusContentTag, lStatusRootTag, lDataTag, aTagId,
					aTagName, aStatusKey, aStatusValue, lStatusStatusChilds, aStatusType);
			// TODO create new data tag out of status tag so for instance mail tree is
			// rendered correctly
			IXMLTag lTempDataTag = getXmlManager().getNewSentDataTag(lStatusTag, lDataTag);
			if (lTempDataTag != null) {
				lDataTag = lTempDataTag;
				setXmlDataPlusStatusTagById(aCaseComponent, lDataTag, aStatusType);
			}
		}
		if (lStatusInRgaTag) {
			// add rga child to status if not existing for current rga otherwise add
			// statuschild of rga child
			// for rating like in limburgian canon
			lStatusTag = addNewOrChangeExistingRgaStatusTag(aCaseComponent, lStatusContentTag, lStatusRootTag, lDataTag,
					aTagId, lStatusStatusChilds, aStatusType);
		}
		if (!(lTagSent || lStatusInRgaTag)) {
			// if not exist add new statustag else change existing statustag
			// for author or user generated content
			lStatusTag = addNewOrChangeExistingStatusTag(aCaseComponent, lStatusContentTag, lStatusRootTag, lDataTag,
					aTagId, aStatusKey, aStatusValue, lStatusStatusChilds, aFromScript, aStatusType);
		}

		// NOTE possibly enrich statustag with attributes and child tags
		if (lStatusTag != null && aStatustagEnrichtment != null) {
			if (aStatustagEnrichtment.getAttributes() != null) {
				for (Enumeration<String> lKeys = aStatustagEnrichtment.getAttributes().keys(); lKeys
						.hasMoreElements();) {
					String lKey = lKeys.nextElement();
					lStatusTag.setAttribute(lKey, aStatustagEnrichtment.getAttributes().get(lKey));
				}
			}
			if (aStatustagEnrichtment.getChildTags() != null) {
				for (IXMLTag lChildTag : aStatustagEnrichtment.getChildTags()) {
					lStatusTag.getChildTags().add(lChildTag);
				}
			}
		}

		// log the status change
		String lTagPid = "";
		String lTagName = "";
		if (lDataTag != null) {
			lTagPid = getXmlManager().unescapeXML(
					getXmlManager().getTagKeyValues(lDataTag, lDataTag.getDefAttribute(AppConstants.defKeyKey)));
			lTagName = lDataTag.getName();
		}
		getSLogHelper().logSetRunTagAction("setruntagstatus", aStatusType,
				getXmlManager().unescapeXML(aCaseComponent.getName()), lTagName, lTagPid, aStatusKey, aStatusValue,
				aCheckScript);

		// save status change depending on aSaveInDb
		// notify player of status change
		// check script depending on aCheckScript
		if (aStatusType.equals(AppConstants.statusTypeRunGroup)) {
			if (aSaveInDb) {
				IERunGroup lRunGroup = getRunGroup();
				if (lRunGroup != null)
					setSaveRunGroupCaseComponentStatusRootTag(lRunGroup, aCaseComponent, lStatusRootTag);
			}
			// notify player of status change
			if (aNotifyPlayer)
				runGroupTagChangeToPlayer(aCaseComponent, lDataTag, lStatusTag, aStatusKey, aStatusValue, aFromScript);
			if (lDataTag != null) {
				if (lDataTag.getAttribute(AppConstants.keyExtended).equals(AppConstants.statusValueTrue)) {
					// NOTE a datatag is extended if it for instance is a sent mail or alert tag or
					// if is user generated content.
					// In case of a mail or alert refdataid is set. For user generated content not,
					// so lDataTag will become null, which is no problem because user generated
					// content can not be checked by script.
					lDataTag = getCaseComponentXmlDataTagById(aCaseComponent,
							lDataTag.getAttribute(AppConstants.keyRefdataid));
				}
			}
			if (lDataTag != null) {
				if (aCheckScript) {
					STriggeredReference lTriggeredReference = new STriggeredReference();
					lTriggeredReference.setCaseComponent(aCaseComponent);
					lTriggeredReference.setDataTag(lDataTag);
					lTriggeredReference.setStatusKey(aStatusKey);
					lTriggeredReference.setStatusValue(aStatusValue);
					getSScriptHelper().checkRunGroupTagScript(lTriggeredReference, aStatusType, aSaveInDb);
				}
			}
		}
		if (aStatusType.equals(AppConstants.statusTypeRun)) {
			if (aSaveInDb) {
				IERun lRun = getRun();
				if (lRun != null)
					setRunCaseComponentStatusRootTag(lRun, aCaseComponent, lStatusRootTag);
			}
			// notify player of status change
			if (aNotifyPlayer)
				runTagChangeToPlayer(aCaseComponent, lDataTag, lStatusTag, aStatusKey, aStatusValue);
			if (aCheckScript)
				;
			// checking script for run status still has to be implemented.
			// Problem is there cannot be script defined for run status change, only for
			// data tag status change.
			// It has to be possible to add script on run status change, for instance 'if a
			// run status tag is added'.
			// checkRunGroupTagScript(aCaseComponent, lDataTag, aStatusKey, aStatusType,
			// aSaveInDb);
		}
		if (aStatusType.equals(AppConstants.statusTypeRunTeam)) {
			if (aSaveInDb) {
				IERunTeam lRunTeam = getRunTeam();
				if (lRunTeam != null)
					setRunTeamCaseComponentStatusRootTag(lRunTeam, aCaseComponent, lStatusRootTag);
			}
			// notify player of status change
			if (aNotifyPlayer)
				runTeamTagChangeToPlayer(aCaseComponent, lDataTag, lStatusTag, aStatusKey, aStatusValue);
			if (aCheckScript)
				;
			// checking script for run team status still has to be implemented.
			// Problem is there cannot be script defined for run team status change, only
			// for datatag status change.
			// It has to be possible to add script on run team status change, for instance
			// 'if a run team status tag is added'.
			// checkRunGroupTagScript(aCaseComponent, lDataTag, aStatusKey, aStatusType,
			// aSaveInDb);
		}

		boolean lStateValueSet = aCaseComponent.getEComponent().getCode().equals("states") && lDataTag != null
				&& (lDataTag.getName().equals("state") || lDataTag.getName().equals("formula"))
				&& aStatusKey.equals(AppConstants.statusKeyValue);
		if (lStateValueSet) {
			handleStatesFormulas(aCaseComponent, lDataTag, aCheckScript, aStatusType, aSaveInDb, aFromScript, false);
		}

		return lStatusTag;
	}

	/**
	 * Filter on version tags. Remove tags in aStatusTags which have version equal
	 * to empty string.
	 *
	 * @param aStatusTags the status tags
	 */
	private void filterOnVersionTags(List<IXMLTag> aStatusTags) {
		for (int i = (aStatusTags.size() - 1); i >= 0; i--) {
			IXMLTag lStatusTag = aStatusTags.get(i);
			// version can be attribute of status child
			String lVersion = lStatusTag.getChildAttribute(AppConstants.statusElement, AppConstants.statusKeyVersion);
			if (lVersion.equals(""))
				// or of tag it self
				lVersion = lStatusTag.getAttribute(AppConstants.statusKeyVersion);
			if (lVersion.equals(""))
				aStatusTags.remove(i);
		}
	}

	/**
	 * Adds new sent status tag. Is used to add e-message or alert status tags.
	 * There can be more status tags referencing to one data tag, so a version
	 * attribute is added to distinguish them.
	 *
	 * @param aCaseComponent    the a case component
	 * @param aStatusContentTag the a status content tag
	 * @param aStatusRootTag    the a status root tag
	 * @param aDataTag          the a data tag
	 * @param aTagId            the a tag id of the data tag
	 * @param aTagName          the a tag name of the new tag
	 * @param aStatusKey        the a status key
	 * @param aStatusValue      the a status value
	 * @param aStatusChilds     the a status childs
	 * @param aStatusType       the a status type, see IAppManager
	 *
	 * @return the new status tag
	 */
	private IXMLTag addNewSentStatusTag(IECaseComponent aCaseComponent, IXMLTag aStatusContentTag,
			IXMLTag aStatusRootTag, IXMLTag aDataTag, String aTagId, String aTagName, String aStatusKey,
			String aStatusValue, List<IXMLTag> aStatusChilds, String aStatusType) {
		if (aDataTag == null)
			// new user generated tag so no datatag reference
			return addNewUserGeneratedTag(aCaseComponent, aStatusRootTag, aTagName, aStatusKey, aStatusValue,
					aStatusChilds);
		// refstatusid attribute is only set if statustag exists before rendering tree
		String lStatusTagId = aDataTag.getAttribute(AppConstants.keyRefstatusid);
		IXMLTag lStatusTag = null;
		int lVersion = 0;
		if (lStatusTagId.equals("")) {
			// if data tag has no reference to status tag, check if there are status tags
			// referencing to the data tag
			// this will be the case for e-messages and alerts
			List<IXMLTag> lStatusTags = getXmlManager().getTagsByRefdataid(aStatusContentTag, aTagId);
			// only get the ones with version attribute set
			filterOnVersionTags(lStatusTags);
			// last version
			lVersion = lStatusTags.size();
		} else {
			// get status tag
			lStatusTag = getXmlStatusTagById(aCaseComponent, lStatusTagId, aStatusType);
			if (lStatusTag != null)
				// if there happens to be a status tag set last version to 1
				// probably will never happen.
				lVersion = 1;
		}
		// create new status tag
		lStatusTag = getNewSentStatusTag(aStatusRootTag, aDataTag.getAttribute(AppConstants.keyId), aDataTag.getName(),
				aStatusKey, aStatusValue, aStatusChilds, lVersion + 1);
		// add specific status childs
		addSentStatusChilds(aCaseComponent, aDataTag, lStatusTag);
		getXmlManager().newChildNodeSimple(aStatusRootTag, lStatusTag, null);
		if (lStatusTag != null) {
			String lIds = aDataTag.getAttribute(AppConstants.keyRefstatusids);
			if (!lIds.equals(""))
				lIds = lIds + ",";
			lIds = lIds + lStatusTag.getAttribute(AppConstants.keyId);
			aDataTag.setAttribute(AppConstants.keyRefstatusids, lIds);
			aDataTag.addStatusTag(lStatusTag);
			setXmlStatusTagById(aCaseComponent, lStatusTag, aStatusType);
		}
		return lStatusTag;
	}

	/**
	 * Adds new user generated status tag. Is used for user generated tags. There
	 * can be more status tags, so a version attribute is added to distinguish them.
	 *
	 * @param aCaseComponent the a case component
	 * @param aStatusRootTag the a status root tag
	 * @param aTagName       the a tag name of the new tag
	 * @param aStatusKey     the a status key
	 * @param aStatusValue   the a status value
	 * @param aStatusChilds  the a status childs
	 *
	 * @return the new status tag
	 */
	private IXMLTag addNewUserGeneratedTag(IECaseComponent aCaseComponent, IXMLTag aStatusRootTag, String aTagName,
			String aStatusKey, String aStatusValue, List<IXMLTag> aStatusChilds) {
		int lVersion = 0;
		// create new status tag
		IXMLTag lStatusTag = getNewSentStatusTag(aStatusRootTag, "", aTagName, aStatusKey, aStatusValue, aStatusChilds,
				lVersion + 1);
		getXmlManager().newChildNodeSimple(aStatusRootTag, lStatusTag, null);
		return lStatusTag;
	}

	/**
	 * Gets new sent status tag. Is used to get e-message, attachment or alert
	 * status tags. There can be more status tags referencing to one data tag, so a
	 * version attribute is added to distinguish them.
	 *
	 * @param aStatusRootTag the a status root tag
	 * @param aTagId         the a tag id of the data tag
	 * @param aTagName       the a tag name of the new tag
	 * @param aStatusKey     the a status key
	 * @param aStatusValue   the a status value
	 * @param aStatusChilds  the a status childs
	 * @param aVersion       the a version
	 *
	 * @return the new status tag
	 */
	private IXMLTag getNewSentStatusTag(IXMLTag aStatusRootTag, String aTagId, String aTagName, String aStatusKey,
			String aStatusValue, List<IXMLTag> aStatusChilds, int aVersion) {
		// create new status tag
		IXMLTag lStatusTag = newRunNodeTag(aTagName, aTagId, aStatusKey, aStatusValue);
		IXMLTag lStatusStatusTag = lStatusTag.getChild(AppConstants.statusElement);
		if (lStatusStatusTag != null) {
			// always add version
			lStatusStatusTag.setAttribute(AppConstants.statusKeyVersion, "" + (aVersion + 1));
			// always add creation date time
			lStatusStatusTag.setAttribute(AppConstants.statusKeyDatetime, "" + System.currentTimeMillis());
		}
		if (aStatusChilds != null && lStatusStatusTag != null) {
			// add status childs
			for (IXMLTag lStatusChild : aStatusChilds) {
				lStatusChild.setParentTag(lStatusStatusTag);
				lStatusStatusTag.getChildTags().add(lStatusChild);
			}
		}
		return lStatusTag;
	}

	/**
	 * Adds new or change existing rga status tag.
	 *
	 * @param aCaseComponent    the a case component
	 * @param aStatusContentTag the a status content tag
	 * @param aStatusRootTag    the a status root tag
	 * @param aDataTag          the a data tag
	 * @param aTagId            the a tag id of the data tag
	 * @param aStatusChilds     the a status childs
	 * @param aStatusType       the a status type, see IAppManager
	 *
	 * @return the new or changed status tag
	 */
	private IXMLTag addNewOrChangeExistingRgaStatusTag(IECaseComponent aCaseComponent, IXMLTag aStatusContentTag,
			IXMLTag aStatusRootTag, IXMLTag aDataTag, String aTagId, List<IXMLTag> aStatusChilds, String aStatusType) {
		// refstatusid attribute is only set if statustag exists before rendering tree
		String lStatusTagId = aDataTag.getAttribute(AppConstants.keyRefstatusid);
		IXMLTag lStatusTag = null;
		if (lStatusTagId.equals(""))
			// if data tag has no reference to status tag, check if there is a status tag
			// referencing to the data tag
			lStatusTag = getXmlManager().getTagByRefdataid(aStatusContentTag, aTagId);
		else {
			// get status tag
			lStatusTag = getXmlStatusTagById(aCaseComponent, lStatusTagId, aStatusType);
		}
		boolean lNew = (lStatusTag == null);
		if (lNew) {
//			create status tag without refdataid (""), because shared content does not have reference to datatag like e-messages tags have.
			lStatusTag = newRunNodeTag(aDataTag.getName(), "", null, null);
		}
		addNewOrChangeExistingRgaTagToStatusTag(lStatusTag, aStatusChilds);
		if (lNew) {
			getXmlManager().newChildNodeSimple(aStatusRootTag, lStatusTag, null);
			if (lStatusTag != null) {
				aDataTag.setAttribute(AppConstants.keyRefstatusid, lStatusTag.getAttribute(AppConstants.keyId));
				aDataTag.addStatusTag(lStatusTag);
				setXmlStatusTagById(aCaseComponent, lStatusTag, aStatusType);
			}
		} else
			getXmlManager().replaceNodeSimple(aStatusRootTag, lStatusTag);
		return lStatusTag;
	}

	/**
	 * Adds new or change existing rga tag to status tag. Rga tag is added as a
	 * child of status tag. Status changes per rga are added as childs of the rga
	 * tag.
	 *
	 * @param aStatusTag    the a status tag
	 * @param aStatusChilds the a status childs
	 */
	public void addNewOrChangeExistingRgaTagToStatusTag(IXMLTag aStatusTag, List<IXMLTag> aStatusChilds) {
		IXMLTag lStatusStatusTag = aStatusTag.getChild(AppConstants.statusElement);
		if ((aStatusChilds != null) && (lStatusStatusTag != null)) {
			// add or change status childs
			List<IXMLTag> lStatusStatusChildTags = lStatusStatusTag.getChildTags();
			for (IXMLTag lStatusChild : aStatusChilds) {
				if (lStatusChild.getName().equals("rga")) {
					// if rga child
					IXMLTag lFoundTag = null;
					// check if rga child exists
					for (IXMLTag lStatusStatusChildTag : lStatusStatusChildTags) {
						if ((lStatusStatusChildTag.getName().equals("rga"))
								&& (lStatusStatusChildTag.getValue().equals(lStatusChild.getValue())))
							lFoundTag = lStatusStatusChildTag;
					}
					if (lFoundTag == null) {
						// if rga child does not exist, add status child
						lStatusChild.setParentTag(lStatusStatusTag);
						lStatusStatusTag.getChildTags().add(lStatusChild);
					} else {
						// otherwise
						Hashtable<String, List<IXMLAttributeValueTime>> lAttributes = lStatusChild.getAttributes();
						// add attributes of status child to rga child
						for (Enumeration<String> lKeys = lAttributes.keys(); lKeys.hasMoreElements();) {
							String lKey = lKeys.nextElement();
							lFoundTag.setAttributeAsList(lKey, lAttributes.get(lKey));
						}
						// add child tags of status child to rga child
						for (IXMLTag lTag3 : lStatusChild.getChildTags()) {
							boolean lFound = false;
							for (IXMLTag lTag4 : lFoundTag.getChildTags()) {
								if ((lTag4.getName().equals(lTag3.getName()))
										&& (lTag4.getAttribute(AppConstants.statusKeyTime)
												.equals(lTag3.getAttribute(AppConstants.statusKeyTime)))
										&& (lTag4.getValue().equals(lTag3.getValue())))
									// skip identical tags!!
									// time is added to distinguish different status changes for a rga tag.
									// every status change is saved as a child, so not as attribute!
									lFound = true;
							}
							if (!lFound)
								// if child tag does not exist add it
								lFoundTag.getChildTags().add(lTag3);
						}
					}
				}
			}
		}
	}

	/**
	 * Adds new or change existing status tag. This reflects more or less the normal
	 * situation. The status of a data tag defined by an author is changed. There
	 * will be no or one status tag per data tag and no childs to be add to the
	 * status tag. Only status attributes are changed. N.B. The status of a user
	 * generated status tag also can be set, then there will be no reference to a
	 * data tag present.
	 *
	 * @param aCaseComponent    the a case component
	 * @param aStatusContentTag the a status content tag
	 * @param aStatusRootTag    the a status root tag
	 * @param aDataTag          the a data tag
	 * @param aTagId            the a tag id of the data tag
	 * @param aStatusKey        the a status key
	 * @param aStatusValue      the a status value
	 * @param aStatusChilds     the a status childs
	 * @param aFromScript       the a from script
	 * @param aStatusType       the a status type, see IAppManager
	 *
	 * @return the new or changed status tag
	 */
	private IXMLTag addNewOrChangeExistingStatusTag(IECaseComponent aCaseComponent, IXMLTag aStatusContentTag,
			IXMLTag aStatusRootTag, IXMLTag aDataTag, String aTagId, String aStatusKey, String aStatusValue,
			List<IXMLTag> aStatusChilds, boolean aFromScript, String aStatusType) {
		if (aDataTag == null)
			return null;
		// refstatusid attribute is only set if statustag exists before rendering tree
		String lStatusTagId = aDataTag.getAttribute(AppConstants.keyRefstatusid);
		IXMLTag lStatusTag = null;
		if (lStatusTagId.equals("")) {
			if (aFromScript) {
				// if called from script the status of a data tag has to be set.
				// but for e-messages and alerts refstatusid will be empty because there can be
				// more status tags
				// referencing to one data tag.
				// if there are more status tags with refdataid aTagId get the one without
				// version set
				// because this one has to be set if key is set from script
				List<IXMLTag> lStatusTags = getXmlManager().getTagsByRefdataid(aStatusContentTag, aTagId);
				for (IXMLTag lTempStatusTag : lStatusTags) {
					if (lTempStatusTag.getChildAttribute(AppConstants.statusElement, AppConstants.statusKeyVersion)
							.equals(""))
						// searched tag does not have version key set
						lStatusTag = lTempStatusTag;
				}
			} else
				// get status tag by aTagId, it is a user generated status tag
				lStatusTag = getXmlManager().getTagByRefdataid(aStatusContentTag, aTagId);
		} else {
			// get status tag, it is a status tag referencing to an author generated data
			// tag
			lStatusTag = getXmlStatusTagById(aCaseComponent, lStatusTagId, aStatusType);
		}
		boolean lIsNewStatusTag = false;
		if (lStatusTag == null) {
			// NOTE only create new status tag if data tag is not user generated
			if (!aDataTag.getAttribute(AppConstants.keyExtended).equals(AppConstants.statusValueTrue)) {
				// create new status tag
				lStatusTag = newRunNodeTag(aDataTag.getName(), aDataTag.getAttribute(AppConstants.keyId), aStatusKey,
						aStatusValue);
				getXmlManager().newChildNodeSimple(aStatusRootTag, lStatusTag, null);
				if (lStatusTag != null) {
					aDataTag.setAttribute(AppConstants.keyRefstatusid, lStatusTag.getAttribute(AppConstants.keyId));
					aDataTag.addStatusTag(lStatusTag);
					setXmlStatusTagById(aCaseComponent, lStatusTag, aStatusType);
					lIsNewStatusTag = true;
				}
			} else {
				// get status tag of user generated data tag
				lStatusTag = aDataTag.getStatusTag();
			}
		}
		if (!lIsNewStatusTag && lStatusTag != null) {
			replaceRunTagStatusValue(lStatusTag, aStatusKey, aStatusValue);
			getXmlManager().replaceNodeSimple(aStatusRootTag, lStatusTag);
		}
		if (aStatusChilds != null && lStatusTag != null) {
			IXMLTag lStatusStatusTag = lStatusTag.getChild(AppConstants.statusElement);
			if (lStatusStatusTag != null) {
				// NOTE child tags are always added so it is the responsability of the calling
				// method to check if childs already exist or not
				// add status childs
				for (IXMLTag lStatusChild : aStatusChilds) {
					lStatusChild.setParentTag(lStatusStatusTag);
					lStatusStatusTag.getChildTags().add(lStatusChild);
				}
			}
		}
		return lStatusTag;
	}

	/**
	 * Handles single status value true. There are status keys for which in a number
	 * of sibling tags there can be only one with status value true. If status value
	 * of one sibling is set to true, status value of the sibling which had status
	 * value true is set to false.
	 *
	 * @param aCaseComponent the a case component
	 * @param aDataTag       the a data tag
	 * @param aStatusRootTag the a status root tag
	 * @param aStatusKey     the a status key
	 * @param aStatusValue   the a status value
	 * @param aStatusType    the a status type, see IAppManager
	 */
	private void handleSingleStatusValueTrue(IECaseComponent aCaseComponent, IXMLTag aDataTag, IXMLTag aStatusRootTag,
			String aStatusKey, String aStatusValue, String aStatusType) {
		if (aDataTag == null)
			return;
		IXMLTag lDataParentTag = aDataTag.getParentTag();
		if (lDataParentTag == null)
			return;
		// check if aStatusValue is true
		if (!aStatusValue.equals(AppConstants.statusValueTrue))
			// if not do nothing
			return;
		// get single status value attribute name
		String lSingleAttr = getAppManager().getSingleStatusValueTrue(aStatusKey);
		if (lSingleAttr.equals(""))
			// sibling tags can have status key value true too
			return;
		// check if lTag has singleattr true
		// if single status attribute value in definition for this kind of tag is not
		// true, no need to check siblings
		boolean lSingle = aDataTag.getDefAttribute(lSingleAttr).equals(AppConstants.statusValueTrue);
		if (!lSingle)
			return;
		// check if sibling of lTag has aStatusKey true
		List<IXMLTag> lDataChildTags = lDataParentTag.getChildTags();
		for (IXMLTag lDataChildTag : lDataChildTags) {
			// same tag name as aDataTag ...
			boolean lCandidate = (lDataChildTag.getName().equals(aDataTag.getName()));
			// ... but not same tag id as aDataTag
			lCandidate = (lCandidate && (!lDataChildTag.getAttribute(AppConstants.keyId)
					.equals(aDataTag.getAttribute(AppConstants.keyId))));
			if (lCandidate) {
				// check if childtag has statustag
				String lId = lDataChildTag.getAttribute(AppConstants.keyId);
				IXMLTag lStatusTag = getXmlManager()
						.getTagByRefdataid(aStatusRootTag.getChild(AppConstants.contentElement), lId);
				if (lStatusTag == null) {
					// if not add it
					String lDataStatusValue = lDataChildTag.getChildAttribute(AppConstants.statusElement, aStatusKey);
					if (lDataStatusValue.equals(AppConstants.statusValueTrue)) {
						// but only add it if default author value is true
						// NOTE only create new status tag if data tag is not user generated
						if (!lDataChildTag.getAttribute(AppConstants.keyExtended)
								.equals(AppConstants.statusValueTrue)) {
							lStatusTag = newRunNodeTag(aDataTag.getName(), lId, aStatusKey,
									AppConstants.statusValueFalse);
							if (lStatusTag != null) {
								aDataTag.setAttribute(AppConstants.keyRefstatusid,
										lStatusTag.getAttribute(AppConstants.keyId));
								lDataChildTag.addStatusTag(lStatusTag);
								setXmlStatusTagById(aCaseComponent, lStatusTag, aStatusType);
							}
							getXmlManager().newChildNodeSimple(aStatusRootTag, lStatusTag, null);
						}
					}
				} else {
					// check if current status value is true
					String lStatusValue = lStatusTag.getCurrentStatusAttribute(aStatusKey);
					if (!lStatusValue.equals(AppConstants.statusValueFalse)) {
						// if so change it to false
						replaceRunTagStatusValue(lStatusTag, aStatusKey, AppConstants.statusValueFalse);
						getXmlManager().replaceNodeSimple(aStatusRootTag, lStatusTag);
					}
				}
			}
		}
	}

	/**
	 * Inits states components formula's containing states or formulas.
	 */
	public void initStatesFormulas() {
		List<IECaseComponent> lCaseComponents = getCaseComponentsByComponentCode(getCase(), "states");
		for (IECaseComponent lCaseComponent : lCaseComponents) {
			IXMLTag lRootTag = getXmlDataTree(lCaseComponent);
			List<IXMLTag> lNodeTags = getPropCScript().getNodeTags(lRootTag);
			for (IXMLTag lNodeTag : lNodeTags) {
				if (lNodeTag.getName().equals("state")) {
					handleStatesFormulas(lCaseComponent, lNodeTag, false, AppConstants.statusTypeRunGroup, true, false,
							true);
				}
			}
		}
	}

	/**
	 * Get formula value.
	 *
	 * @param aFormulaTag the a formula tag
	 *
	 * @return the value
	 */
	public String getFormulaValue(IXMLTag aFormulaTag) {
		String lValue = "";
		if (aFormulaTag == null) {
			return lValue;
		}
		// determine value of formula
		String lFormulaText = aFormulaTag.getChildValue("formulatext");
		lFormulaText = replaceVariablesWithinString(lFormulaText);
		boolean lParseError = false;
		try {
			Object object = getPropBsh().eval("(" + lFormulaText + ");");
			if (object != null) {
				lValue = object.toString();
			}
		} catch (EvalError ee) {
			lParseError = true;
		}
		if (lParseError) {
			return "";
		} else {
			return lValue;
		}
	}

	/**
	 * Handles states components formula's containing states or formulas.
	 *
	 * @param aCaseComponent the a case component
	 * @param aDataTag       the a data tag
	 * @param aStatusKey     the a status key
	 * @param aStatusValue   the a status value
	 * @param aCheckScript   the a check script, whether to call script to make it
	 *                       possible to react to the status change
	 * @param aStatusType    the a status type, see IAppManager
	 * @param aSaveInDb      the a save in db, whether status change is saved in
	 *                       database, or only in memory
	 * @param aFromScript    the a from script, whether this method is called from
	 *                       within script, so is a result of a script action
	 * @param aInit          the a init
	 */
	private void handleStatesFormulas(IECaseComponent aCaseComponent, IXMLTag aDataTag, boolean aCheckScript,
			String aStatusType, boolean aSaveInDb, boolean aFromScript, boolean aInit) {
		if (aCaseComponent == null || aDataTag == null)
			return;
		IECaseRole lCaseRole = getSCaseRoleHelper().getCaseRole();
		// NOTE First handle state change in formula, then formula change in formula,
		// because formulas can be nested.
		String[] lReftypes = { "statetag_formulatag", "formulatag_formulatag" };
		for (int i = 0; i < lReftypes.length; i++) {
			List<String> lRefIds = getCaseHelper().getTagRefIds(lReftypes[i], "" + AppConstants.statusKeySelectedIndex,
					lCaseRole, aCaseComponent, aDataTag, false);
			if (lRefIds != null) {
				for (String lRefId : lRefIds) {
					String[] lIds = lRefId.split(",");
					// get formula tag
					IXMLTag lFormulaTag = getTag(lIds[1], lIds[2]);
					if (lFormulaTag != null) {
						// determine value of formula
						String lValue = getFormulaValue(lFormulaTag);
						if (!lValue.equals("")) {
							boolean lSetValue = true;
							if (aInit) {
								lSetValue = getCurrentRunTagStatus(getCaseComponent(lIds[1]), lFormulaTag,
										AppConstants.statusKeyValue, aStatusType).equals("");
							} else
								// only update if formula value has changed
								lSetValue = !getCurrentRunTagStatus(getCaseComponent(lIds[1]), lFormulaTag,
										AppConstants.statusKeyValue, aStatusType).equals(lValue);
							if (lSetValue) {
								// set value of formula
								setRunTagStatus(getCaseComponent(lIds[1]), lFormulaTag, AppConstants.statusKeyValue,
										lValue, aCheckScript, aStatusType, aSaveInDb, aFromScript);
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Adds sent status childs. Predefined inmails sent by script or tutor should
	 * get sendername within status.
	 *
	 * @param aCaseComponent the a case component
	 * @param aDataTag       the a data tag
	 * @param aStatusTag     the a status tag
	 */
	public void addSentStatusChilds(IECaseComponent aCaseComponent, IXMLTag aDataTag, IXMLTag aStatusTag) {
		// predefined inmails sent by script or tutor should get sendername within
		// status
		// only predefined inmails have a childtag refsendernpc
		IXMLTag lChildTag = aDataTag.getChild("refsendernpc");
		if (lChildTag != null) {
			IECaseRole lCaseRole = getSCaseRoleHelper().getCaseRole();
			String lReftype = aDataTag.getDefTag().getChildAttribute("refsendernpc", AppConstants.defKeyReftype);
			// get reference to npc sender
			List<String> lRefIds = getCaseHelper().getRefTagIds(lReftype, "" + AppConstants.statusKeySelectedIndex,
					lCaseRole, aCaseComponent, aDataTag);
			if ((lRefIds != null) && (lRefIds.size() == 1)) {
				// only one sender of course
				String lRefId = lRefIds.get(0);
				String lCarId = "" + lCaseRole.getCarId();

				// first sender was defined in a case component persons and there could be one
				// or more persons components
				// there are still some old cases so we still have to support this
				List<IECaseComponent> lCaseComponents = getCaseComponents(aCaseComponent.getECase(), "persons");
				for (IECaseComponent lCaseComponent : lCaseComponents) {
					String lCacId = "" + lCaseComponent.getCacId();
					List<IXMLTag> lPersonTags = getPropCScript().getNodeTags(lCaseComponent, "person");
					for (IXMLTag lPersonTag : lPersonTags) {
						String lContactId = lCarId + "," + lCacId + "," + lPersonTag.getAttribute(AppConstants.keyId);
						if (lContactId.equals(lRefId)) {
							addSenderNameToStatusTag(aStatusTag, lPersonTag.getChildValue("name"));
						}
					}
				}

				// now sender is defined as npc case role
				// so newer cases use this code
				// get npc case roles
				List<IECaseRole> lCaseRoles = getSCaseRoleHelper().getCaseRoles(true);
				for (IECaseRole lTempCaseRole : lCaseRoles) {
					String lTempCarId = "" + lTempCaseRole.getCarId();
					String lContactId = lTempCarId + ",0,0";
					if (lContactId.equals(lRefId)) {
						addSenderNameToStatusTag(aStatusTag, lTempCaseRole.getName());
					}
				}

			}
		}
	}

	/**
	 * Add sender name to status tag.
	 *
	 * @param aStatusTag  the a status tag
	 * @param aSenderName the a sender name
	 */
	private void addSenderNameToStatusTag(IXMLTag aStatusTag, String aSenderName) {
		IXMLTag lStatusStatusTag = aStatusTag.getChild(AppConstants.statusElement);
		IXMLTag lOldSenderTag = lStatusStatusTag.getChild("sendername");
		Boolean lAdd = true;
		if (lOldSenderTag != null)
			if (lOldSenderTag.getValue().equals(aSenderName))
				lAdd = false;
		if (lAdd) {
			List<IXMLTag> lXmlChildTags = lStatusStatusTag.getChildTags();
			// add person as childtag
			IXMLTag lChildTag2 = getXmlManager().newXMLTag("sendername", aSenderName);
			lChildTag2.setParentTag(lStatusStatusTag);
			lXmlChildTags.add(lChildTag2);
		}
	}

	/**
	 * Creates new run tag.
	 *
	 * @param aName the a name
	 *
	 * @return the run tag
	 */
	public IXMLTag newRunTag(String aName) {
		return getXmlManager().newXMLTag(aName, "");
	}

	/**
	 * Creates new run node tag.
	 *
	 * @param aName        the a name
	 * @param aRefDataId   the a ref data id
	 * @param aStatusKey   the a status key
	 * @param aStatusValue the a status value
	 *
	 * @return the run node tag
	 */
	public IXMLTag newRunNodeTag(String aName, String aRefDataId, String aStatusKey, String aStatusValue) {
		IXMLTag lXmlRunTag = getXmlManager().newXMLTag(aName, "");
		lXmlRunTag.setAttribute(AppConstants.defKeyType, AppConstants.defValueNode);
		lXmlRunTag.setAttribute(AppConstants.keyRefdataid, aRefDataId);
		setRunTagStatusValue(lXmlRunTag, aStatusKey, aStatusValue);
		// NOTE following statement creates status child tag if it does not exist yet
		getStatusChild(lXmlRunTag);
		return lXmlRunTag;
	}

	/**
	 * Replaces run tag status value.
	 *
	 * @param aXmlRunTag   the a xml run tag
	 * @param aStatusKey   the a status key
	 * @param aStatusValue the a status value
	 */
	private void replaceRunTagStatusValue(IXMLTag aXmlRunTag, String aStatusKey, String aStatusValue) {
		setRunTagStatusValue(aXmlRunTag, aStatusKey, aStatusValue);
	}

	/**
	 * Sets run tag status value. If overwrite for status key is true, value is
	 * overwritten. Otherwise value is added.
	 *
	 * @param aXmlTag      the a xml tag
	 * @param aStatusKey   the a status key
	 * @param aStatusValue the a status value
	 */
	public void setRunTagStatusValue(IXMLTag aXmlTag, String aStatusKey, String aStatusValue) {
		if (aXmlTag == null || aStatusKey == null || aStatusKey.equals(""))
			return;
		IXMLTag lStatusTag = getStatusChild(aXmlTag);
		boolean lOverwrite = getAppManager().getOverwriteStatusValue(aStatusKey);
		if (lOverwrite) {
			lStatusTag.setAttribute(aStatusKey, aStatusValue, getCaseTime());
		} else {
			lStatusTag.addAttributeValueTimeToList(aStatusKey, aStatusValue, getCaseTime());
		}
	}

	/**
	 * Sets run tag status values for a number of status keys.
	 *
	 * @param aXmlTag       the a xml tag
	 * @param aStatusKeys   the a status keys
	 * @param aStatusValues the a status values
	 */
	public void setRunTagStatusValues(IXMLTag aXmlTag, String[] aStatusKeys, String[] aStatusValues) {
		if (aXmlTag == null)
			return;
		for (int i = 0; i < aStatusKeys.length; i++)
			setRunTagStatusValue(aXmlTag, aStatusKeys[i], aStatusValues[i]);
	}

	/**
	 * Gets status tag of data tag. If it does not exist it is added.
	 *
	 * @param aDataTag the a data tag
	 *
	 * @return the status tag
	 */
	public IXMLTag getStatusTagOfDataTag(IXMLTag aDataTag) {
		if (aDataTag == null) {
			return null;
		}
		if (aDataTag.getStatusTag() == null) {
			aDataTag.setStatusTag(
					newRunNodeTag(aDataTag.getName(), aDataTag.getAttribute(AppConstants.keyId), null, null));
		}
		return aDataTag.getStatusTag();
	}

	/**
	 * Gets status child of run tag. If it does not exist it is added.
	 *
	 * @param aXmlRunTag the a xml run tag
	 *
	 * @return the status child tag
	 */
	public IXMLTag getStatusChild(IXMLTag aXmlRunTag) {
		IXMLTag lStatusTag = aXmlRunTag.getChild(AppConstants.statusElement);
		if (lStatusTag == null) {
			lStatusTag = getXmlManager().newXMLTag(AppConstants.statusElement, "");
			lStatusTag.setParentTag(aXmlRunTag);
			aXmlRunTag.getChildTags().add(lStatusTag);
		}
		return lStatusTag;
	}

	/**
	 * Gets the current run component status. That is the last value saved.
	 *
	 * @param aCaseComponent the a case component
	 * @param aStatusKey     the a status key
	 * @param aStatusType    the a status type, see IAppManager
	 *
	 * @return the current run component status
	 */
	public String getCurrentRunComponentStatus(IECaseComponent aCaseComponent, String aStatusKey, String aStatusType) {
		IXMLTag lDataStatusTag = getComponentDataStatusTag(aCaseComponent, aStatusType);
		if (lDataStatusTag == null) {
			return "";
		}
		return lDataStatusTag.getCurrentStatusAttribute(aStatusKey);
	}

	/**
	 * Gets the current run component status. That is the last value saved.
	 *
	 * @param aRugId         the a rug id
	 * @param aCaseComponent the a case component
	 * @param aStatusKey     the a status key
	 *
	 * @return the current run component status for aRugId
	 */
	public String getCurrentRunComponentStatus(int aRugId, IECaseComponent aCaseComponent, String aStatusKey) {
		IXMLTag lDataStatusTag = getComponentDataStatusTag(aRugId, aCaseComponent);
		if (lDataStatusTag == null) {
			return "";
		}
		return lDataStatusTag.getCurrentStatusAttribute(aStatusKey);
	}

	/**
	 * Gets the previous run component status. That is the value given by aIndex.
	 *
	 * @param aCaseComponent the a case component
	 * @param aStatusKey     the a status key
	 * @param aIndex         the a index
	 * @param aStatusType    the a status type, see IAppManager
	 *
	 * @return the previous run component status
	 */
	public String getPreviousRunComponentStatus(IECaseComponent aCaseComponent, String aStatusKey, int aIndex,
			String aStatusType) {
		IXMLTag lDataStatusTag = getComponentDataStatusTag(aCaseComponent, aStatusType);
		if (lDataStatusTag == null) {
			return "";
		}
		return lDataStatusTag.getPreviousStatusAttribute(aStatusKey, aIndex);
	}

	/**
	 * Gets the previous run component status. That is the value given by aIndex.
	 *
	 * @param aRugId         the a rug id
	 * @param aCaseComponent the a case component
	 * @param aStatusKey     the a status key
	 * @param aIndex         the a index
	 *
	 * @return the previous run component status
	 */
	public String getPreviousRunComponentStatus(int aRugId, IECaseComponent aCaseComponent, String aStatusKey,
			int aIndex, String aStatusType) {
		IXMLTag lDataStatusTag = getComponentDataStatusTag(aRugId, aCaseComponent);
		if (lDataStatusTag == null) {
			return "";
		}
		return lDataStatusTag.getPreviousStatusAttribute(aStatusKey, aIndex);
	}

	/**
	 * Gets the current run component status time. That is the last time saved.
	 *
	 * @param aCaseComponent the a case component
	 * @param aStatusKey     the a status key
	 * @param aStatusType    the a status type, see IAppManager
	 *
	 * @return the current run component status time
	 */
	public double getCurrentRunComponentStatusTime(IECaseComponent aCaseComponent, String aStatusKey,
			String aStatusType) {
		IXMLTag lDataStatusTag = getComponentDataStatusTag(aCaseComponent, aStatusType);
		if (lDataStatusTag == null) {
			return -1;
		}
		return lDataStatusTag.getCurrentStatusAttributeTime(aStatusKey);
	}

	/**
	 * Gets the current run component status time. That is the last time saved.
	 *
	 * @param aRugId         the a rug id
	 * @param aCaseComponent the a case component
	 * @param aStatusKey     the a status key
	 *
	 * @return the current run component status time for aRugId
	 */
	public double getCurrentRunComponentStatusTime(int aRugId, IECaseComponent aCaseComponent, String aStatusKey) {
		IXMLTag lDataStatusTag = getComponentDataStatusTag(aRugId, aCaseComponent);
		if (lDataStatusTag == null) {
			return -1;
		}
		return lDataStatusTag.getCurrentStatusAttributeTime(aStatusKey);
	}

	/**
	 * Gets the previous run component status time. That is the time given by
	 * aIndex.
	 *
	 * @param aCaseComponent the a case component
	 * @param aStatusKey     the a status key
	 * @param aIndex         the a index
	 * @param aStatusType    the a status type, see IAppManager
	 *
	 * @return the previous run component status time
	 */
	public String getPreviousRunComponentStatusTime(IECaseComponent aCaseComponent, String aStatusKey, int aIndex,
			String aStatusType) {
		IXMLTag lDataStatusTag = getComponentDataStatusTag(aCaseComponent, aStatusType);
		if (lDataStatusTag == null) {
			return "";
		}
		return "" + lDataStatusTag.getPreviousStatusAttributeTime(aStatusKey, aIndex);
	}

	/**
	 * Gets the previous run component status time. That is the vtime given by
	 * aIndex.
	 *
	 * @param aRugId         the a rug id
	 * @param aCaseComponent the a case component
	 * @param aStatusKey     the a status key
	 * @param aIndex         the a index
	 *
	 * @return the previous run component time
	 */
	public String getPreviousRunComponentStatusTime(int aRugId, IECaseComponent aCaseComponent, String aStatusKey,
			int aIndex, String aStatusType) {
		IXMLTag lDataStatusTag = getComponentDataStatusTag(aRugId, aCaseComponent);
		if (lDataStatusTag == null) {
			return "";
		}
		return "" + lDataStatusTag.getPreviousStatusAttributeTime(aStatusKey, aIndex);
	}

	/**
	 * Gets the component data status tag.
	 *
	 * @param aCaseComponent the a case component
	 * @param aStatusType    the a status type, see IAppManager
	 *
	 * @return the component data status tag
	 */
	public IXMLTag getComponentDataStatusTag(IECaseComponent aCaseComponent, String aStatusType) {
		if (aCaseComponent == null)
			return null;
		IXMLTag lRootTag = getXmlDataPlusRunStatusTree(aCaseComponent, aStatusType);
		if (lRootTag == null)
			return null;
		return lRootTag.getChild(AppConstants.componentElement);
	}

	/**
	 * Gets the component data status tag.
	 *
	 * @param aRugId         the a rug id
	 * @param aCaseComponent the a case component
	 *
	 * @return the component data status tag
	 */
	public IXMLTag getComponentDataStatusTag(int aRugId, IECaseComponent aCaseComponent) {
		if (aCaseComponent == null)
			return null;
		IXMLTag lRootTag = getXmlDataPlusRunGroupStatusTree(aRugId, aCaseComponent);
		if (lRootTag == null)
			return null;
		return lRootTag.getChild(AppConstants.componentElement);
	}

	/**
	 * Sets run component status.
	 *
	 * @param aCaseComponent the a case component
	 * @param aStatusKey     the a status key
	 * @param aStatusValue   the a status value
	 * @param aCheckScript   the a check script, whether to call script to make it
	 *                       possible to react to the status change
	 * @param aStatusType    the a status type, see IAppManager
	 * @param aSaveInDb      the a save in db, whether status change is saved in
	 *                       database, or only in memory
	 */
	public void setRunComponentStatus(IECaseComponent aCaseComponent, String aStatusKey, String aStatusValue,
			boolean aCheckScript, String aStatusType, boolean aSaveInDb) {
		STriggeredReference lTriggeredReference = new STriggeredReference();
		lTriggeredReference.setCaseComponent(aCaseComponent);
		lTriggeredReference.setStatusKey(aStatusKey);
		lTriggeredReference.setStatusValue(aStatusValue);
		setRunComponentStatus(lTriggeredReference, aCheckScript, aStatusType, aSaveInDb, false);
	}

	/**
	 * Sets run component status.
	 *
	 * @param aTriggeredReference the a triggered reference
	 * @param aCheckScript        the a check script, whether to call script to make
	 *                            it possible to react to the status change
	 * @param aStatusType         the a status type, see IAppManager
	 * @param aSaveInDb           the a save in db, whether status change is saved
	 *                            in database, or only in memory
	 */
	public void setRunComponentStatus(STriggeredReference aTriggeredReference, boolean aCheckScript, String aStatusType,
			boolean aSaveInDb) {
		setRunComponentStatus(aTriggeredReference, aCheckScript, aStatusType, aSaveInDb, false);
	}

	/**
	 * Sets run component status.
	 *
	 * @param aTriggeredReference the a triggered reference
	 * @param aCheckScript        the a check script, whether to call script to make
	 *                            it possible to react to the status change
	 * @param aStatusType         the a status type, see IAppManager
	 * @param aSaveInDb           the a save in db, whether status change is saved
	 *                            in database, or only in memory
	 * @param aFromScript         the a from script, whether this method is called
	 *                            from within script, so is a result of a script
	 *                            action
	 */
	protected void setRunComponentStatus(STriggeredReference aTriggeredReference, boolean aCheckScript,
			String aStatusType, boolean aSaveInDb, boolean aFromScript) {
		IECaseComponent lCaseComponent = aTriggeredReference.getCaseComponent();
		String lStatusKey = aTriggeredReference.getStatusKey();
		String lStatusValue = aTriggeredReference.getStatusValue();
		if ((lStatusKey == null) || (lStatusKey.equals("")))
			return;
		if (lCaseComponent == null)
			return;
		IXMLTag lStatusRootTag = getXmlRunStatusTree(lCaseComponent, aStatusType);
		if (lStatusRootTag == null)
			return;
		IXMLTag lStatusComponentTag = lStatusRootTag.getChild(AppConstants.componentElement);
		if (lStatusComponentTag == null)
			return;

		// only attributes of component tag are set
		setRunTagStatusValue(lStatusComponentTag, lStatusKey, lStatusValue);

		if (!lStatusKey.equals(AppConstants.statusKeyCurrentTime))
			// log the status change
			getSLogHelper().logSetRunComponentAction("setruncomponentstatus", aStatusType,
					getXmlManager().unescapeXML(lCaseComponent.getName()), lStatusKey, lStatusValue, aCheckScript);

		// save status change depending on aSaveInDb
		// notify player of status change
		// check script depending on aCheckScript
		if (aStatusType.equals(AppConstants.statusTypeRunGroup)) {
			if (aSaveInDb) {
				IERunGroup lRunGroup = getRunGroup();
				if (lRunGroup != null)
					setSaveRunGroupCaseComponentStatusRootTag(lRunGroup, lCaseComponent, lStatusRootTag);
			}
			// notify player of status change
			runGroupComponentChangeToPlayer(lCaseComponent, lStatusKey, lStatusValue, aFromScript);
			if (aCheckScript) {
				getSScriptHelper().checkRunGroupComponentScript(aTriggeredReference, aStatusType, aSaveInDb);
			}
		}
		if (aStatusType.equals(AppConstants.statusTypeRun)) {
			if (aSaveInDb) {
				IERun lRun = getRun();
				if (lRun != null)
					setRunCaseComponentStatusRootTag(lRun, lCaseComponent, lStatusRootTag);
			}
			// notify player of status change
			runComponentChangeToPlayer(lCaseComponent, lStatusKey, lStatusValue);
			if (aCheckScript)
				;
			// checking script for run status still has to be implemented.
			// Problem is there cannot be script defined for run status change, only for
			// data tag status change.
			// It has to be possible to add script on run status change, for instance 'if a
			// run status tag is added'.
			// checkRunGroupComponentScript(aCaseComponent, aStatusKey, aStatusType,
			// aSaveInDb);
		}
		if (aStatusType.equals(AppConstants.statusTypeRunTeam)) {
			if (aSaveInDb) {
				IERunTeam lRunTeam = getRunTeam();
				if (lRunTeam != null)
					setRunTeamCaseComponentStatusRootTag(lRunTeam, lCaseComponent, lStatusRootTag);
			}
			// notify player of status change
			runTeamComponentChangeToPlayer(lCaseComponent, lStatusKey, lStatusValue);
			if (aCheckScript)
				;
			// checking script for run team status still has to be implemented.
			// Problem is there cannot be script defined for run team status change, only
			// for datatag status change.
			// It has to be possible to add script on run team status change, for instance
			// 'if a run team status tag is added'.
			// checkRunGroupComponentScript(aCaseComponent, aStatusKey, aStatusType,
			// aSaveInDb);
		}
	}

	/**
	 * Run group tag change to player.
	 *
	 * @param aCaseComponent the a case component
	 * @param aTag           the a tag
	 * @param aStatusTag     the a status tag
	 * @param aStatusKey     the a status key
	 * @param aStatusValue   the a status value
	 * @param aFromScript    the a from script, whether this method is called from
	 *                       within script, so is a result of a script action
	 */
	protected void runGroupTagChangeToPlayer(IECaseComponent aCaseComponent, IXMLTag aTag, IXMLTag aStatusTag,
			String aStatusKey, String aStatusValue, boolean aFromScript) {
		if (getPropRunWnd() != null)
			getPropRunWnd().statusChange(aCaseComponent, aTag, aStatusTag, aStatusKey, aStatusValue, aFromScript);
	}

	/**
	 * Run tag change to player.
	 *
	 * @param aCaseComponent the a case component
	 * @param aTag           the a tag
	 * @param aStatusTag     the a status tag
	 * @param aStatusKey     the a status key
	 * @param aStatusValue   the a status value
	 */
	protected void runTagChangeToPlayer(IECaseComponent aCaseComponent, IXMLTag aTag, IXMLTag aStatusTag,
			String aStatusKey, String aStatusValue) {
		// What has to happen in the player to reflect a runTag change?
		// Probably it should be handled different as a runGroupTag change.
	}

	/**
	 * Run team tag change to player.
	 *
	 * @param aCaseComponent the a case component
	 * @param aTag           the a tag
	 * @param aStatusTag     the a status tag
	 * @param aStatusKey     the a status key
	 * @param aStatusValue   the a status value
	 */
	protected void runTeamTagChangeToPlayer(IECaseComponent aCaseComponent, IXMLTag aTag, IXMLTag aStatusTag,
			String aStatusKey, String aStatusValue) {
		// What has to happen in the player to reflect a runTeamTag change?
		// Probably it should be handled different as a runGroupTag change.
	}

	/**
	 * Run group component change to player.
	 *
	 * @param aCaseComponent the a case component
	 * @param aStatusKey     the a status key
	 * @param aStatusValue   the a status value
	 * @param aFromScript    the a from script, whether this method is called from
	 *                       within script, so is a result of a script action
	 */
	protected void runGroupComponentChangeToPlayer(IECaseComponent aCaseComponent, String aStatusKey,
			String aStatusValue, boolean aFromScript) {
		if (getPropRunWnd() != null)
			getPropRunWnd().statusChange(aCaseComponent, null, null, aStatusKey, aStatusValue, aFromScript);
	}

	/**
	 * Run component change to player.
	 *
	 * @param aCaseComponent the a case component
	 * @param aStatusKey     the a status key
	 * @param aStatusValue   the a status value
	 */
	protected void runComponentChangeToPlayer(IECaseComponent aCaseComponent, String aStatusKey, String aStatusValue) {
		// What has to happen in the player to reflect a runComponent change?
		// Probably it should be handled different as a runGroupComponent change.
	}

	/**
	 * Run team component change to player.
	 *
	 * @param aCaseComponent the a case component
	 * @param aStatusKey     the a status key
	 * @param aStatusValue   the a status value
	 */
	protected void runTeamComponentChangeToPlayer(IECaseComponent aCaseComponent, String aStatusKey,
			String aStatusValue) {
		// What has to happen in the player to reflect a runTeamComponent change?
		// Probably it should be handled different as a runGroupComponent change.
	}

	/**
	 * Gets the case time in seconds. The total time the Emergo player has been open
	 * for the current run group account.
	 *
	 * @return the case time
	 */
	public double getCaseTime() {
		double lTime = 0;
		if (getPropRunMainTimer() != null) {
			lTime = getPropRunMainTimer().getCaseTime();
			lTime = lTime / 1000;
			// store caseTime within this class.
			caseTime = lTime;
		}
		return caseTime;
	}

	/**
	 * Gets the case start time in seconds. The total time the Emergo player has
	 * been open till it was opened the last time, for the current run group
	 * account.
	 *
	 * @return the case start time
	 */
	public double getCaseStartTime() {
		double lTime = 0;
		if (getPropRunMainTimer() != null) {
			lTime = getPropRunMainTimer().getCaseStartTime();
			lTime = lTime / 1000;
		}
		return lTime;
	}

	/**
	 * Gets the real time in seconds (since 1970).
	 *
	 * @return the real time.
	 */
	public double getRealTime() {
		double lTime = 0;
		if (getPropRunMainTimer() != null) {
			lTime = getPropRunMainTimer().getRealTime();
			lTime = lTime / 1000;
		}
		return lTime;
	}

	/**
	 * Gets the real time in seconds till the start of the current run.
	 *
	 * @return the real time form start of run.
	 */
	public double getRealTimeFromStartOfRun() {
		double lTime = 0;
		if (getPropRunMainTimer() != null) {
			lTime = getPropRunMainTimer().getRealTimeFromStartOfRun();
			lTime = lTime / 1000;
		}
		return lTime;
	}

	/**
	 * Copies xml dependencies. aNewCaseComponent is already a copy of
	 * aOldCaseComponent, but within the xml data of the new case component also
	 * copies have to be made. Blobs referenced have to be copied and the references
	 * in xml data changed accordingly. References to cac id in xml data have to be
	 * changed. And within the xml data of the case case component references to cac
	 * id have to copied too.
	 *
	 * @param aOldCaseComponent the a old case component
	 * @param aNewCaseComponent the a new case component
	 *
	 * @return the new case component
	 */
	public IECaseComponent copyXmlDependencies(IECaseComponent aOldCaseComponent, IECaseComponent aNewCaseComponent) {
		if ((aOldCaseComponent == null) || (aNewCaseComponent == null))
			return aNewCaseComponent;
//		get roottag
		IXMLTag lRootTag = getXmlDataTree(aNewCaseComponent);
		if (lRootTag == null)
			return aNewCaseComponent;
//		copy existing blobs within xmldata
		copyCaseComponentBlobs(lRootTag);
//		change cacid references to self
//		now applicable for scripts and items
		changeCaseComponentReferencesToSelf(aOldCaseComponent, aNewCaseComponent, lRootTag);
		ICaseComponentManager lBean = (ICaseComponentManager) getBean("caseComponentManager");
//		update xmldata
		String lXmlData = getXmlManager().xmlTreeToDoc(lRootTag);
		if (lXmlData != null && !lXmlData.equals(""))
			aNewCaseComponent.setXmldata(lXmlData);
		lBean.updateCaseComponent(aNewCaseComponent);
//		copy cac references within casecasecomponent data
		copyCaseCaseComponentReferences(aOldCaseComponent, aNewCaseComponent);
		return lBean.getCaseComponent(aNewCaseComponent.getCacId());
	}

	/**
	 * Copies case component blobs. Makes copies of all blobs referenced within
	 * aRootTag and changes the references.
	 *
	 * @param aRootTag the a root tag
	 */
	private void copyCaseComponentBlobs(IXMLTag aRootTag) {
		List<IXMLTag> lNodeTags = getPropCScript().getNodeTags(aRootTag);
		// NOTE component and content tag might have blob childs too, so add them
		lNodeTags.add(aRootTag.getChild(AppConstants.componentElement));
		lNodeTags.add(aRootTag.getChild(AppConstants.contentElement));
		copyBlobTags(lNodeTags);
	}

	/**
	 * Copies blob tags. Makes copies of all blobs referenced within aTags and
	 * changes the references.
	 *
	 * @param aTags the a tags
	 */
	private void copyBlobTags(List<IXMLTag> aTags) {
		if (aTags == null)
			return;
		IBlobManager lBean = (IBlobManager) getBean("blobManager");
		for (IXMLTag lTag : aTags) {
			List<IXMLTag> lBlobTags = new ArrayList<IXMLTag>();
			for (String childTagName : AppConstants.nodeChildTagsWithBlob) {
				lBlobTags.addAll(lTag.getChilds(childTagName));
			}
			for (IXMLTag lBlobTag : lBlobTags) {
				String lBlobId = lBlobTag.getValue();
				if (!lBlobId.equals("")) {
//					copy existing blob
					lBlobId = "" + lBean.copyBlob(Integer.parseInt(lBlobId));
//					and change blob id
					lBlobTag.setValue(lBlobId);
				}
			}
		}
	}

	/**
	 * Change case component references to self. References to cac id in aRootTag
	 * have to be changed.
	 *
	 * @param aOldCaseComponent the a old case component
	 * @param aNewCaseComponent the a new case component
	 * @param aRootTag          the a root tag
	 */
	private void changeCaseComponentReferencesToSelf(IECaseComponent aOldCaseComponent,
			IECaseComponent aNewCaseComponent, IXMLTag aRootTag) {
//		change cacid references to self
//		now applicable for scripts and items
		if ((aOldCaseComponent == null) || (aNewCaseComponent == null))
			return;
		CScript lScript = getPropCScript();
		List<IXMLTag> lAllTags = lScript.getAllTags(aRootTag);
		List<IXMLTag> lCacidTags = lScript.getTags(lAllTags, "cacid");
		changeReferences(lCacidTags, "" + aOldCaseComponent.getCacId(), "" + aNewCaseComponent.getCacId());
	}

	/**
	 * Change references in aTags to aOldId to aNewId.
	 *
	 * @param aTags  the a tags
	 * @param aOldId the a old id
	 * @param aNewId the a new id
	 */
	private void changeReferences(List<IXMLTag> aTags, String aOldId, String aNewId) {
		if (aTags == null)
			return;
		for (IXMLTag lTag : aTags) {
			if (lTag.getValue().equals(aOldId))
				lTag.setValue(aNewId);
		}
	}

	/**
	 * Copies case case component references. References to old cac id are copied to
	 * new cac id.
	 *
	 * @param aOldCaseComponent the a old case component
	 * @param aNewCaseComponent the a new case component
	 */
	private void copyCaseCaseComponentReferences(IECaseComponent aOldCaseComponent, IECaseComponent aNewCaseComponent) {
//		copy cac references within casecasecomponent data
		if ((aOldCaseComponent == null) || (aNewCaseComponent == null))
			return;
//		get case case component
		IECaseComponent lCaseCaseComponent = getCaseComponent(getCase(), "case", "");
//		get roottag
		IXMLTag lRootTag = getXmlDataTree(lCaseCaseComponent);
		CScript lScript = getPropCScript();
		List<IXMLTag> lAllTags = lScript.getAllTags(lRootTag);
		List<IXMLTag> lRefcacTags = lScript.getTags(lAllTags, "refcac");
		copyRefcacTagsReferences(lRefcacTags, "" + aOldCaseComponent.getCacId(), "" + aNewCaseComponent.getCacId());
//		save xmldata
		String lXmlData = getXmlManager().xmlTreeToDoc(lRootTag);
		if ((lXmlData == null) || (lXmlData.equals("")))
			return;
		lCaseCaseComponent.setXmldata(lXmlData);
		ICaseComponentManager lBean = (ICaseComponentManager) getBean("caseComponentManager");
		lBean.updateCaseComponent(lCaseCaseComponent);
	}

	/**
	 * Copies refcac tags references. References to old cac id are copied to new cac
	 * id.
	 *
	 * @param aTags     the a tags
	 * @param aOldCacId the a old cac id
	 * @param aNewCacId the a new cac id
	 */
	private void copyRefcacTagsReferences(List<IXMLTag> aTags, String aOldCacId, String aNewCacId) {
		if (aTags == null)
			return;
		for (IXMLTag lTag : aTags) {
			if (lTag.getValue().equals(aOldCacId)) {
				IXMLTag lParent = lTag.getParentTag();
				if (lParent != null) {
					IXMLTag lCac = lTag.getParentTag();
					List<IXMLTag> lAncestors = new ArrayList<IXMLTag>(0);
					while (!lCac.getName().equals("cac")) {
						lAncestors.add(0, lCac);
						lCac = lCac.getParentTag();
					}
					if (lCac.getValue().equals(aOldCacId)) {
						// if cac and refcac are equal, first the branch has to be copied
						lParent = null;
						// get (new) cac
						IXMLTag lCopyTag = getOrCopyTagByValue(lCac.getParentTag(), lCac, aNewCacId);
						if (lCopyTag != null) {
							for (IXMLTag lAncestor : lAncestors) {
								if (lCopyTag != null)
									// get (new) ancestor
									lCopyTag = getOrCopyTagByValue(lCopyTag, lAncestor, lAncestor.getValue());
							}
						}
						if (lCopyTag != null)
							// if ok
							lParent = lCopyTag;
					}
					if (lParent != null) {
						// copy refcac with childs
						IXMLTag lCopyTag = getXmlManager().copyTag(lTag, lParent);
						if (lCopyTag != null) {
							lCopyTag.setValue(aNewCacId);
							lParent.getChildTags().add(lCopyTag);
						}
					}
				}
			}
		}
	}

	/**
	 * Gets the tag within the children of aParentTag, given by aValue. If it does
	 * not exist a copy is made of aTag with value set to aValue.
	 *
	 * @param aParentTag the a parent tag
	 * @param aTag       the a tag
	 * @param aValue     the a value
	 *
	 * @return the xml tag
	 */
	private IXMLTag getOrCopyTagByValue(IXMLTag aParentTag, IXMLTag aTag, String aValue) {
		IXMLTag lTag = null;
		List<IXMLTag> lChildTags = aParentTag.getChildTags();
		for (IXMLTag lChildTag : lChildTags) {
			if (lChildTag.getValue().equals(aValue))
				// if tag exists don't copy
				lTag = lChildTag;
		}
		if (lTag == null) {
			lTag = aTag.cloneWithoutParentAndChildren();
			if (lTag != null) {
				lTag.setParentTag(aParentTag);
				lTag.setValue(aValue);
				aParentTag.getChildTags().add(lTag);
			}
		}
		return lTag;
	}

	/**
	 * Copies case data. aNewCase is already a copy of aOldCase, but all case data
	 * has to be copied too. Copies case roles, case components and case component
	 * roles. And within the xml data of the case components blobs are copied and
	 * references to blob ids, car ids and cac ids are adjusted.
	 *
	 * @param aOldCase the a old case
	 * @param aNewCase the a new case
	 *
	 * @return the new case
	 */
	public IECase copyCaseData(IECase aOldCase, IECase aNewCase) {
		if ((aOldCase == null) || (aNewCase == null))
			return aNewCase;
//		copy caseroles
		ICaseRoleManager lBean2 = (ICaseRoleManager) getBean("caseRoleManager");
		List<IECaseRole> lCaseRoles = lBean2.getAllCaseRolesByCasId(aOldCase.getCasId());
//		"student" role auto generated when creating new case!!
		List<IECaseRole> lNewAutoGenRoles = lBean2.getAllCaseRolesByCasId(aNewCase.getCasId());
		Hashtable<String, IECaseRole> lCarIds = new Hashtable<String, IECaseRole>(0);
		for (IECaseRole lCaseRole : lCaseRoles) {
			IECaseRole lNewCaseRole = null;
			for (IECaseRole lNewAGRole : lNewAutoGenRoles) {
//				role names must be unique for each case
				if (lNewAGRole.getName().equals(lCaseRole.getName()))
					lNewCaseRole = (IECaseRole) lNewAGRole.clone();
			}
			if (lNewCaseRole == null) {
				lNewCaseRole = (IECaseRole) lCaseRole.clone();
				// set to 0 to get new record in db
				lNewCaseRole.setCarId(0);
				lNewCaseRole.setECase(aNewCase);
				lBean2.newCaseRole(lNewCaseRole);
			}
			lCarIds.put("" + lCaseRole.getCarId(), lNewCaseRole);
		}
//		copy casecomponents
		ICaseComponentManager lBean = (ICaseComponentManager) getBean("caseComponentManager");
		List<IECaseComponent> lCaseComponents = lBean.getAllCaseComponentsByCasId(aOldCase.getCasId());
		Hashtable<String, IECaseComponent> lCacIds = new Hashtable<String, IECaseComponent>(0);
		for (IECaseComponent lCaseComponent : lCaseComponents) {
			IEComponent lComponent = lCaseComponent.getEComponent();
			boolean lIsSystemComponent = (lComponent.getType() == AppConstants.system_component);
			IECaseComponent lNewCaseComponent = null;
			if (lIsSystemComponent) {
				// system component is already created when case was created so get it
				List<IECaseComponent> lNewCaseComponents2 = lBean.getAllCaseComponentsByCasId(aNewCase.getCasId());
				for (IECaseComponent lNewCaseComponent2 : lNewCaseComponents2) {
					if (lNewCaseComponent2.getEComponent().getCode().equals(lCaseComponent.getEComponent().getCode())) {
						// only one system component per code
						lNewCaseComponent = lNewCaseComponent2;
						// and copy xmldata
						lNewCaseComponent.setXmldata(lCaseComponent.getXmldata());
						// and save
						lBean.updateCaseComponent(lNewCaseComponent);
					}
				}
			} else {
				lNewCaseComponent = (IECaseComponent) lCaseComponent.clone();
				// set to 0 to get new record in db
				lNewCaseComponent.setCacId(0);
				lNewCaseComponent.setECase(aNewCase);
				lBean.newCaseComponent(lNewCaseComponent);
			}
			if (lNewCaseComponent != null) {
//				copy existing blobs within xmldata
//				get roottag
				IXMLTag lRootTag = getXmlDataTree(lNewCaseComponent);
				copyCaseComponentBlobs(lRootTag);
//				save xmldata
				String lXmlData = getXmlManager().xmlTreeToDoc(lRootTag);
				if (lXmlData != null && !lXmlData.equals(""))
					lNewCaseComponent.setXmldata(lXmlData);
				lBean.updateCaseComponent(lNewCaseComponent);
				String lCacId = "" + lCaseComponent.getCacId();
				lCacIds.put(lCacId, lNewCaseComponent);
			}
		}
		// change refs to cacid and carid to new cacid and carid
		for (Enumeration<String> keys = lCacIds.keys(); keys.hasMoreElements();) {
			IECaseComponent lCaseComponent = lCacIds.get(keys.nextElement());
//			get roottag
			IXMLTag lRootTag = getXmlDataTree(lCaseComponent);
			for (Enumeration<String> keys2 = lCacIds.keys(); keys2.hasMoreElements();) {
				String lCacId = keys2.nextElement();
				String lNewCacId = "" + lCacIds.get(lCacId).getCacId();
				// change refs to cacid to new cacid
				changeCaseComponentReferences(lCacId, lNewCacId, "cacid", lRootTag);
				changeCaseComponentReferences(lCacId, lNewCacId, "cac", lRootTag);
				changeCaseComponentReferences(lCacId, lNewCacId, "refcac", lRootTag);
			}
			for (Enumeration<String> keys3 = lCarIds.keys(); keys3.hasMoreElements();) {
				String lCarId = keys3.nextElement();
				String lNewCarId = "" + lCarIds.get(lCarId).getCarId();
				// change refs to carid to new carid
				changeCaseComponentReferences(lCarId, lNewCarId, "carid", lRootTag);
				changeCaseComponentReferences(lCarId, lNewCarId, "car", lRootTag);
				changeCaseComponentReferences(lCarId, lNewCarId, "refcar", lRootTag);
			}
//			save xmldata
			String lXmlData = getXmlManager().xmlTreeToDoc(lRootTag);
			if (lXmlData != null && !lXmlData.equals(""))
				lCaseComponent.setXmldata(lXmlData);
			lBean.updateCaseComponent(lCaseComponent);
		}
//		copy casecomponentroles
		ICaseComponentRoleManager lBean3 = (ICaseComponentRoleManager) getBean("caseComponentRoleManager");
		for (Enumeration<String> keys = lCacIds.keys(); keys.hasMoreElements();) {
			String lCacId = keys.nextElement();
			IECaseComponent lNewCaseComponent = lCacIds.get(lCacId);
			int lNewCacId = lNewCaseComponent.getCacId();
			for (Enumeration<String> keys2 = lCarIds.keys(); keys2.hasMoreElements();) {
				String lCarId = keys2.nextElement();
				IECaseRole lNewCaseRole = lCarIds.get(lCarId);
				if (!lNewCaseRole.getNpc()) {
					// only playing characters have an entry in casecomponentroles!
					int lNewCarId = lNewCaseRole.getCarId();
//					first check if new casecomponentrole already exists (already added for system components)
					List<IECaseComponentRole> lNewCaseComponentRolesExist = lBean3
							.getAllCaseComponentRolesByCacIdCarId(lNewCacId, lNewCarId);
					if (lNewCaseComponentRolesExist.isEmpty()) {
						List<IECaseComponentRole> lCaseComponentRoles = lBean3.getAllCaseComponentRolesByCacIdCarId(
								Integer.parseInt(lCacId), Integer.parseInt(lCarId));
						for (IECaseComponentRole lCaseComponentRole : lCaseComponentRoles) {
							IECaseComponentRole lNewCaseComponentRole = (IECaseComponentRole) lCaseComponentRole
									.clone();
							// set to 0 to get new record in db
							lNewCaseComponentRole.setCcrId(0);
							lNewCaseComponentRole.setCacCacId(lNewCaseComponent.getCacId());
							lNewCaseComponentRole.setCarCarId(lNewCaseRole.getCarId());
							lBean3.newCaseComponentRole(lNewCaseComponentRole);
						}
					}
				}
			}
		}
		getCaseManager().updateCase(aNewCase);
		return getCaseManager().getCase(aNewCase.getCasId());
	}

	/**
	 * Change case component references. References to tags with aTagName in
	 * aRootTag have to be changed.
	 *
	 * @param aOldId   the a old id
	 * @param aNewId   the a new id
	 * @param aTagName the a tag name
	 * @param aRootTag the a root tag
	 */
	private void changeCaseComponentReferences(String aOldId, String aNewId, String aTagName, IXMLTag aRootTag) {
//		change cacid references to self
//		now applicable for scripts and items
		CScript lScript = getPropCScript();
		List<IXMLTag> lAllTags = lScript.getAllTags(aRootTag);
		List<IXMLTag> lNamedTags = lScript.getTags(lAllTags, aTagName);
		changeReferences(lNamedTags, aOldId, aNewId);
	}

	/**
	 * Copies run data. aNewRun is already a copy of aOldRun, but run status has to
	 * be copied too.
	 *
	 * @param aOldRun the a old run
	 * @param aNewRun the a new run
	 *
	 * @return the new run
	 */
	public IERun copyRunData(IERun aOldRun, IERun aNewRun) {
		if (aOldRun == null || aNewRun == null)
			return aNewRun;
		// copy run status
		IRunCaseComponentManager lBean = (IRunCaseComponentManager) getBean("runCaseComponentManager");
		for (IERunCaseComponent lRunCaseComponent : lBean.getAllRunCaseComponentsByRunId(aOldRun.getRunId())) {
			IERunCaseComponent lNewRunCaseComponent = (IERunCaseComponent) lRunCaseComponent.clone();
			// set to 0 to get new record in db
			lNewRunCaseComponent.setRccId(0);
			lNewRunCaseComponent.setRunRunId(aNewRun.getRunId());
			lBean.newRunCaseComponent(lNewRunCaseComponent);
		}
		getRunManager().updateRun(aNewRun);
		return getRunManager().getRun(aNewRun.getRunId());
	}

	/**
	 * Copies run group case components, the status of a run group for all case
	 * components. Also copies blobs referenced within xml data and updates
	 * references to blob ids.
	 *
	 * @param aOldRunGroup the a old run group
	 * @param aNewRunGroup the a new run group
	 * @param aCase        the a case
	 *
	 * @return the new run group
	 */
	public IERunGroup copyRunGroupCaseComponents(IERunGroup aOldRunGroup, IERunGroup aNewRunGroup, IECase aCase) {
		if ((aOldRunGroup == null) || (aNewRunGroup == null))
			return aNewRunGroup;
		IRunGroupManager lBean = (IRunGroupManager) getBean("runGroupManager");
//		get old rungroupcasecomponents
		IRunGroupCaseComponentManager lBean2 = getRunGroupCaseComponentManager();
		List<IERunGroupCaseComponent> lOldRunGroupCaseComponents = lBean2
				.getAllRunGroupCaseComponentsByRugId(aOldRunGroup.getRugId());
		for (IERunGroupCaseComponent lOldRunGroupCaseComponent : lOldRunGroupCaseComponents) {
			IERunGroupCaseComponent lNewRunGroupCaseComponent = (IERunGroupCaseComponent) lOldRunGroupCaseComponent
					.clone();
			lNewRunGroupCaseComponent.setRugRugId(aNewRunGroup.getRugId());
//			set to 0 to create new one
			lNewRunGroupCaseComponent.setRgcId(0);
			lBean2.newRunGroupCaseComponent(lNewRunGroupCaseComponent);
//			get dates correct
			lNewRunGroupCaseComponent = lBean2.getRunGroupCaseComponent(lNewRunGroupCaseComponent.getRgcId());
//			get roottag
			IXMLTag lRootTag = getXmlRunGroupStatusTree(lNewRunGroupCaseComponent);
			if (lRootTag != null) {
//				copy existing blobs within xmldata
				copyRunGroupCaseComponentBlobs(lRootTag);
//				save xmldata
				String lXmlData = getXmlManager().xmlTreeToDoc(lRootTag);
				if (lXmlData != null && !lXmlData.equals(""))
					lNewRunGroupCaseComponent.setXmldata(lXmlData);
				lBean2.updateRunGroupCaseComponent(lNewRunGroupCaseComponent);
			}
		}
		return lBean.getRunGroup(aNewRunGroup.getRugId());
	}

	/**
	 * Copies run group case component blobs. Makes copies of all blobs referenced
	 * within aRootTag and changes the references.
	 *
	 * @param aRootTag the a root tag
	 */
	private void copyRunGroupCaseComponentBlobs(IXMLTag aRootTag) {
		List<IXMLTag> lAllTags = getPropCScript().getAllTags(aRootTag);
		copyBlobTags(lAllTags);
	}

	/**
	 * Gets the run group accounts within run.
	 *
	 * @return the run group accounts within run
	 */
	public List<IERunGroupAccount> getRunGroupAccountsWithinRun() {
		List<IERunGroupAccount> lResult = new ArrayList<IERunGroupAccount>();
		if (getRunGroupAccount() == null)
			return lResult;
		int lRunId = getRunGroupAccount().getERunGroup().getERun().getRunId();
		List<IERunGroup> lRunGroups = ((IRunGroupManager) getBean("runGroupManager")).getAllRunGroupsByRunId(lRunId);
		List<Integer> rugIds = new ArrayList<Integer>();
		for (IERunGroup lRunGroup : lRunGroups) {
			if (!rugIds.contains(lRunGroup.getRugId()))
				rugIds.add(lRunGroup.getRugId());
		}
		return ((IRunGroupAccountManager) getBean("runGroupAccountManager")).getAllRunGroupAccountsByRugIds(rugIds);
	}

	/**
	 * Gets the run group accounts within current runteam.
	 *
	 * @return the run group accounts within runteam
	 */
	public List<IERunGroupAccount> getRunGroupAccountsWithinRunTeam() {
		List<IERunGroupAccount> lResult = new ArrayList<IERunGroupAccount>();
		if (getRunTeam() == null)
			return lResult;
		int lRutId = getRunTeam().getRutId();
		List<IERunTeamRunGroup> lRunTeamRunGroups = ((IRunTeamRunGroupManager) getBean("runTeamRunGroupManager"))
				.getAllRunTeamRunGroupsByRutId(lRutId);
		List<Integer> rugIds = new ArrayList<Integer>();
		for (IERunTeamRunGroup lRunTeamRunGroup : lRunTeamRunGroups) {
			if (!rugIds.contains(lRunTeamRunGroup.getRugRugId()))
				rugIds.add(lRunTeamRunGroup.getRugRugId());
		}
		return ((IRunGroupAccountManager) getBean("runGroupAccountManager")).getAllRunGroupAccountsByRugIds(rugIds);
	}

	/**
	 * Gets the all active run group accounts, saved in application memory. All
	 * accounts for whom the Emergo player is playing, for all runs.
	 *
	 * @return the all active run group accounts
	 */
	public Hashtable<String, IERunGroupAccount> getAppPropAllActiveRunGroupAccounts() {
		if (getAppManager().getAppContainer().get("activeRunGroupAccounts") == null)
			getAppManager().getAppContainer().put("activeRunGroupAccounts",
					new Hashtable<String, IERunGroupAccount>(0));
		return (Hashtable) getAppManager().getAppContainer().get("activeRunGroupAccounts");
	}

	/**
	 * Is current run group account added to the list of run group accounts in
	 * application memory?
	 *
	 * @return boolean
	 */
	public boolean isRunGroupAccountAddedToActiveRunGroupAccounts() {
		IERunGroupAccount lRunGroupAccount = getRunGroupAccount();
		if (lRunGroupAccount == null)
			return false;
		Hashtable<String, IERunGroupAccount> lActiveRunGroupAccounts = getAppPropAllActiveRunGroupAccounts();
		return lActiveRunGroupAccounts.containsKey("" + lRunGroupAccount.getRgaId());
	}

	/**
	 * Adds current run group account to the list of run group accounts in
	 * application memory.
	 */
	public void addRunGroupAccountToActiveRunGroupAccounts() {
		IERunGroupAccount lRunGroupAccount = getRunGroupAccount();
		if (lRunGroupAccount == null)
			return;
		if (!isRunGroupAccountAddedToActiveRunGroupAccounts()) {
			// if not already added, add it
			Hashtable<String, IERunGroupAccount> lActiveRunGroupAccounts = getAppPropAllActiveRunGroupAccounts();
			lActiveRunGroupAccounts.put("" + lRunGroupAccount.getRgaId(), lRunGroupAccount);
		}
	}

	/**
	 * Removes current run group account from the list of run group accounts in
	 * application memory.
	 */
	public void removeRunGroupAccountFromActiveRunGroupAccounts() {
		IERunGroupAccount lRunGroupAccount = getRunGroupAccount();
		if (lRunGroupAccount == null)
			return;
		Hashtable<String, IERunGroupAccount> lActiveRunGroupAccounts = getAppPropAllActiveRunGroupAccounts();
		if (lActiveRunGroupAccounts.containsKey("" + lRunGroupAccount.getRgaId())) {
			lActiveRunGroupAccounts.remove("" + lRunGroupAccount.getRgaId());
//			also remove pending external data updates for current run group account
			Hashtable<String, List<Hashtable<String, Object>>> lExternalDataUpdates = getSUpdateHelper()
					.getAppPropAllExternalDataUpdates();
			if (lExternalDataUpdates.containsKey("" + lRunGroupAccount.getRgaId()))
				lExternalDataUpdates.remove("" + lRunGroupAccount.getRgaId());
		}
		// NOTE If no rga's anymore use the current case, remove case components from
		// application memory
		if (getActiveRunGroupAccountsWithinCase().size() == 0) {
			String lCasId = "" + lRunGroupAccount.getERunGroup().getERun().getECase().getCasId();
			List<IECaseComponent> lCaseComponentsInRunByCasId = getCaseComponentsInRunByCasId(lCasId);
			for (IECaseComponent lCaseComponent : lCaseComponentsInRunByCasId) {
				// NOTE Remove from application memory
				String lCacId = "" + lCaseComponent.getCacId();
				Hashtable<String, IXMLTag> lXmlDataTreeByCacId = getAppPropXmlDataTreeByCacId();
				if ((lXmlDataTreeByCacId != null) && (lXmlDataTreeByCacId.containsKey(lCacId))) {
					lXmlDataTreeByCacId.remove(lCacId);
				}
			}
			removeCaseComponentsInRunByCasId(lCasId);
		}
	}

	/**
	 * Gets the active run group accounts, saved in application memory. All accounts
	 * for whom the Emergo player is playing.
	 *
	 * @return the active run group accounts
	 */
	public List<IERunGroupAccount> getActiveRunGroupAccounts() {
		Hashtable<String, IERunGroupAccount> lActiveRunGroupAccounts = getAppPropAllActiveRunGroupAccounts();
		List<IERunGroupAccount> lRunGroupAccounts = new ArrayList<IERunGroupAccount>(0);
		for (Enumeration<String> keys = lActiveRunGroupAccounts.keys(); keys.hasMoreElements();) {
			String key = keys.nextElement();
			IERunGroupAccount lRunGroupAccount = lActiveRunGroupAccounts.get(key);
			lRunGroupAccounts.add(lRunGroupAccount);
		}
		return lRunGroupAccounts;
	}

	/**
	 * Gets the active run group accounts within run, saved in application memory.
	 * All accounts for whom the Emergo player is playing, for the current run.
	 *
	 * @return the active run group accounts within run
	 */
	public List<IERunGroupAccount> getActiveRunGroupAccountsWithinRun() {
		IERunGroupAccount lRunGroupAccount = getRunGroupAccount();
		if (lRunGroupAccount == null)
			return new ArrayList<IERunGroupAccount>(0);
		int lRunId = lRunGroupAccount.getERunGroup().getERun().getRunId();
		Hashtable<String, IERunGroupAccount> lActiveRunGroupAccounts = getAppPropAllActiveRunGroupAccounts();
		List<IERunGroupAccount> lRunGroupAccounts = new ArrayList<IERunGroupAccount>(0);
		for (Enumeration<String> keys = lActiveRunGroupAccounts.keys(); keys.hasMoreElements();) {
			String key = keys.nextElement();
			lRunGroupAccount = lActiveRunGroupAccounts.get(key);
			if (lRunGroupAccount.getERunGroup().getERun().getRunId() == lRunId)
				lRunGroupAccounts.add(lRunGroupAccount);
		}
		return lRunGroupAccounts;
	}

	/**
	 * Gets the active run group accounts within case, saved in application memory.
	 * All accounts for whom the Emergo player is playing, for the current case.
	 *
	 * @return the active run group accounts within case
	 */
	public List<IERunGroupAccount> getActiveRunGroupAccountsWithinCase() {
		IERunGroupAccount lRunGroupAccount = getRunGroupAccount();
		if (lRunGroupAccount == null)
			return new ArrayList<IERunGroupAccount>(0);
		int lCasId = lRunGroupAccount.getERunGroup().getERun().getECase().getCasId();
		Hashtable<String, IERunGroupAccount> lActiveRunGroupAccounts = getAppPropAllActiveRunGroupAccounts();
		List<IERunGroupAccount> lRunGroupAccounts = new ArrayList<IERunGroupAccount>(0);
		for (Enumeration<String> keys = lActiveRunGroupAccounts.keys(); keys.hasMoreElements();) {
			String key = keys.nextElement();
			lRunGroupAccount = lActiveRunGroupAccounts.get(key);
			if (lRunGroupAccount.getERunGroup().getERun().getECase().getCasId() == lCasId)
				lRunGroupAccounts.add(lRunGroupAccount);
		}
		return lRunGroupAccounts;
	}

	/**
	 * Gets the active run group accounts within current runteam, saved in
	 * application memory. All accounts for whom the Emergo player is playing, for
	 * the current runteam.
	 *
	 * @return the active run group accounts within runteam
	 */
	public List<IERunGroupAccount> getActiveRunGroupAccountsWithinRunTeam() {
		IERunTeam lRunTeam = getRunTeam();
		if (lRunTeam == null)
			return new ArrayList<IERunGroupAccount>(0);
		int lRutId = lRunTeam.getRutId();
		Hashtable<String, IERunGroupAccount> lActiveRunGroupAccounts = getAppPropAllActiveRunGroupAccounts();
		List<IERunGroupAccount> lRunGroupAccounts = new ArrayList<IERunGroupAccount>(0);
		for (Enumeration<String> keys = lActiveRunGroupAccounts.keys(); keys.hasMoreElements();) {
			String key = keys.nextElement();
			IERunGroupAccount lRunGroupAccount = lActiveRunGroupAccounts.get(key);
			List<IERunTeamRunGroup> lRunTeamRunGroups = ((IRunTeamRunGroupManager) getBean("runTeamRunGroupManager"))
					.getAllRunTeamRunGroupsByRutIdRugId(lRutId, lRunGroupAccount.getERunGroup().getRugId());
			if (lRunTeamRunGroups.size() > 0 && !lRunGroupAccounts.contains(lRunGroupAccount))
				lRunGroupAccounts.add(lRunGroupAccount);
		}
		return lRunGroupAccounts;
	}

	/**
	 * Gets the active run group names, saved in application memory. All names of
	 * accounts for whom the Emergo player is playing.
	 *
	 * @return the active run group names
	 */
	public String getActiveRunGroupNames() {
		List<IERunGroupAccount> lRunGroupAccounts = getActiveRunGroupAccounts();
		String lNames = "";
		for (IERunGroupAccount lRunGroupAccount : lRunGroupAccounts) {
			if (!lNames.equals(""))
				lNames = lNames + ", ";
			lNames = lNames + lRunGroupAccount.getERunGroup().getName();
		}
		return lNames;
	}

	/**
	 * Gets the active run group names within run, saved in application memory. All
	 * names of accounts for whom the Emergo player is playing, for the current run.
	 *
	 * @return the active run group names within run
	 */
	public String getActiveRunGroupNamesWithinRun() {
		List<IERunGroupAccount> lRunGroupAccounts = getActiveRunGroupAccountsWithinRun();
		String lNames = "";
		for (IERunGroupAccount lRunGroupAccount : lRunGroupAccounts) {
			if (!lNames.equals(""))
				lNames = lNames + ", ";
			lNames = lNames + lRunGroupAccount.getERunGroup().getName();
		}
		return lNames;
	}

	/**
	 * Gets the all active run windows, saved in application memory. All Emergo
	 * players.
	 *
	 * @return the all active run windows
	 */
	public Hashtable<String, List<CRunWnd>> getAppPropAllActiveRunWnds() {
		if (getAppManager().getAppContainer().get("activeRunWnds") == null)
			getAppManager().getAppContainer().put("activeRunWnds", new Hashtable<String, List<CRunWnd>>(0));
		return (Hashtable) getAppManager().getAppContainer().get("activeRunWnds");
	}

	/**
	 * Gets the all active run windows per rga id, saved in application memory. All
	 * Emergo players per rga id.
	 *
	 * @param aRgaId the a rga id
	 *
	 * @return the all active run windows per rga id
	 */
	public List<CRunWnd> getAllActiveRunWndsPerRgaId(String aRgaId) {
		Hashtable<String, List<CRunWnd>> lActiveRunWnds = getAppPropAllActiveRunWnds();
		List<CRunWnd> lActiveRunWndsPerRgaId = lActiveRunWnds.get(aRgaId);
		if (lActiveRunWndsPerRgaId == null) {
			lActiveRunWndsPerRgaId = new ArrayList<CRunWnd>();
			lActiveRunWnds.put(aRgaId, lActiveRunWndsPerRgaId);
		}
		return lActiveRunWndsPerRgaId;
	}

	/**
	 * Adds current run wnd to the list of run windows in application memory.
	 */
	public void addRunWndToActiveRunWndsPerRgaId() {
		CRunWnd lRunWnd = getPropRunWnd();
		if (lRunWnd == null)
			return;
		IERunGroupAccount lRunGroupAccount = getRunGroupAccount();
		if (lRunGroupAccount == null)
			return;
		String lKey = "" + lRunGroupAccount.getRgaId();
		if (getAllActiveRunWndsPerRgaId(lKey).contains(lRunWnd)) {
			// position refreshed window as last in the list, so window with most up to date
			// state is last one
			getAllActiveRunWndsPerRgaId(lKey).remove(lRunWnd);
		}
		getAllActiveRunWndsPerRgaId(lKey).add(lRunWnd);
	}

	/**
	 * Removes current run wnd from the list of run windows in application memory.
	 */
	public void removeRunWndFromActiveRunWndsPerRgaId() {
		CRunWnd lRunWnd = getPropRunWnd();
		if (lRunWnd == null)
			return;
		IERunGroupAccount lRunGroupAccount = getRunGroupAccount();
		if (lRunGroupAccount == null)
			return;
		String lKey = "" + lRunGroupAccount.getRgaId();
		if (getAllActiveRunWndsPerRgaId(lKey).contains(lRunWnd)) {
			removeExternalStatusChangesPerRgaIdAndDesktopId(lRunWnd.rgaId, lRunWnd.desktopId);
			getAllActiveRunWndsPerRgaId(lKey).remove(lRunWnd);
		}
	}

	/**
	 * Gets the all external status changes, saved in application memory.
	 *
	 * @return the all external status changes
	 */
	public Hashtable<String, List<CRunStatusChange>> getAppPropAllExternalStatusChanges() {
		if (getAppManager().getAppContainer().get("externalStatusChanges") == null)
			getAppManager().getAppContainer().put("externalStatusChanges",
					new Hashtable<String, List<CRunStatusChange>>(0));
		return (Hashtable) getAppManager().getAppContainer().get("externalStatusChanges");
	}

	/**
	 * Gets the external status changes per rga id and desktop id, saved in
	 * application memory.
	 *
	 * @param aRgaId     the a rga id
	 * @param aDesktopId the a desktop id
	 *
	 * @return the all external status changes per rga id and desktop id
	 */
	public List<CRunStatusChange> getExternalStatusChangesPerRgaIdAndDesktopId(String aRgaId, String aDesktopId) {
		Hashtable<String, List<CRunStatusChange>> lAllExternalStatusChanges = getAppPropAllExternalStatusChanges();
		List<CRunStatusChange> lAllExternalStatusChangesPerRgaIdAndDesktopId = lAllExternalStatusChanges
				.get(aRgaId + "_" + aDesktopId);
		if (lAllExternalStatusChangesPerRgaIdAndDesktopId == null) {
			lAllExternalStatusChangesPerRgaIdAndDesktopId = new ArrayList<CRunStatusChange>();
			lAllExternalStatusChanges.put(aRgaId + "_" + aDesktopId, lAllExternalStatusChangesPerRgaIdAndDesktopId);
		}
		return lAllExternalStatusChangesPerRgaIdAndDesktopId;
	}

	/**
	 * Removes the external status changes per rga id and desktop id from
	 * application memory.
	 *
	 * @param aRgaId     the a rga id
	 * @param aDesktopId the a desktop id
	 */
	public void removeExternalStatusChangesPerRgaIdAndDesktopId(String aRgaId, String aDesktopId) {
		Hashtable<String, List<CRunStatusChange>> lAllExternalStatusChanges = getAppPropAllExternalStatusChanges();
		if (lAllExternalStatusChanges.contains(aRgaId + "_" + aDesktopId)) {
			lAllExternalStatusChanges.remove(aRgaId + "_" + aDesktopId);
		}
	}

	/**
	 * Inits skin specific desktop attributes to be used by other code.
	 * 
	 * @param aDesktop the a desktop
	 */
	public void initDesktopAttributes(Desktop aDesktop) {
		if (aDesktop == null)
			return;
		if (aDesktop.getAttribute("initDesktopAttributes") != null) {
			return;
		}

		aDesktop.setAttribute("runWndWidth", getSCaseSkinHelper().getCaseSkinValue(getCase(), "size", 0) + "px");
		aDesktop.setAttribute("runWndHeight", getSCaseSkinHelper().getCaseSkinValue(getCase(), "size", 1) + "px");
		aDesktop.setAttribute("browserWidth", getSCaseSkinHelper().getCaseSkinValue(getCase(), "browser.size", 0));
		aDesktop.setAttribute("browserHeight", getSCaseSkinHelper().getCaseSkinValue(getCase(), "browser.size", 1));

		aDesktop.setAttribute("flashMemosLeft",
				getSCaseSkinHelper().getCaseSkinValue(getCase(), "flash_memos.position", 0));
		aDesktop.setAttribute("flashMemosTop",
				getSCaseSkinHelper().getCaseSkinValue(getCase(), "flash_memos.position", 1));
		aDesktop.setAttribute("flashMemosMaxWidth",
				getSCaseSkinHelper().getCaseSkinValue(getCase(), "flash_memos.maxsize", 0));
		aDesktop.setAttribute("flashMemosMaxHeight",
				getSCaseSkinHelper().getCaseSkinValue(getCase(), "flash_memos.maxsize", 1));

		aDesktop.setAttribute("flashVideomanualLeft",
				getSCaseSkinHelper().getCaseSkinValue(getCase(), "flash_videomanual.position", 0));
		aDesktop.setAttribute("flashVideomanualTop",
				getSCaseSkinHelper().getCaseSkinValue(getCase(), "flash_videomanual.position", 1));
		aDesktop.setAttribute("flashVideomanualMaxWidth",
				getSCaseSkinHelper().getCaseSkinValue(getCase(), "flash_videomanual.maxsize", 0));
		aDesktop.setAttribute("flashVideomanualMaxHeight",
				getSCaseSkinHelper().getCaseSkinValue(getCase(), "flash_videomanual.maxsize", 1));

		aDesktop.setAttribute("initDesktopAttributes", "true");
	}

	/**
	 * Gets the style img src.
	 *
	 * @param aImgName the a img name
	 *
	 * @return style img src
	 */
	public String getStyleImgSrc(String aImgName) {
		// NOTE first search in specific skin
		String lSkinPath = "";
		if (getCase() != null) {
			lSkinPath = getSCaseSkinHelper().getCaseSkinPath(getCase());
		}
		String lShortImgSrc = getShortImgSrc(aImgName, lSkinPath);
		if (!lShortImgSrc.equals("")) {
			return getStylePath(true, lSkinPath) + lShortImgSrc;
		}
		// NOTE if not found, search in general skin
		String lSkinsPath = VView.getInitParameter("emergo.skins.path");
		lShortImgSrc = getShortImgSrc(aImgName, lSkinsPath);
		if (!lShortImgSrc.equals("")) {
			return getStylePath(false, lSkinsPath) + lShortImgSrc;
		}
		return "";
	}

	private String getStylePath(boolean aSpecificSkin, String aPath) {
		if (getRun() != null) {
			// within player
			if (aSpecificSkin) {
				return VView.getInitParameter("emergo.style.path");
			} else {
				return VView.getInitParameter("emergo.general.style.path");
			}
		} else {
			// within author environment
			return "/" + aPath + VView.getInitParameter("emergo.style.path");
		}
	}

	/**
	 * Gets the short img src.
	 *
	 * @param aImgName the a img name
	 * @param aSubPath the a sub path
	 *
	 * @return short img src
	 */
	protected String getShortImgSrc(String aImgName, String aSubPath) {
		String lDiskPath = getAppManager().getAbsoluteAppPath() + "/" + aSubPath
				+ VView.getInitParameter("emergo.style.path");
		if (getFileManager().fileExists(lDiskPath + aImgName + ".png"))
			return aImgName + ".png";
		else if (getFileManager().fileExists(lDiskPath + aImgName + ".gif"))
			return aImgName + ".gif";
		else if (getFileManager().fileExists(lDiskPath + aImgName + ".jpg"))
			return aImgName + ".jpg";
		return "";
	}

	/**
	 * Is use of component allowed for current skin
	 *
	 * @param aComponent the a componenent
	 *
	 * @return boolean
	 */
	public boolean isComponentAllowed(IEComponent aComponent) {
		String lSupportedComponents = VView.getInitParameter(
				"emergo.skin." + getSCaseSkinHelper().getCaseSkin(getCase()) + ".components.supported");
		if (lSupportedComponents.length() > 0) {
			String[] lComponentCodes = lSupportedComponents.split(",");
			for (String lComponentCode : lComponentCodes) {
				if (lComponentCode.equals(aComponent.getCode())) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Gets the multiple value of a component for current skin. Multiple value can
	 * be overwritten per skin within web.xml.
	 *
	 * @param aComponent the a componenent
	 *
	 * @return the multiple value
	 */
	public boolean getComponentMultiple(IEComponent aComponent) {
		boolean lMultiple = aComponent.getMultiple();
		if (!lMultiple) {
			if (isComponentMultipleOverwritten(aComponent, AppConstants.statusValueTrue)) {
				// if component should have multiple true instead of false
				return true;
			}
		} else {
			if (isComponentMultipleOverwritten(aComponent, AppConstants.statusValueFalse)) {
				// if component should have multiple false instead of true
				return true;
			}
		}
		return lMultiple;
	}

	/**
	 * Is multiple value of component overwritten for current skin in web.xml
	 *
	 * @param aComponent   the a componenent
	 * @param aTrueOrFalse the a true or false string
	 *
	 * @return boolean
	 */
	protected boolean isComponentMultipleOverwritten(IEComponent aComponent, String aTrueOrFalse) {
		String lComponents = VView.getInitParameter(
				"emergo.skin." + getSCaseSkinHelper().getCaseSkin(getCase()) + ".components.multiple." + aTrueOrFalse);
		if (lComponents.length() > 0) {
			String[] lComponentCodes = lComponents.split(",");
			for (String lComponentCode : lComponentCodes) {
				if (lComponentCode.equals(aComponent.getCode())) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Escapes string.
	 *
	 * @param aString the a string
	 *
	 * @return the string
	 */
	public String escapeXML(String aString) {
		return getXmlManager().escapeXML(aString);
	}

	/**
	 * Escapes string using apache.commons.text.StringEscapeUtils.
	 *
	 * @param aString the a string
	 *
	 * @return the string
	 */
	public String escapeXMLAlt(String aString) {
		return getXmlManager().escapeXMLAlt(aString);
	}

	/**
	 * Unescapes string. If used within player also replaces state keys within a
	 * string by their current state values.
	 *
	 * @param aString the a string
	 *
	 * @return the string
	 */
	public String unescapeXML(String aString) {
		aString = getXmlManager().unescapeXML(aString);
		if (getRun() != null) {
			aString = replaceVariablesWithinString(aString);
		}
		return aString;
	}

	/**
	 * Unescapes string using apache.commons.text.StringEscapeUtils. If used within player also replaces state keys within a
	 * string by their current state values.
	 *
	 * @param aString the a string
	 *
	 * @return the string
	 */
	public String unescapeXMLAlt(String aString) {
		aString = getXmlManager().unescapeXMLAlt(aString);
		if (getRun() != null) {
			aString = replaceVariablesWithinString(aString);
		}
		return aString;
	}

	/**
	 * Unescapes string without replacing state keys. Sometimes runwindow is/has
	 * been shown, so run exists, but case developer must see state keys in input
	 * elements.
	 *
	 * @param aString the a string
	 *
	 * @return the string
	 */
	public String unescapeXMLWithoutReplace(String aString) {
		aString = getXmlManager().unescapeXML(aString);
		return aString;
	}

	/**
	 * Replaces variables within a string by their current state values.
	 *
	 * @param aString the a string
	 *
	 * @return the string
	 */
	public String replaceVariablesWithinString(String aString) {
		return getSReplaceVariablesWithinStringHelper().replaceVariablesWithinString(aString);
	}

	/**
	 * Inits replacing temporary variables within a number of strings.
	 */
	public void initReplaceTemporaryVarsWithinStrings() {
		getSReplaceVariablesWithinStringHelper().initReplaceTemporaryVarsWithinStrings();
	}

	/**
	 * If replacing temporary variables within a number of strings is done.
	 * 
	 * @param aRollback the a roll back
	 */
	public void doneReplaceTemporaryVarsWithinStrings(boolean aRollback) {
		getSReplaceVariablesWithinStringHelper().doneReplaceTemporaryVarsWithinStrings(aRollback);
	}

	/**
	 * Replaces temporary variables within a number of strings.
	 *
	 * @param aStrings         the a strings
	 * @param aTriggeredTagKey the triggered tag key
	 *
	 * @return list of string
	 */
	public List<String> replaceTemporaryVarsWithinStrings(List<String> aStrings, String aTriggeredTagKey) {
		return getSReplaceVariablesWithinStringHelper().replaceTemporaryVarsWithinStrings(aStrings, aTriggeredTagKey);
	}

	/**
	 * Replaces matches within a number of strings.
	 *
	 * @param aStrings         the a strings
	 * @param aTriggeredTagKey the triggered tag key
	 *
	 * @return list of string
	 */
	public List<String> replaceMatchesWithinStrings(List<String> aStrings, String aTriggeredTagKey) {
		return getSReplaceVariablesWithinStringHelper().replaceMatchesWithinStrings(aStrings, aTriggeredTagKey);
	}

	/**
	 * Replaces temporary variables within a string.
	 *
	 * @param aString          the a string
	 * @param aTriggeredTagKey the triggered tag key
	 *
	 * @return string
	 */
	public String replaceTemporaryVarsWithinString(String aString, String aTriggeredTagKey) {
		return getSReplaceVariablesWithinStringHelper().replaceTemporaryVarsWithinString(aString, aTriggeredTagKey);
	}

	/**
	 * Gets the current run component status. The last value saved.
	 *
	 * @param aCaseComponentName the a case component name
	 * @param aStatusKey         the a status key
	 * @param aStatusType        the a status type
	 * @param aErrors            the a errors
	 *
	 * @return the current run tag status
	 */
	public String getCurrentRunComponentStatus(String aCaseComponentName, String aStatusKey, String aStatusType,
			List<String> aErrors) {
		return getSGetAndSetStatusByStringIdsHelper().getCurrentRunComponentStatus(aCaseComponentName, aStatusKey,
				aStatusType, aErrors);
	}

	/**
	 * Gets the current run tag status. The last value saved.
	 *
	 * @param aCaseComponentName the a case component name
	 * @param aTagName           the a tag name
	 * @param aTagKey            the a tag key
	 * @param aStatusKey         the a status key
	 * @param aStatusType        the a status type
	 * @param aErrors            the a errors
	 *
	 * @return the current run tag status
	 */
	public String getCurrentRunTagStatus(String aCaseComponentName, String aTagName, String aTagKey, String aStatusKey,
			String aStatusType, List<String> aErrors) {
		return getSGetAndSetStatusByStringIdsHelper().getCurrentRunTagStatus(aCaseComponentName, aTagName, aTagKey,
				aStatusKey, aStatusType, aErrors);
	}

	/**
	 * Gets the current run tag status. The last value saved.
	 *
	 * @param aCaseComponent the a case component
	 * @param aTagName       the a tag name
	 * @param aTagKey        the a tag key
	 * @param aStatusKey     the a status key
	 * @param aStatusType    the a status type
	 * @param aErrors        the a errors
	 *
	 * @return the current run tag status
	 */
	public String getCurrentRunTagStatus(IECaseComponent aCaseComponent, String aTagName, String aTagKey,
			String aStatusKey, String aStatusType, List<String> aErrors) {
		return getSGetAndSetStatusByStringIdsHelper().getCurrentRunTagStatus(aCaseComponent, aTagName, aTagKey,
				aStatusKey, aStatusType, aErrors);
	}

	/**
	 * Gets the current run tag status time. The last value saved time.
	 *
	 * @param aCaseComponentName the a case component name
	 * @param aTagName           the a tag name
	 * @param aTagKey            the a tag key
	 * @param aStatusKey         the a status key
	 * @param aStatusType        the a status type
	 * @param aErrors            the a errors
	 *
	 * @return the current run tag status time
	 */
	public double getCurrentRunTagStatusTime(String aCaseComponentName, String aTagName, String aTagKey,
			String aStatusKey, String aStatusType, List<String> aErrors) {
		return getSGetAndSetStatusByStringIdsHelper().getCurrentRunTagStatusTime(aCaseComponentName, aTagName, aTagKey,
				aStatusKey, aStatusType, aErrors);
	}

	/**
	 * Gets multiple current run tag status. The last values saved.
	 *
	 * @param aStateData  the a state date
	 * @param aStatusType the a status type
	 * @param aErrors     the a errors
	 *
	 * @return the current run tag status values
	 */
	public String[] getMultipleCurrentRunTagStatus(List<String[]> aStateData, String aStatusType,
			List<String> aErrors) {
		return getSGetAndSetStatusByStringIdsHelper().getMultipleCurrentRunTagStatus(aStateData, aStatusType, aErrors);
	}

	/**
	 * Gets multiple current run tag status time. The last values saved time.
	 *
	 * @param aStateData  the a state date
	 * @param aStatusType the a status type
	 * @param aErrors     the a errors
	 *
	 * @return the current run tag status values time
	 */
	public double[] getMultipleCurrentRunTagStatusTime(List<String[]> aStateData, String aStatusType,
			List<String> aErrors) {
		return getSGetAndSetStatusByStringIdsHelper().getMultipleCurrentRunTagStatusTime(aStateData, aStatusType,
				aErrors);
	}

	/**
	 * Sets the current run component status.
	 *
	 * @param aCaseComponentName the a case component name
	 * @param aStatusKey         the a status key
	 * @param aStatusValue       the a status value
	 * @param aStatusType        the a status type
	 * @param aCheckScript       the a check script
	 * @param aErrors            the a errors
	 */
	public void setCurrentRunComponentStatus(String aCaseComponentName, String aStatusKey, String aStatusValue,
			String aStatusType, boolean aCheckScript, List<String> aErrors) {
		getSGetAndSetStatusByStringIdsHelper().setCurrentRunComponentStatus(aCaseComponentName, aStatusKey,
				aStatusValue, aStatusType, aCheckScript, aErrors);
	}

	/**
	 * Sets the current run tag status.
	 *
	 * @param aCaseComponentName the a case component name
	 * @param aTagName           the a tag name
	 * @param aTagKey            the a tag key
	 * @param aStatusKey         the a status key
	 * @param aStatusValue       the a status value
	 * @param aStatusType        the a status type
	 * @param aCheckScript       the a check script
	 * @param aErrors            the a errors
	 */
	public void setCurrentRunTagStatus(String aCaseComponentName, String aTagName, String aTagKey, String aStatusKey,
			String aStatusValue, String aStatusType, boolean aCheckScript, List<String> aErrors) {
		getSGetAndSetStatusByStringIdsHelper().setCurrentRunTagStatus(aCaseComponentName, aTagName, aTagKey, aStatusKey,
				aStatusValue, aStatusType, aCheckScript, aErrors);
	}

	/**
	 * Sets the current run tag status.
	 *
	 * @param aCaseComponent the a case component
	 * @param aTagName       the a tag name
	 * @param aTagKey        the a tag key
	 * @param aStatusKey     the a status key
	 * @param aStatusValue   the a status value
	 * @param aStatusType    the a status type
	 * @param aCheckScript   the a check script
	 * @param aErrors        the a errors
	 */
	public void setCurrentRunTagStatus(IECaseComponent aCaseComponent, String aTagName, String aTagKey,
			String aStatusKey, String aStatusValue, String aStatusType, boolean aCheckScript, List<String> aErrors) {
		getSGetAndSetStatusByStringIdsHelper().setCurrentRunTagStatus(aCaseComponent, aTagName, aTagKey, aStatusKey,
				aStatusValue, aStatusType, aCheckScript, aErrors);
	}

}