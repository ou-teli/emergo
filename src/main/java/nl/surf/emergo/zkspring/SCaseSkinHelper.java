/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.zkspring;

import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.view.VView;

public class SCaseSkinHelper {

	protected SSpring sSpring;
	
	public SCaseSkinHelper(SSpring sSpring) {
		this.sSpring = sSpring;
	}

	/**
	 * Gets the corresponding case skin.
	 *
	 * @param aCase the a case
	 *
	 * @return skin
	 */
	public String getCaseSkin(IECase aCase) {
		String lDefaultSkinNumber = VView.getInitParameter("emergo.default.skinnumber");
		if (lDefaultSkinNumber.equals("")) {
			//6 = ounl2 skin
			lDefaultSkinNumber = "6";
		}
		String lDefaultSkinId = VView.getInitParameter("emergo.skin." + Integer.parseInt(lDefaultSkinNumber));
		if (aCase == null) {
			return lDefaultSkinId;
		}
		String lSkinId = aCase.getSkin();
		if (lSkinId == null || lSkinId.equals("")) {
			lSkinId = lDefaultSkinId;
			aCase.setSkin(lSkinId);
		}
		return lSkinId;
	}

	/**
	 * Gets the case skin path.
	 *
	 * @param aRunGroup the a run
	 *
	 * @return skin path
	 */
	public String getCaseSkinPath(IERun aRun) {
		return getCaseSkinPath(aRun.getECase());
	}

	/**
	 * Gets the case skin path.
	 *
	 * @param aRunGroup the a run group
	 *
	 * @return skin path
	 */
	public String getCaseSkinPath(IERunGroup aRunGroup) {
		return getCaseSkinPath(aRunGroup.getERun().getECase());
	}

	/**
	 * Gets the case skin path.
	 *
	 * @param aRunGroupAccount the a run group account
	 *
	 * @return skin path
	 */
	public String getCaseSkinPath(IERunGroupAccount aRunGroupAccount) {
		return getCaseSkinPath(aRunGroupAccount.getERunGroup().getERun().getECase());
	}

	/**
	 * Gets the case skin path.
	 *
	 * @param aCase the a case
	 *
	 * @return skin path
	 */
	public String getCaseSkinPath(IECase aCase) {
		return VView.getInitParameter("emergo.skin." + getCaseSkin(aCase) + ".path");
	}

	/**
	 * Gets the case skin width.
	 *
	 * @param aCase the a case
	 *
	 * @return skin width
	 */
	public int getCaseSkinWidth(IECase aCase) {
		int lValue = getCaseSkinSizeValue(aCase, 0);
		if (lValue == -1) {
			return 990;
		}
		return lValue;
	}

	/**
	 * Gets the case skin height.
	 *
	 * @param aCase the a case
	 *
	 * @return skin height
	 */
	public int getCaseSkinHeight(IECase aCase) {
		int lValue = getCaseSkinSizeValue(aCase, 1);
		if (lValue == -1) {
			return 610;
		}
		return lValue;
	}

	/**
	 * Gets the case skin size value.
	 *
	 * @param aCase the a case
	 * @param aIndex the a index
	 *
	 * @return skin width
	 */
	public int getCaseSkinSizeValue(IECase aCase, int aIndex) {
		return stringToInt(getCaseSkinValue(aCase, "size", aIndex));
	}

	/**
	 * Convert string to int.
	 *
	 * @param aValue the a value
	 *
	 * @return int
	 */
	protected int stringToInt(String aValue) {
		if (aValue.equals("")) {
			return -1;
		}
		int lInt = 0;
		try {
			lInt = Integer.parseInt(aValue);
		} catch (NumberFormatException e) {
			return -1;
		}
		return lInt;
	}

	/**
	 * Gets the case skin value.
	 *
	 * @param aCase the a case
	 * @param aKey the a key
	 * @param aIndex the a index
	 *
	 * @return value
	 */
	public String getCaseSkinValue(IECase aCase, String aKey, int aIndex) {
		String lValues = VView.getInitParameter("emergo.skin." + getCaseSkin(aCase) + "." + aKey);
		if (lValues.equals("")) {
			return "";
		}
		String[] lArr = lValues.split(",");
		if (aIndex >= lArr.length) {
			return "";
		}
		return lArr[aIndex];
	}
	
}
