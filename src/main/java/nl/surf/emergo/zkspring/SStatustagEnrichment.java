/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.zkspring;

import java.util.Hashtable;
import java.util.List;

import nl.surf.emergo.business.IXMLTag;

/**
 * The Class SStatustagEnrichment, is used to enrich a status tag with attributes, child tags or status child tags.
 */
public class SStatustagEnrichment {

	/** The attributes. */
	protected Hashtable<String,String> attributes = null;

	/** The child tags. */
	protected List<IXMLTag> childTags = null;

	/** The status child tags. */
	protected List<IXMLTag> statusChildTags = null;

	public Hashtable<String, String> getAttributes() {
		return attributes;
	}

	public void setAttributes(Hashtable<String, String> attributes) {
		this.attributes = attributes;
	}

	public List<IXMLTag> getChildTags() {
		return childTags;
	}

	public void setChildTags(List<IXMLTag> childTags) {
		this.childTags = childTags;
	}

	public List<IXMLTag> getStatusChildTags() {
		return statusChildTags;
	}

	public void setStatusChildTags(List<IXMLTag> statusChildTags) {
		this.statusChildTags = statusChildTags;
	}

}
