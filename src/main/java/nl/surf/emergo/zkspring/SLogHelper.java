/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.zkspring;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import nl.surf.emergo.control.run.CRunWnd;

public class SLogHelper {

	private static final Logger _log = LogManager.getLogger(SSpring.class);

	protected SSpring sSpring;
	
	protected String currentRunStatus;

	protected String currentRgaIdStr;

	protected String currentDesktopIdStr;

	private long previousTime = -1;
	
	public int logIndent = 0;
	
	public SLogHelper(SSpring sSpring) {
		this.sSpring = sSpring;
	}

	/**
	 * Log get current run status.
	 *
	 * @return run status str
	 */
	protected String logGetCurrentRunStatus() {
		if (currentRunStatus == null) {
			if (sSpring.getPropRunStatus() != null && !sSpring.getPropRunStatus().equals("")) {
				currentRunStatus = sSpring.getPropRunStatus(); 
			}
			else {
				currentRunStatus = "unknown";
			}
		}
		return currentRunStatus;
	}

	/**
	 * Log get current rga id str.
	 *
	 * @return id str
	 */
	protected String logGetCurrentRgaIdStr() {
		if (currentRgaIdStr == null) {
			if (sSpring.getRunGroupAccount() != null) {
				currentRgaIdStr = "" + sSpring.getRunGroupAccount().getRgaId(); 
			}
			else {
				currentRgaIdStr = "unknown";
			}
		}
		return currentRgaIdStr;
	}

	/**
	 * Log get current desktop id str.
	 *
	 * @return id str
	 */
	protected String logGetCurrentDesktopIdStr() {
		if (StringUtils.isEmpty(currentDesktopIdStr)) {
			if (sSpring.getPropRunWnd() != null) {
				currentDesktopIdStr = sSpring.getPropRunWnd().desktopId; 
			}
			else if (sSpring.getRunGroupAccount() != null) {
				// NOTE if SSpring is used by web services, no run wnd is set within SSpring,
				// so get desktop id from memory, if rga who is using web service has started player too. 
				List<CRunWnd> lRunWnds = sSpring.getAllActiveRunWndsPerRgaId("" + sSpring.getRunGroupAccount().getRgaId());
				if ((lRunWnds != null) && (lRunWnds.size() > 0)) {
					currentDesktopIdStr = lRunWnds.get(lRunWnds.size() - 1).desktopId;
				}
			}
			else {
				currentDesktopIdStr = "player not loaded";
			}
		}
		return currentDesktopIdStr;
	}
	
	private void appendLogIndent(StringBuffer aStringBuffer, String aStatusType) {
		aStringBuffer.append(logGetCurrentRunStatus());
		aStringBuffer.append(" - ");
		aStringBuffer.append("(");
		if (!aStatusType.equals("")) {
			aStringBuffer.append(aStatusType);
			aStringBuffer.append(":");
		}
		aStringBuffer.append("rgaid:");
		aStringBuffer.append(logGetCurrentRgaIdStr());
		aStringBuffer.append(":desktopid:");
		aStringBuffer.append(logGetCurrentDesktopIdStr());
		aStringBuffer.append(") ");
		for (int i=0;i<logIndent;i++) {
			aStringBuffer.append("\t");
		}
	}

	private void appendAction(StringBuffer aStringBuffer, String aAction) {
		aStringBuffer.append(aAction);
		aStringBuffer.append(": ");
	}

	private void appendCaseTimeAndFreeMemoryAndTime(StringBuffer aStringBuffer) {
		aStringBuffer.append("Case time:");
		aStringBuffer.append(sSpring.getCaseTime());
		aStringBuffer.append(":");
		aStringBuffer.append("Free memory:");
		aStringBuffer.append(Runtime.getRuntime().freeMemory());
		aStringBuffer.append(":");
		appendTime(aStringBuffer);
	}

	private void appendTime(StringBuffer aStringBuffer) {
		long time = System.currentTimeMillis();
		if (previousTime == -1) {
			previousTime = time;
			time = 0;
		}
		else {
			time = time - previousTime;
		}
		aStringBuffer.append(new Date());
		aStringBuffer.append(":");
		aStringBuffer.append(time);
		aStringBuffer.append(" ms");
	}

	/**
	 * Logs action.
	 *
	 * @param aAction the a action
	 * @param aStatusType the a status type
	 * @param aCaseComponentName the a case component name
	 * @param aTagName the a tag name
	 * @param aTagPid the a tag pid
	 * @param aStatusKey the a status key
	 * @param aStatusValue the a status value
	 * @param aCheckScript the a check script
	 */
	public void logSetRunTagAction(String aAction, String aStatusType, String aCaseComponentName, String aTagName,
			String aTagPid, String aStatusKey, String aStatusValue, boolean aCheckScript) {
		StringBuffer lStringBuffer = new StringBuffer(256);
		appendLogIndent(lStringBuffer, aStatusType);
		appendAction(lStringBuffer, aAction);

		lStringBuffer.append(aCaseComponentName);
		lStringBuffer.append(":");
		lStringBuffer.append(aTagName);
		lStringBuffer.append(":");
		lStringBuffer.append(aTagPid);
		lStringBuffer.append(":");
		lStringBuffer.append(aStatusKey);
		lStringBuffer.append(":");
		lStringBuffer.append(aStatusValue);
		lStringBuffer.append(":");
		lStringBuffer.append("check script:");
		lStringBuffer.append(aCheckScript);
		lStringBuffer.append(":");
		
		appendCaseTimeAndFreeMemoryAndTime(lStringBuffer);
		_log.info(lStringBuffer);
	}

	/**
	 * Logs action.
	 *
	 * @param aAction the a action
	 * @param aStatusType the a status type
	 * @param aCaseComponentName the a case component name
	 * @param aStatusKey the a status key
	 * @param aStatusValue the a status value
	 * @param aCheckScript the a check script
	 */
	public void logSetRunComponentAction(String aAction, String aStatusType, String aCaseComponentName,
			String aStatusKey, String aStatusValue, boolean aCheckScript) {
		StringBuffer lStringBuffer = new StringBuffer(256);
		appendLogIndent(lStringBuffer, aStatusType);
		appendAction(lStringBuffer, aAction);

		lStringBuffer.append(aCaseComponentName);
		lStringBuffer.append(":");
		lStringBuffer.append(aStatusKey);
		lStringBuffer.append(":");
		lStringBuffer.append(aStatusValue);
		lStringBuffer.append(":");
		lStringBuffer.append("check script:");
		lStringBuffer.append(aCheckScript);
		lStringBuffer.append(":");

		appendCaseTimeAndFreeMemoryAndTime(lStringBuffer);
		_log.info(lStringBuffer);
	}

	/**
	 * Logs action.
	 *
	 * @param aAction the a action
	 * @param aCaseComponentName the a case component name
	 * @param aTagName the a tag name
	 * @param aTagPid the a tag pid
	 */
	public void logSetScriptAction(String aAction, String aCaseComponentName, String aTagName, String aTagPid) {
		StringBuffer lStringBuffer = new StringBuffer(256);
		appendLogIndent(lStringBuffer, "");
		appendAction(lStringBuffer, aAction);

		lStringBuffer.append(aCaseComponentName);lStringBuffer.append(":");
		lStringBuffer.append(aTagName);lStringBuffer.append(":");
		lStringBuffer.append(aTagPid);lStringBuffer.append(":");

		appendCaseTimeAndFreeMemoryAndTime(lStringBuffer);
		_log.info(lStringBuffer);
	}

	/**
	 * Logs action.
	 *
	 * @param aAction the a action
	 * @param aCaseComponentName the a case component name
	 * @param aTagName the a tag name
	 * @param aTagPid the a tag pid
	 */
	public void logSetFeedbackAction(String aAction, String aCaseComponentName, String aTagName, String aTagPid) {
		logSetScriptAction(aAction, aCaseComponentName, aTagName, aTagPid);
	}

	/**
	 * Logs action.
	 *
	 * @param aAction the a action
	 */
	public void logAction(String aAction) {
		StringBuffer lStringBuffer = new StringBuffer(256);
		appendLogIndent(lStringBuffer, "");
		appendAction(lStringBuffer, aAction);

		appendCaseTimeAndFreeMemoryAndTime(lStringBuffer);
		_log.info(lStringBuffer);
	}


	/**
	 * Logs debug message.
	 *
	 * @param aMessage the debug message
	 */
	public void logDebug(String aMessage) {
		StringBuffer lStringBuffer = new StringBuffer(256);
		appendLogIndent(lStringBuffer, "");
		appendAction(lStringBuffer, aMessage);

		appendCaseTimeAndFreeMemoryAndTime(lStringBuffer);
		_log.debug(lStringBuffer);
	}


	/**
	 * Logs client info action.
	 *
	 * @param aAction the a action
	 * @param aClientInfo the a client info
	 */
	public void logClientInfoAction(String aAction, String aClientInfo) {
		StringBuffer lStringBuffer = new StringBuffer(256);
		appendLogIndent(lStringBuffer, "");
		appendAction(lStringBuffer, aAction);

		lStringBuffer.append(aClientInfo);lStringBuffer.append(":");

		appendCaseTimeAndFreeMemoryAndTime(lStringBuffer);
		_log.info(lStringBuffer);
	}

}
