/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.zkspring;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import nl.surf.emergo.business.ICaseRoleManager;
import nl.surf.emergo.domain.IECaseComponentRole;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.view.VView;

public class SCaseRoleHelper {
	
	protected VView vView;
	
	protected SSpring sSpring;
	
	/** The pc case roles. Used to store Emergo player data in memory, for performance. */
	protected List<IECaseRole> pcCaseRoles = null;
	
	/** The npc case roles. Used to store Emergo player data in memory, for performance. */
	protected List<IECaseRole> npcCaseRoles = null;
	
	/** The case role in run by car id. Used to store Emergo player data in memory, for performance. */
	protected Hashtable<String,IECaseRole> caseRoleInRunByCarId = null;

	/** The case roles in run by cac id. Used to store Emergo player data in memory, for performance. */
	protected Hashtable<String,List<IECaseRole>> caseRolesInRunByCacId = null;

	public SCaseRoleHelper(SSpring sSpring) {
		this.sSpring = sSpring;
		this.vView = sSpring.getPropVView();
	}

	/**
	 * Gets the prop pc case roles.
	 *
	 * @return the prop pc case roles
	 */
	protected List<IECaseRole> getPropPcCaseRoles() {
		//NOTE is initialized by setting it
		return pcCaseRoles;
	}

	/**
	 * Sets the prop pc case roles.
	 *
	 * @param aPcCaseRoles the new prop pc case roles
	 */
	protected void setPropPcCaseRoles(List<IECaseRole> aPcCaseRoles) {
		pcCaseRoles = aPcCaseRoles;
	}

	/**
	 * Gets the prop npc case roles.
	 *
	 * @return the prop npc case roles
	 */
	protected List<IECaseRole> getPropNpcCaseRoles() {
		//NOTE is initialized by setting it
		return npcCaseRoles;
	}

	/**
	 * Sets the prop npc case roles.
	 *
	 * @param aNpcCaseRoles the new prop npc case roles
	 */
	protected void setPropNpcCaseRoles(List<IECaseRole> aNpcCaseRoles) {
		npcCaseRoles = aNpcCaseRoles;
	}

	/**
	 * Gets the prop case role in run by car id.
	 *
	 * @return the prop case role in run by car id
	 */
	protected Hashtable<String,IECaseRole> getPropCaseRoleInRunByCarId() {
		if (caseRoleInRunByCarId == null)
			caseRoleInRunByCarId = new Hashtable<String,IECaseRole>(0);
		return caseRoleInRunByCarId;
	}

	/**
	 * Gets the prop case roles in run by cac id.
	 *
	 * @return the prop case roles in run by cac id
	 */
	protected Hashtable<String,List<IECaseRole>> getPropCaseRolesInRunByCacId() {
		if (caseRolesInRunByCacId == null)
			caseRolesInRunByCacId = new Hashtable<String,List<IECaseRole>>(0);
		return caseRolesInRunByCacId;
	}

	/**
	 * Gets the case role given by aCarId.
	 * If called from within Emergo player case role saved in memory is returned.
	 *
	 * @param aCarId the a car id
	 *
	 * @return the case role
	 */
	public IECaseRole getCaseRole(int aCarId) {
		if (sSpring.inRun())
			return getCaseRoleInRun(aCarId);
		return ((ICaseRoleManager) (sSpring.getBean("caseRoleManager"))).getCaseRole(aCarId);
	}

	/**
	 * Gets the case role in run by car id.
	 *
	 * @return the case role in run by car id
	 */
	protected Hashtable<String,IECaseRole> getCaseRoleInRunByCarId() {
		return getPropCaseRoleInRunByCarId();
	}

	/**
	 * Gets the case role in run given by aCarId.
	 * If existing case role saved in memory is returned.
	 * Otherwise case role is read and saved in memory.
	 *
	 * @param aCarId the a car id
	 *
	 * @return the case role in run
	 */
	protected IECaseRole getCaseRoleInRun(int aCarId) {
		String lId = "" + aCarId;
		if (getCaseRoleInRunByCarId().containsKey(lId))
			return (IECaseRole) getCaseRoleInRunByCarId().get(lId);
		else {
			IECaseRole lCar = ((ICaseRoleManager) (sSpring.getBean("caseRoleManager"))).getCaseRole(aCarId);
			if (lCar != null)
				getCaseRoleInRunByCarId().put(lId, lCar);
			return lCar;
		}
	}

	/**
	 * Gets the current case role, so null if not called from within Emergo player.
	 * Case role saved in memory is returned.
	 *
	 * @return the case role
	 */
	public IECaseRole getCaseRole() {
		if (sSpring.inRun())
			return getCaseRoleInRun();
		return null;
	}

	/**
	 * Gets the case role in run, so null if not called from within Emergo player.
	 *
	 * @return the case role in run
	 */
	protected IECaseRole getCaseRoleInRun() {
		if (sSpring.getRunGroupAccount() == null)
			return null;
		return sSpring.getRunGroupAccount().getERunGroup().getECaseRole();
	}

	/**
	 * Gets all case roles. Either npcs or pcs.
	 *
	 * @param npc the npc status
	 *
	 * @return the case roles
	 */
	public List<IECaseRole> getCaseRoles(boolean npc) {
		if (sSpring.inRun()) {
			if (npc) {
				if (getPropNpcCaseRoles() == null) {
					setPropNpcCaseRoles(((ICaseRoleManager)sSpring.getBean("caseRoleManager")).getAllCaseRolesByCasId(sSpring.getCase().getCasId(), npc));
				}
				return getPropNpcCaseRoles();
			}
			else {
				if (getPropPcCaseRoles() == null) {
					setPropPcCaseRoles(((ICaseRoleManager)sSpring.getBean("caseRoleManager")).getAllCaseRolesByCasId(sSpring.getCase().getCasId(), npc));
				}
				return getPropPcCaseRoles();
			}
		}
		else {
			return ((ICaseRoleManager)sSpring.getBean("caseRoleManager")).getAllCaseRolesByCasId(sSpring.getCase().getCasId(), npc);
		}
	}

	/**
	 * Gets the case roles in run by cac id.
	 *
	 * @return the case roles in run by cac id
	 */
	protected Hashtable<String,List<IECaseRole>> getCaseRolesInRunByCacId() {
		return getPropCaseRolesInRunByCacId();
	}

	/**
	 * Gets the case roles by cac id.
	 *
	 * @param aCacId the a cac id
	 *
	 * @return the case roles by cac id
	 */
	protected List<IECaseRole> getCaseRolesByCacId(int aCacId) {
		List<IECaseRole> lPcCaseRoles = getCaseRoles(false);
		List<IECaseRole> lCaseRoles = new ArrayList<IECaseRole>(0);
		for (IECaseRole lCaseRole : lPcCaseRoles) {
			List<IECaseComponentRole> lCaseComponentRoles = sSpring.getCaseComponentRolesByCarId(lCaseRole.getCarId());
			for (IECaseComponentRole lCaseComponentRole : lCaseComponentRoles) {
				if (lCaseComponentRole.getCacCacId() == aCacId && 
						!lCaseRoles.contains(lCaseRole)) {
					lCaseRoles.add(lCaseRole);
				}
			}
		}
		return lCaseRoles;
	}

	/**
	 * Gets the case roles by cac id.
	 *
	 * @param aCacId the a cac id
	 *
	 * @return the case roles by cac id
	 */
	public List<IECaseRole> getCaseRolesByCacId(String aCacId) {
		if (aCacId == null || aCacId.equals("")) {
			return null;
		}
		return getCaseRolesByCacId(Integer.parseInt(aCacId));
	}

	/**
	 * Returns all case roles which are shared by casecomponents given by aMCacId
	 * and aSCacId.
	 *
	 * @param aMCacId the a m cac id
	 * @param aSCacId the a s cac id
	 *
	 * @return the case roles by master slave cac ids
	 */
	public List<IECaseRole> getCaseRolesByMasterSlaveCacIds(String aMCacId, String aSCacId) {
		List<IECaseRole> lMasterCaseRoles = getCaseRolesByCacId(aMCacId);
		List<IECaseRole> lSlaveCaseRoles = getCaseRolesByCacId(aSCacId);
		if ((lMasterCaseRoles == null) || (lSlaveCaseRoles == null))
			return null;
		for (int i = (lMasterCaseRoles.size() - 1); i >= 0; i--) {
			IECaseRole lMCaseRole = lMasterCaseRoles.get(i);
			boolean lFound = false;
			for (int j = (lSlaveCaseRoles.size() - 1); j >= 0; j--) {
				IECaseRole lSCaseRole = (IECaseRole) lSlaveCaseRoles.get(j);
				if (lSCaseRole.getCarId() == lMCaseRole.getCarId())
					lFound = true;
			}
			if (!lFound)
				lMasterCaseRoles.remove(i);
		}
		return lMasterCaseRoles;
	}

	/**
	 * Gets the case role name.
	 *
	 * @param aCarId the a car id
	 *
	 * @return the case role name
	 */
	public String getCaseRoleName(String aCarId) {
		IECaseRole lItem = getCaseRole(Integer.parseInt(aCarId));
		if (lItem == null)
			return vView.getLabel("cde_s_script.invalid");
		return lItem.getName();
	}

}
