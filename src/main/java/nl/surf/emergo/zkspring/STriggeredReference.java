/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.zkspring;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class STriggeredReference, is used to store data of a triggered case component or data tag and/or status tag within it.
 */
public class STriggeredReference {

	/** The case component. */
	protected IECaseComponent caseComponent = null;

	/** The data tag. */
	protected IXMLTag dataTag = null;

	/** The status tag. */
	protected IXMLTag statusTag = null;

	/** The status key. */
	protected String statusKey = "";

	/** The status value. */
	protected String statusValue = "";

	/** The set from script value. */
	protected boolean setFromScript = false;

	/**
	 * Instantiates a new STriggeredReference.
	 */
	public STriggeredReference() {
	}

	/**
	 * Instantiates a new STriggeredReference.
	 *
	 * @param aCaseComponent the spring a case component
	 */
	public STriggeredReference(IECaseComponent caseComponent, IXMLTag dataTag, IXMLTag statusTag, String statusKey, String statusValue, boolean fromScript) {
		setCaseComponent(caseComponent);
		setDataTag(dataTag);
		setStatusTag(statusTag);
		setStatusKey(statusKey);
		setStatusValue(statusValue);
		setSetFromScript(fromScript);
	}

	public IECaseComponent getCaseComponent() {
		return caseComponent;
	}

	public void setCaseComponent(IECaseComponent caseComponent) {
		this.caseComponent = caseComponent;
	}

	public IXMLTag getDataTag() {
		return dataTag;
	}

	public void setDataTag(IXMLTag dataTag) {
		this.dataTag = dataTag;
	}

	public IXMLTag getStatusTag() {
		return statusTag;
	}

	public void setStatusTag(IXMLTag statusTag) {
		this.statusTag = statusTag;
	}

	public String getStatusKey() {
		return statusKey;
	}

	public void setStatusKey(String statusKey) {
		this.statusKey = statusKey;
	}

	public String getStatusValue() {
		return statusValue;
	}

	public void setStatusValue(String statusValue) {
		this.statusValue = statusValue;
	}

	public boolean getSetFromScript() {
		return setFromScript;
	}

	public void setSetFromScript(boolean setFromScript) {
		this.setFromScript = setFromScript;
	}

}
