/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.zkspring;

import static nl.surf.emergo.business.impl.AppConstants.statusKeyValueIndex;
import static nl.surf.emergo.business.impl.AppConstants.statusValueTrue;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jboolexpr.BooleanExpression;
import jboolexpr.MalformedBooleanException;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CXmlHelper;
import nl.surf.emergo.control.script.CScript;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.domain.IEComponent;
import nl.surf.emergo.domain.IERun;

public class SScriptHelper {
	private static final Logger _log = LogManager.getLogger(SScriptHelper.class);
	protected SSpring sSpring;

	protected CScript cScript;

	/**
	 * The tag script ids. Used to store Emergo player data in memory, for
	 * performance.
	 */
	protected Hashtable<String, List<String>> tagScriptIdsInRun = null;

	/**
	 * The tag script template ids. Used to store Emergo player data in memory, for
	 * performance.
	 */
	protected Hashtable<String, List<String>> tagScriptTemplateIdsInRun = null;

	/**
	 * The component script ids. Used to store Emergo player data in memory, for
	 * performance.
	 */
	protected Hashtable<String, List<String>> componentScriptIdsInRun = null;

	/**
	 * The template tag script ids. Used to store Emergo player data in memory, for
	 * performance.
	 */
	protected Hashtable<String, List<String>> templateTagScriptIdsInRun = null;

	/**
	 * The template component script ids. Used to store Emergo player data in
	 * memory, for performance.
	 */
	protected Hashtable<String, List<String>> templateComponentScriptIdsInRun = null;

	/**
	 * The boolean templateTriggerFound, indicates wether in a template condition,
	 * the condition trigger is refered to by the template.
	 */
	protected Boolean templateTriggerFound = false;

	/**
	 * Gets the prop template trigger found.
	 *
	 * @return the prop template trigger found
	 */
	protected boolean getPropTemplateTriggerFound() {
		return templateTriggerFound;
	}

	/**
	 * Sets the prop template trigger found.
	 *
	 * @param aFound the new prop template trigger found
	 */
	protected void setPropTemplateTriggerFound(Boolean aFound) {
		templateTriggerFound = aFound;
	}

	public SScriptHelper(SSpring sSpring) {
		this.sSpring = sSpring;
		this.cScript = sSpring.getPropCScript();
	}

	/**
	 * Gets the prop tag script ids in run.
	 *
	 * @return the prop tag script ids in run
	 */
	protected Hashtable<String, List<String>> getPropTagScriptIdsInRun() {
		if (tagScriptIdsInRun == null)
			tagScriptIdsInRun = new Hashtable<String, List<String>>(0);
		return tagScriptIdsInRun;
	}

	/**
	 * Gets the tag prop script template ids in run.
	 *
	 * @return the tag prop script template ids in run
	 */
	protected Hashtable<String, List<String>> getPropTagScriptTemplateIdsInRun() {
		if (tagScriptTemplateIdsInRun == null)
			tagScriptTemplateIdsInRun = new Hashtable<String, List<String>>(0);
		return tagScriptTemplateIdsInRun;
	}

	/**
	 * Gets the component script ids in run.
	 *
	 * @return the prop component script ids in run
	 */
	protected Hashtable<String, List<String>> getPropComponentScriptIdsInRun() {
		if (componentScriptIdsInRun == null)
			componentScriptIdsInRun = new Hashtable<String, List<String>>(0);
		return componentScriptIdsInRun;
	}

	/**
	 * Gets the prop template tag script ids in run.
	 *
	 * @return the prop template tag script ids in run
	 */
	protected Hashtable<String, List<String>> getPropTemplateTagScriptIdsInRun() {
		if (templateTagScriptIdsInRun == null)
			templateTagScriptIdsInRun = new Hashtable<String, List<String>>(0);
		return templateTagScriptIdsInRun;
	}

	/**
	 * Gets the component template script ids in run.
	 *
	 * @return the prop component template script ids in run
	 */
	protected Hashtable<String, List<String>> getPropTemplateComponentScriptIdsInRun() {
		if (templateComponentScriptIdsInRun == null)
			templateComponentScriptIdsInRun = new Hashtable<String, List<String>>(0);
		return templateComponentScriptIdsInRun;
	}

	/**
	 * Checks script timers. All timers defined within all script components. This
	 * method is called periodically to check of timers have to fire. And if so
	 * finished for timer is set to true and conditions which check the timer can be
	 * fired. A timer can be defined to count case time, that is the total time the
	 * Emergo player is opened over all sessions, or to count real time. If real
	 * time, it can be measured from the start of the run or from the moment the
	 * timer is started, that is the moment the condition fired that started the
	 * timer.
	 *
	 * @param aSaveInDb the a save in db, whether possible status change is saved in
	 *                  database, or only in memory
	 */
	public void checkScriptTimers(boolean aSaveInDb) {
		IERun lRun = sSpring.getRun();
		if (lRun == null)
			// only checking script in Emergo player
			return;
		double lCaseTime = sSpring.getCaseTime();
		double lRealTime = sSpring.getRealTime();
		double lRealTimeFromStartOfRun = sSpring.getRealTimeFromStartOfRun();
		// get all script components
		List<IECaseComponent> lCaseComponents = sSpring.getCaseComponents(lRun.getECase(), "scripts");
		for (IECaseComponent lCaseComponent : lCaseComponents) {
			IXMLTag lRootTag = sSpring.getXmlDataPlusRunStatusTree(lCaseComponent, AppConstants.statusTypeRunGroup);
			// is script component present?
			boolean lComponentIsPresent = false;
			if (lRootTag != null) {
				IXMLTag lComponentTag = lRootTag.getChild(AppConstants.componentElement);
				if (lComponentTag != null) {
					lComponentIsPresent = (!sSpring.getCurrentTagStatus(lComponentTag, AppConstants.statusKeyPresent)
							.equals(AppConstants.statusValueFalse));
				}
			}
			if (lComponentIsPresent) {
				// get timers within the script
				List<IXMLTag> lTags = new ArrayList<IXMLTag>(0);
				getNamedTags(lRootTag, "timer", lTags);
				for (IXMLTag lTag : lTags) {
					// check if timer is present and started
					boolean lIsPresent = (!sSpring.getCurrentTagStatus(lTag, AppConstants.statusKeyPresent)
							.equals(AppConstants.statusValueFalse));
					boolean lIsStarted = (sSpring.getCurrentTagStatus(lTag, AppConstants.statusKeyStarted)
							.equals(AppConstants.statusValueTrue));
					if (lIsPresent && lIsStarted) {
						boolean lIsRealtime = (lTag.getChildValue("realtime").equals(AppConstants.statusValueTrue));
						boolean lFromStartOfRun = (lTag.getChildValue("fromstartofrun")
								.equals(AppConstants.statusValueTrue));
						// get current time and starttime of timer
						double lTimeToCheck = 0;
						String lStarttime = "";
						if (lIsRealtime) {
							if (lFromStartOfRun) {
								lTimeToCheck = lRealTimeFromStartOfRun;
								lStarttime = "" + lTag.getCurrentStatusAttributeTime(AppConstants.statusKeyStarted);
							} else {
								lTimeToCheck = lRealTime;
								lStarttime = lTag.getCurrentStatusAttribute(AppConstants.statusKeyRealtime);
							}
						} else {
							lTimeToCheck = lCaseTime;
							lStarttime = "" + lTag.getCurrentStatusAttributeTime(AppConstants.statusKeyStarted);
						}
						// get delay of timer
						String lDelay = lTag.getChildValue("delay");
						if ((!lStarttime.equals("")) && (!lDelay.equals(""))) {
							// get repeats of timer
							boolean lRepeats = (lTag.getChildValue("repeats").equals(AppConstants.statusValueTrue));
							int lRepeatCount = 0;
							long lFinishedCount = 0;
							if (lRepeats) {
								String lRepeatCountStr = lTag.getChildValue("repeatcount");
								if (!lRepeatCountStr.equals(""))
									lRepeatCount = Integer.parseInt(lRepeatCountStr);
								// get number of times the timer has gone off
								lFinishedCount = lTag.getStatusAttributeCount(AppConstants.statusKeyFinished,
										AppConstants.statusValueTrue);
							}
							// check if timer has to fire
							if (lTimeToCheck >= (Double.parseDouble(lStarttime)
									+ ((lFinishedCount + 1) * Double.parseDouble(lDelay)))) {
								if ((!lRepeats) || (lRepeatCount == 0) || (lFinishedCount < lRepeatCount)) {
									// if so set timer finished status to true
									sSpring.setRunTagStatus(lCaseComponent, lTag, AppConstants.statusKeyFinished,
											AppConstants.statusValueTrue, true, AppConstants.statusTypeRunGroup,
											aSaveInDb, true);
									if (!lRepeats)
										// and if timer is no repeat timer, set started to false
										sSpring.setRunTagStatus(lCaseComponent, lTag, AppConstants.statusKeyStarted,
												AppConstants.statusValueFalse, true, AppConstants.statusTypeRunGroup,
												aSaveInDb, true);
								}
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Gets the named tags, child tags of aParentTag with name aTagName
	 *
	 * @param aParentTag the a parent tag
	 * @param aTagName   the a tag name
	 * @param aXmlTags   the a xml tags
	 */
	private void getNamedTags(IXMLTag aParentTag, String aTagName, List<IXMLTag> aXmlTags) {
		if (aParentTag == null)
			return;
		List<IXMLTag> lChildTags = aParentTag.getChildTags();
		for (IXMLTag lChildTag : lChildTags) {
			if (lChildTag.getName().equals(aTagName))
				aXmlTags.add(lChildTag);
			getNamedTags(lChildTag, aTagName, aXmlTags);
		}
	}

	/**
	 * Checks run group tag script. Status key of a content tag is set. Checks if
	 * script conditions are valid.
	 *
	 * @param aTriggeredReference the a triggered reference
	 * @param aStatusType         the a status type, see IAppManager
	 * @param aSaveInDb           the a save in db, whether status change is saved
	 *                            in database, or only in memory
	 */
	protected void checkRunGroupTagScript(STriggeredReference aTriggeredReference, String aStatusType,
			boolean aSaveInDb) {
		if (aTriggeredReference.getCaseComponent() == null || aTriggeredReference.getDataTag() == null)
			return;
		String lIds = "" + aTriggeredReference.getCaseComponent().getCacId() + ","
				+ aTriggeredReference.getDataTag().getAttribute(AppConstants.keyId) + ","
				+ aTriggeredReference.getStatusKey();
		List<String> lTagScriptIds = null;
		List<String> lTagScriptTemplateIds = null;
		List<String> lTemplateTagScriptIds = null;
		String lTriggerId = "" + sSpring.getAppManager().getStatusKeyIndex(aTriggeredReference.getStatusKey());
		IECaseRole lCaseRole = sSpring.getSCaseRoleHelper().getCaseRole();
		if (getPropTagScriptIdsInRun().containsKey(lIds)) {
			lTagScriptIds = getPropTagScriptIdsInRun().get(lIds);
			lTagScriptTemplateIds = getPropTagScriptTemplateIdsInRun().get(lIds);
			lTemplateTagScriptIds = getPropTemplateTagScriptIdsInRun().get(lIds);
		} else {
			if (lCaseRole == null)
				return;
			// get scriptids for current case role within player, aCaseComponent and aTag
			// a script id is a string with carid, script cacid and script tagid separated
			// by commas
			lTagScriptIds = sSpring.getCaseHelper().getTagRefIds(AppConstants.defValueReftypeAllScriptTags, lTriggerId,
					lCaseRole, aTriggeredReference.getCaseComponent(), aTriggeredReference.getDataTag(), true);
			// templateids are a subset of scriptids that contain only template strings as
			// reference to content tags
			lTagScriptTemplateIds = sSpring.getCaseHelper().getScriptConditionTemplateIds(lTriggerId, lCaseRole,
					aTriggeredReference.getCaseComponent(), aTriggeredReference.getDataTag(), true);
			lTemplateTagScriptIds = sSpring.getCaseHelper().getTemplateTagRefIds(
					AppConstants.defValueReftypeAllTemplateScriptTags, lTriggerId, lCaseRole,
					aTriggeredReference.getCaseComponent(), aTriggeredReference.getDataTag(), true);
			if ((lTagScriptIds != null) && (lTagScriptIds.size() > 0)) {
				getPropTagScriptIdsInRun().put(lIds, lTagScriptIds);
				getPropTagScriptTemplateIdsInRun().put(lIds, lTagScriptTemplateIds);
				getPropTemplateTagScriptIdsInRun().put(lIds, lTemplateTagScriptIds);
			}
		}
		Hashtable<String, List<String>> lHScriptIds = new Hashtable<String, List<String>>();
		lHScriptIds.put(AppConstants.defValueReftypeAllScriptTags, lTagScriptIds);
		lHScriptIds.put("tag_script_template_ids", lTagScriptTemplateIds);
		lHScriptIds.put(AppConstants.defValueReftypeAllTemplateScriptTags, lTemplateTagScriptIds);
		checkScript(aTriggeredReference, lHScriptIds, aStatusType, aSaveInDb);
	}

	/**
	 * Checks run group component script. Status key of a component is set. Checks
	 * if script conditions are valid.
	 *
	 * @param aTriggeredReference the a triggered reference
	 * @param aStatusType         the a status type, see IAppManager
	 * @param aSaveInDb           the a save in db, whether status change is saved
	 *                            in database, or only in memory
	 */
	protected void checkRunGroupComponentScript(STriggeredReference aTriggeredReference, String aStatusType,
			boolean aSaveInDb) {
		if (aTriggeredReference.getCaseComponent() == null)
			return;
		String lIds = "" + aTriggeredReference.getCaseComponent().getCacId() + "," + aTriggeredReference.getStatusKey();
		List<String> lComponentScriptIds = null;
		List<String> lTemplateComponentScriptIds = null;
		String lTriggerId = "" + sSpring.getAppManager().getStatusKeyIndex(aTriggeredReference.getStatusKey());
		IECaseRole lCaseRole = sSpring.getSCaseRoleHelper().getCaseRole();
		if (getPropComponentScriptIdsInRun().containsKey(lIds)) {
			lComponentScriptIds = getPropComponentScriptIdsInRun().get(lIds);
			lTemplateComponentScriptIds = getPropTemplateComponentScriptIdsInRun().get(lIds);
		} else {
			if (lCaseRole == null)
				return;
			// get scriptids for current case role within player and aCaseComponent
			// a script id is a string with carid, script cacid and script tagid separated
			// by commas
			lComponentScriptIds = sSpring.getCaseHelper().getCacRefIds(AppConstants.defValueReftypeAllScriptTags,
					lTriggerId, lCaseRole, aTriggeredReference.getCaseComponent());
			lTemplateComponentScriptIds = sSpring.getCaseHelper().getTemplateCacRefIds(
					AppConstants.defValueReftypeAllTemplateScriptTags, lTriggerId, lCaseRole,
					aTriggeredReference.getCaseComponent());
			if (lComponentScriptIds != null) {
				getPropComponentScriptIdsInRun().put(lIds, lComponentScriptIds);
				getPropTemplateComponentScriptIdsInRun().put(lIds, lTemplateComponentScriptIds);
			}
		}
		Hashtable<String, List<String>> lHScriptIds = new Hashtable<String, List<String>>();
		lHScriptIds.put(AppConstants.defValueReftypeAllScriptTags, lComponentScriptIds);
		lHScriptIds.put(AppConstants.defValueReftypeAllTemplateScriptTags, lTemplateComponentScriptIds);
		checkScript(aTriggeredReference, lHScriptIds, aStatusType, aSaveInDb);
	}

	/**
	 * Checks script. Loops through all script components and checks if conditions
	 * are valid. If so its actions are executed.
	 *
	 * @param aTriggeredReference the a triggered reference
	 * @param aScriptIds          the a script ids hashtable, a script id is a
	 *                            string with carid, script cacid and script tagid
	 *                            separated by commas
	 * @param aStatusType         the a status type, see IAppManager
	 * @param aSaveInDb           the a save in db, whether status change is saved
	 *                            in database, or only in memory
	 */
	protected void checkScript(STriggeredReference aTriggeredReference, Hashtable<String, List<String>> aScriptIds,
			String aStatusType, boolean aSaveInDb) {
		if ((aScriptIds == null) || (aScriptIds.size() == 0))
			return;
		IERun lRun = sSpring.getRun();
		if (lRun == null)
			// only check script in player
			return;
		IECase lCase = lRun.getECase();
		if (lCase == null)
			return;
		// Get current caserole
		IECaseRole lCaseRole = sSpring.getSCaseRoleHelper().getCaseRole();
		if (lCaseRole == null)
			return;
		// Get script components
		List<IECaseComponent> lCaseComponents = sSpring.getCaseComponents(lRun.getECase(), "scripts");
		if (lCaseComponents == null)
			return;
		sSpring.getSLogHelper().logIndent++;
		for (IECaseComponent lCaseComponent : lCaseComponents) {
			// check if script component is present
//			boolean lCacPresent = !getCurrentRunComponentStatus(lCaseComponent, AppConstants.statusKeyPresent, aStatusType).equals(AppConstants.statusValueFalse);
			boolean lCacPresent = false;
			IXMLTag lRootTag = sSpring.getXmlDataPlusRunStatusTree(lCaseComponent, aStatusType);
			if (lRootTag != null) {
				IXMLTag lComponentTag = lRootTag.getChild(AppConstants.componentElement);
				if (lComponentTag != null) {
					lCacPresent = !lComponentTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent)
							.equals(AppConstants.statusValueFalse);
				}
			}
			if (lCacPresent) {
				// only check script component if present
				IXMLTag lContentTag = null;
				if (lRootTag != null)
					lContentTag = lRootTag.getChild(AppConstants.contentElement);
				// get script tag ids for current case role and script component
				List<String> lScriptTagIds = getScriptTagIds("" + lCaseRole.getCarId(), "" + lCaseComponent.getCacId(),
						aScriptIds.get(AppConstants.defValueReftypeAllScriptTags));
				List<String> lScriptTemplateTagIds = new ArrayList<String>();
				if (aScriptIds.containsKey("tag_script_template_ids")) {
					lScriptTemplateTagIds = getScriptTagIds("" + lCaseRole.getCarId(), "" + lCaseComponent.getCacId(),
							aScriptIds.get("tag_script_template_ids"));
				}
				List<String> lTemplateScriptTagIds = getScriptTagIds("" + lCaseRole.getCarId(),
						"" + lCaseComponent.getCacId(),
						aScriptIds.get(AppConstants.defValueReftypeAllTemplateScriptTags));
				Hashtable<String, List<String>> lHScriptIds = new Hashtable<String, List<String>>();
				lHScriptIds.put(AppConstants.defValueReftypeAllScriptTags, lScriptTagIds);
				lHScriptIds.put("tag_script_template_ids", lScriptTemplateTagIds);
				lHScriptIds.put(AppConstants.defValueReftypeAllTemplateScriptTags, lTemplateScriptTagIds);
				if ((lContentTag != null) && (lScriptTagIds.size() > 0 || lTemplateScriptTagIds.size() > 0)) {
					// hashtable is used to prevent conditions or actions to be checked more than
					// once
					Hashtable<String, String> lCheckedTags = new Hashtable<String, String>(0);
					// list is used to gather all conditions and actions which are valid, so for
					// which status opened has to be set to true
					// conditions and actions are added in the same order as they are found in the
					// script
					List<IXMLTag> lOpenedTags = new ArrayList<IXMLTag>(0);
					// hashtable and list are filled by checkScript

					checkScript(aTriggeredReference, lContentTag, lHScriptIds, lCheckedTags, lOpenedTags, aStatusType);
					// log conditions, and execute and log actions
					// in case of sub conditions logIndent has to be restored after for loop
					int lLogIndent = sSpring.getSLogHelper().logIndent;
					for (IXMLTag lScriptTag : lOpenedTags) {
						String lTagPid = lScriptTag.getChildValue("name");
						if (lTagPid.equals("")) {
							lTagPid = lScriptTag.getChildValue("pid");
						}
						lTagPid = sSpring.getXmlManager().unescapeXML(lTagPid);
						if (lScriptTag.getName().equals("condition")) {
							sSpring.getSLogHelper().logIndent = lLogIndent;
							sSpring.getSLogHelper().logIndent += getNumberOfParentConditions(lScriptTag);
							sSpring.getSLogHelper().logSetScriptAction("condition triggered",
									sSpring.getXmlManager().unescapeXML(lCaseComponent.getName()), lScriptTag.getName(),
									lTagPid);
						}
						// actions are executed in the same order as they are found in script
						else if (lScriptTag.getName().equals("action")) {
							sSpring.getSLogHelper().logIndent++;
							// log action
							sSpring.getSLogHelper().logSetScriptAction("action started",
									sSpring.getXmlManager().unescapeXML(lCaseComponent.getName()), lScriptTag.getName(),
									lTagPid);
							// execute action
							if (executeActionTag(aTriggeredReference, lScriptTag, aStatusType, aSaveInDb)) {
								// log action
								sSpring.getSLogHelper().logSetScriptAction("action executed",
										sSpring.getXmlManager().unescapeXML(lCaseComponent.getName()),
										lScriptTag.getName(), lTagPid);
							}
							sSpring.getSLogHelper().logIndent--;
						}
					}
					// in case of sub conditions logIndent has to be restored
					sSpring.getSLogHelper().logIndent = lLogIndent;
					// set opened of script tags
					for (IXMLTag lScriptTag : lOpenedTags) {
						sSpring.setRunTagStatus(lCaseComponent, lScriptTag, AppConstants.statusKeyOpened,
								AppConstants.statusValueTrue, true, aStatusType, aSaveInDb, true);
						if (lScriptTag.getName().equals("timer")) {
							// start timer
							sSpring.setRunTagStatus(lCaseComponent, lScriptTag, AppConstants.statusKeyStarted,
									AppConstants.statusValueTrue, true, aStatusType, aSaveInDb, true);
							// set real start time
							sSpring.setRunTagStatus(lCaseComponent, lScriptTag, AppConstants.statusKeyRealtime,
									"" + sSpring.getRealTime(), true, aStatusType, aSaveInDb, true);
						}
					}
				}
			}
		}
		sSpring.getSLogHelper().logIndent--;
	}

	/**
	 * Gets the script tag ids for aCarId and aCacId out of list aScriptIds.
	 *
	 * @param aCarId     the a car id
	 * @param aCacId     the a cac id
	 * @param aScriptIds the a script ids
	 *
	 * @return the script tag ids
	 */
	private List<String> getScriptTagIds(String aCarId, String aCacId, List<String> aScriptIds) {
		List<String> lScriptIds = new ArrayList<String>(0);
		for (String lScriptId : aScriptIds) {
			String[] lScriptIdArr = lScriptId.split(",");
			if ((lScriptIdArr[0].equals(aCarId)) && (lScriptIdArr[1].equals(aCacId)))
				lScriptIds.add(lScriptIdArr[2]);
		}
		return lScriptIds;
	}

	/**
	 * Gets the number of parent conditions of aConditionTag.
	 *
	 * @param aConditionTag the a condition tag
	 *
	 * @return the number of parent conditions
	 */
	private int getNumberOfParentConditions(IXMLTag aConditionTag) {
		int lNumber = 0;
		IXMLTag lParentTag = aConditionTag.getParentTag();
		while (lParentTag != null && lParentTag.getName().equals("condition")) {
			lNumber++;
			lParentTag = lParentTag.getParentTag();
		}
		return lNumber;
	}

	/**
	 * Checks script. Loops through all node child tags (conditions) of aContentTag
	 * and if tag id is present in aScriptTagIds and if condition status is present,
	 * checks the condition.
	 *
	 * @param aTriggeredReference the a triggered reference
	 * @param aContentTag         the a content tag
	 * @param aScriptTagIds       the a script tag ids
	 * @param aCheckedTags        the a checked tags
	 * @param aOpenedTags         the a opened tags
	 * @param aStatusType         the a status type, see IAppManager
	 */
	private void checkScript(STriggeredReference aTriggeredReference, IXMLTag aContentTag,
			Hashtable<String, List<String>> aScriptTagIds, Hashtable<String, String> aCheckedTags,
			List<IXMLTag> aOpenedTags, String aStatusType) {
		if (aContentTag == null)
			return;
		List<IXMLTag> lChildTags = aContentTag.getChildTags();
		List<String> lScriptTagIds = new ArrayList<String>();
		lScriptTagIds.addAll(aScriptTagIds.get(AppConstants.defValueReftypeAllScriptTags));
		for (String lTemplateScriptTagId : aScriptTagIds.get(AppConstants.defValueReftypeAllTemplateScriptTags)) {
			// add template script tag ids if they are not yet present.
			// in case of condition containing check on status and on template script tag id
			// will be both in all script tags and
			// in all template script tags
			if (!lScriptTagIds.contains(lTemplateScriptTagId)) {
				lScriptTagIds.add(lTemplateScriptTagId);
			}
		}
		for (IXMLTag lScriptTag : lChildTags) {
			if (lScriptTagIds.size() > 0) {
				// get scripttag
				boolean lPresent = false;
				boolean lNode = (lScriptTag.getAttribute(AppConstants.defKeyType).equals(AppConstants.defValueNode));
				if (lNode)
					lPresent = (!sSpring.getCurrentTagStatus(lScriptTag, AppConstants.statusKeyPresent)
							.equals(AppConstants.statusValueFalse));
				if (lNode && lPresent) {
					// if node tag and present
					if (lScriptTag.getName().equals("condition")) {
						// and if condition tag
						List<String> lRemoveTagIds = new ArrayList<String>();
						for (String lScriptTagId : lScriptTagIds) {
							if (lScriptTagId.equals(lScriptTag.getAttribute(AppConstants.keyId))) {
								// if scripttag in aScriptTagIds
								Boolean lTemplateScript = false;
								if ((aScriptTagIds.containsKey("tag_script_template_ids")
										&& (aScriptTagIds.get("tag_script_template_ids").size() > 0)))
									if (aScriptTagIds.get("tag_script_template_ids").contains(lScriptTagId))
										lTemplateScript = true;
								checkCondition(aTriggeredReference, lScriptTag, aCheckedTags, aOpenedTags, true,
										aStatusType, lTemplateScript);
								lRemoveTagIds.add(lScriptTagId);
							}
						}
						if (lRemoveTagIds.size() > 0) {
							for (String lTagId : lRemoveTagIds) {
								lScriptTagIds.remove(lTagId);
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Checks condition. If it is a sub condition all of its parent conditions must
	 * evaluate to true too so this is checked. If condition evaluates to true, all
	 * of its actions are checked. An action can also be a sub condition again.
	 *
	 * @param aTriggeredReference the a triggered reference
	 * @param aScriptTag          the a script tag
	 * @param aCheckedTags        the a checked tags
	 * @param aOpenedTags         the a opened tags
	 * @param aDoChildren         the a do children
	 * @param aStatusType         the a status type, see IAppManager
	 * @param aTemplateScript     if true, then this condition only has template
	 *                            refers, and triggered data tag should be refered
	 *                            to by template
	 *
	 * @return true, if successful
	 */
	private boolean checkCondition(STriggeredReference aTriggeredReference, IXMLTag aScriptTag,
			Hashtable<String, String> aCheckedTags, List<IXMLTag> aOpenedTags, boolean aDoChildren, String aStatusType,
			boolean aTemplateScript) {
		String lId = aScriptTag.getAttribute(AppConstants.keyId);
		if (aCheckedTags.containsKey(lId))
			// if already checked
			return (aCheckedTags.get(lId).equals("true"));
		boolean lEvaluate = evaluateConditionTag(aTriggeredReference, aScriptTag, aStatusType, true, aTemplateScript);
		// save checked of condition
		aCheckedTags.put(lId, "" + lEvaluate);
		if (aDoChildren && lEvaluate) {
			// if dochildren and condition evaluates to true, children are possibly actions
			// and conditions
			// determine if all parent conditions are true. this has to be the case for
			// nested conditions
			boolean lParentConditionsTrue = true;
			IXMLTag lParentTag = aScriptTag.getParentTag();
			while ((lParentTag.getName().equals("condition")) && (lParentConditionsTrue)) {
				// check parent condition without performing childactions/conditions, thus
				// parameter aDoChildren false
				boolean lDoChildren = false;
				lParentConditionsTrue = checkCondition(null, lParentTag, aCheckedTags, aOpenedTags, lDoChildren,
						aStatusType, false);
				lParentTag = lParentTag.getParentTag();
			}
			if (lParentConditionsTrue) {
				// if all parent conditions are true
				// save opened of condition
				aOpenedTags.add(aScriptTag);
				// action can be sub condition!
				checkActions(null, aScriptTag.getChildTags(), aCheckedTags, aOpenedTags, aStatusType);
			}
		}
		return lEvaluate;
	}

	/**
	 * Checks actions. Loops through all actions and checks them. Actions can also
	 * be sub conditions.
	 *
	 * @param aTriggeredReference the a triggered reference
	 * @param aScriptTags         the a script tags
	 * @param aCheckedTags        the a checked tags
	 * @param aOpenedTags         the a opened tags
	 * @param aStatusType         the a status type, see IAppManager
	 */
	private void checkActions(STriggeredReference aTriggeredReference, List<IXMLTag> aScriptTags,
			Hashtable<String, String> aCheckedTags, List<IXMLTag> aOpenedTags, String aStatusType) {
		if (aScriptTags == null)
			return;
		for (IXMLTag lScriptTag : aScriptTags) {
			boolean lPresent = false;
			boolean lNode = (lScriptTag.getAttribute(AppConstants.defKeyType).equals(AppConstants.defValueNode));
			if (lNode)
				lPresent = (!sSpring.getCurrentTagStatus(lScriptTag, AppConstants.statusKeyPresent)
						.equals(AppConstants.statusValueFalse));
			if (lNode && lPresent) {
				// if node tag and present
				if (lScriptTag.getName().equals("condition"))
					checkCondition(aTriggeredReference, lScriptTag, aCheckedTags, aOpenedTags, true, aStatusType,
							false);
				if (lScriptTag.getName().equals("action"))
					checkAction(lScriptTag, aCheckedTags, aOpenedTags);
				if (lScriptTag.getName().equals("timer"))
					checkAction(lScriptTag, aCheckedTags, aOpenedTags);
				if (lScriptTag.getName().equals("counter"))
					checkAction(lScriptTag, aCheckedTags, aOpenedTags);
			}
		}
	}

	/**
	 * Checks action.
	 *
	 * @param aScriptTag   the a script tag
	 * @param aCheckedTags the a checked tags
	 * @param aOpenedTags  the a opened tags
	 *
	 * @return true, if successful
	 */
	private boolean checkAction(IXMLTag aScriptTag, Hashtable<String, String> aCheckedTags, List<IXMLTag> aOpenedTags) {
		String lId = aScriptTag.getAttribute(AppConstants.keyId);
		if (aCheckedTags.containsKey(lId))
			return (aCheckedTags.get(lId).equals("true"));
		// action always evaluates to true, it always has to be executed
		boolean lEvaluate = true;
		// save checked of action
		aCheckedTags.put(lId, "" + lEvaluate);
		// save opened of action
		aOpenedTags.add(aScriptTag);
		return lEvaluate;
	}

	/**
	 * Evaluates logical Java expression.
	 *
	 * @param aJava the a java
	 *
	 * @return true, if successful
	 */
	private boolean evaluateLogicalExpression(String aJava) {
		if (aJava.equals(""))
			return true;
		String lJava = new String(aJava);
		Boolean lBoolean = null;
		String lJavaTrimmed = lJava.replace(" ", "");

		BooleanExpression boolExpr = null;
		try {
			boolExpr = BooleanExpression.readLeftToRight(lJavaTrimmed);
			lBoolean = boolExpr.booleanValue();
		} catch (MalformedBooleanException e) {
			_log.error(e);
		}
		if (lBoolean == null)
			return false;
		return (lBoolean.booleanValue());
	}

	/**
	 * Checks if current car id is present within a list of car ids.
	 *
	 * @param aCarIdTags the a car id tags
	 *
	 * @return true, if successful
	 */
	private boolean isCurrentCarIdWithinCarIds(List<IXMLTag> aCarIdTags) {
		IECaseRole lCaseRole = sSpring.getSCaseRoleHelper().getCaseRole();
		if (lCaseRole == null)
			return false;
		if (aCarIdTags == null)
			return false;
		String lCurrentCarId = "" + lCaseRole.getCarId();
		for (IXMLTag lCarIdTag : aCarIdTags) {
			if (lCarIdTag.getValue().equals(lCurrentCarId))
				return true;
		}
		return false;
	}

	/**
	 * Gets car ids unequal to current car id.
	 *
	 * @param aCarIdTags the a car id tags
	 *
	 * @return list of car ids
	 */
	private List<Integer> getCarIdsUnequalToCurrentCarId(List<IXMLTag> aCarIdTags) {
		List<Integer> lCarIds = new ArrayList<Integer>();
		IECaseRole lCaseRole = sSpring.getSCaseRoleHelper().getCaseRole();
		if (lCaseRole == null)
			return lCarIds;
		if (aCarIdTags == null)
			return lCarIds;
		String lCurrentCarId = "" + lCaseRole.getCarId();
		for (IXMLTag lCarIdTag : aCarIdTags) {
			if (!lCarIdTag.getValue().equals(lCurrentCarId)) {
				lCarIds.add(Integer.parseInt(lCarIdTag.getValue()));
			}
		}
		return lCarIds;
	}

	/**
	 * Evaluates condition tag, checks if the condition is true. A condition can be
	 * compound and can contain logical expressions. All condition elements are
	 * children of the condition tag. Condition parts are evaluated to true or false
	 * and in this way a logical Java expression is created that will be evaluated.
	 *
	 * @param aTriggeredReference     the a triggered reference
	 * @param aTriggeredCaseComponent the triggered a case component
	 * @param aTriggeredDataTag       the triggered a data tag
	 * @param aStatusKey              the a status key
	 * @param aStatusValue            the a status value
	 * @param aConditionTag           the a condition tag
	 * @param aStatusType             the a status type, see IAppManager
	 * @param aUseTempVars            if true, then use temporary variables to speed
	 *                                up condition check
	 * @param aTemplateScript         if true, then this condition only has template
	 *                                refers, and triggered data tag should be
	 *                                refered to by template
	 *
	 * @return true, if successful
	 */
	public boolean evaluateConditionTag(STriggeredReference aTriggeredReference, IXMLTag aConditionTag,
			String aStatusType, boolean aUseTempVars, boolean aTemplateScript) {
		return pEvaluateConditionTag(aTriggeredReference, aConditionTag, null, null, aStatusType, aUseTempVars,
				aTemplateScript);
	}

	/**
	 * Analyzes tag template strings, converts them to node names by replacing state
	 * parameters by their values. For each node name, if node with that name
	 * exists, the tag is added to the return list.
	 *
	 * @param aTagIdTags        the initial list of node tags
	 * @param aTagTemplateTags  the list of tags with template strings
	 * @param aCaseComponent    the a case component
	 * @param aTagName          the name of the tag nodes
	 * @param aTriggeredTagKey  the triggered tag key
	 * @param aCompareToTrigger if true, then compare resulting tags to trigger tag
	 * @param aDoOrEvaluation   if true, then the template string should operate as
	 *                          or condition, so if the template string matches
	 *                          aTriggeredTagKey aTagIdTags will contain only one
	 *                          tag id, that of the triggered tag. Otherwise
	 *                          aTagIdTags contains all tag ids for which tag keys
	 *                          match with the template string.
	 *
	 * @return aTagIdTags the resulting list of node tags
	 */
	private List<IXMLTag> addTagTemplatesToIdTags(List<IXMLTag> aTagIdTags, List<IXMLTag> aTagTemplateTags,
			IECaseComponent aCaseComponent, String aTagName, String aTriggeredTagKey, boolean aCompareToTrigger,
			boolean aDoOrEvaluation) {
		List<String> lIdNamesFromTemplates = new ArrayList<String>();
		for (IXMLTag lTemplateTag : aTagTemplateTags) {
			lIdNamesFromTemplates.add(sSpring.replaceVariablesWithinString(lTemplateTag.getValue()));
		}
		lIdNamesFromTemplates = sSpring.replaceTemporaryVarsWithinStrings(lIdNamesFromTemplates, aTriggeredTagKey);
		if (aDoOrEvaluation) {
			lIdNamesFromTemplates = sSpring.replaceMatchesWithinStrings(lIdNamesFromTemplates, aTriggeredTagKey);
		}
		List<String> lTagIds = new ArrayList<String>();
		for (IXMLTag lIdTag : aTagIdTags) {
			lTagIds.add(lIdTag.getValue());
		}
		if (!aTriggeredTagKey.equals(""))
			aTriggeredTagKey = sSpring.getXmlManager().unescapeXML(aTriggeredTagKey);
		List<IXMLTag> lNodeTags = cScript.getNodeTags(aCaseComponent, aTagName);
		Boolean lTemplateTriggerFound = false;
		for (IXMLTag lNodeTag : lNodeTags) {
			// regular expression search
			boolean lSearch = true;
			String lTagName = sSpring.getTagName(lNodeTag);
			for (String lTemplStr : lIdNamesFromTemplates) {
				if (lSearch) {
					if (lTagName.equals(lTemplStr)) {
						lSearch = false;
					} else {
						try {
							if (lTagName.matches(lTemplStr)) {
								lSearch = false;
							}
						} catch (PatternSyntaxException pError) {
						}
					}
					if (!lSearch) {
						if ((aCompareToTrigger) && (!aTriggeredTagKey.equals(""))
								&& (aTriggeredTagKey.equals(lTagName)))
							lTemplateTriggerFound = true;
						String lId = lNodeTag.getAttribute(AppConstants.keyId);
						if (!lTagIds.contains(lId)) {
							IXMLTag lTag = sSpring.getXmlManager().newXMLTag("tagid", lId);
							aTagIdTags.add(lTag);
						}
					}
				}
			}
		}
		if (lTemplateTriggerFound)
			setPropTemplateTriggerFound(true);
		return aTagIdTags;
	}

	/**
	 * Analyzes tag template strings, converts them to node names by replacing state
	 * parameters by their values. For each node name, if node with that name
	 * exists, the tag is added to the return list.
	 *
	 * @param aCaseComponents        the case components
	 * @param aComponentTemplateTags the list of tags with template strings
	 *
	 * @return the resulting list of case components
	 */
	private List<IECaseComponent> getTemplateCaseComponents(List<IECaseComponent> aCaseComponents,
			List<IXMLTag> aComponentTemplateTags) {
		List<String> lIdNamesFromTemplates = new ArrayList<String>();
		for (IXMLTag lTemplateTag : aComponentTemplateTags) {
			lIdNamesFromTemplates.add(sSpring.replaceVariablesWithinString(lTemplateTag.getValue()));
		}
		List<IECaseComponent> lCaseComponents = new ArrayList<IECaseComponent>();
		for (IECaseComponent lCaseComponent : aCaseComponents) {
			List<String> lTempIdNamesFromTemplates = sSpring.replaceTemporaryVarsWithinStrings(lIdNamesFromTemplates,
					lCaseComponent.getName());
			// regular expression search
			boolean lSearch = true;
			String lCaseComponentName = lCaseComponent.getName();
			for (String lTemplStr : lTempIdNamesFromTemplates) {
				if (lSearch) {
					if (lCaseComponentName.equals(lTemplStr)) {
						lSearch = false;
					} else {
						try {
							if (lCaseComponentName.matches(lTemplStr)) {
								lSearch = false;
							}
						} catch (PatternSyntaxException pError) {
						}
					}
					if (!lSearch) {
						lCaseComponents.add(lCaseComponent);
					}
				}
			}
		}
		return lCaseComponents;
	}

	/**
	 * Evaluates condition tag, checks if the condition is true. A condition can be
	 * compound and can contain logical expressions. All condition elements are
	 * children of the condition tag. Condition parts are evaluated to true or false
	 * and in this way a logical Java expression is created that will be evaluated.
	 * 
	 * NOTE For assessment items opened of chosen alternatives and determined
	 * feedback conditions is set. But an assessment item can be used within
	 * multiple assessments, so opened values of alternatives and feedback
	 * conditions mix up. That's why we have parameters aOpenedAlternativeTagIds and
	 * aOpenedFeedbackconditionTagId. These values are stored within refitem so give
	 * the alternative ids and feedback condition id related to an assessment.
	 * 
	 * NOTE aTriggeredReference is null in case of a child condition or if a parent
	 * condition of a child condition are evaluated. In this case the condition is
	 * evaluated without a trigger.
	 *
	 * NOTE If condition contains template script methods only evaluate to true if
	 * aTriggeredCaseComponent and aTriggeredDataTag are evaluated in a template
	 * script method or if condition contains script methods for which
	 * aTriggeredCaseComponent and aTriggeredDataTag are evaluated
	 *
	 * @param aTriggeredReference              the a triggered reference
	 * @param aConditionTag                    the a condition tag
	 * @param aOpenedItemChildTags             a hashtable with per item child tag
	 *                                         id a tag for opened
	 * @param aOpenedItemFeedbackconditionTags a hashtable with per item feedback
	 *                                         condition tag id a tag for opened
	 * @param aStatusType                      the a status type, see IAppManager
	 * @param aUseTempVars                     if true, then use temporary variables
	 *                                         to speed up condition check
	 * @param aTemplateScript                  if true, then this condition only has
	 *                                         template refers, and triggered data
	 *                                         tag should be refered to by template
	 *
	 * @return true, if successful
	 */
	protected boolean pEvaluateConditionTag(STriggeredReference aTriggeredReference, IXMLTag aConditionTag,
			Hashtable<String, IXMLTag> aOpenedItemChildTags,
			Hashtable<String, IXMLTag> aOpenedItemFeedbackconditionTags, String aStatusType, boolean aUseTempVars,
			boolean aTemplateScript) {
		sSpring.initReplaceTemporaryVarsWithinStrings();
		setPropTemplateTriggerFound(false);
		IXMLTag lConditionStringTag = aConditionTag.getChild("conditionstring");
		List<IXMLTag> lChildTags = lConditionStringTag.getChildTags();
		String lJava = "";
		boolean lIsTriggeredInOneOfTheConditionMethods = false;
		// loop through condition elements
		for (IXMLTag lChildTag : lChildTags) {
			String lName = lChildTag.getName();
			if (lName.equals("parenthesisopen"))
				lJava = lJava + "(";
			else if (lName.equals("parenthesisclose"))
				lJava = lJava + ")";
			else if (lName.equals("and"))
				lJava = lJava + "&&";
			else if (lName.equals("or"))
				lJava = lJava + "||";
			else if (lName.equals("not"))
				lJava = lJava + "!";
			else if (lName.equals("evalrungrouptagstatus")) {
				// check status of content tag(s)
				boolean lOk = false;
				if (isCurrentCarIdWithinCarIds(lChildTag.getChilds("carid"))) {
					List<IECaseComponent> lCaseComponents = getMethodCaseComponents(lChildTag, aTriggeredReference);
					Hashtable<Integer, List<IXMLTag>> lHTagIdTags = new Hashtable<Integer, List<IXMLTag>>();
					String lCacId = lChildTag.getChildValue("cacid");
					if (!lCacId.equals("")) {
						// NOTE make copy of tag id child tag list because pEvaluateTagMethodTag may
						// adjust the list
						List<IXMLTag> lTagIdTags = new ArrayList<IXMLTag>();
						for (IXMLTag lTagIdTag : lChildTag.getChild("cacid").getChilds("tagid")) {
							lTagIdTags.add(lTagIdTag);
						}
						lHTagIdTags.put(Integer.parseInt(lCacId), lTagIdTags);
					}
					// NOTE Below templatesOrOperation indicates an Or operation on the templates
					// but also on the input list of tags and on template field and the tag list.
					// So it should better be called orOperation. However the xml data of older
					// cases contains templatesOrOperation tags, so the tag name cannot be changed.
					lOk = pEvaluateTagMethodTag(aTriggeredReference, lCaseComponents, lChildTag, false,
							CXmlHelper.isMethodUsedInMainCondition(lChildTag)
									&& CXmlHelper.getMethodTagTemplatesOrOperation(lChildTag),
							lHTagIdTags, lChildTag.getChild("cacid").getChilds("tagtemplate"), aOpenedItemChildTags,
							aOpenedItemFeedbackconditionTags, aUseTempVars, aTemplateScript);
					if (aTriggeredReference != null) {
						lIsTriggeredInOneOfTheConditionMethods = lIsTriggeredInOneOfTheConditionMethods
								|| isTriggeredInMethod(aTriggeredReference, lCaseComponents, lChildTag, lHTagIdTags,
										true);
					}
				}
				lJava = lJava + lOk;
			} else if (lName.equals("evalrungroupcomponentstatus")) {
				// check status of component(s)
				boolean lOk = true;
				if (!isCurrentCarIdWithinCarIds(lChildTag.getChilds("carid")))
					// condition not relevant for current case role
					lOk = false;
				else {
					List<IECaseComponent> lCaseComponents = getMethodCaseComponents(lChildTag, aTriggeredReference);
					lOk = pEvaluateComponentMethodTag(aTriggeredReference, lCaseComponents, lChildTag, false,
							aStatusType);
					if (aTriggeredReference != null) {
						lIsTriggeredInOneOfTheConditionMethods = lIsTriggeredInOneOfTheConditionMethods
								|| isTriggeredInMethod(aTriggeredReference, lCaseComponents, lChildTag, null, false);
					}
				}
				lJava = lJava + lOk;
			} else if (lName.equals("evalrungrouptagtemplate")) {
				// check status of content tag(s)
				List<IECaseComponent> lCaseComponents = getMethodTemplateCaseComponents(lChildTag, aTriggeredReference);
				Hashtable<Integer, List<IXMLTag>> lHTagIdTags = new Hashtable<Integer, List<IXMLTag>>();
				boolean lOk = pEvaluateTagMethodTag(aTriggeredReference, lCaseComponents, lChildTag, true,
						CXmlHelper.isMethodUsedInMainCondition(lChildTag)
								&& CXmlHelper.getMethodTagTemplatesOrOperation(lChildTag),
						lHTagIdTags, lChildTag.getChild("comid").getChilds("tagtemplate"), aOpenedItemChildTags,
						aOpenedItemFeedbackconditionTags, aUseTempVars, aTemplateScript);
				if (aTriggeredReference != null) {
					lIsTriggeredInOneOfTheConditionMethods = lIsTriggeredInOneOfTheConditionMethods
							|| isTriggeredInMethod(aTriggeredReference, lCaseComponents, lChildTag, lHTagIdTags, true);
				}
				lJava = lJava + lOk;
			} else if (lName.equals("evalrungroupcomponenttemplate")) {
				// check status of component(s)
				// NOTE use parameter aTriggeredReference.
				// If it is null (in case of a sub condition) multiple case components can be
				// returned and for all these case components this condition has to be valid.
				// If it is not null (in case of a main condition) only one case component will
				// be returned, the triggered one and the condition will be only checked for
				// this case component.
				List<IECaseComponent> lCaseComponents = getMethodTemplateCaseComponents(lChildTag, aTriggeredReference);
				boolean lOk = pEvaluateComponentMethodTag(aTriggeredReference, lCaseComponents, lChildTag, true,
						aStatusType);
				if (aTriggeredReference != null) {
					lIsTriggeredInOneOfTheConditionMethods = lIsTriggeredInOneOfTheConditionMethods
							|| isTriggeredInMethod(aTriggeredReference, lCaseComponents, lChildTag, null, false);
				}
				lJava = lJava + lOk;
			}
		}

		boolean lResult = false;
		if (aTriggeredReference == null || lIsTriggeredInOneOfTheConditionMethods) {
			if (!aTemplateScript || getPropTemplateTriggerFound()) {
				lResult = evaluateLogicalExpression(lJava);
			}
		}
		sSpring.doneReplaceTemporaryVarsWithinStrings(!lResult);
		return lResult;
	}

	/**
	 * Gets method case components.
	 * 
	 * @param aComponentMethodTag the a component method tag
	 * @param aTriggeredReference the a triggered reference
	 *
	 * @return case components
	 */
	private List<IECaseComponent> getMethodCaseComponents(IXMLTag aComponentMethodTag,
			STriggeredReference aTriggeredReference) {
		IECaseComponent lTriggeredCaseComponent = null;
		if (aTriggeredReference != null) {
			lTriggeredCaseComponent = aTriggeredReference.getCaseComponent();
		}
		List<IXMLTag> lCacIdTags = aComponentMethodTag.getChilds("cacid");
		List<IECaseComponent> lCaseComponents = new ArrayList<IECaseComponent>();
		for (IXMLTag lCacIdTag : lCacIdTags) {
			IECaseComponent lCaseComponent = sSpring.getCaseComponent(lCacIdTag.getValue());
			if (lCaseComponent != null) {
				lCaseComponents.add(lCaseComponent);
			}
		}
		return lCaseComponents;
	}

	/**
	 * Gets method template case components.
	 * 
	 * @param aComponentMethodTag the a component method tag
	 * @param aTriggeredReference the a triggered reference
	 *
	 * @return case components
	 */
	private List<IECaseComponent> getMethodTemplateCaseComponents(IXMLTag aComponentMethodTag,
			STriggeredReference aTriggeredReference) {
		IECaseComponent lTriggeredCaseComponent = null;
		if (aTriggeredReference != null) {
			lTriggeredCaseComponent = aTriggeredReference.getCaseComponent();
		}
		String lComId = CXmlHelper.getMethodComId(aComponentMethodTag);
		IEComponent lComponent = sSpring.getComponent(lComId);
		List<IECaseComponent> lCaseComponents = new ArrayList<IECaseComponent>();
		if (lComponent != null) {
			List<IXMLTag> lComponentTemplateTags = aComponentMethodTag.getChild("comid").getChilds("componenttemplate");
			boolean lTemplateOrEvaluate = CXmlHelper.isMethodUsedInMainCondition(aComponentMethodTag)
					&& CXmlHelper.getMethodComponentTemplatesOrOperation(aComponentMethodTag);
			if (lTriggeredCaseComponent != null && lTemplateOrEvaluate) {
				lCaseComponents.add(lTriggeredCaseComponent);
				// filter lCaseComponents on pattern
				lCaseComponents = getTemplateCaseComponents(lCaseComponents, lComponentTemplateTags);
			}
			// NOTE if triggered case component does not result in a template case
			// component, get all case components of the type defined by the component code
			if (lCaseComponents.size() == 0) {
				lCaseComponents = sSpring.getCaseComponentsByComponentCode(lComponent.getCode());
				// filter lCaseComponents on pattern
				lCaseComponents = getTemplateCaseComponents(lCaseComponents, lComponentTemplateTags);
			}
		}
		return lCaseComponents;
	}

	/**
	 * Is triggered in method.
	 *
	 * @param aTriggeredReference the a triggered reference
	 * @param aCaseComponents     the a case components
	 * @param aComponentMethodTag the a component method tag
	 * @param aHTagIdTags         the hashtable tag id tags
	 * @param aTagUsed            the a tag used
	 *
	 * @return true, if so
	 */
	protected boolean isTriggeredInMethod(STriggeredReference aTriggeredReference,
			List<IECaseComponent> aCaseComponents, IXMLTag aComponentMethodTag,
			Hashtable<Integer, List<IXMLTag>> aHTagIdTags, boolean aTagUsed) {
		if (aTriggeredReference == null || aCaseComponents.size() == 0) {
			return false;
		}
		for (IECaseComponent lCaseComponent : aCaseComponents) {
			if (aTriggeredReference.getCaseComponent() != null
					&& lCaseComponent.getCacId() == aTriggeredReference.getCaseComponent().getCacId()) {
				if (!aTagUsed) {
					return true;
				} else if (aHTagIdTags.containsKey(lCaseComponent.getCacId())) {
					// aHTagIdTags contains for each case component the relevant component item id's
					// in the evaluated condition
					if (aTriggeredReference.getDataTag() == null)
						// the component itself is triggered, not a component item
						// can occur in a compound condition, where part of the condition refers to the
						// triggered component, and another part refers to
						// an item of the triggered component
						return false;
					for (IXMLTag lTagIdTag : aHTagIdTags.get(lCaseComponent.getCacId())) {
						if (lTagIdTag.getValue()
								.equals(aTriggeredReference.getDataTag().getAttribute(AppConstants.keyId))) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	/**
	 * Get status key.
	 *
	 * @param aStatusId the a status id
	 *
	 * @return true, if so
	 */
	protected String getStatusKey(String aStatusId) {
		return sSpring.getAppManager().getStatusKey(Integer.parseInt(aStatusId));
	}

	/**
	 * Evaluates tag method tag, checks if the method is true. NOTE method adjusts
	 * aHTagIdTags
	 *
	 * @param aTriggeredReference              the a triggered reference
	 * @param aCaseComponents                  the a case components
	 * @param aComponentMethodTag              the a component method tag
	 * @param aIsTemplateMethod                the a is template method
	 * @param aDoOrEvaluation                  the a do or evaluation
	 * @param aHTagIdTags                      the hashtable tag id tags
	 * @param aTagTemplateTags                 the tag template tags
	 * @param aOpenedItemChildTags             a hashtable with per item child tag
	 *                                         id a tag for opened
	 * @param aOpenedItemFeedbackconditionTags a hashtable with per item feedback
	 *                                         condition tag id a tag for opened
	 * @param aUseTempVars                     if true, then use temporary variables
	 *                                         to speed up condition check
	 * @param aTemplateScript                  if true, then this condition only has
	 *                                         template refers, and triggered data
	 *                                         tag should be refered to by template
	 *
	 * @return true, if successful
	 */
	protected boolean pEvaluateTagMethodTag(STriggeredReference aTriggeredReference,
			List<IECaseComponent> aCaseComponents, IXMLTag aComponentMethodTag, boolean aIsTemplateMethod,
			boolean aDoOrEvaluation, Hashtable<Integer, List<IXMLTag>> aHTagIdTags, List<IXMLTag> aTagTemplateTags,
			Hashtable<String, IXMLTag> aOpenedItemChildTags,
			Hashtable<String, IXMLTag> aOpenedItemFeedbackconditionTags, boolean aUseTempVars,
			boolean aTemplateScript) {
		boolean lOk = false;
		if (aCaseComponents.size() == 0) {
			return lOk;
		}
		IECaseComponent lTriggeredCaseComponent = null;
		IXMLTag lTriggeredDataTag = null;
		if (aTriggeredReference != null) {
			lTriggeredCaseComponent = aTriggeredReference.getCaseComponent();
			lTriggeredDataTag = aTriggeredReference.getDataTag();
		}
		String lTagName = aComponentMethodTag.getChildValue("tagname");
		String lStatusId = aComponentMethodTag.getChildValue("statusid");
		String lFunctionId = aComponentMethodTag.getChildValue("functionid");
		String lValue = aComponentMethodTag.getChildValue("value");
		String lOperatorId = aComponentMethodTag.getChildValue("operatorid");
		List<IXMLTag> lOperatorValueTags = aComponentMethodTag.getChild("operatorid").getChilds("operatorvalue");
		for (IECaseComponent lCaseComponent : aCaseComponents) {
			String lComId = "" + lCaseComponent.getEComponent().getComId();
			String lCacId = "" + lCaseComponent.getCacId();
			String lTempValue = lValue;
			boolean lBoolValueInput = sSpring.getAppManager()
					.getTagOperatorValueType(lComId, lCacId, lTagName, lStatusId, "0").equals("boolean");
			if (lBoolValueInput) {
				if (lTempValue.equals("")) {
					// NOTE To keep supporting old situation where count was a count of value true,
					// set default value to true.
					lTempValue = "1";
				}
				lTempValue = sSpring.getAppManager().getStatusValue(Integer.parseInt(lTempValue));
			}
			boolean lEvaluate = true;
			if (aIsTemplateMethod) {
				// NOTE check if case component is valid for current case role
				lEvaluate = sSpring.getSCaseRoleHelper().getCaseRolesByCacId(lCaseComponent.getCacId()).size() > 0;
			}
			if (lEvaluate) {
				List<IXMLTag> lTagIdTags = null;
				if (aHTagIdTags.containsKey(lCaseComponent.getCacId())) {
					lTagIdTags = aHTagIdTags.get(lCaseComponent.getCacId());
				} else {
					lTagIdTags = new ArrayList<IXMLTag>();
				}
				String lTriggeredTagKeyValue = "";
				if (aTagTemplateTags.size() > 0) {
					// NOTE if triggered and condition case components are identical
					// and triggered data tag name equals lTagName, set lTriggeredTagKey
					// so it can be matched with template tag values containing temporary variables
					boolean lAddTemplatesToIdTags = true;
					if (lTriggeredDataTag == null) {
						// we cannot compare template to triggered tag
						setPropTemplateTriggerFound(true);
					} else {
						if (lTriggeredCaseComponent != null
								&& lCaseComponent.getCacId() == lTriggeredCaseComponent.getCacId()) {
							if (lTriggeredDataTag.getName().equals(lTagName)) {
								lTriggeredTagKeyValue = lTriggeredDataTag
										.getChildValue(lTriggeredDataTag.getDefAttribute(AppConstants.defKeyKey));
							} else {
								lAddTemplatesToIdTags = false;
							}
						}
					}
					if (lAddTemplatesToIdTags || !lTriggeredTagKeyValue.equals("")) {
						lTagIdTags = addTagTemplatesToIdTags(lTagIdTags, aTagTemplateTags, lCaseComponent, lTagName,
								lTriggeredTagKeyValue, aTemplateScript, aDoOrEvaluation);
					}
				}
				if (lTagIdTags.size() > 0) {
					aHTagIdTags.put(lCaseComponent.getCacId(), lTagIdTags);
					lOk = !aDoOrEvaluation;
					String lOperatorValues = "";

					lTriggeredTagKeyValue = "";
					String lKey = getStatusKey(lStatusId);
					if (lTriggeredDataTag != null) {
						// determine the value of the triggered item's status parameter
						/*
						 * NOTE lCaseComponent can be unequal to lTriggeredCaseComponent. If so the line
						 * below could get a tag of another case component with the same id as within
						 * lTriggeredCaseComponent. This could lead to wrong results. But I'm not sure.
						 * IXMLTag lTag = getCaseComponentXmlDataTagById(lCaseComponent,
						 * lTriggeredDataTag.getAttribute(AppConstants.keyId)); if (lTag != null) {
						 * lTriggeredTagKeyValue = lTag.getCurrentStatusAttribute(lKey); }
						 */
						if (lTriggeredCaseComponent != null
								&& lCaseComponent.getCacId() == lTriggeredCaseComponent.getCacId()) {
							lTriggeredTagKeyValue = lTriggeredDataTag.getCurrentStatusAttribute(lKey);
						}
					}
					// check if operatorvalue string implies adding a temporary variable, and if so,
					// set it to the value of the triggered item's status parameter
					for (IXMLTag lOperatorValueTag : lOperatorValueTags) {
						String lOperatorValue = sSpring.replaceTemporaryVarsWithinString(
								sSpring.replaceVariablesWithinString(lOperatorValueTag.getValue()),
								lTriggeredTagKeyValue);
						lOperatorValue = lOperatorValue.replace(",", AppConstants.statusCommaReplace);
						if (!lOperatorValues.equals(""))
							lOperatorValues = lOperatorValues + ",";
						lOperatorValues = lOperatorValues + lOperatorValue;
					}
					boolean lIsStatusValueChangedForItemsCaseComponent = lCaseComponent.getEComponent().getCode()
							.equals("items")
							&& (lKey.equals(AppConstants.statusKeyOpened) || lKey.equals(AppConstants.statusKeyAnswer));
					for (IXMLTag lTagIdTag : lTagIdTags) {
						String lTagId = lTagIdTag.getValue();
						IXMLTag lTag = null;
						if (lIsStatusValueChangedForItemsCaseComponent) {
							if (aOpenedItemChildTags != null) {
								if (aOpenedItemChildTags.containsKey(lTagId)) {
									lTag = aOpenedItemChildTags.get(lTagId);
								}
							}
							if (lTag == null && aOpenedItemFeedbackconditionTags != null) {
								if (aOpenedItemFeedbackconditionTags.containsKey(lTagId)) {
									lKey = AppConstants.statusKeyFeedbackConditionId;
									lTag = aOpenedItemFeedbackconditionTags.get(lTagId);
								}
							}
						}
						if (lTag == null) {
							lTag = sSpring.getCaseComponentXmlDataTagById(lCaseComponent, lTagId);
						}
						boolean lIsStatusValueOk = isStatusValueOk(aTriggeredReference, lCaseComponent, lTag, lKey,
								lStatusId, lFunctionId, lTempValue, lOperatorId, lOperatorValues);
						if (aDoOrEvaluation) {
							// condition part has to be valid for one tag
							lOk = lOk || lIsStatusValueOk;
						} else {
							// condition part has to be valid for all tags
							lOk = lOk && lIsStatusValueOk;
						}
					}
				}
			}
		}
		return lOk;
	}

	/**
	 * Evaluates component method tag, checks if the method is true.
	 * 
	 * @param aTriggeredReference the a triggered reference
	 * @param aCaseComponents     the a case components
	 * @param aComponentMethodTag the a component method tag
	 * @param aIsTemplateMethod   the a is template method
	 * @param aStatusType         the a status type, see IAppManager
	 *
	 * @return true, if successful
	 */
	private boolean pEvaluateComponentMethodTag(STriggeredReference aTriggeredReference,
			List<IECaseComponent> aCaseComponents, IXMLTag aComponentMethodTag, boolean aIsTemplateMethod,
			String aStatusType) {
		if (aCaseComponents == null || aCaseComponents.size() == 0) {
			return false;
		}
		String lStatusId = aComponentMethodTag.getChildValue("statusid");
		String lKey = getStatusKey(lStatusId);
		if (lKey.equals("")) {
			return false;
		}
		IECaseComponent lTriggeredCaseComponent = null;
		if (aTriggeredReference != null) {
			lTriggeredCaseComponent = aTriggeredReference.getCaseComponent();
		}
		String lFunctionId = aComponentMethodTag.getChildValue("functionid");
		String lValue = aComponentMethodTag.getChildValue("value");
		String lOperatorId = aComponentMethodTag.getChildValue("operatorid");
		List<IXMLTag> lOperatorValueTags = aComponentMethodTag.getChild("operatorid").getChilds("operatorvalue");
		String lOperatorValues = "";
		for (IXMLTag lOperatorValueTag : lOperatorValueTags) {
			String lOperatorValue = sSpring.replaceTemporaryVarsWithinString(
					sSpring.replaceVariablesWithinString(lOperatorValueTag.getValue()), "");
			lOperatorValue = lOperatorValue.replace(",", AppConstants.statusCommaReplace);
			if (!lOperatorValues.equals(""))
				lOperatorValues = lOperatorValues + ",";
			lOperatorValues = lOperatorValues + lOperatorValue;
		}
		boolean lEvaluated = false;
		boolean lOk = true;
		for (IECaseComponent lCaseComponent : aCaseComponents) {
			boolean lEvaluate = true;
			if (aIsTemplateMethod) {
				// NOTE check if case component is valid for current case role
				lEvaluate = sSpring.getSCaseRoleHelper().getCaseRolesByCacId(lCaseComponent.getCacId()).size() > 0;
			}
			if (lEvaluate) {
				String lComId = "" + lCaseComponent.getEComponent().getComId();
				String lCacId = "" + lCaseComponent.getCacId();
				String lTempValue = lValue;
				boolean lBoolValueInput = sSpring.getAppManager()
						.getTagOperatorValueType(lComId, lCacId, AppConstants.componentElement, lStatusId, "0")
						.equals("boolean");
				if (lBoolValueInput) {
					if (lTempValue.equals("")) {
						// NOTE To keep supporting old situation where count was a count of value true,
						// set default value to true.
						lTempValue = "1";
					}
					lTempValue = sSpring.getAppManager().getStatusValue(Integer.parseInt(lTempValue));
				}
				// condition part has to be valid for all cacs
				lOk = lOk && isStatusValueOk(aTriggeredReference, lCaseComponent,
						sSpring.getComponentDataStatusTag(lCaseComponent, aStatusType), lKey, lStatusId, lFunctionId,
						lTempValue, lOperatorId, lOperatorValues);
				lEvaluated = true;
			}
		}
		return lEvaluated && lOk;
	}

	/**
	 * Evaluates component method tag, checks if the condition is true.
	 * 
	 * @param aCaseComponents     the a case components
	 * @param aComponentMethodTag the a component method tag
	 * @param aStatusType         the a status type, see IAppManager
	 *
	 * @return true, if successful
	 */
	private List<IXMLTag> getCarIdTags(IECaseComponent aCaseComponent) {
		List<IXMLTag> lCarIdTags = new ArrayList<IXMLTag>();
		if (aCaseComponent == null) {
			return lCarIdTags;
		}
		List<IECaseRole> lCaseRoles = sSpring.getSCaseRoleHelper().getCaseRolesByCacId(aCaseComponent.getCacId());
		for (IECaseRole lCaseRole : lCaseRoles) {
			lCarIdTags.add(sSpring.getXmlManager().newXMLTag("carid", "" + lCaseRole.getCarId()));
		}
		return lCarIdTags;
	}

	/**
	 * Executes action tag. This implies setting a status value for a status key of
	 * a run tag or a run component. The setting of this value in it self will be
	 * checked in script, so can trigger other conditions to fire.
	 *
	 * @param aTriggeredReference the a triggered reference
	 * @param aActionTag          the a action tag
	 * @param aStatusType         the a status type, see IAppManager
	 * @param aSaveInDb           the a save in db, whether status change is saved
	 *                            in database, or only in memory
	 *
	 * @return true, if successful
	 */
	private boolean executeActionTag(STriggeredReference aTriggeredReference, IXMLTag aActionTag, String aStatusType,
			boolean aSaveInDb) {
		boolean lResult = true;
		IXMLTag lActionStringTag = aActionTag.getChild("actionstring");
		IXMLTag lSetRunGroupTagStatus = lActionStringTag.getChild("setrungrouptagstatus");
		IXMLTag lSetRunGroupComponentStatus = lActionStringTag.getChild("setrungroupcomponentstatus");
		IXMLTag lSetRunGroupTagTemplate = lActionStringTag.getChild("setrungrouptagtemplate");
		IXMLTag lSetRunGroupComponentTemplate = lActionStringTag.getChild("setrungroupcomponenttemplate");
		if (lSetRunGroupTagStatus != null) {
			// set status for content tag(s)
			IXMLTag lChildTag = lSetRunGroupTagStatus;
			executeActionTagMethodTag(aTriggeredReference, getMethodCaseComponents(lChildTag, null), lChildTag,
					lChildTag.getChilds("carid"), lChildTag.getChild("cacid").getChilds("tagid"),
					lChildTag.getChild("cacid").getChilds("tagtemplate"), aStatusType, aSaveInDb);
		} else if (lSetRunGroupComponentStatus != null) {
			// set status for component(s)
			IXMLTag lChildTag = lSetRunGroupComponentStatus;
			executeActionComponentMethodTag(getMethodCaseComponents(lChildTag, null), lChildTag,
					lChildTag.getChilds("carid"), aStatusType, aSaveInDb);
		} else if (lSetRunGroupTagTemplate != null) {
			IXMLTag lChildTag = lSetRunGroupTagTemplate;
			executeActionTagMethodTag(aTriggeredReference, getMethodTemplateCaseComponents(lChildTag, null), lChildTag,
					null, null, lChildTag.getChild("comid").getChilds("tagtemplate"), aStatusType, aSaveInDb);
		} else if (lSetRunGroupComponentTemplate != null) {
			// set status for component(s)
			IXMLTag lChildTag = lSetRunGroupComponentTemplate;
			executeActionComponentMethodTag(getMethodTemplateCaseComponents(lChildTag, null), lChildTag, null,
					aStatusType, aSaveInDb);
		}
		return lResult;
	}

	/**
	 * Executes action tag method tag.
	 *
	 * @param aTriggeredReference     the a triggered reference
	 * @param aCaseComponents         the a case components
	 * @param aComponentMethodTag     the a component method tag
	 * @param aTriggeredCaseComponent the triggered a case component
	 * @param aTriggeredDataTag       the triggered a data tag
	 * @param aActionTag              the a action tag
	 * @param aStatusType             the a status type, see IAppManager
	 * @param aSaveInDb               the a save in db, whether status change is
	 *                                saved in database, or only in memory
	 */
	private void executeActionTagMethodTag(STriggeredReference aTriggeredReference,
			List<IECaseComponent> aCaseComponents, IXMLTag aComponentMethodTag, List<IXMLTag> aCarIdTags,
			List<IXMLTag> aTagIdTags, List<IXMLTag> aTagTemplateTags, String aStatusType, boolean aSaveInDb) {
		if (aCaseComponents.size() == 0) {
			return;
		}
		String lTagName = aComponentMethodTag.getChildValue("tagname");
		String lStatusId = aComponentMethodTag.getChildValue("statusid");
		String lFunctionId = aComponentMethodTag.getChildValue("functionid");
		String lOperatorValue = aComponentMethodTag.getChildValue("operatorvalue");
		for (IECaseComponent lCaseComponent : aCaseComponents) {
			boolean lExecute = true;
			if (aTagIdTags == null) {
				aTagIdTags = new ArrayList<IXMLTag>();
				// NOTE check if case component is valid for current case role
				lExecute = sSpring.getSCaseRoleHelper().getCaseRolesByCacId(lCaseComponent.getCacId()).size() > 0;
			}
			if (lExecute) {
				String lTriggeredTagKey = "";
				if (aTagTemplateTags.size() > 0) {
					// NOTE if triggered and action case components are identical
					// and triggered data tag name equals lTagName, set lTriggeredTagKey
					// so it can be matched with template tag values containing temporary variables
					if (aTriggeredReference.getCaseComponent() != null
							&& lCaseComponent.getCacId() == aTriggeredReference.getCaseComponent().getCacId()
							&& aTriggeredReference.getDataTag() != null
							&& aTriggeredReference.getDataTag().getName().equals(lTagName)) {
						lTriggeredTagKey = aTriggeredReference.getDataTag().getChildValue(
								aTriggeredReference.getDataTag().getDefAttribute(AppConstants.defKeyKey));
					}
					aTagIdTags = addTagTemplatesToIdTags(aTagIdTags, aTagTemplateTags, lCaseComponent, lTagName,
							lTriggeredTagKey, false, false);
				}
				IXMLTag lContentTag = null;
				if (aTagIdTags.size() > 0) {
					IXMLTag lRootTag = sSpring.getXmlDataPlusRunStatusTree(lCaseComponent, aStatusType);
					if (lRootTag != null) {
						lContentTag = lRootTag.getChild(AppConstants.contentElement);
					}
				}
				String lTempOperatorValue = sSpring.replaceTemporaryVarsWithinString(
						sSpring.replaceVariablesWithinString(lOperatorValue), lTriggeredTagKey);
				String lKey = getStatusKey(lStatusId);
				String lValue = "";
				// NOTE only convert to 'true' or 'false' if key is of type 'boolean'
				if (sSpring.getAppManager()
						.getTagOperatorValueType("", "" + lCaseComponent.getCacId(), lTagName, lStatusId, lFunctionId)
						.equals("boolean"))
					// NOTE converts value 0 to 'false' and value 1 to 'true'
					lValue = sSpring.getAppManager().getStatusValue(Integer.parseInt(lTempOperatorValue));
				else
					lValue = lTempOperatorValue;
				for (IXMLTag lTagIdTag : aTagIdTags) {
					String lTagId = lTagIdTag.getValue();
					if (!(lCaseComponent == null || lTagId.equals("") || lKey.equals(""))) {
						IXMLTag lDataStatusTag = sSpring.getDataStatusTag(lCaseComponent, lTagId, aStatusType);
						String lOldValue = "";
						if (lDataStatusTag != null) {
							lOldValue = lDataStatusTag.getCurrentStatusAttribute(lKey);
						}
						boolean lSetRunTagStatus = true;
						if (lFunctionId.equals("") && (lKey.equals(AppConstants.statusKeyPresent)
								|| lKey.equals(AppConstants.statusKeyAccessible))) {
							// NOTE If function is empty (no count) and present or accessible is set,
							// then check if new value is equal to old value. If so, don't set present or
							// accessible.
							// This prevents unnecessary changes in present or accessible possibly resulting
							// in rerendering of part of player.
							// This implies that the count function cannot be used reliable anymore on
							// present or accessible.
							// But we only used it on student actions like selected, opened or sent, so
							// there should be no problem.
							lSetRunTagStatus = !lValue.equals(lOldValue);
						}
						if (lSetRunTagStatus) {
							String lNewValue = applyFunctionOnStatusValue(lCaseComponent, lTagIdTag.getName(),
									lStatusId, lFunctionId, lValue, lOldValue);
							if (aCarIdTags == null) {
								// if aCarIdTags not given (in case of template) get case role ids for case
								// component
								aCarIdTags = getCarIdTags(lCaseComponent);
							}
							if (isCurrentCarIdWithinCarIds(aCarIdTags)) {
								// action relevant for current case role
								sSpring.setRunTagStatus(lCaseComponent, lTagId, "", lKey, lNewValue, null, true, null,
										aStatusType, aSaveInDb, true);
							}
							if (aSaveInDb) {
								List<Integer> lCarIdsUnequalToCurrentCarId = getCarIdsUnequalToCurrentCarId(aCarIdTags);
								if (lCarIdsUnequalToCurrentCarId.size() > 0) {
									IXMLTag lDataTag = sSpring.getCaseComponentXmlDataTagById(lCaseComponent, lTagId);
									if (lDataTag != null) {
										sSpring.getSUpdateHelper().executeActionTagForOtherCaseRoles(
												lCarIdsUnequalToCurrentCarId, lCaseComponent.getCacId(), lTagId,
												lDataTag.getName(), lKey, lNewValue, aStatusType);
									}
								}
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Executes action component method tag.
	 *
	 * @param aCaseComponents     the a case components
	 * @param aComponentMethodTag the a component method tag
	 * @param aCarIdTags          the a car id tags
	 * @param aStatusType         the a status type, see IAppManager
	 * @param aSaveInDb           the a save in db, whether status change is saved
	 *                            in database, or only in memory
	 */
	private void executeActionComponentMethodTag(List<IECaseComponent> aCaseComponents, IXMLTag aComponentMethodTag,
			List<IXMLTag> aCarIdTags, String aStatusType, boolean aSaveInDb) {
		if (aCaseComponents.size() == 0) {
			return;
		}
		String lStatusId = aComponentMethodTag.getChildValue("statusid");
		String lFunctionId = aComponentMethodTag.getChildValue("functionid");
		String lOperatorValue = sSpring.replaceTemporaryVarsWithinString(
				sSpring.replaceVariablesWithinString(aComponentMethodTag.getChildValue("operatorvalue")), "");
		String lKey = getStatusKey(lStatusId);
		// TODO handle component non-boolean status type
		String lValue = sSpring.getAppManager().getStatusValue(Integer.parseInt(lOperatorValue));
		if (lKey.equals("") || lValue.equals("")) {
			return;
		}
		for (IECaseComponent lCaseComponent : aCaseComponents) {
			IXMLTag lDataStatusTag = sSpring.getComponentDataStatusTag(lCaseComponent, aStatusType);
			String lOldValue = lDataStatusTag.getCurrentStatusAttribute(lKey);
			boolean lSetRunComponentStatus = true;
			if (lFunctionId.equals("")
					&& (lKey.equals(AppConstants.statusKeyPresent) || lKey.equals(AppConstants.statusKeyAccessible))) {
				// NOTE If function is empty (no count) and present or accessible is set,
				// then check if new value is equal to old value. If so, don't set present or
				// accessible.
				// This prevents unnecessary changes in present or accessible possibly resulting
				// in rerendering of part of player.
				// This implies that the count function cannot be used reliable anymore on
				// present or accessible.
				// But we only used it on student actions like selected, opened or sent, so
				// there should be no problem.
				lSetRunComponentStatus = !lValue.equals(lOldValue);
			}
			if (lSetRunComponentStatus) {
				String lNewValue = applyFunctionOnStatusValue(lCaseComponent, "cacid", lStatusId, lFunctionId, lValue,
						lOldValue);
				if (aCarIdTags == null) {
					// if aCarIdTags not given (in case of template) get case role ids for case
					// component
					aCarIdTags = getCarIdTags(lCaseComponent);
				}
				if (isCurrentCarIdWithinCarIds(aCarIdTags)) {
					// action relevant for current case role
					STriggeredReference lTriggeredReference = new STriggeredReference();
					lTriggeredReference.setCaseComponent(lCaseComponent);
					lTriggeredReference.setStatusKey(lKey);
					lTriggeredReference.setStatusValue(lNewValue);
					sSpring.setRunComponentStatus(lTriggeredReference, true, aStatusType, aSaveInDb, true);
				}
				if (aSaveInDb) {
					List<Integer> lCarIdsUnequalToCurrentCarId = getCarIdsUnequalToCurrentCarId(aCarIdTags);
					if (lCarIdsUnequalToCurrentCarId.size() > 0) {
						sSpring.getSUpdateHelper().executeActionTagForOtherCaseRoles(lCarIdsUnequalToCurrentCarId,
								lCaseComponent.getCacId(), "", AppConstants.componentElement, lKey, lNewValue,
								aStatusType);
					}
				}
			} else {
				if (!lKey.equals(AppConstants.statusKeyCurrentTime))
					// log the status change
					sSpring.getSLogHelper().logSetRunComponentAction("setruncomponentstatus", aStatusType,
							sSpring.getXmlManager().unescapeXML(lCaseComponent.getName()), lKey, lValue, true);
			}
		}
	}

	/**
	 * Evaluates assessment feedback condition tag, checks if the condition is true.
	 * A condition can be compound and can contain logical expressions. All
	 * condition elements are children of the condition tag. Condition tag does not
	 * contain evalrungroupcomponentstatus tags, so these are not checked. Condition
	 * parts are evaluated to true or false and in this way a logical Java
	 * expression is created that will be evaluated.
	 *
	 * @param aFeedbackConditionTag            the a condition tag
	 * @param aOpenedItemChildTags             a hashtable with per item child tag
	 *                                         id a tag for opened
	 * @param aOpenedItemFeedbackconditionTags a hashtable with per item feedback
	 *                                         condition tag id a tag for opened
	 * @param aStatusType                      the a status type, see IAppManager
	 *
	 * @return true, if successful
	 */
	public boolean evaluateAssessmentConditionTag(IXMLTag aFeedbackConditionTag,
			Hashtable<String, IXMLTag> aOpenedItemChildTags,
			Hashtable<String, IXMLTag> aOpenedItemFeedbackconditionTags, String aStatusType) {
		return pEvaluateConditionTag(null, aFeedbackConditionTag, aOpenedItemChildTags,
				aOpenedItemFeedbackconditionTags, aStatusType, false, false);
	}

	/**
	 * Checks if status value is ok, if status given by first six parameters
	 * corresponds to values given within aStatusArr.
	 *
	 * @param aTriggeredReference the a triggered reference
	 * @param aCaseComponent      the a case component
	 * @param aDataTag            the a data tag
	 * @param aKey                the a key
	 * @param aStatusId           the a status key id
	 * @param aFunctionId         the a function id
	 * @param aValue              the a value
	 * @param aOperatorId         the a operator id
	 * @param aOperatorValues     the a operator values
	 *
	 * @return true, if status value is ok
	 */
	private boolean isStatusValueOk(STriggeredReference aTriggeredReference, IECaseComponent aCaseComponent,
			IXMLTag aDataTag, String aKey, String aStatusId, String aFunctionId, String aValue, String aOperatorId,
			String aOperatorValues) {
		if (aStatusId.equals(""))
			// status key cannot be empty
			return false;
		if (aOperatorId.equals(""))
			// operator key cannot be empty
			return false;
		String lComId = "" + aCaseComponent.getEComponent().getComId();
		String lCacId = "" + aCaseComponent.getCacId();
		String lTagName = "";
		if (aDataTag != null) {
			lTagName = aDataTag.getName();
		}
		boolean lStringValueInput = sSpring.getAppManager()
				.getTagOperatorValueType(lComId, lCacId, lTagName, aStatusId, "0").equals("string");
		if (aOperatorValues.equals("") && !lStringValueInput)
			// value(s) cannot be empty, except string values
			return false;
		if (aFunctionId.equals(""))
			// option no function
			aFunctionId = "0";
		String lFunction = sSpring.getAppManager().getFunction(Integer.parseInt(aFunctionId));
		String lOperator = sSpring.getAppManager().getOperator(Integer.parseInt(aOperatorId));
		double lStatus = -1;
		String lStatusStr = "@@@";
		String lInputType = "";
		Boolean lNumberError = false;
		Boolean lNumberCheck = false;
		if (aDataTag != null) {
			if (lFunction.equals("")) {
				if (aCaseComponent.getEComponent().getCode().equals("states") && lTagName.equals("state")) {
					// NOTE States have their own type which is saved in value
					int lStatusKeyIndex = -1;
					if (!aStatusId.equals(""))
						lStatusKeyIndex = Integer.parseInt(aStatusId);
					if (lStatusKeyIndex == statusKeyValueIndex) {
						String lStateType = aDataTag.getChildValue("keytype");
						if (lStateType.equals("string")) {
							lInputType = "string";
						} else if (lStateType.equals("boolean")) {
							lInputType = "boolean";
						} else if (lStateType.equals("number")) {
							lInputType = "int";
						} else if (lStateType.equals("time")) {
							lInputType = "time";
						}
					} else {
						lInputType = sSpring.getAppManager().getTagOperatorValueType(lComId, lCacId, lTagName,
								aStatusId, aFunctionId);
					}
				} else {
					lInputType = sSpring.getAppManager().getTagOperatorValueType(lComId, lCacId, lTagName, aStatusId,
							aFunctionId);
				}
				String lResolvedStatus = sSpring.replaceVariablesWithinString(aDataTag.getCurrentStatusAttribute(aKey));
				if (lInputType.equalsIgnoreCase("string")) {
					lStatusStr = lResolvedStatus;
					// NOTE replace possible crlf constants by \n, status value may be open question
					// answer containing crlfs.
					lStatusStr = lStatusStr.replace(AppConstants.statusCrLfReplace, "\n");
				} else if (lInputType.equalsIgnoreCase("int")) {
					lNumberCheck = true;
					try {
						lStatus = Double.parseDouble(lResolvedStatus);
					} catch (NumberFormatException e) {
						lNumberError = true;
					}
				} else {
					if (lInputType.equalsIgnoreCase("boolean")) {
						lStatus = sSpring.getAppManager().getStatusValueIndex(lResolvedStatus);
					} else {
						lNumberCheck = true;
						try {
							lStatus = Double.parseDouble("" + aDataTag.getCurrentStatusAttributeTime(aKey));
						} catch (NumberFormatException e) {
							lNumberError = true;
						}
					}
				}
			}
			if (lFunction.equals("count")) {
				// NOTE 'count' function only checks number of aValue status values
				// so if trigger equals tag to be checked (and thus status key of trigger equals
				// status key of testtag) and status value of trigger != aValue, don't check
				// count!
				// NOTE To keep supporting old situation where count was a count of value true,
				// set default value to true.
				if (aValue.equals("")) {
					aValue = AppConstants.statusValueTrue;
				}
				boolean lCheckCount = aTriggeredReference == null;
				if (!lCheckCount) {
					// trigger not null; check status value; if true, always check count
					lCheckCount = aTriggeredReference.getStatusValue().equals(aValue);
					if (!lCheckCount) {
						// status value is not 'true'; now only check count if trigger is not equal to
						// tag to be checked
						lCheckCount = !(aTriggeredReference.getCaseComponent().getCacId() == aCaseComponent.getCacId());
						if (!lCheckCount) {
							// components are equal; now compare item tags
							lCheckCount = aTriggeredReference.getDataTag() != null && (aDataTag == null
									|| !aTriggeredReference.getDataTag().getAttribute(AppConstants.keyId)
											.equals(aDataTag.getAttribute(AppConstants.keyId)));
						}
					}
				}
				if (lCheckCount) {
					lStatus = aDataTag.getStatusAttributeCount(aKey, aValue);
				}
			}
		}

		if (lNumberError || (!lNumberCheck && (lStatus == -1) && !lInputType.equalsIgnoreCase("string")))
			return false;
		String[] lOperatorValueArr = aOperatorValues.split(",");
		for (int i = 0; i < lOperatorValueArr.length; i++) {
			if (lOperatorValueArr[i].contains(AppConstants.statusCommaReplace))
				lOperatorValueArr[i] = lOperatorValueArr[i].replace(AppConstants.statusCommaReplace, ",");
		}
		if ((lOperator.equals("eq")) || (lOperator.equals("gt")) || (lOperator.equals("ge")) || (lOperator.equals("lt"))
				|| (lOperator.equals("le"))) {
			if (lOperatorValueArr.length != 1) {
				// must be one value
				return false;
			}
			if (lInputType.equalsIgnoreCase("string")) {
				String lOperatorValue = lOperatorValueArr[0];
				if (lOperator.equals("eq")) {
					if (lStatusStr.indexOf("\n") == -1) {
						return lStatusStr.matches(lOperatorValue);
					} else {
						// NOTE to match on line terminators pattern has to be used with DOTALL
						Pattern lPattern = Pattern.compile(lOperatorValue, Pattern.DOTALL);
						Matcher lMatcher = lPattern.matcher(lStatusStr);
						return lMatcher.matches();
					}
				} else if ((lOperator.equals("gt") || lOperator.equals("ge"))
						&& lStatusStr.compareTo(lOperatorValue) > 0) {
					return true;
				} else if ((lOperator.equals("lt") || lOperator.equals("le"))
						&& lStatusStr.compareTo(lOperatorValue) < 0) {
					return true;
				}
				return false;
			}
			// and must be number
			double lOperatorValue = -1;
			String lOperatorValueString = lOperatorValueArr[0];
			try {
				lOperatorValue = Double.parseDouble(lOperatorValueString);
			} catch (NumberFormatException e) {
				if (lInputType.equalsIgnoreCase("boolean")) {
					if (lOperatorValueString.equalsIgnoreCase(statusValueTrue))
						lOperatorValue = 1;
					else
						// assume false
						lOperatorValue = 0;
				} else
					// not a number
					return false;
			}
			if (lOperator.equals("eq") && lStatus == lOperatorValue)
				return true;
			else if (lOperator.equals("gt") && lStatus > lOperatorValue)
				return true;
			else if (lOperator.equals("ge") && lStatus >= lOperatorValue)
				return true;
			else if (lOperator.equals("lt") && lStatus < lOperatorValue)
				return true;
			else if (lOperator.equals("le") && lStatus <= lOperatorValue)
				return true;
		} else if (lOperator.equals("interval")) {
			if (lOperatorValueArr.length == 1) {
				lOperatorValueArr = lOperatorValueArr[0].split(",");
			}
			if (lOperatorValueArr.length != 2)
				// must be two values
				return false;
			// and must be numbers
			double lMin = Double.parseDouble(lOperatorValueArr[0]);
			double lMax = Double.parseDouble(lOperatorValueArr[1]);
			if ((lStatus >= lMin) && (lStatus <= lMax))
				return true;
		} else if (lOperator.equals("in")) {
			for (int i = 0; i < lOperatorValueArr.length; i++) {
				String lIns = lOperatorValueArr[i];
				String[] lInsArr = lIns.split(",");
				for (int j = 0; j < lInsArr.length; j++) {
					double lValue = Double.parseDouble(lInsArr[j]);
					if (lStatus == lValue)
						return true;
				}
			}
		}
		return false;
	}

	/**
	 * If applicable does function on value. aCacId and aTagName are necessary to
	 * get the type of the status key value for a specific case component and tag.
	 *
	 * @param aCaseComponent the a case component
	 * @param aTagName       the a tag name
	 * @param aStatusId      the a status id
	 * @param aFunctionId    the a function id
	 * @param aOperatorValue the a operator value
	 * @param aStatusValue   the a status value
	 *
	 * @return new value
	 */
	private String applyFunctionOnStatusValue(IECaseComponent aCaseComponent, String aTagName, String aStatusId,
			String aFunctionId, String aOperatorValue, String aStatusValue) {
		if (aStatusId.equals("") || aOperatorValue.equals("") || aFunctionId.equals(""))
			return aOperatorValue;
		String lFunction = sSpring.getAppManager().getFunction(Integer.parseInt(aFunctionId));
		if (aStatusValue != null) {
			if ((lFunction.equals("inc")) || (lFunction.equals("dec"))) {
				if (aStatusValue.equals(""))
					aStatusValue = "0";
				if (lFunction.equals("inc"))
					return "" + (Integer.parseInt(aStatusValue) + Integer.parseInt(aOperatorValue));
				if (lFunction.equals("dec"))
					return "" + (Integer.parseInt(aStatusValue) - Integer.parseInt(aOperatorValue));
			}
		}
		return aOperatorValue;
	}

}
