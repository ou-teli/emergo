/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.axisspring;

import java.util.Hashtable;
import java.util.List;

import org.springframework.context.ApplicationContext;

import nl.surf.emergo.business.IAppManager;
import nl.surf.emergo.control.run.CRunWnd;

/**
 * The Class AxisSSpringPerRgaId.
 * 
 * This class is used to get a SSpring instance per rga id.
 * Normally when the Emergo player is loaded there is one SSpring instance created for
 * the run group account opening the player.
 * When using webservices for instance there is no Emergo player started, but there
 * still is a SSpring instance necessary to load and save xml data/status and to use
 * the script component(s). This class provides this SSpring instance, so webservices
 * can use the Emergo engine.
 * To prevent the SSpring instance to be loaded for every webservice call, it is saved
 * in application memory and will be used for every following webservice call for the
 * same run group account.
 */
public class AxisSSpringPerRgaId{

	/** The application context. */
	protected ApplicationContext applicationContext;

	/** The axis sspring. */
	protected AxisSSpring axisSSpring = null;

	/**
	 * Instantiates a new axis s spring per rga id.
	 * 
	 * @param aApplicationContext the a application context
	 */
	public AxisSSpringPerRgaId(ApplicationContext aApplicationContext) {
		super();
		applicationContext = aApplicationContext;
	}

	/**
	 * Gets the bean.
	 * 
	 * @param aBean the a bean
	 * 
	 * @return the bean
	 */
	public Object getBean(String aBean) {
		if (applicationContext != null)
			return applicationContext.getBean(aBean);
		return null;
	}

	/**
	 * Gets the app container.
	 * 
	 * @return the app container
	 */
	public Hashtable<String,Object> getAppContainer() {
		return ((IAppManager)getBean("appManager")).getAppContainer();
	}

	/**
	 * Gets the axis s spring.
	 * 
	 * @param aRgaId the a rga id
	 * 
	 * @return the axis s spring
	 */
	public AxisSSpring getAxisSSpring(String aRgaId) {
		if (axisSSpring == null) {
			axisSSpring = new AxisSSpring(applicationContext);
		}
		axisSSpring.setRgaIdSSpring(null);
		if (!(getAppContainer().get("activeRunWnds") == null)) {
			Hashtable<String,List<CRunWnd>> lActiveRunWnds = (Hashtable<String,List<CRunWnd>>)getAppContainer().get("activeRunWnds"); 
			List<CRunWnd> lActiveRunWndsPerRgaId = lActiveRunWnds.get(aRgaId);
			if (!(lActiveRunWndsPerRgaId == null) && (lActiveRunWndsPerRgaId.size() > 0)) {
				CRunWnd lActiveRunWnd = lActiveRunWndsPerRgaId.get(lActiveRunWndsPerRgaId.size() - 1);
				axisSSpring.setRgaIdSSpring(lActiveRunWnd.sSpring);
			}
		};
		return axisSSpring;
	}

}
