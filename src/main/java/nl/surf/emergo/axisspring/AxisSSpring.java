/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.axisspring;


import java.util.Hashtable;

import org.springframework.context.ApplicationContext;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CCaseHelper;
import nl.surf.emergo.control.CServicesCaseHelper;
import nl.surf.emergo.control.run.CRunStatusChange;
import nl.surf.emergo.control.run.CRunWnd;
import nl.surf.emergo.control.script.CScript;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.zkspring.SReplaceVariablesWithinStringHelper;
import nl.surf.emergo.zkspring.SSpring;

/**
 * The Class AxisSSpring.
 *
 * The class SSpring uses the application context of the ZK Framework to get beans,
 * that is references to the manager classes within the business package.
 * This does not work when ZK is not used, for instance when using web services like
 * AXIS. Then the AXIS application context has to be used.
 *
 * Also used for updating all players if status changes.
 * And for getting case time of SSpring object belonging to player.
 */
public class AxisSSpring extends SSpring{

	private static final long serialVersionUID = -372463870608349254L;
	
	/** The application context. */
	protected ApplicationContext applicationContext;

	/** The rgaId SSpring. */
	private SSpring rgaIdSSpring = null;

	/** The CScript. */
	protected CScript cScript = null;

	/** The replace variables within script helper. */
	protected SReplaceVariablesWithinStringHelper replaceVariablesWithinStringHelper = null;

	/**
	 * Instantiates a new axis s spring.
	 *
	 * @param aApplicationContext the a application context
	 */
	public AxisSSpring(ApplicationContext aApplicationContext) {
		super();
		applicationContext = aApplicationContext;
	}

	/**
	 * Gets the case helper.
	 *
	 * @return the case helper
	 */
	@Override
	protected CCaseHelper getCaseHelper() {
		return caseHelper;
	}

	/**
	 * Sets the case helper.
	 *
	 */
	private void setCaseHelper(SSpring aSpring) {
		caseHelper = new CServicesCaseHelper(aSpring);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.zkspring.SSpring#getBean(java.lang.String)
	 */
	@Override
	public Object getBean(String aBean) {
		if (applicationContext != null)
			return applicationContext.getBean(aBean);
		return null;
	}
	
	/**
	 * set the Rga Id SSpring class.
	 *
	 * @param sSpring the SSpring class for the Rga Id
	 */
	public void setRgaIdSSpring (SSpring sSpring) {
		rgaIdSSpring = sSpring;
		setCaseHelper(this);
		setReplaceVariablesWithinStringHelper();
	}

	/**
	 * get the Rga Id SSpring class.
	 *
	 * @return the SSpring class for the Rga Id
	 */
	public SSpring getRgaIdSSpring () {
		return rgaIdSSpring;
	}

	/**
	 * Gets the prop cScript.
	 *
	 * @return the prop cScript
	 */
	@Override
	public CScript getPropCScript() {
		if (cScript == null)
			cScript = new CScript(this);
		return cScript;
	}

	/**
	 * Sets the ReplaceVariablesWithinStringHelper.
	 *
	 */
	private void setReplaceVariablesWithinStringHelper() {
		replaceVariablesWithinStringHelper = new SReplaceVariablesWithinStringHelper(this);
	}

	/**
	 * Gets the prop replaceVariablesWithinStringHelper.
	 *
	 * @return the prop replaceVariablesWithinStringHelper
	 */
	@Override
	protected SReplaceVariablesWithinStringHelper getSReplaceVariablesWithinStringHelper () {
		if (replaceVariablesWithinStringHelper == null)
			setReplaceVariablesWithinStringHelper();
		return replaceVariablesWithinStringHelper;
	}

	/**
	 * Run group tag change to player.
	 * One run group account can have started more then one player.
	 * All players will have to be updated. Current players are saved in application memory.
	 *
	 * @param aCaseComponent the a case component
	 * @param aTag the a tag
	 * @param aStatusTag the a status tag
	 * @param aStatusKey the a status key
	 * @param aStatusValue the a status value
	 * @param aFromScript the a from script, whether this method is called from within script, so is a result of a script action
	 */
	@Override
	protected void runGroupTagChangeToPlayer(IECaseComponent aCaseComponent, IXMLTag aTag,
			IXMLTag aStatusTag, String aStatusKey, String aStatusValue, boolean aFromScript) {
		// update all run windows
		IERunGroupAccount lRunGroupAccount = getRunGroupAccount();
		if (lRunGroupAccount == null) return;
		String lKey = "" + lRunGroupAccount.getRgaId();
		for (CRunWnd lRunWnd : getAllActiveRunWndsPerRgaId(lKey)) {
			CRunStatusChange lRunStatusChange = new CRunStatusChange();
			lRunStatusChange.setCaseComponent(aCaseComponent);
			lRunStatusChange.setDataTag(aTag);
			lRunStatusChange.setStatusTag(aStatusTag);
			lRunStatusChange.setStatusKey(aStatusKey);
			lRunStatusChange.setStatusValue(aStatusValue);
			lRunStatusChange.setFromScript(aFromScript);
			getExternalStatusChangesPerRgaIdAndDesktopId(lRunWnd.rgaId, lRunWnd.desktopId).add(lRunStatusChange);
		}
	}

	/**
	 * Run group component change to player.
	 * One run group account can have started more then one player.
	 * All players will have to be updated. Current players are saved in application memory.
	 *
	 * @param aCaseComponent the a case component
	 * @param aStatusKey the a status key
	 * @param aStatusValue the a status value
	 * @param aFromScript the a from script, whether this method is called from within script, so is a result of a script action
	 */
	@Override
	protected void runGroupComponentChangeToPlayer(IECaseComponent aCaseComponent,
			String aStatusKey, String aStatusValue, boolean aFromScript) {
		// update all run windows
		IERunGroupAccount lRunGroupAccount = getRunGroupAccount();
		if (lRunGroupAccount == null) return;
		String lKey = "" + lRunGroupAccount.getRgaId();
		for (CRunWnd lRunWnd : getAllActiveRunWndsPerRgaId(lKey)) {
			CRunStatusChange lRunStatusChange = new CRunStatusChange();
			lRunStatusChange.setCaseComponent(aCaseComponent);
			lRunStatusChange.setDataTag(null);
			lRunStatusChange.setStatusTag(null);
			lRunStatusChange.setStatusKey(aStatusKey);
			lRunStatusChange.setStatusValue(aStatusValue);
			lRunStatusChange.setFromScript(aFromScript);
			getExternalStatusChangesPerRgaIdAndDesktopId(lRunWnd.rgaId, lRunWnd.desktopId).add(lRunStatusChange);
		}
	}

	/**
	 * Gets the prop xml data plus run status tree.
	 *
	 * @return the prop xml data plus run status tree
	 */
	@Override
	public Hashtable<String,IXMLTag> getPropXmlDataPlusRunStatusTree() {
		SSpring lRgaIdSSpring = getRgaIdSSpring();
		if (lRgaIdSSpring != null) {
			return lRgaIdSSpring.getPropXmlDataPlusRunStatusTree();
		};
		return super.getPropXmlDataPlusRunStatusTree();
	}

	/**
	 * Gets the prop xml data plus status tags by id.
	 *
	 * @return the prop xml data plus status tags by id
	 */
	@Override
	public Hashtable<String,Hashtable<String,IXMLTag>> getPropXmlDataPlusStatusTagsById() {
		SSpring lRgaIdSSpring = getRgaIdSSpring();
		if (lRgaIdSSpring != null) {
			return lRgaIdSSpring.getPropXmlDataPlusStatusTagsById();
		};
		return super.getPropXmlDataPlusStatusTagsById();
	}

	/**
	 * Gets the prop xml run group status tree.
	 *
	 * @return the prop xml run group status tree
	 */
	@Override
	public Hashtable<String,IXMLTag> getPropXmlRunGroupStatusTree() {
		SSpring lRgaIdSSpring = getRgaIdSSpring();
		if (lRgaIdSSpring != null) {
			return lRgaIdSSpring.getPropXmlRunGroupStatusTree();
		};
		return super.getPropXmlRunGroupStatusTree();
	}

	/**
	 * Gets the prop save run group case component status root tags.
	 *
	 * @return the prop save run group case component status root tags
	 */
	@Override
	public Hashtable<String,IXMLTag> getPropSaveRunGroupCaseComponentStatusRootTags() {
		SSpring lRgaIdSSpring = getRgaIdSSpring();
		if (lRgaIdSSpring != null) {
			return lRgaIdSSpring.getPropSaveRunGroupCaseComponentStatusRootTags();
		};
		return super.getPropSaveRunGroupCaseComponentStatusRootTags();
	}

	/**
	 * Gets the prop status tags by id.
	 *
	 * @return the prop status tags by id
	 */
	@Override
	public Hashtable<String,Hashtable<String,IXMLTag>> getPropXmlStatusTagsById() {
		SSpring lRgaIdSSpring = getRgaIdSSpring();
		if (lRgaIdSSpring != null) {
			return lRgaIdSSpring.getPropXmlStatusTagsById();
		};
		return super.getPropXmlStatusTagsById();
	}

	/**
	 * Gets the case time in seconds. The total time the Emergo player has been open for the current run group account.
	 * It is overriden because there is no CRunMainTimer object for this session.
	 * Case time is gotten from the current player saved in memory for the current run group account.
	 *
	 * @return the case time
	 */
	@Override
	public double getCaseTime() {
		double lTime = 0;
		IERunGroupAccount lRunGroupAccount = getRunGroupAccount();
		if (lRunGroupAccount == null)
			return lTime;
		String lKey = "" + lRunGroupAccount.getRgaId();
		Integer lSize = getAllActiveRunWndsPerRgaId(lKey).size();
		if (lSize > 0)
			// last window has most up to date state
			return getAllActiveRunWndsPerRgaId(lKey).get(lSize - 1).sSpring.caseTime;
		return lTime;
	}

}
