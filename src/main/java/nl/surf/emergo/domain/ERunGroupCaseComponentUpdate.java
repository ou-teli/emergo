/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

import java.io.Serializable;
import java.util.Hashtable;

/**
 * The Class ERunGroupCaseComponentUpdate.
 */
public class ERunGroupCaseComponentUpdate extends EDomainEntity implements Serializable, Cloneable, IERunGroupCaseComponentUpdate {
	
	private static final long serialVersionUID = 8195103735436985935L;

	/** The rgu id. */
	protected int rguId;

	/** The rug rug id. */
	protected int rugRugId;

	/** The cac cac id. */
	protected int cacCacId;

	/** The rug rug from id. */
	protected int rugRugFromId;

	/** The xmldata. */
	protected String xmldata;

	/** The active. */
	protected boolean processed;

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() {
		Object lObject = null;
		try {
			lObject = super.clone();
		} catch (CloneNotSupportedException cnse) {
		}
		return lObject;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunGroupCaseComponentUpdate#getRguId()
	 */
	public int getRguId() {
		return rguId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunGroupCaseComponentUpdate#setRguId(int)
	 */
	public void setRguId(int rguId) {
		this.rguId = rguId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunGroupCaseComponentUpdate#getRugRugId()
	 */
	public int getRugRugId() {
		return rugRugId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunGroupCaseComponentUpdate#setRugRugId(int)
	 */
	public void setRugRugId(int rugRugId) {
		this.rugRugId = rugRugId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunGroupCaseComponentUpdate#getCacCacId()
	 */
	public int getCacCacId() {
		return cacCacId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunGroupCaseComponentUpdate#setCacCacId(int)
	 */
	public void setCacCacId(int cacCacId) {
		this.cacCacId = cacCacId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunGroupCaseComponentUpdate#getRugRugFromId()
	 */
	public int getRugRugFromId() {
		return rugRugFromId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunGroupCaseComponentUpdate#setRugRugFromId(int)
	 */
	public void setRugRugFromId(int rugRugFromId) {
		this.rugRugFromId = rugRugFromId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunGroupCaseComponentUpdate#getXmldata()
	 */
	public String getXmldata() {
		return xmldata;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunGroupCaseComponentUpdate#setXmldata(java.lang.String)
	 */
	public void setXmldata(String xmldata) {
		this.xmldata = xmldata;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunGroupCaseComponentUpdate#getProcessed()
	 */
	public boolean getProcessed() {
		return processed;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunGroupCaseComponentUpdate#setProcessed(java.lang.boolean)
	 */
	public void setProcessed(boolean processed) {
		this.processed = processed;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getProperties()
	 */
	public Hashtable<String,String> getProperties() {
		Hashtable<String, String> lProperties = super.getProperties();
		lProperties.put("rguId", "" + rguId);
		lProperties.put("rugRugId", "" + rugRugId);
		lProperties.put("cacCacId", "" + cacCacId);
		lProperties.put("rugRugFromId", "" + rugRugFromId);
		lProperties.put("xmldata", "" + xmldata);
		lProperties.put("processed", "" + processed);
		return lProperties;
	}

}