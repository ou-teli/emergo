/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

import java.io.Serializable;
import java.util.Hashtable;

/**
 * The Class EAccountRole.
 */
public class EAccountRole extends EDomainEntity implements Serializable, Cloneable, IEAccountRole {
	
	private static final long serialVersionUID = 5035756919026388400L;

	/** The acr id. */
	protected int acrId;

	/** The acc acc id. */
	protected int accAccId;

	/** The rol rol id. */
	protected int rolRolId;

	/** The landingpage. */
	protected String landingpage;

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() {
		Object lObject = null;
		try {
			lObject = super.clone();
		} catch (CloneNotSupportedException cnse) {
		}
		return lObject;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountRole#getAcrId()
	 */
	public int getAcrId() {
		return acrId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountRole#setAcrId(int)
	 */
	public void setAcrId(int acrId) {
		this.acrId = acrId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountRole#getAccAccId()
	 */
	public int getAccAccId() {
		return accAccId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountRole#setAccAccId(int)
	 */
	public void setAccAccId(int accAccId) {
		this.accAccId = accAccId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountRole#getRolRolId()
	 */
	public int getRolRolId() {
		return rolRolId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountRole#setRolRolId(int)
	 */
	public void setRolRolId(int rolRolId) {
		this.rolRolId = rolRolId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountRole#getLandingpage()
	 */
	public String getLandingpage() {
		return landingpage;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountRole#setLandingpage(java.util.String)
	 */
	public void setLandingpage(String landingpage) {
		this.landingpage = landingpage;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getProperties()
	 */
	public Hashtable<String,String> getProperties() {
		Hashtable<String, String> lProperties = super.getProperties();
		lProperties.put("acrId", "" + acrId);
		lProperties.put("accAccId", "" + accAccId);
		lProperties.put("rolRolId", "" + rolRolId);
		lProperties.put("landingpage", "" + landingpage);
		return lProperties;
	}

}