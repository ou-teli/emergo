/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

/**
 * The Interface IECaseComponentRole.
 */
public interface IECaseComponentRole extends IEDomainEntity {

	/**
	 * Clones.
	 * 
	 * @return the object
	 */
	public Object clone();

	/**
	 * Gets the ccr id.
	 * 
	 * @return the ccr id
	 */
	public int getCcrId();

	/**
	 * Sets the ccr id.
	 * 
	 * @param ccrId the new ccr id
	 */
	public void setCcrId(int ccrId);

	/**
	 * Gets the cac cac id.
	 * 
	 * @return the cac cac id
	 */
	public int getCacCacId();

	/**
	 * Sets the cac cac id.
	 * 
	 * @param cacCacId the new cac cac id
	 */
	public void setCacCacId(int cacCacId);

	/**
	 * Gets the car car id.
	 * 
	 * @return the car car id
	 */
	public int getCarCarId();

	/**
	 * Sets the car car id.
	 * 
	 * @param carCarId the new car car id
	 */
	public void setCarCarId(int carCarId);

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName();

	/**
	 * Sets the name.
	 * 
	 * @param name the new name
	 */
	public void setName(String name);

}