/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

/**
 * The Interface IERunGroupAccount.
 */
public interface IERunAccount extends IEDomainEntity {

	/**
	 * Clones.
	 * 
	 * @return the object
	 */
	public Object clone();

	/**
	 * Gets the rua id.
	 * 
	 * @return the rua id
	 */
	public int getRuaId();

	/**
	 * Sets the rua id.
	 * 
	 * @param ruaId the new rua id
	 */
	public void setRuaId(int ruaId);

	/**
	 * Gets the active as student parameter.
	 * 
	 * @return the stu active
	 */
	public boolean getStuactive();

	/**
	 * Sets the active as student parameter.
	 * 
	 * @param stuactive the new value for stu active
	 */
	public void setStuactive(boolean stuactive);

	/**
	 * Gets the active as tutor parameter.
	 * 
	 * @return the tut active
	 */
	public boolean getTutactive();

	/**
	 * Sets the active as tutor parameter.
	 * 
	 * @param tutactive the new value for tut active
	 */
	public void setTutactive(boolean tutactive);

	/**
	 * Gets the e run.
	 * 
	 * @return the e run
	 */
	public IERun getERun();

	/**
	 * Sets the e run.
	 * 
	 * @param eRun the new e run
	 */
	public void setERun(IERun eRun);

	/**
	 * Gets the e account.
	 * 
	 * @return the e account
	 */
	public IEAccount getEAccount();

	/**
	 * Sets the e account.
	 * 
	 * @param eAccount the new e account
	 */
	public void setEAccount(IEAccount eAccount);

}