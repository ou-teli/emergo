/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

/**
 * The Interface IEAccountRole.
 */
public interface IEAccountRole extends IEDomainEntity {

	/**
	 * Clones.
	 * 
	 * @return the object
	 */
	public Object clone();

	/**
	 * Gets the acr id.
	 * 
	 * @return the acr id
	 */
	public int getAcrId();

	/**
	 * Sets the acr id.
	 * 
	 * @param acrId the new acr id
	 */
	public void setAcrId(int acrId);

	/**
	 * Gets the acc acc id.
	 * 
	 * @return the acc acc id
	 */
	public int getAccAccId();

	/**
	 * Sets the acc acc id.
	 * 
	 * @param accAccId the new acc acc id
	 */
	public void setAccAccId(int accAccId);

	/**
	 * Gets the rol rol id.
	 * 
	 * @return the rol rol id
	 */
	public int getRolRolId();

	/**
	 * Sets the rol rol id.
	 * 
	 * @param rolRolId the new rol rol id
	 */
	public void setRolRolId(int rolRolId);

	/**
	 * Gets the landingpage.
	 * 
	 * @return the landingpage
	 */
	public String getLandingpage();

	/**
	 * Sets the landingpage.
	 * 
	 * @param landingpage the new landingpage
	 */
	public void setLandingpage(String landingpage);

}