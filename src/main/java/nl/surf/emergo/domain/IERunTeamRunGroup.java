/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

/**
 * The Interface IERunTeamRunGroup.
 */
public interface IERunTeamRunGroup extends IEDomainEntity {

	/**
	 * Clones.
	 * 
	 * @return the object
	 */
	public Object clone();

	/**
	 * Gets the rtr id.
	 * 
	 * @return the rtr id
	 */
	public int getRtrId();

	/**
	 * Sets the rtr id.
	 * 
	 * @param rtrId the new rtr id
	 */
	public void setRtrId(int rtrId);

	/**
	 * Gets the rut rut id.
	 * 
	 * @return the rut rut id
	 */
	public int getRutRutId();

	/**
	 * Sets the rut rut id.
	 * 
	 * @param rutRutId the new rut rut id
	 */
	public void setRutRutId(int rutRutId);

	/**
	 * Gets the rug rug id.
	 * 
	 * @return the rug rug id
	 */
	public int getRugRugId();

	/**
	 * Sets the rug rug id.
	 * 
	 * @param rugRugId the new rug rug id
	 */
	public void setRugRugId(int rugRugId);

}