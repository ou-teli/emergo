/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

import java.io.Serializable;
import java.util.Hashtable;

/**
 * The Class ESys.
 */
public class ESys extends EDomainEntity implements Serializable,Cloneable,IESys {

	private static final long serialVersionUID = 4171276470690677961L;

	/** The sys id. */
	protected int sysId;

	/** The syskey. */
	protected String syskey;

	/** The sysvalue. */
	protected String sysvalue;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	public Object clone() {
		Object lObject = null;
		try {
			lObject = super.clone();
		} catch (CloneNotSupportedException cnse) {
		}
		return lObject;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.surf.emergo.domain.IESys#getSysId()
	 */
	public int getSysId() {
		return sysId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.surf.emergo.domain.IESys#setSysId(int)
	 */
	public void setSysId(int sysId) {
		this.sysId = sysId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.surf.emergo.domain.IESys#getSyskey()
	 */
	public String getSyskey() {
		return syskey;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.surf.emergo.domain.IESys#setSyskey(java.lang.String)
	 */
	public void setSyskey(String syskey) {
		this.syskey = syskey;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.surf.emergo.domain.IESys#getSysvalue()
	 */
	public String getSysvalue() {
		return sysvalue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.surf.emergo.domain.IESys#setSysvalue(java.lang.String)
	 */
	public void setSysvalue(String sysvalue) {
		this.sysvalue = sysvalue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.surf.emergo.domain.IEAccount#getProperties()
	 */
	public Hashtable<String, String> getProperties() {
		Hashtable<String, String> lProperties = super.getProperties();
		lProperties.put("sysId", "" + sysId);
		lProperties.put("syskey", "" + syskey);
		lProperties.put("sysvalue", "" + sysvalue);
		return lProperties;
	}

}