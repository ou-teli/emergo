/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

import java.io.Serializable;
import java.util.Hashtable;

/**
 * The Class ERunTeam.
 */
public class ERunTeam extends EDomainEntity implements Serializable, Cloneable, IERunTeam {
	
	private static final long serialVersionUID = -4635076649352791217L;

	/** The rut id. */
	protected int rutId;

	/** The name. */
	protected String name;

	/** The active. */
	protected boolean active;

	/** The e run. */
	protected IERun eRun;

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() {
		Object lObject = null;
		try {
			lObject = super.clone();
		} catch (CloneNotSupportedException cnse) {
		}
		return lObject;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunGroup#getRutId()
	 */
	public int getRutId() {
		return rutId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunGroup#setRutId(int)
	 */
	public void setRutId(int rutId) {
		this.rutId = rutId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunGroup#getName()
	 */
	public String getName() {
		return name;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunGroup#setName(java.lang.String)
	 */
	public void setName(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunGroup#getActive()
	 */
	public boolean getActive() {
		return active;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunGroup#setActive(boolean)
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunGroup#getERun()
	 */
	public IERun getERun() {
		return eRun;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunGroup#setERun(nl.surf.emergo.domain.IERun)
	 */
	public void setERun(IERun eRun) {
		this.eRun = eRun;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getProperties()
	 */
	public Hashtable<String,String> getProperties() {
		Hashtable<String, String> lProperties = super.getProperties();
		lProperties.put("rutId", "" + rutId);
		lProperties.put("runRunId", "" + (eRun != null ? eRun.getRunId() : 0));
		lProperties.put("name", "" + name);
		lProperties.put("active", "" + active);
		return lProperties;
	}
	
}
