/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

/**
 * The Interface IEBlob.
 */
public interface IEBlob extends IEDomainEntity {

	/**
	 * Clones.
	 * 
	 * @return the object
	 */
	public Object clone();

	/**
	 * Gets the blo id.
	 * 
	 * @return the blo id
	 */
	public int getBloId();

	/**
	 * Sets the blo id.
	 * 
	 * @param bloId the new blo id
	 */
	public void setBloId(int bloId);

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName();

	/**
	 * Sets the name.
	 * 
	 * @param name the new name
	 */
	public void setName(String name);

	/**
	 * Gets the filename.
	 * 
	 * @return the filename
	 */
	public String getFilename();

	/**
	 * Sets the filename.
	 * 
	 * @param filename the new filename
	 */
	public void setFilename(String filename);

	/**
	 * Gets the contenttype.
	 * 
	 * @return the contenttype
	 */
	public String getContenttype();

	/**
	 * Sets the contenttype.
	 * 
	 * @param contenttype the new contenttype
	 */
	public void setContenttype(String contenttype);

	/**
	 * Gets the format.
	 * 
	 * @return the format
	 */
	public String getFormat();

	/**
	 * Sets the format.
	 * 
	 * @param format the new format
	 */
	public void setFormat(String format);

	/**
	 * Gets the url.
	 * 
	 * @return the url
	 */
	public String getUrl();

	/**
	 * Sets the url.
	 * 
	 * @param url the new url
	 */
	public void setUrl(String url);

}