/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Hashtable;

/**
 * The Class EDomainEntity.
 */
public class EDomainEntity implements Serializable, Cloneable, IEDomainEntity {
	
	private static final long serialVersionUID = -7954007032416046773L;

	/** The creationdate. */
	protected Date creationdate;

	/** The lastupdatedate. */
	protected Date lastupdatedate;

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getCreationdate()
	 */
	public Date getCreationdate() {
		return creationdate;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#setCreationdate(java.util.Date)
	 */
	public void setCreationdate(Date creationdate) {
		this.creationdate = creationdate;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getLastupdatedate()
	 */
	public Date getLastupdatedate() {
		return lastupdatedate;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#setLastupdatedate(java.util.Date)
	 */
	public void setLastupdatedate(Date lastupdatedate) {
		this.lastupdatedate = lastupdatedate;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEDomainEntity#getProperty(java.lang.String)
	 */
	public String getProperty(String name) {
		Hashtable<String, String> lProperties = getProperties();
		if (lProperties.containsKey(name))
			return lProperties.get(name);
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEDomainEntity#getProperties()
	 */
	@Override
	public Hashtable<String, String> getProperties() {
		Hashtable<String, String> lProperties = new Hashtable<String, String>();
		lProperties.put("creationdate", "" + creationdate);
		lProperties.put("lastupdatedate", "" + lastupdatedate);
		return lProperties;
	}

}