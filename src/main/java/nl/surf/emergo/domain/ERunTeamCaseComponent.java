/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

import java.io.Serializable;
import java.util.Hashtable;

/**
 * The Class ERunTeamCaseComponent.
 */
public class ERunTeamCaseComponent extends EDomainEntity implements Serializable, Cloneable, IERunTeamCaseComponent {
	
	private static final long serialVersionUID = 135586060787801371L;

	/** The rtc id. */
	protected int rtcId;

	/** The rut rut id. */
	protected int rutRutId;

	/** The cac cac id. */
	protected int cacCacId;

	/** The tag tag id. */
	protected int tagTagId;

	/** The xmldata. */
	protected String xmldata;

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() {
		Object lObject = null;
		try {
			lObject = super.clone();
		} catch (CloneNotSupportedException cnse) {
		}
		return lObject;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunTeamCaseComponent#getRtcId()
	 */
	public int getRtcId() {
		return rtcId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunTeamCaseComponent#setRtcId(int)
	 */
	public void setRtcId(int rtcId) {
		this.rtcId = rtcId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunTeamCaseComponent#getRutRutId()
	 */
	public int getRutRutId() {
		return rutRutId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunTeamCaseComponent#setRutRutId(int)
	 */
	public void setRutRutId(int rutRutId) {
		this.rutRutId = rutRutId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunTeamCaseComponent#getCacCacId()
	 */
	public int getCacCacId() {
		return cacCacId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunTeamCaseComponent#setCacCacId(int)
	 */
	public void setCacCacId(int cacCacId) {
		this.cacCacId = cacCacId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunTeamCaseComponent#getTagTagId()
	 */
	public int getTagTagId() {
		return tagTagId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunTeamCaseComponent#setTagTagId(int)
	 */
	public void setTagTagId(int tagTagId) {
		this.tagTagId = tagTagId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunTeamCaseComponent#getXmldata()
	 */
	public String getXmldata() {
		return xmldata;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunTeamCaseComponent#setXmldata(java.lang.String)
	 */
	public void setXmldata(String xmldata) {
		this.xmldata = xmldata;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getProperties()
	 */
	public Hashtable<String,String> getProperties() {
		Hashtable<String, String> lProperties = super.getProperties();
		lProperties.put("rtcId", "" + rtcId);
		lProperties.put("rutRutId", "" + rutRutId);
		lProperties.put("cacCacId", "" + cacCacId);
		lProperties.put("xmldata", "" + xmldata);
		return lProperties;
	}

}