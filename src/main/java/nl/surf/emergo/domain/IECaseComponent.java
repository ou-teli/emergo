/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

/**
 * The Interface IECaseComponent.
 */
public interface IECaseComponent extends IEDomainEntity {

	/**
	 * Clones.
	 * 
	 * @return the object
	 */
	public Object clone();

	/**
	 * Gets the cac id.
	 * 
	 * @return the cac id
	 */
	public int getCacId();

	/**
	 * Sets the cac id.
	 * 
	 * @param cacId the new cac id
	 */
	public void setCacId(int cacId);

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName();

	/**
	 * Sets the name.
	 * 
	 * @param name the new name
	 */
	public void setName(String name);

	/**
	 * Gets the xmldata.
	 * 
	 * @return the xmldata
	 */
	public String getXmldata();

	/**
	 * Sets the xmldata.
	 * 
	 * @param xmldata the new xmldata
	 */
	public void setXmldata(String xmldata);

	/**
	 * Gets the e case.
	 * 
	 * @return the e case
	 */
	public IECase getECase();

	/**
	 * Sets the e case.
	 * 
	 * @param eCase the new e case
	 */
	public void setECase(IECase eCase);

	/**
	 * Gets the e component.
	 * 
	 * @return the e component
	 */
	public IEComponent getEComponent();

	/**
	 * Sets the e component.
	 * 
	 * @param eComponent the new e component
	 */
	public void setEComponent(IEComponent eComponent);

	/**
	 * Gets the e account.
	 * 
	 * @return the e account
	 */
	public IEAccount getEAccount();

	/**
	 * Sets the e account.
	 * 
	 * @param eAccount the new e account
	 */
	public void setEAccount(IEAccount eAccount);

}