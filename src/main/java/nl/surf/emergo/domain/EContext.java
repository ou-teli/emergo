/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

import java.io.Serializable;
import java.util.Hashtable;

/**
 * The Class EContext.
 */
public class EContext extends EDomainEntity implements Serializable, Cloneable, IEContext {
	
	private static final long serialVersionUID = 6676187666310319563L;

	/** The context id. */
	protected int conId;

	/** The context. */
	protected String context;

	/** The active. */
	protected boolean active;

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() {
		Object lObject = null;
		try {
			lObject = super.clone();
		} catch (CloneNotSupportedException cnse) {
		}
		return lObject;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEContext#getConId()
	 */
	public int getConId() {
		return conId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEContext#setConId(int)
	 */
	public void setConId(int conId) {
		this.conId = conId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEContext#getContext()
	 */
	public String getContext() {
		return context;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEContext#setContext(java.lang.String)
	 */
	public void setContext(String context) {
		this.context = context;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEContext#getActive()
	 */
	public boolean getActive() {
		return active;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEContext#setActive(boolean)
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEContext#getProperties()
	 */
	public Hashtable<String, String> getProperties() {
		Hashtable<String, String> lProperties = super.getProperties();
		lProperties.put("conId", "" + conId);
		lProperties.put("context", "" + context);
		lProperties.put("active", "" + active);
		return lProperties;
	}

}