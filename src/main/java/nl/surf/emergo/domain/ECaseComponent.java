/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

import java.io.Serializable;
import java.util.Hashtable;

/**
 * The Class ECaseComponent.
 */
public class ECaseComponent extends EDomainEntity implements Serializable, Cloneable, IECaseComponent {
	
	private static final long serialVersionUID = 848322771519269860L;

	/** The cac id. */
	protected int cacId;

	/** The name. */
	protected String name;

	/** The xmldata. */
	protected String xmldata;

	/** The e case. */
	protected IECase eCase;

	/** The e component. */
	protected IEComponent eComponent;

	/** The e account. */
	protected IEAccount eAccount;

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() {
		Object lObject = null;
		try {
			lObject = super.clone();
		} catch (CloneNotSupportedException cnse) {
		}
		return lObject;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECaseComponent#getCacId()
	 */
	public int getCacId() {
		return cacId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECaseComponent#setCacId(int)
	 */
	public void setCacId(int cacId) {
		this.cacId = cacId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECaseComponent#getName()
	 */
	public String getName() {
		return name;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECaseComponent#setName(java.lang.String)
	 */
	public void setName(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECaseComponent#getXmldata()
	 */
	public String getXmldata() {
		return xmldata;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECaseComponent#setXmldata(java.lang.String)
	 */
	public void setXmldata(String xmldata) {
		this.xmldata = xmldata;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECaseComponent#getECase()
	 */
	public IECase getECase() {
		return eCase;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECaseComponent#setECase(nl.surf.emergo.domain.IECase)
	 */
	public void setECase(IECase eCase) {
		this.eCase = eCase;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECaseComponent#getEComponent()
	 */
	public IEComponent getEComponent() {
		return eComponent;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECaseComponent#setEComponent(nl.surf.emergo.domain.IEComponent)
	 */
	public void setEComponent(IEComponent eComponent) {
		this.eComponent = eComponent;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECaseComponent#getEAccount()
	 */
	public IEAccount getEAccount() {
		return eAccount;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECaseComponent#setEAccount(nl.surf.emergo.domain.IEAccount)
	 */
	public void setEAccount(IEAccount eAccount) {
		this.eAccount = eAccount;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getProperties()
	 */
	public Hashtable<String,String> getProperties() {
		Hashtable<String, String> lProperties = super.getProperties();
		lProperties.put("cacId", "" + cacId);
		lProperties.put("casCasId", "" + (eCase != null ? eCase.getCasId() : 0));
		lProperties.put("comComId", "" + (eComponent != null ? eComponent.getComId() : 0));
		lProperties.put("accAccId", "" + (eAccount != null ? eAccount.getAccId() : 0));
		lProperties.put("name", "" + name);
		lProperties.put("xmldata", "" + xmldata);
		return lProperties;
	}

}