/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

/**
 * The Interface IESys.
 */
public interface IESys extends IEDomainEntity {

	/**
	 * Clones.
	 * 
	 * @return the object
	 */
	public Object clone();

	/**
	 * Gets the sys id.
	 * 
	 * @return the sys id
	 */
	public int getSysId();

	/**
	 * Sets the sys id.
	 * 
	 * @param sysId the new sys id
	 */
	public void setSysId(int sysId);

	/**
	 * Gets the syskey.
	 * 
	 * @return the syskey
	 */
	public String getSyskey();

	/**
	 * Sets the syskey.
	 * 
	 * @param syskey the new syskey
	 */
	public void setSyskey(String syskey);

	/**
	 * Gets the sysvalue.
	 * 
	 * @return the sysvalue
	 */
	public String getSysvalue();

	/**
	 * Sets the sysvalue.
	 * 
	 * @param sysvalue the new sysvalue
	 */
	public void setSysvalue(String sysvalue);

}