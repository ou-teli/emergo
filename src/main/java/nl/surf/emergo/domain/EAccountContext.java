/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

import java.io.Serializable;
import java.util.Hashtable;

/**
 * The Class EAccountContext.
 */
public class EAccountContext extends EDomainEntity implements Serializable, Cloneable, IEAccountContext {
	
	private static final long serialVersionUID = 1606567455006733345L;

	/** The acc id. */
	protected int accId;

	/** The acc acc id. */
	protected int accAccId;

	/** The con con id. */
	protected int conConId;

	/** The e active. */
	protected boolean active;

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() {
		Object lObject = null;
		try {
			lObject = super.clone();
		} catch (CloneNotSupportedException cnse) {
		}
		return lObject;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountContext#getAccId()
	 */
	public int getAccId() {
		return accId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountContext#setAccId(int)
	 */
	public void setAccId(int accId) {
		this.accId = accId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountContext#getAccAccId()
	 */
	public int getAccAccId() {
		return accAccId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountContext#setAccAccId(int)
	 */
	public void setAccAccId(int accAccId) {
		this.accAccId = accAccId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountContext#getConConId()
	 */
	public int getConConId() {
		return conConId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountContext#setConConId(int)
	 */
	public void setConConId(int conConId) {
		this.conConId = conConId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountContext#getActive()
	 */
	public boolean getActive() {
		return active;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountContext#setActive(boolean)
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountContext#getProperties()
	 */
	public Hashtable<String, String> getProperties() {
		Hashtable<String, String> lProperties = super.getProperties();
		lProperties.put("accId", "" + accId);
		lProperties.put("accAccId", "" + accAccId);
		lProperties.put("conConId", "" + conConId);
		lProperties.put("active", "" + active);
		return lProperties;
	}

}