/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Hashtable;

/**
 * The Interface IEDomainEntity.
 */
public interface IEDomainEntity extends Serializable{

	/**
	 * Gets the creationdate.
	 * 
	 * @return the creationdate
	 */
	public Date getCreationdate();

	/**
	 * Sets the creationdate.
	 * 
	 * @param creationdate the new creationdate
	 */
	public void setCreationdate(Date creationdate);

	/**
	 * Gets the lastupdatedate.
	 * 
	 * @return the lastupdatedate
	 */
	public Date getLastupdatedate();

	/**
	 * Sets the lastupdatedate.
	 * 
	 * @param lastupdatedate the new lastupdatedate
	 */
	public void setLastupdatedate(Date lastupdatedate);

	/**
	 * Gets the property.
	 * 
	 * @param name the name
	 * 
	 * @return the property
	 */
	public String getProperty(String name);

	/**
	 * Gets the properties.
	 * 
	 * @return the properties
	 */
	public Hashtable<String,String> getProperties();
	
}