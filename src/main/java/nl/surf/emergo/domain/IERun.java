/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

import java.util.Date;

/**
 * The Interface IERun.
 */
public interface IERun extends IEDomainEntity {

	/**
	 * Clones.
	 * 
	 * @return the object
	 */
	public Object clone();

	/**
	 * Gets the run id.
	 * 
	 * @return the run id
	 */
	public int getRunId();

	/**
	 * Sets the run id.
	 * 
	 * @param runId the new run id
	 */
	public void setRunId(int runId);

	/**
	 * Gets the code.
	 * 
	 * @return the code
	 */
	public String getCode();

	/**
	 * Sets the code.
	 * 
	 * @param code the new code
	 */
	public void setCode(String code);

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName();

	/**
	 * Sets the name.
	 * 
	 * @param name the new name
	 */
	public void setName(String name);

	/**
	 * Gets the startdate.
	 * 
	 * @return the startdate
	 */
	public Date getStartdate();

	/**
	 * Sets the startdate.
	 * 
	 * @param startdate the new startdate
	 */
	public void setStartdate(Date startdate);

	/**
	 * Gets the enddate.
	 * 
	 * @return the enddate
	 */
	public Date getEnddate();

	/**
	 * Sets the enddate.
	 * 
	 * @param enddate the new enddate
	 */
	public void setEnddate(Date enddate);

	/**
	 * Gets the status.
	 * 
	 * @return the status
	 */
	public int getStatus();

	/**
	 * Sets the status.
	 * 
	 * @param status the new status
	 */
	public void setStatus(int status);

	/**
	 * Gets the active.
	 * 
	 * @return the active
	 */
	public boolean getActive();

	/**
	 * Sets the active.
	 * 
	 * @param active the new active
	 */
	public void setActive(boolean active);

	/**
	 * Gets the openaccess.
	 * 
	 * @return the openaccess
	 */
	public boolean getOpenaccess();

	/**
	 * Sets the openaccess.
	 * 
	 * @param openaccess the new openaccess
	 */
	public void setOpenaccess(boolean openaccess);

	/**
	 * Gets the accessmailsubject.
	 * 
	 * @return the accessmailsubject
	 */
	public String getAccessmailsubject();

	/**
	 * Sets the accessmailsubject.
	 * 
	 * @param accessmailsubject the new accessmailsubject
	 */
	public void setAccessmailsubject(String accessmailsubject);

	/**
	 * Gets the accessmailbody.
	 * 
	 * @return the accessmailbody
	 */
	public String getAccessmailbody();

	/**
	 * Sets the accessmailbody.
	 * 
	 * @param accessmailbody the new accessmailbody
	 */
	public void setAccessmailbody(String accessmailbody);

	/**
	 * Gets the xmldata.
	 * 
	 * @return the xmldata
	 */
	public String getXmldata();

	/**
	 * Sets the xmldata.
	 * 
	 * @param xmldata the new xmldata
	 */
	public void setXmldata(String xmldata);

	/**
	 * Gets the e case.
	 * 
	 * @return the e case
	 */
	public IECase getECase();

	/**
	 * Sets the e case.
	 * 
	 * @param eCase the new e case
	 */
	public void setECase(IECase eCase);

	/**
	 * Gets the e account.
	 * 
	 * @return the e account
	 */
	public IEAccount getEAccount();

	/**
	 * Sets the e account.
	 * 
	 * @param eAccount the new e account
	 */
	public void setEAccount(IEAccount eAccount);

}