/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

import java.io.Serializable;
import java.util.Hashtable;

/**
 * The Class EBlob.
 */
public class EBlob extends EDomainEntity implements Serializable, Cloneable, IEBlob {
	
	private static final long serialVersionUID = 6644924273836006935L;

	/** The blo id. */
	protected int bloId;

	/** The name. */
	protected String name;

	/** The filename. */
	protected String filename;

	/** The contenttype. */
	protected String contenttype;

	/** The format. */
	protected String format;

	/** The url. */
	protected String url;

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() {
		Object lObject = null;
		try {
			lObject = super.clone();
		} catch (CloneNotSupportedException cnse) {
		}
		return lObject;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEBlob#getBloId()
	 */
	public int getBloId() {
		return bloId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEBlob#setBloId(int)
	 */
	public void setBloId(int bloId) {
		this.bloId = bloId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEBlob#getName()
	 */
	public String getName() {
		return name;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEBlob#setName(java.lang.String)
	 */
	public void setName(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEBlob#getFilename()
	 */
	public String getFilename() {
		return filename;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEBlob#setFilename(java.lang.String)
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEBlob#getContenttype()
	 */
	public String getContenttype() {
		return contenttype;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEBlob#setContenttype(java.lang.String)
	 */
	public void setContenttype(String contenttype) {
		this.contenttype = contenttype;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEBlob#getFormat()
	 */
	public String getFormat() {
		return format;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEBlob#setFormat(java.lang.String)
	 */
	public void setFormat(String format) {
		this.format = format;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEBlob#getUrl()
	 */
	public String getUrl() {
		return url;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEBlob#setUrl(java.lang.String)
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getProperties()
	 */
	public Hashtable<String,String> getProperties() {
		Hashtable<String, String> lProperties = super.getProperties();
		lProperties.put("bloId", "" + bloId);
		lProperties.put("name", "" + name);
		lProperties.put("filename", "" + filename);
		lProperties.put("contenttype", "" + contenttype);
		lProperties.put("format", "" + format);
		lProperties.put("url", "" + url);
		return lProperties;
	}

}