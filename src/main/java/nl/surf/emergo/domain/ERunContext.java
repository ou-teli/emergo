/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

import java.io.Serializable;
import java.util.Hashtable;

/**
 * The Class ERunContext.
 */
public class ERunContext extends EDomainEntity implements Serializable, Cloneable, IERunContext {
	
	private static final long serialVersionUID = 3938877875312054749L;

	/** The ruc id. */
	protected int rucId;

	/** The run run id. */
	protected int runRunId;

	/** The con con id. */
	protected int conConId;

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() {
		Object lObject = null;
		try {
			lObject = super.clone();
		} catch (CloneNotSupportedException cnse) {
		}
		return lObject;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunContext#getRucId()
	 */
	public int getRucId() {
		return rucId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunContext#setRucId(int)
	 */
	public void setRucId(int rucId) {
		this.rucId = rucId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunContext#getRunRunId()
	 */
	public int getRunRunId() {
		return runRunId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunContext#setRunRunId(int)
	 */
	public void setRunRunId(int runRunId) {
		this.runRunId = runRunId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunContext#getConConId()
	 */
	public int getConConId() {
		return conConId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunContext#setConConId(int)
	 */
	public void setConConId(int conConId) {
		this.conConId = conConId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunContext#getProperties()
	 */
	public Hashtable<String, String> getProperties() {
		Hashtable<String, String> lProperties = super.getProperties();
		lProperties.put("rucId", "" + rucId);
		lProperties.put("runRunId", "" + runRunId);
		lProperties.put("conConId", "" + conConId);
		return lProperties;
	}

}