/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

import java.io.Serializable;
import java.util.Hashtable;

/**
 * The Class EAccountRequest.
 */
public class EAccountRequest extends EDomainEntity implements Serializable, Cloneable, IEAccountRequest {
	
	private static final long serialVersionUID = 3251615740990517008L;

	/** The arq id. */
	protected int arqId;

	/** The userid. */
	protected String userid;

	/** The password. */
	protected String password;

	/** The studentid. */
	protected String studentid;

	/** The title. */
	protected String title;

	/** The initials. */
	protected String initials;

	/** The nameprefix. */
	protected String nameprefix;

	/** The lastname. */
	protected String lastname;

	/** The email. */
	protected String email;

	/** The processed. */
	protected boolean processed;

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() {
		Object lObject = null;
		try {
			lObject = super.clone();
		} catch (CloneNotSupportedException cnse) {
		}
		return lObject;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountRequest#getArqId()
	 */
	public int getArqId() {
		return arqId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountRequest#setArqId(int)
	 */
	public void setArqId(int arqId) {
		this.arqId = arqId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountRequest#getUserid()
	 */
	public String getUserid() {
		return userid;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountRequest#setUserid(java.lang.String)
	 */
	public void setUserid(String userid) {
		this.userid = userid;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountRequest#getPassword()
	 */
	public String getPassword() {
		return password;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountRequest#setPassword(java.lang.String)
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getStudentid()
	 */
	public String getStudentid() {
		return studentid;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#setStudentid(java.lang.String)
	 */
	public void setStudentid(String studentid) {
		this.studentid = studentid;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountRequest#getTitle()
	 */
	public String getTitle() {
		return title;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountRequest#setTitle(java.lang.String)
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountRequest#getInitials()
	 */
	public String getInitials() {
		return initials;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountRequest#setInitials(java.lang.String)
	 */
	public void setInitials(String initials) {
		this.initials = initials;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountRequest#getNameprefix()
	 */
	public String getNameprefix() {
		return nameprefix;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountRequest#setNameprefix(java.lang.String)
	 */
	public void setNameprefix(String nameprefix) {
		this.nameprefix = nameprefix;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountRequest#getLastname()
	 */
	public String getLastname() {
		return lastname;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountRequest#setLastname(java.lang.String)
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountRequest#getEmail()
	 */
	public String getEmail() {
		return email;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountRequest#setEmail(java.lang.String)
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountRequest#getProcessed()
	 */
	public boolean getProcessed() {
		return processed;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccountRequest#setProcessed(boolean)
	 */
	public void setProcessed(boolean processed) {
		this.processed = processed;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getProperties()
	 */
	public Hashtable<String, String> getProperties() {
		Hashtable<String, String> lProperties = super.getProperties();
		lProperties.put("arqId", "" + arqId);
		lProperties.put("userid", "" + userid);
		lProperties.put("password", "" + password);
		lProperties.put("studentid", "" + studentid);
		lProperties.put("title", "" + title);
		lProperties.put("initials", "" + initials);
		lProperties.put("nameprefix", "" + nameprefix);
		lProperties.put("lastname", "" + lastname);
		lProperties.put("email", "" + email);
		lProperties.put("processed", "" + processed);
		return lProperties;
	}

}