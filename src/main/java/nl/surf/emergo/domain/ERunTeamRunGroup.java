/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

import java.io.Serializable;
import java.util.Hashtable;

/**
 * The Class ERunTeamRunGroup.
 */
public class ERunTeamRunGroup extends EDomainEntity implements Serializable, Cloneable, IERunTeamRunGroup {
	
	private static final long serialVersionUID = 6576660347667301739L;

	/** The rtr id. */
	protected int rtrId;

	/** The rut rut id. */
	protected int rutRutId;

	/** The rug rug id. */
	protected int rugRugId;

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() {
		Object lObject = null;
		try {
			lObject = super.clone();
		} catch (CloneNotSupportedException cnse) {
		}
		return lObject;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunTeamRunGroup#getRtrId()
	 */
	public int getRtrId() {
		return rtrId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunTeamRunGroup#setRtrId(int)
	 */
	public void setRtrId(int rtrId) {
		this.rtrId = rtrId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunTeamRunGroup#getRutRutId()
	 */
	public int getRutRutId() {
		return rutRutId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunTeamRunGroup#setRutRutId(int)
	 */
	public void setRutRutId(int rutRutId) {
		this.rutRutId = rutRutId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunTeamRunGroup#getRugRugId()
	 */
	public int getRugRugId() {
		return rugRugId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunTeamRunGroup#setRugRugId(int)
	 */
	public void setRugRugId(int rugRugId) {
		this.rugRugId = rugRugId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunTeamRunGroup#getProperties()
	 */
	public Hashtable<String,String> getProperties() {
		Hashtable<String, String> lProperties = super.getProperties();
		lProperties.put("rtrId", "" + rtrId);
		lProperties.put("rutRutId", "" + rutRutId);
		lProperties.put("rugRugId", "" + rugRugId);
		return lProperties;
	}

}