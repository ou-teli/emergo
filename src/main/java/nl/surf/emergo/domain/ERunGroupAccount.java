/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

import java.io.Serializable;
import java.util.Hashtable;

/**
 * The Class ERunGroupAccount.
 */
public class ERunGroupAccount extends EDomainEntity implements Serializable, Cloneable, IERunGroupAccount {
	
	private static final long serialVersionUID = -1767614290338268510L;

	/** The rga id. */
	protected int rgaId;

	/** The e run group. */
	protected IERunGroup eRunGroup;

	/** The e account. */
	protected IEAccount eAccount;

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() {
		Object lObject = null;
		try {
			lObject = super.clone();
		} catch (CloneNotSupportedException cnse) {
		}
		return lObject;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunGroupAccount#getRgaId()
	 */
	public int getRgaId() {
		return rgaId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunGroupAccount#setRgaId(int)
	 */
	public void setRgaId(int rgaId) {
		this.rgaId = rgaId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunGroupAccount#getERunGroup()
	 */
	public IERunGroup getERunGroup() {
		return eRunGroup;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunGroupAccount#setERunGroup(nl.surf.emergo.domain.IERunGroup)
	 */
	public void setERunGroup(IERunGroup eRunGroup) {
		this.eRunGroup = eRunGroup;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunGroupAccount#getEAccount()
	 */
	public IEAccount getEAccount() {
		return eAccount;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunGroupAccount#setEAccount(nl.surf.emergo.domain.IEAccount)
	 */
	public void setEAccount(IEAccount eAccount) {
		this.eAccount = eAccount;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getProperties()
	 */
	public Hashtable<String,String> getProperties() {
		Hashtable<String, String> lProperties = super.getProperties();
		lProperties.put("rgaId", "" + rgaId);
		lProperties.put("accAccId", "" + (eAccount != null ? eAccount.getAccId() : 0));
		lProperties.put("rugRugId", "" + (eRunGroup != null ? eRunGroup.getRugId() : 0));
		return lProperties;
	}

}