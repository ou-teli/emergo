/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

import java.io.Serializable;
import java.util.Hashtable;

/**
 * The Class ERunAccount.
 */
public class ERunAccount extends EDomainEntity implements Serializable, Cloneable, IERunAccount {
	
	private static final long serialVersionUID = -4107515768935572181L;

	/** The rua id. */
	protected int ruaId;

	/** The student active. */
	protected boolean stuactive;

	/** The tutor active. */
	protected boolean tutactive;

	/** The e run. */
	protected IERun eRun;

	/** The e account. */
	protected IEAccount eAccount;

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() {
		Object lObject = null;
		try {
			lObject = super.clone();
		} catch (CloneNotSupportedException cnse) {
		}
		return lObject;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunAccount#getRuaId()
	 */
	public int getRuaId() {
		return ruaId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunAccount#setRuaId(int)
	 */
	public void setRuaId(int ruaId) {
		this.ruaId = ruaId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunAccount#getStuActive()
	 */
	public boolean getStuactive() {
		return stuactive;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunAccount#setStuActive(boolean)
	 */
	public void setStuactive(boolean stuactive) {
		this.stuactive = stuactive;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunAccount#getTutActive()
	 */
	public boolean getTutactive() {
		return tutactive;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunAccount#setTutActive(boolean)
	 */
	public void setTutactive(boolean tutactive) {
		this.tutactive = tutactive;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunAccount#getERun()
	 */
	public IERun getERun() {
		return eRun;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunAccountp#setERun(nl.surf.emergo.domain.IERun)
	 */
	public void setERun(IERun eRun) {
		this.eRun = eRun;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunAccount#getEAccount()
	 */
	public IEAccount getEAccount() {
		return eAccount;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunAccount#setEAccount(nl.surf.emergo.domain.IEAccount)
	 */
	public void setEAccount(IEAccount eAccount) {
		this.eAccount = eAccount;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERunAccount#getProperties()
	 */
	public Hashtable<String,String> getProperties() {
		Hashtable<String, String> lProperties = super.getProperties();
		lProperties.put("ruaId", "" + ruaId);
		lProperties.put("runRunId", "" + (eRun != null ? eRun.getRunId() : 0));
		lProperties.put("accAccId", "" + (eAccount != null ? eAccount.getAccId() : 0));
		lProperties.put("stuactive", "" + stuactive);
		lProperties.put("tutactive", "" + tutactive);
		return lProperties;
	}

}