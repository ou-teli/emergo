/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

/**
 * The Interface IECase.
 */
public interface IECase extends IEDomainEntity {

	/**
	 * Clones.
	 *
	 * @return the object
	 */
	public Object clone();

	/**
	 * Gets the cas id.
	 *
	 * @return the cas id
	 */
	public int getCasId();

	/**
	 * Sets the cas id.
	 *
	 * @param casId the new cas id
	 */
	public void setCasId(int casId);

	/**
	 * Gets the cas cas id.
	 *
	 * @return the cas cas id
	 */
	public int getCasCasId();

	/**
	 * Sets the cas cas id.
	 *
	 * @param casCasId the new cas cas id
	 */
	public void setCasCasId(int casCasId);

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode();

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(String code);

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName();

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name);

	/**
	 * Gets the version.
	 *
	 * @return the version
	 */
	public int getVersion();

	/**
	 * Sets the version.
	 *
	 * @param version the new version
	 */
	public void setVersion(int version);

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public int getStatus();

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(int status);

	/**
	 * Gets the active.
	 *
	 * @return the active
	 */
	public boolean getActive();

	/**
	 * Sets the active.
	 *
	 * @param active the new active
	 */
	public void setActive(boolean active);

	/**
	 * Gets the skin.
	 *
	 * @return the skin
	 */
	public String getSkin();

	/**
	 * Sets the skin.
	 *
	 * @param skin the new skin
	 */
	public void setSkin(String skin);

	/**
	 * Gets the multilingual.
	 *
	 * @return the multilingual
	 */
	public boolean getMultilingual();

	/**
	 * Sets the multilingual.
	 *
	 * @param multilingual the new multilingual
	 */
	public void setMultilingual(boolean multilingual);

	/**
	 * Gets the e account.
	 *
	 * @return the e account
	 */
	public IEAccount getEAccount();

	/**
	 * Sets the e account.
	 *
	 * @param eAccount the new e account
	 */
	public void setEAccount(IEAccount eAccount);

}