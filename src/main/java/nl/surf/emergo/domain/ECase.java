/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

import java.io.Serializable;
import java.util.Hashtable;

/**
 * The Class ECase.
 */
public class ECase extends EDomainEntity implements Serializable, Cloneable, IECase {

	private static final long serialVersionUID = -4360987312173170413L;

	/** The cas id. */
	protected int casId;

	/** The com com id. */
	protected int casCasId;

	/** The code. */
	protected String code;

	/** The name. */
	protected String name;

	/** The version. */
	protected int version;

	/** The status. */
	protected int status;

	/** The active. */
	protected boolean active;
	
	/** The code. */
	protected String skin;

	/** The multilingual. */
	protected boolean multilingual;

	/** The e account. */
	protected IEAccount eAccount;

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() {
		Object lObject = null;
		try {
			lObject = super.clone();
		} catch (CloneNotSupportedException cnse) {
		}
		return lObject;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECase#getCasId()
	 */
	public int getCasId() {
		return casId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECase#setCasId(int)
	 */
	public void setCasId(int casId) {
		this.casId = casId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECase#getCasCasId()
	 */
	public int getCasCasId() {
		return casCasId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECase#setCasCasId(int)
	 */
	public void setCasCasId(int casCasId) {
		this.casCasId = casCasId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECase#getCode()
	 */
	public String getCode() {
		return code;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECase#setCode(java.lang.String)
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECase#getName()
	 */
	public String getName() {
		return name;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECase#setName(java.lang.String)
	 */
	public void setName(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECase#getVersion()
	 */
	public int getVersion() {
		return version;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECase#setVersion(int)
	 */
	public void setVersion(int version) {
		this.version = version;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECase#getStatus()
	 */
	public int getStatus() {
		return status;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECase#setStatus(int)
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECase#getActive()
	 */
	public boolean getActive() {
		return active;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECase#setActive(boolean)
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECase#getSkin()
	 */
	public String getSkin() {
		return skin;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECase#setSkin(java.lang.String)
	 */
	public void setSkin(String skin) {
		this.skin = skin;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECase#getMultilingual()
	 */
	public boolean getMultilingual() {
		return multilingual;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECase#setMultilingual(boolean)
	 */
	public void setMultilingual(boolean multilingual) {
		this.multilingual = multilingual;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECase#getEAccount()
	 */
	public IEAccount getEAccount() {
		return eAccount;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECase#setEAccount(nl.surf.emergo.domain.IEAccount)
	 */
	public void setEAccount(IEAccount eAccount) {
		this.eAccount = eAccount;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getProperties()
	 */
	public Hashtable<String,String> getProperties() {
		Hashtable<String, String> lProperties = super.getProperties();
		lProperties.put("casId", "" + casId);
		lProperties.put("accAccId", "" + (eAccount != null ? eAccount.getAccId() : 0));
		lProperties.put("casCasId", "" + casCasId);
		lProperties.put("code", "" + code);
		lProperties.put("name", "" + name);
		lProperties.put("version", "" + version);
		lProperties.put("status", "" + status);
		lProperties.put("active", "" + active);
		lProperties.put("skin", skin);
		lProperties.put("multilingual", "" + multilingual);
		return lProperties;
	}

}