/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

/**
 * The Interface IERunGroupCaseComponentUpdate.
 */
public interface IERunGroupCaseComponentUpdate extends IEDomainEntity {

	/**
	 * Clones.
	 * 
	 * @return the object
	 */
	public Object clone();

	/**
	 * Gets the rgu id.
	 * 
	 * @return the rgu id
	 */
	public int getRguId();

	/**
	 * Sets the rgu id.
	 * 
	 * @param rguId the new rgu id
	 */
	public void setRguId(int rguId);

	/**
	 * Gets the rug rug id.
	 * 
	 * @return the rug rug id
	 */
	public int getRugRugId();

	/**
	 * Sets the rug rug id.
	 * 
	 * @param rugRugId the new rug rug id
	 */
	public void setRugRugId(int rugRugId);

	/**
	 * Gets the cac cac id.
	 * 
	 * @return the cac cac id
	 */
	public int getCacCacId();

	/**
	 * Sets the cac cac id.
	 * 
	 * @param cacCacId the new cac cac id
	 */
	public void setCacCacId(int cacCacId);

	/**
	 * Gets the rug rug from id.
	 * 
	 * @return the rug rug from id
	 */
	public int getRugRugFromId();

	/**
	 * Sets the rug rug from id.
	 * 
	 * @param rugRugFromId the new rug rug from id
	 */
	public void setRugRugFromId(int rugRugFromId);

	/**
	 * Gets the xmldata.
	 * 
	 * @return the xmldata
	 */
	public String getXmldata();

	/**
	 * Sets the xmldata.
	 * 
	 * @param xmldata the new xmldata
	 */
	public void setXmldata(String xmldata);

	/**
	 * Gets the processed.
	 * 
	 * @return the processed
	 */
	public boolean getProcessed();

	/**
	 * Sets the processed.
	 * 
	 * @param processed the new processed
	 */
	public void setProcessed(boolean processed);

}