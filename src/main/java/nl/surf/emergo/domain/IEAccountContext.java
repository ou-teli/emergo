/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

/**
 * The Interface IEAccountContext.
 */
public interface IEAccountContext extends IEDomainEntity {

	/**
	 * Clones.
	 * 
	 * @return the object
	 */
	public Object clone();

	/**
	 * Gets the acc id.
	 * 
	 * @return the acc id
	 */
	public int getAccId();

	/**
	 * Sets the acc id.
	 * 
	 * @param accId the new acc id
	 */
	public void setAccId(int accId);

	/**
	 * Gets the acc acc id.
	 * 
	 * @return the acc acc id
	 */
	public int getAccAccId();

	/**
	 * Sets the acc acc id.
	 * 
	 * @param accAccId the new acc acc id
	 */
	public void setAccAccId(int accAccId);

	/**
	 * Gets the con con id.
	 * 
	 * @return the con con id
	 */
	public int getConConId();

	/**
	 * Sets the con con id.
	 * 
	 * @param conConId the new con con id
	 */
	public void setConConId(int conConId);

	/**
	 * Gets the active.
	 * 
	 * @return the active
	 */
	public boolean getActive();

	/**
	 * Sets the active.
	 * 
	 * @param active the new active
	 */
	public void setActive(boolean active);

}