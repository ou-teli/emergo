/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

import java.io.Serializable;
import java.util.Hashtable;

/**
 * The Class EMail.
 */
public class EMail extends EDomainEntity implements Serializable,Cloneable,IEMail {

	private static final long serialVersionUID = 1493023387742767587L;

	/** The mai id. */
	protected int maiId;

	/** The code. */
	protected String code;

	/** The description. */
	protected String description;

	/** The subject. */
	protected String subject;

	/** The body. */
	protected String body;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	public Object clone() {
		Object lObject = null;
		try {
			lObject = super.clone();
		} catch (CloneNotSupportedException cnse) {
		}
		return lObject;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.surf.emergo.domain.IEMail#getMaiId()
	 */
	public int getMaiId() {
		return maiId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.surf.emergo.domain.IEMail#setMaiId(int)
	 */
	public void setMaiId(int maiId) {
		this.maiId = maiId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.surf.emergo.domain.IEMail#getCode()
	 */
	public String getCode() {
		return code;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.surf.emergo.domain.IEMail#setCode(java.lang.String)
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.surf.emergo.domain.IEMail#getDescription()
	 */
	public String getDescription() {
		return description;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.surf.emergo.domain.IEMail#setDescription(java.lang.String)
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.surf.emergo.domain.IEMail#getSubject()
	 */
	public String getSubject() {
		return subject;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.surf.emergo.domain.IEMail#setSubject(java.lang.String)
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.surf.emergo.domain.IEMail#getBody()
	 */
	public String getBody() {
		return body;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.surf.emergo.domain.IEMail#setBody(java.lang.String)
	 */
	public void setBody(String body) {
		this.body = body;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getProperties()
	 */
	public Hashtable<String,String> getProperties() {
		Hashtable<String, String> lProperties = super.getProperties();
		lProperties.put("maiId", "" + maiId);
		lProperties.put("code", "" + code);
		lProperties.put("description", "" + description);
		lProperties.put("subject", "" + subject);
		lProperties.put("body", "" + body);
		return lProperties;
	}

}