/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

import java.io.Serializable;
import java.util.Hashtable;

/**
 * The Class EComponent.
 */
public class EComponent extends EDomainEntity implements Serializable, Cloneable, IEComponent {
	
	private static final long serialVersionUID = 4198766707968056245L;

	/** The com id. */
	protected int comId;

	/** The com com id. */
	protected int comComId;

	/** The code. */
	protected String code;

	/** The uuid. */
	protected String uuid;

	/** The type. */
	protected int type;

	/** The version. */
	protected int version;

	/** The xmldefinition. */
	protected String xmldefinition;

	/** The active. */
	protected boolean active;

	/** The e account. */
	protected IEAccount eAccount;

	/** The multiple. */
	protected boolean multiple;

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() {
		Object lObject = null;
		try {
			lObject = super.clone();
		} catch (CloneNotSupportedException cnse) {
		}
		return lObject;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEComponent#getComId()
	 */
	public int getComId() {
		return comId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEComponent#setComId(int)
	 */
	public void setComId(int comId) {
		this.comId = comId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEComponent#getComComId()
	 */
	public int getComComId() {
		return comComId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEComponent#setComComId(int)
	 */
	public void setComComId(int comComId) {
		this.comComId = comComId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEComponent#getCode()
	 */
	public String getCode() {
		return code;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEComponent#setCode(java.lang.String)
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEComponent#getUuid()
	 */
	public String getUuid() {
		return uuid;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEComponent#setUuid(java.lang.String)
	 */
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEComponent#getType()
	 */
	public int getType() {
		return type;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEComponent#setType(int)
	 */
	public void setType(int type) {
		this.type = type;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEComponent#getVersion()
	 */
	public int getVersion() {
		return version;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEComponent#setVersion(int)
	 */
	public void setVersion(int version) {
		this.version = version;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEComponent#getXmldefinition()
	 */
	public String getXmldefinition() {
		return xmldefinition;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEComponent#setXmldefinition(java.lang.String)
	 */
	public void setXmldefinition(String xmldefinition) {
		this.xmldefinition = xmldefinition;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEComponent#getActive()
	 */
	public boolean getActive() {
		return active;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEComponent#setActive(boolean)
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEComponent#getMultiple()
	 */
	public boolean getMultiple() {
		return multiple;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEComponent#setMultiple(boolean)
	 */
	public void setMultiple(boolean multiple) {
		this.multiple = multiple;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEComponent#getEAccount()
	 */
	public IEAccount getEAccount() {
		return eAccount;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEComponent#setEAccount(nl.surf.emergo.domain.IEAccount)
	 */
	public void setEAccount(IEAccount eAccount) {
		this.eAccount = eAccount;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getProperties()
	 */
	public Hashtable<String,String> getProperties() {
		Hashtable<String, String> lProperties = super.getProperties();
		lProperties.put("comId", "" + comId);
		lProperties.put("accAccId", "" + (eAccount != null ? eAccount.getAccId() : 0));
		lProperties.put("comComId", "" + comComId);
		lProperties.put("code", "" + code);
		lProperties.put("uuid", "" + uuid);
		lProperties.put("type", "" + type);
		lProperties.put("version", "" + version);
		lProperties.put("xmldefinition", "" + xmldefinition);
		lProperties.put("active", "" + active);
		lProperties.put("multiple", "" + multiple);
		return lProperties;
	}

}