/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.Set;

/**
 * The Class ERole.
 */
public class ERole extends EDomainEntity implements Serializable, Cloneable, IERole {
	
	private static final long serialVersionUID = 3802499334010385151L;

	/** The rol id. */
	protected int rolId;

	/** The code. */
	protected String code;

	/** The e accounts. */
	protected Set<IEAccount> eAccounts;

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() {
		Object lObject = null;
		try {
			lObject = super.clone();
		} catch (CloneNotSupportedException cnse) {
		}
		return lObject;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERole#getRolId()
	 */
	public int getRolId() {
		return rolId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERole#setRolId(int)
	 */
	public void setRolId(int rolId) {
		this.rolId = rolId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERole#getCode()
	 */
	public String getCode() {
		return code;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERole#setCode(java.lang.String)
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERole#getEAccounts()
	 */
	public Set<IEAccount> getEAccounts() {
		return eAccounts;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERole#setEAccounts(java.util.Set)
	 */
	public void setEAccounts(Set<IEAccount> eAccounts) {
		this.eAccounts = eAccounts;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getProperties()
	 */
	public Hashtable<String,String> getProperties() {
		Hashtable<String, String> lProperties = super.getProperties();
		lProperties.put("rolId", "" + rolId);
		lProperties.put("code", "" + code);
		return lProperties;
	}

}