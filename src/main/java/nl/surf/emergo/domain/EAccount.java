/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.Set;

/**
 * The Class EAccount.
 */
public class EAccount extends EDomainEntity implements Serializable, Cloneable, IEAccount {
	
	private static final long serialVersionUID = 7952954893233701092L;

	/** The acc id. */
	protected int accId;

	/** The acc acc id. */
	protected int accAccId;

	/** The userid. */
	protected String userid;

	/** The password. */
	protected String password;

	/** The studentid. */
	protected String studentid;

	/** The title. */
	protected String title;

	/** The initials. */
	protected String initials;

	/** The nameprefix. */
	protected String nameprefix;

	/** The lastname. */
	protected String lastname;

	/** The email. */
	protected String email;

	/** The phonenumber. */
	protected String phonenumber;

	/** The job. */
	protected String job;

	/** The extradata. */
	protected String extradata;

	/** The active. */
	protected boolean active;

	/** The openaccess. */
	protected boolean openaccess;

	/** The e roles. */
	protected Set<IERole> eRoles;

	/** The e account roles. */
	protected Set<IEAccountRole> eAccountRoles;

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() {
		Object lObject = null;
		try {
			lObject = super.clone();
		} catch (CloneNotSupportedException cnse) {
		}
		return lObject;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getAccId()
	 */
	public int getAccId() {
		return accId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#setAccId(int)
	 */
	public void setAccId(int accId) {
		this.accId = accId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getAccAccId()
	 */
	public int getAccAccId() {
		return accAccId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#setAccAccId(int)
	 */
	public void setAccAccId(int accAccId) {
		this.accAccId = accAccId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getUserid()
	 */
	public String getUserid() {
		return userid;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#setUserid(java.lang.String)
	 */
	public void setUserid(String userid) {
		this.userid = userid;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getPassword()
	 */
	public String getPassword() {
		return password;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#setPassword(java.lang.String)
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getStudentid()
	 */
	public String getStudentid() {
		return studentid;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#setStudentid(java.lang.String)
	 */
	public void setStudentid(String studentid) {
		this.studentid = studentid;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getTitle()
	 */
	public String getTitle() {
		return title;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#setTitle(java.lang.String)
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getInitials()
	 */
	public String getInitials() {
		return initials;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#setInitials(java.lang.String)
	 */
	public void setInitials(String initials) {
		this.initials = initials;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getNameprefix()
	 */
	public String getNameprefix() {
		return nameprefix;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#setNameprefix(java.lang.String)
	 */
	public void setNameprefix(String nameprefix) {
		this.nameprefix = nameprefix;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getLastname()
	 */
	public String getLastname() {
		return lastname;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#setLastname(java.lang.String)
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getEmail()
	 */
	public String getEmail() {
		return email;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#setEmail(java.lang.String)
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getPhonenumber()
	 */
	public String getPhonenumber() {
		return phonenumber;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#setPhonenumber(java.lang.String)
	 */
	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getJob()
	 */
	public String getJob() {
		return job;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#setJob(java.lang.String)
	 */
	public void setJob(String job) {
		this.job = job;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getExtradata()
	 */
	public String getExtradata() {
		return extradata;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#setExtradata(java.lang.String)
	 */
	public void setExtradata(String extradata) {
		this.extradata = extradata;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getActive()
	 */
	public boolean getActive() {
		return active;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#setActive(boolean)
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getOpenaccess()
	 */
	public boolean getOpenaccess() {
		return openaccess;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#setOpenaccess(boolean)
	 */
	public void setOpenaccess(boolean openaccess) {
		this.openaccess = openaccess;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getERoles()
	 */
	public Set<IERole> getERoles() {
		return eRoles;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#setERoles(java.util.Set)
	 */
	public void setERoles(Set<IERole> eRoles) {
		this.eRoles = eRoles;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getERoles()
	 */
	public Set<IEAccountRole> getEAccountRoles() {
		return eAccountRoles;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#setERoles(java.util.Set)
	 */
	public void setEAccountRoles(Set<IEAccountRole> eAccountRoles) {
		this.eAccountRoles = eAccountRoles;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getProperties()
	 */
	public Hashtable<String, String> getProperties() {
		Hashtable<String, String> lProperties = super.getProperties();
		lProperties.put("accId", "" + accId);
		lProperties.put("accAccId", "" + accAccId);
		lProperties.put("userid", "" + userid);
		lProperties.put("password", "" + password);
		lProperties.put("studentid", "" + studentid);
		lProperties.put("title", "" + title);
		lProperties.put("initials", "" + initials);
		lProperties.put("nameprefix", "" + nameprefix);
		lProperties.put("lastname", "" + lastname);
		lProperties.put("email", "" + email);
		lProperties.put("phonenumber", "" + phonenumber);
		lProperties.put("extradata", "" + extradata);
		lProperties.put("job", "" + job);
		lProperties.put("active", "" + active);
		lProperties.put("openaccess", "" + openaccess);
		return lProperties;
	}

}