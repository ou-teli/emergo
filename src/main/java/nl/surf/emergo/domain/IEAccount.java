/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

import java.util.Set;

/**
 * The Interface IEAccount.
 */
public interface IEAccount extends IEDomainEntity {

	/**
	 * Clones.
	 * 
	 * @return the object
	 */
	public Object clone();

	/**
	 * Gets the acc id.
	 * 
	 * @return the acc id
	 */
	public int getAccId();

	/**
	 * Sets the acc id.
	 * 
	 * @param accId the new acc id
	 */
	public void setAccId(int accId);

	/**
	 * Gets the acc acc id.
	 * 
	 * @return the acc acc id
	 */
	public int getAccAccId();

	/**
	 * Sets the acc acc id.
	 * 
	 * @param accAccId the new acc acc id
	 */
	public void setAccAccId(int accAccId);

	/**
	 * Gets the userid.
	 * 
	 * @return the userid
	 */
	public String getUserid();

	/**
	 * Sets the userid.
	 * 
	 * @param userid the new userid
	 */
	public void setUserid(String userid);

	/**
	 * Gets the password.
	 * 
	 * @return the password
	 */
	public String getPassword();

	/**
	 * Sets the password.
	 * 
	 * @param password the new password
	 */
	public void setPassword(String password);

	/**
	 * Gets the studentid.
	 * 
	 * @return the studentid
	 */
	public String getStudentid();

	/**
	 * Sets the studentid.
	 * 
	 * @param studentid the new studentid
	 */
	public void setStudentid(String studentid);

	/**
	 * Gets the title.
	 * 
	 * @return the title
	 */
	public String getTitle();

	/**
	 * Sets the title.
	 * 
	 * @param title the new title
	 */
	public void setTitle(String title);

	/**
	 * Gets the initials.
	 * 
	 * @return the initials
	 */
	public String getInitials();

	/**
	 * Sets the initials.
	 * 
	 * @param initials the new initials
	 */
	public void setInitials(String initials);

	/**
	 * Gets the nameprefix.
	 * 
	 * @return the nameprefix
	 */
	public String getNameprefix();

	/**
	 * Sets the nameprefix.
	 * 
	 * @param nameprefix the new nameprefix
	 */
	public void setNameprefix(String nameprefix);

	/**
	 * Gets the lastname.
	 * 
	 * @return the lastname
	 */
	public String getLastname();

	/**
	 * Sets the lastname.
	 * 
	 * @param lastname the new lastname
	 */
	public void setLastname(String lastname);

	/**
	 * Gets the email.
	 * 
	 * @return the email
	 */
	public String getEmail();

	/**
	 * Sets the email.
	 * 
	 * @param email the new email
	 */
	public void setEmail(String email);

	/**
	 * Gets the phonenumber.
	 * 
	 * @return the phonenumber
	 */
	public String getPhonenumber();

	/**
	 * Sets the phonenumber.
	 * 
	 * @param phonenumber the new phonenumber
	 */
	public void setPhonenumber(String phonenumber);

	/**
	 * Gets the job.
	 * 
	 * @return the job
	 */
	public String getJob();

	/**
	 * Sets the job.
	 * 
	 * @param job the new job
	 */
	public void setJob(String job);

	/**
	 * Gets the extra data.
	 * 
	 * @return the extra data
	 */
	public String getExtradata();

	/**
	 * Sets the extra data.
	 * 
	 * @param extra data the new extra data
	 */
	public void setExtradata(String extradata);

	/**
	 * Gets the active.
	 * 
	 * @return the active
	 */
	public boolean getActive();

	/**
	 * Sets the active.
	 * 
	 * @param active the new active
	 */
	public void setActive(boolean active);

	/**
	 * Gets the openaccess.
	 * 
	 * @return the openaccess
	 */
	public boolean getOpenaccess();

	/**
	 * Sets the openaccess.
	 * 
	 * @param openaccess the new openaccess
	 */
	public void setOpenaccess(boolean openaccess);

	/**
	 * Gets the e roles.
	 * 
	 * @return the e roles
	 */
	public Set<IERole> getERoles();

	/**
	 * Sets the e roles.
	 * 
	 * @param eRoles the new e roles
	 */
	public void setERoles(Set<IERole> eRoles);

	/**
	 * Gets the e account roles.
	 * 
	 * @return the e account roles
	 */
	public Set<IEAccountRole> getEAccountRoles();

	/**
	 * Sets the e account roles.
	 * 
	 * @param eAccountRoles the new e account roles
	 */
	public void setEAccountRoles(Set<IEAccountRole> eAccountRoles);

}