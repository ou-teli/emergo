/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

/**
 * The Interface IEComponent.
 */
public interface IEComponent extends IEDomainEntity {

	/**
	 * Clones.
	 * 
	 * @return the object
	 */
	public Object clone();

	/**
	 * Gets the com id.
	 * 
	 * @return the com id
	 */
	public int getComId();

	/**
	 * Sets the com id.
	 * 
	 * @param comId the new com id
	 */
	public void setComId(int comId);

	/**
	 * Gets the com com id.
	 * 
	 * @return the com com id
	 */
	public int getComComId();

	/**
	 * Sets the com com id.
	 * 
	 * @param comComId the new com com id
	 */
	public void setComComId(int comComId);

	/**
	 * Gets the code.
	 * 
	 * @return the code
	 */
	public String getCode();

	/**
	 * Sets the code.
	 * 
	 * @param code the new code
	 */
	public void setCode(String code);

	/**
	 * Gets the uuid.
	 * 
	 * @return the uuid
	 */
	public String getUuid();

	/**
	 * Sets the uuid.
	 * 
	 * @param uuid the new uuid
	 */
	public void setUuid(String uuid);

	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	public int getType();

	/**
	 * Sets the type.
	 * 
	 * @param type the new type
	 */
	public void setType(int type);

	/**
	 * Gets the version.
	 * 
	 * @return the version
	 */
	public int getVersion();

	/**
	 * Sets the version.
	 * 
	 * @param version the new version
	 */
	public void setVersion(int version);

	/**
	 * Gets the xmldefinition.
	 * 
	 * @return the xmldefinition
	 */
	public String getXmldefinition();

	/**
	 * Sets the xmldefinition.
	 * 
	 * @param xmldefinition the new xmldefinition
	 */
	public void setXmldefinition(String xmldefinition);

	/**
	 * Gets the active.
	 * 
	 * @return the active
	 */
	public boolean getActive();

	/**
	 * Sets the active.
	 * 
	 * @param active the new active
	 */
	public void setActive(boolean active);

	/**
	 * Gets the multiple.
	 * 
	 * @return the multiple
	 */
	public boolean getMultiple();

	/**
	 * Sets the multiple.
	 * 
	 * @param multiple the new multiple
	 */
	public void setMultiple(boolean multiple);

	/**
	 * Gets the e account.
	 * 
	 * @return the e account
	 */
	public IEAccount getEAccount();

	/**
	 * Sets the e account.
	 * 
	 * @param eAccount the new e account
	 */
	public void setEAccount(IEAccount eAccount);

}