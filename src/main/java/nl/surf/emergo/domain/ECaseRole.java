/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

import java.io.Serializable;
import java.util.Hashtable;

/**
 * The Class ECaseRole.
 */
public class ECaseRole extends EDomainEntity implements Serializable, Cloneable, IECaseRole {
	
	private static final long serialVersionUID = -2625185289083733975L;

	/** The car id. */
	protected int carId;

	/** The name. */
	protected String name;

	/** The npc. */
	protected boolean npc;

	/** The e case. */
	protected IECase eCase;

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() {
		Object lObject = null;
		try {
			lObject = super.clone();
		} catch (CloneNotSupportedException cnse) {
		}
		return lObject;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECaseRole#getCarId()
	 */
	public int getCarId() {
		return carId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECaseRole#setCarId(int)
	 */
	public void setCarId(int carId) {
		this.carId = carId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECaseRole#getName()
	 */
	public String getName() {
		return name;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECaseRole#setName(java.lang.String)
	 */
	public void setName(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECaseRole#getNpc()
	 */
	public boolean getNpc() {
		return npc;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECaseRole#setNpc(boolean)
	 */
	public void setNpc(boolean npc) {
		this.npc = npc;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECaseRole#getECase()
	 */
	public IECase getECase() {
		return eCase;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECaseRole#setECase(nl.surf.emergo.domain.IECase)
	 */
	public void setECase(IECase eCase) {
		this.eCase = eCase;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getProperties()
	 */
	public Hashtable<String,String> getProperties() {
		Hashtable<String, String> lProperties = super.getProperties();
		lProperties.put("carId", "" + carId);
		lProperties.put("casCasId", "" + (eCase != null ? eCase.getCasId() : 0));
		lProperties.put("name", "" + name);
		lProperties.put("npc", "" + npc);
		return lProperties;
	}

}