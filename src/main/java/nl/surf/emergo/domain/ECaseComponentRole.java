/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

import java.io.Serializable;
import java.util.Hashtable;

/**
 * The Class ECaseComponentRole.
 */
public class ECaseComponentRole extends EDomainEntity implements Serializable, Cloneable, IECaseComponentRole {
	
	private static final long serialVersionUID = 1103204404434702830L;

	/** The ccr id. */
	protected int ccrId;

	/** The cac cac id. */
	protected int cacCacId;

	/** The car car id. */
	protected int carCarId;

	/** The name. */
	protected String name;

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	public Object clone() {
		Object lObject = null;
		try {
			lObject = super.clone();
		} catch (CloneNotSupportedException cnse) {
		}
		return lObject;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECaseComponentRole#getCcrId()
	 */
	public int getCcrId() {
		return ccrId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECaseComponentRole#setCcrId(int)
	 */
	public void setCcrId(int ccrId) {
		this.ccrId = ccrId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECaseComponentRole#getCacCacId()
	 */
	public int getCacCacId() {
		return cacCacId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECaseComponentRole#setCacCacId(int)
	 */
	public void setCacCacId(int cacCacId) {
		this.cacCacId = cacCacId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECaseComponentRole#getCarCarId()
	 */
	public int getCarCarId() {
		return carCarId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECaseComponentRole#setCarCarId(int)
	 */
	public void setCarCarId(int carCarId) {
		this.carCarId = carCarId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECaseComponentRole#getName()
	 */
	public String getName() {
		return name;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IECaseComponentRole#setName(java.lang.String)
	 */
	public void setName(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getProperties()
	 */
	public Hashtable<String,String> getProperties() {
		Hashtable<String, String> lProperties = super.getProperties();
		lProperties.put("ccrId", "" + ccrId);
		lProperties.put("cacCacId", "" + cacCacId);
		lProperties.put("carCarId", "" + carCarId);
		lProperties.put("name", "" + name);
		return lProperties;
	}

}