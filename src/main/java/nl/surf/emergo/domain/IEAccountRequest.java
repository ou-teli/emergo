/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

/**
 * The Interface IEAccountRequest.
 */
public interface IEAccountRequest extends IEDomainEntity {

	/**
	 * Clones.
	 * 
	 * @return the object
	 */
	public Object clone();

	/**
	 * Gets the arq id.
	 * 
	 * @return the arq id
	 */
	public int getArqId();

	/**
	 * Sets the arq id.
	 * 
	 * @param arqId the new arq id
	 */
	public void setArqId(int arqId);

	/**
	 * Gets the userid.
	 * 
	 * @return the userid
	 */
	public String getUserid();

	/**
	 * Sets the userid.
	 * 
	 * @param userid the new userid
	 */
	public void setUserid(String userid);

	/**
	 * Gets the password.
	 * 
	 * @return the password
	 */
	public String getPassword();

	/**
	 * Sets the password.
	 * 
	 * @param password the new password
	 */
	public void setPassword(String password);

	/**
	 * Gets the studentid.
	 * 
	 * @return the studentid
	 */
	public String getStudentid();

	/**
	 * Sets the studentid.
	 * 
	 * @param studentid the new studentid
	 */
	public void setStudentid(String studentid);

	/**
	 * Gets the title.
	 * 
	 * @return the title
	 */
	public String getTitle();

	/**
	 * Sets the title.
	 * 
	 * @param title the new title
	 */
	public void setTitle(String title);

	/**
	 * Gets the initials.
	 * 
	 * @return the initials
	 */
	public String getInitials();

	/**
	 * Sets the initials.
	 * 
	 * @param initials the new initials
	 */
	public void setInitials(String initials);

	/**
	 * Gets the nameprefix.
	 * 
	 * @return the nameprefix
	 */
	public String getNameprefix();

	/**
	 * Sets the nameprefix.
	 * 
	 * @param nameprefix the new nameprefix
	 */
	public void setNameprefix(String nameprefix);

	/**
	 * Gets the lastname.
	 * 
	 * @return the lastname
	 */
	public String getLastname();

	/**
	 * Sets the lastname.
	 * 
	 * @param lastname the new lastname
	 */
	public void setLastname(String lastname);

	/**
	 * Gets the email.
	 * 
	 * @return the email
	 */
	public String getEmail();

	/**
	 * Sets the email.
	 * 
	 * @param email the new email
	 */
	public void setEmail(String email);

	/**
	 * Gets the processed.
	 * 
	 * @return the processed
	 */
	public boolean getProcessed();

	/**
	 * Sets the processed.
	 * 
	 * @param processed the new processed
	 */
	public void setProcessed(boolean processed);

}