/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

/**
 * The Interface IERunContext.
 */
public interface IERunContext extends IEDomainEntity {

	/**
	 * Clones.
	 * 
	 * @return the object
	 */
	public Object clone();

	/**
	 * Gets the ruc id.
	 * 
	 * @return the ruc id
	 */
	public int getRucId();

	/**
	 * Sets the ruc id.
	 * 
	 * @param rucId the new ruc id
	 */
	public void setRucId(int rucId);

	/**
	 * Gets the run run id.
	 * 
	 * @return the run run id
	 */
	public int getRunRunId();

	/**
	 * Sets the run run id.
	 * 
	 * @param runRunId the new run run id
	 */
	public void setRunRunId(int runRunId);

	/**
	 * Gets the con con id.
	 * 
	 * @return the con con id
	 */
	public int getConConId();

	/**
	 * Sets the con con id.
	 * 
	 * @param conConId the new con con id
	 */
	public void setConConId(int conConId);

}