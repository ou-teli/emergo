/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

import java.util.Set;

/**
 * The Interface IERole.
 */
public interface IERole extends IEDomainEntity {

	/**
	 * Clones.
	 * 
	 * @return the object
	 */
	public Object clone();

	/**
	 * Gets the rol id.
	 * 
	 * @return the rol id
	 */
	public int getRolId();

	/**
	 * Sets the rol id.
	 * 
	 * @param rolId the new rol id
	 */
	public void setRolId(int rolId);

	/**
	 * Gets the code.
	 * 
	 * @return the code
	 */
	public String getCode();

	/**
	 * Sets the code.
	 * 
	 * @param code the new code
	 */
	public void setCode(String code);

	/**
	 * Gets the e accounts.
	 * 
	 * @return the e accounts
	 */
	public Set<IEAccount> getEAccounts();

	/**
	 * Sets the e accounts.
	 * 
	 * @param eAccounts the new e accounts
	 */
	public void setEAccounts(Set<IEAccount> eAccounts);

}