/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Hashtable;

/**
 * The Class ERun.
 */
public class ERun extends EDomainEntity implements Serializable, Cloneable, IERun {
	
	private static final long serialVersionUID = 3236442259191654682L;

	/** The run id. */
	protected int runId;

	/** The code. */
	protected String code;

	/** The name. */
	protected String name;

	/** The startdate. */
	protected Date startdate;

	/** The enddate. */
	protected Date enddate;

	/** The status. */
	protected int status;

	/** The active. */
	protected boolean active;

	/** The openaccess parameter. */
	protected boolean openaccess;

	/** The name. */
	protected String accessmailsubject;

	/** The name. */
	protected String accessmailbody;

	/** The xmldata. */
	protected String xmldata;

	/** The e case. */
	protected IECase eCase;

	/** The e account. */
	protected IEAccount eAccount;

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() {
		Object lObject = null;
		try {
			lObject = super.clone();
		} catch (CloneNotSupportedException cnse) {
		}
		return lObject;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERun#getRunId()
	 */
	public int getRunId() {
		return runId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERun#setRunId(int)
	 */
	public void setRunId(int runId) {
		this.runId = runId;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERun#getCode()
	 */
	public String getCode() {
		return code;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERun#setCode(java.lang.String)
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERun#getName()
	 */
	public String getName() {
		return name;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERun#setName(java.lang.String)
	 */
	public void setName(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERun#getStartdate()
	 */
	public Date getStartdate() {
		return startdate;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERun#setStartdate(java.util.Date)
	 */
	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERun#getEnddate()
	 */
	public Date getEnddate() {
		return enddate;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERun#setEnddate(java.util.Date)
	 */
	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERun#getStatus()
	 */
	public int getStatus() {
		return status;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERun#setStatus(int)
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERun#getActive()
	 */
	public boolean getActive() {
		return active;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERun#setActive(boolean)
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERun#getOpenaccess()
	 */
	public boolean getOpenaccess() {
		return openaccess;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERun#setOpenaccess(boolean)
	 */
	public void setOpenaccess(boolean openaccess) {
		this.openaccess = openaccess;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERun#getAccessmailsubject()
	 */
	public String getAccessmailsubject() {
		return accessmailsubject;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERun#setAccessmailsubject(java.lang.String)
	 */
	public void setAccessmailsubject(String accessmailsubject) {
		this.accessmailsubject = accessmailsubject;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERun#getAccessmailbody()
	 */
	public String getAccessmailbody() {
		return accessmailbody;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERun#setAccessmailbody(java.lang.String)
	 */
	public void setAccessmailbody(String accessmailbody) {
		this.accessmailbody = accessmailbody;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERun#getXmldata()
	 */
	public String getXmldata() {
		return xmldata;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERun#setXmldata(java.lang.String)
	 */
	public void setXmldata(String xmldata) {
		this.xmldata = xmldata;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERun#getECase()
	 */
	public IECase getECase() {
		return eCase;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERun#setECase(nl.surf.emergo.domain.IECase)
	 */
	public void setECase(IECase eCase) {
		this.eCase = eCase;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERun#getEAccount()
	 */
	public IEAccount getEAccount() {
		return eAccount;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IERun#setEAccount(nl.surf.emergo.domain.IEAccount)
	 */
	public void setEAccount(IEAccount eAccount) {
		this.eAccount = eAccount;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.domain.IEAccount#getProperties()
	 */
	public Hashtable<String,String> getProperties() {
		Hashtable<String, String> lProperties = super.getProperties();
		lProperties.put("runId", "" + runId);
		lProperties.put("accAccId", "" + (eAccount != null ? eAccount.getAccId() : 0));
		lProperties.put("casCasId", "" + (eCase != null ? eCase.getCasId() : 0));
		lProperties.put("code", "" + code);
		lProperties.put("name", "" + name);
		lProperties.put("startdate", "" + startdate);
		lProperties.put("enddate", "" + enddate);
		lProperties.put("status", "" + status);
		lProperties.put("active", "" + active);
		lProperties.put("openaccess", "" + openaccess);
		lProperties.put("accessmailsubject", "" + accessmailsubject);
		lProperties.put("accessmailbody", "" + accessmailbody);
		lProperties.put("xmldata", "" + xmldata);
		return lProperties;
	}

}