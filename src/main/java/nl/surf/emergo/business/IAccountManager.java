/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IEAccountRole;
import nl.surf.emergo.domain.IERole;

/**
 * The Interface IAccountManager.
 * 
 * <p>For managing all accounts and their roles.</p>
 * 
 * <p>If an account is deleted all related blobs have to be deleted too.
 * Blobs are not deleted in db cascade because they are referenced from within xml data, so should be deleted separately.</p>
 */
public interface IAccountManager {

	/**
	 * Gets new account.
	 * 
	 * @return account
	 */
	public IEAccount getNewAccount();

	/**
	 * Gets account.
	 * 
	 * @param accId the db acc id
	 * 
	 * @return account
	 */
	public IEAccount getAccount(int accId);

	/**
	 * Gets account.
	 * 
	 * @param userid the db userid
	 * 
	 * @return account
	 */
	public IEAccount getAccount(String userid);

	/**
	 * Gets account.
	 * 
	 * @param userid the db userid
	 * @param password the db password
	 * @param active the db active state
	 * 
	 * @return account
	 */
	public IEAccount getAccount(String userid, String password, boolean active);

	/**
	 * Gets full account name.
	 * 
	 * @param eAccount the account
	 * 
	 * @return full account name
	 */
	public String getAccountName(IEAccount eAccount);

	/**
	 * Saves account whithout checking object properties.
	 * 
	 * @param eAccount the account
	 */
	public void saveAccount(IEAccount eAccount);

	/**
	 * Creates new account if object properties are ok.
	 * 
	 * @param eAccount the account
	 * 
	 * @return error list
	 */
	public List<String[]> newAccount(IEAccount eAccount);

	/**
	 * Updates existing account if object properties are ok.
	 * 
	 * @param eAccount the account
	 * 
	 * @return error list
	 */
	public List<String[]> updateAccount(IEAccount eAccount);

	/**
	 * Validates account, that is if object properties are ok.
	 * 
	 * @param eAccount the account
	 * 
	 * @return error list
	 */
	public List<String[]> validateAccount(IEAccount eAccount);

	/**
	 * Deletes account.
	 * Also deletes related blobs.
	 * 
	 * @param accId the db acc id
	 */
	public void deleteAccount(int accId);

	/**
	 * Deletes account.
	 * Also deletes related blobs.
	 * 
	 * @param eAccount the account
	 */
	public void deleteAccount(IEAccount eAccount);

	/**
	 * Deletes blobs related to account.
	 * Blobs are not deleted in db cascade because they are referenced from within xml data, so should be deleted separately.
	 * 
	 * @param eAccount the account
	 */
	public void deleteBlobs(IEAccount eAccount);

	/**
	 * Gets all accounts.
	 * 
	 * @return accounts
	 */
	public List<IEAccount> getAllAccounts();

	/**
	 * Gets all active or non active accounts.
	 * 
	 * @param active the active state
	 * 
	 * @return accounts
	 */
	public List<IEAccount> getAllAccounts(boolean active);

	/**
	 * Gets all active or non active accounts within certain roles.
	 * 
	 * @param active the active state
	 * @param roles the comma separated role codes
	 * 
	 * @return accounts
	 */
	public List<IEAccount> getAllAccounts(boolean active, String roles);

	/**
	 * Gets filtered accounts.
	 * 
	 * @param keysAndValueParts the db field ids and value parts
	 * 
	 * @return accounts
	 */
	public List<IEAccount> getAllAccountsFilter(Map<String, String> keysAndValueParts);

	/**
	 * Gets active filtered accounts.
	 * 
	 * @param active the active state
	 * @param keysAndValueParts the db field ids and value parts
	 * 
	 * @return accounts
	 */
	public List<IEAccount> getAllAccountsFilter(boolean active, Map<String, String> keysAndValueParts);

	/**
	 * Gets all active or non active accounts within certain roles, filtered.
	 * 
	 * @param active the active state
	 * @param roles the comma separated role codes
	 * @param keysAndValueParts the db field ids and value parts
	 * 
	 * @return accounts
	 */
	public List<IEAccount> getAllAccountsFilter(boolean active, String roles, Map<String, String> keysAndValueParts);

	/**
	 * Gets all active or non active accounts within certain roles, within certain contexts.
	 * 
	 * @param active the active state
	 * @param roles the comma separated role codes
	 * @param conIds the context ids
	 * 
	 * @return accounts
	 */
	public List<IEAccount> getAllAccounts(boolean active, String roles, List<Integer> conIds);

	/**
	 * Gets all active or non active accounts within certain roles, within certain contexts, filtered
	 * 
	 * @param active the active state
	 * @param roles the comma separated role codes
	 * @param conIds the context ids
	 * @param keysAndValueParts the db field ids and value parts
	 * 
	 * @return accounts
	 */
	public List<IEAccount> getAllAccountsFilter(boolean active, String roles, List<Integer> conIds, Map<String, String> keysAndValueParts);

	/**
	 * Gets all accounts by email.
	 * 
	 * @param email the db email
	 * 
	 * @return accounts
	 */
	public List<IEAccount> getAllAccountsByEmail(String email);

	/**
	 * Gets all roles.
	 * 
	 * @return roles
	 */
	public List<IERole> getAllRoles();

	/**
	 * Gets all account roles.
	 * 
	 * @return account roles
	 */
	public List<IEAccountRole> getAllAccountRoles();

	/**
	 * Checks if account has role.
	 * 
	 * @param account the account
	 * @param rolCode the rol code
	 * 
	 * @return true, if has role
	 */
	public boolean hasRole(IEAccount account, String rolCode);

	/**
	 * Gets landing page for account and role.
	 * 
	 * @param account the account
	 * @param rolCode the rol code
	 * 
	 * @return landing page or empty string if account or page does not exist for the role
	 */
	public String getRoleLandingpage(IEAccount account, String rolCode);

	/**
	 * Gets role landing pages for account.
	 * 
	 * @param account the account
	 * 
	 * @return the landingpages per rol code
	 */
	public Hashtable<String,String> getRoleLandingpages(IEAccount account);

	/**
	 * Sets role landing pages for account.
	 * 
	 * @param account the account
	 * @param landingpagesPerRolCode the landingpages per rol code
	 */
	public void setRoleLandingpages(IEAccount account, Hashtable<String,String> landingpagesPerRolCode);

	/**
	 * Gives random password.
	 * 
	 * @return the password
	 */
	public String randomPassword();

	/**
	 * Encodes password using hash mechanism.
	 * 
	 * @param password the password
	 * 
	 * @return encoded password
	 */
	public String encodePassword(String password);
	
	/**
	 * Matches password using hash mechanism.
	 * 
	 * @param password the password
	 * @param encodedPassword the encoded password
	 * 
	 * @return true if match
	 */
	public boolean matchesEncodedPassword(String password, String encodedPassword);
	
}