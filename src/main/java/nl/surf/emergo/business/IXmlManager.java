/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business;

import java.util.Hashtable;
import java.util.List;

/**
 * The Interface IXmlManager.
 * 
 * <p>For managing general application xml tasks.</p>
 */
public interface IXmlManager {

	/**
	 * Escape xml string.
	 * 
	 * @param aStr the str
	 * 
	 * @return the converted string
	 */
	public String escapeXML(String aStr);

	/**
	 * Escape xml string using apache.commons.text.StringEscapeUtils.
	 * 
	 * @param aStr the str
	 * 
	 * @return the converted string
	 */
	public String escapeXMLAlt(String aStr);

	/**
	 * Unescape xml string.
	 * 
	 * @param aStr the str
	 * 
	 * @return the converted string
	 */
	public String unescapeXML(String aStr);

	/**
	 * Unescape xml string using apache.commons.text.StringEscapeUtils.
	 * 
	 * @param aStr the str
	 * 
	 * @return the converted string
	 */
	public String unescapeXMLAlt(String aStr);

	/**
	 * Checks if an xml string is valid.
	 *
	 * @param aXml the a xml string
	 *
	 * @return if valid
	 */
	public boolean isXMLValid(String aXml);

	/**
	 * Creates new xml tag.
	 * 
	 * @param aName the name of the tag
	 * @param aValue the value of the tag
	 * 
	 * @return the XML tag
	 */
	public IXMLTag newXMLTag(String aName, String aValue);

	/**
	 * Gets the xml tag tree using xml definition root tag and xml data.
	 * 
	 * @param aXmlDefRootTag the xml def root tag
	 * @param aXmlData the xml data
	 * @param aTagType the tag type
	 * 
	 * @return the xml tag tree
	 */
	public IXMLTag getXmlTree(IXMLTag aXmlDefRootTag, String aXmlData, int aTagType);

	/**
	 * Gets new sent data tag based on status tag.
	 * 
	 * @param aStatusTag the status tag
	 * @param aDataTag the data tag
	 * 
	 * @return the xml data tag
	 */
	public IXMLTag getNewSentDataTag(IXMLTag aStatusTag, IXMLTag aDataTag);

	/**
	 * Gets the xml tag tree using xml data tag tree and xml status tag tree. Status tags are merged within data tag tree.
	 * 
	 * @param aDataRootTag the data root tag
	 * @param aStatusRootTag the status root tag
	 * @param aDataTagsById the data tags by id
	 * @param aDataTagsByRefstatusid the data tags by ref status id
	 * 
	 * @return the xml tag tree
	 */
	public IXMLTag getXmlTree(IXMLTag aDataRootTag, IXMLTag aStatusRootTag, Hashtable<String, IXMLTag> aDataTagsById, Hashtable<String, IXMLTag> aDataTagsByRefstatusid);

	/**
	 * Gets an xml tag tree from an xml file string.
	 * 
	 * @param aXMLStr the xml string
	 * @param aRoot the name of the child that will be the root in the resulting tree
	 * @param aTagType the tag type
	 * 
	 * @return the xml tag tree
	 */
	public IXMLTag getXmlTreeFromImport(String aXMLStr, String aRoot, int aTagType);

	/**
	 * Copies tag as child of parent tag. Child tags of tag are also copied.
	 * 
	 * @param aTag the tag
	 * @param aParent the parent tag
	 * 
	 * @return the XML tag
	 */
	public IXMLTag copyTag(IXMLTag aTag, IXMLTag aParent);

	/**
	 * Converts a xml tag tree to a document, a xml string.
	 * 
	 * @param aRootTag the root tag
	 * 
	 * @return the string
	 */
	public String xmlTreeToDoc(IXMLTag aRootTag);

	/**
	 * Converts a xml tag tree to a document, a xml string.
	 * 
	 * @param aRootTag the root tag
	 * @param aEncoding the encoding
	 * 
	 * @return the string
	 */
	public String xmlTreeToDoc(IXMLTag aRootTag, String aEncoding);

	/**
	 * Gets the component part of the xml data. It is converted into a list.
	 * Used to render data to screen.
	 * 
	 * @param aXmlTree the xml tag tree
	 * 
	 * @return the component data
	 */
	public List<Hashtable<String,Object>> getComponentData(IXMLTag aXmlTree);

	/**
	 * Sets part of the component part of xml data tag tree.
	 * 
	 * @param aRootTag the root tag
	 * @param aKey the key
	 * @param aValue the value
	 */
	public void setCompInitialstatus(IXMLTag aRootTag,
			String aKey, String aValue);

	/**
	 * Gets status tag status child tag of data tag.
	 *
	 * @param aDataTag the a data tag
	 *
	 * @return the status tag status child tag
	 */
	public IXMLTag getStatusTagStatusChildTag(IXMLTag aDataTag);

	/**
	 * Gets the tag key values.
	 * 
	 * @param aXmlTag the xml tag
	 * @param aKey the key
	 * 
	 * @return the tag key values separated by semicolons.
	 */
	public String getTagKeyValues(IXMLTag aXmlTag, String aKey);

	/**
	 * Gets the content part of a xml tag out of the xml tag tree. It is converted into a list. Node tags are excluded.
	 * If xml tag is not found by xmlTagId or aClickedNodeTag, definition and aNodename are used to get default data.
	 * Used to render data to screen.
	 * 
	 * @param aXmlTreeDef the xml tree def
	 * @param aXmlTree the xml tree
	 * @param aNew the new
	 * @param aClickedNodeTag the clicked node tag
	 * @param aNodename the nodename
	 * @param aNewnodetype the a new node type
	 * 
	 * @return the content tag data
	 */
	public List<Hashtable<String,Object>> getContentData(IXMLTag aXmlTreeDef, IXMLTag aXmlTree,
			boolean aNew, IXMLTag aClickedNodeTag, String aNodename, String aNewnodetype);

	/**
	 * Creates new node tag with name aNodename using aXmlDef.
	 * 
	 * @param aXmlDef the xml def
	 * @param aNodename the nodename
	 * 
	 * @return the XML tag
	 */
	public IXMLTag newNodeTag(String aXmlDef, String aNodename);

	/**
	 * Puts tag aTag before aBeforeTag using aRootTag tree.
	 * 
	 * @param aRootTag the root tag
	 * @param aTag the tag
	 * @param aBeforeTag the before tag
	 * @param aErrors the errors
	 */
	public void newSiblingNode(IXMLTag aRootTag, IXMLTag aTag,
			IXMLTag aBeforeTag, List<String[]> aErrors);

	/**
	 * Adds tag aTag as child of aParentTag using aRootTag tree.
	 * 
	 * @param aRootTag the root tag
	 * @param aTag the tag
	 * @param aParentTag the parent tag
	 * @param aErrors used to return the error list
	 */
	public void newChildNode(IXMLTag aRootTag, IXMLTag aTag,
			IXMLTag aParentTag, List<String[]> aErrors);

	/**
	 * Adds tag aTag as child of aParentTag using aRootTag tree.
	 * Simple means without checking possible tag errors.
	 * 
	 * @param aRootTag the root tag
	 * @param aTag the tag
	 * @param aParentTag the parent tag
	 */
	public void newChildNodeSimple(IXMLTag aRootTag, IXMLTag aTag,
			IXMLTag aParentTag);

	/**
	 * Replaces aTag using tag id and xml root tag tree.
	 * Simple means without checking possible tag errors.
	 * 
	 * @param aRootTag the a root tag
	 * @param aTag the a tag
	 */
	public void replaceNodeSimple(IXMLTag aRootTag, IXMLTag aTag);

	/**
	 * Replaces aTag using tag id and xml root tag tree.
	 * 
	 * @param aRootTag the root tag
	 * @param aTag the tag
	 * @param aErrors used to return the error list
	 */
	public void replaceNode(IXMLTag aRootTag, IXMLTag aTag, List<String[]> aErrors);

	/**
	 * Deletes aTag using tag id and xml root tag tree.
	 * 
	 * @param aRootTag the root tag
	 * @param aTag the tag
	 */
	public void deleteNode(IXMLTag aRootTag, IXMLTag aTag);

	/**
	 * Drops aTag in xml tag tree. aTag is copied or moved depending on aCopy.
	 * 
	 * @param aRootTag the root tag
	 * @param aDraggedTag the dragged tag
	 * @param aDroppedTag the tag on which the dragged tag is dropped
	 * @param aCopy the copy state
	 * @param aDropType drop as child or as sibling
	 * @param aCopiedTags used to return list of copied tags
	 * @param aDropTypes used to return list of drop types
	 * @param aErrors used to return the error list
	 */
	public void dropNode(IXMLTag aRootTag, IXMLTag aDraggedTag,
			IXMLTag aDroppedTag, boolean aCopy,  String aDropType,
			List<IXMLTag> aCopiedTags, List<String> aDropTypes, List<String[]> aErrors);

	/**
	 * Deletes blobs using blob references within xml data.
	 * 
	 * @param aXmlDef the xml def
	 * @param aXmlData the xml data
	 * @param aTagType the tag type
	 */
	public void deleteBlobs(String aXmlDef, String aXmlData, int aTagType);

	/**
	 * Gets the tag by id. Looks in aTag and its xml childs.
	 * 
	 * @param aTag the tag
	 * @param aId the id
	 * 
	 * @return the xml tag
	 */
	public IXMLTag getTagById(IXMLTag aTag, String aId);

	/**
	 * Gets the tag by id. Looks in list aXmlList and its xml childs.
	 * 
	 * @param aXmlList the xml list
	 * @param aId the id
	 * 
	 * @return the xml tag
	 */
	public IXMLTag getTagById(List<IXMLTag> aXmlList, String aId);

	/**
	 * Gets the tag by attribute refdataid, a reference within a status tag to a corresponding data tag.
	 * Looks in aTag and its xml childs.
	 * 
	 * @param aTag the tag
	 * @param aRefdataid the refdataid
	 * 
	 * @return the xml tag
	 */
	public IXMLTag getTagByRefdataid(IXMLTag aTag, String aRefdataid);

	/**
	 * Gets the tags by refdataid, a reference within a status tag to a corresponding data tag.
	 * For some components like e-messages there can be more status tags corresponding to one data tag.
	 * Looks in aTag and its xml childs.
	 * 
	 * @param aTag the tag
	 * @param aRefdataid the refdataid
	 * 
	 * @return the xml tags by refdataid
	 */
	public List<IXMLTag> getTagsByRefdataid(IXMLTag aTag, String aRefdataid);

	/**
	 * Gets the tag by refstatusid, a temporary reference in memory within a data tag to a corresponding status tag.
	 * Looks in aTag and its xml childs.
	 * 
	 * @param aTag the tag
	 * @param aRefstatusid the refstatusid
	 * 
	 * @return the xml tag
	 */
	public IXMLTag getTagByRefstatusid(IXMLTag aTag, String aRefstatusid);

	/**
	 * Sets the tag attribute id of tag aTag.
	 * 
	 * @param aTag the tag
	 */
	public void setTagId(IXMLTag aTag);
	
	/**
	 * Handles singleopen. Within the xml definition it is possible to define tag attribute opened as singleopen.
	 * It means within a list of sibling tags there can be only one tag which has attribute opened set to true.
	 * If aTag has attribute opened set to true, opened is set to false for its siblings if applicable.
	 * 
	 * @param aTag the tag
	 * @param aParentTag the parent tag
	 */
	public void handleSingleopen(IXMLTag aTag, IXMLTag aParentTag);

	/**
	 * Handles singleopen for dropping. Within the xml definition it is possible to define tag attribute opened as singleopen.
	 * It means within a list of sibling tags there can be only one tag which has attribute opened set to true.
	 * If aTag has attribute opened set to true, opened is set to false for its siblings if applicable.
	 * 
	 * @param aDraggedTag the tag
	 * @param aDroppedParentTag the parent tag
	 * @param aCopyOfDraggedTag, the copy of aDraggedTag in case it has to be copied, otherwise it is null
	 */
	public void handleSingleopenForDrop(IXMLTag aDraggedTag, IXMLTag aDroppedParentTag, IXMLTag aCopyOfDraggedTag);

	/**
	 * Checks child tag values of aTag for errors.
	 * aWarnings is only used if aCopy is true. It will contain alternative key values.
	 * 
	 * @param aKey the key attribute of aTag
	 * @param aTag the tag to check for errors.
	 * @param aParentTag the parent tag
	 * @param aNew if aTag is new tag
	 * @param aCopy if aTag is copied tag
	 * @param aErrors used to return the error list
	 * @param aWarnings used to return the warning list
	 * 
	 * @return true, if there are errors
	 */
	public boolean checkTagChildTagValues(String aKey, IXMLTag aTag, IXMLTag aParentTag,
			boolean aNew, boolean aCopy, List<String[]> aErrors, List<String> aWarnings);

	/**
	 * Checks aTagChildValues for errors.
	 * aWarnings is only used if aCopy is true. It will contain alternative key values.
	 * 
	 * @param aKey the key
	 * @param aTagId the tag id
	 * @param aTagName the tag name
	 * @param aChildTagValues the tag child values
	 * @param aDefTag the def tag
	 * @param aParentTag the parent tag
	 * @param aNew if aTag is new tag
	 * @param aCopy if aTag is copied tag
	 * @param aErrors used to return the error list
	 * @param aWarnings used to return the warning list
	 * 
	 * @return true, if there are errors
	 */
	public boolean checkChildTagValues(String aKey, String aTagId, String aTagName, Hashtable<String,String> aChildTagValues, IXMLTag aDefTag, IXMLTag aParentTag,
			boolean aNew, boolean aCopy, List<String[]> aErrors, List<String> aWarnings);
	
	/**
	 * Copies aTag and all its xml childs and gives the new tags unique ids.
	 * 
	 * @param aTag the tag
	 * @param aParent the parent
	 * @param aIncludingBlobs copy blobs referenced within xml tags too
	 * 
	 * @return the new xml tag
	 */
	public IXMLTag copyTagUnique(IXMLTag aTag, IXMLTag aParent, boolean aIncludingBlobs);
	
	/**
	 * Gets drop type.
	 * 
	 * @param aDraggedTag the dragged tag
	 * @param aDroppedTag the dropped tag
	 * @param aErrors used to return the error list
	 * 
	 * @return the drop type
	 */
	public String getDropType(IXMLTag aDraggedTag, IXMLTag aDroppedTag, List<String[]> aErrors);
	
}