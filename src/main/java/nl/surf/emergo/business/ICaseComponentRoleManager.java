/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business;

import java.util.List;

import nl.surf.emergo.domain.IECaseComponentRole;

/**
 * The Interface ICaseComponentRoleManager.
 * 
 * <p>For managing all case component roles.</p>
 */
public interface ICaseComponentRoleManager {

	/**
	 * Gets new case component role.
	 * 
	 * @return case component role
	 */
	public IECaseComponentRole getNewCaseComponentRole();

	/**
	 * Gets case component role.
	 * 
	 * @param ccrId the db ccr id
	 * 
	 * @return case component role
	 */
	public IECaseComponentRole getCaseComponentRole(int ccrId);

	/**
	 * Gets case component role.
	 * 
	 * @param cacId the db cac id
	 * @param carId the db car id
	 * 
	 * @return case component role
	 */
	public IECaseComponentRole getCaseComponentRole(int cacId, int carId);

	/**
	 * Saves case component role whithout checking object properties.
	 * 
	 * @param eCaseComponentRole the case component role
	 */
	public void saveCaseComponentRole(IECaseComponentRole eCaseComponentRole);

	/**
	 * Creates new case component role if object properties are ok.
	 * 
	 * @param eCaseComponentRole the case component role
	 * 
	 * @return error list
	 */
	public List<String[]> newCaseComponentRole(IECaseComponentRole eCaseComponentRole);

	/**
	 * Updates existing case component role if object properties are ok.
	 * 
	 * @param eCaseComponentRole the case component role
	 * 
	 * @return error list
	 */
	public List<String[]> updateCaseComponentRole(IECaseComponentRole eCaseComponentRole);

	/**
	 * Deletes case component role.
	 * 
	 * @param ccrId the db ccr id
	 */
	public void deleteCaseComponentRole(int ccrId);

	/**
	 * Deletes case component role.
	 * 
	 * @param eCaseComponentRole the case component role
	 */
	public void deleteCaseComponentRole(IECaseComponentRole eCaseComponentRole);

	/**
	 * Gets all case component roles.
	 * 
	 * @return case component roles
	 */
	public List<IECaseComponentRole> getAllCaseComponentRoles();

	/**
	 * Gets all case component roles by cac id.
	 * 
	 * @param cacId the db cac id
	 * 
	 * @return case component roles
	 */
	public List<IECaseComponentRole> getAllCaseComponentRolesByCacId(int cacId);

	/**
	 * Gets all case component roles by cac ids.
	 * 
	 * @param cacIds the db cac ids as string list
	 * 
	 * @return case component roles
	 */
	public List<IECaseComponentRole> getAllCaseComponentRolesByCacIds(List<Integer> cacIds);

	/**
	 * Gets all case component roles by car id.
	 * 
	 * @param carId the db car id
	 * 
	 * @return case component roles
	 */
	public List<IECaseComponentRole> getAllCaseComponentRolesByCarId(int carId);

	/**
	 * Gets all case component roles by car ids.
	 * 
	 * @param carIds the db car ids as string
	 * 
	 * @return case component roles
	 */
	public List<IECaseComponentRole> getAllCaseComponentRolesByCarIds(List<Integer> carIds);

	/**
	 * Gets all case component roles by cac id and car id.
	 * 
	 * @param cacId the db cac id
	 * @param carId the db car id
	 * 
	 * @return case component roles
	 */
	public List<IECaseComponentRole> getAllCaseComponentRolesByCacIdCarId(int cacId, int carId);

}