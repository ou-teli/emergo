/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business;

import java.util.List;
import java.util.Map;

import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.domain.IERun;

/**
 * The Interface IRunManager.
 * 
 * <p>For managing all runs.</p>
 * 
 * <p>If a run is deleted all related blobs have to be deleted too.
 * Blobs are not deleted in db cascade because they are referenced from within xml data, so should be deleted separately.</p>
*/
public interface IRunManager {

	/**
	 * Gets new run.
	 * 
	 * @return run
	 */
	public IERun getNewRun();

	/**
	 * Gets run.
	 * 
	 * @param runId the db run id
	 * 
	 * @return run
	 */
	public IERun getRun(int runId);

	/**
	 * Gets test run. If it does not exist, it is created. Is used in preview option.
	 * 
	 * @param eAccount the account
	 * @param eCase the case
	 * 
	 * @return run
	 */
	public IERun getTestRun(IEAccount eAccount, IECase eCase);

	/**
	 * Checks if test run exists.
	 * 
	 * @param eAccount the account
	 * @param eCase the case
	 * 
	 * @return true, if successful
	 */
	public boolean hasTestRun(IEAccount eAccount, IECase eCase);

	/**
	 * Saves run without checking object properties.
	 * 
	 * @param eRun the run
	 */
	public void saveRun(IERun eRun);

	/**
	 * Creates new run if object properties are ok.
	 * 
	 * @param eRun the run
	 * 
	 * @return error list
	 */
	public List<String[]> newRun(IERun eRun);

	/**
	 * Updates existing run if object properties are ok.
	 * 
	 * @param eRun the run
	 * 
	 * @return error list
	 */
	public List<String[]> updateRun(IERun eRun);

	/**
	 * Validates run, that is if object properties are ok.
	 * 
	 * @param eRun the run
	 * 
	 * @return error list
	 */
	public List<String[]> validateRun(IERun eRun);

	/**
	 * Deletes run.
	 * Also deletes related blobs.
	 * 
	 * @param runId the db run id
	 */
	public void deleteRun(int runId);

	/**
	 * Deletes run.
	 * Also deletes related blobs.
	 * 
	 * @param eRun the run
	 */
	public void deleteRun(IERun eRun);

	/**
	 * Deletes blobs related to run.
	 * Blobs are deleted in cascade because they are referenced from within xml data, so should be deleted separately.
	 * 
	 * @param eRun the run
	 */
	public void deleteBlobs(IERun eRun);

	/**
	 * Gets all runs.
	 * 
	 * @return runs
	 */
	public List<IERun> getAllRuns();

	/**
	 * Gets all runs by run ids.
	 * 
	 * @param runIds the run ids
	 * 
	 * @return runs
	 */
	public List<IERun> getAllRunsByRunIds(List<Integer> runIds);

	/**
	 * Gets filtered runs.
	 * 
	 * @param keysAndValueParts the db field ids and value parts
	 * 
	 * @return runs
	 */
	public List<IERun> getAllRunsFilter(Map<String, String> keysAndValueParts);

	/**
	 * Gets all runs by acc id.
	 * 
	 * @param accId the db acc id
	 * 
	 * @return runs
	 */
	public List<IERun> getAllRunsByAccId(int accId);

	/**
	 * Gets all runs by cas id.
	 * 
	 * @param casId the db cas id
	 * 
	 * @return runs
	 */
	public List<IERun> getAllRunsByCasId(int casId);

	/**
	 * Gets all runs by acc id and cas id.
	 * 
	 * @param accId the db acc id
	 * @param casId the db cas id
	 * 
	 * @return runs
	 */
	public List<IERun> getAllRunsByAccIdCasId(int accId, int casId);

	/**
	 * Gets all runnable runs by acc id.
	 * 
	 * @param accId the db acc id
	 * 
	 * @return runs
	 */
	public List<IERun> getAllRunnableRunsByAccId(int accId);

	/**
	 * Gets all runnable open access runs.
	 * 
	 * @return runs
	 */
	public List<IERun> getAllRunnableOpenAccessRuns();
}