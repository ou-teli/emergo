/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business;

import java.util.List;

import nl.surf.emergo.domain.IERunCaseComponent;

/**
 * The Interface IRunCaseComponentManager.
 * 
 * <p>For managing all run case components. Run case component contains xml status of run per case component.
 * That is xml status shared by all rungroups in a run.</p>
 * 
 * <p>If a run case component is deleted all related blobs have to be deleted too.
 * Blobs are not deleted in db cascade because they are referenced from within xml data, so should be deleted separately.</p>
*/
public interface IRunCaseComponentManager {

	/**
	 * Gets new run case component.
	 * 
	 * @return run case component
	 */
	public IERunCaseComponent getNewRunCaseComponent();

	/**
	 * Gets run case component.
	 * 
	 * @param rccId the db rcc id
	 * 
	 * @return run case component
	 */
	public IERunCaseComponent getRunCaseComponent(int rccId);

	/**
	 * Gets run case component.
	 * 
	 * @param runId the db run id
	 * @param cacId the db cac id
	 * 
	 * @return run case component
	 */
	public IERunCaseComponent getRunCaseComponent(int runId, int cacId);

	/**
	 * Saves run case component without checking object properties.
	 * 
	 * @param eRunCaseComponent the run case component
	 */
	public void saveRunCaseComponent(IERunCaseComponent eRunCaseComponent);

	/**
	 * Creates new run case component if object properties are ok.
	 * 
	 * @param eRunCaseComponent the run case component
	 * 
	 * @return error list
	 */
	public List<String[]> newRunCaseComponent(IERunCaseComponent eRunCaseComponent);

	/**
	 * Updates existing run case component if object properties are ok.
	 * 
	 * @param eRunCaseComponent the run case component
	 * 
	 * @return error list
	 */
	public List<String[]> updateRunCaseComponent(IERunCaseComponent eRunCaseComponent);

	/**
	 * Deletes run case component.
	 * Also deletes related blobs.
	 * 
	 * @param rccId the db rcc id
	 */
	public void deleteRunCaseComponent(int rccId);

	/**
	 * Deletes run case component.
	 * Also deletes related blobs.
	 * 
	 * @param eRunCaseComponent the run case component
	 */
	public void deleteRunCaseComponent(IERunCaseComponent eRunCaseComponent);

	/**
	 * Deletes blobs related to run case component.
	 * Blobs are deleted in cascade because they are referenced from within xml data, so should be deleted separately.
	 * 
	 * @param eRunCaseComponent the run case component
	 */
	public void deleteBlobs(IERunCaseComponent eRunCaseComponent);

	/**
	 * Gets all run case components.
	 * 
	 * @return run case components
	 */
	public List<IERunCaseComponent> getAllRunCaseComponents();

	/**
	 * Gets all run case components by run id.
	 * 
	 * @param runId the db run id
	 * 
	 * @return run case components
	 */
	public List<IERunCaseComponent> getAllRunCaseComponentsByRunId(int runId);

	/**
	 * Gets all run case components by cac id.
	 * 
	 * @param cacId the db cac id
	 * 
	 * @return run case components
	 */
	public List<IERunCaseComponent> getAllRunCaseComponentsByCacId(int cacId);

}