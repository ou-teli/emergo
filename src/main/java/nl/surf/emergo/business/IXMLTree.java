/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business;

import java.util.List;

import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

/**
 * The Interface IXMLTree.
 *
 * <p>For reading and converting xml trees.</p>
 */
public interface IXMLTree {

	/**
	 * Escape xml string.
	 *
	 * @param aStr the str
	 *
	 * @return the converted string
	 */
	public String escapeXML(String aStr);

	/**
	 * Escape xml string. Alternative solution, using apache.commons.text
	 *
	 * @param aStr the str
	 *
	 * @return the converted string
	 */
	public String escapeXMLAlt(String aStr);

	/**
	 * Unescape xml string.
	 *
	 * @param aStr the str
	 *
	 * @return the converted string
	 */
	public String unescapeXML(String aStr);

	/**
	 * Unescape xml string. Alternative solution, using apache.commons.text
	 *
	 * @param aStr the str
	 *
	 * @return the converted string
	 */
	public String unescapeXMLAlt(String aStr);

	/**
	 * Gets document builder.
	 *
	 * @return the document builder
	 */
	public DocumentBuilder getDocumentBuilder();
	
	/**
	 * Gets a list of xml tags and their childs using an input url. Definition tag of xml tags is set using aXmlDefList.
	 *
	 * @param aXmlDefList the xml definition List
	 * @param aInputSource the input source
	 * @param aRootelemkey the name of the rootelemkey
	 * @param aTagType the tag type
	 *
	 * @return the list of xml tags
	 */
	public List<IXMLTag> getXMLListIS(List<IXMLTag> aXmlDefList, InputSource aInputSource,
			String aRootelemkey, int aTagType);

	/**
	 * Gets a list of xml tags and their childs using an xml document. Definition tag of xml tags is set using aXmlDefList.
	 *
	 * @param aXmlDefList the xml definition List
	 * @param aDocument the a document
	 * @param aRootelemkey the name of the rootelemkey
	 * @param aTagType the tag type
	 *
	 * @return the list of xml tags
	 */
	public List<IXMLTag> getXMLListIS(List<IXMLTag> aXmlDefList, Document aDocument,
			String aRootelemkey, int aTagType);

	/**
	 * Gets a list of xml tags and their childs using an input string. Definition tag of xml tags are not set.
	 *
	 * @param aXml the a xml
	 * @param aRootelemkey the name of the rootelemkey
	 * @param aTagType the tag type
	 *
	 * @return the list of xml tags
	 */
	public List<IXMLTag> getXMLList(String aXml, String aRootelemkey, int aTagType);
	
	/**
	 * Gets a xml document using an input string.
	 *
	 * @param aXml the a xml
	 *
	 * @return the xml document
	 */
	public Document getDocument(String aXml);

	/**
	 * Gets a list of xml tags and their childs using an xml document. Definition tag of xml tags are not set.
	 *
	 * @param aDocument the a document
	 * @param aRootelemkey the name of the rootelemkey
	 * @param aTagType the tag type
	 *
	 * @return the list of xml tags
	 */
	public List<IXMLTag> getXMLList(Document aDocument, String aRootelemkey, int aTagType);

	/**
	 * Gets a list of xml tags and their childs using an input string. Definition tag of xml tags is set using aXmlDefList.
	 *
	 * @param aXmlDefList the xml definition List
	 * @param aXml the xml string
	 * @param aRootelemkey the name of the rootelemkey
	 * @param aTagType the tag type
	 *
	 * @return the list of xml tags
	 */
	public List<IXMLTag> getXMLListUseDef(List<IXMLTag> aXmlDefList, String aXml,
			String aRootelemkey, int aTagType);

	/**
	 * Gets a list of xml tags and their childs using an xml document. Definition tag of xml tags is set using aXmlDefList.
	 *
	 * @param aXmlDefList the xml definition List
	 * @param aDocument the a document
	 * @param aRootelemkey the name of the rootelemkey
	 * @param aTagType the tag type
	 *
	 * @return the list of xml tags
	 */
	public List<IXMLTag> getXMLListUseDef(List<IXMLTag> aXmlDefList, Document aDocument,
			String aRootelemkey, int aTagType);

	/**
	 * Converts XML Lists to xml document string.
	 *
	 * @param aXmlCompList the xml component List, all childs of component element
	 * @param aXmlContList the xml content List, all childs of content element
	 *
	 * @return the xml string
	 */
	public String XMLListsToDoc(List<IXMLTag> aXmlCompList, List<IXMLTag> aXmlContList);

	/**
	 * Converts XML Lists to xml document string.
	 *
	 * @param aXmlCompList the xml component List, all childs of component element
	 * @param aXmlContList the xml content List, all childs of content element
	 * @param aEncoding the xml encoding
	 *
	 * @return the xml string
	 */
	public String XMLListsToDoc(List<IXMLTag> aXmlCompList, List<IXMLTag> aXmlContList, String aEncoding);

	/**
	 * XML sub list to doc.
	 *
	 * @param aXmlList the a xml list
	 * @param aXmlData the a xml data
	 *
	 * @return the string builder
	 */
	public StringBuilder XMLListToDoc(List<IXMLTag> aXmlList, StringBuilder aXmlData);

	/**
	 * Checks if an xml string is valid.
	 *
	 * @param aXml the a xml string
	 *
	 * @return if valid
	 */
	public boolean isXMLValid(String aXml);

}
