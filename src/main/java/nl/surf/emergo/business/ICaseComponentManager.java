/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business;

import java.util.List;

import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Interface ICaseComponentManager.
 * 
 * <p>For managing all case components. Case component contains xml data.</p>
 * 
 * <p>If a case component is deleted all related blobs have to be deleted too.
 * Blobs are not deleted in db cascade because they are referenced from within xml data, so should be deleted separately.</p>
 * 
 * <p>Case components are kept in application memory for performance reasons, so saveCaseComponent deleteCaseComponent update
 * application memory too.</p>
*/
public interface ICaseComponentManager {

	/**
	 * Gets new case component.
	 * 
	 * @return case component
	 */
	public IECaseComponent getNewCaseComponent();

	/**
	 * Gets case component.
	 * 
	 * @param cacId the db cac id
	 * 
	 * @return case component
	 */
	public IECaseComponent getCaseComponent(int cacId);

	/**
	 * Saves case component whithout checking object properties.
	 * Also saves case component in application memory, for performance reasons.
	 * 
	 * @param eCaseComponent the e case component
	 */
	public void saveCaseComponent(IECaseComponent eCaseComponent);

	/**
	 * Creates new case component if object properties are ok.
	 * 
	 * @param eCaseComponent the case component
	 * 
	 * @return the error list
	 */
	public List<String[]> newCaseComponent(IECaseComponent eCaseComponent);

	/**
	 * Updates existing case component if object properties are ok.
	 * 
	 * @param eCaseComponent the case component
	 * 
	 * @return the error list
	 */
	public List<String[]> updateCaseComponent(IECaseComponent eCaseComponent);

	/**
	 * Validates case component, that is if object properties are ok.
	 * 
	 * @param eCaseComponent the case component
	 * 
	 * @return the error list
	 */
	public List<String[]> validateCaseComponent(IECaseComponent eCaseComponent);

	/**
	 * Deletes case component.
	 * Also deletes case component in application memory.
	 * Also deletes related blobs.
	 * 
	 * @param cacId the db cac id
	 */
	public void deleteCaseComponent(int cacId);

	/**
	 * Deletes case component.
	 * Also deletes case component in application memory.
	 * Also deletes related blobs.
	 * 
	 * @param eCaseComponent the case component
	 */
	public void deleteCaseComponent(IECaseComponent eCaseComponent);

	/**
	 * Deletes blobs related to case component.
	 * Blobs are not deleted in db cascade because they are referenced from within xml data, so should be deleted separately.
	 * 
	 * @param eCaseComponent the case component
	 */
	public void deleteBlobs(IECaseComponent eCaseComponent);

	/**
	 * Gets all case components.
	 * 
	 * @return case components
	 */
	public List<IECaseComponent> getAllCaseComponents();

	/**
	 * Gets all case components by cas id.
	 * 
	 * @param casId the db cas id
	 * 
	 * @return case components
	 */
	public List<IECaseComponent> getAllCaseComponentsByCasId(int casId);

	/**
	 * Gets all case components by cas ids.
	 * 
	 * @param casIds the db cas ids as strings
	 * 
	 * @return case components
	 */
	public List<IECaseComponent> getAllCaseComponentsByCasIds(List<Integer> casIds);

	/**
	 * Gets all case components by com id.
	 * 
	 * @param comId the db com id
	 * 
	 * @return case components
	 */
	public List<IECaseComponent> getAllCaseComponentsByComId(int comId);

	/**
	 * Gets all case components by cas id and com id.
	 * 
	 * @param casId the db cas id
	 * @param comId the db com id
	 * 
	 * @return case components
	 */
	public List<IECaseComponent> getAllCaseComponentsByCasIdComId(int casId, int comId);

	/**
	 * Gets all case components by acc id.
	 * 
	 * @param accId the db acc id
	 * 
	 * @return case components
	 */
	public List<IECaseComponent> getAllCaseComponentsByAccId(int accId);

}