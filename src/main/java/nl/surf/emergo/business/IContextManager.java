/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business;

import java.util.List;

import nl.surf.emergo.domain.IEContext;

/**
 * The Interface IContextManager.
 * 
 * <p>For managing all contexts.</p>
 * 
*/
public interface IContextManager {

	/**
	 * Gets new context.
	 * 
	 * @return context
	 */
	public IEContext getNewContext();

	/**
	 * Gets context.
	 * 
	 * @param conId the db context id
	 * 
	 * @return context
	 */
	public IEContext getContext(int conId);

	/**
	 * Saves context without checking object properties.
	 * 
	 * @param eContext the context
	 */
	public void saveContext(IEContext eContext);

	/**
	 * Creates new context if object properties are ok.
	 * 
	 * @param eContext the context
	 * 
	 * @return error list
	 */
	public List<String[]> newContext(IEContext eContext);

	/**
	 * Updates existing context if object properties are ok.
	 * 
	 * @param eContext the context
	 * 
	 * @return error list
	 */
	public List<String[]> updateContext(IEContext eContext);

	/**
	 * Validates context, that is if object properties are ok.
	 * 
	 * @param eContext the context
	 * 
	 * @return error list
	 */
	public List<String[]> validateContext(IEContext eContext);

	/**
	 * Deletes context.
	 * 
	 * @param conId the db context id
	 */
	public void deleteContext(int conId);

	/**
	 * Deletes context.
	 * 
	 * @param eContext the context
	 */
	public void deleteContext(IEContext eContext);

	/**
	 * Flushes context to database.
	 * 
	 */
	public void flushContext();

	/**
	 * Clears context.
	 * 
	 */
	public void clearContext();

	/**
	 * Gets all contexts.
	 * 
	 * @return contexts
	 */
	public List<IEContext> getAllContexts();

	/**
	 * Gets all active or non active contexts.
	 * 
	 * @param active the active state
	 * 
	 * @return contexts
	 */
	public List<IEContext> getAllContexts(boolean active);

}