/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business;

import java.util.List;

import nl.surf.emergo.domain.IEComponent;

/**
 * The Interface IComponentManager.
 * 
 * <p>For managing all components. Component contains xml definition.</p>
 * 
 * <p>Case components are kept in application memory for performance reasons and have a reference to a component,
 * so saveComponent update this reference too.</p>
 * 
 * <p>A component can be a parent of another component. It is not really a parent. Parent is related to the UI of
 * the player. If parent is deleted, comcomid of children is set to 0.</p>
*/
public interface IComponentManager {

	/**
	 * Gets new component.
	 * 
	 * @return component
	 */
	public IEComponent getNewComponent();

	/**
	 * Gets component.
	 * 
	 * @param comId the db com id
	 * 
	 * @return component
	 */
	public IEComponent getComponent(int comId);

	/**
	 * Gets component.
	 * 
	 * @param uuid the uuid
	 * 
	 * @return component
	 */
	public IEComponent getComponent(String uuid);

	/**
	 * Saves component without checking object properties.
	 * Also saves reference to component within case components in application memory, for performance reasons.
	 * 
	 * @param eComponent the component
	 */
	public void saveComponent(IEComponent eComponent);

	/**
	 * Creates new component if object properties are ok.
	 * 
	 * @param eComponent the component
	 * 
	 * @return error list
	 */
	public List<String[]> newComponent(IEComponent eComponent);

	/**
	 * Updates existing component if object properties are ok.
	 * 
	 * @param eComponent the component
	 * 
	 * @return error list
	 */
	public List<String[]> updateComponent(IEComponent eComponent);

	/**
	 * Validates component, that is if object properties are ok.
	 * 
	 * @param eComponent the component
	 * 
	 * @return error list
	 */
	public List<String[]> validateComponent(IEComponent eComponent);

	/**
	 * Deletes component.
	 * Also deletes reference to component within case components in application memory.
	 * Should also delete related blobs, but does not yet.
	 * 
	 * @param comId the db com id
	 */
	public void deleteComponent(int comId);

	/**
	 * Deletes component.
	 * Also deletes reference to component within case components in application memory.
	 * Should also delete related blobs, but does not yet.
	 * 
	 * @param eComponent the component
	 */
	public void deleteComponent(IEComponent eComponent);

	/**
	 * Gets all components.
	 * 
	 * @return components
	 */
	public List<IEComponent> getAllComponents();

	/**
	 * Gets all components by acc id.
	 * 
	 * @param accId the db acc id
	 * 
	 * @return components
	 */
	public List<IEComponent> getAllComponentsByAccId(int accId);

	/**
	 * Gets all components.
	 * 
	 * @param active the active state
	 * 
	 * @return components
	 */
	public List<IEComponent> getAllComponents(boolean active);

}