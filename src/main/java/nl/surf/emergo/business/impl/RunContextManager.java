/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;

import nl.surf.emergo.business.abstr.AbstractDaoBasedRunContextManager;
import nl.surf.emergo.domain.ERunContext;
import nl.surf.emergo.domain.IERunContext;

/**
 * The Class RunContextManager.
 */
public class RunContextManager extends AbstractDaoBasedRunContextManager implements InitializingBean {

	@Override
	public void afterPropertiesSet() throws Exception {
		// NOOP
	}

	@Override
	public IERunContext getNewRunContext() {
		IERunContext eRunContext = new ERunContext();
		return eRunContext;
	}

	@Override
	public IERunContext getRunContext(int rucId) {
		return runContextDao.get(rucId);
	}

	@Override
	public void saveRunContext(IERunContext eRunContext) {
		runContextDao.save(eRunContext);
	}

	@Override
	public List<String[]> newRunContext(IERunContext eRunContext) {
		// Validate item and if ok, save it after setting creationdate and
		// lastupdatedate.
		List<String[]> lErrors = validateRunContext(eRunContext);
		if (lErrors == null) {
			eRunContext.setCreationdate(new Date());
			eRunContext.setLastupdatedate(new Date());
			saveRunContext(eRunContext);
		}
		return lErrors;
	}

	@Override
	public List<String[]> updateRunContext(IERunContext eRunContext) {
		// Validate item and if ok, save it after setting lastupdatedate.
		List<String[]> lErrors = validateRunContext(eRunContext);
		if (lErrors == null) {
			eRunContext.setLastupdatedate(new Date());
			saveRunContext(eRunContext);
		}
		return lErrors;
	}

	@Override
	public List<String[]> validateRunContext(IERunContext eRunContext) {
		/*
		 * If validation error return it as element in String[] list.
		 */
		List<String[]> lErrors = new ArrayList<>(0);
		return lErrors.isEmpty() ? null : lErrors;
	}

	@Override
	public void deleteRunContext(int rucId) {
		deleteRunContext(getRunContext(rucId));
	}

	@Override
	public void deleteRunContext(IERunContext eRunContext) {
		runContextDao.delete(eRunContext);
	}

	@Override
	public List<IERunContext> getAllRunContexts() {
		return runContextDao.getAll();
	}

	@Override
	public List<IERunContext> getAllRunContextsByRunId(int runId) {
		return runContextDao.getAllByRunId(runId);
	}

	@Override
	public List<IERunContext> getAllRunContextsByContextId(int conId) {
		return runContextDao.getAllByConId(conId);
	}

}