/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business.impl;

import static nl.surf.emergo.business.impl.AppConstants.*;

import java.net.UnknownHostException;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.PathResource;
import org.springframework.mail.javamail.ConfigurableMimeFileTypeMap;
import org.springframework.web.context.support.XmlWebApplicationContext;
import org.zkoss.json.parser.JSONParser;
import org.zkoss.json.parser.ParseException;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;

import com.sun.mail.smtp.SMTPAddressFailedException;

import nl.surf.emergo.business.abstr.AbstractDaoBasedAppManager;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.ESys;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IEComponent;
import nl.surf.emergo.domain.IEMail;
import nl.surf.emergo.domain.IESys;
import nl.surf.emergo.utilities.AppHelper;
import nl.surf.emergo.utilities.PropsValues;

/**
 * The Class AppManager.
 */
public class AppManager extends AbstractDaoBasedAppManager implements InitializingBean {
	private static final Logger _log = LogManager.getLogger(AppManager.class);

	private AppHelper appHelper = new AppHelper();

	public static final String ERROR_EMPTY = "empty";
	public static final String ERROR_NOT_UNIQUE = "not_unique";
	public static final String ERROR_NOT_EQUAL = "not_equal";
	public static final String ERROR_NOT_VALID = "not_valid";
	public static final String ERROR_NOT_SENT = "not_sent";
	public static final String ERROR_NO_CHOICE = "no_choice";
	public static final String ERROR_DROP_PARENT_AS_CHILD_OF_PARENT = "drop_parent_as_child_of_parent";
	public static final String ERROR_DROP_NO_CHILD_OR_SIBLING_POSSIBLE = "drop_no_child_or_sibling_possible";
	public static final String ERROR_NO_VALID_SCRIPT = "no_valid_script";
	public static final String ERROR_NO_JSON = "no_json";

	/** The app container. */
	protected Hashtable<String, Object> appContainer = new Hashtable<>(0);

	/** The def tree references to remove from app memory. */
	protected List<String> defTreeReferencesToRemoveFromAppMemory = new ArrayList<>(0);

	/** The data tree references to remove from app memory. */
	protected List<String> dataTreeReferencesToRemoveFromAppMemory = new ArrayList<>(0);

	public static final DelegatingVariableResolver res = new DelegatingVariableResolver();

	/** The application context. */
	protected static ApplicationContext appContext = null;

	protected JSONParser jsonParser = new JSONParser();

	public AppManager() {
		super();
		addDefTreeReferenceToRemoveFromAppMemory(AppConstants.xmlDefTreeByComId);
		addDataTreeReferenceToRemoveFromAppMemory(AppConstants.xmlDataTreeByCacId);
	}

	/**
	 * Gets the bean, given by aBeanId. A bean is an object defined within the
	 * Spring configuration file, that is injected by Spring at runtime.
	 *
	 * @param aBeanId the a bean id
	 *
	 * @return the bean
	 */
	public static final Object getBean(String aBeanId) {
		return res.resolveVariable(aBeanId);
	}

	/**
	 * Sets the pointer to the application context. Needed to determine application
	 * path, when ZK environment is not started.
	 *
	 * @param aAppContext the application context
	 *
	 */
	public static final void setAppContext(ApplicationContext aAppContext) {
		appContext = aAppContext;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		// NOOP
	}

	@Override
	public String getSysvalue(String aSyskey) {
		String lSysvalue = "";
		IESys lSys = sysDao.get(aSyskey);
		if (lSys != null)
			lSysvalue = lSys.getSysvalue();
		if (lSysvalue != null)
			return lSysvalue;
		return "";
	}

	@Override
	public void setSysvalue(String aSyskey, String aSysvalue) {
		Date lDate = new Date();
		IESys lSys = sysDao.get(aSyskey);
		if (lSys == null) {
			lSys = new ESys();
			lSys.setSyskey(aSyskey);
			lSys.setCreationdate(lDate);
		}
		lSys.setSysvalue(aSysvalue);
		lSys.setLastupdatedate(lDate);
		sysDao.save(lSys);
	}

	@Override
	public void executeSql(String aSql, List<String> aErrors) {
		sysDao.executeSql(aSql, aErrors);
	}

	@Override
	public List<Object> getSqlResult(String aSql, Class aClass, List<String> aErrors) {
		return sysDao.getSqlResult(aSql, aClass, aErrors);
	}

	@Override
	public List<Object> getSqlResult(String aSql, List<String> aErrors) {
		return sysDao.getSqlResult(aSql, aErrors);
	}

	@Override
	public boolean sqlTableExists(String aTableName) {
		return sysDao.sqlTableExists(aTableName);
	}

	@Override
	public boolean sqlTableColumnExists(String aTableName, String aColumnName) {
		return sysDao.sqlTableColumnExists(aTableName, aColumnName);
	}

	@Override
	public String getErrorString(String aErrorString) {
		if (aErrorString.equals("error_empty"))
			return ERROR_EMPTY;
		if (aErrorString.equals("error_not_unique"))
			return ERROR_NOT_UNIQUE;
		if (aErrorString.equals("error_no_choice"))
			return ERROR_NO_CHOICE;
		if (aErrorString.equals("error_drop_parent_as_child_of_parent"))
			return ERROR_DROP_PARENT_AS_CHILD_OF_PARENT;
		if (aErrorString.equals("error_drop_no_child_or_sibling_possible"))
			return ERROR_DROP_NO_CHILD_OR_SIBLING_POSSIBLE;
		if (aErrorString.equals("error_no_valid_script"))
			return ERROR_NO_VALID_SCRIPT;
		if (aErrorString.equals("error_not_equal"))
			return ERROR_NOT_EQUAL;
		if (aErrorString.equals("error_not_valid"))
			return ERROR_NOT_VALID;
		if (aErrorString.equals("error_not_sent"))
			return ERROR_NOT_SENT;
		if (aErrorString.equals("error_no_json"))
			return ERROR_NO_JSON;
		return aErrorString;
	}

	@Override
	public boolean isEmpty(String aValue) {
		return (aValue == null || aValue.trim().equals(""));
	}

	@Override
	public boolean isJson(String aValue) {
		if (isEmpty(aValue)) {
			return true;
		}
		try {
			jsonParser.parse(aValue);
		} catch (ParseException e) {
			return false;
		}
		return true;
	}

	@Override
	public void addError(List<String[]> aErrors, String aField, String aError) {
		if (aErrors == null)
			aErrors = new ArrayList<>(0);
		aErrors.add(new String[] { aField, getErrorString(aError) });
	}

	@Override
	public void addWarning(List<String[]> aWarnings, String aField, String aWarning) {
		if (aWarnings == null)
			aWarnings = new ArrayList<>(0);
		aWarnings.add(new String[] { aField, aWarning });
	}

	@Override
	public String getStatusKey(int aIndex) {
		if (aIndex == statusKeyStartTimeIndex)
			return statusKeyStartTime;
		else if (aIndex == statusKeyCurrentTimeIndex)
			return statusKeyCurrentTime;
		else if (aIndex == statusKeyPresentIndex)
			return statusKeyPresent;
		else if (aIndex == statusKeyAccessibleIndex)
			return statusKeyAccessible;
		else if (aIndex == statusKeyOpenedIndex)
			return statusKeyOpened;
		else if (aIndex == statusKeySelectedIndex)
			return statusKeySelected;
		else if (aIndex == statusKeyOutfoldedIndex)
			return statusKeyOutfolded;
		else if (aIndex == statusKeyOutfoldableIndex)
			return statusKeyOutfoldable;
		else if (aIndex == statusKeyRequiredIndex)
			return statusKeyRequired;
		else if (aIndex == statusKeyFinishedIndex)
			return statusKeyFinished;
		else if (aIndex == statusKeyCanceledIndex)
			return statusKeyCanceled;
		else if (aIndex == statusKeyScoreIndex)
			return statusKeyScore;
		else if (aIndex == statusKeySentIndex)
			return statusKeySent;
		else if (aIndex == statusKeyVersionIndex)
			return statusKeyVersion;
		else if (aIndex == statusKeySelectedTagIdIndex)
			return statusKeySelectedTagId;
		else if (aIndex == statusKeyStartedIndex)
			return statusKeyStarted;
		else if (aIndex == statusKeyAnswerIndex)
			return statusKeyAnswer;
		else if (aIndex == statusKeyFeedbackConditionIdIndex)
			return statusKeyFeedbackConditionId;
		else if (aIndex == statusKeyCorrectIndex)
			return statusKeyCorrect;
		else if (aIndex == statusKeyDeletedIndex)
			return statusKeyDeleted;
		else if (aIndex == statusKeyExtendableIndex)
			return statusKeyExtendable;
		else if (aIndex == statusKeySharedIndex)
			return statusKeyShared;
		else if (aIndex == statusKeyOwnershipIndex)
			return statusKeyOwnership;
		else if (aIndex == statusKeyRatingIndex)
			return statusKeyRating;
		else if (aIndex == statusKeySharedbyteamIndex)
			return statusKeySharedbyteam;
		else if (aIndex == statusKeyValueIndex)
			return statusKeyValue;
		else if (aIndex == statusKeySelectedTagDataIndex)
			return statusKeySelectedTagData;
		else if (aIndex == statusKeyDatetimeIndex)
			return statusKeyDatetime;
		else if (aIndex == statusKeyAlwaysrecordIndex)
			return statusKeyAlwaysrecord;
		else if (aIndex == statusKeyActorIndex)
			return statusKeyActor;
		else if (aIndex == statusKeyPreviousitemsaccesibleIndex)
			return statusKeyPreviousitemsaccessible;
		else if (aIndex == statusKeyNextitemsaccesibleIndex)
			return statusKeyNextitemsaccessible;
		else if (aIndex == statusKeyLayoutnumberIndex)
			return statusKeyLayoutnumber;
		else if (aIndex == statusKeyPreviewedIndex)
			return statusKeyPreviewed;
		else if (aIndex == statusKeyHighlightIndex)
			return statusKeyHighlight;
		else if (aIndex == statusKeySavedIndex)
			return statusKeySaved;
		else if (aIndex == statusKeyUpdateIndex)
			return statusKeyUpdate;
		else if (aIndex == statusKeyStringIndex)
			return statusKeyString;
		else if (aIndex == statusKeyContainerokIndex)
			return statusKeyContainerok;
		else if (aIndex == statusKeyContainerposokIndex)
			return statusKeyContainerposok;
		else if (aIndex == statusKeyContainerposIndex)
			return statusKeyContainerpos;
		else if (aIndex == statusKeyIndexIndex)
			return statusKeyIndex;
		else if (aIndex == statusKeyNobreadcrumbIndex)
			return statusKeyNobreadcrumb;
		else if (aIndex == statusKeyDraggableIndex)
			return statusKeyDraggable;
		else if (aIndex == statusKeyDroppableIndex)
			return statusKeyDroppable;
		else if (aIndex == statusKeyCopyIndex)
			return statusKeyCopy;
		else if (aIndex == statusKeyDataparentidIndex)
			return statusKeyDataparentid;
		else if (aIndex == statusKeyDatabeforeidIndex)
			return statusKeyDatabeforeid;
		else if (aIndex == statusKeyRandomizeitemsIndex)
			return statusKeyRandomizeitems;
		else if (aIndex == statusKeyRandomizedIndex)
			return statusKeyRandomized;
		else if (aIndex == statusKeyInitializedIndex)
			return statusKeyInitialized;
		else if (aIndex == statusKeyNumberofattemptsIndex)
			return statusKeyNumberofattempts;
		else if (aIndex == statusKeyShowcontrolsIndex)
			return statusKeyShowcontrols;
		else if (aIndex == statusKeyNumberofcorrectitemsIndex)
			return statusKeyNumberofcorrectitems;
		else if (aIndex == statusKeyShowinteractionIndex)
			return statusKeyShowinteraction;
		else if (aIndex == statusKeyUrlIndex)
			return statusKeyUrl;
		else if (aIndex == statusKeyIdIndex)
			return statusKeyId;
		else if (aIndex == statusKeyXpositionIndex)
			return statusKeyXposition;
		else if (aIndex == statusKeyYpositionIndex)
			return statusKeyYposition;
		else if (aIndex == statusKeyInterruptableIndex)
			return statusKeyInterruptable;
		else if (aIndex == statusKeyPublishableIndex)
			return statusKeyPublishable;
		else if (aIndex == statusKeyReviewableIndex)
			return statusKeyReviewable;
		else if (aIndex == statusKeyNextifpublishedIndex)
			return statusKeyNextifpublished;
		else if (aIndex == statusKeyAllowpreviousIndex)
			return statusKeyAllowprevious;
		else if (aIndex == statusKeyNumberofresitsIndex)
			return statusKeyNumberofresits;
		else if (aIndex == statusKeyNumberofreactionsIndex)
			return statusKeyNumberofreactions;
		else if (aIndex == statusKeyResetIndex)
			return statusKeyReset;
		else if (aIndex == statusKeyRankingIndex)
			return statusKeyRanking;
		else if (aIndex == statusKeySRatingFixedIndex)
			return statusKeySRatingFixed;
		else if (aIndex == statusKeyGameLoggingIndex)
			return statusKeyGameLogging;
		else if (aIndex == statusKeyGameSettingsIndex)
			return statusKeyGameSettings;
		else if (aIndex == statusKeySRatingIndex)
			return statusKeySRating;
		else if (aIndex == statusKeySPlayCountIndex)
			return statusKeySPlayCount;
		else if (aIndex == statusKeySKFactorIndex)
			return statusKeySKFactor;
		else if (aIndex == statusKeySUncertaintyIndex)
			return statusKeySUncertainty;
		else if (aIndex == statusKeySLastPlayedIndex)
			return statusKeySLastPlayed;
		else if (aIndex == statusKeyTimeLimitIndex)
			return statusKeyTimeLimit;
		else if (aIndex == statusKeyPRatingIndex)
			return statusKeyPRating;
		else if (aIndex == statusKeyPPlayCountIndex)
			return statusKeyPPlayCount;
		else if (aIndex == statusKeyPKFactorIndex)
			return statusKeyPKFactor;
		else if (aIndex == statusKeyPUncertaintyIndex)
			return statusKeyPUncertainty;
		else if (aIndex == statusKeyPLastPlayedIndex)
			return statusKeyPLastPlayed;
		else if (aIndex == statusKeyTimeIndex)
			return statusKeyTime;
		else if (aIndex == statusKeyRealtimeIndex)
			return statusKeyRealtime;
		else if (aIndex == statusKeyMultipleIndex)
			return statusKeyMultiple;
		else if (aIndex == statusKeyAccumulateIndex)
			return statusKeyAccumulate;
		else if (aIndex == statusKeyAutostartIndex)
			return statusKeyAutostart;
		else if (aIndex == statusKeyClosableIndex)
			return statusKeyClosable;
		else if (aIndex == statusKeyDirectfeedbackIndex)
			return statusKeyDirectfeedback;
		else if (aIndex == statusKeyLabelforoverviewbuttonIndex)
			return statusKeyLabelforoverviewbutton;
		else if (aIndex == statusKeyOverwriteIndex)
			return statusKeyOverwrite;
		else if (aIndex == statusKeyPracticeoverviewIndex)
			return statusKeyPracticeoverview;
		else if (aIndex == statusKeyShowfeedbackIndex)
			return statusKeyShowfeedback;
		else if (aIndex == statusKeyShowscoreIndex)
			return statusKeyShowscore;
		else if (aIndex == statusKeyTutorassespracticeIndex)
			return statusKeyTutorassespractice;
		else if (aIndex == statusKeyTutorassestestIndex)
			return statusKeyTutorassestest;
		else if (aIndex == statusKeyStudentassespracticeIndex)
			return statusKeyStudentassespractice;
		else if (aIndex == statusKeyStudentassestestIndex)
			return statusKeyStudentassestest;
		else if (aIndex == statusKeyStudentseefeedbackonothersIndex)
			return statusKeyStudentseefeedbackonothers;
		else if (aIndex == statusKeyStartoverviewIndex)
			return statusKeyStartoverview;
		else if (aIndex == statusKeyTestoverviewIndex)
			return statusKeyTestoverview;
		else if (aIndex == statusKeyTaskscheckableIndex)
			return statusKeyTaskscheckable;
		else if (aIndex == statusKeyZulfileIndex)
			return statusKeyZulfile;
		else if (aIndex == statusKeyRealstarttimeIndex)
			return statusKeyRealstarttime;
		else if (aIndex == statusKeyPositionIndex)
			return statusKeyPosition;
		else if (aIndex == statusKeySizeIndex)
			return statusKeySize;
		else if (aIndex == statusKeyPopupIndex)
			return statusKeyPopup;
		else if (aIndex == statusKeyTransitioneffectIndex)
			return statusKeyTransitioneffect;
		else if (aIndex == statusKeyAlwaysinspectfeedbackIndex)
			return statusKeyAlwaysinspectfeedback;
		else if (aIndex == statusKeyShowassfeedbackIndex)
			return statusKeyShowassfeedback;
		else if (aIndex == statusKeyHideinspectionIndex)
			return statusKeyHideinspection;
		else if (aIndex == statusKeyRepeatIndex)
			return statusKeyRepeat;
		else if (aIndex == statusKeyActiveIndex)
			return statusKeyActive;
		else if (aIndex == statusKeyMaxnumberofitemsIndex)
			return statusKeyMaxnumberofitems;
		else if (aIndex == statusKeySequencenumberIndex)
			return statusKeySequencenumber;
		else if (aIndex == statusKeyChosennumberIndex)
			return statusKeyChosennumber;
		else if (aIndex == statusKeyUsernotabletofinishIndex)
			return statusKeyUsernotabletofinish;
		else if (aIndex == statusKeyTeachermayinitiatestepIndex)
			return statusKeyTeachermayinitiatestep;
		else if (aIndex == statusKeyTeachermaycompletestepIndex)
			return statusKeyTeachermaycompletestep;
		else if (aIndex == statusKeyStudentmaycompletestepIndex)
			return statusKeyStudentmaycompletestep;
		else if (aIndex == statusKeyWeightingteacherstudentIndex)
			return statusKeyWeightingteacherstudent;
		else if (aIndex == statusKeyDeadlineDateIndex)
			return statusKeyDeadlineDate;
		else if (aIndex == statusKeyDeadlineTimeIndex)
			return statusKeyDeadlineTime;
		else if (aIndex == statusKeyReminderDateIndex)
			return statusKeyReminderDate;
		else if (aIndex == statusKeyReminderTimeIndex)
			return statusKeyReminderTime;
		else if (aIndex == statusKeyAllusersmustcompleteallsubstepsIndex)
			return statusKeyAllusersmustcompleteallsubsteps;
		else if (aIndex == statusKeySendDateIndex)
			return statusKeySendDate;
		else if (aIndex == statusKeySendTimeIndex)
			return statusKeySendTime;
		else if (aIndex == statusKeyDrawAttentionIndex)
			return statusKeyDrawAttention;
		else if (aIndex == statusKeyStartOfFadeOutIndex)
			return statusKeyStartOfFadeOut;
		else if (aIndex == statusKeyStartOfFadeInIndex)
			return statusKeyStartOfFadeIn;
		else if (aIndex == statusKeyMayaskteacherfeedbackcountIndex)
			return statusKeyMayaskteacherfeedbackcount;
		else if (aIndex == statusKeyMayscoreskillIndex)
			return statusKeyMayscoreskill;
		else if (aIndex == statusKeyMayscoreskillforteacherIndex)
			return statusKeyMayscoreskillforteacher;
		else if (aIndex == statusKeyMayscoreskillforpeerstudentIndex)
			return statusKeyMayscoreskillforpeerstudent;
		else if (aIndex == statusKeyMaytipsandtopsskillIndex)
			return statusKeyMaytipsandtopsskill;
		else if (aIndex == statusKeyMaytipsandtopsskillforteacherIndex)
			return statusKeyMaytipsandtopsskillforteacher;
		else if (aIndex == statusKeyMaytipsandtopsskillforpeerstudentIndex)
			return statusKeyMaytipsandtopsskillforpeerstudent;
		else if (aIndex == statusKeyTeachermayusescoresofpreviouscycleIndex)
			return statusKeyTeachermayusescoresofpreviouscycle;
		else if (aIndex == statusKeySeeselfIndex)
			return statusKeySeeself;
		else if (aIndex == statusKeyMaychangeseeselfIndex)
			return statusKeyMaychangeseeself;
		else if (aIndex == statusKeyAskteacherfeedbackIndex)
			return statusKeyAskteacherfeedback;
		else if (aIndex == statusKeyTeacherfeedbackaskedcountIndex)
			return statusKeyTeacherfeedbackaskedcount;
		else if (aIndex == statusKeyMayrategivenfeedbackIndex)
			return statusKeyMayrategivenfeedback;
		else if (aIndex == statusKeyMustrategivenfeedbackIndex)
			return statusKeyMustrategivenfeedback;
		else if (aIndex == statusKeyFeedbackratingIndex)
			return statusKeyFeedbackrating;
		else
			return getStatusKeyEx(aIndex);
	}

	@Override
	public int getStatusKeyIndex(String aKey) {
		if (aKey.equals(statusKeyStartTime))
			return statusKeyStartTimeIndex;
		else if (aKey.equals(statusKeyCurrentTime))
			return statusKeyCurrentTimeIndex;
		else if (aKey.equals(statusKeyPresent))
			return statusKeyPresentIndex;
		else if (aKey.equals(statusKeyAccessible))
			return statusKeyAccessibleIndex;
		else if (aKey.equals(statusKeyOpened))
			return statusKeyOpenedIndex;
		else if (aKey.equals(statusKeySelected))
			return statusKeySelectedIndex;
		else if (aKey.equals(statusKeyOutfolded))
			return statusKeyOutfoldedIndex;
		else if (aKey.equals(statusKeyOutfoldable))
			return statusKeyOutfoldableIndex;
		else if (aKey.equals(statusKeyRequired))
			return statusKeyRequiredIndex;
		else if (aKey.equals(statusKeyFinished))
			return statusKeyFinishedIndex;
		else if (aKey.equals(statusKeyCanceled))
			return statusKeyCanceledIndex;
		else if (aKey.equals(statusKeyScore))
			return statusKeyScoreIndex;
		else if (aKey.equals(statusKeySent))
			return statusKeySentIndex;
		else if (aKey.equals(statusKeyVersion))
			return statusKeyVersionIndex;
		else if (aKey.equals(statusKeySelectedTagId))
			return statusKeySelectedTagIdIndex;
		else if (aKey.equals(statusKeyStarted))
			return statusKeyStartedIndex;
		else if (aKey.equals(statusKeyAnswer))
			return statusKeyAnswerIndex;
		else if (aKey.equals(statusKeyFeedbackConditionId))
			return statusKeyFeedbackConditionIdIndex;
		else if (aKey.equals(statusKeyCorrect))
			return statusKeyCorrectIndex;
		else if (aKey.equals(statusKeyDeleted))
			return statusKeyDeletedIndex;
		else if (aKey.equals(statusKeyExtendable))
			return statusKeyExtendableIndex;
		else if (aKey.equals(statusKeyShared))
			return statusKeySharedIndex;
		else if (aKey.equals(statusKeyOwnership))
			return statusKeyOwnershipIndex;
		else if (aKey.equals(statusKeyRating))
			return statusKeyRatingIndex;
		else if (aKey.equals(statusKeySharedbyteam))
			return statusKeySharedbyteamIndex;
		else if (aKey.equals(statusKeyValue))
			return statusKeyValueIndex;
		else if (aKey.equals(statusKeySelectedTagData))
			return statusKeySelectedTagDataIndex;
		else if (aKey.equals(statusKeyDatetime))
			return statusKeyDatetimeIndex;
		else if (aKey.equals(statusKeyAlwaysrecord))
			return statusKeyAlwaysrecordIndex;
		else if (aKey.equals(statusKeyActor))
			return statusKeyActorIndex;
		else if (aKey.equals(statusKeyPreviousitemsaccessible))
			return statusKeyPreviousitemsaccesibleIndex;
		else if (aKey.equals(statusKeyNextitemsaccessible))
			return statusKeyNextitemsaccesibleIndex;
		else if (aKey.equals(statusKeyLayoutnumber))
			return statusKeyLayoutnumberIndex;
		else if (aKey.equals(statusKeyPreviewed))
			return statusKeyPreviewedIndex;
		else if (aKey.equals(statusKeyHighlight))
			return statusKeyHighlightIndex;
		else if (aKey.equals(statusKeySaved))
			return statusKeySavedIndex;
		else if (aKey.equals(statusKeyUpdate))
			return statusKeyUpdateIndex;
		else if (aKey.equals(statusKeyString))
			return statusKeyStringIndex;
		else if (aKey.equals(statusKeyContainerok))
			return statusKeyContainerokIndex;
		else if (aKey.equals(statusKeyContainerposok))
			return statusKeyContainerposokIndex;
		else if (aKey.equals(statusKeyContainerpos))
			return statusKeyContainerposIndex;
		else if (aKey.equals(statusKeyIndex))
			return statusKeyIndexIndex;
		else if (aKey.equals(statusKeyNobreadcrumb))
			return statusKeyNobreadcrumbIndex;
		else if (aKey.equals(statusKeyDraggable))
			return statusKeyDraggableIndex;
		else if (aKey.equals(statusKeyDroppable))
			return statusKeyDroppableIndex;
		else if (aKey.equals(statusKeyCopy))
			return statusKeyCopyIndex;
		else if (aKey.equals(statusKeyDataparentid))
			return statusKeyDataparentidIndex;
		else if (aKey.equals(statusKeyDatabeforeid))
			return statusKeyDatabeforeidIndex;
		else if (aKey.equals(statusKeyRandomizeitems))
			return statusKeyRandomizeitemsIndex;
		else if (aKey.equals(statusKeyRandomized))
			return statusKeyRandomizedIndex;
		else if (aKey.equals(statusKeyInitialized))
			return statusKeyInitializedIndex;
		else if (aKey.equals(statusKeyNumberofattempts))
			return statusKeyNumberofattemptsIndex;
		else if (aKey.equals(statusKeyShowcontrols))
			return statusKeyShowcontrolsIndex;
		else if (aKey.equals(statusKeyNumberofcorrectitems))
			return statusKeyNumberofcorrectitemsIndex;
		else if (aKey.equals(statusKeyShowinteraction))
			return statusKeyShowinteractionIndex;
		else if (aKey.equals(statusKeyUrl))
			return statusKeyUrlIndex;
		else if (aKey.equals(statusKeyId))
			return statusKeyIdIndex;
		else if (aKey.equals(statusKeyXposition))
			return statusKeyXpositionIndex;
		else if (aKey.equals(statusKeyYposition))
			return statusKeyYpositionIndex;
		else if (aKey.equals(statusKeyInterruptable))
			return statusKeyInterruptableIndex;
		else if (aKey.equals(statusKeyPublishable))
			return statusKeyPublishableIndex;
		else if (aKey.equals(statusKeyReviewable))
			return statusKeyReviewableIndex;
		else if (aKey.equals(statusKeyNextifpublished))
			return statusKeyNextifpublishedIndex;
		else if (aKey.equals(statusKeyAllowprevious))
			return statusKeyAllowpreviousIndex;
		else if (aKey.equals(statusKeyNumberofresits))
			return statusKeyNumberofresitsIndex;
		else if (aKey.equals(statusKeyNumberofreactions))
			return statusKeyNumberofreactionsIndex;
		else if (aKey.equals(statusKeyReset))
			return statusKeyResetIndex;
		else if (aKey.equals(statusKeyRanking))
			return statusKeyRankingIndex;
		else if (aKey.equals(statusKeySRatingFixed))
			return statusKeySRatingFixedIndex;
		else if (aKey.equals(statusKeyGameLogging))
			return statusKeyGameLoggingIndex;
		else if (aKey.equals(statusKeyGameSettings))
			return statusKeyGameSettingsIndex;
		else if (aKey.equals(statusKeySRating))
			return statusKeySRatingIndex;
		else if (aKey.equals(statusKeySPlayCount))
			return statusKeySPlayCountIndex;
		else if (aKey.equals(statusKeySKFactor))
			return statusKeySKFactorIndex;
		else if (aKey.equals(statusKeySUncertainty))
			return statusKeySUncertaintyIndex;
		else if (aKey.equals(statusKeySLastPlayed))
			return statusKeySLastPlayedIndex;
		else if (aKey.equals(statusKeyTimeLimit))
			return statusKeyTimeLimitIndex;
		else if (aKey.equals(statusKeyPRating))
			return statusKeyPRatingIndex;
		else if (aKey.equals(statusKeyPPlayCount))
			return statusKeyPPlayCountIndex;
		else if (aKey.equals(statusKeyPKFactor))
			return statusKeyPKFactorIndex;
		else if (aKey.equals(statusKeyPUncertainty))
			return statusKeyPUncertaintyIndex;
		else if (aKey.equals(statusKeyPLastPlayed))
			return statusKeyPLastPlayedIndex;
		else if (aKey.equals(statusKeyTime))
			return statusKeyTimeIndex;
		else if (aKey.equals(statusKeyRealtime))
			return statusKeyRealtimeIndex;
		else if (aKey.equals(statusKeyMultiple))
			return statusKeyMultipleIndex;
		else if (aKey.equals(statusKeyAccumulate))
			return statusKeyAccumulateIndex;
		else if (aKey.equals(statusKeyAutostart))
			return statusKeyAutostartIndex;
		else if (aKey.equals(statusKeyClosable))
			return statusKeyClosableIndex;
		else if (aKey.equals(statusKeyDirectfeedback))
			return statusKeyDirectfeedbackIndex;
		else if (aKey.equals(statusKeyLabelforoverviewbutton))
			return statusKeyLabelforoverviewbuttonIndex;
		else if (aKey.equals(statusKeyOverwrite))
			return statusKeyOverwriteIndex;
		else if (aKey.equals(statusKeyPracticeoverview))
			return statusKeyPracticeoverviewIndex;
		else if (aKey.equals(statusKeyShowfeedback))
			return statusKeyShowfeedbackIndex;
		else if (aKey.equals(statusKeyShowscore))
			return statusKeyShowscoreIndex;
		else if (aKey.equals(statusKeyTutorassespractice))
			return statusKeyTutorassespracticeIndex;
		else if (aKey.equals(statusKeyTutorassestest))
			return statusKeyTutorassestestIndex;
		else if (aKey.equals(statusKeyStudentassespractice))
			return statusKeyStudentassespracticeIndex;
		else if (aKey.equals(statusKeyStudentassestest))
			return statusKeyStudentassestestIndex;
		else if (aKey.equals(statusKeyStudentseefeedbackonothers))
			return statusKeyStudentseefeedbackonothersIndex;
		else if (aKey.equals(statusKeyStartoverview))
			return statusKeyStartoverviewIndex;
		else if (aKey.equals(statusKeyTestoverview))
			return statusKeyTestoverviewIndex;
		else if (aKey.equals(statusKeyTaskscheckable))
			return statusKeyTaskscheckableIndex;
		else if (aKey.equals(statusKeyZulfile))
			return statusKeyZulfileIndex;
		else if (aKey.equals(statusKeyRealstarttime))
			return statusKeyRealstarttimeIndex;
		else if (aKey.equals(statusKeyPosition))
			return statusKeyPositionIndex;
		else if (aKey.equals(statusKeySize))
			return statusKeySizeIndex;
		else if (aKey.equals(statusKeyPopup))
			return statusKeyPopupIndex;
		else if (aKey.equals(statusKeyTransitioneffect))
			return statusKeyTransitioneffectIndex;
		else if (aKey.equals(statusKeyAlwaysinspectfeedback))
			return statusKeyAlwaysinspectfeedbackIndex;
		else if (aKey.equals(statusKeyShowassfeedback))
			return statusKeyShowassfeedbackIndex;
		else if (aKey.equals(statusKeyHideinspection))
			return statusKeyHideinspectionIndex;
		else if (aKey.equals(statusKeyRepeat))
			return statusKeyRepeatIndex;
		else if (aKey.equals(statusKeyActive))
			return statusKeyActiveIndex;
		else if (aKey.equals(statusKeyMaxnumberofitems))
			return statusKeyMaxnumberofitemsIndex;
		else if (aKey.equals(statusKeySequencenumber))
			return statusKeySequencenumberIndex;
		else if (aKey.equals(statusKeyChosennumber))
			return statusKeyChosennumberIndex;
		else if (aKey.equals(statusKeyUsernotabletofinish))
			return statusKeyUsernotabletofinishIndex;
		else if (aKey.equals(statusKeyTeachermayinitiatestep))
			return statusKeyTeachermayinitiatestepIndex;
		else if (aKey.equals(statusKeyTeachermaycompletestep))
			return statusKeyTeachermaycompletestepIndex;
		else if (aKey.equals(statusKeyStudentmaycompletestep))
			return statusKeyStudentmaycompletestepIndex;
		else if (aKey.equals(statusKeyWeightingteacherstudent))
			return statusKeyWeightingteacherstudentIndex;
		else if (aKey.equals(statusKeyDeadlineDate))
			return statusKeyDeadlineDateIndex;
		else if (aKey.equals(statusKeyDeadlineTime))
			return statusKeyDeadlineTimeIndex;
		else if (aKey.equals(statusKeyReminderDate))
			return statusKeyReminderDateIndex;
		else if (aKey.equals(statusKeyReminderTime))
			return statusKeyReminderTimeIndex;
		else if (aKey.equals(statusKeyAllusersmustcompleteallsubsteps))
			return statusKeyAllusersmustcompleteallsubstepsIndex;
		else if (aKey.equals(statusKeySendDate))
			return statusKeySendDateIndex;
		else if (aKey.equals(statusKeySendTime))
			return statusKeySendTimeIndex;
		else if (aKey.equals(statusKeyDrawAttention))
			return statusKeyDrawAttentionIndex;
		else if (aKey.equals(statusKeyStartOfFadeOut))
			return statusKeyStartOfFadeOutIndex;
		else if (aKey.equals(statusKeyStartOfFadeIn))
			return statusKeyStartOfFadeInIndex;
		else if (aKey.equals(statusKeyMayaskteacherfeedbackcount))
			return statusKeyMayaskteacherfeedbackcountIndex;
		else if (aKey.equals(statusKeyMayscoreskill))
			return statusKeyMayscoreskillIndex;
		else if (aKey.equals(statusKeyMayscoreskillforteacher))
			return statusKeyMayscoreskillforteacherIndex;
		else if (aKey.equals(statusKeyMayscoreskillforpeerstudent))
			return statusKeyMayscoreskillforpeerstudentIndex;
		else if (aKey.equals(statusKeyMaytipsandtopsskill))
			return statusKeyMaytipsandtopsskillIndex;
		else if (aKey.equals(statusKeyMaytipsandtopsskillforteacher))
			return statusKeyMaytipsandtopsskillforteacherIndex;
		else if (aKey.equals(statusKeyMaytipsandtopsskillforpeerstudent))
			return statusKeyMaytipsandtopsskillforpeerstudentIndex;
		else if (aKey.equals(statusKeyTeachermayusescoresofpreviouscycle))
			return statusKeyTeachermayusescoresofpreviouscycleIndex;
		else if (aKey.equals(statusKeySeeself))
			return statusKeySeeselfIndex;
		else if (aKey.equals(statusKeyMaychangeseeself))
			return statusKeyMaychangeseeselfIndex;
		else if (aKey.equals(statusKeyAskteacherfeedback))
			return statusKeyAskteacherfeedbackIndex;
		else if (aKey.equals(statusKeyTeacherfeedbackaskedcount))
			return statusKeyTeacherfeedbackaskedcountIndex;
		else if (aKey.equals(statusKeyMayrategivenfeedback))
			return statusKeyMayrategivenfeedbackIndex;
		else if (aKey.equals(statusKeyMustrategivenfeedback))
			return statusKeyMustrategivenfeedbackIndex;
		else if (aKey.equals(statusKeyFeedbackrating))
			return statusKeyFeedbackratingIndex;
		else
			return getStatusKeyExIndex(aKey);
	}

	// start external triggers
	protected String getStatusKeyEx(int aIndex) {
		if (aIndex == statusKeyExLatitudeIndex)
			return statusKeyExLatitude;
		if (aIndex == statusKeyExLongitudeIndex)
			return statusKeyExLongitude;
		if (aIndex == statusKeyExPositionIndex)
			return statusKeyExPosition;
		return "";
	}

	protected int getStatusKeyExIndex(String aKey) {
		if (aKey.equals(statusKeyExLatitude))
			return statusKeyExLatitudeIndex;
		if (aKey.equals(statusKeyExLongitude))
			return statusKeyExLongitudeIndex;
		if (aKey.equals(statusKeyExPosition))
			return statusKeyExPositionIndex;
		return -1;
	}
	// end external triggers

	@Override
	public String getStatusValue(int aIndex) {
		if (aIndex == statusValueTrueIndex)
			return statusValueTrue;
		if (aIndex == statusValueFalseIndex)
			return statusValueFalse;
		return "";
	}

	@Override
	public int getStatusValueIndex(String aValue) {
		if (aValue.equals(statusValueTrue))
			return statusValueTrueIndex;
		if (aValue.equals(statusValueFalse))
			return statusValueFalseIndex;
		return -1;
	}

	@Override
	public String getFunction(int aIndex) {
		if (aIndex == functionCountIndex)
			return functionCount;
		if (aIndex == functionTimeIndex)
			return functionTime;
		if (aIndex == functionIncIndex)
			return functionInc;
		if (aIndex == functionDecIndex)
			return functionDec;
		return "";
	}

	@Override
	public String getOperator(int aIndex) {
		if (aIndex == operatorEqIndex)
			return operatorEq;
		if (aIndex == operatorGtIndex)
			return operatorGt;
		if (aIndex == operatorGeIndex)
			return operatorGe;
		if (aIndex == operatorLtIndex)
			return operatorLt;
		if (aIndex == operatorLeIndex)
			return operatorLe;
		if (aIndex == operatorIntervalIndex)
			return operatorInterval;
		if (aIndex == operatorInIndex)
			return operatorIn;
		return "";
	}

	@Override
	public List<String> getTagConditionFunctionKeyIds(String aStatusKeyId) {
		List<String> lTagFunctionIds = new ArrayList<>(0);
		int lStatusKeyIndex = -1;
		if (!aStatusKeyId.equals("")) {
			try {
				lStatusKeyIndex = Integer.parseInt(aStatusKeyId);
			} catch (NumberFormatException e) {
				_log.error(e);
			}
		}
		if (lStatusKeyIndex != -1 && lStatusKeyIndex != statusKeyStartTimeIndex
				&& lStatusKeyIndex != statusKeyCurrentTimeIndex && lStatusKeyIndex != statusKeyVersionIndex)
			lTagFunctionIds.add("" + functionCountIndex);
		return lTagFunctionIds;
	}

	@Override
	public List<String> getTagActionFunctionKeyIds(String aStatusKeyId) {
		List<String> lTagFunctionIds = new ArrayList<>(0);
		int lStatusKeyIndex = -1;
		if (!aStatusKeyId.equals("")) {
			try {
				lStatusKeyIndex = Integer.parseInt(aStatusKeyId);
			} catch (NumberFormatException e) {
				_log.error(e);
			}
		}
		if (lStatusKeyIndex != -1
				&& (lStatusKeyIndex == statusKeyNumberofresitsIndex || lStatusKeyIndex == statusKeyValueIndex 
					|| lStatusKeyIndex == statusKeyMayaskteacherfeedbackcountIndex || lStatusKeyIndex == statusKeyTeacherfeedbackaskedcountIndex
					|| lStatusKeyIndex == statusKeyWeightingteacherstudentIndex || lStatusKeyIndex == statusKeyFeedbackratingIndex)) {
			lTagFunctionIds.add("" + functionIncIndex);
			lTagFunctionIds.add("" + functionDecIndex);
		}
		return lTagFunctionIds;
	}

	@Override
	public List<String> getTagOperatorKeyIds(String aStatusKeyId, String aStatusFunctionId) {
		List<String> lTagOperatorKeyIds = new ArrayList<>(0);
		int lStatusKeyIndex = -1;
		if (!aStatusKeyId.equals("")) {
			try {
				lStatusKeyIndex = Integer.parseInt(aStatusKeyId);
			} catch (NumberFormatException e) {
				_log.error(e);
			}
		}
		int lFunctionKeyIndex = -1;
		if (!aStatusFunctionId.equals(""))
			lFunctionKeyIndex = Integer.parseInt(aStatusFunctionId);
		if (lStatusKeyIndex == statusKeyStartTimeIndex || lStatusKeyIndex == statusKeyCurrentTimeIndex
				|| lStatusKeyIndex == statusKeyScoreIndex || lStatusKeyIndex == statusKeyVersionIndex
				|| lStatusKeyIndex == statusKeyValueIndex || lStatusKeyIndex == statusKeyContainerposIndex
				|| lStatusKeyIndex == statusKeyIndexIndex || lStatusKeyIndex == statusKeyNumberofattemptsIndex
				|| lStatusKeyIndex == statusKeyNumberofcorrectitemsIndex
				|| lStatusKeyIndex == statusKeyNumberofresitsIndex || lStatusKeyIndex == statusKeyNumberofreactionsIndex
				|| lStatusKeyIndex == statusKeyXpositionIndex || lStatusKeyIndex == statusKeyYpositionIndex
				|| lStatusKeyIndex == statusKeyMayaskteacherfeedbackcountIndex || lStatusKeyIndex == statusKeyTeacherfeedbackaskedcountIndex
				|| lStatusKeyIndex == statusKeyWeightingteacherstudentIndex || lStatusKeyIndex == statusKeyFeedbackratingIndex
				// start external triggers
				|| lStatusKeyIndex == statusKeyExLatitudeIndex || lStatusKeyIndex == statusKeyExLongitudeIndex
				|| lStatusKeyIndex == statusKeyExPositionIndex
				// end external triggers
				|| lFunctionKeyIndex == functionCountIndex) {
			lTagOperatorKeyIds.add("" + operatorEqIndex);
			lTagOperatorKeyIds.add("" + operatorGtIndex);
			lTagOperatorKeyIds.add("" + operatorGeIndex);
			lTagOperatorKeyIds.add("" + operatorLtIndex);
			lTagOperatorKeyIds.add("" + operatorLeIndex);
			lTagOperatorKeyIds.add("" + operatorIntervalIndex);
			lTagOperatorKeyIds.add("" + operatorInIndex);
		} else
			lTagOperatorKeyIds.add("" + operatorEqIndex);
		return lTagOperatorKeyIds;
	}

	@Override
	public List<String> getTagOperatorValueIds() {
		List<String> lTagOperatorValueIds = new ArrayList<>(0);
		lTagOperatorValueIds.add("" + statusValueTrueIndex);
		return lTagOperatorValueIds;
	}

	@Override
	public String getTagOperatorValueType(String aComId, String aCacId, String aTagName, String aStatusKeyId,
			String aStatusFunctionId) {
//		NOTE still to implement:
//		aCacId and aTagName have to be used to get statusparameter default value
//		if value is true or false, then type is boolean
//		if value is 0, then type is int
//		if value is empty, then type is string
//		if no multiple choice question answer is no int
		// NOTE State tags have their own type. Method should be adjusted accordingly.
		int lStatusKeyIndex = -1;
		if (!aStatusKeyId.equals("")) {
			try {
				lStatusKeyIndex = Integer.parseInt(aStatusKeyId);
			} catch (NumberFormatException e) {
				_log.error(e);
			}
		}
		int lFunctionKeyIndex = -1;
		if (!aStatusFunctionId.equals(""))
			lFunctionKeyIndex = Integer.parseInt(aStatusFunctionId);
		if (lStatusKeyIndex == statusKeyStartTimeIndex || lStatusKeyIndex == statusKeyCurrentTimeIndex)
			return "time";
		if (lStatusKeyIndex == statusKeyScoreIndex || lStatusKeyIndex == statusKeyVersionIndex
				|| lStatusKeyIndex == statusKeyContainerposIndex || lStatusKeyIndex == statusKeyIndexIndex
				|| lStatusKeyIndex == statusKeyNumberofattemptsIndex
				|| lStatusKeyIndex == statusKeyNumberofcorrectitemsIndex
				|| lStatusKeyIndex == statusKeyNumberofresitsIndex || lStatusKeyIndex == statusKeyNumberofreactionsIndex
				|| lStatusKeyIndex == statusKeyXpositionIndex || lStatusKeyIndex == statusKeyYpositionIndex
				|| lStatusKeyIndex == statusKeyMayaskteacherfeedbackcountIndex || lStatusKeyIndex == statusKeyTeacherfeedbackaskedcountIndex
				|| lStatusKeyIndex == statusKeyWeightingteacherstudentIndex || lStatusKeyIndex == statusKeyFeedbackratingIndex
				|| lFunctionKeyIndex == functionCountIndex)
			return "int";
		// start external triggers
		if (lStatusKeyIndex == statusKeyExLatitudeIndex || lStatusKeyIndex == statusKeyExLongitudeIndex)
			return "int";
		if (lStatusKeyIndex == statusKeyExPositionIndex)
			return "position";
		if (lStatusKeyIndex == statusKeyAnswerIndex || lStatusKeyIndex == statusKeyValueIndex
				|| lStatusKeyIndex == statusKeyStringIndex || lStatusKeyIndex == statusKeyUrlIndex
				|| lStatusKeyIndex == statusKeyIdIndex || lStatusKeyIndex == statusKeyRankingIndex)
			return "string";
		// end external triggers
		return "boolean";
	}

	@Override
	public boolean getOverwriteStatusValue(String aStatusKey) {
		return (aStatusKey.equals(statusKeyStartTime) || aStatusKey.equals(statusKeyCurrentTime)
				|| aStatusKey.equals(statusKeySelectedTagId) || aStatusKey.equals(statusKeySelectedTagData)
				|| aStatusKey.equals(statusKeyGameLogging) || aStatusKey.equals(statusKeyGameSettings));
	}

	@Override
	public String getSingleStatusValueTrue(String aStatusKey) {
		if (aStatusKey.equals(statusKeyOpened))
			return defKeySingleopen;
		return "";

	}

	@Override
	public boolean statusInRgaTag(String aStatusKey, String aStatusType) {
		return (((aStatusType.equals(statusTypeRun)) || (aStatusType.equals(statusTypeRunTeam)))
				&& (aStatusKey.equals(statusKeyRating)));
	}

	@Override
	public IEMail getMail(int aMaiId) {
		return mailDao.get(aMaiId);
	}

	private String deleteMailAddressFromString(String aAddressString, String aDeleteAddress) {
		if ((aAddressString == null) || (aAddressString.equals("")))
			return "";
		if ((aDeleteAddress == null) || (aDeleteAddress.equals("")))
			return aAddressString;
		aAddressString = "," + aAddressString + ",";
		// delete invalid address:
		aAddressString = aAddressString.replace("," + aDeleteAddress + ",", ",");
		// delete added ",":
		Pattern lPattern = Pattern.compile("^,(.*),$");
		Matcher lMatcher = lPattern.matcher(aAddressString);
		aAddressString = (aAddressString.length() < 2) ? "" : lMatcher.replaceAll("$1");
		return aAddressString;
	}

	private static InternetAddress[] checkMailAddresses(String aAddresses) {
		List<InternetAddress> lAddressList = new ArrayList<>();
		String lCheckedAddress = "";
		if (aAddresses != null && !aAddresses.equals("")) {
			try {
				InternetAddress[] lAddresses = InternetAddress.parse(aAddresses, false);
				for (InternetAddress lAddress : lAddresses) {
					lAddressList.add(lAddress);
				}
			} catch (AddressException e) {
				_log.info("send mail error: mail address not valid: {}", lCheckedAddress);
			}
		}
		InternetAddress[] lAddressesNew = null;
		int lAddressLength = lAddressList.size();
		if (lAddressLength > 0) {
			lAddressesNew = lAddressList.toArray(new InternetAddress[lAddressLength]);
		}
		return lAddressesNew;
	}

	@Override
	public String sendMail(String aFrom, String aTos, String aCcs, String aBccs, String aSubject, String aBody) {
		if (StringUtils.isEmpty(aTos) && StringUtils.isEmpty(aCcs) && StringUtils.isEmpty(aBccs))
			return "to, cc and bcc are empty";
		String lSmtpServer = getSysvalue(syskey_smtpserver);
		if (StringUtils.isEmpty(lSmtpServer))
			return "smtpserver empty";
		try {
			InternetAddress[] lInternetTos = checkMailAddresses(aTos);
			InternetAddress[] lInternetCcs = checkMailAddresses(aCcs);
			InternetAddress[] lInternetBccs = checkMailAddresses(aBccs);
			if (lInternetTos == null && lInternetCcs == null && lInternetBccs == null)
				return "no existing mailaddresses: " + aTos + "; " + aCcs + "; " + aBccs;
			Properties props = System.getProperties();
			// -- Attaching to default Session, or we could start a new one --
			props.put("mail.smtp.host", lSmtpServer);
			Session session = Session.getDefaultInstance(props, null);
			// -- Create a new message --
			Message msg = new MimeMessage(session);
			// -- Set the FROM and TO fields --
			msg.setFrom(new InternetAddress(aFrom));
			if (lInternetTos != null)
				msg.setRecipients(Message.RecipientType.TO, lInternetTos);
			// -- We could include CC recipients too --
			if (lInternetCcs != null)
				msg.setRecipients(Message.RecipientType.CC, lInternetCcs);
			if (lInternetBccs != null)
				msg.setRecipients(Message.RecipientType.BCC, lInternetBccs);
			// -- Set the subject and body text --
			msg.setSubject(aSubject);
			msg.setText(aBody);
			// -- Set some other header information --
			msg.setHeader("Mailer", "Emergo");
			msg.setSentDate(new Date());
			// -- Send the message --
			javax.mail.Transport.send(msg);
		} catch (Exception ex) {
			_log.error(ex);
			String errormsg = "";
			String aOldTos = aTos;
			String aOldCcs = aCcs;
			String aOldBccs = aBccs;
			do {
				if (ex instanceof SendFailedException) {
					SendFailedException sfex = (SendFailedException) ex;
					Address[] invalid = sfex.getInvalidAddresses();
					if (invalid != null && invalid.length > 0) {
						String lStr = "Invalid formatted Addresses: ";
						errormsg = (errormsg.length() > 0) ? String.format("%s%n%s", errormsg, lStr) : lStr;
						for (int i = 0; i < invalid.length; i++) {
							aTos = deleteMailAddressFromString(aTos, invalid[i].toString());
							aCcs = deleteMailAddressFromString(aCcs, invalid[i].toString());
							aBccs = deleteMailAddressFromString(aBccs, invalid[i].toString());
							errormsg += invalid[i] + " ";
						}
					}
					Address[] validUnsent = sfex.getValidUnsentAddresses();
					if (validUnsent != null && validUnsent.length > 0) {
						String lStr = "ValidUnsent Addresses: ";
						errormsg = (errormsg.length() > 0) ? String.format("%s%n%s", errormsg, lStr) : lStr;
						for (int i = 0; i < validUnsent.length; i++) {
							errormsg += validUnsent[i] + " ";
						}
					}
				}
				if (ex instanceof SMTPAddressFailedException) {
					SMTPAddressFailedException smex = (SMTPAddressFailedException) ex;
					String lFailedAddress = smex.getAddress().getAddress();
					String lStr = "Invalid SMTP Address: " + lFailedAddress;
					errormsg = (errormsg.length() > 0) ? String.format("%s%n%s", errormsg, lStr) : lStr;
					aTos = deleteMailAddressFromString(aTos, lFailedAddress);
					aCcs = deleteMailAddressFromString(aCcs, lFailedAddress);
					aBccs = deleteMailAddressFromString(aBccs, lFailedAddress);
				}
				if (ex instanceof UnknownHostException) {
					UnknownHostException uhex = (UnknownHostException) ex;
					String lStr = "unknown host: " + uhex.getMessage();
					errormsg = (errormsg.length() > 0) ? String.format("%s%n%s", errormsg, lStr) : lStr;
					// don't try again:
					aTos = "";
					aCcs = "";
					aBccs = "";
				}
				if (ex instanceof MessagingException) {
					ex = ((MessagingException) ex).getNextException();
				} else {
					ex = null;
				}
			} while (ex != null);
			if (!((StringUtils.isEmpty(aTos) && StringUtils.isEmpty(aCcs) && StringUtils.isEmpty(aBccs))
					|| ((aOldTos == null || aOldTos.equals(aTos)) && (aOldCcs == null || aOldCcs.equals(aCcs))
							&& (aOldBccs == null || aOldBccs.equals(aBccs))))) {
				String lStr = sendMail(aFrom, aTos, aCcs, aBccs, aSubject, aBody);
				errormsg = (errormsg.length() > 0) ? String.format("%s%n%s", errormsg, lStr) : lStr;
				if (!errormsg.equals("")) {
					CDesktopComponents.sSpring().getSLogHelper().logAction(errormsg);
				}
			}
			return errormsg;
		}
		return "";
	}

	@Override
	public Hashtable<String, Object> getAppContainer() {
		return appContainer;
	}

	protected boolean isZkDesktopPresent() {
		return Executions.getCurrent() != null && Executions.getCurrent().getDesktop() != null;
	}

	@Override
	public String getAbsoluteAppPath() {
		if (isZkDesktopPresent()) {
			return appHelper.getAbsoluteAppPath();
		} else if (appContext != null) {
			return (((XmlWebApplicationContext) appContext).getServletContext()).getRealPath("");
		}
		return "";
	}

	@Override
	public String getAbsoluteBlobPath() {
		return (getAbsoluteAppPath() + appHelper.getAbsolutePath(getInitParameter("emergo.blob.path")));
	}

	@Override
	public String getAbsoluteStreamingPath() {
		return (getAbsoluteAppPath() + appHelper.getAbsolutePath(getInitParameter("emergo.streaming.path")));
	}

	@Override
	public String getAbsoluteTempPath() {
		return (getAbsoluteAppPath() + appHelper.getAbsolutePath(getInitParameter("emergo.temp.path")));
	}

	@Override
	public String getFFMPegPath() {
		String lFFMpegVal = PropsValues.FFMPEG_PATH;
		String lFFMpegDir = "";
		try {
			Path lFFMpegPath = Paths.get(lFFMpegVal);
			if(!lFFMpegPath.isAbsolute()) {
				lFFMpegPath = Paths.get(getAbsoluteAppPath(),lFFMpegVal);
			}
			lFFMpegDir = lFFMpegPath.toString();
			CDesktopComponents.sSpring().getSLogHelper().logAction(String.format("FFMpegpath: %s", lFFMpegDir));
		} catch (InvalidPathException e) {
			_log.error(e);
		}
		return lFFMpegDir;
	}

	@Override
	public String getContentType(String pFileName) {
		ConfigurableMimeFileTypeMap mimeMap = new ConfigurableMimeFileTypeMap();
		String lMimetypesPath = appHelper.getAbsolutePath(getInitParameter("emergo.mimetypes.path"));
		Path lMimetypesFile = Paths.get(getAbsoluteAppPath() + lMimetypesPath, "mime.types");
		mimeMap.setMappingLocation(new PathResource(lMimetypesFile));
		return mimeMap.getContentType(pFileName);// defaults to application/octet-stream
	}

	@Override
	public String getInitParameter(String aKey) {
		if (isZkDesktopPresent()) {
			return appHelper.getInitParameter(aKey);
		} else if (appContext != null) {
			String lValue = (((XmlWebApplicationContext) appContext).getServletContext()).getInitParameter(aKey);
			if (lValue == null) {
				lValue = "";
			}
			return lValue;
		}
		return "";
	}

	@Override
	public void addDefTreeReferenceToRemoveFromAppMemory(String aId) {
		if (!defTreeReferencesToRemoveFromAppMemory.contains(aId)) {
			defTreeReferencesToRemoveFromAppMemory.add(aId);
		}
	}

	@Override
	public void removeDefTreeReferencesFromAppMemory(IEComponent aComponent) {
		// NOTE Remove references to def tree from application memory
		String lComId = "" + aComponent.getComId();
		for (String lDefTreeReferenceId : defTreeReferencesToRemoveFromAppMemory) {
			Hashtable lDefTreeReference = (Hashtable) getAppContainer().get(lDefTreeReferenceId);
			if (lDefTreeReference != null && lDefTreeReference.containsKey(lComId)) {
				lDefTreeReference.remove(lComId);
			}
		}
	}

	@Override
	public void addDataTreeReferenceToRemoveFromAppMemory(String aId) {
		if (!dataTreeReferencesToRemoveFromAppMemory.contains(aId)) {
			dataTreeReferencesToRemoveFromAppMemory.add(aId);
		}
	}

	@Override
	public void removeDataTreeReferencesFromAppMemory(IECaseComponent aCaseComponent) {
		// NOTE Remove references to data tree from application memory
		String lCacId = "" + aCaseComponent.getCacId();
		for (String lDataTreeReferenceId : dataTreeReferencesToRemoveFromAppMemory) {
			Hashtable lDataTreeReference = (Hashtable) getAppContainer().get(lDataTreeReferenceId);
			if (lDataTreeReference != null && lDataTreeReference.containsKey(lCacId)) {
				lDataTreeReference.remove(lCacId);
			}
		}
	}

}