/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business.impl;

import java.io.File;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;

import nl.surf.emergo.business.abstr.AbstractDaoBasedBlobManager;
import nl.surf.emergo.domain.EBlob;
import nl.surf.emergo.domain.IEBlob;

/**
 * The Class BlobManager.
 */
public class BlobManager extends AbstractDaoBasedBlobManager implements
		InitializingBean {

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	public void afterPropertiesSet() throws Exception {
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.IBlobManager#getNewBlob()
	 */
	public IEBlob getNewBlob() {
		IEBlob eBlob = new EBlob();
		return eBlob;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.IBlobManager#getBlob(int)
	 */
	public IEBlob getBlob(int bloId) {
		IEBlob eBlob = blobDao.get(bloId);
		return eBlob;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.IBlobManager#saveBlob(nl.surf.emergo.domain.IEBlob)
	 */
	public void saveBlob(IEBlob aBlob) {
		blobDao.save(aBlob);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.IBlobManager#newBlob(nl.surf.emergo.domain.IEBlob)
	 */
	public List<String[]> newBlob(IEBlob aBlob) {
		// Set creationdate and lastupdatedate before saving.
		aBlob.setCreationdate(new Date());
		aBlob.setLastupdatedate(new Date());
		saveBlob(aBlob);
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.IBlobManager#createFile(nl.surf.emergo.domain.IEBlob, byte[])
	 */
	public String createFile(IEBlob aBlob, byte[] aContent) {
		String lFileName = aBlob.getFilename();
		if (lFileName.equals("")) {
			lFileName = aBlob.getUrl();
		}
		// path on disk
		String lDiskPath = getPath(aBlob.getBloId(), lFileName);
		lFileName = getFileName(lFileName);
		if (aContent != null) {
			lFileName = fileManager.createFile(lDiskPath, lFileName, aContent);
			if (!lFileName.equals("")) {
				return (lDiskPath + lFileName);
			}
		}
		return "";
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.IBlobManager#copyFile(int, int, java.lang.String)
	 */
	public boolean copyFile(int aOldBloId, int aNewBloId, String aFileName) {
		// path on disk
		String lDiskPath = appManager.getAbsoluteBlobPath();
		// copy file in sub folder newBloId
		String lOldFileName = lDiskPath + aOldBloId + "/" + aFileName;
		String lNewFileName = lDiskPath + aNewBloId + "/" + aFileName;
		fileManager.createDir(lDiskPath + aNewBloId + "/");
		return fileManager.copyFile(lOldFileName, lNewFileName);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.IBlobManager#readFile(int, java.lang.String)
	 */
	public File getFile(int aBloId, String aFileName) {
		String lDiskPath = getPath(aBloId, aFileName);
		aFileName = getFileName(aFileName);
		return fileManager.getFile(lDiskPath + aFileName);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.IBlobManager#readFile(int, java.lang.String)
	 */
	public byte[] readFile(int aBloId, String aFileName) {
		String lDiskPath = getPath(aBloId, aFileName);
		aFileName = getFileName(aFileName);
		return fileManager.readFile(lDiskPath + aFileName);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.IBlobManager#deleteFile(nl.surf.emergo.domain.IEBlob)
	 */
	public void deleteFile(IEBlob aBlob) {
		deleteFileAndOrFolder(aBlob, false);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.IBlobManager#deleteFile(nl.surf.emergo.domain.IEBlob)
	 */
	public void deleteFileAndFolder(IEBlob aBlob) {
		deleteFileAndOrFolder(aBlob, true);
	}

	/**
	 * Deletes file defined within blob and the folder containing it depending on aDeleteFolder.
	 * 
	 * @param aBlob the blob
	 * @param aDeleteFolder the delete folder
	 */
	protected void deleteFileAndOrFolder(IEBlob aBlob, boolean aDeleteFolder) {
		String lFileName = aBlob.getFilename();
		if (lFileName.equals("")) {
			lFileName = aBlob.getUrl();
		}
		String lDiskPath = getPath(aBlob.getBloId(), lFileName);
		lFileName = getFileName(lFileName);
		if (!lFileName.equals("")) {
			fileManager.deleteFile(lDiskPath + lFileName);
			// update blob
			aBlob.setFilename("");
			updateBlob(aBlob);
		}
		if (aDeleteFolder) {
			fileManager.deleteDir(lDiskPath);
		}
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.IBlobManager#newFileBlob(nl.surf.emergo.domain.IEBlob,byte[])
	 */
	public List<String[]> newFileBlob(IEBlob aBlob, byte[] aContent) {
//		save content
		newBlob(aBlob);
//		save content to file after saving blo so bloId is set
		createFile(aBlob, aContent);
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.IBlobManager#newFileBlob(nl.surf.emergo.domain.IEBlob, nl.surf.emergo.domain.IEBlob)
	 */
	public List<String[]> newFileBlob(IEBlob aOldBlob, IEBlob aNewBlob) {
		newBlob(aNewBlob);
//		copy file
		copyFile(aOldBlob.getBloId(), aNewBlob.getBloId(), aOldBlob.getFilename());
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.IBlobManager#updateBlob(nl.surf.emergo.domain.IEBlob)
	 */
	public List<String[]> updateBlob(IEBlob aBlob) {
		 // Set lastupdatedate before saving.
		aBlob.setLastupdatedate(new Date());
		saveBlob(aBlob);
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.IBlobManager#updateFileBlob(nl.surf.emergo.domain.IEBlob,byte[])
	 */
	public List<String[]> updateFileBlob(IEBlob aBlob, byte[] aContent) {
//		get old file and delete
		IEBlob lOldBlob = getBlob(aBlob.getBloId());
		deleteFileAndFolder(lOldBlob);
//		save content
		updateBlob(aBlob);
//		save content to file
		createFile(aBlob, aContent);
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.IBlobManager#deleteBlob(int)
	 */
	public void deleteBlob(int bloId) {
		deleteBlob(getBlob(bloId));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.IBlobManager#deleteBlob(nl.surf.emergo.domain.IEBlob)
	 */
	public void deleteBlob(IEBlob aBlob) {
		deleteBlob(aBlob, true);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.IBlobManager#deleteBlob(nl.surf.emergo.domain.IEBlob, boolean)
	 */
	public void deleteBlob(IEBlob aBlob, boolean aDeleteFolder) {
		if (aBlob == null)
			return;
//		if file on disk exists, it will be deleted first
		deleteFileAndOrFolder(aBlob, aDeleteFolder);
		if (aBlob.getLastupdatedate() == null) {
			// lastupdatedate field is added later, so value can be empty for older records
			aBlob.setLastupdatedate(aBlob.getCreationdate());
		}
		blobDao.delete(aBlob);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.IBlobManager#copyBlob(int)
	 */
	public int copyBlob(int bloId) {
		IEBlob eBlob = getBlob(bloId);
		IEBlob eNewBlob = getNewBlob();
		eNewBlob.setName(eBlob.getName());
		eNewBlob.setFilename(eBlob.getFilename());
		eNewBlob.setContenttype(eBlob.getContenttype());
		eNewBlob.setFormat(eBlob.getFormat());
		eNewBlob.setUrl(eBlob.getUrl());
		newFileBlob(eBlob, eNewBlob);
		return eNewBlob.getBloId();
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.IBlobManager#getAllBlobs()
	 */
	public List<IEBlob> getAllBlobs() {
		return blobDao.getAll();
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.IBlobManager#getAllBlobsByBloIds(List)
	 */
	public List<IEBlob> getAllBlobsByBloIds(List<Integer> bloIds) {
		return blobDao.getAllByBloIds(bloIds);
	}

	/**
	 * Gets path of blob.
	 *
	 * @param aBloId the blo id
	 * @param aFileName the file name
	 *
	 * @return the path
	 */
	private String getPath(int aBloId, String aFileName) {
		int lPos = aFileName.lastIndexOf("/");
		if (lPos <= 0) {
			// blob path on disk plus blo id
			return appManager.getAbsoluteBlobPath() + aBloId + "/";
		}
		else {
			// blob path on disk plus path in file name
			return appManager.getAbsoluteStreamingPath() + aFileName.substring(0, lPos + 1);
		}
	}

	/**
	 * Gets file name of blob.
	 *
	 * @param aBloId the blo id
	 * @param aFileName the file name
	 *
	 * @return the path
	 */
	private String getFileName(String aFileName) {
		int lPos = aFileName.lastIndexOf("/");
		if (lPos <= 0) {
			return aFileName;
		}
		else {
			return aFileName.substring(lPos + 1);
		}
	}

}