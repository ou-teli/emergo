/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.InitializingBean;

import nl.surf.emergo.business.abstr.AbstractDaoBasedAccountRequestManager;
import nl.surf.emergo.domain.EAccountRequest;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IEAccountRequest;
import nl.surf.emergo.domain.IERole;

/**
 * The Class AccountRequestManager.
 */
public class AccountRequestManager extends AbstractDaoBasedAccountRequestManager implements InitializingBean {

	private static final String ERROR_EMPTY = "error_empty";

	@Override
	public void afterPropertiesSet() throws Exception {
		// NOOP
	}

	@Override
	public IEAccountRequest getNewAccountRequest() {
		return new EAccountRequest();
	}

	@Override
	public IEAccountRequest getAccountRequest(int arqId) {
		return accountRequestDao.get(arqId);
	}

	@Override
	public IEAccountRequest getAccountRequest(String userid) {
		return accountRequestDao.get(userid);
	}

	@Override
	public IEAccountRequest getAccountRequest(String userid, String password, boolean processed) {
		return accountRequestDao.get(userid, password, processed);
	}

	@Override
	public String getAccountRequestName(IEAccountRequest eAccountRequest) {
		// build name using account request title, initials, name prefix and lastname
		String lName = "";
		if (eAccountRequest == null)
			return lName;
		lName = eAccountRequest.getTitle();
		String lStr = eAccountRequest.getInitials();
		if (!(lStr.equals(""))) {
			if (!(lName.equals("")))
				lName = lName + " ";
			lName = lName + lStr;
		}
		lStr = eAccountRequest.getNameprefix();
		if (!(lStr.equals(""))) {
			if (!(lName.equals("")))
				lName = lName + " ";
			lName = lName + lStr;
		}
		lStr = eAccountRequest.getLastname();
		if (!(lStr.equals(""))) {
			if (!(lName.equals("")))
				lName = lName + " ";
			lName = lName + lStr;
		}
		return lName;
	}

	@Override
	public void saveAccountRequest(IEAccountRequest eAccountRequest) {
		accountRequestDao.save(eAccountRequest);
	}

	@Override
	public List<String[]> newAccountRequest(IEAccountRequest eAccountRequest) {
		// Validate item and if ok, save it after setting creationdate and
		// lastupdatedate.
		List<String[]> lErrors = validateAccountRequest(eAccountRequest);
		if (lErrors == null) {
			eAccountRequest.setCreationdate(new Date());
			eAccountRequest.setLastupdatedate(new Date());
			saveAccountRequest(eAccountRequest);
		}
		return lErrors;
	}

	@Override
	public List<String[]> newAccountRequest(Hashtable<String, String> eAccountRequestTable, boolean encodePassword) {
		IEAccountRequest lAccountRequest = getNewAccountRequest();
		lAccountRequest.setUserid(eAccountRequestTable.get("userid"));
		lAccountRequest.setStudentid(eAccountRequestTable.get("studentid"));
		lAccountRequest.setEmail(eAccountRequestTable.get("email"));
		lAccountRequest.setTitle(eAccountRequestTable.get("title"));
		lAccountRequest.setInitials(eAccountRequestTable.get("initials"));
		lAccountRequest.setNameprefix(eAccountRequestTable.get("nameprefix"));
		lAccountRequest.setLastname(eAccountRequestTable.get("lastname"));
		String lPassword = eAccountRequestTable.get("password");
		if (encodePassword) {
			lPassword = accountManager.encodePassword(lPassword);
		}
		lAccountRequest.setPassword(lPassword);
		lAccountRequest.setProcessed(false);
		return newAccountRequest(lAccountRequest);
	}

	@Override
	public List<String[]> updateAccountRequest(IEAccountRequest eAccountRequest) {
		// Validate item and if ok, save it after setting lastupdatedate.
		List<String[]> lErrors = validateAccountRequest(eAccountRequest);
		if (lErrors == null) {
			eAccountRequest.setLastupdatedate(new Date());
			saveAccountRequest(eAccountRequest);
		}
		return lErrors;
	}

	@Override
	public List<String[]> validateAccountRequest(IEAccountRequest eAccountRequest) {
		/*
		 * Validate if userid, password and lastname are not empty. Validate if userid
		 * is unique (also when userid is changed). If validation error return it as
		 * element in String[] list.
		 */
		List<String[]> lErrors = new ArrayList<>(0);
		if (appManager.isEmpty(eAccountRequest.getUserid()))
			appManager.addError(lErrors, "userid", ERROR_EMPTY);
		else {
			IEAccountRequest lExistingAccountRequest = accountRequestDao.get(eAccountRequest.getArqId());
			// check if changed
			boolean lChanged = ((lExistingAccountRequest == null)
					|| (!eAccountRequest.getUserid().equals(lExistingAccountRequest.getUserid())));
			if ((lChanged) && (accountRequestDao.get(eAccountRequest.getUserid()) != null))
				appManager.addError(lErrors, "userid", "error_not_unique");
		}
		if (appManager.isEmpty(eAccountRequest.getPassword()))
			appManager.addError(lErrors, "password", ERROR_EMPTY);
		if (appManager.isEmpty(eAccountRequest.getLastname()))
			appManager.addError(lErrors, "lastname", ERROR_EMPTY);
		return lErrors.isEmpty() ? null : lErrors;
	}

	@Override
	public void deleteAccountRequest(int arqId) {
		deleteAccountRequest(getAccountRequest(arqId));
	}

	@Override
	public void deleteAccountRequest(IEAccountRequest eAccountRequest) {
		accountRequestDao.delete(eAccountRequest);
	}

	@Override
	public void deleteAccountRequest(String aUserId) {
		deleteAccountRequest(getAccountRequest(aUserId));
	}

	@Override
	public List<IEAccountRequest> getAllAccountRequests() {
		return accountRequestDao.getAll();
	}

	@Override
	public List<IEAccountRequest> getAllAccountRequests(boolean processed) {
		return accountRequestDao.getAll(processed);
	}

	@Override
	public String getAccountRequestPassword(String aUserId) {
		IEAccountRequest lAccount = getAccountRequest(aUserId);
		return lAccount.getPassword();
	}

	@Override
	public String getAccountRequestName(String aUserId) {
		IEAccountRequest lAccount = getAccountRequest(aUserId);
		return getAccountRequestName(lAccount);
	}

	@Override
	public IEAccount ratifyAccountRequest(String aUserId, String aPassword, boolean aProcessed) {
		IEAccountRequest lAccReq = getAccountRequest(aUserId, aPassword, aProcessed);
		if (lAccReq == null) {
			lAccReq = getAccountRequest(aUserId);
			if (lAccReq == null || lAccReq.getProcessed() != aProcessed
					|| !accountManager.matchesEncodedPassword(aPassword, lAccReq.getPassword())) {
				return null;
			}
		}
		IEAccount lItem = accountManager.getNewAccount();
		// just choose one admin as parent of this account
		List<IEAccount> lAdmins = accountManager.getAllAccounts(true, AppConstants.c_role_adm);
		int lAdmId = 0;
		if ((lAdmins != null) && (!lAdmins.isEmpty()))
			lAdmId = (lAdmins.get(0)).getAccId();
		lItem.setAccAccId(lAdmId);
		lItem.setUserid(lAccReq.getUserid());
		lItem.setPassword(lAccReq.getPassword());
		lItem.setStudentid(lAccReq.getStudentid());
		lItem.setTitle(lAccReq.getTitle());
		lItem.setInitials(lAccReq.getInitials());
		lItem.setNameprefix(lAccReq.getNameprefix());
		lItem.setLastname(lAccReq.getLastname());
		lItem.setEmail(lAccReq.getEmail());
		lItem.setPhonenumber("");
		lItem.setJob("");
		lItem.setExtradata("");
		lItem.setActive(true);
		lItem.setOpenaccess(true);
		// set student role for this account
		List<IERole> lAllRoles = accountManager.getAllRoles();
		if (lAllRoles != null) {
			Set<IERole> lRoles = new HashSet<>();
			for (IERole lObject : lAllRoles) {
				String lCode = lObject.getCode();
				if (lCode.equals(AppConstants.c_role_stu))
					lRoles.add(lObject);
			}
			lItem.setERoles(lRoles);
		}
		List<String[]> lErrors = accountManager.newAccount(lItem);
		if ((lErrors == null) || (lErrors.isEmpty())) {
			lAccReq.setPassword("");
			lAccReq.setProcessed(true);
			updateAccountRequest(lAccReq);
			return lItem;
		}
		return null;
	}

}