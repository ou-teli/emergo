/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;

import nl.surf.emergo.business.abstr.AbstractDaoBasedRunGroupCaseComponentManager;
import nl.surf.emergo.domain.ERunGroupCaseComponent;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IEComponent;
import nl.surf.emergo.domain.IERunGroupCaseComponent;

/**
 * The Class RunGroupCaseComponentManager.
 */
public class RunGroupCaseComponentManager extends AbstractDaoBasedRunGroupCaseComponentManager
		implements InitializingBean {

	@Override
	public void afterPropertiesSet() throws Exception {
		// NOOP
	}

	@Override
	public IERunGroupCaseComponent getNewRunGroupCaseComponent() {
		return new ERunGroupCaseComponent();
	}

	@Override
	public IERunGroupCaseComponent getRunGroupCaseComponent(int rgcId) {
		return runGroupCaseComponentDao.get(rgcId);
	}

	@Override
	public IERunGroupCaseComponent getRunGroupCaseComponent(int rugId, int cacId) {
		return runGroupCaseComponentDao.get(rugId, cacId);
	}

	@Override
	public void saveRunGroupCaseComponent(IERunGroupCaseComponent eRunGroupCaseComponent) {
		runGroupCaseComponentDao.save(eRunGroupCaseComponent);
	}

	@Override
	public List<String[]> newRunGroupCaseComponent(IERunGroupCaseComponent eRunGroupCaseComponent) {
		// NOTE sometimes multiple records with same rugRugId and cacCacId are created
		// although method getRunGroupCaseComponent(int rugId, int cacId) within SSpring
		// class is synchronized.
		// Therefore check if record already exists and if so save the existing record.
		IERunGroupCaseComponent lRunGroupCaseComponent = getRunGroupCaseComponent(eRunGroupCaseComponent.getRugRugId(),
				eRunGroupCaseComponent.getCacCacId());
		if (lRunGroupCaseComponent != null) {
			// NOTE to be sure check if creation date is not empty
			if (lRunGroupCaseComponent.getCreationdate() == null) {
				lRunGroupCaseComponent.setCreationdate(new Date());
			}
			// NOTE save XML data within existing record
			lRunGroupCaseComponent.setXmldata(eRunGroupCaseComponent.getXmldata());
			updateRunGroupCaseComponent(lRunGroupCaseComponent);
		} else {
			eRunGroupCaseComponent.setCreationdate(new Date());
			eRunGroupCaseComponent.setLastupdatedate(new Date());
			saveRunGroupCaseComponent(eRunGroupCaseComponent);
		}
		return null;
	}

	@Override
	public List<String[]> updateRunGroupCaseComponent(IERunGroupCaseComponent eRunGroupCaseComponent) {
		// NOTE to be sure check if creation date is not empty
		if (eRunGroupCaseComponent.getCreationdate() == null) {
			eRunGroupCaseComponent.setCreationdate(new Date());
		}
		eRunGroupCaseComponent.setLastupdatedate(new Date());
		saveRunGroupCaseComponent(eRunGroupCaseComponent);
		return null;
	}

	@Override
	public void deleteRunGroupCaseComponent(int rgcId) {
		deleteRunGroupCaseComponent(getRunGroupCaseComponent(rgcId));
	}

	@Override
	public void deleteRunGroupCaseComponent(IERunGroupCaseComponent eRunGroupCaseComponent) {
		// first delete possibly related blobs. they are not deleted in cascade.
		deleteBlobs(eRunGroupCaseComponent);
		if (eRunGroupCaseComponent.getLastupdatedate() == null)
			// lastupdatedate field is added later, so value can be empty for older records
			eRunGroupCaseComponent.setLastupdatedate(eRunGroupCaseComponent.getCreationdate());
		runGroupCaseComponentDao.delete(eRunGroupCaseComponent);
	}

	@Override
	public void deleteBlobs(IERunGroupCaseComponent eRunGroupCaseComponent) {
		// delete blobs referenced within xmldata property
		String lXmlDef = "";
		IECaseComponent eCaseComponent = caseComponentManager.getCaseComponent(eRunGroupCaseComponent.getCacCacId());
		if (eCaseComponent != null) {
			IEComponent eComponent = eCaseComponent.getEComponent();
			if (eComponent != null)
				lXmlDef = eComponent.getXmldefinition();
		}
		String lXmlData = eRunGroupCaseComponent.getXmldata();
		xmlManager.deleteBlobs(lXmlDef, lXmlData, AppConstants.status_tag);
	}

	@Override
	public List<IERunGroupCaseComponent> getAllRunGroupCaseComponents() {
		return runGroupCaseComponentDao.getAll();
	}

	@Override
	public List<IERunGroupCaseComponent> getAllRunGroupCaseComponentsByRugId(int rugId) {
		return runGroupCaseComponentDao.getAllByRugId(rugId);
	}

	@Override
	public List<IERunGroupCaseComponent> getAllRunGroupCaseComponentsByCacId(int cacId) {
		return runGroupCaseComponentDao.getAllByCacId(cacId);
	}

	@Override
	public List<IERunGroupCaseComponent> getAllRunGroupCaseComponentsByRugIdCacId(int rugId, int cacId) {
		return runGroupCaseComponentDao.getAllByRugIdCacId(rugId, cacId);
	}

	@Override
	public List<IERunGroupCaseComponent> getAllRunGroupCaseComponentsByRugIdCacIds(int rugId, List<Integer> cacIds) {
		return runGroupCaseComponentDao.getAllByRugIdCacIds(rugId, cacIds);
	}

	@Override
	public List<IERunGroupCaseComponent> getAllRunGroupCaseComponentsByRugIdsCacId(List<Integer> rugIds, int cacId) {
		return runGroupCaseComponentDao.getAllByRugIdsCacId(rugIds, cacId);
	}

	@Override
	public List<IERunGroupCaseComponent> getAllRunGroupCaseComponentsByRugIdsCacIds(List<Integer> rugIds,
			List<Integer> cacIds) {
		return runGroupCaseComponentDao.getAllByRugIdsCacIds(rugIds, cacIds);
	}

}