/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;

import nl.surf.emergo.business.abstr.AbstractDaoBasedCaseRoleManager;
import nl.surf.emergo.domain.ECaseRole;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.domain.IERunGroup;

/**
 * The Class CaseRoleManager.
 */
public class CaseRoleManager extends AbstractDaoBasedCaseRoleManager implements
		InitializingBean {

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	public void afterPropertiesSet() throws Exception {
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseRoleManager#getNewCaseRole()
	 */
	public IECaseRole getNewCaseRole() {
		IECaseRole eCaseRole = new ECaseRole();
		return eCaseRole;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseRoleManager#getCaseRole(int)
	 */
	public IECaseRole getCaseRole(int carId) {
		IECaseRole eCaseRole = caseRoleDao.get(carId);
		return eCaseRole;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseRoleManager#saveCaseRole(nl.surf.emergo.domain.IECaseRole)
	 */
	public void saveCaseRole(IECaseRole eCaseRole) {
		caseRoleDao.save(eCaseRole);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseRoleManager#newCaseRole(nl.surf.emergo.domain.IECaseRole)
	 */
	public List<String[]> newCaseRole(IECaseRole eCaseRole) {
		// Validate item and if ok, save it after setting creationdate and lastupdatedate.
		List<String[]> lErrors = validateCaseRole(eCaseRole);
		if (lErrors == null) {
			eCaseRole.setCreationdate(new Date());
			eCaseRole.setLastupdatedate(new Date());
			saveCaseRole(eCaseRole);
		}
		return lErrors;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseRoleManager#updateCaseRole(nl.surf.emergo.domain.IECaseRole)
	 */
	public List<String[]> updateCaseRole(IECaseRole eCaseRole) {
		// Validate item and if ok, save it after setting lastupdatedate.
		List<String[]> lErrors = validateCaseRole(eCaseRole);
		if (lErrors == null) {
			eCaseRole.setLastupdatedate(new Date());
			saveCaseRole(eCaseRole);
		}
		return lErrors;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseRoleManager#validateCaseRole(nl.surf.emergo.domain.IECaseRole)
	 */
	public List<String[]> validateCaseRole(IECaseRole eCaseRole) {
		/*
		 * Validate if name is not empty.
		 * Validate if name is unique.
		 * If validation error return it as element in String[] list.
		 */
		List<String[]> lErrors = new ArrayList<String[]>(0);
		if (appManager.isEmpty(eCaseRole.getName()))
			appManager.addError(lErrors, "name", "error_empty");
		else {
			IECaseRole lExistingCaseRole = caseRoleDao
					.get(eCaseRole.getCarId());
			// check if changed
			boolean lChanged = ((lExistingCaseRole == null) || (!eCaseRole
					.getName().equals(lExistingCaseRole.getName())));
			if ((lChanged)
					&& (caseRoleDao.exists(eCaseRole.getECase().getCasId(),
							eCaseRole.getName())))
				appManager.addError(lErrors, "name", "error_not_unique");
		}
		if (lErrors.size() > 0)
			return lErrors;
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseRoleManager#deleteCaseRole(int)
	 */
	public void deleteCaseRole(int carId) {
		deleteCaseRole(getCaseRole(carId));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseRoleManager#deleteCaseRole(nl.surf.emergo.domain.IECaseRole)
	 */
	public void deleteCaseRole(IECaseRole eCaseRole) {
		// first delete possibly related blobs. they are not deleted in cascade.
		deleteBlobs(eCaseRole);
		if (eCaseRole.getLastupdatedate() == null)
			// lastupdatedate field is added later, so value can be empty for older records
			eCaseRole.setLastupdatedate(eCaseRole.getCreationdate());
		caseRoleDao.delete(eCaseRole);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseRoleManager#deleteBlobs(nl.surf.emergo.domain.IECaseRole)
	 */
	public void deleteBlobs(IECaseRole eCaseRole) {
		// delete blobs for all run groups referenced by case role
		List<IERunGroup> lItems = runGroupManager.getAllRunGroupsByCarId(eCaseRole.getCarId());
		if (lItems != null) {
			for (IERunGroup lItem : lItems)
				runGroupManager.deleteBlobs(lItem);
		}
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseRoleManager#getAllCaseRoles()
	 */
	public List<IECaseRole> getAllCaseRoles() {
		return caseRoleDao.getAll();
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseRoleManager#getAllCaseRolesByCasId(int)
	 */
	public List<IECaseRole> getAllCaseRolesByCasId(int casId) {
		return caseRoleDao.getAllByCasId(casId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseRoleManager#getAllCaseRoles(boolean)
	 */
	public List<IECaseRole> getAllCaseRoles(boolean npc) {
		return caseRoleDao.getAll(npc);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseRoleManager#getAllCaseRolesByCasId(int, boolean)
	 */
	public List<IECaseRole> getAllCaseRolesByCasId(int casId, boolean npc) {
		return caseRoleDao.getAllByCasId(casId, npc);
	}

}