/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business.impl;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import nl.surf.emergo.business.IXMLAttributeValueTime;
import nl.surf.emergo.business.IXMLTag;

public class XMLTag implements IXMLTag {
	protected int type = -1;
	protected IXMLTag defTag = null;
	protected IXMLTag dataTag = null;
	protected List<IXMLTag> statusTags = null;
	protected IXMLTag parentTag = null;
	protected String name = null;
	protected String value = null;
	protected Hashtable<String, List<IXMLAttributeValueTime>> attributes = null;
	protected List<IXMLTag> childTags = null;

	@Override
	public int getType() {
		return type;
	}

	@Override
	public void setType(int aType) {
		type = aType;
	}

	@Override
	public IXMLTag getDefTag() {
		return defTag;
	}

	@Override
	public void setDefTag(IXMLTag aDefTag) {
		defTag = aDefTag;
	}

	@Override
	public IXMLTag getDataTag() {
		return dataTag;
	}

	@Override
	public void setDataTag(IXMLTag aDataTag) {
		dataTag = aDataTag;
	}

	@Override
	public IXMLTag getStatusTag() {
		if (getStatusTags() == null) {
			return null;
		}
		return getStatusTags().get(0);
	}

	@Override
	public void setStatusTag(IXMLTag aStatusTag) {
		aStatusTag.setDataTag(this);
		initStatusTags();
		getStatusTags().clear();
		getStatusTags().add(aStatusTag);
	}

	@Override
	public List<IXMLTag> getStatusTags() {
		return statusTags;
	}

	@Override
	public void setStatusTags(List<IXMLTag> aStatusTags) {
		statusTags = aStatusTags;
	}

	@Override
	public void addStatusTag(IXMLTag aStatusTag) {
		aStatusTag.setDataTag(this);
		initStatusTags();
		// remove already existing status tag with attribute id equal to id of
		// aStatusTag
		removeStatusTag(aStatusTag.getAttribute(AppConstants.keyId));
		getStatusTags().add(aStatusTag);
	}

	@Override
	public void removeStatusTag(String aId) {
		if (getStatusTags() == null) {
			return;
		}
		IXMLTag lStatusTag = getStatusTag(aId);
		while (lStatusTag != null) {
			getStatusTags().remove(lStatusTag);
			lStatusTag = getStatusTag(aId);
		}
	}

	@Override
	public IXMLTag getStatusTag(String aId) {
		if (getStatusTags() == null) {
			return null;
		}
		for (IXMLTag lStatusTag : getStatusTags()) {
			if (lStatusTag.getAttribute(AppConstants.keyId).equals(aId)) {
				return lStatusTag;
			}
		}
		return null;
	}

	@Override
	public IXMLTag getParentTag() {
		return parentTag;
	}

	@Override
	public void setParentTag(IXMLTag aParentTag) {
		parentTag = aParentTag;
	}

	@Override
	public String getName() {
		if (name == null)
			return "";
		return name;
	}

	@Override
	public void setName(String aName) {
		name = aName;
	}

	@Override
	public String getValue() {
		if (value == null)
			return "";
		return value;
	}

	@Override
	public void setValue(String aValue) {
		value = aValue;
	}

	@Override
	public Hashtable<String, List<IXMLAttributeValueTime>> getAttributes() {
		return attributes;
	}

	@Override
	public void setAttributes(Hashtable<String, List<IXMLAttributeValueTime>> aAttributes) {
		attributes = aAttributes;
	}

	@Override
	public List<IXMLTag> getChildTags() {
		return childTags;
	}

	@Override
	public void setChildTags(List<IXMLTag> aChildTags) {
		childTags = aChildTags;
	}

	@Override
	public String getAttribute(String aKey) {
		List<IXMLAttributeValueTime> lValueTimes = getAttributeAsList(aKey);
		if (lValueTimes == null || lValueTimes.isEmpty()) {
			return "";
		}
		return lValueTimes.get(0).getValue();
	}

	@Override
	public void setAttribute(String aKey, String aValue) {
		setAttribute(aKey, aValue, -1);
	}

	@Override
	public void setAttribute(String aKey, String aValue, double aTime) {
		initAttributes();
		initAttribute(aKey);
		Hashtable<String, List<IXMLAttributeValueTime>> lAttributes = getAttributes();
		if (lAttributes.get(aKey).isEmpty()) {
			// add value time
			lAttributes.get(aKey).add(new XMLAttributeValueTime(aValue, aTime));
		} else {
			// change value time of first entry
			lAttributes.get(aKey).get(0).setValueTime(aValue, aTime);
		}
	}

	@Override
	public List<IXMLAttributeValueTime> getAttributeAsList(String aKey) {
		Hashtable<String, List<IXMLAttributeValueTime>> lAttributes = getAttributes();
		if (lAttributes == null) {
			return null;
		}
		for (Enumeration<String> keys = lAttributes.keys(); keys.hasMoreElements();) {
			String key = keys.nextElement();
			if (key.equals(aKey))
				return lAttributes.get(key);
		}
		return null;
	}

	@Override
	public void setAttributeAsList(String aKey, List<IXMLAttributeValueTime> aValue) {
		getAttributes().put(aKey, aValue);
	}

	@Override
	public void addAttributeValueTimeToList(String aKey, String aValue, double aTime) {
		initAttributes();
		initAttribute(aKey);
		getAttributes().get(aKey).add(new XMLAttributeValueTime(aValue, aTime));
	}

	@Override
	public boolean isAttribute(String aKey) {
		Hashtable<String, List<IXMLAttributeValueTime>> lAttributes = getAttributes();
		if (lAttributes == null)
			return false;
		for (Enumeration<String> keys = lAttributes.keys(); keys.hasMoreElements();) {
			if (keys.nextElement().equals(aKey))
				return true;
		}
		return false;
	}

	@Override
	public IXMLTag getChild(String aName) {
		for (IXMLTag lXmltag : getChildTags()) {
			if (lXmltag.getName().equals(aName))
				return lXmltag;
		}
		return null;
	}

	@Override
	public List<IXMLTag> getChilds(String aNames) {
		String[] lNamesArr = aNames.split(",");
		List<IXMLTag> lChildtags = new ArrayList<>();
		for (IXMLTag lXmltag : getChildTags()) {
			for (int i = 0; i < lNamesArr.length; i++) {
				if (lXmltag.getName().equals(lNamesArr[i])) {
					lChildtags.add(lXmltag);
				}
			}
		}
		return lChildtags;
	}

	@Override
	public List<IXMLTag> getChildTags(String aType) {
		List<IXMLTag> lChildtags = getChildTags();
		List<IXMLTag> lTypedchildtags = new ArrayList<>();
		if (lChildtags == null)
			return lTypedchildtags;
		for (IXMLTag lXmltag : lChildtags) {
			boolean lTypeOk = lXmltag.getAttribute(AppConstants.defKeyType).equals(aType);
			if (!lTypeOk)
				lTypeOk = (lXmltag.getDefTag() != null
						&& lXmltag.getDefTag().getAttribute(AppConstants.defKeyType).equals(aType));
			if (lTypeOk)
				lTypedchildtags.add(lXmltag);
		}
		return lTypedchildtags;
	}

	@Override
	public String getChildValue(String aChildName) {
		IXMLTag lChildTag = getChild(aChildName);
		if (lChildTag == null)
			return "";
		return lChildTag.getValue();
	}

	@Override
	public void setChildValue(String aChildName, String aChildValue) {
		IXMLTag lChildTag = getChild(aChildName);
		if (lChildTag == null)
			return;
		lChildTag.setValue(aChildValue);
	}

	@Override
	public String getChildAttribute(String aChildName, String aKey) {
		IXMLTag lChildTag = getChild(aChildName);
		if (lChildTag == null)
			return "";
		return lChildTag.getAttribute(aKey);
	}

	@Override
	public void setChildAttribute(String aChildName, String aKey, String aValue) {
		IXMLTag lChildTag = getChild(aChildName);
		if (lChildTag == null)
			return;
		lChildTag.setAttribute(aKey, aValue);
	}

	@Override
	public List<IXMLAttributeValueTime> getChildAttributeAsList(String aChildName, String aKey) {
		IXMLTag lChildTag = getChild(aChildName);
		if (lChildTag == null)
			return null;
		return lChildTag.getAttributeAsList(aKey);
	}

	@Override
	public String getDefValue() {
		String lStr = getValue();
		if ((lStr == null || lStr.equals("")) && getDefTag() != null)
			lStr = getDefTag().getDefValue();
		return lStr;
	}

	@Override
	public String getDefAttribute(String aKey) {
		String lStr = getAttribute(aKey);
		if ((lStr == null || lStr.equals("")) && getDefTag() != null)
			lStr = getDefTag().getDefAttribute(aKey);
		return lStr;
	}

	@Override
	public IXMLTag getDefChild(String aName) {
		IXMLTag lChild = getChild(aName);
		if (lChild == null && getDefTag() != null)
			lChild = getDefTag().getChild(aName);
		return lChild;
	}

	@Override
	public String getDefChildValue(String aChildName) {
		IXMLTag lChildTag = getDefChild(aChildName);
		if (lChildTag == null)
			return "";
		return lChildTag.getDefValue();
	}

	@Override
	public String getDefChildAttribute(String aChildName, String aKey) {
		IXMLTag lChildTag = getDefChild(aChildName);
		if (lChildTag == null)
			return "";
		return lChildTag.getDefAttribute(aKey);
	}

	@Override
	public IXMLTag cloneWithoutParentAndChildren() {
		IXMLTag lClone = new XMLTag();
		lClone.setType(this.getType());
		lClone.setDefTag(this.getDefTag());
		lClone.setName(new String(this.getName()));
		lClone.setValue(new String(this.getValue()));
		Hashtable<String, List<IXMLAttributeValueTime>> lAttributes = getAttributes();
		if (lAttributes != null) {
			Hashtable<String, List<IXMLAttributeValueTime>> lClonedAttributes = new Hashtable<>(lAttributes.size());
			for (Enumeration<String> keys = lAttributes.keys(); keys.hasMoreElements();) {
				String key = new String(keys.nextElement());
				List<IXMLAttributeValueTime> lValueTimes = new ArrayList<IXMLAttributeValueTime>();
				for (IXMLAttributeValueTime lValueTime : lAttributes.get(key)) {
					lValueTimes.add(new XMLAttributeValueTime(lValueTime.getValue(), lValueTime.getTime()));
				}
				lClonedAttributes.put(key, lValueTimes);
			}
			lClone.setAttributes(lClonedAttributes);
		}
		lClone.setChildTags(new ArrayList<>());
		return lClone;
	}

	@Override
	public List<IXMLAttributeValueTime> getStatusAttribute(String aKey) {
		List<IXMLAttributeValueTime> lNewValueTimes = new ArrayList<>();
		List<IXMLAttributeValueTime> lValueTimes = getChildAttributeAsList(AppConstants.statusElement, aKey);
		if (lValueTimes != null) {
			lNewValueTimes.addAll(lValueTimes);
		}
		if (getStatusTags() != null) {
			for (IXMLTag lStatusTag : getStatusTags()) {
				lValueTimes = lStatusTag.getChildAttributeAsList(AppConstants.statusElement, aKey);
				if (lValueTimes != null) {
					lNewValueTimes.addAll(lValueTimes);
				}
			}
		}
		return lNewValueTimes;
	}

	@Override
	public String getCurrentStatusAttribute(String aKey) {
		List<IXMLAttributeValueTime> lValueTimes = getStatusAttribute(aKey);
		if (lValueTimes == null || lValueTimes.isEmpty()) {
			// get data or definition value
			return getDefChildAttribute(AppConstants.statusElement, aKey);
		}
		return lValueTimes.get(lValueTimes.size() - 1).getValue();
	}

	@Override
	public String getPreviousStatusAttribute(String aKey, int aIndex) {
		List<IXMLAttributeValueTime> lValueTimes = getStatusAttribute(aKey);
		if (lValueTimes == null || lValueTimes.isEmpty() || aIndex < 0 || aIndex >= lValueTimes.size())
			return "";
		if (lValueTimes.get(0).getTime() == -1) {
			// don't use data or definition value
			aIndex++;
		}
		if (aIndex >= lValueTimes.size())
			return "";
		return lValueTimes.get(aIndex).getValue();
	}

	@Override
	public double getCurrentStatusAttributeTime(String aKey) {
		List<IXMLAttributeValueTime> lValueTimes = getStatusAttribute(aKey);
		if (lValueTimes == null || lValueTimes.isEmpty()
				|| (lValueTimes.size() == 1 && lValueTimes.get(0).getTime() == -1)) {
			return 0;
		}
		return lValueTimes.get(lValueTimes.size() - 1).getTime();
	}

	@Override
	public double getPreviousStatusAttributeTime(String aKey, int aIndex) {
		List<IXMLAttributeValueTime> lValueTimes = getStatusAttribute(aKey);
		if (lValueTimes == null || lValueTimes.isEmpty() || aIndex < 0 || aIndex >= lValueTimes.size())
			return 0;
		if (lValueTimes.get(0).getTime() == -1) {
			// don't use data or definition value
			aIndex++;
		}
		if (aIndex >= lValueTimes.size())
			return 0;
		return lValueTimes.get(aIndex).getTime();
	}

	@Override
	public int getStatusAttributeCount(String aKey, String aValue) {
		List<IXMLAttributeValueTime> lValueTimes = getStatusAttribute(aKey);
		if (lValueTimes == null || lValueTimes.isEmpty()) {
			return 0;
		}
		int lCount = 0;
		for (IXMLAttributeValueTime lValueTime : lValueTimes) {
			if (lValueTime.getValue().equals(aValue) && lValueTime.getTime() != -1) {
				// don't count data or definition value
				lCount++;
			}
		}
		return lCount;
	}

	@Override
	public IXMLTag getStatusChild(String aName) {
		List<IXMLTag> lStatusTags = getStatusTags();
		IXMLTag lStatusStatusTag = null;
		List<IXMLTag> lChildTags = null;
		if (lStatusTags != null) {
			for (int i = lStatusTags.size() - 1; i >= 0; i--) {
				lStatusStatusTag = lStatusTags.get(i).getChild(AppConstants.statusElement);
				if (lStatusStatusTag != null) {
					lChildTags = lStatusStatusTag.getChildTags();
					if (lChildTags != null) {
						for (IXMLTag lXmlTag : lChildTags) {
							if (lXmlTag.getName().equals(aName)) {
								return lXmlTag;
							}
						}
					}
				}
			}
		}
		lChildTags = getChildTags();
		if (lChildTags != null) {
			for (IXMLTag lXmlTag : getChildTags()) {
				if (lXmlTag.getName().equals(aName)) {
					return lXmlTag;
				}
			}
		}
		return null;
	}

	@Override
	public List<IXMLTag> getStatusChilds(String aName) {
		List<IXMLTag> lStatusChilds = new ArrayList<>();
		IXMLTag lStatusStatusTag = null;
		if (getStatusTags() != null) {
			List<IXMLTag> lChildTags = null;
			for (IXMLTag lStatusTag : getStatusTags()) {
				lStatusStatusTag = lStatusTag.getChild(AppConstants.statusElement);
				if (lStatusStatusTag != null) {
					lChildTags = lStatusStatusTag.getChildTags();
					if (lChildTags != null) {
						for (IXMLTag lXmlTag : lChildTags) {
							if (lXmlTag.getName().equals(aName)) {
								lStatusChilds.add(lXmlTag);
							}
						}
					}
				}
			}
		}
		if (lStatusChilds.isEmpty()) {
			return getChilds(aName);
		}
		return lStatusChilds;
	}

	@Override
	public String getStatusChildValue(String aChildName) {
		IXMLTag lChildTag = getStatusChild(aChildName);
		if (lChildTag == null)
			return "";
		return lChildTag.getValue();
	}

	protected void initAttributes() {
		if (getAttributes() == null) {
			setAttributes(new Hashtable<>());
		}
	}

	protected void initAttribute(String aKey) {
		if (!getAttributes().containsKey(aKey)) {
			getAttributes().put(aKey, new ArrayList<>());
		}
	}

	protected void initStatusTags() {
		if (getStatusTags() == null) {
			setStatusTags(new ArrayList<>());
		}
	}

	public static final String restoreCommas(String aValue) {
		if (aValue.contains(AppConstants.statusCommaReplace)) {
			aValue = aValue.replace(AppConstants.statusCommaReplace, ",");
		}
		return aValue;
	}

	public static final String replaceCommas(String aValue) {
		if (aValue.contains(",")) {
			aValue = aValue.replace(",", AppConstants.statusCommaReplace);
		}
		return aValue;
	}

}
