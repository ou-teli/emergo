/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;

import nl.surf.emergo.business.abstr.AbstractDaoBasedRunCaseComponentManager;
import nl.surf.emergo.domain.ERunCaseComponent;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IEComponent;
import nl.surf.emergo.domain.IERunCaseComponent;

/**
 * The Class RunCaseComponentManager.
 */
public class RunCaseComponentManager extends AbstractDaoBasedRunCaseComponentManager implements InitializingBean {

	@Override
	public void afterPropertiesSet() throws Exception {
		// NOOP
	}

	@Override
	public IERunCaseComponent getNewRunCaseComponent() {
		return new ERunCaseComponent();
	}

	@Override
	public IERunCaseComponent getRunCaseComponent(int rccId) {
		return runCaseComponentDao.get(rccId);
	}

	@Override
	public IERunCaseComponent getRunCaseComponent(int runId, int cacId) {
		return runCaseComponentDao.get(runId, cacId);
	}

	@Override
	public void saveRunCaseComponent(IERunCaseComponent eRunCaseComponent) {
		runCaseComponentDao.save(eRunCaseComponent);
	}

	@Override
	public List<String[]> newRunCaseComponent(IERunCaseComponent eRunCaseComponent) {
		eRunCaseComponent.setCreationdate(new Date());
		eRunCaseComponent.setLastupdatedate(new Date());
		saveRunCaseComponent(eRunCaseComponent);
		return null;
	}

	@Override
	public List<String[]> updateRunCaseComponent(IERunCaseComponent eRunCaseComponent) {
		eRunCaseComponent.setLastupdatedate(new Date());
		saveRunCaseComponent(eRunCaseComponent);
		return null;
	}

	@Override
	public void deleteRunCaseComponent(int rccId) {
		deleteRunCaseComponent(getRunCaseComponent(rccId));
	}

	@Override
	public void deleteRunCaseComponent(IERunCaseComponent eRunCaseComponent) {
		// first delete possibly related blobs. they are not deleted in cascade.
		deleteBlobs(eRunCaseComponent);
		if (eRunCaseComponent.getLastupdatedate() == null)
			// lastupdatedate field is added later, so value can be empty for older records
			eRunCaseComponent.setLastupdatedate(eRunCaseComponent.getCreationdate());
		runCaseComponentDao.delete(eRunCaseComponent);
	}

	@Override
	public void deleteBlobs(IERunCaseComponent eRunCaseComponent) {
		// delete blobs referenced within xmldata property
		String lXmlDef = "";
		IECaseComponent eCaseComponent = caseComponentManager.getCaseComponent(eRunCaseComponent.getCacCacId());
		if (eCaseComponent != null) {
			IEComponent eComponent = eCaseComponent.getEComponent();
			if (eComponent != null)
				lXmlDef = eComponent.getXmldefinition();
		}
		String lXmlData = eRunCaseComponent.getXmldata();
		xmlManager.deleteBlobs(lXmlDef, lXmlData, AppConstants.status_tag);
	}

	@Override
	public List<IERunCaseComponent> getAllRunCaseComponents() {
		return runCaseComponentDao.getAll();
	}

	@Override
	public List<IERunCaseComponent> getAllRunCaseComponentsByRunId(int runId) {
		return runCaseComponentDao.getAllByRunId(runId);
	}

	@Override
	public List<IERunCaseComponent> getAllRunCaseComponentsByCacId(int cacId) {
		return runCaseComponentDao.getAllByCacId(cacId);
	}

}