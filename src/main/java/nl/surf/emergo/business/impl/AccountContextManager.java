/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;

import nl.surf.emergo.business.abstr.AbstractDaoBasedAccountContextManager;
import nl.surf.emergo.domain.EAccountContext;
import nl.surf.emergo.domain.IEAccountContext;

/**
 * The Class AccountContextManager.
 */
public class AccountContextManager extends AbstractDaoBasedAccountContextManager implements InitializingBean {

	@Override
	public void afterPropertiesSet() throws Exception {
		// NOOP
	}

	@Override
	public IEAccountContext getNewAccountContext() {
		return new EAccountContext();
	}

	@Override
	public IEAccountContext getAccountContext(int accId) {
		return accountContextDao.get(accId);
	}

	@Override
	public void saveAccountContext(IEAccountContext eAccountContext) {
		accountContextDao.save(eAccountContext);
	}

	@Override
	public List<String[]> newAccountContext(IEAccountContext eAccountContext) {
		// Validate item and if ok, save it after setting creationdate and
		// lastupdatedate.
		List<String[]> lErrors = validateAccountContext(eAccountContext);
		if (lErrors == null) {
			eAccountContext.setActive(true);
			eAccountContext.setCreationdate(new Date());
			eAccountContext.setLastupdatedate(new Date());
			saveAccountContext(eAccountContext);
		}
		return lErrors;
	}

	@Override
	public List<String[]> updateAccountContext(IEAccountContext eAccountContext) {
		// Validate item and if ok, save it after setting lastupdatedate.
		List<String[]> lErrors = validateAccountContext(eAccountContext);
		if (lErrors == null) {
			eAccountContext.setLastupdatedate(new Date());
			saveAccountContext(eAccountContext);
		}
		return lErrors;
	}

	@Override
	public List<String[]> validateAccountContext(IEAccountContext eAccountContext) {
		/*
		 * If validation error return it as element in String[] list.
		 */
		List<String[]> lErrors = new ArrayList<>(0);
		return lErrors.isEmpty() ? null : lErrors;
	}

	@Override
	public void deleteAccountContext(int accId) {
		deleteAccountContext(getAccountContext(accId));
	}

	@Override
	public void deleteAccountContext(IEAccountContext eAccountContext) {
		accountContextDao.delete(eAccountContext);
	}

	@Override
	public List<IEAccountContext> getAllAccountContexts() {
		return accountContextDao.getAll();
	}

	@Override
	public List<IEAccountContext> getAllAccountContextsByAccountId(int accId) {
		return accountContextDao.getAllByAccId(accId);
	}

	@Override
	public List<IEAccountContext> getAllAccountContextsByContextId(int conId) {
		return accountContextDao.getAllByConId(conId);
	}

	@Override
	public List<IEAccountContext> getAllAccountContextsByAccountIdActive(int accId, boolean active) {
		return accountContextDao.getAllByAccIdActive(accId, active);
	}

	@Override
	public List<IEAccountContext> getAllAccountContextsByContextIdActive(int conId, boolean active) {
		return accountContextDao.getAllByConIdActive(conId, active);
	}

	@Override
	public List<IEAccountContext> getAllAccountContextsByContextIdsActive(List<Integer> conIds, boolean active) {
		return accountContextDao.getAllByConIdsActive(conIds, active);
	}

	@Override
	public List<Integer> getAccountContextAccIdsByContextIdsActive(List<Integer> conIds, boolean active) {
		List<IEAccountContext> lAccountContexts = getAllAccountContextsByContextIdsActive(conIds, active);
		Hashtable<String, String> lHAccIds = new Hashtable<>();
		List<Integer> lAccIds = new ArrayList<>();
		for (int i = lAccountContexts.size() - 1; i >= 0; i--) {
			int lAccId = lAccountContexts.get(i).getAccAccId();
			if (!lHAccIds.containsKey("" + lAccId)) {
				lAccIds.add(lAccId);
				lHAccIds.put("" + lAccId, "");
			}
		}
		return lAccIds;
	}

	@Override
	public List<Integer> getAccountContextConIdsByAccountIdActive(int accId, boolean active) {
		List<IEAccountContext> lAccountContexts = getAllAccountContextsByAccountIdActive(accId, active);
		List<Integer> lConIds = new ArrayList<>();
		for (int i = lAccountContexts.size() - 1; i >= 0; i--) {
			int lConId = lAccountContexts.get(i).getConConId();
			if (!lConIds.contains(lConId))
				lConIds.add(lConId);
		}
		return lConIds;
	}

}