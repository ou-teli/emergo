package nl.surf.emergo.business.impl;

import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;

public class AccountLockManager implements InitializingBean {
	private static final Logger _log = LogManager.getLogger(AccountLockManager.class);
	private static final ConcurrentHashMap<String, AccountLock> accountLocks = new ConcurrentHashMap<>();
	private int maxAttempts = 6;
	private static final long HOUR_IN_MILLISECONDS = 60 * 60 * 1000;

	@Override
	public void afterPropertiesSet() throws Exception {
		// NOOP
	}

	public boolean isAccountLocked(final String userId) {
		if (StringUtils.isBlank(userId)) {
			return false; // nothing to see here
		}

		AccountLock accountLock = accountLocks.get(userId);

		// never failed
		if (accountLock == null) {
			_log.debug("isAccountLocked: never failed " + accountLock + " " + userId);
			return false;
		}

		// failed attempts within time range
		if ((accountLock.getLastFailedLoginTimestamp() + HOUR_IN_MILLISECONDS) < System.currentTimeMillis()) {
			unlock(userId);
			_log.debug("isAccountLocked: unlock " + accountLock + " " + userId);
			return false;
		}

		// not enough failed attempts
		if (accountLock.getFailedLoginAttempts() < maxAttempts) {
			_log.debug("isAccountLocked: not enough failures " + accountLock + " " + userId);
			return false;
		}

		_log.debug("isAccountLocked: locked " + accountLock + " " + userId);
		return true;
	}

	public void addFailedAttempt(final String userId) {
		AccountLock accountLock = accountLocks.get(userId);

		if (accountLock == null) {
			accountLock = new AccountLock();
			accountLocks.put(userId, accountLock);
		} else {
			accountLock.increment();
		}
		_log.debug("failed " + accountLock + " " + userId);
	}

	public void unlock(final String userId) {
		accountLocks.remove(userId);
		_log.debug("unlock " + userId);
	}

	private class AccountLock {
		private long lastFailedLoginTimestamp;
		private int failedLoginAttempts;

		private AccountLock() {
			increment();
		}

		public void increment() {
			lastFailedLoginTimestamp = System.currentTimeMillis();
			failedLoginAttempts++;
		}

		public long getLastFailedLoginTimestamp() {
			return lastFailedLoginTimestamp;
		}

		public int getFailedLoginAttempts() {
			return failedLoginAttempts;
		}

		@Override
		public String toString() {
			return "AccountLock [lastFailedLoginTimestamp=" + lastFailedLoginTimestamp + ", failedLoginAttempts="
					+ failedLoginAttempts + "]";
		}

	}
}
