/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business.impl;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;
import org.w3c.dom.Document;

import nl.surf.emergo.business.IXMLAttributeValueTime;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.IXMLTree;
import nl.surf.emergo.business.abstr.AbstractDaoBasedXmlManager;

public class XmlManager extends AbstractDaoBasedXmlManager implements InitializingBean {

	protected IXMLTree pXmlTree = new XMLTree();

	@Override
	public void afterPropertiesSet() throws Exception {
		// NOOP
	}

	@Override
	public String escapeXML(String aStr) {
		return pXmlTree.escapeXML(aStr);
	}

	@Override
	public String escapeXMLAlt(String aStr) {
		return pXmlTree.escapeXMLAlt(aStr);
	}

	@Override
	public String unescapeXML(String aStr) {
		return pXmlTree.unescapeXML(aStr);
	}

	@Override
	public String unescapeXMLAlt(String aStr) {
		return pXmlTree.unescapeXMLAlt(aStr);
	}

	@Override
	public boolean isXMLValid(String aXml) {
		return pXmlTree.isXMLValid(aXml);
	}

	@Override
	public IXMLTag newXMLTag(String aName, String aValue) {
		if (aName.equals(""))
			return null;
		IXMLTag xmlTag = new XMLTag();
		xmlTag.setName(aName);
		xmlTag.setValue(aValue);
		xmlTag.setAttributes(new Hashtable<>(0));
		xmlTag.setChildTags(new ArrayList<>(0));
		return xmlTag;
	}

	/**
	 * Gets a xml tag with name aName out of a list of xml tags.
	 * 
	 * @param aXMLList the xml list
	 * @param aName    the name
	 * 
	 * @return the xml tag
	 */
	private IXMLTag getXMLTag(List<IXMLTag> aXMLList, String aName) {
		if ((aXMLList == null) || (aName.equals("")))
			return null;
		for (IXMLTag xmlTag : aXMLList) {
			if (xmlTag.getName().equals(aName))
				return xmlTag;
		}
		return null;
	}

	/**
	 * Gets a xml tag using aNames out of a list of xml tags and its xml childs.
	 * First element of aNames must be found in aXMLList, second element in xml
	 * childs of first one, and so on.
	 * 
	 * @param aXMLList the xml list
	 * @param aNames   the names
	 * 
	 * @return the xml tag
	 */
	private IXMLTag getXMLTag(List<IXMLTag> aXMLList, String[] aNames) {
		List<IXMLTag> lSubList = aXMLList;
		IXMLTag xmlTag = null;
		for (int i = 0; i < aNames.length; i++) {
			String lName = aNames[i];
			xmlTag = getXMLTag(lSubList, lName);
			if (xmlTag == null)
				lSubList = null;
			else
				lSubList = xmlTag.getChildTags();
		}
		return xmlTag;
	}

	@Override
	public IXMLTag getXmlTree(IXMLTag aXmlDefRootTag, String aXmlData, int aTagType) {
		List<IXMLTag> lXmlDefComp = aXmlDefRootTag == null ? null
				: aXmlDefRootTag.getChilds(AppConstants.componentElement);
		List<IXMLTag> lXmlDefCont = aXmlDefRootTag == null ? null
				: aXmlDefRootTag.getChilds(AppConstants.contentElement);
		return getXmlTree(lXmlDefComp, lXmlDefCont, aXmlData, aTagType);
	}

	protected IXMLTag getXmlTree(List<IXMLTag> aXmlDefComp, List<IXMLTag> aXmlDefCont, String aXmlData, int aTagType) {
		// TODO Document may be null in case of XML parse error.
		// This method then will return a root tag without component and content child
		// tags.
		// This will result in a null pointer exception in the calling method.
		// For the moment this is ok, because we now are able to detect a parse error,
		// because a user gets a null pointer exception on the screen.
		// Students for instance sometimes are able to enter special characters (CTRL)
		// or " in a text box, which is normally not possible.
		// Then their XML progress for one case component is not valid anymore and must
		// be repaired by an administrator.
		// The parse error should be communicated to the student and should be properly
		// logged.
		// Now it is logged in method p_getDocument in XMLTree, but then its context is
		// missing, namely which run group and case component are involved.
		// So the logging should not be in this method but in the calling method that
		// knows the context.
		// It absolutely must be prevented that content or progress is overwritten by an
		// empty XML string,
		// which would be the case if this method would return a null value in case of a
		// parse error!
		Document lXmlDataDoc = pXmlTree.getDocument(aXmlData);
		List<IXMLTag> lXmlDataComp = pXmlTree.getXMLListUseDef(aXmlDefComp, lXmlDataDoc, AppConstants.componentElement,
				aTagType);
		List<IXMLTag> lXmlDataCont = pXmlTree.getXMLListUseDef(aXmlDefCont, lXmlDataDoc, AppConstants.contentElement,
				aTagType);
		IXMLTag lRootTag = newXMLTag(AppConstants.rootElement, "");
		List<IXMLTag> lChildTags = new ArrayList<>(0);
		if ((lXmlDataComp != null) && (!lXmlDataComp.isEmpty())) {
			IXMLTag lTag = lXmlDataComp.get(0);
			lTag.setParentTag(lRootTag);
			lChildTags.add(lTag);
		}
		if ((lXmlDataCont != null) && (!lXmlDataCont.isEmpty())) {
			IXMLTag lTag = lXmlDataCont.get(0);
			lTag.setParentTag(lRootTag);
			lChildTags.add(lTag);
		}
		lRootTag.setChildTags(lChildTags);
		return lRootTag;
	}

	protected IXMLTag getStatusChildTag(IXMLTag aDataTag, IXMLTag aStatusChildDefTag) {
		IXMLTag lDataStatusTag = aDataTag.getChild(AppConstants.statusElement);
		if (lDataStatusTag == null) {
			// if no status child in data tag, create one
			lDataStatusTag = newXMLTag(AppConstants.statusElement, "");
			lDataStatusTag.setParentTag(aDataTag);
			lDataStatusTag.setDefTag(aStatusChildDefTag);
			aDataTag.getChildTags().add(lDataStatusTag);
		}
		return lDataStatusTag;
	}

	protected void mergeComponentTags(IXMLTag aDataComponentTag, IXMLTag aStatusComponentTag) {
		if ((aDataComponentTag == null) || (aStatusComponentTag == null)) {
			return;
		}
		aDataComponentTag.setStatusTag(aStatusComponentTag);
		// NOTE Only have to merge attributes, because component tag does not have child
		// tags
		IXMLTag lStatusStatusTag = aStatusComponentTag.getChild(AppConstants.statusElement);
		if (lStatusStatusTag == null) {
			return;
		}
		// if status is set
		getStatusChildTag(aDataComponentTag, lStatusStatusTag.getDefTag());
	}

	protected IXMLTag newUserGeneratedDataTag(IXMLTag aDataContentTag, IXMLTag aStatusTag,
			Hashtable<String, IXMLTag> aDataTagsById, Hashtable<String, IXMLTag> aDataTagsByRefstatusid) {
		// NOTE if the data tag is not created yet, create it
		// NOTE for user generated status tags not always owner_rgaid is set, e.g. for
		// memo tags, so you cannot use it
		// NOTE get targeted parent tag for new data tag
		String lDataParentId = aStatusTag.getAttribute(AppConstants.keyDataparentid);
		String lStatusParentId = aStatusTag.getAttribute(AppConstants.keyStatusparentid);
		if (lDataParentId.equals("") && lStatusParentId.equals("")) {
			// if no parent, set content root (having id 1) as parent, for instance for
			// e-messages coming from others
			lDataParentId = "1";
		}
		IXMLTag lParentTag = null;
		if (!lDataParentId.equals("")) {
			lParentTag = (aDataTagsById == null) ? getTagById(aDataContentTag, lDataParentId)
					: aDataTagsById.get(lDataParentId);
		}
		if (!lStatusParentId.equals("")) {
			if (lStatusParentId.equals("1")) {
				// content root, get it from data root tag
				lParentTag = (aDataTagsById == null) ? getTagById(aDataContentTag, lStatusParentId)
						: aDataTagsById.get(lStatusParentId);
			} else {
				// get it using refstatusid
				// the parent status tag should be converted to a data tag yet
				// because its status tag is added before
				lParentTag = (aDataTagsByRefstatusid == null) ? getTagByRefstatusid(aDataContentTag, lStatusParentId)
						: aDataTagsByRefstatusid.get(lStatusParentId);
			}
		}
		if (lParentTag == null) {
			// no targeted parent tag found, set it to data content tag
			lParentTag = aDataContentTag;
		}
		if (lParentTag == null) {
			// no targeted parent tag found, so do not create data tag
			return null;
		}

		// NOTE create data tag by cloning status tag
		// clone without children because they will be added from status tag
		// because every time a tag value changes this function will be used, max_id of
		// status content root will grow higher
		// then the number of status tags.
		IXMLTag lDataTag = copyTagUnique(aStatusTag, lParentTag, false);
		// set type to data tag because type of aStatusTag is status_tag
		lDataTag.setType(AppConstants.data_tag);
		// set attribute type to node
		lDataTag.setAttribute(AppConstants.defKeyType, AppConstants.defValueNode);
		lDataTag.setParentTag(lParentTag);
		// set attribute extended to distinguish between original data tags and new data
		// tags created out of status tags
		lDataTag.setAttribute(AppConstants.keyExtended, AppConstants.statusValueTrue);

		// NOTE add new data tag within data tag tree
		// databeforeid or statusbeforeid is saved as status tag attribute to be able to
		// position the data tag
		// before tag can be data tag or status tag
		String lDataBeforeId = aStatusTag.getAttribute(AppConstants.keyDatabeforeid);
		String lStatusBeforeId = aStatusTag.getAttribute(AppConstants.keyStatusbeforeid);
		IXMLTag lBeforeTag = null;
		if (!lDataBeforeId.equals("")) {
			lBeforeTag = (aDataTagsById == null) ? getTagById(aDataContentTag, lDataBeforeId)
					: aDataTagsById.get(lDataBeforeId);
		}
		if (!lStatusBeforeId.equals("")) {
			if (lStatusBeforeId.equals("1")) {
				// content root, get it from data root tag
				lBeforeTag = (aDataTagsById == null) ? getTagById(aDataContentTag, lStatusBeforeId)
						: aDataTagsById.get(lStatusBeforeId);
			} else {
				// get it using refstatusid
				// the before status tag should be converted to a data tag yet
				// because its status tag is added before
				lBeforeTag = (aDataTagsByRefstatusid == null) ? getTagByRefstatusid(aDataContentTag, lStatusBeforeId)
						: aDataTagsByRefstatusid.get(lStatusBeforeId);
			}
		}
		if (lBeforeTag == null) {
			// add as child tag
			lParentTag.getChildTags().add(lDataTag);
		} else {
			// add before lBeforeTag
			String lBeforeTagId = lBeforeTag.getAttribute(AppConstants.keyId);
			List<IXMLTag> lNewChildTags = new ArrayList<>(0);
			for (IXMLTag lChildTag : lParentTag.getChildTags()) {
				if (lChildTag.getAttribute(AppConstants.keyId).equals(lBeforeTagId)) {
					lNewChildTags.add(lDataTag);
				}
				lNewChildTags.add(lChildTag);
			}
			lParentTag.setChildTags(lNewChildTags);
		}
		return lDataTag;
	}

	private void addOrOverwriteAttributes(IXMLTag aFromTag, IXMLTag aToTag) {
		// add or overwrite attributes
		Hashtable<String, List<IXMLAttributeValueTime>> lAttributes = aFromTag.getAttributes();
		for (Enumeration<String> keys = lAttributes.keys(); keys.hasMoreElements();) {
			String key = keys.nextElement();
			List<IXMLAttributeValueTime> value = lAttributes.get(key);
			if (value != null) {
				List<IXMLAttributeValueTime> newvalue = new ArrayList<>();
				for (IXMLAttributeValueTime lValueTime : value) {
					newvalue.add(new XMLAttributeValueTime(lValueTime.getValue(), lValueTime.getTime()));
				}
				aToTag.setAttributeAsList(key, newvalue);
			}
		}
	}

	private void mergeStatusAttributesAndStatusChildTags(IXMLTag aStatusTag, IXMLTag aDataTag,
			boolean aUserGeneratedContent) {
		// PERFORMANCE There has to be a method in XMLTag to get a child tag from the
		// status tag child with name status or
		// if null from the data tag itself.
		IXMLTag lStatusStatusTag = aStatusTag.getChild(AppConstants.statusElement);
		if (lStatusStatusTag == null) {
			return;
		}
		// if status is set
		// and use this to save status in stead of refdataid
		IXMLTag lDataStatusTag = getStatusChildTag(aDataTag, lStatusStatusTag.getDefTag());
		// add or overwrite status status child attributes to data status child
		// attributes
		addOrOverwriteAttributes(lStatusStatusTag, lDataStatusTag);
		// enrich data tag with status status tag childs (used in mail)
		// so status status child tag becomes data child tag
		List<IXMLTag> lStatusStatusChildTags = lStatusStatusTag.getChildTags();
		List<IXMLTag> lDataChildTags = aDataTag.getChildTags();
		for (IXMLTag lStatusStatusChildTag : lStatusStatusChildTags) {
			boolean lRgaStatus = (lStatusStatusChildTag.getAttribute(AppConstants.defKeyType).equals("rgastatus"));
			if (lRgaStatus) {
				// add child to child tags of data status tag
				// for shared components each status contribution to a status tag of another
				// (or the same) rungroupaccount is added as child of status status tag
				lDataStatusTag.getChildTags().add(lStatusStatusChildTag);
			} else {
				// check if child already exists within data child tags.
				// if so remove it, that is if it is no node child and attribute multiple is not
				// true
				// there can be more node childs of course.
				// child can be already present if case developer has entered data
				// status will overrule case developer data
				String lName = lStatusStatusChildTag.getName();
				// may remove if child is no node child and not multiple
				boolean lMayRemove = !lStatusStatusChildTag.getDefAttribute(AppConstants.defKeyType)
						.equals(AppConstants.defValueNode)
						&& !lStatusStatusChildTag.getDefAttribute(AppConstants.defKeyMultiple)
								.equals(AppConstants.statusValueTrue);
				for (int k = (lDataChildTags.size() - 1); k >= 0; k--) {
					IXMLTag lDataChildTag = lDataChildTags.get(k);
					if (lDataChildTag.getName().equals(lName) && lMayRemove) {
						// child with lName exists and may be removed
						lDataChildTags.remove(k);
					}
				}
				// add child to data child tags
				lDataChildTags.add(lStatusStatusChildTag);
				// NOTE status tag may not have childs
				if (aUserGeneratedContent) {
					lDataStatusTag.getChildTags().clear();
				}
			}
		}
	}

	protected IXMLTag newDataTag(IXMLTag aStatusTag, IXMLTag aDataTag) {
		// create clone of data tag if version doesn't exist yet
		boolean lHasVersion = !aStatusTag.getChildAttribute(AppConstants.statusElement, AppConstants.statusKeyVersion)
				.equals("");
		if (!lHasVersion) {
			// for instance in case of e-messages data tag is found
			// if status tag has no version info it is status of the original data tag
			// so don't create a new data tag
			return null;
		}
		String lStatusTagId = aStatusTag.getAttribute(AppConstants.keyId);
		String lDataTagId = aDataTag.getAttribute(AppConstants.keyId);
		List<IXMLTag> lChildTags = aDataTag.getParentTag().getChildTags();
		for (IXMLTag lChildTag : lChildTags) {
			if (lChildTag.getName().equals(aStatusTag.getName())
					&& lChildTag.getAttribute(AppConstants.keyRefstatusid).equals(lStatusTagId)) {
				// if exists don't create
				return null;
			}
		}
		IXMLTag lDataTag = copyTagUnique(aDataTag, aDataTag.getParentTag(), false);
		// set reference to original data tag
		lDataTag.setAttribute(AppConstants.keyRefdataid, lDataTagId);
		// and add it in data tree as child of same data tag parent
		lChildTags.add(lDataTag);
		// clear this attribute because it must stay an attribute of data tag only!!!
		lDataTag.setAttribute(AppConstants.keyRefstatusids, "");
		// set attribute extended to distinguish between updated existing data tags or
		// completely new data tags
		lDataTag.setAttribute(AppConstants.keyExtended, AppConstants.statusValueTrue);
		return lDataTag;
	}

	protected void mergeStatusTag(IXMLTag aDataContentTag, IXMLTag aStatusTag, IXMLTag aDataTag,
			Hashtable<String, String> hDataTagIds, Hashtable<String, IXMLTag> aDataTagsById,
			Hashtable<String, IXMLTag> aDataTagsByRefstatusid) {
		String lStatusTagId = aStatusTag.getAttribute(AppConstants.keyId);
		String lDataTagId = "-1";
		boolean lMultipleStatusTags = false;
		if (aDataTag != null) {
			lDataTagId = aDataTag.getAttribute(AppConstants.keyId);
			lMultipleStatusTags = aDataTag.getDefAttribute(AppConstants.defKeyMultiple)
					.equals(AppConstants.statusValueTrue);
		}
		boolean lUserGeneratedContent = aDataTag == null;
		IXMLTag lDataTag = aDataTag;
		boolean lNewDataTagCreated = false;
		if (lUserGeneratedContent) {
			// user generated content, node tag is created by student, for instance in
			// references
			// dataparentid or statusparentid is saved as statustag attribute
			// parent can be data tag or status tag
			lDataTag = (aDataTagsByRefstatusid == null) ? getTagByRefstatusid(aDataContentTag, lDataTagId)
					: aDataTagsByRefstatusid.get(lDataTagId);
			if (lDataTag == null) {
				// data tag corresponding with status tag is not yet created, so create it
				lDataTag = newUserGeneratedDataTag(aDataContentTag, aStatusTag, aDataTagsById, aDataTagsByRefstatusid);
				// NOTE for user generated data tag, still merging is needed otherwise tag is
				// not rendered correctly or cannot be edited rightly
				mergeStatusAttributesAndStatusChildTags(aStatusTag, lDataTag, lUserGeneratedContent);
				lNewDataTagCreated = lDataTag != null;
			}
		} else {
			// check if data tag is already handled
			// if multiple attribute is set there can be multiple status tag instances of
			// the same data tag
			// like for instance for e-messages, alerts and notifications
			// then a new data tag has to be created
			boolean lCreateNewDataTag = hDataTagIds.containsKey(lDataTagId) || lMultipleStatusTags;
			if (!lCreateNewDataTag) {
				// use existing data tag
				hDataTagIds.put(lDataTagId, "");
			} else {
				// create new data tag, so for instance mail tree can be rendered
				IXMLTag lTempDataTag = newDataTag(aStatusTag, aDataTag);
				if (lTempDataTag != null) {
					lDataTag = lTempDataTag;
					lNewDataTagCreated = lDataTag != null;
				}
			}
		}
		if (lDataTag != null) {
			// set reference to status tag in data tag
			lDataTag.setAttribute(AppConstants.keyRefstatusid, lStatusTagId);
			// and use this to save status in stead of refdataid
			if (lNewDataTagCreated) {
				// NOTE add lDataTag so when rendering status tags that are childs of this data
				// tag, the data tag is found
				aDataTagsById.put(lDataTag.getAttribute(AppConstants.keyId), lDataTag);
				aDataTagsByRefstatusid.put(lDataTag.getAttribute(AppConstants.keyRefstatusid), lDataTag);
			}
		}
		if (lMultipleStatusTags && lDataTag != aDataTag) {
			// NOTE if lDataTag==aDataTag ref status id is already set and status attributes
			// are already added in previous if statement
			// update attribute refstatusids in data tag
			String lIds = aDataTag.getAttribute(AppConstants.keyRefstatusids);
			if (!lIds.equals(""))
				lIds = lIds + ",";
			lIds = lIds + lStatusTagId;
			aDataTag.setAttribute(AppConstants.keyRefstatusids, lIds);
			// and use this to save status in stead of refdataid
		}
		if (aDataTag != null) {
			if (lMultipleStatusTags) {
				aDataTag.addStatusTag(aStatusTag);
			} else {
				aDataTag.setStatusTag(aStatusTag);
			}
		}
		if (lDataTag != null) {
			if (lMultipleStatusTags) {
				lDataTag.addStatusTag(aStatusTag);
			} else {
				lDataTag.setStatusTag(aStatusTag);
			}
		}
	}

	protected void mergeContentTag(IXMLTag aDataContentTag, IXMLTag lStatusTag, Hashtable<String, String> hDataTagIds,
			Hashtable<String, IXMLTag> aDataTagsById, Hashtable<String, IXMLTag> aDataTagsByRefstatusid) {
		// status tag mostly has reference to corresponding data tag.
		// data tag is for instance a question but can also be an e-messages tag which
		// functions
		// as a template, user adds some data of its own.
		// reference will be empty for data completely generated by user, such as an
		// uploaded document,
		// so lDataTag will be null

		// get corresponding data tag for status tag
		IXMLTag lDataTag = null;
		String lDataTagId = lStatusTag.getAttribute(AppConstants.keyRefdataid);
		String lRefCacId = lStatusTag.getAttribute(AppConstants.keyRefcacid);
		boolean lCorrespondingDataTagRemovedByAuthor = false;
		if (lDataTagId.equals("") || !lRefCacId.equals(""))
			// no datatagid so user generated content
			// or refcacid so note
			// no node tag has empty id attribute so set to -1!
			lDataTagId = "-1";
		else {
			lDataTag = (aDataTagsById == null) ? getTagById(aDataContentTag, lDataTagId)
					: aDataTagsById.get(lDataTagId);
			if (lDataTag == null)
				// data tag must be found if datatagid is set
				// otherwise data tag is removed by author
				lCorrespondingDataTagRemovedByAuthor = true;
		}
		if (!lCorrespondingDataTagRemovedByAuthor) {
			mergeStatusTag(aDataContentTag, lStatusTag, lDataTag, hDataTagIds, aDataTagsById, aDataTagsByRefstatusid);
		}
	}

	protected void mergeContentTags(IXMLTag aDataContentTag, IXMLTag aStatusContentTag,
			Hashtable<String, IXMLTag> aDataTagsById, Hashtable<String, IXMLTag> aDataTagsByRefstatusid) {
		if ((aDataContentTag == null) || (aStatusContentTag == null)) {
			return;
		}
		// all status tags are saved as childs of root content so there is no hierarchy
		List<IXMLTag> lStatusTags = aStatusContentTag.getChildTags();
		// for keeping which data and status tags are handled
		Hashtable<String, String> hDataTagIds = new Hashtable<String, String>(0);
		Hashtable<String, String> hStatusTagIds = new Hashtable<String, String>(0);
		// loop through status tags to merge them into the data tree
		// new status tags are appended in correct order because lStatusTags is
		// chronological.
		for (IXMLTag lStatusTag : lStatusTags) {
			String lStatusTagId = lStatusTag.getAttribute(AppConstants.keyId);
			if (!hStatusTagIds.containsKey(lStatusTagId)) {
				// check if status tag is already handled
				// NOTE this should not be necessary but leave it this way to be sure
				hStatusTagIds.put(lStatusTagId, "");
				mergeContentTag(aDataContentTag, lStatusTag, hDataTagIds, aDataTagsById, aDataTagsByRefstatusid);
			}
		}
	}

	@Override
	public IXMLTag getNewSentDataTag(IXMLTag aStatusTag, IXMLTag aDataTag) {
		if (aStatusTag == null || aDataTag == null) {
			return null;
		}
		IXMLTag lDataTag = newDataTag(aStatusTag, aDataTag);
		if (lDataTag == null) {
			return null;
		}
		String lStatusTagId = aStatusTag.getAttribute(AppConstants.keyId);
		// set reference to status tag in data tag
		lDataTag.setAttribute(AppConstants.keyRefstatusid, lStatusTagId);
		if (lDataTag != null) {
			lDataTag.addStatusTag(aStatusTag);
		}
		return lDataTag;
	}

	@Override
	public IXMLTag getXmlTree(IXMLTag aDataRootTag, IXMLTag aStatusRootTag, Hashtable<String, IXMLTag> aDataTagsById,
			Hashtable<String, IXMLTag> aDataTagsByRefstatusid) {
		// merge statusdata into data
		if ((aDataRootTag == null) || (aStatusRootTag == null))
			return aDataRootTag;
		// first handle component part of xml data
		IXMLTag lDataComponentTag = aDataRootTag.getChild(AppConstants.componentElement);
		IXMLTag lStatusComponentTag = aStatusRootTag.getChild(AppConstants.componentElement);
		if ((lDataComponentTag != null) && (lStatusComponentTag != null)) {
			mergeComponentTags(lDataComponentTag, lStatusComponentTag);
		}

		// second handle content part of xml data
		// all status tags are saved as childs of root content so there is no hierarchy
		IXMLTag lDataContentTag = aDataRootTag.getChild(AppConstants.contentElement);
		IXMLTag lStatusContentTag = aStatusRootTag.getChild(AppConstants.contentElement);
		if ((lDataContentTag != null) && (lStatusContentTag != null)) {
			mergeContentTags(lDataContentTag, lStatusContentTag, aDataTagsById, aDataTagsByRefstatusid);
		}

		return aDataRootTag;
	}

	@Override
	public IXMLTag getXmlTreeFromImport(String aXMLStr, String aRoot, int aTagType) {
		IXMLTag lTag = null;
		List<IXMLTag> lXmlList = pXmlTree.getXMLList(aXMLStr, aRoot, aTagType);
		if ((lXmlList != null) && (lXmlList.size() > 0)) {
			lTag = lXmlList.get(0);
		}
		return lTag;
	}

	@Override
	public IXMLTag copyTag(IXMLTag aTag, IXMLTag aParent) {
		IXMLTag lClonedTag = aTag.cloneWithoutParentAndChildren();
		lClonedTag.setParentTag(aParent);
		List<IXMLTag> lChildTags = aTag.getChildTags();
		List<IXMLTag> lClonedChildTags = lClonedTag.getChildTags();
		for (IXMLTag lChildTag : lChildTags) {
			IXMLTag lClonedChildTag = copyTag(lChildTag, lClonedTag);
			lClonedChildTags.add(lClonedChildTag);
		}
		lClonedTag.setChildTags(lClonedChildTags);
		return lClonedTag;
	}

	@Override
	public String xmlTreeToDoc(IXMLTag aRootTag) {
		if (aRootTag == null)
			return "";
		List<IXMLTag> lXmlCompList = aRootTag.getChilds(AppConstants.componentElement);
		List<IXMLTag> lXmlContList = aRootTag.getChilds(AppConstants.contentElement);
		return pXmlTree.XMLListsToDoc(lXmlCompList, lXmlContList);
	}

	@Override
	public String xmlTreeToDoc(IXMLTag aRootTag, String aEncoding) {
		if (aRootTag == null)
			return "";
		List<IXMLTag> lXmlCompList = aRootTag.getChilds(AppConstants.componentElement);
		List<IXMLTag> lXmlContList = aRootTag.getChilds(AppConstants.contentElement);
		return pXmlTree.XMLListsToDoc(lXmlCompList, lXmlContList, aEncoding);
	}

	@Override
	public List<Hashtable<String, Object>> getComponentData(IXMLTag aXmlTree) {
		// component part of xml data contains component tags
		List<Hashtable<String, Object>> lList = new ArrayList<Hashtable<String, Object>>(0);
		if (aXmlTree == null) {
			return lList;
		}
		String lRoot = AppConstants.componentElement;
		IXMLTag lXmlTag = aXmlTree.getChild(lRoot);
		IXMLTag lXmlDefTag = lXmlTag.getDefTag();
		if (lXmlDefTag != null) {
			lList = getTagData(lXmlDefTag, false, lXmlTag, lRoot, "sibling");
		}
		return lList;
	}

	@Override
	public void setCompInitialstatus(IXMLTag aRootTag, String aKey, String aValue) {
		// component part of xml data contains only status attributes, no status childs
		if (aRootTag == null)
			return;
		String lRoot = AppConstants.componentElement;
		IXMLTag lTag = aRootTag.getChild(lRoot);
		if (lTag == null)
			return;
		IXMLTag lStatusTag = lTag.getChild(AppConstants.statusElement);
		if (lStatusTag == null) {
			lStatusTag = newXMLTag(AppConstants.statusElement, "");
			lStatusTag.setParentTag(lTag);
			lTag.getChildTags().add(lStatusTag);
		}
		lStatusTag.setAttribute(aKey, aValue);
	}

	@Override
	public IXMLTag getStatusTagStatusChildTag(IXMLTag aDataTag) {
		if (aDataTag.getStatusTag() != null && aDataTag.getStatusTags().size() == 1) {
			return aDataTag.getStatusTag().getChild(AppConstants.statusElement);
		}
		return null;
	}

	@Override
	public String getTagKeyValues(IXMLTag aXmlTag, String aKey) {
		String lValues = "";
		if (aXmlTag == null || aKey == null || aKey.equals(""))
			return lValues;
		// NOTE if a case component allows adjusting a data tag by a student, e.g., for
		// resources component,
		// key values can be overwritten and are stored as childs of the status child
		// tag of the data tag.
		// That is, if a status tag cannot occur multiple times.
		if (aXmlTag.getType() == AppConstants.data_tag) {
			lValues = getTagKeyValues(getStatusTagStatusChildTag(aXmlTag), aKey);
			if (!lValues.equals("")) {
				return lValues;
			}
		}
		String[] lKeys = aKey.split(",");
		for (int i = 0; i < lKeys.length; i++) {
			IXMLTag xmlchild = aXmlTag.getChild(lKeys[i]);
			if (xmlchild != null) {
				if (!(lValues.equals("")))
					lValues = lValues + ":";
				lValues = lValues + unescapeXML(xmlchild.getValue());
			}
		}
		return lValues;
	}

	@Override
	public List<Hashtable<String, Object>> getContentData(IXMLTag aXmlTreeDef, IXMLTag aXmlTree, boolean aNew,
			IXMLTag aClickedNodeTag, String aNodename, String aNewnodetype) {
		// NOTE if aNew aClickedNodeTag is reference tag, so tag that determines where
		// to insert new tag. Content data of definition tag will be returned.
		// Otherwise aClickedNodeTag is tag whose content data should be returned.
		List<Hashtable<String, Object>> lList = new ArrayList<>(0);
		if (aXmlTree == null) {
			return lList;
		}
		IXMLTag lClickedNodeTag = null;
		if (aClickedNodeTag != null) {
			// NOTE get clicked tag by id, clicked tag might be polluted due to authoring,
			// aXmlTree contains not polluted tags
			lClickedNodeTag = getTagById(aXmlTree, aClickedNodeTag.getAttribute(AppConstants.keyId));
		}
		IXMLTag lXmlDefTag = null;
		if (!aNew) {
			// use definition of clicked tag
			lXmlDefTag = lClickedNodeTag.getDefTag();
		} else {
			// use definition found in xml definition tree
			IXMLTag lContentTag = aXmlTreeDef.getChild(AppConstants.contentElement);
			if (lContentTag != null) {
				lXmlDefTag = lContentTag.getChild(aNodename);
			}
		}
		if (lXmlDefTag != null) {
			lList = getTagData(lXmlDefTag, aNew, lClickedNodeTag, aNodename, aNewnodetype);
		}
		return lList;
	}

	protected List<Hashtable<String, Object>> getTagData(IXMLTag aXmlDefTag, boolean aNew, IXMLTag aClickedNodeTag,
			String aNodename, String aNewnodetype) {
		List<Hashtable<String, Object>> lList = new ArrayList<Hashtable<String, Object>>(0);
		// loop through all possible childs of content tag, found in definition tag
		List<IXMLTag> lDefChildTags = aXmlDefTag.getChildTags();
		if (lDefChildTags != null) {
			for (IXMLTag lDefchildtag : lDefChildTags) {
				String lType = lDefchildtag.getAttribute(AppConstants.defKeyType);
				// NOTE only add node child with type set
				if (!lType.equals("") && !lType.equals(AppConstants.defValueNode)) {
					Hashtable<String, Object> lData = new Hashtable<String, Object>(0);
					lData.put("defnodetag", aXmlDefTag);
					lData.put("deftag", lDefchildtag);
					lData.put("type", lType);
					String lName = lDefchildtag.getName();
					lData.put("name", lName);
					lData.put("attribute", "");
					String lValue = "";
					String lInitialValue = AppConstants.statusValueFalse;
					if (aNew) {
						lValue = lDefchildtag.getValue();
						if (!lValue.equals("")) {
							lInitialValue = AppConstants.statusValueTrue;
						}
					} else {
						// NOTE in case of user generated tag or user adjusted tag (in player) search
						// value in status tag
						if (aClickedNodeTag.getType() == AppConstants.data_tag) {
							IXMLTag lStatusTagStatusChildTag = getStatusTagStatusChildTag(aClickedNodeTag);
							if (lStatusTagStatusChildTag != null) {
								lValue = lStatusTagStatusChildTag.getChildValue(lName);
							}
						}
						// NOTE if not found in status tag use value in data tag
						if (lValue.equals("")) {
							lValue = aClickedNodeTag.getChildValue(lName);
						}
					}
					lData.put("value", lValue);
					lData.put("initialvalue", lInitialValue);
					lData.put("private", "" + (lDefchildtag.getAttribute(AppConstants.defKeyPrivate)
							.equals(AppConstants.statusValueTrue)));
					lData.put("hidden", "" + (lDefchildtag.getAttribute(AppConstants.defKeyHidden)
							.equals(AppConstants.statusValueTrue)));
					// NOTE check if content tag already has child that it should have according to
					// the definition tag
					if (aClickedNodeTag != null) {
						IXMLTag lChildtag = null;
						// NOTE only get child tag of existing data tag
						if (!aNew) {
							// NOTE in case of user generated tag or user adjusted tag (in player) get tag
							// from status tag
							if (aClickedNodeTag.getType() == AppConstants.data_tag) {
								IXMLTag lStatusTagStatusChildTag = getStatusTagStatusChildTag(aClickedNodeTag);
								if (lStatusTagStatusChildTag != null) {
									lChildtag = lStatusTagStatusChildTag.getChild(lName);
								}
							}
							// NOTE if not found in status tag use data tag child
							if (lChildtag == null) {
								lChildtag = aClickedNodeTag.getChild(lName);
							}
						}
						// NOTE if not, add it
						if (lChildtag == null) {
							lChildtag = newXMLTag(lName, "");
							lChildtag.setDefTag(lDefchildtag);
							// NOTE only set parent if existing data tag
							if (!aNew) {
								lChildtag.setParentTag(aClickedNodeTag);
								aClickedNodeTag.getChildTags().add(lChildtag);
							}
						}
						lData.put("clickednodetag", aClickedNodeTag);
						lData.put("newnodetype", aNewnodetype);
						lData.put("tag", lChildtag);
					}
					lList.add(lData);
				}
			}
		}
		// loop through all setable attributes of content tag
		String lDefAttributes = "";
		IXMLTag lDefChildTag = aXmlDefTag.getChild(AppConstants.initialstatusElement);
		if (lDefChildTag != null) {
			lDefAttributes = lDefChildTag.getAttribute(AppConstants.defKeyAttributes);
		}
		if (!lDefAttributes.equals("")) {
			String[] lDefAttrs = lDefAttributes.split(",");
			for (int i = 0; i < lDefAttrs.length; i++) {
				String lName = AppConstants.statusElement;
				String lDefAttr = lDefAttrs[i];
				// get default value for attribute
				String lDefaultValue = aXmlDefTag.getChildAttribute(lName, lDefAttr);
				// determine input type for attribute
				String lInputType = "";
				if (lDefaultValue.equals(AppConstants.statusValueFalse)
						|| lDefaultValue.equals(AppConstants.statusValueTrue)) {
					lInputType = "boolean";
				} else {
					lInputType = "line";
				}

				Hashtable<String, Object> lData = new Hashtable<>(0);
				lData.put("defnodetag", aXmlDefTag);
				lData.put("type", lInputType);
				lData.put("name", lName);
				lData.put("attribute", lDefAttr);
				String lValue = "";
				String lInitialValue = "";
				if (lInputType.equals("boolean")) {
					if (aNew) {
						lInitialValue = AppConstants.statusValueFalse;
					} else {
						lInitialValue = lDefaultValue;
					}
				}
				if (aNew) {
					// in case of new tag determine attribute value out of definition
					lDefChildTag = aXmlDefTag.getChild(lName);
					if (lDefChildTag != null) {
						lValue = lDefChildTag.getAttribute(lDefAttr);
						// if opened has initial value and tag is singleopen
						if (lDefAttr.equals(AppConstants.statusKeyOpened) && aXmlDefTag
								.getAttribute(AppConstants.defKeySingleopen).equals(AppConstants.statusValueTrue)
								&& aClickedNodeTag != null) {
							// value depends on existence of siblings
							String lChildNodes = "," + aClickedNodeTag.getDefAttribute(AppConstants.defKeyChildnodes)
									+ ",";
							boolean lChild = (lChildNodes.indexOf("," + aNodename + ",") >= 0);
							// get siblings
							List<IXMLTag> lChildTags = null;
							if (lChild) {
								lChildTags = aClickedNodeTag.getChilds(aNodename);
							} else {
								lChildTags = aClickedNodeTag.getParentTag().getChilds(aNodename);
							}
							// if no siblings, set opened to true
							if (lChildTags.isEmpty()) {
								if (lInputType.equals("boolean")) {
									lValue = AppConstants.statusValueTrue;
								}
							}
						}
					}
					if (!lValue.equals("") && lInputType.equals("boolean")) {
						lInitialValue = AppConstants.statusValueTrue;
					}
				} else {
					if (lName.equals(AppConstants.statusElement)) {
						// NOTE attribute can be status attribute, meaning a comma separated string of
						// time/value pairs,
						// so use getCurrentAttribute
						lValue = aClickedNodeTag.getCurrentStatusAttribute(lDefAttr);
					} else {
						lValue = aClickedNodeTag.getChildAttribute(lName, lDefAttr);
					}
				}
				if (lInputType.equals("boolean") && lValue.equals("")) {
					lValue = lInitialValue;
				}
				lData.put("value", lValue);
				lData.put("initialvalue", lInitialValue);
				lData.put("private", AppConstants.statusValueFalse);
				lData.put("hidden", AppConstants.statusValueFalse);
				lList.add(lData);
			}
		}
		return lList;
	}

	@Override
	public IXMLTag newNodeTag(String aXmlDef, String aNodename) {
		String lRoot = AppConstants.contentElement;
		List<IXMLTag> lXmlContList = pXmlTree.getXMLList(aXmlDef, lRoot, AppConstants.data_tag);
		if (lXmlContList == null)
			return null;
		IXMLTag xmlDefTag = getXMLTag(lXmlContList, new String[] { lRoot, aNodename });
		return xmlTagDefToNodeTag(xmlDefTag);
	}

	private IXMLTag xmlTagDefToNodeTag(IXMLTag aDefTag) {
		if (aDefTag == null)
			return null;
		IXMLTag xmlTag = newXMLTag(aDefTag.getName(), aDefTag.getValue());
		xmlTag.setDefTag(aDefTag);
		xmlTag.setAttribute(AppConstants.defKeyType, aDefTag.getAttribute(AppConstants.defKeyType));
		List<IXMLTag> defTagChilds = aDefTag.getChildTags();
		if (defTagChilds == null)
			return xmlTag;
		List<IXMLTag> childtags = new ArrayList<IXMLTag>(0);
		for (IXMLTag defTagChild : defTagChilds) {
			boolean lOmit = false;
			for (String childTagName : AppConstants.nodeChildTagsToOmit) {
				if (defTagChild.getName().equals(childTagName)) {
					lOmit = true;
					break;
				}
			}
			if (!lOmit) {
				IXMLTag lChild = newXMLTag(defTagChild.getName(), defTagChild.getValue());
				lChild.setDefTag(defTagChild);
				childtags.add(lChild);
			}
		}
		xmlTag.setChildTags(childtags);
		return xmlTag;
	}

	@Override
	public void newSiblingNode(IXMLTag aRootTag, IXMLTag aTag, IXMLTag aBeforeTag, List<String[]> aErrors) {
		newTagBySibling(aRootTag, aTag, aBeforeTag, aErrors);
		if (aErrors.size() > 0)
			return;
		setTagId(aRootTag, aTag);
	}

	@Override
	public void newChildNodeSimple(IXMLTag aRootTag, IXMLTag aTag, IXMLTag aParentTag) {
		newTagByParentSimple(aRootTag, aTag, aParentTag);
		setTagId(aRootTag, aTag);
	}

	@Override
	public void newChildNode(IXMLTag aRootTag, IXMLTag aTag, IXMLTag aParentTag, List<String[]> aErrors) {
		newTagByParent(aRootTag, aTag, aParentTag, aErrors);
		if (aErrors.size() == 0)
			setTagId(aRootTag, aTag);
	}

	/**
	 * Sets tag attribute id for tag aTag using xml root tag.
	 * 
	 * @param aRootTag the root tag
	 * @param aTag     the tag
	 */
	private void setTagId(IXMLTag aRootTag, IXMLTag aTag) {
		if (aRootTag == null)
			return;
		IXMLTag lContentTag = aRootTag.getChild(AppConstants.contentElement);
		if (lContentTag == null)
			return;
		// get max_id, is attribute of content tag
		String max_id = lContentTag.getAttribute(AppConstants.keyMax_id);
		if (max_id.equals(""))
			max_id = "0";
		// inc max_id
		max_id = "" + (Integer.parseInt(max_id) + 1);
		// set tag id to max_id
		aTag.setAttribute(AppConstants.keyId, max_id);
		// save new value of max_id in content tag
		lContentTag.setAttribute(AppConstants.keyMax_id, max_id);
	}

	@Override
	public void setTagId(IXMLTag aTag) {
		IXMLTag lRootTag = aTag;
		while (lRootTag.getParentTag() != null)
			lRootTag = lRootTag.getParentTag();
		setTagId(lRootTag, aTag);
	}

	@Override
	public void replaceNodeSimple(IXMLTag aRootTag, IXMLTag aTag) {
		String lId = aTag.getAttribute(AppConstants.keyId);
		replaceTagByIdSimple(aRootTag, aTag, lId);
	}

	@Override
	public void replaceNode(IXMLTag aRootTag, IXMLTag aTag, List<String[]> aErrors) {
		String lId = aTag.getAttribute(AppConstants.keyId);
		replaceTagById(aRootTag, aTag, lId, aErrors);
	}

	@Override
	public void deleteNode(IXMLTag aRootTag, IXMLTag aTag) {
		String lId = aTag.getAttribute(AppConstants.keyId);
		deleteTagById(aRootTag, lId);
	}

	@Override
	public void dropNode(IXMLTag aRootTag, IXMLTag aDraggedTag, IXMLTag aDroppedTag, boolean aCopy, String aDropType,
			List<IXMLTag> aCopiedTags, List<String> aDropTypes, List<String[]> aErrors) {
		String lDraggedId = aDraggedTag.getAttribute(AppConstants.keyId);
		String lDroppedId = aDroppedTag.getAttribute(AppConstants.keyId);
		dropTagByIds(aRootTag, lDraggedId, lDroppedId, aCopy, aDropType, aCopiedTags, aDropTypes, aErrors);
	}

	@Override
	public void deleteBlobs(String aXmlDef, String aXmlData, int aTagType) {
		if ((aXmlDef == null) || (aXmlDef.equals("")))
			return;
		if ((aXmlData == null) || (aXmlData.equals("")))
			return;
		String lContentTagName = AppConstants.contentElement;
		List<IXMLTag> lXmlContDefList = pXmlTree.getXMLList(aXmlDef, lContentTagName, AppConstants.definition_tag);
		List<IXMLTag> lXmlContList = pXmlTree.getXMLListUseDef(lXmlContDefList, aXmlData, lContentTagName, aTagType);
		IXMLTag lContentTag = getXMLTag(lXmlContList, new String[] { lContentTagName });
		if (lContentTag != null) {
			deleteBlobs(lContentTag);
		}
	}

	/**
	 * Adds tag aTag as sibling.
	 * 
	 * @param aParentTag   the parent tag
	 * @param aTag         the tag
	 * @param aBeforeTagId the tag id of the sibling
	 * 
	 * @return true, if successful
	 */
	private boolean addAsSibling(IXMLTag aParentTag, IXMLTag aTag, String aBeforeTagId) {
		if (aParentTag != null) {
			aTag.setParentTag(aParentTag);
			List<IXMLTag> xmlchilds = aParentTag.getChildTags();
			List<IXMLTag> newxmlchilds = new ArrayList<IXMLTag>(0);
			for (IXMLTag xmlchild : xmlchilds) {
				if (xmlchild.getAttribute(AppConstants.keyId).equals(aBeforeTagId))
					newxmlchilds.add(aTag);
				newxmlchilds.add(xmlchild);
			}
			aParentTag.setChildTags(newxmlchilds);
			return true;
		}
		return false;
	}

	/**
	 * Adds tag aTag as child.
	 * 
	 * @param aParentTag the parent tag
	 * @param aTag       the tag
	 * 
	 * @return true, if successful
	 */
	private boolean addAsChild(IXMLTag aParentTag, IXMLTag aTag) {
		if (aParentTag != null) {
			aTag.setParentTag(aParentTag);
			List<IXMLTag> xmlchilds = aParentTag.getChildTags();
			if (xmlchilds == null) {
				xmlchilds = new ArrayList<IXMLTag>(0);
				aParentTag.setChildTags(xmlchilds);
			}
			xmlchilds.add(aTag);
			return true;
		}
		return false;
	}

	/**
	 * Creates new tag by sibling.
	 * 
	 * @param aRootTag   the root tag
	 * @param aTag       the tag
	 * @param aBeforeTag the before tag
	 * @param aErrors    used to return the error list
	 * 
	 * @return true, if successful
	 */
	private boolean newTagBySibling(IXMLTag aRootTag, IXMLTag aTag, IXMLTag aBeforeTag, List<String[]> aErrors) {
		if (aRootTag == null)
			return false;
		IXMLTag lContentTag = aRootTag.getChild(AppConstants.contentElement);
		if (lContentTag == null)
			return false;
		if (aBeforeTag == null)
			return newTagByParent(aRootTag, aTag, null, aErrors);
		String lKey = aTag.getDefAttribute(AppConstants.defKeyKey);
		String lBeforeTagId = aBeforeTag.getAttribute(AppConstants.keyId);
		aBeforeTag = getTagById(lContentTag, lBeforeTagId);
		IXMLTag parentTag = aBeforeTag.getParentTag();
		List<String> lWarnings = new ArrayList<String>(0);
		if (checkTagChildTagValues(lKey, aTag, parentTag, true, false, aErrors, lWarnings))
			return false;
		handleSingleopen(aTag, parentTag);
		if (addAsSibling(parentTag, aTag, lBeforeTagId))
			return true;
		return false;
	}

	/**
	 * Creates new tag by parent without checking for errors.
	 * 
	 * @param aRootTag   the root tag
	 * @param aTag       the tag
	 * @param aParentTag the parent tag
	 * 
	 * @return true, if successful
	 */
	private boolean newTagByParentSimple(IXMLTag aRootTag, IXMLTag aTag, IXMLTag aParentTag) {
		if (aRootTag == null)
			return false;
		IXMLTag lContentTag = aRootTag.getChild(AppConstants.contentElement);
		if (lContentTag == null)
			return false;
		IXMLTag parentTag = null;
		if (aParentTag == null)
			parentTag = lContentTag;
		else
			parentTag = getTagById(lContentTag, aParentTag.getAttribute(AppConstants.keyId));
		if (addAsChild(parentTag, aTag))
			return true;
		return false;
	}

	/**
	 * Creates new tag by parent.
	 * 
	 * @param aRootTag   the root tag
	 * @param aTag       the tag
	 * @param aParentTag the parent tag
	 * @param aErrors    used to return the error list
	 * 
	 * @return true, if successful
	 */
	private boolean newTagByParent(IXMLTag aRootTag, IXMLTag aTag, IXMLTag aParentTag, List<String[]> aErrors) {
		if (aRootTag == null)
			return false;
		IXMLTag lContentTag = aRootTag.getChild(AppConstants.contentElement);
		if (lContentTag == null)
			return false;
		String lKey = aTag.getDefAttribute(AppConstants.defKeyKey);
		IXMLTag parentTag = null;
		if (aParentTag == null)
			parentTag = lContentTag;
		else
			parentTag = getTagById(lContentTag, aParentTag.getAttribute(AppConstants.keyId));
		List<String> lWarnings = new ArrayList<String>(0);
		if (checkTagChildTagValues(lKey, aTag, parentTag, true, false, aErrors, lWarnings))
			return false;
		handleSingleopen(aTag, parentTag);
		if (addAsChild(parentTag, aTag))
			return true;
		return false;
	}

	/**
	 * Replaces tag by id without checking for errors.
	 * 
	 * @param aRootTag the root tag
	 * @param aTag     the tag
	 * @param aId      the id
	 * 
	 * @return true, if successful
	 */
	private boolean replaceTagByIdSimple(IXMLTag aRootTag, IXMLTag aTag, String aId) {
		if (aRootTag == null)
			return false;
		IXMLTag lContentTag = aRootTag.getChild(AppConstants.contentElement);
		if (lContentTag == null)
			return false;
		IXMLTag xmltag = getTagById(lContentTag, aId);
		if (xmltag != null) {
			xmltag.setValue(aTag.getValue());
			xmltag.setAttributes(aTag.getAttributes());
			if (aTag.getParentTag() != null)
				xmltag.setParentTag(aTag.getParentTag());
			xmltag.setChildTags(aTag.getChildTags());
		}
		return true;
	}

	/**
	 * Replaces tag by id.
	 * 
	 * @param aRootTag the root tag
	 * @param aTag     the tag
	 * @param aId      the id
	 * @param aErrors  used to return the error list
	 * 
	 * @return true, if successful
	 */
	private boolean replaceTagById(IXMLTag aRootTag, IXMLTag aTag, String aId, List<String[]> aErrors) {
		if (aRootTag == null)
			return false;
		IXMLTag lContentTag = aRootTag.getChild(AppConstants.contentElement);
		if (lContentTag == null)
			return false;
		String lKey = aTag.getDefAttribute(AppConstants.defKeyKey);
		IXMLTag xmltag = getTagById(lContentTag, aId);
		IXMLTag parentTag = xmltag.getParentTag();
		List<String> lWarnings = new ArrayList<String>(0);
		if (checkTagChildTagValues(lKey, aTag, parentTag, false, false, aErrors, lWarnings))
			return false;
		handleSingleopen(aTag, parentTag);
		return replaceTagByIdSimple(aRootTag, aTag, aId);
	}

	/**
	 * Gets the tag child. Checks if there is a child with name status. If so it
	 * checks if the status child has a child with name aChildName. If so it returns
	 * it. If not it returns child with name aChildName of aTag.
	 * 
	 * @param aTag       the tag
	 * @param aChildName the child name
	 * 
	 * @return the tag child
	 */
	private IXMLTag getTagChild(IXMLTag aTag, String aChildName) {
		// Child tag can be child of aTag, child of status child tag of aTag or child of
		// the status tag that corresponds with aTag.
		if (aTag == null)
			return null;
		// Status overrules data so first check if child tag exists in status tag
		// reference.
		IXMLTag lStatusTagStatusChildTag = getStatusTagStatusChildTag(aTag);
		if (lStatusTagStatusChildTag != null) {
			IXMLTag lChildTag = lStatusTagStatusChildTag.getChild(aChildName);
			if (lChildTag != null) {
				return lChildTag;
			}
		}
		// If no child tag in status tag reference, search child in status child tag of
		// aTag
		IXMLTag lStatusChildTag = aTag.getChild(AppConstants.statusElement);
		if (lStatusChildTag != null) {
			IXMLTag lChildTag = lStatusChildTag.getChild(aChildName);
			if (lChildTag != null) {
				return lChildTag;
			}
		}
		// Otherwise return aTag child tag.
		return aTag.getChild(aChildName);
	}

	@Override
	public boolean checkTagChildTagValues(String aKey, IXMLTag aTag, IXMLTag aParentTag, boolean aNew, boolean aCopy,
			List<String[]> aErrors, List<String> aWarnings) {
		// tag key empty?
		if (aKey.equals("")) {
			return false;
		}
		Hashtable<String, String> lChildTagValues = new Hashtable<>();
		List<IXMLTag> lChildTags = aTag.getChildTags();
		if (lChildTags != null) {
			// TODO if aTag is a status tag it has only one child, the status child tag.
			// Its childs have to be checked then
			for (IXMLTag lChildTag : lChildTags) {
				// NOTE use method getTagChild so method tagErrors also works fine for user
				// generated content
				lChildTag = getTagChild(aTag, lChildTag.getName());
				// if non node child
				if (lChildTag != null
						&& !lChildTag.getAttribute(AppConstants.defKeyType).equals(AppConstants.defValueNode)) {
					lChildTagValues.put(lChildTag.getName(), lChildTag.getValue());
				}
			}
		}
		return checkChildTagValues(aKey, aTag.getAttribute(AppConstants.keyId), aTag.getName(), lChildTagValues,
				aTag.getDefTag(), aParentTag, aNew, aCopy, aErrors, aWarnings);
	}

	@Override
	public boolean checkChildTagValues(String aKey, String aTagId, String aTagName,
			Hashtable<String, String> aChildTagValues, IXMLTag aDefTag, IXMLTag aParentTag, boolean aNew, boolean aCopy,
			List<String[]> aErrors, List<String> aWarnings) {
		// tag key empty?
		if (aKey.equals("")) {
			return false;
		}
		// Check if tag key values given by aKey are not empty within aTagChildValues.
		// NOTE a tag key can be composed of multiple child tag names separated by a
		// comma
		String[] lKeyTagNames = aKey.split(",");
		boolean lHasEmptyValue = false;
		// lTagNameStr is needed to compose error string
		String lTagNameStr = "";
		for (int i = 0; i < lKeyTagNames.length; i++) {
			if (!aChildTagValues.containsKey(lKeyTagNames[i]) || aChildTagValues.get(lKeyTagNames[i]).equals("")) {
				lHasEmptyValue = true;
			}
			if (!lTagNameStr.equals("")) {
				lTagNameStr += "_";
			}
			lTagNameStr += lKeyTagNames[i];
		}
		// If one or more key values are empty
		if (lHasEmptyValue) {
			appManager.addError(aErrors, lTagNameStr, "error_empty");
		}
		// Check if tag key values given by aKey are unique for siblings of aTag.
		boolean lNotUnique = false;
		List<IXMLTag> lSiblingNodeTags = null;
		// Get all node childs of parent
		if (aParentTag != null) {
			lSiblingNodeTags = aParentTag.getChildTags(AppConstants.defValueNode);
		}
		// lFirstKeyValues is needed to be able to change the first key value, if key
		// values are not unique within siblings and tag is copied (by drag and drop)
		String lFirstKeyValues = "@";
		if (lSiblingNodeTags != null) {
			for (IXMLTag lSiblingNodeTag : lSiblingNodeTags) {
				// NOTE also check sibling node tag, because tag key values in aTagChildValues
				// can be changed
				// only check node tags with same name
				// if status deleted is true then skip node tag
				// NOTE if status present is false then do not skip node tag, because present
				// may become true by script action
				if (lSiblingNodeTag.getName().equals(aTagName) && !getStatusIsDeleted(lSiblingNodeTag)) {
					boolean lAreValuesEqual = true;
					IXMLTag lSiblingNodeTagChild = null;
					for (int j = 0; j < lKeyTagNames.length; j++) {
						lSiblingNodeTagChild = getTagChild(lSiblingNodeTag, lKeyTagNames[j]);
						// if both sibling child and tag child value exist ...
						if (lSiblingNodeTagChild != null && aChildTagValues.containsKey(lKeyTagNames[j])) {
							// and are unequal
							if (!aChildTagValues.get(lKeyTagNames[j]).equals(lSiblingNodeTagChild.getValue())) {
								lAreValuesEqual = false;
							}
							if (j == 0) {
								lFirstKeyValues += lSiblingNodeTagChild.getValue() + "@";
							}
						}
					}
					// if key tag child values are equal to the values of one of his siblings or
					// itself
					if (lAreValuesEqual) {
						// key values of a new tag must be unique
						if (aNew) {
							lNotUnique = true;
						}
						// key values of a copied tag must be unique or
						// key values of aTag may not be equal to the values of one of his siblings
						else if (aCopy || !lSiblingNodeTag.getAttribute(AppConstants.keyId).equals(aTagId)) {
							lNotUnique = true;
						}
					}
				}
			}
		}
		// If key values are not unique within siblings
		if (lNotUnique) {
			// In case of copy tag key values are not unique, so give alternative first key
			// value back within warnings.
			if (aCopy) {
				if (aChildTagValues.containsKey(lKeyTagNames[0])) {
					String lValue = aChildTagValues.get(lKeyTagNames[0]);
					int lCounter = 1;
					String lAltName = "copy (" + lCounter + ") of " + lValue;
					while (lFirstKeyValues.indexOf("@" + lAltName + "@") >= 0) {
						lCounter = lCounter + 1;
						lAltName = "copy (" + lCounter + ") of " + lValue;
					}
					aWarnings.add(lAltName);
				}
			}
			// NOTE if aCopy is false except same key values: is author's responsibility.
			// Problem arose with user (student) generated content. If author defines
			// content tag not present
			// a student could not add content with the same key values.
			/*
			 * else { appManager.addError(aErrors, lPropKey, "error_not_unique"); }
			 */
		}
		// Data child tags can have attribute notempty set to true, meaning value must
		// be non empty.
		// For instance for the lastname field of an account. So check this.
		if (aDefTag != null) {
			for (Enumeration<String> lNames = aChildTagValues.keys(); lNames.hasMoreElements();) {
				String lName = lNames.nextElement();
				IXMLTag lDefTagChild = aDefTag.getChild(lName);
				boolean lMayNotBeEmpty = lDefTagChild != null
						&& lDefTagChild.getAttribute(AppConstants.defKeyNotempty).equals(AppConstants.statusValueTrue);
				if (lMayNotBeEmpty && aChildTagValues.get(lName).equals("")) {
					appManager.addError(aErrors, lName, "error_empty");
				}
			}
		}
		return (aErrors.size() > 0);
	}

	@Override
	public void handleSingleopen(IXMLTag aTag, IXMLTag aParentTag) {
		boolean lSingleopen = (aTag.getDefAttribute(AppConstants.defKeySingleopen)
				.equals(AppConstants.statusValueTrue));
		if (!lSingleopen)
			return;
		if (!getStatusIsOpened(aTag))
			return;
		List<IXMLTag> lXmlChilds = aParentTag.getChildTags();
		for (IXMLTag lXmlChild : lXmlChilds) {
			if ((lXmlChild.getName().equals(aTag.getName())) && (getStatusIsOpened(lXmlChild)))
				setStatusOpened(lXmlChild, aTag, false);
		}
	}

	@Override
	public void handleSingleopenForDrop(IXMLTag aDraggedTag, IXMLTag aDroppedParentTag, IXMLTag aCopyOfDraggedTag) {
		// note that if aCopyOfDraggedTag is not null, its opened status is equal to the
		// opened status of aDraggedTag, because it is a copy.
		// several conditions may occur.
		// first if dragged tag has opened status false do nothing.
		// second if dragged and dropped tag originally have same parent (so dragging of
		// sibling):
		// - the opened status of both dragged and dropped tag remains unaffected
		// because they are siblings
		// - but if aCopyOfDraggedTag is not null, set the opened status of it to false
		// third if dragged and dropped tag originally have different parents:
		// - if the opened status of aDroppedTag or one of its siblings is true:
		// - - if aCopyOfDraggedTag is null, opened of aDraggedTag is set to false
		// - - if aCopyOfDraggedTag is not null, opened of aCopyOfDraggedTag is set to
		// false
		// - otherwise the opened status of both dragged, dropped and copyofdragged tag
		// remains unaffected
		boolean lSingleopen = (aDraggedTag.getDefAttribute(AppConstants.defKeySingleopen)
				.equals(AppConstants.statusValueTrue));
		if (!lSingleopen)
			return;
		if (!getStatusIsOpened(aDraggedTag))
			return;
		boolean parentsAreEqual = (aDraggedTag.getParentTag() == aDroppedParentTag);
		if (parentsAreEqual) {
			if (aCopyOfDraggedTag != null) {
				setStatusOpened(aCopyOfDraggedTag, aDraggedTag, false);
			}
		} else {
			boolean lIsOneOfDroppedSiblingsOpened = false;
			List<IXMLTag> lXmlChilds = aDroppedParentTag.getChildTags();
			for (IXMLTag lXmlChild : lXmlChilds) {
				if ((lXmlChild.getName().equals(aDraggedTag.getName())) && (getStatusIsOpened(lXmlChild)))
					lIsOneOfDroppedSiblingsOpened = true;
			}
			if (lIsOneOfDroppedSiblingsOpened) {
				if (aCopyOfDraggedTag == null) {
					setStatusOpened(aDraggedTag, aDraggedTag, false);
				} else {
					setStatusOpened(aCopyOfDraggedTag, aDraggedTag, false);
				}
			}
		}
	}

	/**
	 * Gets the current status value.
	 * 
	 * @param aTag the tag
	 * @param aKey the attribute key
	 * 
	 * @return the current status value
	 */
	private String getCurrentStatusValue(IXMLTag aTag, String aKey) {
		String lStatusKeyStr = aTag.getChildAttribute(AppConstants.statusElement, aKey);
		if (lStatusKeyStr.equals(""))
			return "";
		String[] lStatusKeyArr = lStatusKeyStr.split(",");
		// length is 1 for pre definied status in data, but for status within
		// status it is an array
		if (lStatusKeyArr.length == 1)
			return lStatusKeyArr[0];
		else
			return lStatusKeyArr[lStatusKeyArr.length - 2];
	}

	/**
	 * Sets current status value. aTimeTag contains the time the status has to be
	 * set. Because XmlManager does not know the time it has to extracted from
	 * aTimeTag. aTimeTag is a Tag which status was set before. Has to do with
	 * method handleSingleOpen.
	 * 
	 * @param aTag     the tag
	 * @param aTimeTag the time tag
	 * @param aKey     the attribute key
	 * @param aValue   the attribute value
	 */
	private void setCurrentStatusValue(IXMLTag aTag, IXMLTag aTimeTag, String aKey, String aValue) {
		// determine statuskeytime
		String lStatusKeyStr = aTimeTag.getChildAttribute(AppConstants.statusElement, aKey);
		if (lStatusKeyStr.equals(""))
			return;
		String[] lStatusKeyArr = lStatusKeyStr.split(",");
		String lStatusKeyTime = "";
		// length is 1 for pre definied status in data, but for status within
		// status it is an array
		if (lStatusKeyArr.length > 1)
			lStatusKeyTime = lStatusKeyArr[lStatusKeyArr.length - 1];
		// set value
		IXMLTag lStatusTag = aTag.getChild(AppConstants.statusElement);
		if (lStatusTag == null) {
			// add status tag
			lStatusTag = newXMLTag(AppConstants.statusElement, "");
			lStatusTag.setParentTag(aTag);
			List<IXMLTag> lChildTags = aTag.getChildTags();
			lChildTags.add(lStatusTag);
		}
		if (lStatusKeyTime.equals(""))
			// replace value
			lStatusTag.setAttribute(aKey, aValue);
		else {
			// add value and time
			lStatusTag.addAttributeValueTimeToList(aKey, aValue, Double.parseDouble(lStatusKeyTime));
		}
	}

	/**
	 * Is status opened of aTag true. Has to do with method handleSingleOpen.
	 * 
	 * @param aTag the tag
	 * 
	 * @return true, if status opened is true
	 */
	private boolean getStatusIsOpened(IXMLTag aTag) {
		String lOpenedValue = getCurrentStatusValue(aTag, AppConstants.statusKeyOpened);
		// return true so status will be set false in handleSingleopen
		if (lOpenedValue.equals(""))
			return true;
		return (lOpenedValue.equals(AppConstants.statusValueTrue));
	}

	/**
	 * Sets status opened of aTag. aTimeTag contains the time the status has to be
	 * set. Because XmlManager does not know the time it has to extracted from
	 * aTimeTag. aTimeTag is a Tag which status was set before. Has to do with
	 * method handleSingleOpen.
	 * 
	 * @param aTag     the tag
	 * @param aTimeTag the time tag
	 * @param aOpened  the opened value
	 */
	private void setStatusOpened(IXMLTag aTag, IXMLTag aTimeTag, boolean aOpened) {
		setCurrentStatusValue(aTag, aTimeTag, AppConstants.statusKeyOpened, "" + aOpened);
	}

	/**
	 * Is status deleted of aTag true. Has to do with method tagErrors.
	 * 
	 * @param aTag the tag
	 * 
	 * @return true, if status deleted is true
	 */
	private boolean getStatusIsDeleted(IXMLTag aTag) {
		String lDeletedValue = getCurrentStatusValue(aTag, AppConstants.statusKeyDeleted);
		if (lDeletedValue.equals(""))
			return false;
		return (lDeletedValue.equals(AppConstants.statusValueTrue));
	}

	/**
	 * Deletes tag by id.
	 * 
	 * @param aXmlList the xml list
	 * @param aId      the id
	 * 
	 * @return true, if successful
	 */
	private boolean deleteTagById(List<IXMLTag> aXmlList, String aId) {
		if ((aXmlList == null) || (aXmlList.size() == 0))
			return false;
		for (int i = (aXmlList.size() - 1); i >= 0; i--) {
			IXMLTag xmltag = aXmlList.get(i);
			if (xmltag.getAttribute(AppConstants.keyId).equals(aId)) {
				deleteBlobs(xmltag);
				aXmlList.remove(xmltag);
				return true;
			}
			if (deleteTagById(xmltag.getChildTags(), aId))
				return true;
		}
		return false;
	}

	/**
	 * Deletes tag by id.
	 * 
	 * @param aRootTag the root tag
	 * @param aId      the id
	 * 
	 * @return true, if successful
	 */
	private boolean deleteTagById(IXMLTag aRootTag, String aId) {
		if (aRootTag == null)
			return false;
		IXMLTag lContentTag = aRootTag.getChild(AppConstants.contentElement);
		if (lContentTag == null)
			return false;
		return deleteTagById(lContentTag.getChildTags(), aId);
	}

	/**
	 * Drops tag by ids. Tag is copied or moved depending on aCopy.
	 * 
	 * @param aXmlList      the xml list
	 * @param aDraggedTagId the dragged tag id
	 * @param aDroppedTagId the dropped tag id
	 * @param aCopy         true/false
	 * @param aDropType     drop as child or as sibling
	 * @param aCopiedTags   used to return the copied tags
	 * @param aDropTypes    used to return the drop types
	 * @param aErrors       used to return the error list
	 * 
	 * @return true, if successful
	 */
	private boolean dropTagByIds(List<IXMLTag> aXmlList, String aDraggedTagId, String aDroppedTagId, boolean aCopy,
			String aDropType, List<IXMLTag> aCopiedTags, List<String> aDropTypes, List<String[]> aErrors) {
		if ((aXmlList == null) || (aXmlList.size() == 0)) {
			aDropTypes.add("" + AppConstants.noDrop);
			return false;
		}
		IXMLTag lDraggedTag = getTagById(aXmlList, aDraggedTagId);
		IXMLTag lDroppedTag = getTagById(aXmlList, aDroppedTagId);
		String lDropType = "";
		if (aDropType.equals(""))
			lDropType = getDropType(lDraggedTag, lDroppedTag, aErrors);
		else
			lDropType = aDropType;
		if ((lDropType.equals("" + AppConstants.noDrop)) || (lDropType.equals("" + AppConstants.siblingOrChildDrop))) {
			aDropTypes.add(lDropType);
			return false;
		}
		String lKey = lDraggedTag.getDefAttribute(AppConstants.defKeyKey);
		IXMLTag lDraggedParentTag = lDraggedTag.getParentTag();
		IXMLTag lDroppedParentTag = null;
		if (lDropType.equals("" + AppConstants.siblingDrop))
			lDroppedParentTag = lDroppedTag.getParentTag();
		else
			lDroppedParentTag = lDroppedTag;
		List<String> lWarnings = new ArrayList<String>(0);
		if (checkTagChildTagValues(lKey, lDraggedTag, lDroppedParentTag, false, aCopy, aErrors, lWarnings)) {
			aDropTypes.add(lDropType);
			return false;
		}
		IXMLTag lCopyOfDraggedTag = null;
		if (aCopy) {
			// copy
			lCopyOfDraggedTag = copyTagUnique(lDraggedTag, lDroppedParentTag, true);
			if (lWarnings.size() > 0) {
				String[] lKeys = lKey.split(",");
				String lFirstKey = lWarnings.get(0);
				(lCopyOfDraggedTag.getChild(lKeys[0])).setValue(lFirstKey);
			}
			aCopiedTags.add(lCopyOfDraggedTag);
		} else {
			// move
			// remove DraggedTag from DraggedParentTag
			List<IXMLTag> draggedxmlchilds = lDraggedParentTag.getChildTags();
			for (int i = (draggedxmlchilds.size() - 1); i >= 0; i--) {
				IXMLTag xmlchild = draggedxmlchilds.get(i);
				if (xmlchild.getAttribute(AppConstants.keyId).equals(aDraggedTagId))
					draggedxmlchilds.remove(i);
			}
		}
		handleSingleopenForDrop(lDraggedTag, lDroppedParentTag, lCopyOfDraggedTag);
		if (aCopy) {
			lDraggedTag = lCopyOfDraggedTag;
		}
		// set parenttag
		lDraggedTag.setParentTag(lDroppedParentTag);
		// add DraggedTag to DroppedParentTag
		List<IXMLTag> droppedxmlchilds = lDroppedParentTag.getChildTags();
		if (lDropType.equals("" + AppConstants.childDrop)) {
			droppedxmlchilds.add(lDraggedTag);
			lDroppedParentTag.setChildTags(droppedxmlchilds);
		} else {
			List<IXMLTag> newdroppedxmlchilds = new ArrayList<IXMLTag>(0);
			for (int i = 0; i < droppedxmlchilds.size(); i++) {
				IXMLTag xmlchild = droppedxmlchilds.get(i);
				if (xmlchild.getAttribute(AppConstants.keyId).equals(aDroppedTagId))
					newdroppedxmlchilds.add(lDraggedTag);
				newdroppedxmlchilds.add(xmlchild);
			}
			lDroppedParentTag.setChildTags(newdroppedxmlchilds);
		}
		aDropTypes.add(lDropType);
		return true;
	}

	/**
	 * Drops tag by ids. Tag is copied or moved depending on aCopy.
	 * 
	 * @param aRootTag      the root tag
	 * @param aDraggedTagId the dragged tag id
	 * @param aDroppedTagId the dropped tag id
	 * @param aCopy         true/false
	 * @param aDropType     drop as child or as sibling
	 * @param aCopiedTags   used to return the copied tags
	 * @param aDropTypes    used to return the drop types
	 * @param aErrors       used to return the error list
	 * 
	 * @return true, if successful
	 */
	private boolean dropTagByIds(IXMLTag aRootTag, String aDraggedTagId, String aDroppedTagId, boolean aCopy,
			String aDropType, List<IXMLTag> aCopiedTags, List<String> aDropTypes, List<String[]> aErrors) {
		if (aRootTag == null)
			return false;
		IXMLTag lContentTag = aRootTag.getChild(AppConstants.contentElement);
		if (lContentTag == null)
			return false;
		List<IXMLTag> lXmlTags = new ArrayList<IXMLTag>();
		lXmlTags.add(lContentTag);
		lXmlTags.addAll(lContentTag.getChildTags());
		return dropTagByIds(lXmlTags, aDraggedTagId, aDroppedTagId, aCopy, aDropType, aCopiedTags, aDropTypes, aErrors);
	}

	@Override
	public IXMLTag copyTagUnique(IXMLTag aTag, IXMLTag aParent, boolean aIncludingBlobs) {
		IXMLTag lClonedTag = aTag.cloneWithoutParentAndChildren();
		if (aTag.getDefAttribute(AppConstants.defKeyType).equals(AppConstants.defValueNode)) {
			// NOTE if aTag is a node tag include reference to original node tag
			lClonedTag.setAttribute(AppConstants.keyReforiginalid, aTag.getAttribute(AppConstants.keyId));
		}
		// give same parent
		lClonedTag.setParentTag(aParent);
		String lType = lClonedTag.getDefAttribute(AppConstants.defKeyType);
		if (lType.equals(AppConstants.defValueNode))
			// if node tag, set attribute id
			setTagId(lClonedTag);
		boolean lIsBlobType = false;
		for (String childTagName : AppConstants.nodeChildTagsWithBlob) {
			if (lType.equals(childTagName)) {
				lIsBlobType = true;
				break;
			}
		}
		if (lIsBlobType) {
			String lBlobId = lClonedTag.getValue();
			if (!lBlobId.equals("")) {
				if (aIncludingBlobs)
					// copy blob
					lBlobId = copyBlob(lBlobId);
				lClonedTag.setValue(lBlobId);
			}
		}
		// childs has to copied too
		List<IXMLTag> lChildTags = aTag.getChildTags();
		List<IXMLTag> lClonedChildTags = lClonedTag.getChildTags();
		for (IXMLTag lChildTag : lChildTags) {
			IXMLTag lClonedChildTag = copyTagUnique(lChildTag, lClonedTag, aIncludingBlobs);
			lClonedChildTags.add(lClonedChildTag);
		}
		lClonedTag.setChildTags(lClonedChildTags);
		return lClonedTag;
	}

	@Override
	public String getDropType(IXMLTag aDraggedTag, IXMLTag aDroppedTag, List<String[]> aErrors) {
		if ((aDraggedTag == null) || (aDroppedTag == null)) {
			return ("" + AppConstants.noDrop);
		}
		// check if parent will be dropped as child of itself
		String lDraggedTagId = aDraggedTag.getAttribute(AppConstants.keyId);
		boolean lParentIsChildOfChild = false;
		IXMLTag lParentTag = aDroppedTag.getParentTag();
		while (lParentTag != null) {
			if (lParentTag.getAttribute(AppConstants.keyId).equals(lDraggedTagId))
				lParentIsChildOfChild = true;
			lParentTag = lParentTag.getParentTag();
		}
		if (lParentIsChildOfChild) {
			appManager.addError(aErrors, "tag", "error_drop_parent_as_child_of_parent");
			return ("" + AppConstants.noDrop);
		}

		boolean lChildDrop = false;
		boolean lSiblingDrop = false;
		// check if will be dropped as child
		String lChildNames = aDroppedTag.getDefAttribute(AppConstants.defKeyChildnodes);
		String lDraggedName = aDraggedTag.getName();
		// if childnodes, check if can be dropped as child
		if ((lChildNames != null) && (!lChildNames.equals(""))) {
			String[] lChildNameArr = lChildNames.split(",");
			for (int i = 0; i < lChildNameArr.length; i++) {
				if (lChildNameArr[i].equals(lDraggedName)) {
					lChildDrop = true;
				}
			}
		}
		// if no childnodes or no valid childnodes, check if can be dropped as child of
		// parent
		lChildNames = aDroppedTag.getParentTag().getDefAttribute(AppConstants.defKeyChildnodes);
		if ((lChildNames != null) && (!lChildNames.equals(""))) {
			String[] lChildNameArr = lChildNames.split(",");
			for (int i = 0; i < lChildNameArr.length; i++) {
				if (lChildNameArr[i].equals(lDraggedName)) {
					lSiblingDrop = true;
				}
			}
		}
		if (lChildDrop && lSiblingDrop) {
			appManager.addError(aErrors, "tag", "error_drop_both_child_and_sibling_possible");
			return ("" + AppConstants.siblingOrChildDrop);
		}
		if (lChildDrop && (!lSiblingDrop))
			return ("" + AppConstants.childDrop);
		if ((!lChildDrop) && lSiblingDrop)
			return ("" + AppConstants.siblingDrop);
		// can not be dropped because can not be child or sibling of dropped tag
		appManager.addError(aErrors, "tag", "error_drop_no_child_or_sibling_possible");
		return ("" + AppConstants.noDrop);
	}

	/**
	 * Deletes blobs referenced within aTag and its child tags.
	 * 
	 * @param aTag the tag
	 */
	private void deleteBlobs(IXMLTag aTag) {
		if (aTag == null) {
			return;
		}
		// Check if childs of type picture/audio/video/url.
		// If so remove blob.
		List<IXMLTag> lChildTags = aTag.getChildTags();
		if (lChildTags == null) {
			return;
		}
		for (IXMLTag lChildTag : lChildTags) {
			String lType = lChildTag.getDefAttribute(AppConstants.defKeyType);
			// Childs of type AppConstants.defValueNode are not found in definition as
			// child, so
			// get type of child. For nodes it is set to AppConstants.defValueNode.
			// This all wasn't necessary if all tags referenced their definition tag.
			// But lot of code is still old and definition tags are not set.
			if (lType.equals("")) {
				lType = lChildTag.getAttribute(AppConstants.defKeyType);
			}
			boolean lIsBlobType = false;
			for (String lChildTagName : AppConstants.nodeChildTagsWithBlob) {
				if (lType.equals(lChildTagName)) {
					lIsBlobType = true;
					break;
				}
			}
			if (lIsBlobType) {
				String lId = lChildTag.getValue();
				if (!(lId == null || lId.equals(""))) {
					int lInt = 0;
					try {
						lInt = Integer.parseInt(lId);
						if (lInt > 0) {
							blobManager.deleteBlob(lInt);
						}
					} catch (NumberFormatException nfe) {
					}
				}
			}
			deleteBlobs(lChildTag);
		}
	}

	/**
	 * Copies blob.
	 * 
	 * @param aBlobId the db blob id
	 * 
	 * @return the new db blob id
	 */
	private String copyBlob(String aBlobId) {
		if ((aBlobId == null) || (aBlobId.equals("")))
			return "";
		String lBlobId = "" + blobManager.copyBlob(Integer.parseInt(aBlobId));
		return lBlobId;
	}

	@Override
	public IXMLTag getTagById(IXMLTag aTag, String aId) {
		if (aTag == null || aId.equals(""))
			return null;
		return getTagByAttributeValue(aTag, AppConstants.keyId, aId);
	}

	@Override
	public IXMLTag getTagById(List<IXMLTag> aXmlList, String aId) {
		if (aXmlList == null || aId.equals(""))
			return null;
		return getTagByAttributeValue(aXmlList, AppConstants.keyId, aId);
	}

	@Override
	public IXMLTag getTagByRefdataid(IXMLTag aTag, String aRefdataid) {
		if (aTag == null || aRefdataid.equals(""))
			return null;
		return getTagByAttributeValue(aTag, AppConstants.keyRefdataid, aRefdataid);
	}

	@Override
	public List<IXMLTag> getTagsByRefdataid(IXMLTag aTag, String aRefdataid) {
		if (aTag == null || aRefdataid.equals(""))
			return (new ArrayList<IXMLTag>(0));
		return getTagsByAttributeValue(aTag, AppConstants.keyRefdataid, aRefdataid);
	}

	@Override
	public IXMLTag getTagByRefstatusid(IXMLTag aTag, String aRefstatusid) {
		if (aTag == null || aRefstatusid.equals(""))
			return null;
		return getTagByAttributeValue(aTag, AppConstants.keyRefstatusid, aRefstatusid);
	}

	/**
	 * Gets the tag by attribute value. Looks in aTag and its xml childs.
	 * 
	 * @param aTag   the tag
	 * @param aKey   the attribute key
	 * @param aValue the attribute value
	 * 
	 * @return the tag by attribute value
	 */
	private IXMLTag getTagByAttributeValue(IXMLTag aTag, String aKey, String aValue) {
		if (aTag == null)
			return null;
		List<IXMLTag> lTags = new ArrayList<IXMLTag>(0);
		lTags.add(aTag);
		return getTagByAttributeValue(lTags, aKey, aValue);
	}

	/**
	 * Gets the tags by attribute value. Looks in aTag and its xml childs.
	 * 
	 * @param aTag   the tag
	 * @param aKey   the attribute key
	 * @param aValue the attribute value
	 * 
	 * @return the tags by attribute value
	 */
	private List<IXMLTag> getTagsByAttributeValue(IXMLTag aTag, String aKey, String aValue) {
		List<IXMLTag> lTags = new ArrayList<IXMLTag>(0);
		if (aTag == null)
			return lTags;
		getTagsByAttributeValue(aTag.getChildTags(), aKey, aValue, lTags);
		return lTags;
	}

	/**
	 * Gets the tag by attribute value. Looks in list aXmlList and its xml childs.
	 * 
	 * @param aXmlList the xml list
	 * @param aKey     the attribute key
	 * @param aValue   the attribute value
	 * 
	 * @return the tag by attribute value
	 */
	private IXMLTag getTagByAttributeValue(List<IXMLTag> aXmlList, String aKey, String aValue) {
		if ((aXmlList == null) || (aXmlList.isEmpty()))
			return null;
		for (int i = (aXmlList.size() - 1); i >= 0; i--) {
			IXMLTag xmltag = aXmlList.get(i);
			if (xmltag.getAttribute(aKey).equals(aValue)) {
				// NOTE id of xmltag may not be empty. There are some tags that, e.g., have
				// refdataid set but have id empty. For instance refalternative. These tags
				// should be omitted.
				if (!xmltag.getAttribute(AppConstants.keyId).equals("")) {
					return xmltag;
				}
			}
			xmltag = getTagByAttributeValue(xmltag.getChildTags(), aKey, aValue);
			if (xmltag != null)
				return xmltag;
		}
		return null;
	}

	/**
	 * Gets the tags by attribute value. Looks in list aXmlList and its xml childs.
	 * 
	 * @param aXmlList the xml list
	 * @param aKey     the attribute key
	 * @param aValue   the attribute value
	 * @param aTags    used to return the tags
	 */
	private void getTagsByAttributeValue(List<IXMLTag> aXmlList, String aKey, String aValue, List<IXMLTag> aTags) {
		if ((aXmlList == null) || (aXmlList.size() == 0))
			return;
		for (int i = (aXmlList.size() - 1); i >= 0; i--) {
			IXMLTag xmltag = aXmlList.get(i);
			if (xmltag.getAttribute(aKey).equals(aValue)) {
				// NOTE id of xmltag may not be empty. There are some tags that, e.g., have
				// refdataid set but have id empty. For instance refalternative. These tags
				// should be omitted.
				if (!xmltag.getAttribute(AppConstants.keyId).equals("")) {
					aTags.add(xmltag);
				}
			}
			getTagsByAttributeValue(xmltag.getChildTags(), aKey, aValue, aTags);
		}
	}

}