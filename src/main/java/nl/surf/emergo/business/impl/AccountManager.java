/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business.impl;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;

import nl.surf.emergo.business.abstr.AbstractDaoBasedAccountManager;
import nl.surf.emergo.domain.EAccount;
import nl.surf.emergo.domain.EAccountRole;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IEAccountRole;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.domain.IERole;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;

/**
 * The Class AccountManager.
 */
public class AccountManager extends AbstractDaoBasedAccountManager implements InitializingBean {

	private static final String ERROR_EMPTY = "error_empty";

	@Override
	public void afterPropertiesSet() throws Exception {
	}

	@Override
	public IEAccount getNewAccount() {
		return new EAccount();
	}

	@Override
	public IEAccount getAccount(int accId) {
		return addRolesData(accountDao.get(accId));
	}

	@Override
	public IEAccount getAccount(String userid) {
		return addRolesData(accountDao.get(userid));
	}

	@Override
	public IEAccount getAccount(String userid, String password, boolean active) {
		return addRolesData(accountDao.get(userid, password, active));
	}

	protected IEAccount addRolesData(IEAccount account) {
		if (account != null) {
			Set<IERole> eRoles = new HashSet<>();
			Set<IEAccountRole> eAccRoles = new HashSet<>();
			List<IEAccountRole> lAccountRoles = accountRoleDao.getAllByAccId(account.getAccId());
			List<IERole> lAllRoles = roleDao.getAll();
			for (IEAccountRole lAccountRole : lAccountRoles) {
				eAccRoles.add(lAccountRole);
				int lRolId = lAccountRole.getRolRolId();
				for (IERole lRole : lAllRoles) {
					if (lRole.getRolId() == lRolId)
						eRoles.add(lRole);
				}
			}
			account.setERoles(eRoles);
			account.setEAccountRoles(eAccRoles);
		}
		return account;
	}

	@Override
	public String getAccountName(IEAccount eAccount) {
		// build name using account title, initials, name prefix and lastname
		String lName = "";
		if (eAccount == null)
			return lName;
		lName = eAccount.getTitle();
		String lStr = eAccount.getInitials();
		if (!(lStr.equals(""))) {
			if (!(lName.equals("")))
				lName += " ";
			lName += lStr;
		}
		lStr = eAccount.getNameprefix();
		if (!(lStr.equals(""))) {
			if (!(lName.equals("")))
				lName += " ";
			lName += lStr;
		}
		lStr = eAccount.getLastname();
		if (!(lStr.equals(""))) {
			if (!(lName.equals("")))
				lName += " ";
			lName += lStr;
		}
		return lName;
	}

	@Override
	public void saveAccount(IEAccount eAccount) {
		accountDao.save(eAccount);
		saveAccountRoles(eAccount);
	}

	protected void saveAccountRoles(IEAccount account) {
		if (account != null) {
			// add or delete account roles
			Set<IERole> eRoles = account.getERoles();
			List<IEAccountRole> lAccountRoles = accountRoleDao.getAllByAccId(account.getAccId());
			// delete old account roles
			for (IEAccountRole lAccountRole : lAccountRoles) {
				int lRolId = lAccountRole.getRolRolId();
				boolean lFound = false;
				for (IERole lRole : eRoles) {
					if (lRole.getRolId() == lRolId)
						lFound = true;
				}
				if (!lFound)
					accountRoleDao.delete(lAccountRole);
			}
			// add new roles
			for (IERole lRole : eRoles) {
				boolean lFound = false;
				for (IEAccountRole lAccountRole : lAccountRoles) {
					if (lRole.getRolId() == lAccountRole.getRolRolId())
						lFound = true;
				}
				if (!lFound) {
					EAccountRole lNewAccountRole = new EAccountRole();
					lNewAccountRole.setAccAccId(account.getAccId());
					lNewAccountRole.setRolRolId(lRole.getRolId());
					lNewAccountRole.setLandingpage("");
					lNewAccountRole.setCreationdate(account.getLastupdatedate());
					lNewAccountRole.setLastupdatedate(account.getLastupdatedate());
					accountRoleDao.save(lNewAccountRole);
				}
			}
		}
	}

	@Override
	public List<String[]> newAccount(IEAccount eAccount) {
		// Validate item and if ok, save it after setting creationdate and
		// lastupdatedate.
		List<String[]> lErrors = validateAccount(eAccount);
		if (lErrors == null) {
			eAccount.setCreationdate(new Date());
			eAccount.setLastupdatedate(new Date());
			saveAccount(eAccount);
		}
		return lErrors;
	}

	@Override
	public List<String[]> updateAccount(IEAccount eAccount) {
		// Validate item and if ok, save it after setting lastupdatedate.
		List<String[]> lErrors = validateAccount(eAccount);
		if (lErrors == null) {
			eAccount.setLastupdatedate(new Date());
			saveAccount(eAccount);
		}
		return lErrors;
	}

	@Override
	public List<String[]> validateAccount(IEAccount eAccount) {
		/*
		 * Validate if userid, password and lastname are not empty. Validate if userid
		 * is unique (also when userid is changed). If validation error return it as
		 * element in String[] list.
		 */
		List<String[]> lErrors = new ArrayList<>(0);
		if (appManager.isEmpty(eAccount.getUserid()))
			appManager.addError(lErrors, "userid", ERROR_EMPTY);
		else {
			IEAccount lExistingAccount = accountDao.get(eAccount.getAccId());
			// check if changed
			boolean lChanged = ((lExistingAccount == null)
					|| (!eAccount.getUserid().equals(lExistingAccount.getUserid())));
			if ((lChanged) && (accountDao.get(eAccount.getUserid()) != null))
				appManager.addError(lErrors, "userid", "error_not_unique");
		}
		if (appManager.isEmpty(eAccount.getPassword()))
			appManager.addError(lErrors, "password", ERROR_EMPTY);
		if (appManager.isEmpty(eAccount.getLastname()))
			appManager.addError(lErrors, "lastname", ERROR_EMPTY);
		if (!appManager.isJson(eAccount.getExtradata()))
			appManager.addError(lErrors, "extradata", "error_no_json");
		return lErrors.isEmpty() ? null : lErrors;
	}

	@Override
	public void deleteAccount(int accId) {
		deleteAccount(getAccount(accId));
	}

	@Override
	public void deleteAccount(IEAccount eAccount) {
		// first delete possibly related blobs. they are not deleted in cascade.
		deleteBlobs(eAccount);
		// delete run group accounts of account if account is last one in run group
		List<IERunGroupAccount> lItems = runGroupAccountManager.getAllRunGroupAccountsByAccId(eAccount.getAccId());
		if (lItems != null) {
			for (IERunGroupAccount lRunGroupAccount : lItems) {
				IERunGroup lRunGroup = lRunGroupAccount.getERunGroup();
				List<IERunGroupAccount> lRunGroupAccounts = runGroupAccountManager
						.getAllRunGroupAccountsByRugId(lRunGroup.getRugId());
//				if only one run group account left per run group also delete run group
				if (lRunGroupAccounts.size() == 1)
					runGroupManager.deleteRunGroup(lRunGroup);
			}
		}
		if (eAccount.getLastupdatedate() == null)
			// lastupdatedate field is added later, so value can be empty for older records
			eAccount.setLastupdatedate(eAccount.getCreationdate());
		accountDao.delete(eAccount);
	}

	@Override
	public void deleteBlobs(IEAccount eAccount) {
		// delete blobs for all cases owned by account
		List<IECase> lCases = caseManager.getAllCasesByAccId(eAccount.getAccId());
		if (lCases != null) {
			for (IECase lCase : lCases)
				caseManager.deleteBlobs(lCase);
		}
		// delete blobs for all runs owned by account
		List<IERun> lRuns = runManager.getAllRunsByAccId(eAccount.getAccId());
		if (lRuns != null) {
			for (IERun lRun : lRuns)
				runManager.deleteBlobs(lRun);
		}
	}

	@Override
	public List<IEAccount> getAllAccounts() {
		return addRolesData(accountDao.getAll(), true);
	}

	@Override
	public List<IEAccount> getAllAccounts(boolean active) {
		return addRolesData(accountDao.getAll(active), true);
	}

	@Override
	public List<IEAccount> getAllAccounts(boolean active, String roles) {
		if (roles.indexOf(AppConstants.c_role_stu) >= 0)
			return getAllAccountsStudentRoleIncluded(active, roles);
		else
			return getAllAccountsStudentRoleExcluded(active, roles);
	}

	protected List<IEAccount> getAllAccountsStudentRoleIncluded(boolean active, String roles) {
		List<IEAccount> lAccounts = addRolesData(accountDao.getAll(active), true);
		return filterAccountsOnRoles(lAccounts, roles);
	}

	protected List<IEAccount> getAllAccountsStudentRoleExcluded(boolean active, String roles) {
		return addRolesData(accountDao.getAllByAccIds(active, getAccountRoleAccIds(roles)), false);
	}

	protected List<Integer> getAccountRoleAccIds(String roles) {
		List<IERole> lAllRoles = roleDao.getAll();
		List<Integer> lRolIds = new ArrayList<>();
		for (IERole lRole : lAllRoles) {
			if (roles.contains(lRole.getCode()))
				lRolIds.add(lRole.getRolId());
		}
		List<IEAccountRole> lAccRoles = accountRoleDao.getAllByRolIds(lRolIds);
		List<Integer> lAccIds = new ArrayList<>();
		for (IEAccountRole lAccRole : lAccRoles) {
			int lAccId = lAccRole.getAccAccId();
			if (!lAccIds.contains(lAccId))
				lAccIds.add(lAccId);
		}
		return lAccIds;
	}

	@Override
	public List<IEAccount> getAllAccountsFilter(boolean active, String roles, Map<String, String> keysAndValueParts) {
		if (roles.indexOf(AppConstants.c_role_stu) >= 0)
			return getAllAccountsStudentRoleIncluded(active, roles, keysAndValueParts);
		else
			return getAllAccountsStudentRoleExcluded(active, roles, keysAndValueParts);
	}

	protected List<IEAccount> getAllAccountsStudentRoleIncluded(boolean active, String roles,
			Map<String, String> keysAndValueParts) {
		List<IEAccount> lAccounts = addRolesData(accountDao.getAllFilter(active, keysAndValueParts), true);
		return filterAccountsOnRoles(lAccounts, roles);
	}

	protected List<IEAccount> getAllAccountsStudentRoleExcluded(boolean active, String roles,
			Map<String, String> keysAndValueParts) {
		return addRolesData(accountDao.getAllByAccIdsFilter(active, getAccountRoleAccIds(roles), keysAndValueParts),
				false);
	}

	@Override
	public List<IEAccount> getAllAccountsFilter(Map<String, String> keysAndValueParts) {
		return addRolesData(accountDao.getAllFilter(keysAndValueParts), true);
	}

	@Override
	public List<IEAccount> getAllAccountsFilter(boolean active, Map<String, String> keysAndValueParts) {
		return addRolesData(accountDao.getAllFilter(active, keysAndValueParts), true);
	}

	@Override
	public List<IEAccount> getAllAccounts(boolean active, String roles, List<Integer> conIds) {
		List<Integer> lAccIds = getAccountContextAccIds(active, conIds);
		List<IEAccount> lAccounts = addRolesData(accountDao.getAllByAccIds(active, lAccIds), false);
		return filterAccountsOnRoles(lAccounts, roles);
	}

	protected List<Integer> getAccountContextAccIds(boolean active, List<Integer> conIds) {
		return accountContextManager.getAccountContextAccIdsByContextIdsActive(conIds, active);
	}

	protected List<IEAccount> filterAccountsOnRoles(List<IEAccount> aAccounts, String aRoles) {
		String[] rolearr = aRoles.split(",");
		for (int i = aAccounts.size() - 1; i >= 0; i--) {
			Set<IERole> lRoles = aAccounts.get(i).getERoles();
			boolean lFound = false;
			for (IERole lRole : lRoles) {
				for (int j = 0; j < rolearr.length; j++) {
					if (lRole.getCode().equals(rolearr[j]))
						lFound = true;
				}
			}
			if (!lFound)
				aAccounts.remove(i);
		}
		return aAccounts;
	}

	@Override
	public List<IEAccount> getAllAccountsFilter(boolean active, String roles, List<Integer> conIds,
			Map<String, String> keysAndValueParts) {
		List<Integer> lAccIds = getAccountContextAccIds(active, conIds);
		List<IEAccount> lAccounts = addRolesData(accountDao.getAllByAccIdsFilter(active, lAccIds, keysAndValueParts),
				false);
		return filterAccountsOnRoles(lAccounts, roles);
	}

	@Override
	public List<IEAccount> getAllAccountsByEmail(String email) {
		return accountDao.getAllByEmail(email);
	}

	protected List<IEAccount> addRolesData(List<IEAccount> accounts, boolean all) {
		if (accounts != null) {
			List<IERole> lAllRoles = roleDao.getAll();
			Hashtable<Integer, IERole> lHAllRoles = new Hashtable<>();
			for (IERole role : lAllRoles) {
				lHAllRoles.put(role.getRolId(), role);
			}
			List<IEAccountRole> lAccountRoles = null;
			if (all)
				lAccountRoles = accountRoleDao.getAll();
			else {
				List<Integer> lAccIds = new ArrayList<>();
				for (IEAccount lAccount : accounts) {
					lAccIds.add(lAccount.getAccId());
				}
				lAccountRoles = accountRoleDao.getAllByAccIds(lAccIds);
				lAccIds.clear();
			}
			Hashtable<Integer, IEAccountRole> lHAcrIdAccountRoles = new Hashtable<>();
			for (IEAccountRole accrole : lAccountRoles) {
				lHAcrIdAccountRoles.put(accrole.getAcrId(), accrole);
			}

			Hashtable<Integer, List<Integer>> lHAccountRoles = new Hashtable<>();
			Hashtable<Integer, List<Integer>> lHAccountAccRoles = new Hashtable<>();
			for (IEAccountRole accountRole : lAccountRoles) {
				int accId = accountRole.getAccAccId();
				if (lHAccountRoles.containsKey(accId)) {
					List<Integer> rolIds = lHAccountRoles.get(accId);
					rolIds.add(accountRole.getRolRolId());
					lHAccountRoles.put(accId, rolIds);
					List<Integer> acrIds = lHAccountAccRoles.get(accId);
					acrIds.add(accountRole.getAcrId());
					lHAccountAccRoles.put(accId, acrIds);
				} else {
					List<Integer> rolIds = new ArrayList<>();
					rolIds.add(accountRole.getRolRolId());
					lHAccountRoles.put(accId, rolIds);
					List<Integer> acrIds = new ArrayList<>();
					acrIds.add(accountRole.getAcrId());
					lHAccountAccRoles.put(accId, acrIds);
				}
			}
			for (IEAccount account : accounts) {
				int accId = account.getAccId();
				Set<IERole> eRoles = new HashSet<>();
				Set<IEAccountRole> eAccRoles = new HashSet<>();
				if (lHAccountRoles.containsKey(accId)) {
					List<Integer> rolIds = lHAccountRoles.get(accId);
					for (Integer rolId : rolIds) {
						eRoles.add(lHAllRoles.get(rolId));
					}
					List<Integer> acrIds = lHAccountAccRoles.get(accId);
					for (Integer acrId : acrIds) {
						eAccRoles.add(lHAcrIdAccountRoles.get(acrId));
					}
				}
				account.setERoles(eRoles);
				account.setEAccountRoles(eAccRoles);
			}
			lHAccountRoles.clear();
			lHAccountAccRoles.clear();
			lAllRoles.clear();
			lAccountRoles.clear();
			lHAllRoles.clear();
			lHAcrIdAccountRoles.clear();
		}
		return accounts;
	}

	@Override
	public List<IERole> getAllRoles() {
		return roleDao.getAll();
	}

	@Override
	public List<IEAccountRole> getAllAccountRoles() {
		return accountRoleDao.getAll();
	}

	@Override
	public boolean hasRole(IEAccount account, String rolCode) {
		if (account == null)
			return false;
		List<IEAccountRole> lAcrs = accountRoleDao.getAllByAccId(account.getAccId());
		List<IERole> lAllRoles = roleDao.getAll();
		for (IEAccountRole lAcr : lAcrs) {
			int lRolId = lAcr.getRolRolId();
			for (IERole lRole : lAllRoles) {
				if (lRole.getRolId() == lRolId && lRole.getCode().equals(rolCode))
					return true;
			}
		}
		return false;
	}

	@Override
	public String getRoleLandingpage(IEAccount account, String rolCode) {
		if (account == null)
			return "";
		List<IEAccountRole> lAcrs = accountRoleDao.getAllByAccId(account.getAccId());
		List<IERole> lAllRoles = roleDao.getAll();
		for (IEAccountRole lAcr : lAcrs) {
			int lRolId = lAcr.getRolRolId();
			for (IERole lRole : lAllRoles) {
				if (lRole.getRolId() == lRolId && lRole.getCode().equals(rolCode)) {
					String lLandingpage = lAcr.getLandingpage();
					if (StringUtils.isEmpty(lLandingpage)) {
						return "";
					} else {
						return lLandingpage;
					}
				}
			}
		}
		return "";
	}

	@Override
	public Hashtable<String, String> getRoleLandingpages(IEAccount account) {
		Hashtable<String, String> landingpagesPerRolCode = new Hashtable<>();
		if (account == null)
			return landingpagesPerRolCode;
		List<IEAccountRole> lAcrs = accountRoleDao.getAllByAccId(account.getAccId());
		List<IERole> lAllRoles = roleDao.getAll();
		for (IEAccountRole lAcr : lAcrs) {
			if (!StringUtils.isEmpty(lAcr.getLandingpage())) {
				int lRolId = lAcr.getRolRolId();
				for (IERole lRole : lAllRoles) {
					if (lRole.getRolId() == lRolId) {
						if (!landingpagesPerRolCode.containsKey(lRole.getCode())) {
							landingpagesPerRolCode.put(lRole.getCode(), lAcr.getLandingpage());
						}
					}
				}
			}
		}
		return landingpagesPerRolCode;
	}

	@Override
	public void setRoleLandingpages(IEAccount account, Hashtable<String, String> landingpagesPerRolCode) {
		if (account == null || landingpagesPerRolCode == null)
			return;
		List<IEAccountRole> lAcrs = accountRoleDao.getAllByAccId(account.getAccId());
		List<IERole> lAllRoles = roleDao.getAll();
		Set<IEAccountRole> lHAccRoles = new HashSet<>();
		for (IEAccountRole lAcr : lAcrs) {
			int lRolId = lAcr.getRolRolId();
			for (IERole lRole : lAllRoles) {
				if (lRole.getRolId() == lRolId) {
					if (landingpagesPerRolCode.containsKey(lRole.getCode())) {
						lAcr.setLandingpage(landingpagesPerRolCode.get(lRole.getCode()));
						if (lAcr.getCreationdate() == null) {
							lAcr.setCreationdate(account.getLastupdatedate());
						}
						lAcr.setLastupdatedate(account.getLastupdatedate());
						accountRoleDao.save(lAcr);
					}
				}
			}
			lHAccRoles.add(lAcr);
		}
		account.setEAccountRoles(lHAccRoles);
	}

	@Override
	public String randomPassword() {
		int pw_len = 12;
		// no chars l, o and no digits 0 and 1: to prevent problems typing in passwords
		String pw_chars = "abcdefghijkmnpqrstuvwxyz23456789ABCDEFGHIJKMNPQRSTUVWXYZ!@#$%*";
		// NOTE start password with an alphabetical character
		int max = 24;
		SecureRandom secureRandom = new SecureRandom();
		int rand = secureRandom.nextInt(max);
		if (rand == max) {
			rand = 0;
		}
		String result = pw_chars.substring(rand, rand + 1);
		// NOTE from now on take characters randomly from pw_chars
		max = pw_chars.length();
		for (int i = 1; i < pw_len; i++) {
			rand = secureRandom.nextInt(max);
			if (rand == max) {
				rand = 0;
			}
			result += pw_chars.substring(rand, rand + 1);
		}
		return result;
	}

	@Override
	public String encodePassword(String password) {
		return bCryptPasswordEncoder.encode(password);
	}

	@Override
	public boolean matchesEncodedPassword(String password, String encodedPassword) {
		return bCryptPasswordEncoder.matches(password, encodedPassword);
	}

}