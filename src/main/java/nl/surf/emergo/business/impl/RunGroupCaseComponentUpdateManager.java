/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;

import nl.surf.emergo.business.abstr.AbstractDaoBasedRunGroupCaseComponentUpdateManager;
import nl.surf.emergo.domain.ERunGroupCaseComponentUpdate;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IEComponent;
import nl.surf.emergo.domain.IERunGroupCaseComponentUpdate;

/**
 * The Class RunGroupCaseComponentUpdateManager.
 */
public class RunGroupCaseComponentUpdateManager extends AbstractDaoBasedRunGroupCaseComponentUpdateManager
		implements InitializingBean {

	@Override
	public void afterPropertiesSet() throws Exception {
		// NOOP
	}

	@Override
	public IERunGroupCaseComponentUpdate getNewRunGroupCaseComponentUpdate() {
		return new ERunGroupCaseComponentUpdate();
	}

	@Override
	public IERunGroupCaseComponentUpdate getRunGroupCaseComponentUpdate(int rguId) {
		return runGroupCaseComponentUpdateDao.get(rguId);
	}

	@Override
	public void saveRunGroupCaseComponentUpdate(IERunGroupCaseComponentUpdate eRunGroupCaseComponentUpdate) {
		runGroupCaseComponentUpdateDao.save(eRunGroupCaseComponentUpdate);
	}

	@Override
	public List<String[]> newRunGroupCaseComponentUpdate(IERunGroupCaseComponentUpdate eRunGroupCaseComponentUpdate) {
		eRunGroupCaseComponentUpdate.setCreationdate(new Date());
		eRunGroupCaseComponentUpdate.setLastupdatedate(new Date());
		saveRunGroupCaseComponentUpdate(eRunGroupCaseComponentUpdate);
		return null;
	}

	@Override
	public List<String[]> updateRunGroupCaseComponentUpdate(
			IERunGroupCaseComponentUpdate eRunGroupCaseComponentUpdate) {
		eRunGroupCaseComponentUpdate.setLastupdatedate(new Date());
		saveRunGroupCaseComponentUpdate(eRunGroupCaseComponentUpdate);
		return null;
	}

	@Override
	public void deleteRunGroupCaseComponentUpdate(int rguId) {
		deleteRunGroupCaseComponentUpdate(getRunGroupCaseComponentUpdate(rguId));
	}

	@Override
	public void deleteRunGroupCaseComponentUpdate(IERunGroupCaseComponentUpdate eRunGroupCaseComponentUpdate) {
		// first delete possibly related blobs. they are not deleted in cascade.
		deleteBlobs(eRunGroupCaseComponentUpdate);
		if (eRunGroupCaseComponentUpdate.getLastupdatedate() == null)
			// lastupdatedate field is added later, so value can be empty for older records
			eRunGroupCaseComponentUpdate.setLastupdatedate(eRunGroupCaseComponentUpdate.getCreationdate());
		runGroupCaseComponentUpdateDao.delete(eRunGroupCaseComponentUpdate);
	}

	@Override
	public void deleteBlobs(IERunGroupCaseComponentUpdate eRunGroupCaseComponentUpdate) {
		// delete blobs referenced within xmldata property
		String lXmlDef = "";
		IECaseComponent eCaseComponent = caseComponentDao.get(eRunGroupCaseComponentUpdate.getCacCacId());
		if (eCaseComponent != null) {
			IEComponent eComponent = eCaseComponent.getEComponent();
			if (eComponent != null)
				lXmlDef = eComponent.getXmldefinition();
		}
		String lXmlData = eRunGroupCaseComponentUpdate.getXmldata();
		xmlManager.deleteBlobs(lXmlDef, lXmlData, AppConstants.status_tag);
	}

	@Override
	public List<IERunGroupCaseComponentUpdate> getAllRunGroupCaseComponentUpdates() {
		return runGroupCaseComponentUpdateDao.getAll();
	}

	@Override
	public List<IERunGroupCaseComponentUpdate> getAllRunGroupCaseComponentUpdatesByRugId(int rugId) {
		return runGroupCaseComponentUpdateDao.getAllByRugId(rugId);
	}

	@Override
	public List<IERunGroupCaseComponentUpdate> getAllRunGroupCaseComponentUpdatesByRugId(int rugId, boolean processed) {
		return runGroupCaseComponentUpdateDao.getAllByRugId(rugId, processed);
	}

	@Override
	public List<IERunGroupCaseComponentUpdate> getAllRunGroupCaseComponentUpdatesByCacId(int cacId) {
		return runGroupCaseComponentUpdateDao.getAllByCacId(cacId);
	}

	@Override
	public List<IERunGroupCaseComponentUpdate> getAllRunGroupCaseComponentUpdatesByRugFromId(int rugFromId) {
		return runGroupCaseComponentUpdateDao.getAllByRugFromId(rugFromId);
	}

}