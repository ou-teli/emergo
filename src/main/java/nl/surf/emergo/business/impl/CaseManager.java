/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;

import nl.surf.emergo.business.abstr.AbstractDaoBasedCaseManager;
import nl.surf.emergo.domain.ECase;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IECaseComponentRole;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.domain.IEComponent;
import nl.surf.emergo.domain.IERun;

/**
 * The Class CaseManager.
 */
public class CaseManager extends AbstractDaoBasedCaseManager implements
		InitializingBean {

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	public void afterPropertiesSet() throws Exception {
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseManager#getNewCase()
	 */
	public IECase getNewCase() {
		IECase eCase = new ECase();
		return eCase;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseManager#getCase(int)
	 */
	public IECase getCase(int casId) {
		IECase eCase = caseDao.get(casId);
		return eCase;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseManager#saveCase(nl.surf.emergo.domain.IECase)
	 */
	public void saveCase(IECase eCase) {
		caseDao.save(eCase);
		saveCaseInMemory(eCase);
	}

	/**
	 * Saves references to case in application memory.
	 *
	 * @param eCase the e case
	 */
	private void saveCaseInMemory(IECase eCase) {
		// save within casecomponents in memory for better performance of player
		Hashtable<String,List<IECaseComponent>> lCaseComponentsInRunByCasId = (Hashtable)appManager.getAppContainer().get(AppConstants.caseComponentsInRunByCasId);
		if (lCaseComponentsInRunByCasId != null) {
			for (Enumeration<String> keys = lCaseComponentsInRunByCasId.keys(); keys.hasMoreElements();) {
				String lId = (String) keys.nextElement();
				List<IECaseComponent> lCaseComponents = lCaseComponentsInRunByCasId.get(lId);
				if (lCaseComponents != null) {
					for (IECaseComponent lCaseComponent : lCaseComponents) {
						if (lCaseComponent.getECase().getCasId() == eCase.getCasId()) {
							lCaseComponent.setECase(eCase);
						}
					}
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseManager#newCase(nl.surf.emergo.domain.IECase, boolean)
	 */
	public List<String[]> newCase(IECase eCase, boolean aCreateOneCaseRole) {
		// Validate item and if ok, save it after setting creationdate and lastupdatedate.
		List<String[]> lErrors = validateCase(eCase);
		if (lErrors == null) {
// 			case under construction or template not used yet
			eCase.setStatus(AppConstants.case_status_runnable);
// 			new case is always active
			eCase.setActive(true);
			eCase.setCreationdate(new Date());
			eCase.setLastupdatedate(new Date());
			saveCase(eCase);
			IECaseRole eCaseRole = null;
			if (aCreateOneCaseRole) {
// 				Add student case role so already one case role is present
				eCaseRole = caseRoleManager.getNewCaseRole();
				eCaseRole.setECase(eCase);
				eCaseRole.setName("student");
				caseRoleManager.newCaseRole(eCaseRole);
			}
			// Add system components as case components
			// System components are not visible to case developers
			List<IEComponent> eComponents = componentManager.getAllComponents(true);
			for (IEComponent eComponent : eComponents) {
				if (eComponent.getType() == AppConstants.system_component) {
					IECaseComponent eCaseComponent = caseComponentManager.getNewCaseComponent();
					eCaseComponent.setEAccount(eCase.getEAccount());
					eCaseComponent.setECase(eCase);
					eCaseComponent.setEComponent(eComponent);
					eCaseComponent.setName(eComponent.getCode());
					eCaseComponent.setXmldata(AppConstants.emptyXml);
					caseComponentManager.newCaseComponent(eCaseComponent);
					if (aCreateOneCaseRole) {
//			 			Add student case component role
						IECaseComponentRole eCaseComponentRole = caseComponentRoleManager.getNewCaseComponentRole();
						eCaseComponentRole.setCacCacId(eCaseComponent.getCacId());
						eCaseComponentRole.setCarCarId(eCaseRole.getCarId());
						eCaseComponentRole.setName(eCaseComponent.getName());
						caseComponentRoleManager.newCaseComponentRole(eCaseComponentRole);
					}
				}
			}
		}
		return lErrors;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseManager#updateCase(nl.surf.emergo.domain.IECase)
	 */
	public List<String[]> updateCase(IECase eCase) {
		// Validate item and if ok, save it after setting lastupdatedate.
		List<String[]> lErrors = validateCase(eCase);
		if (lErrors == null) {
			eCase.setLastupdatedate(new Date());
			saveCase(eCase);
		}
		return lErrors;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseManager#validateCase(nl.surf.emergo.domain.IECase)
	 */
	public List<String[]> validateCase(IECase eCase) {
		/*
		 * Validate if name is not empty.
		 * Validate if name/version combination is unique.
		 * If validation error return it as element in String[] list.
		 */
		List<String[]> lErrors = new ArrayList<String[]>(0);

		if (appManager.isEmpty(eCase.getName()))
			appManager.addError(lErrors, "name", "error_empty");
		else {
			IECase lExistingCase = caseDao.get(eCase.getCasId());
			// check if changed
			boolean lChanged = ((lExistingCase == null)
					|| (!eCase.getName().equals(lExistingCase.getName())) || (eCase
					.getVersion() != lExistingCase.getVersion()));
			if ((lChanged)
					&& (caseDao.exists(eCase.getEAccount().getAccId(), eCase
							.getName(), eCase.getVersion())))
				appManager.addError(lErrors, "name_version", "error_not_unique");
		}
		if (lErrors.size() > 0)
			return lErrors;
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseManager#deleteCase(int)
	 */
	public void deleteCase(int casId) {
		deleteCase(getCase(casId));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseManager#deleteCase(nl.surf.emergo.domain.IECase)
	 */
	public void deleteCase(IECase eCase) {
		// first delete possibly related blobs. they are not deleted in cascade.
		deleteBlobs(eCase);
		if (eCase.getLastupdatedate() == null)
			// lastupdatedate field is added later, so value can be empty for older records
			eCase.setLastupdatedate(eCase.getCreationdate());
		caseDao.delete(eCase);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseManager#deleteBlobs(nl.surf.emergo.domain.IECase)
	 */
	public void deleteBlobs(IECase eCase) {
		// delete blobs for all case components of case
		List<IECaseComponent> lCaseComponents = caseComponentManager.getAllCaseComponentsByCasId(eCase.getCasId());
		if (lCaseComponents != null) {
			for (IECaseComponent lCaseComponent : lCaseComponents)
				caseComponentManager.deleteBlobs(lCaseComponent);
		}
		// delete blobs for all runs of case
		List<IERun> lRuns = runManager.getAllRunsByCasId(eCase.getCasId());
		if (lRuns != null) {
			for (IERun lRun : lRuns)
				runManager.deleteBlobs(lRun);
		}
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseManager#getAllCases()
	 */
	public List<IECase> getAllCases() {
		return caseDao.getAll();
	}

	@Override
	public List<IECase> getAllCasesFilter(Map<String, String> keysAndValueParts) {
		return caseDao.getAllFilter(keysAndValueParts);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseManager#getAllCasesByAccId(int)
	 */
	public List<IECase> getAllCasesByAccId(int accId) {
		return caseDao.getAllByAccId(accId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseManager#getAllCasesByOwnerOrAuthorAccId(int)
	 */
	public List<IECase> getAllCasesByOwnerOrAuthorAccId(int accId) {
		//NOTE get all cases for which accId is owner
		List<IECase> lItems = getAllCasesByAccId(accId);
		if (lItems == null) {
			lItems = new ArrayList<IECase>();
		}
		List<Integer> lCasIds = new ArrayList<Integer>();
		for (IECase lItem : lItems) {
			lCasIds.add(lItem.getCasId());
		}
		//NOTE get all cases for which accId is author of one of its case components
		List<String> lErrors = new ArrayList<String>();
		List<Object> lObjects = appManager.getSqlResult("SELECT DISTINCT casCasId FROM `casecomponents` WHERE accAccId=" + accId + ";", lErrors);
		String lSqlPart = "";
		if (lObjects != null) {
			for(Object lObject : lObjects){
				if (lObject instanceof Integer) {
					Integer lCasId = (Integer)lObject;
					if (!lCasIds.contains(lCasId)) {
						if (!lSqlPart.equals("")) {
							lSqlPart += ",";
						}
						lSqlPart += lCasId.toString();
					}
				}
			}
		}
		if (!lSqlPart.equals("")) {
			lSqlPart = "casId in (" + lSqlPart + ")";
			lObjects = appManager.getSqlResult("SELECT * FROM `cases` WHERE " + lSqlPart + ";", ECase.class, lErrors);
			if (lObjects != null) {
				for(Object lObject : lObjects){
					if (lObject instanceof IECase) {
						lItems.add((IECase)lObject);
					}
				}
			}
		}
		Collections.sort(lItems, new CaseSortByCaseName());
		return lItems;
	}

	protected class CaseSortByCaseName implements Comparator<IECase>{
		public int compare(IECase o1, IECase o2) {
			return o1.getName().compareTo(o2.getName());
		}
	}
	
	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseManager#getAllRunnableCases()
	 */
	public List<IECase> getAllRunnableCases() {
		return caseDao.getAllRunnable();
	}

}