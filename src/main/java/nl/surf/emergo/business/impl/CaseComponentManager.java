/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;

import nl.surf.emergo.business.abstr.AbstractDaoBasedCaseComponentManager;
import nl.surf.emergo.domain.ECaseComponent;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IEComponent;

/**
 * The Class CaseComponentManager.
 */
public class CaseComponentManager extends AbstractDaoBasedCaseComponentManager
		implements InitializingBean {

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	public void afterPropertiesSet() throws Exception {
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseComponentManager#getNewCaseComponent()
	 */
	public IECaseComponent getNewCaseComponent() {
		IECaseComponent eCaseComponent = new ECaseComponent();
		return eCaseComponent;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseComponentManager#getCaseComponent(int)
	 */
	public IECaseComponent getCaseComponent(int cacId) {
		IECaseComponent eCaseComponent = caseComponentDao.get(cacId);
		return eCaseComponent;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseComponentManager#saveCaseComponent(nl.surf.emergo.domain.IECaseComponent)
	 */
	public void saveCaseComponent(IECaseComponent eCaseComponent) {
		caseComponentDao.save(eCaseComponent);
		saveCaseComponentInMemory(eCaseComponent);
	}

	/**
	 * Saves case component in application memory.
	 *
	 * @param eCaseComponent the e case component
	 */
	private void saveCaseComponentInMemory(IECaseComponent eCaseComponent) {
		// save within casecomponents in memory for better performance of player
		String lId = "" + eCaseComponent.getECase().getCasId();
		Hashtable<String,List<IECaseComponent>> lCaseComponentsInRunByCasId = (Hashtable)appManager.getAppContainer().get(AppConstants.caseComponentsInRunByCasId);
		if ((lCaseComponentsInRunByCasId != null) && (lCaseComponentsInRunByCasId.containsKey(lId))) {
			List<IECaseComponent> lCaseComponents = lCaseComponentsInRunByCasId.get(lId);
			if (lCaseComponents != null) {
				boolean lExists = false;
				for (int i = 0;i < lCaseComponents.size(); i++) {
					IECaseComponent lCaseComponent = (IECaseComponent)lCaseComponents.get(i);
					if (lCaseComponent.getCacId() == eCaseComponent.getCacId()) {
						lExists = true;
						lCaseComponents.set(i, eCaseComponent);
					}
				}
				if (!lExists)
					lCaseComponents.add(eCaseComponent);
			}
		}
		//NOTE Remove data tree references from application memory
		appManager.removeDataTreeReferencesFromAppMemory(eCaseComponent);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseComponentManager#newCaseComponent(nl.surf.emergo.domain.IECaseComponent)
	 */
	public List<String[]> newCaseComponent(IECaseComponent eCaseComponent) {
		// Validate item and if ok, save it after setting creationdate and lastupdatedate.
		List<String[]> lErrors = validateCaseComponent(eCaseComponent);
		if (lErrors == null) {
			eCaseComponent.setCreationdate(new Date());
			eCaseComponent.setLastupdatedate(new Date());
			saveCaseComponent(eCaseComponent);
		}
		return lErrors;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseComponentManager#updateCaseComponent(nl.surf.emergo.domain.IECaseComponent)
	 */
	public List<String[]> updateCaseComponent(IECaseComponent eCaseComponent) {
		// Validate item and if ok, save it after setting lastupdatedate.
		List<String[]> lErrors = validateCaseComponent(eCaseComponent);
		if (lErrors == null) {
			eCaseComponent.setLastupdatedate(new Date());
			saveCaseComponent(eCaseComponent);
		}
		return lErrors;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseComponentManager#validateCaseComponent(nl.surf.emergo.domain.IECaseComponent)
	 */
	public List<String[]> validateCaseComponent(IECaseComponent eCaseComponent) {
		/*
		 * Validate if name is not empty.
		 * Validate if name is unique.
		 * Validate if component is not null.
		 * Validate if owner account is not null.
		 * If validation error return it as element in String[] list.
		 */
		List<String[]> lErrors = new ArrayList<String[]>(0);

		if (appManager.isEmpty(eCaseComponent.getName()))
			appManager.addError(lErrors, "name", "error_empty");
		else {
			IECaseComponent lExistingCaseComponent = caseComponentDao
					.get(eCaseComponent.getCacId());
			// check if changed
			boolean lChanged = ((lExistingCaseComponent == null) || (!eCaseComponent
					.getName().equals(lExistingCaseComponent.getName())));
			if ((lChanged)
					&& (caseComponentDao.exists(eCaseComponent.getECase()
							.getCasId(), eCaseComponent.getName())))
				appManager.addError(lErrors, "name", "error_not_unique");
		}
		if (eCaseComponent.getEComponent() == null)
			appManager.addError(lErrors, "component", "error_no_choice");
		if (eCaseComponent.getEAccount() == null)
			appManager.addError(lErrors, "account", "error_no_choice");
		if (lErrors.size() > 0)
			return lErrors;
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseComponentManager#deleteCaseComponent(int)
	 */
	public void deleteCaseComponent(int cacId) {
		deleteCaseComponent(getCaseComponent(cacId));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseComponentManager#deleteCaseComponent(nl.surf.emergo.domain.IECaseComponent)
	 */
	public void deleteCaseComponent(IECaseComponent eCaseComponent) {
		// first delete possibly related blobs. they are not deleted in cascade.
		deleteBlobs(eCaseComponent);
		if (eCaseComponent.getLastupdatedate() == null)
			// lastupdatedate field is added later, so value can be empty for older records
			eCaseComponent.setLastupdatedate(eCaseComponent.getCreationdate());
		deleteCaseComponentInMemory(eCaseComponent);
		caseComponentDao.delete(eCaseComponent);
	}

	/**
	 * Deletes case component in application memory.
	 *
	 * @param eCaseComponent the e case component
	 */
	private void deleteCaseComponentInMemory(IECaseComponent eCaseComponent) {
		String lId = "" + eCaseComponent.getECase().getCasId();
		Hashtable<String,List<IECaseComponent>> lCaseComponentsInRunByCasId = (Hashtable)appManager.getAppContainer().get(AppConstants.caseComponentsInRunByCasId);
		if ((lCaseComponentsInRunByCasId != null) && (lCaseComponentsInRunByCasId.containsKey(lId))) {
			List<IECaseComponent> lCaseComponents = lCaseComponentsInRunByCasId.get(lId);
			if (lCaseComponents != null) {
				int lIndex = -1;
				for (int i = 0;i < lCaseComponents.size(); i++) {
					IECaseComponent lCaseComponent = (IECaseComponent)lCaseComponents.get(i);
					if (lCaseComponent.getCacId() == eCaseComponent.getCacId()) {
						lIndex = i;
					}
				}
				if (lIndex >= 0)
					lCaseComponents.remove(lIndex);
			}
		}
		//NOTE Remove data tree references from application memory
		appManager.removeDataTreeReferencesFromAppMemory(eCaseComponent);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseComponentManager#deleteBlobs(nl.surf.emergo.domain.IECaseComponent)
	 */
	public void deleteBlobs(IECaseComponent eCaseComponent) {
		// delete blobs referenced within xmldata property
		String lXmlDef = "";
		if (eCaseComponent != null) {
			IEComponent eComponent = eCaseComponent.getEComponent();
			if (eComponent != null)
				lXmlDef = eComponent.getXmldefinition();
		}
		String lXmlData = eCaseComponent.getXmldata();
		xmlManager.deleteBlobs(lXmlDef, lXmlData, AppConstants.data_tag);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseComponentManager#getAllCaseComponents()
	 */
	public List<IECaseComponent> getAllCaseComponents() {
		return caseComponentDao.getAll();
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseComponentManager#getAllCaseComponentsByCasId(int)
	 */
	public List<IECaseComponent> getAllCaseComponentsByCasId(int casId) {
		return caseComponentDao.getAllByCasId(casId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseComponentManager#getAllCaseComponentsByCasId(List)
	 */
	public List<IECaseComponent> getAllCaseComponentsByCasIds(List<Integer> casIds) {
		return caseComponentDao.getAllByCasIds(casIds);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseComponentManager#getAllCaseComponentsByComId(int)
	 */
	public List<IECaseComponent> getAllCaseComponentsByComId(int comId) {
		return caseComponentDao.getAllByComId(comId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseComponentManager#getAllCaseComponentsByCasIdComId(int, int)
	 */
	public List<IECaseComponent> getAllCaseComponentsByCasIdComId(int casId, int comId) {
		return caseComponentDao.getAllByCasIdComId(casId, comId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseComponentManager#getAllCaseComponentsByAccId(int)
	 */
	public List<IECaseComponent> getAllCaseComponentsByAccId(int accId) {
		return caseComponentDao.getAllByAccId(accId);
	}

}