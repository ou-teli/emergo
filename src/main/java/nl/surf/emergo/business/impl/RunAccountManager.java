/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;

import nl.surf.emergo.business.abstr.AbstractDaoBasedRunAccountManager;
import nl.surf.emergo.domain.ERunAccount;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunAccount;

public class RunAccountManager extends AbstractDaoBasedRunAccountManager implements InitializingBean {

	@Override
	public void afterPropertiesSet() throws Exception {
		// NOOP
	}

	@Override
	public IERunAccount getNewRunAccount() {
		return new ERunAccount();
	}

	@Override
	public IERunAccount getRunAccount(int ruaId) {
		return runAccountDao.get(ruaId);
	}

	@Override
	public void saveRunAccount(IERunAccount eRunAccount) {
		runAccountDao.save(eRunAccount);
	}

	@Override
	public List<String[]> newRunAccount(IERunAccount eRunAccount) {
		eRunAccount.setCreationdate(new Date());
		eRunAccount.setLastupdatedate(new Date());
		saveRunAccount(eRunAccount);
		return null;
	}

	@Override
	public List<String[]> updateRunAccount(IERunAccount eRunAccount) {
		eRunAccount.setLastupdatedate(new Date());
		saveRunAccount(eRunAccount);
		return null;
	}

	@Override
	public void deleteRunAccount(int ruaId) {
		deleteRunAccount(getRunAccount(ruaId));
	}

	@Override
	public void deleteRunAccount(IERunAccount eRunAccount) {
		runAccountDao.delete(eRunAccount);
	}

	@Override
	public List<IERunAccount> getAllRunAccounts() {
		return runAccountDao.getAll();
	}

	@Override
	public List<IERunAccount> getAllRunAccountsByRunId(int runId) {
		return runAccountDao.getAllByRunId(runId);
	}

	@Override
	public List<IERunAccount> getAllRunAccountsByAccId(int accId) {
		return runAccountDao.getAllByAccId(accId);
	}

	@Override
	public List<IERunAccount> getAllRunAccountsByAccIdActiveAsTutor(int accId) {
		return runAccountDao.getAllByAccIdTutor(accId, true);
	}

	@Override
	public List<IERunAccount> getAllRunAccountsByAccIdActiveAsStudent(int accId) {
		return runAccountDao.getAllByAccIdStudent(accId, true);
	}

	@Override
	public IERunAccount getRunAccountByRunIdAccId(int runId, int accId) {
		List<IERunAccount> lRuacs = runAccountDao.getAllByRunIdAccId(runId, accId);
		if ((lRuacs != null) && (lRuacs.size() == 1))
			return lRuacs.get(0);
		return null;
	}

	@Override
	public IERunAccount addRunAccount(int aRunId, int aAccId) {
		IERunAccount lRunaccount = getNewRunAccount();
		IEAccount lAcc = accountManager.getAccount(aAccId);
		IERun lRun = runManager.getRun(aRunId);
		lRunaccount.setEAccount(lAcc);
		lRunaccount.setERun(lRun);
		newRunAccount(lRunaccount);
		return lRunaccount;
	}

	@Override
	public IERunAccount addRunAccountIfNotExists(int aRunId, int aAccId, String aRole) {
		IERunAccount lRunaccount = getRunAccountByRunIdAccId(aRunId, aAccId);
		if (lRunaccount == null) {
			lRunaccount = addRunAccount(aRunId, aAccId);
			// if runaccount must be added, active for role will be true
			if (lRunaccount != null)
				setRunAccountStatus(lRunaccount, aRole, true);
		}
		return lRunaccount;
	}

	/**
	 * Sets runaccount status.
	 * 
	 * @param aRunaccount the run account
	 * @param aRole       the emergo role of the account for which the status is set
	 * @param aChecked    the value of the new status
	 * 
	 */
	@Override
	public void setRunAccountStatus(IERunAccount aRunaccount, String aRole, boolean aChecked) {
		if (aRunaccount != null) {
			if (aRole.equals(AppConstants.c_role_tut))
				aRunaccount.setTutactive(aChecked);
			else
				aRunaccount.setStuactive(aChecked);
			updateRunAccount(aRunaccount);
		}
	}

}