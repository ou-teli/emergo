/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business.impl;

import java.io.File;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;

import nl.surf.emergo.business.abstr.AbstractDaoBasedFileManager;
import nl.surf.emergo.utilities.FileHelper;

/**
 * The Class FileManager.
 */
public class FileManager extends AbstractDaoBasedFileManager implements InitializingBean {

	private FileHelper fileHelper = new FileHelper();

	@Override
	public void afterPropertiesSet() throws Exception {
		// NOOP
	}

	@Override
	public boolean createDir(String dir) {
		return fileHelper.createDir(dir);
	}

	@Override
	public boolean deleteDir(String dir) {
		return fileHelper.deleteDir(dir);
	}

	@Override
	public String createFile(String dir, String fileName, byte[] bytes) {
		return fileHelper.createFile(dir, fileName, bytes);
	}

	@Override
	public boolean deleteFile(String fileName) {
		return fileHelper.deleteFile(fileName);
	}

	@Override
	public boolean copyFile(String fromFile, String toFile) {
		return fileHelper.copyFile(fromFile, toFile);
	}

	@Override
	public File getFile(String fileName) {
		return fileHelper.getFile(fileName);
	}

	@Override
	public byte[] readFile(String fileName) {
		return fileHelper.readFile(fileName);
	}

	@Override
	public List<String> readTextFile(String fileName) {
		return fileHelper.readTextFile(fileName);
	}

	@Override
	public List<String> getFileNames(String dir) {
		return fileHelper.getFileNames(dir);
	}

	@Override
	public List<String> getFileNamesInPathSubTree(String dir) {
		return fileHelper.getFileNamesInPathSubTree(dir);
	}

	@Override
	public boolean fileExists(String fileName) {
		return fileHelper.fileExists(fileName);
	}

	@Override
	public boolean renameFile(String oldFileName, String newFileName) {
		return fileHelper.renameFile(oldFileName, newFileName);
	}

	@Override
	public File[] getFilesInDir(String dir) {
		return fileHelper.getFilesInDir(dir);
	}

}