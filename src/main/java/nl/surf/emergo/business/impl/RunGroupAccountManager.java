/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;

import nl.surf.emergo.business.abstr.AbstractDaoBasedRunGroupAccountManager;
import nl.surf.emergo.domain.ERunGroupAccount;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;

/**
 * The Class RunGroupAccountManager.
 */
public class RunGroupAccountManager extends AbstractDaoBasedRunGroupAccountManager implements InitializingBean {

	@Override
	public void afterPropertiesSet() throws Exception {
		// NOOP
	}

	@Override
	public IERunGroupAccount getNewRunGroupAccount() {
		return new ERunGroupAccount();
	}

	@Override
	public IERunGroupAccount getRunGroupAccount(int rgaId) {
		return runGroupAccountDao.get(rgaId);
	}

	@Override
	public IERunGroupAccount getTestRunGroupAccount(IERunGroup eRunGroup, IEAccount eAccount) {
		List<IERunGroupAccount> lItems = getAllRunGroupAccountsByRugIdAccId(eRunGroup.getRugId(), eAccount.getAccId());
		IERunGroupAccount testRunGroupAccount = null;
		if ((lItems != null) && (!lItems.isEmpty())) {
			// only one test rungroupaccount per rungroup
			testRunGroupAccount = lItems.get(0);
		}
		if (testRunGroupAccount == null) {
			// create new one
			testRunGroupAccount = getNewRunGroupAccount();
			testRunGroupAccount.setERunGroup(eRunGroup);
			testRunGroupAccount.setEAccount(eAccount);
			newRunGroupAccount(testRunGroupAccount);
			testRunGroupAccount = getRunGroupAccount(testRunGroupAccount.getRgaId());
		}
		return testRunGroupAccount;
	}

	@Override
	public void saveRunGroupAccount(IERunGroupAccount eRunGroupAccount) {
		runGroupAccountDao.save(eRunGroupAccount);
	}

	@Override
	public List<String[]> newRunGroupAccount(IERunGroupAccount eRunGroupAccount) {
		eRunGroupAccount.setCreationdate(new Date());
		eRunGroupAccount.setLastupdatedate(new Date());
		saveRunGroupAccount(eRunGroupAccount);
		return null;
	}

	@Override
	public List<String[]> updateRunGroupAccount(IERunGroupAccount eRunGroupAccount) {
		eRunGroupAccount.setLastupdatedate(new Date());
		saveRunGroupAccount(eRunGroupAccount);
		return null;
	}

	@Override
	public void deleteRunGroupAccount(int rgaId) {
		deleteRunGroupAccount(getRunGroupAccount(rgaId));
	}

	@Override
	public void deleteRunGroupAccount(IERunGroupAccount eRunGroupAccount) {
		if (eRunGroupAccount.getLastupdatedate() == null)
			// lastupdatedate field is added later, so value can be empty for older records
			eRunGroupAccount.setLastupdatedate(eRunGroupAccount.getCreationdate());
		runGroupAccountDao.delete(eRunGroupAccount);
	}

	@Override
	public List<IERunGroupAccount> getAllRunGroupAccounts() {
		return runGroupAccountDao.getAll();
	}

	@Override
	public List<IERunGroupAccount> getAllRunGroupAccountsByRgaIds(List<Integer> rgaIds) {
		return runGroupAccountDao.getAllByRgaIds(rgaIds);
	}

	@Override
	public List<IERunGroupAccount> getAllRunGroupAccountsByRugId(int rugId) {
		return runGroupAccountDao.getAllByRugId(rugId);
	}

	@Override
	public List<IERunGroupAccount> getAllRunGroupAccountsByRugIds(List<Integer> rugIds) {
		return runGroupAccountDao.getAllByRugIds(rugIds);
	}

	@Override
	public List<IERunGroupAccount> getAllRunGroupAccountsByAccId(int accId) {
		return runGroupAccountDao.getAllByAccId(accId);
	}

	@Override
	public List<IERunGroupAccount> getAllRunGroupAccountsByAccIds(List<Integer> accIds) {
		return runGroupAccountDao.getAllByAccIds(accIds);
	}

	@Override
	public List<IERunGroupAccount> getAllRunGroupAccountsByRugIdAccId(int rugId, int accId) {
		return runGroupAccountDao.getAllByRugIdAccId(rugId, accId);
	}
}