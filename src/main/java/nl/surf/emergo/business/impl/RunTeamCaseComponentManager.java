/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;

import nl.surf.emergo.business.abstr.AbstractDaoBasedRunTeamCaseComponentManager;
import nl.surf.emergo.domain.ERunTeamCaseComponent;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IEComponent;
import nl.surf.emergo.domain.IERunTeamCaseComponent;

/**
 * The Class RunTeamCaseComponentManager.
 */
public class RunTeamCaseComponentManager extends AbstractDaoBasedRunTeamCaseComponentManager
		implements InitializingBean {

	@Override
	public void afterPropertiesSet() throws Exception {
		// NOOP
	}

	@Override
	public IERunTeamCaseComponent getNewRunTeamCaseComponent() {
		IERunTeamCaseComponent eRunTeamCaseComponent = new ERunTeamCaseComponent();
		return eRunTeamCaseComponent;
	}

	@Override
	public IERunTeamCaseComponent getRunTeamCaseComponent(int rtcId) {
		IERunTeamCaseComponent eRunTeamCaseComponent = runTeamCaseComponentDao.get(rtcId);
		return eRunTeamCaseComponent;
	}

	@Override
	public IERunTeamCaseComponent getRunTeamCaseComponent(int rutId, int cacId) {
		IERunTeamCaseComponent eRunTeamCaseComponent = runTeamCaseComponentDao.get(rutId, cacId);
		return eRunTeamCaseComponent;
	}

	@Override
	public void saveRunTeamCaseComponent(IERunTeamCaseComponent eRunTeamCaseComponent) {
		runTeamCaseComponentDao.save(eRunTeamCaseComponent);
	}

	@Override
	public List<String[]> newRunTeamCaseComponent(IERunTeamCaseComponent eRunTeamCaseComponent) {
		eRunTeamCaseComponent.setCreationdate(new Date());
		eRunTeamCaseComponent.setLastupdatedate(new Date());
		saveRunTeamCaseComponent(eRunTeamCaseComponent);
		return null;
	}

	@Override
	public List<String[]> updateRunTeamCaseComponent(IERunTeamCaseComponent eRunTeamCaseComponent) {
		eRunTeamCaseComponent.setLastupdatedate(new Date());
		saveRunTeamCaseComponent(eRunTeamCaseComponent);
		return null;
	}

	@Override
	public void deleteRunTeamCaseComponent(int rtcId) {
		deleteRunTeamCaseComponent(getRunTeamCaseComponent(rtcId));
	}

	@Override
	public void deleteRunTeamCaseComponent(IERunTeamCaseComponent eRunTeamCaseComponent) {
		// first delete possibly related blobs. they are not deleted in cascade.
		deleteBlobs(eRunTeamCaseComponent);
		if (eRunTeamCaseComponent.getLastupdatedate() == null)
			// lastupdatedate field is added later, so value can be empty for older records
			eRunTeamCaseComponent.setLastupdatedate(eRunTeamCaseComponent.getCreationdate());
		runTeamCaseComponentDao.delete(eRunTeamCaseComponent);
	}

	@Override
	public void deleteBlobs(IERunTeamCaseComponent eRunTeamCaseComponent) {
		// delete blobs referenced within xmldata property
		String lXmlDef = "";
		IECaseComponent eCaseComponent = caseComponentManager.getCaseComponent(eRunTeamCaseComponent.getCacCacId());
		if (eCaseComponent != null) {
			IEComponent eComponent = eCaseComponent.getEComponent();
			if (eComponent != null)
				lXmlDef = eComponent.getXmldefinition();
		}
		String lXmlData = eRunTeamCaseComponent.getXmldata();
		xmlManager.deleteBlobs(lXmlDef, lXmlData, AppConstants.status_tag);
	}

	@Override
	public List<IERunTeamCaseComponent> getAllRunTeamCaseComponents() {
		return runTeamCaseComponentDao.getAll();
	}

	@Override
	public List<IERunTeamCaseComponent> getAllRunTeamCaseComponentsByRutId(int rutId) {
		return runTeamCaseComponentDao.getAllByRutId(rutId);
	}

	@Override
	public List<IERunTeamCaseComponent> getAllRunTeamCaseComponentsByCacId(int cacId) {
		return runTeamCaseComponentDao.getAllByCacId(cacId);
	}

	@Override
	public List<IERunTeamCaseComponent> getAllRunTeamCaseComponentsByRutIdCacId(int rutId, int cacId) {
		return runTeamCaseComponentDao.getAllByRutIdCacId(rutId, cacId);
	}

}