/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;

import nl.surf.emergo.business.abstr.AbstractDaoBasedContextManager;
import nl.surf.emergo.domain.EContext;
import nl.surf.emergo.domain.IEContext;

/**
 * The Class ContextManager.
 */
public class ContextManager extends AbstractDaoBasedContextManager implements InitializingBean {

	@Override
	public void afterPropertiesSet() throws Exception {
		// NOOP
	}

	@Override
	public IEContext getNewContext() {
		return new EContext();
	}

	@Override
	public IEContext getContext(int conId) {
		IEContext eContext = contextDao.get(conId);
		return eContext;
	}

	@Override
	public void saveContext(IEContext eContext) {
		contextDao.save(eContext);
	}

	@Override
	public List<String[]> newContext(IEContext eContext) {
		// Validate item and if ok, save it after setting creationdate and
		// lastupdatedate.
		List<String[]> lErrors = validateContext(eContext);
		if (lErrors == null) {
			eContext.setCreationdate(new Date());
			eContext.setLastupdatedate(new Date());
			saveContext(eContext);
		}
		return lErrors;
	}

	@Override
	public List<String[]> updateContext(IEContext eContext) {
		// Validate item and if ok, save it after setting lastupdatedate.
		List<String[]> lErrors = validateContext(eContext);
		if (lErrors == null) {
			eContext.setLastupdatedate(new Date());
			saveContext(eContext);
		}
		return lErrors;
	}

	@Override
	public List<String[]> validateContext(IEContext eContext) {
		/*
		 * Validate if context is not empty. Validate if context is unique. If
		 * validation error return it as element in String[] list.
		 */
		List<String[]> lErrors = new ArrayList<>(0);
		if (appManager.isEmpty(eContext.getContext()))
			appManager.addError(lErrors, "context", "error_empty");
		else {
			IEContext lExistingContext = contextDao.get(eContext.getConId());
			// check if changed
			boolean lChanged = ((lExistingContext == null)
					|| (!eContext.getContext().equals(lExistingContext.getContext())));
			if ((lChanged) && (contextDao.exists(eContext.getContext())))
				appManager.addError(lErrors, "context", "error_not_unique");
		}
		return lErrors.isEmpty() ? null : lErrors;
	}

	@Override
	public void deleteContext(int conId) {
		deleteContext(getContext(conId));
	}

	@Override
	public void deleteContext(IEContext eContext) {
		contextDao.delete(eContext);
	}

	@Override
	public void flushContext() {
		contextDao.flush();
	}

	@Override
	public void clearContext() {
		contextDao.clear();
	}

	@Override
	public List<IEContext> getAllContexts() {
		return contextDao.getAll();
	}

	@Override
	public List<IEContext> getAllContexts(boolean active) {
		return contextDao.getAll(active);
	}

}