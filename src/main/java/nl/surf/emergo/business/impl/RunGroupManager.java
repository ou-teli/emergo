/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;

import nl.surf.emergo.business.abstr.AbstractDaoBasedRunGroupManager;
import nl.surf.emergo.domain.ERunGroup;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupCaseComponent;

/**
 * The Class RunGroupManager.
 */
public class RunGroupManager extends AbstractDaoBasedRunGroupManager implements InitializingBean {

	@Override
	public void afterPropertiesSet() throws Exception {
		// NOOP
	}

	@Override
	public IERunGroup getNewRunGroup() {
		return new ERunGroup();
	}

	@Override
	public IERunGroup getRunGroup(int rugId) {
		return runGroupDao.get(rugId);
	}

	@Override
	public IERunGroup getTestRunGroup(IERun eRun, IECaseRole eCaseRole) {
		List<IERunGroup> lItems = getAllRunGroupsByRunIdCarId(eRun.getRunId(), eCaseRole.getCarId());
		// give silly name to distinguish from other run groups
		String lTestRunGroupName = "testtesttest_" + eCaseRole.getName();
		IERunGroup testRunGroup = null;
		if (lItems != null) {
			for (IERunGroup lRunGroup : lItems) {
				if (lRunGroup.getName().equals(lTestRunGroupName))
					testRunGroup = lRunGroup;
			}
		}
		if (testRunGroup == null) {
			// create new one
			testRunGroup = getNewRunGroup();
			testRunGroup.setERun(eRun);
			testRunGroup.setECaseRole(eCaseRole);
			testRunGroup.setName(lTestRunGroupName);
			testRunGroup.setComposite(false);
			testRunGroup.setActive(true);
			newRunGroup(testRunGroup);
			testRunGroup = getRunGroup(testRunGroup.getRugId());
		}
		return testRunGroup;
	}

	@Override
	public void saveRunGroup(IERunGroup eRunGroup) {
		runGroupDao.save(eRunGroup);
	}

	@Override
	public List<String[]> newRunGroup(IERunGroup eRunGroup) {
		// Validate item and if ok, save it after setting creationdate and
		// lastupdatedate.
		List<String[]> lErrors = validateRunGroup(eRunGroup);
		if (lErrors == null) {
			eRunGroup.setActive(true);
			eRunGroup.setCreationdate(new Date());
			eRunGroup.setLastupdatedate(new Date());
			saveRunGroup(eRunGroup);
		}
		return lErrors;
	}

	@Override
	public List<String[]> updateRunGroup(IERunGroup eRunGroup) {
		// Validate item and if ok, save it after setting lastupdatedate.
		List<String[]> lErrors = validateRunGroup(eRunGroup);
		if (lErrors == null) {
			eRunGroup.setLastupdatedate(new Date());
			saveRunGroup(eRunGroup);
		}
		return lErrors;
	}

	@Override
	public List<String[]> validateRunGroup(IERunGroup eRunGroup) {
		/*
		 * Validate if name is not empty. Validate if name is unique. If validation
		 * error return it as element in String[] list.
		 */
		List<String[]> lErrors = new ArrayList<>(0);
		if (appManager.isEmpty(eRunGroup.getName()))
			appManager.addError(lErrors, "name", "error_empty");
		else {
			IERunGroup lExistingRunGroup = runGroupDao.get(eRunGroup.getRugId());
			// check if changed
			boolean lChanged = ((lExistingRunGroup == null)
					|| (!eRunGroup.getName().equals(lExistingRunGroup.getName())));
			if ((lChanged) && (runGroupDao.exists(eRunGroup.getECaseRole().getCarId(), eRunGroup.getERun().getRunId(),
					eRunGroup.getName())))
				appManager.addError(lErrors, "name", "error_not_unique");
		}
		return lErrors.isEmpty() ? null : lErrors;
	}

	@Override
	public void deleteRunGroup(int rugId) {
		deleteRunGroup(getRunGroup(rugId));
	}

	@Override
	public void deleteRunGroup(IERunGroup eRunGroup) {
		// first delete possibly related blobs. they are not deleted in cascade.
		deleteBlobs(eRunGroup);
		if (eRunGroup.getLastupdatedate() == null)
			// lastupdatedate field is added later, so value can be empty for older records
			eRunGroup.setLastupdatedate(eRunGroup.getCreationdate());
		runGroupDao.delete(eRunGroup);
	}

	@Override
	public void deleteBlobs(IERunGroup eRunGroup) {
		// delete blobs for all run group case components referenced by run group
		List<IERunGroupCaseComponent> lItems = runGroupCaseComponentManager
				.getAllRunGroupCaseComponentsByRugId(eRunGroup.getRugId());
		if (lItems != null) {
			for (IERunGroupCaseComponent lItem : lItems)
				runGroupCaseComponentManager.deleteBlobs(lItem);
		}
	}

	@Override
	public List<IERunGroup> getAllRunGroups() {
		return runGroupDao.getAll();
	}

	@Override
	public List<IERunGroup> getAllRunGroupsByRunId(int runId) {
		return runGroupDao.getAllByRunId(runId);
	}

	@Override
	public List<IERunGroup> getAllRunGroupsByCarId(int carId) {
		return runGroupDao.getAllByCarId(carId);
	}

	@Override
	public List<IERunGroup> getAllRunGroupsByRunIdCarId(int runId, int carId) {
		return runGroupDao.getAllByRunIdCarId(runId, carId);
	}

	@Override
	public boolean runGroupExists(int carId, int runId, String name) {
		return runGroupDao.exists(carId, runId, name);
	}
}