/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;

import nl.surf.emergo.business.abstr.AbstractDaoBasedRunTeamManager;
import nl.surf.emergo.domain.ERunTeam;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunTeam;
import nl.surf.emergo.domain.IERunTeamCaseComponent;

/**
 * The Class RunTeamManager.
 */
public class RunTeamManager extends AbstractDaoBasedRunTeamManager implements InitializingBean {

	@Override
	public void afterPropertiesSet() throws Exception {
		// NOOP
	}

	@Override
	public IERunTeam getNewRunTeam() {
		return new ERunTeam();
	}

	@Override
	public IERunTeam getRunTeam(int rutId) {
		return runTeamDao.get(rutId);
	}

	@Override
	public IERunTeam getTestRunTeam(IERun eRun) {
		List<IERunTeam> lItems = getAllRunTeamsByRunId(eRun.getRunId());
		// give silly name to distinguish from other run teams
		String lTestRunTeamName = "testtesttest_";
		IERunTeam testRunTeam = null;
		if (lItems != null) {
			for (IERunTeam lRunTeam : lItems) {
				if (lRunTeam.getName().equals(lTestRunTeamName))
					testRunTeam = lRunTeam;
			}
		}
		if (testRunTeam == null) {
			// create new one
			testRunTeam = getNewRunTeam();
			testRunTeam.setERun(eRun);
			testRunTeam.setName(lTestRunTeamName);
			testRunTeam.setActive(true);
			newRunTeam(testRunTeam);
			testRunTeam = getRunTeam(testRunTeam.getRutId());
		}
		return testRunTeam;
	}

	@Override
	public void saveRunTeam(IERunTeam eRunTeam) {
		runTeamDao.save(eRunTeam);
	}

	@Override
	public List<String[]> newRunTeam(IERunTeam eRunTeam) {
		// Validate item and if ok, save it after setting creationdate and
		// lastupdatedate.
		List<String[]> lErrors = validateRunTeam(eRunTeam);
		if (lErrors == null) {
			eRunTeam.setActive(true);
			eRunTeam.setCreationdate(new Date());
			eRunTeam.setLastupdatedate(new Date());
			saveRunTeam(eRunTeam);
		}
		return lErrors;
	}

	@Override
	public List<String[]> updateRunTeam(IERunTeam eRunTeam) {
		// Validate item and if ok, save it after setting lastupdatedate.
		List<String[]> lErrors = validateRunTeam(eRunTeam);
		if (lErrors == null) {
			eRunTeam.setLastupdatedate(new Date());
			saveRunTeam(eRunTeam);
		}
		return lErrors;
	}

	@Override
	public List<String[]> validateRunTeam(IERunTeam eRunTeam) {
		/*
		 * Validate if name is not empty. Validate if name is unique. If validation
		 * error return it as element in String[] list.
		 */
		List<String[]> lErrors = new ArrayList<>(0);
		if (appManager.isEmpty(eRunTeam.getName()))
			appManager.addError(lErrors, "name", "error_empty");
		else {
			IERunTeam lExistingRunTeam = runTeamDao.get(eRunTeam.getRutId());
			// check if changed
			boolean lChanged = ((lExistingRunTeam == null) || (!eRunTeam.getName().equals(lExistingRunTeam.getName())));
			if ((lChanged) && (runTeamDao.exists(eRunTeam.getERun().getRunId(), eRunTeam.getName())))
				appManager.addError(lErrors, "name", "error_not_unique");
		}
		return lErrors.isEmpty() ? null : lErrors;
	}

	@Override
	public void deleteRunTeam(int rutId) {
		deleteRunTeam(getRunTeam(rutId));
	}

	@Override
	public void deleteRunTeam(IERunTeam eRunTeam) {
		// first delete possibly related blobs. they are not deleted in cascade.
		deleteBlobs(eRunTeam);
		if (eRunTeam.getLastupdatedate() == null)
			// lastupdatedate field is added later, so value can be empty for older records
			eRunTeam.setLastupdatedate(eRunTeam.getCreationdate());
		runTeamDao.delete(eRunTeam);
	}

	@Override
	public void deleteBlobs(IERunTeam eRunTeam) {
		// delete blobs for all run team case components referenced by run team
		List<IERunTeamCaseComponent> lItems = runTeamCaseComponentManager
				.getAllRunTeamCaseComponentsByRutId(eRunTeam.getRutId());
		if (lItems != null) {
			for (IERunTeamCaseComponent lItem : lItems)
				runTeamCaseComponentManager.deleteBlobs(lItem);
		}
	}

	@Override
	public List<IERunTeam> getAllRunTeams() {
		return runTeamDao.getAll();
	}

	@Override
	public List<IERunTeam> getAllRunTeamsByRunId(int runId) {
		return runTeamDao.getAllByRunId(runId);
	}
}