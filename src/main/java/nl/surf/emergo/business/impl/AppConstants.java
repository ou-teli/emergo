/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business.impl;

import java.util.Arrays;
import java.util.List;

/**
 * The Class AppConstants.
 *
 * <p>
 * Contains application constants and.
 * </p>
 */
public class AppConstants {

	/** Prevents instantiation */
	private AppConstants() {
	}

	/** String variables for inserting labels within other labels or strings. */
	public static final String labelVarPrefix = "[#";
	public static final String labelVarPostfix = "#]";
	public static final String labelVarPrefixC = "[C#";
	public static final String labelVarPostfixC = "#C]";
	public static final String sessionVarPrefix = "[!";
	public static final String sessionVarPostfix = "!]";

	/** The Constant syskey_smtpserver. A db key containing the mail server. */
	public static final String syskey_smtpserver = "smtpserver";

	/**
	 * The Constant syskey_smtpnoreplysender. A db key containing the noreply sender
	 * address.
	 */
	public static final String syskey_smtpnoreplysender = "smtpnoreplysender";

	/** The Constant syskey_gmapskey. A db key containing the gmapskey value. */
	public static final String syskey_gmapskey = "gmapskey";

	/** The Constant account_filter_map. Used to keep info to filter accounts. */
	public static final String account_filter_map = "account_filter_map";

	/** The Constant selected_accounts. Used to keep selected accounts. */
	public static final String selected_accounts = "selected_accounts";

	/** The Constant run_filter_map. Used to keep info to filter runs. */
	public static final String run_filter_map = "run_filter_map";

	/** The Constant selected_runs. Used to keep selected runs. */
	public static final String selected_runs = "selected_runs";

	/** The Constant case_filter_map. Used to keep info to filter cases. */
	public static final String case_filter_map = "case_filter_map";

	/** The Constant selected_cases. Used to keep selected cases. */
	public static final String selected_cases = "selected_cases";

	/** The Constant listbox_user_settings. Used to keep listbox user settings. */
	public static final String listbox_user_settings = "listbox_user_settings";

	/** The Constant grid_user_settings. Used to keep grid user settings. */
	public static final String grid_user_settings = "grid_user_settings";

	/**
	 * The Constant functional_component. A functional component is visible to a
	 * case developer.
	 */
	public static final int functional_component = 1;

	/**
	 * The Constant system_component. A system component is not visible to a case
	 * developer and is used to handle system tasks or store system data.
	 */
	public static final int system_component = 2;

	/**
	 * The Constant case_status_under_construction. A case can be under
	 * construction. Not used yet.
	 */
	public static final int case_status_under_construction = 0;

	/**
	 * The Constant case_status_template. A case can be a template case. Not used
	 * yet.
	 */
	public static final int case_status_template = 1;

	/** The Constant case_status_runnable. A normal case. */
	public static final int case_status_runnable = 2;

	/** The Constant case_status_locked. A case can be locked. Not used yet. */
	public static final int case_status_locked = 3;

	/**
	 * The Constant run_status_under_construction. A run can be under construction.
	 * Not used yet.
	 */
	public static final int run_status_under_construction = 0;

	/**
	 * The Constant run_status_template. A run can be a template run. Not used yet.
	 */
	public static final int run_status_template = 1;

	/** The Constant run_status_runnable. A normal run. */
	public static final int run_status_runnable = 2;

	/** The Constant run_status_test. A test run. Is used for preview option. */
	public static final int run_status_test = 3;

	/** The Constant dataElement. Used within xml. */
	public static final String dataElement = "data";

	/** The Constant rootElement. Used within xml. */
	public static final String rootElement = "root";

	/** The Constant componentElement. Used within xml. */
	public static final String componentElement = "component";

	/** The Constant contentElement. Used within xml. */
	public static final String contentElement = "content";

	/** The Constant statusElement. Used within xml. */
	public static final String statusElement = "status";

	/** The Constant initialstatusElement. Used within xml. */
	public static final String initialstatusElement = "initialstatus";

	/** The Constant getstatusElement. Used within xml. */
	public static final String getstatusElement = "getstatus";

	/** The Constant setstatusElement. Used within xml. */
	public static final String setstatusElement = "setstatus";

	/** The Constant reftagElement. Used within xml. */
	public static final String reftagElement = "reftag";

	/** Constants for different blob types. */
	public static final String blobtypeDatabase = "database";
	public static final String blobtypeIntUrl = "int_url";
	public static final String blobtypeExtUrl = "ext_url";

	/**
	 * The Constant nodeChildTagsWithBlob. All node child tags which have blobs
	 * within them.
	 */
	// NOTE possibly add new values if XML definition is added or changed
	public static final List<String> nodeChildTagsWithBlob = Arrays.asList("blob", "picture", "illustration",
			"pictureactive", "pictureinactive", "picturehover", "pictureselected", "picturelogo", "overlay", "package",
			"feedbackbutton");

	/**
	 * The Constant nodeChildTagsWithText. All node child tags which have a text
	 * within them.
	 */
	// NOTE possibly add new values if XML definition is added or changed
	public static final List<String> nodeChildTagsWithText = Arrays.asList("pid", "name", "text", "richtext",
			"hovertext", "title", "inbox", "motivation", "description", "shorttitle");

	/**
	 * The Constant nonNodeChildTagsToOmit. All node child tags that should be
	 * omitted when creating a new data node tag.
	 */
	public static final List<String> nodeChildTagsToOmit = Arrays.asList(initialstatusElement, getstatusElement,
			setstatusElement);

	/**
	 * The Constant nodeChildTagsWithTextToExport. All node child tags that may be
	 * exported.
	 */
	// NOTE possibly add new values if XML definition is added or changed
	public static final String nodeChildTagsWithTextToExport = "name|title|shorttitle|text|defaulttext|richtext|hovertext|description|inbox|correctanswer|options|label|unit|correctoptions|instruction|learningactivity";

	/** The Constant emptyXml. Used for giving xmldata initial value. */
	public static final String emptyXml = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>" + "<" + dataElement + ">"
			+ "<" + componentElement + ">" + "<" + statusElement + ">" + "</" + statusElement + ">" + "</"
			+ componentElement + ">" + "<" + contentElement + " id=\"1\" max_id=\"1\"/>" + "</" + dataElement + ">";

	/** The Constant noDrop. Used for drag and drop of tree items. */
	public static final int noDrop = 0;

	/**
	 * The Constant siblingDrop. Used for drag and drop of tree items. A sibling
	 * item should be made.
	 */
	public static final int siblingDrop = 1;

	/**
	 * The Constant childDrop. Used for drag and drop of tree items. A child item
	 * should be made.
	 */
	public static final int childDrop = 2;

	/** The Constant siblingOrChildDrop. Used for drag and drop of tree items. */
	public static final int siblingOrChildDrop = 3;

	/** The Constant c_role_adm. Admin code. */
	public static final String c_role_adm = "adm";

	/** The Constant c_role_cde. Case developer code. */
	public static final String c_role_cde = "cde";

	/** The Constant c_role_crm. Case role manager code. */
	public static final String c_role_crm = "crm";

	/** The Constant c_role_tut. Tutor code. */
	public static final String c_role_tut = "tut";

	/** The Constant c_role_stu. Student code. */
	public static final String c_role_stu = "stu";

	/**
	 * The Constant statusTypeRun. Status of all run groups for a specific case
	 * component is saved in run case component status.
	 */
	public static final String statusTypeRun = "run";

	/**
	 * The Constant statusTypeRunGroup. Status for a specific case component is
	 * privately saved per run group in run group case component status.
	 */
	public static final String statusTypeRunGroup = "rungroup";

	/**
	 * The Constant statusTypeRunTeam. Status of run team members for a specific
	 * case component is saved in run team case component status.
	 */
	public static final String statusTypeRunTeam = "runteam";

	/** The Constant definition_tag. */
	public static final int definition_tag = 0;

	/** The Constant data_tag. */
	public static final int data_tag = 1;

	/** The Constant status_tag. */
	public static final int status_tag = 2;

	/** The Constant status_tag. */
	public static final int other_tag = 3;

	/** The Constant defKeyType. Key of type attribute. */
	public static final String defKeyType = "type";

	/** The Constant defKeyKey. Key of key attribute. */
	public static final String defKeyKey = "key";

	/** The Constant defKeyMultiple. Key of multiple attribute. */
	public static final String defKeyMultiple = "multiple";

	/** The Constant defKeyNpc. Key of npc attribute. */
	public static final String defKeyNpc = "npc";

	/** The Constant defKeyPrivate. Key of private attribute. */
	public static final String defKeyPrivate = "private";

	/** The Constant defKeyHidden. Key of hidden attribute. */
	public static final String defKeyHidden = "hidden";

	/** The Constant defKeyAttributes. Key of attributes attribute. */
	public static final String defKeyAttributes = "attributes";

	/** The Constant defKeySingleopen. Key of single open attribute. */
	public static final String defKeySingleopen = "singleopen";

	/** The Constant defKeyChildnodes. Key of child nodes attribute. */
	public static final String defKeyChildnodes = "childnodes";

	/**
	 * The Constant defKeyMaxnumberofchildnodes. Key of max number of child nodes
	 * attribute.
	 */
	public static final String defKeyMaxnumberofchildnodes = "maxnumberofchildnodes";

	/** The Constant defKeyNotempty. Key of not empty attribute. */
	public static final String defKeyNotempty = "notempty";

	/** The Constant defKeyItems. Key of items attribute. */
	public static final String defKeyItems = "items";

	/** The Constant defKeyBlobtypes. Key of blob types attribute. */
	public static final String defKeyBlobtypes = "blobtypes";

	/** The Constant defKeyMediatypes. Key of media types attribute. */
	public static final String defKeyMediatypes = "mediatypes";

	/** The Constant defKeySubtype. Key of sub type attribute. */
	public static final String defKeySubtype = "subtype";

	/** The Constant defKeyReftype. Key of reference type attribute. */
	public static final String defKeyReftype = "reftype";

	/** The Constant defKeyRefcomp. Key of reference component attribute. */
	public static final String defKeyRefcomp = "refcomp";

	/** The Constant defKeyReftag. Key of reference tag attribute. */
	public static final String defKeyReftag = "reftag";

	/** The Constant defKeyReftagcontent. Key of reference tag content attribute. */
	public static final String defKeyReftagcontent = "reftagcontent";

	/** The Constant defKeyPreview. Key of preview attribute. */
	public static final String defKeyPreview = "preview";

	/** The Constant defKeyNotempty. Key of not empty attribute. */
	public static final String defKeyChoiceempty = "choiceempty";

	/** The Constant defKeyNotempty. Key of not empty attribute. */
	public static final String defKeyTagChoiceempty = "tagchoiceempty";

	/** The Constant defKeyImageprefix. Key of imageprefix attribute. */
	public static final String defKeyImageprefix = "imageprefix";

	/**
	 * The Constant defValueReftypeAllScriptTags. Value of ref type for all script
	 * tags.
	 */
	public static final String defValueReftypeAllScriptTags = "all_scripttags";

	/**
	 * The Constant defValueReftypeAllTemplateScriptTags. Value of ref type for all
	 * template tags.
	 */
	public static final String defValueReftypeAllTemplateScriptTags = "all_templatescripttags";

	/** The Constant defValueNode. Value node of type attribute. */
	public static final String defValueNode = "node";

	/** The Constant keyId. Key of tag id attribute. */
	public static final String keyId = "id";

	/** The Constant keyRefcacid. Key of reference cac id. */
	public static final String keyRefcacid = "refcacid";

	/** The Constant keyRefdataid. Key of reference data id attribute. */
	public static final String keyRefdataid = "refdataid";

	/**
	 * The Constant keyRefdatatemplate. Key of reference data template attribute.
	 */
	public static final String keyRefdatatemplate = "refdatatemplate";

	/** The Constant keyRefstatusid. Key of reference status id attribute. */
	public static final String keyRefstatusid = "refstatusid";

	/** The Constant keyRefstatusids. Key of reference status ids attribute. */
	public static final String keyRefstatusids = "refstatusids";

	/** The Constant keyDataparentid. Key of data parent id attribute. */
	public static final String keyDataparentid = "dataparentid";

	/** The Constant keyStatusparentid. Key of status parent id attribute. */
	public static final String keyStatusparentid = "statusparentid";

	/** The Constant keyDatabeforeid. Key of data before id attribute. */
	public static final String keyDatabeforeid = "databeforeid";

	/** The Constant keyStatusbeforeid. Key of status before id attribute. */
	public static final String keyStatusbeforeid = "statusbeforeid";

	/** The Constant keyExtended. Key of extended attribute. */
	public static final String keyExtended = "extended";

	/** The Constant keyRefdataid. Key of reference original id attribute. */
	public static final String keyReforiginalid = "reforiginalid";

	/** The Constant keyMax_id. Key of max_id attribute. */
	public static final String keyMax_id = "max_id";

	/** The Constant keyBlobtype. Key of blob type attribute. */
	public static final String keyBlobtype = "blobtype";

	/** The Constant keyMediatype. Key of media type attribute. */
	public static final String keyMediatype = "mediatype";

	/** The Constant keyTempnumber. Key of temp number attribute. */
	public static final String keyTempnumber = "tempnumber";

	/** The Constant keyExtendedid. Key of extended id attribute. */
	public static final String keyExtendedid = "extendedid";

	/**
	 * The Constant keyOwner_rgaid. Key of owner_rga id attribute. Rga id is db id
	 * of RunGroupAccount.
	 */
	public static final String keyOwner_rgaid = "owner_rgaid";

	/**
	 * The Constant nodeChildKeyTagContentName. Key of tagcontentname attribute of
	 * node child.
	 */
	public static final String nodeChildKeyTagContentName = "key_tagcontentname";

	/**
	 * Status keys. Strings are stored within content and status of case components.
	 * An exception is the case case component where numbers are stored instead of
	 * strings.
	 */
	public static final String statusKeyStartTime = "starttime";
	public static final int statusKeyStartTimeIndex = 1;

	public static final String statusKeyCurrentTime = "currenttime";
	public static final int statusKeyCurrentTimeIndex = 2;

	public static final String statusKeyPresent = "present";
	public static final int statusKeyPresentIndex = 3;

	public static final String statusKeyAccessible = "accessible";
	public static final int statusKeyAccessibleIndex = 4;

	public static final String statusKeyOpened = "opened";
	public static final int statusKeyOpenedIndex = 5;

	public static final String statusKeySelected = "selected";
	public static final int statusKeySelectedIndex = 6;

	public static final String statusKeyOutfolded = "outfolded";
	public static final int statusKeyOutfoldedIndex = 7;

	public static final String statusKeyOutfoldable = "outfoldable";
	public static final int statusKeyOutfoldableIndex = 8;

	public static final String statusKeyRequired = "required";
	public static final int statusKeyRequiredIndex = 9;

	public static final String statusKeyFinished = "finished";
	public static final int statusKeyFinishedIndex = 10;

	public static final String statusKeyCanceled = "canceled";
	public static final int statusKeyCanceledIndex = 11;

	public static final String statusKeyScore = "score";
	public static final int statusKeyScoreIndex = 12;

	public static final String statusKeySent = "sent";
	public static final int statusKeySentIndex = 13;

	public static final String statusKeyVersion = "version";
	public static final int statusKeyVersionIndex = 14;

	public static final String statusKeySelectedTagId = "selectedtagid";
	public static final int statusKeySelectedTagIdIndex = 15;

	public static final String statusKeyStarted = "started";
	public static final int statusKeyStartedIndex = 16;

	public static final String statusKeyAnswer = "answer";
	public static final int statusKeyAnswerIndex = 17;

	public static final String statusKeyFeedbackConditionId = "feedbackconditionid";
	public static final int statusKeyFeedbackConditionIdIndex = 18;

	public static final String statusKeyCorrect = "correct";
	public static final int statusKeyCorrectIndex = 19;

	public static final String statusKeyDeleted = "deleted";
	public static final int statusKeyDeletedIndex = 20;

	public static final String statusKeyExtendable = "extendable";
	public static final int statusKeyExtendableIndex = 21;

	public static final String statusKeyShared = "shared";
	public static final int statusKeySharedIndex = 22;

	public static final String statusKeyOwnership = "ownership";
	public static final int statusKeyOwnershipIndex = 23;

	public static final String statusKeyRating = "rating";
	public static final int statusKeyRatingIndex = 24;

	public static final String statusKeySharedbyteam = "sharedbyteam";
	public static final int statusKeySharedbyteamIndex = 25;

	public static final String statusKeyValue = "value";
	public static final int statusKeyValueIndex = 26;

	public static final String statusKeySelectedTagData = "selectedtagdata";
	public static final int statusKeySelectedTagDataIndex = 27;

	public static final String statusKeyDatetime = "datetime";
	public static final int statusKeyDatetimeIndex = 28;

	public static final String statusKeyAlwaysrecord = "alwaysrecord";
	public static final int statusKeyAlwaysrecordIndex = 29;

	public static final String statusKeyActor = "actor";
	public static final int statusKeyActorIndex = 30;

	public static final String statusKeyPreviousitemsaccessible = "previousitemsaccessible";
	public static final int statusKeyPreviousitemsaccesibleIndex = 31;

	public static final String statusKeyNextitemsaccessible = "nextitemsaccessible";
	public static final int statusKeyNextitemsaccesibleIndex = 32;

	public static final String statusKeyLayoutnumber = "layoutnumber";
	public static final int statusKeyLayoutnumberIndex = 33;

	public static final String statusKeyPreviewed = "previewed";
	public static final int statusKeyPreviewedIndex = 34;

	public static final String statusKeyHighlight = "highlight";
	public static final int statusKeyHighlightIndex = 35;

	public static final String statusKeySaved = "saved";
	public static final int statusKeySavedIndex = 36;

	public static final String statusKeyUpdate = "update";
	public static final int statusKeyUpdateIndex = 37;

	public static final String statusKeyString = "string";
	public static final int statusKeyStringIndex = 38;

	public static final String statusKeyContainerok = "containerok";
	public static final int statusKeyContainerokIndex = 39;

	public static final String statusKeyContainerposok = "containerposok";
	public static final int statusKeyContainerposokIndex = 40;

	public static final String statusKeyContainerpos = "containerpos";
	public static final int statusKeyContainerposIndex = 41;

	public static final String statusKeyIndex = "index";
	public static final int statusKeyIndexIndex = 42;

	public static final String statusKeyNobreadcrumb = "nobreadcrumb";
	public static final int statusKeyNobreadcrumbIndex = 43;

	public static final String statusKeyDraggable = "draggable";
	public static final int statusKeyDraggableIndex = 44;

	public static final String statusKeyDroppable = "droppable";
	public static final int statusKeyDroppableIndex = 45;

	public static final String statusKeyCopy = "copy";
	public static final int statusKeyCopyIndex = 46;

	public static final String statusKeyDataparentid = "dataparentid";
	public static final int statusKeyDataparentidIndex = 47;

	public static final String statusKeyDatabeforeid = "databeforeid";
	public static final int statusKeyDatabeforeidIndex = 48;

	public static final String statusKeyRandomizeitems = "randomizeitems";
	public static final int statusKeyRandomizeitemsIndex = 49;

	public static final String statusKeyRandomized = "randomized";
	public static final int statusKeyRandomizedIndex = 50;

	public static final String statusKeyInitialized = "initialized";
	public static final int statusKeyInitializedIndex = 51;

	public static final String statusKeyNumberofattempts = "numberofattempts";
	public static final int statusKeyNumberofattemptsIndex = 52;

	public static final String statusKeyShowcontrols = "showcontrols";
	public static final int statusKeyShowcontrolsIndex = 53;

	public static final String statusKeyNumberofcorrectitems = "numberofcorrectitems";
	public static final int statusKeyNumberofcorrectitemsIndex = 54;

	public static final String statusKeyShowinteraction = "showinteraction";
	public static final int statusKeyShowinteractionIndex = 55;

	public static final String statusKeyUrl = "url";
	public static final int statusKeyUrlIndex = 56;

	public static final String statusKeyId = "id";
	public static final int statusKeyIdIndex = 57;

	public static final String statusKeyXposition = "xposition";
	public static final int statusKeyXpositionIndex = 58;

	public static final String statusKeyYposition = "yposition";
	public static final int statusKeyYpositionIndex = 59;

	public static final String statusKeyInterruptable = "interruptable";
	public static final int statusKeyInterruptableIndex = 60;

	public static final String statusKeyPublishable = "publishable";
	public static final int statusKeyPublishableIndex = 61;

	public static final String statusKeyReviewable = "reviewable";
	public static final int statusKeyReviewableIndex = 62;

	public static final String statusKeyNextifpublished = "nextifpublished";
	public static final int statusKeyNextifpublishedIndex = 63;

	public static final String statusKeyAllowprevious = "allowprevious";
	public static final int statusKeyAllowpreviousIndex = 64;

	public static final String statusKeyNumberofresits = "numberofresits";
	public static final int statusKeyNumberofresitsIndex = 65;

	public static final String statusKeyNumberofreactions = "numberofreactions";
	public static final int statusKeyNumberofreactionsIndex = 66;

	public static final String statusKeyReset = "reset";
	public static final int statusKeyResetIndex = 67;

	public static final String statusKeyRanking = "ranking";
	public static final int statusKeyRankingIndex = 68;

	public static final String statusKeySRatingFixed = "sratingfixed";
	public static final int statusKeySRatingFixedIndex = 69;

	public static final String statusKeyGameLogging = "gamelogging";
	public static final int statusKeyGameLoggingIndex = 70;

	public static final String statusKeyGameSettings = "gamesettings";
	public static final int statusKeyGameSettingsIndex = 71;

	public static final String statusKeySRating = "srating";
	public static final int statusKeySRatingIndex = 72;

	public static final String statusKeySPlayCount = "splaycount";
	public static final int statusKeySPlayCountIndex = 73;

	public static final String statusKeySKFactor = "skfactor";
	public static final int statusKeySKFactorIndex = 74;

	public static final String statusKeySUncertainty = "suncertainty";
	public static final int statusKeySUncertaintyIndex = 75;

	public static final String statusKeySLastPlayed = "slastplayed";
	public static final int statusKeySLastPlayedIndex = 76;

	public static final String statusKeyTimeLimit = "timelimit";
	public static final int statusKeyTimeLimitIndex = 77;

	public static final String statusKeyPRating = "prating";
	public static final int statusKeyPRatingIndex = 78;

	public static final String statusKeyPPlayCount = "pplaycount";
	public static final int statusKeyPPlayCountIndex = 79;

	public static final String statusKeyPKFactor = "pkfactor";
	public static final int statusKeyPKFactorIndex = 80;

	public static final String statusKeyPUncertainty = "puncertainty";
	public static final int statusKeyPUncertaintyIndex = 81;

	public static final String statusKeyPLastPlayed = "plastplayed";
	public static final int statusKeyPLastPlayedIndex = 82;

	public static final String statusKeyTime = "time";
	public static final int statusKeyTimeIndex = 83;

	public static final String statusKeyRealtime = "realtime";
	public static final int statusKeyRealtimeIndex = 84;

	public static final String statusKeyMultiple = "multiple";
	public static final int statusKeyMultipleIndex = 85;

	public static final String statusKeyAccumulate = "accumulate";
	public static final int statusKeyAccumulateIndex = 86;

	public static final String statusKeyAutostart = "autostart";
	public static final int statusKeyAutostartIndex = 87;

	public static final String statusKeyClosable = "closable";
	public static final int statusKeyClosableIndex = 88;

	public static final String statusKeyDirectfeedback = "directfeedback";
	public static final int statusKeyDirectfeedbackIndex = 89;

	public static final String statusKeyLabelforoverviewbutton = "labelforoverviewbutton";
	public static final int statusKeyLabelforoverviewbuttonIndex = 90;

	public static final String statusKeyOverwrite = "overwrite";
	public static final int statusKeyOverwriteIndex = 91;

	public static final String statusKeyPracticeoverview = "practiceoverview";
	public static final int statusKeyPracticeoverviewIndex = 92;

	public static final String statusKeyShowfeedback = "showfeedback";
	public static final int statusKeyShowfeedbackIndex = 93;

	public static final String statusKeyShowscore = "showscore";
	public static final int statusKeyShowscoreIndex = 94;

	public static final String statusKeyTutorassespractice = "tutorassespractice";
	public static final int statusKeyTutorassespracticeIndex = 95;

	public static final String statusKeyTutorassestest = "tutorassestest";
	public static final int statusKeyTutorassestestIndex = 96;

	public static final String statusKeyStudentassespractice = "studentassespractice";
	public static final int statusKeyStudentassespracticeIndex = 97;

	public static final String statusKeyStudentassestest = "studentassestest";
	public static final int statusKeyStudentassestestIndex = 98;

	public static final String statusKeyStudentseefeedbackonothers = "studentseefeedbackonothers";
	public static final int statusKeyStudentseefeedbackonothersIndex = 99;

	public static final String statusKeyStartoverview = "startoverview";
	public static final int statusKeyStartoverviewIndex = 100;

	public static final String statusKeyTestoverview = "testoverview";
	public static final int statusKeyTestoverviewIndex = 101;

	public static final String statusKeyTaskscheckable = "taskscheckable";
	public static final int statusKeyTaskscheckableIndex = 102;

	public static final String statusKeyZulfile = "zulfile";
	public static final int statusKeyZulfileIndex = 103;

	public static final String statusKeyRealstarttime = "realstarttime";
	public static final int statusKeyRealstarttimeIndex = 104;

	public static final String statusKeyPosition = "position";
	public static final int statusKeyPositionIndex = 105;

	public static final String statusKeySize = "size";
	public static final int statusKeySizeIndex = 106;

	public static final String statusKeyPopup = "popup";
	public static final int statusKeyPopupIndex = 107;

	public static final String statusKeyTransitioneffect = "transitioneffect";
	public static final int statusKeyTransitioneffectIndex = 108;

	public static final String statusKeyAlwaysinspectfeedback = "alwaysinspectfeedback";
	public static final int statusKeyAlwaysinspectfeedbackIndex = 109;

	public static final String statusKeyShowassfeedback = "showassfeedback";
	public static final int statusKeyShowassfeedbackIndex = 110;

	public static final String statusKeyHideinspection = "hideinspection";
	public static final int statusKeyHideinspectionIndex = 111;

	public static final String statusKeyRepeat = "repeat";
	public static final int statusKeyRepeatIndex = 112;

	public static final String statusKeyActive = "active";
	public static final int statusKeyActiveIndex = 113;

	public static final String statusKeyMaxnumberofitems = "maxnumberofitems";
	public static final int statusKeyMaxnumberofitemsIndex = 114;

	public static final String statusKeySequencenumber = "sequencenumber";
	public static final int statusKeySequencenumberIndex = 115;

	public static final String statusKeyChosennumber = "chosennumber";
	public static final int statusKeyChosennumberIndex = 116;

	public static final String statusKeyUsernotabletofinish = "usernotabletofinish";
	public static final int statusKeyUsernotabletofinishIndex = 117;

	public static final String statusKeyTeachermayinitiatestep = "teachermayinitiatestep";
	public static final int statusKeyTeachermayinitiatestepIndex = 118;

	public static final String statusKeyTeachermaycompletestep = "teachermaycompletestep";
	public static final int statusKeyTeachermaycompletestepIndex = 119;

	public static final String statusKeyStudentmaycompletestep = "studentmaycompletestep";
	public static final int statusKeyStudentmaycompletestepIndex = 120;

	public static final String statusKeyWeightingteacherstudent = "weightingteacherstudent";
	public static final int statusKeyWeightingteacherstudentIndex = 121;

	public static final String statusKeyDeadlineDate = "deadlinedate";
	public static final int statusKeyDeadlineDateIndex = 122;

	public static final String statusKeyDeadlineTime = "deadlinetime";
	public static final int statusKeyDeadlineTimeIndex = 123;

	public static final String statusKeyReminderDate = "reminderdate";
	public static final int statusKeyReminderDateIndex = 124;

	public static final String statusKeyReminderTime = "remindertime";
	public static final int statusKeyReminderTimeIndex = 125;

	public static final String statusKeyAllusersmustcompleteallsubsteps = "allusersmustcompleteallsubsteps";
	public static final int statusKeyAllusersmustcompleteallsubstepsIndex = 126;

	public static final String statusKeySendDate = "senddate";
	public static final int statusKeySendDateIndex = 127;

	public static final String statusKeySendTime = "sendtime";
	public static final int statusKeySendTimeIndex = 128;

	public static final String statusKeyDrawAttention = "drawattention";
	public static final int statusKeyDrawAttentionIndex = 129;

	public static final String statusKeyStartOfFadeOut = "startoffadeout";
	public static final int statusKeyStartOfFadeOutIndex = 130;

	public static final String statusKeyStartOfFadeIn = "startoffadein";
	public static final int statusKeyStartOfFadeInIndex = 131;

	public static final String statusKeyMayaskteacherfeedbackcount = "mayaskteacherfeedbackcount";
	public static final int statusKeyMayaskteacherfeedbackcountIndex = 132;

	public static final String statusKeyMayscoreskill = "mayscoreskill";
	public static final int statusKeyMayscoreskillIndex = 133;

	public static final String statusKeyMayscoreskillforteacher = "mayscoreskillforteacher";
	public static final int statusKeyMayscoreskillforteacherIndex = 134;

	public static final String statusKeyMayscoreskillforpeerstudent = "mayscoreskillforpeerstudent";
	public static final int statusKeyMayscoreskillforpeerstudentIndex = 135;

	public static final String statusKeyMaytipsandtopsskill = "maytipsandtopsskill";
	public static final int statusKeyMaytipsandtopsskillIndex = 136;

	public static final String statusKeyMaytipsandtopsskillforteacher = "maytipsandtopsskillforteacher";
	public static final int statusKeyMaytipsandtopsskillforteacherIndex = 137;

	public static final String statusKeyMaytipsandtopsskillforpeerstudent = "maytipsandtopsskillforpeerstudent";
	public static final int statusKeyMaytipsandtopsskillforpeerstudentIndex = 138;

	public static final String statusKeyTeachermayusescoresofpreviouscycle = "teachermayusescoresofpreviouscycle";
	public static final int statusKeyTeachermayusescoresofpreviouscycleIndex = 139;

	public static final String statusKeySeeself = "seeself";
	public static final int statusKeySeeselfIndex = 140;

	public static final String statusKeyMaychangeseeself = "maychangeseeself";
	public static final int statusKeyMaychangeseeselfIndex = 141;

	public static final String statusKeyAskteacherfeedback = "askteacherfeedback";
	public static final int statusKeyAskteacherfeedbackIndex = 142;

	public static final String statusKeyTeacherfeedbackaskedcount = "teacherfeedbackaskedcount";
	public static final int statusKeyTeacherfeedbackaskedcountIndex = 143;

	public static final String statusKeyMayrategivenfeedback = "mayrategivenfeedback";
	public static final int statusKeyMayrategivenfeedbackIndex = 144;

	public static final String statusKeyMustrategivenfeedback = "mustrategivenfeedback";
	public static final int statusKeyMustrategivenfeedbackIndex = 145;

	public static final String statusKeyFeedbackrating = "feedbackrating";
	public static final int statusKeyFeedbackratingIndex = 146;

	public static final String statusKeyLatitude = "latitude";
	public static final int statusKeyLatitudeIndex = 1001;

	public static final String statusKeyLongitude = "longitude";
	public static final int statusKeyLongitudeIndex = 1002;

	/** The Constant statusValueTrue. Status value true. */
	public static final String statusValueTrue = "true";

	/** The Constant statusValueFalse. Status value false. */
	public static final String statusValueFalse = "false";

	/** The Constant statusValueSeparator. */
	public static final String statusValueSeparator = "_and_";

	/**
	 * The Constant statusCommaReplace. To replace commas in status value, because
	 * comma is used to split status value from case time value.
	 */
	public static final String statusCommaReplace = "_COMMAAMMOC_";

	/** The Constant statusCrlfReplace. To replace crlfs (\n) in status value. */
	public static final String statusCrLfReplace = "_CRLFFLRC_";

	/**
	 * The Constant sessKeyRunMediaViewer. The session key which refers to the media
	 * viewer zul file in a run session.
	 */
	public static final String sessKeyRunMediaViewer = "media_viewer";

	/**
	 * The Constant sessKeyRunMediaRef. The session key which refers to the media
	 * file to be shown by the media viewer in a run session.
	 */
	public static final String sessKeyRunMediaRef = "media_href";

	/**
	 * The Constant sessKeyContentFiles. The session key which refers to the content
	 * files that are accessed during running a case.
	 */
	public static final String sessKeyContentFiles = "content_files";

	/**
	 * The Constant sessKeyContentFileBlobIds. The session key which refers to the
	 * blob ids of the content files that are accessed during running a case.
	 */
	public static final String sessKeyContentFileBlobIds = "content_file_blobids";

	/** The Constant functionCount. Function on status. */
	public static final String functionCount = "count";

	/** The Constant functionTime. Function on status time. Not used yet. */
	public static final String functionTime = "time";

	/** The Constant function�ncrement. Function on status int. */
	public static final String functionInc = "inc";

	/** The Constant functionDec. Function on status int. */
	public static final String functionDec = "dec";

	/** The Constant operatorEq. Operator equal on status. */
	public static final String operatorEq = "eq";

	/** The Constant operatorGt. Operator greater than on status. */
	public static final String operatorGt = "gt";

	/** The Constant operatorGe. Operator greater or equal on status. */
	public static final String operatorGe = "ge";

	/** The Constant operatorLt. Operator less then on status. */
	public static final String operatorLt = "lt";

	/** The Constant operatorLe. Operator less or equal on status. */
	public static final String operatorLe = "le";

	/**
	 * The Constant operatorInterval. Operator interval on status. An interval of
	 * status values.
	 */
	public static final String operatorInterval = "interval";

	/**
	 * The Constant operatorIn. Operator in on status. Status value in collection.
	 */
	public static final String operatorIn = "in";

	/** The Constant statusValueTrueIndex. Corresponding index of status value. */
	public static final int statusValueTrueIndex = 1;

	/** The Constant statusValueFalseIndex. Corresponding index of status value. */
	public static final int statusValueFalseIndex = 0;

	/** The Constant functionCountIndex. Corresponding index of function. */
	public static final int functionCountIndex = 1;

	/** The Constant functionTimeIndex. Corresponding index of function. */
	public static final int functionTimeIndex = 2;

	/** The Constant functionIncIndex. Corresponding index of function. */
	public static final int functionIncIndex = 3;

	/** The Constant functionDecIndex. Corresponding index of function. */
	public static final int functionDecIndex = 4;

	/** The Constant operatorEqIndex. Corresponding index of operator. */
	public static final int operatorEqIndex = 1;

	/** The Constant operatorGtIndex. Corresponding index of operator. */
	public static final int operatorGtIndex = 2;

	/** The Constant operatorGeIndex. Corresponding index of operator. */
	public static final int operatorGeIndex = 3;

	/** The Constant operatorLtIndex. Corresponding index of operator. */
	public static final int operatorLtIndex = 4;

	/** The Constant operatorLeIndex. Corresponding index of operator. */
	public static final int operatorLeIndex = 5;

	/** The Constant operatorIntervalIndex. Corresponding index of operator. */
	public static final int operatorIntervalIndex = 6;

	/** The Constant operatorInIndex. Corresponding index of operator. */
	public static final int operatorInIndex = 7;

	/**
	 * The Constant updateStatusKey. Used for updating status of one account by
	 * another.
	 */
	public static final String updateStatusKey = "update_statuskey";

	/**
	 * The Constant updateStatusValue. Used for updating status of one account by
	 * another.
	 */
	public static final String updateStatusValue = "update_statusvalue";

	/**
	 * The Constant updateCheckScript. Used for updating status of one account by
	 * another.
	 */
	public static final String updateCheckScript = "update_checkscript";

	/**
	 * The Constant processLocally. Used for updating status of one account by
	 * another.
	 */
	public static final String processLocally = "process_locally";

	/**
	 * The Constant mai_id_outmailpredef_sent. Db id, used for sending mail
	 * notification to tutor if account uses e-messages.
	 */
	public static final int mai_id_outmailpredef_sent = 1;

	/**
	 * The Constant mai_id_outmailhelp_sent. Db id, used for sending mail
	 * notification to tutor if account uses e-messages.
	 */
	public static final int mai_id_outmailhelp_sent = 2;

	/**
	 * The Constant mai_id_inmailpredef_sent. Db id, used for sending mail
	 * notification to tutor if account uses e-messages.
	 */
	public static final int mai_id_inmailpredef_sent = 3;

	/**
	 * The Constant mai_id_inmailhelp_sent. Db id, used for sending mail
	 * notification to tutor if account uses e-messages.
	 */
	public static final int mai_id_inmailhelp_sent = 4;

	/**
	 * The Constant caseComponentsInRunByCasId. A name of a container used for
	 * keeping casecomponents in AppManager property. So keeping them in application
	 * memory shared by all sessions.
	 */
	public static final String caseComponentsInRunByCasId = "caseComponentsInRunByCasId";

	/**
	 * The Constant xmlDefTreeByComId. A name of a container used for keeping xml
	 * def trees in AppManager property. So keeping them in application memory
	 * shared by all sessions.
	 */
	public static final String xmlDefTreeByComId = "xmlDefTreeByComId";

	/**
	 * The Constant xmlDataTreeByCacId. A name of a container used for keeping xml
	 * data trees in AppManager property. So keeping them in application memory
	 * shared by all sessions.
	 */
	public static final String xmlDataTreeByCacId = "xmlDataTreeByCacId";

	/**
	 * The Constant appAttributesPerSessionIds. A name of a container used for
	 * keeping attributes per session ids in AppManager property.
	 */
	public static final String appAttributesPerSessionIds = "appAttributesPerSessionIds";

	/**
	 * The Constant runStatusRun. The normal status as a student opens the player,
	 * progress is saved.
	 */
	public static final String runStatusRun = "run";

	/**
	 * The Constant runStatusTutorRun. A tutor opens the player as if he is a
	 * student, progress is not saved.
	 */
	public static final String runStatusTutorRun = "tutorrun";

	/**
	 * The Constant runStatusPreview. A case developer opens the player in preview
	 * mode, progress is saved.
	 */
	public static final String runStatusPreview = "preview";

	/**
	 * The Constant runStatusPreviewReadOnly. A case developer opens the player in
	 * previewreadonly mode, progress is not saved.
	 */
	public static final String runStatusPreviewReadOnly = "previewreadonly";

	/**
	 * The Constant select all.
	 */
	public static final String selectAll = "all";

	/**
	 * String variables for replacing states and internal variables within strings.
	 */
	public static final String stringVarPrefix = "[$";
	public static final String stringVarPostfix = "$]";
	public static final String stringLinkPrefix = "[@";
	public static final String stringLinkPostfix = "@]";
	public static final String stringVarUserNameKey = "USER_NAME";
	public static final String stringVarUserRoleKey = "USER_ROLE";
	public static final String stringVarCaseTimeKey = "CASE_TIME";

	/**
	 * String variables for replacing temporary variables within strings and storing
	 * them.
	 */
	public static final String tempVarPrefix = "[?";
	public static final String tempVarPostfix = "?]";
	public static final String tempVarAdd = "+";
	public static final String tempVarIndexPrefix = "(";
	public static final String tempVarIndexPostfix = ")";
	public static final String tempVarRegEx = "\\[\\?(\\w[\\w\\s]*(?>\\(\\-?\\d+\\))?)\\?\\]";
	public static final String tempVarRegExAdd = "\\[\\?\\+(\\w[\\w\\s]*)\\?\\]";

	/** The javascript actionstart. */
	public static final String javascriptWindowOpenStart = "window.open('";

	/** The javascript actionmid. */
	public static final String javascriptWindowOpenMid = "','','";

	/** The javascript actionend. */
	public static final String javascriptWindowOpenEnd = "');";

	/** The style. */
	public static final String javascriptWindowOpenStyleFullscreen = "fullscreen";

	/** The javascript actionstartdoc. */
	public static final String javascriptWindowLocationHrefStart = "window.location.href='";

	/** The javascript actionenddoc. */
	public static final String javascriptWindowLocationHrefEnd = "';";

	// start external triggers
	/**
	 * The Constant statusKeyExLatitude. Status key for external latitude attribute.
	 */
	public static final String statusKeyExLatitude = "exlatitude";

	/**
	 * The Constant statusKeyExLongitude. Status key for external longitude
	 * attribute.
	 */
	public static final String statusKeyExLongitude = "exlongitude";

	/**
	 * The Constant statusKeyExPosition. Status key for external position attribute.
	 */
	public static final String statusKeyExPosition = "exposition";

	/** The Constant statusKeyExLatitudeIndex. Corresponding index of status key. */
	public static final int statusKeyExLatitudeIndex = 1001;

	/**
	 * The Constant statusKeyExLongitudeIndex. Corresponding index of status key.
	 */
	public static final int statusKeyExLongitudeIndex = 1002;

	/** The Constant statusKeyExPositionIndex. Corresponding index of status key. */
	public static final int statusKeyExPositionIndex = 1003;
	// end external triggers

}