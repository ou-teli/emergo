/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;

import nl.surf.emergo.business.abstr.AbstractDaoBasedCaseComponentRoleManager;
import nl.surf.emergo.domain.ECaseComponentRole;
import nl.surf.emergo.domain.IECaseComponentRole;

/**
 * The Class CaseComponentRoleManager.
 */
public class CaseComponentRoleManager extends
		AbstractDaoBasedCaseComponentRoleManager implements InitializingBean {

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	public void afterPropertiesSet() throws Exception {
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseComponentRoleManager#getNewCaseComponentRole()
	 */
	public IECaseComponentRole getNewCaseComponentRole() {
		IECaseComponentRole eCaseComponentRole = new ECaseComponentRole();
		return eCaseComponentRole;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseComponentRoleManager#getCaseComponentRole(int)
	 */
	public IECaseComponentRole getCaseComponentRole(int ccrId) {
		IECaseComponentRole eCaseComponentRole = caseComponentRoleDao
				.get(ccrId);
		return eCaseComponentRole;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseComponentRoleManager#getCaseComponentRole(int, int)
	 */
	public IECaseComponentRole getCaseComponentRole(int cacId, int carId) {
		IECaseComponentRole eCaseComponentRole = caseComponentRoleDao.get(
				cacId, carId);
		return eCaseComponentRole;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseComponentRoleManager#saveCaseComponentRole(nl.surf.emergo.domain.IECaseComponentRole)
	 */
	public void saveCaseComponentRole(IECaseComponentRole eCaseComponentRole) {
		caseComponentRoleDao.save(eCaseComponentRole);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseComponentRoleManager#newCaseComponentRole(nl.surf.emergo.domain.IECaseComponentRole)
	 */
	public List<String[]> newCaseComponentRole(IECaseComponentRole eCaseComponentRole) {
		eCaseComponentRole.setCreationdate(new Date());
		eCaseComponentRole.setLastupdatedate(new Date());
		saveCaseComponentRole(eCaseComponentRole);
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseComponentRoleManager#updateCaseComponentRole(nl.surf.emergo.domain.IECaseComponentRole)
	 */
	public List<String[]> updateCaseComponentRole(IECaseComponentRole eCaseComponentRole) {
		eCaseComponentRole.setLastupdatedate(new Date());
		saveCaseComponentRole(eCaseComponentRole);
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseComponentRoleManager#deleteCaseComponentRole(int)
	 */
	public void deleteCaseComponentRole(int ccrId) {
		deleteCaseComponentRole(getCaseComponentRole(ccrId));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseComponentRoleManager#deleteCaseComponentRole(nl.surf.emergo.domain.IECaseComponentRole)
	 */
	public void deleteCaseComponentRole(IECaseComponentRole eCaseComponentRole) {
		if (eCaseComponentRole.getLastupdatedate() == null)
			// lastupdatedate field is added later, so value can be empty for older records
			eCaseComponentRole.setLastupdatedate(eCaseComponentRole.getCreationdate());
		caseComponentRoleDao.delete(eCaseComponentRole);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseComponentRoleManager#getAllCaseComponentRoles()
	 */
	public List<IECaseComponentRole> getAllCaseComponentRoles() {
		return caseComponentRoleDao.getAll();
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseComponentRoleManager#getAllCaseComponentRolesByCacId(int)
	 */
	public List<IECaseComponentRole> getAllCaseComponentRolesByCacId(int cacId) {
		return caseComponentRoleDao.getAllByCacId(cacId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseComponentRoleManager#getAllCaseComponentRolesByCacIds(List)
	 */
	public List<IECaseComponentRole> getAllCaseComponentRolesByCacIds(List<Integer> cacIds) {
		return caseComponentRoleDao.getAllByCacIds(cacIds);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseComponentRoleManager#getAllCaseComponentRolesByCarId(int)
	 */
	public List<IECaseComponentRole> getAllCaseComponentRolesByCarId(int carId) {
		return caseComponentRoleDao.getAllByCarId(carId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseComponentRoleManager#getAllCaseComponentRolesByCarIds(List)
	 */
	public List<IECaseComponentRole> getAllCaseComponentRolesByCarIds(List<Integer> carIds) {
		return caseComponentRoleDao.getAllByCarIds(carIds);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.ICaseComponentRoleManager#getAllCaseComponentRolesByCacIdCarId(int, int)
	 */
	public List<IECaseComponentRole> getAllCaseComponentRolesByCacIdCarId(int cacId, int carId) {
		return caseComponentRoleDao.getAllByCacIdCarId(cacId, carId);
	}

}