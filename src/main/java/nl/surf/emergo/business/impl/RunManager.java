/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;

import nl.surf.emergo.business.abstr.AbstractDaoBasedRunManager;
import nl.surf.emergo.domain.ERun;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunGroup;

/**
 * The Class RunManager.
 */
public class RunManager extends AbstractDaoBasedRunManager implements InitializingBean {

	@Override
	public void afterPropertiesSet() throws Exception {
		// NOOP
	}

	@Override
	public IERun getNewRun() {
		return new ERun();
	}

	@Override
	public IERun getRun(int runId) {
		return runDao.get(runId);
	}

	@Override
	public IERun getTestRun(IEAccount eAccount, IECase eCase) {
		List<IERun> lItems = getAllRunsByAccIdCasId(eAccount.getAccId(), eCase.getCasId());
		IERun testRun = null;
		if (lItems != null) {
			for (IERun eRun : lItems) {
				if (eRun.getStatus() == AppConstants.run_status_test)
					testRun = eRun;
			}
		}
		if (testRun == null) {
			testRun = getNewRun();
			testRun.setEAccount(eAccount);
			testRun.setECase(eCase);
			// give silly code to distinguish from other run groups
			testRun.setCode("testtesttest");
			// give silly name to distinguish from other run groups
			testRun.setName("testtesttest");
			testRun.setAccessmailsubject("");
			testRun.setAccessmailbody("");
			testRun.setXmldata(AppConstants.emptyXml);
			testRun.setStartdate(new Date());
			testRun.setEnddate(new Date());
			testRun.setActive(true);
			testRun.setOpenaccess(false);
			newRun(testRun);
			// default of newRun is under construction, so set status after newRun
			testRun.setStatus(AppConstants.run_status_test);
			updateRun(testRun);
//			Get run so date format, name etc. is correct.
			testRun = getRun(testRun.getRunId());
		}
		return testRun;
	}

	@Override
	public boolean hasTestRun(IEAccount eAccount, IECase eCase) {
		List<IERun> lItems = getAllRunsByAccIdCasId(eAccount.getAccId(), eCase.getCasId());
		IERun testRun = null;
		if (lItems != null) {
			for (IERun eRun : lItems) {
				if (eRun.getStatus() == AppConstants.run_status_test)
					testRun = eRun;
			}
		}
		return (testRun != null);
	}

	@Override
	public void saveRun(IERun eRun) {
		runDao.save(eRun);
	}

	@Override
	public List<String[]> newRun(IERun eRun) {
		// Validate item and if ok, save it after setting creationdate and
		// lastupdatedate.
		List<String[]> lErrors = validateRun(eRun);
		if (lErrors == null) {
			eRun.setStatus(AppConstants.run_status_under_construction);
			eRun.setCreationdate(new Date());
			eRun.setLastupdatedate(new Date());
			saveRun(eRun);
		}
		return lErrors;
	}

	@Override
	public List<String[]> updateRun(IERun eRun) {
		// Validate item and if ok, save it after setting lastupdatedate.
		List<String[]> lErrors = validateRun(eRun);
		if (lErrors == null) {
			eRun.setLastupdatedate(new Date());
			saveRun(eRun);
		}
		return lErrors;
	}

	@Override
	public List<String[]> validateRun(IERun eRun) {
		/*
		 * Validate if name is not empty. Validate if name is unique. Validate if case
		 * is not null. If validation error return it as element in String[] list.
		 */
		List<String[]> lErrors = new ArrayList<>(0);
		if (appManager.isEmpty(eRun.getName()))
			appManager.addError(lErrors, "name", "error_empty");
		else {
			IERun lExistingRun = runDao.get(eRun.getRunId());
			// check if changed
			boolean lChanged = ((lExistingRun == null) || (!eRun.getName().equals(lExistingRun.getName())));
			if ((lChanged) && (eRun.getECase() != null)
					&& (runDao.exists(eRun.getEAccount().getAccId(), eRun.getECase().getCasId(), eRun.getName())))
				appManager.addError(lErrors, "name", "error_not_unique");
		}
		if (eRun.getECase() == null)
			appManager.addError(lErrors, "case", "error_no_choice");

		return lErrors.isEmpty() ? null : lErrors;
	}

	@Override
	public void deleteRun(int runId) {
		deleteRun(getRun(runId));
	}

	@Override
	public void deleteRun(IERun eRun) {
		// first delete possibly related blobs. they are not deleted in cascade.
		deleteBlobs(eRun);
		if (eRun.getLastupdatedate() == null)
			// lastupdatedate field is added later, so value can be empty for older records
			eRun.setLastupdatedate(eRun.getCreationdate());
		runDao.delete(eRun);
	}

	@Override
	public void deleteBlobs(IERun eRun) {
		// delete blobs for all run groups referenced by run
		List<IERunGroup> lItems = runGroupManager.getAllRunGroupsByRunId(eRun.getRunId());
		if (lItems != null) {
			for (IERunGroup lItem : lItems)
				runGroupManager.deleteBlobs(lItem);
		}
	}

	@Override
	public List<IERun> getAllRuns() {
		return runDao.getAll();
	}

	@Override
	public List<IERun> getAllRunsFilter(Map<String, String> keysAndValueParts) {
		return runDao.getAllFilter(keysAndValueParts);
	}

	@Override
	public List<IERun> getAllRunsByRunIds(List<Integer> runIds) {
		return runDao.getAllByRunIds(runIds);
	}

	@Override
	public List<IERun> getAllRunsByAccId(int accId) {
		return runDao.getAllByAccId(accId);
	}

	@Override
	public List<IERun> getAllRunsByCasId(int casId) {
		return runDao.getAllByCasId(casId);
	}

	@Override
	public List<IERun> getAllRunsByAccIdCasId(int accId, int casId) {
		return runDao.getAllByAccIdCasId(accId, casId);
	}

	@Override
	public List<IERun> getAllRunnableRunsByAccId(int accId) {
		return runDao.getAllRunnableByAccId(accId);
	}

	@Override
	public List<IERun> getAllRunnableOpenAccessRuns() {
		return runDao.getAllRunnableOpenAccess();
	}
}