/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business.impl;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import nl.surf.emergo.business.IXMLAttributeValueTime;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.IXMLTree;

/**
 * The Class XMLTree.
 */
public class XMLTree implements IXMLTree {
	private static final Logger _log = LogManager.getLogger(XMLTree.class);
	private DocumentBuilderFactory factory = null;
	private DocumentBuilder builder = null;

	public XMLTree() {
		super();
		factory = DocumentBuilderFactory.newInstance();
		try {
			// NOTE see
			// https://docs.oracle.com/javase/8/docs/technotes/guides/security/jaxp/jaxp.html#potential-attacks-during-xml-processing
			// to prevent XXE attacks.
			// 'Note: Explicitly turning on Feature for Secure Processing (FSP) through the
			// API, for example, factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING,
			// true), disables all external connections.'
			factory.setFeature(javax.xml.XMLConstants.FEATURE_SECURE_PROCESSING, true);
		} catch (ParserConfigurationException e1) {
			_log.error(e1);
		}
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			_log.error(e);
		}
	}

	@Override
	public String escapeXML(String aStr) {
		if (aStr.length() == 0) {
			return aStr;
		}

		StringBuffer result = new StringBuffer(aStr.length() * 6 / 5);

		for (int i = 0; i < aStr.length(); i++) {
			char c = aStr.charAt(i);
			String esc = (c == '<') ? "&lt;"
					: (c == '>') ? "&gt;"
							: (c == '\'') ? "&#39;" : (c == '\"') ? "&quot;" : (c == '&') ? "&amp;" : null;
			if (esc == null)
				result.append(c);
			else
				result.append(esc);
		}
		return result.toString();
	}

	@Override
	public String escapeXMLAlt(String aStr) {
		String lTest = StringEscapeUtils.escapeXml11(aStr);
		//NOTE also replace HTML code
		lTest = StringEscapeUtils.escapeHtml4(lTest);
		lTest = StringEscapeUtils.escapeJava(lTest);
		return lTest;
	}
	
	@Override
	public String unescapeXML(String aStr) {
		if (aStr.length() == 0) {
			return aStr;
		}

		int size = aStr.length();
		StringBuffer result = new StringBuffer(size);

		for (int i = 0; i < size; ++i) {
			char c = aStr.charAt(i);

			if (c == '&') {
				StringBuffer escapedSequence = new StringBuffer();

				++i;
				while (i < size) {
					c = aStr.charAt(i);
					if (c == ';')
						break;
					escapedSequence.append(c);
					++i;
				}

				if (escapedSequence.length() >= 2 && escapedSequence.charAt(0) == '#') {
					int j = 0;

					try {
						if (escapedSequence.charAt(1) == 'x')

							// create an integer value based on the hexidecimal
							// representation
							j = Integer.parseInt(escapedSequence.substring(2), 16);
						else

							// create an integer value based on the decimal
							// representation
							j = Integer.parseInt(escapedSequence.substring(1));
					} catch (NumberFormatException e) {
					}

					if (j < 0 || j > Character.MAX_VALUE) {
					}
					// create character based on the numeric value
					c = (char) j;
				} else {
					String escapedToken = escapedSequence.toString();
					if (escapedToken.equals("amp")) {
						c = '&';
					} else if (escapedToken.equals("apos")) {
						c = '\'';
					} else if (escapedToken.equals("quot")) {
						c = '\"';
					} else if (escapedToken.equals("lt")) {
						c = '<';
					} else if (escapedToken.equals("gt")) {
						c = '>';
					} else {
					}
				}
			}

			result.append(c);
		}

		return result.toString();
	}

	@Override
	public String unescapeXMLAlt(String aStr) {
		//NOTE nested escape sequences will not be unescaped by unescapeXML
		String lTest = StringEscapeUtils.unescapeXml(aStr);
		while (!lTest.equals(aStr)) {
			aStr = lTest;
			lTest = StringEscapeUtils.unescapeXml(aStr);
		}
		//NOTE also replace HTML code
		lTest = StringEscapeUtils.unescapeHtml4(lTest);
		while (!lTest.equals(aStr)) {
			aStr = lTest;
			lTest = StringEscapeUtils.unescapeHtml4(aStr);
		}
		lTest = StringEscapeUtils.unescapeJava(lTest);
		while (!lTest.equals(aStr)) {
			aStr = lTest;
			lTest = StringEscapeUtils.unescapeJava(aStr);
		}
		return lTest;
	}
	
	@Override
	public DocumentBuilder getDocumentBuilder() {
		return builder;
	}

	@Override
	public synchronized List<IXMLTag> getXMLListIS(List<IXMLTag> xmlDefList, InputSource inputSource,
			String rootelemkey, int aTagType) {
		if (inputSource == null)
			return null;
		return p_getXMLList(xmlDefList, null, inputSource, rootelemkey, aTagType);
	}

	@Override
	public synchronized List<IXMLTag> getXMLListIS(List<IXMLTag> xmlDefList, Document doc, String rootelemkey,
			int aTagType) {
		if (doc == null)
			return null;
		return p_getXMLList(xmlDefList, doc, rootelemkey, aTagType);
	}

	@Override
	public synchronized List<IXMLTag> getXMLList(String xml, String rootelemkey, int aTagType) {
		if (xml == null)
			return null;
		return getXMLListIS(null, StringToInputSource(xml), rootelemkey, aTagType);
	}

	@Override
	public synchronized Document getDocument(String xml) {
		if (xml == null)
			return null;
		return p_getDocument(null, StringToInputSource(xml));
	}

	@Override
	public synchronized List<IXMLTag> getXMLList(Document doc, String rootelemkey, int aTagType) {
		if (doc == null)
			return null;
		return getXMLListIS(null, doc, rootelemkey, aTagType);
	}

	@Override
	public synchronized List<IXMLTag> getXMLListUseDef(List<IXMLTag> xmlDefList, String xml, String rootelemkey,
			int aTagType) {
		if (xml == null)
			return null;
		return getXMLListIS(xmlDefList, StringToInputSource(xml), rootelemkey, aTagType);
	}

	@Override
	public synchronized List<IXMLTag> getXMLListUseDef(List<IXMLTag> xmlDefList, Document doc, String rootelemkey,
			int aTagType) {
		if (doc == null)
			return null;
		return getXMLListIS(xmlDefList, doc, rootelemkey, aTagType);
	}

	@Override
	public synchronized String XMLListsToDoc(List<IXMLTag> XmlCompList, List<IXMLTag> XmlContList) {
		StringBuilder lXmlData = new StringBuilder();
		lXmlData.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><data>");
		lXmlData = XMLListToDoc(XmlCompList, lXmlData);
		lXmlData = XMLListToDoc(XmlContList, lXmlData);
		lXmlData.append("</data>");
		return lXmlData.toString();
	}

	@Override
	public synchronized String XMLListsToDoc(List<IXMLTag> XmlCompList, List<IXMLTag> XmlContList, String encoding) {
		StringBuilder lXmlData = new StringBuilder();
		lXmlData.append("<?xml version=\"1.0\" encoding=\"" + encoding + "\"?><data>");
		lXmlData = XMLListToDoc(XmlCompList, lXmlData);
		lXmlData = XMLListToDoc(XmlContList, lXmlData);
		lXmlData.append("</data>");
		return lXmlData.toString();
	}

	private InputSource StringToInputSource(String aString) {
		Reader sr = new StringReader(aString);
		return new InputSource(sr);
	}

	@Override
	public StringBuilder XMLListToDoc(List<IXMLTag> aXmlList, StringBuilder aXmlData) {
		if (aXmlList != null) {
			for (IXMLTag xmlTag : aXmlList) {
				StringBuilder lXmlData = new StringBuilder();
				lXmlData.append("<");
				lXmlData.append(xmlTag.getName());
				// attributes
				Hashtable<String, List<IXMLAttributeValueTime>> xmlAttributes = xmlTag.getAttributes();
				if (xmlAttributes != null) {
					for (Enumeration<String> keys = xmlAttributes.keys(); keys.hasMoreElements();) {
						String key = keys.nextElement();
						List<IXMLAttributeValueTime> lValueTimes = xmlAttributes.get(key);
						if (lValueTimes != null && lValueTimes.size() > 0) {
							lXmlData.append(" ");
							lXmlData.append(key);
							lXmlData.append("=\"");
							int count = 0;
							for (IXMLAttributeValueTime lValueTime : lValueTimes) {
								if (count > 0) {
									lXmlData.append(",");
								}
								String lValue = lValueTime.getValue();
								// NOTE if status attribute (time != -1, definition and data tags time=-1),
								// replace comma's by string constant
								if (lValueTime.getTime() != -1) {
									lValue = XMLTag.replaceCommas(lValue);
								}
								lXmlData.append(lValue);
								if (lValueTime.getTime() >= 0) {
									lXmlData.append(",");
									lXmlData.append(lValueTime.getTime());
								}
								count++;
							}
							lXmlData.append("\"");
						}
					}
				}
				lXmlData.append(">");
				lXmlData.append(xmlTag.getValue());
				aXmlData.append(lXmlData);
				// childnodes
				aXmlData = XMLListToDoc(xmlTag.getChildTags(), aXmlData);
				lXmlData = new StringBuilder();
				lXmlData.append("</");
				lXmlData.append(xmlTag.getName());
				lXmlData.append(">");
				aXmlData.append(lXmlData);
			}
		}
		return aXmlData;
	}

	@Override
	public synchronized boolean isXMLValid(String aXml) {
		if (builder == null) {
			return false;
		}
		try {
			builder.parse(StringToInputSource(aXml));
		} catch (IOException | SAXException e) {
			return false;
		}
		return true;
	}

	/**
	 * Gets xml document out of input url or input source.
	 *
	 * @param inputURL    the input url
	 * @param inputSource the input source
	 *
	 * @return the document
	 */
	private synchronized Document p_getDocument(String inputURL, InputSource inputSource) {
		if (builder == null) {
			return null;
		}
		Document doc = null;
		try {
			if (inputURL != null) {
				doc = builder.parse(inputURL);
			} else {
				doc = builder.parse(inputSource);
			}
		} catch (IOException | SAXException e) {
			_log.error(e.getMessage() + "\ninputURL:\n" + inputURL + "\ninputSource:\n" + inputSource, e);
		}
		return doc;
	}

	/**
	 * Gets xml tag list out of input url or input source.
	 *
	 * @param xmlDefList  the xml defition tag list
	 * @param inputURL    the input url
	 * @param inputSource the input source
	 * @param rootelemkey the rootelemkey
	 * @param aTagType    the tag type
	 *
	 * @return the list
	 */
	private synchronized List<IXMLTag> p_getXMLList(List<IXMLTag> xmlDefList, String inputURL, InputSource inputSource,
			String rootelemkey, int aTagType) {
		return p_getXMLList(xmlDefList, p_getDocument(inputURL, inputSource), rootelemkey, aTagType);
	}

	/**
	 * Gets xml tag list out of input url or input source.
	 *
	 * @param xmlDefList  the xml defition tag list
	 * @param inputURL    the input url
	 * @param inputSource the input source
	 * @param rootelemkey the rootelemkey
	 * @param aTagType    the tag type
	 *
	 * @return the list
	 */
	private synchronized List<IXMLTag> p_getXMLList(List<IXMLTag> xmlDefList, Document doc, String rootelemkey,
			int aTagType) {
		if (doc == null) {
			return null;
		}
		NodeList nodes = doc.getElementsByTagName(rootelemkey);
		if (nodes == null || nodes.getLength() == 0) {
			return null;
		}
		IXMLTag xmlDefTree = null;
		if (xmlDefList != null && !xmlDefList.isEmpty()) {
			xmlDefTree = xmlDefList.get(0);
		}
		return getXMLChildTags(xmlDefTree, nodes, null, aTagType);
	}

	/**
	 * Gets the xml child tags. Nested.
	 *
	 * @param xmlDefTree the xml definition tree
	 * @param nodes      the nodes to convert to xml tags
	 * @param parent     the parent tag
	 * @param aTagType   the tag type
	 *
	 * @return the xml child tags
	 */
	private List<IXMLTag> getXMLChildTags(IXMLTag xmlDefTree, NodeList nodes, IXMLTag parent, int aTagType) {
		List<IXMLTag> xmlTagList = new ArrayList<IXMLTag>(0);
		Node node = null;
		NodeList childNodes = null;
		IXMLTag xmlTag = null;
		String tagName = "";
		String tagValue = "";
		String attributeName = "";
		String attributeValue = "";
		Hashtable<String, List<IXMLAttributeValueTime>> xmlTagAttributes = null;
		List<IXMLTag> xmlChildTags = null;
		for (int i = 0; i < nodes.getLength(); i++) {
			node = nodes.item(i);
			tagName = node.getNodeName();
			if (!tagName.equals("#text")) {
				// valid node
				// create tag
				xmlTag = new XMLTag();
				xmlTag.setType(aTagType);
				// set parent tag
				xmlTag.setParentTag(parent);
				// set name
				xmlTag.setName(tagName);
				// set value
				tagValue = getNodeText(node);
				if (tagValue == null) {
					tagValue = "";
				}
				xmlTag.setValue(tagValue);
				// set attributes
				NamedNodeMap nodeAttributes = node.getAttributes();
				xmlTagAttributes = new Hashtable<String, List<IXMLAttributeValueTime>>(0);
				if (nodeAttributes != null) {
					for (int j = 0; j < nodeAttributes.getLength(); j++) {
						Node nodeAttribute = nodeAttributes.item(j);
						attributeName = nodeAttribute.getNodeName();
						attributeValue = getNodeText(nodeAttribute);
						List<IXMLAttributeValueTime> lValueTimes = new ArrayList<IXMLAttributeValueTime>();
						if (attributeValue.indexOf(",") < 0 || !tagName.equals(AppConstants.statusElement)) {
							lValueTimes.add(new XMLAttributeValueTime(attributeValue, -1));
						} else {
							if (aTagType != AppConstants.status_tag) {
								// NOTE if status tag restore comma's, otherwise not. For definition and data
								// tags the xml attribute can contain comma's, but no timestamp
								lValueTimes.add(new XMLAttributeValueTime(attributeValue, -1));
							} else {
								String[] lAttributeValueArray = attributeValue.split(",");
								IXMLAttributeValueTime lValueTime = null;
								int k = 0;
								boolean lIsValue = true;
								// NOTE first part of attribute is value, second time, third value etc.
								while (k < lAttributeValueArray.length) {
									if (lIsValue) {
										// NOTE if status tag restore comma's, because comma's are replaced by
										// AppConstants.statusCommaReplace
										lValueTime = new XMLAttributeValueTime(
												XMLTag.restoreCommas(lAttributeValueArray[k]), -1);
										lIsValue = false;
									} else {
										try {
											lValueTime.setTime(Double.parseDouble(lAttributeValueArray[k]));
											lValueTimes.add(lValueTime);
											lIsValue = true;
										} catch (NumberFormatException e) {
											// NOTE for old status attributes it could be that comma's were not replaced
											// by AppConstants.statusCommaReplace
											// Then the split function splits the value in more parts
											// A NumberFormatException is generated if the part is no double
											// If exception then the part should be added to the value
											// It is no guarantee that it works ok, because for instance ',12' could be
											// part of the value and this would not generate an exception
											lValueTime.setValue(lValueTime.getValue() + "," + lAttributeValueArray[k]);
										}
									}
									k++;
								}
							}
						}
						xmlTagAttributes.put(attributeName, lValueTimes);
					}
				}
				xmlTag.setAttributes(xmlTagAttributes);
				// set definition tag
				// NOTE set definition tag after attributes are set because type attribute is
				// needed beneath
				if (xmlDefTree != null) {
					IXMLTag lDefTag = null;
					if (xmlTag.getAttribute(AppConstants.defKeyType).equals(AppConstants.defValueNode)) {
						// if node tag set definition tag to child of root definition tag (this contains
						// definitions of all node tags)
						lDefTag = xmlDefTree.getChild(tagName);
					} else {
						// if no node tag it is a child of a root tag or node tag
						if (parent == null) {
							// if child of root tag set definition tag to root definition
							lDefTag = xmlDefTree;
						} else {
							if (parent.getDefTag() != null) {
								// if child of node tag set definition tag to definition tag of parent
								lDefTag = parent.getDefTag().getChild(tagName);
							}
						}
					}
					xmlTag.setDefTag(lDefTag);
				}
				// set child tags
				xmlChildTags = new ArrayList<>(0);
				if (node.hasChildNodes()) {
					childNodes = node.getChildNodes();
					IXMLTag lParent = xmlTag;
					xmlChildTags = getXMLChildTags(xmlDefTree, childNodes, lParent, aTagType);
				}
				xmlTag.setChildTags(xmlChildTags);
				xmlTagList.add(xmlTag);
			}
		}
		return xmlTagList;
	}

	private String getNodeText(Node pnode) {
		StringBuffer sb = new StringBuffer();
		for (Node node = pnode.getFirstChild(); node != null; node = node.getNextSibling()) {
			if (node.getNodeType() == Node.TEXT_NODE) {
				Text textNode = (Text) node;
				sb.append(textNode.getData());
			}
		}
		// NOTE escape xml because parsing an xml document already unescapes ", <, > and
		// &, etc
		return escapeXML(sb.toString().trim());
	}
}
