/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;

import nl.surf.emergo.business.abstr.AbstractDaoBasedRunTeamRunGroupManager;
import nl.surf.emergo.domain.ERunTeamRunGroup;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunTeam;
import nl.surf.emergo.domain.IERunTeamRunGroup;

/**
 * The Class RunTeamRunGroupManager.
 */
public class RunTeamRunGroupManager extends AbstractDaoBasedRunTeamRunGroupManager implements InitializingBean {

	@Override
	public void afterPropertiesSet() throws Exception {
		// NOOP
	}

	@Override
	public IERunTeamRunGroup getNewRunTeamRunGroup() {
		return new ERunTeamRunGroup();
	}

	@Override
	public IERunTeamRunGroup getRunTeamRunGroup(int rtrId) {
		return runTeamRunGroupDao.get(rtrId);
	}

	@Override
	public IERunTeamRunGroup getTestRunTeamRunGroup(IERunTeam eRunTeam, IERunGroup eRunGroup) {
		List<IERunTeamRunGroup> lItems = getAllRunTeamRunGroupsByRutIdRugId(eRunTeam.getRutId(), eRunGroup.getRugId());
		IERunTeamRunGroup testRunTeamRunGroup = null;
		if ((lItems != null) && (!lItems.isEmpty())) {
			// only one test runteamrungroup per runteam
			testRunTeamRunGroup = lItems.get(0);
		}
		if (testRunTeamRunGroup == null) {
			// create new one
			testRunTeamRunGroup = getNewRunTeamRunGroup();
			testRunTeamRunGroup.setRutRutId(eRunTeam.getRutId());
			testRunTeamRunGroup.setRugRugId(eRunGroup.getRugId());
			newRunTeamRunGroup(testRunTeamRunGroup);
			testRunTeamRunGroup = getRunTeamRunGroup(testRunTeamRunGroup.getRtrId());
		}
		return testRunTeamRunGroup;
	}

	@Override
	public void saveRunTeamRunGroup(IERunTeamRunGroup eRunTeamRunGroup) {
		runTeamRunGroupDao.save(eRunTeamRunGroup);
	}

	@Override
	public List<String[]> newRunTeamRunGroup(IERunTeamRunGroup eRunTeamRunGroup) {
		eRunTeamRunGroup.setCreationdate(new Date());
		eRunTeamRunGroup.setLastupdatedate(new Date());
		saveRunTeamRunGroup(eRunTeamRunGroup);
		return null;
	}

	@Override
	public List<String[]> updateRunTeamRunGroup(IERunTeamRunGroup eRunTeamRunGroup) {
		eRunTeamRunGroup.setLastupdatedate(new Date());
		saveRunTeamRunGroup(eRunTeamRunGroup);
		return null;
	}

	@Override
	public void deleteRunTeamRunGroup(int rtrId) {
		deleteRunTeamRunGroup(getRunTeamRunGroup(rtrId));
	}

	@Override
	public void deleteRunTeamRunGroup(IERunTeamRunGroup eRunTeamRunGroup) {
		if (eRunTeamRunGroup.getLastupdatedate() == null)
			// lastupdatedate field is added later, so value can be empty for older records
			eRunTeamRunGroup.setLastupdatedate(eRunTeamRunGroup.getCreationdate());
		runTeamRunGroupDao.delete(eRunTeamRunGroup);
	}

	@Override
	public List<IERunTeamRunGroup> getAllRunTeamRunGroups() {
		return runTeamRunGroupDao.getAll();
	}

	@Override
	public List<IERunTeamRunGroup> getAllRunTeamRunGroupsByRutId(int rutId) {
		return runTeamRunGroupDao.getAllByRutId(rutId);
	}

	@Override
	public List<IERunTeamRunGroup> getAllRunTeamRunGroupsByRugId(int rugId) {
		return runTeamRunGroupDao.getAllByRugId(rugId);
	}

	@Override
	public List<IERunTeamRunGroup> getAllRunTeamRunGroupsByRugIds(List<Integer> rugIds) {
		return runTeamRunGroupDao.getAllByRugIds(rugIds);
	}

	@Override
	public List<IERunTeamRunGroup> getAllRunTeamRunGroupsByRutIdRugId(int rutId, int rugId) {
		return runTeamRunGroupDao.getAllByRutIdRugId(rutId, rugId);
	}
}