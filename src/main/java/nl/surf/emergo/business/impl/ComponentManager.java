/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;

import nl.surf.emergo.business.abstr.AbstractDaoBasedComponentManager;
import nl.surf.emergo.domain.EComponent;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IEComponent;

/**
 * The Class ComponentManager.
 */
public class ComponentManager extends AbstractDaoBasedComponentManager
		implements InitializingBean {

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	public void afterPropertiesSet() throws Exception {
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.IComponentManager#getNewComponent()
	 */
	public IEComponent getNewComponent() {
		IEComponent eComponent = new EComponent();
		return eComponent;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.IComponentManager#getComponent(int)
	 */
	public IEComponent getComponent(int comId) {
		IEComponent eComponent = componentDao.get(comId);
		return eComponent;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.IComponentManager#getComponent(java.lang.String)
	 */
	public IEComponent getComponent(String uuid) {
		IEComponent eComponent = componentDao.get(uuid);
		return eComponent;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.IComponentManager#saveComponent(nl.surf.emergo.domain.IEComponent)
	 */
	public void saveComponent(IEComponent eComponent) {
		componentDao.save(eComponent);
		saveComponentInMemory(eComponent);
	}

	/**
	 * Saves references to component in application memory.
	 *
	 * @param eComponent the e component
	 */
	private void saveComponentInMemory(IEComponent eComponent) {
		// save within casecomponents in memory for better performance of player
		Hashtable<String,List<IECaseComponent>> lCaseComponentsInRunByCasId = (Hashtable)appManager.getAppContainer().get(AppConstants.caseComponentsInRunByCasId);
		if (lCaseComponentsInRunByCasId != null) {
			for (Enumeration<String> keys = lCaseComponentsInRunByCasId.keys(); keys.hasMoreElements();) {
				String lId = keys.nextElement();
				List<IECaseComponent> lCaseComponents = lCaseComponentsInRunByCasId.get(lId);
				if (lCaseComponents != null) {
					for (IECaseComponent lCaseComponent : lCaseComponents) {
						if (lCaseComponent.getEComponent().getComId() == eComponent.getComId()) {
							lCaseComponent.setEComponent(eComponent);
							//NOTE Remove data tree references from application memory
							appManager.removeDataTreeReferencesFromAppMemory(lCaseComponent);
						}
					}
				}
			}
		}
		//NOTE Remove def tree references from application memory
		appManager.removeDefTreeReferencesFromAppMemory(eComponent);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.IComponentManager#newComponent(nl.surf.emergo.domain.IEComponent)
	 */
	public List<String[]> newComponent(IEComponent eComponent) {
		// Validate item and if ok, save it after setting creationdate and lastupdatedate.
		List<String[]> lErrors = validateComponent(eComponent);
		if (lErrors == null) {
			eComponent.setCreationdate(new Date());
			eComponent.setLastupdatedate(new Date());
			saveComponent(eComponent);
		}
		return lErrors;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.IComponentManager#updateComponent(nl.surf.emergo.domain.IEComponent)
	 */
	public List<String[]> updateComponent(IEComponent eComponent) {
		// Validate item and if ok, save it after setting lastupdatedate.
		List<String[]> lErrors = validateComponent(eComponent);
		if (lErrors == null) {
			eComponent.setLastupdatedate(new Date());
			saveComponent(eComponent);
		}
		return lErrors;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.IComponentManager#validateComponent(nl.surf.emergo.domain.IEComponent)
	 */
	public List<String[]> validateComponent(IEComponent eComponent) {
		/*
		 * Validate if code is not empty.
		 * Validate if code/version combination is unique.
		 * If validation error return it as element in String[] list.
		 */
		List<String[]> lErrors = new ArrayList<String[]>(0);
		if (appManager.isEmpty(eComponent.getCode()))
			appManager.addError(lErrors, "code", "error_empty");
		else {
			IEComponent lExistingComponent = componentDao.get(eComponent
					.getComId());
			// check if changed
			boolean lChanged = ((lExistingComponent == null)
					|| (!eComponent.getCode().equals(
							lExistingComponent.getCode())) || (eComponent
					.getVersion() != lExistingComponent.getVersion()));
			if ((lChanged)
					&& (componentDao.exists(
							eComponent.getEAccount().getAccId(), eComponent
									.getCode(), eComponent.getVersion())))
				appManager
						.addError(lErrors, "code_version", "error_not_unique");
		}
		if (lErrors.size() > 0)
			return lErrors;
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.IComponentManager#deleteComponent(int)
	 */
	public void deleteComponent(int comId) {
		deleteComponent(getComponent(comId));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.IComponentManager#deleteComponent(nl.surf.emergo.domain.IEComponent)
	 */
	public void deleteComponent(IEComponent eComponent) {
		// check if component removed is parent of other components
		ifParentRemove(eComponent.getComId());
		if (eComponent.getLastupdatedate() == null)
			// lastupdatedate field is added later, so value can be empty for older records
			eComponent.setLastupdatedate(eComponent.getCreationdate());
		componentDao.delete(eComponent);
		//NOTE Remove def tree references from application memory
		appManager.removeDefTreeReferencesFromAppMemory(eComponent);
	}

	/**
	 * Check if component removed is parent of other components.
	 * If so, set parent to 0, no parent.
	 *
	 * @param comId the db com id
	 */
	private void ifParentRemove(int comId) {
		List<IEComponent> lComponents = getAllComponents();
		for (IEComponent lComponent : lComponents) {
			if (lComponent.getComComId() == comId) {
				lComponent.setComComId(0);
				updateComponent(lComponent);
			}
		}
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.IComponentManager#getAllComponents()
	 */
	public List<IEComponent> getAllComponents() {
		return componentDao.getAll();
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.IComponentManager#getAllComponentsByAccId(int)
	 */
	public List<IEComponent> getAllComponentsByAccId(int accId) {
		return componentDao.getAllByAccId(accId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.business.IComponentManager#getAllComponents(boolean)
	 */
	public List<IEComponent> getAllComponents(boolean active) {
		return componentDao.getAll(active);
	}

}