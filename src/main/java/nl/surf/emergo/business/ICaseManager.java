/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business;

import java.util.List;
import java.util.Map;

import nl.surf.emergo.domain.IECase;

/**
 * The Interface ICaseManager.
 * 
 * <p>For managing all cases.</p>
 * 
 * <p>If a case is deleted all related blobs have to be deleted too.
 * Blobs are not deleted in db cascade because they are referenced from within xml data, so should be deleted separately.</p>
 * 
 * <p>Case components are kept in application memory for performance reasons and have a reference to a case, so saveCase updates
 * this reference too.</p>
 */
public interface ICaseManager {

	/**
	 * Gets new case.
	 * 
	 * @return case
	 */
	public IECase getNewCase();

	/**
	 * Gets case.
	 * 
	 * @param casId the db cas id
	 * 
	 * @return case
	 */
	public IECase getCase(int casId);

	/**
	 * Saves case whithout checking object properties.
	 * Also saves reference to case within case components in application memory, for performance reasons.
	 * 
	 * @param eCase the case
	 */
	public void saveCase(IECase eCase);

	/**
	 * Creates new case if object properties are ok.
	 * 
	 * @param eCase the case
	 * @param aCreateOneCaseRole states if one student case role has to be created too, if new case normally case role is created too, if case is copy of another case then case role normally will not be created
	 * 
	 * @return error list
	 */
	public List<String[]> newCase(IECase eCase, boolean aCreateOneCaseRole);

	/**
	 * Updates existing case if object properties are ok.
	 * 
	 * @param eCase the case
	 * 
	 * @return error list
	 */
	public List<String[]> updateCase(IECase eCase);

	/**
	 * Validates case, that is if object properties are ok.
	 * 
	 * @param eCase the case
	 * 
	 * @return error list
	 */
	public List<String[]> validateCase(IECase eCase);

	/**
	 * Deletes case.
	 * Also deletes reference to case within case components in application memory.
	 * Also deletes related blobs.
	 * 
	 * @param casId the db cas id
	 */
	public void deleteCase(int casId);

	/**
	 * Deletes case.
	 * Also deletes reference to case within case components in application memory.
	 * Also deletes related blobs.
	 * 
	 * @param eCase the case
	 */
	public void deleteCase(IECase eCase);

	/**
	 * Deletes blobs related to case.
	 * Blobs are deleted in cascade because they are referenced from within xml data, so should be deleted separately.
	 * 
	 * @param eCase the case
	 */
	public void deleteBlobs(IECase eCase);

	/**
	 * Gets all cases.
	 * 
	 * @return all cases
	 */
	public List<IECase> getAllCases();

	/**
	 * Gets filtered cases.
	 * 
	 * @param keysAndValueParts the db field ids and value parts
	 * 
	 * @return cases
	 */
	public List<IECase> getAllCasesFilter(Map<String, String> keysAndValueParts);

	/**
	 * Gets all cases by acc id.
	 * 
	 * @param accId the db acc id
	 * 
	 * @return cases
	 */
	public List<IECase> getAllCasesByAccId(int accId);

	/**
	 * Gets all cases by owner or author acc id, that is all cases for which account given by accId is either owner or author.
	 * 
	 * @param accId the db acc id
	 * 
	 * @return cases
	 */
	public List<IECase> getAllCasesByOwnerOrAuthorAccId(int accId);

	/**
	 * Gets all runnable cases, so not the ones under construction.
	 * 
	 * @return cases
	 */
	public List<IECase> getAllRunnableCases();

}