/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business;

import java.util.List;

import nl.surf.emergo.domain.IEAccountContext;

/**
 * The Interface IAccountContextManager.
 * 
 * <p>For managing all account contexts.</p>
 * 
 */
public interface IAccountContextManager {

	/**
	 * Gets new account context.
	 * 
	 * @return account context
	 */
	public IEAccountContext getNewAccountContext();

	/**
	 * Gets account context.
	 * 
	 * @param accId the db account context id
	 * 
	 * @return account context
	 */
	public IEAccountContext getAccountContext(int accId);

	/**
	 * Saves account context without checking object properties.
	 * 
	 * @param eAccountContext the account context
	 */
	public void saveAccountContext(IEAccountContext eAccountContext);

	/**
	 * Creates new account context if object properties are ok.
	 * 
	 * @param eAccountContext the account context
	 * 
	 * @return error list
	 */
	public List<String[]> newAccountContext(IEAccountContext eAccountContext);

	/**
	 * Updates existing account context if object properties are ok.
	 * 
	 * @param eAccountContext the account context
	 * 
	 * @return error list
	 */
	public List<String[]> updateAccountContext(IEAccountContext eAccountContext);

	/**
	 * Validates account context, that is if object properties are ok.
	 * 
	 * @param eAccountContext the account context
	 * 
	 * @return error list
	 */
	public List<String[]> validateAccountContext(IEAccountContext eAccountContext);

	/**
	 * Deletes account context.
	 * 
	 * @param accId the db account context id
	 */
	public void deleteAccountContext(int accId);

	/**
	 * Deletes account context.
	 * 
	 * @param eAccountContext the account context
	 */
	public void deleteAccountContext(IEAccountContext eAccountContext);

	/**
	 * Gets all account context.
	 * 
	 * @return account context
	 */
	public List<IEAccountContext> getAllAccountContexts();

	/**
	 * Gets all account context by account id.
	 * 
	 * @param accId the db account id
	 * 
	 * @return account context
	 */
	public List<IEAccountContext> getAllAccountContextsByAccountId(int accId);

	/**
	 * Gets all account context by context id.
	 * 
	 * @param conId the db context id
	 * 
	 * @return account context
	 */
	public List<IEAccountContext> getAllAccountContextsByContextId(int conId);

	/**
	 * Gets all account context by account id and active value.
	 * 
	 * @param accId the db account id
	 * @param active the db active value
	 * 
	 * @return account context
	 */
	public List<IEAccountContext> getAllAccountContextsByAccountIdActive(int accId, boolean active);

	/**
	 * Gets all account context by context id and active value.
	 * 
	 * @param conId the db context id
	 * @param active the db active value
	 * 
	 * @return account context
	 */
	public List<IEAccountContext> getAllAccountContextsByContextIdActive(int conId, boolean active);

	/**
	 * Gets all account context by context ids and active value.
	 * 
	 * @param conIds the db context ids
	 * @param active the db active value
	 * 
	 * @return account context
	 */
	public List<IEAccountContext> getAllAccountContextsByContextIdsActive(List<Integer> conIds, boolean active);

	/**
	 * Gets all account ids by context ids and active value.
	 * 
	 * @param conIds the db context ids
	 * @param active the db active value
	 * 
	 * @return account ids
	 */
	public List<Integer> getAccountContextAccIdsByContextIdsActive(List<Integer> conIds, boolean active);

	/**
	 * Gets all context ids by account id and active value.
	 * 
	 * @param accId the db account id
	 * @param active the db active value
	 * 
	 * @return context ids
	 */
	public List<Integer> getAccountContextConIdsByAccountIdActive(int accId, boolean active);

}