/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business;

import java.util.Hashtable;
import java.util.List;

import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IEComponent;
import nl.surf.emergo.domain.IEMail;

/**
 * The Interface IAppManager.
 *
 * <p>Contains constants and is meant for managing general application tasks.</p>
 */
public interface IAppManager {

	/**
	 * Gets sysvalue.
	 *
	 * @param aSyskey the syskey
	 *
	 * @return sysvalue
	 */
	public String getSysvalue(String aSyskey);

	/**
	 * Sets sysvalue.
	 *
	 * @param aSyskey the syskey
	 * @param aSysvalue the sysvalue
	 */
	public void setSysvalue(String aSyskey, String aSysvalue);

	/**
	 * Executes Sql.
	 *
	 * @param aSql the a sql
	 * @param aErrors the a errors, used to return errors
	 */
	public void executeSql(String aSql, List<String> aErrors);

	/**
	 * Get Sql result.
	 *
	 * @param aSql the a sql
	 * @param aClass the a class
	 * @param aErrors the a errors, used to return errors
	 *
	 * @return result as objects
	 */
	public List<Object> getSqlResult(String aSql, Class aClass, List<String> aErrors);

	/**
	 * Get Sql result.
	 *
	 * @param aSql the a sql
	 * @param aErrors the a errors, used to return errors
	 *
	 * @return result as objects
	 */
	public List<Object> getSqlResult(String aSql, List<String> aErrors);

	/**
	 * Checks if table exists.
	 * 
	 * @param aTableName the a table name 
	 * 
	 * @return if true
	 */
	public boolean sqlTableExists(String aTableName);

	/**
	 * Checks if table column exists.
	 * 
	 * @param aTableName the a table name 
	 * @param aColumnName the a column name 
	 * 
	 * @return if true
	 */
	public boolean sqlTableColumnExists(String aTableName, String aColumnName);

	/**
	 * Gets error string.
	 *
	 * @param aErrorString the error string
	 *
	 * @return error string
	 */
	public String getErrorString(String aErrorString);

	/**
	 * Checks if string is empty.
	 *
	 * @param aValue the value
	 *
	 * @return true, if is empty
	 */
	public boolean isEmpty(String aValue);

	/**
	 * Checks if string is json array.
	 *
	 * @param aValue the value
	 *
	 * @return true, if is json array
	 */
	public boolean isJson(String aValue);

	/**
	 * Adds error to list of errors.
	 *
	 * @param aErrors the errors
	 * @param aField the field
	 * @param aError the error
	 */
	public void addError(List<String[]> aErrors, String aField, String aError);

	/**
	 * Adds warning to list of warnings.
	 *
	 * @param aWarnings the warnings
	 * @param aField the field
	 * @param aWarning the warning
	 */
	public void addWarning(List<String[]> aWarnings, String aField, String aWarning);

	/**
	 * Gets status key.
	 *
	 * @param aIndex the index
	 *
	 * @return status key
	 */
	public String getStatusKey(int aIndex);

	/**
	 * Gets status key index.
	 *
	 * @param aKey the key
	 *
	 * @return status key index
	 */
	public int getStatusKeyIndex(String aKey);

	/**
	 * Gets status value.
	 *
	 * @param aIndex the index
	 *
	 * @return status value
	 */
	public String getStatusValue(int aIndex);

	/**
	 * Gets status value index.
	 *
	 * @param aValue the value
	 *
	 * @return status value index
	 */
	public int getStatusValueIndex(String aValue);

	/**
	 * Gets function.
	 *
	 * @param aIndex the index
	 *
	 * @return function
	 */
	public String getFunction(int aIndex);

	/**
	 * Gets operator.
	 *
	 * @param aIndex the index
	 *
	 * @return operator
	 */
	public String getOperator(int aIndex);

	/**
	 * Gets the tag condition function key ids. That is the function ids that are relevant
	 * for the status key given by aStatusKeyId, when used within a script condition.
	 *
	 * @param aStatusKeyId the a status key id
	 *
	 * @return the tag condition function key ids
	 */
	public List<String> getTagConditionFunctionKeyIds(String aStatusKeyId);

	/**
	 * Gets the tag action function key ids. That is the function ids that are relevant
	 * for the status key given by aStatusKeyId, when used within an script action.
	 *
	 * @param aStatusKeyId the a status key id
	 *
	 * @return the tag action function key ids
	 */
	public List<String> getTagActionFunctionKeyIds(String aStatusKeyId);

	/**
	 * Gets the tag operator key ids. That is the operator ids that are relevant
	 * for the status key given by aStatusKeyId, and the status function given by
	 * aStatusFunctionId.
	 *
	 * @param aStatusKeyId the a status key id
	 * @param aStatusFunctionId the a status function id
	 *
	 * @return the tag operator key ids
	 */
	public List<String> getTagOperatorKeyIds(String aStatusKeyId, String aStatusFunctionId);

	/**
	 * Gets the tag operator value ids.
	 *
	 * @return the tag operator value ids
	 */
	public List<String> getTagOperatorValueIds();

	/**
	 * Gets the tag operator value type. That is the type, like int or boolean, that belongs
	 * to the status key given by aStatusKeyId, and the status function given by
	 * aStatusFunctionId. And within the context of a component given by aComId or a case component given by aCacId,
	 * and a tag given by aTagName.
	 *
	 * @param aComId the a com id
	 * @param aCacId the a cac id
	 * @param aTagName the a tag name
	 * @param aStatusKeyId the a status key id
	 * @param aStatusFunctionId the a status function id
	 *
	 * @return the tag operator value type
	 */
	public String getTagOperatorValueType(String aComId, String aCacId, String aTagName, String aStatusKeyId, String aStatusFunctionId);

	/**
	 * Gets the overwrite status value.
	 * Every change in a status key value is saved within the status, except for start time and current time. These are overwritten.
	 *
	 * @param aStatusKey the a status key
	 *
	 * @return the overwrite status value
	 */
	public boolean getOverwriteStatusValue(String aStatusKey);

	/**
	 * Gets the single status value true. Within a collection some status keys can have only one value true.
	 * For instance there can be only one door that has opened true.
	 *
	 * @param aStatusKey the a status key
	 *
	 * @return the single status value true
	 */
	public String getSingleStatusValueTrue(String aStatusKey);

	/**
	 * Determines if status key should be saved within a rga child tag of the status tag.
	 * This can be true if status is shared between different users.
	 *
	 * @param aStatusKey the a status key
	 * @param aStatusType the a status type
	 *
	 * @return the status in rga tag value
	 */
	public boolean statusInRgaTag(String aStatusKey, String aStatusType);

	/**
	 * Gets mail.
	 *
	 * @param aMaiId the mai id within db
	 *
	 * @return mail
	 */
	public IEMail getMail(int aMaiId);

	/**
	 * Sends mail.
	 *
	 * @param aFrom the from
	 * @param aTos the comma separated tos
	 * @param aCcs the comma separated ccs
	 * @param aBccs the comma separated bccs
	 * @param aSubject the subject
	 * @param aBody the body
	 *
	 * @return error string, empty string if no errors
	 */
	public String sendMail(String aFrom, String aTos, String aCcs, String aBccs, String aSubject, String aBody);

	/**
	 * Gets app container.
	 *
	 * <p>App container is a hashtable to store general application data shared by all sessions.
	 * For instance which run group accounts (rgas) are on-line. And it is used to update run or run team status
	 * of on-line rgas. And to store general data for speeding up application.</p>
	 *
	 * @return app container
	 */
	public Hashtable<String,Object> getAppContainer();

	/**
	 * Gets absolute app path for webapps/emergo
	 *
	 * @return absolute app path.
	 */
	public String getAbsoluteAppPath();

	/**
	 * Gets absolute blob path for webapps/emergo/blob
	 *
	 * @return absolute blob path.
	 */
	public String getAbsoluteBlobPath();

	/**
	 * Gets absolute streaming path for webapps/emergo/streaming
	 *
	 * @return absolute streaming path.
	 */
	public String getAbsoluteStreamingPath();

	/**
	 * Gets absolute temp path for webapps/emergo/temp
	 *
	 * @return absolute temp path.
	 */
	public String getAbsoluteTempPath();

	/**
	 * Gets FFMPeg path pointing to ffmpeg directory used in video conversion
	 *
	 * @return FFMPeg path.
	 */
	public String getFFMPegPath();

	/**
	 * Gets content type for given file name
	 *
	 * @return content type
	 */
	public String getContentType(String pFileName);

	/**
	 * Gets init parameter, a value within web.xml, by key.
	 *
	 * @return init parameter value
	 */
	public String getInitParameter(String aKey);
	
	/**
	 * Adds deftree reference id that should be removed from application memory.
	 *
	 * @param aId the a id
	 */
	public void addDefTreeReferenceToRemoveFromAppMemory(String aId);
	
	/**
	 * Remove deftree references from application memory.
	 *
	 * @param aComponent the a component
	 */
	public void removeDefTreeReferencesFromAppMemory(IEComponent aComponent);

	/**
	 * Adds datatree reference id that should be removed from application memory.
	 *
	 * @param aId the a id
	 */
	public void addDataTreeReferenceToRemoveFromAppMemory(String aId);
	
	/**
	 * Remove datatree references from application memory.
	 *
	 * @param aCaseComponent the a case component
	 */
	public void removeDataTreeReferencesFromAppMemory(IECaseComponent aCaseComponent);

}