/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business;

import java.util.Hashtable;
import java.util.List;

import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IEAccountRequest;

/**
 * The Interface IAccountRequestManager.
 * 
 * <p>For managing all account requests.</p>
 * 
 */
public interface IAccountRequestManager {

	/**
	 * Gets new account request.
	 * 
	 * @return account request
	 */
	public IEAccountRequest getNewAccountRequest();

	/**
	 * Gets account request.
	 * 
	 * @param arqId the db arq id
	 * 
	 * @return account request
	 */
	public IEAccountRequest getAccountRequest(int arqId);

	/**
	 * Gets account request.
	 * 
	 * @param userid the db userid
	 * 
	 * @return account request
	 */
	public IEAccountRequest getAccountRequest(String userid);

	/**
	 * Gets account request.
	 * 
	 * @param userid the db userid
	 * @param password the db password
	 * @param processed the db processed state
	 * 
	 * @return account request
	 */
	public IEAccountRequest getAccountRequest(String userid, String password, boolean processed);

	/**
	 * Gets full account request name.
	 * 
	 * @param eAccountRequest the account request
	 * 
	 * @return full account request name
	 */
	public String getAccountRequestName(IEAccountRequest eAccountRequest);

	/**
	 * Saves account request whithout checking object properties.
	 * 
	 * @param eAccountRequest the account request
	 */
	public void saveAccountRequest(IEAccountRequest eAccountRequest);

	/**
	 * Creates new account request if object properties are ok.
	 * 
	 * @param eAccountRequest the account request
	 * 
	 * @return error list
	 */
	public List<String[]> newAccountRequest(IEAccountRequest eAccountRequest);

	/**
	 * Creates new account request from hashtable items.
	 * 
	 * @param eAccountRequestTable the account request
	 * @param encodePassword the encode password
	 * 
	 * @return error list
	 */
	public List<String[]> newAccountRequest(Hashtable<String,String> eAccountRequestTable, boolean encodePassword);

	/**
	 * Updates existing account request if object properties are ok.
	 * 
	 * @param eAccountRequest the account request
	 * 
	 * @return error list
	 */
	public List<String[]> updateAccountRequest(IEAccountRequest eAccountRequest);

	/**
	 * Validates account request, that is if object properties are ok.
	 * 
	 * @param eAccountRequest the account request
	 * 
	 * @return error list
	 */
	public List<String[]> validateAccountRequest(IEAccountRequest eAccountRequest);

	/**
	 * Deletes account request.
	 * 
	 * @param arqId the db arq id
	 */
	public void deleteAccountRequest(int arqId);

	/**
	 * Deletes account request.
	 * 
	 * @param eAccountRequest the account request
	 */
	public void deleteAccountRequest(IEAccountRequest eAccountRequest);

	/**
	 * Deletes account request.
	 * 
	 * @param aUserId the account request user id
	 */
	public void deleteAccountRequest(String aUserId);

	/**
	 * Gets all account requests.
	 * 
	 * @return account requests
	 */
	public List<IEAccountRequest> getAllAccountRequests();

	/**
	 * Gets all processed or not processed account requests.
	 * 
	 * @param processed the processed state
	 * 
	 * @return account requests
	 */
	public List<IEAccountRequest> getAllAccountRequests(boolean processed);

	/**
	 * Gets the password of a user account request.
	 * 
	 * @param userid the account request userid
	 * 
	 * @return the password
	 */
	public String getAccountRequestPassword(String userid);

	/**
	 * Gets the full name of a user account request.
	 * 
	 * @param userid the account request userid
	 * 
	 * @return the full name
	 */
	public String getAccountRequestName(String userid);
	
	/**
	 * Converts the account request with user id aUserId to a real EMERGO account.
	 * Password and processed state are checked.
	 * 
	 * @param aUserId the account request user id
	 * @param aPassword the account request password
	 * @param aProcessed wether or not the account request already has been processed
	 * 
	 * @return the new account
	 */
	public IEAccount ratifyAccountRequest(String aUserId, String aPassword, boolean aProcessed);

}