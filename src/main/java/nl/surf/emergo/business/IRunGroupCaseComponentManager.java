/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business;

import java.util.List;

import nl.surf.emergo.domain.IERunGroupCaseComponent;

/**
 * The Interface IRunGroupCaseComponentManager.
 * 
 * <p>For managing all run group case components. Run group case component contains xml status of run group per case component.
 * That is xml status for one rungroup in one run.</p>
 * 
 * <p>If a run group case component is deleted all related blobs have to be deleted too.
 * Blobs are not deleted in db cascade because they are referenced from within xml data, so should be deleted separately.</p>
 */
public interface IRunGroupCaseComponentManager {

	/**
	 * Gets new run group case component.
	 * 
	 * @return run group case component
	 */
	public IERunGroupCaseComponent getNewRunGroupCaseComponent();

	/**
	 * Gets run group case component.
	 * 
	 * @param rgcId the db rgc id
	 * 
	 * @return run group case component
	 */
	public IERunGroupCaseComponent getRunGroupCaseComponent(int rgcId);

	/**
	 * Gets run group case component.
	 * 
	 * @param rugId the db rug id
	 * @param cacId the db cac id
	 * 
	 * @return run group case component
	 */
	public IERunGroupCaseComponent getRunGroupCaseComponent(int rugId, int cacId);

	/**
	 * Saves run group case component without checking object properties.
	 * 
	 * @param eRunGroupCaseComponent the run group case component
	 */
	public void saveRunGroupCaseComponent(
			IERunGroupCaseComponent eRunGroupCaseComponent);

	/**
	 * Creates new run group case component if object properties are ok.
	 * 
	 * @param eRunGroupCaseComponent the run group case component
	 * 
	 * @return error list
	 */
	public List<String[]> newRunGroupCaseComponent(
			IERunGroupCaseComponent eRunGroupCaseComponent);

	/**
	 * Updates existing run group case component if object properties are ok.
	 * 
	 * @param eRunGroupCaseComponent the run group case component
	 * 
	 * @return error list
	 */
	public List<String[]> updateRunGroupCaseComponent(
			IERunGroupCaseComponent eRunGroupCaseComponent);

	/**
	 * Deletes run group case component.
	 * Also deletes related blobs.
	 * 
	 * @param rgcId the db rgc id
	 */
	public void deleteRunGroupCaseComponent(int rgcId);

	/**
	 * Deletes run group case component.
	 * Also deletes related blobs.
	 * 
	 * @param eRunGroupCaseComponent the run group case component
	 */
	public void deleteRunGroupCaseComponent(
			IERunGroupCaseComponent eRunGroupCaseComponent);

	/**
	 * Deletes blobs related to run group case component.
	 * Blobs are deleted in cascade because they are referenced from within xml data, so should be deleted separately.
	 * 
	 * @param eRunGroupCaseComponent the run group case component
	 */
	public void deleteBlobs(IERunGroupCaseComponent eRunGroupCaseComponent);

	/**
	 * Gets all run group case components.
	 * 
	 * @return run group case components
	 */
	public List<IERunGroupCaseComponent> getAllRunGroupCaseComponents();

	/**
	 * Gets all run group case components by rug id.
	 * 
	 * @param rugId the db rug id
	 * 
	 * @return run group case components
	 */
	public List<IERunGroupCaseComponent> getAllRunGroupCaseComponentsByRugId(int rugId);

	/**
	 * Gets all run group case components by cac id.
	 * 
	 * @param cacId the db cac id
	 * 
	 * @return run group case components
	 */
	public List<IERunGroupCaseComponent> getAllRunGroupCaseComponentsByCacId(int cacId);

	/**
	 * Gets all run group case components by rug id and cac id.
	 * 
	 * @param rugId the db rug id
	 * @param cacId the db cac id
	 * 
	 * @return run group case components
	 */
	public List<IERunGroupCaseComponent> getAllRunGroupCaseComponentsByRugIdCacId(int rugId, int cacId);
	
	/**
	 * Gets all run group case component by rug ids and cac ids.
	 * 
	 * @param rugId the db rug id
	 * @param cacIds the db cac ids
	 * 
	 * @return run group case components
	 */
	public List<IERunGroupCaseComponent> getAllRunGroupCaseComponentsByRugIdCacIds(int rugId, List<Integer> cacIds);
	
	/**
	 * Gets all run group case components by rug ids and cac id.
	 * 
	 * @param rugIds the db rug ids
	 * @param cacId the db cac id
	 * 
	 * @return run group case components
	 */
	public List<IERunGroupCaseComponent> getAllRunGroupCaseComponentsByRugIdsCacId(List<Integer> rugIds, int cacId);

	/**
	 * Gets all run group case components by rug ids and cac ids.
	 * 
	 * @param rugIds the db rug ids
	 * @param cacIds the db cac ids
	 * 
	 * @return run group case components
	 */
	public List<IERunGroupCaseComponent> getAllRunGroupCaseComponentsByRugIdsCacIds(List<Integer> rugIds, List<Integer> cacIds);
	
}