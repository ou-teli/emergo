/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business;

import java.io.File;
import java.util.List;

/**
 * The Interface IFileManager.
 * 
 * <p>For managing all (blob) files on server.</p>
 */
public interface IFileManager {

	/**
	 * Creates dir.
	 * 
	 * @param dir the dir
	 * 
	 * @return true, if successful
	 */
	public boolean createDir(String dir);

	/**
	 * Deletes dir and all its files and subdirs.
	 * 
	 * @param dir the dir
	 * 
	 * @return true, if successful
	 */
	public boolean deleteDir(String dir);

	/**
	 * Creates file.
	 * 
	 * @param dir the dir
	 * @param fileName the file name
	 * @param bytes the bytes array
	 * 
	 * @return path and filename
	 */
	public String createFile(String dir, String fileName, byte[] bytes);

	/**
	 * Deletes file.
	 * 
	 * @param fileName the file name
	 * 
	 * @return true, if successful
	 */
	public boolean deleteFile(String fileName);

	/**
	 * Copies file.
	 * 
	 * @param fromFile the from file
	 * @param toFile the to file
	 * 
	 * @return true, if successful
	 */
	public boolean copyFile(String fromFile, String toFile);

	/**
	 * Get file.
	 * 
	 * @param fileName the file name
	 * 
	 * @return File
	 */
	public File getFile(String fileName);

	/**
	 * Read file.
	 * 
	 * @param fileName the file name
	 * 
	 * @return byte[] array
	 */
	public byte[] readFile(String fileName);

	/**
	 * Read text file.
	 * 
	 * @param fileName the file name
	 * 
	 * @return list of String
	 */
	public List<String> readTextFile(String fileName);
	
	/**
	 * Gets the file names.
	 * 
	 * @param dir the dir
	 * 
	 * @return file names
	 */
	public List<String> getFileNames(String dir);

	/**
	 * Gets the file names in directory and all subdirectories.
	 * 
	 * @param dir the dir
	 * 
	 * @return file names
	 */
	public List<String> getFileNamesInPathSubTree(String dir);

	/**
	 * File exists.
	 * 
	 * @param fileName the file name
	 * 
	 * @return true, if successful
	 */
	public boolean fileExists(String fileName);

	/**
	 * Rename file.
	 * 
	 * @param oldFileName the old file name
	 * @param newFileName the new file name
	 * 
	 * @return true, if successful
	 */
	public boolean renameFile(String oldFileName, String newFileName);
	
	/**
	 * Get files ind dir.
	 * 
	 * @param dir the dir
	 * 
	 * @return list of Files
	 */
	public File[] getFilesInDir(String dir);

}