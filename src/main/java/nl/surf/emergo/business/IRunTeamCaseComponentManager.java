/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business;

import java.util.List;

import nl.surf.emergo.domain.IERunTeamCaseComponent;

/**
 * The Interface IRunTeamCaseComponentManager.
 * 
 * <p>For managing all run team case components. Run team case component contains xml status of run team per case component.
 * That is xml status shared by all rungroups in a run team.</p>
 * 
 * <p>If a run team case component is deleted all related blobs have to be deleted too.
 * Blobs are not deleted in db cascade because they are referenced from within xml data, so should be deleted separately.</p>
*/
public interface IRunTeamCaseComponentManager {

	/**
	 * Gets new run team case component.
	 * 
	 * @return new run team case component
	 */
	public IERunTeamCaseComponent getNewRunTeamCaseComponent();

	/**
	 * Gets run team case component.
	 * 
	 * @param rtcId the db rtc id
	 * 
	 * @return run team case component
	 */
	public IERunTeamCaseComponent getRunTeamCaseComponent(int rtcId);

	/**
	 * Gets run team case component.
	 * 
	 * @param rutId the db rut id
	 * @param cacId the db cac id
	 * 
	 * @return run team case component
	 */
	public IERunTeamCaseComponent getRunTeamCaseComponent(int rutId, int cacId);

	/**
	 * Saves run team case component without checking object properties.
	 * 
	 * @param eRunTeamCaseComponent the run team case component
	 */
	public void saveRunTeamCaseComponent(
			IERunTeamCaseComponent eRunTeamCaseComponent);

	/**
	 * Creates new run team case component if object properties are ok.
	 * 
	 * @param eRunTeamCaseComponent the run team case component
	 * 
	 * @return error list
	 */
	public List<String[]> newRunTeamCaseComponent(
			IERunTeamCaseComponent eRunTeamCaseComponent);

	/**
	 * Updates existing run team case component if object properties are ok.
	 * 
	 * @param eRunTeamCaseComponent the run team case component
	 * 
	 * @return error list
	 */
	public List<String[]> updateRunTeamCaseComponent(
			IERunTeamCaseComponent eRunTeamCaseComponent);

	/**
	 * Deletes run team case component.
	 * Also deletes related blobs.
	 * 
	 * @param rtcId the db rtc id
	 */
	public void deleteRunTeamCaseComponent(int rtcId);

	/**
	 * Deletes run team case component.
	 * Also deletes related blobs.
	 * 
	 * @param eRunTeamCaseComponent the run team case component
	 */
	public void deleteRunTeamCaseComponent(
			IERunTeamCaseComponent eRunTeamCaseComponent);

	/**
	 * Deletes blobs related to run	team case component.
	 * Blobs are deleted in cascade because they are referenced from within xml data, so should be deleted separately.
	 * 
	 * @param eRunTeamCaseComponent the run team case component
	 */
	public void deleteBlobs(IERunTeamCaseComponent eRunTeamCaseComponent);

	/**
	 * Gets all run team case components.
	 * 
	 * @return run team case components
	 */
	public List<IERunTeamCaseComponent> getAllRunTeamCaseComponents();

	/**
	 * Gets all run team case components by rut id.
	 * 
	 * @param rutId the db rut id
	 * 
	 * @return run team case components
	 */
	public List<IERunTeamCaseComponent> getAllRunTeamCaseComponentsByRutId(int rutId);

	/**
	 * Gets all run team case components by cac id.
	 * 
	 * @param cacId the db cac id
	 * 
	 * @return run team case components
	 */
	public List<IERunTeamCaseComponent> getAllRunTeamCaseComponentsByCacId(int cacId);

	/**
	 * Gets all run team case components by rut id and cac id.
	 * 
	 * @param rutId the db rut id
	 * @param cacId the db cac id
	 * 
	 * @return run team case components
	 */
	public List<IERunTeamCaseComponent> getAllRunTeamCaseComponentsByRutIdCacId(int rutId, int cacId);

}