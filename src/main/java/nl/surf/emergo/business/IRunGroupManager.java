/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business;

import java.util.List;

import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunGroup;

/**
 * The Interface IRunGroupManager.
 * 
 * <p>For managing all run groups.</p>
 * 
  * <p>If a run group is deleted all related blobs have to be deleted too.
 * Blobs are not deleted in db cascade because they are referenced from within xml data, so should be deleted separately.</p>
*/
public interface IRunGroupManager {

	/**
	 * Gets new run group.
	 * 
	 * @return run group
	 */
	public IERunGroup getNewRunGroup();

	/**
	 * Gets run group.
	 * 
	 * @param rugId the db rug id
	 * 
	 * @return run group
	 */
	public IERunGroup getRunGroup(int rugId);

	/**
	 * Gets test run group. If it does not exist, it is created. Is used in preview option.
	 * 
	 * @param eRun the run
	 * @param eCaseRole the case role
	 * 
	 * @return run group
	 */
	public IERunGroup getTestRunGroup(IERun eRun, IECaseRole eCaseRole);

	/**
	 * Saves run group without checking object properties.
	 * 
	 * @param eRunGroup the run group
	 */
	public void saveRunGroup(IERunGroup eRunGroup);

	/**
	 * Creates new run group if object properties are ok.
	 * 
	 * @param eRunGroup the run group
	 * 
	 * @return error list
	 */
	public List<String[]> newRunGroup(IERunGroup eRunGroup);

	/**
	 * Updates existing run group if object properties are ok.
	 * 
	 * @param eRunGroup the run group
	 * 
	 * @return error list
	 */
	public List<String[]> updateRunGroup(IERunGroup eRunGroup);

	/**
	 * Validates run group, that is if object properties are ok.
	 * 
	 * @param eRunGroup the run group
	 * 
	 * @return error list
	 */
	public List<String[]> validateRunGroup(IERunGroup eRunGroup);

	/**
	 * Deletes run group.
	 * Also deletes related blobs.
	 * 
	 * @param rugId the db rug id
	 */
	public void deleteRunGroup(int rugId);

	/**
	 * Deletes run group.
	 * Also deletes related blobs.
	 * 
	 * @param eRunGroup the run group
	 */
	public void deleteRunGroup(IERunGroup eRunGroup);

	/**
	 * Deletes blobs related to run group.
	 * Blobs are deleted in cascade because they are referenced from within xml data, so should be deleted separately.
	 * 
	 * @param eRunGroup the run group
	 */
	public void deleteBlobs(IERunGroup eRunGroup);

	/**
	 * Gets all run groups.
	 * 
	 * @return run groups
	 */
	public List<IERunGroup> getAllRunGroups();

	/**
	 * Gets all run groups by run id.
	 * 
	 * @param runId the db run id
	 * 
	 * @return run groups
	 */
	public List<IERunGroup> getAllRunGroupsByRunId(int runId);

	/**
	 * Gets all run groups by car id.
	 * 
	 * @param carId the db car id
	 * 
	 * @return run groups
	 */
	public List<IERunGroup> getAllRunGroupsByCarId(int carId);

	/**
	 * Gets all run groups by run id and car id.
	 * 
	 * @param runId the db run id
	 * @param carId the db car id
	 * 
	 * @return run groups
	 */
	public List<IERunGroup> getAllRunGroupsByRunIdCarId(int runId, int carId);

	/**
	 * checks if run group allready exists.
	 * 
	 * @param carId the db car id
	 * @param runId the db run id
	 * @param name the db name
	 * 
	 * @return true if exists, false if not
	 */
	public boolean runGroupExists(int carId, int runId, String name);

}