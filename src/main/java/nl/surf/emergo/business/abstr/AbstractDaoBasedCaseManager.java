/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business.abstr;

import nl.surf.emergo.business.IAppManager;
import nl.surf.emergo.business.ICaseComponentManager;
import nl.surf.emergo.business.ICaseComponentRoleManager;
import nl.surf.emergo.business.ICaseManager;
import nl.surf.emergo.business.ICaseRoleManager;
import nl.surf.emergo.business.IComponentManager;
import nl.surf.emergo.business.IRunManager;
import nl.surf.emergo.data.ICaseDao;

/**
 * The Class AbstractDaoBasedCaseManager.
 * 
 * <p>Needed for Spring so dependencies can be injected.
 * All properties are injected at runtime by Spring using setters.
 * Manager uses Dao classes or other manager classes.</p>
 */
public abstract class AbstractDaoBasedCaseManager implements ICaseManager {

	/** The case dao. */
	protected ICaseDao caseDao;

	/** The app manager. */
	protected IAppManager appManager;

	/** The component manager. */
	protected IComponentManager componentManager;

	/** The case component manager. */
	protected ICaseComponentManager caseComponentManager;

	/** The case role manager. */
	protected ICaseRoleManager caseRoleManager;

	/** The case component role manager. */
	protected ICaseComponentRoleManager caseComponentRoleManager;

	/** The run manager. */
	protected IRunManager runManager;

	/**
	 * Sets the case dao.
	 * 
	 * @param dao the case dao
	 */
	public void setCaseDao(ICaseDao dao) {
		this.caseDao = dao;
	}

	/**
	 * Sets the app manager.
	 * 
	 * @param manager the app manager
	 */
	public void setAppManager(IAppManager manager) {
		this.appManager = manager;
	}

	/**
	 * Sets the component manager.
	 * 
	 * @param manager the component manager
	 */
	public void setComponentManager(IComponentManager manager) {
		this.componentManager = manager;
	}

	/**
	 * Sets the case component manager.
	 * 
	 * @param manager the case component manager
	 */
	public void setCaseComponentManager(ICaseComponentManager manager) {
		this.caseComponentManager = manager;
	}

	/**
	 * Sets the case role manager.
	 * 
	 * @param manager the case role manager
	 */
	public void setCaseRoleManager(ICaseRoleManager manager) {
		this.caseRoleManager = manager;
	}

	/**
	 * Sets the case component role manager.
	 * 
	 * @param manager the case component role manager
	 */
	public void setCaseComponentRoleManager(ICaseComponentRoleManager manager) {
		this.caseComponentRoleManager = manager;
	}

	/**
	 * Sets the run manager.
	 * 
	 * @param manager the run manager
	 */
	public void setRunManager(IRunManager manager) {
		this.runManager = manager;
	}
}