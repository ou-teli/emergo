/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business.abstr;

import nl.surf.emergo.business.IAppManager;
import nl.surf.emergo.business.IRunGroupCaseComponentManager;
import nl.surf.emergo.business.IRunGroupManager;
import nl.surf.emergo.data.IRunGroupDao;

/**
 * The Class AbstractDaoBasedRunGroupManager.
 * 
 * <p>Needed for Spring so dependencies can be injected.
 * All properties are injected at runtime by Spring using setters.
 * Manager uses Dao classes or other manager classes.</p>
 */
public abstract class AbstractDaoBasedRunGroupManager implements
		IRunGroupManager {

	/** The run group dao. */
	protected IRunGroupDao runGroupDao;

	/** The app manager. */
	protected IAppManager appManager;

	/** The run group case component manager. */
	protected IRunGroupCaseComponentManager runGroupCaseComponentManager;

	/**
	 * Sets the run group dao.
	 * 
	 * @param dao the run group dao
	 */
	public void setRunGroupDao(IRunGroupDao dao) {
		this.runGroupDao = dao;
	}

	/**
	 * Sets the app manager.
	 * 
	 * @param manager the app manager
	 */
	public void setAppManager(IAppManager manager) {
		this.appManager = manager;
	}

	/**
	 * Sets the run group case component manager.
	 * 
	 * @param manager the run group case component manager
	 */
	public void setRunGroupCaseComponentManager(IRunGroupCaseComponentManager manager) {
		this.runGroupCaseComponentManager = manager;
	}
}