/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business.abstr;

import nl.surf.emergo.business.IAppManager;
import nl.surf.emergo.business.ICaseComponentManager;
import nl.surf.emergo.business.ICaseComponentRoleManager;
import nl.surf.emergo.business.IXmlManager;
import nl.surf.emergo.data.ICaseComponentDao;

/**
 * The Class AbstractDaoBasedCaseComponentManager.
 * 
 * <p>Needed for Spring so dependencies can be injected.
 * All properties are injected at runtime by Spring using setters.
 * Manager uses Dao classes or other manager classes.</p>
 */
public abstract class AbstractDaoBasedCaseComponentManager implements
		ICaseComponentManager {

	/** The case component dao. */
	protected ICaseComponentDao caseComponentDao;

	/** The app manager. */
	protected IAppManager appManager;

	/** The case component role manager. */
	protected ICaseComponentRoleManager caseComponentRoleManager;

	/** The xml manager. */
	protected IXmlManager xmlManager;

	/**
	 * Sets the case component dao.
	 * 
	 * @param dao the case component dao
	 */
	public void setCaseComponentDao(ICaseComponentDao dao) {
		this.caseComponentDao = dao;
	}

	/**
	 * Sets the app manager.
	 * 
	 * @param manager the app manager
	 */
	public void setAppManager(IAppManager manager) {
		this.appManager = manager;
	}

	/**
	 * Sets the case component role manager.
	 * 
	 * @param manager the case component role manager
	 */
	public void setCaseComponentRoleManager(ICaseComponentRoleManager manager) {
		this.caseComponentRoleManager = manager;
	}

	/**
	 * Sets the xml manager.
	 * 
	 * @param manager the xml manager
	 */
	public void setXmlManager(IXmlManager manager) {
		this.xmlManager = manager;
	}
}