/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business.abstr;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import nl.surf.emergo.business.IAccountContextManager;
import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.IAppManager;
import nl.surf.emergo.business.ICaseManager;
import nl.surf.emergo.business.IComponentManager;
import nl.surf.emergo.business.IRunGroupAccountManager;
import nl.surf.emergo.business.IRunGroupManager;
import nl.surf.emergo.business.IRunManager;
import nl.surf.emergo.data.IAccountDao;
import nl.surf.emergo.data.IAccountRoleDao;
import nl.surf.emergo.data.IRoleDao;

/**
 * The Class AbstractDaoBasedAccountManager.
 * 
 * <p>Needed for Spring so dependencies can be injected.
 * All properties are injected at runtime by Spring using setters.
 * Manager uses Dao classes or other manager classes.</p>
 */
public abstract class AbstractDaoBasedAccountManager implements IAccountManager {

	/** The account dao. */
	protected IAccountDao accountDao;

	/** The role dao. */
	protected IRoleDao roleDao;

	/** The account role dao. */
	protected IAccountRoleDao accountRoleDao;

	/** The app manager. */
	protected IAppManager appManager;

	/** The component manager. */
	protected IComponentManager componentManager;

	/** The case manager. */
	protected ICaseManager caseManager;

	/** The run manager. */
	protected IRunManager runManager;

	/** The run group account manager. */
	protected IRunGroupAccountManager runGroupAccountManager;

	/** The run group manager. */
	protected IRunGroupManager runGroupManager;

	/** The account context manager. */
	protected IAccountContextManager accountContextManager;

	/** The bcrypt password encoder. */
	protected BCryptPasswordEncoder bCryptPasswordEncoder;

	/**
	 * Sets the account dao.
	 * 
	 * @param dao the account dao
	 */
	public void setAccountDao(IAccountDao dao) {
		this.accountDao = dao;
	}

	/**
	 * Sets the role dao.
	 * 
	 * @param dao the role dao
	 */
	public void setRoleDao(IRoleDao dao) {
		this.roleDao = dao;
	}

	/**
	 * Sets the account role dao.
	 * 
	 * @param dao the account role dao
	 */
	public void setAccountRoleDao(IAccountRoleDao dao) {
		this.accountRoleDao = dao;
	}

	/**
	 * Sets the app manager.
	 * 
	 * @param manager the app manager
	 */
	public void setAppManager(IAppManager manager) {
		this.appManager = manager;
	}

	/**
	 * Sets the component manager.
	 * 
	 * @param manager the component manager
	 */
	public void setComponentManager(IComponentManager manager) {
		this.componentManager = manager;
	}

	/**
	 * Sets the case manager.
	 * 
	 * @param manager the case manager
	 */
	public void setCaseManager(ICaseManager manager) {
		this.caseManager = manager;
	}

	/**
	 * Sets the run manager.
	 * 
	 * @param manager the run manager
	 */
	public void setRunManager(IRunManager manager) {
		this.runManager = manager;
	}

	/**
	 * Sets the run group account manager.
	 * 
	 * @param manager the run group account manager
	 */
	public void setRunGroupAccountManager(IRunGroupAccountManager manager) {
		this.runGroupAccountManager = manager;
	}

	/**
	 * Sets the run group manager.
	 * 
	 * @param manager the run group manager
	 */
	public void setRunGroupManager(IRunGroupManager manager) {
		this.runGroupManager = manager;
	}

	/**
	 * Sets account context manager.
	 * 
	 * @param manager the account context manager
	 */
	public void setAccountContextManager(IAccountContextManager manager) {
		this.accountContextManager = manager;
	}

	/**
	 * Sets bcrypt password encoder.
	 * 
	 * @param bCryptPasswordEncoder the bcrypt password encoder
	 */
	public void setBCryptPasswordEncoder(BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}

}