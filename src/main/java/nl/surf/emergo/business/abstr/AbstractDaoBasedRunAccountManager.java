/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business.abstr;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.IRunAccountManager;
import nl.surf.emergo.business.IRunManager;
import nl.surf.emergo.data.IRunAccountDao;

/**
 * The Class AbstractDaoBasedRunAccountManager.
 * 
 * <p>Needed for Spring so dependencies can be injected.
 * All properties are injected at runtime by Spring using setters.
 * Manager uses Dao classes or other manager classes.</p>
 */
public abstract class AbstractDaoBasedRunAccountManager implements
		IRunAccountManager {

	/** The run account dao. */
	protected IRunAccountDao runAccountDao;

	/** The run manager. */
	protected IRunManager runManager;

	/** The account manager. */
	protected IAccountManager accountManager;

	/**
	 * Sets the run account dao.
	 * 
	 * @param dao the run account dao
	 */
	public void setRunAccountDao(IRunAccountDao dao) {
		this.runAccountDao = dao;
	}

	/**
	 * Sets the run manager.
	 * 
	 * @param manager the run manager
	 */
	public void setRunManager(IRunManager manager) {
		this.runManager = manager;
	}

	/**
	 * Sets the account manager.
	 * 
	 * @param manager the account manager
	 */
	public void setAccountManager(IAccountManager manager) {
		this.accountManager = manager;
	}

}