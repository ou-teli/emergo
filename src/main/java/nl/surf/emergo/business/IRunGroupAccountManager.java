/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business;

import java.util.List;

import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;

/**
 * The Interface IRunGroupAccountManager.
 * 
 * <p>For managing all run group accounts.</p>
 */
public interface IRunGroupAccountManager {

	/**
	 * Gets new run group account.
	 * 
	 * @return run group account
	 */
	public IERunGroupAccount getNewRunGroupAccount();

	/**
	 * Gets run group account.
	 * 
	 * @param rgaId the db rga id
	 * 
	 * @return run group account
	 */
	public IERunGroupAccount getRunGroupAccount(int rgaId);

	/**
	 * Gets the test run group account. If it does not exist, it is created. Used in preview option.
	 * 
	 * @param eRunGroup the run group
	 * @param eAccount the account
	 * 
	 * @return run group account
	 */
	public IERunGroupAccount getTestRunGroupAccount(IERunGroup eRunGroup,
			IEAccount eAccount);

	/**
	 * Saves run group account without checking object properties.
	 * 
	 * @param eRunGroupAccount the run group account
	 */
	public void saveRunGroupAccount(IERunGroupAccount eRunGroupAccount);

	/**
	 * Creates new run group account if object properties are ok.
	 * 
	 * @param eRunGroupAccount the run group account
	 * 
	 * @return error list
	 */
	public List<String[]> newRunGroupAccount(IERunGroupAccount eRunGroupAccount);

	/**
	 * Updates existing run group account if object properties are ok.
	 * 
	 * @param eRunGroupAccount the run group account
	 * 
	 * @return error list
	 */
	public List<String[]> updateRunGroupAccount(IERunGroupAccount eRunGroupAccount);

	/**
	 * Deletes run group account.
	 * 
	 * @param rgaId the db rga id
	 */
	public void deleteRunGroupAccount(int rgaId);

	/**
	 * Deletes run group account.
	 * 
	 * @param eRunGroupAccount the run group account
	 */
	public void deleteRunGroupAccount(IERunGroupAccount eRunGroupAccount);

	/**
	 * Gets all run group accounts.
	 * 
	 * @return run group accounts
	 */
	public List<IERunGroupAccount> getAllRunGroupAccounts();

	/**
	 * Gets all run group accounts by rga ids.
	 * 
	 * @param rgaIds the db rga ids
	 * 
	 * @return run group accounts
	 */
	public List<IERunGroupAccount> getAllRunGroupAccountsByRgaIds(List<Integer> rgaIds);

	/**
	 * Gets all run group accounts by rug id.
	 * 
	 * @param rugId the db rug id
	 * 
	 * @return run group accounts
	 */
	public List<IERunGroupAccount> getAllRunGroupAccountsByRugId(int rugId);

	/**
	 * Gets all run group accounts by rug ids.
	 * 
	 * @param rugIds the db rug ids
	 * 
	 * @return run group accounts
	 */
	public List<IERunGroupAccount> getAllRunGroupAccountsByRugIds(List<Integer> rugIds);

	/**
	 * Gets all run group accounts by acc id.
	 * 
	 * @param accId the db acc id
	 * 
	 * @return run group accounts
	 */
	public List<IERunGroupAccount> getAllRunGroupAccountsByAccId(int accId);

	/**
	 * Gets all run group accounts by acc ids.
	 * 
	 * @param accIds the db acc ids
	 * 
	 * @return run group accounts
	 */
	public List<IERunGroupAccount> getAllRunGroupAccountsByAccIds(List<Integer> accIds);

	/**
	 * Gets all run group accounts by rug id and acc id.
	 * 
	 * @param rugId the db rug id
	 * @param accId the db acc id
	 * 
	 * @return run group accounts
	 */
	public List<IERunGroupAccount> getAllRunGroupAccountsByRugIdAccId(int rugId, int accId);

}