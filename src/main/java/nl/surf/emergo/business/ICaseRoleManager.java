/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business;

import java.util.List;

import nl.surf.emergo.domain.IECaseRole;

/**
 * The Interface ICaseRoleManager.
 * 
 * <p>For managing all case roles.</p>
 * 
  * <p>If a case role is deleted all related blobs have to be deleted too.
 * Blobs are not deleted in db cascade because they are referenced from within xml data, so should be deleted separately.</p>
 * 
*/
public interface ICaseRoleManager {

	/**
	 * Gets new case role.
	 * 
	 * @return case role
	 */
	public IECaseRole getNewCaseRole();

	/**
	 * Gets case role.
	 * 
	 * @param carId the db car id
	 * 
	 * @return case role
	 */
	public IECaseRole getCaseRole(int carId);

	/**
	 * Saves case role without checking object properties.
	 * 
	 * @param eCaseRole the case role
	 */
	public void saveCaseRole(IECaseRole eCaseRole);

	/**
	 * Creates new case role if object properties are ok.
	 * 
	 * @param eCaseRole the case role
	 * 
	 * @return error list
	 */
	public List<String[]> newCaseRole(IECaseRole eCaseRole);

	/**
	 * Updates existing case role if object properties are ok.
	 * 
	 * @param eCaseRole the case role
	 * 
	 * @return error list
	 */
	public List<String[]> updateCaseRole(IECaseRole eCaseRole);

	/**
	 * Validates case role, that is if object properties are ok.
	 * 
	 * @param eCaseRole the case role
	 * 
	 * @return error list
	 */
	public List<String[]> validateCaseRole(IECaseRole eCaseRole);

	/**
	 * Deletes case role.
	 * Also deletes related blobs.
	 * 
	 * @param carId the db car id
	 */
	public void deleteCaseRole(int carId);

	/**
	 * Deletes case role.
	 * Also deletes related blobs.
	 * 
	 * @param eCaseRole the case role
	 */
	public void deleteCaseRole(IECaseRole eCaseRole);

	/**
	 * Deletes blobs related to case role.
	 * Blobs are not deleted in db cascade because they are referenced from within xml data, so should be deleted separately.
	 * 
	 * @param eCaseRole the case role
	 */
	public void deleteBlobs(IECaseRole eCaseRole);

	/**
	 * Gets all case roles.
	 * 
	 * @return case roles
	 */
	public List<IECaseRole> getAllCaseRoles();

	/**
	 * Gets all case roles by cas id.
	 * 
	 * @param casId the db cas id
	 * 
	 * @return case roles
	 */
	public List<IECaseRole> getAllCaseRolesByCasId(int casId);

	/**
	 * Gets all case roles.
	 * 
	 * @param npc the npc state, npc=non player character
	 * 
	 * @return case roles
	 */
	public List<IECaseRole> getAllCaseRoles(boolean npc);

	/**
	 * Gets all case roles by cas id.
	 * 
	 * @param casId the db cas id
	 * @param npc the npc state, npc=non player character
	 * 
	 * @return case roles
	 */
	public List<IECaseRole> getAllCaseRolesByCasId(int casId, boolean npc);

}