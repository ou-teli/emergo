/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business;

import java.util.List;

import nl.surf.emergo.domain.IERunAccount;

/**
 * The Interface IRunAccountManager.
 * 
 * <p>For managing all run accounts.</p>
 */
public interface IRunAccountManager {

	/**
	 * Gets new run account.
	 * 
	 * @return run account
	 */
	public IERunAccount getNewRunAccount();

	/**
	 * Gets run account.
	 * 
	 * @param ruaId the db rua id
	 * 
	 * @return run account
	 */
	public IERunAccount getRunAccount(int ruaId);

	/**
	 * Saves run account without checking object properties.
	 * 
	 * @param eRunAccount the run account
	 */
	public void saveRunAccount(IERunAccount eRunAccount);

	/**
	 * Creates new run account if object properties are ok.
	 * 
	 * @param eRunAccount the run account
	 * 
	 * @return error list
	 */
	public List<String[]> newRunAccount(IERunAccount eRunAccount);

	/**
	 * Updates existing run account if object properties are ok.
	 * 
	 * @param eRunAccount the run account
	 * 
	 * @return error list
	 */
	public List<String[]> updateRunAccount(IERunAccount eRunAccount);

	/**
	 * Deletes run account.
	 * 
	 * @param ruaId the db rua id
	 */
	public void deleteRunAccount(int ruaId);

	/**
	 * Deletes run account.
	 * 
	 * @param eRunAccount the run account
	 */
	public void deleteRunAccount(IERunAccount eRunAccount);

	/**
	 * Gets all run accounts.
	 * 
	 * @return run accounts
	 */
	public List<IERunAccount> getAllRunAccounts();

	/**
	 * Gets all run accounts by run id.
	 * 
	 * @param runId the db run id
	 * 
	 * @return run accounts
	 */
	public List<IERunAccount> getAllRunAccountsByRunId(int runId);

	/**
	 * Gets all run accounts by acc id.
	 * 
	 * @param accId the db acc id
	 * 
	 * @return run accounts
	 */
	public List<IERunAccount> getAllRunAccountsByAccId(int accId);

	/**
	 * Gets all run accounts by acc id where account is active as tutor.
	 * 
	 * @param accId the db acc id
	 * 
	 * @return run accounts
	 */
	public List<IERunAccount> getAllRunAccountsByAccIdActiveAsTutor(int accId);

	/**
	 * Gets all run accounts by acc id where account is active as student.
	 * 
	 * @param accId the db acc id
	 * 
	 * @return run accounts
	 */
	public List<IERunAccount> getAllRunAccountsByAccIdActiveAsStudent(int accId);

	/**
	 * Gets run account by run id and acc id if it exists.
	 * 
	 * @param runId the db run id
	 * @param accId the db acc id
	 * 
	 * @return run account if exists, else null
	 */
	public IERunAccount getRunAccountByRunIdAccId(int runId, int accId);

	/**
	 * Adds runaccount.
	 * 
	 * @param aAccId the account id
	 * @param aRunId the run id
	 * 
	 * @return the runaccount
	 */
	public IERunAccount addRunAccount(int aRunId, int aAccId);
	
	/**
	 * Adds runaccount if not already present. If not present, set new runaccount status of corresponding Emergo role to true
	 * 
	 * @param aAccId the account id
	 * @param aRunId the run id
	 * @param aRole the role
	 * 
	 * @return the runaccount
	 */
	public IERunAccount addRunAccountIfNotExists(int aRunId, int aAccId, String aRole);

	/**
	 * Sets runaccount status.
	 * 
	 * @param aRunaccount the run account
	 * @param aRole the emergo role of the account for which the status is set
	 * @param aChecked the value of the new status
	 * 
	 */
	public void setRunAccountStatus (IERunAccount aRunaccount, String aRole, boolean aChecked);
	
}