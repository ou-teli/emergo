/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business;

import java.util.List;

import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunTeam;

/**
 * The Interface IRunTeamManager.
 * 
 * <p>For managing all run teams.</p>
 * 
 * <p>If a run team is deleted all related blobs have to be deleted too.
 * Blobs are not deleted in db cascade because they are referenced from within xml data, so should be deleted separately.</p>
 */
public interface IRunTeamManager {

	/**
	 * Gets new run team.
	 * 
	 * @return run team
	 */
	public IERunTeam getNewRunTeam();

	/**
	 * Gets run team.
	 * 
	 * @param rutId the db rut id
	 * 
	 * @return run team
	 */
	public IERunTeam getRunTeam(int rutId);

	/**
	 * Gets test run team. If it does not exist, it is created. Is used in preview option.
	 * 
	 * @param eRun the run
	 * 
	 * @return run team
	 */
	public IERunTeam getTestRunTeam(IERun eRun);

	/**
	 * Saves run team without checking object properties.
	 * 
	 * @param eRunTeam the run team
	 */
	public void saveRunTeam(IERunTeam eRunTeam);

	/**
	 * Creates new run team if object properties are ok.
	 * 
	 * @param eRunTeam the run team
	 * 
	 * @return error list
	 */
	public List<String[]> newRunTeam(IERunTeam eRunTeam);

	/**
	 * Updates existing run team if object properties are ok.
	 * 
	 * @param eRunTeam the run team
	 * 
	 * @return error list
	 */
	public List<String[]> updateRunTeam(IERunTeam eRunTeam);

	/**
	 * Validates run team, that is if object properties are ok.
	 * 
	 * @param eRunTeam the run team
	 * 
	 * @return error list
	 */
	public List<String[]> validateRunTeam(IERunTeam eRunTeam);

	/**
	 * Deletes run team.
	 * Also deletes related blobs.
	 * 
	 * @param rutId the db rut id
	 */
	public void deleteRunTeam(int rutId);

	/**
	 * Deletes run team.
	 * Also deletes related blobs.
	 * 
	 * @param eRunTeam the run team
	 */
	public void deleteRunTeam(IERunTeam eRunTeam);

	/**
	 * Deletes blobs related to run team.
	 * Blobs are deleted in cascade because they are referenced from within xml data, so should be deleted separately.
	 * 
	 * @param eRunTeam the run team
	 */
	public void deleteBlobs(IERunTeam eRunTeam);

	/**
	 * Gets all run teams.
	 * 
	 * @return run teams
	 */
	public List<IERunTeam> getAllRunTeams();

	/**
	 * Gets all run teams by run id.
	 * 
	 * @param runId the db run id
	 * 
	 * @return run teams
	 */
	public List<IERunTeam> getAllRunTeamsByRunId(int runId);

}