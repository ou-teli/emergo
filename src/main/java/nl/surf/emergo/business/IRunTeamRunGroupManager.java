/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business;

import java.util.List;

import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunTeam;
import nl.surf.emergo.domain.IERunTeamRunGroup;

/**
 * The Interface IRunTeamRunGroupManager.
 * 
 * <p>For managing all run team run groups.</p>
 */
public interface IRunTeamRunGroupManager {

	/**
	 * Gets new run team run group.
	 * 
	 * @return run team run group
	 */
	public IERunTeamRunGroup getNewRunTeamRunGroup();

	/**
	 * Gets run team run group.
	 * 
	 * @param rtrId the db rtr id
	 * 
	 * @return run team run group
	 */
	public IERunTeamRunGroup getRunTeamRunGroup(int rtrId);

	/**
	 * Gets test run team run group. If it does not exist, it is created. Is used in preview option.
	 * 
	 * @param eRunTeam the run team
	 * @param eRunGroup the run group
	 * 
	 * @return run team run group
	 */
	public IERunTeamRunGroup getTestRunTeamRunGroup(IERunTeam eRunTeam,
			IERunGroup eRunGroup);

	/**
	 * Saves run team run group without checking object properties.
	 * 
	 * @param eRunTeamRunGroup the e run team run group
	 */
	public void saveRunTeamRunGroup(IERunTeamRunGroup eRunTeamRunGroup);

	/**
	 * Creates new run team run group if object properties are ok.
	 * 
	 * @param eRunTeamRunGroup the run team run group
	 * 
	 * @return error list
	 */
	public List<String[]> newRunTeamRunGroup(IERunTeamRunGroup eRunTeamRunGroup);

	/**
	 * Updates existing run team run group if object properties are ok.
	 * 
	 * @param eRunTeamRunGroup the run team run group
	 * 
	 * @return error list
	 */
	public List<String[]> updateRunTeamRunGroup(IERunTeamRunGroup eRunTeamRunGroup);

	/**
	 * Deletes run team run group.
	 * 
	 * @param rtrId the db rtr id
	 */
	public void deleteRunTeamRunGroup(int rtrId);

	/**
	 * Deletes run team run group.
	 * 
	 * @param eRunTeamRunGroup the run team run group
	 */
	public void deleteRunTeamRunGroup(IERunTeamRunGroup eRunTeamRunGroup);

	/**
	 * Gets all run team run groups.
	 * 
	 * @return run team run groups
	 */
	public List<IERunTeamRunGroup> getAllRunTeamRunGroups();

	/**
	 * Gets all run team run groups by rut id.
	 * 
	 * @param rutId the db rut id
	 * 
	 * @return run team run groups
	 */
	public List<IERunTeamRunGroup> getAllRunTeamRunGroupsByRutId(int rutId);

	/**
	 * Gets all run team run groups by rug id.
	 * 
	 * @param rugId the db rug id
	 * 
	 * @return run team run groups
	 */
	public List<IERunTeamRunGroup> getAllRunTeamRunGroupsByRugId(int rugId);

	/**
	 * Gets all run team run groups by rug ids.
	 * 
	 * @param rugIds the db rug ids
	 * 
	 * @return run team run groups
	 */
	public List<IERunTeamRunGroup> getAllRunTeamRunGroupsByRugIds(List<Integer> rugIds);

	/**
	 * Gets all run team run groups by rut id and rug id.
	 * 
	 * @param rutId the db rut id
	 * @param rugId the db rug id
	 * 
	 * @return run team run groups
	 */
	public List<IERunTeamRunGroup> getAllRunTeamRunGroupsByRutIdRugId(int rutId, int rugId);

}