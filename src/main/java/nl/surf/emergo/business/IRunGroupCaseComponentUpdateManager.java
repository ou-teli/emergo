/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business;

import java.util.List;

import nl.surf.emergo.domain.IERunGroupCaseComponentUpdate;

/**
 * The Interface IRunGroupCaseComponentUpdateManager.
 * 
 * <p>For managing all run group case component updates.
 * Run group case component update contains xml data that is used to update status of one rungroup that is set by another.
 * For instance when one rungroup sends an e-message to another rungroup.</p>
 * 
 * <p>If a run group case component update is deleted all related blobs have to be deleted too.
 * Blobs are not deleted in db cascade because they are referenced from within xml data, so should be deleted separately.</p>
*/
public interface IRunGroupCaseComponentUpdateManager {

	/**
	 * Gets new run group case component update.
	 * 
	 * @return run group case component update
	 */
	public IERunGroupCaseComponentUpdate getNewRunGroupCaseComponentUpdate();

	/**
	 * Gets run group case component update.
	 * 
	 * @param rguId the db rgu id
	 * 
	 * @return run group case component update
	 */
	public IERunGroupCaseComponentUpdate getRunGroupCaseComponentUpdate(int rguId);

	/**
	 * Saves run group case component update without checking object properties.
	 * 
	 * @param eRunGroupCaseComponentUpdate the run group case component update
	 */
	public void saveRunGroupCaseComponentUpdate(
			IERunGroupCaseComponentUpdate eRunGroupCaseComponentUpdate);

	/**
	 * Creates new run group case component update if object properties are ok.
	 * 
	 * @param eRunGroupCaseComponentUpdate the run group case component update
	 * 
	 * @return error list
	 */
	public List<String[]> newRunGroupCaseComponentUpdate(
			IERunGroupCaseComponentUpdate eRunGroupCaseComponentUpdate);

	/**
	 * Updates existing run group case component update if object properties are ok.
	 * 
	 * @param eRunGroupCaseComponentUpdate the run group case component update
	 * 
	 * @return error list
	 */
	public List<String[]> updateRunGroupCaseComponentUpdate(
			IERunGroupCaseComponentUpdate eRunGroupCaseComponentUpdate);

	/**
	 * Deletes run group case component update.
	 * Also deletes related blobs.
	 * 
	 * @param rguId the db rgu id
	 */
	public void deleteRunGroupCaseComponentUpdate(int rguId);

	/**
	 * Deletes run group case component update.
	 * Also deletes related blobs.
	 * 
	 * @param eRunGroupCaseComponentUpdate the run group case component update
	 */
	public void deleteRunGroupCaseComponentUpdate(
			IERunGroupCaseComponentUpdate eRunGroupCaseComponentUpdate);

	/**
	 * Deletes blobs related to run group case component update.
	 * Blobs are deleted in cascade because they are referenced from within xml data, so should be deleted separately.
	 * 
	 * @param eRunGroupCaseComponentUpdate the run group case component update
	 */
	public void deleteBlobs(IERunGroupCaseComponentUpdate eRunGroupCaseComponentUpdate);

	/**
	 * Gets all run group case components update.
	 * 
	 * @return run group case components update
	 */
	public List<IERunGroupCaseComponentUpdate> getAllRunGroupCaseComponentUpdates();

	/**
	 * Gets all run group case component updates by rug id.
	 * 
	 * @param rugId the db rug id
	 * 
	 * @return run group case component updates
	 */
	public List<IERunGroupCaseComponentUpdate> getAllRunGroupCaseComponentUpdatesByRugId(int rugId);

	/**
	 * Gets all run group case component updates by rug id.
	 * 
	 * @param rugId the db rug id
	 * @param processed the processed state, that is if the update is processed
	 * 
	 * @return run group case component updates
	 */
	public List<IERunGroupCaseComponentUpdate> getAllRunGroupCaseComponentUpdatesByRugId(int rugId, boolean processed);

	/**
	 * Gets all run group case component updates by cac id.
	 * 
	 * @param cacId the db cac id
	 * 
	 * @return run group case component updates
	 */
	public List<IERunGroupCaseComponentUpdate> getAllRunGroupCaseComponentUpdatesByCacId(int cacId);

	/**
	 * Gets all run group case component updates by rug from id.
	 * 
	 * @param rugFromId the db rug from id
	 * 
	 * @return run group case component updates
	 */
	public List<IERunGroupCaseComponentUpdate> getAllRunGroupCaseComponentUpdatesByRugFromId(int rugFromId);

}