/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business;

import java.io.File;
import java.util.List;

import nl.surf.emergo.domain.IEBlob;

/**
 * The Interface IBlobManager.
 * 
 * <p>For managing all blobs within application. Blobs can be database blobs, files on the server or urls.
 * Blobs can be used everywhere. For instance as an e-message attachment, or a user-generated reference.</p>
 * 
 * <p>First blobs were stored completely within the database, also the file itself. Therefore they are called blobs.
 * Due to performance reasons, we later decided to store all blob files as files on the server in a separate blob map.</p>
 */
public interface IBlobManager {

	/**
	 * Gets new blob.
	 * 
	 * @return blob
	 */
	public IEBlob getNewBlob();

	/**
	 * Gets blob.
	 * 
	 * @param bloId the db blo id
	 * 
	 * @return blob
	 */
	public IEBlob getBlob(int bloId);

	/**
	 * Saves blob whithout checking object properties.
	 * 
	 * @param aBlob the blob
	 */
	public void saveBlob(IEBlob aBlob);

	/**
	 * Creates new blob if object properties are ok.
	 * Depreciated, use newFileBlob instead.
	 * 
	 * @param eBlob the blob
	 * 
	 * @return error list
	 */
	public List<String[]> newBlob(IEBlob aBlob);

	/**
	 * Creates new blob if object properties are ok. Content is saved as file on server.
	 * 
	 * @param aBlob the blob
	 * @param aContent the content
	 * 
	 * @return error list
	 */
	public List<String[]> newFileBlob(IEBlob aBlob, byte[] aContent);

	/**
	 * Creates new blob if object properties are ok. Blob file of eOldBlob is used and is saved as file on server.
	 * 
	 * @param aOldBlob the old blob
	 * @param aNewBlob the new blob
	 * 
	 * @return error list
	 */
	public List<String[]> newFileBlob(IEBlob aOldBlob, IEBlob aNewBlob);

	/**
	 * Updates existing blob if object properties are ok.
	 * Depreciated, use updateFileBlob instead.
	 * 
	 * @param aBlob the blob
	 * 
	 * @return error list
	 */
	public List<String[]> updateBlob(IEBlob aBlob);

	/**
	 * Updates existing blob if object properties are ok. Content is saved as file on server.
	 * 
	 * @param aBlob the blob
	 * @param aContent the content
	 * 
	 * @return error list
	 */
	public List<String[]> updateFileBlob(IEBlob aBlob, byte[] aContent);

	/**
	 * Creates file in map given by aBloId and within blob map.
	 * 
	 * @param aBlob the blob
	 * @param aContent the content
	 * 
	 * @return path plus file on the server
	 */
	public String createFile(IEBlob aBlob, byte[] aContent);

	/**
	 * Copies file from map given by aOldBloId to map given by aNewBloId. Both in the blob map.
	 * 
	 * @param aOldBloId the old blo id
	 * @param aNewBloId the new blo id
	 * @param aFileName the file name
	 * 
	 * @return true, if successful
	 */
	public boolean copyFile(int aOldBloId, int aNewBloId, String aFileName);

	/**
	 * Gets a file in map given by aBloId and within blob map.
	 * 
	 * @param aBloId the blo id
	 * @param aFileName the file name
	 * 
	 * @return File
	 */
	public File getFile(int aBloId, String aFileName);

	/**
	 * Reads a file in map given by aBloId and within blob map, into a byte array.
	 * 
	 * @param aBloId the blo id
	 * @param aFileName the file name
	 * 
	 * @return byte[] array
	 */
	public byte[] readFile(int aBloId, String aFileName);

	/**
	 * Deletes file defined within blob.
	 * 
	 * @param aBlob the blob
	 */
	public void deleteFile(IEBlob aBlob);

	/**
	 * Deletes file defined within blob and the folder containing it.
	 * 
	 * @param aBlob the blob
	 */
	public void deleteFileAndFolder(IEBlob aBlob);

	/**
	 * Deletes blob.
	 * 
	 * @param bloId the db blo id
	 */
	public void deleteBlob(int bloId);

	/**
	 * Deletes blob.
	 * 
	 * @param aBlob the blob
	 */
	public void deleteBlob(IEBlob aBlob);

	/**
	 * Deletes blob.
	 * 
	 * @param aBlob the blob
	 * @param aDeleteFolder the delete folder
	 */
	public void deleteBlob(IEBlob aBlob, boolean aDeleteFolder);

	/**
	 * Copies blob.
	 * 
	 * @param bloId the db blo id
	 * 
	 * @return new db blo id
	 */
	public int copyBlob(int bloId);

	/**
	 * Gets all blobs.
	 * 
	 * @return blobs
	 */
	public List<IEBlob> getAllBlobs();

	/**
	 * Gets all blobs given by blo ids.
	 * 
	 * @param bloIds the db blo ids as string list
	 * 
	 * @return blobs
	 */
	public List<IEBlob> getAllBlobsByBloIds(List<Integer> bloIds);

}

