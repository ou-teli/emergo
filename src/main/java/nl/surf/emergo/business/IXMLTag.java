/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.business;

import java.util.Hashtable;
import java.util.List;

/**
 * The Interface IXMLTag.
 * 
 * <p>For all xml tag operations.</p>
 */
public interface IXMLTag {

	/**
	 * Gets type.
	 * 
	 * @return type
	 */
	public int getType();

	/**
	 * Sets type.
	 * 
	 * @param aType the type
	 */
	public void setType(int aType);

	/**
	 * Gets xml defition tag.
	 * 
	 * @return def tag
	 */
	public IXMLTag getDefTag();

	/**
	 * Sets xml definition tag.
	 * 
	 * @param aDefTag the def tag
	 */
	public void setDefTag(IXMLTag aDefTag);

	/**
	 * Gets the data tag.
	 * 
	 * @return the data tag
	 */
	public IXMLTag getDataTag();

	/**
	 * Sets the data tag.
	 * 
	 * @param aDataTag the data tag
	 */
	public void setDataTag(IXMLTag aDataTag);

	/**
	 * Gets the status tag.
	 * 
	 * @return the status tag
	 */
	public IXMLTag getStatusTag();

	/**
	 * Sets the status tag.
	 * 
	 * @param aStatusTag the status tag
	 */
	public void setStatusTag(IXMLTag aStatusTag);

	/**
	 * Gets the status tags.
	 * 
	 * @return the status tags
	 */
	public List<IXMLTag> getStatusTags();

	/**
	 * Sets the status tags.
	 * 
	 * @param aStatusTags the status tags
	 */
	public void setStatusTags(List<IXMLTag> aStatusTags);

	/**
	 * Add the status tag.
	 * 
	 * @param aStatusTag the status tag
	 */
	public void addStatusTag(IXMLTag aStatusTag);

	/**
	 * Remove the status tag.
	 * 
	 * @param aId the id
	 */
	public void removeStatusTag(String aId);

	/**
	 * Get the status tag.
	 * 
	 * @param aId the id
	 */
	public IXMLTag getStatusTag(String aId);

	/**
	 * Gets parent tag.
	 * 
	 * @return parent tag
	 */
	public IXMLTag getParentTag();

	/**
	 * Sets parent tag.
	 * 
	 * @param aParentTag the parent tag
	 */
	public void setParentTag(IXMLTag aParentTag);

	/**
	 * Gets tag name.
	 * 
	 * @return name
	 */
	public String getName();

	/**
	 * Sets tag name.
	 * 
	 * @param aName the name
	 */
	public void setName(String aName);

	/**
	 * Gets the tag value.
	 * 
	 * @return the value
	 */
	public String getValue();

	/**
	 * Sets the tag value.
	 * 
	 * @param aValue the value
	 */
	public void setValue(String aValue);

	/**
	/**
	 * Gets the tag attributes.
	 * 
	 * @return the attributes
	 */
	public Hashtable<String,List<IXMLAttributeValueTime>> getAttributes();

	/**
	 * Sets the tag attributes.
	 * 
	 * @param aAttributes the attributes
	 */
	public void setAttributes(Hashtable<String,List<IXMLAttributeValueTime>> aAttributes);

	/**
	 * Gets the child tags.
	 * 
	 * @return the child tags
	 */
	public List<IXMLTag> getChildTags();

	/**
	 * Sets the child tags.
	 * 
	 * @param aChildTags the child tags
	 */
	public void setChildTags(List<IXMLTag> aChildTags);

	/**
	 * Gets the attribute value.
	 * 
	 * @param aKey the attribute key
	 * 
	 * @return the attribute value
	 */
	public String getAttribute(String aKey);

	/**
	 * Sets attribute value. If aKey not exists it is added.
	 * 
	 * @param aKey the attribute key
	 * @param aValue the attribute value
	 */
	public void setAttribute(String aKey, String aValue);

	/**
	 * Sets attribute value and time. If aKey not exists it is added.
	 * 
	 * @param aKey the attribute key
	 * @param aValue the attribute value
	 * @param aTime the attribute time
	 */
	public void setAttribute(String aKey, String aValue, double aTime);

	/**
	 * Gets the attribute value as list.
	 * 
	 * @param aKey the attribute key
	 * 
	 * @return the attribute value
	 */
	public List<IXMLAttributeValueTime> getAttributeAsList(String aKey);

	/**
	 * Sets attribute value as list. If aKey not exists it is added.
	 * 
	 * @param aKey the attribute key
	 * @param aValue the attribute value as list
	 */
	public void setAttributeAsList(String aKey, List<IXMLAttributeValueTime> aValue);

	/**
	 * Adds attribute value time to list. If attribute aKey not exists it is added.
	 * 
	 * @param aKey the attribute key
	 * @param aValue the value
	 * @param aTime the time
	 */
	public void addAttributeValueTimeToList(String aKey, String aValue, double aTime);
	
	/**
	 * Checks if attribute aKey is existing.
	 * 
	 * @param aKey the key
	 * 
	 * @return true, if aKey is attribute
	 */
	public boolean isAttribute(String aKey);

	/**
	 * Gets the child with name aName.
	 * 
	 * @param aName the name
	 * 
	 * @return the xml child tag
	 */
	public IXMLTag getChild(String aName);

	/**
	 * Gets the childs with name aNames. aNames is comma separated list. Not nested.
	 * 
	 * @param aNames the names
	 * 
	 * @return the xml child tags
	 */
	public List<IXMLTag> getChilds(String aNames);

	/**
	 * Gets the child tags with attribute type set to aType. Not nested.
	 * 
	 * @param aType the type
	 * 
	 * @return the xml child tags
	 */
	public List<IXMLTag> getChildTags(String aType);

	/**
	 * Gets the value of xml child with name aChildName.
	 * 
	 * @param aChildName the child name
	 * 
	 * @return the child value
	 */
	public String getChildValue(String aChildName);

	/**
	 * Sets the value of xml child with name aChildName.
	 * 
	 * @param aChildName the child name
	 * @param aChildValue the child value
	 */
	public void setChildValue(String aChildName, String aChildValue);
	
	/**
	 * Gets the child attribute value.
	 * 
	 * @param aChildName the child name
	 * @param aKey the attribute key
	 * 
	 * @return the child attribute value
	 */
	public String getChildAttribute(String aChildName, String aKey);

	/**
	 * Sets the child attribute value.
	 * 
	 * @param aChildName the child name
	 * @param aKey the attribute key
	 * @param aValue the attribute value
	 */
	public void setChildAttribute(String aChildName, String aKey, String aValue);

	/**
	 * Gets the child attribute value as list.
	 * 
	 * @param aChildName the child name
	 * @param aKey the attribute key
	 * 
	 * @return the child attribute value as list
	 */
	public List<IXMLAttributeValueTime> getChildAttributeAsList(String aChildName, String aKey);

	/**
	 * Gets tag value. If it is empty than gets tag value of definition tag.
	 * 
	 * @return the (def) value
	 */
	public String getDefValue();

	/**
	 * Gets tag attribute value. If it is empty than gets tag attribute value of definition tag.
	 * 
	 * @param aKey the attribute key
	 * 
	 * @return the (def) attribute
	 */
	public String getDefAttribute(String aKey);

	/**
	 * Gets child tag by aName. If it is empty than gets child tag by aName of definition tag.
	 * 
	 * @param aName the tag name
	 * 
	 * @return the (def) child tag
	 */
	public IXMLTag getDefChild(String aName);

	/**
	 * Gets child tag value by aChildName. If it is empty than gets child tag value by aChildName of definition tag.
	 * 
	 * @param aChildName the child name
	 * 
	 * @return the (def) child value
	 */
	public String getDefChildValue(String aChildName);

	/**
	 * Gets child tag attribute by aChildName. If it is empty than gets child tag attribute by aChildName of definition tag.
	 * 
	 * @param aChildName the child name
	 * @param aKey the attribute key
	 * 
	 * @return the (def) child attribute
	 */
	public String getDefChildAttribute(String aChildName, String aKey);

	/**
	 * Clones called tag without parent and children.
	 * 
	 * @return the new XML tag
	 */
	public IXMLTag cloneWithoutParentAndChildren();

	/**
	}
	 * Gets the status attribute, the last attribute value.
	 *
	 * @param aKey the a key
	 *
	 * @return the attribute
	 */
	public List<IXMLAttributeValueTime> getStatusAttribute(String aKey);
		
	/**
	}
	 * Gets the current status attribute, the last attribute value.
	 *
	 * @param aKey the a key
	 *
	 * @return the current attribute
	 */
	public String getCurrentStatusAttribute(String aKey);

	/**
	 * Gets the previous status attribute, the value given by aIndex.
	 *
	 * @param aKey the a key
	 * @param aIndex the a index
	 *
	 * @return the previous attribute
	 */
	public String getPreviousStatusAttribute(String aKey, int aIndex);

	/**
	 * Gets the current status attribute time, the last time the value was set.
	 *
	 * @param aKey the a key
	 *
	 * @return the current attribute time
	 */
	public double getCurrentStatusAttributeTime(String aKey);
		
	/**
	 * Gets the previous status attribute time, the time given by aIndex.
	 *
	 * @param aKey the a key
	 * @param aIndex the a index
	 *
	 * @return the previous attribute time
	 */
	public double getPreviousStatusAttributeTime(String aKey, int aIndex);
	
	/**
	 * Gets the status attribute count, the number of aValue values within the status.
	 *
	 * @param aKey the a key
	 * @param aValue the a value
	 *
	 * @return the attribute count
	 */
	public int getStatusAttributeCount(String aKey, String aValue);
	
	/**
	 * Gets the status child.
	 *
	 * @param aName the a name
	 *
	 * @return the status child
	 */
	public IXMLTag getStatusChild(String aName);
	
	/**
	 * Gets the status childs.
	 *
	 * @param aName the a name
	 *
	 * @return the status childs
	 */
	public List<IXMLTag> getStatusChilds(String aName);

	/**
	 * Gets the value of xml status child with name aChildName.
	 * 
	 * @param aChildName the child name
	 * 
	 * @return the status child value
	 */
	public String getStatusChildValue(String aChildName);
	
}
