/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.experiments;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import nl.surf.emergo.business.IRunGroupAccountManager;
import nl.surf.emergo.business.IRunGroupManager;
import nl.surf.emergo.business.IRunTeamManager;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.adm.CAdmRunLoggingHelper;
import nl.surf.emergo.control.adm.CAdmRunLoggingTextbox;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;

/**
 * The Class CAdmRunLoggingTextboxChermug.
 */
public class CAdmRunLoggingTextboxChermug extends CAdmRunLoggingTextbox {
	
	private static final long serialVersionUID = -3547006235938879571L;

	protected CAdmRunLoggingHelper getHelper() {
		return (new CAdmRunLoggingHelperChermug());
	}

	public void onCreate() {
		String lValue = "";

		int lRunId = Integer.parseInt((String)CDesktopComponents.cControl().getAccSessAttr("runId"));
		runTeams = ((IRunTeamManager)CDesktopComponents.sSpring().getBean("runTeamManager")).getAllRunTeamsByRunId(lRunId);
		List<IERunGroup> lRunGroups = ((IRunGroupManager)CDesktopComponents.sSpring().getBean("runGroupManager")).getAllRunGroupsByRunId(lRunId);
		List<Integer> lRugIds = new ArrayList<Integer>();
		for (IERunGroup lRunGroup : lRunGroups) {
			if (!lRugIds.contains(lRunGroup.getRugId()))
				lRugIds.add(lRunGroup.getRugId());
		}
		List<IERunGroupAccount> lItems = ((IRunGroupAccountManager)CDesktopComponents.sSpring().getBean("runGroupAccountManager")).getAllRunGroupAccountsByRugIds(lRugIds);
		List<IEAccount> lAccounts = new ArrayList<IEAccount>();
		runGroupIds = new ArrayList<Integer>();
		List<Hashtable<String,Object>> lStuItems = new ArrayList<Hashtable<String,Object>>(0);
		for (IERunGroupAccount lRunGroupAccount : lItems) {
			IEAccount lAccount = lRunGroupAccount.getEAccount();
			if (!lAccounts.contains(lAccount)) {
				List<IERunGroupAccount> lRunGroupAccounts = getRunGroupAccounts(lAccount.getAccId(),lRunId,lItems);
				if (lRunGroupAccounts.size() > 0) {
					lAccounts.add(lAccount);
					if (!runGroupIds.contains(lRunGroupAccount.getERunGroup().getRugId()))
						runGroupIds.add(lRunGroupAccount.getERunGroup().getRugId());
					Hashtable<String,Object> lStuItem = new Hashtable<String,Object>(0);
					lStuItem.put("item",lAccount);
					lStuItem.put("rungroupaccounts",lRunGroupAccounts);
					lStuItem.put("runteams",getRunTeams(lRunGroupAccounts));
					lStuItems.add(lStuItem);
				}
			}
		}
		CAdmRunLoggingHelper cHelper = getHelper();
		int counter = 0;
		for (Hashtable<String,Object> lStuItem : lStuItems) {
			lValue += cHelper.getValue(lStuItem, (counter == 0));
			counter++;
		}
		this.setValue(lValue);
	}

}
