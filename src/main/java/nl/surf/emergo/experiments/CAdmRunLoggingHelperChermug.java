/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.experiments;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.IXMLAttributeValueTime;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.adm.CAdmRunLoggingHelper;
import nl.surf.emergo.control.script.CScript;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunGroupAccount;

/**
 * The Class CAdmRunLoggingHelperChermug.
 */
public class CAdmRunLoggingHelperChermug extends CAdmRunLoggingHelper {

	/**
	 * Get value.
	 * 
	 * @param aItem the a item
	 * @param aAddHeaders the a add headers
	 * 
	 * @return value
	 */
	public String getValue(Hashtable<String,Object> aItem, boolean aAddHeaders) {
		List<IERunGroupAccount> lRunGroupAccounts = (List)aItem.get("rungroupaccounts");

		CScript cScript = CDesktopComponents.cScript();
		IAccountManager accountManager = (IAccountManager)CDesktopComponents.sSpring().getBean("accountManager");
		
		String lBody = "";
		String lNewLine = "\n";
		String lTab = "\t";
		for (IERunGroupAccount runGroupAccount : lRunGroupAccounts) {
			CDesktopComponents.sSpring().setRunStatus(AppConstants.runStatusRun);
			CDesktopComponents.sSpring().setRunGroupAccount(runGroupAccount);
			String lAccountUserId = runGroupAccount.getEAccount().getUserid();
			String lAccountEmail = runGroupAccount.getEAccount().getEmail();
			String lAccountName = accountManager.getAccountName(runGroupAccount.getEAccount());
			IECaseRole lCaseRole = CDesktopComponents.sSpring().getRunGroupAccount().getERunGroup().getECaseRole();
			IERun lRun = runGroupAccount.getERunGroup().getERun(); 
			if (caseComponents == null)
				caseComponents = CDesktopComponents.sSpring().getCaseComponents(CDesktopComponents.sSpring().getRunGroupAccount().getERunGroup().getERun().getECase());
			if (caseComponents == null || caseComponents.size() == 0)
				lBody += lNewLine + "casecomponenten niet aanwezig";
			else {
				String lHeader = "";
				String lHeader2 = "";
				if (aAddHeaders) {
					lHeader = 
						"casid" + lTab + 
						"runid" + lTab + 
						"userid" + lTab + 
						"email" + lTab + 
						"name" + lTab + 
						"total time" + lTab;
					lHeader2 = lNewLine + 
						"-" + lTab + 
						"-" + lTab + 
						"-" + lTab + 
						"-" + lTab + 
						"-" + lTab + 
						"-" + lTab;
				}
				IECaseComponent lCaseCaseComponent = null;
				IECaseComponent lTasksComponent = null;
				IECaseComponent lStatesComponent = null;
				List<IECaseComponent> lAssessmentsComponents = new ArrayList<IECaseComponent>();
				for (IECaseComponent lCaseComponent : caseComponents) {
					if (lCaseComponent.getEComponent().getCode().equals("case")) {
						lCaseCaseComponent = lCaseComponent;
					}
					else if (lCaseComponent.getEComponent().getCode().equals("tasks")) {
						lTasksComponent = lCaseComponent;
					}
					else if (lCaseComponent.getEComponent().getCode().equals("states")) {
						lStatesComponent = lCaseComponent;
					}
					else if (lCaseComponent.getEComponent().getCode().equals("assessments")) {
						lAssessmentsComponents.add(lCaseComponent);
					}
				}
				double lTotalTime = -1;
				IXMLTag lComponentTag = getDataPlusStatusRootTag("" + lCaseCaseComponent.getCacId()).getChild(AppConstants.componentElement);
				if (lComponentTag != null) {
					IXMLTag lStatusTag = lComponentTag.getChild(AppConstants.statusElement);
					if (lStatusTag != null) {
						lTotalTime = lStatusTag.getCurrentStatusAttributeTime(AppConstants.statusKeyCurrentTime);
					}
				}
				String lRow = lNewLine +
					lRun.getECase().getCasId() + lTab +
					lRun.getRunId() + lTab +
					lAccountUserId + lTab +
					lAccountEmail + lTab +
					lAccountName + lTab +
					lTotalTime + lTab; 
				IXMLTag lRootTag = getDataPlusStatusRootTag("" + lTasksComponent.getCacId());
				List<IXMLTag> lTags = cScript.getNodeTags(lRootTag);
				for (IXMLTag lTag : lTags) {
					if (aAddHeaders) {
						String lId = lTag.getChildValue(lTag.getDefAttribute(AppConstants.defKeyKey));
						if (lId.equals("")) {
							lId = "-";
						}
						lHeader += 
							lId + lTab;
						lHeader2 += 
							"tijdstip afgerond" + lTab;
					}
					lRow += lTag.getPreviousStatusAttributeTime(AppConstants.statusKeyFinished, 0) + lTab; 
				}
				lRootTag = getDataPlusStatusRootTag("" + lStatesComponent.getCacId());
				lTags = cScript.getNodeTags(lRootTag);
				for (IXMLTag lTag : lTags) {
					if (lTag.getName().equals("state")) {
						if (aAddHeaders) {
							String lId = lTag.getChildValue(lTag.getDefAttribute(AppConstants.defKeyKey));
							if (lId.equals("")) {
								lId = "-";
							}
							lHeader += 
								lId + lTab;
							lHeader2 += 
								"waarde" + lTab;
						}
						lRow += lTag.getCurrentStatusAttribute(AppConstants.statusKeyValue) + lTab; 
					}
				}
				for (IECaseComponent lAssessmentComponent : lAssessmentsComponents) {
					if (aAddHeaders) {
						String lId = lAssessmentComponent.getName();
						if (lId.equals("")) {
							lId = "-";
						}
						lHeader += 
							lId + lTab + 
							"-" + lTab + 
							"-" + lTab + 
							"-" + lTab + 
							"-" + lTab + 
							"-" + lTab;
						lHeader2 += 
							"tijdstip afgerond" + lTab + 
							"duur" + lTab + 
							"doorlooptijd" + lTab + 
							"aantal sessies" + lTab + 
							"aantal gewijzigde alternatieven" + lTab +
							"gemiddeld alternatief tijdstip" + lTab;
					}
					lRootTag = getDataPlusStatusRootTag("" + lAssessmentComponent.getCacId());
					lComponentTag = lRootTag.getChild(AppConstants.componentElement);
					lTags = cScript.getNodeTags(lRootTag);
					String lStateValue = 
						"-1" + lTab + 
						"-1" + lTab + 
						"-1" + lTab + 
						"-1" + lTab;
					double lFirstOpenedTime = -1;
					double lFinishedTime = -1;
					List<Double> lOpenedTimes = new ArrayList<Double>();
					double lDuration = -1;
					double lTurnaround = -1;
					int lNumberOfSessions = -1;
					for (IXMLTag lTag : lTags) {
						if (lTag.getName().equals("assessment")) {
							//NOTE only one assessment per assessments component
							IXMLTag lStatusTag = lTag.getChild(AppConstants.statusElement);
							if (lStatusTag != null) {
								if (lTag.getCurrentStatusAttribute(AppConstants.statusKeyValue).equals(AppConstants.statusValueTrue)) {
									lFinishedTime = lTag.getCurrentStatusAttributeTime(AppConstants.statusKeyValue);
								}
							}
						}
					}
					if (lComponentTag != null) {
						IXMLTag lStatusTag = lComponentTag.getChild(AppConstants.statusElement);
						if (lStatusTag != null) {
							List<IXMLAttributeValueTime> lValueTimes = lStatusTag.getAttributeAsList(AppConstants.statusKeyOpened);
							if (lValueTimes != null && lValueTimes.size() > 0) {
								for (IXMLAttributeValueTime lValueTime : lValueTimes) {
									if (lFirstOpenedTime == -1) {
										if (lValueTime.getValue().equals(AppConstants.statusValueTrue) && lValueTime.getTime() != -1) {
											lFirstOpenedTime = lValueTime.getTime();
											if (lFirstOpenedTime != -1 && lFinishedTime != -1) {
												lTurnaround = lFinishedTime - lFirstOpenedTime;
											}
										}
									}
									double lOpenedTime = -1;
									double lClosedTime = -1;
									if (lValueTime.getValue().equals(AppConstants.statusValueTrue)) {
										if (lValueTime.getTime() != -1) {
											if (lValueTime.getTime() < lFinishedTime) {
												lOpenedTime = lValueTime.getTime();
											}
										}
									}
									if (lOpenedTime != -1 && lValueTime.getValue().equals(AppConstants.statusValueFalse)) {
										if (lNumberOfSessions == -1) {
											lNumberOfSessions = 0;
										}
										lNumberOfSessions++;
										lClosedTime = lValueTime.getTime();
										double lTime = 0;
										if (lOpenedTime != -1 && lClosedTime != -1) {
											lOpenedTimes.add(lOpenedTime);
											lTime = lClosedTime - lOpenedTime;
										}
										if (lTime >= 0) {
											if (lDuration == -1) {
												lDuration = 0;
											}
											lDuration += lTime;
										}
										lOpenedTime = -1;
										lClosedTime = -1;
									}
								}
								lStateValue = 
									lFinishedTime + lTab + 
									lDuration + lTab + 
									lTurnaround + lTab + 
									lNumberOfSessions + lTab;
							}
						}
					}
					lRow += lStateValue; 
					int lNumberOfAlternatives = -1;
					double lMeanAlternativeTime = -1;
					for (IXMLTag lTag : lTags) {
						if (lTag.getName().equals("refitem")) {
							IXMLTag lStatusTag = lTag.getChild(AppConstants.statusElement);
							if (lStatusTag != null) {
								List<IXMLAttributeValueTime> lValueTimes = lStatusTag.getAttributeAsList(AppConstants.statusKeyAnswer);
								if (lValueTimes != null && lValueTimes.size() > 0) {
									for (IXMLAttributeValueTime lValueTime : lValueTimes) {
										if (lValueTime.getTime() != -1) {
											if (lNumberOfAlternatives == -1) {
												lNumberOfAlternatives = 0;
											}
											lNumberOfAlternatives++;
											if (lMeanAlternativeTime == -1) {
												lMeanAlternativeTime = 0;
											}
											double lAlternativeTime = lValueTime.getTime();
											double lSessionOpenedTime = -1;
											for (double lOpenedTime : lOpenedTimes) {
												if (lOpenedTime <= lAlternativeTime) {
													lSessionOpenedTime = lOpenedTime;
												}
											}
											if (lSessionOpenedTime != -1) {
												lMeanAlternativeTime += lAlternativeTime - lSessionOpenedTime;
											}
										}
									}
								}
							}
						}
					}
					if (lMeanAlternativeTime != -1) {
						lMeanAlternativeTime = Math.round((100 * lMeanAlternativeTime/(lNumberOfAlternatives * lDuration)));
					}
					lRow += 
						"" + lNumberOfAlternatives + lTab + 
						"" + lMeanAlternativeTime + lTab; 
				}
				lBody += 
					lHeader + 
					lHeader2 + 
					lRow;
			}
		}
		return lBody;
	}

}