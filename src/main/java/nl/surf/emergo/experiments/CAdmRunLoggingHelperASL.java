/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.experiments;

import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.adm.CAdmRunLoggingHelper;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunGroupAccount;

/**
 * The Class CAdmRunLoggingHelperASL.
 */
public class CAdmRunLoggingHelperASL extends CAdmRunLoggingHelper {

	/**
	 * Get value.
	 * 
	 * @param aItem the a item
	 * @param aAddHeaders the a add headers
	 * 
	 * @return value
	 */
	public String getValue(Hashtable<String,Object> aItem, boolean aAddHeaders) {
		List<IERunGroupAccount> lRunGroupAccounts = (List)aItem.get("rungroupaccounts");

		IAccountManager accountManager = (IAccountManager)CDesktopComponents.sSpring().getBean("accountManager");
		
		String lBody = "";
		for (IERunGroupAccount runGroupAccount : lRunGroupAccounts) {
			CDesktopComponents.sSpring().setRunStatus(AppConstants.runStatusRun);
			CDesktopComponents.sSpring().setRunGroupAccount(runGroupAccount);
			boolean lOk = true;
			if (accountManager.hasRole(runGroupAccount.getEAccount(), AppConstants.c_role_adm)) {
				lOk = false;
			}
			else if (accountManager.hasRole(runGroupAccount.getEAccount(), AppConstants.c_role_cde)) {
				lOk = false;
			}
			else if (accountManager.hasRole(runGroupAccount.getEAccount(), AppConstants.c_role_crm)) {
				lOk = false;
			}
			else if (accountManager.hasRole(runGroupAccount.getEAccount(), AppConstants.c_role_tut)) {
				lOk = false;
			}
			if (lOk) {
			String lAccountUserId = runGroupAccount.getEAccount().getUserid();
			String lAccountEmail = runGroupAccount.getEAccount().getEmail();
			String lAccountName = accountManager.getAccountName(runGroupAccount.getEAccount());
			IECaseRole lCaseRole = CDesktopComponents.sSpring().getRunGroupAccount().getERunGroup().getECaseRole();
			IERun lRun = runGroupAccount.getERunGroup().getERun(); 
			if (caseComponents == null)
				caseComponents = CDesktopComponents.sSpring().getCaseComponents(CDesktopComponents.sSpring().getRunGroupAccount().getERunGroup().getERun().getECase());
			if (caseComponents == null || caseComponents.size() == 0)
				lBody += "\ncasecomponenten niet aanwezig";
			else {
				TreeMap<String,String> treeMap = new TreeMap<String,String>();
				counter = 1;
				for (int j=0;j<caseComponents.size();j++) {
					IECaseComponent lCaseComponent = (IECaseComponent)caseComponents.get(j);
					if (lCaseComponent.getEComponent().getCode().equals("case")) {
					IXMLTag lRootTag = getDataPlusStatusRootTag("" + lCaseComponent.getCacId());
					IXMLTag lStatusRootTag = getStatusRootTag("" + lCaseComponent.getCacId());
					if (lRootTag == null)
						lBody += "";
					else {
						List<IXMLTag> lTags = CDesktopComponents.cScript().getNodeTags(lRootTag);
						IXMLTag lComponentTag = lRootTag.getChild(AppConstants.componentElement);
						if (lComponentTag != null) {
							IXMLTag lTag = lComponentTag;
							addStatus(treeMap, lRun, lCaseRole, lCaseComponent, lTag, lStatusRootTag, lAccountUserId, lAccountEmail, lAccountName, "", false);
						}
						if (lTags == null || lTags.size() == 0)
							lBody += "";
						else {
							IXMLTag lTag = lTags.get(lTags.size() - 1);
							if (!lTag.getAttribute(AppConstants.keyRefstatusid).equals("")) {
//								addStatus(treeMap, lRun, lCaseRole, lCaseComponent, lTag, lAccountName);
							}
						}
					}
					}
				}
			    Collection<String> c = treeMap.values();
			    //obtain an Iterator for Collection
			    Iterator<String> itr = c.iterator();
			    //iterate through TreeMap values iterator
			    while(itr.hasNext()) {
					lBody += (String)itr.next();
			    }
			}
			}
		}
		return lBody;
	}

	/**
	 * Add status.
	 * 
	 * @param aTreeMap the a tree map
	 * @param aRun the a run
	 * @param aCaseComponent the a case component
	 * @param aTag the a tag
	 * @param aAccountName the a account name
	 */
	protected void addStatus(TreeMap<String,String> aTreeMap, IERun aRun, IECaseRole aCaseRole, IECaseComponent aCaseComponent, IXMLTag aTag, String aAccountName) {
		String lCaseComponentName = aCaseComponent.getName();
		String lTagName = aTag.getName();
		String lTagId = "";
		if (!aTag.getDefAttribute(AppConstants.defKeyKey).equals("")) {
			lTagId = aTag.getChildValue(aTag.getDefAttribute(AppConstants.defKeyKey));
		}
		if (lTagName.equals(AppConstants.componentElement))
			lTagId = lTagName;
		IXMLTag lStatusTag = aTag.getChild(AppConstants.statusElement);
		if (lStatusTag != null) {
			String lStarttime = "";
			String lCurrenttime = "";
			String lMapkey = "";
			for (Enumeration<String> keys = lStatusTag.getAttributes().keys(); keys.hasMoreElements();) {
				String key = (String) keys.nextElement();
				if (key.equals("starttime")) {
					lStarttime = (String) lStatusTag.getAttribute(key);
					String[] lValues = lStarttime.split(","); 
					if (lValues.length > 1) {
						lStarttime = lValues[0];
					}
				}
				else if (key.equals("currenttime")) {
					lCurrenttime = (String) lStatusTag.getAttribute(key);
					String[] lValues = lCurrenttime.split(","); 
					if (lValues.length > 1) {
						lCurrenttime = lValues[0];
						lMapkey = lValues[1];
						while (lMapkey.length() < 10) {
							lMapkey = "0" + lMapkey;
						}
					}
				} 
				// get relevant data
/*				String value = (String) lStatusTag.getAttributes().get(key);
				if (!value.equals("")) {
					String[] lValues = value.split(","); 
					if (lValues.length > 1) {
						// time code included
						for (int j = 0;j < (lValues.length - 1);j++) {
							String time = lValues[j+1];
							String mapkey = time;
							while (mapkey.length() < 10) {
								mapkey = "0" + mapkey;
							}
							// determine if system or user event or possibly both
							String[] lEventData = getEventData(aCaseRole, aCaseComponent, aCaseComponent.getEComponent().getCode(), aTag, lTagName, key, lValues[j]);
							aTreeMap.put(mapkey + counter,
									"\n" +
									aRun.getECase().getCasId() + "\t" +
									aRun.getRunId() + "\t" +
									aAccountName + "\t" +
									time + "\t" + 
									lCaseComponentName + "\t" + 
									lTagName + "\t" + 
									lTagId + "\t" + 
									key + "\t" + 
									lValues[j] + "\t" +
									lEventData[0] + "\t" +
									lEventData[1] + "\t" +
									lEventData[2]);
							counter++;
							j++;
						}
					}
				} */
			}
			if (!lStarttime.equals("") && !lCurrenttime.equals("")) {
				double lCaseTime = 0;
				lCaseTime += Double.parseDouble(lCurrenttime);
				lCaseTime -= Double.parseDouble(lStarttime);
				lCaseTime = Math.round(lCaseTime/(100*3600));
				lCaseTime = lCaseTime/10;
				aTreeMap.put(lMapkey + counter,
						"\n" +
						aRun.getECase().getCasId() + "\t" +
						aRun.getRunId() + "\t" +
						aAccountName + "\t" +
						lCaseTime + "\t" + 
						lCaseComponentName + "\t" + 
						lTagName + "\t" + 
						lTagId + "\t" + 
						"\t" + 
						"\t" +
						"\t" +
						"\t");
				counter++;
			}
		}
	}

}