/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.view;

import static java.util.Arrays.asList;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.MouseEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Html;
import org.zkoss.zul.Image;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import org.zkoss.zul.impl.LabelElement;
import org.zkoss.zul.impl.XulElement;

import nl.surf.emergo.business.IXMLTree;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.business.impl.XMLTree;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.utilities.FtpHelper;
import nl.surf.emergo.utilities.PropsValues;

/**
 * The Class VView. Contains public constants for all zk zul files and methods
 * used within the view layer, that is the zk user interface.
 */
public class VView {
	private static final Logger _log = LogManager.getLogger(VView.class);

	public static final String v_login = "login.zul";
	public static final String v_emergo = "emergo.zul";
	public static final String v_Forbidden = "forbidden.zul";
	public static final String v_login_s_account = "login_s_account.zul";
	public static final String v_s_no_valid_account = "s_no_valid_account.zul";
	public static final String v_s_password_pwned = "s_password_pwned.zul";
	public static final String v_s_account_locked = "s_account_locked.zul";
	public static final String v_account_roles = "account_roles.zul";
	public Hashtable<String, String> v_account_role = new Hashtable<>(0);
	public static final String v_delete_item = "delete_item.zul";
	public static final String v_drop_item_as_sibling_or_child = "drop_item.zul";
	public static final String v_messagebox = "messagebox.zul";
	public static final String v_adm = "adm.zul";
	public static final String v_adm_configure = "adm_configure.zul";
	public static final String v_adm_filter_accounts = "adm_filter_accounts.zul";
	public static final String v_adm_accounts = "adm_accounts.zul";
	public static final String v_adm_s_account = "adm_s_account.zul";
	public static final String v_adm_cases = "adm_cases.zul";
	public static final String v_adm_s_case = "adm_s_case.zul";
	public static final String v_adm_components = "adm_components.zul";
	public static final String v_adm_s_component = "adm_s_component.zul";
	public static final String v_adm_s_components_export = "adm_s_components_export.zul";
	public static final String v_adm_s_components_import = "adm_s_components_import.zul";
	public static final String v_adm_filter_runs = "adm_filter_runs.zul";
	public static final String v_adm_runs = "adm_runs.zul";
	public static final String v_adm_s_run = "adm_s_run.zul";
	public static final String v_adm_rungroupaccounts = "adm_rungroupaccounts.zul";
	public static final String v_adm_runlogging = "adm_runlogging.zul";
	public static final String v_adm_s_rungroupaccountstatus = "adm_s_rungroupaccountstatus.zul";
	public static final String v_adm_contexts = "adm_contexts.zul";
	public static final String v_adm_s_context = "adm_s_context.zul";
	public static final String v_adm_filter_contextaccounts = "adm_filter_contextaccounts.zul";
	public static final String v_adm_contextaccounts = "adm_contextaccounts.zul";
	public static final String v_adm_maintenance = "adm_maintenance.zul";
	public static final String v_adm_query = "adm_query.zul";
	public Hashtable<String, String> v_adm_tasks = new Hashtable<>(0);
	public static final String v_cde_cases = "cde_cases.zul";
	public static final String v_cde_s_case = "cde_s_case.zul";
	public static final String v_cde_s_case_export = "cde_s_case_export.zul";
	public static final String v_cde_s_case_import = "cde_s_case_import.zul";
	public static final String v_cde_s_casecomponent_export = "cde_s_casecomponent_export.zul";
	public static final String v_cde_s_casecomponents_export = "cde_s_casecomponents_export.zul";
	public static final String v_cde_s_casecomponents_content_export = "cde_s_casecomponents_content_export.zul";
	public static final String v_cde_s_casecomponent_import = "cde_s_casecomponent_import.zul";
	public static final String v_cde_s_casecomponents_import = "cde_s_casecomponents_import.zul";
	public static final String v_cde_s_casecomponents_content_import = "cde_s_casecomponents_content_import.zul";
	public static final String v_cde_casecomponents = "cde_casecomponents.zul";
	public static final String v_cde_s_casecomponent = "cde_s_casecomponent.zul";
	public static final String v_cde_caseroles = "cde_caseroles.zul";
	public static final String v_cde_s_caserole = "cde_s_caserole.zul";
	public static final String v_cde_components = "cde_components.zul";
	public static final String v_cde_s_component = "cde_s_component.zul";
	public static final String v_cde_s_preview_richtext = "cde_s_preview_richtext.zul";
	public static final String v_cde_s_script_method = "cde_s_script_method.zul";
	public static final String v_cde_s_script = "cde_s_script.zul";
	public static final String v_cde_s_choose_file = "cde_s_choose_file.zul";
	public static final String v_cde_gmaps_fr = "cde_gmaps_fr.zul";
	public static final String v_cde_s_rungroupaccountstatus = "cde_s_rungroupaccountstatus.zul";
	public static final String v_cde_gamebricsauthor = "cde_gamebricsauthor.zul";
	public static final String v_preview_item = "preview_item.zul";
	public static final String v_s_preview_item = "s_preview_item.zul";
	public static final String v_s_preview_item_runteams = "s_preview_item_runteams.zul";
	public static final String v_s_preview_item_runteamrungroups = "s_preview_item_runteamrungroups.zul";
	public static final String v_crm_runs = "crm_runs.zul";
	public static final String v_crm_s_run = "crm_s_run.zul";
	public static final String v_crm_filter_rungroupaccounts = "crm_filter_rungroupaccounts.zul";
	public static final String v_crm_rungroupaccounts = "crm_rungroupaccounts.zul";
	public static final String v_crm_runteams = "crm_runteams.zul";
	public static final String v_crm_s_runteam = "crm_s_runteam.zul";
	public static final String v_crm_runteamrungroups = "crm_runteamrungroups.zul";
	public static final String v_crm_s_runaccountinitstatus = "crm_s_runaccountinitstatus.zul";
	public static final String v_crm_s_runaccountstatus = "crm_s_runaccountstatus.zul";
	public static final String v_crm_runinitialstatus = "crm_runinitialstatus.zul";
	public static final String v_crm_s_runinitialstatus = "crm_s_runinitialstatus.zul";
	public static final String v_tut_runs = "tut_runs.zul";
	public static final String v_tut_rungroupaccounts = "tut_rungroupaccounts.zul";
	public static final String v_tut_rungroupaccountemails = "tut_rungroupaccountemails.zul";
	public static final String v_tut_rungroupaccountemailtext = "tut_rungroupaccountemailtext.zul";
	public static final String v_tut_rungroupaccounttasks = "tut_rungroupaccounttasks.zul";
	public static final String v_tut_rungroupaccountinterventions = "tut_rungroupaccountinterventions.zul";
	public static final String v_tut_rungroupaccountownoverview = "tut_rungroupaccountownoverview.zul";
	public static final String v_stu_runs = "stu_runs.zul";
	public static final String v_run = "run.zul";
	public static final String v_rununity = "rununity.zul";
	public static final String v_run_mail = "run_mail.zul";
	public static final String v_run_new_mail = "run_new_mail.zul";
	public static final String v_run_alert = "run_alert.zul";
	public static final String v_run_tasks = "run_tasks.zul";
	public static final String v_run_note = "run_note.zul";
	public static final String v_run_logbook_overview = "run_logbook_overview.zul";
	public static final String v_run_assessment_end = "run_assessment_end.zul";
	public static final String v_run_assessment_start = "run_assessment_start.zul";
	public static final String v_run_location_actions = "run_location_actions.zul";
	public static final String v_run_locations = "run_locations.zul";
	public static final String v_run_edit_component = "run_edit_component.zul";
	public static final String v_run_s_choose_file = "run_s_choose_file.zul";
	public static final String v_run_empty_fr = "run_empty_fr.zul";
	public static final String v_run_text_fr = "run_text_fr.zul";
	public static final String v_run_description_fr = "run_description_fr.zul";
	public static final String v_run_windowsmedia_with_control_fr = "run_windowsmedia_with_control_fr.zul";
	public static final String v_run_windowsmedia_fr = "run_windowsmedia_fr.zul";
	public static final String v_run_windowsmedia_mpg_fr = "run_windowsmedia_mpg_fr.zul";
	public static final String v_run_flash_fr = "run_flash_fr.zul";
	public static final String v_run_flash_swf_fr = "run_flash_swf_fr.zul";
	public static final String v_run_flash_ref_fr = "run_flash_references_fr.zul";
	public static final String v_run_unity_fr = "run_unity_fr.zul";
	public static final String v_run_website_fr = "run_website_fr.zul";
	public static final String v_run_gmaps_fr = "run_gmaps_fr.zul";
	public static final String v_run_parallax_fr = "run_parallax_fr.zul";
	public static final String v_run_flash_directing_fr = "run_flash_directing_fr.zul";
	public static final String v_run_references_report_file = "run_references.jasper";
	public static final String v_run_memos_fr = "run_memos_fr.zul";
	public static final String v_run_flash_memos_fr = "run_flash_memos_fr.zul";
	public static final String v_run_tutorial_fr = "run_tutorial_fr.zul";
	public static final String v_run_tutorialinstruction_fr = "run_tutorialinstruction_fr.zul";
	public static final String v_run_tutorialreport_fr = "run_tutorialreport_fr.zul";
	public static final String v_run_profile_fr = "run_profile_fr.zul";
	public static final String v_run_logbook_fr = "run_logbook_fr.zul";
	public static final String v_run_logbook_report_file = "run_logbook.jasper";
	public static final String v_run_videomanual_fr = "run_videomanual_fr.zul";
	public static final String v_run_flash_videomanual_fr = "run_flash_videomanual_fr.zul";
	public static final String v_run_flash_panel_fr = "run_flash_panel_fr.zul";
	public static final String v_run_assessments_fr = "run_assessments_fr.zul";
	public static final String v_run_scores_fr = "run_scores_fr.zul";
	public static final String v_run_notifications_fr = "run_notifications_fr.zul";
	public static final String v_run_jasperreport = "run_jasperreport.zul";
	public static final String v_run_webcam_fr = "run_webcam_fr.zul";
	public static final String v_run_conversation_intervention_fr = "run_conversation_intervention_fr.zul";
	public static final String v_run_conversation_editfield_fr = "run_conversation_editfield_fr.zul";
	public static final String v_run_ispot_fr = "run_ispot_fr.zul";
	public static final String v_run_editforms_fr = "run_editforms_fr.zul";
	public static final String v_run_dragdropforms_fr = "run_dragdropforms_fr.zul";
	public static final String v_run_selectionforms_fr = "run_selectionforms_fr.zul";
	public static final String v_run_videosceneselector_fr = "run_videosceneselector_fr.zul";
	public static final String v_run_tests_fr = "run_tests_fr.zul";
	public static final String v_run_graphicalforms_fr = "run_graphicalforms_fr.zul";
	public static final String v_run_dashboard_fr = "run_dashboard_fr.zul";
	public static final String v_default_plugin_file = "plugin.zul";
	public static final String parameterPluginPath = "plugin_path";
	public static final String initTooltip = ", position=after_pointer, delay=500";
	public static final String tooltipId = "toolTip" + initTooltip;

	/**
	 * The Constant colWidth1: width in pixels of first column in pop-up dialogues
	 * used during authoring.
	 */
	public static final int colWidth1 = 250;

	/**
	 * The Constant colWidth2: width in pixels of second column in pop-up dialogues
	 * used during authoring.
	 */
	public static final int colWidth2 = 800;

	/**
	 * The Constant childtagLabelKeyPrefix: prefix of label keys used for child
	 * tags.
	 */
	public static final String childtagLabelKeyPrefix = "childtag.";

	/**
	 * The Constant componentLabelKeyPrefix: prefix of label keys used for
	 * components.
	 */
	public static final String componentLabelKeyPrefix = "component.";

	/**
	 * The Constant nodetagLabelKeyPrefix: prefix of label keys used for node tags.
	 */
	public static final String nodetagLabelKeyPrefix = "nodetag.";

	/**
	 * The Constant statuskeyLabelKeyPrefix: prefix of label keys used for status
	 * keys.
	 */
	public static final String statuskeyLabelKeyPrefix = "statuskey.";

	/**
	 * The Constant helpHoverWnd. This is the id of help hover window of the
	 * platform.
	 */
	public static final String helpHoverWndId = "helpHoverWnd";

	/** The Constant helpWnd. This is the id of help window of the platform. */
	public static final String helpWndId = "helpWnd";

	/** The Constant v_eme_root_url. */
	private String v_eme_root_url;

	/** The application context. */
	protected static ServletContext servletContext = null;

	/** The xml tree object. */
	protected static IXMLTree pXmlTree = new XMLTree();

	/** The html5 streaming urls. */
	protected Map<String, String> pHtml5StreamingUrls = new HashMap<>();

	/**
	 * The user specific labels, may overrule labels given by Labels.getLabel within
	 * method getOriginalLabel or add labels.
	 */
	protected Map<String, String> userSpecificLabels = new HashMap<>();

	public Map<String, String> getUserSpecificLabels() {
		return userSpecificLabels;
	}

	public void setUserSpecificLabels(Map<String, String> userSpecificLabels) {
		this.userSpecificLabels = userSpecificLabels;
	}

	/**
	 * Instantiates a new v view.
	 */
	public VView() {
		// fill with start pages for all roles
		v_account_role.put("adm", v_adm);
		v_account_role.put("cde", v_cde_cases);
		v_account_role.put("crm", v_crm_runs);
		v_account_role.put("tut", v_tut_runs);
		v_account_role.put("stu", v_stu_runs);
		// fill with start pages for role adm
		v_adm_tasks.put("configure", v_adm_configure);
		v_adm_tasks.put("accounts", v_adm_filter_accounts);
		v_adm_tasks.put("cases", v_adm_cases);
		v_adm_tasks.put("components", v_adm_components);
		v_adm_tasks.put("runs", v_adm_filter_runs);
		v_adm_tasks.put("contexts", v_adm_contexts);
		v_adm_tasks.put("maintenance", v_adm_maintenance);
		v_adm_tasks.put("query", v_adm_query);
	}

	public static IXMLTree getXmlTree() {
		return pXmlTree;
	}

	/**
	 * Sets the pointer to the servlet context. Needed to determine servlet path,
	 * when ZK environment is not started.
	 *
	 * @param aServletContext the servlet context
	 *
	 */
	public static final void setServletContext(ServletContext aServletContext) {
		servletContext = aServletContext;
	}

	/**
	 * Forbidden. Called when someone tries to open a page but has not yet a Emergo
	 * account.
	 */
	public void forbidden() {
		redirectToView(getEmergoRootUrl() + getEmergoWebappsRoot() + "/" + v_Forbidden);
	}

	/**
	 * Forbidden for role. Called when a Emergo account opens a page which he is not
	 * meant for his role(s).
	 */
	public void forbiddenForRole() {
		redirectToView(v_login);
	}

	/**
	 * Shuffle string.
	 *
	 * @param aString the a string
	 *
	 * @return the shuffled string
	 */
	public String shuffleString(String aString) {
		List<Character> lCharacters = new ArrayList<Character>();
		for (char lChar : aString.toCharArray()) {
			lCharacters.add(lChar);
		}
		StringBuilder lBuilder = new StringBuilder(aString.length());
		while (lCharacters.size() != 0) {
			int lRandPicker = (int) (Math.random() * lCharacters.size());
			lBuilder.append(lCharacters.remove(lRandPicker));
		}
		return lBuilder.toString();
	}

	/**
	 * Get unique id.
	 *
	 * @return the string
	 */
	public String getUniqueId() {
		return shuffleString(((HttpSession) getSession().getNativeSession()).getId() + System.currentTimeMillis());
	}

	/**
	 * Get unique desktop id.
	 *
	 * @return the string
	 */
	public String getUniqueDesktopId() {
		if (Executions.getCurrent() == null) {
			return "";
		}
		String lUniqueDesktopId = (String) Executions.getCurrent().getDesktop().getAttribute("uniqueDesktopId");
		if (lUniqueDesktopId == null) {
			// create unique string
			if (getSession() == null) {
				lUniqueDesktopId = "";
			} else {
				lUniqueDesktopId = getUniqueId();
			}
			Executions.getCurrent().getDesktop().setAttribute("uniqueDesktopId", lUniqueDesktopId);
		}
		return lUniqueDesktopId;
	}

	/**
	 * Get unique temp sub path.
	 *
	 * @return the string
	 */
	public String getUniqueTempSubPath() {
		String lSubPath = getUniqueDesktopId();
		if (lSubPath.equals("")) {
			return "";
		}
		return lSubPath + "/";
	}

	/**
	 * Get unique view. That is a zk page url plus a unique identifier to make the
	 * url unique. To prevent cashing by browser or institution.
	 *
	 * @param aView the a view
	 *
	 * @return the string
	 */
	public String uniqueView(String aView) {
		if ((aView.indexOf("unique_par=") >= 0))
			return aView;
		String lUniquePar = "unique_par=" + getUniqueDesktopId();
		if (aView.indexOf("?") < 0)
			return aView + "?" + lUniquePar;
		else
			return aView + "&" + lUniquePar;
	}

	/**
	 * Redirects to view.
	 *
	 * @param aView the a view
	 */
	public void redirectToView(String aView) {
		sendRedirect(uniqueView(aView), null);
	}

	/**
	 * Redirects to view absolute. Makes aView absolute.
	 *
	 * @param aView the a view
	 */
	public void redirectToViewAbsolute(String aView) {
		redirectToView(getAbsoluteUrl(aView));
	}

	/**
	 * Redirects to view.
	 *
	 * @param aView   the a view
	 * @param aTarget the name of the browser window that send-redirect will load
	 *                the specified URI, or null if the current browser window is
	 *                used.
	 */
	public void redirectToView(String aView, String aTarget) {
		sendRedirect(uniqueView(aView), aTarget);
	}

	/**
	 * Reloads view.
	 */
	public void reloadView() {
		redirectToView(getCurrentUrl());
	}

	/**
	 * Redirects to a not unique view.
	 *
	 * @param aView   the a view
	 * @param aTarget the name of the browser window that send-redirect will load
	 *                the specified URI, or null if the current browser window is
	 *                used.
	 */
	public void redirectToViewNotUnique(String aView, String aTarget) {
		sendRedirect(aView, aTarget);
	}

	/**
	 * Gets include that runs Emergo.
	 *
	 * @return the include Object, null if not existing.
	 */
	public Object getIncludeThatRunsEmergo() {
		Object lObject = getComponent("includeDesktop");
		if (lObject instanceof Include)
			return lObject;
		return null;
	}

	/**
	 * Gets include parent.
	 *
	 * @param aComponent the a component
	 *
	 * @return the include Object, null if not existing.
	 */
	public static final Object getIncludeParent(Component aComponent) {
		if (aComponent == null) {
			return null;
		}
		Component lParent = aComponent.getParent();
		while (lParent != null && !(lParent instanceof Include)) {
			lParent = lParent.getParent();
		}
		if (lParent != null && !lParent.getId().equals("includeDesktop")) {
			return lParent;
		}
		return null;
	}

	/**
	 * Redirects to view.
	 *
	 * @param aView   the a view
	 * @param aTarget the name of the browser window that send-redirect will load
	 *                the specified URI, or null if the current browser window is
	 *                used.
	 */
	public void sendRedirect(String aView, String aTarget) {
		if (Executions.getCurrent() == null)
			return;
		Object lObject = getIncludeThatRunsEmergo();
		if (lObject != null) {
			((Include) lObject).setSrc("");
			((Include) lObject).setSrc(aView);
		} else {
			if (aTarget == null)
				Executions.getCurrent().sendRedirect(aView);
			else
				Executions.getCurrent().sendRedirect(aView, aTarget);
		}
	}

	/**
	 * Clear session attributes.
	 *
	 */
	public void clearSession() {
		CDesktopComponents.cControl().clearSession();
	}

	/**
	 * Shows popup window.
	 *
	 * @param aView     the a view
	 * @param aParent   the a parent
	 * @param aParams   the a params
	 * @param aPosition the a position
	 *
	 * @return the window
	 */
	public Window popup(String aView, Component aParent, Map<String, Object> aParams, String aPosition) {
		Window lWindow = createWindow(aView, aParent, aParams);
		if (lWindow != null) {
			lWindow.doOverlapped();
			if (!aPosition.equals(""))
				lWindow.setPosition(aPosition);
		}
		return lWindow;
	}

	/**
	 * Shows quasi modal popup.
	 *
	 * @param aView                   the a view
	 * @param aParentOfTransparentDiv the a parent of the transparent div to show
	 * @param aParams                 the a params
	 * @param aPosition               the a position
	 *
	 * @return the window
	 */
	public Window quasiModalPopup(String aView, Component aParentOfTransparentDiv, Map<String, Object> aParams,
			String aPosition) {
		// NOTE show transparent div below window
		Div lTransparentDiv = initQuasiModalBehavior(aParentOfTransparentDiv);
		// NOTE window gets no parent
		Window lWindow = createWindow(aView, null, aParams);
		if (lWindow != null) {
			// NOTE set focus to window so possible input controls within other windows are
			// de-focused. So they can not be used anymore using the Enter key.
			lWindow.setFocus(true);
			if (lTransparentDiv != null) {
				lWindow.setAttribute("componentToDetach", lTransparentDiv);
			}
			lWindow.doOverlapped();
			if (!aPosition.equals(""))
				lWindow.setPosition(aPosition);
		}
		return lWindow;
	}

	public int showMessagebox(Component root, String message, String title, int buttons, String icon) {
		// NOTE Messagebox is doing the job so no need to simulate quasi-modal behavior.
		return Messagebox.show(message, title, buttons, icon);
	}

	/**
	 * Inits quasi modal behavior.
	 *
	 * @param aParentOfTransparentDiv the a parent of the transparent div to show
	 *
	 * @return the semi-transparent div
	 */
	public Div initQuasiModalBehavior(Component aParentOfTransparentDiv) {
		if (aParentOfTransparentDiv == null) {
			aParentOfTransparentDiv = Executions.getCurrent().getDesktop().getFirstPage().getLastRoot();
		}
		if (aParentOfTransparentDiv == null) {
			return null;
		}
		// NOTE show semi-transparent div below window
		Div lTransparentDiv = showSemiTransparentDiv(aParentOfTransparentDiv);
		// NOTE first set tabbing false and then create window. Otherwise window
		// controls would also have tabbing disabled.
		disableTabbing(true);
		// NOTE if parent window is overlapped it is set to embedded, but its position
		// is centered.
		// If two windows are overlapped clicking on the one in the background results
		// in placing it in the foreground, which is unwanted behavior.
		doQuasiOverlapped(aParentOfTransparentDiv);
		return lTransparentDiv;
	}

	/**
	 * Undo quasi modal behavior.
	 *
	 * @param aTransparentDiv the a transparent div
	 */
	public void undoQuasiModalBehavior(Component aTransparentDiv) {
		if (aTransparentDiv == null) {
			return;
		}
		aTransparentDiv.detach();
		// NOTE a quasi model window also disables using the Tab key for all components
		// except the ones within the quasi model window. Using the Tab key be enabled
		// again.
		disableTabbing(false);
		if (aTransparentDiv.getRoot() != null) {
			undoQuasiOverlapped(aTransparentDiv.getRoot());
		}
	}

	/**
	 * Disables using Tab key for all components that allow tabindex to be set.
	 */
	public void disableTabbing(boolean aDisable) {
		if (Executions.getCurrent() != null) {
			Collection<Component> lComponents = Executions.getCurrent().getDesktop().getComponents();
			if (lComponents != null) {
				Integer lNoTabbing = -1;
				for (Component lComponent : lComponents) {
					// NOTE Only for HtmlBasedComponent tabindex may be set. However, for Checkbox a
					// DOM error occurs, so it is excluded.
					if (lComponent instanceof HtmlBasedComponent && !(lComponent instanceof Checkbox)) {
						HtmlBasedComponent lHtmlBasedComponent = (HtmlBasedComponent) lComponent;
						// NOTE use a list to store tab indexes because setTabbing can be called for
						// multiple quasi modal popups on top of each other.
						List<Integer> lTabindices = (List<Integer>) lHtmlBasedComponent.getAttribute("tabindexes");
						if (aDisable) {
							if (lTabindices == null) {
								lTabindices = new ArrayList<Integer>();
								lHtmlBasedComponent.setAttribute("tabindexes", lTabindices);
							}
							lTabindices.add(lHtmlBasedComponent.getTabindexInteger());
							lHtmlBasedComponent.setTabindex(lNoTabbing);
						} else {
							if (lTabindices != null && lTabindices.size() > 0) {
								lHtmlBasedComponent.setTabindex(lTabindices.get(lTabindices.size() - 1));
								lTabindices.remove(lTabindices.size() - 1);
								if (lTabindices.isEmpty()) {
									lHtmlBasedComponent.setAttribute("tabindexes", null);
								}
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Shows semi-transparent div that covers parent if aParent unequal to null and
	 * div is allowed as child of aParent.
	 *
	 * @param aParent the a parent
	 *
	 * @return the div
	 */
	public Div showSemiTransparentDiv(Component aParent) {
		Div lDiv = null;
		try {
			lDiv = new Div();
			lDiv.setStyle("position:absolute;left:0px;top:0px;background-color:#e0e1e3;opacity:.6;z-index:2;");
			aParent.appendChild(lDiv);
			Clients.evalJavaScript(
					"setSizeToSizeOfOtherComponent('" + lDiv.getUuid() + "', '" + aParent.getUuid() + "');");
		} catch (Exception e) {
			lDiv = null;
		}
		return lDiv;
	}

	/**
	 * If component is window and it is overlapped it is set to embedded, but its
	 * position is centered. If two windows are overlapped clicking on the one in
	 * the background results in placing it in the foreground, which is unwanted
	 * behavior.
	 *
	 * @param aComponent the a component
	 */
	public void doQuasiOverlapped(Component aComponent) {
		if (aComponent instanceof Window) {
			Window lWindow = (Window) aComponent;
			if (lWindow.inOverlapped()) {
				lWindow.setAttribute("isOriginallyOverlapped", true);
				lWindow.doEmbedded();
				Clients.evalJavaScript("setWndOverlapped('" + lWindow.getUuid() + "', false);");
			}
		}
	}

	/**
	 * Undo quasi overlapped status of component if it is a window.
	 *
	 * @param aComponent the a component
	 */
	public void undoQuasiOverlapped(Component aComponent) {
		if (aComponent instanceof Window) {
			Window lWindow = (Window) aComponent.getRoot();
			Boolean lIsOriginallyOverlapped = (Boolean) lWindow.getAttribute("isOriginallyOverlapped");
			if (lWindow.inEmbedded() && lIsOriginallyOverlapped != null && lIsOriginallyOverlapped.booleanValue()) {
				lWindow.setAttribute("isOverlapped", null);
				lWindow.doOverlapped();
			}
		}
	}

	/**
	 * Shows modal popup window.
	 *
	 * @param aView     the a view
	 * @param aParent   the a parent
	 * @param aParams   the a params
	 * @param aPosition the a position
	 *
	 * @return the window
	 */
	public Window modalPopup(String aView, Component aParent, Map<String, Object> aParams, String aPosition) {
		Window lWindow = createWindow(aView, aParent, aParams);
		if (lWindow != null) {
			if (!aPosition.equals("")) {
				lWindow.doOverlapped();
				lWindow.setPosition(aPosition);
			}
			lWindow.doModal();
		}
		return lWindow;
	}

	/**
	 * Shows modal popup window without waiting.
	 *
	 * @param aView     the a view
	 * @param aParent   the a parent
	 * @param aParams   the a params
	 * @param aPosition the a position
	 */
	public void modalPopupWithoutWaiting(String aView, Component aParent, Map<String, Object> aParams,
			String aPosition) {
		Window lWindow = createWindow(aView, aParent, aParams);
		if (lWindow != null) {
			if (!aPosition.equals("")) {
				lWindow.doOverlapped();
				lWindow.setPosition(aPosition);
			}
			lWindow.setMode("modal");
		}
	}

	/**
	 * Creates window.
	 *
	 * @param aView     the a view
	 * @param aParent   the a parent
	 * @param aParams   the a params
	 * @param aPosition the a position
	 *
	 * @return the window
	 */
	private Window createWindow(String aView, Component aParent, Map<String, Object> aParams) {
		if (Executions.getCurrent() == null)
			return null;
		Window lWindow = null;
		Component lComponent = Executions.createComponents(aView, aParent, aParams);
		while (lComponent != null) {
			if (!(lComponent instanceof Window)) {
				lComponent = lComponent.getNextSibling();
			} else {
				lWindow = (Window) lComponent;
				lComponent = null;
			}
		}
		return lWindow;
	}

	/**
	 * Gets the value of a label key in the .properties file and replace references
	 * to other label keys by their values. Label values may contain references to
	 * other labels, for instance '[#another-label-key#]' and
	 * '[C#another-label-key#C]', where C means the first character of the label
	 * value is capitalized. Session-independent.
	 *
	 * @param aKey the a key
	 *
	 * @return the label
	 */
	public static String getStaticLabel(String aKey) {
		return insertStaticLabelsWithinString(Labels.getLabel(aKey, ""));
	}

	/**
	 * Inserts labels within a string by given by their key.
	 *
	 * @param aString          the string
	 * @param aLabelVarPrefix  the label var prefix string
	 * @param aLabelVarPostfix the label var postfix string
	 *
	 * @return the string
	 */
	private static String insertStaticLabelsWithinString(String aString, String aLabelVarPrefix, String aLabelVarPostfix) {
		// find first label key
		int lStart = aString.indexOf(aLabelVarPrefix);
		int lEnd = aString.indexOf(aLabelVarPostfix);
		if (!(lStart >= 0 && lEnd > lStart)) {
			// no label keys within string
			return aString;
		}

		while (lStart >= 0 && lEnd > lStart) {
			// enable label pointers within label pointers
			lStart = aString.substring(0, lEnd).lastIndexOf(aLabelVarPrefix);
			String lLabelKey = aString.substring(lStart + aLabelVarPrefix.length(), lEnd);
			// get label value
			String lLabelValue = "";
			if (aLabelVarPrefix.equals(AppConstants.labelVarPrefix)) {
				lLabelValue = getStaticLabel(lLabelKey);
			} else if (aLabelVarPrefix.equals(AppConstants.labelVarPrefixC)) {
				lLabelValue = getStaticCLabel(lLabelKey);
			}
			if (StringUtils.isEmpty(lLabelValue)) {
				lLabelValue = "---LABEL OR SESSION VAR WITH KEY  '" + lLabelKey + "' NOT FOUND!---";
			}
			aString = aString.substring(0, lStart) + lLabelValue + aString.substring(lEnd + aLabelVarPostfix.length());
			lStart = aString.indexOf(aLabelVarPrefix);
			lEnd = aString.indexOf(aLabelVarPostfix);
		}
		return aString;
	}

	/**
	 * Inserts labels within a string by given by their key.
	 *
	 * @param aString the string
	 *
	 * @return the string
	 */
	private static String insertStaticLabelsWithinString(String aString) {
		aString = insertStaticLabelsWithinString(aString, AppConstants.labelVarPrefix, AppConstants.labelVarPostfix);
		aString = insertStaticLabelsWithinString(aString, AppConstants.labelVarPrefixC, AppConstants.labelVarPostfixC);
		return aString;
	}

	/**
	 * Gets the value of a labelkey in the .properties file and capitalizes first
	 * character.
	 *
	 * @param aKey the a key
	 *
	 * @return the label
	 */
	private static String getStaticCLabel(String aKey) {
		return getCapitalizeFirstChar(getStaticLabel(aKey));
	}

	/**
	 * Gets the value of a label key in the .properties file or an overruled or added
	 * value within userSpecificLabels.
	 *
	 * @param aKey the a key
	 *
	 * @return the label
	 */
	public String getOriginalLabel(String aKey) {
		if (getUserSpecificLabels().containsKey(aKey)) {
			return getUserSpecificLabels().get(aKey);
		}
		return Labels.getLabel(aKey, "");
	}

	/**
	 * Gets the value of a label key in the .properties file and replace references
	 * to other label keys by their values. Label values may contain references to
	 * other labels, for instance '[#another-label-key#]' and
	 * '[C#another-label-key#C]', where C means the first character of the label
	 * value is capitalized. Also replace references to session variables.
	 *
	 * @param aKey the a key
	 *
	 * @return the label
	 */
	public String getLabel(String aKey) {
		return insertLabelsWithinString(getOriginalLabel(aKey));
	}

	/**
	 * Inserts labels within a string by given by their key.
	 *
	 * @param aString          the string
	 * @param aLabelVarPrefix  the label var prefix string
	 * @param aLabelVarPostfix the label var postfix string
	 *
	 * @return the string
	 */
	private String insertLabelsWithinString(String aString, String aLabelVarPrefix, String aLabelVarPostfix) {
		// find first label key
		int lStart = aString.indexOf(aLabelVarPrefix);
		int lEnd = aString.indexOf(aLabelVarPostfix);
		if (!(lStart >= 0 && lEnd > lStart)) {
			// no label keys within string
			return aString;
		}

		while (lStart >= 0 && lEnd > lStart) {
			// enable label pointers within label pointers
			lStart = aString.substring(0, lEnd).lastIndexOf(aLabelVarPrefix);
			String lLabelKey = aString.substring(lStart + aLabelVarPrefix.length(), lEnd);
			// get label value
			String lLabelValue = "";
			if (aLabelVarPrefix.equals(AppConstants.labelVarPrefix)) {
				lLabelValue = getLabel(lLabelKey);
			} else if (aLabelVarPrefix.equals(AppConstants.labelVarPrefixC)) {
				lLabelValue = getCLabel(lLabelKey);
			} else if (aLabelVarPrefix.equals(AppConstants.sessionVarPrefix)) {
				lLabelValue = (String) getSession().getAttribute(lLabelKey);
			}
			if (StringUtils.isEmpty(lLabelValue)) {
				lLabelValue = "---LABEL OR SESSION VAR WITH KEY  '" + lLabelKey + "' NOT FOUND!---";
			}
			aString = aString.substring(0, lStart) + lLabelValue + aString.substring(lEnd + aLabelVarPostfix.length());
			lStart = aString.indexOf(aLabelVarPrefix);
			lEnd = aString.indexOf(aLabelVarPostfix);
		}
		return aString;
	}

	/**
	 * Inserts labels within a string by given by their key.
	 *
	 * @param aString the string
	 *
	 * @return the string
	 */
	public String insertLabelsWithinString(String aString) {
		aString = insertLabelsWithinString(aString, AppConstants.labelVarPrefix, AppConstants.labelVarPostfix);
		aString = insertLabelsWithinString(aString, AppConstants.labelVarPrefixC, AppConstants.labelVarPostfixC);
		aString = insertLabelsWithinString(aString, AppConstants.sessionVarPrefix, AppConstants.sessionVarPostfix);
		return aString;
	}

	/**
	 * Gets the value of a labelkey in the .properties file and capitalizes first
	 * character.
	 *
	 * @param aKey the a key
	 *
	 * @return the label
	 */
	public String getCLabel(String aKey) {
		return getCapitalizeFirstChar(getLabel(aKey));
	}

	/**
	 * Capitalizes first character of a String.
	 *
	 * @param aString the a string
	 *
	 * @return the string
	 */
	public static String getCapitalizeFirstChar(String aString) {
		if (aString.length() > 0 && Character.isLowerCase(aString.charAt(0))) {
			aString = Character.toUpperCase(aString.charAt(0)) + aString.substring(1);
		}
		return aString;
	}

	/**
	 * Gets the component given by aId, within the current zk page, that is desktop.
	 * aId may also be a Uuid.
	 *
	 * @param aId the a id
	 *
	 * @return the component
	 */
	public Component getComponent(String aId) {
		if (Executions.getCurrent() == null)
			return null;
		Collection<Component> lComponents = Executions.getCurrent().getDesktop().getComponents();
		if (lComponents != null) {
			for (Component lComponent : lComponents) {
				if (lComponent.getId().equals(aId) || lComponent.getUuid().equals(aId))
					return lComponent;
			}
		}
		return null;
	}

	/**
	 * Gets components that start with aIdPrefix, within the current zk page, that
	 * is desktop.
	 *
	 * @param aIdPrefix the a id prefix
	 *
	 * @return the components
	 */
	public List<Component> getComponentsByPrefix(String aIdPrefix) {
		List<Component> lFoundComponents = new ArrayList<>();
		if (Executions.getCurrent() == null)
			return lFoundComponents;
		Collection<Component> lComponents = Executions.getCurrent().getDesktop().getComponents();
		if (lComponents != null) {
			for (Component lComponent : lComponents) {
				if (lComponent.getId().startsWith(aIdPrefix)) {
					lFoundComponents.add(lComponent);
				}
			}
		}
		return lFoundComponents;
	}

	/**
	 * Gets components that match aIdMatch, within the current zk page, that is
	 * desktop.
	 *
	 * @param aIdMatch the a id match
	 *
	 * @return the components
	 */
	public List<Component> getComponentsByMatch(String aIdMatch) {
		List<Component> lFoundComponents = new ArrayList<>();
		if (Executions.getCurrent() == null)
			return lFoundComponents;
		Collection<Component> lComponents = Executions.getCurrent().getDesktop().getComponents();
		if (lComponents != null) {
			for (Component lComponent : lComponents) {
				if (lComponent.getId().matches(aIdMatch)) {
					lFoundComponents.add(lComponent);
				}
			}
		}
		return lFoundComponents;
	}

	/**
	 * Gets components by style class, within the current zk page, that is desktop.
	 *
	 * @param aStyleClass
	 *
	 * @return the components
	 */
	public List<Component> getComponentsByStyleClass(String aStyleClass) {
		List<Component> lFoundComponents = new ArrayList<>();
		if (Executions.getCurrent() == null)
			return lFoundComponents;
		Collection<Component> lComponents = Executions.getCurrent().getDesktop().getComponents();
		if (lComponents != null) {
			for (Component lComponent : lComponents) {
				if (lComponent instanceof HtmlBasedComponent) {
					HtmlBasedComponent lHtmlBasedComponent = (HtmlBasedComponent) lComponent;
					boolean lMatch = lHtmlBasedComponent.getSclass() != null
							&& lHtmlBasedComponent.getSclass().equals(aStyleClass)
							|| lHtmlBasedComponent.getZclass() != null
									&& lHtmlBasedComponent.getZclass().equals(aStyleClass);
					if (lMatch) {
						lFoundComponents.add(lComponent);
					}
				}
			}
		}
		return lFoundComponents;
	}

	/**
	 * Gets all components of a component type within the current zk page, that is
	 * desktop.
	 *
	 * @param aComponentCode the a id prefix
	 *
	 * @return the components
	 */
	public List<CRunComponent> getRunComponentsByComponentCode(String aComponentCode) {
		List<CRunComponent> lFoundComponents = new ArrayList<>();
		if (Executions.getCurrent() == null)
			return lFoundComponents;
		Collection<Component> lComponents = Executions.getCurrent().getDesktop().getComponents();
		if (lComponents != null) {
			for (Component lComponent : lComponents) {
				if (lComponent instanceof CRunComponent) {
					CRunComponent lRunComponent = (CRunComponent) lComponent;
					IECaseComponent lCaseComponent = lRunComponent.getCaseComponent();
					// NOTE could be that desktop run component is not (yet) connected to case
					// component
					if (lCaseComponent != null) {
						if (lCaseComponent.getEComponent().getCode().equals(aComponentCode)) {
							lFoundComponents.add(lRunComponent);
						}
					}
				}
			}
		}
		return lFoundComponents;
	}

	/**
	 * Gets the request parameter given by aKey.
	 *
	 * @param aKey the a key
	 *
	 * @return the req par
	 */
	public static final String getReqPar(String aKey) {
		if (Executions.getCurrent() == null)
			return null;
		return Executions.getCurrent().getParameter(aKey);
	}

	/**
	 * Gets the session.
	 *
	 * @return the session
	 */
	public Session getSession() {
		return CDesktopComponents.cControl().getSession();
	}

	/**
	 * Gets init parameter, a value within web.xml, by key. Same method also defined
	 * in AppManager!
	 *
	 * @param aKey the a key
	 *
	 * @return init parameter value
	 */
	public static final String getInitParameter(String aKey) {
		String lValue = "";
		_log.debug("Get value for key " + aKey);
		if (Executions.getCurrent() != null && Executions.getCurrent().getDesktop() != null) {
			_log.debug("Get value from ZK");
			lValue = Executions.getCurrent().getDesktop().getWebApp().getServletContext().getInitParameter(aKey);
		} else if (servletContext != null) {
			_log.debug("Get value from servletcontext");
			lValue = servletContext.getInitParameter(aKey);
		}
		if (lValue == null) {
			_log.debug("Value is null. Default to empty string");
			lValue = "";
		}
		return lValue;
	}

	/**
	 * Gets init parameter, a value within web.xml, by key. Same method also defined
	 * in AppManager!
	 *
	 * @param aKey the a key
	 *
	 * @return init parameter value
	 */
	public static final String getInitParameter(final String aKey, final String defaultValue) {
		String lValue = getInitParameter(aKey);

		if (StringUtils.isEmpty(lValue)) {
			lValue = defaultValue;
			_log.debug("Got empty value for " + aKey + ". Will use default value " + lValue);
		}
	
		return lValue;
	}
	
	/**
	 * gets temp server specific parameter by key, just in memory.
	 *
	 * @param aKey the a key
	 *
	 * @return temp server parameter value
	 */
	public String getTempServerParameter(String aKey) {
		if (getSession() == null) {
			return null;
		}
		HashMap<String, String> tempServerParameters = (HashMap<String, String>) getSession()
				.getAttribute("temp_server_parameters");
		if (tempServerParameters == null) {
			return null;
		}
		return tempServerParameters.get(aKey);
	}

	/**
	 * Sets temp server specific parameter by key. Used to overwrite a server
	 * parameter value during a session.
	 *
	 * @param aKey   the a key
	 * @param aValue the a value
	 */
	public void setTempServerParameter(String aKey, String aValue) {
		if (getSession() == null) {
			return;
		}
		HashMap<String, String> tempServerParameters = (HashMap<String, String>) getSession()
				.getAttribute("temp_server_parameters");
		if (tempServerParameters == null) {
			tempServerParameters = new HashMap<String, String>();
			getSession().setAttribute("temp_server_parameters", tempServerParameters);
		}
		tempServerParameters.put(aKey, aValue);
	}

	/**
	 * Clears server specific parameters in memory.
	 */
	public void clearServerConfiguration() {
		PropsValues.reset();
		if (getSession() != null) {
			getSession().setAttribute("temp_server_parameters", null);
		}
	}

	/**
	 * Gets absolute app path for webapps/emergo Same method with other body also
	 * defined in AppManager!
	 *
	 * @return absolute app path.
	 */
	public String getAbsoluteAppPath() {
		if (Executions.getCurrent() != null && Executions.getCurrent().getDesktop() != null) {
			return Executions.getCurrent().getDesktop().getWebApp().getServletContext().getRealPath("");
		} else if (servletContext != null) {
			return servletContext.getRealPath("");
		}
		return "";
	}

	/**
	 * Returns parameter from request. But if null return attribute from parent
	 * include. But if null return alternative value.
	 *
	 * @param aKey              the a key
	 * @param aComponent        the a component
	 * @param aAlternativeValue the a alternative value
	 *
	 * @return value
	 */
	public static final Object getParameter(String aKey, Component aComponent, String aAlternativeValue) {
		Object lValue = getReqPar(aKey);
		if (lValue == null) {
			Include lIncludeParent = (Include) getIncludeParent(aComponent);
			if (lIncludeParent != null) {
				lValue = lIncludeParent.getAttribute(aKey);
			}
			if (lValue == null || ((lValue instanceof String) && lValue.equals(""))) {
				lValue = aAlternativeValue;
			}
		}
		return lValue;
	}
	
	/**
	 * Sets current server protocol.
	 *
	 */
	public void setServerProtocol(String aProtocol) {
		setTempServerParameter("emergo.server.protocol", aProtocol);
	}

	/**
	 * Gets current server protocol.
	 *
	 * @return server protocol
	 */
	public String getServerProtocol() {
		String lScheme = getTempServerParameter("emergo.server.protocol");
		if (StringUtils.isEmpty(lScheme)) {
			lScheme = PropsValues.SERVER_PROTOCOL;
			if (StringUtils.isEmpty(lScheme)) {
				lScheme = Executions.getCurrent().getScheme();
			}
		}
		return StringUtils.isEmpty(lScheme) ? "" : lScheme;
	}

	/**
	 * Gets Emergo root url.
	 *
	 * @return Emergo root url
	 */
	// NOTE executions.getCurrent().getScheme() may contain wrong scheme, for
	// example when the Emergo server has http but is accessed
	// through an external server that is secured by https; in that case, browsers
	// block the access to http content.
	// So then, we need to overrule the calculation of the root url by setting the
	// correct scheme in web.xml
	public String getEmergoRootUrl() {
		if (Executions.getCurrent() == null)
			return "";
		if ((v_eme_root_url == null) || v_eme_root_url.equals("")) {
			String lScheme = getServerProtocol();
			if (lScheme.equals("")) {
				v_eme_root_url = Executions.getCurrent().getHeader("origin");
			}
			if (!lScheme.equals("")) {
				String port = (Executions.getCurrent().getServerPort() == 80) ? ""
						: (":" + Executions.getCurrent().getServerPort());
				v_eme_root_url = lScheme + "://" + Executions.getCurrent().getServerName() + port;
			}
		}
		return v_eme_root_url;
	}

	/**
	 * Gets Emergo current url, url of zul page shown.
	 *
	 * @return Emergo current url
	 */
	public String getCurrentUrl() {
		if (Executions.getCurrent() == null)
			return "";
		return getEmergoRootUrl() + Executions.getCurrent().getContextPath()
				+ Executions.getCurrent().getDesktop().getRequestPath();
	}

	/**
	 * Gets relative url if possible.
	 *
	 * @param aUrl the a url
	 *
	 * @return the (relative) url
	 */
	public String getRelativeUrl(String aUrl) {
		if (isAbsoluteUrl(aUrl)) {
			String lEmergoRootUrl = getEmergoRootUrl() + getEmergoWebappsRoot() + "/";
			if (aUrl.indexOf(lEmergoRootUrl) == 0) {
				String aRelativeUrl = aUrl;
				aRelativeUrl = aRelativeUrl.substring(lEmergoRootUrl.length());
				String lUrl = getCurrentUrl().substring(lEmergoRootUrl.length());
				for (int i = 0; i < StringUtils.countMatches(lUrl, "/"); i++) {
					aRelativeUrl = "../" + aRelativeUrl;
				}
				return aRelativeUrl;
			}
		}
		return aUrl;
	}

	/**
	 * Gets absolute url.
	 *
	 * @param aUrl the a url
	 *
	 * @return the absolute url
	 */
	public String getAbsoluteUrl(String aUrl) {
		if ((aUrl == null) || aUrl.equals(""))
			return "";
		if (!isAbsoluteUrl(aUrl)) {
			// possible put / before url
			if (!aUrl.startsWith("/")) {
				aUrl = "/" + aUrl;
			}
			// put emergo path before url
			aUrl = CDesktopComponents.vView().getEmergoRootUrl() + CDesktopComponents.vView().getEmergoWebappsRoot()
					+ aUrl;
		}
		return aUrl;
	}

	/**
	 * Gets Emergo webapps root, so emergo folder within Tomcat webapps folder
	 *
	 * @return Emergo root url
	 */
	public String getEmergoWebappsRoot() {
		if (Executions.getCurrent() == null)
			return "";
		return Executions.getCurrent().getContextPath();
	}

	public String getAbsoluteWebappsRoot() {
		return getEmergoRootUrl() + getEmergoWebappsRoot() + "/";
	}

	private String getStreamingApplication(String aAppName) {
		return PropsValues.STREAMING_SERVER_STREAMING_PROTOCOL + "://" + PropsValues.STREAMING_SERVER_NAME + "/"
				+ aAppName;
	}

	/**
	 * Gets url of application on streaming server, that handles recording of
	 * (webcam) video
	 *
	 * @return streaming recorder application url
	 */
	public String getStreamingRecorderApplication() {
		return getStreamingApplication(PropsValues.STREAMING_RECORDER_APPLICATION);
	}

	/**
	 * Gets url of streaming server, that handles HTML5-webrtc recording of (webcam)
	 * video
	 *
	 * @return streaming HTML5 recorder url
	 */
	public String getHtml5RecorderUrl() {
		return PropsValues.STREAMING_RECORDER_HTML5_PROTOCOL + "://" + PropsValues.STREAMING_SERVER_NAME + "/"
				+ PropsValues.STREAMING_RECORDER_HTML5_SESSIONINFO;
	}

	/**
	 * Gets url of application on streaming server, that handles recording of
	 * (webcam) video
	 *
	 * @return streaming recorder application url
	 */
	public String getHtml5RecorderApplication() {
		return PropsValues.STREAMING_RECORDER_HTML5_APPLICATION;
	}

	/**
	 * Gets url of application on streaming server, that handles steaming of video
	 *
	 * @return streaming player application url
	 */
	public String getStreamingPlayerApplication() {
		return PropsValues.STREAMING_PLAYER_APPLICATION;
	}

	/**
	 * Saves html5 streaming url in memory
	 *
	 * @param aUrl the a url, including extension, e.g. 'mp4'
	 */
	public void putHtml5StreamingUrl(String aUrl) {
		if (!pHtml5StreamingUrls.containsKey(aUrl)) {
			String lHtml5StreamingUrl = "";
			String lStreamingPlayerApplication = getStreamingPlayerApplication();
			if (!StringUtils.isEmpty(lStreamingPlayerApplication)) {
				// NOTE only determine html5 url if streaming server is used
				String lUrlPrefix = lStreamingPlayerApplication + "/";
				if (aUrl.indexOf(lUrlPrefix) == 0) {
					// NOTE url to streaming server contains back slashes. Replace by slashes
					String lUrlSuffix = aUrl.substring(lUrlPrefix.length())
							.replaceAll(PropsValues.STREAMING_PLAYER_PATH_SEPARATOR, "/");
					lHtml5StreamingUrl = getServerProtocol() + "://" + PropsValues.STREAMING_SERVER_NAME + "/"
							+ PropsValues.STREAMING_PLAYER_APPLICATION + "/" + getFileExtension(lUrlSuffix) + ":"
							+ PropsValues.STREAMING_PLAYER_APPLICATION + "/" + lUrlSuffix
							+ PropsValues.STREAMING_PLAYER_MANIFEST;
				}
			}
			pHtml5StreamingUrls.put(aUrl, lHtml5StreamingUrl);
		}
	}

	/**
	 * Gets html5 streaming url depending on aUrl
	 *
	 * @param aUrl the a url
	 *
	 * @return html5 streaming url
	 */
	public String getHtml5StreamingUrl(String aUrl) {
		if (!pHtml5StreamingUrls.containsKey(aUrl)) {
			putHtml5StreamingUrl(aUrl);
		}
		return pHtml5StreamingUrls.get(aUrl);
	}

	/**
	 * Gets all HTML5 streaming URLs.
	 *
	 * @param aUrl the original streaming url
	 *
	 * @return html5 streaming urls
	 */
	public List<List<String>> getHtml5StreamingUrls(String aUrl) {
		List<List<String>> lHtml5Urls = new ArrayList<List<String>>();
		String lStreamingPlayerApplication = getStreamingPlayerApplication();
		if (!StringUtils.isEmpty(lStreamingPlayerApplication)) {
			// NOTE only determine list of html5 urls if streaming server is used
			String lUrlPrefix = lStreamingPlayerApplication + "/";
			if (aUrl.indexOf(lUrlPrefix) == 0) {
				// NOTE original file is already found on streaming server
				// NOTE url to streaming server contains back slashes. Replace by slashes
				String lUrlSuffix = aUrl.substring(lUrlPrefix.length())
						.replaceAll(PropsValues.STREAMING_PLAYER_PATH_SEPARATOR, "/");
				String lUrlBody = getFileBody(lUrlSuffix);
				String lExtension = lUrlSuffix.substring(lUrlBody.length(), lUrlSuffix.length());
				String lUrlStart = getServerProtocol() + "://" + PropsValues.STREAMING_SERVER_NAME + "/"
						+ PropsValues.STREAMING_PLAYER_APPLICATION + "/" + getFileExtension(lUrlSuffix) + ":"
						+ PropsValues.STREAMING_PLAYER_APPLICATION + "/" + lUrlBody;
				List<String> lStreamFormats = PropsValues.STREAMING_PLAYER_FORMATS;
				List<String> lTransSuffixes = new ArrayList<String>(PropsValues.STREAMING_PLAYER_TRANSCODE_SUFFIXES);
				lTransSuffixes.add("");
				if (!lStreamFormats.isEmpty()) {
					FtpHelper lFtpHelper = new FtpHelper();
					for (String lTransSuffix : lTransSuffixes) {
						if (lTransSuffix.equals("")
								|| lFtpHelper.ftpTransFileExists("", lUrlBody + lTransSuffix + lExtension)) {
							List<String> lTransUrls = new ArrayList<String>();
							for (String lFormat : lStreamFormats) {
								String lTransUrl = lUrlStart + lTransSuffix + lExtension + lFormat;
								lTransUrls.add("\"" + uniqueView(lTransUrl) + "\"");
								// lTransUrls.add(uniqueView(lTransUrl));
							}
							lHtml5Urls.add((lTransUrls));
						}
					}
				}
			}
		}
		return lHtml5Urls;
	}

	/**
	 * Clears html5 streaming urls
	 */
	public void clearPresentAndAbsentFiles() {
		pHtml5StreamingUrls.clear();
	}

	/**
	 * Determines if absolute Url refers to rtmp streaming server
	 *
	 * @param aUrl the a url
	 *
	 * @return if true
	 */
	public boolean isRtmpUrl(String aUrl) {
		if ((aUrl != null) && !(aUrl.equals(""))) {
			Iterable<String> lStreamingServers = asList(getStreamingRecorderApplication(),
					getStreamingPlayerApplication());
			for (String lServerName : lStreamingServers) {
				if (!((lServerName == null) || (lServerName.equals("")))) {
					if (aUrl.indexOf(lServerName) == 0)
						return true;
				}
			}
		}
		return false;
	}

	/**
	 * Determines if Url is absolute
	 *
	 * @param aUrl the a url
	 *
	 * @return if true
	 */
	public boolean isAbsoluteUrl(String aUrl) {
		try {
			URL url = new URL(aUrl);
//			final URI lUri = new URI(aUrl);
			final URI lUri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(),
					url.getQuery(), url.getRef());
			return lUri.isAbsolute();
		} catch (MalformedURLException e) {
			// no streamhandler registered for 'rtmp'-protocol, so for the moment we just
			// compare url with streaming server url
			return isRtmpUrl(aUrl);
		} catch (URISyntaxException e) {
			return false;
		}
	}

	/**
	 * Get url without parameters such as '?par1=value'
	 *
	 * @param aUrl the a url
	 *
	 * @return stripped url
	 */
	public String getUrlWithoutPars(String aUrl) {
		if (aUrl != null && aUrl.length() > 0) {
			int lPos = aUrl.indexOf("?");
			if (lPos >= 0) {
				return aUrl.substring(0, lPos);
			}
		}
		return aUrl;
	}

	/**
	 * Encodes only the filename of a URL
	 *
	 * @param aUrl the a url
	 *
	 * @return the encoded url
	 */
	public String getUrlEncodedFileName(String aUrl) {
		if (aUrl == null || aUrl.isEmpty())
			return "";
		// NOTE if streaming server urls are encoded the url is is not found! Even
		// replacing spaces by '%20' causes trouble.
		if (isRtmpUrl(aUrl))
			return aUrl;
		try {
			int lPos = aUrl.lastIndexOf("/");
			if (!(lPos < 0) && (lPos < (aUrl.length() - 1))) {
				String lFileName = aUrl.substring(lPos + 1);
				String lDir = aUrl.substring(0, lPos + 1);
				// 'encode' replaces spaces by '+' (and '+' by '%2B') but we need spaces to be
				// replaced by '%20':
				aUrl = lDir + URLEncoder.encode(lFileName, "UTF-8").replace("+", "%20");
			}
		} catch (UnsupportedEncodingException e) {
		}
		return aUrl;
	}

	/**
	 * Get file extension.
	 *
	 * @param aUrl the a url
	 *
	 * @return extension
	 */
	public String getFileExtension(String aUrl) {
		String lFileExtension = "";
		if (aUrl == null) {
			return lFileExtension;
		}
		int lPos = aUrl.lastIndexOf(".");
		if (lPos > 0) {
			lFileExtension = (aUrl.substring(lPos + 1, aUrl.length())).toLowerCase(Locale.ENGLISH);
		}
		// urls can have parameters so remove
		lPos = lFileExtension.indexOf("?");
		if (lPos > 0) {
			lFileExtension = lFileExtension.substring(0, lPos);
		}
		return lFileExtension;
	}

	/**
	 * Get file name without extension.
	 *
	 * @param aUrl the a url
	 *
	 * @return file body
	 */
	protected String getFileBody(String aUrl) {
		if (aUrl == null) {
			return "";
		}
		int lPos = aUrl.lastIndexOf(".");
		if (lPos > 0) {
			return aUrl.substring(0, lPos);
		}
		return aUrl;
	}

	/**
	 * Gets run class name. Strips skin from run class name.
	 *
	 * @param aClassName the a class name
	 *
	 * @return run class name
	 */
	public String getRunClassName(String aClassName) {
		String lClassName = new String(aClassName);
		String lStartsWith = "CRun";
		if (lClassName.indexOf(lStartsWith) == 0) {
			String lClassExtensions = VView.getInitParameter("emergo.skin.class.extensions");
			String[] lClassExtensionsArr = lClassExtensions.split(",");
			int lCounter = 1;
			String lSkinId = VView.getInitParameter("emergo.skin." + lCounter);
			while (!lSkinId.equals("")) {
				String lEndsWith = lSkinId;
				lEndsWith = lSkinId.substring(0, 1).toUpperCase() + lSkinId.substring(1, lSkinId.length());
				if (lClassName.endsWith(lEndsWith)) {
					for (int i = 0; i < lClassExtensionsArr.length; i++) {
						if (lEndsWith.equals(lClassExtensionsArr[i])) {
							lClassName = lClassName.substring(0, lClassName.length() - lEndsWith.length());
						}
					}
				}
				lCounter += 1;
				lSkinId = VView.getInitParameter("emergo.skin." + lCounter);
			}

		}
		return lClassName;
	}

	/**
	 * Sets tooltip for label if label value length is too large.
	 *
	 * @param aLabel           the a label
	 * @param aMaxStringLength the a max string length
	 */
	public void setLabelTooltiptext(Label aLabel, int aMaxStringLength) {
		String labelValue = aLabel.getValue();
		if (labelValue.length() > aMaxStringLength) {
			aLabel.setTooltiptext(labelValue);
			aLabel.setValue(labelValue.substring(0, aMaxStringLength) + "...");
		}
	}

	/**
	 * Gets javascript string for window.open fullscreen.
	 *
	 * @param aUrl the a url
	 *
	 * @return javascript expression
	 */
	public String getJavascriptWindowOpenFullscreen(String aUrl) {
		return AppConstants.javascriptWindowOpenStart + aUrl + AppConstants.javascriptWindowOpenMid
				+ AppConstants.javascriptWindowOpenStyleFullscreen + AppConstants.javascriptWindowOpenEnd;
	}

	/**
	 * Gets javascript string for window.open.
	 *
	 * @param aUrl the a url
	 *
	 * @return javascript expression
	 */
	public String getJavascriptWindowOpen(String aUrl) {
		return AppConstants.javascriptWindowOpenStart + aUrl + AppConstants.javascriptWindowOpenMid
				+ AppConstants.javascriptWindowOpenEnd;
	}

	/**
	 * Gets javascript string for window.open.
	 *
	 * @param aUrlPrefix the a url prefix
	 * @param aUrl       the a url
	 *
	 * @return javascript expression
	 */
	public String getJavascriptWindowLocationHref(String aUrl) {
		return AppConstants.javascriptWindowLocationHrefStart + aUrl + AppConstants.javascriptWindowLocationHrefEnd;
	}

	/**
	 * Sets label for an arbitrary ZUL component.
	 *
	 * @param aComponent the ZUL component
	 * @param aLabelKey  the label id within the .properties files
	 */
	public void setLabel(Component aComponent, String aLabelKey) {
		String label = getLabel(aLabelKey);
		if (label.length() == 0) {
			return;
		}
		// NOTE For following classes a 'label' may be used
		if (aComponent instanceof Window) {
			((Window) aComponent).setTitle(label);
		} else if (aComponent instanceof Label) {
			((Label) aComponent).setValue(label);
		} else if (aComponent instanceof Listitem) {
			((Listitem) aComponent).setLabel(label);
		} else if (aComponent instanceof Html) {
			((Html) aComponent).setContent(label);
		} else if (aComponent instanceof LabelElement) {
			((LabelElement) aComponent).setLabel(label);
		}
	}

	/**
	 * Get root window of aComponent.
	 *
	 * @param aComponent the ZUL component
	 *
	 * @return root window or null
	 */
	public Component getRootWindow(Component aComponent) {
		Component lParent = aComponent.getParent();
		while (lParent != null && !(lParent instanceof Window)) {
			lParent = lParent.getParent();
		}
		return lParent;
	}

	/**
	 * Sets help text for ZUL component. As well as hover (for devices that support
	 * hovering) as by clicking on an image. Note that not all components allow for
	 * having an image as child, so the image is added as child of the root window
	 * given by aComponent.getRoot() and positioned relative to aComponent using
	 * javascript.
	 *
	 * @param aComponent the ZUL component
	 * @param aLabelKey  the label id within the .properties files
	 */
	public void setHelpTextByLabelKey(Component aComponent, String aLabelKey) {
		setHelpText(aComponent, getLabel(aLabelKey));
	}

	/**
	 * Sets help text for parent of ZUL component. As well as hover (for devices
	 * that support hovering) as by clicking on an image. Note that it is assumed
	 * that aParent allows for having an image as child.
	 *
	 * @param aParent   the intended parent of the help image
	 * @param aLabelKey the label id within the .properties files
	 */
	public void setParentHelpTextByLabelKey(Component aParent, String aLabelKey) {
		setParentHelpText(aParent, getLabel(aLabelKey));
	}

	/**
	 * Sets help text for ZUL component. As well as hover (for devices that support
	 * hovering) as by clicking on an image. Note that not all components allow for
	 * having an image as child, so the image is added as child of the root window
	 * given by aComponent.getRoot() and positioned relative to aComponent using
	 * javascript.
	 *
	 * @param aComponent the ZUL component
	 * @param aHelpText  the help text
	 */
	public void setHelpText(Component aComponent, String aHelpText) {
		if (aHelpText == null || aHelpText.length() == 0) {
			return;
		}
		Image lImage = getHelpImage(aComponent.getRoot(), aHelpText);
		lImage.setZclass("CHelpImageAbsolute");
		// position image relative to aComponent
		Clients.evalJavaScript("positionHelpImage('" + aComponent.getUuid() + "','" + lImage.getUuid() + "');");
	}

	/**
	 * Sets help text for parent of ZUL component. As well as hover (for devices
	 * that support hovering) as by clicking on an image. Note that it is assumed
	 * that aParent allows for having an image as child.
	 *
	 * @param aParent   the intended parent of the help image
	 * @param aHelpText the help text
	 */
	public void setParentHelpText(Component aParent, String aHelpText) {
		if (aHelpText == null || aHelpText.length() == 0) {
			return;
		}
		Image lImage = getHelpImage(aParent, aHelpText);
		lImage.setZclass("CHelpImage");
	}

	/**
	 * Sets help component for ZUL component, that will render help content. As well
	 * as hover (for devices that support hovering) as by clicking on an image. Note
	 * that not all components allow for having an image as child, so the image is
	 * added as child of the root window given by aComponent.getRoot() and
	 * positioned relative to aComponent using javascript.
	 *
	 * @param aComponent     the ZUL component
	 * @param aHelpComponent the help component
	 */
	public void setHelpComponent(Component aComponent, Component aHelpComponent) {
		if (aHelpComponent == null) {
			return;
		}
		Image lImage = getHelpImage(aComponent.getRoot(), aHelpComponent);
		lImage.setZclass("CHelpImageAbsolute");
		// position image relative to aComponent
		Clients.evalJavaScript("positionHelpImage('" + aComponent.getUuid() + "','" + lImage.getUuid() + "');");
	}

	/**
	 * Sets help component for ZUL component, that will render help content. As well
	 * as hover (for devices that support hovering) as by clicking on an image. Note
	 * that it is assumed that aParent allows for having an image as child.
	 *
	 * @param aParent        the intended parent of the help image
	 * @param aHelpComponent the help component
	 */
	public void setParentHelpComponent(Component aParent, Component aHelpComponent) {
		if (aHelpComponent == null) {
			return;
		}
		Image lImage = getHelpImage(aParent, aHelpComponent);
		lImage.setZclass("CHelpImage");
	}

	/**
	 * Gets an image to show a help content on hovering or clicking.
	 *
	 * @param aParent     the intended parent of the image
	 * @param aHelpObject the help object
	 */
	public Image getHelpImage(Component aParent, Object aHelpObject) {
		Image lImage = new Image();
		aParent.appendChild(lImage);
		if (aHelpObject instanceof String) {
			// set help text as attribute to show it in tooltip
			setTooltip(lImage, aHelpObject);
		} else if (aHelpObject instanceof Component) {
			// set help component as attribute to render help content
			setHelpImageEventListener(lImage, "onMouseOver", aHelpObject);
		}
		// add an onClick handler to show the help text on the screen, in case hovering
		// is not supported
		setHelpImageEventListener(lImage, "onClick", aHelpObject);
		return lImage;
	}

	/**
	 * Sets image event listener.
	 *
	 * @param aComponent  the ZUL component
	 * @param aMouseEvent the mouse event
	 * @param aHelpObject the help object
	 */
	public void setHelpImageEventListener(Component aComponent, String aMouseEvent, Object aHelpObject) {
		aComponent.addEventListener(aMouseEvent, new EventListener<MouseEvent>() {
			@Override
			public void onEvent(MouseEvent event) {
				Window lHelpWnd = (Window) getComponent(helpWndId);
				if (lHelpWnd != null) {
					// show help
					Events.postEvent("onShowHelp", lHelpWnd, aHelpObject);
				}
			}
		});
	}

	/**
	 * Sets tool tip and text for XUL element.
	 *
	 * @param aXulElement the XUL element
	 * @param aHelpObject the help object
	 */
	public void setTooltip(XulElement aXulElement, Object aHelpObject) {
		if (aHelpObject == null) {
			return;
		}
		aXulElement.setAttribute("helpObject", aHelpObject);
		// set tooltip
		aXulElement.setTooltip(helpHoverWndId);
	}

	public int getColWidth1() {
		return colWidth1;
	}

	public int getColWidth2() {
		return colWidth2;
	}

	public void decorateComponent(Component aComponent) {
		if (aComponent == null || aComponent.getAttributes().size() == 0) {
			return;
		}
		String lWidth = getAttributeString(aComponent, "width");
		if (lWidth.length() > 0) {
			if (aComponent instanceof HtmlBasedComponent) {
				int lWidthCorrection = 0;
				try {
					lWidthCorrection = Integer.parseInt(getAttributeString(aComponent, "widthCorrection"));
				} catch (NumberFormatException e) {
				}
				if (lWidth.equals("windowWidth")) {
					((HtmlBasedComponent) aComponent)
							.setWidth("" + (getColWidth1() + getColWidth2() + 52 + lWidthCorrection) + "px");
				} else if (lWidth.equals("colWidth1")) {
					((HtmlBasedComponent) aComponent).setWidth("" + (getColWidth1() + 5 + lWidthCorrection) + "px");
				} else if (lWidth.equals("colWidth2")) {
					if (aComponent instanceof Textbox) {
						((HtmlBasedComponent) aComponent).setWidth("" + (getColWidth2() + 5 + lWidthCorrection) + "px");
					} else {
						((HtmlBasedComponent) aComponent)
								.setWidth("" + (getColWidth2() + 15 + lWidthCorrection) + "px");
					}
				}
			}
		}

		String lLabel = "";
		String lLabelKey = getAttributeString(aComponent, "labelKey");
		if (lLabelKey.length() > 0) {
			boolean lLabelCapitalizeFirstChar = getAttributeString(aComponent, "labelCapitalizeFirstChar")
					.equals(AppConstants.statusValueTrue);
			if (lLabelCapitalizeFirstChar) {
				lLabel = getCLabel(lLabelKey);
			} else {
				lLabel = getLabel(lLabelKey);
			}
			if (getAttributeString(aComponent, "InputRequired").equals(AppConstants.statusValueTrue)) {
				lLabel += " " + getLabel("input_required");
			}
		}
		String lLabelValue = getAttributeString(aComponent, "labelValue");
		if (lLabelValue.length() > 0) {
			lLabel = lLabelValue;
		}
		if (lLabel.length() > 0) {
			if (aComponent instanceof Label) {
				((Label) aComponent).setValue(lLabel);
			} else if (aComponent instanceof LabelElement) {
				((LabelElement) aComponent).setLabel(lLabel);
			} else if (aComponent instanceof Html) {
				((Html) aComponent).setContent(lLabel);
			} else if (aComponent instanceof Window) {
				((Window) aComponent).setTitle(lLabel);
			}
		}

		String lHelpLabelKey = getAttributeString(aComponent, "helpLabelKey");
		if (lHelpLabelKey.length() > 0) {
			setHelpTextByLabelKey(aComponent, lHelpLabelKey);
		}
		lHelpLabelKey = getAttributeString(aComponent, "parentHelpLabelKey");
		if (lHelpLabelKey.length() > 0 && aComponent.getParent() != null) {
			setParentHelpTextByLabelKey(aComponent.getParent(), lHelpLabelKey);
		}
		String lUrl = getAttributeString(aComponent, "urlToOpenOnClick");
		if (lUrl.length() > 0) {
			if (!isAbsoluteUrl(lUrl)) {
				lUrl = getAbsoluteUrl(lUrl);
			}
			aComponent.setWidgetListener("onClick", getJavascriptWindowOpen(lUrl));
		}
		lUrl = getAttributeString(aComponent, "urlToRedirectTo");
		if (lUrl.length() > 0) {
			if (!isAbsoluteUrl(lUrl)) {
				lUrl = getAbsoluteUrl(lUrl);
			}
			redirectToView(lUrl);
		}
	}

	public String getAttributeString(Component aComponent, String aAttributeKey) {
		if (aComponent == null || aComponent.getAttributes().size() == 0) {
			return "";
		}
		if (!StringUtils.isEmpty((String) aComponent.getAttribute(aAttributeKey))) {
			return (String) aComponent.getAttribute(aAttributeKey);
		}
		return "";
	}

}
