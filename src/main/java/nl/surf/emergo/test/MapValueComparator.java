package nl.surf.emergo.test;

import java.util.Comparator;
import java.util.Map;

public class MapValueComparator implements Comparator {

	private Map map;

	public MapValueComparator(Map map) {
		this.map = map;
	}

	public int compare(Object keyA, Object keyB) {
		if (map.get(keyA) != null && map.get(keyB) != null) {
			return ((String) map.get(keyA)).compareTo((String) map.get(keyB));
		}
		return 0;
	}
}
