/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.test;

import java.util.Hashtable;
import java.util.List;

import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.tut.CTutRunGroupAccountsHelper;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERunGroupAccount;

/**
 * The Class CTutRunGroupAccountScoresHelper.
 */
public class CTutRunGroupAccountScoresHelper extends CTutRunGroupAccountsHelper {

	/** The tasks case component. */
	private IECaseComponent alertsCaseComponent = null;

	/**
	 * Renders sent interventions for one account.
	 * 
	 * @param aListbox the a listbox
	 * @param aInsertBefore the a insert before
	 * @param aItem the a item
	 */
	public void renderItem(Listbox aListbox,Listitem aInsertBefore,Hashtable<String,Object> aItem) {
		Listitem lListitem = super.newListitem();
		List<IERunGroupAccount> lRunGroupAccounts = (List<IERunGroupAccount>)aItem.get("rungroupaccounts");
		IEAccount lItem = (IEAccount)aItem.get("item");
		lListitem.setValue(lItem);
		super.appendListcell(lListitem,lItem.getTitle());
		super.appendListcell(lListitem,lItem.getInitials());
		super.appendListcell(lListitem,lItem.getNameprefix());
		super.appendListcell(lListitem,lItem.getLastname());
		Listcell lListcell = new Listcell();

		int lScore = 0;
		for (int i=0;i<lRunGroupAccounts.size();i++) {
			CDesktopComponents.sSpring().setRunStatus(AppConstants.runStatusRun);
			CDesktopComponents.sSpring().setRunGroupAccount((IERunGroupAccount)lRunGroupAccounts.get(i));
			if (alertsCaseComponent == null)
				alertsCaseComponent = CDesktopComponents.sSpring().getCaseComponent("alerts", "");
			if (alertsCaseComponent == null)
				return;
			List<IXMLTag> lTags = CDesktopComponents.cScript().getRunGroupNodeTags(""+alertsCaseComponent.getCacId(), "alert");
			for (int j=(lTags.size()-1);j>=0;j--) {
				IXMLTag lTag = lTags.get(j);
				boolean lSent = ((CDesktopComponents.sSpring().getCurrentTagStatus(lTag, AppConstants.statusKeySent)).equals(AppConstants.statusValueTrue));
				if (lSent) {
					String lId = lTag.getChild("pid").getValue();
					if ((lId.indexOf("helaas") == 0) || (lId.indexOf("wrong") == 0) || (lId.indexOf("ok") == 0)) {
						if ((lId.indexOf("helaas1") == 0) || (lId.indexOf("wrong1") == 0)) {
							if (lId.indexOf("_1") >= 0)
								lScore = 8;
							if (lId.indexOf("_2") >= 0)
								lScore = 11;
							if (lId.indexOf("_3") >= 0)
								lScore = 14;
							if (lId.indexOf("_4") >= 0)
								lScore = 17;
							if (lId.indexOf("_5") >= 0)
								lScore = 20;
							if (lId.indexOf("_6") >= 0)
								lScore = 24;
							if (lId.indexOf("_7") >= 0)
								lScore = 27;
						}
						if ((lId.indexOf("helaas2") == 0) || (lId.indexOf("wrong2") == 0)) {
							if (lId.indexOf("_1") >= 0)
								lScore = 28;
							if (lId.indexOf("_2") >= 0)
								lScore = 31;
							if (lId.indexOf("_3") >= 0)
								lScore = 34;
							if (lId.indexOf("_4") >= 0)
								lScore = 37;
							if (lId.indexOf("_5") >= 0)
								lScore = 40;
							if (lId.indexOf("_6") >= 0)
								lScore = 43;
							if (lId.indexOf("_7") >= 0)
								lScore = 46;
						}
						if ((lId.indexOf("helaas3") == 0) || (lId.indexOf("wrong3") == 0)) {
							if (lId.indexOf("_1") >= 0)
								lScore = 49;
							if (lId.indexOf("_2") >= 0)
								lScore = 52;
							if (lId.indexOf("_3") >= 0)
								lScore = 55;
							if (lId.indexOf("_4") >= 0)
								lScore = 58;
							if (lId.indexOf("_5") >= 0)
								lScore = 61;
							if (lId.indexOf("_6") >= 0)
								lScore = 64;
							if (lId.indexOf("_7") >= 0)
								lScore = 67;
						}
						if (lId.indexOf("ok3") == 0) {
							if (lId.indexOf("_1") >= 0)
								lScore = 61;
							if (lId.indexOf("_2") >= 0)
								lScore = 66;
							if (lId.indexOf("_3") >= 0)
								lScore = 71;
							if (lId.indexOf("_4") >= 0)
								lScore = 77;
							if (lId.indexOf("_5") >= 0)
								lScore = 83;
							if (lId.indexOf("_6") >= 0)
								lScore = 89;
							if (lId.indexOf("_7") >= 0)
								lScore = 96;
						}
					}
				}
			}
		}

		String lScoreStr = "" + lScore + "%";
		if (lScoreStr.length() == 2)
			lScoreStr = "0" + lScoreStr;
		lListcell.setLabel(lScoreStr);

		lListitem.appendChild(lListcell);
		super.insertListitem(aListbox,lListitem,aInsertBefore);
	}

}