package nl.surf.emergo.test;

public class Container{
	    
	private String _label;
	private String _value;
			
	public Container (String label, String value) {
		_label = label;
		_value = value;
	}
	public String getLabel() {
		return _label;
	}
	public String getValue() {
		return _value;
	}
	public void setLabel(String label) {
		_label = label;
	}
	public void setValue(String value) {
		_value = value;
	}
	public String toString () {
		return getLabel() + " (" + getValue() + ")";
	}
	public boolean equals (Object o) {
		return (o instanceof Container) && ((Container)o).toString().equals(toString());
	}
}
