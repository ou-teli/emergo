package nl.surf.emergo.test;

import java.util.Comparator;
import java.util.Map;

public class MapKeyComparator implements Comparator {

	private Map map;

	public MapKeyComparator(Map map) {
		this.map = map;
	}

	public int compare(Object keyA, Object keyB) {
		if (keyA != null && keyB != null) {
			return ((String)keyA).compareTo(((String)keyB));
		}
		return 0;
	}
}
