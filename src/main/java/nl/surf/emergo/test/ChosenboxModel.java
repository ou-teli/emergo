package nl.surf.emergo.test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.zkspring.SSpring;

public class ChosenboxModel{
	    
	private ListModelList _model;
 	private Set _selectedObjects = new HashSet();;
	public static SSpring sSpring = CDesktopComponents.sSpring();
 	
	public String getTagAttribute(IXMLTag tag, String key) {
		return sSpring.unescapeXML(tag.getAttribute(key));
	}

 	public ListModel getModel (Object pItems, String pKey, List pSelectedValues) {
 		if (_model == null) {
 			List lObjects = new ArrayList();
 			Set lSelection = new HashSet();
			if (pItems instanceof List) {
				//items is list of IXMLTag
				String labelKey = "";
				if (pKey.equals("studyCode")) {
					labelKey = "code";
				}
				List objects = (List)pItems;
				for (Object object : objects) {
					IXMLTag item = (IXMLTag)object;
					lObjects.add(new Container(getTagAttribute(item, "id"), getTagAttribute(item, labelKey)));
					if (pSelectedValues.contains(getTagAttribute(item, labelKey))) {
						lSelection.add(new Container(getTagAttribute(item, "id"), getTagAttribute(item, labelKey)));
					}
				}
			}
			else if (pItems instanceof Map) {
				//items is map of <String,String>
				Map objects = (Map)pItems;
				for (Object object : objects.entrySet()) {
					Map.Entry entry = (Map.Entry)object;
					lObjects.add(new Container((String)entry.getValue(), (String)entry.getKey()));
					if (pSelectedValues.contains(entry.getKey())) {
						lSelection.add(new Container((String)entry.getValue(), (String)entry.getKey()));
					}
				}
			}
 			_model = new ListModelList(lObjects);
 			_selectedObjects.addAll(lSelection);
 		}
 		return _model;
 	}
 	
	public Set getSelectedObjects () {
		return _selectedObjects;
	}

}
	
