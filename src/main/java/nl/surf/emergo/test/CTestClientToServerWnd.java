/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.test;

import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Map;

import org.zkoss.json.JSONArray;
import org.zkoss.zk.au.AuRequest;
import org.zkoss.zul.Window;

public class CTestClientToServerWnd extends Window
{

	private static final long serialVersionUID = 9173361580480370646L;

	static {
	    addClientEvent(CTestClientToServerWnd.class, "onExternalEvent", CE_REPEAT_IGNORE);
	}

	public void service(AuRequest request, boolean everError) {
		if (!everError) {
			final String command = request.getCommand();
			if (command.equals("onExternalEvent")) {
//				final Component component = request.getComponent();
				final Map<String,Object> reqdata = request.getData();
				if ((reqdata == null) || (!reqdata.containsKey("data")) || (reqdata.size() != 1))
					return;
				JSONArray jdata = (JSONArray)reqdata.get("data");
				if ((jdata == null) || (jdata.size() != 1))
					return;
				LinkedList<String> ldata = (LinkedList<String>)jdata.getFirst();
				if ((ldata == null) || (ldata.size() != 3))
					return;
				String[] status = new String[3];
				ListIterator<String> litr = ldata.listIterator();
				while(litr.hasNext()) {
					if (litr.nextIndex() > 2)
						litr.next();
					if (litr.nextIndex() == 2)
						status[2] = litr.next();
					if (litr.nextIndex() == 1)
						status[1] = litr.next();
					if (litr.nextIndex() == 0)
						status[0] = litr.next();
				}
				String sender = status[0];
				onAction(sender, command, status);
			} else super.service(request, everError);
		} else super.service(request, everError);
	}

	public void onAction(String sender, String action, Object status) {
		if (action.equals("onExternalEvent")) {
		}
	}

}
