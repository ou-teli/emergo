/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */

package nl.surf.emergo.test;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import nl.surf.emergo.utilities.PropsValues;

/**
 * A simple Java REST GET example using the Apache HTTP library. This executes a
 * call against the Yahoo Weather API service, which is actually an RSS service
 * (http://developer.yahoo.com/weather/).
 * 
 * Try this Twitter API URL for another example (it returns JSON results):
 * http://search.twitter.com/search.json?q=%40apple (see this url for more
 * twitter info: https://dev.twitter.com/docs/using-search)
 * 
 * Apache HttpClient: http://hc.apache.org/httpclient-3.x/
 *
 */
public class ApacheHttpRestClient {
	private static final Logger _log = LogManager.getLogger(ApacheHttpRestClient.class);

	protected void httpGet1() {
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		try {
			// specify the host, protocol, and port
			HttpHost target = new HttpHost("weather.yahooapis.com", 80, "http");

			// specify the get request
			HttpGet getRequest = new HttpGet("/forecastrss?p=80020&u=f");

			_log.info("executing request to " + target);

			HttpResponse httpResponse = httpClient.execute(target, getRequest);
			HttpEntity entity = httpResponse.getEntity();

			_log.info("----------------------------------------");
			_log.info(httpResponse.getStatusLine());
			Header[] headers = httpResponse.getAllHeaders();
			for (int i = 0; i < headers.length; i++) {
				_log.info(headers[i]);
			}
			_log.info("----------------------------------------");

			if (entity != null) {
				_log.info(EntityUtils.toString(entity));
			}

		} catch (Exception e) {
			_log.error(e);
		} finally {
			// When HttpClient instance is no longer needed,
			// shut down the connection manager to ensure
			// immediate deallocation of all system resources
			try {
				httpClient.close();
			} catch (IOException e) {
				_log.error(e);
			}
		}
	}

	protected void httpGet2() {

		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		try {
			HttpGet httpGetRequest = new HttpGet("https://api.twitter.com/1.1/search/tweets.json?q=%40apple");
			HttpResponse httpResponse = httpClient.execute(httpGetRequest);

			_log.info("----------------------------------------");
			_log.info(httpResponse.getStatusLine());
			_log.info("----------------------------------------");

			HttpEntity entity = httpResponse.getEntity();

			byte[] buffer = new byte[1024];
			if (entity != null) {
				InputStream inputStream = entity.getContent();
				try {
					int bytesRead = 0;
					BufferedInputStream bis = new BufferedInputStream(inputStream);
					while ((bytesRead = bis.read(buffer)) != -1) {
						String chunk = new String(buffer, 0, bytesRead);
						_log.info(chunk);
					}
				} catch (Exception e) {
					_log.error(e);
				} finally {
					try {
						inputStream.close();
					} catch (Exception ignore) {
					}
				}
			}
		} catch (Exception e) {
			_log.error(e);
		} finally {
			try {
				httpClient.close();
			} catch (IOException e) {
				_log.error(e);
			}
		}
	}

	protected void httpGet3() {
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		try {
			// specify the host, protocol, and port
			HttpHost target = new HttpHost("services.groupkt.com", 80, "http");

			// specify the get request
			HttpGet getRequest = new HttpGet("/state/get/IND/UP");

			_log.info("executing request to " + target);

			HttpResponse httpResponse = httpClient.execute(target, getRequest);
			HttpEntity entity = httpResponse.getEntity();

			_log.info("----------------------------------------");
			_log.info(httpResponse.getStatusLine());
			Header[] headers = httpResponse.getAllHeaders();
			for (int i = 0; i < headers.length; i++) {
				_log.info(headers[i]);
			}
			_log.info("----------------------------------------");

			if (entity != null) {
				_log.info(EntityUtils.toString(entity));
			}

		} catch (Exception e) {
			_log.error(e);
		} finally {
			// When HttpClient instance is no longer needed,
			// shut down the connection manager to ensure
			// immediate deallocation of all system resources
			try {
				httpClient.close();
			} catch (IOException e) {
				_log.error(e);
			}
		}
	}

	protected void httpPost1() {
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		try {
			// specify the host, protocol, and port
			HttpHost target = new HttpHost("opensesame.ou.nl", 80, PropsValues.STREAMING_SERVER_STREAMING_PROTOCOL);

			// specify the post request
			HttpPost httpPost = new HttpPost("/rest/system/connect.json");
			httpPost.setHeader("Content-type", "application/json");
			httpPost.setHeader("Authorization: Basic", "VmVnOlZlZ19XaXM=");
			_log.info("executing post request to " + target);

			HttpResponse httpResponse = httpClient.execute(target, httpPost);
			HttpEntity entity = httpResponse.getEntity();

			_log.info("----------------------------------------");
			_log.info(httpResponse.getStatusLine());
			Header[] headers = httpResponse.getAllHeaders();
			for (int i = 0; i < headers.length; i++) {
				_log.info(headers[i]);
			}
			_log.info("----------------------------------------");

			if (entity != null) {
				_log.info(EntityUtils.toString(entity));
			}

		} catch (Exception e) {
			_log.error(e);
		} finally {
			// When HttpClient instance is no longer needed,
			// shut down the connection manager to ensure
			// immediate deallocation of all system resources
			try {
				httpClient.close();
			} catch (IOException e) {
				_log.error(e);
			}
		}
	}

	protected String executePost1(String targetURL, String urlParameters) {
		HttpURLConnection connection = null;

		try {
			// Create connection
			URL url = new URL(targetURL);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("Authorization: Basic", "VmVnOlZlZ19XaXM=");

			connection.setRequestProperty("Content-Length", Integer.toString(urlParameters.getBytes().length));
			connection.setRequestProperty("Content-Language", "en-US");

			connection.setUseCaches(false);
			connection.setDoOutput(true);

			// Send request
			DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.close();

			// Get Response
			InputStream is = connection.getInputStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			StringBuilder response = new StringBuilder(); // or StringBuffer if
															// Java version 5+
			String line;
			while ((line = rd.readLine()) != null) {
				response.append(line);
				response.append('\r');
			}
			rd.close();
			return response.toString();
		} catch (Exception e) {
			_log.error(e);
			return null;
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
	}

}