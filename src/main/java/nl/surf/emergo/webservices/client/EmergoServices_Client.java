/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */

package nl.surf.emergo.webservices.client;

/**
 * Please modify this class to meet your needs
 * This class is not complete
 */

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This class was generated by Apache CXF 3.1.1 2015-08-17T12:04:26.844+02:00
 * Generated source version: 3.1.1
 * 
 */
public final class EmergoServices_Client {
	private static final Logger _log = LogManager.getLogger(EmergoServices_Client.class);
	private static final QName SERVICE_NAME = new QName("http://webservices.emergo.surf.nl/", "EmergoServices");

	private static EmergoServices_Service emservice = null;
	private static EmergoServices emserviceport = null;

	public EmergoServices_Client() {
	}

	public EmergoServices_Client(String args[]) {
		if (emservice == null) {
			URL wsdlURL = EmergoServices_Service.WSDL_LOCATION;
			if (args.length > 0 && args[0] != null && !"".equals(args[0])) {
				File wsdlFile = new File(args[0]);
				try {
					if (wsdlFile.exists()) {
						wsdlURL = wsdlFile.toURI().toURL();
					} else {
						wsdlURL = new URL(args[0]);
					}
				} catch (MalformedURLException e) {
					_log.error(e);
				}
			}
			emservice = new EmergoServices_Service(wsdlURL, SERVICE_NAME);
			emserviceport = emservice.getEmergoServicesEndpointPort();
		}
	}

	public static void test(String args[]) throws java.lang.Exception {

		{
			_log.info("Invoking getContext...");
			java.lang.String _getContext_arg0 = "_getContext_arg0-60572244";
			java.lang.String _getContext_arg1 = "_getContext_arg1-639958368";
			java.lang.String _getContext__return = emserviceport.getContext(_getContext_arg0, _getContext_arg1);
			_log.info("getContext.result=" + _getContext__return);

		}
		{
			_log.info("Invoking getCurrentExlocations...");
			java.lang.String _getCurrentExlocations_arg0 = "_getCurrentExlocations_arg0-1718554456";
			java.util.List<nl.surf.emergo.webservices.client.Exlocation> _getCurrentExlocations__return = emserviceport
					.getCurrentExlocations(_getCurrentExlocations_arg0);
			_log.info("getCurrentExlocations.result=" + _getCurrentExlocations__return);

		}
		{
			_log.info("Invoking getAllUrls...");
			java.lang.String _getAllUrls_arg0 = "_getAllUrls_arg0-771952614";
			java.util.List<nl.surf.emergo.webservices.client.Url> _getAllUrls__return = emserviceport
					.getAllUrls(_getAllUrls_arg0);
			_log.info("getAllUrls.result=" + _getAllUrls__return);

		}
		{
			_log.info("Invoking getExlocationsByPosition...");
			java.lang.String _getExlocationsByPosition_arg0 = "_getExlocationsByPosition_arg0-1748192363";
			java.lang.String _getExlocationsByPosition_arg1 = "_getExlocationsByPosition_arg1-1623436638";
			java.lang.String _getExlocationsByPosition_arg2 = "_getExlocationsByPosition_arg21616796855";
			java.util.List<nl.surf.emergo.webservices.client.Exlocation> _getExlocationsByPosition__return = emserviceport
					.getExlocationsByPosition(_getExlocationsByPosition_arg0, _getExlocationsByPosition_arg1,
							_getExlocationsByPosition_arg2);
			_log.info("getExlocationsByPosition.result=" + _getExlocationsByPosition__return);

		}
		{
			_log.info("Invoking getCurrentUrls...");
			java.lang.String _getCurrentUrls_arg0 = "_getCurrentUrls_arg0-2063774863";
			java.util.List<nl.surf.emergo.webservices.client.Url> _getCurrentUrls__return = emserviceport
					.getCurrentUrls(_getCurrentUrls_arg0);
			_log.info("getCurrentUrls.result=" + _getCurrentUrls__return);

		}
		{
			_log.info("Invoking getAllExlocations...");
			java.lang.String _getAllExlocations_arg0 = "_getAllExlocations_arg01954719257";
			java.util.List<nl.surf.emergo.webservices.client.Exlocation> _getAllExlocations__return = emserviceport
					.getAllExlocations(_getAllExlocations_arg0);
			_log.info("getAllExlocations.result=" + _getAllExlocations__return);

		}

		// System.exit(0);
	}

	public static nl.surf.emergo.webservices.client.WState getStateByKey(String aRgaId, String aKey)
			throws java.lang.Exception {
		_log.info("Invoking getStateByKey...");
		nl.surf.emergo.webservices.client.WState _getStateByKey__return = emserviceport.getStateByKey(aRgaId, aKey);
		_log.info("getStateByKey.result=" + _getStateByKey__return);
		return _getStateByKey__return;
	}

	public String setStateByKey(String aRgaId, String aKey, String aValue) throws java.lang.Exception {
		_log.info("Invoking setStateByKey...");
		String _setStateByKey__return = emserviceport.setStateByKey(aRgaId, aKey, aValue);
		_log.info("setStateByKey.result=" + _setStateByKey__return);
		return _setStateByKey__return;
	}

	public static java.util.List<nl.surf.emergo.webservices.client.WRun> getRuns(String aUserId, String aPassword)
			throws java.lang.Exception {
		_log.info("Invoking getRuns...");
		java.util.List<nl.surf.emergo.webservices.client.WRun> _getRuns__return = emserviceport.getRuns(aUserId,
				aPassword);
		_log.info("getRuns.result=" + _getRuns__return);
		return _getRuns__return;
	}

	public static java.util.List<nl.surf.emergo.webservices.client.WRun> getRunsForCRM(String aUserId, String aPassword)
			throws java.lang.Exception {
		_log.info("Invoking getRunsForCRM...");
		java.util.List<nl.surf.emergo.webservices.client.WRun> _getRunsForCRM__return = emserviceport
				.getRunsForCRM(aUserId, aPassword);
		_log.info("getRunsForCRM.result=" + _getRunsForCRM__return);
		return _getRunsForCRM__return;
	}

	public static java.util.List<nl.surf.emergo.webservices.client.WState> getAllStates(String aRgaId)
			throws java.lang.Exception {
		_log.info("Invoking getAllStates...");
		java.util.List<nl.surf.emergo.webservices.client.WState> _getAllStates__return = emserviceport
				.getAllStates(aRgaId);
		_log.info("getAllStates.result=" + _getAllStates__return);
		return _getAllStates__return;
	}

	public static String playMovie(String aCase, String aStudentId, String aMovie) throws java.lang.Exception {
		_log.info("Invoking playMovie...");
		String _playMovie__return = emserviceport.playMovie(aCase, aStudentId, aMovie);
		_log.info("playMovie.result=" + _playMovie__return);
		return _playMovie__return;
	}

	/**
	 * Sets context.
	 *
	 * @param aRgaId      the rga id
	 * @param aSensorData the sensor data
	 *
	 * @return result string
	 *
	 * @throws Exception the exception
	 */
	public static String setContext(String aRgaId, String aSensorData) throws java.lang.Exception {
		_log.info("Invoking setContext...");
		String _setContext__return = emserviceport.setContext(aRgaId, aSensorData);
		_log.info("setContext.result=" + _setContext__return);
		return _setContext__return;
	}

	public static String setContexts(String aRgaId, String aSensorData) throws java.lang.Exception {
		_log.info("Invoking setContext...");
		String _setContext__return = emserviceport.setContext(aRgaId, aSensorData);
		_log.info("setContext.result=" + _setContext__return);
		return _setContext__return;
	}

	/**
	 * Gets the urls by position.
	 *
	 * @param aRgaId     the a rga id
	 * @param aLatitude  the a latitude
	 * @param aLongitude the a longitude
	 *
	 * @return the urls by position
	 *
	 * @throws Exception the exception
	 */
	public static java.util.List<nl.surf.emergo.webservices.client.Url> getUrlsByPosition(String aRgaId,
			String aLatitude, String aLongitude) throws java.lang.Exception {
		_log.info("Invoking getUrlsByPosition...");
		java.util.List<nl.surf.emergo.webservices.client.Url> _getUrlsByPosition__return = emserviceport
				.getUrlsByPosition(aRgaId, aLatitude, aLongitude);
		_log.info("getUrlsByPosition.result=" + _getUrlsByPosition__return);
		return _getUrlsByPosition__return;
	}

	/**
	 * Gets the urls by name.
	 *
	 * @param aRgaId the rga id
	 * @param aName  the name
	 *
	 * @return the urls by name
	 *
	 * @throws Exception the exception
	 */
	public static java.util.List<nl.surf.emergo.webservices.client.Url> getUrlsByName(String aRgaId, String aName)
			throws java.lang.Exception {
		_log.info("Invoking getUrlsByName...");
		java.util.List<nl.surf.emergo.webservices.client.Url> _getUrlsByName__return = emserviceport
				.getUrlsByName(aRgaId, aName);
		_log.info("getUrlsByName.result=" + _getUrlsByName__return);
		return _getUrlsByName__return;
	}

	/**
	 * Sets url.
	 *
	 * @param aRgaId the a rga id
	 * @param aUrl   the a url
	 *
	 * @return the string
	 *
	 * @throws Exception the exception
	 */
	public static String setUrl(String aRgaId, nl.surf.emergo.webservices.client.Url aUrl) throws java.lang.Exception {
		_log.info("Invoking setUrl...");
		java.lang.String _setUrl__return = emserviceport.setUrl(aRgaId, aUrl);
		_log.info("setUrl.result=" + _setUrl__return);
		return _setUrl__return;
	}

	/**
	 * Deletes url.
	 *
	 * @param aRgaId the a rga id
	 * @param aUrl   the a url
	 *
	 * @return string
	 *
	 * @throws Exception the exception
	 */
	public static String deleteUrl(String aRgaId, nl.surf.emergo.webservices.client.Url aUrl)
			throws java.lang.Exception {
		_log.info("Invoking deleteUrl...");
		java.lang.String _deleteUrl__return = emserviceport.deleteUrl(aRgaId, aUrl);
		_log.info("deleteUrl.result=" + _deleteUrl__return);
		return _deleteUrl__return;
	}

	/**
	 * Gets the case component data.
	 *
	 * @param aRgaId             the a rga id
	 * @param aCaseComponentName the a case component name
	 *
	 * @return the case component data
	 *
	 * @throws Exception the exception
	 */
	public static String getCaseComponentData(String aRgaId, String aCaseComponentName) throws java.lang.Exception {
		_log.info("Invoking getCaseComponentData...");
		java.lang.String _return = emserviceport.getCaseComponentData(aRgaId, aCaseComponentName);
		_log.info("getCaseComponentData.result=" + _return);
		return _return;
	}

}
