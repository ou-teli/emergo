/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */

package nl.surf.emergo.webservices.client.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import javax.net.ssl.HttpsURLConnection;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.concurrent.FutureCallback;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.json.parser.JSONParser;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.utilities.PropsValues;
import nl.surf.emergo.view.VView;

public class TestsClient {
	private static final Logger _log = LogManager.getLogger(TestsClient.class);
	protected VView vView = CDesktopComponents.vView();

	public void httpGetExample() {
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		try {
			// specify the host, protocol, and port
			HttpHost target = new HttpHost("services.groupkt.com", 80, "http");

			// specify the get request
			HttpGet getRequest = new HttpGet("/state/get/IND/UP");

			_log.info("executing request to " + target);

			HttpResponse httpResponse = httpClient.execute(target, getRequest);
			HttpEntity entity = httpResponse.getEntity();

			Header[] headers = httpResponse.getAllHeaders();
			for (int i = 0; i < headers.length; i++) {
				_log.info(headers[i]);
			}
			_log.info("----------------------------------------");

			if (entity != null) {
				_log.info(EntityUtils.toString(entity));
			}

		} catch (Exception e) {
			_log.error(e);
		} finally {
			// When HttpClient instance is no longer needed,
			// shut down the connection manager to ensure
			// immediate deallocation of all system resources
			try {
				httpClient.close();
			} catch (IOException e) {
				_log.error(e);
			}
		}
	}

	public HttpHost getHttpHost() {
		// specify the host, protocol, and port
		return new HttpHost(PropsValues.TESTS_SERVER_NAME, 80, "http");
	}

	protected void setHttpGetHeader(HttpGet httpGet) {
		httpGet.setHeader("Content-type", "application/json");
		String authStr = PropsValues.TESTS_SERVER_USER + ":" + PropsValues.TESTS_SERVER_PASSWORD;
		String base64AuthStr;
		try {
			base64AuthStr = Base64.getEncoder().encodeToString(authStr.getBytes("utf-8"));
			httpGet.setHeader("Authorization", "Basic " + base64AuthStr);
		} catch (UnsupportedEncodingException e) {
		}
	}

	public Hashtable<String, String> getTestsData() {
		Hashtable<String, String> result = new Hashtable<String, String>();
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		try {
			HttpHost target = getHttpHost();

			// specify the get request
			HttpGet httpGet = new HttpGet(PropsValues.TESTS_SERVER_REST_CALL_GETTESTSDATA);
			setHttpGetHeader(httpGet);

			HttpResponse httpResponse = httpClient.execute(target, httpGet);
			HttpEntity entity = httpResponse.getEntity();

			if (entity != null) {
				// NOTE return url that is within the returned json string
				String json = new String(EntityUtils.toString(entity));
				JSONParser jsonParser = new JSONParser();
				JSONObject jsonObject = (JSONObject) jsonParser.parse(json);
				jsonObject = (JSONObject) jsonObject.get("os_guestpass");
				result.put("testsGroupId", (String) jsonObject.get("gid"));
				result.put("testsUrl",
						"http://" + PropsValues.TESTS_SERVER_NAME + "/" + (String) jsonObject.get("link"));
				return result;
			}
		} catch (Exception e) {
			_log.error(e);
		} finally {
			// When HttpClient instance is no longer needed,
			// shut down the connection manager to ensure
			// immediate deallocation of all system resources
			try {
				httpClient.close();
			} catch (IOException e) {
				_log.error(e);
			}
		}
		return result;
	}

	public Hashtable<String, String> getTestsDataAsync() {
		final Hashtable<String, String> result = new Hashtable<String, String>();
		CloseableHttpAsyncClient httpClient = HttpAsyncClients.createDefault();
		try {
			HttpHost target = getHttpHost();

			// specify the get request
			HttpGet httpGet = new HttpGet(PropsValues.TESTS_SERVER_REST_CALL_GETTESTSDATA);
			setHttpGetHeader(httpGet);

			final CountDownLatch latch = new CountDownLatch(1);
			httpClient.execute(target, httpGet,

					new FutureCallback<HttpResponse>() {

						@Override
						public void completed(final HttpResponse httpResponse) {
							latch.countDown();
							HttpEntity entity = httpResponse.getEntity();

							if (entity != null) {
								try {
									// NOTE return url that is within the returned json string
									String json = new String(EntityUtils.toString(entity));
									JSONParser jsonParser = new JSONParser();
									JSONObject jsonObject = (JSONObject) jsonParser.parse(json);
									jsonObject = (JSONObject) jsonObject.get("os_guestpass");
									result.clear();
									result.put("testsGroupId", (String) jsonObject.get("gid"));
									result.put("testsUrl", "http://" + PropsValues.TESTS_SERVER_NAME + "/"
											+ (String) jsonObject.get("link"));
								} catch (ParseException e) {
									_log.error(e);
								} catch (org.zkoss.json.parser.ParseException e) {
									_log.error(e);
								} catch (IOException e) {
									_log.error(e);
								}
							}
						}

						@Override
						public void failed(final Exception ex) {
							latch.countDown();
							// _log.info(request2.getRequestLine() +
							// "->" + ex);
						}

						@Override
						public void cancelled() {
							latch.countDown();
							// _log.info(request2.getRequestLine() +
							// " cancelled");
						}

					});
			_log.info("before latch.await()");
			latch.await();
			_log.info("after latch.await()");
		} catch (Exception e) {
			_log.error(e);
		} finally {
			// When HttpClient instance is no longer needed,
			// shut down the connection manager to ensure
			// immediate deallocation of all system resources
			try {
				httpClient.close();
			} catch (IOException e) {
				_log.error(e);
			}
		}
		return result;
	}

	public Hashtable<String, String> getTestsUserData(String testsUrl) {
		Hashtable<String, String> result = new Hashtable<String, String>();
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		try {
			HttpHost target = getHttpHost();

			// specify the get request
			String url = testsUrl;
			int pos = url.lastIndexOf("/");
			if (pos >= 0 && pos < (url.length() - 1)) {
				url = url.substring(pos + 1);
			}
			HttpGet httpGet = new HttpGet(PropsValues.TESTS_SERVER_REST_CALL_GETTESTSUSERDATA.replace("[$url$]", url));
			setHttpGetHeader(httpGet);

			HttpResponse httpResponse = httpClient.execute(target, httpGet);
			HttpEntity entity = httpResponse.getEntity();

			if (entity != null) {
				// NOTE return url that is within the returned json string
				String json = new String(EntityUtils.toString(entity));
				if (json != null && !json.equals("") && !json.equals("null")) {
					JSONParser jsonParser = new JSONParser();
					JSONObject jsonObject = (JSONObject) jsonParser.parse(json);
					testsUrl = (String) jsonObject.get("guest_link");
					if (testsUrl.indexOf(url) >= 0) {
						result.put("userId", (String) jsonObject.get("guest_uid"));
						result.put("timeCreated", (String) jsonObject.get("created"));
						return result;
					}
				}
				return result;
			}
		} catch (Exception e) {
			_log.error(e);
		} finally {
			// When HttpClient instance is no longer needed,
			// shut down the connection manager to ensure
			// immediate deallocation of all system resources
			try {
				httpClient.close();
			} catch (IOException e) {
				_log.error(e);
			}
		}
		return result;
	}

	public Hashtable<String, String> getTests(String testsGroupId) {
		Hashtable<String, String> result = new Hashtable<String, String>();
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		try {
			HttpHost target = getHttpHost();

			// specify the get request
			HttpGet httpGet = new HttpGet(
					PropsValues.TESTS_SERVER_REST_CALL_GETTESTS.replace("[$testsgroupid$]", testsGroupId));
			setHttpGetHeader(httpGet);

			HttpResponse httpResponse = httpClient.execute(target, httpGet);
			HttpEntity entity = httpResponse.getEntity();

			if (entity != null) {
				// NOTE return url that is within the returned json string
				String json = new String(EntityUtils.toString(entity));
				if (json != null && !json.equals("") && !json.equals("null") && !json.equals("[]")) {
					JSONParser jsonParser = new JSONParser();
					JSONArray jsonArray = (JSONArray) jsonParser.parse(json);
					for (int i = 0; i < jsonArray.size(); i++) {
						JSONObject jsonObject = (JSONObject) jsonArray.get(i);
						result.put((String) jsonObject.get("type"), (String) jsonObject.get("nid"));
					}
					return result;
				}
				return result;
			}
		} catch (Exception e) {
			_log.error(e);
		} finally {
			// When HttpClient instance is no longer needed,
			// shut down the connection manager to ensure
			// immediate deallocation of all system resources
			try {
				httpClient.close();
			} catch (IOException e) {
				_log.error(e);
			}
		}
		return result;
	}

	public String getTestsResult(String testsGroupId, String userId) {
		if (testsGroupId.equals("") || userId.equals("")) {
			return "";
		}
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		try {
			HttpHost target = getHttpHost();

			// specify the get request
			HttpGet httpGet = new HttpGet(PropsValues.TESTS_SERVER_REST_CALL_GETTESTSRESULT
					.replace("[$testsgroupid$]", testsGroupId).replace("[$userid$]", userId));
			setHttpGetHeader(httpGet);

			HttpResponse httpResponse = httpClient.execute(target, httpGet);
			HttpEntity entity = httpResponse.getEntity();

			if (entity != null) {
				// NOTE return url that is within the returned json string
				String json = new String(EntityUtils.toString(entity));
				if (json != null && !json.equals("") && !json.equals("null") && !json.equals("[]")) {
					// TODO return correct json result
					return json;
				}
			}
		} catch (Exception e) {
			_log.error(e);
		} finally {
			// When HttpClient instance is no longer needed,
			// shut down the connection manager to ensure
			// immediate deallocation of all system resources
			try {
				httpClient.close();
			} catch (IOException e) {
				_log.error(e);
			}
		}
		return "";
	}

	public String getTestUrlNew() {
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		try {
			// specify the host, protocol, and port
			HttpHost target = new HttpHost(PropsValues.TESTS_SERVER_NAME, 80, "http");

			// specify the post request
			HttpGet httpGet = new HttpGet(PropsValues.TESTS_SERVER_REST_CALL_URL);
			httpGet.setHeader("Content-type", "application/json");
			String authStr = PropsValues.TESTS_SERVER_USER + ":" + PropsValues.TESTS_SERVER_PASSWORD;
			String base64AuthStr = Base64.getEncoder().encodeToString(authStr.getBytes("utf-8"));
			httpGet.setHeader("Authorization", "Basic " + base64AuthStr);

			ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

				@Override
				public String handleResponse(final HttpResponse response) throws IOException {
					int status = response.getStatusLine().getStatusCode();
					if (status >= 200 && status < 300) {
						HttpEntity entity = response.getEntity();
						if (entity != null) {
							// NOTE return url that is within the returned json
							// string
							String json = new String(EntityUtils.toString(entity));
							JSONParser jsonParser = new JSONParser();
							JSONObject jsonObject = (JSONObject) jsonParser.parse(json);
							jsonObject = (JSONObject) jsonObject.get("os_guestpass");
							String link = (String) jsonObject.get("link");
							return "http://" + PropsValues.TESTS_SERVER_NAME + "/" + link;
						}
						return "";
					} else {
						throw new ClientProtocolException("Unexpected response status: " + status);
					}
				}

			};

			String responseBody = httpClient.execute(target, httpGet, responseHandler);
			_log.info("----------------------------------------");
			_log.info(responseBody);
		} catch (Exception e) {
			_log.error(e);
		} finally {
			// When HttpClient instance is no longer needed,
			// shut down the connection manager to ensure
			// immediate deallocation of all system resources
			try {
				httpClient.close();
			} catch (IOException e) {
				_log.error(e);
			}
		}
		return "";
	}

	public String getTestUrlNew2() {
		CloseableHttpAsyncClient httpClient = HttpAsyncClients.createDefault();
		try {
			// specify the host, protocol, and port
			HttpHost target = new HttpHost(PropsValues.TESTS_SERVER_NAME, 80, "http");

			// specify the post request
			HttpGet httpGet = new HttpGet(PropsValues.TESTS_SERVER_REST_CALL_URL);
			httpGet.setHeader("Content-type", "application/json");
			String authStr = PropsValues.TESTS_SERVER_USER + ":" + PropsValues.TESTS_SERVER_PASSWORD;
			String base64AuthStr = Base64.getEncoder().encodeToString(authStr.getBytes("utf-8"));
			httpGet.setHeader("Authorization", "Basic " + base64AuthStr);

			final CountDownLatch latch = new CountDownLatch(1);
			httpClient.execute(target, httpGet,

					new FutureCallback<HttpResponse>() {

						@Override
						public void completed(final HttpResponse response) {
							latch.countDown();
							HttpEntity entity = response.getEntity();
							if (entity != null) {
								// NOTE return url that is within the returned
								// json string
								try {
									String json = new String(EntityUtils.toString(entity));
									JSONParser jsonParser = new JSONParser();
									JSONObject jsonObject = (JSONObject) jsonParser.parse(json);
									jsonObject = (JSONObject) jsonObject.get("os_guestpass");
									String link = (String) jsonObject.get("link");
									_log.info("http://" + PropsValues.TESTS_SERVER_NAME + "/" + link);
								} catch (ParseException e) {
									_log.error(e);
								} catch (org.zkoss.json.parser.ParseException e) {
									_log.error(e);
								} catch (IOException e) {
									_log.error(e);
								}
							}
						}

						@Override
						public void failed(final Exception ex) {
							latch.countDown();
							// _log.info(request2.getRequestLine() +
							// "->" + ex);
						}

						@Override
						public void cancelled() {
							latch.countDown();
							// _log.info(request2.getRequestLine() +
							// " cancelled");
						}

					});
			_log.info("before latch.await()");
			latch.await();
			_log.info("after latch.await()");
		} catch (Exception e) {
			_log.error(e);
		} finally {
			// When HttpClient instance is no longer needed,
			// shut down the connection manager to ensure
			// immediate deallocation of all system resources
			try {
				httpClient.close();
			} catch (IOException e) {
				_log.error(e);
			}
		}
		return "";
	}

	protected List<String[]> getGameSessieData(String groupId) {
		List<String[]> result = new ArrayList<String[]>();

		HttpsURLConnection connection = null;

		try {

			String httpsURL = "";
			if (groupId == null || groupId.equals("")) {
				httpsURL = "https://docs.google.com/spreadsheets/d/1htHXHjn6cAs94oebWrYXeqjfeEhvi_QzzlsM9-RQJu4/gviz/tq?tq=select%20B%2CC%2CD%2CE%2CF%2CN";
			} else {
				httpsURL = "https://docs.google.com/spreadsheets/d/1htHXHjn6cAs94oebWrYXeqjfeEhvi_QzzlsM9-RQJu4/gviz/tq?tq=select%20B%2CC%2CD%2CE%2CF%2CN%20where%20D%20%3D%20'"
						+ groupId + "'";
			}
			URL myurl = new URL(httpsURL);
			HttpsURLConnection con = (HttpsURLConnection) myurl.openConnection();

			InputStream ins = con.getInputStream();
			InputStreamReader isr = new InputStreamReader(ins);
			BufferedReader in = new BufferedReader(isr);
			StringBuffer response = new StringBuffer();
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
				response.append('\r');
			}
			in.close();
			if (response.length() > 0) {
				String startStr = "google.visualization.Query.setResponse(";
				String endStr = ");";
				int start = response.indexOf(startStr);
				int end = response.lastIndexOf(endStr);
				if (start >= 0 && end >= 0) {
					String json = response.substring(start + startStr.length(), end);
					JSONParser jsonParser = new JSONParser();
					JSONObject jsonObject = (JSONObject) jsonParser.parse(json);
					jsonObject = (JSONObject) jsonObject.get("table");
					JSONArray jsonArray = (JSONArray) jsonObject.get("rows");
					// spl_id, name, group, session, level, ROC
					for (int i = 0; i < jsonArray.size(); i++) {
						JSONObject jsonObject2 = (JSONObject) jsonArray.get(i);
						JSONArray jsonArray2 = (JSONArray) jsonObject2.get("c");
						String[] resultArr = new String[6];
						for (int j = 0; j < jsonArray2.size(); j++) {
							JSONObject jsonObject3 = (JSONObject) jsonArray2.get(j);
							if (j == 3 || j == 4) {
								resultArr[j] = "0";
								if (jsonObject3 != null && jsonObject3.get("f") != null) {
									resultArr[j] = (String) jsonObject3.get("f");
								}
							} else {
								resultArr[j] = "";
								if (jsonObject3 != null && jsonObject3.get("v") != null) {
									resultArr[j] = (String) jsonObject3.get("v");
								}
							}
						}
						result.add(resultArr);
					}
				}
			}
			return result;
		} catch (Exception e) {
			_log.error(e);
			return result;
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
	}

}