/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */

package nl.surf.emergo.webservices.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the nl.surf.emergo.webservices.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DeleteUrl_QNAME = new QName("http://webservices.emergo.surf.nl/", "deleteUrl");
    private final static QName _DeleteUrlResponse_QNAME = new QName("http://webservices.emergo.surf.nl/", "deleteUrlResponse");
    private final static QName _GetAllExlocations_QNAME = new QName("http://webservices.emergo.surf.nl/", "getAllExlocations");
    private final static QName _GetAllExlocationsResponse_QNAME = new QName("http://webservices.emergo.surf.nl/", "getAllExlocationsResponse");
    private final static QName _GetAllStates_QNAME = new QName("http://webservices.emergo.surf.nl/", "getAllStates");
    private final static QName _GetAllStatesResponse_QNAME = new QName("http://webservices.emergo.surf.nl/", "getAllStatesResponse");
    private final static QName _GetAllUrls_QNAME = new QName("http://webservices.emergo.surf.nl/", "getAllUrls");
    private final static QName _GetAllUrlsResponse_QNAME = new QName("http://webservices.emergo.surf.nl/", "getAllUrlsResponse");
    private final static QName _GetCaseComponentData_QNAME = new QName("http://webservices.emergo.surf.nl/", "getCaseComponentData");
    private final static QName _GetCaseComponentDataResponse_QNAME = new QName("http://webservices.emergo.surf.nl/", "getCaseComponentDataResponse");
    private final static QName _GetContext_QNAME = new QName("http://webservices.emergo.surf.nl/", "getContext");
    private final static QName _GetContextResponse_QNAME = new QName("http://webservices.emergo.surf.nl/", "getContextResponse");
    private final static QName _GetCurrentExlocations_QNAME = new QName("http://webservices.emergo.surf.nl/", "getCurrentExlocations");
    private final static QName _GetCurrentExlocationsResponse_QNAME = new QName("http://webservices.emergo.surf.nl/", "getCurrentExlocationsResponse");
    private final static QName _GetCurrentUrls_QNAME = new QName("http://webservices.emergo.surf.nl/", "getCurrentUrls");
    private final static QName _GetCurrentUrlsResponse_QNAME = new QName("http://webservices.emergo.surf.nl/", "getCurrentUrlsResponse");
    private final static QName _GetExlocationsByPosition_QNAME = new QName("http://webservices.emergo.surf.nl/", "getExlocationsByPosition");
    private final static QName _GetExlocationsByPositionResponse_QNAME = new QName("http://webservices.emergo.surf.nl/", "getExlocationsByPositionResponse");
    private final static QName _GetRuns_QNAME = new QName("http://webservices.emergo.surf.nl/", "getRuns");
    private final static QName _GetRunsForCRM_QNAME = new QName("http://webservices.emergo.surf.nl/", "getRunsForCRM");
    private final static QName _GetRunsForCRMResponse_QNAME = new QName("http://webservices.emergo.surf.nl/", "getRunsForCRMResponse");
    private final static QName _GetRunsResponse_QNAME = new QName("http://webservices.emergo.surf.nl/", "getRunsResponse");
    private final static QName _GetStateByKey_QNAME = new QName("http://webservices.emergo.surf.nl/", "getStateByKey");
    private final static QName _GetStateByKeyResponse_QNAME = new QName("http://webservices.emergo.surf.nl/", "getStateByKeyResponse");
    private final static QName _GetUrlsByName_QNAME = new QName("http://webservices.emergo.surf.nl/", "getUrlsByName");
    private final static QName _GetUrlsByNameResponse_QNAME = new QName("http://webservices.emergo.surf.nl/", "getUrlsByNameResponse");
    private final static QName _GetUrlsByPosition_QNAME = new QName("http://webservices.emergo.surf.nl/", "getUrlsByPosition");
    private final static QName _GetUrlsByPositionResponse_QNAME = new QName("http://webservices.emergo.surf.nl/", "getUrlsByPositionResponse");
    private final static QName _PlayMovie_QNAME = new QName("http://webservices.emergo.surf.nl/", "playMovie");
    private final static QName _PlayMovieResponse_QNAME = new QName("http://webservices.emergo.surf.nl/", "playMovieResponse");
    private final static QName _SetContext_QNAME = new QName("http://webservices.emergo.surf.nl/", "setContext");
    private final static QName _SetContextResponse_QNAME = new QName("http://webservices.emergo.surf.nl/", "setContextResponse");
    private final static QName _SetStateByKey_QNAME = new QName("http://webservices.emergo.surf.nl/", "setStateByKey");
    private final static QName _SetStateByKeyResponse_QNAME = new QName("http://webservices.emergo.surf.nl/", "setStateByKeyResponse");
    private final static QName _SetUrl_QNAME = new QName("http://webservices.emergo.surf.nl/", "setUrl");
    private final static QName _SetUrlResponse_QNAME = new QName("http://webservices.emergo.surf.nl/", "setUrlResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: nl.surf.emergo.webservices.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DeleteUrl }
     * 
     */
    public DeleteUrl createDeleteUrl() {
        return new DeleteUrl();
    }

    /**
     * Create an instance of {@link DeleteUrlResponse }
     * 
     */
    public DeleteUrlResponse createDeleteUrlResponse() {
        return new DeleteUrlResponse();
    }

    /**
     * Create an instance of {@link GetAllExlocations }
     * 
     */
    public GetAllExlocations createGetAllExlocations() {
        return new GetAllExlocations();
    }

    /**
     * Create an instance of {@link GetAllExlocationsResponse }
     * 
     */
    public GetAllExlocationsResponse createGetAllExlocationsResponse() {
        return new GetAllExlocationsResponse();
    }

    /**
     * Create an instance of {@link GetAllStates }
     * 
     */
    public GetAllStates createGetAllStates() {
        return new GetAllStates();
    }

    /**
     * Create an instance of {@link GetAllStatesResponse }
     * 
     */
    public GetAllStatesResponse createGetAllStatesResponse() {
        return new GetAllStatesResponse();
    }

    /**
     * Create an instance of {@link GetAllUrls }
     * 
     */
    public GetAllUrls createGetAllUrls() {
        return new GetAllUrls();
    }

    /**
     * Create an instance of {@link GetAllUrlsResponse }
     * 
     */
    public GetAllUrlsResponse createGetAllUrlsResponse() {
        return new GetAllUrlsResponse();
    }

    /**
     * Create an instance of {@link GetCaseComponentData }
     * 
     */
    public GetCaseComponentData createGetCaseComponentData() {
        return new GetCaseComponentData();
    }

    /**
     * Create an instance of {@link GetCaseComponentDataResponse }
     * 
     */
    public GetCaseComponentDataResponse createGetCaseComponentDataResponse() {
        return new GetCaseComponentDataResponse();
    }

    /**
     * Create an instance of {@link GetContext }
     * 
     */
    public GetContext createGetContext() {
        return new GetContext();
    }

    /**
     * Create an instance of {@link GetContextResponse }
     * 
     */
    public GetContextResponse createGetContextResponse() {
        return new GetContextResponse();
    }

    /**
     * Create an instance of {@link GetCurrentExlocations }
     * 
     */
    public GetCurrentExlocations createGetCurrentExlocations() {
        return new GetCurrentExlocations();
    }

    /**
     * Create an instance of {@link GetCurrentExlocationsResponse }
     * 
     */
    public GetCurrentExlocationsResponse createGetCurrentExlocationsResponse() {
        return new GetCurrentExlocationsResponse();
    }

    /**
     * Create an instance of {@link GetCurrentUrls }
     * 
     */
    public GetCurrentUrls createGetCurrentUrls() {
        return new GetCurrentUrls();
    }

    /**
     * Create an instance of {@link GetCurrentUrlsResponse }
     * 
     */
    public GetCurrentUrlsResponse createGetCurrentUrlsResponse() {
        return new GetCurrentUrlsResponse();
    }

    /**
     * Create an instance of {@link GetExlocationsByPosition }
     * 
     */
    public GetExlocationsByPosition createGetExlocationsByPosition() {
        return new GetExlocationsByPosition();
    }

    /**
     * Create an instance of {@link GetExlocationsByPositionResponse }
     * 
     */
    public GetExlocationsByPositionResponse createGetExlocationsByPositionResponse() {
        return new GetExlocationsByPositionResponse();
    }

    /**
     * Create an instance of {@link GetRuns }
     * 
     */
    public GetRuns createGetRuns() {
        return new GetRuns();
    }

    /**
     * Create an instance of {@link GetRunsForCRM }
     * 
     */
    public GetRunsForCRM createGetRunsForCRM() {
        return new GetRunsForCRM();
    }

    /**
     * Create an instance of {@link GetRunsForCRMResponse }
     * 
     */
    public GetRunsForCRMResponse createGetRunsForCRMResponse() {
        return new GetRunsForCRMResponse();
    }

    /**
     * Create an instance of {@link GetRunsResponse }
     * 
     */
    public GetRunsResponse createGetRunsResponse() {
        return new GetRunsResponse();
    }

    /**
     * Create an instance of {@link GetStateByKey }
     * 
     */
    public GetStateByKey createGetStateByKey() {
        return new GetStateByKey();
    }

    /**
     * Create an instance of {@link GetStateByKeyResponse }
     * 
     */
    public GetStateByKeyResponse createGetStateByKeyResponse() {
        return new GetStateByKeyResponse();
    }

    /**
     * Create an instance of {@link GetUrlsByName }
     * 
     */
    public GetUrlsByName createGetUrlsByName() {
        return new GetUrlsByName();
    }

    /**
     * Create an instance of {@link GetUrlsByNameResponse }
     * 
     */
    public GetUrlsByNameResponse createGetUrlsByNameResponse() {
        return new GetUrlsByNameResponse();
    }

    /**
     * Create an instance of {@link GetUrlsByPosition }
     * 
     */
    public GetUrlsByPosition createGetUrlsByPosition() {
        return new GetUrlsByPosition();
    }

    /**
     * Create an instance of {@link GetUrlsByPositionResponse }
     * 
     */
    public GetUrlsByPositionResponse createGetUrlsByPositionResponse() {
        return new GetUrlsByPositionResponse();
    }

    /**
     * Create an instance of {@link PlayMovie }
     * 
     */
    public PlayMovie createPlayMovie() {
        return new PlayMovie();
    }

    /**
     * Create an instance of {@link PlayMovieResponse }
     * 
     */
    public PlayMovieResponse createPlayMovieResponse() {
        return new PlayMovieResponse();
    }

    /**
     * Create an instance of {@link SetContext }
     * 
     */
    public SetContext createSetContext() {
        return new SetContext();
    }

    /**
     * Create an instance of {@link SetContextResponse }
     * 
     */
    public SetContextResponse createSetContextResponse() {
        return new SetContextResponse();
    }

    /**
     * Create an instance of {@link SetStateByKey }
     * 
     */
    public SetStateByKey createSetStateByKey() {
        return new SetStateByKey();
    }

    /**
     * Create an instance of {@link SetStateByKeyResponse }
     * 
     */
    public SetStateByKeyResponse createSetStateByKeyResponse() {
        return new SetStateByKeyResponse();
    }

    /**
     * Create an instance of {@link SetUrl }
     * 
     */
    public SetUrl createSetUrl() {
        return new SetUrl();
    }

    /**
     * Create an instance of {@link SetUrlResponse }
     * 
     */
    public SetUrlResponse createSetUrlResponse() {
        return new SetUrlResponse();
    }

    /**
     * Create an instance of {@link WRun }
     * 
     */
    public WRun createWRun() {
        return new WRun();
    }

    /**
     * Create an instance of {@link WRunGroupAccount }
     * 
     */
    public WRunGroupAccount createWRunGroupAccount() {
        return new WRunGroupAccount();
    }

    /**
     * Create an instance of {@link Exlocation }
     * 
     */
    public Exlocation createExlocation() {
        return new Exlocation();
    }

    /**
     * Create an instance of {@link Url }
     * 
     */
    public Url createUrl() {
        return new Url();
    }

    /**
     * Create an instance of {@link WState }
     * 
     */
    public WState createWState() {
        return new WState();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteUrl }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "deleteUrl")
    public JAXBElement<DeleteUrl> createDeleteUrl(DeleteUrl value) {
        return new JAXBElement<DeleteUrl>(_DeleteUrl_QNAME, DeleteUrl.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteUrlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "deleteUrlResponse")
    public JAXBElement<DeleteUrlResponse> createDeleteUrlResponse(DeleteUrlResponse value) {
        return new JAXBElement<DeleteUrlResponse>(_DeleteUrlResponse_QNAME, DeleteUrlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllExlocations }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "getAllExlocations")
    public JAXBElement<GetAllExlocations> createGetAllExlocations(GetAllExlocations value) {
        return new JAXBElement<GetAllExlocations>(_GetAllExlocations_QNAME, GetAllExlocations.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllExlocationsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "getAllExlocationsResponse")
    public JAXBElement<GetAllExlocationsResponse> createGetAllExlocationsResponse(GetAllExlocationsResponse value) {
        return new JAXBElement<GetAllExlocationsResponse>(_GetAllExlocationsResponse_QNAME, GetAllExlocationsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllStates }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "getAllStates")
    public JAXBElement<GetAllStates> createGetAllStates(GetAllStates value) {
        return new JAXBElement<GetAllStates>(_GetAllStates_QNAME, GetAllStates.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllStatesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "getAllStatesResponse")
    public JAXBElement<GetAllStatesResponse> createGetAllStatesResponse(GetAllStatesResponse value) {
        return new JAXBElement<GetAllStatesResponse>(_GetAllStatesResponse_QNAME, GetAllStatesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllUrls }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "getAllUrls")
    public JAXBElement<GetAllUrls> createGetAllUrls(GetAllUrls value) {
        return new JAXBElement<GetAllUrls>(_GetAllUrls_QNAME, GetAllUrls.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllUrlsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "getAllUrlsResponse")
    public JAXBElement<GetAllUrlsResponse> createGetAllUrlsResponse(GetAllUrlsResponse value) {
        return new JAXBElement<GetAllUrlsResponse>(_GetAllUrlsResponse_QNAME, GetAllUrlsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCaseComponentData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "getCaseComponentData")
    public JAXBElement<GetCaseComponentData> createGetCaseComponentData(GetCaseComponentData value) {
        return new JAXBElement<GetCaseComponentData>(_GetCaseComponentData_QNAME, GetCaseComponentData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCaseComponentDataResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "getCaseComponentDataResponse")
    public JAXBElement<GetCaseComponentDataResponse> createGetCaseComponentDataResponse(GetCaseComponentDataResponse value) {
        return new JAXBElement<GetCaseComponentDataResponse>(_GetCaseComponentDataResponse_QNAME, GetCaseComponentDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetContext }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "getContext")
    public JAXBElement<GetContext> createGetContext(GetContext value) {
        return new JAXBElement<GetContext>(_GetContext_QNAME, GetContext.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetContextResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "getContextResponse")
    public JAXBElement<GetContextResponse> createGetContextResponse(GetContextResponse value) {
        return new JAXBElement<GetContextResponse>(_GetContextResponse_QNAME, GetContextResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrentExlocations }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "getCurrentExlocations")
    public JAXBElement<GetCurrentExlocations> createGetCurrentExlocations(GetCurrentExlocations value) {
        return new JAXBElement<GetCurrentExlocations>(_GetCurrentExlocations_QNAME, GetCurrentExlocations.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrentExlocationsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "getCurrentExlocationsResponse")
    public JAXBElement<GetCurrentExlocationsResponse> createGetCurrentExlocationsResponse(GetCurrentExlocationsResponse value) {
        return new JAXBElement<GetCurrentExlocationsResponse>(_GetCurrentExlocationsResponse_QNAME, GetCurrentExlocationsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrentUrls }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "getCurrentUrls")
    public JAXBElement<GetCurrentUrls> createGetCurrentUrls(GetCurrentUrls value) {
        return new JAXBElement<GetCurrentUrls>(_GetCurrentUrls_QNAME, GetCurrentUrls.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrentUrlsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "getCurrentUrlsResponse")
    public JAXBElement<GetCurrentUrlsResponse> createGetCurrentUrlsResponse(GetCurrentUrlsResponse value) {
        return new JAXBElement<GetCurrentUrlsResponse>(_GetCurrentUrlsResponse_QNAME, GetCurrentUrlsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetExlocationsByPosition }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "getExlocationsByPosition")
    public JAXBElement<GetExlocationsByPosition> createGetExlocationsByPosition(GetExlocationsByPosition value) {
        return new JAXBElement<GetExlocationsByPosition>(_GetExlocationsByPosition_QNAME, GetExlocationsByPosition.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetExlocationsByPositionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "getExlocationsByPositionResponse")
    public JAXBElement<GetExlocationsByPositionResponse> createGetExlocationsByPositionResponse(GetExlocationsByPositionResponse value) {
        return new JAXBElement<GetExlocationsByPositionResponse>(_GetExlocationsByPositionResponse_QNAME, GetExlocationsByPositionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRuns }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "getRuns")
    public JAXBElement<GetRuns> createGetRuns(GetRuns value) {
        return new JAXBElement<GetRuns>(_GetRuns_QNAME, GetRuns.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRunsForCRM }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "getRunsForCRM")
    public JAXBElement<GetRunsForCRM> createGetRunsForCRM(GetRunsForCRM value) {
        return new JAXBElement<GetRunsForCRM>(_GetRunsForCRM_QNAME, GetRunsForCRM.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRunsForCRMResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "getRunsForCRMResponse")
    public JAXBElement<GetRunsForCRMResponse> createGetRunsForCRMResponse(GetRunsForCRMResponse value) {
        return new JAXBElement<GetRunsForCRMResponse>(_GetRunsForCRMResponse_QNAME, GetRunsForCRMResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRunsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "getRunsResponse")
    public JAXBElement<GetRunsResponse> createGetRunsResponse(GetRunsResponse value) {
        return new JAXBElement<GetRunsResponse>(_GetRunsResponse_QNAME, GetRunsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStateByKey }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "getStateByKey")
    public JAXBElement<GetStateByKey> createGetStateByKey(GetStateByKey value) {
        return new JAXBElement<GetStateByKey>(_GetStateByKey_QNAME, GetStateByKey.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStateByKeyResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "getStateByKeyResponse")
    public JAXBElement<GetStateByKeyResponse> createGetStateByKeyResponse(GetStateByKeyResponse value) {
        return new JAXBElement<GetStateByKeyResponse>(_GetStateByKeyResponse_QNAME, GetStateByKeyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUrlsByName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "getUrlsByName")
    public JAXBElement<GetUrlsByName> createGetUrlsByName(GetUrlsByName value) {
        return new JAXBElement<GetUrlsByName>(_GetUrlsByName_QNAME, GetUrlsByName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUrlsByNameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "getUrlsByNameResponse")
    public JAXBElement<GetUrlsByNameResponse> createGetUrlsByNameResponse(GetUrlsByNameResponse value) {
        return new JAXBElement<GetUrlsByNameResponse>(_GetUrlsByNameResponse_QNAME, GetUrlsByNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUrlsByPosition }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "getUrlsByPosition")
    public JAXBElement<GetUrlsByPosition> createGetUrlsByPosition(GetUrlsByPosition value) {
        return new JAXBElement<GetUrlsByPosition>(_GetUrlsByPosition_QNAME, GetUrlsByPosition.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUrlsByPositionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "getUrlsByPositionResponse")
    public JAXBElement<GetUrlsByPositionResponse> createGetUrlsByPositionResponse(GetUrlsByPositionResponse value) {
        return new JAXBElement<GetUrlsByPositionResponse>(_GetUrlsByPositionResponse_QNAME, GetUrlsByPositionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PlayMovie }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "playMovie")
    public JAXBElement<PlayMovie> createPlayMovie(PlayMovie value) {
        return new JAXBElement<PlayMovie>(_PlayMovie_QNAME, PlayMovie.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PlayMovieResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "playMovieResponse")
    public JAXBElement<PlayMovieResponse> createPlayMovieResponse(PlayMovieResponse value) {
        return new JAXBElement<PlayMovieResponse>(_PlayMovieResponse_QNAME, PlayMovieResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetContext }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "setContext")
    public JAXBElement<SetContext> createSetContext(SetContext value) {
        return new JAXBElement<SetContext>(_SetContext_QNAME, SetContext.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetContextResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "setContextResponse")
    public JAXBElement<SetContextResponse> createSetContextResponse(SetContextResponse value) {
        return new JAXBElement<SetContextResponse>(_SetContextResponse_QNAME, SetContextResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetStateByKey }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "setStateByKey")
    public JAXBElement<SetStateByKey> createSetStateByKey(SetStateByKey value) {
        return new JAXBElement<SetStateByKey>(_SetStateByKey_QNAME, SetStateByKey.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetStateByKeyResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "setStateByKeyResponse")
    public JAXBElement<SetStateByKeyResponse> createSetStateByKeyResponse(SetStateByKeyResponse value) {
        return new JAXBElement<SetStateByKeyResponse>(_SetStateByKeyResponse_QNAME, SetStateByKeyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetUrl }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "setUrl")
    public JAXBElement<SetUrl> createSetUrl(SetUrl value) {
        return new JAXBElement<SetUrl>(_SetUrl_QNAME, SetUrl.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetUrlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "setUrlResponse")
    public JAXBElement<SetUrlResponse> createSetUrlResponse(SetUrlResponse value) {
        return new JAXBElement<SetUrlResponse>(_SetUrlResponse_QNAME, SetUrlResponse.class, null, value);
    }

}
