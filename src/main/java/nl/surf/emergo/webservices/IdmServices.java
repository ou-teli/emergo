/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.webservices;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import org.springframework.web.context.support.WebApplicationContextUtils;

import nl.surf.emergo.axisspring.AxisSSpring;
import nl.surf.emergo.axisspring.AxisSSpringPerRgaId;
import nl.surf.emergo.business.IAccountContextManager;
import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.IContextManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IEAccountContext;
import nl.surf.emergo.domain.IEContext;
import nl.surf.emergo.domain.IERole;
import nl.surf.emergo.utilities.PropsValues;

/**
 * The Class IdmServices.
 */
@Component("idmServices")
@WebService(serviceName = "IdmServices", portName = "IdmServicesEndpointPort", targetNamespace = "http://webservices.emergo.surf.nl/")
public class IdmServices extends SpringBeanAutowiringSupport implements Serializable, IdmServicesIF {
	private static final Logger _log = LogManager.getLogger(IdmServices.class);
	private static final long serialVersionUID = -218021805372157709L;

	/** The application context. */
	protected ApplicationContext applicationContext = null;

	/** The s spring per rga id. */
	protected AxisSSpringPerRgaId sSpringPerRgaId = null;

	/** The status type. */
	protected AxisSSpring sSpring = null;

	/** The idm password, password cannot be empty. */
	protected String idmPassword = "idmuser";

	/** the status and status messages to be returned by the webservices. */
	protected String[][] idmStatusArr = new String[][] { { "0", "not processed: ERROR: no valid access" },
			{ "1", "processed" }, { "2", "not processed: ERROR: UserId is null or empty" },
			{ "3", "not processed: ERROR: Lastname is null or empty" }, { "4", "not processed: ERROR: Active is null" },
			{ "5", "not processed: ERROR: Student not added" }, { "6", "not processed: ERROR: Student not updated" },
			{ "7", "not processed: ERROR: Student does not exist" } };

	protected String putStudentLogStr = "idmservice putStudent";
	protected String updateStudentLogStr = "idmservice updateStudent";
	protected String updateContextLogStr = "idmservice updateContexts";
	protected String updateStudentContextLogStr = "idmservice updateStudentContexts";
	protected String removeStudentLogStr = "idmservice removeStudent";
	protected String disableStudentLogStr = "idmservice disableStudent";
	protected String enableStudentLogStr = "idmservice enableStudent";
	protected String getStudentLogStr = "idmservice getStudent";
	protected String getStudentsLogStr = "idmservice getStudents";

	@Resource
	private WebServiceContext context;

	protected final void setSpring() {
		ServletContext lSC = (ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
		applicationContext = WebApplicationContextUtils.getWebApplicationContext(lSC);
		sSpringPerRgaId = new AxisSSpringPerRgaId(applicationContext);
		// get default password
		String lIdmPassword = PropsValues.IDM_DEFAULT_PASSWORD;
		if (!isEmptyString(lIdmPassword))
			idmPassword = lIdmPassword;
	}

	/**
	 * P get s spring.
	 *
	 * @param aRgaId the a rga id
	 *
	 * @return the axis s spring
	 */
	private AxisSSpring pGetSSpring(String aRgaId) {
		if (sSpringPerRgaId == null)
			setSpring();
		return sSpringPerRgaId.getAxisSSpring(aRgaId);
	}

	/**
	 * Puts an IDM student to EMERGO. Used for new and existing users. For Every
	 * change in user profile this method is called by IDM. Maybe strings have to be
	 * escaped or unescaped!
	 *
	 * @param aUserId      the user id
	 * @param aStudentid   the student id
	 * @param aTitle       the title
	 * @param aInitials    the initials
	 * @param aNameprefix  the nameprefix
	 * @param aLastname    the lastname
	 * @param aEmail       the email
	 * @param aCourseCodes the course codes the user has access to
	 * @param aActive      the active state
	 *
	 * @return status and statusmessage
	 */
	@Override
	public String[] putStudent(String aUserId, String aStudentid, String aTitle, String aInitials, String aNameprefix,
			String aLastname, String aEmail, String[] aCourseCodes, Boolean aActive) {
		if (!validAccess()) {
			_log.info(putStudentLogStr + ": userid=" + aUserId + ": " + idmStatusArr[0][1] + ": " + new Date());
			return idmStatusArr[0];
		}
		if (isEmptyString(aUserId)) {
			_log.info(putStudentLogStr + ": userid=" + aUserId + ": " + idmStatusArr[2][1] + ": " + new Date());
			return idmStatusArr[2];
		}
		if (isEmptyString(aLastname)) {
			_log.info(putStudentLogStr + ": userid=" + aUserId + ": " + idmStatusArr[3][1] + ": " + new Date());
			return idmStatusArr[3];
		}
		if (aActive == null) {
			_log.info(putStudentLogStr + ": userid=" + aUserId + ": " + idmStatusArr[4][1] + ": " + new Date());
			return idmStatusArr[4];
		}
		if (aStudentid == null)
			aStudentid = "";
		if (aTitle == null)
			aTitle = "";
		if (aInitials == null)
			aInitials = "";
		if (aNameprefix == null)
			aNameprefix = "";
		if (aEmail == null)
			aEmail = "";
		// putStudent is not depending on rgaid, so use rgaid 0,
		// it will not be present in the db.
		sSpring = pGetSSpring("0");

		// NOTE have to unescape values in case of special characters
		aUserId = sSpring.unescapeXML(StringEscapeUtils.unescapeHtml4(aUserId));
		aStudentid = sSpring.unescapeXML(StringEscapeUtils.unescapeHtml4(aStudentid));
		aTitle = sSpring.unescapeXML(StringEscapeUtils.unescapeHtml4(aTitle));
		aInitials = sSpring.unescapeXML(StringEscapeUtils.unescapeHtml4(aInitials));
		aNameprefix = sSpring.unescapeXML(StringEscapeUtils.unescapeHtml4(aNameprefix));
		aLastname = sSpring.unescapeXML(StringEscapeUtils.unescapeHtml4(aLastname));
		aEmail = sSpring.unescapeXML(StringEscapeUtils.unescapeHtml4(aEmail));
		if (aCourseCodes != null) {
			for (int i = 0; i < aCourseCodes.length; i++) {
				// NOTE have to unescape values in case of special characters
				aCourseCodes[i] = sSpring.unescapeXML(StringEscapeUtils.unescapeHtml4(aCourseCodes[i]));
			}
		}

		List<IEAccount> accountlist = new ArrayList<IEAccount>();
		// update student
		String[] lStatus = updateStudent(aUserId, aStudentid, aTitle, aInitials, aNameprefix, aLastname, aEmail,
				aActive, accountlist);
		if (!lStatus[0].equals("1")) {
			_log.info(putStudentLogStr + ": userid=" + aUserId + ": " + lStatus[1] + ": " + new Date());
			return lStatus;
		}
		// update contexts
		Hashtable<String, IEContext> lHContexts = new Hashtable<String, IEContext>();
		lStatus = updateContexts(aCourseCodes, lHContexts);
		if (!lStatus[0].equals("1")) {
			_log.info(putStudentLogStr + ": userid=" + aUserId + ": " + lStatus[1] + ": " + new Date());
			return lStatus;
		}
		// update contexts for student
		lStatus = updateStudentContexts(accountlist.get(0), lHContexts);
		if (!lStatus[0].equals("1")) {
			_log.info(putStudentLogStr + ": userid=" + aUserId + ": " + lStatus[1] + ": " + new Date());
			return lStatus;
		}
		IAccountManager accountManager = (IAccountManager) sSpring.getBean("accountManager");
		IEAccount account = accountManager.getAccount(aUserId);
		lStatus = getStudentData(account);
		_log.info(putStudentLogStr + ": userid=" + aUserId + ": contexts=" + Arrays.toString(aCourseCodes) + ": "
				+ idmStatusArr[1][1] + ": " + new Date());
		return lStatus;
	}

	/**
	 * Removes an IDM student from EMERGO.
	 *
	 * @param aUserId the user id
	 *
	 * @return status and statusmessage
	 */
	@Override
	public String[] removeStudent(String aUserId) {
		if (!validAccess()) {
			_log.info(removeStudentLogStr + ": userid=" + aUserId + ": " + idmStatusArr[0][1] + ": " + new Date());
			return idmStatusArr[0];
		}
		if (isEmptyString(aUserId)) {
			_log.info(removeStudentLogStr + ": userid=" + aUserId + ": " + idmStatusArr[2][1] + ": " + new Date());
			return idmStatusArr[2];
		}
		// deleteStudent is not depending on rgaid, so use rgaid 0,
		// it will not be present in the db.
		sSpring = pGetSSpring("0");
		// NOTE have to unescape values in case of special characters
		aUserId = sSpring.unescapeXML(StringEscapeUtils.unescapeHtml4(aUserId));
		// delete student. NOTE that due toe cascade all student contexts are deleted
		// too
		String[] lStatus = null;
		IAccountManager accountManager = (IAccountManager) sSpring.getBean("accountManager");
		IEAccount account = accountManager.getAccount(aUserId);
		if (account == null) {
			lStatus = idmStatusArr[7];
		} else {
			// NOTE don't delete account. It also removes all student data and we want to
			// save it forever
//	    	accountManager.deleteAccount(account);
			if (account.getActive()) {
				account.setActive(false);
				accountManager.updateAccount(account);
			}
			_log.info(removeStudentLogStr + ": userid=" + aUserId + ": delete student: " + new Date());
			lStatus = idmStatusArr[1];
		}
		if (!lStatus[0].equals("1")) {
			_log.info(removeStudentLogStr + ": userid=" + aUserId + ": " + lStatus[1] + ": " + new Date());
			return lStatus;
		}
		_log.info(removeStudentLogStr + ": userid=" + aUserId + ": " + idmStatusArr[1][1] + ": " + new Date());
		return idmStatusArr[1];
	}

	/**
	 * Disables an IDM student in EMERGO.
	 *
	 * @param aUserId the user id
	 *
	 * @return status and statusmessage and student data
	 */
	@Override
	public String[] disableStudent(String aUserId) {
		return enableStudent(aUserId, false, disableStudentLogStr);
	}

	/**
	 * Enables an IDM student in EMERGO.
	 *
	 * @param aUserId the user id
	 *
	 * @return status and statusmessage and student data
	 */
	@Override
	public String[] enableStudent(String aUserId) {
		return enableStudent(aUserId, true, enableStudentLogStr);
	}

	/**
	 * Enables an IDM student in EMERGO.
	 *
	 * @param aUserId       the user id
	 * @param aEnable       the enabled
	 * @param aMethodLogStr the method log str
	 *
	 * @return status and statusmessage and student data
	 */
	private String[] enableStudent(String aUserId, boolean aEnable, String aMethodLogStr) {
		if (!validAccess()) {
			_log.info(aMethodLogStr + ": userid=" + aUserId + ": " + idmStatusArr[0][1] + ": " + new Date());
			return idmStatusArr[0];
		}
		if (isEmptyString(aUserId)) {
			_log.info(aMethodLogStr + ": userid=" + aUserId + ": " + idmStatusArr[2][1] + ": " + new Date());
			return idmStatusArr[2];
		}
		// getStudent is not depending on rgaid, so use rgaid 0,
		// it will not be present in the db.
		sSpring = pGetSSpring("0");
		// NOTE have to unescape values in case of special characters
		aUserId = sSpring.unescapeXML(StringEscapeUtils.unescapeHtml4(aUserId));
		// get student
		String[] lStatus = null;
		IAccountManager accountManager = (IAccountManager) sSpring.getBean("accountManager");
		IEAccount account = accountManager.getAccount(aUserId);
		if (account == null) {
			lStatus = idmStatusArr[7];
		} else {
			lStatus = idmStatusArr[1];
		}
		if (!lStatus[0].equals("1")) {
			_log.info(aMethodLogStr + ": userid=" + aUserId + ": " + lStatus[1] + ": " + new Date());
			return lStatus;
		}
		if (account.getActive() != aEnable) {
			account.setActive(aEnable);
			accountManager.updateAccount(account);
		}
		lStatus = getStudentData(account);
		_log.info(aMethodLogStr + ": userid=" + aUserId + ": " + idmStatusArr[1][1] + ": " + new Date());
		return lStatus;
	}

	/**
	 * Gets an IDM student from EMERGO.
	 *
	 * @param aUserId the user id
	 *
	 * @return status and statusmessage and student data
	 */
	@Override
	public String[] getStudent(String aUserId) {
		if (!validAccess()) {
			_log.info(getStudentLogStr + ": userid=" + aUserId + ": " + idmStatusArr[0][1] + ": " + new Date());
			return idmStatusArr[0];
		}
		if (isEmptyString(aUserId)) {
			_log.info(getStudentLogStr + ": userid=" + aUserId + ": " + idmStatusArr[2][1] + ": " + new Date());
			return idmStatusArr[2];
		}
		// getStudent is not depending on rgaid, so use rgaid 0,
		// it will not be present in the db.
		sSpring = pGetSSpring("0");
		// NOTE have to unescape values in case of special characters
		aUserId = sSpring.unescapeXML(StringEscapeUtils.unescapeHtml4(aUserId));
		// get student
		String[] lStatus = null;
		IAccountManager accountManager = (IAccountManager) sSpring.getBean("accountManager");
		IEAccount account = accountManager.getAccount(aUserId);
		if (account == null) {
			lStatus = idmStatusArr[7];
		} else {
			lStatus = idmStatusArr[1];
		}
		if (!lStatus[0].equals("1")) {
			_log.info(getStudentLogStr + ": userid=" + aUserId + ": " + lStatus[1] + ": " + new Date());
			return lStatus;
		}
		lStatus = getStudentData(account);
		_log.info(getStudentLogStr + ": userid=" + aUserId + ": " + idmStatusArr[1][1] + ": " + new Date());
		return lStatus;
	}

	/**
	 * Gets IDM students from EMERGO.
	 *
	 * @return status and statusmessage and student data
	 */
	@Override
	public String[] getStudents() {
		if (!validAccess()) {
			_log.info(getStudentsLogStr + ": " + idmStatusArr[0][1] + ": " + new Date());
			return idmStatusArr[0];
		}
		// getStudents is not depending on rgaid, so use rgaid 0,
		// it will not be present in the db.
		sSpring = pGetSSpring("0");
		// get students
		String[] lStatus = null;
		IAccountManager accountManager = (IAccountManager) sSpring.getBean("accountManager");
		lStatus = getAllStudentData(accountManager.getAllAccounts());
		_log.info(getStudentsLogStr + idmStatusArr[1][1] + ": " + new Date());
		return lStatus;
	}

	/**
	 * Updates student. Used for new and existing users.
	 *
	 * @param aUserId     the user id
	 * @param aStudentid  the student id
	 * @param aTitle      the title
	 * @param aInitials   the initials
	 * @param aNameprefix the nameprefix
	 * @param aLastname   the lastname
	 * @param aEmail      the email
	 * @param aActive     the active state
	 * @param aAccount    the account
	 *
	 * @return status and statusmessage
	 */
	private synchronized String[] updateStudent(String aUserId, String aStudentid, String aTitle, String aInitials,
			String aNameprefix, String aLastname, String aEmail, Boolean aActive, List<IEAccount> aAccount) {
		// check if student exists

		sSpring = pGetSSpring("0");
		// NOTE have to unescape values in case of special characters
		aUserId = sSpring.unescapeXML(StringEscapeUtils.unescapeHtml4(aUserId));
		aStudentid = sSpring.unescapeXML(StringEscapeUtils.unescapeHtml4(aStudentid));
		aTitle = sSpring.unescapeXML(StringEscapeUtils.unescapeHtml4(aTitle));
		aInitials = sSpring.unescapeXML(StringEscapeUtils.unescapeHtml4(aInitials));
		aNameprefix = sSpring.unescapeXML(StringEscapeUtils.unescapeHtml4(aNameprefix));
		aLastname = sSpring.unescapeXML(StringEscapeUtils.unescapeHtml4(aLastname));
		aEmail = sSpring.unescapeXML(StringEscapeUtils.unescapeHtml4(aEmail));

		IAccountManager accountManager = (IAccountManager) sSpring.getBean("accountManager");
		IEAccount account = accountManager.getAccount(aUserId);
		boolean lNew = (account == null);
		boolean lUpdate = false;
		// if not add student, otherwise update student
		if (lNew) {
			account = accountManager.getNewAccount();
			int lAdmId = 1;
			/*
			 * List<IEAccount> lAdmins =
			 * (List<IEAccount>)accountManager.getAllAccounts(true,
			 * AppConstants.c_role_adm); if ((lAdmins != null) && (lAdmins.size() > 0))
			 * lAdmId = (lAdmins.get(0)).getAccId();
			 */
			account.setAccAccId(lAdmId);
			account.setUserid(aUserId);
			account.setPassword(idmPassword);
			account.setOpenaccess(true);
			account.setPhonenumber("");
			account.setJob("");
			account.setExtradata("");
			// set student role for this student
			List<IERole> lAllRoles = accountManager.getAllRoles();
			if (lAllRoles != null) {
				Set<IERole> lRoles = new HashSet<IERole>();
				for (IERole lObject : lAllRoles) {
					String lCode = lObject.getCode();
					if (lCode.equals(AppConstants.c_role_stu))
						lRoles.add(lObject);
				}
				account.setERoles(lRoles);
			}
			_log.info(updateStudentLogStr + ": userid=" + aUserId + ": new student: " + new Date());
		} else {
			account = (IEAccount) account.clone();
			lUpdate = ((!aInitials.equals(account.getInitials())) || (!aNameprefix.equals(account.getNameprefix()))
					|| (!aLastname.equals(account.getLastname())) || (!aEmail.equals(account.getEmail()))
					|| (aActive.booleanValue() != account.getActive()));
			if (lUpdate)
				_log.info(updateStudentLogStr + ": userid=" + aUserId + ": update student - changed: " + new Date());
			else
				_log.info(updateStudentLogStr + ": userid=" + aUserId + ": no change: " + new Date());
		}
		if (lNew || lUpdate) {
			account.setStudentid(aStudentid);
			account.setTitle(aTitle);
			account.setInitials(aInitials);
			account.setNameprefix(aNameprefix);
			account.setLastname(aLastname);
			account.setEmail(aEmail);
			account.setActive(aActive.booleanValue());
		}
		List<String[]> lErrors = new ArrayList<String[]>();
		if (lNew) {
			lErrors = accountManager.newAccount(account);
			if ((lErrors != null) && (lErrors.size() > 0)) {
				_log.info(updateStudentLogStr + ": userid=" + aUserId + ": " + idmStatusArr[5][1] + ": " + new Date());
				return idmStatusArr[5];
			}
		} else {
			if (lUpdate) {
				lErrors = accountManager.updateAccount(account);
				if ((lErrors != null) && (lErrors.size() > 0)) {
					_log.info(updateStudentLogStr + ": userid=" + aUserId + ": " + idmStatusArr[6][1] + ": "
							+ new Date());
					return idmStatusArr[6];
				}
			}
		}
		aAccount.add(account);
		return idmStatusArr[1];
	}

	/**
	 * Updates contexts. Used for new and existing contexts.
	 *
	 * @param aContextStrs the contexts
	 * @param aContexts    the contexts
	 *
	 * @return status and statusmessage
	 */
	private synchronized String[] updateContexts(String[] aContextStrs, Hashtable<String, IEContext> aContexts) {
		if ((aContextStrs == null) || (aContextStrs.length == 0))
			return idmStatusArr[1];
		IContextManager contextManager = (IContextManager) sSpring.getBean("contextManager");
		// get existing contexts and put in hashtable
		List<IEContext> lContexts = contextManager.getAllContexts();
		Hashtable<String, IEContext> lHContexts = new Hashtable<String, IEContext>();
		for (IEContext lObject : lContexts) {
			lHContexts.put(lObject.getContext().trim().toLowerCase(), lObject);
		}
		// loop through contexts and add if applicable
		for (int i = 0; i < aContextStrs.length; i++) {
			String lContextStr = aContextStrs[i].trim().toLowerCase();
			if (!isEmptyString(lContextStr)) {
				if (!lHContexts.containsKey(lContextStr)) {
					IEContext lNewContext = contextManager.getNewContext();
					lNewContext.setContext(lContextStr);
					lNewContext.setActive(true);
					contextManager.newContext(lNewContext);
					// add to handle double contexts
					lHContexts.put(lContextStr, lNewContext);
					aContexts.put(lContextStr, lNewContext);
					_log.info(updateContextLogStr + ": context=" + lContextStr + ": added: " + new Date());
				} else {
					IEContext lContext = lHContexts.get(lContextStr);
					if (lContext != null)
						aContexts.put(lContextStr, lContext);
				}
			}
		}
		return idmStatusArr[1];
	}

	/**
	 * Updates student contexts. Used for new and existing student contexts.
	 *
	 * @param aAccount  the account
	 * @param aContexts the contexts
	 *
	 * @return status and statusmessage
	 */
	private synchronized String[] updateStudentContexts(IEAccount aAccount, Hashtable<String, IEContext> aContexts) {
		if (aAccount == null) {
			return idmStatusArr[7];
		}
		// adjust existing contexts
		IAccountContextManager accountContextManager = (IAccountContextManager) sSpring
				.getBean("accountContextManager");
		List<IEAccountContext> lAccountContexts = accountContextManager
				.getAllAccountContextsByAccountId(aAccount.getAccId());
		IContextManager contextManager = (IContextManager) sSpring.getBean("contextManager");
		// get existing contexts and put in hashtable
		List<IEContext> lContexts = contextManager.getAllContexts();
		Hashtable<String, IEContext> lHContexts = new Hashtable<String, IEContext>();
		for (IEContext lObject : lContexts) {
			lHContexts.put("" + lObject.getConId(), lObject);
		}
		String lLogUserId = aAccount.getUserid();
		for (IEAccountContext lObject : lAccountContexts) {
			IEContext lObject2 = lHContexts.get("" + lObject.getConConId());
			if (lObject2 != null) {
				String lContext = lObject2.getContext().trim().toLowerCase();
				if (aContexts.containsKey(lContext)) {
					if (!lObject.getActive()) {
						lObject.setActive(true);
						accountContextManager.updateAccountContext(lObject);
						_log.info(updateStudentContextLogStr + ": userid=" + lLogUserId + ": context=" + lContext
								+ ": enabled: " + new Date());
					}
					aContexts.remove(lContext);
				} else {
					if (lObject.getActive()) {
						lObject.setActive(false);
						accountContextManager.updateAccountContext(lObject);
						_log.info(updateStudentContextLogStr + ": userid=" + lLogUserId + ": context=" + lContext
								+ ": disabled: " + new Date());
					}
				}
			}
		}
		// add new contexts
		for (Enumeration<String> keys = aContexts.keys(); keys.hasMoreElements();) {
			String key = keys.nextElement();
			IEAccountContext lAccountContext = accountContextManager.getNewAccountContext();
			lAccountContext.setActive(true);
			lAccountContext.setAccAccId(aAccount.getAccId());
			lAccountContext.setConConId(aContexts.get(key).getConId());
			accountContextManager.newAccountContext(lAccountContext);
			_log.info(updateStudentContextLogStr + ": userid=" + lLogUserId + ": context="
					+ aContexts.get(key).getContext() + ": added: " + new Date());
		}
		return idmStatusArr[1];
	}

	/**
	 * Gets student data.
	 *
	 * @param aAccount the account
	 *
	 * @return status and status message and student data
	 */
	private synchronized String[] getStudentData(IEAccount aAccount) {
		// getStudentData is not depending on rgaid, so use rgaid 0,
		// it will not be present in the db.
		sSpring = pGetSSpring("0");

		List<IEAccount> lAccounts = new ArrayList<IEAccount>();
		lAccounts.add(aAccount);
		// get existing contexts
		IAccountContextManager accountContextManager = (IAccountContextManager) sSpring
				.getBean("accountContextManager");
		List<IEAccountContext> lAccountContexts = accountContextManager
				.getAllAccountContextsByAccountId(aAccount.getAccId());
		return getStudentData(lAccounts, lAccountContexts);
	}

	/**
	 * Gets student data.
	 *
	 * @param aAccounts the account
	 *
	 * @return status and status message and student data
	 */
	private synchronized String[] getAllStudentData(List<IEAccount> aAccounts) {
		// getAllStudentData is not depending on rgaid, so use rgaid 0,
		// it will not be present in the db.
		sSpring = pGetSSpring("0");

		// get existing contexts
		IAccountContextManager accountContextManager = (IAccountContextManager) sSpring
				.getBean("accountContextManager");
		List<IEAccountContext> lAccountContexts = accountContextManager.getAllAccountContexts();
		return getStudentData(aAccounts, lAccountContexts);
	}

	/**
	 * Gets student data.
	 *
	 * @param aAccounts the account
	 *
	 * @return status and status message and student data
	 */
	private synchronized String[] getStudentData(List<IEAccount> aAccounts, List<IEAccountContext> aAccountContexts) {
		IContextManager contextManager = (IContextManager) sSpring.getBean("contextManager");
		// get existing contexts and put in hashtable
		List<IEContext> lContexts = contextManager.getAllContexts();
		Hashtable<Integer, String> lHContextNames = new Hashtable<Integer, String>();
		for (IEContext lObject : lContexts) {
			// NOTE have to escape values in case of special characters
			lHContextNames.put(lObject.getConId(),
					sSpring.escapeXML(StringEscapeUtils.escapeHtml4(lObject.getContext().trim().toLowerCase())));
		}

		// put account contexts in hashtable, for better performance
		Hashtable<Integer, List<IEAccountContext>> lHAccountContextsPerAccId = new Hashtable<Integer, List<IEAccountContext>>();
		for (IEAccountContext lObject : aAccountContexts) {
			if (lObject.getActive()) {
				if (!lHAccountContextsPerAccId.containsKey(lObject.getAccAccId())) {
					lHAccountContextsPerAccId.put(lObject.getAccAccId(), new ArrayList<IEAccountContext>());
				}
				lHAccountContextsPerAccId.get(lObject.getAccAccId()).add(lObject);
			}
		}

		String[] lStatus = new String[2 + aAccounts.size() * 9];
		lStatus[0] = idmStatusArr[1][0];
		lStatus[1] = idmStatusArr[1][1];
		int lCounter = 2;

		for (IEAccount lAccount : aAccounts) {
			// NOTE have to escape values in case of special characters
			lStatus[lCounter] = sSpring.escapeXML(StringEscapeUtils.escapeHtml4(lAccount.getUserid()));
			lStatus[lCounter + 1] = sSpring.escapeXML(StringEscapeUtils.escapeHtml4(lAccount.getStudentid()));
			lStatus[lCounter + 2] = sSpring.escapeXML(StringEscapeUtils.escapeHtml4(lAccount.getTitle()));
			lStatus[lCounter + 3] = sSpring.escapeXML(StringEscapeUtils.escapeHtml4(lAccount.getInitials()));
			lStatus[lCounter + 4] = sSpring.escapeXML(StringEscapeUtils.escapeHtml4(lAccount.getNameprefix()));
			lStatus[lCounter + 5] = sSpring.escapeXML(StringEscapeUtils.escapeHtml4(lAccount.getLastname()));
			lStatus[lCounter + 6] = sSpring.escapeXML(StringEscapeUtils.escapeHtml4(lAccount.getEmail()));
			lStatus[lCounter + 7] = "" + lAccount.getActive();
			lStatus[lCounter + 8] = getStudentContexts(lHAccountContextsPerAccId.get(lAccount.getAccId()),
					lHContextNames, lAccount);
			lCounter += 9;
		}
		return lStatus;
	}

	/**
	 * Gets student contexts.
	 *
	 * @param aAccount the account
	 *
	 * @return student contexts
	 */
	private synchronized String getStudentContexts(List<IEAccountContext> aAccountContextsPerAccId,
			Hashtable<Integer, String> aHContextNames, IEAccount aAccount) {
		if (aAccount == null) {
			return null;
		}
		if (aAccountContextsPerAccId == null || aAccountContextsPerAccId.size() == 0) {
			return "";
		}
		StringBuffer lStudentContexts = new StringBuffer();
		boolean lAddComma = false;
		for (IEAccountContext lObject : aAccountContextsPerAccId) {
			String lName = aHContextNames.get(lObject.getConConId());
			if (lName != null && lName.length() > 0) {
				if (lAddComma) {
					lStudentContexts.append(",");
				} else {
					lAddComma = true;
				}
				lStudentContexts.append(lName);
			}
		}
		return lStudentContexts.toString();
	}

	/**
	 *
	 * Checks if calling server is permitted to call webservice.
	 *
	 * @return if valid access.
	 */
	private boolean validAccess() {
		String lSaveIpPart = PropsValues.IDM_SAVE_IP_PART;
		if ((lSaveIpPart == null) || (lSaveIpPart.equals("")))
			// no access if not defined
			return false;
		MessageContext lMC = context.getMessageContext();

		String lRemoteAddr = null;
		if ((lMC != null) && (lMC.containsKey(MessageContext.SERVLET_REQUEST))) {
			HttpServletRequest lServletReq = (HttpServletRequest) lMC.get(MessageContext.SERVLET_REQUEST);
			if (lServletReq != null)
				lRemoteAddr = lServletReq.getRemoteAddr();
		}
		if ((lRemoteAddr == null) || (lRemoteAddr.equals("")))
			// no access if remote address cannot be found
			return false;
		return (lRemoteAddr.indexOf(lSaveIpPart) >= 0);
	}

	/**
	 * Is string empty.
	 *
	 * @param aString the string
	 *
	 * @return if empty
	 */
	private boolean isEmptyString(String aString) {
		return ((aString == null) || (aString.equals("")));
	}

}
