/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.webservices;

import java.io.Serializable;

/**
 * The Class Url.
 */
public class Url implements Serializable
{
	
	private static final long serialVersionUID = -7252491208155561769L;

	/** The id. */
	protected String id;
	
	/** The name. */
	protected String name;
	
	/** The url. */
	protected String url;
	
	/** The data. */
	protected byte[] data;
	
	/** The description. */
	protected String description;
	
	/** The latitude. */
	protected String latitude;
	
	/** The longitude. */
	protected String longitude;

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 * 
	 * @param aId the new id
	 */
	public void setId(String aId) {
		id = aId;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 * 
	 * @param aName the new name
	 */
	public void setName(String aName) {
		name = aName;
	}

	/**
	 * Gets the url.
	 * 
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	
	/**
	 * Sets the url.
	 * 
	 * @param aUrl the new url
	 */
	public void setUrl(String aUrl) {
		url = aUrl;
	}

	/**
	 * Gets the data.
	 * 
	 * @return the data
	 */
	public byte[] getData() {
		return data;
	}
	
	/**
	 * Sets the data.
	 * 
	 * @param aData the new data
	 */
	public void setData(byte[] aData) {
		data = aData;
	}

	/**
	 * Gets the description.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Sets the description.
	 * 
	 * @param aDescription the new description
	 */
	public void setDescription(String aDescription) {
		description = aDescription;
	}

	/**
	 * Gets the latitude.
	 * 
	 * @return the latitude
	 */
	public String getLatitude() {
		return latitude;
	}
	
	/**
	 * Sets the latitude.
	 * 
	 * @param aLatitude the new latitude
	 */
	public void setLatitude(String aLatitude) {
		latitude = aLatitude;
	}

	/**
	 * Gets the longitude.
	 * 
	 * @return the longitude
	 */
	public String getLongitude() {
		return longitude;
	}
	
	/**
	 * Sets the longitude.
	 * 
	 * @param aLongitude the new longitude
	 */
	public void setLongitude(String aLongitude) {
		longitude = aLongitude;
	}

}