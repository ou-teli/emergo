/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */

package nl.surf.emergo.webservices.idmclient;

/**
 * Please modify this class to meet your needs
 * This class is not complete
 */

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This class was generated by Apache CXF 3.1.1 2015-08-27T16:16:30.999+02:00
 * Generated source version: 3.1.1
 * 
 */
public final class IdmServices_Client {
	private static final Logger _log = LogManager.getLogger(IdmServices_Client.class);
	private static final QName SERVICE_NAME = new QName("http://webservices.emergo.surf.nl/", "IdmServices");

	private static IdmServices_Service idmservice = null;
	private static IdmServices idmserviceport = null;

	public IdmServices_Client() {
	}

	public IdmServices_Client(String args[]) {
		if (idmservice == null) {
			URL wsdlURL = IdmServices_Service.WSDL_LOCATION;
			if (args.length > 0 && args[0] != null && !"".equals(args[0])) {
				File wsdlFile = new File(args[0]);
				try {
					if (wsdlFile.exists()) {
						wsdlURL = wsdlFile.toURI().toURL();
					} else {
						wsdlURL = new URL(args[0]);
					}
				} catch (MalformedURLException e) {
					_log.error(e);
				}
			}
			idmservice = new IdmServices_Service(wsdlURL, SERVICE_NAME);
			idmserviceport = idmservice.getIdmServicesEndpointPort();
		}
	}

	public static void test(String args[]) throws java.lang.Exception {
		URL wsdlURL = IdmServices_Service.WSDL_LOCATION;
		if (args.length > 0 && args[0] != null && !"".equals(args[0])) {
			File wsdlFile = new File(args[0]);
			try {
				if (wsdlFile.exists()) {
					wsdlURL = wsdlFile.toURI().toURL();
				} else {
					wsdlURL = new URL(args[0]);
				}
			} catch (MalformedURLException e) {
				_log.error(e);
			}
		}

		IdmServices_Service ss = new IdmServices_Service(wsdlURL, SERVICE_NAME);
		IdmServices port = ss.getIdmServicesEndpointPort();

		{
			_log.info("Invoking putStudent...");
			java.lang.String _putStudent_arg0 = "_putStudent_arg0-1493375895";
			java.lang.String _putStudent_arg1 = "_putStudent_arg1-472763047";
			java.lang.String _putStudent_arg2 = "_putStudent_arg2-472763048";
			java.lang.String _putStudent_arg3 = "_putStudent_arg3-472763049";
			java.lang.String _putStudent_arg4 = "_putStudent_arg4-1245920379";
			java.lang.String _putStudent_arg5 = "_putStudent_arg5-1815101262";
			java.lang.String _putStudent_arg6 = "_putStudent_arg6-749131915";
			java.util.List<java.lang.String> _putStudent_arg7 = new java.util.ArrayList<java.lang.String>();
			java.lang.String _putStudent_arg7Val1 = "_putStudent_arg7-Val-1480831433";
			_putStudent_arg7.add(_putStudent_arg7Val1);
			java.lang.Boolean _putStudent_arg8 = Boolean.valueOf(true);
			java.util.List<java.lang.String> _putStudent__return = port.putStudent(_putStudent_arg0, _putStudent_arg1,
					_putStudent_arg2, _putStudent_arg3, _putStudent_arg4, _putStudent_arg5, _putStudent_arg6,
					_putStudent_arg7, _putStudent_arg8);
			_log.info("putStudent.result=" + _putStudent__return);

		}

	}

	public List<String> putStudent(String aUserId, String aStudentid, String aTitle, String aInitials,
			String aNameprefix, String aLastname, String aEmail, List<String> aCourseCodes, Boolean aActive)
			throws Exception {
		_log.info("Invoking putStudent...");
		List<String> _putStudent__return = idmserviceport.putStudent(aUserId, aStudentid, aTitle, aInitials,
				aNameprefix, aLastname, aEmail, aCourseCodes, aActive);
		_log.info("putStudent.result=" + _putStudent__return);
		return _putStudent__return;
	}

	public List<String> removeStudent(String aUserId) throws Exception {
		_log.info("Invoking removeStudent...");
		List<String> _removeStudent__return = idmserviceport.removeStudent(aUserId);
		_log.info("removeStudent.result=" + _removeStudent__return);
		return _removeStudent__return;
	}

	public List<String> disableStudent(String aUserId) throws Exception {
		_log.info("Invoking disableStudent...");
		List<String> _disableStudent__return = idmserviceport.disableStudent(aUserId);
		_log.info("disableStudent.result=" + _disableStudent__return);
		return _disableStudent__return;
	}

	public List<String> enableStudent(String aUserId) throws Exception {
		_log.info("Invoking enableStudent...");
		List<String> _enableStudent__return = idmserviceport.enableStudent(aUserId);
		_log.info("enableStudent.result=" + _enableStudent__return);
		return _enableStudent__return;
	}

	public List<String> getStudent(String aUserId) throws Exception {
		_log.info("Invoking getStudent...");
		List<String> _getStudent__return = idmserviceport.getStudent(aUserId);
		_log.info("getStudent.result=" + _getStudent__return);
		return _getStudent__return;
	}

	public List<String> getStudents() throws Exception {
		_log.info("Invoking getStudents...");
		List<String> _getStudents__return = idmserviceport.getStudents();
		_log.info("getStudents.result=" + _getStudents__return);
		return _getStudents__return;
	}

}
