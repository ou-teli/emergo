/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */

package nl.surf.emergo.webservices.idmclient;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the nl.surf.emergo.webservices.idmclient package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PutStudent_QNAME = new QName("http://webservices.emergo.surf.nl/", "putStudent");
    private final static QName _PutStudentResponse_QNAME = new QName("http://webservices.emergo.surf.nl/", "putStudentResponse");
    private final static QName _RemoveStudent_QNAME = new QName("http://webservices.emergo.surf.nl/", "removeStudent");
    private final static QName _RemoveStudentResponse_QNAME = new QName("http://webservices.emergo.surf.nl/", "removeStudentResponse");
    private final static QName _DisableStudent_QNAME = new QName("http://webservices.emergo.surf.nl/", "disableStudent");
    private final static QName _DisableStudentResponse_QNAME = new QName("http://webservices.emergo.surf.nl/", "disableStudentResponse");
    private final static QName _EnableStudent_QNAME = new QName("http://webservices.emergo.surf.nl/", "enableStudent");
    private final static QName _EnableStudentResponse_QNAME = new QName("http://webservices.emergo.surf.nl/", "enableStudentResponse");
    private final static QName _GetStudent_QNAME = new QName("http://webservices.emergo.surf.nl/", "getStudent");
    private final static QName _GetStudentResponse_QNAME = new QName("http://webservices.emergo.surf.nl/", "getStudentResponse");
    private final static QName _GetStudents_QNAME = new QName("http://webservices.emergo.surf.nl/", "getStudents");
    private final static QName _GetStudentsResponse_QNAME = new QName("http://webservices.emergo.surf.nl/", "getStudentsResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: nl.surf.emergo.webservices.idmclient
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PutStudent }
     * 
     */
    public PutStudent createPutStudent() {
        return new PutStudent();
    }

    /**
     * Create an instance of {@link PutStudentResponse }
     * 
     */
    public PutStudentResponse createPutStudentResponse() {
        return new PutStudentResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PutStudent }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "putStudent")
    public JAXBElement<PutStudent> createPutStudent(PutStudent value) {
        return new JAXBElement<PutStudent>(_PutStudent_QNAME, PutStudent.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PutStudentResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "putStudentResponse")
    public JAXBElement<PutStudentResponse> createPutStudentResponse(PutStudentResponse value) {
        return new JAXBElement<PutStudentResponse>(_PutStudentResponse_QNAME, PutStudentResponse.class, null, value);
    }

    /**
     * Create an instance of {@link RemoveStudent }
     * 
     */
    public RemoveStudent createRemoveStudent() {
        return new RemoveStudent();
    }

    /**
     * Create an instance of {@link RemoveStudentResponse }
     * 
     */
    public RemoveStudentResponse createRemoveStudentResponse() {
        return new RemoveStudentResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveStudent }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "removeStudent")
    public JAXBElement<RemoveStudent> createRemoveStudent(RemoveStudent value) {
        return new JAXBElement<RemoveStudent>(_RemoveStudent_QNAME, RemoveStudent.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveStudentResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "removeStudentResponse")
    public JAXBElement<RemoveStudentResponse> createRemoveStudentResponse(RemoveStudentResponse value) {
        return new JAXBElement<RemoveStudentResponse>(_RemoveStudentResponse_QNAME, RemoveStudentResponse.class, null, value);
    }

    /**
     * Create an instance of {@link DisableStudent }
     * 
     */
    public DisableStudent createDisableStudent() {
        return new DisableStudent();
    }

    /**
     * Create an instance of {@link DisableStudentResponse }
     * 
     */
    public DisableStudentResponse createDisableStudentResponse() {
        return new DisableStudentResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DisableStudent }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "disableStudent")
    public JAXBElement<DisableStudent> createDisableStudent(DisableStudent value) {
        return new JAXBElement<DisableStudent>(_DisableStudent_QNAME, DisableStudent.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DisableStudentResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "disableStudentResponse")
    public JAXBElement<DisableStudentResponse> createDisableStudentResponse(DisableStudentResponse value) {
        return new JAXBElement<DisableStudentResponse>(_DisableStudentResponse_QNAME, DisableStudentResponse.class, null, value);
    }

    /**
     * Create an instance of {@link EnableStudent }
     * 
     */
    public EnableStudent createEnableStudent() {
        return new EnableStudent();
    }

    /**
     * Create an instance of {@link EnableStudentResponse }
     * 
     */
    public EnableStudentResponse createEnableStudentResponse() {
        return new EnableStudentResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnableStudent }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "enableStudent")
    public JAXBElement<EnableStudent> createEnableStudent(EnableStudent value) {
        return new JAXBElement<EnableStudent>(_EnableStudent_QNAME, EnableStudent.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnableStudentResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "enableStudentResponse")
    public JAXBElement<EnableStudentResponse> createEnableStudentResponse(EnableStudentResponse value) {
        return new JAXBElement<EnableStudentResponse>(_EnableStudentResponse_QNAME, EnableStudentResponse.class, null, value);
    }

    /**
     * Create an instance of {@link GetStudent }
     * 
     */
    public GetStudent createGetStudent() {
        return new GetStudent();
    }

    /**
     * Create an instance of {@link GetStudentResponse }
     * 
     */
    public GetStudentResponse createGetStudentResponse() {
        return new GetStudentResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStudent }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "getStudent")
    public JAXBElement<GetStudent> createGetStudent(GetStudent value) {
        return new JAXBElement<GetStudent>(_GetStudent_QNAME, GetStudent.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStudentResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "getStudentResponse")
    public JAXBElement<GetStudentResponse> createGetStudentResponse(GetStudentResponse value) {
        return new JAXBElement<GetStudentResponse>(_GetStudentResponse_QNAME, GetStudentResponse.class, null, value);
    }

    /**
     * Create an instance of {@link GetStudents }
     * 
     */
    public GetStudents createGetStudents() {
        return new GetStudents();
    }

    /**
     * Create an instance of {@link GetStudentsResponse }
     * 
     */
    public GetStudentsResponse createGetStudentsResponse() {
        return new GetStudentsResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStudents }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "getStudents")
    public JAXBElement<GetStudents> createGetStudents(GetStudents value) {
        return new JAXBElement<GetStudents>(_GetStudents_QNAME, GetStudents.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStudentsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.emergo.surf.nl/", name = "getStudentsResponse")
    public JAXBElement<GetStudentsResponse> createGetStudentsResponse(GetStudentsResponse value) {
        return new JAXBElement<GetStudentsResponse>(_GetStudentsResponse_QNAME, GetStudentsResponse.class, null, value);
    }

}
