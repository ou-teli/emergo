/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.webservices;

import javax.jws.WebService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

@Component("emergoServicesEndpoint")
@WebService(serviceName = "EmergoServices")
public class EmergoServicesEndpoint extends SpringBeanAutowiringSupport {

    @Autowired
    private EmergoServices emservices;

	public WRun[] getRuns(String aUserId, String aPassword) {
		return emservices.getRuns(aUserId, aPassword);
	}
	
	public WRun[] getRunsForCRM(String aUserId, String aPassword) {
		return emservices.getRunsForCRM(aUserId, aPassword);
	}

	public WState[] getAllStates(String aRgaId) {
		return emservices.getAllStates(aRgaId);
	}

	public WState getStateByKey(String aRgaId, String aKey) {
		return emservices.getStateByKey(aRgaId, aKey);
	}
	
	public String setStateByKey(String aRgaId, String aKey, String aValue) {
		return emservices.setStateByKey(aRgaId, aKey, aValue);
	}

	public String playMovie(String casus, String studentid, String filmpje) {
		return emservices.playMovie(casus, studentid, filmpje);
	}


	public Exlocation[] getAllExlocations(String aRgaId) {
		return emservices.getAllExlocations(aRgaId);
	}

	public Exlocation[] getCurrentExlocations(String aRgaId) {
		return emservices.getCurrentExlocations(aRgaId);
	}

	public Exlocation[] getExlocationsByPosition(String aRgaId, String aLatitude, String aLongitude) {
		return emservices.getExlocationsByPosition(aRgaId, aLatitude, aLongitude);
	}

	public String getContext(String aRgaId, String aSensorType) {
		return emservices.getContext(aRgaId, aSensorType);
	}

	public String setContext(String aRgaId, String aSensorData) {
		return emservices.setContext(aRgaId, aSensorData);
	}
	
	public Url[] getAllUrls(String aRgaId) {
		return emservices.getAllUrls(aRgaId);
	}

	public Url[] getCurrentUrls(String aRgaId) {
		return emservices.getCurrentUrls(aRgaId);
	}

	public Url[] getUrlsByPosition(String aRgaId, String aLatitude, String aLongitude) {
		return emservices.getUrlsByPosition(aRgaId, aLatitude, aLongitude);
	}
	
	public Url[] getUrlsByName(String aRgaId, String aName) {
		return emservices.getUrlsByName(aRgaId, aName);
	}

	public String setUrl(String aRgaId, Url aUrl) {
		return emservices.setUrl(aRgaId, aUrl);
	}
	
	public String deleteUrl(String aRgaId, Url aUrl) {
		return emservices.deleteUrl(aRgaId, aUrl);
	}

	public String getCaseComponentData(String aRgaId, String aCaseComponentName) {
		return emservices.getCaseComponentData(aRgaId, aCaseComponentName);
	}

}
