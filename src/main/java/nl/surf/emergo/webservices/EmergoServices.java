/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.webservices;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.servlet.ServletContext;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import org.springframework.web.context.support.WebApplicationContextUtils;

import nl.surf.emergo.axisspring.AxisSSpring;
import nl.surf.emergo.axisspring.AxisSSpringPerRgaId;
import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.IRunGroupAccountManager;
import nl.surf.emergo.business.IRunGroupManager;
import nl.surf.emergo.business.IRunManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.IXmlManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.run.CRunContentHelper;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.zkspring.STriggeredReference;

/**
 * The Class EmergoServices.
 */
@Component("emergoServices")
@WebService(serviceName="EmergoServices",
			portName = "EmergoServicesEndpointPort",
			targetNamespace = "http://webservices.emergo.surf.nl/")
public class EmergoServices extends SpringBeanAutowiringSupport implements Serializable, EmergoServicesIF {
//public class EmergoServices extends ServletEndpointSupport implements Serializable {

	private static final long serialVersionUID = -1456471571898479743L;

	private static final Logger _log = LogManager.getLogger(EmergoServices.class);

	/** The meterperdegree. */
	protected double meterperdegree = 40041470/360;

	/** The application context. */
	protected ApplicationContext applicationContext = null;

    /** The s spring per rga id. */
    protected AxisSSpringPerRgaId sSpringPerRgaId = null;

    /** The status type. */
    protected String statusType = AppConstants.statusTypeRunGroup;

    /** The helper. */
    protected EmergoServicesHelper helper = new EmergoServicesHelper();

    protected String statesComponentCode = "states";

    @Resource
    private WebServiceContext context;

	public class CacIdTagData {
		
		private Integer cacId;
		private IXMLTag tag;
		
		public Integer getCacId() {
			return cacId;
		}
		public void setCacId(Integer cacId) {
			this.cacId = cacId;
		}
		public IXMLTag getTag() {
			return tag;
		}
		public void setTag(IXMLTag tag) {
			this.tag = tag;
		}

		public CacIdTagData (Integer cacId, IXMLTag tag) {
			setCacId(cacId);
			setTag(tag);
		}
	}
	
	public class CacIdTagsData {
		
		private Integer cacId;
		private List<IXMLTag> tags;
		
		public Integer getCacId() {
			return cacId;
		}
		public void setCacId(Integer cacId) {
			this.cacId = cacId;
		}
		public List<IXMLTag> getTags() {
			return tags;
		}
		public void setTags(List<IXMLTag> tags) {
			this.tags = tags;
		}

		public CacIdTagsData (Integer cacId, List<IXMLTag> tags) {
			setCacId(cacId);
			setTags(tags);
		}
	}
	
	protected final void setSpring() {
		ServletContext lSC = (ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
		applicationContext = WebApplicationContextUtils.getWebApplicationContext(lSC);
	    sSpringPerRgaId = new AxisSSpringPerRgaId(applicationContext);
	}

	/**
	 * P get s spring.
	 *
	 * @param aRgaId the a rga id
	 *
	 * @return the axis s spring
	 */
	private AxisSSpring pGetSSpring(String aRgaId) {
		if (sSpringPerRgaId == null)
			setSpring();
		return sSpringPerRgaId.getAxisSSpring(aRgaId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.webservices.EmergoServicesIF#getRuns(java.lang.String, java.lang.String)
	 */
	@Override
	public WRun[] getRuns(String aUserId, String aPassword) {
		long lStart = System.currentTimeMillis();
		// use -1 to get SSpring object for general usage, so not for a single run group account
		String lRgaId = "-1";
	    AxisSSpring sSpring = pGetSSpring(lRgaId);
	    WRun[] lRuns = pUserIdPasswordToRuns(aUserId, aPassword, sSpring);
		long lEnd = System.currentTimeMillis();
		_log.info("EMERGO webservice getRuns time (msec)" + ":" + (lEnd - lStart));
	    return lRuns;
	}

	private WRun[] pUserIdPasswordToRuns(String aUserId, String aPassword, AxisSSpring sSpring) {
		IEAccount lAccount = ((IAccountManager)sSpring.getBean("accountManager")).getAccount(aUserId, aPassword, true);
		if (lAccount == null) {
			return null;
		}
		List<IERunGroupAccount> rgas = ((IRunGroupAccountManager)sSpring.getBean("runGroupAccountManager")).getAllRunGroupAccountsByAccId(lAccount.getAccId());
		if (rgas == null || rgas.size() == 0) {
			return null;
		}
		List<Integer> lRunIds = new ArrayList<Integer>();
		List<IERun> lRuns = new ArrayList<IERun>();
		List<List<IERunGroupAccount>> lRunRunGroupAccounts = new ArrayList<List<IERunGroupAccount>>();
		for (IERunGroupAccount lRunGroupAccount : rgas) {
			IERun lRun = lRunGroupAccount.getERunGroup().getERun();
			int lRunId = lRun.getRunId();
			if (!lRunIds.contains(lRunId)) {
				lRunIds.add(lRunId);
				if ((runIsRunning(lRun)) && (lRun.getStatus() == AppConstants.run_status_runnable)) {
					lRuns.add(lRun);
					List<IERunGroupAccount> lRunGroupAccounts = new ArrayList<IERunGroupAccount>(0);
					if (lRun.getOpenaccess()) {
						lRunGroupAccounts = getOpenRunGroupAccounts(lRunId,rgas,sSpring);
					} else {
						lRunGroupAccounts = getRunGroupAccounts(lRunId,rgas);
					}
					lRunRunGroupAccounts.add(lRunGroupAccounts);
				}
			}
		}

		WRun[] result = new WRun[lRuns.size()];
		for (int i=0;i<lRuns.size();i++) {
			List<IERunGroupAccount> lRgas = lRunRunGroupAccounts.get(i);
			WRunGroupAccount[] lRunRgas = new WRunGroupAccount[lRgas.size()];
			for (int j=0;j<lRgas.size();j++) {
				IERunGroupAccount lRga = (IERunGroupAccount)lRgas.get(j);
				lRunRgas[j] = pNewWRunGroupAccount("" + lRga.getRgaId(), lRga.getERunGroup().getName());
			}
			result[i] = pNewWRun(""+lRuns.get(i).getRunId(), lRuns.get(i).getCode(), lRuns.get(i).getName(), lRunRgas);
		}
		return result;
	}

	private boolean runIsRunning(IERun aRun) {
		boolean lRunning = true;
		Date lCurrentDate = new Date();
		Date lStartDate = aRun.getStartdate();
		if ((lStartDate != null) && (lStartDate.after(lCurrentDate)))
			lRunning = false;
		return lRunning;
	}

	private List<IERunGroupAccount> getRunGroupAccounts(int aRunId, List<IERunGroupAccount> aRunGroupAccounts) {
		List<IERunGroupAccount> lRunGroupAccounts = new ArrayList<IERunGroupAccount>(0);
		for (IERunGroupAccount lRunGroupAccount : aRunGroupAccounts) {
			IERunGroup lRunGroup = (IERunGroup)lRunGroupAccount.getERunGroup();
			if ((lRunGroup.getActive()) && (lRunGroup.getERun().getRunId() == aRunId)) {
				lRunGroupAccounts.add(lRunGroupAccount);
			}
		}
		return lRunGroupAccounts;
	}

	private List<IERunGroupAccount> getOpenRunGroupAccounts(int aRunId, List<IERunGroupAccount> aRunGroupAccounts, AxisSSpring sSpring) {
		List<IERunGroupAccount> lRunGroupAccounts = new ArrayList<IERunGroupAccount>(0);
		for (IERunGroupAccount lRunGroupAccount : aRunGroupAccounts) {
			IERunGroup lRunGroup = (IERunGroup)lRunGroupAccount.getERunGroup();
			if (lRunGroup.getERun().getRunId() == aRunId) {
				if (!lRunGroup.getActive()) {
					lRunGroup.setActive(true);
					((IRunGroupManager)sSpring.getBean("runGroupManager")).updateRunGroup(lRunGroup);
				}
				lRunGroupAccounts.add(lRunGroupAccount);
			}
		}
		return lRunGroupAccounts;
	}

	private WRun pNewWRun(String aId, String aCode, String aName, WRunGroupAccount[] aRgas) {
		WRun lObject = new WRun();
		lObject.setId(aId);
		lObject.setCode(aCode);
		lObject.setName(aName);
		lObject.setRgas(aRgas);
		return lObject;
	}

	private WRunGroupAccount pNewWRunGroupAccount(String aId, String aName) {
		WRunGroupAccount lObject = new WRunGroupAccount();
		lObject.setId(aId);
		lObject.setName(aName);
		return lObject;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.webservices.EmergoServicesIF#getRunsForCRM(java.lang.String, java.lang.String)
	 */
	@Override
	public WRun[] getRunsForCRM(String aUserId, String aPassword) {
		long lStart = System.currentTimeMillis();
		// use -1 to get SSpring object for general usage, so not for a single run group account
		String lRgaId = "-1";
	    AxisSSpring sSpring = pGetSSpring(lRgaId);
	    WRun[] lRuns = pCRMUserIdPasswordToRuns(aUserId, aPassword, sSpring);
		long lEnd = System.currentTimeMillis();
		_log.info("EMERGO webservice getRunsForCRM time (msec)" + ":" + (lEnd - lStart));
	    return lRuns;
	}

	private WRun[] pCRMUserIdPasswordToRuns(String aUserId, String aPassword, AxisSSpring sSpring) {
		IEAccount lAccount = ((IAccountManager)sSpring.getBean("accountManager")).getAccount(aUserId, aPassword, true);
		if (lAccount == null) {
			return null;
		}
		List<IERun> lRuns = ((IRunManager)sSpring.getBean("runManager")).getAllRunnableRunsByAccId(lAccount.getAccId());
		if (lRuns == null || lRuns.size() == 0) {
			return null;
		}
		List<List<IERunGroupAccount>> lRunRunGroupAccounts = new ArrayList<List<IERunGroupAccount>>();
		List<IERun> lResultRuns = new ArrayList<IERun>();
		for (IERun lRun : lRuns) {
			if (runIsRunning(lRun)) {
				int lRunId = lRun.getRunId();
				List<IERunGroup> lRunGroups = ((IRunGroupManager)sSpring.getBean("runGroupManager")).getAllRunGroupsByRunId(lRunId);
				if (!(lRunGroups == null || lRunGroups.size() == 0)) {
					List<Integer> lRugIds = new ArrayList<Integer>();
					for (IERunGroup lRunGroup : lRunGroups)
						lRugIds.add(lRunGroup.getRugId());
					List<IERunGroupAccount> rgas = ((IRunGroupAccountManager)sSpring.getBean("runGroupAccountManager")).getAllRunGroupAccountsByRugIds(lRugIds);
					List<IERunGroupAccount> lRunGroupAccounts = new ArrayList<IERunGroupAccount>(0);
					if (lRun.getOpenaccess()) {
						lRunGroupAccounts = getOpenRunGroupAccounts(lRunId,rgas,sSpring);
					} else {
						lRunGroupAccounts = getRunGroupAccounts(lRunId,rgas);
					}
					if (!(lRunGroupAccounts == null || lRunGroupAccounts.size() == 0)) {
						lResultRuns.add(lRun);
						lRunRunGroupAccounts.add(lRunGroupAccounts);
					}
				}
			}
		}
		WRun[] result = new WRun[lResultRuns.size()];
		for (int i=0;i<lResultRuns.size();i++) {
			List<IERunGroupAccount> lRgas = lRunRunGroupAccounts.get(i);
			WRunGroupAccount[] lRunRgas = new WRunGroupAccount[lRgas.size()];
			for (int j=0;j<lRgas.size();j++) {
				IERunGroupAccount lRga = (IERunGroupAccount)lRgas.get(j);
				lRunRgas[j] = pNewWRunGroupAccount("" + lRga.getRgaId(), lRga.getERunGroup().getName());
			}
			result[i] = pNewWRun(""+lResultRuns.get(i).getRunId(), lResultRuns.get(i).getCode(), lResultRuns.get(i).getName(), lRunRgas);
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.webservices.EmergoServicesIF#getAllStates(java.lang.String)
	 */
	@Override
	public WState[] getAllStates(String aRgaId) {
		long lStart = System.currentTimeMillis();
	    AxisSSpring sSpring = pGetSSpring(aRgaId);
	    WState[] lStates = pStateTagsToStates(pGetAllStateTags(aRgaId, sSpring), sSpring);
		long lEnd = System.currentTimeMillis();
		_log.info("EMERGO webservice getAllStates time (msec)" + ":" + (lEnd - lStart));
	    return lStates;
	}

	private WState[] pStateTagsToStates(List<IXMLTag> aStateTags, AxisSSpring sSpring) {
		if (aStateTags == null)
			return null;
		WState[] result = new WState[aStateTags.size()];
		int i = 0;
	    for (IXMLTag lStateTag : aStateTags) {
	    	String lId = lStateTag.getAttribute(AppConstants.keyId);
	    	String lKey = helper.unescapeXML(lStateTag.getChildValue("key"));
	    	String lType = lStateTag.getChildValue("keytype");
	    	String lValue = sSpring.getCurrentTagStatus(lStateTag, AppConstants.statusKeyValue);
    		result[i] = pNewWState(lId, lKey, lType, lValue);
    		i++;
	    }
	    return result;
	}

	private List<IXMLTag> pGetAllStateTags(String aRgaId, AxisSSpring sSpring) {
		String lCaseComponentCode = statesComponentCode;
		String lTagName = "state";
		List<CacIdTagsData> lCacIdDataTags = pGetCaseComponentsDataTags(aRgaId, lCaseComponentCode, sSpring, statusType, lTagName);
		List<IXMLTag> lDataTags = new ArrayList<IXMLTag>();
		for (CacIdTagsData lCacIdDataTag : lCacIdDataTags) {
			lDataTags.addAll(lCacIdDataTag.getTags());
		}
	    return lDataTags;
	}

	private WState pNewWState(String aId, String aKey, String aType, String aValue) {
		WState lObject = new WState();
		lObject.setId(aId);
		lObject.setKey(aKey);
		lObject.setType(aType);
		lObject.setValue(aValue);
		return lObject;
	}


	/* (non-Javadoc)
	 * @see nl.surf.emergo.webservices.EmergoServicesIF#getStateByKey(java.lang.String, java.lang.String)
	 */
	@Override
	public WState getStateByKey(String aRgaId, String aKey) {
		long lStart = System.currentTimeMillis();
	    AxisSSpring sSpring = pGetSSpring(aRgaId);
	    WState[] lStates = pStateTagsToStates(pGetAllStateTags(aRgaId, sSpring), sSpring);
		long lEnd = System.currentTimeMillis();
		WState lState = null;
	    for (int i=0;i<lStates.length;i++) {
	    	if (lState == null && lStates[i].getKey().equals(aKey))
	    		lState = lStates[i];
	    }
	    if (lState != null)
	    	_log.info("EMERGO webservice getStateByKey, rga id: " + aRgaId + 
	    			"; key: " + aKey + 
	    			"; time (msec): " + (lEnd - lStart) + 
	    			"; state: " + lState.getValue() +
	    			"; time: " + new Date());
	    else
	    	_log.info("EMERGO webservice getStateByKey, rga id: " + aRgaId + 
	    			"; key: " + aKey + 
	    			"; time (msec): " + (lEnd - lStart) + 
	    			"; STATE NOT FOUND" +
	    			"; time: " + new Date());
		return lState;
	}

	private CacIdTagData getStateTagByKey(String aRgaId, String aKey) {
	    AxisSSpring sSpring = pGetSSpring(aRgaId);
		String lCaseComponentCode = statesComponentCode;
		String lTagName = "state";
		List<CacIdTagsData> lCacIdDataTags = pGetCaseComponentsDataTags(aRgaId, lCaseComponentCode, sSpring, statusType, lTagName);
		CacIdTagData lCacIdDataTag = null;
		for (CacIdTagsData lCacIdDataTagTemp : lCacIdDataTags) {
		    for (IXMLTag lXmlTag : lCacIdDataTagTemp.getTags()) {
		    	if (lCacIdDataTag == null) {
		    		String lKey = helper.unescapeXML(lXmlTag.getChildValue("key"));
		    		if (lKey.equals(aKey)) {
		    			lCacIdDataTag = new CacIdTagData(lCacIdDataTagTemp.getCacId(), lXmlTag);
		    		}
		    	}
		    }
		}
		return lCacIdDataTag;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.webservices.EmergoServicesIF#setStateByKey(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public String setStateByKey(String aRgaId, String aKey, String aValue) {
		long lStart = System.currentTimeMillis();
		String lResult = setState(aRgaId, getStateTagByKey(aRgaId, aKey), aValue);
		long lEnd = System.currentTimeMillis();
		_log.info("EMERGO webservice setStateByKey, rga id: " + aRgaId + 
				"; key: " + aKey + 
				"; value: " + aValue + 
				"; time (msec): " + (lEnd - lStart) + 
				"; result: " + lResult +
    			"; time: " + new Date());
		return lResult;
	}

	private String setState(String aRgaId, CacIdTagData aCacIdDataTag, String aValue) {
		String lResult = "";
	    if (aCacIdDataTag == null) {
	    	lResult = "error: state not found!";
	    	return lResult;
	    }
	    Integer lCacId = aCacIdDataTag.getCacId();
	    IXMLTag lTag = aCacIdDataTag.getTag();
	    if (lTag == null) {
	    	lResult = "error: state not found!";
	    	return lResult;
	    }
    	String lType = lTag.getChildValue("keytype");
    	if (lType.equals("boolean") && !aValue.equals(AppConstants.statusValueTrue) && !aValue.equals(AppConstants.statusValueFalse))
    		lResult = "error: boolean expected!";
    	else if (lType.equals("number")) {
    		try {
				Double.valueOf(aValue);
			} catch (NumberFormatException e) {
	    		lResult = "error: number expected!";
			}
	    }

	    if (lResult.equals("")) {
	    	// save value if value is changed
		    AxisSSpring sSpring = pGetSSpring(aRgaId);
		    if (sSpring.getAllActiveRunWndsPerRgaId(aRgaId).size() == 0) {
    			lResult = "error: no Emergo player started!";
		    }
		    else {
		    	String lValue = sSpring.getCurrentTagStatus(lTag, AppConstants.statusKeyValue);
		    	if (!lValue.equals(aValue)) {
		    		IECaseComponent lCaseComponent = sSpring.getCaseComponent(lCacId);
		    		if (lCaseComponent == null)
		    			lResult = "error: saving state!";
		    		else {
		    			sSpring.setRunTagStatus(lCaseComponent, lTag, AppConstants.statusKeyValue, aValue, true, statusType, true, false);
		    			pDoCheckUpdateSave(sSpring);
		    			lResult = "success";
		    		}
		    	}
		    	else
		    		lResult = "success";
		    }
	    }

	    if (lResult.equals(""))
    		lResult = "error: unknown failure!";

		return lResult;
	}
	
	public String playMovie(String casus, String studentid, String filmpje) {
		String lResult = "";
		long lStart = System.currentTimeMillis();
		long lEnd = System.currentTimeMillis();
		_log.info("EMERGO webservice playMovie" + ":" + casus + ":" + studentid + ":" + filmpje);
		lResult = "ok";
		_log.info("EMERGO webservice playMovie time (msec)" + ":" + (lEnd - lStart));
		return lResult;
	}
	
	/**
	 * Gets the all exlocations.
	 *
	 * @param aRgaId the a rga id
	 *
	 * @return the all exlocations
	 */
	public Exlocation[] getAllExlocations(String aRgaId) {
		long lStart = System.currentTimeMillis();
	    AxisSSpring sSpring = pGetSSpring(aRgaId);
	    Exlocation[] lExlocations = pExlocationTagsToExlocations(pGetAllExlocationTags(aRgaId, sSpring), sSpring);
		long lEnd = System.currentTimeMillis();
		_log.info("webservice getAllExlocations time (msec)" + ":" + (lEnd - lStart));
	    return lExlocations;
	}

	/**
	 * Gets the current exlocations.
	 *
	 * @param aRgaId the a rga id
	 *
	 * @return the current exlocations
	 */
	public Exlocation[] getCurrentExlocations(String aRgaId) {
		long lStart = System.currentTimeMillis();
	    AxisSSpring sSpring = pGetSSpring(aRgaId);
	    String lLatitude = "";
	    String lLongitude = "";
	    String lContext = pGetContext(aRgaId, "gps", sSpring);
	    if (!lContext.equals("")) {
		    String[] lSensorArray = lContext.split(",");
		    if (lSensorArray.length == 3) {
			    lLatitude = lSensorArray[1];
			    lLongitude = lSensorArray[2];
		    }
	    }
	    Exlocation[] lExlocations = pExlocationTagsToExlocations(pGetExlocationTagsByPosition(aRgaId, lLatitude, lLongitude, sSpring), sSpring);
		long lEnd = System.currentTimeMillis();
		_log.info("webservice getCurrentExlocations time (msec)" + ":" + (lEnd - lStart));
	    return lExlocations;
	}

	/**
	 * Gets the exlocations by position.
	 *
	 * @param aRgaId the a rga id
	 * @param aLatitude the a latitude
	 * @param aLongitude the a longitude
	 *
	 * @return the exlocations by position
	 */
	public Exlocation[] getExlocationsByPosition(String aRgaId, String aLatitude, String aLongitude) {
		long lStart = System.currentTimeMillis();
	    AxisSSpring sSpring = pGetSSpring(aRgaId);
		pSetContext(aRgaId, "gps,"+aLatitude+","+aLongitude,sSpring);
	    Exlocation[] lExlocations = pExlocationTagsToExlocations(pGetExlocationTagsByPosition(aRgaId, aLatitude, aLongitude, sSpring), sSpring);
		long lEnd = System.currentTimeMillis();
		_log.info("webservice getExlocationsByPosition time (msec)" + ":" + (lEnd - lStart));
	    return lExlocations;
	}

	/**
	 * P get all exlocation tags.
	 *
	 * @param aRgaId the a rga id
	 * @param sSpring the s spring
	 *
	 * @return the list
	 */
	private List<IXMLTag> pGetAllExlocationTags(String aRgaId, AxisSSpring sSpring) {
		String lCaseComponentName = "exlocaties";
		String lTagName = "exlocation";
	    List<IXMLTag> lExlocationTags = pGetCaseComponentDataTags(aRgaId, lCaseComponentName, sSpring, statusType, lTagName);
	    for (int i=(lExlocationTags.size()-1);i>=0;i--) {
	    	IXMLTag lExlocationTag = (IXMLTag)lExlocationTags.get(i);
	    	boolean lDeleted = (sSpring.getCurrentTagStatus(lExlocationTag, AppConstants.statusKeyDeleted).equals(AppConstants.statusValueTrue));
	    	if (lDeleted)
	    		lExlocationTags.remove(i);
	    }
	    return lExlocationTags;
	}

	/**
	 * P get all present exlocation tags.
	 *
	 * @param aRgaId the a rga id
	 * @param sSpring the s spring
	 *
	 * @return the list
	 */
	private List<IXMLTag> pGetAllPresentExlocationTags(String aRgaId, AxisSSpring sSpring) {
	    List<IXMLTag> lExlocationTags = pGetAllExlocationTags(aRgaId, sSpring);
	    for (int i=(lExlocationTags.size()-1);i>=0;i--) {
	    	IXMLTag lExlocationTag = (IXMLTag)lExlocationTags.get(i);
	    	boolean lPresent = (!sSpring.getCurrentTagStatus(lExlocationTag, AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse));
	    	if (!lPresent)
	    		lExlocationTags.remove(i);
	    }
	    return lExlocationTags;
	}

	/**
	 * P exlocation tags to exlocations.
	 *
	 * @param aExlocationTags the a exlocation tags
	 * @param sSpring the s spring
	 *
	 * @return the exlocation[]
	 */
	private Exlocation[] pExlocationTagsToExlocations(List<IXMLTag> aExlocationTags, AxisSSpring sSpring ) {
		if (aExlocationTags == null)
			return null;
		Exlocation[] result = new Exlocation[aExlocationTags.size()];
	    for (int i=0;i<aExlocationTags.size();i++) {
	    	IXMLTag lExlocationTag = (IXMLTag)aExlocationTags.get(i);
	    	String lId = lExlocationTag.getAttribute(AppConstants.keyId);
	    	String lName = helper.unescapeXML(lExlocationTag.getChildValue("name"));
	    	String lLatitude = lExlocationTag.getChildValue("exlatitude");
	    	String lLongitude = lExlocationTag.getChildValue("exlongitude");
	    	String lRadius = lExlocationTag.getChildValue("exradius");
    		result[i] = pNewExlocation(lId, lName, lLatitude, lLongitude, lRadius);
	    }
	    return result;
	}

	/**
	 * P new exlocation.
	 *
	 * @param aId the a id
	 * @param aName the a name
	 * @param aLatitude the a latitude
	 * @param aLongitude the a longitude
	 * @param aRadius the a radius
	 *
	 * @return the exlocation
	 */
	private Exlocation pNewExlocation(String aId, String aName, String aLatitude, String aLongitude, String aRadius) {
		Exlocation lObject = new Exlocation();
		lObject.setId(aId);
		lObject.setName(aName);
		lObject.setLatitude(aLatitude);
		lObject.setLongitude(aLongitude);
		lObject.setRadius(aRadius);
		return lObject;
	}

	/**
	 * P get exlocation tags by position.
	 *
	 * @param aRgaId the a rga id
	 * @param aLatitude the a latitude
	 * @param aLongitude the a longitude
	 * @param sSpring the s spring
	 *
	 * @return the list
	 */
	private List<IXMLTag> pGetExlocationTagsByPosition(String aRgaId, String aLatitude, String aLongitude, AxisSSpring sSpring) {
	    List<IXMLTag> lExlocationTags = pGetAllPresentExlocationTags(aRgaId, sSpring);
	    if (lExlocationTags.size() == 0)
	    	return null;
	    if ((aLatitude.equals("")) || (aLongitude.equals("")))
	    	return null;
		double lDLatitude = Double.parseDouble(aLatitude);
		double lDLongitude = Double.parseDouble(aLongitude);
	    List<IXMLTag> lExlocationTagsByPosition = new ArrayList<IXMLTag>(0);
	    for (IXMLTag lExlocationTag : lExlocationTags) {
	    	String lLatitude = lExlocationTag.getChildValue("exlatitude");
	    	String lLongitude = lExlocationTag.getChildValue("exlongitude");
	    	String lRadius = lExlocationTag.getChildValue("exradius");
	    	boolean lOk = !(lLatitude.equals("") || lLongitude.equals("") || lRadius.equals(""));
	    	if (lOk) {
				boolean lWithinExlocation = pPositionWithinExlocation(lDLatitude, lDLongitude, Double.parseDouble(lLatitude), Double.parseDouble(lLongitude), Double.parseDouble(lRadius));
		    	if (lWithinExlocation)
		    		lExlocationTagsByPosition.add(lExlocationTag);
	    	}
	    }
		return lExlocationTagsByPosition;
	}

	/**
	 * P check exlocations.
	 *
	 * @param aRgaId the a rga id
	 * @param aLatitude the a latitude
	 * @param aLongitude the a longitude
	 * @param sSpring the s spring
	 */
	private void pCheckExlocations(String aRgaId, String aLatitude, String aLongitude, AxisSSpring sSpring) {
		String lCaseComponentName = "exlocaties";
		String lTagName = "exlocation";
	    List<IXMLTag> lExlocationTags = pGetCaseComponentDataTags(aRgaId, lCaseComponentName, sSpring, statusType, lTagName);
	    if (lExlocationTags.size() == 0)
	    	return;
		double lDLatitude = Double.parseDouble(aLatitude);
		double lDLongitude = Double.parseDouble(aLongitude);
		IECaseComponent lCaseComponent = sSpring.getCaseComponent("", lCaseComponentName);
	    String lKey = AppConstants.statusKeyPresent;
	    for (int i=(lExlocationTags.size()-1);i>=0;i--) {
	    	IXMLTag lExlocationTag = (IXMLTag)lExlocationTags.get(i);
	    	boolean lPresent = (!sSpring.getCurrentRunTagStatus(lCaseComponent, lExlocationTag, lKey, statusType).equals(AppConstants.statusValueFalse));
	    	if (!lPresent)
	    		lExlocationTags.remove(i);
	    }
	    lKey = AppConstants.statusKeyOpened;
	    for (IXMLTag lExlocationTag : lExlocationTags) {
	    	String lLatitude = lExlocationTag.getChildValue("exlatitude");
	    	String lLongitude = lExlocationTag.getChildValue("exlongitude");
	    	String lRadius = lExlocationTag.getChildValue("exradius");
	    	boolean lOk = !(lLatitude.equals("") || lLongitude.equals("") || lRadius.equals(""));
	    	if (lOk) {
/*
				IXMLTag lTag = sSpring.newRunTag("exlocation", lExlocationTag.getAttribute(AppConstants.keyId));
				lTag.setAttribute("update_statuskey", lKey);
				lTag.setAttribute("update_checkscript", "true");
				lTag.setAttribute(AppConstants.keyId, "2");
*/
				boolean lWithinExlocation = pPositionWithinExlocation(lDLatitude, lDLongitude, Double.parseDouble(lLatitude), Double.parseDouble(lLongitude), Double.parseDouble(lRadius));
		    	boolean lOpened = (sSpring.getCurrentRunTagStatus(lCaseComponent, lExlocationTag, lKey, statusType).equals(AppConstants.statusValueTrue));
		    	if (lWithinExlocation && (!lOpened)) {
			    	sSpring.setRunTagStatus(lCaseComponent, lExlocationTag, lKey, AppConstants.statusValueTrue, true, statusType, true, false);
//					lTag.setAttribute("update_statusvalue", AppConstants.statusValueTrue);
//					sSpring.addUpdateRunGroupTag(lCaseComponent, lTag);
		    	}
		    	if ((!lWithinExlocation) && lOpened) {
			    	sSpring.setRunTagStatus(lCaseComponent, lExlocationTag, lKey, AppConstants.statusValueFalse, true, statusType, true, false);
//					lTag.setAttribute("update_statusvalue", AppConstants.statusValueFalse);
//					sSpring.addUpdateRunGroupTag(lCaseComponent, lTag);
		    	}
	    	}
	    }
		return;
	}

	/**
	 * P position within exlocation.
	 *
	 * @param aLatitude the a latitude
	 * @param aLongitude the a longitude
	 * @param aLocLatitude the a loc latitude
	 * @param aLocLongitude the a loc longitude
	 * @param aLocRadius the a loc radius
	 *
	 * @return true, if successful
	 */
	private boolean pPositionWithinExlocation(double aLatitude, double aLongitude, double aLocLatitude, double aLocLongitude, double aLocRadius) {
		double lLatDistance = Math.abs((aLatitude - aLocLatitude) * meterperdegree);
		double lLonDistance = Math.abs((aLongitude - aLocLongitude) * meterperdegree);
		double lDistance = Math.sqrt((lLatDistance*lLatDistance) + (lLonDistance*lLonDistance));
		return (lDistance <= aLocRadius);
	}

	/**
	 * Gets the context.
	 *
	 * @param aRgaId the a rga id
	 * @param aSensorType the a sensor type
	 *
	 * @return the context
	 */
	public String getContext(String aRgaId, String aSensorType) {
		long lStart = System.currentTimeMillis();
	    String lResult = "";
	    if ((aRgaId != null) && (!aRgaId.equals(""))) {
	    	if ((aSensorType != null) && (!aSensorType.equals(""))) {
	    		lResult = pGetContext(aRgaId, aSensorType, pGetSSpring(aRgaId));
	    	}
	    }
		long lEnd = System.currentTimeMillis();
		_log.info("webservice getContext time (msec)" + ":" + (lEnd - lStart));
		return lResult;
	}

	/**
	 * P get context.
	 *
	 * @param aRgaId the a rga id
	 * @param aSensorType the a sensor type
	 * @param sSpring the s spring
	 *
	 * @return the string
	 */
	private String pGetContext(String aRgaId, String aSensorType, AxisSSpring sSpring) {
	    String lResult = "";
	    if ((aRgaId == null) || (aRgaId.equals("")))
	    	return lResult;
	    if ((aSensorType == null) || (aSensorType.equals("")))
	    	return lResult;
		sSpring.setRunStatus(AppConstants.runStatusRun);
	    sSpring.setRunGroupAccount(Integer.parseInt(aRgaId));
	    if (sSpring.getRunGroupAccount() == null)
	    	return lResult;
		IECaseComponent lCaseCaseComponent = sSpring.getCaseComponent("case", "");
	    if (lCaseCaseComponent == null)
	    	return lResult;
	    if (aSensorType.equals("gps")) {
	    	String lKey = AppConstants.statusKeyExLatitude;
	    	String lValue1 = sSpring.getCurrentRunComponentStatus(lCaseCaseComponent, lKey, statusType);
	    	lKey = AppConstants.statusKeyExLongitude;
	    	String lValue2 = sSpring.getCurrentRunComponentStatus(lCaseCaseComponent, lKey, statusType);
//	    	lKey = AppConstants.statusKeyExPosition;
//	    	String lValue3 = sSpring.getCurrentRunComponentStatus(lCaseCaseComponent, lKey, statusType);
	    	if (!((lValue1.equals("")) || (lValue2.equals(""))))
	    		lResult = aSensorType + "," + lValue1 + "," + lValue2;
	    }
	    return lResult;
	}

	/**
	 * Sets context.
	 *
	 * @param aRgaId the a rga id
	 * @param aSensorData the a sensor data
	 *
	 * @return the string
	 */
	public String setContext(String aRgaId, String aSensorData) {
		long lStart = System.currentTimeMillis();
	    String lResult = "false";
	    if ((aRgaId != null) && (!aRgaId.equals(""))) {
	    	if ((aSensorData != null) && (!aSensorData.equals(""))) {
	    		lResult = pSetContext(aRgaId, aSensorData, pGetSSpring(aRgaId));
	    	}
	    }
		long lEnd = System.currentTimeMillis();
		_log.info("webservice setContext time (msec)" + ":" + (lEnd - lStart));
		return lResult;
	}

	/**
	 * P set context.
	 *
	 * @param aRgaId the a rga id
	 * @param aSensorData the a sensor data
	 * @param sSpring the s spring
	 *
	 * @return the string
	 */
	private String pSetContext(String aRgaId, String aSensorData, AxisSSpring sSpring) {
	    String lResult = "false";
	    if ((aRgaId == null) || (aRgaId.equals("")))
	    	return lResult;
	    if ((aSensorData == null) || (aSensorData.equals("")))
	    	return lResult;
		sSpring.setRunStatus(AppConstants.runStatusRun);
	    sSpring.setRunGroupAccount(Integer.parseInt(aRgaId));
	    if (sSpring.getRunGroupAccount() == null)
	    	return lResult;
		IECaseComponent lCaseCaseComponent = sSpring.getCaseComponent("case", "");
	    if (lCaseCaseComponent == null)
	    	return lResult;
	    String[] lSensorArray = aSensorData.split(",");
	    String lType = lSensorArray[0];
	    if (lType.equals("gps")) {
	    	String lKey = AppConstants.statusKeyExLatitude;
	    	String lValue = lSensorArray[1];
	    	String lSavedValue = sSpring.getCurrentRunComponentStatus(lCaseCaseComponent, lKey, statusType);
/*
			IXMLTag lStatusTag = sSpring.newRunTag("componentstatus", "");
			lStatusTag.setAttribute("update_statuskey", lKey);
			lStatusTag.setAttribute("update_statusvalue", lValue);
			lStatusTag.setAttribute("update_checkscript", "true");
*/
			STriggeredReference lTriggeredReference = new STriggeredReference();
			lTriggeredReference.setCaseComponent(lCaseCaseComponent);
			lTriggeredReference.setStatusKey(lKey);
			lTriggeredReference.setStatusValue(lValue);
	    	if (!lValue.equals(lSavedValue)) {
		    	lResult = "true";
		    	sSpring.setRunComponentStatus(lTriggeredReference, true, statusType, true);
//				sSpring.addUpdateRunGroupTag(lCaseCaseComponent, lStatusTag);
	    	}
	    	lKey = AppConstants.statusKeyExLongitude;
	    	lValue = lSensorArray[2];
	    	lSavedValue = sSpring.getCurrentRunComponentStatus(lCaseCaseComponent, lKey, statusType);
	    	if (!lValue.equals(lSavedValue)) {
		    	lResult = "true";
		    	sSpring.setRunComponentStatus(lTriggeredReference, true, statusType, true);
//				sSpring.addUpdateRunGroupTag(lCaseCaseComponent, lStatusTag);
	    	}
	    	lKey = AppConstants.statusKeyExPosition;
	    	lValue = lSensorArray[1] + "+" +lSensorArray[2];
	    	lSavedValue = sSpring.getCurrentRunComponentStatus(lCaseCaseComponent, lKey, statusType);
	    	if (!lValue.equals(lSavedValue)) {
		    	lResult = "true";
		    	sSpring.setRunComponentStatus(lTriggeredReference, true, statusType, true);
//				sSpring.addUpdateRunGroupTag(lCaseCaseComponent, lStatusTag);
	    	}
	    	if (lResult.equals("true")) {
	    		pCheckExlocations(aRgaId, lSensorArray[1], lSensorArray[2], sSpring);
	    		pDoCheckUpdateSave(sSpring);
	    	}
	    }
	    return lResult;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.webservices.EmergoServicesIF#getAllUrls(java.lang.String)
	 */
	@Override
	public Url[] getAllUrls(String aRgaId) {
		long lStart = System.currentTimeMillis();
	    AxisSSpring sSpring = pGetSSpring(aRgaId);
	    List<IXMLTag> lUrlTags = pGetAllUrlTags(aRgaId, sSpring);
	    Url[] lUrls = pUrlTagsToUrls(lUrlTags, sSpring);
		long lEnd = System.currentTimeMillis();
		_log.info("webservice getAllUrls time (msec)" + ":" + (lEnd - lStart));
	    return lUrls;
	}

	/**
	 * Gets the current urls.
	 *
	 * @param aRgaId the a rga id
	 *
	 * @return the current urls
	 */
	public Url[] getCurrentUrls(String aRgaId) {
		long lStart = System.currentTimeMillis();
	    AxisSSpring sSpring = pGetSSpring(aRgaId);
	    Url[] lUrls = pUrlTagsToUrls(pGetAllPresentUrlTags(aRgaId, sSpring), sSpring);
		long lEnd = System.currentTimeMillis();
		_log.info("webservice getCurrentUrls time (msec)" + ":" + (lEnd - lStart));
	    return lUrls;
	}

	/**
	 * Gets the urls by position.
	 *
	 * @param aRgaId the a rga id
	 * @param aLatitude the a latitude
	 * @param aLongitude the a longitude
	 *
	 * @return the urls by position
	 */
	public Url[] getUrlsByPosition(String aRgaId, String aLatitude, String aLongitude) {
		long lStart = System.currentTimeMillis();
	    AxisSSpring sSpring = pGetSSpring(aRgaId);
		pSetContext(aRgaId, "gps,"+aLatitude+","+aLongitude,sSpring);
	    Url[] lUrls = pUrlTagsToUrls(pGetAllPresentUrlTags(aRgaId, sSpring), sSpring);
		long lEnd = System.currentTimeMillis();
		_log.info("webservice getUrlsByPosition time (msec)" + ":" + (lEnd - lStart));
	    return lUrls;
	}

	/**
	 * P get all url tags.
	 *
	 * @param aRgaId the a rga id
	 * @param sSpring the s spring
	 *
	 * @return the list
	 */
	private List<IXMLTag> pGetAllUrlTags(String aRgaId, AxisSSpring sSpring) {
		String lCaseComponentName = "urls";
		String lTagName = "url";
	    List<IXMLTag> lUrlTags = pGetCaseComponentDataTags(aRgaId, lCaseComponentName, sSpring, statusType, lTagName);
	    for (int i=(lUrlTags.size()-1);i>=0;i--) {
	    	IXMLTag lUrlTag = (IXMLTag)lUrlTags.get(i);
	    	boolean lDeleted = (sSpring.getCurrentTagStatus(lUrlTag, AppConstants.statusKeyDeleted).equals(AppConstants.statusValueTrue));
	    	if (lDeleted)
	    		lUrlTags.remove(i);
	    }
	    return lUrlTags;
	}

	/**
	 * P get all present url tags.
	 *
	 * @param aRgaId the a rga id
	 * @param sSpring the s spring
	 *
	 * @return the list
	 */
	private List<IXMLTag> pGetAllPresentUrlTags(String aRgaId, AxisSSpring sSpring) {
	    List<IXMLTag> lUrlTags = pGetAllUrlTags(aRgaId, sSpring);
	    for (int i=(lUrlTags.size()-1);i>=0;i--) {
	    	IXMLTag lUrlTag = (IXMLTag)lUrlTags.get(i);
	    	boolean lPresent = (!sSpring.getCurrentTagStatus(lUrlTag, AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse));
	    	if (!lPresent)
	    		lUrlTags.remove(i);
	    }
	    return lUrlTags;
	}

	/**
	 * P url tags to urls.
	 *
	 * @param aUrlTags the a url tags
	 * @param sSpring the s spring
	 *
	 * @return the url[]
	 */
	private Url[] pUrlTagsToUrls(List<IXMLTag> aUrlTags, AxisSSpring sSpring ) {
		if (aUrlTags == null)
			return null;
		Url[] result = new Url[aUrlTags.size()];
	    for (int i=0;i<aUrlTags.size();i++) {
	    	IXMLTag lUrlTag = (IXMLTag)aUrlTags.get(i);
	    	String lId = lUrlTag.getAttribute(AppConstants.keyId);
	    	String lName = helper.unescapeXML(lUrlTag.getChildValue("name"));
//	    	String lUrl = "";
	    	String lUrl = sSpring.getSBlobHelper().getUrl(lUrlTag);
	    	byte[] lData = null;
	    	String lDescription = helper.unescapeXML(lUrlTag.getChildValue("description"));
	    	String lLatitude = lUrlTag.getChildValue("exlatitude");
	    	String lLongitude = lUrlTag.getChildValue("exlongitude");
/*	    	IXMLTag lBlobTag = lUrlTag.getChild("blob");
	    	if (lBlobTag != null) {
	    		String lBlobtype = lBlobTag.getAttribute(AppConstants.keyBlobtype);
	    		if (lBlobtype.equals(AppConstants.blobtypeDatabase)){
	    			lData = sSpring.getContent(lBlobTag.getValue());
	    		}
	    		if (lBlobtype.equals(AppConstants.blobtypeExtUrl)) {
	    	    	IEBlob lBlob = sSpring.getBlob(lBlobTag.getValue());
	    	    	if (lBlob != null)
	    	    		lUrl = lBlob.getUrl();
	    		}
	    	} */
	    	result[i] = pNewUrl(lId, lName, lUrl, lData, lDescription, lLatitude, lLongitude);
	    }
	    return result;
	}

	/**
	 * P new url.
	 *
	 * @param aId the a id
	 * @param aName the a name
	 * @param aUrl the a url
	 * @param aData the a data
	 * @param aDescription the a description
	 * @param aLatitude the a latitude
	 * @param aLongitude the a longitude
	 *
	 * @return the url
	 */
	private Url pNewUrl(String aId, String aName, String aUrl, byte[] aData, String aDescription, String aLatitude, String aLongitude) {
		Url lObject = new Url();
		lObject.setId(aId);
		lObject.setName(aName);
		lObject.setUrl(aUrl);
		lObject.setData(aData);
		lObject.setDescription(aDescription);
		lObject.setLatitude(aLatitude);
		lObject.setLongitude(aLongitude);
		return lObject;
	}

/*	public Url[] getUrlsByPosition(String aRgaId, String aLatitude, String aLongitude) {
		long lStart = System.currentTimeMillis();
		String lCaseComponentName = "urls";
	    AxisSSpring sSpring = pGetSSpring(aRgaId);
		pSetContext(aRgaId, "gps,"+aLatitude+","+aLongitude,sSpring);
	    Url[] result = null;
	    IXMLTag lRootTag = pGetCaseComponentDataRootTag(aRgaId, lCaseComponentName, sSpring, statusType);
	    if (lRootTag == null) {
			long lEnd = System.currentTimeMillis();
			_log.info("webservice getUrlsByPosition time (msec)" + ":" + (lEnd - lStart));
	    	return result;
	    }
	    IXMLTag lContentTag = lRootTag.getChild(AppConstants.contentElement);
	    if (lContentTag == null){
			long lEnd = System.currentTimeMillis();
			_log.info("webservice getUrlsByPosition time (msec)" + ":" + (lEnd - lStart));
	    	return result;
	    }
	    List lUrlTags = lContentTag.getChilds("url");
	    if (lUrlTags.size() == 0){
			long lEnd = System.currentTimeMillis();
			_log.info("webservice getUrlsByPosition time (msec)" + ":" + (lEnd - lStart));
	    	return result;
	    }
		IECaseComponent lCaseComponent = sSpring.getCaseComponent("", lCaseComponentName);
	    String lKey = AppConstants.statusKeyPresent;
	    List lPresentUrlTags = new ArrayList(0);
	    for (int i=0;i<lUrlTags.size();i++) {
	    	IXMLTag lUrlTag = (IXMLTag)lUrlTags.get(i);
	    	boolean lPresent = (!sSpring.getCurrentRunTagStatus(lCaseComponent, lUrlTag, lKey, statusType).equals(AppConstants.statusValueFalse));
	    	if (lPresent)
	    		lPresentUrlTags.add(lUrlTag);
	    }
	    if (lPresentUrlTags.size() == 0){
			long lEnd = System.currentTimeMillis();
			_log.info("webservice getUrlsByPosition time (msec)" + ":" + (lEnd - lStart));
	    	return result;
	    }
	    result = new Url[lPresentUrlTags.size()];
	    for (int i=0;i<lPresentUrlTags.size();i++) {
	    	IXMLTag lUrlTag = (IXMLTag)lPresentUrlTags.get(i);
    		result[i] = pNewUrl(lUrlTag.getAttribute(AppConstants.keyId), lUrlTag.getChildValue("name"), sSpring.getUrl(lUrlTag), null, lUrlTag.getChildValue("description"), lUrlTag.getChildValue("exlatitude"), lUrlTag.getChildValue("exlongitude"));
	    }
		long lEnd = System.currentTimeMillis();
		_log.info("webservice getUrlsByPosition time (msec)" + ":" + (lEnd - lStart));
	    return result;
	} */

/*	public Url[] getUrls(String aRgaId) {
		long lStart = System.currentTimeMillis();
		String lCaseComponentName = "urls";
	    AxisSSpring sSpring = pGetSSpring(aRgaId);
	    Url[] result = null;
	    IXMLTag lRootTag = pGetCaseComponentDataRootTag(aRgaId, lCaseComponentName, sSpring, statusType);
	    if (lRootTag == null) {
			long lEnd = System.currentTimeMillis();
			_log.info("webservice getUrls time (msec)" + ":" + (lEnd - lStart));
	    	return result;
	    }
	    IXMLTag lContentTag = lRootTag.getChild(AppConstants.contentElement);
	    if (lContentTag == null){
			long lEnd = System.currentTimeMillis();
			_log.info("webservice getUrls time (msec)" + ":" + (lEnd - lStart));
	    	return result;
	    }
	    List lUrlTags = lContentTag.getChilds("url");
	    if (lUrlTags.size() == 0){
			long lEnd = System.currentTimeMillis();
			_log.info("webservice getUrls time (msec)" + ":" + (lEnd - lStart));
	    	return result;
	    }
		IECaseComponent lCaseComponent = sSpring.getCaseComponent("", lCaseComponentName);
	    String lKey = AppConstants.statusKeyPresent;
	    List lPresentUrlTags = new ArrayList(0);
	    for (int i=0;i<lUrlTags.size();i++) {
	    	IXMLTag lUrlTag = (IXMLTag)lUrlTags.get(i);
	    	boolean lPresent = (!sSpring.getCurrentRunTagStatus(lCaseComponent, lUrlTag, lKey, statusType).equals(AppConstants.statusValueFalse));
	    	if (lPresent)
	    		lPresentUrlTags.add(lUrlTag);
	    }
	    if (lPresentUrlTags.size() == 0){
			long lEnd = System.currentTimeMillis();
			_log.info("webservice getUrls time (msec)" + ":" + (lEnd - lStart));
	    	return result;
	    }
	    result = new Url[lPresentUrlTags.size()];
	    for (int i=0;i<lPresentUrlTags.size();i++) {
	    	IXMLTag lUrlTag = (IXMLTag)lPresentUrlTags.get(i);
    		result[i] = pNewUrl(lUrlTag.getAttribute(AppConstants.keyId), lUrlTag.getChildValue("name"), sSpring.getUrl(lUrlTag), null, lUrlTag.getChildValue("description"), lUrlTag.getChildValue("exlatitude"), lUrlTag.getChildValue("exlongitude"));
	    }
		long lEnd = System.currentTimeMillis();
		_log.info("webservice getUrls time (msec)" + ":" + (lEnd - lStart));
	    return result;
	} */

	/**
	 * Gets the urls by name.
	 *
	 * @param aRgaId the a rga id
	 * @param aName the a name
	 *
	 * @return the urls by name
	 */
	public Url[] getUrlsByName(String aRgaId, String aName) {
		long lStart = System.currentTimeMillis();
	    if ((aRgaId == null) || (aRgaId.equals(""))) return null;
		aName = aName.trim();
	    AxisSSpring sSpring = pGetSSpring(aRgaId);
	    List<IXMLTag> lUrlTags = pGetAllUrlTags(aRgaId, sSpring);
	    for (int i=(lUrlTags.size()-1);i>=0;i--) {
	    	IXMLTag lUrlTag = (IXMLTag)lUrlTags.get(i);
	    	String lName = helper.unescapeXML(lUrlTag.getChildValue("name"));
	    	if (!lName.equals(aName))
	    		lUrlTags.remove(i);
	    }
	    Url[] lUrls = pUrlTagsToUrls(lUrlTags, sSpring);
		long lEnd = System.currentTimeMillis();
		_log.info("webservice getUrlsByName time (msec)" + ":" + (lEnd - lStart));
	    return lUrls;
	}

	/**
	 * Sets url.
	 *
	 * @param aRgaId the a rga id
	 * @param aUrl the a url
	 *
	 * @return the string
	 */
	public String setUrl(String aRgaId, Url aUrl) {
		long lStart = System.currentTimeMillis();
	    String lResult = "false";
	    if ((aRgaId != null) && (!aRgaId.equals(""))) {
	    	if ((aUrl.getName() != null) && (!aUrl.getName().equals(""))) {
	    	    AxisSSpring sSpring = pGetSSpring(aRgaId);
    			String lCaseComponentName = "urls";
    			IXMLTag lContentTag = pGetCaseComponentDataContentTag(aRgaId, lCaseComponentName, sSpring, statusType);
   				if (lContentTag != null) {
   					if (!pAddNewUrlTag(lCaseComponentName, lContentTag, aUrl, sSpring))
   						pDoCheckUpdateSave(sSpring);
	    		}
	    	}
	    }
		long lEnd = System.currentTimeMillis();
		_log.info("webservice setUrl time (msec)" + ":" + (lEnd - lStart));
    	lResult = "true";
		return lResult;
	}

	/**
	 * P add new url tag.
	 *
	 * @param aCaseComponentName the a case component name
	 * @param aParentTag the a parent tag
	 * @param aUrl the a url
	 * @param sSpring the s spring
	 *
	 * @return true, if successful
	 */
	private boolean pAddNewUrlTag (String aCaseComponentName, IXMLTag aParentTag, Url aUrl, AxisSSpring sSpring) {
		IECaseComponent lCaseComponent = sSpring.getCaseComponent("", aCaseComponentName);
		if (lCaseComponent == null)
			return false;
		CRunContentHelper cComponent = new CRunContentHelper(null, "", lCaseComponent, null, sSpring);
		cComponent.setRunStatusType(statusType);
		String lXmlDef = cComponent.getXmlDef();
		IXmlManager	xmlManager = sSpring.getXmlManager();

		IXMLTag lItem = xmlManager.newNodeTag(lXmlDef, "url");

		// tags that have to do with blobs are returned in lBlobTags
		// can be empty if no blobs or blobs are not changed
		List<Hashtable<String,Object>> lBlobTagDatas = new ArrayList<Hashtable<String,Object>>(0);
		// errors are returned in lErrors
		// empty if no errors
		List<String[]> lErrors = new ArrayList<String[]>(0);

		// returns true if there are any changes
		// only errors in conditions, actions or refs are returned
		if (!pDataToNode(cComponent, sSpring, aUrl, lItem, lBlobTagDatas, lErrors))
			return false;
		if (lErrors.size() > 0)
			return false;
		// check if errors in tag
		String lKey = lItem.getDefAttribute(AppConstants.defKeyKey);
		IXMLTag lParentTag = null;
		lParentTag = aParentTag;
		List<String> lWarnings = new ArrayList<String>(0);
		if (xmlManager.checkTagChildTagValues(lKey, lItem, lParentTag, true, false,
				lErrors, lWarnings))
			return false;
		// if ok handle blobtags
		pHandleBlobTagDatas(cComponent, lBlobTagDatas);
		lItem.setParentTag(lParentTag);
		lParentTag.getChildTags().add(lItem);
		cComponent.newChildNode(lItem, lParentTag, true);
		return true;
	}

	/**
	 * P data to node.
	 *
	 * @param cComponent the c component
	 * @param sSpring the s spring
	 * @param aUrl the a url
	 * @param aNodeTag the a node tag
	 * @param aBlobTagDatas the a blob tag datas
	 * @param aErrors the a errors
	 *
	 * @return true, if successful
	 */
	private boolean pDataToNode(CRunContentHelper cComponent, AxisSSpring sSpring, Url aUrl, IXMLTag aNodeTag, List<Hashtable<String,Object>> aBlobTagDatas, List<String[]> aErrors) {
		cComponent.getXmlNodeChild(aNodeTag,"name").setValue(sSpring.escapeXML(aUrl.getName().trim()));

		String lUrl = sSpring.escapeXML(aUrl.getUrl().trim());
		String lBlobtype = AppConstants.blobtypeDatabase;
		if (!lUrl.equals(""))
			lBlobtype = AppConstants.blobtypeExtUrl;
		if (lBlobtype.equals(AppConstants.blobtypeDatabase)) {
			Hashtable<String,Object> lHBlobTagData = new Hashtable<String,Object>(0);
			lHBlobTagData.put("childtag", aNodeTag.getChild("blob"));
			lHBlobTagData.put("attribute", "");
			lHBlobTagData.put(AppConstants.keyBlobtype, lBlobtype);
			lHBlobTagData.put("value", "");
			if (aUrl.getData() != null) {
				lHBlobTagData.put("content", aUrl.getData());
			}
			aBlobTagDatas.add(lHBlobTagData);
		}
		if (lBlobtype.equals(AppConstants.blobtypeExtUrl)) {
			Hashtable<String,Object> lHBlobTagData = new Hashtable<String,Object>(0);
			lHBlobTagData.put("childtag", aNodeTag.getChild("blob"));
			lHBlobTagData.put("attribute", "");
			lHBlobTagData.put(AppConstants.keyBlobtype, lBlobtype);
			lHBlobTagData.put("value", "");
			lHBlobTagData.put("text", lUrl);
			aBlobTagDatas.add(lHBlobTagData);
		}
		cComponent.getXmlNodeChild(aNodeTag,"blob").setAttribute(AppConstants.keyBlobtype, lBlobtype);
		cComponent.getXmlNodeChild(aNodeTag,"blob").setAttribute(AppConstants.keyMediatype, "");
		cComponent.getXmlNodeChild(aNodeTag,"description").setValue(sSpring.escapeXML(aUrl.getDescription().trim()));
		cComponent.getXmlNodeChild(aNodeTag,"exlatitude").setValue(aUrl.getLatitude().trim());
		cComponent.getXmlNodeChild(aNodeTag,"exlongitude").setValue(aUrl.getLongitude().trim());
		return true;
	}

	/**
	 * P handle blob tag datas.
	 *
	 * @param cComponent the c component
	 * @param aBlobTagDatas the a blob tag datas
	 */
	private void pHandleBlobTagDatas(CRunContentHelper cComponent, List<Hashtable<String,Object>> aBlobTagDatas) {
		for (Hashtable<String,Object> lHBlobTagData : aBlobTagDatas) {
			IXMLTag lChildTag = (IXMLTag) lHBlobTagData.get("childtag");
			String lAttribute = (String) lHBlobTagData.get("attribute");
			String lBlobtype = (String) lHBlobTagData.get(AppConstants.keyBlobtype);
			String lValue = (String) lHBlobTagData.get("value");
			if (lBlobtype.equals(AppConstants.blobtypeDatabase)) {
				byte[] lContent = null;
				if (lHBlobTagData.containsKey("content"))
					lContent = (byte[]) lHBlobTagData.get("content");
				if (lContent == null) {
					cComponent.deleteBlob(lValue);
					lValue = "";
				} else
					lValue = cComponent.setBlobContent(lValue, lContent);
			} else { // int_url or ext_url
				String lText = (String) lHBlobTagData.get("text");
				lValue = cComponent.setBlobUrl(lValue, lText);
			}
			if (lAttribute.equals(""))
				lChildTag.setValue(lValue);
			else
				lChildTag.setAttribute(lAttribute, lValue);
		}
	}

	/**
	 * Deletes url.
	 *
	 * @param aRgaId the a rga id
	 * @param aUrl the a url
	 *
	 * @return the string
	 */
	public String deleteUrl(String aRgaId, Url aUrl) {
		long lStart = System.currentTimeMillis();
	    String result = "false";
	    if ((aRgaId != null) && (!aRgaId.equals(""))) {
		    AxisSSpring sSpring = pGetSSpring(aRgaId);
    		List<IXMLTag> lUrlTags = pGetAllUrlTags(aRgaId, sSpring);
    		IECaseComponent lCaseComponent = sSpring.getCaseComponent("", "urls");
    		if (lCaseComponent != null) {
    			CRunContentHelper cComponent = new CRunContentHelper(null, "url", lCaseComponent, null, sSpring);
    			cComponent.setRunStatusType(statusType);
    			boolean lCheckId = (!aUrl.getId().equals(""));
    			boolean lCheckName = (!lCheckId) && (!aUrl.getName().equals(""));
    			for (IXMLTag lUrlTag : lUrlTags) {
    				boolean lDelete = false;
    				if ((lCheckId) && (aUrl.getId().equals(lUrlTag.getAttribute(AppConstants.keyId))))
    					lDelete = true;
    				if ((lCheckName) && (aUrl.getName().equals(lUrlTag.getChildValue("name"))))
    					lDelete = true;
    				if (lDelete)
    					cComponent.deleteNode(lUrlTag, true);
    			}
    			pDoCheckUpdateSave(sSpring);
    		}
	    }
		long lEnd = System.currentTimeMillis();
		_log.info("webservice deleteUrl time (msec)" + ":" + (lEnd - lStart));
    	result = "true";
		return result;
	}

	/**
	 * Gets the case component data.
	 *
	 * @param aRgaId the a rga id
	 * @param aCaseComponentName the a case component name
	 *
	 * @return the case component data
	 */
	public String getCaseComponentData(String aRgaId, String aCaseComponentName) {
		long lStart = (new Date()).getTime();
	    String lXmlData = "";
	    AxisSSpring sSpring = pGetSSpring(aRgaId);
	    IXMLTag lRootTag = pGetCaseComponentDataRootTag(aRgaId, aCaseComponentName, sSpring, statusType);
	    if (lRootTag == null)
	    	return lXmlData;
	    lXmlData = sSpring.getXmlManager().xmlTreeToDoc(lRootTag);
	    pDoCheckUpdateSave(sSpring);
		long lEnd = (new Date()).getTime();
		_log.info("webservice getCaseComponentData time (msec)" + ":" + (lEnd - lStart));
	    return lXmlData;
	}

	/**
	 * P get case component data root tag.
	 *
	 * @param aRgaId the a rga id
	 * @param aCaseComponentName the a case component name
	 * @param sSpring the s spring
	 * @param aStatusType the a status type
	 *
	 * @return the iXML tag
	 */
	private IXMLTag pGetCaseComponentDataRootTag(String aRgaId, String aCaseComponentName, AxisSSpring sSpring, String aStatusType) {
		sSpring.setRunStatus(AppConstants.runStatusRun);
	    sSpring.setRunGroupAccount(Integer.parseInt(aRgaId));
	    if (sSpring.getRunGroupAccount() == null)
	    	return null;
		IECaseComponent lCaseComponent = sSpring.getCaseComponent("", aCaseComponentName);
	    if (lCaseComponent == null)
	    	return null;
	    IXMLTag lRootTag = sSpring.getXmlDataPlusRunStatusTree(lCaseComponent, aStatusType);
	    return lRootTag;
	}

	/**
	 * P get case components data root tags.
	 *
	 * @param aRgaId the a rga id
	 * @param aCaseComponentCode the a case component code
	 * @param sSpring the s spring
	 * @param aStatusType the a status type
	 *
	 * @return list of CacIdTagData
	 */
	private List<CacIdTagData> pGetCaseComponentsDataRootTags(String aRgaId, String aCaseComponentCode, AxisSSpring sSpring, String aStatusType) {
		sSpring.setRunStatus(AppConstants.runStatusRun);
	    sSpring.setRunGroupAccount(Integer.parseInt(aRgaId));
	    if (sSpring.getRunGroupAccount() == null)
	    	return null;
		List<IECaseComponent> lCaseComponents = sSpring.getCaseComponents(aCaseComponentCode);
	    if (lCaseComponents == null || lCaseComponents.size() == 0)
	    	return null;
	    List<CacIdTagData> lCacIdRootTags = new ArrayList<CacIdTagData>();
	    for (IECaseComponent lCaseComponent : lCaseComponents) {
	    	IXMLTag lRootTag = sSpring.getXmlDataPlusRunStatusTree(lCaseComponent, aStatusType);
	    	if (lRootTag != null) {
	    		lCacIdRootTags.add(new CacIdTagData(lCaseComponent.getCacId(), lRootTag));
	    	}
	    }
	    return lCacIdRootTags;
	}

	/**
	 * P get case component data content tag.
	 *
	 * @param aRgaId the a rga id
	 * @param aCaseComponentName the a case component name
	 * @param sSpring the s spring
	 * @param aStatusType the a status type
	 *
	 * @return the iXML tag
	 */
	private IXMLTag pGetCaseComponentDataContentTag(String aRgaId, String aCaseComponentName, AxisSSpring sSpring, String aStatusType) {
		IXMLTag lRootTag = pGetCaseComponentDataRootTag(aRgaId, aCaseComponentName, sSpring, aStatusType);
		if (lRootTag == null) return null;
		return lRootTag.getChild(AppConstants.contentElement);
	}

	/**
	 * P get case components data content tags.
	 *
	 * @param aRgaId the a rga id
	 * @param aCaseComponentCode the a case component code
	 * @param sSpring the s spring
	 * @param aStatusType the a status type
	 *
	 * @return list of CacIdTagData
	 */
	private List<CacIdTagData> pGetCaseComponentsDataContentTags(String aRgaId, String aCaseComponentCode, AxisSSpring sSpring, String aStatusType) {
		List<CacIdTagData> lCacIdRootTags = pGetCaseComponentsDataRootTags(aRgaId, aCaseComponentCode, sSpring, aStatusType);
		if (lCacIdRootTags == null) return null;
		List<CacIdTagData> lCacIdContentTags = new ArrayList<CacIdTagData>();
		for (CacIdTagData lCacIdRootTag : lCacIdRootTags) {
			lCacIdContentTags.add(new CacIdTagData(lCacIdRootTag.getCacId(), lCacIdRootTag.getTag().getChild(AppConstants.contentElement)));
		}
		return lCacIdContentTags;
	}

	/**
	 * P get case component data tags.
	 *
	 * @param aRgaId the a rga id
	 * @param aCaseComponentName the a case component name
	 * @param sSpring the s spring
	 * @param aStatusType the a status type
	 * @param aTagName the a tag name
	 *
	 * @return the list
	 */
	private List<IXMLTag> pGetCaseComponentDataTags(String aRgaId, String aCaseComponentName, AxisSSpring sSpring, String aStatusType, String aTagName) {
		IXMLTag lContentTag = pGetCaseComponentDataContentTag(aRgaId, aCaseComponentName, sSpring, aStatusType);
		if (lContentTag == null) return (new ArrayList<IXMLTag>(0));
		return lContentTag.getChilds(aTagName);
	}

	/**
	 * P get case components data tags.
	 *
	 * @param aRgaId the a rga id
	 * @param aCaseComponentCode the a states component code
	 * @param sSpring the s spring
	 * @param aStatusType the a status type
	 * @param aTagName the a tag name
	 *
	 * @return list of CacIdTagsData
	 */
	private List<CacIdTagsData> pGetCaseComponentsDataTags(String aRgaId, String aCaseComponentCode, AxisSSpring sSpring, String aStatusType, String aTagName) {
		List<CacIdTagData> lCacIdContentTags = pGetCaseComponentsDataContentTags(aRgaId, aCaseComponentCode, sSpring, aStatusType);
		List<CacIdTagsData> lCacIdDataTags = new ArrayList<CacIdTagsData>();
		if (lCacIdContentTags != null) {
			for (CacIdTagData lCacIdContentTag : lCacIdContentTags) {
				lCacIdDataTags.add(new CacIdTagsData(lCacIdContentTag.getCacId(), lCacIdContentTag.getTag().getChilds(aTagName)));
			}
		}
		return lCacIdDataTags;
	}

	/**
	 * P do check update save.
	 *
	 * @param sSpring the s spring
	 */
	private void pDoCheckUpdateSave(AxisSSpring sSpring) {
		sSpring.getSScriptHelper().checkScriptTimers(true);
		sSpring.getSUpdateHelper().checkUpdateRunTags(statusType);
		sSpring.saveRunCaseComponentsStatus();
	}

}
