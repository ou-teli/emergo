/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.webservices;

import nl.surf.emergo.business.IXMLTree;
import nl.surf.emergo.business.impl.XMLTree;

/**
 * The Class EmergoServicesHelper.
 */
public class EmergoServicesHelper {

	/** The p xml tree. */
	private IXMLTree pXmlTree = new XMLTree();

	/**
	 * Use client.
	 * 
	 * @param aEnvironment the a environment
	 * 
	 * @return true, if successful
	 */
	public boolean useClient(String aEnvironment) {
		if ((aEnvironment == null) || (aEnvironment.equals("")))
			aEnvironment = "client";
		return (!aEnvironment.equals("server"));
	}

	/**
	 * Escape xml.
	 * 
	 * @param aStr the a str
	 * 
	 * @return the string
	 */
	public String escapeXML(String aStr) {
		return pXmlTree.escapeXML(aStr);
	}

	/**
	 * Unescape xml.
	 * 
	 * @param aStr the a str
	 * 
	 * @return the string
	 */
	public String unescapeXML(String aStr) {
		return pXmlTree.unescapeXML(aStr);
	}

	/**
	 * Convert comma separated string into string array.
	 * 
	 * @param aStr the a str
	 * 
	 * @return the string array
	 */
	public String[] strArray(String aStr) {
		if ((aStr == null) || (aStr.equals("")))
			return null;
	    String[] lStrArr = aStr.split(",");
	    return lStrArr;
	}

}