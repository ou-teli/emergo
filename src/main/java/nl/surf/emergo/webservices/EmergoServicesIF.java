/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.webservices;

import javax.jws.WebService;

import org.springframework.stereotype.Component;

@Component("emergoWsIF")
@WebService
public interface EmergoServicesIF {

	WRun[] getRuns(String aUserId, String aPassword);

	WRun[] getRunsForCRM(String aUserId, String aPassword);

	WState[] getAllStates(String aRgaId);

	WState getStateByKey(String aRgaId, String aKey);

	String setStateByKey(String aRgaId, String aKey, String aValue);
	
	String playMovie(String casus, String studentid, String filmpje);
	
	Exlocation[] getAllExlocations(String aRgaId);
	
	Exlocation[] getCurrentExlocations(String aRgaId);
	
	Exlocation[] getExlocationsByPosition(String aRgaId, String aLatitude, String aLongitude);
	
	String getContext(String aRgaId, String aSensorType);
	
	String setContext(String aRgaId, String aSensorData);

	Url[] getAllUrls(String aRgaId);
	
	Url[] getCurrentUrls(String aRgaId);
	
	Url[] getUrlsByPosition(String aRgaId, String aLatitude, String aLongitude);
	
	Url[] getUrlsByName(String aRgaId, String aName);
	
	String setUrl(String aRgaId, Url aUrl);
	
	String deleteUrl(String aRgaId, Url aUrl);
	
	String getCaseComponentData(String aRgaId, String aCaseComponentName);

}