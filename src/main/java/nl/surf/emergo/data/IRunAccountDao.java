/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data;

import java.util.List;

import nl.surf.emergo.domain.IERunAccount;

/**
 * The Interface IRunAccountDao.
 */
public interface IRunAccountDao {

	/**
	 * Gets.
	 * 
	 * @param ruaId the rua id
	 * 
	 * @return the iE run account
	 */
	public IERunAccount get(int ruaId);

	/**
	 * Checks if exists.
	 * 
	 * @param runId the run id
	 * @param accId the acc id
	 * 
	 * @return true, if successful
	 */
	public boolean exists(int runId, int accId);

	/**
	 * Saves.
	 * 
	 * @param eRunAccount the e run account
	 */
	public void save(IERunAccount eRunAccount);

	/**
	 * Deletes.
	 * 
	 * @param eRunAccount the e run account
	 */
	public void delete(IERunAccount eRunAccount);

	/**
	 * Deletes.
	 * 
	 * @param ruaId the rua id
	 */
	public void delete(int ruaId);

	/**
	 * Gets all.
	 * 
	 * @return all
	 */
	public List<IERunAccount> getAll();

	/**
	 * Gets all by run id.
	 * 
	 * @param runId the run id
	 * 
	 * @return all by run id
	 */
	public List<IERunAccount> getAllByRunId(int runId);

	/**
	 * Gets all by acc id.
	 * 
	 * @param accId the acc id
	 * 
	 * @return all by acc id
	 */
	public List<IERunAccount> getAllByAccId(int accId);

	/**
	 * Gets all by acc id, depending on tutor activity.
	 * 
	 * @param accId the acc id
	 * @param tutactive whether active as tutor or not
	 * 
	 * @return all by acc id, tutactive
	 */
	public List<IERunAccount> getAllByAccIdTutor(int accId, boolean tutactive);

	/**
	 * Gets all by acc id, depending on student activity.
	 * 
	 * @param accId the acc id
	 * @param stuactive whether active as student or not
	 * 
	 * @return all by acc id, stuactive
	 */
	public List<IERunAccount> getAllByAccIdStudent(int accId, boolean stuactive);

	/**
	 * Gets all by run id acc id.
	 * 
	 * @param rugId the run id
	 * @param accId the acc id
	 * 
	 * @return all by run id acc id
	 */
	public List<IERunAccount> getAllByRunIdAccId(int runId, int accId);

}
