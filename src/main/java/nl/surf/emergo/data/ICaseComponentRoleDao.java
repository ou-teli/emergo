/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data;

import java.util.List;

import nl.surf.emergo.domain.IECaseComponentRole;

/**
 * The Interface ICaseComponentRoleDao.
 */
public interface ICaseComponentRoleDao {

	/**
	 * Gets.
	 * 
	 * @param ccrId the ccr id
	 * 
	 * @return the iE case component role
	 */
	public IECaseComponentRole get(int ccrId);

	/**
	 * Gets.
	 * 
	 * @param cacId the cac id
	 * @param carId the car id
	 * 
	 * @return the iE case component role
	 */
	public IECaseComponentRole get(int cacId, int carId);

	/**
	 * Checks if exists.
	 * 
	 * @param cacId the cac id
	 * @param carId the car id
	 * 
	 * @return true, if successful
	 */
	public boolean exists(int cacId, int carId);

	/**
	 * Saves.
	 * 
	 * @param eCaseComponentRole the e case component role
	 */
	public void save(IECaseComponentRole eCaseComponentRole);

	/**
	 * Deletes.
	 * 
	 * @param eCaseComponentRole the e case component role
	 */
	public void delete(IECaseComponentRole eCaseComponentRole);

	/**
	 * Deletes.
	 * 
	 * @param ccrId the ccr id
	 */
	public void delete(int ccrId);

	/**
	 * Gets all.
	 * 
	 * @return all
	 */
	public List<IECaseComponentRole> getAll();

	/**
	 * Gets all by cac id.
	 * 
	 * @param cacId the cac id
	 * 
	 * @return all by cac id
	 */
	public List<IECaseComponentRole> getAllByCacId(int cacId);

	/**
	 * Gets all by cac ids.
	 * 
	 * @param cacIds the cac ids
	 * 
	 * @return all by cac ids
	 */
	public List<IECaseComponentRole> getAllByCacIds(List<Integer> cacIds);

	/**
	 * Gets all by car id.
	 * 
	 * @param carId the car id
	 * 
	 * @return all by car id
	 */
	public List<IECaseComponentRole> getAllByCarId(int carId);

	/**
	 * Gets all by car ids.
	 * 
	 * @param carIds the car ids
	 * 
	 * @return all by car ids
	 */
	public List<IECaseComponentRole> getAllByCarIds(List<Integer> carIds);

	/**
	 * Gets all by cac id car id.
	 * 
	 * @param cacId the cac id
	 * @param carId the car id
	 * 
	 * @return all by cac id car id
	 */
	public List<IECaseComponentRole> getAllByCacIdCarId(int cacId, int carId);

}
