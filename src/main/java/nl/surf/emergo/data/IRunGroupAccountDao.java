/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data;

import java.util.List;

import nl.surf.emergo.domain.IERunGroupAccount;

/**
 * The Interface IRunGroupAccountDao.
 */
public interface IRunGroupAccountDao {

	/**
	 * Gets.
	 * 
	 * @param rgaId the rga id
	 * 
	 * @return the iE run group account
	 */
	public IERunGroupAccount get(int rgaId);

	/**
	 * Checks if exists.
	 * 
	 * @param rugId the rug id
	 * @param accId the acc id
	 * 
	 * @return true, if successful
	 */
	public boolean exists(int rugId, int accId);

	/**
	 * Saves.
	 * 
	 * @param eRunGroupAccount the e run group account
	 */
	public void save(IERunGroupAccount eRunGroupAccount);

	/**
	 * Deletes.
	 * 
	 * @param eRunGroupAccount the e run group account
	 */
	public void delete(IERunGroupAccount eRunGroupAccount);

	/**
	 * Deletes.
	 * 
	 * @param rgaId the rga id
	 */
	public void delete(int rgaId);

	/**
	 * Gets all.
	 * 
	 * @return all
	 */
	public List<IERunGroupAccount> getAll();

	/**
	 * Gets all by rga ids.
	 * 
	 * @param rgaIds the rga ids
	 * 
	 * @return all by rga ids
	 */
	public List<IERunGroupAccount> getAllByRgaIds(List<Integer> rgaIds);

	/**
	 * Gets all by rug id.
	 * 
	 * @param rugId the rug id
	 * 
	 * @return all by rug id
	 */
	public List<IERunGroupAccount> getAllByRugId(int rugId);

	/**
	 * Gets all by rug ids.
	 * 
	 * @param rugIds the rug ids
	 * 
	 * @return all by rug ids
	 */
	public List<IERunGroupAccount> getAllByRugIds(List<Integer> rugIds);

	/**
	 * Gets all by acc id.
	 * 
	 * @param accId the acc id
	 * 
	 * @return all by acc id
	 */
	public List<IERunGroupAccount> getAllByAccId(int accId);

	/**
	 * Gets all by acc ids.
	 * 
	 * @param accIds the acc ids
	 * 
	 * @return all by acc ids
	 */
	public List<IERunGroupAccount> getAllByAccIds(List<Integer> accIds);

	/**
	 * Gets all by rug id acc id.
	 * 
	 * @param rugId the rug id
	 * @param accId the acc id
	 * 
	 * @return all by rug id acc id
	 */
	public List<IERunGroupAccount> getAllByRugIdAccId(int rugId, int accId);

}
