/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data;

import java.util.List;
import java.util.Map;

import nl.surf.emergo.domain.IECase;

/**
 * The Interface ICaseDao.
 */
public interface ICaseDao {

	/**
	 * Gets.
	 * 
	 * @param casId the cas id
	 * 
	 * @return the iE case
	 */
	public IECase get(int casId);

	/**
	 * Checks if exists.
	 * 
	 * @param accId the acc id
	 * @param name the name
	 * @param version the version
	 * 
	 * @return true, if successful
	 */
	public boolean exists(int accId, String name, int version);

	/**
	 * Saves.
	 * 
	 * @param eCase the e case
	 */
	public void save(IECase eCase);

	/**
	 * Deletes.
	 * 
	 * @param eCase the e case
	 */
	public void delete(IECase eCase);

	/**
	 * Deletes.
	 * 
	 * @param casId the cas id
	 */
	public void delete(int casId);

	/**
	 * Gets all.
	 * 
	 * @return all
	 */
	public List<IECase> getAll();

	/**
	 * Gets all by key=value filter.
	 * 
	 * @param keysAndValueParts the db field ids and value parts
	 * 
	 * @return all by filter
	 */
	public List<IECase> getAllFilter(Map<String, String> keysAndValueParts);

	/**
	 * Gets all by acc id.
	 * 
	 * @param accId the acc id
	 * 
	 * @return all by acc id
	 */
	public List<IECase> getAllByAccId(int accId);

	/**
	 * Gets all by acc ids.
	 * 
	 * @param accIds the acc ids
	 * 
	 * @return all by acc ids
	 */
	public List<IECase> getAllByAccIds(List<Integer> accIds);

	/**
	 * Gets all runnable.
	 * 
	 * @return all runnable
	 */
	public List<IECase> getAllRunnable();

}
