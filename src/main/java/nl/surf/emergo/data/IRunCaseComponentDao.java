/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data;

import java.util.List;

import nl.surf.emergo.domain.IERunCaseComponent;

/**
 * The Interface IRunCaseComponentDao.
 */
public interface IRunCaseComponentDao {

	/**
	 * Gets.
	 * 
	 * @param rccId the rcc id
	 * 
	 * @return the iE run case component
	 */
	public IERunCaseComponent get(int rccId);

	/**
	 * Gets.
	 * 
	 * @param runId the run id
	 * @param cacId the cac id
	 * 
	 * @return the iE run case component
	 */
	public IERunCaseComponent get(int runId, int cacId);

	/**
	 * Saves.
	 * 
	 * @param eRunCaseComponent the e run case component
	 */
	public void save(IERunCaseComponent eRunCaseComponent);

	/**
	 * Deletes.
	 * 
	 * @param eRunCaseComponent the e run case component
	 */
	public void delete(IERunCaseComponent eRunCaseComponent);

	/**
	 * Deletes.
	 * 
	 * @param rccId the rcc id
	 */
	public void delete(int rccId);

	/**
	 * Gets all.
	 * 
	 * @return all
	 */
	public List<IERunCaseComponent> getAll();

	/**
	 * Gets all by run id.
	 * 
	 * @param runId the run id
	 * 
	 * @return all by run id
	 */
	public List<IERunCaseComponent> getAllByRunId(int runId);

	/**
	 * Gets all by cac id.
	 * 
	 * @param cacId the cac id
	 * 
	 * @return all by cac id
	 */
	public List<IERunCaseComponent> getAllByCacId(int cacId);

	/**
	 * Gets all by run id cac id.
	 * 
	 * @param runId the run id
	 * @param cacId the cac id
	 * 
	 * @return all by run id cac id
	 */
	public List<IERunCaseComponent> getAllByRunIdCacId(int runId, int cacId);

}
