/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data;

import java.util.List;

import nl.surf.emergo.domain.IEAccountContext;

/**
 * The Interface IAccountContextDao.
 */
public interface IAccountContextDao {

	/**
	 * Gets.
	 * 
	 * @param accId the account context id
	 * 
	 * @return the iE accountcontext
	 */
	public IEAccountContext get(int accId);

	/**
	 * Saves.
	 * 
	 * @param eAccountContext the account context
	 */
	public void save(IEAccountContext eAccountContext);

	/**
	 * Deletes.
	 * 
	 * @param eAccountContext the account context
	 */
	public void delete(IEAccountContext eAccountContext);

	/**
	 * Deletes.
	 * 
	 * @param accountContextId the account context id
	 */
	public void delete(int accountContextId);

	/**
	 * Gets all.
	 * 
	 * @return all
	 */
	public List<IEAccountContext> getAll();

	/**
	 * Gets all by account id.
	 * 
	 * @param accId the account id
	 * 
	 * @return all by account id
	 */
	public List<IEAccountContext> getAllByAccId(int accId);

	/**
	 * Gets all by context id.
	 * 
	 * @param conId the context id
	 * 
	 * @return all by context id
	 */
	public List<IEAccountContext> getAllByConId(int conId);

	/**
	 * Gets all by account id and active value.
	 * 
	 * @param accId the account id
	 * @param active the active
	 * 
	 * @return all by account id
	 */
	public List<IEAccountContext> getAllByAccIdActive(int accId, boolean active);

	/**
	 * Gets all by context id and active value.
	 * 
	 * @param conId the context id
	 * @param active the active
	 * 
	 * @return all by context id
	 */
	public List<IEAccountContext> getAllByConIdActive(int conId, boolean active);

	/**
	 * Gets all by context ids and active value.
	 * 
	 * @param conIds the context ids
	 * @param active the active
	 * 
	 * @return all by context ids
	 */
	public List<IEAccountContext> getAllByConIdsActive(List<Integer> conIds, boolean active);

}
