/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data;

import java.util.List;
import java.util.Map;

import nl.surf.emergo.domain.IEAccount;

/**
 * The Interface IAccountDao.
 */
public interface IAccountDao {

	/**
	 * Gets.
	 * 
	 * @param accId the acc id
	 * 
	 * @return the iE account
	 */
	public IEAccount get(int accId);

	/**
	 * Gets.
	 * 
	 * @param userid the userid
	 * @param password the password
	 * @param active the active
	 * 
	 * @return the iE account
	 */
	public IEAccount get(String userid, String password, boolean active);

	/**
	 * Gets.
	 * 
	 * @param userid the userid
	 * 
	 * @return the iE account
	 */
	public IEAccount get(String userid);

	/**
	 * Saves.
	 * 
	 * @param eAccount the e account
	 */
	public void save(IEAccount eAccount);

	/**
	 * Deletes.
	 * 
	 * @param eAccount the e account
	 */
	public void delete(IEAccount eAccount);

	/**
	 * Deletes.
	 * 
	 * @param accId the acc id
	 */
	public void delete(int accId);

	/**
	 * Gets all.
	 * 
	 * @return all
	 */
	public List<IEAccount> getAll();

	/**
	 * Gets all.
	 * 
	 * @param active the active
	 * 
	 * @return all
	 */
	public List<IEAccount> getAll(boolean active);

	/**
	 * Gets all by acc ids.
	 * 
	 * @param active the active
	 * @param accIds the acc ids
	 * 
	 * @return all by acc ids
	 */
	public List<IEAccount> getAllByAccIds(boolean active, List<Integer> accIds);

	/**
	 * Gets all by user id and last name like.
	 * 
	 * @param keysAndValueParts the db field ids and value parts
	 * 
	 * @return all by filter
	 */
	public List<IEAccount> getAllFilter(Map<String, String> keysAndValueParts);

	/**
	 * Gets all active by user id and last name like.
	 * 
	 * @param active the active
	 * @param keysAndValueParts the db field ids and value parts
	 * 
	 * @return all by filter
	 */
	public List<IEAccount> getAllFilter(boolean active, Map<String, String> keysAndValueParts);

	/**
	 * Gets all active by accIds and user id and last name like.
	 * 
	 * @param active the active
	 * @param accIds the acc ids
	 * @param keysAndValueParts the db field ids and value parts
	 * 
	 * @return all by filter
	 */
	public List<IEAccount> getAllByAccIdsFilter(boolean active, List<Integer> accIds, Map<String, String> keysAndValueParts);

	/**
	 * Get all by email.
	 * 
	 * @param email the email
	 * 
	 * @return all by email
	 */
	public List<IEAccount> getAllByEmail(String email);

}
