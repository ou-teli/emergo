/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data;

import java.util.List;

import nl.surf.emergo.domain.IERunTeamCaseComponent;

/**
 * The Interface IRunTeamCaseComponentDao.
 */
public interface IRunTeamCaseComponentDao {

	/**
	 * Gets.
	 * 
	 * @param rtcId the rtc id
	 * 
	 * @return the iE run team case component
	 */
	public IERunTeamCaseComponent get(int rtcId);

	/**
	 * Gets.
	 * 
	 * @param rutId the rut id
	 * @param cacId the cac id
	 * 
	 * @return the iE run team case component
	 */
	public IERunTeamCaseComponent get(int rutId, int cacId);

	/**
	 * Saves.
	 * 
	 * @param eRunTeamCaseComponent the e run team case component
	 */
	public void save(IERunTeamCaseComponent eRunTeamCaseComponent);

	/**
	 * Deletes.
	 * 
	 * @param eRunTeamCaseComponent the e run team case component
	 */
	public void delete(IERunTeamCaseComponent eRunTeamCaseComponent);

	/**
	 * Deletes.
	 * 
	 * @param rtcId the rtc id
	 */
	public void delete(int rtcId);
	/**
	 * Gets all.
	 * 
	 * @return all
	 */
	public List<IERunTeamCaseComponent> getAll();

	/**
	 * Gets all by rut id.
	 * 
	 * @param rutId the rut id
	 * 
	 * @return all by rut id
	 */
	public List<IERunTeamCaseComponent> getAllByRutId(int rutId);

	/**
	 * Gets all by cac id.
	 * 
	 * @param cacId the cac id
	 * 
	 * @return all by cac id
	 */
	public List<IERunTeamCaseComponent> getAllByCacId(int cacId);

	/**
	 * Gets all by rut id cac id.
	 * 
	 * @param rutId the rut id
	 * @param cacId the cac id
	 * 
	 * @return all by rut id cac id
	 */
	public List<IERunTeamCaseComponent> getAllByRutIdCacId(int rutId, int cacId);

}
