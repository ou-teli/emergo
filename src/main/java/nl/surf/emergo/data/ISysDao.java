/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data;

import java.util.List;

import nl.surf.emergo.domain.IESys;

/**
 * The Interface ISysDao.
 */
public interface ISysDao {

	/**
	 * Gets.
	 * 
	 * @param sysId the sys id
	 * 
	 * @return the iE sys
	 */
	public IESys get(int sysId);

	/**
	 * Gets.
	 * 
	 * @param syskey the syskey
	 * 
	 * @return the iE sys
	 */
	public IESys get(String syskey);

	/**
	 * Saves.
	 * 
	 * @param eSys the e sys
	 */
	public void save(IESys eSys);

	/**
	 * Gets all.
	 * 
	 * @return all
	 */
	public List<IESys> getAll();

	/**
	 * Executes Sql.
	 * 
	 * @param aSql the a sql 
	 * @param aErrors the a errors, used to return errors 
	 */
	public void executeSql(String aSql, List<String> aErrors);

	/**
	 * Get Sql result.
	 * 
	 * @param aSql the a sql 
	 * @param aClass the a class 
	 * @param aErrors the a errors, used to return errors 
	 * 
	 * @return result as objects
	 */
	public List<Object> getSqlResult(String aSql, Class aClass, List<String> aErrors);

	/**
	 * Get Sql result.
	 * 
	 * @param aSql the a sql 
	 * @param aErrors the a errors, used to return errors 
	 * 
	 * @return result as objects
	 */
	public List<Object> getSqlResult(String aSql, List<String> aErrors);

	/**
	 * Checks if table exists.
	 * 
	 * @param aTableName the a table name 
	 * 
	 * @return if true
	 */
	public boolean sqlTableExists(String aTableName);

	/**
	 * Checks if table column exists.
	 * 
	 * @param aTableName the a table name 
	 * @param aColumnName the a column name 
	 * 
	 * @return if true
	 */
	public boolean sqlTableColumnExists(String aTableName, String aColumnName);

}
