/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data;

import java.util.List;

import nl.surf.emergo.domain.IERunGroupCaseComponentUpdate;

/**
 * The Interface IRunGroupCaseComponentUpdateDao.
 */
public interface IRunGroupCaseComponentUpdateDao {

	/**
	 * Gets.
	 * 
	 * @param rguId the rgu id
	 * 
	 * @return the iE run group case component update
	 */
	public IERunGroupCaseComponentUpdate get(int rguId);

	/**
	 * Saves.
	 * 
	 * @param eRunGroupCaseComponentUpdate the e run group case component update
	 */
	public void save(IERunGroupCaseComponentUpdate eRunGroupCaseComponentUpdate);

	/**
	 * Deletes.
	 * 
	 * @param eRunGroupCaseComponentUpdate the e run group case component update
	 */
	public void delete(IERunGroupCaseComponentUpdate eRunGroupCaseComponentUpdate);

	/**
	 * Deletes.
	 * 
	 * @param rguId the rgu id
	 */
	public void delete(int rguId);

	/**
	 * Gets all.
	 * 
	 * @return all
	 */
	public List<IERunGroupCaseComponentUpdate> getAll();

	/**
	 * Gets all by rug id.
	 * 
	 * @param rugId the rug id
	 * 
	 * @return all by rug id
	 */
	public List<IERunGroupCaseComponentUpdate> getAllByRugId(int rugId);

	/**
	 * Gets all by rug id.
	 * 
	 * @param rugId the rug id
	 * @param processed the processed
	 * 
	 * @return all by rug id
	 */
	public List<IERunGroupCaseComponentUpdate> getAllByRugId(int rugId, boolean processed);

	/**
	 * Gets all by cac id.
	 * 
	 * @param cacId the cac id
	 * 
	 * @return all by cac id
	 */
	public List<IERunGroupCaseComponentUpdate> getAllByCacId(int cacId);

	/**
	 * Gets all by rug from id.
	 * 
	 * @param rugFromId the rug from id
	 * 
	 * @return all by rug from id
	 */
	public List<IERunGroupCaseComponentUpdate> getAllByRugFromId(int rugFromId);

	/**
	 * Gets all by rug id cac id.
	 * 
	 * @param rugId the rug id
	 * @param cacId the cac id
	 * 
	 * @return all by rug id cac id
	 */
	public List<IERunGroupCaseComponentUpdate> getAllByRugIdCacId(int rugId, int cacId);
}
