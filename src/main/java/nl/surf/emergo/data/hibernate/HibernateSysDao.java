/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data.hibernate;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;
import org.hibernate.internal.SessionFactoryImpl;
import org.springframework.transaction.annotation.Transactional;

import nl.surf.emergo.data.ISysDao;
import nl.surf.emergo.domain.IESys;

/**
 * The Class HibernateSysDao.
 */
public class HibernateSysDao extends HibernateAllDao implements ISysDao {

	private static final Logger _log = LogManager.getLogger(HibernateSysDao.class);

	private static Connection connection;
	
	@Transactional
	protected List<IESys> getItems(List items) {
		return (List<IESys>)items;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ISysDao#get(int)
	 */
	@Transactional
	public IESys get(int sysId) {
		List<IESys> items = getItems(selectQuery(
				"select e from ESys as e where sysId=:sysId", 
				new String[] { "sysId" },
				new Object[] { sysId }
				));
		if (items.size() == 1) {
			return items.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ISysDao#get(java.lang.String)
	 */
	@Transactional
	public IESys get(String syskey) {
		List<IESys> items = getItems(selectQuery(
				"select e from ESys as e where syskey=:syskey", 
				new String[] { "syskey" },
				new Object[] { syskey }
				));
		if (items.size() == 1) {
			return items.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ISysDao#save(nl.surf.emergo.domain.IESys)
	 */
	public void save(IESys eSys) {
		super.save(eSys);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ISysDao#getAll()
	 */
	@Transactional
	public List<IESys> getAll() {
		return getItems(selectQuery(
				"select e from ESys as e order by sysId asc"
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ISysDao#executeSql(java.lang.String,java.lang.List)
	 */
	@Transactional
	public void executeSql(String aSql, List<String> aErrors) {
		try {
			Query query = currentSession().createSQLQuery(aSql);
			query.executeUpdate();
		}
		catch (Exception e) {
			aErrors.add(e.getMessage());
		}
	}
	
	private static Connection getConnection(Session aSession, List<String> aErrors) {
		if (connection == null) {
			SessionFactoryImpl sessFImp = (SessionFactoryImpl) aSession.getSessionFactory();
			try {
				connection = sessFImp.getSessionFactoryOptions().getServiceRegistry().getService(ConnectionProvider.class).getConnection();
			} catch (SQLException e) {
				aErrors.add(e.getMessage());
			}
		}
		return connection;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ISysDao#getSqlResult(java.lang.String,Class,java.lang.List)
	 */
	@Transactional
	public List<Object> getSqlResult(String aSql, Class aClass, List<String> aErrors) {
		try {
			List<Object> items = currentSession().createSQLQuery(aSql).addEntity("item", aClass).list();
			/*NOTE test code
			Connection connection = getConnection(session, aErrors);
			if (connection != null) {
				DatabaseMetaData metaData = connection.getMetaData();
				ResultSet resultSet = metaData.getColumns(null, null, "components", null);
			    while (resultSet.next()) {
					String name = resultSet.getString("COLUMN_NAME");
					String type = resultSet.getString("TYPE_NAME");
					int size = resultSet.getInt("COLUMN_SIZE");
					_log.info("Column name: [" + name + "]; type: [" + type 
					    + "]; size: [" + size + "]");
				}
				resultSet = metaData.getTables(null, null, "components", null);
			    while (resultSet.next()) {
					String name = resultSet.getString("TABLE_NAME");
					_log.info("Table name: [" + name + "]");
				}
			}
			*/
			return items;
		}
		catch (Exception e) {
			aErrors.add(e.getMessage());
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ISysDao#getSqlResult(java.lang.String,java.lang.List)
	 */
	@Transactional
	public List<Object> getSqlResult(String aSql, List<String> aErrors) {
		try {
			return currentSession().createSQLQuery(aSql).list();
		}
		catch (Exception e) {
			aErrors.add(e.getMessage());
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ISysDao#sqlTableExists(java.lang.String)
	 */
	@Transactional
	public boolean sqlTableExists(String aTableName) {
		boolean tableExists = false;
		try {
			List<String> lErrors = new ArrayList<String>();
			Connection connection = getConnection(currentSession(), lErrors);
			if (connection != null) {
				String lCurrDb = connection.getCatalog();
				String lCurrSchema = connection.getSchema();
				DatabaseMetaData metaData = connection.getMetaData();
				ResultSet resultSet = metaData.getTables(lCurrDb, lCurrSchema, aTableName, null);
			    while (resultSet.next()) {
					String name = resultSet.getString("TABLE_NAME");
					if (name.equals(aTableName)) {
						tableExists = true;
					}
				}
			}
		} catch (HibernateException e) {
			_log.error(e);
		} catch (SQLException e) {
			_log.error(e);
		}
		return tableExists;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ISysDao#sqlTableColumnExists(java.lang.String,java.lang.String)
	 */
	@Transactional
	public boolean sqlTableColumnExists(String aTableName, String aColumnName) {
		if (!sqlTableExists(aTableName)) {
			return false;
		}
		boolean columnExists = false;
		try {
			List<String> lErrors = new ArrayList<String>();
			Connection connection = getConnection(currentSession(), lErrors);
			if (connection != null) {
				String lCurrDb = connection.getCatalog();
				String lCurrSchema = connection.getSchema();
				DatabaseMetaData metaData = connection.getMetaData();
				ResultSet resultSet = metaData.getColumns(lCurrDb, lCurrSchema, aTableName, aColumnName);
			    while (resultSet.next()) {
					String name = resultSet.getString("COLUMN_NAME");
					if (name.equals(aColumnName)) {
						columnExists = true;
					}
//					String typeName = resultSet.getString("TYPE_NAME");
//					String columnSize = resultSet.getString("COLUMN_SIZE");
				}
			}
		} catch (HibernateException e) {
			_log.error(e);
		} catch (SQLException e) {
			_log.error(e);
		}
		return columnExists;
	}

}
