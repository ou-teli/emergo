/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import nl.surf.emergo.data.ICaseComponentRoleDao;
import nl.surf.emergo.domain.IECaseComponentRole;

/**
 * The Class HibernateCaseComponentRoleDao.
 */
public class HibernateCaseComponentRoleDao extends HibernateAllDao
		implements ICaseComponentRoleDao {

	@Transactional
	protected List<IECaseComponentRole> getItems(List items) {
		return (List<IECaseComponentRole>)items;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseComponentRoleDao#get(int)
	 */
	@Transactional
	public IECaseComponentRole get(int ccrId) {
		List<IECaseComponentRole> caseComponentRoles = getItems(selectQuery(
				"select e from ECaseComponentRole as e where ccrId=:ccrId",
				new String[] { "ccrId" },
				new Object[] { ccrId }
				));
		if (caseComponentRoles.size() == 1) {
			return caseComponentRoles.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseComponentRoleDao#get(int, int)
	 */
	@Transactional
	public IECaseComponentRole get(int cacId, int carId) {
		List<IECaseComponentRole> caseComponentRoles = getItems(selectQuery(
				"select e from ECaseComponentRole as e where cacCacId=:cacId and carCarId=:carId",
				new String[] { "cacId", "carId" },
				new Object[] { cacId, carId }
				));
		if (caseComponentRoles.size() == 1) {
			return caseComponentRoles.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseComponentRoleDao#exists(int, int)
	 */
	@Transactional
	public boolean exists(int cacId, int carId) {
		List<IECaseComponentRole> caseComponentRoles = getItems(selectQuery(
				"select e from ECaseComponentRole as e where cacCacId=:cacId and carCarId=:carId",
				new String[] { "cacId", "carId" },
				new Object[] { cacId, carId }
				));
		if (caseComponentRoles.size() > 0) {
			return true;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseComponentRoleDao#save(nl.surf.emergo.domain.IECaseComponentRole)
	 */
	public void save(IECaseComponentRole eCaseComponentRole) {
		super.save(eCaseComponentRole);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseComponentRoleDao#delete(nl.surf.emergo.domain.IECaseComponentRole)
	 */
	public void delete(IECaseComponentRole eCaseComponentRole) {
		super.delete(eCaseComponentRole);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseComponentRoleDao#delete(int)
	 */
	public void delete(int ccrId) {
		super.delete("select e from ECaseComponentRole as e where ccrId=" + ccrId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseComponentRoleDao#getAll()
	 */
	@Transactional
	public List<IECaseComponentRole> getAll() {
		return getItems(selectQuery(
				"select e from ECaseComponentRole as e order by ccrId asc"
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseComponentRoleDao#getAllByCacId(int)
	 */
	@Transactional
	public List<IECaseComponentRole> getAllByCacId(int cacId) {
		return getItems(selectQuery(
				"select e from ECaseComponentRole as e where cacCacId=:cacId order by ccrId asc",
				new String[] { "cacId" },
				new Object[] { cacId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseComponentRoleDao#getAllByCacIds(List)
	 */
	@Transactional
	public List<IECaseComponentRole> getAllByCacIds(List<Integer> cacIds) {
		if (cacIds.size() == 0)
			return new ArrayList<IECaseComponentRole>();
		return getItems(selectQuery(
				"select e from ECaseComponentRole as e where cacCacId in :cacIds order by ccrId asc",
				new String[] { "cacIds" },
				new Object[] { cacIds }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseComponentRoleDao#getAllByCarId(int)
	 */
	@Transactional
	public List<IECaseComponentRole> getAllByCarId(int carId) {
		return getItems(selectQuery(
				"select e from ECaseComponentRole as e where carCarId=:carId order by ccrId asc",
				new String[] { "carId" },
				new Object[] { carId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseComponentRoleDao#getAllByCarIds(List)
	 */
	@Transactional
	public List<IECaseComponentRole> getAllByCarIds(List<Integer> carIds) {
		if (carIds.size() == 0)
			return new ArrayList<IECaseComponentRole>();
		return getItems(selectQuery(
				"select e from ECaseComponentRole as e where carCarId in :carIds order by ccrId asc",
				new String[] { "carIds" },
				new Object[] { carIds }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseComponentRoleDao#getAllByCacIdCarId(int, int)
	 */
	@Transactional
	public List<IECaseComponentRole> getAllByCacIdCarId(int cacId, int carId) {
		return getItems(selectQuery(
				"select e from ECaseComponentRole as e where cacCacId=:cacId and carCarId=:carId order by ccrId asc",
				new String[] { "cacId" , "carId" },
				new Object[] { cacId , carId }
				));
	}

}
