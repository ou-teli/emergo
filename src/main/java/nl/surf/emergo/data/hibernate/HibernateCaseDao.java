/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data.hibernate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;

import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.data.ICaseDao;
import nl.surf.emergo.domain.IECase;

/**
 * The Class HibernateCaseDao.
 */

public class HibernateCaseDao extends HibernateAllDao implements ICaseDao {

	@Transactional
	protected List<IECase> getItems(List items) {
		return (List<IECase>)items;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseDao#get(int)
	 */
	@Transactional
	public IECase get(int casId) {
		List<IECase> cases = getItems(selectQuery(
				"select e from ECase as e where casId=:casId",
				new String[] { "casId" },
				new Object[] { casId }
				));
		if (cases.size() == 1) {
			return cases.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseDao#exists(int, java.lang.String, int)
	 */
	@Transactional
	public boolean exists(int accId, String name, int version) {
		//NOTE that domain class ECase does not have a field accAccId, so e.accAccId would not work. It then should be e.eAccount.accId.
		//However using just accAccId is sufficient and works because the database table cases has field accAccId.
		//Just using accAccId probably is more efficient, because querying database table accounts is not needed.
		List<IECase> cases = getItems(selectQuery(
				"select e from ECase as e where accAccId=:accId and name=:name and version=:version",
				new String[] { "accId", "name", "version" },
				new Object[] { accId, name, version }
				));
		if (cases.size() > 0) {
			return true;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseDao#save(nl.surf.emergo.domain.IECase)
	 */
	public void save(IECase eCase) {
		super.save(eCase);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseDao#delete(nl.surf.emergo.domain.IECase)
	 */
	public void delete(IECase eCase) {
		super.delete(eCase);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseDao#delete(int)
	 */
	public void delete(int casId) {
		super.delete("select e from ECase as e where casId=" + casId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseDao#getAll()
	 */
	@Transactional
	public List<IECase> getAll() {
		return getItems(selectQuery(
				"select e from ECase as e order by name asc"
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseDao#getAllFilter(String, String)
	 */
	@Transactional
	public List<IECase> getAllFilter(Map<String, String> keysAndValueParts) {
		String[] paramNames = getParamNames(keysAndValueParts);
		Object[] paramValues = getParamValues(keysAndValueParts);
		String whereSql = getLikeSql("", paramNames);
		if (whereSql.length() > 0) {
			whereSql = " where " + whereSql;
		}
		return getItems(selectQuery(
				"select e from ECase as e" + whereSql + " order by name asc",
				paramNames,
				paramValues
				));
	}
	
	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseDao#getAllByAccId(int)
	 */
	@Transactional
	public List<IECase> getAllByAccId(int accId) {
		return getItems(selectQuery(
				"select e from ECase as e where accAccId=:accId order by name asc",
				new String[] { "accId" },
				new Object[] { accId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseDao#getAllByAccIds(List)
	 */
	@Transactional
	public List<IECase> getAllByAccIds(List<Integer> accIds) {
		if (accIds.size() == 0)
			return new ArrayList<IECase>();
		return getItems(selectQuery(
				"select e from ECase as e where accAccId in :accIds order by name asc",
				new String[] { "accIds" },
				new Object[] { accIds }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseDao#getAllRunnable()
	 */
	@Transactional
	public List<IECase> getAllRunnable() {
		return getItems(selectQuery(
				"select e from ECase as e where status=:status and active=:active order by name asc",
				new String[] { "status", "active" },
				new Object[] { AppConstants.case_status_runnable, true }
				));
	}

}
