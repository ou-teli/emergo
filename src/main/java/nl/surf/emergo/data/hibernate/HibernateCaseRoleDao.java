/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data.hibernate;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import nl.surf.emergo.data.ICaseRoleDao;
import nl.surf.emergo.domain.IECaseRole;

/**
 * The Class HibernateCaseRoleDao.
 */
public class HibernateCaseRoleDao extends HibernateAllDao implements
		ICaseRoleDao {

	@Transactional
	protected List<IECaseRole> getItems(List items) {
		return (List<IECaseRole>)items;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseRoleDao#get(int)
	 */
	@Transactional
	public IECaseRole get(int carId) {
		List<IECaseRole> caseRoles = getItems(selectQuery(
				"select e from ECaseRole as e where carId=:carId",
				new String[] { "carId" },
				new Object[] { carId }
				));
		if (caseRoles.size() == 1) {
			return caseRoles.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseRoleDao#exists(int, java.lang.String)
	 */
	@Transactional
	public boolean exists(int casId, String name) {
		List<IECaseRole> caseRoles = getItems(selectQuery(
				"select e from ECaseRole as e where casCasId=:casId and name=:name",
				new String[] { "casId", "name" },
				new Object[] { casId, name }
				));
		if (caseRoles.size() > 0) {
			return true;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseRoleDao#save(nl.surf.emergo.domain.IECaseRole)
	 */
	public void save(IECaseRole eCaseRole) {
		super.save(eCaseRole);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseRoleDao#delete(nl.surf.emergo.domain.IECaseRole)
	 */
	public void delete(IECaseRole eCaseRole) {
		super.delete(eCaseRole);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseRoleDao#delete(int)
	 */
	public void delete(int carId) {
		super.delete("select e from ECaseRole as e where carId=" + carId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseRoleDao#getAll()
	 */
	@Transactional
	public List<IECaseRole> getAll() {
		return getItems(selectQuery(
				"select e from ECaseRole as e order by name asc"
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseRoleDao#getAllByCasId(int)
	 */
	@Transactional
	public List<IECaseRole> getAllByCasId(int casId) {
		return getItems(selectQuery(
				"select e from ECaseRole as e where casCasId=:casId order by name asc",
				new String[] { "casId" },
				new Object[] { casId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseRoleDao#getAll(boolean)
	 */
	@Transactional
	public List<IECaseRole> getAll(boolean npc) {
		return getItems(selectQuery(
				"select e from ECaseRole as e where npc=:npc order by name asc",
				new String[] { "npc" },
				new Object[] { npc }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseRoleDao#getAllByCasId(int, boolean)
	 */
	@Transactional
	public List<IECaseRole> getAllByCasId(int casId, boolean npc) {
		return getItems(selectQuery(
				"select e from ECaseRole as e where casCasId=:casId and npc=:npc order by name asc",
				new String[] { "casId" , "npc"},
				new Object[] { casId , npc }
				));
	}

}
