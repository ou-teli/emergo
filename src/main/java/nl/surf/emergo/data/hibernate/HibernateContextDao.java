/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data.hibernate;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import nl.surf.emergo.data.IContextDao;
import nl.surf.emergo.domain.IEContext;

/**
 * The Class HibernateContextDao.
 */
public class HibernateContextDao extends HibernateAllDao implements IContextDao {

	@Transactional
	protected List<IEContext> getItems(List items) {
		return (List<IEContext>)items;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IContextDao#get(int)
	 */
	@Transactional
	public IEContext get(int conId) {
		List<IEContext> contexts = getItems(selectQuery(
				"select e from EContext as e where conId=:conId",
				new String[] { "conId" },
				new Object[] { conId }
				));
		if (contexts.size() == 1) {
			return contexts.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IContextDao#exists(java.lang.String)
	 */
	@Transactional
	public boolean exists(String context) {
		List<IEContext> contexts = getItems(selectQuery(
				"select e from EContext as e where context=:context",
				new String[] { "context" },
				new Object[] { context }
				));
		if (contexts.size() > 0) {
			return true;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IContextDao#save(nl.surf.emergo.domain.IEContext)
	 */
	public void save(IEContext eContext) {
		super.save(eContext);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IContextDao#delete(nl.surf.emergo.domain.IEContext)
	 */
	public void delete(IEContext eContext) {
		super.delete(eContext);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IContextDao#delete(int)
	 */
	public void delete(int conId) {
		super.delete("select e from EContext as e where conId=" + conId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IContextDao#flush()
	 */
	public void flush() {
		super.flush();
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IContextDao#clear()
	 */
	public void clear() {
		super.clear();
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IContextDao#getAll()
	 */
	@Transactional
	public List<IEContext> getAll() {
		return getItems(selectQuery(
				"select e from EContext as e order by context asc"
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IContextDao#getAll(boolean)
	 */
	@Transactional
	public List<IEContext> getAll(boolean active) {
		return getItems(selectQuery(
				"select e from EContext as e where active=:active order by context asc",
				new String[] { "active" },
				new Object[] { active }
				));
	}

}
