/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data.hibernate;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import nl.surf.emergo.data.IAccountRequestDao;
import nl.surf.emergo.domain.IEAccountRequest;

/**
 * The Class HibernateAccountDao.
 */
public class HibernateAccountRequestDao extends HibernateAllDao implements
		IAccountRequestDao {

	@Transactional
	protected List<IEAccountRequest> getItems(List items) {
		return (List<IEAccountRequest>)items;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountDao#get(int)
	 */
	@Transactional
	public IEAccountRequest get(int arqId) {
		List<IEAccountRequest> accountRequests = getItems(selectQuery(
				"select e from EAccountRequest as e where arqId=:arqId",
				new String[] { "arqId" },
				new Object[] { arqId }
				));
		if (accountRequests.size() == 1) {
			return accountRequests.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountRequestDao#get(java.lang.String)
	 */
	@Transactional
	public IEAccountRequest get(String userid) {
		List<IEAccountRequest> accountRequests = getItems(selectQuery(
				"select e from EAccountRequest as e where userid=:userid",
				new String[] { "userid" },
				new Object[] { userid }
				));
		if (accountRequests.size() == 1) {
			return accountRequests.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountRequestDao#get(java.lang.String, java.lang.String, boolean)
	 */
	@Transactional
	public IEAccountRequest get(String userid, String password, boolean processed) {
		List<IEAccountRequest> accountRequests = getItems(selectQuery(
				"select e from EAccountRequest as e where userid=:userid and password=:password and processed=:processed",
				new String[] { "userid", "password", "processed" },
				new Object[] { userid, password, processed }
				));
		if (accountRequests.size() == 1) {
			return accountRequests.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountRequestDao#save(nl.surf.emergo.domain.IEAccountRequest)
	 */
	public void save(IEAccountRequest eAccountRequest) {
		super.save(eAccountRequest);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountRequestDao#delete(nl.surf.emergo.domain.IEAccountRequest)
	 */
	public void delete(IEAccountRequest eAccountRequest) {
		super.delete(eAccountRequest);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountRequestDao#delete(int)
	 */
	public void delete(int arqId) {
		super.delete("select e from EAccountRequest as e where arqId=" + arqId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountRequestDao#getAll()
	 */
	@Transactional
	public List<IEAccountRequest> getAll() {
		return getItems(selectQuery(
				"select e from EAccountRequest as e order by userid asc"
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountRequestDao#getAll(boolean)
	 */
	@Transactional
	public List<IEAccountRequest> getAll(boolean processed) {
		return getItems(selectQuery(
				"select e from EAccountRequest as e where processed=:processed order by userid asc",
				new String[] { "processed" },
				new Object[] { processed }
				));
	}

}
