/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data.hibernate;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import nl.surf.emergo.data.IComponentDao;
import nl.surf.emergo.domain.IEComponent;

/**
 * The Class HibernateComponentDao.
 */
public class HibernateComponentDao extends HibernateAllDao implements
		IComponentDao {

	@Transactional
	protected List<IEComponent> getItems(List items) {
		return (List<IEComponent>)items;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IComponentDao#get(int)
	 */
	@Transactional
	public IEComponent get(int comId) {
		List<IEComponent> components = getItems(selectQuery(
				"select e from EComponent as e where comId=:comId",
				new String[] { "comId" },
				new Object[] { comId }
				));
		if (components.size() == 1) {
			return components.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IComponentDao#get(java.lang.String)
	 */
	@Transactional
	public IEComponent get(String uuid) {
		List<IEComponent> components = getItems(selectQuery(
				"select e from EComponent as e where uuid=:uuid",
				new String[] { "uuid" },
				new Object[] { uuid }
				));
		if (components.size() == 1) {
			return components.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IComponentDao#exists(int, java.lang.String, int)
	 */
	@Transactional
	public boolean exists(int accId, String code, int version) {
		List<IEComponent> components = getItems(selectQuery(
				"select e from EComponent as e where accAccId=:accId and code=:code and version=:version",
				new String[] { "accId", "code", "version" },
				new Object[] { accId, code, version }
				));
		if (components.size() > 0) {
			return true;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IComponentDao#save(nl.surf.emergo.domain.IEComponent)
	 */
	public void save(IEComponent eComponent) {
		super.save(eComponent);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IComponentDao#delete(nl.surf.emergo.domain.IEComponent)
	 */
	public void delete(IEComponent eComponent) {
		super.delete(eComponent);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IComponentDao#delete(int)
	 */
	public void delete(int comId) {
		super.delete("select e from EComponent as e where comId=" + comId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IComponentDao#getAll()
	 */
	@Transactional
	public List<IEComponent> getAll() {
		return getItems(selectQuery(
				"select e from EComponent as e order by code asc"
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IComponentDao#getAllByAccId(int)
	 */
	@Transactional
	public List<IEComponent> getAllByAccId(int accId) {
		return getItems(selectQuery(
				"select e from EComponent as e where accAccId=:accId order by code asc",
				new String[] { "accId" },
				new Object[] { accId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IComponentDao#getAll(boolean)
	 */
	@Transactional
	public List<IEComponent> getAll(boolean active) {
		return getItems(selectQuery(
				"select e from EComponent as e where active=:active order by code asc",
				new String[] { "active" },
				new Object[] { active }
				));
	}

}
