/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import nl.surf.emergo.data.IRunTeamRunGroupDao;
import nl.surf.emergo.domain.IERunTeamRunGroup;

/**
 * The Class HibernateRunTeamRunGroupDao.
 */
public class HibernateRunTeamRunGroupDao extends HibernateAllDao implements
		IRunTeamRunGroupDao {

	@Transactional
	protected List<IERunTeamRunGroup> getItems(List items) {
		return (List<IERunTeamRunGroup>)items;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunTeamRunGroupDao#get(int)
	 */
	@Transactional
	public IERunTeamRunGroup get(int rtrId) {
		List<IERunTeamRunGroup> runteamrungroups = getItems(selectQuery(
				"select e from ERunTeamRunGroup as e where rtrId=:rtrId",
				new String[] { "rtrId" }, 
				new Object[] { rtrId }
				));
		if (runteamrungroups.size() == 1) {
			return runteamrungroups.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunTeamRunGroupDao#exists(int, int)
	 */
	@Transactional
	public boolean exists(int rutId, int rugId) {
		List<IERunTeamRunGroup> runteamrungroups = getItems(selectQuery(
				"select e from ERunTeamRunGroup as e where rutRutId=:rutId and rugRugId=:rugId",
				new String[] { "rutId", "rugId" },
				new Object[] { rutId, rugId }
				));
		if (runteamrungroups.size() > 0) {
			return true;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunTeamRunGroupDao#save(nl.surf.emergo.domain.IERunTeamRunGroup)
	 */
	public void save(IERunTeamRunGroup eRunTeamRunGroup) {
		super.save(eRunTeamRunGroup);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunTeamRunGroupDao#delete(nl.surf.emergo.domain.IERunTeamRunGroup)
	 */
	public void delete(IERunTeamRunGroup eRunTeamRunGroup) {
		super.delete(eRunTeamRunGroup);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunTeamRunGroupDao#delete(int)
	 */
	public void delete(int rtrId) {
		super.delete("select e from ERunTeamRunGroup as e where rtrId=" + rtrId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunTeamRunGroupDao#getAll()
	 */
	@Transactional
	public List<IERunTeamRunGroup> getAll() {
		return getItems(selectQuery(
				"select e from ERunTeamRunGroup as e order by rtrId asc"
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunTeamRunGroupDao#getAllByRutId(int)
	 */
	@Transactional
	public List<IERunTeamRunGroup> getAllByRutId(int rutId) {
		return getItems(selectQuery(
				"select e from ERunTeamRunGroup as e where rutRutId=:rutId order by rtrId asc",
				new String[] { "rutId" },
				new Object[] { rutId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunTeamRunGroupDao#getAllByRugId(int)
	 */
	@Transactional
	public List<IERunTeamRunGroup> getAllByRugId(int rugId) {
		return getItems(selectQuery(
				"select e from ERunTeamRunGroup as e where rugRugId=:rugId order by rtrId asc",
				new String[] { "rugId" },
				new Object[] { rugId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunTeamRunGroupDao#getAllByRugIds(List)
	 */
	@Transactional
	public List<IERunTeamRunGroup> getAllByRugIds(List<Integer> rugIds) {
		if (rugIds.size() == 0)
			return new ArrayList<IERunTeamRunGroup>();
		return getItems(selectQuery(
				"select e from ERunTeamRunGroup as e where rugRugId in :rugIds order by rtrId asc",
				new String[] { "rugIds" },
				new Object[] { rugIds }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunTeamRunGroupDao#getAllByRutIdRugId(int, int)
	 */
	@Transactional
	public List<IERunTeamRunGroup> getAllByRutIdRugId(int rutId, int rugId) {
		return getItems(selectQuery(
				"select e from ERunTeamRunGroup as e where rutRutId=:rutId and rugRugId=:rugId order by rtrId asc",
				new String[] { "rutId", "rugId" },
				new Object[] { rutId, rugId }
				));
	}

}
