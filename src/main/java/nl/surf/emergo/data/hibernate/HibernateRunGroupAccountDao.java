/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import nl.surf.emergo.data.IRunGroupAccountDao;
import nl.surf.emergo.domain.IERunGroupAccount;

/**
 * The Class HibernateRunGroupAccountDao.
 */
public class HibernateRunGroupAccountDao extends HibernateAllDao implements
		IRunGroupAccountDao {

	@Transactional
	protected List<IERunGroupAccount> getItems(List items) {
		return (List<IERunGroupAccount>)items;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupAccountDao#get(int)
	 */
	@Transactional
	public IERunGroupAccount get(int rgaId) {
		List<IERunGroupAccount> rungroupaccounts = getItems(selectQuery(
				"select e from ERunGroupAccount as e where rgaId=:rgaId",
				new String[] { "rgaId" },
				new Object[] { rgaId }
				));
		if (rungroupaccounts.size() == 1) {
			return rungroupaccounts.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupAccountDao#exists(int, int)
	 */
	@Transactional
	public boolean exists(int rugId, int accId) {
		List<IERunGroupAccount> rungroupaccounts = getItems(selectQuery(
				"select e from ERunGroupAccount as e where rugRugId=:rugId and accAccId=:accId",
				new String[] { "rugId", "accId" },
				new Object[] { rugId, accId }
				));
		if (rungroupaccounts.size() > 0) {
			return true;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupAccountDao#save(nl.surf.emergo.domain.IERunGroupAccount)
	 */
	public void save(IERunGroupAccount eRunGroupAccount) {
		super.save(eRunGroupAccount);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupAccountDao#delete(nl.surf.emergo.domain.IERunGroupAccount)
	 */
	public void delete(IERunGroupAccount eRunGroupAccount) {
		super.delete(eRunGroupAccount);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupAccountDao#delete(int)
	 */
	public void delete(int rgaId) {
		super.delete("select e from ERunGroupAccount as e where rgaId=" + rgaId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupAccountDao#getAll()
	 */
	@Transactional
	public List<IERunGroupAccount> getAll() {
		return getItems(selectQuery(
				"select e from ERunGroupAccount as e order by rgaId asc"
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupAccountDao#getAllByRgaIds(List)
	 */
	@Transactional
	public List<IERunGroupAccount> getAllByRgaIds(List<Integer> rgaIds) {
		if (rgaIds.size() == 0)
			return new ArrayList<IERunGroupAccount>();
		return getItems(selectQuery(
				"select e from ERunGroupAccount as e where rgaId in :rgaIds order by rgaId asc",
				new String[] { "rgaIds" },
				new Object[] { rgaIds }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupAccountDao#getAllByRugId(int)
	 */
	@Transactional
	public List<IERunGroupAccount> getAllByRugId(int rugId) {
		return getItems(selectQuery(
				"select e from ERunGroupAccount as e where rugRugId=:rugId order by rgaId asc",
				new String[] { "rugId" },
				new Object[] { rugId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupAccountDao#getAllByRugIds(List)
	 */
	@Transactional
	public List<IERunGroupAccount> getAllByRugIds(List<Integer> rugIds) {
		if (rugIds.size() == 0)
			return new ArrayList<IERunGroupAccount>();
		return getItems(selectQuery(
				"select e from ERunGroupAccount as e where rugRugId in :rugIds order by rgaId asc",
				new String[] { "rugIds" },
				new Object[] { rugIds }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupAccountDao#getAllByAccId(int)
	 */
	@Transactional
	public List<IERunGroupAccount> getAllByAccId(int accId) {
		return getItems(selectQuery(
				"select e from ERunGroupAccount as e where accAccId=:accId order by rgaId asc",
				new String[] { "accId" },
				new Object[] { accId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupAccountDao#getAllByAccIds(List)
	 */
	@Transactional
	public List<IERunGroupAccount> getAllByAccIds(List<Integer> accIds) {
		if (accIds.size() == 0)
			return new ArrayList<IERunGroupAccount>();
		return getItems(selectQuery(
				"select e from ERunGroupAccount as e where accAccId in :accIds order by rgaId asc",
				new String[] { "accIds" },
				new Object[] { accIds }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupAccountDao#getAllByRugIdAccId(int, int)
	 */
	@Transactional
	public List<IERunGroupAccount> getAllByRugIdAccId(int rugId, int accId) {
		return getItems(selectQuery(
				"select e from ERunGroupAccount as e where rugRugId=:rugId and accAccId=:accId order by rgaId asc",
				new String[] { "rugId", "accId" },
				new Object[] { rugId, accId }
				));
	}

}
