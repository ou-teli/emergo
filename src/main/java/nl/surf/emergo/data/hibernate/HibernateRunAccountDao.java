/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data.hibernate;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import nl.surf.emergo.data.IRunAccountDao;
import nl.surf.emergo.domain.IERunAccount;

/**
 * The Class HibernateRunAccountDao.
 */
public class HibernateRunAccountDao extends HibernateAllDao implements
		IRunAccountDao {

	@Transactional
	protected List<IERunAccount> getItems(List items) {
		return (List<IERunAccount>)items;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunAccountDao#get(int)
	 */
	@Transactional
	public IERunAccount get(int ruaId) {
		List<IERunAccount> runaccounts = getItems(selectQuery(
				"select e from ERunAccount as e where ruaId=:ruaId",
				new String[] { "ruaId" },
				new Object[] { ruaId }
				));
		if (runaccounts.size() == 1) {
			return runaccounts.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunAccountDao#exists(int, int)
	 */
	@Transactional
	public boolean exists(int runId, int accId) {
		List<IERunAccount> runaccounts = getItems(selectQuery(
				"select e from ERunAccount as e where runRunId=:runId and accAccId=:accId",
				new String[] { "runId", "accId" },
				new Object[] { runId, accId }
				));
		if (runaccounts.size() > 0) {
			return true;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunAccountDao#save(nl.surf.emergo.domain.IERunAccount)
	 */
	public void save(IERunAccount eRunAccount) {
		super.save(eRunAccount);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunAccountDao#delete(nl.surf.emergo.domain.IERunAccount)
	 */
	public void delete(IERunAccount eRunAccount) {
		super.delete(eRunAccount);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunAccountDao#delete(int)
	 */
	public void delete(int ruaId) {
		super.delete("select e from ERunAccount as e where ruaId=" + ruaId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunAccountDao#getAll()
	 */
	@Transactional
	public List<IERunAccount> getAll() {
		return getItems(selectQuery(
				"select e from ERunAccount as e order by runRunId, accAccId asc"
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunAccountDao#getAllByRunId(int)
	 */
	@Transactional
	public List<IERunAccount> getAllByRunId(int runId) {
		return getItems(selectQuery(
				"select e from ERunAccount as e where runRunId=:runId order by accAccId asc",
				new String[] { "runId" },
				new Object[] { runId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunAccountDao#getAllByAccId(int)
	 */
	@Transactional
	public List<IERunAccount> getAllByAccId(int accId) {
		return getItems(selectQuery(
				"select e from ERunAccount as e where accAccId=:accId order by runRunId asc",
				new String[] { "accId" },
				new Object[] { accId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunAccountDao#getAllByAccIdTutor(int)
	 */
	@Transactional
	public List<IERunAccount> getAllByAccIdTutor(int accId, boolean tutactive) {
		return getItems(selectQuery(
				"select e from ERunAccount as e where accAccId=:accId and tutactive=:tutactive order by runRunId asc",
				new String[] { "accId" , "tutactive" },
				new Object[] { accId , tutactive }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunAccountDao#getAllByAccIdStudent(int)
	 */
	@Transactional
	public List<IERunAccount> getAllByAccIdStudent(int accId, boolean stuactive) {
		return getItems(selectQuery(
				"select e from ERunAccount as e where accAccId=:accId and stuactive=:stuactive order by runRunId asc",
				new String[] { "accId" , "stuactive" },
				new Object[] { accId , stuactive }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunAccountDao#getAllByRunIdAccId(int, int)
	 */
	@Transactional
	public List<IERunAccount> getAllByRunIdAccId(int runId, int accId) {
		return getItems(selectQuery(
				"select e from ERunAccount as e where runRunId=:runId and accAccId=:accId",
				new String[] { "runId" , "accId" },
				new Object[] { runId , accId }
				));
	}

}
