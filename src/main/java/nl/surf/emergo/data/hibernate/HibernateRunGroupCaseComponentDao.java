/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import nl.surf.emergo.data.IRunGroupCaseComponentDao;
import nl.surf.emergo.domain.IERunGroupCaseComponent;

/**
 * The Class HibernateRunGroupCaseComponentDao.
 */
public class HibernateRunGroupCaseComponentDao extends HibernateAllDao
		implements IRunGroupCaseComponentDao {

	@Transactional
	protected List<IERunGroupCaseComponent> getItems(List items) {
		return (List<IERunGroupCaseComponent>)items;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupCaseComponentDao#get(int)
	 */
	@Transactional
	public IERunGroupCaseComponent get(int rgcId) {
		List<IERunGroupCaseComponent> runGroupCaseComponents = getItems(selectQuery(
				"select e from ERunGroupCaseComponent as e where rgcId=:rgcId",
				new String[] { "rgcId" },
				new Object[] { rgcId }
				));
		if (runGroupCaseComponents.size() == 1) {
			return runGroupCaseComponents.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupCaseComponentDao#get(int, int)
	 */
	@Transactional
	public IERunGroupCaseComponent get(int rugId, int cacId) {
		List<IERunGroupCaseComponent> runGroupCaseComponents = getItems(selectQuery(
				"select e from ERunGroupCaseComponent as e where rugRugId=:rugId and cacCacId=:cacId order by rgcId asc",
				new String[] { "rugId", "cacId" },
				new Object[] { rugId, cacId }
				));
		if (runGroupCaseComponents.size() == 1) {
			return runGroupCaseComponents.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupCaseComponentDao#save(nl.surf.emergo.domain.IERunGroupCaseComponent)
	 */
	public void save(IERunGroupCaseComponent eRunGroupCaseComponent) {
		super.save(eRunGroupCaseComponent);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupCaseComponentDao#delete(nl.surf.emergo.domain.IERunGroupCaseComponent)
	 */
	public void delete(IERunGroupCaseComponent eRunGroupCaseComponent) {
		super.delete(eRunGroupCaseComponent);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupCaseComponentDao#delete(int)
	 */
	public void delete(int rgcId) {
		super.delete("select e from ERunGroupCaseComponent as e where rgcId=" + rgcId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupCaseComponentDao#getAll()
	 */
	@Transactional
	public List<IERunGroupCaseComponent> getAll() {
		return getItems(selectQuery(
				"select e from ERunGroupCaseComponent as e order by rgcId asc"
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupCaseComponentDao#getAllByRugId(int)
	 */
	@Transactional
	public List<IERunGroupCaseComponent> getAllByRugId(int rugId) {
		return getItems(selectQuery(
				"select e from ERunGroupCaseComponent as e where rugRugId=:rugId order by rgcId asc",
				new String[] { "rugId" },
				new Object[] { rugId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupCaseComponentDao#getAllByCacId(int)
	 */
	@Transactional
	public List<IERunGroupCaseComponent> getAllByCacId(int cacId) {
		return getItems(selectQuery(
				"select e from ERunGroupCaseComponent as e where cacCacId=:cacId order by rgcId asc",
				new String[] { "cacId" },
				new Object[] { cacId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupCaseComponentDao#getAllByRugIdCacId(int, int)
	 */
	@Transactional
	public List<IERunGroupCaseComponent> getAllByRugIdCacId(int rugId, int cacId) {
		return getItems(selectQuery(
				"select e from ERunGroupCaseComponent as e where rugRugId=:rugId and cacCacId=:cacId order by rgcId asc",
				new String[] { "rugId", "cacId" },
				new Object[] { rugId, cacId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupCaseComponentDao#getAllByRugIdCacIds(int, List)
	 */
	@Transactional
	public List<IERunGroupCaseComponent> getAllByRugIdCacIds(int rugId, List<Integer> cacIds) {
		if (cacIds.size() == 0)
			return new ArrayList<IERunGroupCaseComponent>();
		return getItems(selectQuery(
				"select e from ERunGroupCaseComponent as e where rugRugId=:rugId and cacCacId in :cacIds order by rgcId asc",
				new String[] { "rugId", "cacIds" },
				new Object[] { rugId, cacIds }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupCaseComponentDao#getAllByRugIdsCacId(List, int)
	 */
	@Transactional
	public List<IERunGroupCaseComponent> getAllByRugIdsCacId(List<Integer> rugIds, int cacId) {
		if (rugIds.size() == 0)
			return new ArrayList<IERunGroupCaseComponent>();
		return getItems(selectQuery(
				"select e from ERunGroupCaseComponent as e where rugRugId in :rugIds and cacCacId=:cacId order by rgcId asc",
				new String[] { "rugIds", "cacId" },
				new Object[] { rugIds, cacId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupCaseComponentDao#getAllByRugIdsCacIds(List, List)
	 */
	@Transactional
	public List<IERunGroupCaseComponent> getAllByRugIdsCacIds(List<Integer> rugIds, List<Integer> cacIds) {
		if (rugIds.size() == 0 || cacIds.size() == 0)
			return new ArrayList<IERunGroupCaseComponent>();
		return getItems(selectQuery(
				"select e from ERunGroupCaseComponent as e where rugRugId in :rugIds and cacCacId in :cacIds order by rgcId asc",
				new String[] { "rugIds", "cacIds" },
				new Object[] { rugIds, cacIds }
				));
	}

}
