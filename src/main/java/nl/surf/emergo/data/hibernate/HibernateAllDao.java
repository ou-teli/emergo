/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data.hibernate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;
//import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class HibernateAllDao.
 */
public class HibernateAllDao extends HibernateDaoSupport {

	@Transactional
	protected List<?> selectQuery(String query) {
		return getHibernateTemplate().find(
				query
				);
	}

	@Transactional
	protected List<?> selectQuery(String query, String[] paramNames, Object[] paramValues) {
		return getHibernateTemplate().findByNamedParam(
				query,
				paramNames,
				paramValues
				);
	}

	protected String[] getParamNames(Map<String, String> keysAndValueParts) {
		List<String> itemList = new ArrayList<String>();
		for (Map.Entry<String, String> entry : keysAndValueParts.entrySet()) {
			boolean notEmpty = (!StringUtils.isEmpty(entry.getKey()) && !StringUtils.isEmpty(entry.getValue()));
			if (notEmpty) {
				itemList.add(entry.getKey());
			}
		}
		String[] items = new String[itemList.size()];
		for (int i=0;i<itemList.size();i++) {
			items[i] = itemList.get(i);
		}
		return items;
	}
	
	protected String[] addParamName(String[] paramNames, String paramName) {
		String[] newParamNames = new String[paramNames.length + 1];
		for (int i=0;i<paramNames.length;i++) {
			newParamNames[i] = paramNames[i];
		}
		newParamNames[paramNames.length] = paramName;
		return newParamNames;
	}
	
	protected Object[] getParamValues(Map<String, String> keysAndValueParts) {
		List<String> itemList = new ArrayList<String>();
		for (Map.Entry<String, String> entry : keysAndValueParts.entrySet()) {
			boolean notEmpty = (!StringUtils.isEmpty(entry.getKey()) && !StringUtils.isEmpty(entry.getValue()));
			if (notEmpty) {
				itemList.add(entry.getValue());
			}
		}
		Object[] items = new Object[itemList.size()];
		for (int i=0;i<itemList.size();i++) {
			items[i] = itemList.get(i);
		}
		return items;
	}

	protected Object[] addParamValue(Object[] paramValues, Object paramValue) {
		Object[] newParamValues = new Object[paramValues.length + 1];
		for (int i=0;i<paramValues.length;i++) {
			newParamValues[i] = paramValues[i];
		}
		newParamValues[paramValues.length] = paramValue;
		return newParamValues;
	}
	
	protected String getLikeSql(String prefix, String[] paramNames) {
		String sql = "";
		for (int i=0;i<paramNames.length;i++) {
			if (sql.length() > 0) {
				sql += " and ";
			}
			sql += prefix + paramNames[i] + " like :" + paramNames[i];
		}
		return sql;
	}
	
	protected String getInSql(String prefix, String paramName) {
		return prefix + paramName + " in :" + paramName;
	}
	
	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.I...Dao#save(nl.surf.emergo.domain.IE...)
	 */
	@Transactional
	public void save(Object eObject) {
		getHibernateTemplate().saveOrUpdate(eObject);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.I...Dao#delete(nl.surf.emergo.domain.IE...)
	 */
	@Transactional
	public void delete(Object eObject) {
		getHibernateTemplate().delete(eObject);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.I...Dao#flush(nl.surf.emergo.domain.IE...)
	 */
	@Transactional
	public void flush() {
		getHibernateTemplate().flush();
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.I...Dao#clear(nl.surf.emergo.domain.IE...)
	 */
	@Transactional
	public void clear() {
		getHibernateTemplate().clear();
	}

}
