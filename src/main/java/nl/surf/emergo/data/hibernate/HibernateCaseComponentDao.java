/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import nl.surf.emergo.data.ICaseComponentDao;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class HibernateCaseComponentDao.
 */
public class HibernateCaseComponentDao extends HibernateAllDao implements
		ICaseComponentDao {

	@Transactional
	protected List<IECaseComponent> getItems(List items) {
		return (List<IECaseComponent>)items;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseComponentDao#get(int)
	 */
	@Transactional
	public IECaseComponent get(int cacId) {
		List<IECaseComponent> caseComponents = getItems(selectQuery(
				"select e from ECaseComponent as e where cacId=:cacId",
				new String[] { "cacId" },
				new Object[] { cacId }
				));
		if (caseComponents.size() == 1) {
			return caseComponents.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseComponentDao#exists(int, java.lang.String)
	 */
	@Transactional
	public boolean exists(int casId, String name) {
		List<IECaseComponent> caseComponents = getItems(selectQuery(
				"select e from ECaseComponent as e where casCasId=:casId and name=:name",
				new String[] { "casId", "name" },
				new Object[] { casId, name }
				));
		if (caseComponents.size() > 0) {
			return true;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseComponentDao#save(nl.surf.emergo.domain.IECaseComponent)
	 */
	public void save(IECaseComponent eCaseComponent) {
		super.save(eCaseComponent);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseComponentDao#delete(nl.surf.emergo.domain.IECaseComponent)
	 */
	public void delete(IECaseComponent eCaseComponent) {
		super.delete(eCaseComponent);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseComponentDao#delete(int)
	 */
	public void delete(int cacId) {
		super.delete("select e from ECaseComponent as e where cacId=" + cacId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseComponentDao#getAll()
	 */
	@Transactional
	public List<IECaseComponent> getAll() {
		return getItems(selectQuery(
				"select e from ECaseComponent as e order by name asc"
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseComponentDao#getAllByCasId(int)
	 */
	@Transactional
	public List<IECaseComponent> getAllByCasId(int casId) {
		return getItems(selectQuery(
				"select e from ECaseComponent as e where casCasId=:casId order by name asc",
				new String[] { "casId" },
				new Object[] { casId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseComponentDao#getAllByCasIds(List)
	 */
	@Transactional
	public List<IECaseComponent> getAllByCasIds(List<Integer> casIds) {
		if (casIds.size() == 0)
			return new ArrayList<IECaseComponent>();
		return getItems(selectQuery(
				"select e from ECaseComponent as e where casCasId in :casIds order by name asc",
				new String[] { "casIds" },
				new Object[] { casIds }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseComponentDao#getAllByComId(int)
	 */
	@Transactional
	public List<IECaseComponent> getAllByComId(int comId) {
		return getItems(selectQuery(
				"select e from ECaseComponent as e where comComId=:comId order by name asc",
				new String[] { "comId" },
				new Object[] { comId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseComponentDao#getAllByCasIdComId(int, int)
	 */
	@Transactional
	public List<IECaseComponent> getAllByCasIdComId(int casId, int comId) {
		return getItems(selectQuery(
				"select e from ECaseComponent as e where casCasId=:casId and comComId=:comId order by name asc",
				new String[] { "casId", "comId" },
				new Object[] { casId, comId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.ICaseComponentDao#getAllByAccId(int)
	 */
	@Transactional
	public List<IECaseComponent> getAllByAccId(int accId) {
		return getItems(selectQuery(
				"select e from ECaseComponent as e where accAccId=:accId order by name asc",
				new String[] { "accId" },
				new Object[] { accId }
				));
	}

}
