/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data.hibernate;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import nl.surf.emergo.data.IRunTeamDao;
import nl.surf.emergo.domain.IERunTeam;

/**
 * The Class HibernateRunTeamDao.
 */
public class HibernateRunTeamDao extends HibernateAllDao implements
		IRunTeamDao {

	@Transactional
	protected List<IERunTeam> getItems(List items) {
		return (List<IERunTeam>)items;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunTeamDao#get(int)
	 */
	@Transactional
	public IERunTeam get(int rutId) {
		List<IERunTeam> runteams = getItems(selectQuery(
				"select e from ERunTeam as e where rutId=:rutId",
				new String[] { "rutId" },
				new Object[] { rutId }
				));
		if (runteams.size() == 1) {
			return runteams.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunTeamDao#exists(int, int, java.lang.String)
	 */
	@Transactional
	public boolean exists(int runId, String name) {
		List<IERunTeam> runteams = getItems(selectQuery(
				"select e from ERunTeam as e where runRunId=:runId and name=:name",
				new String[] { "runId", "name" },
				new Object[] { runId, name }
				));
		if (runteams.size() > 0) {
			return true;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunTeamDao#save(nl.surf.emergo.domain.IERunTeam)
	 */
	public void save(IERunTeam eRunTeam) {
		super.save(eRunTeam);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunTeamDao#delete(nl.surf.emergo.domain.IERunTeam)
	 */
	public void delete(IERunTeam eRunTeam) {
		super.delete(eRunTeam);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunTeamDao#delete(int)
	 */
	public void delete(int rutId) {
		super.delete("select e from ERunTeam as e where rutId=" + rutId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunTeamDao#getAll()
	 */
	@Transactional
	public List<IERunTeam> getAll() {
		return getItems(selectQuery(
				"select e from ERunTeam as e order by name asc"
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunTeamDao#getAllByRunId(int)
	 */
	@Transactional
	public List<IERunTeam> getAllByRunId(int runId) {
		return getItems(selectQuery(
				"select e from ERunTeam as e where runRunId=:runId order by name asc",
				new String[] { "runId" },
				new Object[] { runId }
				));
	}

}
