/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data.hibernate;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import nl.surf.emergo.data.IRunGroupCaseComponentUpdateDao;
import nl.surf.emergo.domain.IERunGroupCaseComponentUpdate;

/**
 * The Class HibernateRunGroupCaseComponentUpdateDao.
 */
public class HibernateRunGroupCaseComponentUpdateDao extends HibernateAllDao
		implements IRunGroupCaseComponentUpdateDao {

	@Transactional
	protected List<IERunGroupCaseComponentUpdate> getItems(List items) {
		return (List<IERunGroupCaseComponentUpdate>)items;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupCaseComponentUpdateDao#get(int)
	 */
	@Transactional
	public IERunGroupCaseComponentUpdate get(int rguId) {
		List<IERunGroupCaseComponentUpdate> runGroupCaseComponentUpdates = getItems(selectQuery(
				"select e from ERunGroupCaseComponentUpdate as e where rguId=:rguId",
				new String[] { "rguId" },
				new Object[] { rguId }
				));
		if (runGroupCaseComponentUpdates.size() == 1) {
			return runGroupCaseComponentUpdates.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupCaseComponentUpdateDao#save(nl.surf.emergo.domain.IERunGroupCaseComponentUpdate)
	 */
	public void save(IERunGroupCaseComponentUpdate eRunGroupCaseComponentUpdate) {
		super.save(eRunGroupCaseComponentUpdate);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupCaseComponentUpdateDao#delete(nl.surf.emergo.domain.IERunGroupCaseComponentUpdate)
	 */
	public void delete(IERunGroupCaseComponentUpdate eRunGroupCaseComponentUpdate) {
		super.delete(eRunGroupCaseComponentUpdate);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupCaseComponentUpdateDao#delete(int)
	 */
	public void delete(int rguId) {
		super.delete("select e from ERunGroupCaseComponentUpdate as e where rguId=" + rguId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupCaseComponentUpdateDao#getAll()
	 */
	@Transactional
	public List<IERunGroupCaseComponentUpdate> getAll() {
		return getItems(selectQuery(
				"select e from ERunGroupCaseComponentUpdate as e order by rguId asc"
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupCaseComponentUpdateDao#getAllByRugId(int)
	 */
	@Transactional
	public List<IERunGroupCaseComponentUpdate> getAllByRugId(int rugId) {
		return getItems(selectQuery(
				"select e from ERunGroupCaseComponentUpdate as e where rugRugId=:rugId order by rguId asc",
				new String[] { "rugId" },
				new Object[] { rugId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupCaseComponentUpdateDao#getAllByRugId(int, boolean)
	 */
	@Transactional
	public List<IERunGroupCaseComponentUpdate> getAllByRugId(int rugId, boolean processed) {
		return getItems(selectQuery(
				"select e from ERunGroupCaseComponentUpdate as e where rugRugId=:rugId and processed=:processed order by rguId asc",
				new String[] { "rugId" , "processed" },
				new Object[] { rugId , processed }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupCaseComponentUpdateDao#getAllByCacId(int)
	 */
	@Transactional
	public List<IERunGroupCaseComponentUpdate> getAllByCacId(int cacId) {
		return getItems(selectQuery(
				"select e from ERunGroupCaseComponentUpdate as e where cacCacId=:cacId order by rguId asc",
				new String[] { "cacId" },
				new Object[] { cacId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupCaseComponentUpdateDao#getAllByRugId(int)
	 */
	@Transactional
	public List<IERunGroupCaseComponentUpdate> getAllByRugFromId(int rugFromId) {
		return getItems(selectQuery(
				"select e from ERunGroupCaseComponentUpdate as e where rugRugFromId=:rugFromId order by rguId asc",
				new String[] { "rugFromId" },
				new Object[] { rugFromId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupCaseComponentUpdateDao#getAllByRugIdCacId(int, int)
	 */
	@Transactional
	public List<IERunGroupCaseComponentUpdate> getAllByRugIdCacId(int rugId, int cacId) {
		return getItems(selectQuery(
				"select e from ERunGroupCaseComponentUpdate as e where rugRugId=:rugId and cacCacId=:cacId order by rguId asc",
				new String[] { "rugId", "cacId" },
				new Object[] { rugId, cacId }
				));
	}

}
