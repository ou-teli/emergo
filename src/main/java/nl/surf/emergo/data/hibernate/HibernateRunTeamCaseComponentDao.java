/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data.hibernate;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import nl.surf.emergo.data.IRunTeamCaseComponentDao;
import nl.surf.emergo.domain.IERunTeamCaseComponent;

/**
 * The Class HibernateRunTeamCaseComponentDao.
 */
public class HibernateRunTeamCaseComponentDao extends HibernateAllDao
		implements IRunTeamCaseComponentDao {

	@Transactional
	protected List<IERunTeamCaseComponent> getItems(List items) {
		return (List<IERunTeamCaseComponent>)items;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunTeamCaseComponentDao#get(int)
	 */
	@Transactional
	public IERunTeamCaseComponent get(int rtcId) {
		List<IERunTeamCaseComponent> runTeamCaseComponents = getItems(selectQuery(
				"select e from ERunTeamCaseComponent as e where rtcId=:rtcId",
				new String[] { "rtcId" },
				new Object[] { rtcId }
				));
		if (runTeamCaseComponents.size() == 1) {
			return runTeamCaseComponents.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunTeamCaseComponentDao#get(int, int)
	 */
	@Transactional
	public IERunTeamCaseComponent get(int rutId, int cacId) {
		List<IERunTeamCaseComponent> runTeamCaseComponents = getItems(selectQuery(
				"select e from ERunTeamCaseComponent as e where rutRutId=:rutId and cacCacId=:cacId",
				new String[] { "rutId", "cacId" },
				new Object[] { rutId, cacId }
				));
		if (runTeamCaseComponents.size() == 1) {
			return runTeamCaseComponents.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunTeamCaseComponentDao#save(nl.surf.emergo.domain.IERunTeamCaseComponent)
	 */
	public void save(IERunTeamCaseComponent eRunTeamCaseComponent) {
		super.save(eRunTeamCaseComponent);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunTeamCaseComponentDao#delete(nl.surf.emergo.domain.IERunTeamCaseComponent)
	 */
	public void delete(IERunTeamCaseComponent eRunTeamCaseComponent) {
		super.delete(eRunTeamCaseComponent);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunTeamCaseComponentDao#delete(int)
	 */
	public void delete(int rtcId) {
		super.delete("select e from ERunTeamCaseComponent as e where rtcId=" + rtcId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunTeamCaseComponentDao#getAll()
	 */
	@Transactional
	public List<IERunTeamCaseComponent> getAll() {
		return getItems(selectQuery(
				"select e from ERunTeamCaseComponent as e order by rtcId asc"
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunTeamCaseComponentDao#getAllByRutId(int)
	 */
	@Transactional
	public List<IERunTeamCaseComponent> getAllByRutId(int rutId) {
		return getItems(selectQuery(
				"select e from ERunTeamCaseComponent as e where rutRutId=:rutId order by rtcId asc",
				new String[] { "rutId" },
				new Object[] { rutId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunTeamCaseComponentDao#getAllByCacId(int)
	 */
	@Transactional
	public List<IERunTeamCaseComponent> getAllByCacId(int cacId) {
		return getItems(selectQuery(
				"select e from ERunTeamCaseComponent as e where cacCacId=:cacId order by rtcId asc",
				new String[] { "cacId" },
				new Object[] { cacId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunTeamCaseComponentDao#getAllByRutIdCacId(int, int)
	 */
	@Transactional
	public List<IERunTeamCaseComponent> getAllByRutIdCacId(int rutId, int cacId) {
		return getItems(selectQuery(
				"select e from ERunTeamCaseComponent as e where rutRutId=:rutId and cacCacId=:cacId order by rtcId asc",
				new String[] { "rutId", "cacId" },
				new Object[] { rutId, cacId }
				));
	}

}
