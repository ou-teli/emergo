/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data.hibernate;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import nl.surf.emergo.data.IRunGroupDao;
import nl.surf.emergo.domain.IERunGroup;

/**
 * The Class HibernateRunGroupDao.
 */
public class HibernateRunGroupDao extends HibernateAllDao implements
		IRunGroupDao {

	@Transactional
	protected List<IERunGroup> getItems(List items) {
		return (List<IERunGroup>)items;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupDao#get(int)
	 */
	@Transactional
	public IERunGroup get(int rugId) {
		List<IERunGroup> rungroups = getItems(selectQuery(
				"select e from ERunGroup as e where rugId=:rugId",
				new String[] { "rugId" },
				new Object[] { rugId }
				));
		if (rungroups.size() == 1) {
			return rungroups.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupDao#exists(int, int, java.lang.String)
	 */
	@Transactional
	public boolean exists(int carId, int runId, String name) {
		List<IERunGroup> rungroups = getItems(selectQuery(
				"select e from ERunGroup as e where carCarId=:carId and runRunId=:runId and name=:name",
				new String[] { "carId", "runId", "name" },
				new Object[] { carId, runId, name }
				));
		if (rungroups.size() > 0) {
			return true;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupDao#save(nl.surf.emergo.domain.IERunGroup)
	 */
	public void save(IERunGroup eRunGroup) {
		super.save(eRunGroup);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupDao#delete(nl.surf.emergo.domain.IERunGroup)
	 */
	public void delete(IERunGroup eRunGroup) {
		super.delete(eRunGroup);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupDao#delete(int)
	 */
	public void delete(int rugId) {
		super.delete("select e from ERunGroup as e where rugId=" + rugId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupDao#getAll()
	 */
	@Transactional
	public List<IERunGroup> getAll() {
		return getItems(selectQuery(
				"select e from ERunGroup as e order by name asc"
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupDao#getAllByRunId(int)
	 */
	@Transactional
	public List<IERunGroup> getAllByRunId(int runId) {
		return getItems(selectQuery(
				"select e from ERunGroup as e where runRunId=:runId order by name asc",
				new String[] { "runId" },
				new Object[] { runId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupDao#getAllByCarId(int)
	 */
	@Transactional
	public List<IERunGroup> getAllByCarId(int carId) {
		return getItems(selectQuery(
				"select e from ERunGroup as e where carCarId=:carId order by name asc",
				new String[] { "carId" },
				new Object[] { carId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunGroupDao#getAllByRunIdCarId(int, int)
	 */
	@Transactional
	public List<IERunGroup> getAllByRunIdCarId(int runId, int carId) {
		return getItems(selectQuery(
				"select e from ERunGroup as e where runRunId=:runId and carCarId=:carId order by name asc",
				new String[] { "runId" , "carId" },
				new Object[] { runId , carId }
				));
	}

}
