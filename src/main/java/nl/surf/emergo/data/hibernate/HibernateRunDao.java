/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data.hibernate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;

import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.data.IRunDao;
import nl.surf.emergo.domain.IERun;

/**
 * The Class HibernateRunDao.
 */
public class HibernateRunDao extends HibernateAllDao implements IRunDao {

	@Transactional
	protected List<IERun> getItems(List items) {
		return (List<IERun>)items;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunDao#get(int)
	 */
	@Transactional
	public IERun get(int runId) {
		List<IERun> runs = getItems(selectQuery(
				"select e from ERun as e where runId=:runId",
				new String[] { "runId" },
				new Object[] { runId }
				));
		if (runs.size() == 1) {
			return runs.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunDao#exists(int, int, java.lang.String)
	 */
	@Transactional
	public boolean exists(int accId, int casId, String name) {
		List<IERun> runs = getItems(selectQuery(
				"select e from ERun as e where accAccId=:accId and casCasId=:casId and name=:name",
				new String[] { "accId", "casId", "name" },
				new Object[] { accId, casId, name }
				));
		if (runs.size() > 0) {
			return true;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunDao#save(nl.surf.emergo.domain.IERun)
	 */
	public void save(IERun eRun) {
		super.save(eRun);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunDao#delete(nl.surf.emergo.domain.IERun)
	 */
	public void delete(IERun eRun) {
		super.delete(eRun);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunDao#delete(int)
	 */
	public void delete(int runId) {
		super.delete("select e from ERun as e where runId=" + runId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunDao#getAll()
	 */
	@Transactional
	public List<IERun> getAll() {
		return getItems(selectQuery(
				"select e from ERun as e order by name asc"
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunDao#getAllFilter(String, String)
	 */
	@Transactional
	public List<IERun> getAllFilter(Map<String, String> keysAndValueParts) {
		String[] paramNames = getParamNames(keysAndValueParts);
		Object[] paramValues = getParamValues(keysAndValueParts);
		String whereSql = getLikeSql("", paramNames);
		if (whereSql.length() > 0) {
			whereSql = " where " + whereSql;
		}
		return getItems(selectQuery(
				"select e from ERun as e" + whereSql + " order by name asc",
				paramNames,
				paramValues
				));
	}
	
	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunDao#getAllByRunIds(List)
	 */
	@Transactional
	public List<IERun> getAllByRunIds(List<Integer> runIds) {
		if (runIds.size() == 0)
			return new ArrayList<IERun>();
		return getItems(selectQuery(
				"select e from ERun as e where runId in :runIds order by name asc",
				new String[] { "runIds" },
				new Object[] { runIds }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunDao#getAllByAccId(int)
	 */
	@Transactional
	public List<IERun> getAllByAccId(int accId) {
		return getItems(selectQuery(
				"select e from ERun as e where accAccId=:accId order by name asc",
				new String[] { "accId" },
				new Object[] { accId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunDao#getAllByCasId(int)
	 */
	@Transactional
	public List<IERun> getAllByCasId(int casId) {
		return getItems(selectQuery(
				"select e from ERun as e where casCasId=:casId order by name asc",
				new String[] { "casId" },
				new Object[] { casId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunDao#getAllByAccIdCasId(int, int)
	 */
	@Transactional
	public List<IERun> getAllByAccIdCasId(int accId, int casId) {
		return getItems(selectQuery(
				"select e from ERun as e where accAccId=:accId and casCasId=:casId order by name asc",
				new String[] { "accId", "casId" },
				new Object[] { accId, casId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunDao#getAllRunnableByAccId(int)
	 */
	@Transactional
	public List<IERun> getAllRunnableByAccId(int accId) {
		return getItems(selectQuery(
				"select e from ERun as e where accAccId=:accId and status=:status order by name asc",
				new String[] { "accId", "status" },
				new Object[] { accId, AppConstants.run_status_runnable }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunDao#getAllRunnableOpenAccess()
	 */
	@Transactional
	public List<IERun> getAllRunnableOpenAccess() {
		return getItems(selectQuery(
				"select e from ERun as e where openaccess=:openaccess and status=:status order by name asc",
				new String[] { "openaccess", "status" },
				new Object[] { true, AppConstants.run_status_runnable }
				));
	}

}
