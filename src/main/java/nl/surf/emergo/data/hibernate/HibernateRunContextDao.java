/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data.hibernate;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import nl.surf.emergo.data.IRunContextDao;
import nl.surf.emergo.domain.IERunContext;

/**
 * The Class HibernateRunContextDao.
 */
public class HibernateRunContextDao extends HibernateAllDao implements
		IRunContextDao {

	@Transactional
	protected List<IERunContext> getItems(List items) {
		return (List<IERunContext>)items;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunContextDao#get()
	 */
	@Transactional
	public IERunContext get(int rucId) {
		List<IERunContext> contexts = getItems(selectQuery(
				"select e from ERunContext as e where rucId=:rucId",
				new String[] { "rucId" },
				new Object[] { rucId }
				));
		if (contexts.size() == 1) {
			return contexts.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunContextDao#save(nl.surf.emergo.domain.IERunContext)
	 */
	public void save(IERunContext eRunContext) {
		super.save(eRunContext);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunContextDao#delete(nl.surf.emergo.domain.IERunContext)
	 */
	public void delete(IERunContext eRunContext) {
		super.delete(eRunContext);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunContextDao#delete(int)
	 */
	public void delete(int runContextId) {
		super.delete("select e from ERunContext as e where rucId=" + runContextId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IARunContextDao#getAll()
	 */
	@Transactional
	public List<IERunContext> getAll() {
		return getItems(selectQuery(
				"select e from ERunContext as e order by name asc"
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunContextDao#getAllByRunId(int)
	 */
	@Transactional
	public List<IERunContext> getAllByRunId(int runId) {
		return getItems(selectQuery(
				"select e from ERunContext as e where runRunId=:runId order by rucId asc",
				new String[] { "runId" },
				new Object[] { runId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunContextDao#getAllByConId(int)
	 */
	@Transactional
	public List<IERunContext> getAllByConId(int conId) {
		return getItems(selectQuery(
				"select e from ERunContext as e where conConId=:conId order by rucId asc",
				new String[] { "conId" },
				new Object[] { conId }
				));
	}

}
