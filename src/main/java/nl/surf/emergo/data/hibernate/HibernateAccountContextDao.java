/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import nl.surf.emergo.data.IAccountContextDao;
import nl.surf.emergo.domain.IEAccountContext;

/**
 * The Class HibernateAccountContextDao.
 */
public class HibernateAccountContextDao extends HibernateAllDao implements
		IAccountContextDao {

	@Transactional
	protected List<IEAccountContext> getItems(List<?> items) {
		return (List<IEAccountContext>)items;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountContextDao#get()
	 */
	@Transactional
	public IEAccountContext get(int accId) {
		List<IEAccountContext> accountContexts = getItems(selectQuery(
				"select e from EAccountContext as e where accId=:accId",
				new String[] { "accId" },
				new Object[] { accId }
				));
		if (accountContexts.size() == 1) {
			return accountContexts.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountContextDao#save(nl.surf.emergo.domain.IEAccountContext)
	 */
	public void save(IEAccountContext eAccountContext) {
		super.save(eAccountContext);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountContextDao#delete(nl.surf.emergo.domain.IEAccountContext)
	 */
	public void delete(IEAccountContext eAccountContext) {
		super.delete(eAccountContext);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountContextDao#delete(int)
	 */
	public void delete(int accountContextId) {
		super.delete("select e from EAccountContext as e where accId=" + accountContextId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountContextDao#getAll()
	 */
	@Transactional
	public List<IEAccountContext> getAll() {
		return getItems(selectQuery(
				"select e from EAccountContext as e order by accId asc"
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountContextDao#getAllByAccId(int)
	 */
	@Transactional
	public List<IEAccountContext> getAllByAccId(int accId) {
		return getItems(selectQuery(
				"select e from EAccountContext as e where accAccId=:accId order by accId asc",
				new String[] { "accId" },
				new Object[] { accId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountContextDao#getAllByConId(int)
	 */
	@Transactional
	public List<IEAccountContext> getAllByConId(int conId) {
		return getItems(selectQuery(
				"select e from EAccountContext as e where conConId=:conId order by accId asc",
				new String[] { "conId" },
				new Object[] { conId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountContextDao#getAllByAccIdActive(int, boolean)
	 */
	@Transactional
	public List<IEAccountContext> getAllByAccIdActive(int accId, boolean active) {
		return getItems(selectQuery(
				"select e from EAccountContext as e where accAccId=:accId and active=:active order by accId asc",
				new String[] { "accId" , "active" },
				new Object[] { accId , active }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountContextDao#getAllByConIdActive(int, boolean)
	 */
	@Transactional
	public List<IEAccountContext> getAllByConIdActive(int conId, boolean active) {
		return getItems(selectQuery(
				"select e from EAccountContext as e where conConId=:conId and active=:active order by accId asc",
				new String[] { "conId" , "active" },
				new Object[] { conId , active }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountContextDao#getAllByConIdsActive(List, boolean)
	 */
	@Transactional
	public List<IEAccountContext> getAllByConIdsActive(List<Integer> conIds, boolean active) {
		if (conIds.size() == 0)
			return new ArrayList<IEAccountContext>();
		return getItems(selectQuery(
				"select e from EAccountContext as e where active=:active and conConId in :conIds order by accId asc",
				new String[] { "active" , "conIds" },
				new Object[] { active , conIds }
				));
	}

}
