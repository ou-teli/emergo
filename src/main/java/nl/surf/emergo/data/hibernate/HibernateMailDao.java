/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data.hibernate;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import nl.surf.emergo.data.IMailDao;
import nl.surf.emergo.domain.IEMail;

/**
 * The Class HibernateMailDao.
 */
public class HibernateMailDao extends HibernateAllDao implements IMailDao {

	@Transactional
	protected List<IEMail> getItems(List items) {
		return (List<IEMail>)items;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IMailDao#get(int)
	 */
	@Transactional
	public IEMail get(int maiId) {
		List<IEMail> items = getItems(selectQuery(
				"select e from EMail as e where maiId=:maiId",
				new String[] { "maiId" },
				new Object[] { maiId }
				));
		if (items.size() == 1) {
			return items.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IMailDao#get(java.lang.String)
	 */
	@Transactional
	public IEMail get(String code) {
		List<IEMail> items = getItems(selectQuery(
				"select e from EMail as e where code=:code",
				new String[] { "code" },
				new Object[] { code }
				));
		if (items.size() == 1) {
			return items.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IMailDao#getAll()
	 */
	@Transactional
	public List<IEMail> getAll() {
		return getItems(selectQuery(
				"select e from EMail as e order by maiId asc"
				));
	}

}
