/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data.hibernate;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import nl.surf.emergo.data.IRunCaseComponentDao;
import nl.surf.emergo.domain.IERunCaseComponent;

/**
 * The Class HibernateRunCaseComponentDao.
 */
public class HibernateRunCaseComponentDao extends HibernateAllDao implements
		IRunCaseComponentDao {

	@Transactional
	protected List<IERunCaseComponent> getItems(List items) {
		return (List<IERunCaseComponent>)items;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunCaseComponentDao#get(int)
	 */
	@Transactional
	public IERunCaseComponent get(int rccId) {
		List<IERunCaseComponent> runcasecomponents = getItems(selectQuery(
				"select e from ERunCaseComponent as e where rccId=:rccId",
				new String[] { "rccId" },
				new Object[] { rccId }
				));
		if (runcasecomponents.size() == 1) {
			return runcasecomponents.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunCaseComponentDao#get(int, int)
	 */
	@Transactional
	public IERunCaseComponent get(int runId, int cacId) {
		List<IERunCaseComponent> runcasecomponents = getItems(selectQuery(
				"select e from ERunCaseComponent as e where runRunId=:runId and cacCacId=:cacId",
				new String[] { "runId", "cacId" },
				new Object[] { runId, cacId }
				));
		if (runcasecomponents.size() == 1) {
			return runcasecomponents.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunCaseComponentDao#save(nl.surf.emergo.domain.IERunCaseComponent)
	 */
	public void save(IERunCaseComponent eRunCaseComponent) {
		super.save(eRunCaseComponent);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunCaseComponentDao#delete(nl.surf.emergo.domain.IERunCaseComponent)
	 */
	public void delete(IERunCaseComponent eRunCaseComponent) {
		super.delete(eRunCaseComponent);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunCaseComponentDao#delete(int)
	 */
	public void delete(int rccId) {
		super.delete("select e from ERunCaseComponent as e where rccId=" + rccId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunCaseComponentDao#getAll()
	 */
	@Transactional
	public List<IERunCaseComponent> getAll() {
		return getItems(selectQuery(
				"select e from ERunCaseComponent as e order by rccId asc"
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunCaseComponentDao#getAllByRunId(int)
	 */
	@Transactional
	public List<IERunCaseComponent> getAllByRunId(int runId) {
		return getItems(selectQuery(
				"select e from ERunCaseComponent as e where runRunId=:runId order by rccId asc",
				new String[] { "runId" },
				new Object[] { runId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunCaseComponentDao#getAllByCacId(int)
	 */
	@Transactional
	public List<IERunCaseComponent> getAllByCacId(int cacId) {
		return getItems(selectQuery(
				"select e from ERunCaseComponent as e where cacCacId=:cacId order by rccId asc",
				new String[] { "cacId" },
				new Object[] { cacId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IRunCaseComponentDao#getAllByRunIdCacId(int, int)
	 */
	@Transactional
	public List<IERunCaseComponent> getAllByRunIdCacId(int runId, int cacId) {
		return getItems(selectQuery(
				"select e from ERunCaseComponent as e where runRunId=:runId and cacCacId=:cacId order by rccId asc",
				new String[] { "runId", "cacId" },
				new Object[] { runId, cacId }
				));
	}

}
