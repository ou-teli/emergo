/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data.hibernate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;

import nl.surf.emergo.data.IAccountDao;
import nl.surf.emergo.domain.IEAccount;

/**
 * The Class HibernateAccountDao.
 */
public class HibernateAccountDao extends HibernateAllDao implements
		IAccountDao {

	@Transactional
	protected List<IEAccount> getItems(List items) {
		return (List<IEAccount>)items;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountDao#get(int)
	 */
	@Transactional
	public IEAccount get(int accId) {
		List<IEAccount> accounts = getItems(selectQuery(
				"select e from EAccount as e where accId=:accId",
				new String[] { "accId" },
				new Object[] { accId }
				));
		if (accounts.size() == 1) {
			return accounts.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountDao#get(java.lang.String)
	 */
	@Transactional
	public IEAccount get(String userid) {
		List<IEAccount> accounts = getItems(selectQuery(
				"select e from EAccount as e where userid=:userid",
				new String[] { "userid"},
				new Object[] { userid}
				));
		if (accounts.size() == 1) {
			return accounts.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountDao#get(java.lang.String, java.lang.String, boolean)
	 */
	@Transactional
	public IEAccount get(String userid, String password, boolean active) {
		List<IEAccount> accounts = getItems(selectQuery(
				"select e from EAccount as e where userid=:userid and password=:password and active=:active",
				new String[] { "userid", "password", "active" },
				new Object[] { userid, password, active }
				));
		if (accounts.size() == 1) {
			return accounts.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountDao#save(nl.surf.emergo.domain.IEAccount)
	 */
	public void save(IEAccount eAccount) {
		super.save(eAccount);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountDao#delete(nl.surf.emergo.domain.IEAccount)
	 */
	public void delete(IEAccount eAccount) {
		super.delete(eAccount);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountDao#delete(int)
	 */
	public void delete(int accId) {
		super.delete("select e from EAccount as e where accId=" + accId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountDao#getAll()
	 */
	@Transactional
	public List<IEAccount> getAll() {
		return getItems(selectQuery(
				"select e from EAccount as e order by lastname asc"
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountDao#getAll(boolean)
	 */
	@Transactional
	public List<IEAccount> getAll(boolean active) {
		return getItems(selectQuery(
				"select e from EAccount as e where active=:active order by lastname asc",
				new String[] { "active" },
				new Object[] { active }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountDao#getAllByAccIds(boolean, List)
	 */
	@Transactional
	public List<IEAccount> getAllByAccIds(boolean active, List<Integer> accIds) {
		if (accIds.size() == 0)
			return new ArrayList<IEAccount>();
		return getItems(selectQuery(
				"select e from EAccount as e where active=:active and accId in :accIds order by lastname asc",
				new String[] { "active" , "accIds" },
				new Object[] { active , accIds }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountDao#getAllFilter(String, String)
	 */
	@Transactional
	public List<IEAccount> getAllFilter(Map<String, String> keysAndValueParts) {
		String[] paramNames = getParamNames(keysAndValueParts);
		Object[] paramValues = getParamValues(keysAndValueParts);
		String whereSql = getLikeSql("", paramNames);
		if (whereSql.length() > 0) {
			whereSql = " where " + whereSql;
		}
		return getItems(selectQuery(
				"select e from EAccount as e" + whereSql + " order by lastname asc",
				paramNames,
				paramValues
				));
	}
	
	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountDao#getAllFilter(boolean, String, String)
	 */
	@Transactional
	public List<IEAccount> getAllFilter(boolean active, Map<String, String> keysAndValueParts) {
		String[] paramNames = getParamNames(keysAndValueParts);
		Object[] paramValues = getParamValues(keysAndValueParts);
		paramNames = addParamName(paramNames, "active");
		paramValues = addParamValue(paramValues, active);
		String whereSql = getLikeSql("", paramNames);
		if (whereSql.length() > 0) {
			whereSql = " where " + whereSql;
		}
		return getItems(selectQuery(
				"select e from EAccount as e" + whereSql + " order by lastname asc",
				paramNames,
				paramValues
				));
	}
	
	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountDao#getAllByAccIdsFilter(boolean, List, String, String)
	 */
	@Transactional
	public List<IEAccount> getAllByAccIdsFilter(boolean active, List<Integer> accIds, Map<String, String> keysAndValueParts) {
		if (accIds.size() == 0)
			return new ArrayList<IEAccount>();

		String[] paramNames = getParamNames(keysAndValueParts);
		Object[] paramValues = getParamValues(keysAndValueParts);
		paramNames = addParamName(paramNames, "active");
		paramValues = addParamValue(paramValues, active);
		String whereSql = getLikeSql("", paramNames);
		String paramName = "accId";
		if (whereSql.length() > 0) {
			whereSql += " and ";
		}
		whereSql += getInSql("", paramName);
		if (whereSql.length() > 0) {
			whereSql = " where " + whereSql;
		}
		paramNames = addParamName(paramNames, paramName);
		paramValues = addParamValue(paramValues, accIds);
		return getItems(selectQuery(
				"select e from EAccount as e" + whereSql + " order by lastname asc",
				paramNames,
				paramValues
				));
	}
	
	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountDao#getAllByEmail(java.lang.String)
	 */
	@Transactional
	public List<IEAccount> getAllByEmail(String email) {
		return getItems(selectQuery(
				"select e from EAccount as e where email=:email",
				new String[] {"email"},
				new Object[] {email}
				));
	}

}
