/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import nl.surf.emergo.data.IBlobDao;
import nl.surf.emergo.domain.IEBlob;

/**
 * The Class HibernateBlobDao.
 */
public class HibernateBlobDao extends HibernateAllDao implements IBlobDao {

	@Transactional
	protected List<IEBlob> getItems(List items) {
		return (List<IEBlob>)items;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IBlobDao#get(int)
	 */
	@Transactional
	public IEBlob get(int bloId) {
		List<IEBlob> blobs = getItems(selectQuery(
				"select e from EBlob as e where bloId=:bloId",
				new String[] { "bloId" },
				new Object[] { bloId }
				));
		if (blobs.size() == 1) {
			return blobs.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IBlobDao#save(nl.surf.emergo.domain.IEBlob)
	 */
	public void save(IEBlob eBlob) {
		super.save(eBlob);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IBlobDao#delete(nl.surf.emergo.domain.IEBlob)
	 */
	public void delete(IEBlob eBlob) {
		super.delete(eBlob);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IBlobDao#delete(int)
	 */
	public void delete(int bloId) {
		super.delete("select e from EBlob as e where bloId=" + bloId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IBlobDao#getAll()
	 */
	@Transactional
	public List<IEBlob> getAll() {
		return getItems(selectQuery(
				"select e from EBlob as e order by bloId asc"
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IBlobDao#getAllByBloIds(List)
	 */
	@Transactional
	public List<IEBlob> getAllByBloIds(List<Integer> bloIds) {
		if (bloIds.size() == 0)
			return new ArrayList<IEBlob>();
		return getItems(selectQuery(
				"select e from EBlob as e where bloId in :bloIds order by bloId asc",
				new String[] { "bloIds" },
				new Object[] { bloIds }
				));
	}

}
