/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import nl.surf.emergo.data.IAccountRoleDao;
import nl.surf.emergo.domain.IEAccountRole;

/**
 * The Class HibernateAccountRoleDao.
 */
public class HibernateAccountRoleDao extends HibernateAllDao implements
		IAccountRoleDao {

	@Transactional
	protected List<IEAccountRole> getItems(List items) {
		return (List<IEAccountRole>)items;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountRoleDao#get(int)
	 */
	@Transactional
	public IEAccountRole get(int acrId) {
		List<IEAccountRole> acrs = getItems(selectQuery(
				"select e from EAccountRole as e where acrId=:acrId",
				new String[] { "acrId" },
				new Object[] { acrId }
				));
		if (acrs.size() == 1) {
			return acrs.get(0);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountRoleDao#save(nl.surf.emergo.domain.IEAccountRole)
	 */
	public void save(IEAccountRole eAccountRole) {
		super.save(eAccountRole);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountRoleDao#delete(nl.surf.emergo.domain.IEAccountRole)
	 */
	public void delete(IEAccountRole eAccountRole) {
		super.delete(eAccountRole);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountRoleDao#delete(int)
	 */
	public void delete(int acrId) {
		super.delete("select e from EAccountRole as e where acrId=" + acrId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountRoleDao#getAll()
	 */
	@Transactional
	public List<IEAccountRole> getAll() {
		return getItems(selectQuery(
				"select e from EAccountRole as e"
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountRoleDao#getAllByAccId(int)
	 */
	@Transactional
	public List<IEAccountRole> getAllByAccId(int accId) {
		return getItems(selectQuery(
				"select e from EAccountRole as e where accAccId=:accId order by acrId asc",
				new String[] { "accId" },
				new Object[] { accId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountRoleDao#getAllByAccIds(List)
	 */
	@Transactional
	public List<IEAccountRole> getAllByAccIds(List<Integer> accIds) {
		if (accIds.size() == 0)
			return new ArrayList<IEAccountRole>();
		return getItems(selectQuery(
				"select e from EAccountRole as e where accAccId in :accIds order by acrId asc",
				new String[] { "accIds" },
				new Object[] { accIds }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountRoleDao#getAllByRolId(int)
	 */
	@Transactional
	public List<IEAccountRole> getAllByRolId(int rolId) {
		return getItems(selectQuery(
				"select e from EAccountRole as e where rolRolId=:rolId order by acrId asc",
				new String[] { "rolId" },
				new Object[] { rolId }
				));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.data.IAccountRoleDao#getAllByRolIds(List)
	 */
	@Transactional
	public List<IEAccountRole> getAllByRolIds(List<Integer> rolIds) {
		if (rolIds.size() == 0)
			return new ArrayList<IEAccountRole>();
		return getItems(selectQuery(
				"select e from EAccountRole as e where rolRolId in :rolIds order by acrId asc",
				new String[] { "rolIds" },
				new Object[] { rolIds }
				));
	}

}
