/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data;

import java.util.List;

import nl.surf.emergo.domain.IEComponent;

/**
 * The Interface IComponentDao.
 */
public interface IComponentDao {

	/**
	 * Gets.
	 * 
	 * @param comId the com id
	 * 
	 * @return the iE component
	 */
	public IEComponent get(int comId);

	/**
	 * Gets.
	 * 
	 * @param uuid the uuid
	 * 
	 * @return the iE component
	 */
	public IEComponent get(String uuid);

	/**
	 * Checks if exists.
	 * 
	 * @param accId the acc id
	 * @param code the code
	 * @param version the version
	 * 
	 * @return true, if successful
	 */
	public boolean exists(int accId, String code, int version);

	/**
	 * Saves.
	 * 
	 * @param eComponent the e component
	 */
	public void save(IEComponent eComponent);

	/**
	 * Deletes.
	 * 
	 * @param eComponent the e component
	 */
	public void delete(IEComponent eComponent);

	/**
	 * Deletes.
	 * 
	 * @param comId the com id
	 */
	public void delete(int comId);

	/**
	 * Gets all.
	 * 
	 * @return all
	 */
	public List<IEComponent> getAll();

	/**
	 * Gets all by acc id.
	 * 
	 * @param accId the acc id
	 * 
	 * @return all by acc id
	 */
	public List<IEComponent> getAllByAccId(int accId);

	/**
	 * Gets all.
	 * 
	 * @param active the active
	 * 
	 * @return all
	 */
	public List<IEComponent> getAll(boolean active);

}
