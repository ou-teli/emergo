/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data;

import java.util.List;

import nl.surf.emergo.domain.IERunContext;

/**
 * The Interface IRunContextDao.
 */
public interface IRunContextDao {

	/**
	 * Gets.
	 * 
	 * @param rucId the run context id
	 * 
	 * @return the iE runcontext
	 */
	public IERunContext get(int rucId);

	/**
	 * Saves.
	 * 
	 * @param eRunContext the run context
	 */
	public void save(IERunContext eRunContext);

	/**
	 * Deletes.
	 * 
	 * @param eRunContext the run context
	 */
	public void delete(IERunContext eRunContext);

	/**
	 * Deletes.
	 * 
	 * @param runContextId the run context id
	 */
	public void delete(int runContextId);

	/**
	 * Gets all.
	 * 
	 * @return all
	 */
	public List<IERunContext> getAll();

	/**
	 * Gets all by run id.
	 * 
	 * @param runId the run id
	 * 
	 * @return all by run id
	 */
	public List<IERunContext> getAllByRunId(int runId);

	/**
	 * Gets all by context id.
	 * 
	 * @param conId the context id
	 * 
	 * @return all by context id
	 */
	public List<IERunContext> getAllByConId(int conId);

}
