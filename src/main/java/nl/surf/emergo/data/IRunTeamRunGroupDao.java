/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data;

import java.util.List;

import nl.surf.emergo.domain.IERunTeamRunGroup;

/**
 * The Interface IRunTeamRunGroupDao.
 */
public interface IRunTeamRunGroupDao {

	/**
	 * Gets.
	 * 
	 * @param rtrId the rtr id
	 * 
	 * @return the iE run team run group
	 */
	public IERunTeamRunGroup get(int rtrId);

	/**
	 * Checks if exists.
	 * 
	 * @param rutId the rut id
	 * @param rugId the rug id
	 * 
	 * @return true, if successful
	 */
	public boolean exists(int rutId, int rugId);

	/**
	 * Saves.
	 * 
	 * @param eRunTeamRunGroup the e run team run group
	 */
	public void save(IERunTeamRunGroup eRunTeamRunGroup);

	/**
	 * Deletes.
	 * 
	 * @param eRunTeamRunGroup the e run team run group
	 */
	public void delete(IERunTeamRunGroup eRunTeamRunGroup);

	/**
	 * Deletes.
	 * 
	 * @param rtrId the rtr id
	 */
	public void delete(int rtrId);

	/**
	 * Gets all.
	 * 
	 * @return all
	 */
	public List<IERunTeamRunGroup> getAll();

	/**
	 * Gets all by rut id.
	 * 
	 * @param rutId the rut id
	 * 
	 * @return all by rut id
	 */
	public List<IERunTeamRunGroup> getAllByRutId(int rutId);

	/**
	 * Gets all by rug id.
	 * 
	 * @param rugId the rug id
	 * 
	 * @return all by rug id
	 */
	public List<IERunTeamRunGroup> getAllByRugId(int rugId);

	/**
	 * Gets all by rug ids.
	 * 
	 * @param rugIds the rug ids
	 * 
	 * @return all by rug ids
	 */
	public List<IERunTeamRunGroup> getAllByRugIds(List<Integer> rugIds);

	/**
	 * Gets all by rut id rug id.
	 * 
	 * @param rutId the rut id
	 * @param rugId the rug id
	 * 
	 * @return all by rut id rug id
	 */
	public List<IERunTeamRunGroup> getAllByRutIdRugId(int rutId, int rugId);

}
