/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Interface ICaseComponentDao.
 */
public interface ICaseComponentDao {

	/**
	 * Gets.
	 * 
	 * @param cacId the cac id
	 * 
	 * @return the iE case component
	 */
	public IECaseComponent get(int cacId);

	/**
	 * Checks if exists.
	 * 
	 * @param casId the cas id
	 * @param name the name
	 * 
	 * @return true, if successful
	 */
	public boolean exists(int casId, String name);

	/**
	 * Saves.
	 * 
	 * @param eCaseComponent the e case component
	 */
	public void save(IECaseComponent eCaseComponent);

	/**
	 * Deletes.
	 * 
	 * @param eCaseComponent the e case component
	 */
	public void delete(IECaseComponent eCaseComponent);

	/**
	 * Deletes.
	 * 
	 * @param cacId the cac id
	 */
	public void delete(int cacId);

	/**
	 * Gets all.
	 * 
	 * @return all
	 */
	public List<IECaseComponent> getAll();

	/**
	 * Gets all by cas id.
	 * 
	 * @param casId the cas id
	 * 
	 * @return all by cas id
	 */
	public List<IECaseComponent> getAllByCasId(int casId);

	/**
	 * Gets all by cas ids.
	 * 
	 * @param casIds the cas ids
	 * 
	 * @return all by cas ids
	 */
	public List<IECaseComponent> getAllByCasIds(List<Integer> casIds);

	/**
	 * Gets all by com id.
	 * 
	 * @param comId the com id
	 * 
	 * @return all by com id
	 */
	public List<IECaseComponent> getAllByComId(int comId);

	/**
	 * Gets all by cas id and com id.
	 * 
	 * @param casId the cas id
	 * @param comId the com id
	 * 
	 * @return all by cas id and com id
	 */
	public List<IECaseComponent> getAllByCasIdComId(int casId, int comId);

	/**
	 * Gets all by acc id.
	 * 
	 * @param accId the acc id
	 * 
	 * @return all by acc id
	 */
	public List<IECaseComponent> getAllByAccId(int accId);

}
