/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data;

import java.util.List;

import nl.surf.emergo.domain.IEContext;

/**
 * The Interface IContextDao.
 */
public interface IContextDao {

	/**
	 * Gets.
	 * 
	 * @param conId the context id
	 * 
	 * @return the iE context
	 */
	public IEContext get(int conId);

	/**
	 * Checks if exists.
	 * 
	 * @param context the context
	 * 
	 * @return true, if successful
	 */
	public boolean exists(String context);

	/**
	 * Saves.
	 * 
	 * @param eContext the e context
	 */
	public void save(IEContext eContext);

	/**
	 * Deletes.
	 * 
	 * @param eContext the e context
	 */
	public void delete(IEContext eContext);

	/**
	 * Deletes.
	 * 
	 * @param conId the context id
	 */
	public void delete(int conId);

	/**
	 * Flushes.
	 * 
	 */
	public void flush();

	/**
	 * Clears.
	 * 
	 */
	public void clear();

	/**
	 * Gets all.
	 * 
	 * @return all
	 */
	public List<IEContext> getAll();

	/**
	 * Gets all.
	 * 
	 * @param active the active
	 * 
	 * @return all
	 */
	public List<IEContext> getAll(boolean active);

}
