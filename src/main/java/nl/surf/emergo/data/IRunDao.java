/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data;

import java.util.List;
import java.util.Map;

import nl.surf.emergo.domain.IERun;

/**
 * The Interface IRunDao.
 */
public interface IRunDao {

	/**
	 * Gets.
	 * 
	 * @param runId the run id
	 * 
	 * @return the iE run
	 */
	public IERun get(int runId);

	/**
	 * Checks if exists.
	 * 
	 * @param accId the acc id
	 * @param casId the cas id
	 * @param name the name
	 * 
	 * @return true, if successful
	 */
	public boolean exists(int accId, int casId, String name);

	/**
	 * Saves.
	 * 
	 * @param eRun the e run
	 */
	public void save(IERun eRun);

	/**
	 * Deletes.
	 * 
	 * @param eRun the e run
	 */
	public void delete(IERun eRun);

	/**
	 * Deletes.
	 * 
	 * @param runId the run id
	 */
	public void delete(int runId);

	/**
	 * Gets all.
	 * 
	 * @return all
	 */
	public List<IERun> getAll();

	/**
	 * Gets all by key=value filter.
	 * 
	 * @param keysAndValueParts the db field ids and value parts
	 * 
	 * @return all by filter
	 */
	public List<IERun> getAllFilter(Map<String, String> keysAndValueParts);

	/**
	 * Gets all by run ids.
	 * 
	 * @param runIds the run ids
	 * 
	 * @return all by run ids
	 */
	public List<IERun> getAllByRunIds(List<Integer> runIds);

	/**
	 * Gets all by acc id.
	 * 
	 * @param accId the acc id
	 * 
	 * @return all by acc id
	 */
	public List<IERun> getAllByAccId(int accId);

	/**
	 * Gets all by cas id.
	 * 
	 * @param casId the cas id
	 * 
	 * @return all by cas id
	 */
	public List<IERun> getAllByCasId(int casId);

	/**
	 * Gets all by acc id cas id.
	 * 
	 * @param accId the acc id
	 * @param casId the cas id
	 * 
	 * @return all by acc id cas id
	 */
	public List<IERun> getAllByAccIdCasId(int accId, int casId);

	/**
	 * Gets all runnable by acc id.
	 * 
	 * @param accId the acc id
	 * 
	 * @return all runnable by acc id
	 */
	public List<IERun> getAllRunnableByAccId(int accId);

	/**
	 * Gets all runnable open access runs.
	 * 
	 * @return all runnable open access runs
	 */
	public List<IERun> getAllRunnableOpenAccess();

}
