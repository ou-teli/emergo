/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.data;

import java.util.List;

import nl.surf.emergo.domain.IEAccountRole;

/**
 * The Interface IAccountRoleDao.
 */
public interface IAccountRoleDao {

	/**
	 * Gets.
	 * 
	 * @param acrId the acr id
	 * 
	 * @return the iE account role
	 */
	public IEAccountRole get(int acrId);

	/**
	 * Saves.
	 * 
	 * @param eAccountRole the e account role
	 */
	public void save(IEAccountRole eAccountRole);

	/**
	 * Deletes.
	 * 
	 * @param eAccountRole the e account role
	 */
	public void delete(IEAccountRole eAccountRole);

	/**
	 * Deletes.
	 * 
	 * @param acrId the acr id
	 */
	public void delete(int acrId);

	/**
	 * Gets all.
	 * 
	 * @return all
	 */
	public List<IEAccountRole> getAll();

	/**
	 * Gets all by acc id.
	 * 
	 * @param accId the acc id
	 * 
	 * @return all by acc id
	 */
	public List<IEAccountRole> getAllByAccId(int accId);

	/**
	 * Gets all by acc ids.
	 * 
	 * @param accIds the acc ids
	 * 
	 * @return all by acc ids
	 */
	public List<IEAccountRole> getAllByAccIds(List<Integer> accIds);

	/**
	 * Gets all by rol id.
	 * 
	 * @param rolId the rol id
	 * 
	 * @return all by rol id
	 */
	public List<IEAccountRole> getAllByRolId(int rolId);

	/**
	 * Gets all by rol ids.
	 * 
	 * @param rolIds the rol ids
	 * 
	 * @return all by rol ids
	 */
	public List<IEAccountRole> getAllByRolIds(List<Integer> rolIds);

}
