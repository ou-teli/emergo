package nl.surf.emergo.utilities;

import java.io.InputStream;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import xyz.capybara.clamav.ClamavClient;
import xyz.capybara.clamav.commands.scan.result.ScanResult;

public class AVUtil {
	private static Logger _log = LogManager.getLogger(AVUtil.class);
	private static String URL = PropsValues.CLAMAV_URL;
	private static int PORT = PropsValues.CLAMAV_PORT;

	private AVUtil() {
		// NOOP
	}

	private static boolean mustScan() {
		return StringUtils.isNotBlank(URL);
	}

	public static boolean isInfected(InputStream stream) {
		if (!mustScan()) {
			return false;
		}

		try {
			ClamavClient cl = new ClamavClient(URL, PORT);
			ScanResult scanResult = cl.scan(stream);
			return !(scanResult instanceof ScanResult.OK);
		} catch (RuntimeException e) {
			_log.error(e);
		}

		return true;
	}

	public static void reset() {
		URL = PropsValues.CLAMAV_URL;
		PORT = PropsValues.CLAMAV_PORT;
	}
	
}
