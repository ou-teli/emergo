/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.utilities;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import eu.rageproject.asset.manager.Severity;
import nl.surf.emergo.control.CDesktopComponents;

/**
 * Sample {@link eu.rageproject.asset.manager.IBridge} implementation
 */
public final class RageAssetsLogger {
	private static final Logger _log = LogManager.getLogger(RageAssetsLogger.class);

	// Note: user.dir will point to the Visual Studio Code installation directory
	// and throw access denied errors.
	// See
	// https://docs.oracle.com/javase/tutorial/essential/environment/sysprop.html

	private RageAssetsLogger() {
	}

	public static void Log(Logger aLog, Severity severity, String msg) {
		Map<Severity, Consumer<StringBuffer>> lLogMap = new HashMap<Severity, Consumer<StringBuffer>>() {
			private static final long serialVersionUID = 3285652229189402939L;

			{
				put(Severity.Critical, (StringBuffer) -> aLog.fatal(StringBuffer));
				put(Severity.Error, (StringBuffer) -> aLog.error(StringBuffer));
				put(Severity.Warning, (StringBuffer) -> aLog.warn(StringBuffer));
				put(Severity.Information, (StringBuffer) -> aLog.info(StringBuffer));
				put(Severity.Verbose, (StringBuffer) -> aLog.info(StringBuffer));
				// _log.debug(StringBuffer);
				// _log.trace(StringBuffer));
			}
		};
		String output = String.format("[%s] - %s", severity.toString(), msg);

		StringBuffer lStringBuffer = new StringBuffer(256);
		try {
			Method lAppendLogIndent = CDesktopComponents.sSpring().getClass().getDeclaredMethod("appendLogIndent",
					StringBuffer.class, String.class);
			lAppendLogIndent.setAccessible(true);
			lAppendLogIndent.invoke(CDesktopComponents.sSpring(), lStringBuffer, "");

			Method lAppendAction = CDesktopComponents.sSpring().getClass().getDeclaredMethod("appendAction",
					StringBuffer.class, String.class);
			lAppendAction.setAccessible(true);
			lAppendAction.invoke(CDesktopComponents.sSpring(), lStringBuffer, output);

			Method lAppendCaseTimeAndFreeMemoryAndTime = CDesktopComponents.sSpring().getClass()
					.getDeclaredMethod("appendCaseTimeAndFreeMemoryAndTime", StringBuffer.class);
			lAppendCaseTimeAndFreeMemoryAndTime.setAccessible(true);
			lAppendCaseTimeAndFreeMemoryAndTime.invoke(CDesktopComponents.sSpring(), lStringBuffer);
		} catch (InvocationTargetException | IllegalArgumentException | NoSuchMethodException
				| IllegalAccessException e) {
			_log.error(e);
		}

		lLogMap.get(severity).accept(lStringBuffer);
	}
}
