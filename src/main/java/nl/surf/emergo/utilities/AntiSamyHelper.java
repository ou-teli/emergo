package nl.surf.emergo.utilities;

import java.io.InputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.owasp.validator.html.AntiSamy;
import org.owasp.validator.html.Policy;
import org.owasp.validator.html.PolicyException;
import org.owasp.validator.html.ScanException;

public class AntiSamyHelper {
	private static final Logger _log = LogManager.getLogger(AntiSamyHelper.class);
	private static final String POLICYFILE = "/antisamy-ebay.xml";
	private static Policy policy = null;
	private static AntiSamy antiSamy = null;

	private AntiSamyHelper() {
		// NOOP
	}

	public static String cleanup(final String input) {
		String result = "";
		try {
			result = getAntiSamy().scan(input, getPolicy()).getCleanHTML();
		} catch (PolicyException | ScanException e) {
			_log.error(e);
		}
		return result;
	}

	private static Policy getPolicy() {
		if (policy == null) {
			try {
				_log.debug("Using policy file " + POLICYFILE);
				InputStream policyStream = AntiSamyHelper.class.getResourceAsStream(POLICYFILE);
				policy = Policy.getInstance(policyStream);
			} catch (PolicyException e) {
				_log.error(e);
			}
		}
		return policy;
	}

	private static AntiSamy getAntiSamy() {
		if (antiSamy == null) {
			_log.debug("Using policy file " + POLICYFILE);
			antiSamy = new AntiSamy();
		}
		return antiSamy;
	}

}
