package nl.surf.emergo.utilities.pwned;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PwndHelper {
	private static final Logger _log = LogManager.getLogger(PwndHelper.class);

	public PwndHelper() {
		// NOOP
	}

	public boolean isPwned(final String password) {
		boolean result = false;

		if (password == null) {
			return result;
		}

		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
			String url = "https://api.pwnedpasswords.com/range/" + StringHelper.getHashPrefix(password);

			HttpGet request = new HttpGet(url);
			request.addHeader(HttpHeaders.USER_AGENT, "EMERGO_CLIENT");

			try (CloseableHttpResponse response = httpClient.execute(request)) {
				HttpEntity entity = response.getEntity();
				if (entity != null) {
					result = DefaultPwnedPasswordsMapper.get().map(response.getStatusLine().getStatusCode(),
							EntityUtils.toString(entity), password);
				}
			}
		} catch (IOException e) {
			_log.error(e);
		}
		return result;
	}
}
