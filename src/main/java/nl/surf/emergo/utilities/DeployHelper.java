package nl.surf.emergo.utilities;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import nl.surf.emergo.view.VView;

public class DeployHelper implements ServletContextListener {
	private static final Logger _log = LogManager.getLogger(DeployHelper.class);
	protected AppHelper appHelper = new AppHelper();
	
	protected static final String webAppBase = "webapps";

	protected static final String webAppBasePath = System.getProperty("catalina.base") + File.separator + webAppBase + File.separator;

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		createLink(VView.getInitParameter("emergo.blob.path", "/blob/"));
		createLink(VView.getInitParameter("emergo.streaming.path", "/streaming/"));
		String extraPath = VView.getInitParameter("emergo.extra.path", "/extra/");
		createLink(extraPath);
		String lBasePath = webAppBasePath + PropsUtil.getWebappName() + File.separator;
		try {
			String from = lBasePath + appHelper.getAbsolutePath(VView.getInitParameter("emergo.extra.base.path", "/extra_base/"));
			String to = lBasePath + appHelper.getAbsolutePath(extraPath);
			_log.info("Will copy from " + from + " to " + to);
			FileHelper.copyDirectory(from, to, true, true);
		} catch (IOException e) {
		}
	}

	private void createLink(final String name) {
		Path source = Paths.get(PropsValues.STATIC_RESOURCES_BASE + File.separator + name);
		Path target = Paths.get(webAppBasePath + PropsUtil.getWebappName() + File.separator + name);

		_log.info("Will create symlink from " + source + " to " + target);

		if (!Files.exists(source)) {
			try {
				Files.createDirectory(source);
	        } catch (IOException e) {
	            _log.error(String.format("Failed to create directory: %s, error: %s", source, e.getMessage()));
				return;
	        } catch (SecurityException e) {
	            _log.error(String.format("Failed to create directory due to security restrictions: %s, error: %s", source, e.getMessage()));
				return;
	        }
			_log.info(String.format("Created directory %s", source));
		}

		try {
			if (Files.exists(target)) {
				if (Files.isSymbolicLink(target)) {
					if (!Files.readSymbolicLink(target).toString().equals(source.toString())) {
						Files.delete(target);
						Files.createSymbolicLink(target, source);
						_log.info(String.format("Created fresh symbolic link from %s to %s", source, target));
					}
				} else {
					_log.error(String.format("Target %s exists and is not a symbolic link", target));
				}
			} else {
				Files.createSymbolicLink(target, source);
				_log.info(String.format("Created symbolic link from %s to %s", source, target));
			}
		} catch (IOException e) {
			_log.error(e);
		}
	}

	private void deleteLink(final String name) {
		Path target = Paths.get(webAppBasePath + PropsUtil.getWebappName() + File.separator + name);

		try {
			if (Files.exists(target)) {
				if (Files.isSymbolicLink(target)) {
					Files.delete(target);
					_log.info(String.format("Deleted symbolic link %s.", target));
				} else {
					_log.error(String.format("Target %s exists and is not a symbolic link. Not deleted.", target));
				}
			} else {
				_log.info(String.format("Symbolic link: target %s doesn't exist. Nothing to delete.", target));
			}
		} catch (IOException e) {
			_log.error(e);
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		deleteLink(VView.getInitParameter("emergo.blob.path"));
		deleteLink(VView.getInitParameter("emergo.streaming.path"));
		deleteLink(VView.getInitParameter("emergo.extra.path"));
	}

}
