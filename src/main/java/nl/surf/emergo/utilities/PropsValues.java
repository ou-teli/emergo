package nl.surf.emergo.utilities;

import static java.util.Arrays.asList;

import java.util.Collections;
import java.util.List;

import xyz.capybara.clamav.ClamavClient;

public class PropsValues {

	private PropsValues() {
		// NOOP
	}

	public static boolean ENCODE_PASSWORD = GetterUtil.getBoolean(PropsUtil.get("emergo.password.encode"), true);
	public static boolean PASSWORD_ALLOW_FOR_UNENCODED = GetterUtil
			.getBoolean(PropsUtil.get("emergo.password.allow.for.unencoded"), false);
	public static boolean SSPRING_CACHE_RUNGROUP_STATUS = GetterUtil
			.getBoolean(PropsUtil.get("emergo.sspring.cache.rungroup.status"), true);
	public static boolean ADM_CONTEXTX_ENABLED = GetterUtil
			.getBoolean(PropsUtil.get("emergo.adm.contexts.enabled"), false);
	public static boolean ADM_QUERY_ENABLED = GetterUtil.getBoolean(PropsUtil.get("emergo.adm.query.enabled"),
			false);
	public static boolean ADM_FILES_ENABLED = GetterUtil.getBoolean(PropsUtil.get("emergo.adm.files.enabled"),
			false);
	public static boolean STREAMING_SERVER_STORE = GetterUtil
			.getBoolean(PropsUtil.get("emergo.streaming.server.store"), false);
	public static int STREAMING_RECORDER_CONCURRENT_NUMBER = GetterUtil
			.getInteger(PropsUtil.get("emergo.streaming.recorder.concurrent.number"), 1);
	public static String STREAMING_PLAYER_APPLICATION = GetterUtil
			.getString(PropsUtil.get("emergo.streaming.player.application"));
	public static String STREAMING_PLAYER_PATH_SEPARATOR = GetterUtil
			.getString(PropsUtil.get("emergo.streaming.player.path.separator"));
	public static String STREAMING_PLAYER_MANIFEST = GetterUtil
			.getString(PropsUtil.get("emergo.streaming.player.manifest"));
	public static List<String> STREAMING_PLAYER_FORMATS = Collections.unmodifiableList(
			asList(GetterUtil.getString(PropsUtil.get("emergo.streaming.player.formats")).split(",")));
	public static List<String> STREAMING_PLAYER_TRANSCODE_SUFFIXES = Collections.unmodifiableList(
			asList(GetterUtil.getString(PropsUtil.get("emergo.streaming.player.transcode.suffixes")).split(",")));
	public static String STREAMING_SERVER_FTP_NAME = GetterUtil
			.getString(PropsUtil.get("emergo.streaming.server.ftp.name"));
	public static String STREAMING_SERVER_FTP_PROTOCOL = GetterUtil
			.getString(PropsUtil.get("emergo.streaming.server.ftp.protocol"));
	public static int STREAMING_SERVER_FTP_PORT = GetterUtil
			.getInteger(PropsUtil.get("emergo.streaming.server.ftp.port"));
	public static String STREAMING_SERVER_FTP_USER = GetterUtil
			.getString(PropsUtil.get("emergo.streaming.server.ftp.user"));
	public static String STREAMING_SERVER_FTP_PASSWORD = GetterUtil
			.getString(PropsUtil.get("emergo.streaming.server.ftp.password"));
	public static String STREAMING_SERVER_FTP_ROOTPATH = GetterUtil
			.getString(PropsUtil.get("emergo.streaming.server.ftp.rootpath"));
	public static String STREAMING_SERVER_FTP_CASEVIDEOS_SUBPATH = GetterUtil
			.getString(PropsUtil.get("emergo.streaming.server.casevideos.subpath"));
	public static String STREAMING_SERVER_FTP_RUNVIDEOS_SUBPATH = GetterUtil
			.getString(PropsUtil.get("emergo.streaming.server.runvideos.subpath"));
	public static String LOCAL_RECORDER_FORMAT = GetterUtil
			.getString(PropsUtil.get("emergo.local.recorder.format"));
	public static String LOCAL_RECORDER_FILEEXTENSION = GetterUtil
			.getString(PropsUtil.get("emergo.local.recorder.fileextension"));
	public static boolean LOCAL_RECORDER_CONVERT = GetterUtil
			.getBoolean(PropsUtil.get("emergo.local.recorder.convert"), true);
	public static String LOCAL_RECORDER_VIDEO_CODEC = GetterUtil
			.getString(PropsUtil.get("emergo.local.recorder.video.codec"));
	public static String LOCAL_RECORDER_VIDEO_LIBRARY = GetterUtil
			.getString(PropsUtil.get("emergo.local.recorder.video.library"));
	public static String LOCAL_RECORDER_VIDEO_BITRATE = GetterUtil
			.getString(PropsUtil.get("emergo.local.recorder.video.bitrate"));
	public static String LOCAL_RECORDER_AUDIO_CODEC = GetterUtil
			.getString(PropsUtil.get("emergo.local.recorder.audio.codec"));
	public static String LOCAL_RECORDER_AUDIO_LIBRARY = GetterUtil
			.getString(PropsUtil.get("emergo.local.recorder.audio.library"));
	public static String LOCAL_RECORDER_AUDIO_BITRATE = GetterUtil
			.getString(PropsUtil.get("emergo.local.recorder.audio.bitrate"));
	public static int LOCAL_RECORDER_MAX_BITRATE = GetterUtil
			.getInteger(PropsUtil.get("emergo.local.recorder.max.bitrate"));
	public static boolean LOCAL_RECORDER_SYNCHRONIZATION_CHECK = GetterUtil
			.getBoolean(PropsUtil.get("emergo.local.recorder.synchronization.check"), false);
	public static int LOCAL_RECORDER_SYNCHRONIZATION_DIFFERENCE = GetterUtil
			.getInteger(PropsUtil.get("emergo.local.recorder.synchronization.difference"), 200);
	public static boolean LOCAL_RECORDER_ROTATION_CHECK = GetterUtil
			.getBoolean(PropsUtil.get("emergo.local.recorder.rotation.check"), false);
	public static String TESTS_SERVER_NAME = GetterUtil.getString(PropsUtil.get("emergo.tests.server.name"));
	public static String TESTS_SERVER_REST_CALL_GETTESTSDATA = GetterUtil
			.getString(PropsUtil.get("emergo.tests.server.rest.call.gettestsdata"));
	public static String TESTS_SERVER_REST_CALL_GETTESTSUSERDATA = GetterUtil
			.getString(PropsUtil.get("emergo.tests.server.rest.call.gettestsuserdata"));
	public static String TESTS_SERVER_REST_CALL_GETTESTS = GetterUtil
			.getString(PropsUtil.get("emergo.tests.server.rest.call.gettests"));
	public static String TESTS_SERVER_REST_CALL_GETTESTSRESULT = GetterUtil
			.getString(PropsUtil.get("emergo.tests.server.rest.call.gettestsresult"));
	public static String TESTS_SERVER_USER = GetterUtil.getString(PropsUtil.get("emergo.tests.server.user"));
	public static String TESTS_SERVER_PASSWORD = GetterUtil
			.getString(PropsUtil.get("emergo.tests.server.password"));
	public static String VIDEO_PLAYER_ID = GetterUtil.getString(PropsUtil.get("emergo.video.player.id"));
	public static String VIDEO_JWPLAYER_KEY = GetterUtil.getString(PropsUtil.get("emergo.video.jwplayer.key"));
	public static String SERVER_PROTOCOL = GetterUtil.getString(PropsUtil.get("emergo.server.protocol"));
	public static String ADM_MAINTAIN_FILES_SERVER_EXEC_FILE = GetterUtil
			.getString(PropsUtil.get("emergo.adm_maintain_files.server_exec_file"));

	public static String IDM_LOGOUT_URL = GetterUtil.getString(PropsUtil.get("idm.logout.url"));
	public static String IDM_SAVE_IP_PART = GetterUtil.getString(PropsUtil.get("idm.save.ip.part"));
	public static String IDM_DEFAULT_PASSWORD = GetterUtil.getString(PropsUtil.get("idm.default.password"));
	public static String FFMPEG_PATH = GetterUtil.getString(PropsUtil.get("ffmpeg.path"));

	public static int CLAMAV_PORT = GetterUtil.getInteger(PropsUtil.get("clamav.port"),
			ClamavClient.DEFAULT_SERVER_PORT);
	public static String BASICDATASOURCE_DRIVERCLASSNAME = GetterUtil
			.getString(PropsUtil.get("basicdatasource.driverClassName"));
	public static String BASICDATASOURCE_PASSWORD = GetterUtil
			.getString(PropsUtil.get("basicdatasource.password"));
	public static String BASICDATASOURCE_URL = GetterUtil.getString(PropsUtil.get("basicdatasource.url"));
	public static String BASICDATASOURCE_USERNAME = GetterUtil
			.getString(PropsUtil.get("basicdatasource.username"));
	public static String CLAMAV_URL = GetterUtil.getString(PropsUtil.get("clamav.url"));
	public static String SERVER_ID = GetterUtil.getString(PropsUtil.get("emergo.server.id"));
	public static String STREAMING_PATH_EXT = GetterUtil.getString(PropsUtil.get("emergo.streaming.path.ext"));
	public static String STREAMING_RECORDER_APPLICATION = GetterUtil
			.getString(PropsUtil.get("emergo.streaming.recorder.application"));
	public static String STREAMING_RECORDER_EXTENSION = GetterUtil
			.getString(PropsUtil.get("emergo.streaming.recorder.extension"));
	public static String STREAMING_RECORDER_AUTOFILEEXTENSION = GetterUtil
			.getString(PropsUtil.get("emergo.streaming.recorder.autofileextension"));
	public static String STREAMING_RECORDER_FILEEXTENSION = GetterUtil
			.getString(PropsUtil.get("emergo.streaming.recorder.fileextension"));
	public static String STREAMING_RECORDER_PATH = GetterUtil
			.getString(PropsUtil.get("emergo.streaming.recorder.path"));
	public static String STREAMING_RECORDER_HTML5_APPLICATION = GetterUtil
			.getString(PropsUtil.get("emergo.streaming.recorder.html5.application"));
	public static String STREAMING_RECORDER_HTML5_PROTOCOL = GetterUtil
			.getString(PropsUtil.get("emergo.streaming.recorder.html5.protocol"));
	public static String STREAMING_RECORDER_HTML5_SESSIONINFO = GetterUtil
			.getString(PropsUtil.get("emergo.streaming.recorder.html5.sessioninfo"));
	public static String STREAMING_SERVER_NAME = GetterUtil
			.getString(PropsUtil.get("emergo.streaming.server.name"));
	public static String STREAMING_SERVER_STREAMING_PROTOCOL = GetterUtil
			.getString(PropsUtil.get("emergo.streaming.server.streaming.protocol"));
	public static String TESTS_SERVER_REST_CALL_GETTEST = GetterUtil
			.getString(PropsUtil.get("emergo.tests.server.rest.call.gettest"));
	public static String VIDEO_JWPLAYER_PRIMARY_DEFAULT = GetterUtil
			.getString(PropsUtil.get("emergo.video.jwplayer.primary.default"));
	public static String TESTS_SERVER_REST_CALL_URL = GetterUtil
			.getString(PropsUtil.get("emergo.tests.server.rest.call.geturl"));
	public static List<String> ALLOWED_FILE_EXTENSIONS = Collections.unmodifiableList(
			asList(GetterUtil.getString(PropsUtil.get("allowed.file.extensions")).toLowerCase().split(",")));
	public static int MAX_UPLOAD_IN_BYTES = GetterUtil.getInteger(PropsUtil.get("max.upload.in.bytes"));
	public static String STATIC_RESOURCES_BASE = GetterUtil.getString(PropsUtil.get("static.resources.base"));
	
	public static String CSP_POLICY_NAME = GetterUtil.getString(PropsUtil.get("emergo.csp.policy.name"),"Content-Security-Policy");
	public static String CSP_POLICY_VALUE = GetterUtil.getString(PropsUtil.get("emergo.csp.policy.value"));
	
	public static boolean PROJECT_SSO_MODE = GetterUtil.getBoolean(PropsUtil.get("project.sso.mode"), false);
	
	public static void reset() {
		PropsUtil.reset();
		ENCODE_PASSWORD = GetterUtil.getBoolean(PropsUtil.get("emergo.password.encode"), true);
		PASSWORD_ALLOW_FOR_UNENCODED = GetterUtil
				.getBoolean(PropsUtil.get("emergo.password.allow.for.unencoded"), false);
		ADM_CONTEXTX_ENABLED = GetterUtil
				.getBoolean(PropsUtil.get("emergo.adm.contexts.enabled"), false);
		ADM_QUERY_ENABLED = GetterUtil.getBoolean(PropsUtil.get("emergo.adm.query.enabled"),
				false);
		ADM_FILES_ENABLED = GetterUtil.getBoolean(PropsUtil.get("emergo.adm.files.enabled"),
				false);
		STREAMING_SERVER_STORE = GetterUtil
				.getBoolean(PropsUtil.get("emergo.streaming.server.store"), false);
		STREAMING_RECORDER_CONCURRENT_NUMBER = GetterUtil
				.getInteger(PropsUtil.get("emergo.streaming.recorder.concurrent.number"), 1);
		STREAMING_PLAYER_APPLICATION = GetterUtil
				.getString(PropsUtil.get("emergo.streaming.player.application"));
		STREAMING_PLAYER_PATH_SEPARATOR = GetterUtil
				.getString(PropsUtil.get("emergo.streaming.player.path.separator"));
		STREAMING_PLAYER_MANIFEST = GetterUtil
				.getString(PropsUtil.get("emergo.streaming.player.manifest"));
		STREAMING_PLAYER_FORMATS = Collections.unmodifiableList(
				asList(GetterUtil.getString(PropsUtil.get("emergo.streaming.player.formats")).split(",")));
		STREAMING_PLAYER_TRANSCODE_SUFFIXES = Collections.unmodifiableList(
				asList(GetterUtil.getString(PropsUtil.get("emergo.streaming.player.transcode.suffixes")).split(",")));
		STREAMING_SERVER_FTP_NAME = GetterUtil
				.getString(PropsUtil.get("emergo.streaming.server.ftp.name"));
		STREAMING_SERVER_FTP_PROTOCOL = GetterUtil
				.getString(PropsUtil.get("emergo.streaming.server.ftp.protocol"));
		STREAMING_SERVER_FTP_PORT = GetterUtil
				.getInteger(PropsUtil.get("emergo.streaming.server.ftp.port"));
		STREAMING_SERVER_FTP_USER = GetterUtil
				.getString(PropsUtil.get("emergo.streaming.server.ftp.user"));
		STREAMING_SERVER_FTP_PASSWORD = GetterUtil
				.getString(PropsUtil.get("emergo.streaming.server.ftp.password"));
		STREAMING_SERVER_FTP_ROOTPATH = GetterUtil
				.getString(PropsUtil.get("emergo.streaming.server.ftp.rootpath"));
		STREAMING_SERVER_FTP_CASEVIDEOS_SUBPATH = GetterUtil
				.getString(PropsUtil.get("emergo.streaming.server.casevideos.subpath"));
		STREAMING_SERVER_FTP_RUNVIDEOS_SUBPATH = GetterUtil
				.getString(PropsUtil.get("emergo.streaming.server.runvideos.subpath"));
		LOCAL_RECORDER_FORMAT = GetterUtil
				.getString(PropsUtil.get("emergo.local.recorder.format"));
		LOCAL_RECORDER_FILEEXTENSION = GetterUtil
				.getString(PropsUtil.get("emergo.local.recorder.fileextension"));
		LOCAL_RECORDER_CONVERT = GetterUtil
				.getBoolean(PropsUtil.get("emergo.local.recorder.convert"), true);
		LOCAL_RECORDER_VIDEO_CODEC = GetterUtil
				.getString(PropsUtil.get("emergo.local.recorder.video.codec"));
		LOCAL_RECORDER_VIDEO_LIBRARY = GetterUtil
				.getString(PropsUtil.get("emergo.local.recorder.video.library"));
		LOCAL_RECORDER_VIDEO_BITRATE = GetterUtil
				.getString(PropsUtil.get("emergo.local.recorder.video.bitrate"));
		LOCAL_RECORDER_AUDIO_CODEC = GetterUtil
				.getString(PropsUtil.get("emergo.local.recorder.audio.codec"));
		LOCAL_RECORDER_AUDIO_LIBRARY = GetterUtil
				.getString(PropsUtil.get("emergo.local.recorder.audio.library"));
		LOCAL_RECORDER_AUDIO_BITRATE = GetterUtil
				.getString(PropsUtil.get("emergo.local.recorder.audio.bitrate"));
		LOCAL_RECORDER_MAX_BITRATE = GetterUtil
				.getInteger(PropsUtil.get("emergo.local.recorder.max.bitrate"));
		LOCAL_RECORDER_SYNCHRONIZATION_CHECK = GetterUtil
				.getBoolean(PropsUtil.get("emergo.local.recorder.synchronization.check"), false);
		LOCAL_RECORDER_SYNCHRONIZATION_DIFFERENCE = GetterUtil
				.getInteger(PropsUtil.get("emergo.local.recorder.synchronization.difference"), 200);
		LOCAL_RECORDER_ROTATION_CHECK = GetterUtil
				.getBoolean(PropsUtil.get("emergo.local.recorder.rotation.check"), false);
		TESTS_SERVER_NAME = GetterUtil.getString(PropsUtil.get("emergo.tests.server.name"));
		TESTS_SERVER_REST_CALL_GETTESTSDATA = GetterUtil
				.getString(PropsUtil.get("emergo.tests.server.rest.call.gettestsdata"));
		TESTS_SERVER_REST_CALL_GETTESTSUSERDATA = GetterUtil
				.getString(PropsUtil.get("emergo.tests.server.rest.call.gettestsuserdata"));
		TESTS_SERVER_REST_CALL_GETTESTS = GetterUtil
				.getString(PropsUtil.get("emergo.tests.server.rest.call.gettests"));
		TESTS_SERVER_REST_CALL_GETTESTSRESULT = GetterUtil
				.getString(PropsUtil.get("emergo.tests.server.rest.call.gettestsresult"));
		TESTS_SERVER_USER = GetterUtil.getString(PropsUtil.get("emergo.tests.server.user"));
		TESTS_SERVER_PASSWORD = GetterUtil
				.getString(PropsUtil.get("emergo.tests.server.password"));
		VIDEO_PLAYER_ID = GetterUtil.getString(PropsUtil.get("emergo.video.player.id"));
		VIDEO_JWPLAYER_KEY = GetterUtil.getString(PropsUtil.get("emergo.video.jwplayer.key"));
		SERVER_PROTOCOL = GetterUtil.getString(PropsUtil.get("emergo.server.protocol"));
		ADM_MAINTAIN_FILES_SERVER_EXEC_FILE = GetterUtil
				.getString(PropsUtil.get("emergo.adm_maintain_files.server_exec_file"));

		IDM_LOGOUT_URL = GetterUtil.getString(PropsUtil.get("idm.logout.url"));
		IDM_SAVE_IP_PART = GetterUtil.getString(PropsUtil.get("idm.save.ip.part"));
		IDM_DEFAULT_PASSWORD = GetterUtil.getString(PropsUtil.get("idm.default.password"));
		FFMPEG_PATH = GetterUtil.getString(PropsUtil.get("ffmpeg.path"));

		CLAMAV_PORT = GetterUtil.getInteger(PropsUtil.get("clamav.port"),
				ClamavClient.DEFAULT_SERVER_PORT);
		BASICDATASOURCE_DRIVERCLASSNAME = GetterUtil
				.getString(PropsUtil.get("basicdatasource.driverClassName"));
		BASICDATASOURCE_PASSWORD = GetterUtil
				.getString(PropsUtil.get("basicdatasource.password"));
		BASICDATASOURCE_URL = GetterUtil.getString(PropsUtil.get("basicdatasource.url"));
		BASICDATASOURCE_USERNAME = GetterUtil
				.getString(PropsUtil.get("basicdatasource.username"));
		CLAMAV_URL = GetterUtil.getString(PropsUtil.get("clamav.url"));
		SERVER_ID = GetterUtil.getString(PropsUtil.get("emergo.server.id"));
		STREAMING_PATH_EXT = GetterUtil.getString(PropsUtil.get("emergo.streaming.path.ext"));
		STREAMING_RECORDER_APPLICATION = GetterUtil
				.getString(PropsUtil.get("emergo.streaming.recorder.application"));
		STREAMING_RECORDER_EXTENSION = GetterUtil
				.getString(PropsUtil.get("emergo.streaming.recorder.extension"));
		STREAMING_RECORDER_AUTOFILEEXTENSION = GetterUtil
				.getString(PropsUtil.get("emergo.streaming.recorder.autofileextension"));
		STREAMING_RECORDER_FILEEXTENSION = GetterUtil
				.getString(PropsUtil.get("emergo.streaming.recorder.fileextension"));
		STREAMING_RECORDER_PATH = GetterUtil
				.getString(PropsUtil.get("emergo.streaming.recorder.path"));
		STREAMING_RECORDER_HTML5_APPLICATION = GetterUtil
				.getString(PropsUtil.get("emergo.streaming.recorder.html5.application"));
		STREAMING_RECORDER_HTML5_PROTOCOL = GetterUtil
				.getString(PropsUtil.get("emergo.streaming.recorder.html5.protocol"));
		STREAMING_RECORDER_HTML5_SESSIONINFO = GetterUtil
				.getString(PropsUtil.get("emergo.streaming.recorder.html5.sessioninfo"));
		STREAMING_SERVER_NAME = GetterUtil
				.getString(PropsUtil.get("emergo.streaming.server.name"));
		STREAMING_SERVER_STREAMING_PROTOCOL = GetterUtil
				.getString(PropsUtil.get("emergo.streaming.server.streaming.protocol"));
		TESTS_SERVER_REST_CALL_GETTEST = GetterUtil
				.getString(PropsUtil.get("emergo.tests.server.rest.call.gettest"));
		VIDEO_JWPLAYER_PRIMARY_DEFAULT = GetterUtil
				.getString(PropsUtil.get("emergo.video.jwplayer.primary.default"));
		TESTS_SERVER_REST_CALL_URL = GetterUtil
				.getString(PropsUtil.get("emergo.tests.server.rest.call.geturl"));
		ALLOWED_FILE_EXTENSIONS = Collections.unmodifiableList(
				asList(GetterUtil.getString(PropsUtil.get("allowed.file.extensions")).toLowerCase().split(",")));
		MAX_UPLOAD_IN_BYTES = GetterUtil.getInteger(PropsUtil.get("max.upload.in.bytes"));
		STATIC_RESOURCES_BASE = GetterUtil.getString(PropsUtil.get("static.resources.base"));
		
		CSP_POLICY_NAME = GetterUtil.getString(PropsUtil.get("emergo.csp.policy.name"),"Content-Security-Policy");
		CSP_POLICY_VALUE = GetterUtil.getString(PropsUtil.get("emergo.csp.policy.value"));
		
		PROJECT_SSO_MODE = GetterUtil.getBoolean(PropsUtil.get("project.sso.mode"), false);
		
		AVUtil.reset();
	}
}
