package nl.surf.emergo.utilities;

import java.io.File;
import java.net.URL;

import org.apache.commons.configuration2.BaseConfiguration;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PropsUtil {
	private static final Logger _log = LogManager.getLogger(PropsUtil.class);
	private static Configuration config;

	private PropsUtil() {
		// NOOP
	}

	public static String get(final String key) {
		return get(key, null);
	}

	public static String get(final String key, final String defaultValue) {
		String result = config.getString(key);
		return result != null ? result : defaultValue;
	}

	private static Configuration loadConfigurationFile() {
		Configurations configs = new Configurations();
		try {
			String webappName = getWebappName();
			String path = System.getProperty("catalina.base");

			if (!"".equals(webappName) && (!new File(path + File.separator + webappName + ".properties").isFile())) {
				webappName = "emergo";
				_log.warn(String.format("Falling back to default propertiesfile %s.properties", webappName));
			} else {
				_log.info(String.format("Will use propertiesfile %s.properties", webappName));
			}

			return configs.properties(new File(path + File.separator + webappName + ".properties"));

		} catch (ConfigurationException e) {
			_log.error(e);
			return new BaseConfiguration();
		}
	}

	public static String getWebappName() {
		URL resource = PropsUtil.class.getResource(".");
		String[] split = resource.toString().split("/");
		int pos = ArrayUtils.indexOf(split, "webapps");

		if (pos != ArrayUtils.INDEX_NOT_FOUND) {
			return split[pos + 1];
		}
		return "";
	}

	static {
		config = loadConfigurationFile();
	}

	public static void reset() {
		config = loadConfigurationFile();
	}
	
}
