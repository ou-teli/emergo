package nl.surf.emergo.utilities;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

public class CSPFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		if (!PropsValues.CSP_POLICY_VALUE.equals("") && response instanceof HttpServletResponse) {
			((HttpServletResponse)response).setHeader(PropsValues.CSP_POLICY_NAME, PropsValues.CSP_POLICY_VALUE);
		}
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
	}

}
