/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.utilities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Messagebox;

import nl.surf.emergo.business.impl.FileManager;
import nl.surf.emergo.view.VView;

public class ExcelHelper {

	private static final Logger _log = LogManager.getLogger(ExcelHelper.class);

	public Cell createOrUpdateXLSXCell(Row row, int colNum, CellType cellType, CellStyle cellStyle, String value,
			boolean pLog) {
		if (pLog)
			_log.info("Excel createOrUpdate row: " + row.getRowNum() + ", column: " + colNum + ", value: " + value);
		Cell cell = row.getCell(colNum);
		if (cell == null) {
			cell = row.createCell(colNum);
		}
		if (cellType != null && cellType == CellType.NUMERIC) {
			try {
				cell.setCellValue(Double.parseDouble(value));
			} catch (NumberFormatException e) {
				_log.debug("Excel createOrUpdate row: {}", e.getMessage());
			}
		} else if (cellType != null && cellType == CellType.BOOLEAN) {
			try {
				cell.setCellValue(Boolean.parseBoolean(value));
			} catch (Exception e) {
				_log.debug("Excel createOrUpdate row: {}", e.getMessage());
			}
		} else if (cellType != null && cellType == CellType.STRING) {
			try {
				cell.setCellValue(value);
			} catch (Exception e) {
				_log.debug("Excel createOrUpdate row: {}", e.getMessage());
			}
		} else {
			try {
				cell.setCellValue(Double.parseDouble(value));
			} catch (Exception e1) {
				if (value.equals("true") || value.equals("false")) {
					try {
						cell.setCellValue(Boolean.parseBoolean(value));
					} catch (Exception e2) {
						_log.debug("Excel createOrUpdate row: {}", e2.getMessage());
					}
				} else {
					try {
						cell.setCellValue(value);
					} catch (Exception e) {
						_log.debug("Excel createOrUpdate row: {}", e.getMessage());
					}
				}
			}
		}
		if (cellStyle != null) {
			try {
				cell.setCellStyle(cellStyle);
			} catch (Exception e) {
				_log.debug("Excel createOrUpdate row: {}", e.getMessage());
			}
		}
		return cell;
	}

	public String getXLSXCellValueAsString(Row row, int colNum) {
		Cell cell = row.getCell(colNum);
		if (cell == null) {
			return "";
		}
		if (cell.getCellType().compareTo(CellType.NUMERIC) == 0) {
			Double doubleValue = cell.getNumericCellValue();
			if (doubleValue % 1 == 0) {
				// no decimals
				return new Long(doubleValue.longValue()).toString();
			} else {
				return doubleValue.toString();
			}
		} else if (cell.getCellType().compareTo(CellType.BOOLEAN) == 0) {
			return "" + cell.getBooleanCellValue();
		} else if (cell.getCellType().compareTo(CellType.STRING) == 0) {
			return cell.getStringCellValue();
		} else {
			return "";
		}
	}

	public Row createOrUpdateXLSXRow(Sheet sheet, int rowNum, int colOffset, CellType[] cellTypes,
			CellStyle[] cellStyles, String[] values, boolean pLog) {
		Row row = sheet.getRow(rowNum);
		if (row == null) {
			row = sheet.createRow(rowNum);
		}
		for (int i = colOffset; i < (colOffset + values.length); i++) {
			createOrUpdateXLSXCell(row, i, (cellTypes != null && i < cellTypes.length) ? cellTypes[i] : null,
					(cellStyles != null && i < cellStyles.length) ? cellStyles[i] : null, values[i], pLog);
		}
		return row;
	}

	// See: https://www.callicoder.com/java-write-excel-file-apache-poi/
	// See: https://www.callicoder.com/java-read-excel-file-apache-poi/
	// See:
	// https://dzone.com/articles/resolve-outofmemoryerror-with-excelexport-export-e

	public Workbook createOrLoadWorkbook(boolean createWorkbook, boolean streaming, String absoluteTempPath,
			String fileName) {
		Workbook workbook = null;
		if (createWorkbook) {
			if (streaming) {
				workbook = new SXSSFWorkbook(100);
			} else {
				workbook = new XSSFWorkbook();
			}
		} else {
			// Create a Workbook from an Excel file (.xls or .xlsx)
			try {
				workbook = WorkbookFactory.create(new File(absoluteTempPath + fileName));
			} catch (IOException | EncryptedDocumentException e) {
				_log.error(e);
			}
			if (workbook != null && streaming) {
				workbook = new SXSSFWorkbook((XSSFWorkbook) workbook, 100);
			}
		}
		return workbook;
	}

	public void closeWorkbook(Workbook workbook) {
		try {
			workbook.close();
		} catch (IOException e) {
			_log.error(e);
		}
	}

	public boolean writeAndCloseWorkbook(Workbook workbook, String absoluteTempPath, String fileName,
			Component rootComponent, VView vView) {
		// create excel file within temporary path
		boolean fileCreated = false;
		try {
			FileManager fileManager = new FileManager();
			fileManager.createDir(absoluteTempPath);
			String servFileName = absoluteTempPath + fileName;
			// NOTE create temp file to save workbook, saving workbook in same file
			// generates an error
			String tempServFileName = absoluteTempPath + "temp_" + fileName;
			if (fileManager.fileExists(tempServFileName)) {
				fileManager.deleteFile(tempServFileName);
			}
			FileOutputStream fileOut = new FileOutputStream(tempServFileName);
			workbook.write(fileOut);
			closeWorkbook(workbook);
			fileOut.close();
			// NOTE remove possible old file and rename temp file to file
			if (fileManager.fileExists(servFileName)) {
				fileManager.deleteFile(servFileName);
			}
			fileManager.renameFile(tempServFileName, servFileName);
			fileCreated = true;
		} catch (Exception e) {
			closeWorkbook(workbook);
			vView.showMessagebox(
					rootComponent, vView.getLabel("tut_rungroupaccountownoverview.messagebox.text.filecreationerror")
							.replace("%1", fileName),
					vView.getLabel("messagebox.title.errors"), Messagebox.OK, Messagebox.ERROR);
		}
		return fileCreated;
	}

}
