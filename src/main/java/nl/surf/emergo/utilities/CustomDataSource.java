package nl.surf.emergo.utilities;

import org.apache.commons.dbcp2.BasicDataSource;

public class CustomDataSource extends BasicDataSource {

	public CustomDataSource() {
		super.setDriverClassName(PropsValues.BASICDATASOURCE_DRIVERCLASSNAME);
		super.setUrl(PropsValues.BASICDATASOURCE_URL);
		super.setUsername(PropsValues.BASICDATASOURCE_USERNAME);
		super.setPassword(PropsValues.BASICDATASOURCE_PASSWORD);
	}

}
