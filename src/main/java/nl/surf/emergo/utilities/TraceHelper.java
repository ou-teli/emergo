/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.utilities;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TraceHelper  {
	
	/**
	 * Logging.
	 */
	private static final Logger _log = LogManager.getLogger(TraceHelper.class);

	/**
	 * @see TraceMethod().
	 */
	private static final int STACK_DEPTH = 2;

	/**
	 * @see TraceMethod().
	 */
	private static final String PARMS_SEPARATOR = ", ";

	/**
	 * @see TraceMethod().
	 */
	private static final String PACKAGE_SEPARATOR = ".";

	/**
	 * Log the class and name of the method called.
	 * 
	 * @see STACK_DEPTH
	 */
	public static void traceMethod() {
		StackTraceElement caller = Thread.currentThread().getStackTrace()[STACK_DEPTH];
		String classname = caller.getClassName();
		_log.info(classname.substring(classname.lastIndexOf('.') + 1) + "."
				+ caller.getMethodName() + "() @ line "
				+ new Integer(caller.getLineNumber()).toString());
	}

	/**
	 * Log the class and name of the method called.
	 * 
	 * @see STACK_DEPTH
	 * @see PARMS_SEPARATOR
	 * @see PACKAGE_SEPARATOR
	 * 
	 * @param parms
	 *            Parameters to use in the debug output.
	 */
	public static void traceMethod(final Object... parms) {
		StackTraceElement caller = Thread.currentThread().getStackTrace()[STACK_DEPTH];
		String callername = caller.getClassName();

		// Reinvent the array.join() method once more.
		StringBuilder sb = new StringBuilder();
		for (Object parm : parms) {
			sb.append(PARMS_SEPARATOR);
			if (parm == null) {
				sb.append("null");
			} else {
				String cls = parm.getClass().getName();
				sb.append(cls.substring(cls.lastIndexOf(PACKAGE_SEPARATOR) + 1,
						cls.length()) + " " + parm.toString());
			}
		}

		String joinedparms;
		if (parms.length != 0) {
			joinedparms = sb.substring(2);
		} else {
			joinedparms = sb.toString();
		}

		_log.info(callername.substring(callername.lastIndexOf('.') + 1) + "."
				+ caller.getMethodName() + "(" + joinedparms + ") @ line "
				+ new Integer(caller.getLineNumber()).toString());
	}

	/**
	 * Dumps stack.
	 */
	public static void dumpStack() {
		StackTraceElement[] elements = Thread.currentThread().getStackTrace();
		for (int i=0;i<elements.length;i++) {
			StackTraceElement element = elements[i];
			String classname = element.getClassName();
			_log.info(classname.substring(classname.lastIndexOf('.') + 1) + "."
					+ element.getMethodName() + "() @ line "
					+ new Integer(element.getLineNumber()).toString());
		}
	}

}
