/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.utilities;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.zip.Deflater;
import java.util.zip.ZipInputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipOutputStream;
import org.zkoss.zul.Messagebox;

import nl.surf.emergo.control.CDesktopComponents;

public class ZipHelper {
	private static final Logger _log = LogManager.getLogger(ZipHelper.class);
	private static final int BUFFER_SIZE = 2048;

	private FileHelper fileHelper = new FileHelper();

	/**
	 * Package files to zip file.
	 *
	 * @param files        the files
	 * @param baseDir      the base dir
	 * @param blobDir      the blob dir
	 * @param streamingDir the base dir
	 * @param zipFile      the zip file
	 *
	 * @return true, if successful
	 */
	public boolean packageFilesToZip(List<String> files, String baseDir, String blobDir, String streamingDir,
			File zipFile) {
		String absolutePath = zipFile.getAbsolutePath();
		String filePath = absolutePath.substring(0, absolutePath.lastIndexOf(File.separator));
		fileHelper.createDir(filePath);

		byte[] buffer = new byte[18024];

		Iterator<String> iter = null;

		try {
			Files.deleteIfExists(zipFile.toPath());
		} catch (IOException e) {
			_log.error(e);
		}

		try (ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFile))) {
			// Set the compression ratio
			out.setLevel(Deflater.DEFAULT_COMPRESSION);
			out.setEncoding("UTF-8");

			// iterate through the array of files, adding each to the zip file
			iter = files.iterator();
			while (iter.hasNext()) {
				String fn = iter.next();
				String zfn = fn;
				if (zfn.startsWith(baseDir)) {
					zfn = zfn.substring(baseDir.length());
				}
				if (!blobDir.equals("") && zfn.startsWith(blobDir)) {
					zfn = zfn.substring(blobDir.length());
				} else if (!streamingDir.equals("") && zfn.startsWith(streamingDir)) {
					zfn = zfn.substring(streamingDir.length());
				}
				if (new File(fn).exists()) {
					// Associate a file input stream for the current file
					try (FileInputStream in = new FileInputStream(fn)) {
						// Add ZIP entry to output stream.
						out.putNextEntry(new ZipEntry(zfn));

						// Transfer bytes from the current file to the ZIP file
						int len;
						while ((len = in.read(buffer)) > 0) {
							out.write(buffer, 0, len);
						}

						// Close the current entry
						out.closeEntry();
					}
				}
			}

		} catch (Exception e) {
			_log.error(e);
			return false;
		}
		return true;
	}

	/**
	 * Unpackage zip file to files within baseDir.
	 *
	 * @param baseDir the base dir
	 * @param zipFile the zip file
	 *
	 * @return true, if successful
	 */
	public boolean unpackageZipToFiles(String baseDir, File zipFile) {
		fileHelper.createDir(baseDir);

		try (ZipInputStream zis = new ZipInputStream(new BufferedInputStream(new FileInputStream(zipFile)))) {
			if (!zipFile.exists())
				return false;

			// org.apache.tools.zip-version is not applicable here:
			java.util.zip.ZipEntry entry = null;
			while ((entry = zis.getNextEntry()) != null) {
				int count;
				byte[] data = new byte[BUFFER_SIZE];
				// write the file to the disk
				File outFile = new File((Paths.get(baseDir, entry.getName()).toString()));
				if (outFile.exists() && outFile.isFile()) {
					if (!outFile.getAbsoluteFile().delete()) {
						CDesktopComponents.vView()
								.showMessagebox(null,
										"Error while unpacking '" + zipFile + "'. Could not delete file '"
												+ outFile.getAbsolutePath() + "'.",
										"Error", Messagebox.OK, Messagebox.ERROR);
						return false;
					}
				}
				// possible create sub dirs
				String subDir = "";
				int lPos = entry.getName().lastIndexOf("/");
				if (lPos > 0) {
					subDir = entry.getName().substring(0, lPos);
				}
				fileHelper.createDir((Paths.get(baseDir, subDir).toString()));
				if (!entry.isDirectory()) {
					try (FileOutputStream fos = new FileOutputStream(
							(Paths.get(baseDir, entry.getName()).toString()))) {
						try (BufferedOutputStream dest = new BufferedOutputStream(fos, BUFFER_SIZE)) {
							while ((count = zis.read(data, 0, BUFFER_SIZE)) != -1) {
								dest.write(data, 0, count);
							}
							dest.flush();
						}
					}
				}
			}

		} catch (IOException e) {
			_log.error(e);
			return false;
		}
		return true;
	}

	/**
	 * Get file names within zip file.
	 *
	 * @param zipFile the zip file
	 *
	 * @return file names
	 */
	public List<String> getFileNamesInZip(File zipFile) {
		List<String> lFileNames = new ArrayList<>();
		if (!zipFile.exists()) {
			return lFileNames;
		}

		try (ZipInputStream zis = new ZipInputStream(new BufferedInputStream(new FileInputStream(zipFile)))) {
			// org.apache.tools.zip-version is not applicable here:
			java.util.zip.ZipEntry entry = null;
			while ((entry = zis.getNextEntry()) != null) {
				if (!entry.isDirectory()) {
					lFileNames.add(entry.getName());
				}
			}
		} catch (IOException e) {
			_log.error(e);
		}
		return lFileNames;
	}

}
