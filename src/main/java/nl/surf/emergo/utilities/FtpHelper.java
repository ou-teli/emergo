/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.utilities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSelectInfo;
import org.apache.commons.vfs2.FileSelector;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemOptions;
import org.apache.commons.vfs2.FileType;
import org.apache.commons.vfs2.VFS;
import org.apache.commons.vfs2.impl.StandardFileSystemManager;
import org.apache.commons.vfs2.provider.sftp.SftpFileObject;
import org.apache.commons.vfs2.provider.sftp.SftpFileSystemConfigBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.lang.Strings;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

public class FtpHelper implements Serializable {
	private static final Logger _log = LogManager.getLogger(FtpHelper.class);

	private static final long serialVersionUID = -4809421984042662110L;

	private String em_Server;
	private String em_ServerRootPath;
	private int em_Port;
	private String em_User;
	private String em_Password;
	private String em_Protocol;
	private boolean em_ServerDefined = false;

	private static List<String> pFtpFilesPresent = new ArrayList<String>(0);
	private static List<String> pFtpFilesAbsent = new ArrayList<String>(0);

	private StandardFileSystemManager pFsManager = null;
	private static FileSystemOptions pFsOptions = new FileSystemOptions();
	private static Channel channel = null;

	public FtpHelper() {
		em_Server = PropsValues.STREAMING_SERVER_FTP_NAME;
		if (!Strings.isEmpty(em_Server)) {
			em_ServerDefined = true;
			em_ServerRootPath = PropsValues.STREAMING_SERVER_FTP_ROOTPATH;
			em_Protocol = PropsValues.STREAMING_SERVER_FTP_PROTOCOL;
			em_Port = PropsValues.STREAMING_SERVER_FTP_PORT;
			em_User = PropsValues.STREAMING_SERVER_FTP_USER;
			em_Password = PropsValues.STREAMING_SERVER_FTP_PASSWORD;
		}
		loadFileManager();
	}

	public FtpHelper(String pServerName, String pServerRoot, int pPortNumber, String pUserName, String pPassword,
			String pProtocol) {
		em_Server = pServerName;
		if (!Strings.isEmpty(em_Server)) {
			em_ServerDefined = true;
			em_ServerRootPath = pServerRoot;
			em_Port = pPortNumber;
			em_User = pUserName;
			em_Password = pPassword;
			em_Protocol = pProtocol;
		}
		loadFileManager();
	}

	public void clearPresentAndAbsentFiles() {
		FtpHelper.pFtpFilesPresent.clear();
		FtpHelper.pFtpFilesAbsent.clear();
	}

	private void loadFileManager() {
		if (pFsManager == null) {
			try {
				pFsManager = (StandardFileSystemManager) VFS.getManager();
				SftpFileSystemConfigBuilder.getInstance().setStrictHostKeyChecking(pFsOptions, "no");
				SftpFileSystemConfigBuilder.getInstance().setUserDirIsRoot(pFsOptions, true);
				SftpFileSystemConfigBuilder.getInstance().setSessionTimeoutMillis(pFsOptions, 10000);
			} catch (FileSystemException e) {
				String lError = e.getMessage();
				Throwable lCause = e.getCause();
				while (lCause != null) {
					lError = lCause.getMessage();
					lCause = lCause.getCause();
				}
				_log.error(lError);
			}
		}
	}

	public boolean ftpServerDefined() {
		return em_ServerDefined;
	}

	private boolean ftpFileInPresentList(String pRemoteDirectory, String pRemoteFileName) {
		return FtpHelper.pFtpFilesPresent.contains(pRemoteDirectory + pRemoteFileName);
	}

	private boolean ftpFileInAbsentList(String pRemoteDirectory, String pRemoteFileName) {
		return FtpHelper.pFtpFilesAbsent.contains(pRemoteDirectory + pRemoteFileName);
	}

	private void putFtpFileInPresentList(String pRemoteDirectory, String pRemoteFileName) {
		FtpHelper.pFtpFilesPresent.add(pRemoteDirectory + pRemoteFileName);
	}

	private void putFtpFileInAbsentList(String pRemoteDirectory, String pRemoteFileName) {
		FtpHelper.pFtpFilesAbsent.add(pRemoteDirectory + pRemoteFileName);
	}

	private static Channel getChannelSftp(String host, String user, String password) {
		try {
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, host, 22);
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			config.put("PreferredAuthentications", "publickey,keyboard-interactive,password");
			session.setConfig(config);
			session.setPassword(password);
			session.connect();
			channel = session.openChannel("sftp");

		} catch (Exception e) {
			_log.error("Failed to get sftp channel {}", e.getMessage());
		}
		return channel;
	}

	public boolean ftpFileExists(String pRemoteDirectory, String pRemoteFileName) {
		if (ftpFileInPresentList(pRemoteDirectory, pRemoteFileName))
			return true;
		if (ftpFileInAbsentList(pRemoteDirectory, pRemoteFileName))
			return false;
		boolean lExists = ftpTransFileExists(pRemoteDirectory, pRemoteFileName);
		if (lExists)
			putFtpFileInPresentList(pRemoteDirectory, pRemoteFileName);
		else
			putFtpFileInAbsentList(pRemoteDirectory, pRemoteFileName);
		return lExists;
	}

	public boolean ftpTransFileExists(String pRemoteDirectory, String pRemoteFileName) {
		// NOTE transcoded files (webcam recordings) may not yet be present if original
		// recording already on server
		// so do not use cache
		boolean lExists = false;
		if (em_Protocol.equalsIgnoreCase("sftp")) {
			if (!ftpServerDefined() || (pFsManager == null))
				return false;
			try {
				String sftpUri = em_Protocol + "://" + em_User + ":" + em_Password + "@" + em_Server + em_ServerRootPath
						+ pRemoteDirectory + pRemoteFileName;
				SftpFileObject fo = (SftpFileObject) pFsManager.resolveFile(sftpUri, pFsOptions);
				lExists = fo.exists();
			} catch (FileSystemException e) {
				// NOTE could not connect to SFTP server
				StringBuilder lError = new StringBuilder().append(e.getMessage());
				Throwable lCause = e.getCause();
				while (lCause != null) {
					lError.append("\n\t").append(lCause.getMessage());
					lCause = lCause.getCause();
				}
				_log.error(lError.toString());
			}

		} else if (em_Protocol.equalsIgnoreCase("ftp")) {
			// TODO implement ftp protocol
			FTPClient lClient = new FTPClient();
		} else {
			// TODO implement other protocol
		}
		return lExists;
	}

	public boolean ftpTransFileCopyBlob(String pLocalFile, String pRemoteDirectory, String pRemoteFileName) {
		// NOTE
		// pLocalFile is file on disk, absolute path is included, for instance
		// 'C:\\test.mp4'
		// pRemoteDirectory is a relative path within video folder on Wowza server, for
		// instance 'webcam/asl/746/blob/294197', where 746 is case id and 294197 is
		// blob id
		// pRemoteFileName is the file name the file should get on the Wowza server, for
		// instance 'test.mp4'.
		boolean lOk = false;
		if (em_Protocol.equalsIgnoreCase("sftp")) {
			if (!ftpServerDefined() || (pFsManager == null))
				return false;

			// NOTE alternative method; TODO test stability and performance
			try {
				Channel channel = getChannelSftp(em_Server, em_User, em_Password);
				channel.connect();
				ChannelSftp channelSftp = (ChannelSftp) channel;
				// NOTE create directory. It may already exist if another file is already
				// uploaded for same blob.
				try {
					channelSftp.mkdir(em_ServerRootPath + pRemoteDirectory);
				} catch (Exception e1) {
				}
				// NOTE remove already existing files in the directory. There may be files
				// already if another file is already uploaded for same blob.
				try {
					channelSftp.rm(em_ServerRootPath + pRemoteDirectory + "*.*");
				} catch (Exception e1) {
				}
				// NOTE move to directory
				channelSftp.cd(em_ServerRootPath + pRemoteDirectory);
				try {
					// NOTE copy file
					channelSftp.put(pLocalFile, pRemoteFileName);
				} catch (Exception e) {
					channelSftp.exit();
				}
				lOk = true;
				channelSftp.exit();
			} catch (Exception e) {
				_log.error(e);
			}

		} else if (em_Protocol.equalsIgnoreCase("ftp")) {
			// TODO implement ftp protocol
			FTPClient lClient = new FTPClient();
		} else {
			// TODO implement other protocol
		}
		return lOk;
	}

	public boolean ftpTransFileDeleteBlob(String pRemoteDirectory, String pRemoteFileName) {
		// NOTE
		// pRemoteDirectory is a relative path within video folder on Wowza server, for
		// instance 'webcam/asl/746/blob/294197', where 746 is case id and 294197 is
		// blob id
		// pRemoteFileName is the file name the file should get on the Wowza server, for
		// instance 'test.mp4'.
		boolean lOk = false;
		if (em_Protocol.equalsIgnoreCase("sftp")) {
			if (!ftpServerDefined() || (pFsManager == null))
				return false;

			// NOTE alternative method; TODO test stability and performance
			try {
				Channel channel = getChannelSftp(em_Server, em_User, em_Password);
				channel.connect();
				ChannelSftp channelSftp = (ChannelSftp) channel;
				// NOTE remove files in directory
				try {
					channelSftp.rm(em_ServerRootPath + pRemoteDirectory + "*.*");
				} catch (Exception e1) {
					_log.error(e1);
				}
				// NOTE remove directory
				try {
					channelSftp.rmdir(em_ServerRootPath + pRemoteDirectory);
				} catch (Exception e1) {
					_log.error(e1);
				}
				lOk = true;
				channelSftp.exit();
			} catch (Exception e) {
				_log.error(e);
			}

		} else if (em_Protocol.equalsIgnoreCase("ftp")) {
			// TODO implement ftp protocol
			FTPClient lClient = new FTPClient();
		} else {
			// TODO implement other protocol
		}
		return lOk;
	}

	public List<String> getFileList(String pRemoteDirectory) {
		List<String> lReturn = new ArrayList<String>();
		if (em_Protocol.equalsIgnoreCase("sftp")) {
			if (ftpServerDefined() && (pFsManager != null)) {
				try {
					String sftpUri = em_Protocol + "://" + em_User + ":" + em_Password + "@" + em_Server
							+ em_ServerRootPath + pRemoteDirectory;
					FileObject lFo = pFsManager.resolveFile(sftpUri, pFsOptions);
					List<FileObject> lFiles = resolveFiles(lFo, "*.*");
					for (FileObject lResultFo : lFiles) {
						String lFName = lResultFo.getName().getBaseName();
						lReturn.add(lFName);
					}
				} catch (FileSystemException e) {
					String lError = e.getMessage();
					Throwable lCause = e.getCause();
					while (lCause != null) {
						lError = lCause.getMessage();
						lCause = lCause.getCause();
					}
					_log.error(lError);
				}
			}
		} else if (em_Protocol.equalsIgnoreCase("ftp")) {
			// TODO implement ftp protocol
			FTPClient lClient = new FTPClient();
		} else {
			// TODO implement other protocol
		}
		return lReturn;
	}

	private FileObject resolveFile(FileObject basef, String file) throws FileSystemException {
		if (basef != null)
			return pFsManager.resolveFile(basef, file, pFsOptions);
		else
			return pFsManager.resolveFile(file, pFsOptions);
	}

	private List<FileObject> resolveFiles(FileObject basef, String value) {
		_log.info("resolve files {}", value);
		try {
			ArrayList<FileObject> result = new ArrayList<FileObject>();

			// if no wild card, just return the file
			if (!(value.contains("?") || value.contains("*"))) {
				result.add(resolveFile(basef, value));
				return result;
			}
			// if we have wild cards
			{
				// create a java pattern from the file pattern
				String pattern = value.replace("\\.", "\\\\.");
				pattern = pattern.replace("\\?", ".");
				if (pattern.contains("/**/"))
					pattern = pattern.replace("/\\*\\*/", "/*/");
				pattern = pattern.replace("\\*", ".*");
				pattern = basef.getName().getPath() + "/" + pattern;
				final Pattern pat = Pattern.compile(pattern);

				// find prefix with no pattern
				int istar = value.indexOf("*");
				if (istar <= 0)
					istar = Integer.MAX_VALUE;
				int iquest = value.indexOf("?");
				if (iquest <= 0)
					iquest = Integer.MAX_VALUE;
				int i = Math.min(istar, iquest);
				String prefix = null;
				if (i < Integer.MAX_VALUE) {
					prefix = value.substring(0, i);
				}

				int depth = 0;
				if (value.contains("**/")) {
					depth = Integer.MAX_VALUE;
				} else
					while ((i = value.indexOf("*/")) != -1) {
						depth++;
						value = value.substring(i + 2);
					}
				final int fdepth = depth;
				FileSelector fs = new FileSelector() {
					@Override
					public boolean includeFile(FileSelectInfo info) throws Exception {
						// files /x/x causes exceptions -> these are imaginary
						// files -> ignore
						if (info.getFile().getType() == FileType.IMAGINARY)
							return false;
						boolean result = pat.matcher(info.getFile().getName().getPath()).matches();
						_log.info(info.getFile().getName().getPath() + " " + result);
						return result;
					}

					@Override
					public boolean traverseDescendents(FileSelectInfo info) throws Exception {
						return info.getDepth() <= fdepth;
					}

				};

				FileObject nbase;
				if (prefix != null)
					nbase = basef.resolveFile(prefix);
				else
					nbase = basef;

				FileObject[] files = nbase.findFiles(fs);
				if (files != null && files.length > 0)
					return Arrays.asList(files);
				else
					return new ArrayList<FileObject>();
			}
		} catch (Exception ex) {
			_log.error(ex);
		}
		return null;
	}
}
