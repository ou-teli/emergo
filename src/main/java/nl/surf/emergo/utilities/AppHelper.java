/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.utilities;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import org.zkoss.zk.ui.Executions;

public class AppHelper {

	public String getAbsoluteAppPath() {
		if (Executions.getCurrent() != null && Executions.getCurrent().getDesktop() != null) {
			return Executions.getCurrent().getDesktop().getWebApp().getServletContext().getRealPath("");
		}
		return "";
	}

	public String getAbsoluteBlobPath() {
		return (getAbsoluteAppPath() + getAbsolutePath(getInitParameter("emergo.blob.path")));
	}

	public String getAbsoluteStreamingPath() {
		return (getAbsoluteAppPath() + getAbsolutePath(getInitParameter("emergo.streaming.path")));
	}

	public String getAbsoluteTempPath() {
		return (getAbsoluteAppPath() + getAbsolutePath(getInitParameter("emergo.temp.path")));
	}

	public String getInitParameter(String aKey) {
		if (Executions.getCurrent() != null && Executions.getCurrent().getDesktop() != null) {
			String lValue = Executions.getCurrent().getDesktop().getWebApp().getServletContext().getInitParameter(aKey);
			if (lValue == null) {
				lValue = "";
			}
			return lValue;
		}
		return "";
	}

	public String getAbsolutePath(String aPath) {
		while (aPath.startsWith("/")) {
			aPath = aPath.substring(1);
		}
		return aPath.replace("/", File.separator);
	}

	public static Class[] getClasses(String packageName, boolean nest) {
		return getClasses(packageName, false, nest);
	}

	public static Class[] getInterfaces(String packageName, boolean nest) {
		return getClasses(packageName, true, nest);
	}

	private static Class[] getClasses(String packageName, boolean giveInterfaces, boolean nest) {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		assert classLoader != null;
		String path = packageName.replace('.', '/');
		Enumeration<URL> resources = null;
		try {
			resources = classLoader.getResources(path);
		} catch (IOException e) {
			return new Class[0];
		}
		List<File> dirs = new ArrayList<>();
		while (resources.hasMoreElements()) {
			URL resource = resources.nextElement();
			dirs.add(new File(resource.getFile().replace("%20", " ")));
		}
		List<Class<?>> classes = new ArrayList<>();
		for (File directory : dirs) {
			classes.addAll(findClasses(directory, packageName, giveInterfaces, nest));
		}
		return classes.toArray(new Class[classes.size()]);

	}

	private static List<Class<?>> findClasses(File directory, String packageName, boolean giveInterfaces,
			boolean nest) {
		List<Class<?>> classes = new ArrayList<>();
		if (!directory.exists()) {
			return classes;
		}
		File[] files = directory.listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				assert !file.getName().contains(".");
				if (nest) {
					classes.addAll(findClasses(file, packageName + "." + file.getName(), giveInterfaces, nest));
				}
			} else if (file.getName().endsWith(".class")) {
				try {
					Class newClass = Class
							.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6));
					if (giveInterfaces == newClass.isInterface()) {
						classes.add(newClass);
					}
				} catch (ClassNotFoundException e) {
					// NOOP
				}
			}
		}
		return classes;
	}

}
