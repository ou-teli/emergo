/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.utilities;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.ByteOrderMark;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.input.BOMInputStream;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zkmax.zul.Filedownload;
import org.zkoss.zul.Div;
import org.zkoss.zul.Fileupload;
import org.zkoss.zul.Messagebox;

import nl.surf.emergo.control.CDesktopComponents;

public class FileHelper {
	private static final Logger _log = LogManager.getLogger(FileHelper.class);
	private static final String ERROR = "Error";
	private AppHelper appHelper = new AppHelper();
	private static final String separator = "_BACKUP_";
	protected boolean filesAreUploaded;
	protected String path;
	protected Component notifyComponent;

	public Component getNotifyComponent() {
		return notifyComponent;
	}

	public void setNotifyComponent(Component notifyComponent) {
		this.notifyComponent = notifyComponent;
	}

	public boolean createDir(String dir) {
		return (new File(dir)).mkdirs();
	}

	public boolean deleteDir(String dir) {
		return deleteDir(new File(dir));
	}

	protected boolean deleteDir(File folder) {
		boolean result = true;
		// check if folder file is a real folder
		if (folder.isDirectory()) {
			File[] list = folder.listFiles();
			if (list != null) {
				for (int i = 0; i < list.length; i++) {
					File tempFile = list[i];
					if (tempFile.isDirectory()) {
						result = result && deleteDir(tempFile);
					}
					tempFile.delete();
				}
			}
			if (!folder.delete()) {
				return false;
			}
		}
		return result;
	}

	public String createFile(String dir, String fileName, byte[] bytes) {
		String result = "";
		if ((bytes != null) && (bytes.length > 0)) {
			createDir(dir);
			String servfile = dir + fileName;
			if (fileExists(servfile))
				deleteFile(servfile);
			_log.debug("Will scan file " + fileName);
			if (AVUtil.isInfected(new ByteArrayInputStream(bytes))) {
				_log.warn(String.format("Virus found in %s, file was not saved", fileName));
				return result;
			}
			_log.debug("Done scanning " + fileName);
			InputStream stream = new ByteArrayInputStream(bytes);
			try (OutputStream bos = new FileOutputStream(servfile);) {
				int bytesRead = 0;
				byte[] buffer = new byte[8192];
				while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
					bos.write(buffer, 0, bytesRead);
				}
				stream.close();
				result = fileName;
			} catch (IOException e) {
				_log.error(String.format("Error creating file %s in %s", fileName, dir), e);
			}
		}
		return result;
	}

	public boolean deleteFile(String fileName) {
		return (new File(fileName)).delete();
	}

	public boolean copyFile(String fromFile, String toFile) {
		boolean result = true;
		try (FileInputStream fis = new FileInputStream(fromFile); FileOutputStream fos = new FileOutputStream(toFile)) {
			int bytesRead = 0;
			byte[] buffer = new byte[8192];
			while ((bytesRead = fis.read(buffer, 0, 8192)) != -1) {
				fos.write(buffer, 0, bytesRead);
			}
			_log.debug(String.format("Copied %s to %s", fromFile, toFile));
		} catch (IOException e) {
			result = false;
			_log.error(String.format("Error copying %s to %s", fromFile, toFile), e);
		}
		return result;
	}

	public static void copyDirectory(String pSourceDir, String pDestDir, boolean pOverwrite, boolean pRecursive) throws IOException {
        try {
            copyFiles(new File(pSourceDir), new File(pDestDir), pOverwrite, pRecursive);
        } catch (IOException e) {
			_log.error(String.format("Error copying files from %s to %s", pSourceDir, pDestDir), e);
        }
		
	}
	protected static void copyFiles(File pSourceDir, File pDestDir, boolean pOverwrite, boolean pRecursive) throws IOException {
		if (pSourceDir == null || !pSourceDir.exists() || !pSourceDir.isDirectory()) {
			_log.error(String.format("Error copying files, source directory not valid: %s", pSourceDir));
			return;
        }
		
		if (!pDestDir.exists())
        	pDestDir.mkdirs();
        
        File[] lFiles = pSourceDir.listFiles();
        if (lFiles == null || lFiles.length == 0)
            return;

        for (File lSourceFile : lFiles) {
            String lName = lSourceFile.getName();
            File lDestFile = new File(pDestDir, lName);
            if (lSourceFile.isDirectory()) {
            	if (pRecursive)
                	copyFiles(lSourceFile, lDestFile, pOverwrite, pRecursive);
            } else {
                Path lSourcePath = Paths.get(lSourceFile.getAbsolutePath());
                Path lDestPath = Paths.get(lDestFile.getAbsolutePath());
                if (pOverwrite || !lDestFile.exists())
                    Files.copy(lSourcePath, lDestPath, StandardCopyOption.REPLACE_EXISTING);
            }
        }
	}

	public File getFile(String fileName) {
		File lFile = new File(fileName);
		if (!lFile.exists()) {
			return null;
		}
		return lFile;
	}

	public byte[] readFile(String fileName) {
		byte[] result = null;
		File lFile = new File(fileName);

		try (FileInputStream fis = new FileInputStream(lFile)) {
			long lLength = lFile.length();
			if (lLength > Integer.MAX_VALUE) {
				// File is too large
				return null;
			}
			//NOTE delete BOM characters if present
			BOMInputStream lBis = new BOMInputStream(fis, ByteOrderMark.UTF_8, ByteOrderMark.UTF_16LE, ByteOrderMark.UTF_16BE, ByteOrderMark.UTF_32LE, ByteOrderMark.UTF_32BE);
	        if (lBis.hasBOM())
	        	lLength -= lBis.getBOM().length();
			result = new byte[(int) lLength];
			int lOffset = 0;
			int lNumRead = 0;
			while (lOffset < result.length && (lNumRead = lBis.read(result, lOffset, result.length - lOffset)) >= 0) {
				lOffset += lNumRead;
			}
			lBis.close();
			// Ensure all the bytes have been read in
			if (lOffset < result.length) {
				throw new IOException("Could not completely read file " + lFile.getName());
			}
		} catch (IOException e) {
			result = null;
			_log.error(String.format("Error reading %s", fileName), e);
		}
		return result;
	}

	public List<String> readTextFile(String fileName) {
		List<String> result = new ArrayList<>();
		try (FileInputStream fstream = new FileInputStream(fileName);
				//NOTE delete BOM characters if present
				BOMInputStream lBis = new BOMInputStream(fstream, ByteOrderMark.UTF_8, ByteOrderMark.UTF_16LE, ByteOrderMark.UTF_16BE, ByteOrderMark.UTF_32LE, ByteOrderMark.UTF_32BE);
				DataInputStream in = new DataInputStream(lBis)) {
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			while ((strLine = br.readLine()) != null) {
				result.add(strLine);
			}
		} catch (Exception e) {
			_log.error(String.format("Error reading textfile %s", fileName), e);
		}
		return result;
	}

	public List<String> getFileNames(String dir) {
		List<String> lNames = new ArrayList<>(0);
		File[] lFiles = (new File(dir)).listFiles();
		if ((lFiles == null) || (lFiles.length == 0))
			return lNames;
		for (int i = 0; i < lFiles.length; i++) {
			File lFile = lFiles[i];
			if (!lFile.isDirectory())
				lNames.add(lFile.getName());
		}
		return lNames;
	}

	public List<String> getFileNamesInPathSubTree(String dir) {
		List<String> lNames = new ArrayList<>(0);
		File[] lFiles = (new File(dir)).listFiles();
		if ((lFiles == null) || (lFiles.length == 0))
			return lNames;
		for (int i = 0; i < lFiles.length; i++) {
			File lFile = lFiles[i];
			if (!lFile.isDirectory())
				lNames.add(lFile.getName());
			else
				lNames.addAll(getFileNamesInPathSubTree(lFile.getPath()));
		}
		return lNames;
	}

	public List<String> getFileNames(String aPath, String aSubPath) {
		return getFileNames(aPath, aSubPath, false);
	}

	public List<String> getFileNamesToRestore(String aPath, String aSubPath) {
		return getFileNames(aPath, aSubPath, true);
	}

	private List<String> getFileNames(String aPath, String aSubPath, boolean aOnlyRestoredNames) {
		String lPath = aPath;
		List<String> result = new ArrayList<>();

		if (lPath == null) {
			lPath = "";
		}

		if (!StringUtils.isEmpty(aSubPath)) {
			lPath += aSubPath + File.separator;
		}

		List<File> lFiles = getOnlyFilesInDir(lPath);

		for (File file : lFiles) {
			if (aOnlyRestoredNames) {
				int lIndex = file.getName().indexOf(separator);
				if (lIndex > 0) {
					String lFileName = file.getName().substring(0, lIndex);
					if (!result.contains(lFileName)) {
						result.add(lFileName);
					}
				}
			} else {
				result.add(file.getName());
			}
		}
		return result;
	}

	public List<String> getFileNamesExcludingSubPaths(String aPath, boolean aIncludeSubPaths,
			List<String> aSubPathsToExclude) {
		List<String> result = new ArrayList<>();

		File[] lFiles = getFilesInDir(aPath);

		if (lFiles == null) {
			return result;
		}

		for (File lFile : lFiles) {
			if (!lFile.isDirectory()) {
				if (isIncluded(aSubPathsToExclude, lFile)) {
					result.add(lFile.getPath());
				}
			} else if (aIncludeSubPaths) {
				result.addAll(getFileNamesExcludingSubPaths(lFile.getPath(), aIncludeSubPaths, aSubPathsToExclude));
			}
		}
		return result;
	}

	private boolean isIncluded(List<String> aSubPathsToExclude, File lFile) {
		if (aSubPathsToExclude == null) {
			return true;
		}

		for (String lExcludeSubPath : aSubPathsToExclude) {
			if (lFile.getPath().startsWith(lExcludeSubPath)) {
				return false;
			}
		}
		return true;
	}

	public List<String> getSubFolders(String aPath, String aSubPath) {
		String lPath = aPath;
		if (lPath == null) {
			lPath = "";
		}
		if (!StringUtils.isEmpty(aSubPath)) {
			lPath += aSubPath + "/";
		}
		File[] lFiles = getFilesInDir(lPath);
		List<String> lFileNames = new ArrayList<>();
		if (lFiles != null) {
			for (int i = 0; i < lFiles.length; i++) {
				if (lFiles[i].isDirectory()) {
					lFileNames.add(lFiles[i].getName());
				}
			}
		}
		return lFileNames;
	}

	public String combinePath(String aPath, String aSubPath) {
		Path lP1 = Paths.get(aPath);
		Path lP2 = lP1.resolve(aSubPath);
		try {
			lP2 = lP2.toRealPath();
		} catch (IOException e) {
			_log.error(String.format("Error combining paths %s and %s", aPath, aSubPath), e);
		}
		return lP2.toString();
	}

	public boolean fileExists(String fileName) {
		return (new File(fileName)).exists();
	}

	public boolean renameFile(String oldFileName, String newFileName) {
		File oldFile = new File(oldFileName);
		File newFile = new File(newFileName);
		return oldFile.renameTo(newFile);
	}

	public File[] getFilesInDir(final String dir) {
		return new File(dir).listFiles();
	}

	private List<File> getOnlyFilesInDir(final String dir) {
		List<File> result = new ArrayList<>();
		File[] filesInDir = getFilesInDir(dir);
		for (File file : filesInDir) {
			if (file.isFile()) {
				result.add(file);
			}
		}
		return result;
	}

	/**
	 * Media to byte array. Converts the stream within aMedia to a byte array.
	 *
	 * @param aMedia the a media
	 *
	 * @return the byte[]
	 */
	public byte[] mediaToByteArray(Media aMedia) {
		try {
			InputStream lStream = null;
			Reader lTest = null;
			if (aMedia.isBinary()) {
				lStream = aMedia.getStreamData();
				int lLength = lStream.available();
				byte[] lBytes = new byte[lLength];
				if (lStream.read(lBytes, 0, lLength) == lLength)
					return lBytes;
			} else {
				lTest = aMedia.getReaderData();
				int lMax = 0;
				int lCnt = 60000;
				char[] lChar = new char[lCnt];
				while (lCnt > -1) {
					lCnt = lTest.read(lChar, 0, lCnt);
					if (lCnt > -1) {
						lMax = lMax + lCnt;
					}
				}
				if (lMax > 0) {
					char[] lChar2 = new char[lMax];
					lTest = aMedia.getReaderData();
					lTest.read(lChar2);
					byte[] lBytes = new byte[lMax];
					for (int lInd = 0; lInd < lMax; lInd++) {
						lBytes[lInd] = (byte) lChar2[lInd];
					}
					return lBytes;
				}
			}
		} catch (IOException e) {
			_log.error(String.format("Error converting media %s", aMedia), e);
		}
		return null;
	}

	public boolean downloadFile(String aSubPath, String aFileName) {
		boolean fileIsDownloaded = false;
		if (aFileName.equals("")) {
			return fileIsDownloaded;
		}
		String lPath = aSubPath;
		if (lPath == null) {
			lPath = "";
		}
		if (lPath.startsWith("/") || lPath.startsWith("\\")) {
			lPath = lPath.substring(1, lPath.length());
		}
		if (!(lPath.endsWith("/") || lPath.endsWith("\\"))) {
			lPath += "/";
		}

		if (fileExists(lPath + aFileName)) {
			String lAbsPath = appHelper.getAbsoluteAppPath();
			String lToPath = appHelper.getAbsoluteTempPath();

			copyFile(lPath + aFileName, lToPath + aFileName);
			String lSubPath = lToPath.substring(lAbsPath.length());
			try {
				Filedownload.save(lSubPath + aFileName, null);
				fileIsDownloaded = true;
				// cannot delete because code runs independently from file dialog
				// fileHelper.deleteFile(lToPath + aFileName);
			} catch (FileNotFoundException e) {
				_log.error(String.format("Error downloading %s from %s", aFileName, aSubPath), e);
				CDesktopComponents.vView().showMessagebox(null,
						"Error while downloading '" + lPath + aFileName
								+ "'. Note that 'Path' is not used when downloading.",
						ERROR, Messagebox.OK, Messagebox.ERROR);
			}
		} else {
			CDesktopComponents.vView().showMessagebox(null,
					"File not found: '" + lPath + aFileName + "'. Note that 'Path' is not used when downloading.",
					ERROR, Messagebox.OK, Messagebox.ERROR);
		}
		return fileIsDownloaded;
	}

	public boolean uploadFile(String aPath, String aSubPath, boolean aCreateBackup, boolean aShowMessages) {
		return uploadMedias(Fileupload.get(Integer.MAX_VALUE), aPath, aSubPath, aCreateBackup, aShowMessages);
	}

	public boolean uploadMedias(Media[] aMedias, String aPath, String aSubPath, boolean aCreateBackup,
			boolean aShowMessages) {
		filesAreUploaded = false;
		if (aMedias != null) {
			path = aPath;
			if (path == null) {
				path = "";
			}
			if (!StringUtils.isEmpty(aSubPath)) {
				path += aSubPath + "/";
			}
			for (int i = 0; i < aMedias.length; i++) {
				Object media = aMedias[i];
				if (media instanceof Media) {
					Media lMedia = (Media) media;
					String lFileName = lMedia.getName();
					byte[] lBytes = mediaToByteArray(lMedia);
					if (fileExists(path + lFileName)) {
						if (aShowMessages) {
							filesAreUploaded = false;
							Div lTransparentDiv = CDesktopComponents.vView().initQuasiModalBehavior(null);
							Messagebox.show("File already exists. Are you sure you want to overwrite it?",
									"Confirm Dialog", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
									new EventListener<Event>() {
										@Override
										public void onEvent(Event evt) {
											switch (((Integer) evt.getData()).intValue()) {
											case Messagebox.OK:
												filesAreUploaded = overwriteFile(path, lFileName, lBytes, aCreateBackup,
														aShowMessages);
												CDesktopComponents.vView().undoQuasiModalBehavior(lTransparentDiv);
												if (getNotifyComponent() != null) {
													Events.postEvent("onSucces", getNotifyComponent(), aMedias);
												}
												break;
											case Messagebox.CANCEL:
												CDesktopComponents.vView().undoQuasiModalBehavior(lTransparentDiv);
												break;
											}
										}
									});
						} else {
							filesAreUploaded = overwriteFile(path, lFileName, lBytes, aCreateBackup, aShowMessages);
						}
					} else {
						String pathPlusFileName = createFile(path, lFileName, lBytes);
						if (pathPlusFileName.equals("")) {
							if (aShowMessages) {
								CDesktopComponents.vView().showMessagebox(null,
										"Error while creating file'" + path + lFileName + "'.", ERROR, Messagebox.OK,
										Messagebox.ERROR);
							}
						} else {
							filesAreUploaded = true;
							if (aShowMessages) {
								CDesktopComponents.vView().showMessagebox(null, "File is created.", "Information",
										Messagebox.OK, Messagebox.INFORMATION);
							}
						}
					}
				}
			}
		}
		return filesAreUploaded;
	}

	public boolean overwriteFile(String lPath, String lFileName, byte[] lBytes, boolean aCreateBackup,
			boolean aShowMessages) {
		boolean filesAreUploaded = false;
		String lRenamedFileName = "";
		boolean lRenamedOk = true;
		if (aCreateBackup) {
			int lCounter = 1;
			while (fileExists(lPath + lFileName + separator + lCounter)) {
				lCounter++;
			}
			lRenamedFileName = lFileName + separator + lCounter;
			lRenamedOk = renameFile(lPath + lFileName, lPath + lRenamedFileName);
			if (!lRenamedOk && aShowMessages) {
				CDesktopComponents.vView().showMessagebox(null, "Error while renaming file'" + lPath + lFileName + "'.",
						ERROR, Messagebox.OK, Messagebox.ERROR);
			}
		}
		if (!aCreateBackup || lRenamedOk) {
			String pathPlusFileName = createFile(lPath, lFileName, lBytes);
			if (pathPlusFileName.equals("")) {
				if (aShowMessages) {
					CDesktopComponents.vView().showMessagebox(null,
							"Error while creating file'" + lPath + lFileName + "'.", ERROR, Messagebox.OK,
							Messagebox.ERROR);
				}
			} else {
				filesAreUploaded = true;
				if (aShowMessages) {
					if (aCreateBackup) {
						CDesktopComponents.vView().showMessagebox(
								null, "File is overwritten. A copy of the old file is saved as '" + lPath
										+ lRenamedFileName + "'.",
								"Information", Messagebox.OK, Messagebox.INFORMATION);
					} else {
						CDesktopComponents.vView().showMessagebox(null, "File is overwritten.", "Information",
								Messagebox.OK, Messagebox.INFORMATION);
					}
				}
			}
		}
		return filesAreUploaded;
	}

	public boolean restoreFile(String aPath, String aSubPath, String aFileName) {
		boolean fileIsRestored = false;
		if (aFileName.equals("")) {
			return fileIsRestored;
		}
		String lPath = aPath;
		if (lPath == null) {
			lPath = "";
		}
		if (!StringUtils.isEmpty(aSubPath)) {
			lPath += aSubPath + "/";
		}
		int lCounter = 0;
		while (fileExists(lPath + aFileName + separator + (lCounter + 1))) {
			lCounter++;
		}
		if (lCounter > 0) {
			if (deleteFile(lPath + aFileName)) {
				if (renameFile(lPath + aFileName + separator + lCounter, lPath + aFileName)) {
					fileIsRestored = true;
				} else {
					CDesktopComponents.vView().showMessagebox(null,
							"Error while renaming file'" + lPath + aFileName + separator + lCounter + "'.", ERROR,
							Messagebox.OK, Messagebox.ERROR);
				}
			} else {
				CDesktopComponents.vView().showMessagebox(null, "Error while deleting file'" + lPath + aFileName + "'.",
						ERROR, Messagebox.OK, Messagebox.ERROR);
			}
		}
		return fileIsRestored;
	}

	public static boolean isMediaValid(final Media media, List<String> errors) {
		if (media == null) {
			errors.add(CDesktopComponents.vView().getLabel("error.file.empty"));
			return false;
		}

		if (!(media instanceof Media)) {
			errors.add(CDesktopComponents.vView().getLabel("error.file.no_valid_format"));
			return false;
		}

		if (StringUtils.isBlank(media.getName())) {
			errors.add(CDesktopComponents.vView().getLabel("error.file.name_not_valid"));
			return false;
		}

		if (media.getName().length() > 256) {
			errors.add(CDesktopComponents.vView().getLabel("error.file.name_too_long"));
			return false;
		}

		if (!isFileExtensionAllowed(media.getName())) {
			errors.add(CDesktopComponents.vView().getLabel("error.file.extension_not_allowed"));
			return false;
		}

		if (media.getName().startsWith(".")) {
			errors.add(CDesktopComponents.vView().getLabel("error.file.name_not_valid"));
			return false;
		}

		if (!isFileNameValid(media.getName())) {
			errors.add(CDesktopComponents.vView().getLabel("error.file.name_not_valid"));
			return false;
		}

		int maxSize = PropsValues.MAX_UPLOAD_IN_BYTES;
		if (media.getByteData().length > maxSize) {
			Object[] arguments = {maxSize};
			errors.add(MessageFormat.format(CDesktopComponents.vView().getLabel("error.file.too_big"), arguments));
			return false;
		}

		if (AVUtil.isInfected(media.getStreamData())) {
			errors.add(CDesktopComponents.vView().getLabel("error.file.virus_found"));
			return false;
		}

		return true;
	}

	public static boolean isFileNameValid(String filename) {
		int[] illegalChars = { 34, 60, 62, 124, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
				20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 58, 42, 63, 92, 47 };
		Arrays.sort(illegalChars);
		for (int i = 0; i < filename.length(); i++) {
			int c = filename.charAt(i);
			if (Arrays.binarySearch(illegalChars, c) >= 0) {
				return false;
			}
		}
		return true;
	}

	public static void showUploadErrorMessagebox(final Media media, final List<String> errors) {
		StringBuilder sb = new StringBuilder();
		sb.append(CDesktopComponents.vView().getLabel("error.file.upload_not_valid")).append(" ");
		if (media != null) {
			sb.append(media);
		}
		if (!errors.isEmpty()) {
			sb.append(" ");
			sb.append(errors.get(0));
		}
		_log.warn(sb);
		Messagebox.show(sb.toString(), ERROR, Messagebox.OK, Messagebox.ERROR);
	}

	public static boolean isFileExtensionAllowed(final String name) {
		return PropsValues.ALLOWED_FILE_EXTENSIONS.contains(FilenameUtils.getExtension(name).toLowerCase());
	}

}
