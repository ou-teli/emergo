package nl.surf.emergo.security.servlet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

public final class HttpServletRequestUtils {
	private static final String[] HEADERS_TO_TRY = { 
	    "X-Forwarded-For",
	    "Proxy-Client-IP",
	    "WL-Proxy-Client-IP",
	    "HTTP_X_FORWARDED_FOR",
	    "HTTP_X_FORWARDED",
	    "HTTP_X_CLUSTER_CLIENT_IP",
	    "HTTP_CLIENT_IP",
	    "HTTP_FORWARDED_FOR",
	    "HTTP_FORWARDED",
	    "HTTP_VIA",
	    "REMOTE_ADDR" };

	public static String getAllIpAddresses(HttpServletRequest request) {
		List<String> iplist = new ArrayList<String>();
		
		iplist.add("RemoteAddr = " + request.getRemoteAddr());
		
	    for (String header : HEADERS_TO_TRY) {
	        String ip = request.getHeader(header);
	        if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
	        	iplist.add(header + " = " + ip);
	        }
	    }
	    
	    String ipstring = iplist.toString();

	    return ipstring.substring(1, ipstring.length() - 1).replace(", ", ",");
	}

}
