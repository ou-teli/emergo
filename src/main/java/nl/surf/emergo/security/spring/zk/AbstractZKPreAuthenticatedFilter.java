package nl.surf.emergo.security.spring.zk;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WebApp;
import org.zkoss.zk.ui.http.WebManager;
import org.zkoss.zk.ui.sys.SessionsCtrl;

//based on code at: http://forum.zkoss.org/question/61805/how-to-find-the-desktop-id-for-current-request-from-a-servlet-filter/
//Note that you cannot modify the desktop safely using sessionCtrl.getDesktopCache().getDesktop(desktopId);
public abstract class AbstractZKPreAuthenticatedFilter extends
		AbstractPreAuthenticatedProcessingFilter {
	private static final Logger LOG = LogManager.getLogger(AbstractZKPreAuthenticatedFilter.class);
	
	protected WebApp webApp = null;
	
	//usually the password
	@Override
	final protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {
		Session zksession = getZKSession(request);
		
		if (zksession != null)
			return getPreAuthenticatedCredentials(request,zksession);
		else {
			//does not matter what i return here as principal will also be null
			return "ZK_NOT_INITIALIZED_YET";
		}
	}

	//identity, usually username
	@Override
	final protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {
		Session zksession = getZKSession(request);
		
		if (zksession != null)
			return getPreAuthenticatedPrincipal(request,zksession);
		else
			return null;
	}
	
	//TODO: could get desktop from webmanager getdesktopcache and dtid from the request (desktop is created after the filter, so this would be more of a hack)
	protected Session getZKSession(HttpServletRequest request) {
		initializeFromZK();
		Session zksession = null;
		
		if (webApp != null) {
			zksession = SessionsCtrl.getSession(webApp,request.getSession());
		
			if (zksession == null)
				LOG.info("Could not find zk session");
		}
		else
			LOG.debug("Could not find zk web app to get session from");
		
		return zksession;
	}


	synchronized private void initializeFromZK() {
		if (webApp == null) {
			//http://www.zkoss.org/javadoc/7.0.1/zk/org/zkoss/zk/ui/http/WebManager.html
			webApp = WebManager.getWebAppIfAny(getServletContext());
			//webManager = WebManager.getWebManagerIfAny(getServletContext());
			
			if (webApp == null)
				LOG.info("ZK not inialized (yet), could not find zk webApp in servletcontext");
			else
				LOG.info("ZK webApp initialized from servletcontext");
		}
	}
	
	/***
	 * get credentials, usually the password
	 * 
	 * Only called when zkdesktop can be found
	 * 
	 * @param request
	 * @param zkdesktop
	 * @return
	 */
	abstract protected Object getPreAuthenticatedCredentials(HttpServletRequest request,Session zksession);

	/***
	 * get identity, usually the username
	 *  
	 * Only called when zkdesktop can be found
	 * 
	 * @param request
	 * @param zkdesktop
	 * @return
	 */
	abstract protected Object getPreAuthenticatedPrincipal(HttpServletRequest request,Session zksession);

}
