package nl.surf.emergo.security.spring;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import nl.surf.emergo.security.servlet.HttpServletRequestUtils;

public class Http404AccessDeniedHandler implements AccessDeniedHandler {
	private static final Logger LOG = LogManager.getLogger(Http404AccessDeniedHandler.class);
	
	@Override
	public void handle(HttpServletRequest request, HttpServletResponse response,
			AccessDeniedException accessDeniedException) throws IOException, ServletException {
		
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();  
		}
		
		LOG.info("Access denied for authenticated user, using http 404 (hiding) for uri: " + request.getRequestURI() + " ip: " + HttpServletRequestUtils.getAllIpAddresses(request));
		response.sendError(HttpServletResponse.SC_NOT_FOUND,request.getRequestURI());
	}
}
