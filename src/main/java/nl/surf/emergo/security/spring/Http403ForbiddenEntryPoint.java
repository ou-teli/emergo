package nl.surf.emergo.security.spring;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.AuthenticationException;

import nl.surf.emergo.security.servlet.HttpServletRequestUtils;


public class Http403ForbiddenEntryPoint extends org.springframework.security.web.authentication.Http403ForbiddenEntryPoint {
	private static final Logger LOG = LogManager.getLogger(Http403ForbiddenEntryPoint.class);
	
	@Override
	public void commence(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException arg2)
					throws IOException {
		LOG.info("Access denied for unauthenticated user using http 403 for uri: " + request.getRequestURI() + " ip: " + HttpServletRequestUtils.getAllIpAddresses(request));
		super.commence(request, response, arg2);
	}

}
