package nl.surf.emergo.security.spring.zk;

import org.zkoss.zk.ui.Session;

import nl.surf.emergo.control.CControl;

public class CustomSessionCControl extends CControl {

	public CustomSessionCControl(Session customsession) {
		super();
		session = customsession;
	}

	public void setSession(Session customsession) {
		session = customsession;
	}
}
