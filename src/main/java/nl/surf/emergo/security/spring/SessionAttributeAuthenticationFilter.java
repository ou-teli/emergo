package nl.surf.emergo.security.spring;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

public class SessionAttributeAuthenticationFilter extends
		AbstractPreAuthenticatedProcessingFilter {
	
	private static final Logger LOG = LogManager.getLogger(SessionAttributeAuthenticationFilter.class);

	
	/* See:http://grepcode.com/file/repo1.maven.org/maven2/org.springframework.security/spring-security-web/3.0.1.RELEASE/org/springframework/security/web/authentication/preauth/RequestHeaderAuthenticationFilter.java
	 * Credentials aren't usually applicable, but if a credentialsRequestHeader is set, this will be read and used as the credentials value. Otherwise a dummy value will be used.
	 * (non-Javadoc)
	 * @see org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter#getPreAuthenticatedPrincipal(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {
		LOG.info("GET CREDENTIALS");
		return "N/A";
	}
	

	@Override
	protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {
		setContinueFilterChainOnUnsuccessfulAuthentication(Boolean.FALSE);
		
		//TODO: make session attribute name customizable using getter/setter setPrincipalRequestHeader
		
		//Does not work in filter: CControl ccontrol = new CControl();
		//TODO: here and in autologin, use the emergo constants class instead of this!

		Integer principal = (Integer)request.getSession().getAttribute("sess_accId");
		if (principal != null)
			LOG.info("getPreAuthenticatedPrincipal userid found, returning principal: " + principal + " path " + request.getRequestURI());
		else
			LOG.info("getPreAuthenticatedPrincipal : no idm user id found, returning null, path " + request.getRequestURI());
		return principal;
	}

}
