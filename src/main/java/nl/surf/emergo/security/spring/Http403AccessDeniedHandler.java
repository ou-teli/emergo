package nl.surf.emergo.security.spring;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;

import nl.surf.emergo.security.servlet.HttpServletRequestUtils;

public class Http403AccessDeniedHandler extends AccessDeniedHandlerImpl {
	private static final Logger LOG = LogManager.getLogger(Http403AccessDeniedHandler.class);

	//overridden just for logging the ip address (and AccessDeniedHandlerImpl doesnt even log in debug mode)
	@Override
	public void handle(HttpServletRequest request, HttpServletResponse arg1,
			AccessDeniedException arg2) throws IOException, ServletException {
		LOG.info("Access denied for authenticated user, using http 403 for uri: " + request.getRequestURI() + " ip: " + HttpServletRequestUtils.getAllIpAddresses(request));
		super.handle(request, arg1, arg2);
	}

}
