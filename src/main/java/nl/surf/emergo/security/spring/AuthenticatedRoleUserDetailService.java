package nl.surf.emergo.security.spring;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * @author Klaas-Jan
 *
 */
public class AuthenticatedRoleUserDetailService implements UserDetailsService {
	private static final Logger LOG = LogManager.getLogger(AuthenticatedRoleUserDetailService.class);
	
	protected String authenticatedUserRole = "ROLE_USER";
	
	public AuthenticatedRoleUserDetailService(String authenticatedUserRole) {
		super();
		this.authenticatedUserRole = authenticatedUserRole;
	}
	
	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		
		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority(authenticatedUserRole));
		
		UserDetails user = new User(username, "passwordnotprovided", true, true, true, true, authorities);
		
		LOG.debug("Created new user details for user " + username);
        return user;
	}
}
