package nl.surf.emergo.security.spring;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import nl.surf.emergo.security.servlet.HttpServletRequestUtils;
public class Http404ForbiddenEntryPoint implements AuthenticationEntryPoint {
	//private static final Log logger = LogFactory.getLog(Http403ForbiddenEntryPoint.class);
	private static final Logger LOG = LogManager.getLogger(Http404ForbiddenEntryPoint.class);
	/**
	 * Always returns a 403 error code to the client.
	 */
	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException arg2) throws IOException, ServletException {
		//if (logger.isDebugEnabled()) {
		//	logger.debug("Pre-authenticated entry point called. Rejecting access");
		//}
		   
		LOG.info("Access denied for unauthenticated user using http 404 (hiding) for uri: " + request.getRequestURI() + " ip: " + HttpServletRequestUtils.getAllIpAddresses(request));
		response.sendError(HttpServletResponse.SC_NOT_FOUND,request.getRequestURI());
	}

}
