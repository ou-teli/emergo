package nl.surf.emergo.security.spring;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;


public class UnusedEntryPoint extends org.springframework.security.web.authentication.Http403ForbiddenEntryPoint {
	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException arg2) throws IOException {
		throw new RuntimeException("Spring security was incorrectly configured, this entry point should never be reached (always unauthenticated access)");
	}
}
