package nl.surf.emergo.security.spring;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class EmergoUserDetailsService implements UserDetailsService {
	private static final Logger LOG = LogManager.getLogger(EmergoUserDetailsService.class);
	
	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
			
		LOG.info("Load user by user name " + username);
		//TODO: filter will get username (is userid) from session, this can just give always so default role.. or it can load the roles from the database
		UserDetails user = new User(username, "password", true, true, true, true, authorities);
        return user;
	}

}
	