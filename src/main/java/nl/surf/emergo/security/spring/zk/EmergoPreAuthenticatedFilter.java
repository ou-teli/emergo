package nl.surf.emergo.security.spring.zk;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.Session;

import nl.surf.emergo.control.CControl;

public class EmergoPreAuthenticatedFilter extends AbstractZKPreAuthenticatedFilter {
	
	private static final Logger LOG = LogManager.getLogger(EmergoPreAuthenticatedFilter.class);
	
	@Override
	protected Object getPreAuthenticatedCredentials(HttpServletRequest request,
			Session zksession) {
		//see http://l-lin.github.io/2014/09/09/Auth_with_certificates_Tomcat_spring/
		//http://docs.spring.io/autorepo/docs/spring-security/3.2.3.RELEASE/apidocs/org/springframework/security/web/authentication/preauth/AbstractPreAuthenticatedProcessingFilter.html#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
		//Should not return null for a valid principal, though some implementations may return a dummy value.
		return "N/A";
	}

	/**
	 * ZK Session will be there when this function is called
	 */
	@Override
	protected Object getPreAuthenticatedPrincipal(HttpServletRequest request, Session zksession) {
		//CControl is used to get and set session attributes.
		CControl cControl = new CustomSessionCControl(zksession);
		//get account id
		int accId = getAccId(cControl);
		
		if (accId > 0) {
			LOG.debug("accid found, returning principal: " + accId + " path " + request.getRequestURI());
			return new String("" + accId);
		}
		else {
			LOG.debug("no accid found, returning null, path " + request.getRequestURI());
			return null;
		}
	}
	
	protected int getAccId(CControl cControl) {
		Integer lAccId = (Integer)cControl.getAccSessAttr("accId");
		if (lAccId == null) {
			return 0;
		}
		else {
			return lAccId.intValue();
		}
	}

}
