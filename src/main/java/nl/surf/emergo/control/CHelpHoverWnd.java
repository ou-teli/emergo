/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.OpenEvent;
import org.zkoss.zk.ui.util.Clients;

import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.def.CDefHtml;
import nl.surf.emergo.control.def.CDefPopup;
import nl.surf.emergo.view.VView;

public class CHelpHoverWnd extends CDefPopup {

	private static final long serialVersionUID = 6994408078180103140L;

	private CDefHtml htmlChild = null;
	private CDefDiv divChild = null;

	public CHelpHoverWnd() {
		setSclass("CHelpWnd");
		htmlChild = new CDefHtml();
		appendChild(htmlChild);
		htmlChild.setSclass("CHelpWndHtml");
		divChild = new CDefDiv();
		appendChild(divChild);
		divChild.setSclass("CHelpWndDiv");
	}

	public void onOpen(OpenEvent aEvent) {
		clear();
		if (aEvent.getReference() != null) {
			Events.sendEvent("onShowHelp", this, aEvent.getReference().getAttribute("helpObject"));
		}
	}
	
	public void onShowHelp(Event aEvent) {
		if (aEvent.getData() instanceof String) {
			//replace line breaks by html breaks, because text is shown in an html zul element
			htmlChild.setContent((String)aEvent.getData());
		}
		else if (aEvent.getData() instanceof Component) {
			close();
			Events.sendEvent("onShowHelp", CDesktopComponents.vView().getComponent(VView.helpWndId), aEvent.getData());
		}
		Clients.evalJavaScript("setHelpWndHeight('" + getUuid() + "');");
	}

	public void clear() {
		htmlChild.setContent("");
		divChild.getChildren().clear();
	}
	
}
