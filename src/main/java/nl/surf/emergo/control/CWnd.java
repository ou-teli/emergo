/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zk.ui.Component;

import nl.surf.emergo.control.def.CDefWindow;

/**
 * The Class CInputWnd.
 */
public class CWnd extends CDefWindow {

	private static final long serialVersionUID = 3459308545556475798L;

	//NOTE most input dialogues set attribute 'item' of the input window to the item in question if the 'ok' button is clicked.
	//However, in case of deleting an item the attribute 'ok' is set to true if the 'ok' button is clicked, see class CDeleteItemWnd.
	//In case the 'cancel' button is clicked either attribute 'item' or 'ok' is set to null.
	protected String attributeKey = "item";

	protected String cancelAction = "cancel";
	
	public void detach() {
		String notifyAction = null;
		Object notifyStatus = null;
		if (getAttribute(attributeKey) != null) {
			//NOTE if attributeKey is unequal to null the input is ok
			notifyAction = attributeKey;
			notifyStatus = getAttribute(attributeKey);
		}
		else if (hasAttribute(attributeKey)) {
			//NOTE if attributeKey is set to null the input is canceled
			notifyAction = cancelAction;
		}
		if (notifyAction != null) {
			Component notifyComponent = (Component)getAttribute("notifyComponent");
			String id = notifyComponent.getId();
			if (StringUtils.isEmpty(id)) {
				id = notifyComponent.getUuid();
			}
			if (notifyComponent != null && !StringUtils.isEmpty(id)) {
				registerObserver(id);
				notifyObservers(notifyAction, notifyStatus);
			}
		}
		//NOTE a quasi modal window uses a semi-transparent div to cover the underlying window. This div should be removed.
		Component componentToDetach = (Component)getAttribute("componentToDetach");
		if (componentToDetach != null) {
			CDesktopComponents.vView().undoQuasiModalBehavior(componentToDetach);
		}
		super.detach();
	}

}
