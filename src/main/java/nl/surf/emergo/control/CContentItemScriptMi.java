/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import org.zkoss.zk.ui.Component;

/**
 * The Class CContentItemScriptMi. Not yet used.
 */
public class CContentItemScriptMi extends CContentItemMi {

	private static final long serialVersionUID = -5654347108232441773L;

	/**
	 * Instantiates a contentitem script menu item.
	 * 
	 * @param aContentComponent the content xml tree
	 * @param aContentHelper the content helper
	 * @param aIsAuthor the is author state
	 */
	public CContentItemScriptMi(Component aContentComponent, CContentHelper aContentHelper, boolean aIsAuthor) {
		super(aContentComponent, aContentHelper, aIsAuthor);
	}

	/**
	 * On click show the script window popup.
	 */
	public void onClick() {
		contentRoot = getComponent();
		contentItem = (Component)contentRoot.getAttribute("target");
		item = contentItem.getAttribute("item");
		String lValue = (String)contentItem.getAttribute("keyvalues");
		params.put("item", item);
		params.put("value", lValue);
		params.put("accisscriptauthor", getIsAuthor());
		params.put("contenthelper", getContentHelper());
		showPopup(getContentHelper().getScriptView());
	}
	
}
