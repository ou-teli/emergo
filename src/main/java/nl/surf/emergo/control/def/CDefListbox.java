/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.def;

import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.control.CDesktopComponents;

/**
 * The Class CDefListbox.
 *
 * Is extension of ZK framework class.
 * All corresponding application classes extend from this one.
 */
public class CDefListbox extends Listbox {

	private static final long serialVersionUID = 7671390022011022900L;

	/** The class name. */
	public final String className = this.getClass().getSimpleName();

	public CDefListbox() {
		super();
	  	Events.postEvent("onDecorate", this, null);
	}

	/**
	 * On decorate, decorate component.
	 */
	public void onDecorate(Event aEvent) {
		CDesktopComponents.vView().decorateComponent(this);
	}

	/**
	 * Inserts listitem with label.
	 *
	 * @param aItem the object item which has to be connected to the listitem.
	 * @param aLabel the label string
	 * @param aInsertBefore the listitem to insert before
	 *
	 * @return the new listitem
	 */
	public Listitem insertListitem(Object aItem, String aLabel, Listitem aInsertBefore) {
		Listitem lListitem = new Listitem();
		// set value of listitem to item
		lListitem.setValue(aItem);
		Listcell lListcell = new Listcell(aLabel);
		lListitem.appendChild(lListcell);
		if (aInsertBefore == null)
			appendChild(lListitem);
		else
			insertBefore(lListitem, aInsertBefore);
		return lListitem;
	}

	/**
	 * On select set attribute changed to true.
	 */
	public void onSelect() {
		setAttribute("changed", "true");
	}

	/**
	 * On paging store paging.
	 */
	public void onPaging() {
		storePaging();
	}

	/**
	 * Stores listbox active page in session.
	 */
	protected void storePaging() {
		if (getId().equals("")) {
			return;
		}
		//NOTE only use session if listbox id is set, otherwise multiple listboxs on one page conflict
		String lSessionKeyPrefix = getSessionKeyPrefix(); 
		CDesktopComponents.cControl().setListboxUserSetting(lSessionKeyPrefix + "activepage", getActivePage()); 
	}
		
	/**
	 * Restores listbox settings from session.
	 */
	public void restoreSettings() {
		restorePaging();
		restoreSorting();
	}

	/**
	 * Restores listbox active page from session.
	 */
	protected void restorePaging() {
		if (getId().equals("")) {
			return;
		}
		//NOTE only use session if listbox id is set, otherwise multiple listboxs on one page conflict
		String lSessionKeyPrefix = getSessionKeyPrefix(); 
		Integer lActivePage = (Integer)CDesktopComponents.cControl().getListboxUserSetting(lSessionKeyPrefix + "activepage");
		//Active page is default 0, so check lActivePage > 0
		//check lActivePage < getPageCount() in case of deleted listitems
		if (lActivePage != null && lActivePage > 0 && lActivePage < getPageCount()) {
			setActivePage(lActivePage);
		}
	}

	/**
	 * Restores listbox sorting from session. It is stored by CDefListheader.
	 */
	protected void restoreSorting() {
		if (getId().equals("")) {
			return;
		}
		//NOTE only use session if listbox id is set, otherwise multiple listboxs on one page conflict
		String lSessionKeyPrefix = getSessionKeyPrefix(); 
		Integer lColumnIndex = (Integer)CDesktopComponents.cControl().getListboxUserSetting(lSessionKeyPrefix + "columnindex"); 
		String lSortDirection = (String)CDesktopComponents.cControl().getListboxUserSetting(lSessionKeyPrefix + "sortdirection");
		//check lColumnIndex < getListhead().getChildren().size() in case of columns are deleted
		if (lColumnIndex != null && lColumnIndex < getListhead().getChildren().size() && lSortDirection != null) {
			((Listheader)getListhead().getChildren().get(lColumnIndex)).sort(lSortDirection.equals("natural") || lSortDirection.equals("ascending"));
		}
	}

	/**
	 * Gets session key prefix, so it is unique per zul page.
	 *
	 * @return session key prefix
	 */
	protected String getSessionKeyPrefix() {
		//Use webpage and listbox id to make session key unique
		String lWebpage = Executions.getCurrent().getDesktop().getRequestPath();
		return lWebpage + "_" + getId() + "_"; 
	}

}
