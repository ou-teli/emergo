/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.def;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;

/**
 * The Class CDefHelper.
 * 
 * Is root class for application Helper classes.
 * Implements some general Listbox en Tree methods.
 */
public class CDefHelper {

	/**
	 * Creates new listitem.
	 * 
	 * @return the listitem
	 */
	public Listitem newListitem() {
		return new Listitem();
	}

	/**
	 * Appends listcell with label to listitem.
	 * 
	 * @param aListitem the listitem
	 * @param aLabel the label string
	 * 
	 * @return the listcell
	 */
	public Listcell appendListcell(Listitem aListitem, String aLabel) {
		Listcell lListcell = new Listcell(aLabel);
		aListitem.appendChild(lListcell);
		return lListcell;
	}

	/**
	 * Inserts listitem within listbox.
	 * 
	 * @param aListbox the listbox
	 * @param aListitem the listitem
	 * @param aInsertBefore the listitem to insert before
	 */
	public void insertListitem(Listbox aListbox, Listitem aListitem, Listitem aInsertBefore) {
		if (aInsertBefore == null)
			aListbox.appendChild(aListitem);
		else
			aListbox.insertBefore(aListitem, aInsertBefore);
	}

	/**
	 * Inserts treeitem within treechildren.
	 * 
	 * @param aTreechildren the treechildren
	 * @param aTreeitem the treeitem
	 * @param aInsertBefore the treeitem to insert before
	 */
	public void insertTreeitem(Component aTreechildren, Component aTreeitem, Component aInsertBefore) {
		if ((aTreechildren == null) || (aTreeitem == null))
			return;
		if (aInsertBefore == null)
			aTreechildren.appendChild(aTreeitem);
		else
			aTreechildren.insertBefore(aTreeitem, aInsertBefore);
	}

	/**
	 * Formats date as YMD string.
	 * 
	 * @param aDate the a date
	 * 
	 * @return the date str
	 */
	public static final String getDateStrAsYMD(Date aDate) {
		SimpleDateFormat lFormatter = new SimpleDateFormat("yyyy-MM-dd");
		return lFormatter.format(aDate);
	}

	/**
	 * Formats date as DMY string.
	 * 
	 * @param aDate the a date
	 * 
	 * @return the date str
	 */
	public static final String getDateStrAsDMY(Date aDate) {
		SimpleDateFormat lFormatter = new SimpleDateFormat("dd-MM-yyyy");
		return lFormatter.format(aDate);
	}

	/**
	 * Formats date as HMS string.
	 * 
	 * @param aDate the a date
	 * 
	 * @return the date str
	 */
	public static final String getDateStrAsHMS(Date aDate) {
		SimpleDateFormat lFormatter = new SimpleDateFormat("HH:mm:ss");
		return lFormatter.format(aDate);
	}

	/**
	 * Gets date from YMD string.
	 * 
	 * @param aDateStr the a date str
	 * 
	 * @return the date str
	 */
	public static final Date getDateFromStrYMD(String aDateStr) {
		Date lDate = null;
		if (!aDateStr.equals("")) {
			try {
				lDate = new SimpleDateFormat("yyyy-MM-dd").parse(aDateStr);
			} catch (ParseException e) {
			}
		}
		return lDate;
	}

	/**
	 * Gets date from DMY string.
	 * 
	 * @param aDateStr the a date str
	 * 
	 * @return the date str
	 */
	public static final Date getDateFromStrDMY(String aDateStr) {
		Date lDate = null;
		if (!aDateStr.equals("")) {
			try {
				lDate = new SimpleDateFormat("dd-MM-yyyy").parse(aDateStr);
			} catch (ParseException e) {
			}
		}
		return lDate;
	}

	/**
	 * Gets date from HMS string.
	 * 
	 * @param aTimeStr the a time str
	 * 
	 * @return the date str
	 */
	public static final Date getDateFromStrHMS(String aTimeStr) {
		Date lDate = null;
		if (!aTimeStr.equals("")) {
			try {
				lDate = new SimpleDateFormat("HH:mm:ss").parse(aTimeStr);
			} catch (ParseException e) {
			}
		}
		return lDate;
	}

	/**
	 * Gets date from YMD and HMS string.
	 * 
	 * @param aDateStr the a date str
	 * @param aTimeStr the a time str
	 * 
	 * @return the date str
	 */
	public static final Date getDateFromStrYMDAndHMS(String aDateStr, String aTimeStr) {
		Date lDate = null;
		if (!aDateStr.equals("")) {
			Date lDateDate = getDateFromStrYMD(aDateStr);
			if (lDateDate != null) {
				Date lDateTime = CDefHelper.getDateFromStrHMS(aTimeStr);
				if (lDateTime == null) {
					lDate = lDateDate;
				}
				else {
					//NOTE increase by one hour to correct (new Date(0)).getTime()
					lDate = new Date(lDateDate.getTime() + lDateTime.getTime() + 3600000);
				}
			}
		}
		return lDate;
	}

	/**
	 * Gets date from DMY and HMS string.
	 * 
	 * @param aDateStr the a date str
	 * @param aTimeStr the a time str
	 * 
	 * @return the date str
	 */
	public static final Date getDateFromStrDMYAndHMS(String aDateStr, String aTimeStr) {
		Date lDate = null;
		if (!aDateStr.equals("")) {
			Date lDateDate = getDateFromStrDMY(aDateStr);
			if (lDateDate != null) {
				Date lDateTime = CDefHelper.getDateFromStrHMS(aTimeStr);
				if (lDateTime == null) {
					lDate = lDateDate;
				}
				else {
					//NOTE increase by one hour to correct (new Date(0)).getTime()
					lDate = new Date(lDateDate.getTime() + lDateTime.getTime() + 3600000);
				}
			}
		}
		return lDate;
	}

	/**
	 * Formats time in secs as HMS string.
	 * 
	 * @param aTime the a time
	 * 
	 * @return the time str
	 */
	public static final String getTimeStrAsHMS(int aTime) {
		int lSeconds = aTime % 60;
	    int lHours = aTime / 60;
	    int lMinutes = lHours % 60;
	    lHours = lHours / 60;
	    return (lHours<10?"0":"") + lHours + ":" + (lMinutes<10?"0":"") + lMinutes + ":" + (lSeconds<10?"0":"") + lSeconds;
	}

}