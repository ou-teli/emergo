/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.def;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Hbox;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CIObserved;
import nl.surf.emergo.control.CIObserver;
import nl.surf.emergo.control.CObserverManager;

/**
 * The Class CDefHbox.
 * 
 * Is extension of ZK framework class.
 * All corresponding application classes extend from this one.
 * 
 * Implements CIObserved en CIObserver interfaces and uses observerManager
 * to implement Observer design pattern. Boxes can be clicked.
 */
public class CDefHbox extends Hbox implements CIObserved, CIObserver {

	private static final long serialVersionUID = 8362093129096018150L;

	/** The observer manager. */
	protected CObserverManager observerManager = new CObserverManager(this);

	/** The class name. */
	public final String className = this.getClass().getSimpleName();

	public CDefHbox() {
		super();
	  	Events.postEvent("onDecorate", this, null);
	}

	/**
	 * On decorate, decorate component.
	 */
	public void onDecorate(Event aEvent) {
		CDesktopComponents.vView().decorateComponent(this);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.def.CIObserved#registerObserver(java.lang.String)
	 */
	public void registerObserver(String aObserverId) {
		observerManager.registerObserver(aObserverId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.def.CIObserved#removeObserver(java.lang.String)
	 */
	public void removeObserver(String aObserverId) {
		observerManager.removeObserver(aObserverId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.def.CIObserved#notifyObservers(java.lang.String, java.lang.Object)
	 */
	public void notifyObservers(String aAction, Object aStatus) {
		observerManager.notifyObservers(aAction, aStatus);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.def.CIObserver#observedNotify(nl.surf.emergo.control.def.CIObserved, java.lang.String, java.lang.Object)
	 */
	public void observedNotify(CIObserved aObserved, String aAction,
			Object aStatus) {
		if (aObserved != null)
			this.notifyObservers(aAction, aStatus);
	}

}
