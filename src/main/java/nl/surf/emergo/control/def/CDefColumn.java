/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.def;

import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.SortEvent;
import org.zkoss.zul.Column;

import nl.surf.emergo.control.CDesktopComponents;

/**
 * The Class CDefColumn.
 * 
 * Is extension of ZK framework class.
 * All corresponding application classes extend from this one.
 */
public class CDefColumn extends Column {

	private static final long serialVersionUID = 9209046450915839873L;

	public CDefColumn() {
		super();
	  	Events.postEvent("onDecorate", this, null);
	}

	public void onSort(SortEvent event) {
		super.onSort(event);
		storeSorting();
	}

	/**
	 * Stores gridbox sorting in session. It is restored by CDefGrid.
	 */
	protected void storeSorting() {
		if (getGrid().getId().equals("")) {
			return;
		}
		//NOTE only use session if grid id is set, otherwise multiple grids on one page conflict
		String lSessionKeyPrefix = getSessionKeyPrefix();
		CDesktopComponents.cControl().setGridUserSetting(lSessionKeyPrefix + "columnindex", getGrid().getColumns().getChildren().indexOf(this)); 
		CDesktopComponents.cControl().setGridUserSetting(lSessionKeyPrefix + "sortdirection", getSortDirection());
	}

	/**
	 * Gets session key prefix, so it is unique per zul page.
	 *
	 * @return session key prefix
	 */
	protected String getSessionKeyPrefix() {
		//Use webpage and grid id to make session key unique
		String lWebpage = Executions.getCurrent().getDesktop().getRequestPath();
		return lWebpage + "_" + getGrid().getId() + "_"; 
	}

	/**
	 * On decorate, decorate component.
	 */
	public void onDecorate(Event aEvent) {
		CDesktopComponents.vView().decorateComponent(this);
	}

}
