/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.def;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Column;
import org.zkoss.zul.Grid;

import nl.surf.emergo.control.CDesktopComponents;

/**
 * The Class CDefGrid.
 *
 * Is extension of ZK framework class.
 * All corresponding application classes extend from this one.
 */
public class CDefGrid extends Grid {

	private static final long serialVersionUID = -4997409866321648747L;

	/** The class name. */
	public final String className = this.getClass().getSimpleName();

	public CDefGrid() {
		super();
	  	Events.postEvent("onDecorate", this, null);
	}

	/**
	 * On decorate, decorate component.
	 */
	public void onDecorate(Event aEvent) {
		CDesktopComponents.vView().decorateComponent(this);
		restoreSettings();
	}

	/**
	 * On select set attribute changed to true.
	 */
	public void onSelect() {
		setAttribute("changed", "true");
	}

	/**
	 * On paging store paging.
	 */
	public void onPaging() {
		storePaging();
	}

	/**
	 * Stores grid active page in session.
	 */
	protected void storePaging() {
		if (getId().equals("")) {
			return;
		}
		//NOTE only use session if grid id is set, otherwise multiple grids on one page conflict
		String lSessionKeyPrefix = getSessionKeyPrefix(); 
		CDesktopComponents.cControl().setGridUserSetting(lSessionKeyPrefix + "activepage", getActivePage()); 
	}
		
	/**
	 * Restores grid settings from session.
	 */
	public void restoreSettings() {
		restorePaging();
		restoreSorting();
	}

	/**
	 * Restores grid active page from session.
	 */
	protected void restorePaging() {
		if (getId().equals("")) {
			return;
		}
		//NOTE only use session if grid id is set, otherwise multiple grids on one page conflict
		String lSessionKeyPrefix = getSessionKeyPrefix(); 
		Integer lActivePage = (Integer)CDesktopComponents.cControl().getGridUserSetting(lSessionKeyPrefix + "activepage");
		//Active page is default 0, so check lActivePage > 0
		//check lActivePage < getPageCount() in case of deleted  grid items
		if (lActivePage != null && lActivePage > 0 && lActivePage < getPageCount()) {
			setActivePage(lActivePage);
		}
	}

	/**
	 * Restores grid sorting from session. It is stored by CDefListheader.
	 */
	protected void restoreSorting() {
		if (getId().equals("")) {
			return;
		}
		//NOTE only use session if grid id is set, otherwise multiple grids on one page conflict
		String lSessionKeyPrefix = getSessionKeyPrefix(); 
		Integer lColumnIndex = (Integer)CDesktopComponents.cControl().getGridUserSetting(lSessionKeyPrefix + "columnindex"); 
		String lSortDirection = (String)CDesktopComponents.cControl().getGridUserSetting(lSessionKeyPrefix + "sortdirection");
		//check lColumnIndex < getListhead().getChildren().size() in case of columns are deleted
		if (lColumnIndex != null && lColumnIndex < getColumns().getChildren().size() && lSortDirection != null) {
			int lIndex = 0;
			for (Component component : getColumns().getChildren()) {
				Column column = (Column)component;
				if (lIndex == lColumnIndex) {
					column.sort(lSortDirection.equals("natural") || lSortDirection.equals("ascending"));
					break;
				}
				lIndex++;
			}
		}
	}

	/**
	 * Gets session key prefix, so it is unique per zul page.
	 *
	 * @return session key prefix
	 */
	protected String getSessionKeyPrefix() {
		//Use webpage and grid id to make session key unique
		String lWebpage = Executions.getCurrent().getDesktop().getRequestPath();
		return lWebpage + "_" + getId() + "_"; 
	}

}
