/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import org.zkoss.zk.ui.event.CreateEvent;

import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.def.CDefLabel;
import nl.surf.emergo.view.VView;

/**
 * The Class CInputLabelDiv.
 */
public class CInputLabelDiv extends CDefDiv {

	private static final long serialVersionUID = -2776045397853993684L;

	/** The view. */
	private VView vView = CDesktopComponents.vView();

	public CInputLabelDiv() {
		super();
		setAttribute("width", "colWidth1");
		setAttribute("widthCorrection", "-10");
	}

	public void onCreate(CreateEvent aEvent) {
		CDefLabel lLabel = new CDefLabel();
		appendChild(lLabel);
		lLabel.setAttribute("labelCapitalizeFirstChar", "true");
		String[] lPossiblePrivateAttributes = new String[] {"_labelKey", "_labelValue", "_InputRequired", "_helpLabelKey", "_parentHelpLabelKey"};
		for (int i=0;i<lPossiblePrivateAttributes.length;i++) {
			String lAttributeValue = vView.getAttributeString(this, lPossiblePrivateAttributes[i]);
			if (lAttributeValue.length() > 0) {
				lLabel.setAttribute(lPossiblePrivateAttributes[i].substring(1), lAttributeValue);
			}
		}
	}
	
}
