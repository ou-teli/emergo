/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.IXmlManager;
import nl.surf.emergo.control.def.CDefListbox;
import nl.surf.emergo.control.script.CScriptMethodHbox;

/**
 * The Class CListbox.
 * 
 * Used as ancestor for all listboxes used within the CScriptMethodHbox class.
 */
public abstract class CListbox extends CDefListbox {

	private static final long serialVersionUID = 2963573405581554468L;

	/** The xml manager. */
	protected IXmlManager xmlManager = null;

	/** The label prefix. */
	protected String labelPrefix = "";

	/** The max rows. */
	protected int maxRows = 10;

	/**
	 * Instantiates a new listbox.
	 * 
	 * @param aId the id
	 * @param aLabelPrefix the label prefix
	 */
	public CListbox(String aId, String aLabelPrefix) {
		setId(aId);
		setMultiple(true);
		setCheckmark(true);
		labelPrefix = aLabelPrefix;
	}

	/**
	 * Instantiates a new listbox.
	 * 
	 * @param aId the id
	 * @param aAttKey the attribute key to set
	 * @param aAttValue the attribute value
	 * @param aLabelPrefix the label prefix
	 */
	public CListbox(String aId, String aAttKey, String aAttValue, String aLabelPrefix) {
		setId(aId);
		setMultiple(true);
		setCheckmark(true);
		setAttribute(aAttKey, aAttValue);
		labelPrefix = aLabelPrefix;
	}

	/**
	 * Gets the xml manager.
	 * 
	 * @return the xml manager
	 */
	protected IXmlManager getXmlManager() {
		if (xmlManager == null)
			xmlManager = CDesktopComponents.sSpring().getXmlManager();
		return xmlManager;
	}

	/**
	 * Shows listbox items using a list of objects.
	 * Multiple items can be selected.
	 * 
	 * @param aItems the items
	 * @param aIds the ids of the previously selected combobox items or empty
	 * @param aPreselectIfOneItem preselect if only one item
	 * @param aSort sort
	 */
	public void showItems(List<Object> aItems, String[] aIds, boolean aPreselectIfOneItem, boolean aSort) {
		// items can change due to other selections so clear children
		getChildren().clear();
		if (aItems == null || aItems.size() == 0) {
			// if list empty hide
			setVisible(false);
			return;
		}
		setVisible(true);
		
		if (aSort) {
			//use a treemap to be able to sort aItems on key item name.
		    Map<String,Object> lMap = new TreeMap<String,Object>();
			for (Object lItem : aItems) {
				lMap.put(getItemName(lItem), lItem);
			}
			//loop through map to create new array of items
			List<Object> lItems = new ArrayList<Object>();
		    Iterator<Map.Entry<String,Object>> iterator = lMap.entrySet().iterator();
		    while(iterator.hasNext()) {
		    	lItems.add(iterator.next().getValue());
		    }
		    //use new array of items
		    aItems = lItems;
		}

		String lId = null;
		String lName = null;
		String lHelpText = null;
		for (Object lItem : aItems) {
			//add item
			lId = getItemId(lItem);
			lName = getItemName(lItem);
			lHelpText = getItemHelp(lItem);
			Listitem lListitem = insertListitem(lId, lName, null);
			if (!lHelpText.equals("")) {
				lListitem.setTooltiptext(lHelpText);
			}
			if (aItems.size() == 1) {
				if (aPreselectIfOneItem || isIdPresentInIds(lId, aIds)) {
					addItemToSelection(lListitem);
				}
			}
			else if (isIdPresentInIds(lId, aIds)) {
				// if item chosen before preselect it
				addItemToSelection(lListitem);
			}
	    }
	    
		if (aItems.size() > maxRows) {
			setRows(maxRows);
		}
		else {
			setRows(0);
		}
		// no select is sent like within CCombo, because multiple selections
	}

	/**
	 * Is an id present in an array of ids.
	 * 
	 * @param aId the ids
	 * @param aIds the ids
	 * 
	 * @return if present
	 */
	private boolean isIdPresentInIds(String aId, String[] aIds) {
		if (aIds.length > 0) {
			for (int i = 0; i < aIds.length; i++) {
				if (aId.equals(aIds[i]))
					return true;
			}
		}
		return false;
	}

	/**
	 * Gets the item id. Should be overridden.
	 * 
	 * @param aItem the item
	 * 
	 * @return the item id
	 */
	protected abstract String getItemId(Object aItem);

	/**
	 * Gets the item name. Should be overridden.
	 * 
	 * @param aItem the item
	 * 
	 * @return the item name
	 */
	protected abstract String getItemName(Object aItem);

	/**
	 * Gets the item help. Should be overridden.
	 * 
	 * @param aItem the item
	 * 
	 * @return the item help
	 */
	protected abstract String getItemHelp(Object aItem);

	/**
	 * Gets the selected item ids.
	 * 
	 * @return the item ids
	 */
	protected List<String> getSelectedItemIds() {
		List<String> lItemIds = new ArrayList<String>();
		if (getSelectedItems() == null|| getSelectedItems().size() == 0)
			return lItemIds;
		for (Listitem lListitem : getSelectedItems()) {
			String lId = (String)lListitem.getValue();
			if (lId != null) {
				lItemIds.add(lId);
			}
		}
		return lItemIds;
	}

	/**
	 * Gets the script method hbox.
	 * 
	 * @return the item id
	 */
	protected CScriptMethodHbox getScriptMethodHbox() {
		Component lComponent = this.getParent();
		while ((lComponent != null) && (!(lComponent instanceof CScriptMethodHbox))) {
			lComponent = lComponent.getParent();
		}
		return (CScriptMethodHbox)lComponent;
	}

	/**
	 * Gets the component code.
	 * 
	 * @return the component code
 	 */
	protected String getComponentCode() {
		CScriptMethodHbox lHbox = getScriptMethodHbox();
		if (lHbox == null || lHbox.getCaseComponent() == null)
			return "";
		return lHbox.getCaseComponent().getEComponent().getCode();
	}

	/**
	 * On select notify the CScriptMethodHbox class.
	 */
	@Override
	public void onSelect() {
		CScriptMethodHbox lHbox = getScriptMethodHbox();
		if (lHbox == null)
			return;
		lHbox.onAction(this, "listboxSelectItem", getSelectedItemIds());
	}
	
}
