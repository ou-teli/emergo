/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import java.util.Hashtable;
import java.util.List;

import org.zkoss.gmaps.Gmarker;
import org.zkoss.gmaps.event.MapDropEvent;
import org.zkoss.gmaps.event.MapMouseEvent;
import org.zkoss.zk.ui.Component;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.def.CDefGmaps;
import nl.surf.emergo.zkspring.SSpring;

/**
 * The Class CGmaps.
 *
 * Ancestor of all google maps controls within app, author as well as player
 * environment.
 */
public class CGmaps extends CDefGmaps {

	private static final long serialVersionUID = -3876922131966567350L;

	/**
	 * Sets init values.
	 */
	public void setInitValues() {
		String lScheme = CDesktopComponents.vView().getServerProtocol();
		if (!lScheme.isEmpty())
			setProtocol(lScheme);
		setZclass(className + "_map_area");
		setWidth("938px");
		setHeight("428px");
		setShowSmallCtrl(true);
		setMapType("satellite");
		setShowLargeCtrl(true);
		setShowTypeCtrl(true);
		setShowOverviewCtrl(true);
	}

	/**
	 * Instantiates a new c gmaps.
	 */
	public CGmaps() {
		setInitValues();

//		geocoder = new GClientGeocoder();

		initMap();
		update();
	}

	/**
	 * Instantiates a new c gmaps.
	 *
	 * @param aCreateContent the a create content
	 */
	public CGmaps(boolean aCreateContent) {
		setInitValues();
		if (aCreateContent) {
			initMap();
			update();
		}
	}

	/**
	 * Gets the sspring.
	 *
	 * @return the sspring
	 */
	public SSpring getSSpring() {
		return CDesktopComponents.sSpring();
	}

	/**
	 * Get Content Helper.
	 *
	 * @return the content helper
	 */
	protected CContentHelper getContentHelper() {
		CContentHelper lHelper = new CContentHelper(null);
		return lHelper;
	}

	/**
	 * Inits map.
	 */
	public void initMap() {
		super.setZoom(getExZoom());
		super.setLat(getExLatitude());
		super.setLng(getExLongitude());

		super.setEnableGoogleBar(true);

	}

	/**
	 * Updates google maps.
	 */
	public void update() {
		update(null);
	}

	/**
	 * Updates google maps.
	 *
	 * @param aTag the a tag, if null all tags are rendered
	 */
	public void update(IXMLTag aTag) {
		getChildren().clear();

		CContentHelper lHelper = getContentHelper();
		if (aTag == null) {
			aTag = lHelper.getXmlDataTree();
			lHelper.setItemAttributes(this, aTag.getChild(AppConstants.contentElement));
		}
		lHelper.xmlContentToContentItems(aTag, this);
	}

	/**
	 * Set Zoom of map.
	 *
	 * @param aZoom the a zoom
	 */
	@Override
	public void setZoom(int aZoom) {
		super.setZoom(aZoom);
		setComponentStatus("exzoom", "" + aZoom);
	}

	/**
	 * Set Lat of map.
	 *
	 * @param aLat the a lat
	 */
	@Override
	public void setLat(double aLat) {
		super.setLat(aLat);
		setComponentStatus("exlatitude", "" + aLat);
	}

	/**
	 * Set Lng of map.
	 *
	 * @param aLng the a lng
	 */
	@Override
	public void setLng(double aLng) {
		super.setLng(aLng);
		setComponentStatus("exlongitude", "" + aLng);
	}

	/**
	 * Get ExZoom, the zoom saved as component status.
	 *
	 * @return the ex zoom
	 */
	protected int getExZoom() {
		String lZoomStr = getComponentStatus("exzoom");
		if (lZoomStr.equals(""))
			return 7;
		else
			return Integer.parseInt(lZoomStr);
	}

	/**
	 * Set ExZoom, save zoom within component status.
	 *
	 * @param aZoom the a zoom
	 */
	protected void setExZoom(int aZoom) {
		setComponentStatus("exzoom", "" + aZoom);
	}

	/**
	 * Get ExLatitude, the latitude saved as component status.
	 *
	 * @return the ex latitude
	 */
	protected double getExLatitude() {
		String lLatitudeStr = getComponentStatus("exlatitude");
		if (lLatitudeStr.equals(""))
			return 51.876491;
		else
			return Double.parseDouble(lLatitudeStr);
	}

	/**
	 * Set ExLatitude, save latitude within component status.
	 *
	 * @param aLatitude the a latitude
	 */
	protected void setExLatitude(double aLatitude) {
		setComponentStatus("exlatitude", "" + aLatitude);
	}

	/**
	 * Get ExLongitude, the longitude saved as component status.
	 *
	 * @return the ex longitude
	 */
	protected double getExLongitude() {
		String lLongitudeStr = getComponentStatus("exlongitude");
		if (lLongitudeStr.equals(""))
			return 5.460205;
		else
			return Double.parseDouble(lLongitudeStr);
	}

	/**
	 * Set ExLongitude, save longitude within component status.
	 *
	 * @param aLongitude the a longitude
	 */
	protected void setExLongitude(double aLongitude) {
		setComponentStatus("exlongitude", "" + aLongitude);
	}

	/**
	 * Get Component status.
	 *
	 * @param aStatusKey the a status key
	 *
	 * @return the component status
	 */
	public String getComponentStatus(String aStatusKey) {
		CContentHelper lHelper = getContentHelper();
		// lHelper uses session variables to get current xml tree
		IXMLTag lRootTag = lHelper.getXmlDataTree();
		if (lRootTag == null)
			return "";
		IXMLTag lComponentTag = lRootTag.getChild(AppConstants.componentElement);
		if (lComponentTag == null)
			return "";
		return lComponentTag.getChildAttribute(AppConstants.statusElement, aStatusKey);
	}

	/**
	 * Set Component status.
	 *
	 * @param aStatusKey   the a status key
	 * @param aStatusValue the a status value
	 */
	public void setComponentStatus(String aStatusKey, String aStatusValue) {
		CContentHelper lHelper = getContentHelper();
		// lHelper uses session variables to get current xml tree
		IXMLTag lRootTag = lHelper.getXmlDataTree();
		if (lRootTag == null)
			return;
		CDesktopComponents.sSpring().getXmlManager().setCompInitialstatus(lRootTag, aStatusKey, aStatusValue);
		lHelper.setXmlData(CDesktopComponents.sSpring().getXmlManager().xmlTreeToDoc(lRootTag));
	}

	/**
	 * On onMapMove, save new position as component status.
	 */
	public void onMapMove() {
		// save new position
		setComponentStatus("exlatitude", "" + getLat());
		setComponentStatus("exlongitude", "" + getLng());
	}

	/**
	 * On onMapZoom, save new zoom as component status.
	 */
	public void onMapZoom() {
		// save new zoom
		setComponentStatus("exzoom", "" + getZoom());
	}

	/**
	 * On onMapRightClick. If marker is right clicked show menu for marker,
	 * otherwise show menu to add marker.
	 *
	 * @param aEvent the a event
	 */
	public void onMapClick(MapMouseEvent aEvent) {
		onMapRightClick(aEvent);
	}

	/**
	 * On onMapRightClick. If marker is right clicked show menu for marker,
	 * otherwise show menu to add marker.
	 *
	 * @param aEvent the a event
	 */
	@SuppressWarnings("deprecation")
	public void onMapRightClick(MapMouseEvent aEvent) {
		Gmarker gmarker = aEvent.getGmarker();
		CContentItemMenuPopup lMenuPopup = getMenuPopup();
		if (lMenuPopup == null)
			return;
		Hashtable<String, String> lContext = new Hashtable<String, String>(0);
		if (gmarker != null) {
			// gmarker could be opened or changed or deleted
			// show popupmenu with inspect, edit and delete
//			lMenuPopup.open(gmarker);
			lContext.put("exlatitude", "" + gmarker.getLat());
			lContext.put("exlongitude", "" + gmarker.getLng());
			lMenuPopup.setAttribute("nodecontext", lContext);
			lMenuPopup.setAttribute("target", gmarker);
			lMenuPopup.open(this);
		} else {
			// new gmarker
			// show popupmenu with new
			lContext.put("exlatitude", "" + aEvent.getLatLng().getLatitude());
			lContext.put("exlongitude", "" + aEvent.getLatLng().getLongitude());
			lMenuPopup.setAttribute("nodecontext", lContext);
			lMenuPopup.setAttribute("target", null);
			lMenuPopup.open(this);
		}
		CContentHelper lHelper = getContentHelper();
		String lLeft = "" + Math.round(CDesktopComponents.sSpring().getSCaseSkinHelper()
				.getCaseSkinWidth(lHelper.getCaseComponent().getECase()) / 2);
		String lTop = "" + Math.round(CDesktopComponents.sSpring().getSCaseSkinHelper()
				.getCaseSkinHeight(lHelper.getCaseComponent().getECase()) / 2);
		lMenuPopup.setLeft("" + lLeft + "px");
		lMenuPopup.setTop("" + lTop + "px");
	}

	/**
	 * Open marker.
	 *
	 * @param aGmarker the a gmarker
	 */
	public void openMarker(Gmarker aGmarker) {
		if (aGmarker != null) {
			String lContent = aGmarker.getContent();
			if (lContent == null || lContent.equals("")) {
				IXMLTag lTag = (IXMLTag) aGmarker.getAttribute("item");
				CContentHelper lHelper = getContentHelper();
				if (lTag != null) {
					lContent = "";
					String lUrl = "";
					lContent = getSSpring().unescapeXML(lHelper.getNodeChildValue(lTag, "name"));
					lUrl = getSSpring().getSBlobHelper().getUrl(lTag);
					lUrl = getSSpring().getSBlobHelper().convertHrefForCertainMediaTypes(lUrl);
					if (!lUrl.equals("")) {
						if (!CDesktopComponents.vView().isAbsoluteUrl(lUrl)) {
							// if relative path, add root, otherwise path doesn't contain emergo
							if (!(lUrl.indexOf("/") == 0))
								lUrl = "/" + lUrl;
							lUrl = CDesktopComponents.vView().getEmergoWebappsRoot() + lUrl;
							/*
							 * following code results in anchor tag 'http://localhost/\emergo\....' -> not
							 * found int lPos = lUrl.indexOf("?"); String lTest = lUrl; String lRest = "";
							 * if (lPos > 0) { lTest = lUrl.substring(0, lPos); lRest =
							 * lUrl.substring(lPos); } lTest =
							 * Paths.get(CDesktopComponents.vView().getEmergoWebappsRoot(),lTest).toString()
							 * ; lUrl = lTest + lRest;
							 */
						}
						lContent = "<a href=\"" + lUrl + "\" target=\"_blank\">" + lContent + "</a>";
					}
					lContent = lContent + "\n";
					lContent = lContent + getSSpring().unescapeXML(lHelper.getNodeChildValue(lTag, "description"));
					aGmarker.setContent(lContent);
				}
			}
			aGmarker.setOpen(true);
		}
	}

	/**
	 * Gets the menu popup to show when map is clicked.
	 *
	 * @return the menu popup
	 */
	protected CContentItemMenuPopup getMenuPopup() {
		return (CContentItemMenuPopup) CDesktopComponents.vView().getComponent("menuPopup");
	}

	/**
	 * On onMapDrop.
	 */
	@SuppressWarnings("deprecation")
	public void onMapDrop(MapDropEvent aEvent) {
		if (aEvent.getDragged() instanceof Gmarker) {
//			int lKeys = aEvent.getKeys();
//			boolean lCtrlKeyPressed = (lKeys == (MouseEvent.CTRL_KEY + MouseEvent.LEFT_CLICK));
			// don't use Ctrl key to copy markers, because marker position already is
			// changed when dropped
			Gmarker gmarker = (Gmarker) aEvent.getDragged();
			if (gmarker != null) {
				onDrop(gmarker, aEvent.getLat(), aEvent.getLng());
			}
		}
	}

	/**
	 * On drop, move a gmarker on the map, and change the corresponding xml tag
	 * within the xml tag tree. On error or if dropping not possible do nothing
	 *
	 * @param aDragged    the dragged gmarker
	 * @param aDraggedLat the dragged latitude
	 * @param aDraggedLng the dragged longitude
	 */
	public void onDrop(Gmarker aDragged, double aDraggedLat, double aDraggedLng) {
		// map can also be moved!
		// doesn't work, maybe to early and timer is solution!
//		setExLatitude(getLat());
//		setExLongitude(getLng());
		// only gps coords are changed
		CContentHelper lHelper = getContentHelper();
		IXMLTag lDraggedTag = (IXMLTag) aDragged.getAttribute("item");
		IXMLTag lChildTag = lHelper.getNodeChild(lDraggedTag, "exlatitude");
		if (lChildTag != null) {
			lChildTag.setValue("" + aDraggedLat);
		}
		lChildTag = lHelper.getNodeChild(lDraggedTag, "exlongitude");
		if (lChildTag != null) {
			lChildTag.setValue("" + aDraggedLng);
		}
		List<String[]> lErrors = setContentItem(lDraggedTag);
		if ((lErrors != null) && (lErrors.size() > 0)) {
			// if errors show them on target aDropped
			CDesktopComponents.cControl().showErrors(aDragged, lErrors);
		}
	}

	/**
	 * setContentItem. Updates aTag.
	 *
	 * @param aTag the a tag
	 *
	 * @return the error list
	 */
	protected List<String[]> setContentItem(IXMLTag aTag) {
		CContentHelper lHelper = getContentHelper();
		// do move xml tag within xml tag tree
		return lHelper.updateNode(aTag, true);
	}

	/**
	 * Gets the xml tag referenced within the contentitem.
	 *
	 * @param aContentItem the contentitem
	 *
	 * @return the xml tag
	 */
	public IXMLTag getContentItemTag(Component aContentItem) {
		if (aContentItem == null)
			return null;
		return ((IXMLTag) aContentItem.getAttribute("item"));
	}

	/**
	 * Gets the contentitem with xml tag given by aTagId.
	 *
	 * @param aTagId the tag id
	 *
	 * @return the contentitem
	 */
	public Component getContentItem(String aTagId) {
		return getContentItem(this, aTagId);
	}

	/**
	 * Gets the contentitem with xml tag given by aTagId and parent component by
	 * aParent. Nested.
	 *
	 * @param aParent the parent component
	 * @param aTagId  the tag id
	 *
	 * @return the contentitem
	 */
	protected Component getContentItem(Component aParent, String aTagId) {
		if (aTagId.equals("1"))
			// if root, '1', then return this component
			// gmaps has no root element like tree, so gmaps is root
			return this;
		for (Component lComponent : aParent.getChildren()) {
			if (lComponent instanceof Gmarker) {
				IXMLTag lTag = getContentItemTag(lComponent);
				if ((lTag != null) && (lTag.getAttribute(AppConstants.keyId).equals(aTagId)))
					return lComponent;
			}
			// nesting
			Component lContentItem = getContentItem(lComponent, aTagId);
			if (lContentItem != null)
				return lContentItem;
		}
		return null;
	}

}
