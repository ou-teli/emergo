/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.zkoss.web.Attributes;
import org.zkoss.zk.ui.AbstractComponent;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zul.Messagebox;

import nl.surf.emergo.business.IAppManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.view.VView;

/**
 * The Class CControl.
 *
 * Is used for performing general control layer tasks, such as session variable management
 * and showing error messages.
 */
public class CControl {

	/** The Constant runWnd. This is the id of main window of the player. */
	public static final String runWnd = "runWnd";

	/** The Constant scriptComponent. The id to use for the CScript class when evaluating Java using Bsh class. */
	public static final String scriptComponent = "cScript";

	/** The session. */
	protected Session session = Sessions.getCurrent();
	
	/** used to store session attributes per session ids in application memory */
	protected Map<String,Map<String,Object>> appAttributesPerSessionIds = null;
	
	/** used to store and retrieve maxInactiveIntervalKey and lastAccessedTimeKey per session id */
	protected static final String sessionMaxInactiveIntervalKey = "session_maxInactiveInterval";
	protected static final String sessionLastAccessedTimeKey = "session_lastAccessedTime";
	
	/**
	 * Gets the session.
	 *
	 * @return the session
	 */
	public Session getSession() {
		return session;
	}

	/**
	 * Gets the app attributes per session id, stored in application memory.
	 *
	 * @return the app attributes per session id
	 */
	synchronized public Map<String,Map<String,Object>> getAppAttributesPerSessionIds() {
		if (getSession() == null) {
			return null;
		}
		if (appAttributesPerSessionIds == null) {
			ServletContext servletContext = session.getWebApp().getServletContext();
			ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			IAppManager appManager = (IAppManager)applicationContext.getBean("appManager");
			Hashtable<String,Object> appContainer = appManager.getAppContainer();
			if (appContainer.containsKey(AppConstants.appAttributesPerSessionIds)) {
				appAttributesPerSessionIds = (Map<String,Map<String,Object>>)appContainer.get(AppConstants.appAttributesPerSessionIds);
			}
			else {
				appAttributesPerSessionIds = new HashMap<String,Map<String,Object>>();
				appContainer.put(AppConstants.appAttributesPerSessionIds, appAttributesPerSessionIds);
			}
		}
		return appAttributesPerSessionIds;
	}

	/**
	 * Gets the app attributes for the current session id, stored in application memory.
	 *
	 * @return the app attributes per session id
	 */
	synchronized public Map<String,Object> getAppAttributesPerSessionId() {
		if (getSession() == null || getAppAttributesPerSessionIds() == null) {
			return null;
		}
		String sessionId = ((HttpSession)getSession().getNativeSession()).getId();
		Map<String,Object> sessionAttributes = null;
		if (!getAppAttributesPerSessionIds().containsKey(sessionId)) {
			//if sessionId is missing, add new map
			sessionAttributes = new HashMap<String,Object>();
			//store session time out time in secs
			sessionAttributes.put(sessionMaxInactiveIntervalKey, new Integer(((HttpSession)getSession().getNativeSession()).getMaxInactiveInterval()));
			getAppAttributesPerSessionIds().put(sessionId, sessionAttributes);
		}
		else {
			sessionAttributes = getAppAttributesPerSessionIds().get(sessionId);
		}
		return sessionAttributes;
	}

	/**
	 * Gets the sess attr.
	 *
	 * @param aKey the key
	 *
	 * @return the sess attr
	 */
	public Object getSessAttr(String aKey) {
		if (getSession() == null) {
			return null;
		}
		else {
			return getSession().getAttribute(aKey);
		}
	}

	/**
	 * Sets sess attr.
	 *
	 * @param aKey the key
	 * @param aValue the value
	 */
	public void setSessAttr(String aKey, Object aValue) {
		if (getSession() == null) {
			return;
		}
		if (aValue == null) {
			getSession().removeAttribute(aKey);
		}
		else {
			getSession().setAttribute(aKey, aValue);
		}
	}

	/**
	 * Gets the app attribute, stored in application memory.
	 *
	 * @param aKey the key
	 *
	 * @return the app attribute
	 */
	synchronized public Object getAppAttribute(String aKey) {
		Map<String,Object> appAttributes = getAppAttributesPerSessionId(); 
		if (appAttributes == null) {
			return null;
		}
		if (appAttributes.containsKey(aKey)) {
			//store last access time in msecs
			appAttributes.put(sessionLastAccessedTimeKey, new Long(((HttpSession)getSession().getNativeSession()).getLastAccessedTime()));
			return appAttributes.get(aKey);
		}
		else {
			return null;
		}
	}

	/**
	 * Sets app attribute, stored in application memory.
	 *
	 * @param aKey the key
	 * @param aValue the value
	 */
	synchronized public void setAppAttribute(String aKey, Object aValue) {
		Map<String,Object> appAttributes = getAppAttributesPerSessionId(); 
		if (appAttributes == null) {
			return;
		}
		if (aValue == null) {
			if (!appAttributes.containsKey(aKey)) {
				return;
			}
			appAttributes.remove(aKey);
		}
		else {
			appAttributes.put(aKey, aValue);
		}
		//store last access time in msecs
		appAttributes.put(sessionLastAccessedTimeKey, new Long(((HttpSession)getSession().getNativeSession()).getLastAccessedTime()));
	}

	/**
	 * Gets the account sess attr.
	 *
	 * @param aKey the key
	 *
	 * @return the account sess attr
	 */
	public Object getAccSessAttr(String aKey) {
		return getAppAttribute("sess_" + aKey);
	}

	/**
	 * Sets account sess attr.
	 *
	 * @param aKey the key
	 * @param aValue the value
	 */
	public void setAccSessAttr(String aKey, Object aValue) {
		setAppAttribute("sess_" + aKey, aValue);
	}

	/**
	 * Gets the run sess attr.
	 *
	 * @param aKey the key
	 *
	 * @return the run sess attr
	 */
	public Object getRunSessAttr(String aKey) {
		return getAppAttribute("sess_run_" + aKey);
	}

	/**
	 * Sets the run sess attr.
	 *
	 * @param aKey the key
	 * @param aValue the value
	 */
	public void setRunSessAttr(String aKey, Object aValue) {
		setAppAttribute("sess_run_" + aKey, aValue);
	}

	/**
	 * Clear session attributes.
	 *
	 */
	public void clearSession() {
		//NOTE keep preferred locale containing preferred language and simple session
	    Iterator<Map.Entry<String,Object>> iterator = session.getAttributes().entrySet().iterator();
	    while(iterator.hasNext()) {
	    	Map.Entry<String,Object> entry = iterator.next();
	    	if (!(entry.getKey().equals(Attributes.PREFERRED_LOCALE) || entry.getKey().equals("javax.zkoss.zk.ui.Session"))) {
				setSessAttr(entry.getKey(), null);
	    	}
	    }
	    //NOTE remove 'session' keys kept in application memory as well
		String sessionId = ((HttpSession)getSession().getNativeSession()).getId();
		if (getAppAttributesPerSessionIds() != null && getAppAttributesPerSessionIds().containsKey(sessionId)) {
			getAppAttributesPerSessionIds().remove(sessionId);
		}
	}

	/**
	 * Remove timed out app attributes per session ids.
	 */
	synchronized public void removeTimedOutAppAttributesPerSessionIds() {
		if (getAppAttributesPerSessionIds() == null) {
			return;
		}
		//NOTE because session cleanup is not sent 'session' keys in memory are removed on session init of the same or another user
		//current time in seconds
		Long currentTime = new Long(System.currentTimeMillis() / 1000);
		List<String> sessionsToBeRemoved = new ArrayList<String>();
		for (Map.Entry<String,Map<String,Object>> entry : getAppAttributesPerSessionIds().entrySet()) {
			Map<String,Object> sessionAttributes = entry.getValue();
			//session time out in seconds
			Integer maxInactiveIntervalKey = (Integer)sessionAttributes.get(sessionMaxInactiveIntervalKey);
			//last accessed time in milliseconds
			Long lastAccessedTimeKey = (Long)sessionAttributes.get(sessionLastAccessedTimeKey);
			if (maxInactiveIntervalKey != null && lastAccessedTimeKey != null) {
				//last accessed time in seconds
				lastAccessedTimeKey = lastAccessedTimeKey / 1000;
				Long elapsedTime = currentTime - lastAccessedTimeKey;
				if (elapsedTime.intValue() > maxInactiveIntervalKey) {
					sessionsToBeRemoved.add(entry.getKey());
				}
			}
		}
		for (String sessionToBeRemoved : sessionsToBeRemoved) {
			getAppAttributesPerSessionIds().remove(sessionToBeRemoved);
		}
	}

	/**
	 * Shows errors relative to ZK component aTarget.
	 *
	 * @param aTarget the ZK target
	 * @param aErrors the error list
	 */
	public void showErrors(AbstractComponent aTarget, List<String[]> aErrors) {
		showErrors(aTarget, aErrors, null);
	}

	/**
	 * Shows warnings relative to ZK component aTarget.
	 *
	 * @param aTarget the ZK target
	 * @param aWarnings the warning list
	 */
	public void showWarnings(AbstractComponent aTarget, List<String[]> aWarnings) {
		showMessages("warning.", aTarget, aWarnings, null);
	}

	/**
	 * Shows errors relative to ZK component aTarget.
	 *
	 * @param aTarget the ZK target
	 * @param aErrors the error list
	 * @param aSpecificError additional error message, for example returned by an external function
	 */
	public void showErrors(AbstractComponent aTarget, List<String[]> aErrors, String aSpecificError) {
		showMessages("error.", aTarget, aErrors, aSpecificError);
	}

	/**
	 * Shows messages relative to ZK component aTarget.
	 *
	 * @param aPrefix the prefix
	 * @param aTarget the ZK target
	 * @param aMessages the message list
	 * @param aSpecificMessage additional message, for example returned by an external function
	 */
	protected void showMessages(String aPrefix, AbstractComponent aTarget, List<String[]> aMessages, String aSpecificMessage) {
		String lStr = "";
		for (String[] lError : aMessages) {
			String lKey = aPrefix + lError[0] + "." + lError[1];
			if (!lStr.equals("")) {
				lStr += "\n";
			}
			lStr += CDesktopComponents.vView().getLabel(lKey);
		}
		if (aSpecificMessage != null && !aSpecificMessage.equals("")) {
			if (!lStr.equals(""))
				lStr += "\n";
			lStr += aSpecificMessage;
		}
		if (!lStr.equals("")) {
			if (aPrefix.equals("warning.") && aTarget == null) {
				CDesktopComponents.vView().showMessagebox(null, lStr, CDesktopComponents.vView().getLabel("messagebox.title.warnings"), Messagebox.OK, Messagebox.EXCLAMATION);
			}
			else {
				// causes ZK to show error message
				throw new WrongValueException(aTarget, lStr);
			}
		}
	}

	/**
	 * returns error message string.
	 *
	 * @param aErrors the error list
	 *
	 * @return the error string
	 */
	public String getErrorString(List<String[]> aErrors) {
		String lStr = "";
		for (String[] lError : aErrors) {
			String lKey = "error." + lError[0] + "." + lError[1];
			if (!lStr.equals(""))
				lStr += "\n";
			lStr += CDesktopComponents.vView().getLabel(lKey);
		}
		return lStr;
	}

	/**
	 * returns language parameter string for requests.
	 *
	 * @return the language request parameter
	 */
	public String getReqLangParams() {
		Locale lLanguage = (Locale)getSessAttr(Attributes.PREFERRED_LOCALE);
		String lStr = "&lang_lang=";
		if (lLanguage != null) {
			String lTmp = lLanguage.getLanguage();
			if (!((lTmp == null) || (lTmp.equals(""))))
				lStr = lStr + lTmp;
			lStr = lStr + "&lang_count=";
			lTmp = lLanguage.getCountry();
			if (!((lTmp == null) || (lTmp.equals(""))))
				lStr = lStr + lTmp;
			lStr = lStr + "&lang_var=";
			lTmp = lLanguage.getVariant();
			if (!((lTmp == null) || (lTmp.equals(""))))
				lStr = lStr + lTmp;
		}
		return lStr;
	}

	/**
	 * returns Locale string.
	 *
	 * @return the Locale string
	 */
	public String getLocaleString() {
		Locale lLocale = (Locale)getSessAttr(Attributes.PREFERRED_LOCALE);
		if (lLocale != null && !StringUtils.isEmpty(lLocale.getLanguage()) && !StringUtils.isEmpty(lLocale.getCountry())) {
			return lLocale.getLanguage() + "_" + lLocale.getCountry();
		}
		else {
			//NOTE default Locale is Dutch
			return "nl_NL";
		}
	}

	/**
	 * Handles request parameters for language.
	 * 
	 * @param aRedirectUrl the redirect url
	 */
	public void handleLanguage(String aRedirectUrl) {
		String lLangLang = (String)VView.getReqPar("lang_lang");
		String lLangCount = (String)VView.getReqPar("lang_count");
		String lLangVar = (String)VView.getReqPar("lang_var");
		if (!StringUtils.isEmpty(lLangLang)) {
			Locale locale = null;
			if (StringUtils.isEmpty(lLangCount)) {
				locale = new Locale(lLangLang);
			}
			else {
				if (StringUtils.isEmpty(lLangVar)) {
					locale = new Locale(lLangLang, lLangCount);
				}
				else {
					locale = new Locale(lLangLang, lLangCount, lLangVar);
				}
			}
			if (locale != null) {
				setSessAttr(Attributes.PREFERRED_LOCALE, locale);
				if (!StringUtils.isEmpty(aRedirectUrl)) {
					CDesktopComponents.vView().redirectToView(aRedirectUrl);
				}
			}
		}
	}
	
	/**
	 * Gets the current account id saved as desktop variable.
	 *
	 * @return the account
	 */
	public int getAccId() {
		Integer lAccId = (Integer)getAccSessAttr("accId");
		if (lAccId == null) {
			return 0;
		}
		return lAccId.intValue();
	}

	/**
	 * Sets the current account id. 
	 *
	 * @param accId the acc id
	 */
	public void setAccId(int accId) {
		setAccSessAttr("accId", new Integer(accId));
	}
	
	/**
	 * Gets the account sess attr.
	 *
	 * @param aKey the key
	 *
	 * @return the account sess attr
	 */
	public Object getListboxUserSetting(String aKey) {
		Map<String,Object> lListboxUserSettings = (Map<String,Object>)getAccSessAttr(AppConstants.listbox_user_settings);
		if (lListboxUserSettings != null && lListboxUserSettings.containsKey(aKey)) {
			return lListboxUserSettings.get(aKey);
		}
		return null;
	}

	/**
	 * Sets account sess attr.
	 *
	 * @param aKey the key
	 * @param aValue the value
	 */
	public void setListboxUserSetting(String aKey, Object aValue) {
		Map<String,Object> lListboxUserSettings = (Map<String,Object>)getAccSessAttr(AppConstants.listbox_user_settings);
		if (lListboxUserSettings == null) {
			lListboxUserSettings = new HashMap<String,Object>();
			setAccSessAttr(AppConstants.listbox_user_settings, new HashMap<String,Object>()); 
		}
		lListboxUserSettings.put(aKey, aValue);
	}

	/**
	 * Gets the account sess attr.
	 *
	 * @param aKey the key
	 *
	 * @return the account sess attr
	 */
	public Object getGridUserSetting(String aKey) {
		Map<String,Object> lGridUserSettings = (Map<String,Object>)getAccSessAttr(AppConstants.grid_user_settings);
		if (lGridUserSettings != null && lGridUserSettings.containsKey(aKey)) {
			return lGridUserSettings.get(aKey);
		}
		return null;
	}

	/**
	 * Sets account sess attr.
	 *
	 * @param aKey the key
	 * @param aValue the value
	 */
	public void setGridUserSetting(String aKey, Object aValue) {
		Map<String,Object> lGridUserSettings = (Map<String,Object>)getAccSessAttr(AppConstants.grid_user_settings);
		if (lGridUserSettings == null) {
			lGridUserSettings = new HashMap<String,Object>();
			setAccSessAttr(AppConstants.grid_user_settings, new HashMap<String,Object>()); 
		}
		lGridUserSettings.put(aKey, aValue);
	}

}
