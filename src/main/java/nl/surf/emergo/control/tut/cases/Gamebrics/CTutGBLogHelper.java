/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.tut.cases.Gamebrics;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.IXMLAttributeValueTime;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CCaseHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.tut.CTutRunGroupAccountsHelper;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunGroupAccount;

/**
 * The Class CTutGBLogHelper.
 */
public class CTutGBLogHelper extends CTutRunGroupAccountsHelper {
	
	protected CTutGBLogWnd root = (CTutGBLogWnd)CDesktopComponents.vView().getComponent(CTutGBLogWnd.wndWndId);
	
	protected CCaseHelper caseHelper = new CCaseHelper();
	
	protected List<String> studentResults = new ArrayList<String>();
	//not needed?:
	protected List<String> studentResultTimes = new ArrayList<String>();
	
	//NOTE to be subtracted from total time spent in game:
	protected double timeSpentInAssessments = 0;
	
	//NOTE amount of time that player spent in game:
	protected long gameFinishedTime = 0;
	
	//NOTE assessment Excel headers need to be set only one time
	protected boolean assessmentHeadersSet = false;
	
	private static final Logger _log = LogManager.getLogger(CTutGBLogLb.class);

	public CTutGBLogHelper() {
		super();
	}

	protected void addToAssessmentsHeaders(IECaseComponent pAssessmentsCaseComponent, IXMLTag pAssessmentTag) {
		List<String> lAssNames = (List<String>)root.xcelAssHeaderInfo.get(CTutGBLogWnd.xcelAssHeaderId);
		if (lAssNames == null)
			lAssNames = new ArrayList<String>();
		
		//NOTE assessments all seem to have the same name, so also add case component name
		lAssNames.add(pAssessmentsCaseComponent.getName() + " - " + pAssessmentTag.getChildValue("name"));
		root.xcelAssHeaderInfo.put(CTutGBLogWnd.xcelAssHeaderId, lAssNames);
	}

	protected List<IXMLAttributeValueTime> getAttributeValueTimes(IXMLTag tag, String attributeKey, boolean includeDefaultValues) {				
		List<IXMLAttributeValueTime> attributeValueTimes = tag.getStatusAttribute(attributeKey);
		if (includeDefaultValues) {
			if (attributeValueTimes == null || attributeValueTimes.size() == 0) {
				attributeValueTimes = tag.getChildAttributeAsList(AppConstants.statusElement, attributeKey);
			}
			if (attributeValueTimes == null || attributeValueTimes.size() == 0) {
				if (tag.getDefTag() != null) {
					attributeValueTimes = tag.getDefTag().getChildAttributeAsList(AppConstants.statusElement, attributeKey);
				}
			}
		}
		return attributeValueTimes;
	}
	
	protected List<Double> getAssessmentTimeInterval(IECaseComponent pAssessmentsCaseComponent, List<IXMLTag> nodeTags, IXMLTag pComponentTag, int pFinishedNumber, boolean pDuringGame) {
		List<Double> lTimes = new ArrayList<Double>();
		double lMaxTime = 0;
		List<Double> lFinishedTimes = new ArrayList<Double>();
		for (IXMLTag nodeTag : nodeTags) {
			//NOTE only one assessment in each Gamebrics asessments case component
			if (nodeTag.getName().equals("assessment")) {
				if (!assessmentHeadersSet)
					addToAssessmentsHeaders(pAssessmentsCaseComponent, nodeTag);
				List<IXMLAttributeValueTime> lFinishedStatus = nodeTag.getStatusAttribute(AppConstants.statusKeyFinished);
				int lCount = 0;
				for (IXMLAttributeValueTime lFSMoment : lFinishedStatus) {
					if (lFSMoment.getValue().equals(AppConstants.statusValueTrue)) {
						double lTmp = lFSMoment.getTime();
						lFinishedTimes.add(lTmp);
						lCount++;
						if (lCount == pFinishedNumber) {
							lMaxTime = lTmp;
							break;
						}
					}
				}
				break;
			}
		}
		//NOTE start status is set for component, not for assessment??
		double lMinTime = 0;
		if (pFinishedNumber > 1) {
			lMinTime = lFinishedTimes.get(pFinishedNumber - 2);
		}
		double lStartTime = lMaxTime;
		List<IXMLAttributeValueTime> lStartedStatus = pComponentTag.getStatusAttribute(AppConstants.statusKeyStarted);
		for (IXMLAttributeValueTime lFSMoment : lStartedStatus) {
			if (lFSMoment.getValue().equals(AppConstants.statusValueTrue)) {
				double lTime = lFSMoment.getTime();
				if ((lTime > lMinTime) && (lTime < lStartTime))
					lStartTime = lTime;
			}
		}
		lTimes.add(lStartTime);
		lTimes.add(lMaxTime);
		if ((pDuringGame) && (lMaxTime > lStartTime))
			timeSpentInAssessments += (lMaxTime - lStartTime);
		if (!pDuringGame) {
			//NOTE only 1 assessment after game
			gameFinishedTime = Math.round(lStartTime - timeSpentInAssessments);
		}
		return lTimes;
	}
	
	protected List<String> getAnswer(IXMLTag alternativeTag) {
		List<String> lAnswerInfo = new ArrayList<String>();
		String lAnswerId = alternativeTag.getChildValue("pid");
		String lText = root.sSpring.unescapeXMLAlt(alternativeTag.getChildValue("richtext"));
		String lAnswer = lAnswerId.substring(lAnswerId.lastIndexOf("-") + 1);
		int lAnswerInt = 0;
		try {
			lAnswerInt = Integer.parseInt(lAnswer);
			if (!StringUtils.isEmpty(lText))
				lAnswerId += " - " + lText;
		} catch (NumberFormatException e) {
			lAnswer = lText;
		}
		lAnswerInfo.add(lAnswerId);
		lAnswerInfo.add(lAnswer);
		return lAnswerInfo;
	}

	protected List<String> getFieldAnswer(IXMLTag pFieldTag) {
		List<String> lAnswerInfo = new ArrayList<String>();
		String lAnswerId = pFieldTag.getChildValue("pid");
		//NOTE no history is kept, only one value
		String lText = root.sSpring.unescapeXMLAlt(root.sSpring.getCurrentTagStatus(pFieldTag, AppConstants.statusKeyAnswer));
		lAnswerInfo.add(lAnswerId);
		lAnswerInfo.add(lText);
		return lAnswerInfo;
	}

	protected void addAssessmentStudentResults(IECaseComponent pAssessmentsCaseComponent, IERunGroupAccount pRugAc, int pFinishedNumber, boolean pDuringGame) {
		IXMLTag lComponentTag = root.sSpring.getComponentDataStatusTag(pRugAc.getERunGroup().getRugId(), pAssessmentsCaseComponent);
		IXMLTag rootTag = root.sSpring.getXmlDataPlusRunGroupStatusTree(pRugAc.getERunGroup().getRugId(), pAssessmentsCaseComponent);
		List<IXMLTag> nodeTags = CDesktopComponents.cScript().getNodeTags(rootTag);
		List<Double> lStartStopTimes = getAssessmentTimeInterval(pAssessmentsCaseComponent, nodeTags, lComponentTag, pFinishedNumber, pDuringGame);
		IECaseRole lCaseRole = root.sSpring.getSCaseRoleHelper().getCaseRole();
		_log.info("rendering assessment: {}", pAssessmentsCaseComponent.getName());
		List<List<String>> lAssItemNamesList = (List<List<String>>)root.xcelAssHeaderInfo.get(CTutGBLogWnd.xcelAssItemHeaderId);
		if (lAssItemNamesList == null)
			lAssItemNamesList = new ArrayList<List<String>>();
		List<String> lAssItemNames = new ArrayList<String>();
		for (IXMLTag nodeTag : nodeTags) {
			if (nodeTag.getName().equals("refitem")) {
				double lMinTime = -1;
				String itemAlternativeTagId = "";
				List<IXMLAttributeValueTime> lAnswersStatus = nodeTag.getStatusAttribute(AppConstants.statusKeyAnswer);
				for (IXMLAttributeValueTime lAnswerStatus : lAnswersStatus) {
					if ((lAnswerStatus.getTime() >= lStartStopTimes.get(0)) && (lAnswerStatus.getTime() >= lMinTime) && (lAnswerStatus.getTime() <= lStartStopTimes.get(1)) && !StringUtils.isEmpty(lAnswerStatus.getValue())) {
						//NOTE when selecting other multiple choice alternative, first answer is cleared, at same time as setting new selection
						lMinTime = lAnswerStatus.getTime();
						itemAlternativeTagId = lAnswerStatus.getValue();
					}
				}

				List<String> lAnswerInfo = new ArrayList<String>();
				if (!StringUtils.isEmpty(itemAlternativeTagId)) {
					_log.info("rendering answer for question: {}", nodeTag.getChildValue("pid"));
					String reftype = nodeTag.getChild("ref").getDefTag().getAttribute(AppConstants.defKeyReftype);
					List<String> refIds = caseHelper.getRefTagIds(reftype, "" + AppConstants.statusKeySelectedIndex, lCaseRole, pAssessmentsCaseComponent, nodeTag);
					if (!(refIds == null) && (refIds.size() > 0)) {
						String refId = refIds.get(0);
						if (!StringUtils.isEmpty(refId)) {
							String[] idArr = refId.split(",");
							if (idArr.length == 3) {
								String cacId = idArr[1];
								String tagId = idArr[2];
								IXMLTag itemTag = root.sSpring.getTag(root.sSpring.getCaseComponent(Integer.parseInt(cacId)), tagId);
								if (itemTag != null) {
									for (IXMLTag tempAlternativeTag : itemTag.getChilds("alternative")) {
										if (tempAlternativeTag.getAttribute(AppConstants.keyId).equals(itemAlternativeTagId)) {
											lAnswerInfo = getAnswer(tempAlternativeTag);
											break;
										}
									}
									if (lAnswerInfo.size() == 0)
										//NOTE try text entry question
										for (IXMLTag tempFieldTag : itemTag.getChilds("field")) {
											if (tempFieldTag.getAttribute(AppConstants.keyId).equals(itemAlternativeTagId)) {
												lAnswerInfo = getFieldAnswer(tempFieldTag);
												break;
											}
										}
								}
							}
						}
					} else {
						_log.info("no reference found for answer tag id : {}", itemAlternativeTagId);
						refIds = caseHelper.getRefTagIds(reftype, "" + AppConstants.statusKeySelectedIndex, lCaseRole, pAssessmentsCaseComponent, nodeTag);
					}
				}
				if (lAnswerInfo.isEmpty()) {
					lAnswerInfo.add("");
					lAnswerInfo.add("");
				}
				
				studentResults.add(lAnswerInfo.get(0));
				studentResults.add(lAnswerInfo.get(1));
				
				if (!assessmentHeadersSet) {
					String reftype = nodeTag.getChild("ref").getDefTag().getAttribute(AppConstants.defKeyReftype);
					List<String> refIds = caseHelper.getRefTagIds(reftype, "" + AppConstants.statusKeySelectedIndex, lCaseRole, pAssessmentsCaseComponent, nodeTag);
					String refId = refIds.get(0);
					if (!StringUtils.isEmpty(refId)) {
						String[] idArr = refId.split(",");
						if (idArr.length == 3) {
							String cacId = idArr[1];
							String tagId = idArr[2];
							IXMLTag itemTag = root.sSpring.getTag(root.sSpring.getCaseComponent(Integer.parseInt(cacId)), tagId);
							if (itemTag != null) {
								lAssItemNames.add(itemTag.getChildValue("pid") + " - " + root.sSpring.unescapeXMLAlt(itemTag.getChildValue("richtext")));
							}
						}
					}
					
				}

			}
		}
		if (!assessmentHeadersSet) {
			lAssItemNamesList.add(lAssItemNames);
			root.xcelAssHeaderInfo.put(CTutGBLogWnd.xcelAssItemHeaderId, lAssItemNamesList);
		}
	}

	//NOTE overwrites renderItem of CTutRunGroupAccountsHelper
	//It will not render the item but generate data for the item to be rendered later on
	public void renderItem(Listbox aListbox, Listitem aInsertBefore, Hashtable<String,Object> hItem, String typeOfOverview) {
		root.sSpring.clearRunBoostProps();
		caseHelper = new CCaseHelper();
		root.sSpring.setRun((IERun)hItem.get("run"));
		root.sSpring.setRunStatus(AppConstants.runStatusRun);
		List<IERunGroupAccount> runGroupAccounts = (List<IERunGroupAccount>)hItem.get("rungroupaccounts");
		//NOTE only one
		IERunGroupAccount runGroupAccount = runGroupAccounts.get(0);
		root.sSpring.setRunGroupAccount(runGroupAccount);
		hItem.put("rungroupaccount", runGroupAccount);
		
		_log.info("rendering student: {}", runGroupAccount.getEAccount().getLastname());
		
		studentResults.clear();
		studentResultTimes.clear();
		timeSpentInAssessments = 0;
		gameFinishedTime = 0;
		IECaseComponent lPrePostAssessmentsCaseComponent = (IECaseComponent)hItem.get("prepostassessmentscasecomponent");
		List<IECaseComponent> lOtherAssessmentsCaseComponents = (List<IECaseComponent>)hItem.get("otherassessmentscasecomponents");
		//NOTE pre assessment
		addAssessmentStudentResults(lPrePostAssessmentsCaseComponent, runGroupAccount, 1, true);
		for (IECaseComponent lAssCC : lOtherAssessmentsCaseComponents) {
			addAssessmentStudentResults(lAssCC, runGroupAccount, 1, true);
		}
		//NOTE post assessment
		addAssessmentStudentResults(lPrePostAssessmentsCaseComponent, runGroupAccount, 2, false);
		
		assessmentHeadersSet = true;

		generateSubItem(
				hItem, 
				true,
				true,
				typeOfOverview);
		
		//NOTE clear clear cached data of last run group account
		root.sSpring.setRunGroupAccount(null);
		//NOTE clear run status, so run group account data will no longer be cached within SSpring
		root.sSpring.setRunStatus("");
		
	}

	protected void generateSubItem(Hashtable<String,Object> hItem, boolean includeRunData, boolean includeStudentData, String typeOfOverview) {
		IEAccount account = (IEAccount)hItem.get("item");
		IERun run = (IERun)hItem.get("run");
		IERunGroupAccount runGroupAccount = (IERunGroupAccount)hItem.get("rungroupaccount");
		List<String> itemResult = new ArrayList<String>();
		if (includeRunData) {
			itemResult.add(run.getCode());
			itemResult.add(run.getName());
		}
		itemResult.add("" + runGroupAccount.getRgaId());
		itemResult.add("" + (runGroupAccount.getERunGroup().getActive() && runGroupAccount.getEAccount().getActive()));
		if (includeStudentData) {
			itemResult.add(account.getUserid());
			itemResult.add(account.getStudentid());
			itemResult.add(account.getTitle());
			itemResult.add(account.getInitials());
			itemResult.add(account.getNameprefix());
			itemResult.add(account.getLastname());
			itemResult.add(account.getEmail());
		}
		for (String lStudentResult: studentResults) {
			itemResult.add(lStudentResult);
		}
		itemResult.add("" + gameFinishedTime);
		
		root.queryResult.add(itemResult);
	}

}
