/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.tut;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.Pattern;

import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.IXMLAttributeValueTime;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.view.VView;

/**
 * The Class CTutRunGroupAccountOwnOverviewHelper.
 */
public class CTutRunGroupAccountOwnOverviewHelper extends CTutRunGroupAccountsHelper {
	
	protected CTutRunGroupAccountOwnOverviewWnd root = (CTutRunGroupAccountOwnOverviewWnd)CDesktopComponents.vView().getComponent("ownOverviewWnd");
	
	public CTutRunGroupAccountOwnOverviewHelper() {
		super();
	}

	protected List<IXMLAttributeValueTime> getAttributeValueTimes(IXMLTag tag, String attributeKey, boolean includeDefaultValues) {				
		List<IXMLAttributeValueTime> attributeValueTimes = tag.getStatusAttribute(attributeKey);
		if (includeDefaultValues) {
			if (attributeValueTimes == null || attributeValueTimes.size() == 0) {
				attributeValueTimes = tag.getChildAttributeAsList(AppConstants.statusElement, attributeKey);
			}
			if (attributeValueTimes == null || attributeValueTimes.size() == 0) {
				if (tag.getDefTag() != null) {
					attributeValueTimes = tag.getDefTag().getChildAttributeAsList(AppConstants.statusElement, attributeKey);
				}
			}
		}
		return attributeValueTimes;
	}

	//NOTE overwrites renderItem of CTutRunGroupAccountsHelper
	//It will not render the item but generate data for the item to be rendered later on
	public void renderItem(Listbox aListbox, Listitem aInsertBefore, Hashtable<String,Object> hItem, String typeOfOverview) {
		List<IECaseComponent> tutCaseComponents = root.getSelectedCaseComponents();
		List<String> tutTagNames = (List<String>)root.cControl.getAccSessAttr("tutTagNames");
		String tutTagKeyTemplate = (String)root.cControl.getAccSessAttr("tutTagKeyTemplate");
		List<String> tutAttributeKeys = (List<String>)root.cControl.getAccSessAttr("tutAttributeKeys");
		String tutAttributeValueTemplate = (String)root.cControl.getAccSessAttr("tutAttributeValueTemplate");
		boolean generate = tutCaseComponents != null && tutCaseComponents.size() > 0 && tutAttributeKeys != null && tutAttributeKeys.size() > 0;
		if (!generate) {
			return;
		}
		boolean showTags = tutTagNames != null && tutTagNames.size() > 0 && tutTagKeyTemplate != null && !tutTagKeyTemplate.equals("");  
		boolean componentAndTagDataCombined = ((Checkbox)root.vView.getComponent("componentAndTagDataCombined")).isChecked();
		boolean includeHistory = ((Checkbox)root.vView.getComponent("includeHistory")).isChecked();
		boolean includeDefaultValues = ((Checkbox)root.vView.getComponent("includeDefaultValues")).isChecked();
		boolean includeRunData = ((Checkbox)root.vView.getComponent("includeRunData")).isChecked();
		boolean includeStudentData = ((Checkbox)root.vView.getComponent("includeStudentData")).isChecked();
		boolean attributeValueConvertPointsToCommas = ((Checkbox)root.vView.getComponent("attributeValueConvertPointsToCommas")).isChecked() &&
				(typeOfOverview.equals("dataInListbox") || typeOfOverview.equals("dataAsText"));
		String[] tutTagKeyTemplates = new String[0];
		if (showTags) {
			tutTagKeyTemplates = tutTagKeyTemplate.split(",");
		}
		String[] tutAttributeValueTemplates = new String[0];
		if (tutAttributeValueTemplate != null && !tutAttributeValueTemplate.equals("")) {
			tutAttributeValueTemplates = tutAttributeValueTemplate.split(",");
		}

		//NOTE merging of data and status is not the bottleneck. It takes only a few millisecs per rungroupcasecomponent.

		//NOTE set run status, so run group account data will be cached within SSpring
		//TODO setting run status seems to have no effect, so remove?
		root.sSpring.setRunStatus(AppConstants.runStatusRun);
		List<IERunGroupAccount> runGroupAccounts = (List<IERunGroupAccount>)hItem.get("rungroupaccounts");
		for (IERunGroupAccount runGroupAccount : runGroupAccounts) {
			hItem.put("rungroupaccount", runGroupAccount);
			//NOTE setting rungroupaccount will clear cached run group account data
			root.sSpring.setRunGroupAccount(runGroupAccount);
			for (IECaseComponent caseComponent : tutCaseComponents) {
				if (componentAndTagDataCombined || !showTags) {
					IXMLTag tag = root.sSpring.getComponentDataStatusTag(caseComponent, AppConstants.statusTypeRunGroup);
					if (tag != null) {
						for (String key : tutAttributeKeys) {
							generateSubItems(aListbox, aInsertBefore, caseComponent, hItem, tag, key, "", "", tutAttributeValueTemplates, includeDefaultValues, includeHistory, includeRunData, includeStudentData, attributeValueConvertPointsToCommas, typeOfOverview);
						}
					}
				}
				if (showTags) {
					for (int i=0;i<tutTagKeyTemplates.length;i++) {
						Pattern p = Pattern.compile(tutTagKeyTemplates[i], Pattern.DOTALL);
						for (String tagName : tutTagNames) {
							List<IXMLTag> tags = root.cScript.getRunGroupNodeTags(caseComponent, tagName);
							if (tags != null) {
								for (IXMLTag tag : tags) {
									String tagKeyValue = root.sSpring.getXmlManager().getTagKeyValues(tag, tag.getDefAttribute(AppConstants.defKeyKey));
									if (p.matcher(tagKeyValue).matches()) {
										for (String key : tutAttributeKeys) {
											generateSubItems(aListbox, aInsertBefore, caseComponent, hItem, tag, key, tag.getName(), tagKeyValue, tutAttributeValueTemplates, includeDefaultValues, includeHistory, includeRunData, includeStudentData, attributeValueConvertPointsToCommas, typeOfOverview);
										}
									}
								}
							}
						}
					}
				}
			}
		}
		//NOTE clear clear cached data of last run group account
		root.sSpring.setRunGroupAccount(null);
		//NOTE clear run status, so run group account data will no longer be cached within SSpring
		root.sSpring.setRunStatus("");
	}

	protected void generateSubItems(Listbox aListbox, Listitem aInsertBefore, IECaseComponent caseComponent, Hashtable<String,Object> hItem, 
			IXMLTag tag, String key, String tagName, String tagKeyValue, String[]attributeValueTemplates, boolean includeDefaultValues, boolean includeHistory, boolean includeRunData, boolean includeStudentData, boolean attributeValueConvertPointsToCommas, String typeOfOverview) {
		String tempKey = "";
		try {
			int number = Integer.parseInt(key);
			tempKey = root.sSpring.getAppManager().getStatusKey(number);
		} catch (Exception e) {
			tempKey = key;
		}
		List<IXMLAttributeValueTime> attributeValueTimes = getAttributeValueTimes(tag, tempKey, includeDefaultValues);
		if (attributeValueTimes != null && attributeValueTimes.size() > 0) {
			int start = 0;
			if (!includeHistory) {
				start = attributeValueTimes.size() - 1;
			}
			for (int i=start;i<attributeValueTimes.size();i++) {
				if (includeDefaultValues || attributeValueTimes.get(i).getTime() >= 0) {
					String attributeValue = attributeValueTimes.get(i).getValue();
					boolean generate = true;
					if (attributeValueTemplates != null && attributeValueTemplates.length > 0) {
						generate = false;
						for (int j=0;j<attributeValueTemplates.length;j++) {
							Pattern p = Pattern.compile(attributeValueTemplates[j], Pattern.DOTALL);
							if (p.matcher(attributeValue).matches()) {
								generate = true;
								break;
							}
						}
					}
					if (generate) {
						if (attributeValueConvertPointsToCommas) {
							attributeValue = attributeValue.replaceAll("\\.", ",");
						}
						String cacName = caseComponent.getName();
						if (root.areMultipleCases) {
							cacName += " - " + caseComponent.getECase().getName() + " (" + caseComponent.getECase().getVersion() + ")";
						}
						generateSubItem(
								aListbox, 
								aInsertBefore, 
								hItem, 
								cacName,
								tagName,
								tagKeyValue,
								key,
								attributeValue,
								attributeValueTimes.get(i).getTime(),
								includeRunData,
								includeStudentData,
								typeOfOverview);
					}
				}
			}
		}
	}

	protected void generateSubItem(Listbox aListbox, Listitem aInsertBefore, Hashtable<String,Object> hItem, String cacName, String tagName, String tagKeyValue, String key, String value, double time, boolean includeRunData, boolean includeStudentData, String typeOfOverview) {
		IEAccount account = (IEAccount)hItem.get("item");
		IERun run = (IERun)hItem.get("run");
		IERunGroupAccount runGroupAccount = (IERunGroupAccount)hItem.get("rungroupaccount");
		//NOTE time is secs with millisecs as three decimals. Multiply by 1000 to get time in millisecs
		long longTime = Math.round(time * 1000);
		//NOTE time value -1.0 indicates a predefined value either in the XML definition or by a case developer.
		//longTime then will become -1000. Set it to -1.
		if (longTime < 0) {
			longTime = -1;
		}
		List<String> itemResult = new ArrayList<String>();
		if (includeRunData) {
			itemResult.add(run.getCode());
			itemResult.add(run.getName());
		}
		itemResult.add("" + runGroupAccount.getRgaId());
		itemResult.add("" + (runGroupAccount.getERunGroup().getActive() && runGroupAccount.getEAccount().getActive()));
		if (includeStudentData) {
			itemResult.add(account.getUserid());
			itemResult.add(account.getStudentid());
			itemResult.add(account.getTitle());
			itemResult.add(account.getInitials());
			itemResult.add(account.getNameprefix());
			itemResult.add(account.getLastname());
			itemResult.add(account.getEmail());
		}
		itemResult.add(cacName);
		itemResult.add(tagName);
		itemResult.add(tagKeyValue);
		String keyStr = root.vView.getLabel(VView.statuskeyLabelKeyPrefix + key);
		if (keyStr.equals("")) {
			keyStr = key;
		}
		itemResult.add(keyStr);
		itemResult.add(value);
		itemResult.add("" + longTime);
		root.queryResult.add(itemResult);
	}

}
