/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.tut.cases.Sexology;

import java.util.List;
import java.util.Set;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.zkoss.zul.Auxhead;
import org.zkoss.zul.Auxheader;
import org.zkoss.zul.Column;
import org.zkoss.zul.Columns;
import org.zkoss.zul.Label;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;

import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.def.CDefGrid;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.zkspring.SSpring;

public class CTutRunsSxMonitorGrid extends CDefGrid {

	private static final long serialVersionUID = -807739730333656304L;

	protected static final String gStateCompName = "status verplicht afgerond";
	protected static final String gYes = "ja";
	protected static final String gNo = "nee";
	protected static final String gInapp = "n.v.t.";
	protected int gPageSize = 20;
	protected Hashtable gCaseNames = new Hashtable();
	protected Hashtable gAccsInfo = new Hashtable();

	protected SSpring sSpring = CDesktopComponents.sSpring();
	
	public void onCreate() {
		System.out.println("voor runs: " + (new Date()).toString());
		CTutRunsSxMonitorHelper gTutSekRunsHelper = new CTutRunsSxMonitorHelper();
		List<IERun> gSekRuns = gTutSekRunsHelper.getRuns();
		System.out.println("na runs: " + (new Date()).toString());

		if (gSekRuns.isEmpty()) {
			gTutSekRunsHelper.toRegularOverview();
		}

		CTutRunsSxDSqlQuery lSqlQ = new CTutRunsSxDSqlQuery();
		for (IERun run : gSekRuns) {
			sSpring.clearRunBoostProps();

			IECase lCase = run.getECase();
			int lCasId = lCase.getCasId();
			if (!gCaseNames.containsKey(lCasId))
				gCaseNames.put(lCasId, lCase.getName());

			int lStatCompId = -1;
			ResultSet lSqlRes1 = lSqlQ.getSqlRes("SELECT c1.cacid FROM casecomponents c1 " +
					"where c1.name='" + gStateCompName + "' and c1.cascasid=" + lCasId + ";");
			try {
				while (lSqlRes1.next()) {
					//only 1 casecomponent should exist
					lStatCompId = lSqlRes1.getInt(1);
				}
			} catch (SQLException lEx) {
				lEx.printStackTrace();
			}
			lSqlQ.closeConnection();
			if (lStatCompId > -1) {

				int lRunId = run.getRunId();
				System.out.println("vóór casecasecomponent: " + (new Date()).toString());

				sSpring.setRun(run);
				sSpring.setRunStatus(AppConstants.runStatusRun);

				int lCasCompId = -1;
				ResultSet lSqlRes2 = lSqlQ.getSqlRes("SELECT c1.cacid FROM casecomponents c1 " +
						"inner join components c2 on c2.code='case' and c1.cascasid=" + lCasId + " and c1.comcomid=c2.comid;");
				try {
					while (lSqlRes2.next()) {
						//only 1 casecomponent should exist
						lCasCompId = lSqlRes2.getInt(1);
					}
				} catch (SQLException lEx) {
					lEx.printStackTrace();
				}
				lSqlQ.closeConnection();

				ResultSet lSqlRes = lSqlQ.getSqlRes("SELECT r3.rgaid, r2.caccacid, r2.xmldata, r2.creationdate, r2.lastupdatedate, r4.accid, " +
						"r4.userid, r4.title, r4.initials, r4.nameprefix, r4.lastname, r4.email, r4.studentid FROM rungroups r1 " +
						"inner join rungroupcasecomponents r2 on r1.runrunid=" + lRunId + " and r2.rugrugid=r1.rugid and (r2.caccacid=" + lCasCompId +
						" or r2.caccacid=" + lStatCompId + ") " +
						"inner join rungroupaccounts r3 on r1.rugid=r3.rugrugid " +
						"inner join accounts r4 on r3.accaccid=r4.accid;");
				Hashtable lCasCompInfo = new Hashtable();
				Hashtable lStatCompInfo = new Hashtable();
				Hashtable lRgaInfo = new Hashtable();
				try {
					while (lSqlRes.next()) {
						int lRgaId = lSqlRes.getInt(1);
						int lCasComId = lSqlRes.getInt(2);
						String lXmlData = lSqlRes.getString(3);
						Date lCDate = lSqlRes.getDate(4);
						Date lUDate = lSqlRes.getDate(5);
						Hashtable lCompValue = new Hashtable();
						if (lCasComId == lCasCompId) {
							lCompValue.put(0, lCasComId);
							lCompValue.put(1, lCDate);
							lCompValue.put(2, lUDate);
							lCasCompInfo.put(lRgaId, lCompValue);
						} else {
							lCompValue.put(0, lCasComId);
							lCompValue.put(1, lXmlData);
							lCompValue.put(2, lCDate);
							lCompValue.put(3, lUDate);
							lStatCompInfo.put(lRgaId, lCompValue);
						}
						if (!lRgaInfo.containsKey(lRgaId)) {
							Hashtable lAccInfo = new Hashtable();
							lAccInfo.put("accountid", lSqlRes.getString(6));
							lAccInfo.put("userid", lSqlRes.getString(7));
							lAccInfo.put("title", lSqlRes.getString(8));
							lAccInfo.put("initials", lSqlRes.getString(9));
							lAccInfo.put("nameprefix", lSqlRes.getString(10));
							lAccInfo.put("lastname", lSqlRes.getString(11));
							lAccInfo.put("email", lSqlRes.getString(12));
							lAccInfo.put("studentid", lSqlRes.getString(13));
							lRgaInfo.put(lRgaId, lAccInfo);
						}
					}
				} catch (SQLException lEx) {
					lEx.printStackTrace();
				}
				lSqlQ.closeConnection();
				System.out.println("na sql-loop: " + (new Date()).toString());

				System.out.println("voor rga-loop: " + (new Date()).toString());

				Enumeration lRgaKeys = lRgaInfo.keys();
				while (lRgaKeys.hasMoreElements()) {
					int lRgaId = (int)lRgaKeys.nextElement();
					Hashtable lCasCompValue = (Hashtable)lCasCompInfo.get(lRgaId);
					Hashtable lStatCompValue = (Hashtable)lStatCompInfo.get(lRgaId);
					if (!((lCasCompValue == null) || (lStatCompValue == null))) {
						Hashtable lAccInfo = (Hashtable)lRgaInfo.get(lRgaId);
						String userid = (String)lAccInfo.get("userid");
						Date lSDate = (Date)lCasCompValue.get(1);
						//legacy:
						Date lCaseLastUpdate = (Date)lCasCompValue.get(2);
//						System.out.println(lRugId + " " + name);
						if (gAccsInfo.get(userid) == null) {
							lAccInfo.put("startdate", lSDate);
							lAccInfo.put("finished", true); //NOTE set to false at first case that is not finished
							gAccsInfo.put(userid, lAccInfo);
						} else {
							lAccInfo = (Hashtable)gAccsInfo.get(userid);
							if (((Date)lAccInfo.get("startdate")).after(lSDate)) {
								lAccInfo.put("startdate", lSDate);
								gAccsInfo.put(userid, lAccInfo);
							}
						}

						boolean lFinished = false;
						boolean lAdapt = false;
						boolean lExists = false;
						Date lDate = new Date();
						int lFinishedItems = 0;
						Hashtable lCasInfo = new Hashtable();
						if (lAccInfo.get(lCasId) == null) {
							lCasInfo.put("finisheditems", lFinishedItems);
							lCasInfo.put("gamefinished", lFinished);
							lAdapt = true;
						} else {
							lCasInfo = (Hashtable)lAccInfo.get(lCasId);
							lExists = true;
						}
						String lXml = (String)lStatCompValue.get(1);
						if (!lXml.equals(AppConstants.emptyXml)) {
							String lXml2 = lXml.substring(lXml.indexOf("value=\"") + 7);
							String lAfgerondNr = "";
							if (!lXml2.equals(lXml)) {
								String[] lVal1 = (lXml2.substring(0, lXml2.indexOf("\""))).split(",");
								if (!lVal1[lVal1.length - 1].equals("0.0")) {
									lAfgerondNr = lVal1[lVal1.length - 2];
								} else {
									//handled by legacy inspector; no time value stored; first value is legacy counter;
									//always 1 greater than finished items counter
									//lAfgerondNr = (Integer.parseInt(lVal1[lVal1.length - 2]) - 1).toString();
									lAfgerondNr = (Integer.parseInt(lVal1[lVal1.length - 2]) - 1) + "";
								}
							}

							if (!(lAfgerondNr == null || lAfgerondNr.equals(""))) {
								lFinishedItems = Integer.parseInt(lAfgerondNr);
								if (lFinishedItems > (int)lCasInfo.get("finisheditems")) {
									lAdapt = true;
								}
								lFinished = lXml.contains("true");
								if (lFinished) {
									lDate = (Date) lStatCompValue.get(3);
									//legacy: if finished determined with legacy check code, then use case last update date
									if (lDate.after(lCaseLastUpdate))
										lDate = lCaseLastUpdate;
									if ((boolean)lCasInfo.get("gamefinished")) {
										//compare date, when already finished in other run for this case
										if (lDate.after((Date)lCasInfo.get("finisheddate")))
											lAdapt = true;
									}
								}
							}
						}
						if (lAdapt) {
							lCasInfo.put("finisheditems", lFinishedItems);
							if (lFinished) {
								if (!(boolean)lCasInfo.get("gamefinished")) {
									lCasInfo.put("gamefinished", lFinished);
									if (lExists) {
										//could be student did not finish in earlier run for this case
										//now we have to test overall student activity
										boolean lOverallFinished = true;
										Enumeration lCasIds = gCaseNames.keys();
										while (lCasIds.hasMoreElements()) {
											int lTmpCasId = (int)lCasIds.nextElement();
											if (lTmpCasId != lCasId) {
												if (lAccInfo.get(lTmpCasId) == null) {
													lOverallFinished = false;
												} else {
													Hashtable lTmpCasInfo = (Hashtable)lAccInfo.get(lTmpCasId);
													if (!(boolean)lTmpCasInfo.get("gamefinished"))
														lOverallFinished = false;
												}
											}
										}
										if (lOverallFinished) {
											//restore finished state
											lAccInfo.put("finished", true);
										}
									}
								}
								lCasInfo.put("finisheddate", lDate);
								if ((boolean)lAccInfo.get("finished")) {
									if ((lAccInfo.get("finisheddate") == null) || ((Date)lAccInfo.get("finisheddate")).before(lDate))
										lAccInfo.put("finisheddate", lDate);
								}
							} else {
								if ((boolean)lAccInfo.get("finished")) {
									lAccInfo.put("finished", false);
								}
							}
							lAccInfo.put(lCasId, lCasInfo);
							gAccsInfo.put(userid, lAccInfo);
						}

					}
				}

				System.out.println("na rga-loop: " + (new Date()).toString());

			}
		}
		System.out.println("klaar: " + (new Date()).toString());
		renderGrid(gPageSize);
	}

	public void renderGrid(int aPageSize) {
		List lChildren = this.getChildren();
		Auxhead lAuhead = null;
		Columns lColumns = null;
		Rows lRows = null;
		for (Object lChild : lChildren) {
			if (lChild.getClass().getName().contains("Auxhead")) {
				lAuhead = (Auxhead)lChild;
				lAuhead.getChildren().clear();
			}
			if (lChild.getClass().getName().contains("Columns")) {
				lColumns = (Columns)lChild;
				lColumns.getChildren().clear();
			}
			if (lChild.getClass().getName().contains("Rows")) {
				lRows = (Rows)lChild;
				lRows.getChildren().clear();
			}
		}
		if (lAuhead != null) {
			this.removeChild(lAuhead);
		}
		if (lColumns != null) {
			this.removeChild(lColumns);
		}
		if (lRows != null) {
			this.removeChild(lRows);
		}
		this.setMold("paging");
		this.setPageSize(aPageSize);
		Column lSortCol = initHeaders(gCaseNames);
		renderStudents(gCaseNames.keySet(), gAccsInfo);
		lSortCol.sort(false);
	}

	private Column initHeaders (Hashtable aCaseNames) {
		Auxhead lAuxhead = new Auxhead();
		Auxheader lAH1 = new Auxheader();
		lAH1.setLabel("studentgegevens");
		lAH1.setColspan(4);
		lAuxhead.appendChild(lAH1);
		Auxheader lAH3 = new Auxheader();
		lAH3.setLabel("totaal afgerond");
		lAH3.setColspan(2);
		lAuxhead.appendChild(lAH3);
		Enumeration lCasIds = aCaseNames.keys();
		while (lCasIds.hasMoreElements()) {
			int lCasId = (int)lCasIds.nextElement();
			Auxheader lAH2 = new Auxheader();
			lAH2.setLabel((String)aCaseNames.get(lCasId));
			lAH2.setColspan(3);
			lAuxhead.appendChild(lAH2);
		}
		appendChild(lAuxhead);
		Columns lCols = new Columns();
		Column lCol1 = new Column();
		lCol1.setLabel("studentnummer");
		try {
			lCol1.setSort("auto");
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		lCols.appendChild(lCol1);
		Column lCol2 = new Column();
		lCol2.setLabel("naam");
		try {
			lCol2.setSort("auto");
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		lCols.appendChild(lCol2);
		Column lCol3 = new Column();
		lCol3.setLabel("e-mail");
		try {
			lCol3.setSort("auto");
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		lCols.appendChild(lCol3);
		Column lCol4 = new Column();
		lCol4.setLabel("startdatum");
		try {
			lCol4.setSort("auto");
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		lCols.appendChild(lCol4);
		Column lCol5 = new Column();
		lCol5.setLabel("afgerond");
		try {
			lCol5.setSort("auto");
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		lCols.appendChild(lCol5);
		Column lCol6 = new Column();
		lCol6.setLabel("einddatum");
		try {
			lCol6.setSort("auto");
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		lCols.appendChild(lCol6);
		lCasIds = aCaseNames.keys();
		while (lCasIds.hasMoreElements()) {
			int lCasId = (int)lCasIds.nextElement();
			Column lCol7 = new Column();
			lCol7.setLabel("afgerond");
			try {
				lCol7.setSort("auto");
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			lCols.appendChild(lCol7);
			Column lCol8 = new Column();
			lCol8.setLabel("einddatum");
			try {
				lCol8.setSort("auto");
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			lCols.appendChild(lCol8);
			Column lCol9 = new Column();
			lCol9.setLabel("aantal afgeronde onderdelen");
			try {
				lCol9.setSort("auto");
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			lCols.appendChild(lCol9);
		}
		appendChild(lCols);
		return lCol4;
	}

	private void renderStudents (Set aCaseKeys, Hashtable aStudentsList) {
		Rows lRows = new Rows();
		Enumeration lStuIds = aStudentsList.keys();
		while (lStuIds.hasMoreElements()) {
			String lStuId = (String)lStuIds.nextElement();
			renderStudent(lRows, aCaseKeys, lStuId, (Hashtable)aStudentsList.get(lStuId));
		}
		appendChild(lRows);
	}

	private String dateToString (Date aDate) {
		if (aDate == null)
			return gInapp;
		String lDate = "" + aDate;
		lDate = lDate.split(" ")[0];
		String[] lDateStr = lDate.split("-");
		lDate = lDateStr[0] + "-" + lDateStr[1] + "-" + lDateStr[2];
		return lDate;
	}

	private void renderStudent (Rows aRows, Set aCaseKeys, String aStuId, Hashtable aStudentInfo) {
		Row lRow = new Row();
		Label lL1 = new Label();
		lL1.setValue((String)aStudentInfo.get("studentid"));
		lRow.appendChild (lL1);
		String lStudentName = "";
		if (!((aStudentInfo.get("title") == null) || (((String)aStudentInfo.get("title")).equals("")))) {
			lStudentName = (String)aStudentInfo.get("title");
		}
		if (!((aStudentInfo.get("initials") == null) || (((String)aStudentInfo.get("initials")).equals("")))) {
			lStudentName = lStudentName.equals("") ? (String)aStudentInfo.get("initials") : lStudentName + " " + (String)aStudentInfo.get("initials");
		}
		if (!((aStudentInfo.get("nameprefix") == null) || (((String)aStudentInfo.get("nameprefix")).equals("")))) {
			lStudentName = lStudentName.equals("") ? (String)aStudentInfo.get("nameprefix") : lStudentName + " " + (String)aStudentInfo.get("nameprefix");
		}
		lStudentName = lStudentName.equals("") ? (String)aStudentInfo.get("lastname") : (String)aStudentInfo.get("lastname") + ", " + lStudentName;
		Label lL2 = new Label();
		lL2.setValue(lStudentName);
		lRow.appendChild (lL2);
		Label lL3 = new Label();
		lL3.setValue((String)aStudentInfo.get("email"));
		lRow.appendChild (lL3);
		Label lL4 = new Label();
		lL4.setValue(dateToString((Date)aStudentInfo.get("startdate")));
		lRow.appendChild (lL4);
		Label lL5 = new Label();
		lRow.appendChild (lL5);
		Label lL6 = new Label();
		lRow.appendChild (lL6);
		boolean lTotFinished = true;
		for (Object lCasIdStr: aCaseKeys) {
			int lCasId = (int)lCasIdStr;
			Label lL7 = new Label();
			lL7.setValue(gNo);
			Label lL8 = new Label();
			lL8.setValue(gInapp);
			Label lL9 = new Label();
			lL9.setValue("0");
			Hashtable lCasInfo = (Hashtable)aStudentInfo.get(lCasId);
			if (lCasInfo != null) {
				boolean lCasFinished = (boolean)lCasInfo.get("gamefinished");
				String lStr2 = lCasFinished ? gYes : gNo;
				if (!lCasFinished)
					lTotFinished = false;
				lL7.setValue(lStr2);
				lL8.setValue(dateToString((Date)lCasInfo.get("finisheddate")));
				lL9.setValue(((int)lCasInfo.get("finisheditems")) + "");
			} else
				lTotFinished = false;
			lRow.appendChild (lL7);
			lRow.appendChild (lL8);
			lRow.appendChild (lL9);
		}
		String lStr = lTotFinished ? gYes : gNo;
		lL5.setValue(lStr);
		String lL6Str = lStr.equals(gYes) ? dateToString((Date)aStudentInfo.get("finisheddate")) : gInapp;
		lL6.setValue(lL6Str);
		aRows.appendChild(lRow);
	}

}
