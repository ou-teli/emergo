/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.tut;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Include;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Timer;

import nl.surf.emergo.business.IRunGroupAccountManager;
import nl.surf.emergo.business.IRunGroupManager;
import nl.surf.emergo.business.IRunTeamManager;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.domain.IERunTeam;


/**
 * The Class CTutRunGroupAccountOwnOverviewLb.
 */
public class CTutRunGroupAccountOwnOverviewLb extends CTutRunGroupAccountsLb {

	//NOTE dataListbox is not only used to show data in a listbox but also to show data in other types of overviews
	//The reason is that dataListbox uses CTutRunGroupAccountOwnOverviewLb that extends from CTutRunGroupAccountsLb. The latter class is responsible for
	//rendering of all rungroupaccount data so already has methods for handling this.

	private static final long serialVersionUID = -7658696494387763756L;

	protected CTutRunGroupAccountOwnOverviewWnd root = (CTutRunGroupAccountOwnOverviewWnd)CDesktopComponents.vView().getComponent("ownOverviewWnd");

	protected List<Hashtable<String,Object>> runGroupAccountItems = null;
	protected CTutRunGroupAccountsHelper helper = null;
	protected int itemNumber = 0;

	protected boolean busy = false;
	protected boolean ready = false;

	protected CTutRunGroupAccountsHelper getHelper() {
		return new CTutRunGroupAccountOwnOverviewHelper();
	}

	public void onCreate() {
		//NOTE overwrite onCreate of super. Instead onGenerateData is called.
		restoreSettings();
	}

	public void onUpdate(Event event) {
		//NOTE onUpdate is sent if an overview has to be created or updated
		//initialize
		root.vView.getComponent("dataListbox").setVisible(false);
		root.vView.getComponent("dataInclude").setVisible(false);
		boolean includeRunData = ((Checkbox)root.vView.getComponent("includeRunData")).isChecked();
		boolean includeStudentData = ((Checkbox)root.vView.getComponent("includeStudentData")).isChecked();
		boolean includeDefaultValues = ((Checkbox)root.vView.getComponent("includeDefaultValues")).isChecked();
		if (root.typeOfOverview.equals("dataInListbox")) {
			//adjust listbox headers depending on query settings
			for (int i=1;i<=2;i++) {
				root.vView.getComponent("dataListboxRunData" + i).setVisible(includeRunData);
			}
			for (int i=1;i<=7;i++) {
				root.vView.getComponent("dataListboxStudentData" + i).setVisible(includeStudentData);
			}
			if (includeDefaultValues) {
				((Listheader)root.vView.getComponent("dataListboxTime")).setLabel(root.vView.getLabel("tut_rungroupaccountownoverview.time.defaultincluded"));
			}
			else {
				((Listheader)root.vView.getComponent("dataListboxTime")).setLabel(root.vView.getLabel("tut_rungroupaccountownoverview.time"));
			} 
		}
		root.queryResult = new ArrayList<List<String>>();
		//store excelFileName for later use in onReady, see below
		if (root.typeOfOverview.equals("dataInExistingExcelFile") || root.typeOfOverview.equals("loadQueriesAndDataInExistingExcelFile")) {
			setAttribute("excelFileName", event.getData());
		}
		//generate data to be shown in overview, data is stored in queryResult
		Events.postEvent("onGenerateData", this, null);
	}

	public void onReady() {
		//NOTE onReady is sent if all data needed for an overview has been gathered
		boolean includeRunData = ((Checkbox)root.vView.getComponent("includeRunData")).isChecked();
		boolean includeStudentData = ((Checkbox)root.vView.getComponent("includeStudentData")).isChecked();
		boolean includeDefaultValues = ((Checkbox)root.vView.getComponent("includeDefaultValues")).isChecked();
		//sort query result if needed
		root.queryResult = root.sortQueryResult(root.queryResult, ((Textbox)root.vView.getComponent("sortOnColumns")).getValue(), includeRunData, includeStudentData);
		if (root.typeOfOverview.equals("dataInListbox")) {
			//render list items, list headers are already existing
			getItems().clear();

			int numberOfRunColumns = 2;
			int numberOfStudentColumns = 7;

			for (List<String> result : root.queryResult) {
				Listitem listitem = new Listitem();
				appendChild(listitem);

				//NOTE if !includeRunData listcells have to be rendered because listheaders exist (although not visible), so add empty values.
				if (!includeRunData) {
					for (int i=1;i<=numberOfRunColumns;i++) {
						result.add(0, "");
					}
				}
				//NOTE add zero's to be able to properly sort on rgaid
				String rgaid = result.get(2);
				while (rgaid.length() < 12) {
					rgaid = "0" + rgaid;
				}
				result.set(2, rgaid);
				//NOTE if !includeStudentData listcells have to be rendered because listheaders exist (although not visible), so add empty values.
				if (!includeStudentData) {
					for (int i=1;i<=numberOfStudentColumns;i++) {
						result.add(4, "");
					}
				}
				//NOTE add zero's to be able to properly sort on time
				String time = result.get(result.size() - 1);
				while (time.length() < 12) {
					time = "0" + time;
				}
				result.set(result.size() - 1, time);

				for (String data : result) {
					listitem.appendChild(new Listcell(data));
				}
			}
			setVisible(true);
			root.queryResult = new ArrayList<List<String>>();
		}
		else {
			//add query headers
			root.queryResult = root.addQueryHeader(root.queryResult, includeRunData, includeStudentData, includeDefaultValues);
		}
		if (root.typeOfOverview.equals("dataAsText")) {
			//update text overview
			Executions.getCurrent().setAttribute("queryResult", root.queryResult);
			((Include)root.vView.getComponent("dataInclude")).setSrc("");
			((Include)root.vView.getComponent("dataInclude")).setSrc("tut_rungroupaccountownoverview.jsp");
			root.vView.getComponent("dataInclude").setVisible(true);
			root.queryResult = new ArrayList<List<String>>();
		}
		else if (root.typeOfOverview.equals("dataAsExcelFile") || root.typeOfOverview.equals("dataInExistingExcelFile") || root.typeOfOverview.equals("loadQueriesAndDataInExistingExcelFile")) {
			String fileName = "";
			if (root.typeOfOverview.equals("dataAsExcelFile")) {
				//create new excel file
				fileName = ((Textbox)root.vView.getComponent("excelFileName")).getValue();
				if (!fileName.equals("") && !fileName.contains(".")) {
					fileName += ".xlsx";
				}
			}
			else {
				//update existing excel file
				fileName = (String)getAttribute("excelFileName");
			}
			if (root.queryResult.size() > 0) {
				HashMap<String,Object> hQueryResult = new HashMap<String,Object>();
				hQueryResult.put("queryResult", root.queryResult); 
				hQueryResult.put("includeRunData", includeRunData); 
				hQueryResult.put("includeStudentData", includeStudentData); 
				hQueryResult.put("sheetName", ((Textbox)root.vView.getComponent("excelSheetName")).getValue()); 
				hQueryResult.put("rowOffset", ((Intbox)root.vView.getComponent("excelRowOffset")).getValue()); 
				hQueryResult.put("colOffset", ((Intbox)root.vView.getComponent("excelColOffset")).getValue()); 
				hQueryResult.put("addValues", ((Checkbox)root.vView.getComponent("excelAddValues")).isChecked()); 
				hQueryResult.put("splitAttributeValuesOn", ((Textbox)root.vView.getComponent("excelSplitAttributeValuesOn")).getValue()); 
				hQueryResult.put("splitAttributeValuesColOffset", ((Intbox)root.vView.getComponent("excelSplitAttributeValuesColOffset")).getValue());
				root.queryResults.add(hQueryResult);
			} 
			if (root.typeOfOverview.equals("loadQueriesAndDataInExistingExcelFile")) {
				//multiple queries are loaded and executed. See loadQueriesAndDataInExistingExcelFileButton, it handles updating and download of excel file
				Events.postEvent("onReady", root.vView.getComponent("loadQueriesAndDataInExistingExcelFileButton"), null);
			}
			else {
				//create or update workbook (a XLSX file)
				if (!root.createOrUpdateWorkbook(root.queryResults, root.typeOfOverview.equals("dataAsExcelFile"), fileName)) {
					root.vView.showMessagebox(getRoot(), root.vView.getLabel("tut_rungroupaccountownoverview.messagebox.text.filecreationerror").replace("%1", fileName), 
							root.vView.getLabel("messagebox.title.errors"), Messagebox.OK, Messagebox.ERROR);
				}
				else {
					//download excel file
					root.downloadFile(root.getAbsoluteTempPath(), fileName);
				}
				root.queryResult = new ArrayList<List<String>>();
				root.queryResults = new ArrayList<HashMap<String,Object>>();
			}
		}
	}

	public void initRunGroupAccountItems() {
		if (runGroupAccountItems != null) {
			return;
		} 
		runTeams = new ArrayList<IERunTeam>();
		runGroupIds = new ArrayList<Integer>();
		runGroupAccountItems = new ArrayList<Hashtable<String,Object>>();
		for (IERun run : root.runs) {
			runTeams.addAll(((IRunTeamManager)root.sSpring.getBean("runTeamManager")).getAllRunTeamsByRunId(run.getRunId()));
			List<IERunGroup> runGroups = ((IRunGroupManager)root.sSpring.getBean("runGroupManager")).getAllRunGroupsByRunId(run.getRunId());
			List<Integer> rugIds = new ArrayList<Integer>();
			for (IERunGroup runGroup : runGroups) {
				if (!rugIds.contains(runGroup.getRugId()))
					rugIds.add(runGroup.getRugId());
			}
			List<IERunGroupAccount> items = ((IRunGroupAccountManager)root.sSpring.getBean("runGroupAccountManager")).getAllRunGroupAccountsByRugIds(rugIds);
			List<IEAccount> accounts = new ArrayList<IEAccount>();
			for (IERunGroupAccount runGroupAccount : items) {
				IEAccount account = runGroupAccount.getEAccount();
				if (account.getActive() && (!accounts.contains(account))) {
					List<IERunGroupAccount> runGroupAccounts = getRunGroupAccounts(account.getAccId(), run.getRunId(), items);
					if (runGroupAccounts.size() > 0) {
						accounts.add(account);
						if (!runGroupIds.contains(runGroupAccount.getERunGroup().getRugId()))
							runGroupIds.add(runGroupAccount.getERunGroup().getRugId());
						Hashtable<String,Object> runGroupAccountItem = new Hashtable<String,Object>(0);
						runGroupAccountItem.put("item",account);
						runGroupAccountItem.put("rungroupaccounts",runGroupAccounts);
						runGroupAccountItem.put("runteams",getRunTeams(runGroupAccounts));
						runGroupAccountItem.put("run",run);
						runGroupAccountItem.put("runid",run.getRunId());
						runGroupAccountItems.add(runGroupAccountItem);
					}
				}
			}
		}
		helper = getHelper(); 
	}

	public void onGenerateData(Event event) {
		//NOTE onGeneratedData is called for each query. rungroupaccounts have to be loaded only once.
		initRunGroupAccountItems();

		busy = false;
		ready = false;
		itemNumber = 0; 

		//NOTE asynchronously presenting results using timer
		((Label)root.vView.getComponent("progress")).setValue("");
		if (runGroupAccountItems.size() > 0) {
			Events.echoEvent("onStart", root.vView.getComponent("progressTimer"), null);
		}

		/*
		//NOTE synchronously presenting results 
		for (int i=0;i<runGroupAccountItems.size();i++) {
			onGenerateDataItem();
		}
		Events.postEvent("onReady", self, null);
		 */
	}

	public void onGenerateDataItem() {
		if (itemNumber >= runGroupAccountItems.size()) {
			((Timer)root.vView.getComponent("progressTimer")).stop();
			if (!ready) {
				ready = true;
				Events.postEvent("onReady", this, null);
			}
		}
		else {
			busy = true;
			((CTutRunGroupAccountOwnOverviewHelper)helper).renderItem(this, null, runGroupAccountItems.get(itemNumber), root.typeOfOverview);
			itemNumber++;
			long percentage = (100 * itemNumber) / runGroupAccountItems.size();
			((Label)root.vView.getComponent("progress")).setValue("" + percentage + "%");
			if (root.typeOfOverview.equals("loadQueriesAndDataInExistingExcelFile")) {
				Events.postEvent("onProgress", root.vView.getComponent("loadQueriesAndDataInExistingExcelFileButton"), percentage);
			}
			busy = false;
			Events.echoEvent("onStart", root.vView.getComponent("progressTimer"), null);
		}
	}

	boolean isBusy() {
		return busy;
	}

}
