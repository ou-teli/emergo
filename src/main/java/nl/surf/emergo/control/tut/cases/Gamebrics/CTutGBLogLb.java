/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.tut.cases.Gamebrics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Timer;

import nl.surf.emergo.business.IRunGroupAccountManager;
import nl.surf.emergo.business.IRunGroupManager;
import nl.surf.emergo.business.IRunTeamManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CCaseHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.tut.CTutRunGroupAccountsLb;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.domain.IERunTeam;


/**
 * The Class CTutGBLogLb.
 */
public class CTutGBLogLb extends CTutRunGroupAccountsLb {

	//NOTE CTutGBLogLb extends from CTutRunGroupAccountsLb. The latter class is responsible for
	//rendering of all rungroupaccount data and already has methods to show data in all types of overviews.

	private static final long serialVersionUID = 1L;
	private static final Logger _log = LogManager.getLogger(CTutGBLogLb.class);

	protected CTutGBLogWnd root = (CTutGBLogWnd)CDesktopComponents.vView().getComponent(CTutGBLogWnd.wndWndId);

	protected List<Hashtable<String,Object>> runGroupAccountItems = null;
	protected CTutGBLogHelper helper = null;
	protected int itemNumber = 0;

	protected boolean busy = false;
	protected boolean ready = false;

	protected String[] assessmentsCaseComponentNames = new String[] {};
	protected String prePostAssessmentsCaseComponentName = "";
	
	protected CCaseHelper caseHelper = new CCaseHelper();
	
	protected CTutGBLogHelper getHelper() {
		return new CTutGBLogHelper();
	}

	public void onCreate() {
		//NOTE overwrite onCreate of super. Instead onGenerateData is called.
		restoreSettings();
	}

	public void onUpdate(Event event) {
		//NOTE onUpdate is sent if an overview has to be created or updated
		//initialize
		root.vView.getComponent("dataListbox").setVisible(false);
		root.vView.getComponent("dataInclude").setVisible(false);
		runGroupAccountItems = null;

		prePostAssessmentsCaseComponentName = ((Textbox)root.vView.getComponent(CTutGBLogWnd.wndPPAssNameFieldId)).getValue();
		if (prePostAssessmentsCaseComponentName.equals("")) {
			root.vView.showMessagebox(getRoot(), "pre- en posttestcasecomponentnaam mag niet leeg zijn", 
					root.vView.getLabel("messagebox.title.information"), Messagebox.OK, Messagebox.INFORMATION);
		} else {
			assessmentsCaseComponentNames = new String[] {};
			String lAssessmentsCaseComponentNames = ((Textbox)root.vView.getComponent(CTutGBLogWnd.wndAssNamesFieldId)).getValue();
			if (!StringUtils.isEmpty(lAssessmentsCaseComponentNames))
				assessmentsCaseComponentNames = (lAssessmentsCaseComponentNames.split(","));
			root.queryResult = new ArrayList<List<String>>();
			//generate data to be shown in overview, data is stored in queryResult
			Events.postEvent("onGenerateData", this, null);
		}
	}

	protected Boolean studentHasFinishedPrePost(IERunGroup student, IECaseComponent assessmentsCaseComponent) {
		Boolean lReady = false;
		IXMLTag rootTag = root.sSpring.getXmlDataPlusRunGroupStatusTree(student.getRugId(), assessmentsCaseComponent);
		List<IXMLTag> nodeTags = CDesktopComponents.cScript().getNodeTags(rootTag);
		for (IXMLTag nodeTag : nodeTags) {
			if (nodeTag.getName().equals("assessment")) {
				//NOTE only one
				String lAssName = CDesktopComponents.sSpring().unescapeXML(nodeTag.getChildValue("hovertext"));
				int lCountFinished = nodeTag.getStatusAttributeCount(AppConstants.statusKeyFinished, AppConstants.statusValueTrue);
				if (lCountFinished == 2)
					lReady = true;
				_log.info("number of finished pre/post for student {}: {}", student.getName(), lCountFinished);
				break;
			}
		}
		return lReady;
	}

	public void initRunGroupAccountItems() {
		if (runGroupAccountItems != null) {
			return;
		} 
		runTeams = new ArrayList<IERunTeam>();
		runGroupIds = new ArrayList<Integer>();
		runGroupAccountItems = new ArrayList<Hashtable<String,Object>>();
		int lNrOfRunsIncluded = 0;
		for (IERun run : root.runs) {
			root.sSpring.clearRunBoostProps();
			caseHelper = new CCaseHelper();
			root.sSpring.setRun(run);
			root.sSpring.setRunStatus(AppConstants.runStatusRun);

			IECaseComponent lPrePostAssessmentsCaseComponent = root.sSpring.getCaseComponentByComponentCodeOrCaseComponentName(root.getCaseComponents(run.getECase()),"",  prePostAssessmentsCaseComponentName);
			List<IECaseComponent> lOtherAssessmentsCaseComponents = new ArrayList<IECaseComponent>();
			for (String lAssCCName : assessmentsCaseComponentNames) {
				IECaseComponent lCC = root.sSpring.getCaseComponentByComponentCodeOrCaseComponentName(root.getCaseComponents(run.getECase()),"",  lAssCCName);
				if (lCC == null)
					break;
				lOtherAssessmentsCaseComponents.add(lCC);
			}
			if ((lPrePostAssessmentsCaseComponent != null) && (lOtherAssessmentsCaseComponents.size() == assessmentsCaseComponentNames.length)) {
				lNrOfRunsIncluded++;
				runTeams.addAll(((IRunTeamManager)root.sSpring.getBean("runTeamManager")).getAllRunTeamsByRunId(run.getRunId()));
				List<IERunGroup> runGroups = ((IRunGroupManager)root.sSpring.getBean("runGroupManager")).getAllRunGroupsByRunId(run.getRunId());
				List<Integer> rugIds = new ArrayList<Integer>();
				for (IERunGroup runGroup : runGroups) {
					int lRugId = runGroup.getRugId();
					if (!rugIds.contains(lRugId)) {
						List<IERunGroupAccount> lRgas = ((IRunGroupAccountManager)root.sSpring.getBean("runGroupAccountManager")).getAllRunGroupAccountsByRugId(lRugId);
						// only one
						root.sSpring.setRunGroupAccount(lRgas.get(0));
						//NOTE only add students that have finished post assessment
						if (studentHasFinishedPrePost(runGroup, lPrePostAssessmentsCaseComponent))
							rugIds.add(lRugId);
					}
				}
				
				List<IERunGroupAccount> items = ((IRunGroupAccountManager)root.sSpring.getBean("runGroupAccountManager")).getAllRunGroupAccountsByRugIds(rugIds);
				List<IEAccount> accounts = new ArrayList<IEAccount>();
				for (IERunGroupAccount runGroupAccount : items) {
					IEAccount account = runGroupAccount.getEAccount();
					if (!accounts.contains(account)) {
						List<IERunGroupAccount> runGroupAccounts = new ArrayList<IERunGroupAccount>();
						runGroupAccounts.add(runGroupAccount);
						accounts.add(account);
						if (!runGroupIds.contains(runGroupAccount.getERunGroup().getRugId()))
							runGroupIds.add(runGroupAccount.getERunGroup().getRugId());
						Hashtable<String,Object> runGroupAccountItem = new Hashtable<String,Object>(0);
						runGroupAccountItem.put("item",account);
						runGroupAccountItem.put("rungroupaccounts",runGroupAccounts);
						runGroupAccountItem.put("run",run);
						runGroupAccountItem.put("runid",run.getRunId());
						runGroupAccountItem.put("prepostassessmentscasecomponent",lPrePostAssessmentsCaseComponent);
						runGroupAccountItem.put("otherassessmentscasecomponents",lOtherAssessmentsCaseComponents);
						runGroupAccountItems.add(runGroupAccountItem);
					}
				}
				
			} else
				_log.info("one or more assessment components not present, run will not be inlcuded: {}", run.getName());
		}
		if (lNrOfRunsIncluded == 0)
			root.vView.showMessagebox(getRoot(), "pretoets-/posttoets-casecomponent of andere toets-casecomponent ontbreekt in alle runs", 
					root.vView.getLabel("messagebox.title.information"), Messagebox.OK, Messagebox.INFORMATION);
		helper = getHelper(); 
	}

	public void onGenerateData(Event event) {
		//NOTE onGeneratedData is called for each query.
		initRunGroupAccountItems();

		busy = false;
		ready = false;
		itemNumber = 0; 

		//NOTE asynchronously presenting results using timer
		((Label)root.vView.getComponent("progress")).setValue("");
		if (runGroupAccountItems.size() > 0) {
			Events.echoEvent("onStart", root.vView.getComponent("progressTimer"), null);
		}

		/*
		//NOTE synchronously presenting results 
		for (int i=0;i<runGroupAccountItems.size();i++) {
			onGenerateDataItem();
		}
		Events.postEvent("onReady", self, null);
		 */
	}

	public void onGenerateDataItem() {
		if (itemNumber >= runGroupAccountItems.size()) {
			((Timer)root.vView.getComponent("progressTimer")).stop();
			if (!ready) {
				ready = true;
				Events.postEvent("onReady", this, null);
			}
		}
		else {
			busy = true;
			helper.renderItem(this, null, runGroupAccountItems.get(itemNumber), root.typeOfOverview);
			itemNumber++;
			long percentage = (100 * itemNumber) / runGroupAccountItems.size();
			((Label)root.vView.getComponent("progress")).setValue("" + percentage + "%");
			busy = false;
			Events.echoEvent("onStart", root.vView.getComponent("progressTimer"), null);
		}
	}

	boolean isBusy() {
		return busy;
	}

	public void onReady() {
		//NOTE onReady is sent if all data needed for an overview has been gathered
		//add query headers
		HashMap<String,Object> lHeaderInfo = root.getQueryHeader();
		root.queryResult.add(0, (List<String>)lHeaderInfo.get(CTutGBLogWnd.xcelHeaderCellsText));
		//create new excel file
		String fileName = ((Textbox)root.vView.getComponent("excelFileName")).getValue();
		if (!fileName.equals("") && !fileName.contains(".")) {
			fileName += ".xlsx";
		}
		if (root.queryResult.size() > 0) {
			HashMap<String,Object> hQueryResult = new HashMap<String,Object>();
			hQueryResult.put("queryResult", root.queryResult); 
			hQueryResult.put("sheetName", ((Textbox)root.vView.getComponent("excelSheetName")).getValue()); 
			hQueryResult.put("rowOffset", 0); 
			hQueryResult.put("colOffset", 0); 
			hQueryResult.put("addValues", false); 
			hQueryResult.put("splitAttributeValuesOn", ""); 
			hQueryResult.put("splitAttributeValuesColOffset", 20);
			hQueryResult.put(CTutGBLogWnd.xcelHeaderCellsInfo, lHeaderInfo.get(CTutGBLogWnd.xcelHeaderCellsInfo));
			root.queryResults.add(hQueryResult);
		} 
		//create or update workbook (an XLSX file)
		if (!root.createOrUpdateWorkbook(root.queryResults, root.typeOfOverview.equals("dataAsExcelFile"), fileName)) {
			root.vView.showMessagebox(getRoot(), root.vView.getLabel("tut_rungroupaccountownoverview.messagebox.text.filecreationerror").replace("%1", fileName), 
					root.vView.getLabel("messagebox.title.errors"), Messagebox.OK, Messagebox.ERROR);
		}
		else {
			//download excel file
			root.downloadFile(root.getAbsoluteTempPath(), fileName);
		}
		root.queryResult = new ArrayList<List<String>>();
		root.queryResults = new ArrayList<HashMap<String,Object>>();
		root.xcelAssHeaderInfo = new HashMap<String,Object>();
	}

}
