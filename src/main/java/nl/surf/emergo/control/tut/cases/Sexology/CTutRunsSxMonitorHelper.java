/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.tut.cases.Sexology;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import nl.surf.emergo.business.IRunAccountManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunAccount;
import nl.surf.emergo.zkspring.SSpring;

public class CTutRunsSxMonitorHelper {

	protected CControl cControl = CDesktopComponents.cControl();
	protected SSpring sSpring = CDesktopComponents.sSpring();
	protected static final String gRunCode = "sks_required";
	protected String gTutorPortal = "/" + CDesktopComponents.vView().v_account_role.get((String)CDesktopComponents.cControl().getAccSessAttr("role"));

	private boolean isRunning(IERun aRun) {
		if (aRun.getStatus() != AppConstants.run_status_runnable)
			return false;
		return true;
	}

	public List<IERun> getRuns() {
		List<IERun> lRuns = new ArrayList<IERun>();
		Object lAccIdObj = cControl.getAccSessAttr("accId");
		if (lAccIdObj != null) {
			int lAccId = (int)lAccIdObj;
			List<IERunAccount> lRunAccounts = ((IRunAccountManager)CDesktopComponents.sSpring().getBean("runAccountManager")).getAllRunAccountsByAccIdActiveAsTutor(lAccId);
			for (Iterator<IERunAccount> it = lRunAccounts.iterator(); it.hasNext(); ) {
				IERun lRun = it.next().getERun();
				if (isRunning(lRun)) {
					if (lRun.getCode().contains(gRunCode)) {
						lRuns.add(lRun);
					}
				}
			}
		}
		return lRuns;
	}

	public void toRegularOverview() {
		CDesktopComponents.vView().redirectToView(gTutorPortal);
	}

}
