/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.tut;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Html;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefListcell;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERunGroupAccount;

/**
 * The Class CTutRunGroupAccountTasksHelper.
 */
public class CTutRunGroupAccountTasksHelper extends CTutRunGroupAccountsHelper {

	/** The tasks case components. */
	//NOTE there can be multiple tasks components
	private List<IECaseComponent> tasksCaseComponents = null;

	/**
	 * Renders finished tasks for one account.
	 * 
	 * @param aListbox the a listbox
	 * @param aInsertBefore the a insert before
	 * @param aItem the a item
	 */
	public void renderItem(Listbox aListbox,Listitem aInsertBefore,Hashtable<String,Object> aItem) {
		Listitem lListitem = super.newListitem();
		List<IERunGroupAccount> lRunGroupAccounts = (List)aItem.get("rungroupaccounts");
		IEAccount lItem = (IEAccount)aItem.get("item");
		lListitem.setValue(lItem);
		super.appendListcell(lListitem,lItem.getTitle());
		super.appendListcell(lListitem,lItem.getInitials());
		super.appendListcell(lListitem,lItem.getNameprefix());
		super.appendListcell(lListitem,lItem.getLastname());
		Listcell lListcell = new CDefListcell();

		List<Component> lTaskLabels = new ArrayList<Component>(0);
		for (IERunGroupAccount lRunGroupAccount : lRunGroupAccounts) {
			CDesktopComponents.sSpring().setRunStatus(AppConstants.runStatusRun);
			CDesktopComponents.sSpring().setRunGroupAccount(lRunGroupAccount);
			if (tasksCaseComponents == null)
				tasksCaseComponents = CDesktopComponents.sSpring().getCaseComponents("tasks");
			if (tasksCaseComponents == null || tasksCaseComponents.size() == 0)
				return;
			for (IECaseComponent lTasksCaseComponent : tasksCaseComponents) {
				boolean lCaseComponentNameAdded = false;
				List<IXMLTag> lTags = CDesktopComponents.cScript().getRunGroupNodeTags(""+lTasksCaseComponent.getCacId(), "task");
				for (int j=(lTags.size()-1);j>=0;j--) {
					IXMLTag lTag = (IXMLTag)lTags.get(j);
					boolean lFinished = lTag.getCurrentStatusAttribute(AppConstants.statusKeyFinished)
							.equals(AppConstants.statusValueTrue);
					if (!lFinished)
						lTags.remove(j);
					else {
						if (tasksCaseComponents.size() > 1 && !lCaseComponentNameAdded) {
							//add tasks case component name
							if (lTaskLabels.size() > 0) {
								lTaskLabels.add(new Html("<br/>"));
							}
							lTaskLabels.add(new Html(lTasksCaseComponent.getName() + ":"));
							lCaseComponentNameAdded = true;
						}
						if (lTaskLabels.size() > 0) {
							lTaskLabels.add(new Html("<br/>"));
						}
						lTaskLabels.add(new Html("- " + lTag.getChild("name").getValue()));
					}
				}
			}
		}

		for (int i=0;i<lTaskLabels.size();i++) {
			lListcell.appendChild((Component)lTaskLabels.get(i));
		}

		lListitem.appendChild(lListcell);
		super.insertListitem(aListbox,lListitem,aInsertBefore);
	}
}