/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.tut;

import java.util.HashMap;
import java.util.Map;

import org.zkoss.zk.ui.event.Event;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.CRunBlobLabel;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpring;

/**
 * The Class CTutRunGroupAccountEmailsText.
 */
public class CTutRunGroupAccountEmailsTextLabel extends CRunBlobLabel {

	private static final long serialVersionUID = -2998688531877701462L;

	/** The s spring. */
	//private static SSpring sSpring = new SSpring();
	private static SSpring sSpring = CDesktopComponents.sSpring();
	
	/**
	 * Inits by setting value to aLabel and setting style to hot word style.
	 *
	 * @param aLabel the a label
	 */
	public void init(String aLabel) {
		setValue(aLabel);
		setZclass("CRunBlobLabel_active");
	}

	public void onClick(Event aEvent) {
		IXMLTag tag = this.blobcontainer;
		if (tag == null || sSpring == null)
			return;
		String lName = tag.getName();
		if (lName.equals("map"))
			return;
		//String lBody = sSpring.unescapeXML(tag.getChildValue("richtext"));
		String lBody = sSpring.unescapeXML(tag.getStatusChildValue("richtext"));
		String lPcTag = "&lt;pc&gt;";
		int lIndex = lBody.indexOf(lPcTag);
		while (lIndex >= 0) {
			String lNewBody = "";
			if (lIndex > 0)
				lNewBody = lNewBody + lBody.substring(0, lIndex);
			lNewBody = lNewBody + sSpring.getRunGroup().getName();
			if ((lIndex + lPcTag.length()) < lBody.length())
				lNewBody = lNewBody + lBody.substring(lIndex + lPcTag.length(), lBody.length());
			lBody = lNewBody;
			lIndex = lBody.indexOf(lPcTag);
		}
		Map<String, Object> lParams = new HashMap<String, Object>();
		lParams.put("item", tag);
		lParams.put("body", lBody);
		//TODO Better use quasiModalPopup?
		CDesktopComponents.vView().popup(VView.v_tut_rungroupaccountemailtext, null, lParams, "center");
	}

	/**
	 * set on click widget listener.
	 */
	protected void setOnClickWidgetListener() {
	}

}
