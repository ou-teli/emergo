/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.tut;

import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefButton;

/**
 * The Class CTutRunGroupAccountOwnOverviewSaveDataAsExcelFileBtn.
 */
public class CTutRunGroupAccountOwnOverviewSaveDataAsExcelFileBtn extends CDefButton {

	private static final long serialVersionUID = -7239297796989382352L;

	protected CTutRunGroupAccountOwnOverviewWnd root = (CTutRunGroupAccountOwnOverviewWnd)CDesktopComponents.vView().getComponent("ownOverviewWnd");

	public void onClick() {
		//NOTE an excel XLSX file will created
		//initialize progress values
		((Label)root.vView.getComponent("progress")).setValue("");
		((Label)root.vView.getComponent("queryProgress")).setValue("");
		//get file name
		String fileName = ((Textbox)root.vView.getComponent("excelFileName")).getValue();
		if (!fileName.equals("") && !fileName.contains(".")) {
			fileName += ".xlsx";
		}
		String excelSheetName = ((Textbox)root.vView.getComponent("excelSheetName")).getValue();
		//excel file name has to be filled in
		//excel sheet name has to be filled in
		if (fileName.equals("")) {
			root.vView.showMessagebox(getRoot(), root.vView.getLabel("tut_rungroupaccountownoverview.messagebox.text.emptyfilename"), 
					root.vView.getLabel("messagebox.title.information"), Messagebox.OK, Messagebox.INFORMATION);
		}
		else if (excelSheetName.equals("")) {
			root.vView.showMessagebox(getRoot(), root.vView.getLabel("tut_rungroupaccountownoverview.messagebox.text.emptyexcelsheetname"), 
					root.vView.getLabel("messagebox.title.information"), Messagebox.OK, Messagebox.INFORMATION);
		}
		else {
			root.typeOfOverview = "dataAsExcelFile";
			//execute query and save in new excel file
			Events.postEvent("onUpdate", root.vView.getComponent("dataListbox"), null);
		}
	}

}
