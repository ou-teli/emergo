/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.tut;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefButton;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.domain.IERun;

/**
 * The Class CTutRunsEmailAttachmentsLb.
 */
public class CTutRunsEmailAttachmentsLb extends CTutRunsLb {

	private static final long serialVersionUID = -3201394819444289054L;

	/**
	 * On create render listitems.
	 */
	public void onCreate() {
		super.onCreate();
		for (Listitem lListitem : getItems()) {
			Listcell lListcell = new Listcell();
			CDefButton lRedirect = new CDefButton();
			lRedirect.setLabel(CDesktopComponents.vView().getLabel("nodetag.attachments"));
			lRedirect.addEventListener("onClick",
					new EventListener<Event>() {
				public void onEvent(Event event) {
					IERun lItem = (IERun)((Listitem)event.getTarget().getParent().getParent()).getValue();
					IECase lCase = lItem.getECase();
					CDesktopComponents.cControl().setAccSessAttr("case", lCase);
					CDesktopComponents.cControl().setAccSessAttr("run", lItem);
					CDesktopComponents.cControl().setAccSessAttr("runId", "" + lItem.getRunId());
					CDesktopComponents.cControl().setAccSessAttr("runName", "" + lItem.getName());
					CDesktopComponents.vView().redirectToView("tut_runemailsattachments.zul");
				}
			});
			lListcell.appendChild(lRedirect);
			lListitem.appendChild(lListcell);
		}
	}

}
