/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.tut;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;

import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDecoratedInputWndTC;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.script.CScript;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.utilities.ExcelHelper;
import nl.surf.emergo.utilities.FileHelper;
import nl.surf.emergo.utilities.ZipHelper;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpring;


/**
 * The Class CTutRunGroupAccountOwnOverviewWnd.
 */
public class CTutRunGroupAccountOwnOverviewWnd extends CDecoratedInputWndTC {

	private static final long serialVersionUID = -4452611757947993366L;
	private static final Logger _log = LogManager.getLogger(CTutRunGroupAccountOwnOverviewWnd.class);

	public VView vView = CDesktopComponents.vView();

	public CControl cControl = CDesktopComponents.cControl();

	public SSpring sSpring = CDesktopComponents.sSpring();

	public CScript cScript = CDesktopComponents.cScript();

	public FileHelper fileHelper = new FileHelper();

	public ZipHelper zipHelper = new ZipHelper();

	public ExcelHelper excelHelper = new ExcelHelper();

	//NOTE supports multiple cases!

	public List<IERun> runs = (List<IERun>)cControl.getAccSessAttr("runs");

	public List<IECase> cases = new ArrayList<IECase>();

	public boolean areMultipleCases = false;

	public String typeOfOverview = "";

	public List<IECaseComponent> caseComponents = null;

	public static final String queryComponentAndTagDataCombined = "componentAndTagDataCombined";
	public static final String queryCaseComponentNames = "caseComponentNames";
	public static final String queryCaseComponentTemplate = "caseComponentTemplate";
	public static final String queryTagNames = "tagNames";
	public static final String queryTagKeyTemplate = "tagKeyTemplate";
	public static final String queryAttributeKeys = "attributeKeys";
	public static final String queryAttributeValueTemplate = "attributeValueTemplate";
	public static final String queryIncludeDefaultValues = "includeDefaultValues";
	public static final String queryIncludeHistory = "includeHistory";
	public static final String queryIncludeRunData = "includeRunData";
	public static final String queryIncludeStudentData = "includeStudentData";
	public static final String queryAttributeValueConvertPointsToCommas = "attributeValueConvertPointsToCommas";
	public static final String querySortOnColumns = "sortOnColumns";
	public static final String queryExcelSheetName = "excelSheetName";
	public static final String queryExcelRowOffset = "excelRowOffset";
	public static final String queryExcelColOffset = "excelColOffset";
	public static final String queryExcelAddValues = "excelAddValues";
	public static final String queryExcelSplitAttributeValuesOn = "excelSplitAttributeValuesOn";
	public static final String queryExcelSplitAttributeValuesColOffset = "excelSplitAttributeValuesColOffset";

	public static List<String> queryKeys = new ArrayList<String>();

	public List<List<String>> queryResult = new ArrayList<List<String>>();
	public List<HashMap<String,Object>> queryResults = new ArrayList<HashMap<String,Object>>();

	public CTutRunGroupAccountOwnOverviewWnd() {
		for (IERun run : runs) {
			if (!cases.contains(run.getECase())) {
				cases.add(run.getECase());
			}
		}
		areMultipleCases = cases.size() > 1;
		if (cControl.getAccSessAttr("tutInitSessionVars") == null) {
			cControl.setAccSessAttr("tutInitSessionVars", true);
			initSessionVars();
		}
		initQueryKeys();
	}

	public void initSessionVars() {
		cControl.setAccSessAttr("tutComponentAndTagDataCombined", false);
		cControl.setAccSessAttr("tutCaseComponents", null);
		cControl.setAccSessAttr("tutCaseComponentTemplate", "");
		cControl.setAccSessAttr("tutCaseComponentTemplateCaseComponents", null);
		cControl.setAccSessAttr("tutTagNames", null);
		cControl.setAccSessAttr("tutTagKeyTemplate", "");
		cControl.setAccSessAttr("tutAttributeKeys", null);
		cControl.setAccSessAttr("tutAttributeValueTemplate", "");
		cControl.setAccSessAttr("tutIncludeDefaultValues", false);
		cControl.setAccSessAttr("tutIncludeHistory", false);
		cControl.setAccSessAttr("tutIncludeRunData", false);
		cControl.setAccSessAttr("tutIncludeStudentData", false);
		cControl.setAccSessAttr("tutAttributeValueConvertPointsToCommas", false);
		cControl.setAccSessAttr("tutSortOnColumns", "");
		cControl.setAccSessAttr("tutExcelSheetName", "");
		cControl.setAccSessAttr("tutExcelRowOffset", 0);
		cControl.setAccSessAttr("tutExcelColOffset", 0);
		cControl.setAccSessAttr("tutExcelAddValues", false);
		cControl.setAccSessAttr("tutExcelSplitAttributeValuesOn", "");
		cControl.setAccSessAttr("tutExcelSplitAttributeValuesColOffset", 21);
	}

	public static void initQueryKeys() {
		queryKeys.add(queryComponentAndTagDataCombined);
		queryKeys.add(queryCaseComponentNames);
		queryKeys.add(queryCaseComponentTemplate);
		queryKeys.add(queryTagNames);
		queryKeys.add(queryTagKeyTemplate);
		queryKeys.add(queryAttributeKeys);
		queryKeys.add(queryAttributeValueTemplate);
		queryKeys.add(queryIncludeDefaultValues);
		queryKeys.add(queryIncludeHistory);
		queryKeys.add(queryIncludeRunData);
		queryKeys.add(queryIncludeStudentData);
		queryKeys.add(queryAttributeValueConvertPointsToCommas);
		queryKeys.add(querySortOnColumns);
		queryKeys.add(queryExcelSheetName);
		queryKeys.add(queryExcelRowOffset);
		queryKeys.add(queryExcelColOffset);
		queryKeys.add(queryExcelAddValues);
		queryKeys.add(queryExcelSplitAttributeValuesOn);
		queryKeys.add(queryExcelSplitAttributeValuesColOffset);
	}

	public List<IECaseComponent> getCaseComponents() {
		if (caseComponents != null) {
			return caseComponents;
		}
		return sSpring.getCaseComponents(cases);
	}

	public boolean listContainsCaseComponent(List<IECaseComponent> list, IECaseComponent cac) {
		if (list == null || cac == null) {
			return false;
		}
		for (IECaseComponent listCac : list) {
			if (listCac.getCacId() == cac.getCacId()) {
				return true;
			}
		}
		return false;
	}

	public boolean listContainsString(List<String> list, String string) {
		if (list == null || string == null) {
			return false;
		}
		for (String listString : list) {
			if (listString.equals(string)) {
				return true;
			}
		}
		return false;
	}

	public List<IECaseComponent> getCaseComponentTemplateCaseComponents(String templateStr) {
		List<IECaseComponent> caseComponentTemplateCaseComponents = new ArrayList<IECaseComponent>();
		if (templateStr != null && !templateStr.equals("")) {
			String[] templates = templateStr.split(",");
			for (int i=0;i<templates.length;i++) {
				Pattern p = Pattern.compile(templates[i], Pattern.DOTALL);
				for (IECaseComponent caseComponent : getCaseComponents()) {
					if (p.matcher(caseComponent.getName()).matches()) {
						caseComponentTemplateCaseComponents.add(caseComponent);
					}
				}
			}
		}
		return caseComponentTemplateCaseComponents;
	}

	public List<IECaseComponent> getSelectedCaseComponents() {
		List<IECaseComponent> selectedCaseComponents = new ArrayList<IECaseComponent>();
		List<IECaseComponent> tutCaseComponents = (List<IECaseComponent>)cControl.getAccSessAttr("tutCaseComponents");
		if (tutCaseComponents != null) {
			selectedCaseComponents.addAll(tutCaseComponents);
		}
		List<IECaseComponent> tutCaseComponentTemplateCaseComponents = (List<IECaseComponent>)cControl.getAccSessAttr("tutCaseComponentTemplateCaseComponents");
		if (tutCaseComponentTemplateCaseComponents != null) {
			for (IECaseComponent caseComponent : tutCaseComponentTemplateCaseComponents) {
				if (!selectedCaseComponents.contains(caseComponent)) {
					selectedCaseComponents.add(caseComponent);
				}
			}
		}
		return selectedCaseComponents;
	}

	public boolean areCaseComponentsSelected() {
		List<IECaseComponent> tutCaseComponents = (List<IECaseComponent>)cControl.getAccSessAttr("tutCaseComponents");
		List<IECaseComponent> tutCaseComponentTemplateCaseComponents = (List<IECaseComponent>)cControl.getAccSessAttr("tutCaseComponentTemplateCaseComponents");
		return (tutCaseComponents != null && tutCaseComponents.size() > 0) || (tutCaseComponentTemplateCaseComponents != null && tutCaseComponentTemplateCaseComponents.size() > 0);
	}

	public List<String> getTagNames() {
		List<String> tagNames = new ArrayList<String>();
		List<IECaseComponent> selectedCaseComponents = getSelectedCaseComponents();
		boolean caseComponentsChosen = selectedCaseComponents.size() > 0;
		if (caseComponentsChosen) {
			for (IECaseComponent caseComponent : selectedCaseComponents) {
				List<String> nodeTagNames = cScript.getNodeTagNames(caseComponent, AppConstants.getstatusElement);
				if (nodeTagNames != null) {
					for (String tagName : nodeTagNames) {
						if (!listContainsString(tagNames, tagName)) {
							tagNames.add(tagName);
						}
					}
				}
			}
		}
		return tagNames;
	}

	public void updateAttributeKeys(List<String> attributeKeys, List<String> tagKeys) {
		if (tagKeys == null) {
			return;
		}
		for (String key : tagKeys) {
			if (!listContainsString(attributeKeys, key)) {
				attributeKeys.add(key);
			}
		}
	}

	public List<String> getAttributeKeys(boolean componentAndTagDataCombined) {
		List<String> attributeKeys = new ArrayList<String>();
		List<IECaseComponent> selectedCaseComponents = getSelectedCaseComponents();
		boolean caseComponentsChosen = selectedCaseComponents.size() > 0;
		List<String> tutTagNames = (List<String>)cControl.getAccSessAttr("tutTagNames");
		boolean tagNamesChosen = tutTagNames != null && tutTagNames.size() > 0;
		if (caseComponentsChosen) {
			for (IECaseComponent caseComponent : selectedCaseComponents) {
				if (componentAndTagDataCombined || !tagNamesChosen) {
					updateAttributeKeys(attributeKeys, cScript.getTagStatusKeyIds("" + caseComponent.getCacId(), "", AppConstants.getstatusElement));
				}
				if (tagNamesChosen) {
					for (String tagName : tutTagNames) {
						updateAttributeKeys(attributeKeys, cScript.getTagStatusKeyIds("" + caseComponent.getCacId(), tagName, AppConstants.getstatusElement));
					}
				}
			}
		}
		return attributeKeys;
	}

	public boolean areAttributeKeysSelected() {
		List<String> tutAttributeKeys = (List<String>)cControl.getAccSessAttr("tutAttributeKeys");
		return tutAttributeKeys != null && tutAttributeKeys.size() > 0;
	}

	public void setListboxSessionAttr(Listbox listbox, String attrKey) {
		List<Object> values = (List<Object>)cControl.getAccSessAttr(attrKey);
		if (values == null) {
			values = new ArrayList<Object>();
		}
		values.clear();
		for (Listitem listitem : listbox.getSelectedItems()) {
			values.add(listitem.getValue());
		}
		cControl.setAccSessAttr(attrKey, values);
	}

	public String getAbsoluteTempPath() {
		String absoluteTempPath = sSpring.getAppManager().getAbsoluteTempPath();
		String subPath = "";
		if (Sessions.getCurrent() != null) {
			subPath = CDesktopComponents.vView().getUniqueTempSubPath();
		}
		return absoluteTempPath + subPath;
	}

	public String getDownloadPath(String absoluteTempPath) {
		String absPath = sSpring.getAppManager().getAbsoluteAppPath();
		int pos = absoluteTempPath.indexOf(absPath);
		String result = absoluteTempPath.substring(pos + absPath.length());
		
		if (!(result.endsWith("/") || result.endsWith("\\"))) {
			result += "/";
		}
		return result;
	}

	public boolean downloadFile(String absoluteTempPath, String fileName) {
		//determine download path
		absoluteTempPath = getDownloadPath(absoluteTempPath);								

		//download file
		boolean fileIsDownloaded = false;
		String probeContentType = null;
		try {
			probeContentType = Files.probeContentType(Paths.get(absoluteTempPath + fileName));
		} catch (IOException e) {
			_log.error(e);
		}
		try {
			Filedownload.save(absoluteTempPath + fileName, probeContentType);
			fileIsDownloaded = true;
		} catch (Exception e) {
			vView.showMessagebox(getRoot(), vView.getLabel("tut_rungroupaccountownoverview.messagebox.text.filedownloaderror").replace("%1", fileName), 
					vView.getLabel("messagebox.title.errors"), Messagebox.OK, Messagebox.ERROR);
		}
		if (fileIsDownloaded) {
			vView.showMessagebox(getRoot(), vView.getLabel("tut_rungroupaccountownoverview.messagebox.text.filedownload").replace("%1", fileName),
					vView.getLabel("messagebox.title.information"), Messagebox.OK, Messagebox.INFORMATION);
		}
		return fileIsDownloaded;
	}

	public boolean loadQuery(String fileName, byte[] bytes) {
		if (bytes != null && bytes.length > 0) {

			//determine keys and values within query string
			String query = new String(bytes);
			String[] lines = query.split("\n");
			Hashtable<String,String> keyValues = new Hashtable<String,String>();
			boolean readingQueryOk = true;
			for (int i=0;i<lines.length;i++) {
				String line = lines[i].trim();
				if (!line.equals("")) {
					int pos = line.indexOf("=");
					if (pos < 0) {
						readingQueryOk = false;
						break;
					}
					else {
						String key = line.substring(0, pos);
						String value = line.substring(pos + 1);
						if (queryKeys.contains(key)) {
							keyValues.put(key, value);
						}
						else {
							readingQueryOk = false;
							break;
						}
					}
				}
			}

			// restore keys and values
			if (readingQueryOk && keyValues.size() > 0) {
				for (String key : queryKeys) {
					String value = "";
					if (keyValues.containsKey(key)) {
						value = keyValues.get(key);
					}

					if (key.equals(queryComponentAndTagDataCombined)) {
						cControl.setAccSessAttr("tutComponentAndTagDataCombined", value.equals("true"));
					}

					else if (key.equals(queryCaseComponentNames)) {
						List<IECaseComponent> allCaseComponents = getCaseComponents();
						List<IECaseComponent> caseComponents = new ArrayList<IECaseComponent>();
						String[] values = value.split(",");
						for (int i=0;i<values.length;i++) {
							for (IECaseComponent caseComponent : allCaseComponents) {
								if (caseComponent.getName().equals(values[i])) {
									caseComponents.add(caseComponent);
								}
							}
						}
						cControl.setAccSessAttr("tutCaseComponents", caseComponents);
					}

					else if (key.equals(queryCaseComponentTemplate)) {
						cControl.setAccSessAttr("tutCaseComponentTemplate", value);
						cControl.setAccSessAttr("tutCaseComponentTemplateCaseComponents", getCaseComponentTemplateCaseComponents(value));
					}

					else if (key.equals(queryTagNames)) {
						List<String> tagNames = new ArrayList<String>();
						String[] values = value.split(",");
						for (int i=0;i<values.length;i++) {
							tagNames.add(values[i]);
						}
						cControl.setAccSessAttr("tutTagNames", tagNames);
					}

					else if (key.equals(queryTagKeyTemplate)) {
						cControl.setAccSessAttr("tutTagKeyTemplate", value);
					}

					else if (key.equals(queryAttributeKeys)) {
						List<String> tutAttributeKeys = new ArrayList<String>();
						String[] values = value.split(",");
						for (int i=0;i<values.length;i++) {
							int index = sSpring.getAppManager().getStatusKeyIndex(values[i]);
							if (index != -1) {
								tutAttributeKeys.add("" + index);
							}
							else {
								tutAttributeKeys.add(values[i]);
							}
						}
						cControl.setAccSessAttr("tutAttributeKeys", tutAttributeKeys);
					}

					else if (key.equals(queryAttributeValueTemplate)) {
						cControl.setAccSessAttr("tutAttributeValueTemplate", value);
					}

					else if (key.equals(queryIncludeDefaultValues)) {
						cControl.setAccSessAttr("tutIncludeDefaultValues", value.equals("true"));
					}

					else if (key.equals(queryIncludeHistory)) {
						cControl.setAccSessAttr("tutIncludeHistory", value.equals("true"));
					}

					else if (key.equals(queryIncludeRunData)) {
						cControl.setAccSessAttr("tutIncludeRunData", value.equals("true"));
					}

					else if (key.equals(queryIncludeStudentData)) {
						cControl.setAccSessAttr("tutIncludeStudentData", value.equals("true"));
					}

					else if (key.equals(queryAttributeValueConvertPointsToCommas)) {
						cControl.setAccSessAttr("tutAttributeValueConvertPointsToCommas", value.equals("true"));
					}

					else if (key.equals(querySortOnColumns)) {
						cControl.setAccSessAttr("tutSortOnColumns", value);
					}

					else if (key.equals(queryExcelSheetName)) {
						cControl.setAccSessAttr("tutExcelSheetName", value);
					}

					else if (key.equals(queryExcelRowOffset)) {
						int intValue = 0;
						try {
							intValue = Integer.parseInt(value);
						} catch (Exception e) {
						}
						cControl.setAccSessAttr("tutExcelRowOffset", intValue);
					}

					else if (key.equals(queryExcelColOffset)) {
						int intValue = 0;
						try {
							intValue = Integer.parseInt(value);
						} catch (Exception e) {
						}
						cControl.setAccSessAttr("tutExcelColOffset", intValue);
					}

					else if (key.equals(queryExcelAddValues)) {
						cControl.setAccSessAttr("tutExcelAddValues", value.equals("true"));
					}

					else if (key.equals(queryExcelSplitAttributeValuesOn)) {
						cControl.setAccSessAttr("tutExcelSplitAttributeValuesOn", value);
					}

					else if (key.equals(queryExcelSplitAttributeValuesColOffset)) {
						int intValue = 0;
						try {
							intValue = Integer.parseInt(value);
						} catch (Exception e) {
						}
						cControl.setAccSessAttr("tutExcelSplitAttributeValuesColOffset", intValue);
					}
				}
				return true;
			}
			else {
				vView.showMessagebox(getRoot(), vView.getLabel("tut_rungroupaccountownoverview.messagebox.text.wrongquerystring").replace("%1", fileName), 
						vView.getLabel("messagebox.title.errors"), Messagebox.OK, Messagebox.ERROR);
			}
		}
		else {
			vView.showMessagebox(getRoot(), vView.getLabel("tut_rungroupaccountownoverview.messagebox.text.emptyquerystring").replace("%1", fileName), 
					vView.getLabel("messagebox.title.errors"), Messagebox.OK, Messagebox.ERROR);
		}
		return false;
	}

	public String buildQuery() {
		//build query string to save
		String caseComponentNamesStr = "";
		if (cControl.getAccSessAttr("tutCaseComponents") != null) {
			List<String> names = new ArrayList<String>();
			for (IECaseComponent caseComponent : (List<IECaseComponent>)cControl.getAccSessAttr("tutCaseComponents")) {
				if (!names.contains(caseComponent.getName())) {
					names.add(caseComponent.getName());
					if (caseComponentNamesStr.length() > 0) {
						caseComponentNamesStr += ",";
					}
					caseComponentNamesStr += caseComponent.getName();
				}
			}
		}
		String caseComponentTemplateStr = (String)cControl.getAccSessAttr("tutCaseComponentTemplate");
		if (caseComponentTemplateStr == null) {
			caseComponentTemplateStr = "";
		}
		String tagNamesStr = "";
		if (cControl.getAccSessAttr("tutTagNames") != null) {
			for (String tagName : (List<String>)cControl.getAccSessAttr("tutTagNames")) {
				if (tagNamesStr.length() > 0) {
					tagNamesStr += ",";
				}
				tagNamesStr += tagName;
			}
		}
		String tagKeyTemplateStr = (String)cControl.getAccSessAttr("tutTagKeyTemplate");
		if (tagKeyTemplateStr == null) {
			tagKeyTemplateStr = "";
		}
		String attributeKeysStr = "";
		if (cControl.getAccSessAttr("tutAttributeKeys") != null) {
			for (String attributeKey : (List<String>)cControl.getAccSessAttr("tutAttributeKeys")) {
				if (attributeKeysStr.length() > 0) {
					attributeKeysStr += ",";
				}
				String key = "";
				try {
					int number = Integer.parseInt(attributeKey);
					key = sSpring.getAppManager().getStatusKey(number);
				} catch (Exception e) {
					key = attributeKey;
				}
				attributeKeysStr += key;
			}
		}
		String attributeValueTemplateStr = (String)cControl.getAccSessAttr("tutAttributeValueTemplate");
		if (attributeValueTemplateStr == null) {
			attributeValueTemplateStr = "";
		}
		String query = queryComponentAndTagDataCombined + "=" + cControl.getAccSessAttr("tutComponentAndTagDataCombined") + "\n" +
				queryCaseComponentNames + "=" + caseComponentNamesStr + "\n" +
				queryCaseComponentTemplate + "=" + caseComponentTemplateStr + "\n" +
				queryTagNames + "=" + tagNamesStr + "\n" +
				queryTagKeyTemplate + "=" + tagKeyTemplateStr + "\n" +
				queryAttributeKeys + "=" + attributeKeysStr + "\n" +
				queryAttributeValueTemplate + "=" + attributeValueTemplateStr + "\n" +
				queryIncludeDefaultValues + "=" + cControl.getAccSessAttr("tutIncludeDefaultValues") + "\n" +
				queryIncludeHistory + "=" + cControl.getAccSessAttr("tutIncludeHistory") + "\n" +
				queryIncludeRunData + "=" + cControl.getAccSessAttr("tutIncludeRunData") + "\n" +
				queryIncludeStudentData + "=" + cControl.getAccSessAttr("tutIncludeStudentData") + "\n" +
				queryAttributeValueConvertPointsToCommas + "=" + cControl.getAccSessAttr("tutAttributeValueConvertPointsToCommas") + "\n" +
				querySortOnColumns + "=" + cControl.getAccSessAttr("tutSortOnColumns") + "\n" +
				queryExcelSheetName + "=" + cControl.getAccSessAttr("tutExcelSheetName") + "\n" +
				queryExcelRowOffset + "=" + cControl.getAccSessAttr("tutExcelRowOffset") + "\n" +
				queryExcelColOffset + "=" + cControl.getAccSessAttr("tutExcelColOffset") + "\n" +
				queryExcelAddValues + "=" + cControl.getAccSessAttr("tutExcelAddValues") + "\n" +
				queryExcelSplitAttributeValuesOn + "=" + cControl.getAccSessAttr("tutExcelSplitAttributeValuesOn") + "\n" +
				queryExcelSplitAttributeValuesColOffset + "=" + cControl.getAccSessAttr("tutExcelSplitAttributeValuesColOffset");
		return query;
	}

	public boolean createOrUpdateWorkbook(List<HashMap<String,Object>> queryResults, boolean createWorkbook, String fileName) {

		/*NOTE
			Cells in already existing rows may not be overwritten! So be sure that rowOffset points to a non-existing row.
			There are three use-cases to use SXSSFWorkbook(XSSFWorkbook) :
				Append new sheets to existing workbooks. You can open existing workbook from a file or create on the fly with XSSF.
				Append rows to existing sheets. The row number MUST be greater than max(rownum) in the template sheet.
				Use existing workbook as a template and re-use global objects such as cell styles, formats, images, etc.
		 */

		if (queryResults.size() == 0) {
			return false;
		}

		//NOTE default streaming is true so a streaming workbook is used.
		//However if for at least one of the queries 'addValues' is false a non streaming workbook is used, because
		//cells may be overwritten which is not allowed for a streaming workbook.
		boolean streaming = true;
		for (HashMap<String,Object> hQueryResult : queryResults) {
			boolean addValues = (Boolean)hQueryResult.get("addValues");
			if (!addValues) {
				streaming = false;
				break;
			} 
		}

		Workbook workbook = excelHelper.createOrLoadWorkbook(createWorkbook, streaming, getAbsoluteTempPath(), fileName);

		// Create a Font for styling header cells
		Font headerFont = workbook.createFont();
		headerFont.setBold(true);

		// Create a CellStyle with the font
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);

		for (HashMap<String,Object> hQueryResult : queryResults) {
			List<List<String>> queryResult = (List<List<String>>)hQueryResult.get("queryResult");
			boolean includeRunData = (Boolean)hQueryResult.get("includeRunData"); 
			boolean includeStudentData = (Boolean)hQueryResult.get("includeStudentData"); 
			String sheetName = (String)hQueryResult.get("sheetName"); 
			Integer rowOffset = (Integer)hQueryResult.get("rowOffset"); 
			Integer colOffset = (Integer)hQueryResult.get("colOffset"); 
			boolean addValues = (Boolean)hQueryResult.get("addValues"); 
			String splitAttributeValuesOn = (String)hQueryResult.get("splitAttributeValuesOn"); 
			Integer splitAttributeValuesColOffset = (Integer)hQueryResult.get("splitAttributeValuesColOffset"); 

			// Get or create a Sheet
			Sheet sheet = workbook.getSheet(sheetName);
			boolean createNewSheet = (sheet == null);
			if (createNewSheet) {
				sheet = workbook.createSheet(sheetName);
			}
			workbook.setActiveSheet(workbook.getSheetIndex(sheetName));


			// Create rows and cells
			int rowNum = rowOffset;
			int maxCols = queryResult.get(0).size();

			if (streaming && createNewSheet) {
				//NOTE Track all columns for auto sizing. Has to be done before rows are written to the sheet, if SXSSFWorkbook (streaming workbook) is used.
				for(int i = colOffset; i < (colOffset + maxCols); i++) {
					((SXSSFSheet)sheet).trackColumnForAutoSizing(i);
				}
			}

			if (addValues) {
				//NOTE add data after rows where cell in column colOffset exists
				Row row = sheet.getRow(rowNum);
				if (row != null) {
					Cell cell = row.getCell(colOffset);
					while (row != null && cell != null && !cell.getStringCellValue().equals("")) {
						if (cell != null) {
							rowNum++;
						}
						row = sheet.getRow(rowNum);
						if (row != null) {
							cell = row.getCell(colOffset);
						}
						else {
							cell = null;
						}
					}
				}
				if (rowNum > rowOffset) {
					//if existing data, skip header
					if (queryResult.size() > 0) {
						queryResult.remove(0);
					}
				}
			}

			//NOTE result is shown in column 7. First column is 0, so number is 6
			int resultCellNumber = 6;
			if (includeRunData) {
				//NOTE run data takes up 2 columns so increase number by 2
				resultCellNumber += 2;
			}
			if (includeStudentData) {
				//NOTE run data takes up 7 columns so increase number by 7
				resultCellNumber += 7;
			}
			for (List<String> resultRow : queryResult) {
				Row row = sheet.getRow(rowNum);
				if (row == null) {
					row = sheet.createRow(rowNum);
				}
				int colNum = colOffset;
				int cellNumber = 0;
				for (String resultCell : resultRow) {
					CellStyle style = null;
					if (rowNum == rowOffset) {
						style = headerCellStyle;
					}
					excelHelper.createOrUpdateXLSXCell(row, colNum, null, style, resultCell, true); // FIXME boolean added as a workaround

					if (cellNumber == resultCellNumber && rowNum > rowOffset && !splitAttributeValuesOn.equals("")) {
						//NOTE only split attribute values and don't split header
						int splitColNum = splitAttributeValuesColOffset; 						
						String[] resultCellArr = resultCell.split(splitAttributeValuesOn);
						for (int i=0;i<resultCellArr.length;i++) {
							excelHelper.createOrUpdateXLSXCell(row, splitColNum, null, null, resultCellArr[i], true); // FIXME boolean added as a workaround
							splitColNum++;
						}
					}
					colNum++;
					cellNumber++;
				}
				rowNum++;
			}

			if (createNewSheet) {
				// Resize all columns to fit the content size
				for(int i = colOffset; i < (colOffset + maxCols); i++) {
					sheet.autoSizeColumn(i);
				}
			}
		}

		return excelHelper.writeAndCloseWorkbook(workbook, getAbsoluteTempPath(), fileName, getRoot(), vView);
	}

	public String getFirstQueryFileNameInList(List<String> fileNames) {
		String fileName = "";
		if (fileNames != null && fileNames.size() > 0) {
			int i = 0;
			while (i < fileNames.size() && !fileNames.get(i).endsWith(".query")) {
				i++;
			}
			if (i < fileNames.size()) {
				fileName = fileNames.get(i);
			}
		}
		return fileName;
	}

	public Listitem addListitem(Listbox parent, Object value, String label, boolean addToSelection) {
		Listitem listitem = new Listitem();
		listitem.setValue(value);
		Listcell listcell = new Listcell(label);
		listitem.appendChild(listcell);
		//NOTE insert listitem in such a way that listitems are ordered alphabetically
		int index = -1;
		for (int i=0;i<parent.getItemCount();i++) {
			if (label.compareTo(((Listcell)parent.getItemAtIndex(i).getChildren().get(0)).getLabel()) <= 0) {
				//The first listitem with a label 'greater' than parameter label
				index = i;
				break;
			}
		}
		if (index == -1) {
			//No listitem with a label 'greater' than parameter label found
			parent.appendChild(listitem);
		}
		else {
			parent.insertBefore(listitem, parent.getItemAtIndex(index));
		}
		if (addToSelection) {
			parent.addItemToSelection(listitem);
		}
		return listitem;
	}

	/* Query functions */

	public List<List<String>> addQueryHeader(List<List<String>> queryResult, boolean includeRunData, boolean includeStudentData, boolean includeDefaultValues) {
		List<String> header = new ArrayList<String>();
		if (includeRunData) {
			header.add(vView.getLabel("tut_rungroupaccountownoverview.runcode"));
			header.add(vView.getLabel("tut_rungroupaccountownoverview.runname"));
		}
		header.add(vView.getLabel("tut_rungroupaccountownoverview.rgaid"));
		header.add(vView.getLabel("tut_rungroupaccountownoverview.active"));
		if (includeStudentData) {
			header.add(vView.getLabel("userid"));
			header.add(vView.getLabel("studentid"));
			header.add(vView.getLabel("title"));
			header.add(vView.getLabel("initials"));
			header.add(vView.getLabel("nameprefix"));
			header.add(vView.getLabel("lastname"));
			header.add(vView.getLabel("email"));
		}
		header.add(vView.getLabel("casecomponent"));
		header.add(vView.getLabel("tut_rungroupaccountownoverview.tagname"));
		header.add(vView.getLabel("tut_rungroupaccountownoverview.tagkeyvalue"));
		header.add(vView.getLabel("tut_rungroupaccountownoverview.key"));
		header.add(vView.getLabel("tut_rungroupaccountownoverview.value"));
		if (includeDefaultValues) {
			header.add(vView.getLabel("tut_rungroupaccountownoverview.time.defaultincluded"));
		}
		else {
			header.add(vView.getLabel("tut_rungroupaccountownoverview.time"));
		} 
		queryResult.add(0, header);
		return queryResult;
	}

	public List<List<String>> sortQueryResult(List<List<String>> queryResult, String columnsToSortOn, boolean includeRunData, boolean includeStudentData) {
		if (columnsToSortOn == null || columnsToSortOn.equals("")) {
			return queryResult;
		}
		String[] columns = columnsToSortOn.split(",");
		List<Integer> indices = new ArrayList<Integer>();
		int numberOfRunColumns = 0;
		if (includeRunData) {
			//NOTE run data takes up 2 columns
			numberOfRunColumns = 2;
		}
		//NOTE rga id and active
		int numberOfRgaColumns = 2;
		int numberOfStudentColumns = 0;
		if (includeStudentData) {
			//NOTE student data takes up 7 columns
			numberOfStudentColumns = 7;
		}
		int numberOfDataColumns = 6;
		
		int offsetRunData = 0;
		int offsetRgaData = numberOfRunColumns;
		int offsetStudentData = offsetRgaData + numberOfRgaColumns;
		int offsetData = offsetStudentData + numberOfStudentColumns;

		int rgaidIndex = offsetRgaData;
		int timeIndex = offsetData + numberOfDataColumns - 1;
		for (int i=0;i<columns.length;i++) {
			if (includeRunData && columns[i].equals("runcode")) {
				indices.add(offsetRunData);
			}
			else if (includeRunData && columns[i].equals("runname")) {
				indices.add(offsetRunData + 1);
			}
			else if (columns[i].equals("rgaid")) {
				indices.add(offsetRgaData);
			}
			else if (columns[i].equals("active")) {
				indices.add(offsetRgaData + 1);
			}
			else if (includeStudentData && columns[i].equals("userid")) {
				indices.add(offsetStudentData);
			}
			else if (includeStudentData && columns[i].equals("studentid")) {
				indices.add(offsetStudentData + 1);
			}
			else if (includeStudentData && columns[i].equals("title")) {
				indices.add(offsetStudentData + 2);
			}
			else if (includeStudentData && columns[i].equals("initials")) {
				indices.add(offsetStudentData + 3);
			}
			else if (includeStudentData && columns[i].equals("nameprefix")) {
				indices.add(offsetStudentData + 4);
			}
			else if (includeStudentData && columns[i].equals("lastname")) {
				indices.add(offsetStudentData + 5);
			}
			else if (includeStudentData && columns[i].equals("email")) {
				indices.add(offsetStudentData + 6);
			}
			else if (columns[i].equals("cacname")) {
				indices.add(offsetData);
			}
			else if (columns[i].equals("tagname")) {
				indices.add(offsetData + 1);
			}
			else if (columns[i].equals("tagkeyvalue")) {
				indices.add(offsetData + 2);
			}
			else if (columns[i].equals("key")) {
				indices.add(offsetData + 3);
			}
			else if (columns[i].equals("value")) {
				indices.add(offsetData + 4);
			}
			else if (columns[i].equals("time")) {
				indices.add(offsetData + 5);
			}
		}
		if (indices.size() == 0) {
			return queryResult;
		}

		TreeMap<String,List<String>> sortedData = new TreeMap<String,List<String>>();
		for (List<String> result : queryResult) {
			String key = "";
			for (int index : indices) {
				if (index == rgaidIndex || index == timeIndex) {
					String number = result.get(index);
					while (number.length() < 12) {
						number = "0" + number;
					}
					key += number + "_";
				}
				else {
					key += result.get(index) + "_";
				}
			}
			//NOTE a key may exist multiple times, e.g. if only sorted on tagname.
			//if so add a number to the key so will be unique
			int counter = 1;
			String tempKey = key;
			while (sortedData.containsKey(tempKey)) {
				tempKey = key + "_" + counter;
				counter++;
			}
			if (!tempKey.equals(key)) {
				key = tempKey;
			}
			sortedData.put(key, result);
		}

		List<List<String>> sortedQueryResult = new ArrayList<List<String>>();
		Iterator<Entry<String, List<String>>> iterator = sortedData.entrySet().iterator();
		while(iterator.hasNext() ) {
			sortedQueryResult.add(iterator.next().getValue());
		}

		return sortedQueryResult;
	}

}
