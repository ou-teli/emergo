/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.tut.cases.Gamebrics;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zul.Include;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.IAppManager;
import nl.surf.emergo.business.IRunAccountManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefListbox;
import nl.surf.emergo.control.tut.landingpages.CTutRunsFinishedHelper;
import nl.surf.emergo.domain.ECaseComponent;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunAccount;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpring;

/**
 * The Class CTutGBLogAssessmentsLb.
 */
public class CTutGBLogAssessmentsLb extends CDefListbox {

	private static final long serialVersionUID = -8680355406819802377L;

	protected VView vView = CDesktopComponents.vView();

	protected CControl cControl = CDesktopComponents.cControl();

	protected SSpring sSpring = CDesktopComponents.sSpring();

	protected List<IERun> runs = new ArrayList<IERun>();

	protected List<List<String>> jspResult = new ArrayList<List<String>>();
	
	protected String finishedCacName = "M_FINAL states"; 
	protected String prePostAssName = "GB_items_domein"; 

	protected String finishedTagKey = "boolReady"; 

	public void onCreate(CreateEvent aEvent) {
		getItems().clear();
		List<IERunAccount> lRunAccounts = ((IRunAccountManager)CDesktopComponents.sSpring().getBean("runAccountManager")).getAllRunAccountsByAccIdActiveAsTutor(CDesktopComponents.cControl().getAccId());
		int i = 0;
		for (IERunAccount lRunAccount : lRunAccounts) {
			IERun lRun = lRunAccount.getERun();
			if (!runs.contains(lRun)) {
				runs.add(lRun);
				Listitem lListitem = new Listitem();
				lListitem.setValue(lRun);
				lListitem.appendChild(new Listcell(lRun.getName()));
				this.appendChild(lListitem);
				if (i == 0) {
					this.addItemToSelection(lListitem);
				}
				i++;
			}
		}
		restoreSettings();
		onSelect();
	}

	public void onSelect() {
		initJspResult();
		fillExcelResult(runs);
	}

	void initJspResult() {
		jspResult.clear();
		List<String> headerResult = new ArrayList<String>();
		headerResult.add("naam");
		headerResult.add("studentnummer");
		headerResult.add("datum van afronding (j-m-d)");
		headerResult.add("run code");
		jspResult.add(headerResult);
	}

	void fillExcelResult(List<IERun> runs) {
		if (runs.size() == 0) {
			return;
		}
		//NOTE use first run to get casId
		IERun firstRun = runs.get(0);

		//get all case components. In this case only one with name <finishedCacName>.
		List<IECaseComponent> tutCaseComponents = new ArrayList<IECaseComponent>();
		List<String> errors = new ArrayList<String>();
		List<Object> sqlCaseComponents = ((IAppManager)sSpring.getBean("appManager")).getSqlResult("SELECT * FROM `casecomponents` WHERE casCasId=" + firstRun.getECase().getCasId() + " AND name='" + prePostAssName + "';", ECaseComponent.class, errors);
		if (sqlCaseComponents != null && sqlCaseComponents.size() == 1) {
			tutCaseComponents.add((IECaseComponent)sqlCaseComponents.get(0));
		}

		//determine status to retrieve
		String tutTagName = "state";
		String tutTagKey = finishedTagKey;
		String tutStatusKey = AppConstants.statusKeyValue;

		//get rugIds for all runs
		String sqlPart = "";
		for (IERun run : runs) {
			if (!sqlPart.equals("")) {
				sqlPart += ",";
			}
			sqlPart += run.getRunId();
		} 
		sqlPart = "WHERE runRunId in (" + sqlPart + ")";
		List<Object> sqlRugIds = ((IAppManager)sSpring.getBean("appManager")).getSqlResult("SELECT DISTINCT rugId FROM `rungroups` " + sqlPart + ";", errors);
		List<Integer> rugIds = new ArrayList<Integer>();
		for (Object sqlRugId : sqlRugIds) {
			rugIds.add((Integer)sqlRugId);
		}

		//get sql data that will be presented
		sqlPart = "";
		for (Object rugId : rugIds) {
			if (!sqlPart.equals("")) {
				sqlPart += ",";
			}
			sqlPart += rugId;
		}
		if (!sqlPart.equals("")) {
			sqlPart = "AND rungroups.rugId in (" + sqlPart + ") ";
		}
		String sqlQuery = "SELECT rungroups.rugId,accounts.userid,accounts.lastname,accounts.nameprefix,accounts.initials,runs.code " +
				"FROM `accounts` " +
				"INNER JOIN `rungroupaccounts` " +
				"INNER JOIN `rungroups` " +
				"INNER JOIN `runs` " +
				"WHERE rungroupaccounts.accAccId=accounts.accId " +
				"AND rungroups.rugId=rungroupaccounts.rugRugId " +
				"AND rungroups.active=1 " +
				"AND runs.runId=rungroups.runRunId " +
				sqlPart +
				"ORDER BY accounts.lastname;";
		List<Object> sqlResults = ((IAppManager)sSpring.getBean("appManager")).getSqlResult(sqlQuery, errors);

		//render sql data
		CTutRunsFinishedHelper finishedHelper = new CTutRunsFinishedHelper();
		finishedHelper.renderItems(tutCaseComponents, tutTagName, tutTagKey, tutStatusKey, rugIds, sqlResults, finishedCacName, jspResult);
		Executions.getCurrent().setAttribute("jspResult", jspResult);
		((Include)vView.getComponent("excelOverview")).setSrc("");
		((Include)vView.getComponent("excelOverview")).setSrc("tut_runs_finished.jsp");
	}

}
