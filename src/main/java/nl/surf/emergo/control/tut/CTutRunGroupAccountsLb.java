/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.tut;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import nl.surf.emergo.business.IRunGroupAccountManager;
import nl.surf.emergo.business.IRunGroupManager;
import nl.surf.emergo.business.IRunTeamManager;
import nl.surf.emergo.business.IRunTeamRunGroupManager;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefListbox;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.domain.IERunTeam;
import nl.surf.emergo.domain.IERunTeamRunGroup;

/**
 * The Class CTutRunGroupAccountsLb.
 */
public class CTutRunGroupAccountsLb extends CDefListbox {

	private static final long serialVersionUID = 6211959992466816002L;

	/** The run group ids. */
	protected List<Integer> runGroupIds = null;

	/** The run team run groups. */
	protected List<IERunTeamRunGroup> runTeamRunGroups = null;

	/** The run teams. */
	protected List<IERunTeam> runTeams = null;

	/**
	 * Gets the helper.
	 *
	 * @return the helper
	 */
	protected CTutRunGroupAccountsHelper getHelper() {
		return (new CTutRunGroupAccountsHelper());
	}

	/**
	 * On create render listitems.
	 */
	public void onCreate() {
		getItems().clear();
		int lRunId = Integer.parseInt((String)CDesktopComponents.cControl().getAccSessAttr("runId"));
		runTeams = ((IRunTeamManager)CDesktopComponents.sSpring().getBean("runTeamManager")).getAllRunTeamsByRunId(lRunId);
		List<IERunGroup> lRunGroups = ((IRunGroupManager)CDesktopComponents.sSpring().getBean("runGroupManager")).getAllRunGroupsByRunId(lRunId);
		List<Integer> lRugIds = new ArrayList<Integer>();
		for (IERunGroup lRunGroup : lRunGroups) {
			if (!lRugIds.contains(lRunGroup.getRugId()))
				lRugIds.add(lRunGroup.getRugId());
		}
		List<IERunGroupAccount> lItems = ((IRunGroupAccountManager)CDesktopComponents.sSpring().getBean("runGroupAccountManager")).getAllRunGroupAccountsByRugIds(lRugIds);
		List<IEAccount> lAccounts = new ArrayList<IEAccount>();
		runGroupIds = new ArrayList<Integer>();
		List<Hashtable<String,Object>> lStuItems = new ArrayList<Hashtable<String,Object>>(0);
		for (IERunGroupAccount lRunGroupAccount : lItems) {
			IEAccount lAccount = lRunGroupAccount.getEAccount();
			if (lAccount.getActive() && (!lAccounts.contains(lAccount))) {
				List<IERunGroupAccount> lRunGroupAccounts = getRunGroupAccounts(lAccount.getAccId(),lRunId,lItems);
				if (lRunGroupAccounts.size() > 0) {
					lAccounts.add(lAccount);
					if (!runGroupIds.contains(lRunGroupAccount.getERunGroup().getRugId()))
						runGroupIds.add(lRunGroupAccount.getERunGroup().getRugId());
					Hashtable<String,Object> lStuItem = new Hashtable<String,Object>(0);
					lStuItem.put("item",lAccount);
					lStuItem.put("rungroupaccounts",lRunGroupAccounts);
					lStuItem.put("runteams",getRunTeams(lRunGroupAccounts));
					lStuItem.put("runid",lRunId);
					lStuItems.add(lStuItem);
				}
			}
		}
		CTutRunGroupAccountsHelper cHelper = getHelper();
		for (Hashtable<String,Object> lStuItem : lStuItems) {
			cHelper.renderItem(this, null, lStuItem);
		}

		restoreSettings();
	}

	/**
	 * Gets the active run group accounts out of aRunGroupAccounts for aAccId and aRunId.
	 *
	 * @param aAccId the a acc id
	 * @param aRunId the a run id
	 * @param aRunGroupAccounts the a run group accounts
	 *
	 * @return the run group accounts
	 */
	protected List<IERunGroupAccount> getRunGroupAccounts(int aAccId, int aRunId, List<IERunGroupAccount> aRunGroupAccounts) {
		List<IERunGroupAccount> lRunGroupAccounts = new ArrayList<IERunGroupAccount>(0);
		for (IERunGroupAccount lRunGroupAccount : aRunGroupAccounts) {
			IEAccount lAccount = lRunGroupAccount.getEAccount();
			IERunGroup lRunGroup = (IERunGroup)lRunGroupAccount.getERunGroup();
			// tutor can also see inactive accounts
			if ((lAccount.getAccId() == aAccId) && (lRunGroup.getERun().getRunId() == aRunId)) {
//			if ((lRunGroup.getActive()) && (lAccount.getAccId() == aAccId) && (lRunGroup.getERun().getRunId() == aRunId)) {
				lRunGroupAccounts.add(lRunGroupAccount);
			}
		}
		return lRunGroupAccounts;
	}


	/**
	 * Gets the run teams for aRunGroupAccounts.
	 *
	 * @param aRunGroupAccounts the a run group accounts
	 *
	 * @return the run teams
	 */
	public List<IERunTeam> getRunTeams(List<IERunGroupAccount> aRunGroupAccounts) {
		List<IERunTeam> lRunTeams = new ArrayList<IERunTeam>(0);
		Hashtable<String,IERunTeam> lRunTeamsHash = new Hashtable<String,IERunTeam>(0);
		for (IERunGroupAccount lRunGroupAccount : aRunGroupAccounts) {
			if (runTeamRunGroups == null)
				runTeamRunGroups = ((IRunTeamRunGroupManager)CDesktopComponents.sSpring().getBean("runTeamRunGroupManager")).getAllRunTeamRunGroupsByRugIds(runGroupIds);
			List<IERunTeamRunGroup> lItems2 = runTeamRunGroups;
			for (IERunTeamRunGroup lRunTeamRunGroup : lItems2) {
				if (lRunTeamRunGroup.getRugRugId() == lRunGroupAccount.getERunGroup().getRugId()) {
					IERunTeam lRunTeam = null;
					for (IERunTeam lRut : runTeams) {
						if (lRut.getRutId() == lRunTeamRunGroup.getRutRutId())
							lRunTeam = lRut;
					}
					if (lRunTeam != null) {
						lRunTeamsHash.put(""+lRunTeam.getRutId(), lRunTeam);
					}
				}
			}
		}
		for (Enumeration<String> lKeys = lRunTeamsHash.keys(); lKeys.hasMoreElements();) {
			lRunTeams.add(lRunTeamsHash.get(lKeys.nextElement()));
		}
		return lRunTeams;
	}
}
