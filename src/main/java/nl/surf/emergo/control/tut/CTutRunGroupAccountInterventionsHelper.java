/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.tut;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Html;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefListcell;
import nl.surf.emergo.control.run.CRunBlobLabel;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERunGroupAccount;

/**
 * The Class CTutRunGroupAccountInterventionsHelper.
 */
public class CTutRunGroupAccountInterventionsHelper extends CTutRunGroupAccountsHelper {

	/** The conversations case components. */
	private List<IECaseComponent> conversationsCaseComponents = null;

	/**
	 * Renders sent interventions for one account.
	 * 
	 * @param aListbox the a listbox
	 * @param aInsertBefore the a insert before
	 * @param aItem the a item
	 */
	public void renderItem(Listbox aListbox,Listitem aInsertBefore,Hashtable<String,Object> aItem) {
		Listitem lListitem = super.newListitem();
		List<IERunGroupAccount> lRunGroupAccounts = (List<IERunGroupAccount>)aItem.get("rungroupaccounts");
		IEAccount lItem = (IEAccount)aItem.get("item");
		lListitem.setValue(lItem);
		super.appendListcell(lListitem,lItem.getTitle());
		super.appendListcell(lListitem,lItem.getInitials());
		super.appendListcell(lListitem,lItem.getNameprefix());
		super.appendListcell(lListitem,lItem.getLastname());
		Listcell lListcell = new CDefListcell();

		List<Component> lInterventionLabels = new ArrayList<Component>(0);
		for (int i=0;i<lRunGroupAccounts.size();i++) {
			CDesktopComponents.sSpring().setRunStatus(AppConstants.runStatusRun);
			CDesktopComponents.sSpring().setRunGroupAccount((IERunGroupAccount)lRunGroupAccounts.get(i));
			if (conversationsCaseComponents == null)
				conversationsCaseComponents = CDesktopComponents.sSpring().getCaseComponentsByComponentCode("conversations");
			if (conversationsCaseComponents == null || conversationsCaseComponents.size() == 0)
				return;
			for (IECaseComponent lConversationsCaseComponent : conversationsCaseComponents) {
				List<IXMLTag> lTags = CDesktopComponents.cScript().getRunGroupNodeTags(""+lConversationsCaseComponent.getCacId(), "intervention");
				for (IXMLTag lTag : lTags) {
					String lHref = CDesktopComponents.sSpring().getSBlobHelper().getUrl(lTag);
					if (!lHref.equals("")) {
						CRunBlobLabel lLabel = new CRunBlobLabel();
						lLabel.setSpring(CDesktopComponents.sSpring());
						lLabel.setBlobContainer(lTag);
						lLabel.init();
						if (lInterventionLabels.size() > 0)
							lInterventionLabels.add(new Html("<br/>"));
						lInterventionLabels.add(new Html("- "));
						lInterventionLabels.add(lLabel);
					}
				}
			}
		}

		for (Component lInterventionLabel : lInterventionLabels) {
			lListcell.appendChild(lInterventionLabel);
		}

		lListitem.appendChild(lListcell);
		super.insertListitem(aListbox,lListitem,aInsertBefore);
	}

}