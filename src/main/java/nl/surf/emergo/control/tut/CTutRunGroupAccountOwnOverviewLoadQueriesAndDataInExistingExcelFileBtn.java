/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.tut;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

import nl.surf.emergo.business.impl.FileManager;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefButton;

/**
 * The Class CTutRunGroupAccountOwnOverviewLoadQueriesAndDataInExistingExcelFileBtn.
 */
public class CTutRunGroupAccountOwnOverviewLoadQueriesAndDataInExistingExcelFileBtn extends CDefButton {

	private static final long serialVersionUID = -5021350137550162141L;

	protected CTutRunGroupAccountOwnOverviewWnd root = (CTutRunGroupAccountOwnOverviewWnd)CDesktopComponents.vView().getComponent("ownOverviewWnd");

	public void onUpload(UploadEvent event) {
		if (event.getMedia() == null) {
			root.vView.showMessagebox(getRoot(), root.vView.getLabel("tut_rungroupaccountownoverview.messagebox.text.fileuploaderror"), 
					root.vView.getLabel("messagebox.title.errors"), Messagebox.OK, Messagebox.ERROR);
			return;
		} 

		root.queryResults = new ArrayList<HashMap<String,Object>>();

		//initialize progress values
		((Label)root.vView.getComponent("progress")).setValue("");
		((Label)root.vView.getComponent("queryProgress")).setValue("");

		if (event.getMedia() != null) {
			root.fileHelper.setNotifyComponent(this);
			
			String absoluteTempPath = root.getAbsoluteTempPath();
			//upload zip file
			if (root.fileHelper.uploadMedias(new Media[]{event.getMedia()}, absoluteTempPath, "", false, false)) {
				Events.postEvent("onSucces", this, event.getMedias());
			}
		}
	}

	public void onSucces(Event event) {
		root.fileHelper.setNotifyComponent(null);
		
		String fileName = ((Media[])event.getData())[0].getName();
		String absoluteTempPath = root.getAbsoluteTempPath();
		String uploadFileName = absoluteTempPath + fileName;
		//get file names in zip
		List<String> fileNamesInZip = root.zipHelper.getFileNamesInZip(new File(uploadFileName));
		String excelFileName = "";
		for (String tempFileName : fileNamesInZip) {
			if (tempFileName.endsWith(".xlsx")) {
				excelFileName = tempFileName;
			}
		}
		//if excel file in zip file unpack files in zip to temporary path
		if (!excelFileName.equals("")) {
			if (root.zipHelper.unpackageZipToFiles(absoluteTempPath, new File(uploadFileName))) {
				//remove excel file name from file names in zip
				fileNamesInZip.remove(excelFileName);
				//initialize queries process and progress
				setAttribute("excelFileName", excelFileName);
				setAttribute("fileNamesInZip", fileNamesInZip);
				setAttribute("fileNamesInZipSize", fileNamesInZip.size());
				setAttribute("fileNamesInZipCounter", 0);
				//handle first query
				Events.postEvent("onHandleQuery", this, null);
			}
			else {
				root.vView.showMessagebox(getRoot(), root.vView.getLabel("tut_rungroupaccountownoverview.messagebox.text.fileuploaderror").replace("%1", fileName), 
						root.vView.getLabel("messagebox.title.errors"), Messagebox.OK, Messagebox.ERROR);
			}
		}
		else {
			root.vView.showMessagebox(getRoot(), root.vView.getLabel("tut_rungroupaccountownoverview.messagebox.text.filecreationerror").replace("%1", fileName), 
					root.vView.getLabel("messagebox.title.errors"), Messagebox.OK, Messagebox.ERROR);
		}
	}

	public void onHandleQuery() {
		List<String> fileNamesInZip = (List<String>)getAttribute("fileNamesInZip");
		//get query
		String fileName = root.getFirstQueryFileNameInList(fileNamesInZip);
		if (!fileName.equals("")) {
			//if query execute it
			//remove it from file names to be handled
			fileNamesInZip.remove(fileName);
			//read query file
			String absoluteTempPath = root.getAbsoluteTempPath();
			FileManager fileManager = new FileManager();
			byte[] bytes = fileManager.readFile(absoluteTempPath + fileName);
			//load query
			if (root.loadQuery(fileName, bytes)) {
				//delete query file in temporary folder
				fileManager.deleteFile(absoluteTempPath + fileName);
				//update query input elements
				Events.postEvent("onUpdate", root.vView.getComponent("caseComponents"), null);
				((Textbox)root.vView.getComponent("queryFileName")).setValue(fileName);
				root.cControl.setAccSessAttr("tutQueryFileName", fileName);
				root.typeOfOverview = "loadQueriesAndDataInExistingExcelFile";
				//execute query
				Events.postEvent("onUpdate", root.vView.getComponent("dataListbox"), getAttribute("excelFileName"));
			}
		}
		else {
			//no query files left
			fileNamesInZip.clear();
		}
	}
	
	public void onReady() {
		List<String> fileNamesInZip = (List<String>)getAttribute("fileNamesInZip");
		//get query
		String fileName = root.getFirstQueryFileNameInList(fileNamesInZip);
		if (!fileName.equals("")) {
			//if still query handle it
			Events.postEvent("onHandleQuery", this, null);
		}
		else {
			//create excel file
			fileName = (String)getAttribute("excelFileName");
			if (!root.createOrUpdateWorkbook(root.queryResults, false, fileName)) {
				root.vView.showMessagebox(getRoot(), root.vView.getLabel("tut_rungroupaccountownoverview.messagebox.text.filecreationerror").replace("%1", fileName), 
						root.vView.getLabel("messagebox.title.errors"), Messagebox.OK, Messagebox.ERROR);
			}
			else {
				//download excel file
				root.downloadFile(root.getAbsoluteTempPath(), fileName);
			}
			root.queryResults = new ArrayList<HashMap<String,Object>>();
		}
		//update progress
		setAttribute("fileNamesInZipCounter", (Integer)getAttribute("fileNamesInZipCounter") + 1);
	}
	
	public void onProgress(Event event) {
		//update progress
		long detailedPercentage = (Long)event.getData();
		long percentage = ((100 * (Integer)getAttribute("fileNamesInZipCounter")) + detailedPercentage) / (Integer)getAttribute("fileNamesInZipSize");
		((Label)root.vView.getComponent("queryProgress")).setValue("" + percentage + "%");
	}
	
}
