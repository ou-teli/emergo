/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.tut.cases.Sexology;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.zkspring.SSpring;

public class CTutRunsSxLegacyHelper {

	protected CControl cControl = CDesktopComponents.cControl();
	protected SSpring sSpring = CDesktopComponents.sSpring();

	public boolean evaluateCondition(IECaseComponent aCaseComponent, IXMLTag aDataTag, String aKey, int aKeyInd, int aFunctionInd, String aValue, int aOperatorInd, String aOperatorValue, String aOperatorValueType) {
		String lComId = "" + aCaseComponent.getEComponent().getComId();
		String lCacId = "" + aCaseComponent.getCacId();
		String lTagName = "";
		if (aDataTag != null) {
			lTagName = aDataTag.getName();
		}
		boolean lStringValueInput = aOperatorValueType.equals("string");
		if (aOperatorValue.equals("") && !lStringValueInput)
			// value(s) cannot be empty, except string values
			return false;
		String lFunction = sSpring.getAppManager().getFunction(aFunctionInd);
		String lOperator = sSpring.getAppManager().getOperator(aOperatorInd);
		double lStatus = -1;
		String lStatusStr = "@@@";
		String lInputType = "";
		Boolean lNumberError = false;
		Boolean lNumberCheck = false;
		if (aDataTag != null) {
			if (lFunction.equals("")) {
				if (aCaseComponent.getEComponent().getCode().equals("states") && lTagName.equals("state")) {
					//NOTE States have their own type which is saved in value
					String lStateType = aDataTag.getChildValue("keytype");
					if (lStateType.equals("string")) {
						lInputType = "string";
					}
					else if (lStateType.equals("boolean")) {
						lInputType = "boolean";
					}
					else if (lStateType.equals("number")) {
						lInputType = "int";
					}
					else if (lStateType.equals("time")) {
						lInputType = "time";
					}
				}
				else {
					lInputType = sSpring.getAppManager().getTagOperatorValueType(lComId, lCacId, lTagName, aKeyInd + "", aFunctionInd + "");
				}
				if (lInputType.equalsIgnoreCase("string")) {
					lStatusStr = aDataTag.getCurrentStatusAttribute(aKey);
					//NOTE replace possible crlf constants by \n, status value may be open question answer containing crlfs.
					lStatusStr = lStatusStr.replace(AppConstants.statusCrLfReplace, "\n");
				}
				else if (lInputType.equalsIgnoreCase("int")) {
					lNumberCheck = true;
					try {
						lStatus = Double.parseDouble(aDataTag.getCurrentStatusAttribute(aKey));
					} catch (NumberFormatException e) {
						lNumberError = true;
					}
				}
				else
				{
					if (lInputType.equalsIgnoreCase("boolean")) {
						lStatus = sSpring.getAppManager().getStatusValueIndex(aDataTag.getCurrentStatusAttribute(aKey));
					}
					else {
						lNumberCheck = true;
						try {
							lStatus = Double.parseDouble("" + aDataTag.getCurrentStatusAttributeTime(aKey));
						} catch (NumberFormatException e) {
							lNumberError = true;
						}
					}
				}
			}
			if (lFunction.equals("count")) {
				// NOTE To keep supporting old situation where count was a count of value true, set default value to true.
				if (aValue.equals("")) {
					aValue = AppConstants.statusValueTrue;
				}
				lStatus = aDataTag.getStatusAttributeCount(aKey, aValue);
			}
		}

		if (lNumberError || (!lNumberCheck && (lStatus == -1) && !lInputType.equalsIgnoreCase("string")))
			return false;
		if ((lOperator.equals("eq")) || (lOperator.equals("gt"))
			|| (lOperator.equals("ge")) || (lOperator.equals("lt"))
			|| (lOperator.equals("le"))) {
			if (lInputType.equalsIgnoreCase("string")) {
				if (lOperator.equals("eq")) {
					if (lStatusStr.indexOf("\n") == -1) {
						return lStatusStr.matches(aOperatorValue);
					}
					else {
						//NOTE to match on line terminators pattern has to be used with DOTALL
						Pattern lPattern = Pattern.compile(aOperatorValue, Pattern.DOTALL);
						Matcher lMatcher = lPattern.matcher(lStatusStr);
						return lMatcher.matches();
					}
				}
				else if ((lOperator.equals("gt") || lOperator.equals("ge")) && lStatusStr.compareTo(aOperatorValue) > 0) {
					return true;
				}
				else if ((lOperator.equals("lt") || lOperator.equals("le")) && lStatusStr.compareTo(aOperatorValue) < 0) {
					return true;
				}
				return false;
			}
			// and must be number
			double lOperatorValue = -1;
			try {
				lOperatorValue = Double.parseDouble(aOperatorValue);
			} catch (NumberFormatException e) {
				if (lInputType.equalsIgnoreCase("boolean")) {
					if (aOperatorValue.equalsIgnoreCase(AppConstants.statusValueTrue))
						lOperatorValue = 1;
					else
						// assume false
						lOperatorValue = 0;
				}
				else
					// not a number
					return false;
			}
			if (lOperator.equals("eq") && lStatus == lOperatorValue)
				return true;
			else if (lOperator.equals("gt") && lStatus > lOperatorValue)
				return true;
			else if (lOperator.equals("ge") && lStatus >= lOperatorValue)
				return true;
			else if (lOperator.equals("lt") && lStatus < lOperatorValue)
				return true;
			else if (lOperator.equals("le") && lStatus <= lOperatorValue)
				return true;
		}
		return false;
	}
	
}
