/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.tut;

import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.control.CControlHelper;
import nl.surf.emergo.control.def.CDefListcell;
import nl.surf.emergo.domain.IERun;

/**
 * The Class CTutRunsHelper.
 */
public class CTutRunsHelper extends CControlHelper {
	
	/**
	 * Renders one run with redirect buttons for run group accounts, their sent emails
	 * and their finished tasks.
	 * 
	 * @param aListbox the a listbox
	 * @param aInsertBefore the a insert before
	 * @param aItem the a item
	 */
	public void renderItem(Listbox aListbox,Listitem aInsertBefore,IERun aItem) {
		Listitem lListitem = super.newListitem();
		lListitem.setValue(aItem);
		showTooltiptextIfAdmin(lListitem, "runId=" + aItem.getRunId());
		super.appendListcell(lListitem,aItem.getName());
		String lName = aItem.getECase().getName();
		Listcell lCell = super.appendListcell(lListitem,lName);
		showTooltiptextIfAdmin(lCell, "casId=" + aItem.getECase().getCasId());
		super.appendListcell(lListitem,""+aItem.getActive());
		super.appendListcell(lListitem,getDateStrAsYMD(aItem.getStartdate()));
		super.appendListcell(lListitem,getDateStrAsYMD(aItem.getEnddate()));
		Listcell lListcell = new CDefListcell();
		CTutRunRedirectBtn lRedirect = new CTutRunRedirectBtn();
		lListcell.appendChild(lRedirect);
		lListitem.appendChild(lListcell);
		lListcell = new CDefListcell();
		CTutRunRedirectRunGroupAccountEmailsBtn lRedirect2 = new CTutRunRedirectRunGroupAccountEmailsBtn();
		lListcell.appendChild(lRedirect2);
		lListitem.appendChild(lListcell);
		lListcell = new CDefListcell();
		CTutRunRedirectRunGroupAccountTasksBtn lRedirect3 = new CTutRunRedirectRunGroupAccountTasksBtn();
		lListcell.appendChild(lRedirect3);
		lListitem.appendChild(lListcell);
		/*NOTE not used anymore
		lListcell = new CDefListcell();
		CTutRunRedirectRunGroupAccountInterventionsBtn lRedirect4 = new CTutRunRedirectRunGroupAccountInterventionsBtn();
		lListcell.appendChild(lRedirect4);
		lListitem.appendChild(lListcell);
		*/
		super.insertListitem(aListbox,lListitem,aInsertBefore);
	}
}