/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.tut.landingpages;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import nl.surf.emergo.business.IRunGroupAccountManager;
import nl.surf.emergo.business.IRunGroupCaseComponentManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.domain.IERunGroupCaseComponent;
import nl.surf.emergo.zkspring.SSpring;

public class CTutRunsFinishedHelper {

	protected SSpring sSpring = CDesktopComponents.sSpring();
	protected String finishedCacName = ""; 
	protected String finishedKeyName = "state"; 
	protected String finishedKeyValue = "strDateFinished"; 
	
	public void renderItems(List<IECaseComponent> tutCaseComponents, String tutTagName, String tutTagKey, 
							String tutStatusKey, List<Integer> rugIds, List<Object> sqlResults, String pFinishedCacName, List<List<String>> jspResult) {
		if (tutCaseComponents == null || tutCaseComponents.size() == 0 ||
				tutStatusKey == null || tutStatusKey.isEmpty() ||
				rugIds == null || rugIds.size() == 0 ||
				sqlResults == null || sqlResults.size() == 0) {
			return;
		}
		
		finishedCacName = pFinishedCacName;

		//get run group case components						
		List<Integer> cacIds = new ArrayList<Integer>();
		for (IECaseComponent caseComponent : tutCaseComponents) {
			if (!cacIds.contains(caseComponent.getCacId())) {
				cacIds.add(caseComponent.getCacId());
			}
		}
		List<IERunGroupCaseComponent> runGroupCaseComponents = ((IRunGroupCaseComponentManager)CDesktopComponents.sSpring().getBean("runGroupCaseComponentManager"))
						.getAllRunGroupCaseComponentsByRugIdsCacIds(rugIds, cacIds);
		//put in hashtable for faster lookup
		Hashtable<String,IERunGroupCaseComponent> hRunGroupCaseComponents = new Hashtable<String,IERunGroupCaseComponent>();
		for (IERunGroupCaseComponent tempRunGroupCaseComponent : runGroupCaseComponents) {
			hRunGroupCaseComponents.put("" + tempRunGroupCaseComponent.getRugRugId() + "_" + tempRunGroupCaseComponent.getCacCacId(), tempRunGroupCaseComponent);
		}

		//render data
		for (Object sqlResult : sqlResults) {
			//only 1 caseComponent
			for (Integer cacId : cacIds) {
				//NOTE sqlResultArr[0] contains rugId
				Object[] sqlResultArr = (Object[])sqlResult;
				IERunGroupCaseComponent runGroupCaseComponent = hRunGroupCaseComponents.get("" + sqlResultArr[0] + "_" + cacId);
				renderItem(sqlResultArr, runGroupCaseComponent, jspResult, tutTagKey, tutTagName, tutStatusKey);
			} 
		}
	}

	public void renderItem(Object[] sqlResult, IERunGroupCaseComponent runGroupCaseComponent, List<List<String>> jspResult, 
					String pKeyName, String pTagName, String pStatusKey) {
		/*NOTE
			sqlResult[0] = rungroups.rugId
			sqlResult[1] = accounts.userid
			sqlResult[2] = accounts.lastname
			sqlResult[3] = accounts.nameprefix
			sqlResult[4] = accounts.initials
			sqlResult[5] = runs.code
		 */
		String userid = (String)sqlResult[1];
		if (userid.indexOf("ou_") == 0)
			userid = userid.substring(3);
		String name = (String)sqlResult[2];
		if (!sqlResult[4].equals(""))
			name = name + ", " + sqlResult[4];
		if (!sqlResult[3].equals("")) {
			if (!sqlResult[4].equals(""))
				name = name + " " + sqlResult[3];
			else
				name = name + ", " + sqlResult[3];
		}
		String finishedDate = "";

		if (runGroupCaseComponent != null) {
			sSpring.clearRunBoostProps();
			int lRugId = (int)sqlResult[0];
			sSpring.setRun(sSpring.getRunGroup(lRugId).getERun());
			sSpring.setRunStatus(AppConstants.runStatusRun);
			List<IERunGroupAccount> lRgas = ((IRunGroupAccountManager)sSpring.getBean("runGroupAccountManager")).getAllRunGroupAccountsByRugId(lRugId);
			// only one
			sSpring.setRunGroupAccount(lRgas.get(0));
			
			List<String> lErrors = new ArrayList<String>();
			finishedDate = sSpring.getCurrentRunTagStatus(finishedCacName, finishedKeyName, finishedKeyValue, 
									AppConstants.statusKeyValue, AppConstants.statusTypeRunGroup, lErrors);
			if ((finishedDate == null) || (finishedDate.isEmpty())) {
				String lCacName = sSpring.getCaseComponent(runGroupCaseComponent.getCacCacId()).getName();
				String lAfgerond = sSpring.getCurrentRunTagStatus(lCacName, pTagName, pKeyName, pStatusKey, AppConstants.statusTypeRunGroup, lErrors);
				if (lAfgerond.equalsIgnoreCase("true")) {
					finishedDate = "" + runGroupCaseComponent.getLastupdatedate();
					finishedDate = finishedDate.split(" ")[0];
					String[] dateStr = finishedDate.split("-");
					//year-month-date, to be able to sort on last date:
					finishedDate = dateStr[0] + "-" + dateStr[1] + "-" + dateStr[2];
					sSpring.setCurrentRunTagStatus(finishedCacName, finishedKeyName, finishedKeyValue, AppConstants.statusKeyValue, finishedDate, AppConstants.statusTypeRunGroup, false, lErrors);
					sSpring.saveRunCaseComponentsStatus();
				}
			}
		}
		
		String runCode = (String)sqlResult[5];

		List<String> itemResult = new ArrayList<String>();
		itemResult.add(name);
		itemResult.add(userid);
		itemResult.add(finishedDate);
		itemResult.add(runCode);
		jspResult.add(itemResult);
	}

}
