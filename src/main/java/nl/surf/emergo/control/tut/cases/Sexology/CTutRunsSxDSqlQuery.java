/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.tut.cases.Sexology;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.Statement;

import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;
import org.hibernate.internal.SessionFactoryImpl;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.zkspring.SSpring;

public class CTutRunsSxDSqlQuery {

	private Connection pConnection;
	private Statement pStatement;
	protected SSpring sSpring = CDesktopComponents.sSpring();
	
	public ResultSet getSqlRes(String aSqlStr) {
		try {
			SessionFactoryImpl lSessFImp = (SessionFactoryImpl)sSpring.getBean("sessionFactory");
			pConnection = lSessFImp.getSessionFactoryOptions().getServiceRegistry().getService(ConnectionProvider.class).getConnection();
			pStatement = pConnection.createStatement();
			ResultSet lRes = pStatement.executeQuery(aSqlStr);
			return lRes;
		} catch (SQLException lEx) {
			lEx.printStackTrace();
		}
		return null;
	}

	public void closeConnection() {
		try {
			pStatement.close();
			pConnection.close();
		} catch (SQLException lEx) {
			lEx.printStackTrace();
		}
	}

}
