/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.tut;

import java.util.List;

import org.zkoss.zul.Listitem;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefListbox;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.domain.IERunTeam;

/**
 * The Class CTutRunGroupAccountCaseRoleCombo.
 */
public class CTutRunGroupAccountCaseRoleCombo extends CDefListbox {

	private static final long serialVersionUID = -2095121699825443497L;

	/** The account. */
	protected IEAccount account = null;

	/**
	 * Instantiates a new c tut run group account case role combo.
	 * Show possible case roles for aRunGroupAccounts.
	 * 
	 * @param aId the a id
	 * @param aAccount the a account
	 * @param aRunGroupAccounts the a run group accounts
	 */
	public CTutRunGroupAccountCaseRoleCombo(String aId,IEAccount aAccount,List<IERunGroupAccount> aRunGroupAccounts) {
		setRows(1);
		setMold("select");
		if (!aId.equals(""))
			setId(aId);
		account = aAccount;
		Listitem lListitem = super.insertListitem(null,CDesktopComponents.vView().getLabel("none"),null);
		setSelectedItem(lListitem);
		for (IERunGroupAccount lObject : aRunGroupAccounts) {
			lListitem = super.insertListitem(lObject,lObject.getERunGroup().getECaseRole().getName(),null);
		}
	}

	/**
	 * On select update run team combobox and redirect button, with selected
	 * run group account and run team.
	 */
	public void onSelect() {
		boolean lCaseRoleSelected = (getSelectedItem().getValue() != null);
		CTutRunGroupAccountRunTeamCombo lRunTeamCombo = (CTutRunGroupAccountRunTeamCombo)getFellowIfAny("rungroupaccountsLbRunTeamCombo"+account.getAccId());
		boolean lRunTeamSelected = true;
		IERunTeam lRunTeam = null;
		if (lRunTeamCombo != null) {
			// if caserole selected enable runteam combo
			// if caserole not selected disable runteam combo
			lRunTeamCombo.setDisabled(!lCaseRoleSelected);
			lRunTeamCombo.setRunGroupAccount((IERunGroupAccount)getSelectedItem().getValue());
			lRunTeamSelected = (lRunTeamCombo.getSelectedItem().getValue() != null);
			lRunTeam = (IERunTeam)lRunTeamCombo.getSelectedItem().getValue();
		}
		CTutRunGroupAccountRedirectBtn lRunRedirectButton = (CTutRunGroupAccountRedirectBtn)getFellowIfAny("rungroupaccountsLbRedirectButton"+account.getAccId());
		lRunRedirectButton.setDisabled((!lCaseRoleSelected) || (!lRunTeamSelected));
		lRunRedirectButton.setRunGroupAccount((IERunGroupAccount)getSelectedItem().getValue());
		lRunRedirectButton.setRunTeam(lRunTeam);
	}
}
