/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.tut;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Html;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefListcell;
import nl.surf.emergo.control.run.CRunBlobLabel;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERunGroupAccount;

/**
 * The Class CTutRunGroupAccountEmailsHelper.
 */
public class CTutRunGroupAccountEmailsHelper extends CTutRunGroupAccountsHelper {

	/** The mail case component. */
	private List<IECaseComponent> mailCaseComponents = null;

	/**
	 * Renders sent e-mails for one account.
	 * 
	 * @param aListbox the a listbox
	 * @param aInsertBefore the a insert before
	 * @param aItem the a item
	 */
	public void renderItem(Listbox aListbox,Listitem aInsertBefore,Hashtable<String,Object> aItem) {
		Listitem lListitem = super.newListitem();
		List<IERunGroupAccount> lRunGroupAccounts = (List)aItem.get("rungroupaccounts");
		IEAccount lItem = (IEAccount)aItem.get("item");
		lListitem.setValue(lItem);
		super.appendListcell(lListitem,lItem.getTitle());
		super.appendListcell(lListitem,lItem.getInitials());
		super.appendListcell(lListitem,lItem.getNameprefix());
		super.appendListcell(lListitem,lItem.getLastname());
		Listcell lListcell = new CDefListcell();

		List<Component> lEmailLabels = new ArrayList<Component>(0);
		for (int i=0;i<lRunGroupAccounts.size();i++) {
			CDesktopComponents.sSpring().setRunStatus(AppConstants.runStatusRun);
			CDesktopComponents.sSpring().setRunGroupAccount((IERunGroupAccount)lRunGroupAccounts.get(i));
			if (mailCaseComponents == null)
				mailCaseComponents = CDesktopComponents.sSpring().getCaseComponents("mail");
			if (mailCaseComponents == null || mailCaseComponents.size() == 0)
				return;
			String lPre = "";
			if (mailCaseComponents.size() > 1) {
				lPre = "- ";
			}
			for (IECaseComponent lMailCaseComponent : mailCaseComponents) {
				boolean lCaseComponentNameAdded = false;
				List<IXMLTag> lTags = CDesktopComponents.cScript().getRunGroupNodeTags(""+lMailCaseComponent.getCacId(), "outmailpredef");
				for (int j=(lTags.size()-1);j>=0;j--) {
					IXMLTag lTag = lTags.get(j);
					boolean lSent = lTag.getCurrentStatusAttribute(AppConstants.statusKeySent)
							.equals(AppConstants.statusValueTrue);
					boolean lOriginalTag = (!lTag.getAttribute(AppConstants.keyRefstatusids).equals(""));
					if ((lOriginalTag) || (!lSent))
						lTags.remove(j);
					else {
						if (mailCaseComponents.size() > 1 && !lCaseComponentNameAdded) {
							//add mail case component name
							if (lEmailLabels.size() > 0) {
								lEmailLabels.add(new Html("<br/>"));
							}
							lEmailLabels.add(new Html(lMailCaseComponent.getName() + ":"));
							lCaseComponentNameAdded = true;
						}
						List<IXMLTag> lAttachments = lTag.getStatusChilds("attachment");
						List<IXMLTag> lTitles = lTag.getChilds("title");
						IXMLTag lTitleOb = (IXMLTag)lTitles.get(0);
						String lTitle = lTitleOb.getValue();
						if (lEmailLabels.size() > 0)
						{
							lEmailLabels.add(new Html("<br/>"));
						}
						lEmailLabels.add(new Html(lPre + lTitle + ":"));
						CTutRunGroupAccountEmailsTextLabel lTLabel = new CTutRunGroupAccountEmailsTextLabel();
						lTLabel.setSpring(CDesktopComponents.sSpring());
						lTLabel.setBlobContainer(lTag);
						lTLabel.init(lPre + CDesktopComponents.vView().getLabel("tut_rungroupaccounts.emailtext"));
						lEmailLabels.add(new Html("<br/>- "));
						lEmailLabels.add(lTLabel);
						if (lAttachments.size() > 0) {
							lEmailLabels.add(new Html("<br/>" + CDesktopComponents.vView().getLabel("tut_rungroupaccounts.emailattachments")));
							for (IXMLTag lAttachment : lAttachments) {
								CRunBlobLabel lLabel = new CRunBlobLabel();
								lLabel.setSpring(CDesktopComponents.sSpring());
								lLabel.setBlobContainer(lAttachment);
								lLabel.init();
								if (lEmailLabels.size() > 0)
									lEmailLabels.add(new Html("<br/>"));
								lEmailLabels.add(new Html(lPre + "- "));
								lEmailLabels.add(lLabel);
							}
						}
						if (lEmailLabels.size() > 0)
						{
							lEmailLabels.add(new Html("<br/>"));
						}
					}
				}
			}
		}

		for (Component lEmailLabel : lEmailLabels) {
			lListcell.appendChild(lEmailLabel);
		}

		lListitem.appendChild(lListcell);
		super.insertListitem(aListbox,lListitem,aInsertBefore);
	}

}