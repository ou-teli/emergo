/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.tut.cases.Sexology;

import nl.surf.emergo.control.def.CDefGrid;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.domain.IERunGroupCaseComponent;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import nl.surf.emergo.business.IRunGroupCaseComponentManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.business.impl.RunGroupAccountManager;
import nl.surf.emergo.business.impl.RunGroupManager;
import nl.surf.emergo.zkspring.SSpring;

public class CTutRunsSxLegacyGrid extends CDefGrid {

	private static final long serialVersionUID = -807739730333656304L;

	String gLegacyAfgerondKey = "s_LegacyAfgerond";
	String gAfgerondNrKey = "s_VerplichtAfgerondNr";
	String gStateCompName = "status verplicht afgerond";
	String gScriptCompName = "script verplicht afgerond";

	public void onCreate() {
		CTutRunsSxMonitorHelper gTutSekRunsHelper = new CTutRunsSxMonitorHelper();
		List<IERun> gSekRuns = gTutSekRunsHelper.getRuns();
		
		if (gSekRuns.isEmpty()) {
			gTutSekRunsHelper.toRegularOverview();
		}

		CTutRunsSxLegacyHelper gTutSekLegacyHelper = new CTutRunsSxLegacyHelper();
		for (IERun run : gSekRuns) {
			SSpring lSpring = new SSpring();
			lSpring.clearRunBoostProps();

			IECase lCase = run.getECase();
			//lSpring.setRun(run);
			//lSpring.setRunStatus(AppConstants.runStatusRun);
			int lCasId = lCase.getCasId();

			List<Integer> rugIds = new ArrayList<Integer>();
			List<IERunGroup> runGroups = ((RunGroupManager) lSpring.getBean("runGroupManager")).getAllRunGroupsByRunId(run.getRunId());
			for (IERunGroup runGroup : runGroups) {
				if (!rugIds.contains(runGroup.getRugId())) {
					rugIds.add(runGroup.getRugId());
				}
			}
			List<IERunGroupAccount> runGroupAccounts = ((RunGroupAccountManager) lSpring.getBean("runGroupAccountManager")).getAllRunGroupAccountsByRugIds(rugIds);
			//NOTE need to set runGroupAccount for current run, for else cached rGA is used resulting in empty
			//	casecomponents list if run is attached to other case
			if ((runGroupAccounts != null) && (runGroupAccounts.size() > 0)) {
				lSpring.setRunGroupAccount((IERunGroupAccount)runGroupAccounts.get(0));
			}

			List<IECaseComponent> allCaseComponents = lSpring.getCaseComponents(lCase);
			IECaseComponent tutCaseComponent = lSpring.getCaseComponentByComponentCodeOrCaseComponentName(allCaseComponents, "", gStateCompName);
			if (tutCaseComponent != null) {
				System.out.println(lCase.getName());
				lSpring.setRun(run);
				lSpring.setRunStatus(AppConstants.runStatusRun);
				IECaseComponent lCaseCaseComponent = lSpring.getCaseComponentByComponentCodeOrCaseComponentName(allCaseComponents, "case", "");
				IECaseComponent lScriptFinishedCaseComponent = lSpring.getCaseComponentByComponentCodeOrCaseComponentName(allCaseComponents, "", gScriptCompName);
				for (IERunGroupAccount runGroupAccount : runGroupAccounts) {
					lSpring.setRunGroupAccount(runGroupAccount);
					IEAccount account = runGroupAccount.getEAccount();
					String userid = account.getUserid();
					String name = account.getLastname();
					System.out.println(name);

					IERunGroupCaseComponent lRGCaseCaseComponent =
							((IRunGroupCaseComponentManager)lSpring.getBean("runGroupCaseComponentManager")).
										getRunGroupCaseComponent(runGroupAccount.getERunGroup().getRugId(), lCaseCaseComponent.getCacId());
					if (!(lRGCaseCaseComponent == null)) {
						Date lUDate = lRGCaseCaseComponent.getLastupdatedate();
						Date lCDate = lRGCaseCaseComponent.getCreationdate();
						List<String> lErrors = new ArrayList<String>();
						IXMLTag lCCTag = lSpring.getComponentDataStatusTag(lCaseCaseComponent, AppConstants.statusTypeRunGroup);
						lCCTag = lSpring.getComponentDataStatusTag(lScriptFinishedCaseComponent, AppConstants.statusTypeRunGroup);
						boolean lSCPresent = gTutSekLegacyHelper.evaluateCondition(lScriptFinishedCaseComponent, lCCTag, AppConstants.statusKeyPresent, AppConstants.statusKeyPresentIndex, 0,
								lSpring.getAppManager().getStatusValue(1), AppConstants.operatorEqIndex, lSpring.getAppManager().getStatusValue(1), "boolean");

						System.out.println("started: " + lCDate);
						System.out.println("ended: " + lUDate);
						System.out.println("present: " + lSCPresent);

						if (lUDate.after(lCDate) && !lSCPresent) {
							String lFNr = lSpring.getCurrentRunTagStatus(gStateCompName, "state", gLegacyAfgerondKey, AppConstants.statusKeyValue, AppConstants.statusTypeRunGroup, lErrors);
							if ((lFNr != null) && (lFNr.equals("0"))) {
								System.out.println("adapting legacy");
								lSpring.setCurrentRunTagStatus(gStateCompName, "state", gLegacyAfgerondKey, AppConstants.statusKeyValue, "1", AppConstants.statusTypeRunGroup, true, lErrors);
								lSpring.saveRunCaseComponentsStatus();
								lFNr = lSpring.getCurrentRunTagStatus(gStateCompName, "state", gAfgerondNrKey, AppConstants.statusKeyValue, AppConstants.statusTypeRunGroup, lErrors);
								System.out.println(name + ": " + lFNr);
								String errorString = "";
								for (int i=0;i<lErrors.size();i++) {
									errorString += "\n" + lErrors.get(i);
								}
								if (lErrors.size() > 0) {
									System.out.println(errorString);
								}

							}
						}
					}
				}
			}
		
		}
	}

}
