/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.tut.cases.Gamebrics;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Messagebox;

import nl.surf.emergo.business.IRunAccountManager;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDecoratedInputWndTC;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.script.CScript;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunAccount;
import nl.surf.emergo.utilities.ExcelHelper;
import nl.surf.emergo.utilities.FileHelper;
import nl.surf.emergo.utilities.ZipHelper;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpring;


/**
 * The Class CTutGBLogWnd.
 */
public class CTutGBLogWnd extends CDecoratedInputWndTC {

	private static final long serialVersionUID = 1L;

	private static final Logger _log = LogManager.getLogger(CTutGBLogWnd.class);

	public VView vView = CDesktopComponents.vView();

	public CControl cControl = CDesktopComponents.cControl();

	public SSpring sSpring = CDesktopComponents.sSpring();

	public CScript cScript = CDesktopComponents.cScript();

	public FileHelper fileHelper = new FileHelper();

	public ZipHelper zipHelper = new ZipHelper();

	public ExcelHelper excelHelper = new ExcelHelper();

	//NOTE supports multiple cases!

	public List<IERun> runs = new ArrayList<IERun>();

	public List<IECase> cases = new ArrayList<IECase>();

	public boolean areMultipleCases = false;

	public String typeOfOverview = "";

	public List<IECaseComponent> caseComponents = null;

	public static final String wndWndId = "tutGBLogWnd";
	public static final String wndPPAssNameFieldId = "prePostAssessmentsCaseComponentName";
	public static final String wndAssNamesFieldId = "assessmentsCaseComponentNames";

	public static final String xcelHeaderCellsText = "headerText";
	public static final String xcelHeaderCellsInfo = "headerCellsInfo";
	public static final String xcelHCellColOffset = "colOffset";
	public static final String xcelHCellRowOffset = "rowOffset";
	public static final String xcelHCellColSpan = "colSpan";
	public static final String xcelHCellRowSpan = "rowSpan";
	public static final String xcelHCellRotate = "rotate";
	public static final String xcelAssHeaderId = "assessmentHeader";
	public static final String xcelAssItemHeaderId = "assessmentItemHeader";

	public HashMap<String,Object> xcelAssHeaderInfo = new HashMap<String,Object>();

	public List<List<String>> queryResult = new ArrayList<List<String>>();
	public List<HashMap<String,Object>> queryResults = new ArrayList<HashMap<String,Object>>();

	public CTutGBLogWnd() {
		List<IERunAccount> lItems = ((IRunAccountManager)CDesktopComponents.sSpring().getBean("runAccountManager")).getAllRunAccountsByAccIdActiveAsTutor(CDesktopComponents.cControl().getAccId());
		for (IERunAccount lItem : lItems) {
			IERun lRun = lItem.getERun();
			if (!runs.contains(lRun))
				runs.add(lRun);
		}

		for (IERun run : runs) {
			if (!cases.contains(run.getECase())) {
				cases.add(run.getECase());
			}
		}
		areMultipleCases = cases.size() > 1;
		if (cControl.getAccSessAttr("tutInitSessionVars") == null) {
			cControl.setAccSessAttr("tutInitSessionVars", true);
		}
	}

	public List<IECaseComponent> getCaseComponents() {
		if (caseComponents != null) {
			return caseComponents;
		}
		caseComponents = sSpring.getCaseComponents(cases);
		return caseComponents;
	}

	public List<IECaseComponent> getCaseComponents(IECase pCase) {
		List<IECaseComponent> lCaseComponents = new ArrayList<IECaseComponent>();
		for (IECaseComponent lCaseComponent : getCaseComponents()) {
			if (lCaseComponent.getECase().getCasId() == pCase.getCasId())
				lCaseComponents.add(lCaseComponent);
		}
		return lCaseComponents;
	}

	public String getAbsoluteTempPath() {
		String absoluteTempPath = sSpring.getAppManager().getAbsoluteTempPath();
		String subPath = "";
		if (Sessions.getCurrent() != null) {
			subPath = CDesktopComponents.vView().getUniqueTempSubPath();
		}
		return absoluteTempPath + subPath;
	}

	public String getDownloadPath(String absoluteTempPath) {
		String absPath = sSpring.getAppManager().getAbsoluteAppPath();
		int pos = absoluteTempPath.indexOf(absPath);
		String result = absoluteTempPath.substring(pos + absPath.length());
		
		if (!(result.endsWith("/") || result.endsWith("\\"))) {
			result += "/";
		}
		return result;
	}

	public boolean downloadFile(String absoluteTempPath, String fileName) {
		//determine download path
		absoluteTempPath = getDownloadPath(absoluteTempPath);								

		//download file
		boolean fileIsDownloaded = false;
		String probeContentType = null;
		try {
			probeContentType = Files.probeContentType(Paths.get(absoluteTempPath + fileName));
		} catch (IOException e) {
			_log.error(e);
		}
		try {
			Filedownload.save(absoluteTempPath + fileName, probeContentType);
			fileIsDownloaded = true;
		} catch (Exception e) {
			vView.showMessagebox(getRoot(), vView.getLabel("tut_rungroupaccountownoverview.messagebox.text.filedownloaderror").replace("%1", fileName), 
					vView.getLabel("messagebox.title.errors"), Messagebox.OK, Messagebox.ERROR);
		}
		if (fileIsDownloaded) {
			vView.showMessagebox(getRoot(), vView.getLabel("tut_rungroupaccountownoverview.messagebox.text.filedownload").replace("%1", fileName),
					vView.getLabel("messagebox.title.information"), Messagebox.OK, Messagebox.INFORMATION);
		}
		return fileIsDownloaded;
	}
	
	protected Row getRow(Sheet pSheet, int pRowNumber) {
		Row lRow = pSheet.getRow(pRowNumber);
		if (lRow == null) {
			lRow = pSheet.createRow(pRowNumber);
		}
		return lRow;
	}

	public boolean createOrUpdateWorkbook(List<HashMap<String,Object>> queryResults, boolean createWorkbook, String fileName) {

		/*NOTE
			Cells in already existing rows may not be overwritten! So be sure that rowOffset points to a non-existing row.
			There are three use-cases to use SXSSFWorkbook(XSSFWorkbook) :
				Append new sheets to existing workbooks. You can open existing workbook from a file or create on the fly with XSSF.
				Append rows to existing sheets. The row number MUST be greater than max(rownum) in the template sheet.
				Use existing workbook as a template and re-use global objects such as cell styles, formats, images, etc.
		 */

		if (queryResults.size() == 0) {
			return false;
		}

		//NOTE default streaming is true so a streaming workbook is used.
		//However if for at least one of the queries 'addValues' is false a non streaming workbook is used, because
		//cells may be overwritten which is not allowed for a streaming workbook.
		boolean streaming = true;

		SXSSFWorkbook workbook = (SXSSFWorkbook)excelHelper.createOrLoadWorkbook(createWorkbook, streaming, getAbsoluteTempPath(), fileName);

		// Create a Font for styling header cells
		Font headerFont = workbook.createFont();
		headerFont.setBold(true);

		// Create a CellStyle with the font
		XSSFCellStyle headerCellStyle = (XSSFCellStyle)workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
		headerCellStyle.getStyleXf().setApplyAlignment(true);
		XSSFCellStyle headerCellStyleRotated = (XSSFCellStyle)workbook.createCellStyle();
		headerCellStyleRotated.setFont(headerFont);
		headerCellStyleRotated.setRotation((short)90);
		headerCellStyleRotated.getStyleXf().setApplyAlignment(true);

		for (HashMap<String,Object> hQueryResult : queryResults) {
			List<List<String>> queryResult = (List<List<String>>)hQueryResult.get("queryResult");
			String sheetName = (String)hQueryResult.get("sheetName"); 
			Integer rowOffset = (Integer)hQueryResult.get("rowOffset"); 
			Integer colOffset = (Integer)hQueryResult.get("colOffset"); 
			boolean addValues = (Boolean)hQueryResult.get("addValues"); 
			List<HashMap<String,Object>> lHeaderInfo = (List<HashMap<String,Object>>)hQueryResult.get(xcelHeaderCellsInfo);

			// Get or create a Sheet
			Sheet sheet = workbook.getSheet(sheetName);
			boolean createNewSheet = (sheet == null);
			if (createNewSheet) {
				sheet = workbook.createSheet(sheetName);
			}
			workbook.setActiveSheet(workbook.getSheetIndex(sheetName));


			// Create rows and cells
			int rowNum = rowOffset;
			int maxCols = queryResult.get(0).size();

			if (streaming && createNewSheet) {
				//NOTE Track all columns for auto sizing. Has to be done before rows are written to the sheet, if SXSSFWorkbook (streaming workbook) is used.
				for(int i = colOffset; i < (colOffset + maxCols); i++) {
					((SXSSFSheet)sheet).trackColumnForAutoSizing(i);
				}
			}

			if (addValues) {
				//NOTE add data after rows where cell in column colOffset exists
				Row row = sheet.getRow(rowNum);
				if (row != null) {
					Cell cell = row.getCell(colOffset);
					while (row != null && cell != null && !cell.getStringCellValue().equals("")) {
						if (cell != null) {
							rowNum++;
						}
						row = sheet.getRow(rowNum);
						if (row != null) {
							cell = row.getCell(colOffset);
						}
						else {
							cell = null;
						}
					}
				}
				if (rowNum > rowOffset) {
					//if existing data, skip header
					if (queryResult.size() > 0) {
						queryResult.remove(0);
					}
				}
			}

			for (List<String> resultRow : queryResult) {
				Row row = getRow(sheet, rowNum);
				int colNum = colOffset;
				int cellNumber = 0;
				for (String resultCell : resultRow) {
					CellStyle style = null;
					if (rowNum == rowOffset) {
						HashMap<String,Object> lHeaderCellInfo = lHeaderInfo.get(cellNumber);
						if ((boolean)lHeaderCellInfo.get(xcelHCellRotate))
							style = headerCellStyleRotated;
						else
							style = headerCellStyle;
						int lColOffset = (int)lHeaderCellInfo.get(xcelHCellColOffset);
						int lRowOffset = (int)lHeaderCellInfo.get(xcelHCellRowOffset);
						int lColSpan = (int)lHeaderCellInfo.get(xcelHCellColSpan);
						int lRowSpan = (int)lHeaderCellInfo.get(xcelHCellRowSpan);
						if ((lColSpan > 0) || (lRowSpan > 0)) {
							sheet.addMergedRegion(new CellRangeAddress(rowNum + lRowOffset, rowNum + lRowOffset + lRowSpan, colNum + lColOffset, colNum + lColOffset + lColSpan));
						}
						//NOTE create row if it doesn't exist
						Row lRow = getRow(sheet, rowNum + lRowOffset);
						excelHelper.createOrUpdateXLSXCell(lRow, colNum + lColOffset, null, style, resultCell, true); // FIXME boolean added as a workaround
						cellNumber++;
					} else {
						excelHelper.createOrUpdateXLSXCell(row, colNum, null, style, resultCell, true); // FIXME boolean added as a workaround
						colNum++;
					}
				}
				if (rowNum == rowOffset) {
					rowNum+=3;
				} else
					rowNum++;
			}

			if (createNewSheet) {
				// Resize all columns to fit the content size
				for(int i = colOffset; i < (colOffset + maxCols); i++) {
					sheet.autoSizeColumn(i);
				}
			}
		}

		return excelHelper.writeAndCloseWorkbook(workbook, getAbsoluteTempPath(), fileName, getRoot(), vView);
	}

	/* Query functions */

	public HashMap<String,Object> getHeaderCellInfo(int pColOffset, int pRowOffset, int pColSpan, int pRowSpan, boolean pRotate) {
		HashMap<String,Object> lCellInfo = new HashMap<String,Object>();
		lCellInfo.put(xcelHCellColOffset, pColOffset);
		lCellInfo.put(xcelHCellRowOffset, pRowOffset);
		lCellInfo.put(xcelHCellColSpan, pColSpan);
		lCellInfo.put(xcelHCellRowSpan, pRowSpan);
		lCellInfo.put(xcelHCellRotate, pRotate);
		return lCellInfo;
	}

	
	public HashMap<String,Object> getQueryHeader() {
		HashMap<String,Object> lHeaderInfo = new HashMap<String,Object>();
		List<String> header = new ArrayList<String>();
		List<HashMap<String,Object>> lCellsInfo = new ArrayList<HashMap<String,Object>>();
		int lColOffset = 0;
		header.add(vView.getLabel("tut_rungroupaccountownoverview.runcode"));
		lCellsInfo.add(getHeaderCellInfo(lColOffset, 0, 0, 2, true));
		lColOffset++;
		header.add(vView.getLabel("tut_rungroupaccountownoverview.runname"));
		lCellsInfo.add(getHeaderCellInfo(lColOffset, 0, 0, 2, true));
		lColOffset++;
		header.add(vView.getLabel("tut_rungroupaccountownoverview.rgaid"));
		lCellsInfo.add(getHeaderCellInfo(lColOffset, 0, 0, 2, true));
		lColOffset++;
		header.add(vView.getLabel("tut_rungroupaccountownoverview.active"));
		lCellsInfo.add(getHeaderCellInfo(lColOffset, 0, 0, 2, true));
		lColOffset++;
		header.add(vView.getLabel("userid"));
		lCellsInfo.add(getHeaderCellInfo(lColOffset, 0, 0, 2, true));
		lColOffset++;
		header.add(vView.getLabel("studentid"));
		lCellsInfo.add(getHeaderCellInfo(lColOffset, 0, 0, 2, true));
		lColOffset++;
		header.add(vView.getLabel("title"));
		lCellsInfo.add(getHeaderCellInfo(lColOffset, 0, 0, 2, true));
		lColOffset++;
		header.add(vView.getLabel("initials"));
		lCellsInfo.add(getHeaderCellInfo(lColOffset, 0, 0, 2, true));
		lColOffset++;
		header.add(vView.getLabel("nameprefix"));
		lCellsInfo.add(getHeaderCellInfo(lColOffset, 0, 0, 2, true));
		lColOffset++;
		header.add(vView.getLabel("lastname"));
		lCellsInfo.add(getHeaderCellInfo(lColOffset, 0, 0, 2, true));
		lColOffset++;
		header.add(vView.getLabel("email"));
		lCellsInfo.add(getHeaderCellInfo(lColOffset, 0, 0, 2, true));
		lColOffset++;
		
		List<String> lAssessmentNamesList = (List<String>)xcelAssHeaderInfo.get(xcelAssHeaderId);
		List<List<String>> lAssessmentItemNamesList = (List<List<String>>)xcelAssHeaderInfo.get(xcelAssItemHeaderId);
		int lInd = 0;
		for (String lAssessmentName : lAssessmentNamesList) {
			List<String> lAssessmentItemNames = lAssessmentItemNamesList.get(lInd);
			int lNrOfItems = lAssessmentItemNames.size();
			if (lNrOfItems > 0) {
				header.add(lAssessmentName);
				lCellsInfo.add(getHeaderCellInfo(lColOffset, 0, lNrOfItems * 2 - 1, 0, false));
				for (String lAssessmentItemName : lAssessmentItemNames) {
					header.add(lAssessmentItemName);
					lCellsInfo.add(getHeaderCellInfo(lColOffset, 1, 1, 0, false));
					header.add("antwoord-ID");
					lCellsInfo.add(getHeaderCellInfo(lColOffset, 2, 0, 0, true));
					header.add("antwoordtekst");
					lCellsInfo.add(getHeaderCellInfo(lColOffset + 1, 2, 0, 0, true));
					lColOffset+=2;
				}
			}
			lInd++;
		}
		header.add(vView.getLabel("tut_rungroupaccountownoverview.time") + " (seconden)");
		lCellsInfo.add(getHeaderCellInfo(lColOffset, 0, 0, 2, true));
		
		lHeaderInfo.put(xcelHeaderCellsText, header);
		lHeaderInfo.put(xcelHeaderCellsInfo, lCellsInfo);
		return lHeaderInfo;
	}

}
