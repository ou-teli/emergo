/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.tut;

import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

import nl.surf.emergo.business.impl.FileManager;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefButton;

/**
 * The Class CTutRunGroupAccountOwnOverviewSaveQueryBtn.
 */
public class CTutRunGroupAccountOwnOverviewSaveQueryBtn extends CDefButton {

	private static final long serialVersionUID = -4388662852793766563L;

	protected CTutRunGroupAccountOwnOverviewWnd root = (CTutRunGroupAccountOwnOverviewWnd)CDesktopComponents.vView().getComponent("ownOverviewWnd");

	public void onClick() {
		FileManager fileManager = new FileManager();
		
		//get file name
		String fileName = ((Textbox)root.vView.getComponent("queryFileName")).getValue();
		if (fileName.equals("")) {
			root.vView.showMessagebox(getRoot(), root.vView.getLabel("tut_rungroupaccountownoverview.messagebox.text.emptyfilename"), 
					root.vView.getLabel("messagebox.title.information"), Messagebox.OK, Messagebox.INFORMATION);
		}
		else {
			//determine temporary path for creating file
			String absoluteTempPath = root.getAbsoluteTempPath();
		
			//create query file	
			if (fileManager.createFile(absoluteTempPath, fileName, root.buildQuery().getBytes()).equals(fileName)) {
				//download query file
				if (root.downloadFile(absoluteTempPath, fileName)) {
					root.cControl.setAccSessAttr("tutQueryFileName", fileName);
				}
			}
			else {
				root.vView.showMessagebox(getRoot(), root.vView.getLabel("tut_rungroupaccountownoverview.messagebox.text.filecreationerror").replace("%1", fileName), 
						root.vView.getLabel("messagebox.title.errors"), Messagebox.OK, Messagebox.ERROR);
			}
		}
	}

}
