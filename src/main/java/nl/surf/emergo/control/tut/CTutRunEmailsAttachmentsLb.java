/**
 * 
 */
package nl.surf.emergo.control.tut;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;

import nl.surf.emergo.business.IRunGroupAccountManager;
import nl.surf.emergo.business.IRunGroupManager;
import nl.surf.emergo.business.IRunTeamManager;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;


/**
 * The Class CTutRunEmailsAttachmentsLb.
 */
public class CTutRunEmailsAttachmentsLb extends CTutRunGroupAccountEmailsLb {

	private static final long serialVersionUID = 7044999225643460247L;

	protected CTutRunEmailsAttachmentsHelper getHelper() {
		return (new CTutRunEmailsAttachmentsHelper());
	}
	
	/**
	 * On create render listitems.
	 */
	public void onCreateOne() {
		//getItems().clear();
		int lRunId = Integer.parseInt((String)CDesktopComponents.cControl().getAccSessAttr("runId"));
		runTeams = ((IRunTeamManager)CDesktopComponents.sSpring().getBean("runTeamManager")).getAllRunTeamsByRunId(lRunId);
		List<IERunGroup> lRunGroups = ((IRunGroupManager)CDesktopComponents.sSpring().getBean("runGroupManager")).getAllRunGroupsByRunId(lRunId);
		List<Integer> lRugIds = new ArrayList<Integer>();
		for (IERunGroup lRunGroup : lRunGroups) {
			if (!lRugIds.contains(lRunGroup.getRugId()))
				lRugIds.add(lRunGroup.getRugId());
		}
		List<IERunGroupAccount> lItems = ((IRunGroupAccountManager)CDesktopComponents.sSpring().getBean("runGroupAccountManager")).getAllRunGroupAccountsByRugIds(lRugIds);
		List<IEAccount> lAccounts = new ArrayList<IEAccount>();
		runGroupIds = new ArrayList<Integer>();
		List<Hashtable<String,Object>> lStuItems = new ArrayList<Hashtable<String,Object>>(0);
		for (IERunGroupAccount lRunGroupAccount : lItems) {
			IEAccount lAccount = lRunGroupAccount.getEAccount();
			if (lAccount.getActive() && (!lAccounts.contains(lAccount))) {
				List<IERunGroupAccount> lRunGroupAccounts = getRunGroupAccounts(lAccount.getAccId(),lRunId,lItems);
				if (lRunGroupAccounts.size() > 0) {
					lAccounts.add(lAccount);
					if (!runGroupIds.contains(lRunGroupAccount.getERunGroup().getRugId()))
						runGroupIds.add(lRunGroupAccount.getERunGroup().getRugId());
					Hashtable<String,Object> lStuItem = new Hashtable<String,Object>(0);
					lStuItem.put("item",lAccount);
					lStuItem.put("rungroupaccounts",lRunGroupAccounts);
					lStuItem.put("runteams",getRunTeams(lRunGroupAccounts));
					lStuItem.put("runid",lRunId);
					lStuItems.add(lStuItem);
				}
			}
		}
		CTutRunEmailsAttachmentsHelper cHelper = getHelper();
		cHelper.renderItem(this, null, lStuItems);
	}
	
	private static String[] runIds = {"40","85","126","172","183","217","246","289"};

	public void onCreate() {
		CDesktopComponents.cControl().setAccSessAttr("role", "cde");  //allow downloading reports
		getItems().clear();
		getHelper().initCopyMails();
    	Execution lExec = Executions.getCurrent();
		String lRunId = lExec.getParameter("runId");

		String lRunIds[];
		if (lRunId != null)
			lRunIds = new String[] {lRunId};
		else
			lRunIds = runIds;
		for (int i = 0; i < lRunIds.length; i++) {
			lRunId = lRunIds[i];
			CDesktopComponents.sSpring().clearRunBoostProps();
			CDesktopComponents.cControl().setAccSessAttr("runId", lRunId);
			onCreateOne();
		}
		getHelper().saveInfoFile();
		restoreSettings();
	}

}

