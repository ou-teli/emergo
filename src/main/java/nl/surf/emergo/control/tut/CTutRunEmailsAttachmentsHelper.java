package nl.surf.emergo.control.tut;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.IAppManager;
import nl.surf.emergo.business.IFileManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CImportExportHelper;
import nl.surf.emergo.control.def.CDefListcell;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERunGroupAccount;

/**
 * The Class CTutRunEmailsAttachmentsHelper.
 */
public class CTutRunEmailsAttachmentsHelper extends CTutRunGroupAccountEmailsHelper {

	protected static String tempPath;

	protected static String appPath;

	protected static String infoFile;

	protected static HSSFWorkbook infoBook;

	protected static HSSFSheet infoSheet;

	protected static int infoRowNumber;

	/** the file manager. */
	protected IFileManager fileManager = null;

	/** the app manager. */
	protected IAppManager appManager = null;

	protected static ArrayList<String> mailTitles;

	private static final Logger _log = LogManager.getLogger(CTutRunEmailsAttachmentsHelper.class);

	/**
	 * Gets the file manager.
	 *
	 * @return the file manager
	 */
	protected IFileManager getFileManager() {
		if (fileManager == null)
			fileManager = CDesktopComponents.sSpring().getFileManager();
		return fileManager;
	}

	/**
	 * Gets the app manager.
	 *
	 * @return the app manager
	 */
	protected IAppManager getAppManager() {
		if (appManager == null)
			appManager = CDesktopComponents.sSpring().getAppManager();
		return appManager;
	}

	/**
	 * Renders sent e-mails for one account.
	 * 
	 * @param aListbox      the a listbox
	 * @param aInsertBefore the a insert before
	 * @param aItems        the a items
	 */
	public void renderItem(Listbox aListbox, Listitem aInsertBefore, List<Hashtable<String, Object>> aItems) {
		Listitem lListitem = super.newListitem();

		Listcell lListcell = new CDefListcell();
		lListitem.appendChild(lListcell);

//		List<Component> lEmailLabels = new ArrayList<Component>(0);
		Hashtable<String, List<String>> lEmailLabels = new Hashtable<String, List<String>>(0);
		for (Hashtable<String, Object> lStuItem : aItems) {
			List<IERunGroupAccount> lRunGroupAccounts = (List<IERunGroupAccount>) lStuItem.get("rungroupaccounts");
			IEAccount lItem = (IEAccount) lStuItem.get("item");
			String lUserId = lItem.getUserid();
			String lUserName = lItem.getInitials() + " " + lItem.getNameprefix() + " " + lItem.getLastname();
			int lRunId = (int) lStuItem.get("runid");
			for (int i = 0; i < lRunGroupAccounts.size(); i++) {
				int lRgaCounter = 0;
				CDesktopComponents.sSpring().setRunStatus(AppConstants.runStatusRun);
				CDesktopComponents.sSpring().setRunGroupAccount(lRunGroupAccounts.get(i));
				List<IECaseComponent> mailCaseComponents = CDesktopComponents.sSpring().getCaseComponents("mail");
				if (!(mailCaseComponents == null || mailCaseComponents.size() == 0)) {
					int lMailCompCounter = 0;
					for (IECaseComponent lMailCaseComponent : mailCaseComponents) {
						List<IXMLTag> lTags = CDesktopComponents.cScript()
								.getRunGroupNodeTags("" + lMailCaseComponent.getCacId(), "outmailpredef");
						String lMailComponentName = lMailCaseComponent.getName();
						int lOutMailCounter = 0;
						for (int j = (lTags.size() - 1); j >= 0; j--) {
							IXMLTag lTag = lTags.get(j);
							boolean lSent = ((CDesktopComponents.sSpring().getCurrentTagStatus(lTag,
									AppConstants.statusKeySent)).equals(AppConstants.statusValueTrue));
							boolean lOriginalTag = (!lTag.getAttribute(AppConstants.keyRefstatusids).equals(""));
							if ((lOriginalTag) || (!lSent))
								lTags.remove(j);
							else {
								List<IXMLTag> lAttachments = lTag.getStatusChilds("attachment");
								List<IXMLTag> lTitles = lTag.getChilds("title");
								IXMLTag lTitleOb = lTitles.get(0);
								String lTitle = lTitleOb.getValue();
								int lPos = mailTitles.indexOf(lTitle);
								if (lPos < 0) {
									mailTitles.add(lTitle);
									lPos = mailTitles.indexOf(lTitle);
								}
								if (lAttachments.size() > 0) {
									int lAttachCounter = 0;
									for (IXMLTag lAttachment : lAttachments) {
										String lFrom = getHref(lAttachment);
										String lToBase = lMailCompCounter + "_" + lPos + "_" + lOutMailCounter + "_"
												+ lUserId + "_" + lRgaCounter + "_" + lAttachCounter + "_" + lRunId;
										String lCopied = copyAttachmentFile(lFrom, lToBase);
										if (!lCopied.isEmpty()) {
											fillInfoRow(lUserName, lUserId, lCopied, lTitle, lRunId, lAttachCounter,
													lMailComponentName, lRgaCounter);
										}
										lAttachCounter++;
									}
								}
								if (lEmailLabels.size() > 0) {
//									lEmailLabels.add(new Html("<br/>"));
								}
							}
							lOutMailCounter++;
						}
						lMailCompCounter++;
					}
				}
				lRgaCounter++;
			}
		}
		/*
		 * for (Component lEmailLabel : lEmailLabels) {
		 * lListcell.appendChild(lEmailLabel); }
		 * 
		 * lListitem.appendChild(lListcell);
		 */
//		super.insertListitem(aListbox,lListitem,aInsertBefore);
	}

	public void initCopyMails() {
		getFileManager().deleteDir(getTempPath());
		tempPath = null;
		if (!(mailTitles == null))
			mailTitles.clear();
		else
			mailTitles = new ArrayList<String>();
		if ((infoFile == null) || (infoFile.isEmpty())) {
			infoFile = getTempPath() + "mailattachments.xls";
		}
		infoBook = new HSSFWorkbook();
		infoSheet = infoBook.createSheet("Overview");
		infoRowNumber = 0;
		HSSFRow lRow = infoSheet.createRow(infoRowNumber);
		lRow.createCell(0).setCellValue("user name");
		lRow.createCell(1).setCellValue("user id");
		lRow.createCell(2).setCellValue("attachment name");
		lRow.createCell(3).setCellValue("mail subject");
		lRow.createCell(4).setCellValue("run id");
		lRow.createCell(5).setCellValue("attachment number");
		lRow.createCell(6).setCellValue("mail component");
		lRow.createCell(7).setCellValue("rungroup account number");
	}

	protected void fillInfoRow(String aUserName, String aUserId, String aCopied, String aTitle, int aRunId,
			int aAttNumber, String aCompName, int aRgaNumber) {
		infoRowNumber++;
		HSSFRow lRow = infoSheet.createRow(infoRowNumber);
		lRow.createCell(0).setCellValue(aUserName);
		lRow.createCell(1).setCellValue(aUserId);
		lRow.createCell(2).setCellValue(aCopied);
		lRow.createCell(3).setCellValue(aTitle);
		lRow.createCell(4).setCellValue(aRunId);
		lRow.createCell(5).setCellValue(aAttNumber);
		lRow.createCell(6).setCellValue(aCompName);
		lRow.createCell(7).setCellValue(aRgaNumber);
	}

	public void saveInfoFile() {
		if (!((infoFile == null) || (infoFile.isEmpty()))) {
			try {
				FileOutputStream lOutStream = new FileOutputStream(infoFile);
				infoBook.write(lOutStream);
				lOutStream.close();
			} catch (Exception lEx) {
				_log.error(lEx);
			}
		}
	}

	protected String getHref(IXMLTag pAttachment) {
		String lHref = "";
		if (pAttachment != null)
			lHref = CDesktopComponents.sSpring().getSBlobHelper().getUrl(pAttachment);
		return lHref;
	}

	protected String getTempPath() {
		if ((tempPath == null) || (tempPath.isEmpty())) {
			CImportExportHelper lIOHelper = new CImportExportHelper();
			tempPath = lIOHelper.getTempPath();
			getFileManager().createDir(tempPath);
		}
		return tempPath;
	}

	protected String getAppPath() {
		if ((appPath == null) || (appPath.isEmpty())) {
			appPath = getAppManager().getAbsoluteAppPath();
		}
		return appPath;
	}

	protected String copyAttachmentFile(String aFrom, String aToBase) {
		String lExt = CDesktopComponents.vView().getFileExtension(aFrom);
		if (!((lExt == null) || (lExt.isEmpty())))
			aToBase = aToBase + "." + lExt;
		String lTo = getTempPath() + aToBase;
		aFrom = getAppPath() + aFrom;
		IFileManager lFileManager = getFileManager();
		String lCopied = "";
		if (!lFileManager.copyFile(aFrom, lTo)) {
			_log.info("error copying " + aFrom + " to " + lTo);
		} else {
			_log.info("copied " + aFrom + " to " + lTo);
			lCopied = aToBase;
		}
		return lCopied;
	}
}
