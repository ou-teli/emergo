/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.tut;

import java.util.List;

import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefListbox;
import nl.surf.emergo.domain.IECaseComponent;


/**
 * The Class CTutRunGroupAccountOwnOverviewCaseComponentsLb.
 */
public class CTutRunGroupAccountOwnOverviewCaseComponentsLb extends CDefListbox {

	private static final long serialVersionUID = -965357685388790575L;

	protected CTutRunGroupAccountOwnOverviewWnd root = (CTutRunGroupAccountOwnOverviewWnd)CDesktopComponents.vView().getComponent("ownOverviewWnd");

	public void onCreate() {
		List<IECaseComponent> tutCaseComponents = (List<IECaseComponent>)root.cControl.getAccSessAttr("tutCaseComponents");
		for (IECaseComponent caseComponent : root.getCaseComponents()) {
			String name = caseComponent.getName();
			String longName = "";
			if (root.areMultipleCases) {
				name += " - " + caseComponent.getECase().getName() + " (" + caseComponent.getECase().getVersion() + ")";
			}
			if (name.length() > 40) {
				longName = name;
				name = name.substring(0, 40);
			}
			Listitem listitem = root.addListitem(this, caseComponent, name, root.listContainsCaseComponent(tutCaseComponents, caseComponent));
			if (!longName.equals("")) {
				listitem.setTooltiptext(longName);
			}
		}
		Events.postEvent("onSelect", this, null);
		restoreSettings();
	}
	
	public void onUpdate() {
		List<IECaseComponent> tutCaseComponents = (List<IECaseComponent>)root.cControl.getAccSessAttr("tutCaseComponents");
		if (tutCaseComponents != null) {
			clearSelection();
			for (Listitem listitem : getItems()) {
				if (root.listContainsCaseComponent(tutCaseComponents, listitem.getValue())) {
					addItemToSelection(listitem);
				}
			}
		}
		Events.postEvent("onSelect", this, null);
	}
	
	public void onSelect() {
		root.setListboxSessionAttr(this, "tutCaseComponents");
		//update depending input elements
		Events.postEvent("onUpdate", root.vView.getComponent("componentAndTagDataCombined"), null);
		Events.postEvent("onUpdate", root.vView.getComponent("tagNames"), null);
		Events.postEvent("onUpdate", root.vView.getComponent("caseComponentTemplateBox"), null);
		Events.postEvent("onUpdate", root.vView.getComponent("tagKeyTemplateBox"), null);
		Events.postEvent("onUpdate", root.vView.getComponent("attributeKeys"), null);
		Events.postEvent("onUpdate", root.vView.getComponent("attributeValueTemplateBox"), null);
		Events.postEvent("onUpdate", root.vView.getComponent("includeDefaultValues"), null);
		Events.postEvent("onUpdate", root.vView.getComponent("includeHistory"), null);
		Events.postEvent("onUpdate", root.vView.getComponent("includeRunData"), null);
		Events.postEvent("onUpdate", root.vView.getComponent("includeStudentData"), null);
		Events.postEvent("onUpdate", root.vView.getComponent("attributeValueConvertPointsToCommas"), null);
		Events.postEvent("onUpdate", root.vView.getComponent("sortOnColumns"), null);
		Events.postEvent("onUpdate", root.vView.getComponent("excelSheetName"), null);
		Events.postEvent("onUpdate", root.vView.getComponent("excelRowOffset"), null);
		Events.postEvent("onUpdate", root.vView.getComponent("excelColOffset"), null);
		Events.postEvent("onUpdate", root.vView.getComponent("excelAddValues"), null);
		Events.postEvent("onUpdate", root.vView.getComponent("excelSplitAttributeValuesOn"), null);
		Events.postEvent("onUpdate", root.vView.getComponent("excelSplitAttributeValuesColOffset"), null);
		//update depending output elements
		Events.postEvent("onUpdateOutputElements", root.vView.getComponent("queryBox"), null);
	}
	
	public void onClearSelection() {
		clearSelection();
		Events.postEvent("onSelect", this, null);
	}
	
}
