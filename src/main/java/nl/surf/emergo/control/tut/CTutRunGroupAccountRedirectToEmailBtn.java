/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.tut;

import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.COpenWebPageUsingJavascriptBtn;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.domain.IERunTeam;
import nl.surf.emergo.view.VView;

/**
 * The Class CTutRunGroupAccountRedirectBtn. When clicked it opens the Emergo player read only
 * in a new browser instance for a particular run group account, possibly within in a particular run team.
 */
public class CTutRunGroupAccountRedirectToEmailBtn extends COpenWebPageUsingJavascriptBtn {

	private static final long serialVersionUID = 1886180735164339560L;

	/** The account. */
	protected IEAccount account = null;

	/** The rungroupaccount. */
	protected IERunGroupAccount rungroupaccount = null;

	/** The runteam. */
	protected IERunTeam runteam = null;

	/** The should be in team. */
	protected boolean shouldBeInTeam = false;

	/**
	 * Instantiates a new c tut run group account redirect btn.
	 *
	 * @param aId the a id
	 * @param aAccount the a account
	 */
	public CTutRunGroupAccountRedirectToEmailBtn(String aId,IEAccount aAccount) {
		if (!aId.equals(""))
			setId(aId);
		account = aAccount;
	}

	/**
	 * Sets the run group account and creates appropriate client action.
	 *
	 * @param aRunGroupAccount the new run group account
	 */
	public void setRunGroupAccount(IERunGroupAccount aRunGroupAccount) {
		rungroupaccount = aRunGroupAccount;
		createAction();
	}

	/**
	 * Sets the run team and creates appropriate client action.
	 *
	 * @param aRunTeam the new run team
	 */
	public void setRunTeam(IERunTeam aRunTeam) {
		runteam = aRunTeam;
		createAction();
	}

	/**
	 * Sets the should be in team and creates appropriate client action.
	 *
	 * @param aShouldBeInTeam the new should be in team
	 */
	public void setShouldBeInTeam(boolean aShouldBeInTeam) {
		shouldBeInTeam = aShouldBeInTeam;
		createAction();
	}

	/**
	 * Creates client action to open Emergo player read only in new browser instance for
	 * a run group account, possibly within a run team.
	 */
	private void createAction() {
		String lRgaId = "";
		if (rungroupaccount != null)
			lRgaId = "" + rungroupaccount.getRgaId();
		String lRutId = "";
		if (runteam != null)
			lRutId = "" + runteam.getRutId();
		String lParams =
			"&cacId=0"+
			"&tagId=0"+
			"&runstatus="+AppConstants.runStatusTutorRun+
			"&rgaId="+lRgaId+
			"&rutId="+lRutId+
			CDesktopComponents.cControl().getReqLangParams();

		if (shouldBeInTeam) {
			javascriptOnClickAction = "alert('" + CDesktopComponents.vView().getLabel("alert.notinteam") + "');";
		}
		else {
			String lUrl = CDesktopComponents.vView().uniqueView(CDesktopComponents.sSpring().getSCaseSkinHelper().getCaseSkinPath(rungroupaccount) + VView.v_run);
			if (!CDesktopComponents.vView().isAbsoluteUrl(lUrl)) {
				lUrl = CDesktopComponents.vView().getEmergoWebappsRoot() + "/" + lUrl;
			}
			javascriptOnClickAction = CDesktopComponents.vView().getJavascriptWindowOpenFullscreen(lUrl + lParams);
		}
	}
	
}
