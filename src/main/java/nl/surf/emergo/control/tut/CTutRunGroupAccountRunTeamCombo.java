/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.tut;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zul.Listitem;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefListbox;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.domain.IERunTeam;

/**
 * The Class CTutRunGroupAccountRunTeamCombo.
 */
public class CTutRunGroupAccountRunTeamCombo extends CDefListbox {

	private static final long serialVersionUID = 8838978716369834890L;

	/** The account. */
	protected IEAccount account = null;
	
	/** The rungroupaccount. */
	protected IERunGroupAccount rungroupaccount = null;

	/**
	 * Instantiates a new c tut run group account run team combo and renders possible run teams.
	 * 
	 * @param aId the a id
	 * @param aAccount the a account
	 * @param aRunTeams the a run teams
	 */
	public CTutRunGroupAccountRunTeamCombo(String aId,IEAccount aAccount,List<IERunTeam> aRunTeams) {
		setRows(1);
		setMold("select");
		if (!aId.equals(""))
			setId(aId);
		account = aAccount;
		update();
	}

	/**
	 * Sets the run group account and updates combobox.
	 * 
	 * @param aRunGroupAccount the new run group account
	 */
	public void setRunGroupAccount(IERunGroupAccount aRunGroupAccount) {
		rungroupaccount = aRunGroupAccount;
		update();
	}

	/**
	 * (Re)render combobox items and preselect one if applicable.
	 */
	public void update() {
		getChildren().clear();
		Listitem lListitem = super.insertListitem(null,CDesktopComponents.vView().getLabel("none"),null);
		if (rungroupaccount == null) return;
		CTutRunGroupAccountsLb lRungroupaccountsLb = (CTutRunGroupAccountsLb)getFellowIfAny("rungroupaccountsLb");
		if (lRungroupaccountsLb == null) return;
		List<IERunGroupAccount> lRunGroupAccounts = new ArrayList<IERunGroupAccount>(0);
		lRunGroupAccounts.add(rungroupaccount);
		List<IERunTeam> lRunTeams = lRungroupaccountsLb.getRunTeams(lRunGroupAccounts);
		setSelectedItem(lListitem);
		for (IERunTeam lObject : lRunTeams) {
			lListitem = super.insertListitem(lObject,lObject.getName(),null);
			if (lRunTeams.size() == 1)
				setSelectedItem(lListitem);
		}
	}
	
	/**
	 * On select update redirect button with selected run team.
	 */
	public void onSelect() {
		boolean lRunTeamSelected = (getSelectedItem().getValue() != null);
		CTutRunGroupAccountRedirectBtn lRunRedirectButton = (CTutRunGroupAccountRedirectBtn)getFellowIfAny("rungroupaccountsLbRedirectButton"+account.getAccId());
		lRunRedirectButton.setDisabled(!lRunTeamSelected);
		lRunRedirectButton.setRunTeam((IERunTeam)getSelectedItem().getValue());
	}
}
