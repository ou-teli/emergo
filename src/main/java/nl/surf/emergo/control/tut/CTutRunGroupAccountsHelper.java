/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.tut;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.zkoss.zul.Html;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.IRunAccountManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControlHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefHbox;
import nl.surf.emergo.control.def.CDefListcell;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunAccount;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.domain.IERunTeam;

/**
 * The Class CTutRunGroupAccountsHelper.
 */
public class CTutRunGroupAccountsHelper extends CControlHelper {

	/** The runteamcasecomponents. */
	private List<IECaseComponent> runteamcasecomponents = null;

	/** The casecomponents. */
	private List<IECaseComponent> casecomponents = null;

	/** The run account manager. */
	protected IRunAccountManager runAccountManager = (IRunAccountManager)CDesktopComponents.sSpring().getBean("runAccountManager");
	
	/**
	 * Renders one account and buttons and combo boxes to act on the account.
	 * 
	 * @param aListbox the a listbox
	 * @param aInsertBefore the a insert before
	 * @param aItem the a item
	 */
	public void renderItem(Listbox aListbox,Listitem aInsertBefore,Hashtable<String,Object> aItem) {
		Listitem lListitem = super.newListitem();
		List<IERunGroupAccount> lRunGroupAccounts = (List)aItem.get("rungroupaccounts");
		List<IERunTeam> lRunTeams = (List)aItem.get("runteams");
		IEAccount lItem = (IEAccount)aItem.get("item");
		lListitem.setValue(lItem);
		showTooltiptextIfAdmin(lListitem, "accId=" + lItem.getAccId());
		super.appendListcell(lListitem,lItem.getStudentid());
		super.appendListcell(lListitem,lItem.getTitle());
		super.appendListcell(lListitem,lItem.getInitials());
		super.appendListcell(lListitem,lItem.getNameprefix());
		super.appendListcell(lListitem,lItem.getLastname());

		int lRunId = (int)aItem.get("runid");
		Listcell lListcell = new CDefListcell();
		CDefHbox lHbox = new CDefHbox();
		IERunAccount lRunAccount = runAccountManager.getRunAccountByRunIdAccId(lRunId, lItem.getAccId());
		CTutRunAccountCb lCheckbox = new CTutRunAccountCb();
		lCheckbox.setAttribute("accId",""+lItem.getAccId());
		lCheckbox.setAttribute("runId",""+lRunId);
		lCheckbox.setAttribute("role",AppConstants.c_role_stu);
		boolean lChecked = false;
		if (lRunAccount != null)
			lChecked = lRunAccount.getStuactive();
		lCheckbox.setChecked(lChecked);
		lHbox.appendChild(lCheckbox);
		lListcell.appendChild(lHbox);
		lListitem.appendChild(lListcell);

		lListcell = new CDefListcell();
		boolean lShowCaseRoleCombo = (lRunGroupAccounts.size() > 1);
		if (lShowCaseRoleCombo) {
			CTutRunGroupAccountCaseRoleCombo lCaseRoleCombo = new CTutRunGroupAccountCaseRoleCombo("rungroupaccountsLbCaseRoleCombo"+lItem.getAccId(),lItem,lRunGroupAccounts);
			lListcell.appendChild(lCaseRoleCombo);
			lListcell.appendChild(new Html("<br/>"));
		}

		boolean lShowRunTeamCombo = ((lShowCaseRoleCombo && (lRunTeams.size() == 1)) || (lRunTeams.size() > 1));
		if (lShowRunTeamCombo) {
			CTutRunGroupAccountRunTeamCombo lRunTeamCombo = new CTutRunGroupAccountRunTeamCombo("rungroupaccountsLbRunTeamCombo"+lItem.getAccId(),lItem,lRunTeams);
			lRunTeamCombo.setDisabled(lShowCaseRoleCombo);
			if (lRunGroupAccounts.size() == 1)
				lRunTeamCombo.setRunGroupAccount((IERunGroupAccount)lRunGroupAccounts.get(0));
			lListcell.appendChild(lRunTeamCombo);
			lListcell.appendChild(new Html("<br/>"));
		}

		CTutRunGroupAccountRedirectBtn lRedirect = new CTutRunGroupAccountRedirectBtn("rungroupaccountsLbRedirectButton"+lItem.getAccId(),lItem);
		lRedirect.setLabel(CDesktopComponents.vView().getLabel("open_case"));
		lRedirect.setDisabled(lShowCaseRoleCombo || lShowRunTeamCombo);
		lRedirect.setRunStatus(AppConstants.runStatusPreviewReadOnly);

		CTutRunGroupAccountRedirectBtn lRedirectToMail = null;
		
		if (lRunGroupAccounts.size() == 1) {
			lRedirect.setRunGroupAccount((IERunGroupAccount)lRunGroupAccounts.get(0));
			IERun lRun = ((IERunGroupAccount)lRunGroupAccounts.get(0)).getERunGroup().getERun();
			if (runteamcasecomponents == null)
				runteamcasecomponents = CDesktopComponents.sSpring().getCaseComponentsByStatusType(lRun.getECase(), AppConstants.statusTypeRunTeam);
			List<IECaseComponent> lCaseComponents = new ArrayList<IECaseComponent>();
			for (IECaseComponent lCaseComponent : runteamcasecomponents) {
				if (lCaseComponent.getECase().getCasId() == lRun.getECase().getCasId())
					lCaseComponents.add(lCaseComponent);
			}
			boolean lShouldBeInTeam = (lCaseComponents.size() > 0);
			lRedirect.setShouldBeInTeam(lShouldBeInTeam);

			if (casecomponents == null)
				casecomponents = CDesktopComponents.sSpring().getCaseComponentsByStatusType(lRun.getECase(), AppConstants.statusTypeRunGroup);
			long lCacId = 0;
			for (IECaseComponent lCaseComponent : casecomponents) {
				if (lCaseComponent.getECase().getCasId() == lRun.getECase().getCasId()) {
					if (lCaseComponent.getEComponent().getCode().equals("mail"))
						lCacId = lCaseComponent.getCacId();
				}
			}
			if (lCacId > 0) {
				lRedirectToMail = new CTutRunGroupAccountRedirectBtn("rungroupaccountsLbRedirectButton"+lItem.getAccId(),lItem);
				lRedirectToMail.setLabel(CDesktopComponents.vView().getLabel("tut_rungroupaccounts.sendmail"));
				lRedirectToMail.setDisabled(lShowCaseRoleCombo || lShowRunTeamCombo);
				lRedirectToMail.setRunStatus(AppConstants.runStatusTutorRun);
				lRedirectToMail.setRunGroupAccount((IERunGroupAccount)lRunGroupAccounts.get(0));
				lRedirectToMail.setShouldBeInTeam(lShouldBeInTeam);
				lRedirectToMail.setCacId(lCacId);
			}
		}
		if (lRunTeams.size() == 1) {
			lRedirect.setRunTeam((IERunTeam)lRunTeams.get(0));
			IERun lRun = ((IERunTeam)lRunTeams.get(0)).getERun();
			if (runteamcasecomponents == null)
				runteamcasecomponents = CDesktopComponents.sSpring().getCaseComponentsByStatusType(lRun.getECase(), AppConstants.statusTypeRunTeam);
			List<IECaseComponent> lCaseComponents = new ArrayList<IECaseComponent>();
			for (IECaseComponent lCaseComponent : runteamcasecomponents) {
				if (lCaseComponent.getECase().getCasId() == lRun.getECase().getCasId())
					lCaseComponents.add(lCaseComponent);
			}
			boolean lShouldBeInTeam = (lCaseComponents.size() > 0);
			lRedirect.setShouldBeInTeam(lShouldBeInTeam);

			if (casecomponents == null)
				casecomponents = CDesktopComponents.sSpring().getCaseComponentsByStatusType(lRun.getECase(), AppConstants.statusTypeRunGroup);
			long lCacId = 0;
			for (IECaseComponent lCaseComponent : casecomponents) {
				if (lCaseComponent.getECase().getCasId() == lRun.getECase().getCasId()) {
					if (lCaseComponent.getEComponent().getCode().equals("mail"))
						lCacId = lCaseComponent.getCacId();
				}
			}
			if (lCacId > 0) {
				lRedirectToMail = new CTutRunGroupAccountRedirectBtn("rungroupaccountsLbRedirectButton"+lItem.getAccId(),lItem);
				lRedirectToMail.setLabel(CDesktopComponents.vView().getLabel("tut_rungroupaccounts.sendmail"));
				lRedirectToMail.setDisabled(lShowCaseRoleCombo || lShowRunTeamCombo);
				lRedirectToMail.setRunStatus(AppConstants.runStatusTutorRun);
				lRedirectToMail.setRunTeam((IERunTeam)lRunTeams.get(0));
				lRedirectToMail.setShouldBeInTeam(lShouldBeInTeam);
				lRedirectToMail.setCacId(lCacId);
			}
		}
		lListcell.appendChild(lRedirect);

		if (lRedirectToMail != null)
			lListcell.appendChild(lRedirectToMail);

		lListitem.appendChild(lListcell);
		super.insertListitem(aListbox,lListitem,aInsertBefore);
	}

}