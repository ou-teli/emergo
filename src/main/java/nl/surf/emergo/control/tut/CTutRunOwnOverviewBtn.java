/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.tut;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefButton;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.view.VView;

/**
 * The Class CTutRunOwnOverviewBtn.
 */
public class CTutRunOwnOverviewBtn extends CDefButton {

	private static final long serialVersionUID = -6738946335792637456L;

	/**
	 * Instantiates a new c tut run redirect run group account own overview btn.
	 */
	public CTutRunOwnOverviewBtn() {
	}

	/**
	 * On click open run group account emails window for the selected run.
	 */
	public void onClick() {
		Listbox lListbox = (Listbox)getFellowIfAny("runsLb");
		Set<Listitem> lListitems = lListbox.getSelectedItems();
		if (lListitems == null || lListitems.size() == 0) {
			CDesktopComponents.vView().showMessagebox(getRoot(), CDesktopComponents.vView().getLabel("tut_runs.messagebox.text.selectruns"),
					CDesktopComponents.vView().getLabel("messagebox.title.information"), Messagebox.OK, Messagebox.INFORMATION);
			return;
		}
		List<IERun> lRuns = new ArrayList<IERun>();
		for (Listitem lListitem : lListitems) {
			lRuns.add((IERun)lListitem.getValue());
		}
		CDesktopComponents.cControl().setAccSessAttr("runs", lRuns);
		CDesktopComponents.vView().redirectToView(VView.v_tut_rungroupaccountownoverview);
	}
}
