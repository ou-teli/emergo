/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.IXmlManager;
import nl.surf.emergo.control.def.CDefListbox;
import nl.surf.emergo.control.script.CScriptMethodHbox;

/**
 * The Abstract Class CCombo.
 * 
 * Used as ancestor for all comboxes used within the CScriptMethodHbox class.
 */
public abstract class CCombo extends CDefListbox {

	private static final long serialVersionUID = 796703899709674673L;

	/** The xml manager. */
	protected IXmlManager xmlManager = null;

	/** The label prefix. */
	protected String labelPrefix = "";

	/**
	 * Instantiates a new combobox.
	 * 
	 * @param aId the id
	 * @param aLabelPrefix the label prefix
	 */
	public CCombo(String aId, String aLabelPrefix) {
		setId(aId);
		setRows(1);
		setMold("select");
		labelPrefix = aLabelPrefix;
	}

	/**
	 * Instantiates a new combobox.
	 * 
	 * @param aId the id
	 * @param aAttKey the attribute key to set
	 * @param aAttValue the attribute value
	 * @param aLabelPrefix the label prefix
	 */
	public CCombo(String aId, String aAttKey, String aAttValue, String aLabelPrefix) {
		setId(aId);
		setRows(1);
		setMold("select");
		setAttribute(aAttKey, aAttValue);
		labelPrefix = aLabelPrefix;
	}

	/**
	 * Gets the xml manager.
	 * 
	 * @return the xml manager
	 */
	protected IXmlManager getXmlManager() {
		if (xmlManager == null)
			xmlManager = CDesktopComponents.sSpring().getXmlManager();
		return xmlManager;
	}

	/**
	 * Gets the item label using a number.
	 * There should be a label within the properties file ending with aId.
	 * 
	 * @param aId the id
	 * 
	 * @return the item label
	 */
	protected String getItemLabel(int aId) {
		return CDesktopComponents.vView().getLabel(labelPrefix + aId);
	}

	/**
	 * Shows combobox items using a list of objects.
	 * 
	 * @param aItems the items
	 * @param aId the id of the previously selected combobox item or empty
	 * @param aSendSelect if select should be sent
	 * @param aPreselectIfOneItem select one item
	 * @param aSort sort
	 */
	public void showItems(List<Object> aItems, String aId, boolean aSendSelect, boolean aPreselectIfOneItem, boolean aSort) {
		//items can change due to other selections so clear children
		getChildren().clear();
		if (aItems == null || aItems.size() == 0) {
			//if list empty hide
			setVisible(false);
			return;
		}
		setVisible(true);
		
		//get label with id 0, the none option, and add it
		String lId = "0";
		String lLabel = CDesktopComponents.vView().getLabel(labelPrefix + lId);
		if (lLabel.length() == 0) {
			//get default
			lLabel = CDesktopComponents.vView().getLabel("none");
		}
		Listitem lListitem = insertListitem(lId, lLabel, null);
		//preselect none option
		setSelectedItem(lListitem);
		Listitem lSelListitem = null;

		if (aSort) {
			//use a treemap to be able to sort aItems on key item name.
		    Map<String,Object> lMap = new TreeMap<String,Object>();
			for (Object lItem : aItems) {
				lMap.put(getItemName(lItem), lItem);
			}
			//loop through map to create new array of items
			List<Object> lItems = new ArrayList<Object>();
		    Iterator<Map.Entry<String,Object>> iterator = lMap.entrySet().iterator();
		    while(iterator.hasNext()) {
		    	lItems.add(iterator.next().getValue());
		    }
		    //use new array of items
		    aItems = lItems;
		}

		String lName = null;
		String lHelpText = null;
		for (Object lItem : aItems) {
			//add item
			lId = getItemId(lItem);
			lName = getItemName(lItem);
			lHelpText = getItemHelp(lItem);
			lListitem = insertListitem(lId, lName, null);
			if (!lHelpText.equals("")) {
				lListitem.setTooltiptext(lHelpText);
			}
			if ((aPreselectIfOneItem && aItems.size() == 1) || lId.equals(aId)) {
				//if only one item or item chosen before preselect it
				lSelListitem = lListitem;
				setSelectedItem(lSelListitem);
				//NOTE make tooltiptext of combobox equal to tooltiptext of selected item, so the tooltiptext will be shown.
				setTooltiptext(getSelectedItem().getTooltiptext());
			}
	    }
	    
		//if select should be sent and item preselected, emulate user selection
		if (aSendSelect && lSelListitem != null) {
			onSelect();
		}
	}

	/**
	 * Gets the item id. Should be overridden.
	 * 
	 * @param aItem the item
	 * 
	 * @return the item id
	 */
	protected abstract String getItemId(Object aItem);

	/**
	 * Gets the item name. Should be overridden.
	 * 
	 * @param aItem the item
	 * 
	 * @return the item name
	 */
	protected abstract String getItemName(Object aItem);

	/**
	 * Gets the item help. Should be overridden.
	 * 
	 * @param aItem the item
	 * 
	 * @return the item help
	 */
	protected abstract String getItemHelp(Object aItem);

	/**
	 * Gets the selected item id.
	 * 
	 * @return the item id
	 */
	protected String getSelectedItemId() {
		if (getSelectedItem() == null)
			return "";
		String lId = (String)getSelectedItem().getValue();
		if (lId == null)
			return "";
		return lId;
	}

	/**
	 * Gets the script method hbox.
	 * 
	 * @return the item id
	 */
	protected CScriptMethodHbox getScriptMethodHbox() {
		Component lComponent = this.getParent();
		while ((lComponent != null) && (!(lComponent instanceof CScriptMethodHbox))) {
			lComponent = lComponent.getParent();
		}
		return (CScriptMethodHbox)lComponent;
	}

	/**
	 * Gets the component code.
	 * 
	 * @return the component code
 	 */
	protected String getComponentCode() {
		CScriptMethodHbox lHbox = getScriptMethodHbox();
		if (lHbox == null || lHbox.getCaseComponent() == null)
			return "";
		return lHbox.getCaseComponent().getEComponent().getCode();
	}

	/**
	 * Gets the component code.
	 * 
	 * @return the component code
 	 */
	protected String getChosenTagName() {
		CScriptMethodHbox lHbox = getScriptMethodHbox();
		if (lHbox == null || lHbox.getCaseComponent() == null)
			return "";
		return lHbox.getChosenTagName();
	}

	/**
	 * On select set tooltiptext of combobox and notify the CScriptMethodHbox class.
	 */
	@Override
	public void onSelect() {
		if (getSelectedItem() != null) {
			//NOTE make tooltiptext of combobox equal to tooltiptext of selected item, so the tooltiptext will be shown.
			setTooltiptext(getSelectedItem().getTooltiptext());
		}
		CScriptMethodHbox lHbox = getScriptMethodHbox();
		if (lHbox == null)
			return;
		lHbox.onAction(this, "comboboxSelectItem", getSelectedItemId());
	}
	
}
