/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.crm;

import nl.surf.emergo.business.IRunGroupManager;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.adm.CAdmRunGroupAccountStatusBtn;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.view.VView;

/**
 * The Class CCrmRunStatusBtn.
 */
public class CCrmRunStatusBtn extends CAdmRunGroupAccountStatusBtn {

	private static final long serialVersionUID = 2323873310001164387L;

	@Override
	public void setSessionVars() {
//		set session var for case to use within script window		
		CDesktopComponents.cControl().setAccSessAttr("case", run.getECase());
	}

	@Override
	public String getEditView() {
		return VView.v_crm_s_runaccountstatus;
	}

	@Override
	public void saveAction(int aCacId, String aXmldata) {
		IRunGroupManager lRunGroupManager = (IRunGroupManager)CDesktopComponents.sSpring().getBean("runGroupManager");
		for (IERunGroup lRunGroup : lRunGroupManager.getAllRunGroupsByRunId(run.getRunId())) {
			if (lRunGroup.getActive()) {
				//only active run groups are updated 
				CDesktopComponents.sSpring().getSUpdateHelper().saveXmlDataComponentOrTagStatusUpdate(lRunGroup, aCacId, aXmldata);
			}
		}
	}
}
