/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.crm;

import java.util.ArrayList;
import java.util.List;

import nl.surf.emergo.business.IRunTeamManager;
import nl.surf.emergo.control.CControlHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefButton;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunTeam;

/**
 * The Class CCrmRunTeamOkBtn.
 */
public class CCrmRunTeamOkBtn extends CDefButton {

	private static final long serialVersionUID = 8439728829352564894L;

	/**
	 * On click create new item or update item if no errors and close edit window.
	 */
	public void onClick() {
		CCrmRunTeamWnd lWindow = (CCrmRunTeamWnd)getRoot();
		IERunTeam lItem = (IERunTeam)lWindow.getItem();
		IRunTeamManager runTeamManager = (IRunTeamManager)CDesktopComponents.sSpring().getBean("runTeamManager");
		Boolean lNew = false;
		if (lItem == null) {
//		new item
			lNew = true;
			lItem = runTeamManager.getNewRunTeam();
			lItem.setERun((IERun)CDesktopComponents.cControl().getAccSessAttr("run"));
		}
		else {
			//update
			lItem = (IERunTeam)lItem.clone();
		}
		lItem.setName(CControlHelper.getTextboxValue(this, "name"));
		lItem.setActive(CControlHelper.isCheckboxChecked(this, "active"));

		List<String[]> lErrors = new ArrayList<String[]>(0);
		if (lNew) {
			lErrors = runTeamManager.newRunTeam(lItem);
		}
		else {
			lErrors = runTeamManager.updateRunTeam(lItem);
		}
		if ((lErrors == null) || (lErrors.size() == 0)) {
//			Get runteam so date format, name etc. is correct.
			lItem = runTeamManager.getRunTeam(lItem.getRutId());
			lWindow.setAttribute("item",lItem);
		}
		else {
			lWindow.setAttribute("item",null);
			CDesktopComponents.cControl().showErrors(this,lErrors);
		}
		lWindow.detach();
	}
	
}
