/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.crm;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zul.Button;

import nl.surf.emergo.business.ICaseComponentManager;
import nl.surf.emergo.business.IContextManager;
import nl.surf.emergo.business.IRunManager;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefListbox;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERun;

/**
 * The Class CCrmRunsLb.
 */
public class CCrmRunsLb extends CDefListbox {

	private static final long serialVersionUID = 6883040706778127771L;

	/** The contextManager. */
	protected IContextManager contextManager = null;

	/** The hasContexts. */
	protected boolean hasContexts = false;

	/**
	 * On create render listitems.
	 */
	public void onCreate() {
		contextManager = (IContextManager)CDesktopComponents.sSpring().getBean("contextManager");
		hasContexts = (contextManager.getAllContexts(true).size() > 0);

		getItems().clear();
		List<IERun> lItems = ((IRunManager)CDesktopComponents.sSpring().getBean("runManager")).getAllRunnableRunsByAccId(CDesktopComponents.cControl().getAccId());
		CCrmRunsHelper cHelper = new CCrmRunsHelper();
		List<IECase> lCases = new ArrayList<IECase>();
		for (IERun lRun : lItems) {
			IECase lCase = lRun.getECase();
			if (!lCases.contains(lCase))
				lCases.add(lCase);
		}
		cHelper.setCases(lCases);
		for (IERun lRun : lItems) {
			cHelper.renderItem(this, null, lRun);
		}
		restoreSettings();
	}

	public void onSelect() {
		((Button)getFellow("editBtn")).setDisabled(false);
		((Button)getFellow("copyBtn")).setDisabled(false);
		((Button)getFellow("deleteBtn")).setDisabled(false);

		((CCrmRunAddPlayersLinkBtn)getFellow("addPlayersLinkBtn")).checkAddPlayersForm();
	}

	/**
	 * Has Contexts.
	 *
	 * @return if has contexts
	 */
	public boolean hasContexts() {
		return hasContexts;
	}

	/**
	 * Deletes run status.
	 * 
	 * @param aRun the a run
	 */
	public void deleteStatus(IERun aRun) {
		IECase lCase = aRun.getECase();
		List<IECaseComponent> lCaseComponents = ((ICaseComponentManager) (CDesktopComponents.sSpring().getBean("caseComponentManager"))).getAllCaseComponentsByCasId(lCase.getCasId());
		for (IECaseComponent lCaseComponent : lCaseComponents) {
			// clear status
			CDesktopComponents.sSpring().setRunCaseComponentStatus(aRun.getRunId(), lCaseComponent.getCacId(), "");
		}
	}
	
}
