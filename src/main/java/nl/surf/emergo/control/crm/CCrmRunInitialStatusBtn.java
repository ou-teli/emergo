/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.crm;

import nl.surf.emergo.business.IAppManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CInputBtn;
import nl.surf.emergo.control.CXmlHelper;

/**
 * The Class CCrmRunInitialStatusBtn.
 */
public class CCrmRunInitialStatusBtn extends CInputBtn {

	private static final long serialVersionUID = -3561243597286714890L;

	/**
	 * Validates and handles script action string tag if validation is ok.
	 * 
	 * @param aMethodTag the a method tag
	 * 
	 * @return if successful.
	 */
	protected boolean validateAndHandleMethodTag(IXMLTag aMethodTag) {
		if (aMethodTag == null)
			return false;
		String lCacId = CXmlHelper.getMethodCacId(aMethodTag);
		if (lCacId.equals("") || lCacId.equals("0"))
			return false;
		String lTagname = CXmlHelper.getMethodTagName(aMethodTag);
		String lStatusid = CXmlHelper.getMethodStatusId(aMethodTag);
		if (lStatusid.equals("") || lStatusid.equals("0"))
			return false;
		IAppManager lBean = (IAppManager)CDesktopComponents.sSpring().getBean("appManager");
		String lStatusKey = lStatusid;
		try {
			lStatusKey = lBean.getStatusKey(Integer.parseInt(lStatusid));
		} catch (NumberFormatException e) {
		}
		if (lStatusKey.equals(""))
			return false;
		String lOperatorvalue = CXmlHelper.getMethodOperatorValues(aMethodTag);
		if (lOperatorvalue.equals(""))
			return false;
		String lStatusValue = lOperatorvalue;
		String lType = CDesktopComponents.sSpring().getAppManager().getTagOperatorValueType("", lCacId, lTagname, lStatusid, "");
		if (lType.equals("boolean"))
			lStatusValue = lBean.getStatusValue(Integer.parseInt(lStatusValue));
		return true;
	}

	/**
	 * Creates new action tag.
	 * 
	 * @param aMethodTag the a set tag status tag
	 * 
	 * @return action tag.
	 */
	protected IXMLTag createNewActionTag(IXMLTag aMethodTag) {
		if (aMethodTag == null) {
			return null;
		}
		CCrmRunInitialStatusHelper cHelper = new CCrmRunInitialStatusHelper();
		IXMLTag lRootTag = cHelper.getXmlRunDataTree();
		if (lRootTag == null) {
			return null;
		}
		IXMLTag lContentTag = lRootTag.getChild(AppConstants.contentElement);
		if (lContentTag == null) {
			return null;
		}
		//create new action tag
		IXMLTag lActionTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag("action", "");
		lActionTag.setAttribute(AppConstants.defKeyType, AppConstants.defValueNode);
		
		lActionTag.setParentTag(lContentTag);
		lContentTag.getChildTags().add(lActionTag);

		IXMLTag lActionstringTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag("actionstring", "");
		
		lActionstringTag.setParentTag(lActionTag);
		lActionTag.getChildTags().add(lActionstringTag);
		
		aMethodTag.setParentTag(lActionstringTag);
		lActionstringTag.getChildTags().add(aMethodTag);
		
		if (cHelper.newChildNode(lActionTag, lContentTag).size() > 0) {
			//errors
			return null;
		}
		return lActionTag;
	}

	/**
	 * Updates existing action tag.
	 * 
	 * @param aActionTag the a action tag
	 * @param aMethodTag the a set tag status tag
	 * 
	 * @return action tag.
	 */
	protected IXMLTag updateExistingActionTag(IXMLTag aActionTag, IXMLTag aMethodTag) {
		if (aActionTag == null || aMethodTag == null) {
			return null;
		}
		CCrmRunInitialStatusHelper cHelper = new CCrmRunInitialStatusHelper();
		IXMLTag lOldMethodTag = cHelper.getMethodTag(aActionTag);
		if (lOldMethodTag == null) {
			return null;
		}
		IXMLTag lParentTag = lOldMethodTag.getParentTag();
		lParentTag.getChildTags().remove(lOldMethodTag);
		lParentTag.getChildTags().add(aMethodTag);
		if (cHelper.updateNode(aActionTag).size() > 0) {
			//errors
			return null;
		}
		return aActionTag;
	}

}
