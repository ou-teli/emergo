/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.crm;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.ICaseManager;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CInputListbox;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.view.VView;

/**
 * The Class CCrmRunCaseLb.
 */
public class CCrmRunCaseLb extends CInputListbox {

	private static final long serialVersionUID = 76808197339845167L;

	public CCrmRunCaseLb() {
		super();
	}

	/**
	 * On create render listitems and preselect one if applicable.
	 * 
	 * @param aEvent the a event
	 */
	public void onCreate(CreateEvent aEvent) {
		VView vView = CDesktopComponents.vView();
		IERun lItem = (IERun)((CCrmRunWnd)getRoot()).getItem(aEvent);
		ICaseManager caseManager = (ICaseManager)CDesktopComponents.sSpring().getBean("caseManager");
		Listitem lListitem = super.insertListitem(null,CDesktopComponents.vView().getLabel("none"),null);
		setSelectedItem(lListitem);
		for (IECase lCase : caseManager.getAllCases()) {
			String lVersion = "";
			if (lCase.getVersion() > 1) {
				lVersion = " (" + vView.getLabel("version") + "=" + lCase.getVersion() + ")";
			}
			String lCode = "";
			if (!StringUtils.isEmpty(lCase.getCode())) {
				lCode = " (" + vView.getLabel("code") + "='" + lCase.getCode() + "')";
			}
			lListitem = super.insertListitem(lCase, lCase.getName() + lVersion + lCode,null);
			if (lItem != null && lCase.getCasId() == lItem.getECase().getCasId()) {
				setSelectedItem(lListitem);
			}
		}
		setDisabled(((String)aEvent.getArg().get("isupdate")).equals("true"));
	}

}
