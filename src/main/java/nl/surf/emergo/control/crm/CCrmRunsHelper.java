/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.crm;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.IContextManager;
import nl.surf.emergo.business.IRunContextManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControlHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefListcell;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IEContext;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunContext;

/**
 * The Class CCrmRunsHelper.
 */
public class CCrmRunsHelper extends CControlHelper {

	/** The cases. */
	protected List<IECase> cases = null;

	/** The casecomponents. */
	protected List<IECaseComponent> casecomponents = null;

	/** The runContextManager. */
	protected IRunContextManager runContextManager = null;

	/** The contextManager. */
	protected IContextManager contextManager = null;

	/** The contextManager. */
	protected Hashtable<Integer,String> hContexts = null;

	public CCrmRunsHelper() {
		contextManager = (IContextManager)CDesktopComponents.sSpring().getBean("contextManager");
		runContextManager = (IRunContextManager)CDesktopComponents.sSpring().getBean("runContextManager");
		hContexts = new Hashtable<Integer,String>();
		List<IEContext> lContexts = contextManager.getAllContexts();
		for (IEContext lContext : lContexts) {
			hContexts.put(lContext.getConId(), lContext.getContext());
		}
	}

	/**
	 * Sets the cases.
	 *
	 * @param aCases the new cases
	 */
	public void setCases(List<IECase> aCases) {
		cases = aCases;
	}

	/**
	 * Renders one run with redirect buttons for run group accounts and run teams.
	 *
	 * @param aListbox the a listbox
	 * @param aInsertBefore the a insert before
	 * @param aItem the a item
	 */
	public void renderItem(Listbox aListbox, Listitem aInsertBefore, IERun aItem) {
		Listitem lListitem = super.newListitem();
		lListitem.setValue(aItem);
		showTooltiptextIfAdmin(lListitem, "runId=" + aItem.getRunId());
		super.appendListcell(lListitem,aItem.getCode());
		super.appendListcell(lListitem,aItem.getName());
		String lName = aItem.getECase().getName() + " (" + aItem.getECase().getVersion() + ")";
		Listcell lCell = super.appendListcell(lListitem,lName);
		showTooltiptextIfAdmin(lCell, "casId=" + aItem.getECase().getCasId());

		String lContexts = "";
		if (((CCrmRunsLb)aListbox).hasContexts()) {
			// if there are contexts, render run contexts
			List<IERunContext> lRunContexts = runContextManager.getAllRunContextsByRunId(aItem.getRunId());
			for (IERunContext lRunContext : lRunContexts) {
				if (!lContexts.equals(""))
					lContexts += ",";
				String lCon = (String)hContexts.get(lRunContext.getConConId());
				if (lCon != null)
					lContexts += lCon;
			}
		}
		super.appendListcell(lListitem,lContexts);

		super.appendListcell(lListitem,""+aItem.getActive());
		super.appendListcell(lListitem,getDateStrAsYMD(aItem.getStartdate()));
		super.appendListcell(lListitem,getDateStrAsYMD(aItem.getEnddate()));
//		String lRunning = vView.getLabel("boolean."+aItem.getRunning());
//		super.appendListcell(lListitem,lRunning);
		super.appendListcell(lListitem,getDateStrAsYMD(aItem.getCreationdate()));
		boolean lOpenAccess = aItem.getOpenaccess();
		super.appendListcell(lListitem,""+lOpenAccess);
		Listcell lListcell = new CDefListcell();
		CCrmRunRedirectRunGroupsBtn lRedirect = new CCrmRunRedirectRunGroupsBtn(lOpenAccess);
		lListcell.appendChild(lRedirect);
		lListitem.appendChild(lListcell);
		super.insertListitem(aListbox,lListitem,aInsertBefore);
		lListcell = new CDefListcell();
		List<IECase> lCases = cases;
		if (lCases == null) {
			lCases = new ArrayList<IECase>();
			lCases.add(aItem.getECase());
		}
		if (casecomponents == null)
			casecomponents = CDesktopComponents.sSpring().getCaseComponentsByStatusType(lCases, AppConstants.statusTypeRunTeam);
		List<IECaseComponent> lCaseComponents = new ArrayList<IECaseComponent>();
		for (IECaseComponent lCaseComponent : casecomponents) {
			if (lCaseComponent.getECase().getCasId() == aItem.getECase().getCasId())
				lCaseComponents.add(lCaseComponent);
		}
		if (lCaseComponents.size() > 0) {
			CCrmRunRedirectRunTeamsBtn lRedirect2 = new CCrmRunRedirectRunTeamsBtn();
			lListcell.appendChild(lRedirect2);
		}
		lListitem.appendChild(lListcell);

		lListcell = new CDefListcell();
		CCrmRunRedirectInitialStatusBtn lRedirect2 = new CCrmRunRedirectInitialStatusBtn();
		lListcell.appendChild(lRedirect2);
		lListitem.appendChild(lListcell);

		lListcell = new CDefListcell();
		CCrmRunTagStatusBtn lStatus1 = new CCrmRunTagStatusBtn();
		lStatus1.setLabel(CDesktopComponents.vView().getLabel("crm_runs.tagstatus"));
		lStatus1.setRun(aItem);
		lListcell.appendChild(lStatus1);
		lListitem.appendChild(lListcell);

		lListcell = new CDefListcell();
		CCrmRunComponentStatusBtn lStatus2 = new CCrmRunComponentStatusBtn();
		lStatus2.setLabel(CDesktopComponents.vView().getLabel("crm_runs.componentstatus"));
		lStatus2.setRun(aItem);
		lListcell.appendChild(lStatus2);
		lListitem.appendChild(lListcell);

		super.insertListitem(aListbox,lListitem,aInsertBefore);
	}
	
	/**
	 * Returns all (in)active run contexts for run id.
	 *
	 * @param runId the run id.
	 * @param contextActive the boolean for context to be active.
	 * 
	 * @return the list with run contexts.
	 */
	public List<IERunContext> getAllRunContextsByRunId(int runId, boolean contextActive) {
		List<IERunContext> lRunContexts = runContextManager.getAllRunContextsByRunId(runId);
		List<IERunContext> lResultRunContexts = new ArrayList<IERunContext>();
		if (lRunContexts != null) {
			for (IERunContext lRunContext : lRunContexts) {
				IEContext lContext = contextManager.getContext(lRunContext.getConConId());
				if (lContext.getActive() == contextActive) {
					lResultRunContexts.add(lRunContext);
				}
			}
		}
		return lResultRunContexts;
	}

}