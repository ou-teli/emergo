/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.crm;

import java.util.ArrayList;
import java.util.List;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.ICaseRoleManager;
import nl.surf.emergo.business.IRunGroupAccountManager;
import nl.surf.emergo.business.IRunGroupManager;
import nl.surf.emergo.business.IRunManager;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefCheckbox;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;

/**
 * The Class CCrmRunGroupAccountCb.
 */
public class CCrmRunGroupAccountCb extends CDefCheckbox {

	private static final long serialVersionUID = 5089192489590463971L;

	/** The account manager. */
	protected IAccountManager accountManager = (IAccountManager)CDesktopComponents.sSpring().getBean("accountManager");
	
	/** The run group account manager. */
	protected IRunGroupAccountManager runGroupAccountManager = (IRunGroupAccountManager)CDesktopComponents.sSpring().getBean("runGroupAccountManager");
	
	/** The run group manager. */
	protected IRunGroupManager runGroupManager = (IRunGroupManager)CDesktopComponents.sSpring().getBean("runGroupManager");
	
	/** The run manager. */
	protected IRunManager runManager = (IRunManager)CDesktopComponents.sSpring().getBean("runManager");
	
	/** The case role manager. */
	protected ICaseRoleManager caseRoleManager = (ICaseRoleManager)CDesktopComponents.sSpring().getBean("caseRoleManager");

	/** The errors. */
	protected List<String[]> errors = new ArrayList<String[]>(0);

	/**
	 * On check add run group and run group account for chosen account and case role, if it does not exist.
	 * And set active of run group to value of checked.
	 */
	public void onCheck() {
		int lAccId = Integer.parseInt((String)getAttribute("accId"));
//		get run
		int lRunId = Integer.parseInt((String)CDesktopComponents.cControl().getAccSessAttr("runId"));
//		get case role, there is only 1, otherwise crm has to choose one
		int lCarId = Integer.parseInt((String)getAttribute("carId"));
//		determine if rungroup/rungroupaccount exists
		IERunGroup lRungroup = getRungroup(lAccId,lRunId,lCarId);
		if (lRungroup == null) {
//			add rungroup
			lRungroup = addRungroup(lAccId,lRunId,lCarId,isChecked());
			if (lRungroup != null) {
				CCrmRunGroupAccountsHelper cHelper = new CCrmRunGroupAccountsHelper();
				cHelper.addRungroupaccount(accountManager.getAccount(lAccId),lRungroup);
			}
		}
//		set checked/unchecked
		lRungroup.setActive(isChecked());
		runGroupManager.updateRunGroup(lRungroup);
	}

	/**
	 * Gets the rungroup.
	 * 
	 * @param aAccId the a acc id
	 * @param aRunId the a run id
	 * @param aCarId the a car id
	 * 
	 * @return the rungroup
	 */
	private IERunGroup getRungroup(int aAccId,int aRunId,int aCarId) {
		for (IERunGroupAccount rungroupaccount : runGroupAccountManager.getAllRunGroupAccountsByAccId(aAccId)) {
			IERunGroup rungroup = rungroupaccount.getERunGroup();
			if ((rungroup.getERun().getRunId() == aRunId) && (rungroup.getECaseRole().getCarId() == aCarId))
				return rungroup;
		}
		return null;
	}

	/**
	 * Adds rungroup.
	 * 
	 * @param aAccId the a acc id
	 * @param aRunId the a run id
	 * @param aCarId the a car id
	 * @param aActive whether or not the run group is active
	 * 
	 * @return the rungroup
	 */
	private IERunGroup addRungroup(int aAccId,int aRunId,int aCarId, boolean aActive) {
		CCrmRunGroupAccountsHelper cHelper = new CCrmRunGroupAccountsHelper();
		IERunGroup lRungroup = cHelper.addRungroup(accountManager.getAccount(aAccId), runManager.getRun(aRunId), caseRoleManager.getCaseRole(aCarId), aActive);
		if (lRungroup == null) {
			errors = cHelper.getErrors();
		}
		return lRungroup;
	}

	/**
	 * Gets the errors.
	 * 
	 * @return the errors
	 */
	public List<String[]> getErrors() {
		return errors;
	}
}
