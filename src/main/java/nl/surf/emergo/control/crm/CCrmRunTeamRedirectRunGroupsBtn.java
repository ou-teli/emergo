/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.crm;

import org.zkoss.zul.Listitem;

import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CInputBtn;
import nl.surf.emergo.domain.IERunTeam;
import nl.surf.emergo.view.VView;

/**
 * The Class CCrmRunTeamRedirectRunGroupsBtn.
 */
public class CCrmRunTeamRedirectRunGroupsBtn extends CInputBtn {

	private static final long serialVersionUID = -9200106018221583414L;

	/**
	 * Instantiates a new c crm run team redirect run groups btn.
	 */
	public CCrmRunTeamRedirectRunGroupsBtn() {
		setLabel(CDesktopComponents.vView().getLabel("teammembers"));
	}

	/**
	 * On click open run team run groups window for the selected run team.
	 */
	public void onClick() {
		IERunTeam lItem = (IERunTeam)((Listitem)getParent().getParent()).getValue();
		CControl cControl = CDesktopComponents.cControl();
		cControl.setAccSessAttr("runteam",lItem);
		cControl.setAccSessAttr("rutId",""+lItem.getRutId());
		String lCrm = (String)cControl.getAccSessAttr("role");
		if (lCrm != null && lCrm.equals("crm")) {
			CDesktopComponents.vView().redirectToView(VView.v_crm_runteamrungroups);
		}
		else {
			showPopup(VView.v_s_preview_item_runteamrungroups);
		}
	}
	
}
