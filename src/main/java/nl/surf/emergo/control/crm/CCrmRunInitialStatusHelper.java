/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.crm;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.IAppManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CContentHelper;
import nl.surf.emergo.control.CControlHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CXmlHelper;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.view.VView;

/**
 * The Class CCrmRunInitialStatusHelper.
 */
public class CCrmRunInitialStatusHelper extends CControlHelper {

	/**
	 * Renders one status tag.
	 *
	 * @param aListbox the a listbox
	 * @param aInsertBefore the a insert before
	 * @param aActionTag the a action tag
	 */
	protected void renderItem(Listbox aListbox, Listitem aInsertBefore, IXMLTag aActionTag) {
		IXMLTag lMethodTag = getMethodTag(aActionTag);
		if (lMethodTag == null) {
			return;
		}
		String lType = "";
		if (lMethodTag.getName().equals("setruncomponentstatus")) {
			lType = "componentstatus";
		}
		else if (lMethodTag.getName().equals("setruntagstatus")) {
			lType = "tagstatus";
		}
		if (lType.equals("")) {
			return;
		}
		String lCacId = CXmlHelper.getMethodCacId(lMethodTag);
		IECaseComponent lCaseComponent = getCaseComponent(aActionTag);
		if (lCaseComponent == null) {
			return;
		}
		String lTagname = CXmlHelper.getMethodTagName(lMethodTag);
		String lContent = getContent(aActionTag);
		String lStatusid = CXmlHelper.getMethodStatusId(lMethodTag);
		IAppManager lBean = (IAppManager)CDesktopComponents.sSpring().getBean("appManager");
		String lStatusKey = lStatusid;
		try {
			lStatusKey = lBean.getStatusKey(Integer.parseInt(lStatusid));
		} catch (NumberFormatException e) {
		}
		String lOperatorvalue = CXmlHelper.getMethodOperatorValues(lMethodTag);
		String lStatusValue = lOperatorvalue;
		String lValueType = CDesktopComponents.sSpring().getAppManager().getTagOperatorValueType("", lCacId, lTagname, lStatusid, "");
		if (lValueType.equals("boolean")) {
			lStatusValue = lBean.getStatusValue(Integer.parseInt(lStatusValue));
		}

		Listitem lListitem = super.newListitem();
		lListitem.setValue(aActionTag);
		//type
		super.appendListcell(lListitem, CDesktopComponents.vView().getLabel("crm_runinitialstatus." + lType));
		//casecomponent
		super.appendListcell(lListitem, lCaseComponent.getName());
		//contenttype
		//NOTE Multiple case components may be selected, so there is no one component type. Therefore first parameter of getNodeTagLabel is empty string. 
		super.appendListcell(lListitem, CContentHelper.getNodeTagLabel("", lTagname));
		//content
		super.appendListcell(lListitem, lContent);
		//statuskey
		String lLabel = CDesktopComponents.vView().getLabel(VView.statuskeyLabelKeyPrefix + lStatusid);
		if (lLabel.equals("")) {
			lLabel = lStatusKey;
		}
		super.appendListcell(lListitem, lLabel);
		//statusvalue
		super.appendListcell(lListitem, lStatusValue);

		super.insertListitem(aListbox,lListitem,aInsertBefore);
	}
	
	/**
	 * Gets set tag status tag.
	 *
	 * @param aActionTag the a action tag
	 *
	 * @return set tag status tag
	 */
	protected IXMLTag getMethodTag(IXMLTag aActionTag) {
		if (aActionTag == null) {
			return null;
		}
		IXMLTag lActionstringTag = aActionTag.getChild("actionstring");
		if (lActionstringTag == null) {
			return null;
		}
		if (lActionstringTag.getChildTags().size() != 1) {
			//can have only one child
			return null;
		}
		return lActionstringTag.getChildTags().get(0);
	}
	
	/**
	 * Gets case component.
	 *
	 * @param aActionTag the a action tag
	 *
	 * @return case component
	 */
	protected IECaseComponent getCaseComponent(IXMLTag aActionTag) {
		IXMLTag lMethodTag = getMethodTag(aActionTag);
		if (lMethodTag == null) {
			return null;
		}
		String lCacId = CXmlHelper.getMethodCacId(lMethodTag);
		return CDesktopComponents.sSpring().getCaseComponent(lCacId);
	}
	
	/**
	 * Gets content.
	 *
	 * @param aActionTag the a action tag
	 *
	 * @return content
	 */
	protected String getContent(IXMLTag aActionTag) {
		IECaseComponent lCaseComponent = getCaseComponent(aActionTag);
		IXMLTag lMethodTag = getMethodTag(aActionTag);
		if (lMethodTag == null) {
			return "";
		}
		String lContent = "";
		String[] lTagIds = CXmlHelper.getMethodTagIds(lMethodTag);
		if (lTagIds.length != 1) {
			return "";
		}
		IXMLTag lDataTag = CDesktopComponents.sSpring().getTag(lCaseComponent, lTagIds[0]);
		if (lDataTag != null) {
			lContent = CDesktopComponents.sSpring().getXmlManager().getTagKeyValues(lDataTag, lDataTag.getDefAttribute(AppConstants.defKeyKey));
		}
		return lContent;
	}
	
	/**
	 * Gets the xml run data tree.
	 *
	 * @return the xml run data tree
	 */
	protected IXMLTag getXmlRunDataTree() {
		return CDesktopComponents.sSpring().getXmlRunDataTree((IERun)CDesktopComponents.cControl().getAccSessAttr("run"));
	}

	/**
	 * Sets xml run data tree.
	 *
	 * @param aRootTag the a root tag
	 */
	protected void setXmlRunDataTree(IXMLTag aRootTag) {
		CDesktopComponents.sSpring().setXmlRunDataTree((IERun)CDesktopComponents.cControl().getAccSessAttr("run"), aRootTag);
	}

	/**
	 * Creates new child xml node tag aItem within xml run data.
	 *
	 * @param aItem the a item
	 * @param aParentnode the a parentnode
	 *
	 * @return the error list, empty if ok
	 */
	protected List<String[]> newChildNode(IXMLTag aItem, IXMLTag aParentnode) {
		List<String[]> lErrors = new ArrayList<String[]>(0);
		IERun lRun = (IERun)CDesktopComponents.cControl().getAccSessAttr("run");
		if (lRun == null) {
			return lErrors;
		}
		IXMLTag lRootTag = getXmlRunDataTree();
		CDesktopComponents.sSpring().getXmlManager().newChildNode(lRootTag, aItem, aParentnode, lErrors);
		if (lErrors.size() == 0) {
			setXmlRunDataTree(lRootTag);
		}
		return lErrors;
	}

	/**
	 * Updates existing xml node tag aItem within xml run data.
	 *
	 * @param aItem the a item
	 *
	 * @return the error list, empty if ok
	 */
	protected List<String[]> updateNode(IXMLTag aItem) {
		List<String[]> lErrors = new ArrayList<String[]>(0);
		IERun lRun = (IERun)CDesktopComponents.cControl().getAccSessAttr("run");
		if (lRun == null) {
			return lErrors;
		}
		IXMLTag lRootTag = getXmlRunDataTree();
		CDesktopComponents.sSpring().getXmlManager().replaceNode(lRootTag, aItem, lErrors);
		if (lErrors.size() == 0) {
			setXmlRunDataTree(lRootTag);
		}
		return lErrors;
	}

	/**
	 * Deletes xml node tag.
	 *
	 * @param aItem the xml tag
	 */
	protected void deleteNode(IXMLTag aItem) {
		// remove as child in parenttag
		removeInParent(aItem);
		IXMLTag lRootTag = getXmlRunDataTree();
		CDesktopComponents.sSpring().getXmlManager().deleteNode(lRootTag, aItem);
		setXmlRunDataTree(lRootTag);
	}

	/**
	 * Removes an xml tag within the parent xml tag.
	 *
	 * @param aItem the xml tag
	 */
	protected void removeInParent(IXMLTag aItem) {
		if (aItem == null)
			return;
		IXMLTag lParent = aItem.getParentTag();
		if (lParent == null)
			return;
		List<IXMLTag> lChilds = lParent.getChildTags();
		for (int i = (lChilds.size() - 1); i >= 0; i--) {
			IXMLTag lChild = (IXMLTag) lChilds.get(i);
			if (lChild.getAttribute(AppConstants.keyId).equals(aItem.getAttribute(AppConstants.keyId)))
				lChilds.remove(i);
		}
	}

}