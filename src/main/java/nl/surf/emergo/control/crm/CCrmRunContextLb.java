/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.crm;

import java.util.Hashtable;
import java.util.List;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.IContextManager;
import nl.surf.emergo.business.IRunContextManager;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CInputListbox;
import nl.surf.emergo.domain.IEContext;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunContext;

/**
 * The Class CCrmRunContextLb.
 */
public class CCrmRunContextLb extends CInputListbox {

	private static final long serialVersionUID = -4215854569271706406L;
	
	private static String idmEducationIndicator = "%";

	/**
	 * On create render listitems and preselect items if applicable.
	 *
	 * @param aEvent the a event
	 */
	public void onCreate(CreateEvent aEvent) {
		IERun lItem = (IERun)((CCrmRunWnd)getRoot()).getItem(aEvent);
		boolean lNewRun = (lItem == null);
		IContextManager contextManager = (IContextManager)CDesktopComponents.sSpring().getBean("contextManager");
		// get all active contexts
		List<IEContext> lContexts = contextManager.getAllContexts(true);
		IRunContextManager runContextManager = (IRunContextManager)CDesktopComponents.sSpring().getBean("runContextManager");
		List<IERunContext> lRunContexts = null;
		if (!lNewRun)
			// only run contexts if run is created before
			lRunContexts = runContextManager.getAllRunContextsByRunId(lItem.getRunId());
		Hashtable<String,String> lHContexts = new Hashtable<String,String>(); 
		for (IEContext lContext : lContexts) {
			String lContextStr = lContext.getContext().toUpperCase();
			//NOTE contexts pushed by IDM contain idmEducationIndicator to indicate Education course belongs to.
			//Strip this part in presentation
			int lPos = lContextStr.indexOf(idmEducationIndicator);
			if (lPos > 0) {
				lContextStr = lContextStr.substring(0, lPos);
			}
			if (!lHContexts.containsKey(lContextStr)) {
				Listitem lListitem = insertListitem(lContext,lContextStr,null);
				lHContexts.put(lContextStr, lContextStr);
				if (!lNewRun) {
					// only run contexts if run is created before
					for (IERunContext lRunContext : lRunContexts) {
						if (lRunContext.getConConId() == lContext.getConId())
							addItemToSelection(lListitem);
					}
					// disabling multiple listbox doesn't work, so disable items.
//					lListitem.setDisabled(true);
				}
			}
		}
	}
}
