/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.crm;

import org.zkoss.zul.Listbox;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.view.VView;

/**
 * The Class CCrmRunInitialStatusEditBtn.
 */
public class CCrmRunInitialStatusEditBtn extends CCrmRunInitialStatusBtn {

	private static final long serialVersionUID = -4789877001813058935L;

	protected CCrmRunInitialStatusHelper cHelper = new CCrmRunInitialStatusHelper();

	/**
	 * On click show edit window for selected item and if changed replace with new item in listbox.
	 */
	public void onClick() {
		listbox = (Listbox)getFellowIfAny("runinitialstatusLb");
		listitem = listbox.getSelectedItem();
		if (listitem == null)
			return;
		item = listitem.getValue();
		if (item == null) {
			return;
		}
		IXMLTag lMethodTag = cHelper.getMethodTag((IXMLTag)item);
		params.put("item", lMethodTag);
		params.put("action", "edit");
		params.put("isupdate", "true");
		params.put("nodename", lMethodTag.getName());

		CDesktopComponents.cControl().setAccSessAttr("is_author", "true");

		showPopup(VView.v_crm_s_runinitialstatus);
	}

	@Override
	protected void handleItem(Object aStatus) {
		IXMLTag lMethodTag = (IXMLTag)aStatus;
		IXMLTag lActionTag = (IXMLTag)item;
//		Inserting item and deleting old item is more easy than trying to update one item.
		if (validateAndHandleMethodTag(lMethodTag)) {
			lActionTag = updateExistingActionTag(lActionTag, lMethodTag);
			if 	(lActionTag != null) {
				cHelper.renderItem((Listbox)getFellowIfAny("runinitialstatusLb"), listitem, lActionTag);
				listitem.detach();
			}
		}
	}

}
