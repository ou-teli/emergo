/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.crm;

import org.zkoss.zul.Listbox;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CInputBtn;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.utilities.PropsValues;
import nl.surf.emergo.view.VView;

/**
 * The Class CCrmRunNewBtn.
 */
public class CCrmRunNewBtn extends CInputBtn {

	private static final long serialVersionUID = -4458258508697823073L;

	/** The view. */

	/**
	 * On click show edit window for new item and if saved add new item in listbox.
	 */
	public void onClick() {
		listbox = (Listbox) getFellowIfAny("runsLb");
		params.put("action", "new");
		params.put("item", null);
		params.put("isupdate", "false");
		params.put("iscontext", "" + ((CCrmRunsLb) listbox).hasContexts());
		params.put("mailsubject", CDesktopComponents.vView().getLabel(
				(!PropsValues.ENCODE_PASSWORD ? "mail.access_to_run." : "mail.register_for_run.") + "subject"));
		params.put("mailbody",
				CDesktopComponents.vView().getLabel(
						(!PropsValues.ENCODE_PASSWORD ? "mail.access_to_run." : "mail.register_for_run.") + "body")
						.replaceAll("<br>", "\n"));
		showPopup(VView.v_crm_s_run);
	}

	@Override
	protected void handleItem(Object aStatus) {
		CCrmRunsHelper cHelper = new CCrmRunsHelper();
		cHelper.renderItem(listbox, null, (IERun) aStatus);
	}

}
