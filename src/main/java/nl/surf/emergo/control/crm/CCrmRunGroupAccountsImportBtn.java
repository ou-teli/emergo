/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.crm;

import java.util.ArrayList;
import java.util.Base64;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.Matcher;

import nl.surf.emergo.business.IAppManager;
import nl.surf.emergo.business.ICaseRoleManager;
import nl.surf.emergo.business.IRunGroupManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CAccountsImportBtn;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunContext;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.utilities.PropsValues;
import nl.surf.emergo.view.VView;

/**
 * The Class CCrmRunGroupAccountsImportBtn.
 *
 * Adds accounts from XML file to run. New rungroups are defined, or existing
 * rungroups are updated.
 *
 * template file for import:
 *
 * <?xml version="1.0" encoding="ISO-8859-1"?> <accounts>
 * <account id="" caseroles="'',''" sendaccountmail="true"></account>
 * <account id="" caseroles="'',''" sendaccountmail="false"></account>
 * </accounts>
 *
 * Could be the same file as for account import (see CAdmAccountsImportBtn)-
 * with combined account attributes - "caseroles" must be comma separated list:
 * - each element must point to the name of a (playing character) role defined
 * for this case - each element must be surrounded by "'" - if an account has a
 * rungroup for a case role not present in the import file, that rungroup will
 * be deactivated - account must be unique, must have student or tutor EMERGO
 * role
 *
 */
public class CCrmRunGroupAccountsImportBtn extends CAccountsImportBtn {

	private static final long serialVersionUID = 993190053574843462L;

	/** The label preface for the indices in the i3-label files. */
	protected static String labelPreface = "crm_rungroupaccounts.import.";

	/** The run id. */
	protected int runId = -1;

	/** The run. */
	protected IERun run = null;

	/** The case roles. */
	protected List<IECaseRole> caseRoles = null;

	/** The case role manager. */
	protected ICaseRoleManager caseRoleManager = (ICaseRoleManager) CDesktopComponents.sSpring()
			.getBean("caseRoleManager");

	/** The app manager. */
	protected IAppManager appManager = (IAppManager) CDesktopComponents.sSpring().getBean("appManager");

	/** The rungroup manager. */
	protected IRunGroupManager runGroupManager = (IRunGroupManager) CDesktopComponents.sSpring()
			.getBean("runGroupManager");

	/** The hasContexts. */
	protected boolean hasContexts = false;

	/** The crm runs helper. */
	protected CCrmRunsHelper crmRunsHelper = new CCrmRunsHelper();

	/**
	 * On create set visible.
	 *
	 * Not visible for open run, only tutors can be chosen (mostly only 1 or 2)
	 */
	public void onCreate() {
		run = (IERun) CDesktopComponents.cControl().getAccSessAttr("run");
		List<IERunContext> lRunContexts = crmRunsHelper.getAllRunContextsByRunId(run.getRunId(), true);
		hasContexts = ((lRunContexts != null) && (lRunContexts.size() > 0));
		setVisible((!run.getOpenaccess()) && (!hasContexts));
	}

	/**
	 * Processes the import of one account. New rungroups and rungroupaccounts are
	 * added to the database, or existing rungroups are disabled.
	 *
	 * @param aAccount the account to add to the run
	 *
	 * @return hashtable with account import information
	 */
	@Override
	protected Hashtable<String, Object> handleAccount(IXMLTag aAccount) {

		Boolean lValid = true;
		String lUserid = CDesktopComponents.sSpring().unescapeXML(aAccount.getAttribute("id"));
		StringBuilder lAccInfo = new StringBuilder();
		if ((lUserid == null) || (lUserid.equals(""))) {
			lAccInfo.append(CDesktopComponents.vView().getLabel("error.userid.empty") + "\n");
			lValid = false;
		} else
			lAccInfo.append(lUserid);
		Boolean lChanged = false;
		if (lValid) {
			IEAccount lExistingAccount = accountManager.getAccount(lUserid);
			if (!(lExistingAccount == null)) {
				if (accountManager.hasRole(lExistingAccount, AppConstants.c_role_stu)
						|| accountManager.hasRole(lExistingAccount, AppConstants.c_role_tut)) {
					if (runId == -1) {
						runId = Integer.parseInt((String) CDesktopComponents.cControl().getAccSessAttr("runId"));
						int caseId = run.getECase().getCasId();
						caseRoles = caseRoleManager.getAllCaseRolesByCasId(caseId, false);
					}
					String lAccRoles = CDesktopComponents.sSpring().unescapeXML(aAccount.getAttribute("caseroles"));
					// default no mail is sent with account data
					Boolean lSendAccountMail = stringToBoolean(aAccount.getAttribute("sendaccountmail"));
					CCrmRunGroupAccountsHelper cHelper = new CCrmRunGroupAccountsHelper();
					Boolean lRunGroupIsSetToActive = false;
					int lAccId = lExistingAccount.getAccId();
					IERunGroup lRunGroup = null;
					List<Integer> lRugIds = new ArrayList<Integer>();
					for (IECaseRole lCaseRole : caseRoles) {
						String lName = lCaseRole.getName();
						int lCarId = lCaseRole.getCarId();
						lRunGroup = cHelper.getRungroup(lAccId, runId, lCarId);
						if ((lAccRoles).indexOf("'" + lName + "'") >= 0) {
							if (lRunGroup == null) {
//								add rungroup
								// NOTE if password is encoded active will be false. If user registers it will
								// be set true.
								lRunGroup = cHelper.addRungroup(lExistingAccount, run, lCaseRole,
										!PropsValues.ENCODE_PASSWORD);
//								add rungroupaccount (only 1 per rungroup, no groups)
								if (lRunGroup != null) {
									Object lRungroupaccount = cHelper.addRungroupaccount(lExistingAccount, lRunGroup);
									if (lRungroupaccount == null) {
										lValid = false;
										lAccInfo.append(makeTabLine(
												CDesktopComponents.vView().getLabel("error.userid.import.runroles_rga")
														+ " " + lName + ":"));
										lAccInfo.append(makeTabLine(
												CDesktopComponents.cControl().getErrorString(cHelper.getErrors())));
									} else {
										lChanged = true;
										lRunGroupIsSetToActive = true;
										lRugIds.add(lRunGroup.getRugId());
										lAccInfo.append(makeTabLine(
												CDesktopComponents.vView().getLabel(labelPreface + "runrole_active_new")
														+ lName));
									}
								} else {
									lValid = false;
									lAccInfo.append(makeTabLine(
											CDesktopComponents.vView().getLabel("error.userid.import.runroles_rg") + " "
													+ lName + ":"));
									lAccInfo.append(makeTabLine(
											CDesktopComponents.cControl().getErrorString(cHelper.getErrors())));
								}
							} else {
								if (!lRunGroup.getActive()) {
									// NOTE don't set run group active. It is set active if a user registers
									// himself.
//									lRunGroup.setActive(true);
//									runGroupManager.updateRunGroup(lRunGroup);
									lChanged = true;
									lRunGroupIsSetToActive = true;
									lRugIds.add(lRunGroup.getRugId());
									lAccInfo.append(makeTabLine(
											CDesktopComponents.vView().getLabel(labelPreface + "runrole_active_new")
													+ lName));
								} else {
									lAccInfo.append(makeTabLine(
											CDesktopComponents.vView().getLabel(labelPreface + "runrole_active")
													+ lName));
								}
							}

						} else {
							if (lRunGroup != null) {
								if (lRunGroup.getActive()) {
									lRunGroup.setActive(false);
									runGroupManager.updateRunGroup(lRunGroup);
									lChanged = true;
									lAccInfo.append(makeTabLine(
											CDesktopComponents.vView().getLabel(labelPreface + "runrole_inactive_new")
													+ lName));
								} else {
									lAccInfo.append(makeTabLine(
											CDesktopComponents.vView().getLabel(labelPreface + "runrole_inactive")
													+ lName));
								}
							} else {
								lAccInfo.append(makeTabLine(
										CDesktopComponents.vView().getLabel(labelPreface + "runrole_inactive")
												+ lName));
							}
						}
					}
					if (lRunGroupIsSetToActive && lSendAccountMail) {
						String lMailError = sendAccountMail(lExistingAccount, lRugIds);
						if (lMailError.equals("")) {
							CDesktopComponents.sSpring().getSLogHelper()
									.logAction("crm import accounts for run " + runId
											+ ": accountmail sent to account id: " + lAccId + "; mailaddress: "
											+ lExistingAccount.getEmail() + "; notification to crm: "
											+ run.getEAccount().getEmail());
							lAccInfo.append(makeTabLine(
									CDesktopComponents.vView().getLabel(labelPreface + "send_account_mail")));
						} else {
							CDesktopComponents.sSpring().getSLogHelper().logAction("crm import accounts for run "
									+ runId + ": error in sending accountmail to account id: " + lAccId
									+ "; mailaddress: " + lExistingAccount.getEmail() + "; notification to crm: "
									+ run.getEAccount().getEmail() + "; error: " + lMailError);
							lAccInfo.append(makeTabLine(
									CDesktopComponents.vView().getLabel("error.userid.import.send_account_mail")));
							lAccInfo.append(makeTabLine(lMailError));
						}
					}
				} else {
					lAccInfo.append(makeTabLine(CDesktopComponents.vView().getLabel(labelPreface + "no_runroles")));
					lValid = false;
				}
			} else {
				lAccInfo.append(makeTabLine(CDesktopComponents.vView().getLabel("error.userid.not_found")));
				lValid = false;
			}
		}

		Hashtable<String, Object> lResult = new Hashtable<String, Object>(0);
		lResult.put("message", lAccInfo);
		lResult.put("valid", lValid);
		lResult.put("changed", lChanged);
		if (aAccount != null)
			lResult.put("account", aAccount);
		return lResult;
	}

	/**
	 * Regenerates the page after account import.
	 *
	 */
	@Override
	protected void regeneratePage() {
//		regenerate the accounts listbox
		CCrmRunGroupAccountsLb lListbox = (CCrmRunGroupAccountsLb) getFellowIfAny("rungroupaccountsLb");
		lListbox.update();
	}

	/**
	 * Send account mail.
	 * 
	 * @param aAccount the account
	 * @param aRugIds  the list of run group ids
	 *
	 * @return if successful
	 */
	protected String sendAccountMail(IEAccount aAccount, List<Integer> aRugIds) {
		String lUserName = accountManager.getAccountName(aAccount);
		String lCaseName = "";
		String lRunName = "";
		String lMailsubject = CDesktopComponents.vView().getLabel(
				(!PropsValues.ENCODE_PASSWORD ? "mail.access_to_run." : "mail.register_for_run.") + "subject");
		String lMailbody = CDesktopComponents.vView()
				.getLabel((!PropsValues.ENCODE_PASSWORD ? "mail.access_to_run." : "mail.register_for_run.") + "body");
		if (run != null) {
			lRunName = run.getName();
			lCaseName = run.getECase().getName();
			if (!run.getAccessmailsubject().equals("")) {
				lMailsubject = run.getAccessmailsubject();
			}
			if (!run.getAccessmailbody().equals("")) {
				lMailbody = run.getAccessmailbody();
			}
		}
		// NOTE if password is encoded it should not be sent by mail. It is the
		// responsibility of a case run manager to not include <password> in the mail
		String lSubject = convertMailText(lMailsubject, aAccount, aRugIds, lUserName, lCaseName, lRunName);
		String lBody = convertMailText(lMailbody, aAccount, aRugIds, lUserName, lCaseName, lRunName);
		String lCrmMailAddress = run.getEAccount().getEmail();
		return appManager.sendMail(appManager.getSysvalue(AppConstants.syskey_smtpnoreplysender), aAccount.getEmail(),
				null, lCrmMailAddress, lSubject, lBody);
	}

	/**
	 * Replaces tags in "send account mail" mail text.
	 *
	 * @param aMailText the email text
	 * @param aAccount  the account
	 * @param aRugIds   the list of run group ids
	 * @param aUserName the user name
	 * @param aCaseName the case name
	 */
	private String convertMailText(String aMailText, IEAccount aAccount, List<Integer> aRugIds, String aUserName,
			String aCaseName, String aRunName) {
		String lText = aMailText.replace("<emergo-login>",
				CDesktopComponents.vView().getEmergoRootUrl() + CDesktopComponents.vView().getEmergoWebappsRoot());
		lText = lText.replace("<userid>", Matcher.quoteReplacement(aAccount.getUserid()));
		lText = lText.replace("<password>", Matcher.quoteReplacement(aAccount.getPassword()));
		lText = lText.replace("<username>", Matcher.quoteReplacement(aUserName));
		lText = lText.replace("<casename>", Matcher.quoteReplacement(aCaseName));
		lText = lText.replace("<runname>", Matcher.quoteReplacement(aRunName));
		String lToken = "" + aAccount.getAccId();
		for (int lRugId : aRugIds) {
			lToken = lToken + "," + lRugId;
		}
		lToken = new String(Base64.getEncoder().encode(lToken.getBytes()));
		VView vView = CDesktopComponents.vView();
		lText = lText.replace("<register>",
				vView.getEmergoRootUrl() + vView.getEmergoWebappsRoot() + "/extra/register.zul?token=" + lToken);
		lText = lText.replace("<br>", "\n");
		return lText;
	}

}