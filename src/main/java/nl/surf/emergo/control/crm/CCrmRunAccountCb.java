/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.crm;

import java.util.ArrayList;
import java.util.List;

import nl.surf.emergo.business.IRunAccountManager;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefCheckbox;
import nl.surf.emergo.domain.IERunAccount;

/**
 * The Class CCrmRunAccountCb.
 */
public class CCrmRunAccountCb extends CDefCheckbox {

	private static final long serialVersionUID = -1497304260083273589L;

	/** The errors. */
	protected List<String[]> errors = new ArrayList<String[]>(0);

	/**
	 * On check add run account for chosen account and run, if it does not exist.
	 * And set active of run account to value of checked.
	 */
	public void onCheck() {
		int lAccId = Integer.parseInt((String)getAttribute("accId"));
//		get run
		int lRunId = Integer.parseInt((String)getAttribute("runId"));
		setRunaccountStatus(lRunId, lAccId, (String)getAttribute("role"), isChecked());
	}

	/**
	 * Gets the runaccount.
	 * 
	 * @param aAccId the a acc id
	 * @param aRunId the a run id
	 * @param aRole the account role attached to this checkbox
	 * @param aStatus the active status that has to be set for this account's role
	 * 
	 * @return the runaccount
	 */
	private IERunAccount setRunaccountStatus(int aRunId, int aAccId, String aRole, boolean aStatus) {
		IRunAccountManager lRunAccManager = (IRunAccountManager)CDesktopComponents.sSpring().getBean("runAccountManager");
		IERunAccount lRunaccount = lRunAccManager.addRunAccountIfNotExists(aRunId, aAccId, aRole);
		if (lRunaccount != null)
			lRunAccManager.setRunAccountStatus(lRunaccount, aRole, aStatus);
		return lRunaccount;
	}

	/**
	 * Gets the errors.
	 * 
	 * @return the errors
	 */
	public List<String[]> getErrors() {
		return errors;
	}
}
