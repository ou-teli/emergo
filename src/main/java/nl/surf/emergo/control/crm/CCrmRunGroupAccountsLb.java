/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.crm;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import nl.surf.emergo.business.IAccountContextManager;
import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.IContextManager;
import nl.surf.emergo.business.IRunContextManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefListbox;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IEContext;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunContext;

/**
 * The Class CCrmRunGroupAccountsLb.
 */
public class CCrmRunGroupAccountsLb extends CDefListbox {

	private static final long serialVersionUID = -366880273843649335L;

	/** The account manager. */
	protected IAccountManager accountManager = (IAccountManager)CDesktopComponents.sSpring().getBean("accountManager");

	/** The account context manager. */
	protected IAccountContextManager accountContextManager = (IAccountContextManager)CDesktopComponents.sSpring().getBean("accountContextManager");

	/** The context manager. */
	protected IContextManager contextManager = (IContextManager)CDesktopComponents.sSpring().getBean("contextManager");

	/** The run context manager. */
	protected IRunContextManager runContextManager = (IRunContextManager)CDesktopComponents.sSpring().getBean("runContextManager");

	/** The crm runs helper. */
	protected CCrmRunsHelper crmRunsHelper = new CCrmRunsHelper();
	
	private static String idmEducationIndicator = "%";

	/**
	 * On create render listitems.
	 */
	public void onCreate() {
		update();
		restoreSettings();
	}

	/**
	 * (Re)render listitems.
	 */
	public void update() {
		getItems().clear();
		IERun lRun = (IERun)CDesktopComponents.cControl().getAccSessAttr("run");
		int lRunId = lRun.getRunId();
		boolean lOpenRun = lRun.getOpenaccess();
		List<IEAccount> lItems = new ArrayList<IEAccount>();
		CControl cControl = CDesktopComponents.cControl();
		if (lOpenRun)
			// all student accounts already have access, so only get tutor accounts
			lItems = accountManager.getAllAccountsFilter(true,AppConstants.c_role_tut, (Map<String, String>)cControl.getAccSessAttr(AppConstants.account_filter_map));
		else {
			// get all active run contexts
			List<IERunContext> lRunContexts = crmRunsHelper.getAllRunContextsByRunId(lRunId,true);
			if ((lRunContexts == null) || (lRunContexts.size() == 0)) {
				// no active contexts, all student and tutor accounts are allowed
				lItems = accountManager.getAllAccountsFilter(true,AppConstants.c_role_tut + "," + AppConstants.c_role_stu, (Map<String, String>)cControl.getAccSessAttr(AppConstants.account_filter_map));
			}
			else {
				// get all active contexts
				List<IEContext> lContexts = contextManager.getAllContexts(true);
				// if there are contexts defined, only get student accounts with this contexts and tutor accounts
				Hashtable<String,String> lHConIds = new Hashtable<String,String>();
				List<Integer> lConIds = new ArrayList<Integer>();
				for (IERunContext lRunContext : lRunContexts) {
					//NOTE contexts pushed by IDM contain idmEducationIndicator to indicate Education course belongs to.
					//Also get these contexts
					String lRunContextStr = contextManager.getContext(lRunContext.getConConId()).getContext().toUpperCase();
					for (IEContext lContext : lContexts) {
						String lContextStr = lContext.getContext().toUpperCase();
						if (lContextStr.equals(lRunContextStr) || (lContextStr.indexOf(lRunContextStr + idmEducationIndicator) == 0)) {
							int lConId = lContext.getConId();
							if (!lHConIds.containsKey("" + lConId)) {
								lConIds.add(lConId);
								lHConIds.put("" + lConId, "");
							}
						}
					}					
				}
				// get student and tutor accounts
				List<IEAccount> lItems1 = accountManager.getAllAccountsFilter(true,AppConstants.c_role_tut, (Map<String, String>)cControl.getAccSessAttr(AppConstants.account_filter_map));
				//Hashtable<String,IEAccount> lHTutors = new Hashtable<String,IEAccount>();
				/*
				for (IEAccount lAccount : lItems1)
					lHTutors.put(""+lAccount.getAccId(), lAccount);
					*/
				List<IEAccount> lItems2 = accountManager.getAllAccountsFilter(true,AppConstants.c_role_stu, lConIds, (Map<String, String>)cControl.getAccSessAttr(AppConstants.account_filter_map));
				lItems1.addAll(lItems2);
				Hashtable<String,String> lHFoundAccounts = new Hashtable<String,String>();
				// get all active account contexts
				for (IEAccount lAccount : lItems1) {
					if (!lHFoundAccounts.containsKey(""+lAccount.getAccId())) {
						lHFoundAccounts.put(""+lAccount.getAccId(), "");
						lItems.add(lAccount);
					}
				}
				/*
				for (int i=(lItems.size()-1);i>=0;i--) {
					IEAccount lAccount = (IEAccount)lItems.get(i);
					if ((!lHFoundAccounts.containsKey(""+lAccount.getAccId())) && (!lHTutors.containsKey(""+lAccount.getAccId())))
						// if account does not have context and is no tutor account (so it is student account) remove it
						lItems.remove(i);
				}
				*/
			}
		}
		List<Hashtable<String,IEAccount>> lStuItems = new ArrayList<Hashtable<String,IEAccount>>(0);
		for (IEAccount lItem : lItems) {
			Hashtable<String,IEAccount> lStuItem = new Hashtable<String,IEAccount>(0);
			lStuItem.put("item",lItem);
			lStuItems.add(lStuItem);
		}
		CCrmRunGroupAccountsHelper cHelper = new CCrmRunGroupAccountsHelper();
		for (Hashtable<String,IEAccount> lStuItem : lStuItems) {
			cHelper.renderItem(this, null, lStuItem);
		}
	}

}
