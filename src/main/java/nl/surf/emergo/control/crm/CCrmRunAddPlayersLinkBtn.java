/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.crm;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.regex.Matcher;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.lang.Strings;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;

import nl.surf.emergo.business.IAppManager;
import nl.surf.emergo.business.ICaseRoleManager;
import nl.surf.emergo.business.IFileManager;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CInputBtn;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.utilities.AppHelper;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpring;

/**
 * The Class CCrmRunDeleteBtn.
 */
public class CCrmRunAddPlayersLinkBtn extends CInputBtn {

	private static final long serialVersionUID = 3610461468370568245L;

	/** the vView. */
	protected VView vView = CDesktopComponents.vView();

	private static final Logger _log = LogManager.getLogger(CCrmRunAddPlayersLinkBtn.class);
	
	protected String templateFileName;

	/**
	 * Gets the SSpring instantiation for the current desktop.
	 *
	 * @return the sSpring
	 */
	protected SSpring getSpring() {
		return CDesktopComponents.sSpring();
	}

	/**
	 * Gets the bean, given by aBeanId. A bean is an object defined within the
	 * Spring configuration file, that is injected by Spring at runtime.
	 *
	 * @param aBeanId the a bean id
	 *
	 * @return the bean
	 */
	protected Object getBean(String aBeanId) {
		return getSpring().getBean(aBeanId);
	}
	
	/** the file manager. */
	protected IFileManager fileManager = (IFileManager)getBean("fileManager");
	
	/** the app manager. */
	protected IAppManager appManager = (IAppManager)getBean("appManager");
	
	protected AppHelper appHelper = new AppHelper();

	public void onCreate() {
		templateFileName = appManager.getAbsoluteAppPath() + appHelper.getAbsolutePath(VView.getInitParameter("emergo.extra.path")) + VView.getInitParameter("emergo.crm.addplayers.templateform.file");
	}
	
	protected void checkAddPlayersForm() {
		IERun lRun = getRun();
		if (lRun == null) {
			((Hbox)getFellowIfAny("addPlayersLinkBox")).setVisible(true);
			setDisabled(true);
			((Hbox)getFellowIfAny("copyAddPlayersLinkBox")).setVisible(false);
			return;
		}
		setAddPlayersFormLink(getURLString(lRun));
	}
	
	protected void setAddPlayersFormLink(String pURLStr) {
		Hbox lAddBox = (Hbox)getFellowIfAny("addPlayersLinkBox");
		Hbox lCopyBox = (Hbox)getFellowIfAny("copyAddPlayersLinkBox");
		if (Strings.isEmpty(pURLStr)) {
			lAddBox.setVisible(true);
			setDisabled(false);
			lCopyBox.setVisible(false);
		} else {
			lAddBox.setVisible(false);
			lCopyBox.setVisible(true);
			((Label)getFellowIfAny("addPlayersLink")).setValue(pURLStr);
		}
	}
	
	protected IERun getRun() {
		listbox = (Listbox)getFellowIfAny("runsLb");
		listitem = listbox.getSelectedItem();
		if (listitem == null)
			return null;
		item = listitem.getValue();
		return (IERun)item;
	}
	
	/**
	 * On click, create add players form for selected run and show link to form.
	 */
	public void onClick() {
		createAddPlayersLink(getRun());
	}

	protected void createAddPlayersLink(IERun pRun) {
		if (pRun == null)
			return;
		if (fileManager.fileExists(templateFileName)) {
			byte[] lTempContent = fileManager.readFile(templateFileName);
			if ((lTempContent == null) || (lTempContent.length == 0)) {
				_log.info("ERROR crm - create game players application link: template file not readable: {}", templateFileName);
				return;
			}
			String lFilledFileStr = replaceRunParameters(pRun, new String(lTempContent));
			if ((lFilledFileStr == null) || (lFilledFileStr.length() == 0)) {
				_log.info("ERROR crm - create game players application link: template file empty: {}", templateFileName);
				return;
			}
			String lCasId = "" + pRun.getECase().getCasId();
			String lNewFilePrefix = lCasId + "_";
			Path lTFPath = Paths.get(templateFileName);
			int lPathCount = lTFPath.getNameCount();
			String lTargetDir = lTFPath.getRoot().resolve(lTFPath.subpath(0, lPathCount - 1)).resolve(lCasId).resolve(lNewFilePrefix).toString();
			String lTargetFileName = lTFPath.subpath(lPathCount - 1, lPathCount).toString();
			if (lTargetFileName.contains("_"))
				lTargetFileName = lTargetFileName.substring(0, lTargetFileName.lastIndexOf("_"));
			if (lTargetFileName.contains("."))
				lTargetFileName = lTargetFileName.substring(0, lTargetFileName.lastIndexOf("."));
			lTargetFileName += ("_" + pRun.getRunId() + ".zul");
			//NOTE createFile places new file in penultimate sub folder of lTargetDir, last sub folder name (lNewPrefix) is added as prefix of new file name 
			fileManager.createFile(lTargetDir, lTargetFileName, lFilledFileStr.getBytes());
			fileManager.deleteDir(lTargetDir);
			setAddPlayersFormLink(getURLString(pRun));
		}
		else {
			_log.info("ERROR crm - create game players application link: template file not found: {}", templateFileName);
		}
	}
	
	protected String replaceRunParameters(IERun pRun, String pTemplate) {
		String lCaseName = pRun.getECase().getName();
		String lHelpdeskEmail = vView.getLabel("helpdesk.email");
		ICaseRoleManager lCaseRoleManager = (ICaseRoleManager)getBean("caseRoleManager");
		List<IECaseRole> lCaseRoles = lCaseRoleManager.getAllCaseRolesByCasId(pRun.getECase().getCasId(), false);
		String lCRStr = "";
		for (IECaseRole lCaseRole : lCaseRoles) {
			if (lCRStr.length() > 0)
				lCRStr += ",";
			lCRStr += ("'" + Matcher.quoteReplacement(lCaseRole.getName()) + "'");
		}
		return pTemplate.replace("<runid>", "" + pRun.getRunId()).replace("<casename>", Matcher.quoteReplacement(lCaseName)).replace("<runname>", Matcher.quoteReplacement(pRun.getName())).replace("<caseroles>", lCRStr).replace("<helpdeskemail>", lHelpdeskEmail);
	}
	
	protected String getURLString(IERun pRun) {
		if (pRun == null)
			return "";
		String lCasId = "" + pRun.getECase().getCasId();
		String lFormPrefix = lCasId + "_";
		Path lTFPath = Paths.get(templateFileName);
		int lPathCount = lTFPath.getNameCount();
		String lTargetFileName = lTFPath.subpath(lPathCount - 1, lPathCount).toString();
		if (lTargetFileName.contains("_"))
			lTargetFileName = lTargetFileName.substring(0, lTargetFileName.lastIndexOf("_"));
		if (lTargetFileName.contains("."))
			lTargetFileName = lTargetFileName.substring(0, lTargetFileName.lastIndexOf("."));
		lTargetFileName += ("_" + pRun.getRunId() + ".zul");
		lTargetFileName = lFormPrefix + lTargetFileName; 
		String lFormFile = lTFPath.getRoot().resolve(lTFPath.subpath(0, lPathCount - 1)).resolve(lCasId).resolve(lTargetFileName).toString();
		String lFormURL = "";
		if (fileManager.fileExists(lFormFile)) {
			Path lAbsPath = Paths.get(appHelper.getAbsolutePath(VView.getInitParameter("emergo.extra.path")));
			lFormURL = vView.getAbsoluteWebappsRoot();
			lPathCount = lAbsPath.getNameCount();
			for (Path lPart : lAbsPath)
				lFormURL += lPart.toString() + "/";
			lFormURL += lCasId + "/" + lTargetFileName;
		}
		return lFormURL;
	}
	
}
