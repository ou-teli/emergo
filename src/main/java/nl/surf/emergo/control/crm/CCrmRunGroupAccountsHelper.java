/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.crm;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.ICaseRoleManager;
import nl.surf.emergo.business.IRunAccountManager;
import nl.surf.emergo.business.IRunGroupAccountManager;
import nl.surf.emergo.business.IRunGroupManager;
import nl.surf.emergo.business.IRunManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControlHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefHbox;
import nl.surf.emergo.control.def.CDefListcell;
import nl.surf.emergo.control.def.CDefVbox;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.domain.IERole;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunAccount;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;

/**
 * The Class CCrmRunGroupAccountsHelper.
 */
public class CCrmRunGroupAccountsHelper extends CControlHelper {

	/** The case roles. */
	protected List<IECaseRole> caseRoles = null; 
	
	/** The account manager. */
	protected IAccountManager accountManager = (IAccountManager)CDesktopComponents.sSpring().getBean("accountManager");

	/** The case role manager. */
	protected ICaseRoleManager caseRoleManager = (ICaseRoleManager)CDesktopComponents.sSpring().getBean("caseRoleManager");
	
	/** The run group account manager. */
	protected IRunGroupAccountManager runGroupAccountManager = (IRunGroupAccountManager)CDesktopComponents.sSpring().getBean("runGroupAccountManager");

	/** The run manager. */
	protected IRunManager runManager = (IRunManager)CDesktopComponents.sSpring().getBean("runManager");

	/** The run group manager. */
	protected IRunGroupManager runGroupManager = (IRunGroupManager)CDesktopComponents.sSpring().getBean("runGroupManager");

	/** The run account manager. */
	protected IRunAccountManager runAccountManager = (IRunAccountManager)CDesktopComponents.sSpring().getBean("runAccountManager");
	
	/** The errors. */
	protected List<String[]> errors = new ArrayList<String[]>(0);

	/** The run group accounts. */
	protected List<IERunGroupAccount> runGroupAccounts = null;
	
	/**
	 * Renders one account and for each case role a check box to add to run.
	 * 
	 * @param aListbox the a listbox
	 * @param aInsertBefore the a insert before
	 * @param aHItem the a h item
	 */
	public void renderItem(Listbox aListbox,Listitem aInsertBefore,Hashtable<String,IEAccount> aHItem) {
		Listitem lListitem = super.newListitem();
		IEAccount lItem = aHItem.get("item");
		lListitem.setValue(lItem);
		showTooltiptextIfAdmin(lListitem, "accId=" + lItem.getAccId());
		super.appendListcell(lListitem,lItem.getUserid());
		super.appendListcell(lListitem,lItem.getStudentid());
		super.appendListcell(lListitem,lItem.getTitle());
		super.appendListcell(lListitem,lItem.getInitials());
		super.appendListcell(lListitem,lItem.getNameprefix());
		super.appendListcell(lListitem,lItem.getLastname());
		super.appendListcell(lListitem,lItem.getEmail());
		super.appendListcell(lListitem,lItem.getExtradata());

		boolean lHasStudentRole = false;
		boolean lHasTutorRole = false;
		String lTutorCode = "";
		String lStudentCode = "";
		for (IERole lEmRole : lItem.getERoles()) {
			String lRoleCode = lEmRole.getCode();
			if (lRoleCode.equals(AppConstants.c_role_tut)) {
				lHasTutorRole = true;
				lTutorCode = lRoleCode;
			}
			if (lRoleCode.equals(AppConstants.c_role_stu)) {
				lHasStudentRole = true;
				lStudentCode = lRoleCode;
			}
		}
		String lRolesLabel = "";
		if (lHasTutorRole)
			lRolesLabel = CDesktopComponents.vView().getLabel(lTutorCode);
		if (lHasStudentRole) {
			if (!lRolesLabel.equals("")) 
				lRolesLabel = lRolesLabel + ", ";
			lRolesLabel = lRolesLabel + CDesktopComponents.vView().getLabel(lStudentCode);
		}
		super.appendListcell(lListitem,lRolesLabel);

		int lRunId = Integer.parseInt((String)CDesktopComponents.cControl().getAccSessAttr("runId"));
		Listcell lListcell = new CDefListcell();
		CDefHbox lHbox = new CDefHbox();
		if (lHasTutorRole) {
			IERunAccount lRunAccount = runAccountManager.getRunAccountByRunIdAccId(lRunId, lItem.getAccId());
			CCrmRunAccountCb lCheckbox = new CCrmRunAccountCb();
			lCheckbox.setAttribute("accId",""+lItem.getAccId());
			lCheckbox.setAttribute("runId",""+lRunId);
			lCheckbox.setAttribute("role",lTutorCode);
			boolean lChecked = false;
			if (lRunAccount != null)
				lChecked = lRunAccount.getTutactive();
			lCheckbox.setChecked(lChecked);
			lHbox.appendChild(lCheckbox);
			lHbox.appendChild(new Label(CDesktopComponents.vView().getLabel(lTutorCode)));
		}
		lListcell.appendChild(lHbox);
		lListitem.appendChild(lListcell);

		lListcell = new CDefListcell();
		CDefVbox lVbox = new CDefVbox();
		lVbox = new CDefVbox();
		if (lHasStudentRole) {
			int lCasId = Integer.parseInt((String)CDesktopComponents.cControl().getAccSessAttr("casId"));
			if (caseRoles == null)
				caseRoles = caseRoleManager.getAllCaseRolesByCasId(lCasId, false);
			for (IECaseRole lCaseRole : caseRoles) {
				CCrmRunGroupAccountCb lCheckbox = new CCrmRunGroupAccountCb();
				lCheckbox.setAttribute("accId",""+lItem.getAccId());
				lCheckbox.setAttribute("carId",""+lCaseRole.getCarId());
				IERunGroup lRunGroup = getRungroupFromCache(lItem.getAccId(),lRunId,lCaseRole.getCarId());
				lCheckbox.setChecked((lRunGroup != null) && (lRunGroup.getActive()));
				lHbox = new CDefHbox();
				lHbox.appendChild(lCheckbox);
				lHbox.appendChild(new Label(lCaseRole.getName()));
				lVbox.appendChild(lHbox);
			}
		}
		lListcell.appendChild(lVbox);
		lListitem.appendChild(lListcell);
		
		super.insertListitem(aListbox,lListitem,aInsertBefore);
	}

	/**
	 * Gets the roles.
	 * 
	 * @param aItem the a item
	 * 
	 * @return the rolecodes, comma separated
	 */
	public String getRoles(IEAccount aItem) {
		String lRoles = "";
		for (IERole lItem : aItem.getERoles()) {
			if ((lItem.getCode().equals("tut")) || (lItem.getCode().equals("stu"))) {
				if (!lRoles.equals("")) lRoles = lRoles + ",";
				lRoles = lRoles + CDesktopComponents.vView().getLabel(lItem.getCode());
			}
		}
		return lRoles;
	}

	/**
	 * Gets the rungroup for account, run, caserole.
	 * Uses cache of rungroupaccounts for run.  
	 * 
	 * @param aAccId the a acc id
	 * @param aRunId the a run id
	 * @param aCarId the a car id
	 * 
	 * @return the rungroup
	 */
	private IERunGroup getRungroupFromCache(int aAccId,int aRunId,int aCarId) {
		if (runGroupAccounts == null) {
			List<IERunGroup> lRugs = (List<IERunGroup>)runGroupManager.getAllRunGroupsByRunId(aRunId);
			List<Integer> lRugIds = new ArrayList<Integer>(0);
			for (IERunGroup lRug : lRugs) {
				lRugIds.add(lRug.getRugId());
			}
			runGroupAccounts = runGroupAccountManager.getAllRunGroupAccountsByRugIds(lRugIds);
		}
		for (IERunGroupAccount rungroupaccount : runGroupAccounts) {
			IERunGroup rungroup = rungroupaccount.getERunGroup();
			if ((rungroupaccount.getEAccount().getAccId() == aAccId) && (rungroup.getECaseRole().getCarId() == aCarId))
				return rungroup;
		}
		return null;
	}

	/**
	 * Gets the rungroup.
	 * 
	 * @param aAccId the a acc id
	 * @param aRunId the a run id
	 * @param aCarId the a car id
	 * 
	 * @return the rungroup
	 */
	public IERunGroup getRungroup(int aAccId,int aRunId,int aCarId) {
		for (IERunGroupAccount rungroupaccount : runGroupAccountManager.getAllRunGroupAccountsByAccId(aAccId)) {
			IERunGroup rungroup = rungroupaccount.getERunGroup();
			if ((rungroup.getERun().getRunId() == aRunId) && (rungroup.getECaseRole().getCarId() == aCarId))
				return rungroup;
		}
		return null;
	}

	/**
	 * Adds rungroup.
	 * 
	 * @param aAcc the account
	 * @param aRun the run
	 * @param aCar the case role
	 * @param aActive whether or not the run group is active
	 * 
	 * @return the rungroup
	 */
	public IERunGroup addRungroup(IEAccount aAcc, IERun aRun, IECaseRole aCar, boolean aActive) {
		IERunGroup lRungroup = runGroupManager.getNewRunGroup();
		lRungroup.setERun(aRun);
		lRungroup.setECaseRole(aCar);
		String lName = accountManager.getAccountName(aAcc);
		Boolean lExists = runGroupManager.runGroupExists(aCar.getCarId(), aRun.getRunId(), lName);
//		error when adding rungroup with name that exists for other account
		while (lExists) {
			lName = lName + "_";
			lExists = runGroupManager.runGroupExists(aCar.getCarId(), aRun.getRunId(), lName);
		}
		lRungroup.setName(lName);
		lRungroup.setComposite(false);
		lRungroup.setActive(aActive);
		errors = runGroupManager.newRunGroup(lRungroup);
		if (!(errors == null) && (errors.size() > 0))
			return null;
		//NOTE method runGroupManager.newRunGroup sets active to true
		lRungroup.setActive(aActive);
		runGroupManager.updateRunGroup(lRungroup);
		return lRungroup;
	}

	/**
	 * Adds rungroup.
	 * 
	 * @param aAccId the account id
	 * @param aRunId the run id
	 * @param aCarId the case role id
	 * @param aActive whether or not the run group is active
	 * 
	 * @return the rungroup
	 */
	public IERunGroup addRungroup(int aAccId, int aRunId, int aCarId, boolean aActive) {
		IEAccount lAcc = accountManager.getAccount(aAccId);
		IERun lRun = ((IRunManager)CDesktopComponents.sSpring().getBean("runManager")).getRun(aRunId);
		IECaseRole lCar = caseRoleManager.getCaseRole(aCarId);
		return addRungroup (lAcc, lRun, lCar, aActive); 
	}

	/**
	 * Adds rungroup if not already present.
	 * 
	 * @param aAccId the account id
	 * @param aRunId the run id
	 * @param aCarId the case role id
	 * @param aActive whether or not the run group is active
	 * 
	 * @return the rungroup
	 */
	public IERunGroup addRungroupIfNotExists(int aAccId, int aRunId, int aCarId, boolean aActive) {
		IERunGroup lRug = getRungroup(aAccId, aRunId, aCarId);
		if (lRug != null)
			return lRug;
		return addRungroup (aAccId, aRunId, aCarId, aActive); 
	}

	/**
	 * Adds rungroupaccount.
	 * 
	 * @param aAcc the account
	 * @param aRungroup the rungroup
	 * 
	 * @return the run group account
	 */
	public IERunGroupAccount addRungroupaccount(IEAccount aAcc,IERunGroup aRungroup) {
//		add rungroupaccount (only 1 per rungroup, no groups)
		IERunGroupAccount lRungroupaccount = runGroupAccountManager.getNewRunGroupAccount();
		lRungroupaccount.setEAccount(aAcc);
		lRungroupaccount.setERunGroup(aRungroup);
		errors = runGroupAccountManager.newRunGroupAccount(lRungroupaccount);
		if (!(errors == null) && (errors.size() > 0))
			return null;
		runAccountManager.addRunAccountIfNotExists(aRungroup.getERun().getRunId(),aAcc.getAccId(), AppConstants.c_role_stu);
		return lRungroupaccount;
	}

	/**
	 * Adds rungroupaccount.
	 * 
	 * @param aAccId the account id
	 * @param aRungroupId the rungroup id
	 * 
	 * @return the run group account
	 */
	public IERunGroupAccount addRungroupaccount(int aAccId, int aRungroupId) {
//		add rungroupaccount (only 1 per rungroup, no groups)
		IEAccount lAcc = accountManager.getAccount(aAccId);
		IERunGroup lRungroup = runGroupManager.getRunGroup(aRungroupId);
		return addRungroupaccount(lAcc,lRungroup);
	}

	/**
	 * Adds rungroupaccount if not already present.
	 * 
	 * @param aAccId the account id
	 * @param aRungroupId the rungroup id
	 * 
	 * @return the run group account
	 */
	public IERunGroupAccount addRungroupaccountIfNotExists(int aAccId, int aRungroupId) {
//		add rungroupaccount (only 1 per rungroup, no groups)
		List<IERunGroupAccount> lRugAccs = (List<IERunGroupAccount>)runGroupAccountManager.getAllRunGroupAccountsByRugIdAccId(aRungroupId, aAccId);
		if (lRugAccs.size() > 0)
			return lRugAccs.get(0);
		return addRungroupaccount(aAccId,aRungroupId);
	}

	/**
	 * Gets the errors.
	 * 
	 * @return the errors
	 */
	public List<String[]> getErrors() {
		return errors;
	}
}