/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.crm;

import org.zkoss.zul.Listbox;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CInputBtn;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;

/**
 * The Class CCrmRunInitialStatusDeleteBtn.
 */
public class CCrmRunInitialStatusDeleteBtn extends CInputBtn {

	private static final long serialVersionUID = 5432112302848054690L;

	protected CCrmRunInitialStatusHelper cHelper = new CCrmRunInitialStatusHelper();

	/**
	 * On click, after confirmation delete selected item and remove item from listbox.
	 */
	public void onClick() {
		listbox = (Listbox)getFellowIfAny("runinitialstatusLb");
		listitem = listbox.getSelectedItem();
		if (listitem == null)
			return;
		item = listitem.getValue();
		IECaseComponent lCaseComponent = cHelper.getCaseComponent((IXMLTag)item);
		if (lCaseComponent == null) {
			return;
		}
		String lItemStr = lCaseComponent.getName() + ": " + cHelper.getContent((IXMLTag)item);
		params.put("item", lItemStr);
		params.put("itemtype",CDesktopComponents.vView().getLabel("crm_runinitialstatus.item"));
		showPopup(VView.v_delete_item);
	}

	@Override
	protected void deleteItem() {
		cHelper.deleteNode((IXMLTag)item);
		listitem.detach();
	}

}
