/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.crm;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.IContextManager;
import nl.surf.emergo.business.IRunContextManager;
import nl.surf.emergo.business.IRunManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControlHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefButton;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.domain.IEContext;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunContext;

/**
 * The Class CCrmRunOkBtn.
 */
public class CCrmRunOkBtn extends CDefButton {

	private static final long serialVersionUID = -849290966733976366L;

	/** The context manager. */
	protected IContextManager contextManager = (IContextManager)CDesktopComponents.sSpring().getBean("contextManager");;
	
	/** The run context manager. */
	protected IRunContextManager runContextManager = (IRunContextManager)CDesktopComponents.sSpring().getBean("runContextManager");;
	
	/**
	 * On click create new item or update item if no errors and close edit window.
	 */
	public void onClick() {
		CCrmRunWnd lWindow = (CCrmRunWnd)getRoot();
		IERun lItem = (IERun)lWindow.getItem();
		String lAction = lWindow.getAction();
		IRunManager runManager = (IRunManager)CDesktopComponents.sSpring().getBean("runManager");
		boolean lNew = (lAction.equals("new"));
		boolean lCopy = (lAction.equals("copy"));
		IERun lOldItem = lItem;
		if (lItem == null) {
			//new
			lItem = runManager.getNewRun();
			lItem.setEAccount(CDesktopComponents.sSpring().getAccount());
		}
		else {
			//update
			lItem = (IERun)lItem.clone();
		}
		if (lNew) {
			// case is set once and can not be changed
			lItem.setECase((IECase)CControlHelper.getListboxValue(this, "casCasId"));
		}
		lItem.setCode(CControlHelper.getTextboxValue(this, "code"));
		lItem.setName(CControlHelper.getTextboxValue(this, "name"));
		
		
		/*
		Date lStartDate = new Date();
		//NOTE Date methods are deprecated, but this is the only way to get date right!
		//1. If you change time in ZK time box in the interface, its year, month and date are set to current date values even if the original year,
		//month or date was earlier or later.
		//2. So I tried to use getTime of the values of date and time box, to fix the problem, by stripping time from the date box value and
		//stripping date from the time box value. Still I got problems because saved times were 1 hour higher and/or saved date was 1 day earlier.
		//3. Calendar should be used instead of Date, but ZK date and time box use Date so this is no option.
		lStartDate.setYear(lStartDateTimeDate.getYear());
		lStartDate.setMonth(lStartDateTimeDate.getMonth());
		lStartDate.setDate(lStartDateTimeDate.getDate());
		lStartDate.setHours(lStartDateTimeTime.getHours());
		lStartDate.setMinutes(lStartDateTimeTime.getMinutes());
		lStartDate.setSeconds(lStartDateTimeTime.getSeconds());
		lItem.setStartdate(lStartDate);
		*/
		
		Date lStartDateTimeDate = CControlHelper.getDateboxValue(this, "startdate");
		Date lStartDateTimeTime = CControlHelper.getTimeboxValue(this, "starttime");
		Calendar lCalStart = dateToCalendar(lStartDateTimeDate, lStartDateTimeTime);
		//NOTE timezone information from Calendar object is lost?
		Date lStartDate = lCalStart.getTime();
		lItem.setStartdate(lStartDate);
		
		/*
		Date lEndDate = new Date();
		lEndDate.setYear(lEndDateTimeDate.getYear());
		lEndDate.setMonth(lEndDateTimeDate.getMonth());
		lEndDate.setDate(lEndDateTimeDate.getDate());
		lEndDate.setHours(lEndDateTimeTime.getHours());
		lEndDate.setMinutes(lEndDateTimeTime.getMinutes());
		lEndDate.setSeconds(lEndDateTimeTime.getSeconds());
		lItem.setEnddate(lEndDate);
		*/
		
		Date lEndDateTimeDate = CControlHelper.getDateboxValue(this, "enddate");
		Date lEndDateTimeTime = CControlHelper.getTimeboxValue(this, "endtime");
		Calendar lCalEnd = dateToCalendar(lEndDateTimeDate, lEndDateTimeTime);
		//NOTE timezone information from Calendar object is lost?
		Date lEndDate = lCalEnd.getTime();
		lItem.setEnddate(lEndDate);

		lItem.setActive(CControlHelper.isCheckboxChecked(this, "active"));
		lItem.setOpenaccess(CControlHelper.isCheckboxChecked(this, "openaccess"));

		String lAccessmailsubject = CControlHelper.getTextboxValue(this, "accessmailsubject").replaceAll("\n","<br>");
		if (lAccessmailsubject.equals(CDesktopComponents.vView().getLabel("mail.access_to_run.subject")) || lAccessmailsubject.equals(CDesktopComponents.vView().getLabel("mail.register_for_run.subject"))) {
			lAccessmailsubject = "";
		}
		lItem.setAccessmailsubject(lAccessmailsubject);
		String lAccessmailbody = CControlHelper.getTextboxValue(this, "accessmailbody").replaceAll("\n","<br>");
		if (lAccessmailbody.equals(CDesktopComponents.vView().getLabel("mail.access_to_run.body")) || lAccessmailsubject.equals(CDesktopComponents.vView().getLabel("mail.register_for_run.body"))) {
			lAccessmailbody = "";
		}
		lItem.setAccessmailbody(lAccessmailbody);
		if (lNew) {
			lItem.setXmldata(AppConstants.emptyXml);
		}
		
		List<String[]> lErrors = new ArrayList<String[]>(0);
		if (lNew || lCopy) {
			lItem.setRunId(0);
			lErrors = runManager.newRun(lItem);
		}
		else {
			lErrors = runManager.updateRun(lItem);
		}
		if (lErrors == null || lErrors.size() == 0) {
			lItem.setStatus(AppConstants.run_status_runnable);
			lErrors = runManager.updateRun(lItem);
//			Get run so date format, name etc. is correct.
			lItem = runManager.getRun(lItem.getRunId());
			if (lCopy)
				lItem = CDesktopComponents.sSpring().copyRunData(lOldItem, lItem);
			
			if (contextManager.getAllContexts().size() > 0) {
				// only set run contexts if there are contexts
				Listbox lListbox = (Listbox)CControlHelper.getInputComponent(this, "rcoRcoIds");
				for (Listitem lConItem : lListbox.getItems()) {
					IEContext lContext = (IEContext) lConItem.getValue();
					IERunContext lRunContext = getRunContext(lItem.getRunId(), lContext.getConId());
					if (lConItem.isSelected()) {
						if (lRunContext == null) {
							lRunContext = runContextManager.getNewRunContext();
							lRunContext.setRunRunId(lItem.getRunId());
							lRunContext.setConConId(lContext.getConId());
							runContextManager.newRunContext(lRunContext);
						}
					} else {
						if (lRunContext != null) {
							runContextManager.deleteRunContext(lRunContext);
						}
					}
				}
			}
			lWindow.setAttribute("item",lItem);
		}
		else {
			lWindow.setAttribute("item",null);
			CDesktopComponents.cControl().showErrors(this,lErrors);
		}
		lWindow.detach();
	}
	
	private Calendar dateToCalendar(Date lYMD, Date lHMS) {
		Calendar lCal = Calendar.getInstance();
		lCal.setTime(lYMD);
		int lY = lCal.get(Calendar.YEAR);
		int lM = lCal.get(Calendar.MONTH);
		int lD = lCal.get(Calendar.DAY_OF_MONTH);
		lCal.setTime(lHMS);
		lCal.set(Calendar.YEAR, lY);
		lCal.set(Calendar.MONTH, lM);
		lCal.set(Calendar.DAY_OF_MONTH, lD);
		return lCal;
	}
	
	/**
	 * Gets the run context.
	 * 
	 * @param aRunId the a run id
	 * @param aConId the context id
	 * 
	 * @return the run context
	 */
	public IERunContext getRunContext(int aRunId, int aConId) {
		for (IERunContext lRunContext : runContextManager.getAllRunContextsByRunId(aRunId)) {
			if (aConId == lRunContext.getConConId())
				return lRunContext;
		}
		return null;
	}
	
}
