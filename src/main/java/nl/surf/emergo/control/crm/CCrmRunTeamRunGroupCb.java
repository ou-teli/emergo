/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.crm;

import java.util.List;

import nl.surf.emergo.business.IRunTeamRunGroupManager;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefCheckbox;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunTeam;
import nl.surf.emergo.domain.IERunTeamRunGroup;

/**
 * The Class CCrmRunTeamRunGroupCb.
 */
public class CCrmRunTeamRunGroupCb extends CDefCheckbox {

	private static final long serialVersionUID = -6877474800166896647L;

	/**
	 * On check if checked add run team run group for chosen run team and run group. If not checked, delete it.
	 */
	public void onCheck() {
		IERunTeam lRunTeam = (IERunTeam)CDesktopComponents.cControl().getAccSessAttr("runteam");
		IERunGroup lRunGroup = (IERunGroup)getAttribute("rungroup");
		IRunTeamRunGroupManager runTeamRunGroupManager = (IRunTeamRunGroupManager)CDesktopComponents.sSpring().getBean("runTeamRunGroupManager");
		List<IERunTeamRunGroup> lRunTeamRunGroups = runTeamRunGroupManager.getAllRunTeamRunGroupsByRutIdRugId(lRunTeam.getRutId(), lRunGroup.getRugId());
		IERunTeamRunGroup lRunTeamRunGroup = null;
		if (lRunTeamRunGroups.size() > 0)
			// only one possible
			lRunTeamRunGroup = lRunTeamRunGroups.get(0);
		if ((lRunTeamRunGroup == null) && (isChecked())) {
			// add
			lRunTeamRunGroup = runTeamRunGroupManager.getNewRunTeamRunGroup();
			lRunTeamRunGroup.setRutRutId(lRunTeam.getRutId());
			lRunTeamRunGroup.setRugRugId(lRunGroup.getRugId());
			runTeamRunGroupManager.newRunTeamRunGroup(lRunTeamRunGroup);
		}
		if ((lRunTeamRunGroup != null) && (!isChecked())) {
			// delete
			runTeamRunGroupManager.deleteRunTeamRunGroup(lRunTeamRunGroup);
		}
	}
}
