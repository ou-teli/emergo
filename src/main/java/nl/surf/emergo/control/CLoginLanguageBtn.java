/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import java.util.Locale;

import org.zkoss.web.Attributes;

import nl.surf.emergo.control.def.CDefButton;
import nl.surf.emergo.view.VView;

/**
 * The Class CLoginLanguageBtn.
 * 
 * Used to handle setting language.
 */
public class CLoginLanguageBtn extends CDefButton {

	private static final long serialVersionUID = 5275128315999095908L;

	/** The view. */
	private VView vView = CDesktopComponents.vView();

	/**
	 * On click change language using Locale.
	 */
	public void onClick() {
		if (vView.getAttributeString(this, "_language").length() == 0 || vView.getAttributeString(this, "_country").length() == 0) {
			CDesktopComponents.cControl().setSessAttr(Attributes.PREFERRED_LOCALE, null);
			//reload page to see effect
			vView.redirectToView(VView.v_login);
		}
		else {
			Locale locale = new Locale(vView.getAttributeString(this, "_language"), vView.getAttributeString(this, "_country"));
			CDesktopComponents.cControl().setSessAttr(Attributes.PREFERRED_LOCALE, locale);
			//reload page to see effect
			vView.redirectToView(VView.v_login);
		}
	}

}
