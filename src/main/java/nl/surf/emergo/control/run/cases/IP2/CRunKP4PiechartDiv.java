/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.IP2;

import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.CRunArea;
import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.control.run.CRunEditforms;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpringHelper;

public class CRunKP4PiechartDiv extends CRunArea {

	private static final long serialVersionUID = -3872531559901377962L;

	public VView vView = CDesktopComponents.vView();
	public SSpringHelper sSpringHelper = new SSpringHelper();

	public CRunEditforms currentEmergoComponent = null;
	public int numberOfPiePieces = 6;
	public String delimiter = ",";
	public String defaultValues = "";

	public void onCreate(CreateEvent aEvent) {
		HtmlMacroComponent macro = (HtmlMacroComponent)CRunComponent.getMacroParent(this);
		Map<String,Object> propertyMap = (Map<String,Object>)macro.getDynamicProperty("a_propertyMap");
		currentEmergoComponent = (CRunEditforms)propertyMap.get("currentEmergoComponent");

		if (currentEmergoComponent == null) {
			return;
		}

		defaultValues = "";
		for (int i=0;i<numberOfPiePieces;i++) {
			if (i > 0) {
				defaultValues += delimiter;
			}
			defaultValues += "Betrokken partij " + (i + 1);
		}
		for (int i=0;i<numberOfPiePieces;i++) {
			defaultValues += delimiter;
			defaultValues += "0.0";
		}

		String values = sSpringHelper.getCurrentStatusTagStatusChildAttribute(getPluginTag(), "value", "");
		if (values.equals("")) {
			//store default values
			sSpringHelper.setRunTagStatus(currentEmergoComponent, getPluginTag(), "value", defaultValues, true);
			setScoreAndCorrect();
		}

		Events.postEvent("onUpdate", this, null);
	}
	

	/**
	 * Methods originally used in ZUL file.
	 */

	public IXMLTag getPluginTag() {
		return (IXMLTag)getParent().getAttribute("tag");
	}

	public void setScoreAndCorrect() {
		String values = sSpringHelper.getCurrentStatusTagStatusChildAttribute(getPluginTag(), "value", defaultValues);
		String[] valuesArr = values.split(delimiter);
		int numberOfPositiveValues = 0;
		double total = 0;
		for (int i=0;i<valuesArr.length;i++) {
			if (i >= numberOfPiePieces) {
				double value = Double.parseDouble(valuesArr[i]);
				if (value > 0) {
					numberOfPositiveValues++;
				}
				total += value;
			}
		}
		String score = "0";
		if (numberOfPositiveValues >= 3) {
			score = "1";
			if (total == 100) {
				score = "2";
			}
		}

		if (currentEmergoComponent == null) {
			return;
		}
		String correct = "" + score.equals("2");
		String oldscore = sSpringHelper.getCurrentStatusTagStatusChildAttribute(getPluginTag(), "score", "0");
		if (!score.equals(oldscore)) {
			sSpringHelper.setRunTagStatus(currentEmergoComponent, getPluginTag(), "score", score, true);
		}
		String oldcorrect = sSpringHelper.getCurrentStatusTagStatusChildAttribute(getPluginTag(), "correct", "false");
		if (!correct.equals(oldcorrect)) {
			sSpringHelper.setRunTagStatus(currentEmergoComponent, getPluginTag(), "correct", correct, true);
		}
	}

	public String getSavedCategory(int fieldNumber) {
		String values = sSpringHelper.getCurrentStatusTagStatusChildAttribute(getPluginTag(), "value", defaultValues);
		String[] valuesArr = values.split(delimiter);
		int index = fieldNumber - 1;
		if (index >= valuesArr.length) {
			return "";
		}
		return valuesArr[index];
	}

	public void saveCategory(int fieldNumber, String stringValue) {
		if (currentEmergoComponent == null) {
			return;
		}
		String values = sSpringHelper.getCurrentStatusTagStatusChildAttribute(getPluginTag(), "value", defaultValues);
		String[] valuesArr = values.split(delimiter);
		if (!valuesArr[fieldNumber - 1].equals("" + stringValue)) {
			//value is changed
			valuesArr[fieldNumber - 1] = stringValue;
			values = "";
			for (int i=0;i<valuesArr.length;i++) {
				if (i > 0) {
					values += ",";
				}
				values += valuesArr[i];
			}
			sSpringHelper.setRunTagStatus(currentEmergoComponent, getPluginTag(), "value", values, true);
		}
	}

	public Double getSavedValue(int fieldNumber) {
		String values = sSpringHelper.getCurrentStatusTagStatusChildAttribute(getPluginTag(), "value", defaultValues);
		String[] valuesArr = values.split(delimiter);
		double doubleValue = 0;
		int index = fieldNumber + numberOfPiePieces - 1;
		if (index >= valuesArr.length) {
			return doubleValue;
		}
		try {
			doubleValue = Double.parseDouble(valuesArr[index]);
		} catch (NumberFormatException e) {
		}
		return doubleValue; 
	}

	public void saveValue(int fieldNumber, double doubleValue) {
		if (currentEmergoComponent == null) {
			return;
		}
		String values = sSpringHelper.getCurrentStatusTagStatusChildAttribute(getPluginTag(), "value", defaultValues);
		String[] valuesArr = values.split(delimiter);
		if (!valuesArr[fieldNumber + numberOfPiePieces - 1].equals("" + doubleValue)) {
			//value is changed
			valuesArr[fieldNumber + numberOfPiePieces - 1] = "" + doubleValue;
			values = "";
			for (int i=0;i<valuesArr.length;i++) {
				if (i > 0) {
					values += ",";
				}
				values += valuesArr[i];
			}
			sSpringHelper.setRunTagStatus(currentEmergoComponent, getPluginTag(), "value", values, true);
			setScoreAndCorrect();
		}
	}

	public void onSaveCategory(Event aEvent) {
		//NOTE parent is Include that has as attribute the plugin tag
		saveCategory((int)((Object[])aEvent.getData())[0], (String)((Object[])aEvent.getData())[1]);
	}
	
	public void onSaveValue(Event aEvent) {
		//NOTE parent is Include that has as attribute the plugin tag
		saveValue((int)((Object[])aEvent.getData())[0], (double)((Object[])aEvent.getData())[1]);
	}
	
	public void onUpdate(Event aEvent) {
		if (currentEmergoComponent == null) {
			return;
		}
		List<Component> pluginItems = vView.getComponentsByPrefix("pluginItem_");
		for (Component pluginItem : pluginItems) {
			pluginItem.setAttribute("tag", getPluginTag());
			currentEmergoComponent.updateEditformItem((HtmlBasedComponent)pluginItem, "CRunEditformSectionOverwrite", (String)aEvent.getData());
		}
	}
	
}
