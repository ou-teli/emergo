/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTag;
import nl.surf.emergo.domain.IERunGroup;

public class CRunPVToolkitSkillWheelRecordingDiv extends CRunPVToolkitSkillWheelLevel1Div {

	private static final long serialVersionUID = -7859978516577670701L;

	protected String _componentToShowId = "";

	public void init(IERunGroup actor, boolean editable, int cycleNumber, int skillClusterNumber, String componentToShowId) {
		_classPrefix = "skillWheelRecording";
		_componentToShowId = componentToShowId;
		
		super.init(actor, editable, cycleNumber, skillClusterNumber);
	}
	
	@Override
	public void update(int cycleNumber) {
		_cycleNumber = cycleNumber;
		
		//NOTE put all elements in a specific div and if it exists remove it.
		//Other elements are defined in the ZUL file and may not be removed 
		Div specificDiv = pvToolkit.getSpecificDiv(this);

		pvToolkit.setMemoryCaching(true);
		
		new CRunPVToolkitDefImage(specificDiv, 
				new String[]{"class", "src"}, 
				new Object[]{_classPrefix + "Background", getBackGroundSrc()}
		);
		
		Div div = new CRunPVToolkitDefDiv(specificDiv, 
				new String[]{"class"}, 
				new Object[]{"titleRight"}
		);
		
		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "value"}, 
				new Object[]{"font titleRight", getTitle()}
				);
		
		_numberOfFeedbacks = getNumberOfCycleFeedbacks(_cycleNumber, _sharedFeedbackTagsPerFeedbackStep);

		if (!_compareWithExperts) {
			showCycle(specificDiv, _numberOfFeedbacks);
		}

		Button btn = new CRunPVToolkitDefButton(specificDiv, 
				new String[]{"class", "cLabelKey"}, 
				new Object[]{"font pvtoolkitButton " + _classPrefix + "BackButton", "PV-toolkit.back"}
		);
		addBackButtonOnClickEventListener(btn, _idPrefix + "RecordingDiv", _componentToShowId);
		
		if (_compareWithExperts) {
			if (_skillExampleTag == null) {
				return;
			}

			new CRunPVToolkitDefLabel(specificDiv, 
					new String[]{"class", "labelKey"}, 
					new Object[]{"font " + _classPrefix + "NameHeader", "PV-toolkit-skillwheel.column.label.name"}
			);
			
			new CRunPVToolkitDefLabel(specificDiv, 
					new String[]{"class", "value"}, 
					new Object[]{"font " + _classPrefix + "Name", pvToolkit.getTagChildValue(new CRunPVToolkitCacAndTag(pvToolkit.getCurrentRubricsCaseComponent(), _skillExampleTag), "name")}
			);
			
			pvToolkit.showDataVideo(_skillExampleTag, "blob", _idPrefix + "Recording", _classPrefix);
		}
		else if (_cycleNumber > 0 && _cycleNumber <= _sharedPracticeTagsPerFeedbackStep.size()) {
			IXMLTag practiceTag = _sharedPracticeTagsPerFeedbackStep.get(_cycleNumber - 1);

			IXMLTag pieceTag = pvToolkit.getStatusChildTag(practiceTag).getChild("piece");
			pvToolkit.showStatusLink(pieceTag, specificDiv, "", "font gridLink " + _classPrefix + "FileLink", pvToolkit.getRubricLabelText("PV-toolkit-file"));
			
			new CRunPVToolkitDefLabel(specificDiv, 
					new String[]{"class", "labelKey"}, 
					new Object[]{"font " + _classPrefix + "NameHeader", "PV-toolkit-skillwheel.column.label.name"}
			);
			
			new CRunPVToolkitDefLabel(specificDiv, 
					new String[]{"class", "value"}, 
					new Object[]{"font " + _classPrefix + "Name", pvToolkit.getStatusChildTagAttribute(practiceTag, "name")}
			);
			
			new CRunPVToolkitDefLabel(specificDiv, 
					new String[]{"class", "labelKey"}, 
					new Object[]{"font " + _classPrefix + "DateHeader", "PV-toolkit-skillwheel.column.label.date"}
			);
			
			new CRunPVToolkitDefLabel(specificDiv, 
					new String[]{"class", "value"}, 
					new Object[]{"font " + _classPrefix + "Date", pvToolkit.getStatusChildTagAttribute(practiceTag, "sharedate")}
			);
			
			pvToolkit.showStatusVideo(practiceTag, _idPrefix + "Recording", _classPrefix);
		}
		
		pvToolkit.setMemoryCaching(false);
	}

	protected String getBackGroundSrc() {
		String src = "";
		if (_cycleNumber == _maxCycleNumber) {
			src = zulfilepath + _classPrefix + "_wit.svg";
		}
		if (_cycleNumber < _maxCycleNumber) {
			src = zulfilepath + _classPrefix + "_geel.svg";
		}
		return src;
	}

	protected String getTitle() {
		if (!_compareWithExperts) {
			return CDesktopComponents.vView().getLabel("PV-toolkit-skillwheel.header");
		}
		else {
			return CDesktopComponents.vView().getLabel("PV-toolkit-skillwheel.header.compareWithExperts");
		}
	}

	protected void addBackButtonOnClickEventListener(Component component, String fromComponentId, String toComponentId) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				pvToolkit.clearVideo(_idPrefix + "Recording");
				Component fromComponent = CDesktopComponents.vView().getComponent(fromComponentId);
				if (fromComponent != null) {
					fromComponent.setVisible(false);
				}
				Component toComponent = CDesktopComponents.vView().getComponent(toComponentId);
				if (toComponent != null) {
					if (toComponent instanceof CRunPVToolkitSkillWheelLevel1Div) {
						if (((CRunPVToolkitSkillWheelLevel1Div)toComponent)._cycleNumber != _cycleNumber) {
							// NOTE: set componentTo._cycleNumber, because parameter in init method will be ignored for level 1 and level 2:
							((CRunPVToolkitSkillWheelLevel1Div)toComponent)._cycleNumber = _cycleNumber;
							((CRunPVToolkitSkillWheelLevel1Div)toComponent).init(_actor, _editable, _cycleNumber, _skillClusterNumberToFilter);
						}
					}
					toComponent.setVisible(true);
				}
			}
		});
	}

}
