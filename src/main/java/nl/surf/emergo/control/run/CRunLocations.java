/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.List;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zul.Label;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefLabel;

/**
 * The Class CRunLocations. Used to show a list of locations to be
 * chosen from on location by the Emergo user.
 */
public class CRunLocations extends CRunArea {

	private static final long serialVersionUID = 4836813344384874757L;

	/** The location tags. */
	protected List<IXMLTag> locationTags = null;

	/**
	 * Instantiates a new c run location actions.
	 */
	public CRunLocations() {
		super();
		setZclass(getClassName());
	}

	/**
	 * Gets the location tags.
	 *
	 * @return the location tags
	 */
	public List<IXMLTag> getLocationTags() {
		return locationTags;
	}

	/**
	 * Sets the location tags.
	 *
	 * @param aLocationTags the new location tags
	 */
	public void setLocationTags(List<IXMLTag> aLocationTags) {
		locationTags = aLocationTags;
	}

	/**
	 * On create fill location tags and show locations.
	 *
	 * @param aEvent the a event
	 */
	public void onCreate(CreateEvent aEvent) {
		setLocationTags((List<IXMLTag>) (aEvent.getArg()).get("item"));
		// create all necessary components within window
		createComponents();
	}

	/**
	 * Creates components. Shows locations as clickable labels.
	 */
	public void createComponents() {
		if (locationTags != null) {
			if (locationTags.size() > 0) {
				Label lLabel = null;
				lLabel = new CDefLabel(CDesktopComponents.vView().getLabel("run_locations.locations"));
				appendChild(lLabel);
				for (IXMLTag lLocationTag : locationTags) {
					lLabel = new CRunLocationLabel(lLocationTag);
					appendChild(lLabel);
				}
			}
		}
	}
}