/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import java.util.List;

import org.zkoss.zk.ui.Component;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;

/**
 * The Class CRunLocationBtns. Used to show location buttons within run choice area.
 * Clicking an location button, opens the corresponding location in the run view area.
 */
public class CRunLocationBtnsClassic extends CRunChoiceHoverBtnsClassic {

	private static final long serialVersionUID = -3449027695761159120L;

	/**
	 * Instantiates a new c run location btns.
	 * 
	 * @param aId the a id
	 * @param aParent the a parent
	 */
	public CRunLocationBtnsClassic(Component aParent, String aId) {
		super(aParent, aId);
	}

	/**
	 * Updates hover buttons. Shows all location buttons having the correct status.
	 */
	@Override
	public void update() {
//		buttonHbox.getChildren().clear();
		for (Component lComp : buttonHbox.getChildren()) {
			if (lComp instanceof CRunHoverBtnClassic) {
				CRunHoverBtnClassic lBtn = (CRunHoverBtnClassic)lComp;
				lBtn.detach();
			}
		}
		if (runWnd == null)
			return;
		List<IXMLTag> lLocationTags = ((CRunWndClassic)runWnd).getLocationTags();
		if ((lLocationTags == null) || (lLocationTags.size() == 0))
			return;

		int lVisibleCount = 0;
		for (IXMLTag lLocationTag : lLocationTags) {
			String lId = lLocationTag.getAttribute(AppConstants.keyId);
			String lName = CDesktopComponents.sSpring().unescapeXML(lLocationTag.getChildValue("name"));
			String lStatus = getBtnStatus(lLocationTag);
			String lBtnName = "location" + lId + "Btn";
			CRunLocationBtnClassic lBtn = newHoverBtn(lBtnName, lStatus, "showLocation", lId,
					"location", lName);
			boolean lAccessible = ((CDesktopComponents.sSpring().getCurrentTagStatus(lLocationTag,
					AppConstants.statusKeyAccessible))
					.equals(AppConstants.statusValueTrue));
			lBtn.setAttribute("accessible", "" + lAccessible);
			buttonHbox.appendChild(lBtn);
			if ((firstUpdate) && (lStatus.equals("selected"))) {
				runWnd.onTimedAction(lBtnName, "showLocation", lId);
			}
			if (!lStatus.equals("empty"))
				lVisibleCount = lVisibleCount + 1;
		}
		setItemCount(lVisibleCount);
		setParallax(true);
		firstUpdate = false;
	}

	/**
	 * Creates new hover btn.
	 *
	 * @param aId the a id
	 * @param aStatus the a status the button should get
	 * @param aAction the a action to be send when clicked
	 * @param aActionStatus the a action status of the action
	 * @param aPosition the a position, where on the screen the button appears for layout
	 * @param aImgPrefix the a img prefix of the hover images
	 * @param aLabel the a label to be shown above the button
	 *
	 * @return the c run hover btn
	 */
	protected CRunLocationBtnClassic newHoverBtn(String aId, String aStatus, String aAction,
			Object aActionStatus, String aImgPrefix, String aLabel) {
		CRunLocationBtnClassic cHoverBtn = createHoverBtn(aId, aStatus, aAction, aActionStatus, aImgPrefix, aLabel);
		cHoverBtn.registerObserver(CControl.runWnd);
		cHoverBtn.setVisible(!aStatus.equals("empty"));
		if (runWnd != null)
			cHoverBtn.setWidgetListener("onClick", runWnd.getMediaplayerStopAction());
		return cHoverBtn;
	}

	/**
	 * Creates new hover btn.
	 *
	 * @param aId the a id
	 * @param aStatus the a status the button should get
	 * @param aAction the a action to be send when clicked
	 * @param aActionStatus the a action status of the action
	 * @param aImgPrefix the a img prefix of the hover images
	 * @param aLabel the a label to be shown above the button
	 *
	 * @return the c run hover btn
	 */
	protected CRunLocationBtnClassic createHoverBtn(String aId, String aStatus, String aAction,
			Object aActionStatus, String aImgPrefix, String aLabel) {
		return new CRunLocationBtnClassic(aId, aStatus, aAction, aActionStatus, aImgPrefix, aLabel, "");
	}

	/**
	 * Gets the btn status for the button identified by aLocationTag.
	 * 
	 * @param aLocationTag the a location tag
	 * 
	 * @return the btn status
	 */
	protected String getBtnStatus(IXMLTag aLocationTag) {
		boolean lPresent = ((CDesktopComponents.sSpring().getCurrentTagStatus(aLocationTag,
				AppConstants.statusKeyPresent))
				.equals(AppConstants.statusValueTrue));
		boolean lAccessible = ((CDesktopComponents.sSpring().getCurrentTagStatus(aLocationTag,
				AppConstants.statusKeyAccessible))
				.equals(AppConstants.statusValueTrue));
		boolean lOpened = ((CDesktopComponents.sSpring().getCurrentTagStatus(aLocationTag,
				AppConstants.statusKeyOpened))
				.equals(AppConstants.statusValueTrue));
		return getBtnStatus(lPresent, lAccessible, lOpened);
	}

}
