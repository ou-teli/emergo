/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.scheduled;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.context.WebApplicationContext;

import nl.surf.emergo.business.ICaseComponentManager;
import nl.surf.emergo.business.IComponentManager;
import nl.surf.emergo.business.IRunGroupAccountManager;
import nl.surf.emergo.business.IRunGroupManager;
import nl.surf.emergo.business.IRunManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.business.impl.AppManager;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefHelper;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.util.CRunPVToolkitSendMessagesHelper;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IEComponent;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpring;

@Configuration
@EnableScheduling
@EnableAsync
public class PVToolkitSchedulers implements ApplicationListener <ContextRefreshedEvent> {

	protected ApplicationContext applicationContext;
	
	protected final static String pvProjectsCode = CRunPVToolkit.gCaseComponentNameProjects;
	
	protected SSpring sSpring;

	public static final String messageVarCycleNumber = "cycle_number";
	public static final String messageVarSenderName = "sender_name";
	public static final String messageVarReceiverName = "receiver_name";

	protected CRunPVToolkitSendMessagesHelper sendMessagesHelper;
	
	protected boolean skipFirstTime = true;

	@Async
	public void onApplicationEvent(ContextRefreshedEvent event) {
		applicationContext = event.getApplicationContext();
		AppManager.setAppContext(applicationContext);
		
		WebApplicationContext webApplicationContext = (WebApplicationContext)applicationContext;
		ServletContext sc = webApplicationContext.getServletContext();
		VView.setServletContext(sc);
		
		sSpring = new SSpring(applicationContext);
		CDesktopComponents.setSSpring(sSpring);
		
		sendMessagesHelper = new CRunPVToolkitSendMessagesHelper(sSpring, null, null);
	}

	@Scheduled(fixedDelay = 120000)
	@Async
    public void checkForReminderMessages() {
		if (skipFirstTime) {
			skipFirstTime = false;
			return;
		}

		long startTime = System.currentTimeMillis();
		
		//get components with code PV_projects
		IEComponent pvProjectsComponent = getComponent(pvProjectsCode);
		if (pvProjectsComponent == null) {
			return;
		}
		//get all PV_projects
		List<IECaseComponent> allProjectsCaseComponents = getCaseComponents(pvProjectsComponent);
		if (allProjectsCaseComponents.size() == 0) {
			return;
		}
		//get all cases that use PV_projects case components
		List<IECase> commonCases = getCommonCases(allProjectsCaseComponents);
		if (commonCases.size() == 0) {
			return;
		}
		//get all active runs for these cases. Might also be test runs
		List<IERun> runs = getActiveRuns(commonCases);
		if (runs.size() == 0) {
			return;
		}

		//set run status to be able to get run group data
		sendMessagesHelper.setRunStatus(AppConstants.runStatusRun);
		sendMessagesHelper.setRun(null);
		sendMessagesHelper.setRunGroupAccount(null);

		//loop through runs
		for (IERun run : runs) {
			//NOTE if run is defined for case developer in preview dialog
			boolean isPreviewRun = run.getStatus() == AppConstants.run_status_test;
			//set run to be able to get run group data
			sendMessagesHelper.setRun(run);
			//get active run group accounts
			List<IERunGroupAccount> activeRunGroupAccounts = getActiveRunGroupAccounts(run);
			if (activeRunGroupAccounts.size() > 0) {
				//set a run group account to be able to get run group data for methods and steps
				sendMessagesHelper.setRunGroupAccount(activeRunGroupAccounts.get(0));
				//get projects case components in run
				List<IECaseComponent> projectsCaseComponentsInRun = getProjectsCaseComponentsInRun(allProjectsCaseComponents, run);
				//loop through projects case components
				for (IECaseComponent projectsCaseComponent : projectsCaseComponentsInRun) {
					sendMessagesHelper.setProjectsCaseComponent(projectsCaseComponent);
					//loop through method tags
					for (IXMLTag methodTag : getAllMethodTags(projectsCaseComponent)) {
						sendMessagesHelper.setMethodTag(methodTag);
						//NOTE only get one step message tag per run to handle, otherwise this method will take too much time
						IXMLTag stepMessageTag = getFirstUnHandledStepMessageTag(run, methodTag);
						if (stepMessageTag != null) {
							sSpring.getSLogHelper().logDebug("Handling unhandled message for run '" + run.getName() + "'.");
							handleStepMessageTag(projectsCaseComponent, methodTag, stepMessageTag, activeRunGroupAccounts, isPreviewRun);
						}
					}
				}
			}
		}

		sendMessagesHelper.setRunStatus("");
		sendMessagesHelper.setRun(null);
		sendMessagesHelper.setRunGroupAccount(null);

		sSpring.getSLogHelper().logAction("Class nl.surf.emergo.control.run.cases.PVtoolkit.scheduled.PVToolkitSchedulers, method checkForReminderMessages() took " + (System.currentTimeMillis() - startTime) + " msecs.");
	}

    protected IEComponent getComponent(String componentCode) {
		IComponentManager componentManager = (IComponentManager)sSpring.getBean("componentManager");
		for (IEComponent component : componentManager.getAllComponents(true)) {
			if (component.getCode().equals(componentCode)) {
				return component;
			}
		}
		return null;
	}

    protected List<IECaseComponent> getCaseComponents(IEComponent component) {
		ICaseComponentManager caseComponentManager = (ICaseComponentManager)sSpring.getBean("caseComponentManager");
		return caseComponentManager.getAllCaseComponentsByComId(component.getComId());
	}

    protected List<IECase> getCommonCases(List<IECaseComponent> projectsCaseComponents) {
		List<IECase> commonCases = new ArrayList<IECase>();
		for (IECaseComponent projectsCaseComponent : projectsCaseComponents) {
			if (projectsCaseComponent.getECase().getActive() &&
					!commonCases.contains(projectsCaseComponent.getECase())) {
				commonCases.add(projectsCaseComponent.getECase());
			}
		}
		return commonCases;
	}

    protected List<IERun> getActiveRuns(List<IECase> commonCases) {
		IRunManager runManager = (IRunManager)sSpring.getBean("runManager");
		List<IERun> runs = new ArrayList<IERun>();
		for (IECase commonCase : commonCases) {
			List<IERun> caseRuns = runManager.getAllRunsByCasId(commonCase.getCasId());
			for (IERun caseRun : caseRuns) {
				if (caseRun.getActive()) {
					runs.add(caseRun);
				}
			}
		}
		return runs;
	}

    protected List<IERunGroupAccount> getActiveRunGroupAccounts(IERun run) {
		IRunGroupManager runGroupManager = (IRunGroupManager)sSpring.getBean("runGroupManager");
		IRunGroupAccountManager runGroupAccountManager = (IRunGroupAccountManager)sSpring.getBean("runGroupAccountManager");
		List<IERunGroup> runGroups = runGroupManager.getAllRunGroupsByRunId(run.getRunId());
		List<Integer> activeRugIds = new ArrayList<Integer>();
		for (IERunGroup runGroup : runGroups) {
			if (runGroup.getActive()) {
				activeRugIds.add(runGroup.getRugId());
			}
		}
		return runGroupAccountManager.getAllRunGroupAccountsByRugIds(activeRugIds);
	}

    protected List<IECaseComponent> getProjectsCaseComponentsInRun(List<IECaseComponent> allCaseComponents, IERun run) {
    	List<IECaseComponent> caseComponents = new ArrayList<IECaseComponent>();
    	for (IECaseComponent caseComponent : allCaseComponents) {
    		if (caseComponent.getECase().getCasId() == run.getECase().getCasId()) {
    			caseComponents.add(caseComponent);
    		}
		}
    	return caseComponents;
	}

    protected List<IXMLTag> getAllMethodTags(IECaseComponent projectsCaseComponent) {
    	return sSpring.getPropCScript().getRunGroupNodeTags(projectsCaseComponent, "method");
    }
    
    protected IXMLTag getMethodTag(IECaseComponent projectsCaseComponent, String methodTagId) {
    	for (IXMLTag methodTag : sSpring.getPropCScript().getRunGroupNodeTags(projectsCaseComponent, "method")) {
    		//NOTE parent of method tag should be present as well
			if (methodTag.getAttribute(AppConstants.keyId).equals(methodTagId) &&
					!methodTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse) && 
					!methodTag.getParentTag().getCurrentStatusAttribute(AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse)) {
				return methodTag;
    		}
    	}
    	return null;
    }
    
    protected List<IXMLTag> getStepTags(IXMLTag methodTag) {
    	List<IXMLTag> tags = new ArrayList<IXMLTag>();
		for (IXMLTag stepTag : methodTag.getChilds("step")) {
			if (!stepTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse)) {
    			tags.add(stepTag);
    		}
    	}
		for (IXMLTag cycleTag : methodTag.getChilds("cycle")) {
			if (!cycleTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse)) {
				for (IXMLTag stepTag : cycleTag.getChilds("step")) {
					if (!stepTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse)) {
		    			tags.add(stepTag);
		    		}
		    	}
    		}
    	}
    	return tags;
    }
    
    protected IXMLTag getFirstUnHandledStepMessageTag(IERun run, IXMLTag methodTag) {
    	//loop through method step message tags
		//NOTE step message tags are stored in run status, not in run group status
		for (IXMLTag stepMessageTag : getStepMessageTags(run, methodTag)) {
			//determine if step message tag is sent
			String sendDate = sendMessagesHelper.getStatusChildTagAttribute(stepMessageTag, AppConstants.statusKeySendDate);
			String sendTime = sendMessagesHelper.getStatusChildTagAttribute(stepMessageTag, AppConstants.statusKeySendTime);
			if (sendDate.equals("") || sendTime.equals("")) {
				//if step message tag is not sent yet
				//get reminder data and time of step message tag
				Date reminderDate = CDefHelper.getDateFromStrYMDAndHMS(
						sendMessagesHelper.getStatusChildTagAttribute(stepMessageTag, AppConstants.statusKeyReminderDate), 
						sendMessagesHelper.getStatusChildTagAttribute(stepMessageTag, AppConstants.statusKeyReminderTime));
				if (reminderDate != null && reminderDate.before(new Date())) {
					return stepMessageTag;
				}
			}
		}
		return null;
	}

    public void handleStepMessageTag(IECaseComponent projectsCaseComponent, IXMLTag methodTag, IXMLTag stepMessageTag, List<IERunGroupAccount> activeRunGroupAccounts, boolean isPreviewRun) {
		//get method tag id
    	String methodTagId = methodTag.getAttribute(AppConstants.keyId);
		//get step tag id of step message tag
		String stepTagId = sendMessagesHelper.getStatusChildTagAttribute(stepMessageTag, "steptagid");
		//get messages case component and message tag from step message tag
		IECaseComponent messagesCaseComponent = getMessagesCaseComponent(sendMessagesHelper.getStatusChildTagAttribute(stepMessageTag, "messagecacid"));
		IXMLTag messageTag = getMessageTag(messagesCaseComponent, sendMessagesHelper.getStatusChildTagAttribute(stepMessageTag, "messagetagid"));
		if (messagesCaseComponent != null && messageTag != null) {
			sSpring.getSLogHelper().logDebug("Sending message '" +  messageTag.getChildValue("title") + "'.");
			//if messages case component and message tag are found
			List<IERunGroupAccount> runGroupAccountsToSendMailTo = new ArrayList<IERunGroupAccount>();
			IXMLTag stepTagToSendMailFor = null;
			for (IERunGroupAccount runGroupAccount : activeRunGroupAccounts) {
				//set run group account to be able to get run group data
				sendMessagesHelper.setRunGroupAccount(runGroupAccount);
				sSpring.getSLogHelper().logDebug("Checking account " + runGroupAccount.getEAccount().getUserid());
				//get method tag for specific run group account
				methodTag = getMethodTag(projectsCaseComponent, methodTagId);
				if (methodTag != null) {
					sendMessagesHelper.setMethodTag(methodTag);
					//get step tag
					List<IXMLTag> stepTags = getStepTags(methodTag);
					IXMLTag stepTag = getStepTag(stepTags, stepTagId);
					if (sendMessagesHelper.mayReceiveMessage(runGroupAccount, stepTag, false)) {
						//if run group account may receive message, add run group account
						sSpring.getSLogHelper().logDebug("Account " + runGroupAccount.getEAccount().getUserid() + " may receive message.");
						runGroupAccountsToSendMailTo.add(runGroupAccount);
						if (stepTag != null) {
							stepTagToSendMailFor = stepTag;
						}
					} else {
						sSpring.getSLogHelper().logDebug("Account " + runGroupAccount.getEAccount().getUserid() + " excluded from message.");
					}
				}
			}
			if (runGroupAccountsToSendMailTo.size() > 0 && stepTagToSendMailFor != null) {
				//if run group account may receive message, send message
				sendMessagesHelper.sendMessage(runGroupAccountsToSendMailTo, messagesCaseComponent, messageTag, stepTagToSendMailFor, isPreviewRun);
				sSpring.getSLogHelper().logAction("reminder message sent for method '" + methodTag.getChildValue("pid") + "' and step '" + stepTagToSendMailFor.getChildValue("pid") +  "'.");
			}
		}

		//store senddate and sendtime for step message tag, so it won't be send again
		//get step message tag uuid to be able to update step message tag later on
		String stepMessageTagUuid = stepMessageTag.getAttribute(CRunPVToolkit.stepMessageTagName + "uuid");
		IXMLTag statusRootTag = sSpring.getXmlRunStatusTree(projectsCaseComponent, AppConstants.statusTypeRun);
		for (IXMLTag tempStepMessageTag : sSpring.getPropCScript().getAllTags(statusRootTag)) {
			if (tempStepMessageTag.getName().equals(CRunPVToolkit.stepMessageTagName)) {
				if (tempStepMessageTag.getAttribute(CRunPVToolkit.stepMessageTagName + "uuid").equals(stepMessageTagUuid)) {
					IXMLTag statusChildTag = tempStepMessageTag.getChild(AppConstants.statusElement);
					statusChildTag.setAttribute(AppConstants.statusKeySendDate, CDefHelper.getDateStrAsYMD(new Date()));
					statusChildTag.setAttribute(AppConstants.statusKeySendTime, CDefHelper.getDateStrAsHMS(new Date()));
				}
			}
		}
		sSpring.setRunCaseComponentStatusRootTag(sSpring.getRun(), projectsCaseComponent, statusRootTag);
    }

    protected List<IXMLTag> getStepMessageTags(IERun run, IXMLTag methodTag) {
    	IXMLTag runProjectsStatusChildTag = sendMessagesHelper.getRunProjectsStatusChildTag(run, methodTag);
    	if (runProjectsStatusChildTag == null) {
			sSpring.getSLogHelper().logDebug("No PV_projects method status tag in run '" + run.getName() + "'.");
    		return new ArrayList<IXMLTag>();
    	}
    	else {
    		return runProjectsStatusChildTag.getChilds(CRunPVToolkit.stepMessageTagName);
    	}
    }

	protected IECaseComponent getMessagesCaseComponent(String messageCacId) {
		if (!StringUtils.isEmpty(messageCacId)) {
			return sSpring.getCaseComponent(messageCacId);
		}
		return null;
	}
    
	protected IXMLTag getMessageTag(IECaseComponent messagesCaseComponent, String messageTagId) {
		if (messagesCaseComponent == null) {
			return null;
		}
		if (!StringUtils.isEmpty(messageTagId)) {
			return sSpring.getXmlDataPlusRunStatusTag(messagesCaseComponent, AppConstants.statusTypeRunGroup, messageTagId);
		}
		return null;
	}
    
	protected IXMLTag getStepTag(List<IXMLTag> stepTags, String stepTagId) {
		IXMLTag stepTag = null;
		for (IXMLTag tempStepTag : stepTags) {
			if (tempStepTag.getAttribute(AppConstants.keyId).equals(stepTagId)) {
				stepTag = tempStepTag;
				break;
			}
		}
		return stepTag;
	}
		
}
