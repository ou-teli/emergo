/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import nl.surf.emergo.view.VView;

/**
 * The Class CRunHoverBtnImage. Used to show the different images of a hover button.
 */
public class CRunHoverBtnImageClassic extends CRunButtonClassic {

	private static final long serialVersionUID = 2951734792283123067L;

	/** The imgprefix of the images used to show the different states. */
	protected String imgprefix = "";

	/** The position within the player. */
	protected String position = "";

	/** The client actions for mouseover, mouseout and click. */
	protected String clientActions = "";

	/** The current status of the image. */
	protected String status = "";

	/**
	 * Instantiates a new c run hover btn image.
	 * 
	 * @param aId the a id
	 * @param aPosition the a position
	 * @param aImgPrefix the a img prefix
	 * @param aEventAction the a event action to be send to observers when clicked
	 * @param aEventActionStatus the a event action status to be send to observers when clicked
	 * @param aClientAction the a client action to be executed when clicked
	 */
	public CRunHoverBtnImageClassic(String aId, String aPosition, String aImgPrefix,
			String aEventAction, Object aEventActionStatus, String aClientAction) {
		super(aId, aEventAction, aEventActionStatus, "", "", "");
		position = aPosition;
		imgprefix = aImgPrefix;
		setZclass(className + "_" + position + "_" + "active");

		String lBtnPrefix = VView.getInitParameter("emergo.style.path") + "" + imgprefix;
		String lBtnMouseover = "onmouseover:this.childNodes.item(0).src='" + lBtnPrefix + "_mouseover.gif';";
		String lBtnMouseout = "onmouseout:this.childNodes.item(0).src='" + lBtnPrefix + "_mouseout.gif';";
		clientActions = lBtnMouseover + lBtnMouseout + aClientAction;
		status = "empty";
	}

	/**
	 * Checks if is status.
	 * 
	 * @param aStatus the a status
	 * 
	 * @return true, if is status
	 */
	public boolean isStatus(String aStatus) {
		return (aStatus.equals(status));
	}

	/**
	 * Gets the status.
	 * 
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 * 
	 * @param aStatus the new status
	 */
	public void setStatus(String aStatus) {
		if ((aStatus == null) || (aStatus.equals("")))
			aStatus = "empty";
		status = aStatus;

		setDisabled(true);
		if (status.equals("active")) {
			setDisabled(false);
		}

		if (status.equals("active"))
			setZclass(className + "_" + position + "_" + "active");
		else
			setZclass(className + "_" + position + "_" + "inactive");

//		setAction(lAction);
//		setWidgetListener("onClick", lAction);

		String lImageName = position;
		if (!status.equals("empty"))
			lImageName = imgprefix;
		lImageName = lImageName + "_" + status;
		String lImage = VView.getInitParameter("emergo.style.path") + "" + lImageName + ".gif";
		setImage(lImage);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunButton#onClick()
	 */
	@Override
	public void onClick() {
		if (!status.equals("active"))
			return;
		super.onClick();
	}
}
