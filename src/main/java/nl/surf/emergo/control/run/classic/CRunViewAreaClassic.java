/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import java.util.List;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunViewArea. An area within the player to view content of different components.
 */
public class CRunViewAreaClassic extends CRunAreaClassic {

	private static final long serialVersionUID = 3298920626601280051L;

	/** The run wnd. */
	protected CRunWndClassic runWnd = (CRunWndClassic) CDesktopComponents.vView().getComponent(CControl.runWnd);

	/** The locationview, used to show location image. */
	protected CRunLocationViewClassic locationview = null;

	/** The componentview, used to show case component content. */
	protected CRunComponentViewClassic componentview = null;

	/** The actcasecomponent, the current casecomponent for which content is shown. */
	protected IECaseComponent actcasecomponent = null;

	/** The actcasecomponenttag, the current tag of the casecomponent for which content is shown. */
	protected IXMLTag actcasecomponenttag = null;

	/**
	 * Instantiates a new c run view area.
	 * 
	 * @param aId the a id
	 * @param aStatus the a status
	 */
	public CRunViewAreaClassic(String aId, String aStatus) {
		super(aId);
		setSclass(className);
		setStatus(aStatus);
	}

	/**
	 * Sets the status. Either 'location' or 'component'. Corresponding views are created.
	 * 
	 * @param aStatus the new status
	 */
	@Override
	public void setStatus(String aStatus) {
		super.setStatus(aStatus);
		if ((status.equals("location")) && (locationview == null)) {
			locationview = new CRunLocationViewClassic("runLocationView");
			appendChild(locationview);
		}
		if ((status.equals("component")) && (componentview == null)) {
			componentview = new CRunComponentViewClassic("runComponentView");
			appendChild(componentview);
		}
		if (locationview != null)
			locationview.setVisible(aStatus.equals("location"));
		if (componentview != null)
			componentview.setVisible(aStatus.equals("component"));
	}

	/**
	 * Sets status of run view area depending on action and status.
	 * Shows either a location image or the content of a case component.
	 * 
	 * @param sender the sender
	 * @param action the action
	 * @param status the status
	 */
	public void setStatus(String sender, String action, Object status) {
		actcasecomponenttag = null;
		CRunTitleAreaClassic lTitleArea = (CRunTitleAreaClassic) CDesktopComponents.vView().getComponent("runTitleArea");
		if (lTitleArea != null) {
			lTitleArea.setMediaBufferingVisible(false);
			lTitleArea.setMediaBtnsVisible(false);
		}
		if (action.equals("showLocation")) {
			setStatus("location");
			actcasecomponent = CDesktopComponents.sSpring().getCaseComponent("locations", "");
			CRunBackgroundImageClassic lImage = locationview.getRunBackgroundImage();
			lImage.setEventActionStatus(status);
			lImage.showImage((IXMLTag) status);
		}
		if ((action.equals("showEmpackAction")) || (action.equals("showLocationAction"))) {
			setStatus("component");
			if (action.equals("showLocationAction")) {
				actcasecomponent = (IECaseComponent) ((List) status).get(0);
				actcasecomponenttag = (IXMLTag) ((List) status).get(1);
			} else
				actcasecomponent = (IECaseComponent) status;
			componentview.onAction(sender, action, status);
		}
	}

	/**
	 * Sets background image of location.
	 * 
	 * @param sender the sender
	 * @param status the background image
	 */
	public void setLocationBackground(String sender, Object status) {
		locationview.setRunBackgroundImage(status);
	}

	/**
	 * Gets the current case component.
	 * 
	 * @return the act case component
	 */
	public IECaseComponent getActCaseComponent() {
		return actcasecomponent;
	}

	/**
	 * Gets the current case component tag.
	 * 
	 * @return the act case component tag
	 */
	public IXMLTag getActCaseComponentTag() {
		return actcasecomponenttag;
	}

	/**
	 * Gets the run component view.
	 * 
	 * @return the run component view
	 */
	public CRunComponentViewClassic getRunComponentView() {
		return componentview;
	}
}
