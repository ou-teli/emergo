/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.CreateEvent;

import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CIObserved;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunTasksWnd is used to show the tasks component.
 */
public class CRunTasksWndClassic extends CRunAreaClassic {

	private static final long serialVersionUID = -1377571428259410306L;

	/** The run wnd. */
	protected CRunWndClassic runWnd = (CRunWndClassic) CDesktopComponents.vView().getComponent(CControl.runWnd);

	/** The tasks case component. */
	protected IECaseComponent caseComponent = null;

	/** The act ZK component which shows the tasks. */
	protected Component actComponent = null;

	/**
	 * Gets the case component.
	 * 
	 * @return the case component
	 */
	public IECaseComponent getCaseComponent() {
		return caseComponent;
	}

	/**
	 * Sets the case component.
	 * 
	 * @param aCaseComponent the new case component
	 */
	public void setCaseComponent(IECaseComponent aCaseComponent) {
		caseComponent = aCaseComponent;
	}

	/**
	 * On create fill item and show tasks.
	 * 
	 * @param aEvent the a event
	 */
	public void onCreate(CreateEvent aEvent) {
		handleAccess();
		setCaseComponent((IECaseComponent) (aEvent.getArg()).get("item"));
//		setTitle(caseComponent.getName());
		setZclass(getClassName());
		createComponents();
	}

	/**
	 * Creates component to show tasks.
	 */
	public void createComponents() {
		CRunVboxClassic lVbox = new CRunVboxClassic();

		caseComponent = getCaseComponent();
		actComponent = new CRunTasksClassic("runTasks", caseComponent);
		if (actComponent != null)
			lVbox.appendChild(actComponent);

		appendChild(lVbox);
	}

	/**
	 * Is called by end tasks button.
	 * 
	 * @param aObserved the observed object, it should have interface CIObserved implemented
	 * @param aAction the action
	 * @param aStatus the status object, can be String or other class
	 */
	@Override
	public void observedNotify(CIObserved aObserved, String aAction, Object aStatus) {
		if (aAction.equals("endTasks")) {
			detach();
		}
	}
}