/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.feedback;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Div;
import org.zkoss.zul.Messagebox;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitInitBox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.view.VView;

public class CRunPVToolkitFeedbackReadyDiv extends CDefDiv {

	private static final long serialVersionUID = 670448418226632320L;

	public String zulfilepath = ((CRunPVToolkitInitBox)CDesktopComponents.vView().getComponent("PV-toolkit_initBox")).getZulfilepath();
		
	protected IXMLTag _currentTag;

	protected String _idPrefix = "feedback";
	protected String _classPrefix = "";
	
	public void init(IXMLTag currentTag, String peerRole) {
		
		_currentTag = currentTag;
		
		setClass("popupDiv");
		
		getChildren().clear();
		
		String backgroundSrc = "";
		String peerRoleLabelKeyPrefix = "";
		if (peerRole.equals(CRunPVToolkit.peerGroupStudentRole) || peerRole.equals(CRunPVToolkit.peerGroupPeerStudentRole)) {
			_classPrefix = "feedbackReadyStudent";
			backgroundSrc = "student-feedback-ready-background.svg";
			peerRoleLabelKeyPrefix = "PV-toolkit-feedback.button." + CRunPVToolkit.peerGroupStudentRole + ".";
		}
		else if (peerRole.equals(CRunPVToolkit.peerGroupTeacherRole) || peerRole.equals(CRunPVToolkit.peerGroupStudentAssistantRole)) {
			//TODO if consultation with other teachers is possible use teacher, but for the moment set same variables as for student
/*			_classPrefix = "feedbackReadyTeacher";
			backgroundSrc = "teacher-feedback-ready-background.svg";
			peerRoleLabelKeyPrefix = "PV-toolkit-feedback.button." + CRunPVToolkit.peerGroupTeacherRole + "."; */
			_classPrefix = "feedbackReadyStudent";
			backgroundSrc = "student-feedback-ready-background.svg";
			peerRoleLabelKeyPrefix = "PV-toolkit-feedback.button." + CRunPVToolkit.peerGroupStudentRole + ".";
		}

		new CRunPVToolkitDefImage(this, 
				new String[]{"class"}, 
				new Object[]{"popupBackground"}
		);

		Div popupDiv = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix}
		);

		new CRunPVToolkitDefImage(popupDiv, 
				new String[]{"class", "src"}, 
				new Object[]{_classPrefix + "Background", zulfilepath + backgroundSrc}
		);
		
		if (peerRole.equals(CRunPVToolkit.peerGroupTeacherRole) || peerRole.equals(CRunPVToolkit.peerGroupStudentAssistantRole)) {
			//TODO add this option
/*			Component btn = new CRunPVToolkitDefButton(popupDiv, 
					new String[]{"class", "labelKey"}, 
					new Object[]{"font pvtoolkitButton " + _classPrefix + "InConsultationButton", peerRoleLabelKeyPrefix + "inconsultation"}
					);
			addFeedbackReadyAndInConsultationAndBackToFeedbackRecordingsOnClickEventListener(btn);
			//TODO show in consultation interaction for choosing teachers
			//then a continue button is needed as well */
		}
		Component btn = new CRunPVToolkitDefButton(popupDiv, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font pvtoolkitButton " + _classPrefix + "ReadyButton", peerRoleLabelKeyPrefix + "ready"}
				);
		addFeedbackReadyAndBackToFeedbackRecordingsOnClickEventListener(btn, _currentTag);
		btn = new CRunPVToolkitDefButton(popupDiv, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font pvtoolkitButton " + _classPrefix + "SendButton", peerRoleLabelKeyPrefix + "send"}
				);
		addSendFeedbackAndBackToFeedbackRecordingsOnClickEventListener(btn, _currentTag);
		
		setVisible(true);

	}
	
	protected void addFeedbackReadyAndInConsultationAndBackToFeedbackRecordingsOnClickEventListener(Component component, IXMLTag tag) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				CRunPVToolkitFeedbackOverviewRubricDiv overviewRubricDiv = (CRunPVToolkitFeedbackOverviewRubricDiv)CDesktopComponents.vView().getComponent(_idPrefix + "OverviewRubricDiv");
				if (overviewRubricDiv != null) {
					overviewRubricDiv.setFeedbackReady();
					overviewRubricDiv.setFeedbackInConsultation(true);
				}
				//hide pop-up
				setVisible(false);
				backToFeedbackRecordings(tag);
			}
		});
	}

	protected void addFeedbackReadyAndBackToFeedbackRecordingsOnClickEventListener(Component component, IXMLTag tag) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				CRunPVToolkitFeedbackOverviewRubricDiv overviewRubricDiv = (CRunPVToolkitFeedbackOverviewRubricDiv)CDesktopComponents.vView().getComponent(_idPrefix + "OverviewRubricDiv");
				if (overviewRubricDiv != null) {
					overviewRubricDiv.setFeedbackReady();
				}
				//hide pop-up
				setVisible(false);
				backToFeedbackRecordings(tag);
			}
		});
	}

	protected void addSendFeedbackAndBackToFeedbackRecordingsOnClickEventListener(Component component, IXMLTag tag) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				VView vView = CDesktopComponents.vView();
				int choice = vView.showMessagebox(getRoot(), vView.getCLabel("PV-toolkit-feedback.send.confirmation"), vView.getCLabel("PV-toolkit-feedback.send"), Messagebox.CANCEL | Messagebox.OK, Messagebox.QUESTION);
				if (choice == Messagebox.OK) {
					CRunPVToolkitFeedbackOverviewRubricDiv overviewRubricDiv = (CRunPVToolkitFeedbackOverviewRubricDiv)CDesktopComponents.vView().getComponent(_idPrefix + "OverviewRubricDiv");
					if (overviewRubricDiv != null) {
						overviewRubricDiv.setFeedbackReady();
						overviewRubricDiv.setFeedbackShared();
					}
				}
				//hide pop-up
				setVisible(false);
				if (choice == Messagebox.OK) {
					backToFeedbackRecordings(tag);
				}
			}
		});
	}

	protected void backToFeedbackRecordings(IXMLTag tag) {
		CRunPVToolkitFeedbackOverviewRubricDiv feedbackOverview = (CRunPVToolkitFeedbackOverviewRubricDiv)CDesktopComponents.vView().getComponent(_idPrefix + "OverviewRubricDiv");
		if (feedbackOverview != null) {
			feedbackOverview.toFeedbackRecordings(_idPrefix + "OverviewRubricDiv", _idPrefix, tag);
		}
	}

}
