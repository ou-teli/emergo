/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.control.run.CRunComponentHelper;
import nl.surf.emergo.control.run.CRunConversationInteraction;
import nl.surf.emergo.control.run.CRunConversations;
import nl.surf.emergo.control.run.CRunInteractionTree;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.zkspring.STriggeredReference;

/**
 * The Class CRunConversationsOunl is used to show conversation videos and images within the run view area of the
 * Emergo player. Conversation interaction is shown within the run choice area of the player.
 */
public class CRunConversationsOunl extends CRunConversations {

	private static final long serialVersionUID = -1787330778346099415L;

	/**
	 * Instantiates a new c run conversations.
	 * Creates views for image and for video.
	 * Gets current conversation, sets selected and opened of it to true and starts it.
	 */
	public CRunConversationsOunl() {
		super();
	}

	/**
	 * Instantiates a new c run conversations.
	 * Creates views for image and for video.
	 * Gets current conversation, sets selected and opened of it to true and starts it.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the a case component
	 */
	public CRunConversationsOunl(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
	}

	/**
	 * Instantiates a new c run conversations.
	 *
	 * @param aId the a id
	 * @param aRunConversationInteractionId the a run conversation interaction id
	 * @param aNotifyComponent the a notify component
	 */
	public CRunConversationsOunl(String aId, String aRunConversationInteractionId, CRunComponent aNotifyComponent) {
		super(aId, aRunConversationInteractionId, aNotifyComponent);
	}

	/**
	 * Instantiates a new c run conversations.
	 * Creates views for image and for video.
	 * Gets current conversation, sets selected and opened of it to true and starts it.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the a case component
	 * @param aConversationTag the a conversation tag
	 * @param aRunConversationInteraction the a run conversation interaction
	 */
	public CRunConversationsOunl(String aId, IECaseComponent aCaseComponent, IXMLTag aConversationTag, CRunConversationInteraction aRunConversationInteraction) {
		super(aId, aCaseComponent, aConversationTag, aRunConversationInteraction);
	}

	/**
	 * Creates views for image and for video.
	 * Gets current conversation, sets selected and opened of it to true and starts it.
	 */
	public void init() {
		super.init();
		if (currentstartfragmenttag != null) {
			showInteraction(currentconversationtag);
		}
		if (showIntervention) {
			CRunConversationInteraction lInteraction = getRunConversationInteraction();
			if (lInteraction != null) {
				lInteraction.setStatus("intervention");
				lInteraction.setVisible(true);
			}
		}
	}

	/**
	 * Gets the run component helper.
	 *
	 * @return the run component helper
	 */
	@Override
	public CRunComponentHelper getRunComponentHelper() {
		return new CRunConversationsHelperOunl(null, "question,map,alternative,field,button", caseComponent, this);
	}

	@Override
	public CRunConversationInteraction getRunConversationInteraction() {
		if (runConversationInteraction == null) {
			return (CRunConversationInteraction) CDesktopComponents.vView().getComponent(getConversationInteractionId());
		}
		else {
			return runConversationInteraction;
		}
	}

	/**
	 * Shows conversation on top of tablet or not. Sets style of interaction area.
	 *
	 * @param aConversation the a conversation
	 */
	@Override
	protected void initStyle(IXMLTag aConversation) {
		super.initStyle(aConversation);
		boolean lShowOnTop = aConversation != null && aConversation.getDefChildValue("showontop").equals(AppConstants.statusValueTrue);
		if (lShowOnTop) {
			setZclass(getClassName() + "OnTop");
		}
		else {
			setZclass(getClassName());
		}
		CRunConversationInteraction lInteraction = getRunConversationInteraction();
		if (lInteraction == null) {
			return;
		}

		boolean lInteractionTransparent = aConversation != null && aConversation.getDefChildValue("interactiontransparent").equals(AppConstants.statusValueTrue);
		if (lShowOnTop) {
			lInteraction.setZclass(lInteraction.getClassName() + "OnTop");
		}
		else {
			lInteraction.setZclass(lInteraction.getClassName());
		}
		String lInteractionPositionLeft = getTagPixelChildValue(aConversation, "interactionposition", 0);
		String lInteractionPositionTop = getTagPixelChildValue(aConversation, "interactionposition", 1);
		// NOTE Set position of interaction
		if (!lInteractionPositionLeft.equals("")) {
			lInteraction.setLeft(lInteractionPositionLeft);
		}
		if (!lInteractionPositionTop.equals("")) {
			lInteraction.setTop(lInteractionPositionTop);
		}
		if (lInteractionTransparent) {
			lInteraction.setStyle("background-image:none;");
		}
	}

	/**
	 * Shows interaction for conversation within runchoicearea and shows current background image of conversation, can be non existing.
	 *
	 * @param aConversation the a conversation
	 */
	@Override
	public void showInteraction(IXMLTag aConversation) {
		CRunConversationInteraction lInteraction = getRunConversationInteraction();
		if (lInteraction == null)
			return;
		lInteraction.setStatus(CDesktopComponents.sSpring().unescapeXML(currentconversationtag.getChildValue("name")));
		// show interaction
		CRunInteractionTree lTree = lInteraction.getRunInteractionTree();
		lTree.showInteraction(caseComponent, aConversation);

		thereIsInteractionToShow = isThereInteractionToShow(aConversation);
		lInteraction.setVisible(thereIsInteractionToShow && !isShowInteractionAfterIntroduction() && !lInteraction.isInteractionHidden());
		lInteraction.setIsInteractionShown(thereIsInteractionToShow);

		showCurrentBackgroundImage(aConversation);
	}

	/**
	 * Handles status change due to firing of script actions.
	 *
	 * @param aTriggeredReference the a triggered reference
	 */
	@Override
	public void handleStatusChange(STriggeredReference aTriggeredReference) {
		if (aTriggeredReference.getDataTag() == null) {
			return;
		}
		CRunWndOunl lRunWnd = (CRunWndOunl)runWnd;
		if (lRunWnd == null) {
			return;
		}
		if (caseComponent.getCacId() != aTriggeredReference.getCaseComponent().getCacId()) {
			// only handle status change on own case component
			return;
		}
		update(aTriggeredReference);
	}

}
