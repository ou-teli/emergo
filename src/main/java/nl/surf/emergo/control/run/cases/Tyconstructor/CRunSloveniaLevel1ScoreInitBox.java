/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.Tyconstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.domain.IECaseComponent;

public class CRunSloveniaLevel1ScoreInitBox extends CRunScoreInitBox {

	private static final long serialVersionUID = 398720250280002597L;

	public void onCreate() {
		//NOTE use echoEvent, otherwise macro parent does not yet exist, if code is used within zscript
	  	Events.echoEvent("onInit", this, null);
	}
	
	public void onInit() {
		caseComponentName = "M_B1_conversations";

		Map<String,Object> propertyMap = new HashMap<String,Object>();
		propertyMap.put("casecomponent", ((HtmlMacroComponent)CRunComponent.getMacroParent(this)).getDynamicProperty("a_casecomponent"));
		propertyMap.put("tag", ((HtmlMacroComponent)CRunComponent.getMacroParent(this)).getDynamicProperty("a_tag"));

		IECaseComponent caseComponent = CDesktopComponents.sSpring().getCaseComponent(CDesktopComponents.sSpring().getCase(), "", caseComponentName);
		propertyMap.put("answersdata", getAnswersData(caseComponent, "button", new String[]{
				"construction_assistant_constructionsite_2_(.?)",
				"construction_assistant_constructionsite_2_1_(.?)",
				"mentor_instructions_2_(.?)"
			}));
		
		//NOTE to support multiple languages labels are stored as invisible pieces within the navigation component. Pieces are misused.
		String headerleft = "";
		String headerright = "";
		caseComponent = CDesktopComponents.sSpring().getCaseComponent(CDesktopComponents.sSpring().getCase(), "", "game_navigation");
		if (caseComponent != null) {
			List<IXMLTag> nodeTags = CDesktopComponents.cScript().getRunGroupNodeTags(caseComponent, "piece");
			for (IXMLTag nodeTag : nodeTags) {
				String nodeTagPid = nodeTag.getChildValue("pid");
				if (nodeTagPid.equals("B1_score_header_left")) {
					headerleft = CDesktopComponents.sSpring().unescapeXML(nodeTag.getChildValue("name"));
				}
				if (nodeTagPid.equals("B1_score_header_right")) {
					headerright = CDesktopComponents.sSpring().unescapeXML(nodeTag.getChildValue("name"));
				}
			}
		}
		propertyMap.put("headerleft", headerleft);
		propertyMap.put("headerright", headerright);

		String zulfilepath = (String)((HtmlMacroComponent)CRunComponent.getMacroParent(this)).getDynamicProperty("a_zulfilepath");
		propertyMap.put("zulfilepath", zulfilepath);
	
		HtmlMacroComponent macro = new HtmlMacroComponent();
		appendChild(macro);
		macro.setDynamicProperty("a_propertyMap", propertyMap);
		macro.setMacroURI(zulfilepath + "run_slovenia_level1_score_view_macro.zul");
	}

}
