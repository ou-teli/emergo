/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Vbox;
import org.zkoss.zul.Window;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CCaseHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefMacro;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SStatustagEnrichment;

/**
 * The Class CRunTutorial is used to show tutorial component within the run view area of the emergo player.
 */
public class CRunTutorial extends CRunComponent {

	private static final long serialVersionUID = 6219813484769347825L;

	/** The chapter tags. */
	protected List<IXMLTag> chaptertags = new ArrayList<IXMLTag>();

	/** The initial chapter tag id. */
	protected String initialchaptertagid = "";

	/** The current chapter tag. */
	protected IXMLTag currentchaptertag = null;

	/** The previous chapter tag. */
	protected IXMLTag previouschaptertag = null;

	/** The next chapter tag. */
	protected IXMLTag nextchaptertag = null;

	/** The current page tag. */
	protected IXMLTag currentpagetag = null;

	/** The previous page tag. */
	protected IXMLTag previouspagetag = null;

	/** The next page tag. */
	protected IXMLTag nextpagetag = null;

	/** The next page tag. */
	protected boolean islastchapterpage = false;

	/** The current page tags. */
	protected Hashtable<String,IXMLTag> currentpagetags = null;

	/** The current instruction tag. */
	protected IXMLTag currentinstructiontag = null;

	/** The current report tag. */
	protected IXMLTag currentreporttag = null;

	/** Can user visit previously visited pages. */
	protected boolean previouspagesaccessible = true;

	/** Can user visit following pages. */
	protected boolean nextpagesaccessible = false;

	/** The layout number. */
	public int layoutnumber = 1;
	
	public static final String tabId = "tutorialtab";
	public static final String tablabelId = "tutorialtablabel";
	public static final String tabpanelId = "tutorialtabpanel";
	public static final String listboxId = "tutoriallistbox";
	public static final String listitembackgroundId = "tutoriallistitembackground";
	public static final String listitemId = "tutoriallistitem";
	public static final String labelTitleId = "tutoriallabeltitle";
	public static final String vboxContentId = "tutorialvboxcontent";
	public static final String buttonReadyId = "buttonready";

	public String currentChapterId = "";
	public String currentPageId = "";

	/**
	 * Instantiates a new c run tutorial.
	 */
	public CRunTutorial() {
		super("runTutorial", null);
		init();
	}

	/**
	 * Instantiates a new c run tutorial.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the tutorial case component
	 * @param aInitialChapterTagId the initial chapter tag id
	 */
	public CRunTutorial(String aId, IECaseComponent aCaseComponent, String aInitialChapterTagId) {
		super(aId, aCaseComponent);
		initialchaptertagid = aInitialChapterTagId;
		init();
	}

	public IXMLTag getCurrentChapterTag() {
		return currentchaptertag;
	}

	public IXMLTag getCurrentPageTag() {
		return currentpagetag;
	}

	public Hashtable<String,IXMLTag> getCurrentPageTags() {
		return currentpagetags;
	}

	public IXMLTag getCurrentInstructionTag() {
		return currentinstructiontag;
	}

	public IXMLTag getCurrentReportTag() {
		return currentreporttag;
	}

	/**
	 * Creates title area, content area to show tutorial tab structure,
	 * and buttons area with close button.
	 */
	@Override
	protected void createComponents() {
		CRunVbox lVbox = new CRunVbox();

		createTitleArea(lVbox);
		createContentArea(lVbox);
		createButtonsArea(lVbox);

		appendChild(lVbox);
	}

	/**
	 * Creates new content component, the tutorial tree.
	 *
	 * @return the component
	 */
	@Override
	protected Component newContentComponent() {
		HtmlMacroComponent tutorialView = new CDefMacro();
		tutorialView.setId("runTutorialView");
		tutorialView.setVisible(false);
		return tutorialView;
	}

	/**
	 * Creates buttons area and adds close button.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run hbox
	 */
	@Override
	protected CRunHbox createButtonsArea(Component aParent) {
		CRunHbox lButtonsHbox = super.createButtonsArea(aParent);
		createCloseButton(lButtonsHbox);
		return lButtonsHbox;
	}

	/**
	 * Creates view for tutorial.
	 * Gets current chapter, sets selected and opened of it to true and opens it.
	 */
	public void init() {
		chaptertags = getChapters();
		currentpagetags = getCurrentPages();
		
		if (runWnd != null) {
			previouspagesaccessible = !runWnd.getCurrentCaseComponentStatusValue(caseComponent, AppConstants.statusKeyPreviousitemsaccessible).equals(AppConstants.statusValueFalse);
			nextpagesaccessible = runWnd.getCurrentCaseComponentStatusValue(caseComponent, AppConstants.statusKeyNextitemsaccessible).equals(AppConstants.statusValueTrue);
			try {
				layoutnumber = Integer.parseInt(runWnd.getCurrentCaseComponentStatusValue(caseComponent, AppConstants.statusKeyLayoutnumber));
			} catch (NumberFormatException e) {
				layoutnumber = 1;
			}
			if (layoutnumber < 1) {
				layoutnumber = 1;
			}
			else if (layoutnumber > 7) {
				layoutnumber = 7;
			}
		}

		// save tutorial first time started
		boolean lStarted = CDesktopComponents.sSpring().getCurrentRunComponentStatus(caseComponent,
				AppConstants.statusKeyStarted, AppConstants.statusTypeRunGroup)
				.equals(AppConstants.statusValueTrue);
		if (!lStarted) {
			setRunComponentStatus(caseComponent, AppConstants.statusKeyStarted, AppConstants.statusValueTrue, true);
		}

		setChapterTag(getCurrentChapter());
	}

	/**
	 * Gets the run component helper.
	 *
	 * @return the run component helper
	 */
	@Override
	public CRunComponentHelper getRunComponentHelper() {
		return new CRunComponentHelper(null, "chapter", caseComponent, this);
	}

	/**
	 * Gets all chapters.
	 */
	public List<IXMLTag> getChapters() {
		CRunComponentHelper cComponent = (CRunComponentHelper)getRunComponentHelper();
		IXMLTag lXmlTree = cComponent.getXmlDataPlusStatusTree();
		if (lXmlTree != null) {
			return lXmlTree.getChild(AppConstants.contentElement).getChildTags(AppConstants.defValueNode);
		}
		else {
			return new ArrayList<IXMLTag>();
		}
	}

	/**
	 * Gets the current chapter tag.
	 *
	 * @return the chapter tag
	 */
	public IXMLTag getCurrentChapter() {
		if (chaptertags == null || chaptertags.size() == 0) {
			return null;
		}
		// get initially set chapter tag id
		for (IXMLTag lChapterTag : chaptertags) {
			if (lChapterTag.getAttribute(AppConstants.keyId).equals(initialchaptertagid)) {
				return lChapterTag;
			}
		}
		// get last opened chapter
		for (IXMLTag lChapterTag : chaptertags) {
			boolean lOpened = CDesktopComponents.sSpring().getCurrentTagStatus(lChapterTag,
					AppConstants.statusKeyOpened)
					.equals(AppConstants.statusValueTrue);
			if (lOpened) {
				return lChapterTag;
			}
		}
		// return first chapter
		return chaptertags.get(0);
	}

	/**
	 * Gets the current pages.
	 *
	 * @return the current pages
	 */
	public Hashtable<String,IXMLTag> getCurrentPages() {
		if (chaptertags == null || chaptertags.size() == 0) {
			return null;
		}
		Hashtable<String,IXMLTag> lCurrentPageTags = new Hashtable<String,IXMLTag>();
		for (IXMLTag lChapterTag : chaptertags) {
			List<IXMLTag> lPageTags = lChapterTag.getChildTags(AppConstants.defValueNode);
			if (lPageTags.size() > 0) {
				IXMLTag lCurrentPageTag = null;
				for (IXMLTag lPageTag : lPageTags) {
					boolean lOpened = CDesktopComponents.sSpring().getCurrentTagStatus(lPageTag,
							AppConstants.statusKeyOpened)
							.equals(AppConstants.statusValueTrue);
					if (lOpened) {
						lCurrentPageTag = lPageTag;
					}
				}
				if (lCurrentPageTag == null) {
					lCurrentPageTag = lPageTags.get(0);
				}
				lCurrentPageTags.put(lChapterTag.getAttribute(AppConstants.keyId), lCurrentPageTag);
			}
		}
		return lCurrentPageTags;
	}

	/**
	 * Opens chapter.
	 *
	 * @param aChapter the a chapter
	 */
	public void openChapter(IXMLTag aChapterTag) {
		((HtmlMacroComponent)getRunContentComponent()).setMacroURI(VView.v_run_empty_fr);
		getRunContentComponent().setVisible(false);
		if (aChapterTag == null) {
			return;
		}
		if (chaptertags == null) {
			return;
		}

		// Get chapter data
		boolean lIsBeforeCurrentChapter = true;
		boolean lIsCurrentChapter = false;
		List<String> lChapterIds = new ArrayList<String>();
		List<String> lChapterLabels = new ArrayList<String>();
		List<String> lChapterDisableds = new ArrayList<String>();
		List<String> lChapterFinisheds = new ArrayList<String>();
		for (IXMLTag lChapterTag : chaptertags) {
			lIsCurrentChapter = lChapterTag == aChapterTag;
			if (lIsCurrentChapter) {
				lIsBeforeCurrentChapter = false;
			}
			boolean lHidden = ((CDesktopComponents.sSpring().getCurrentTagStatus(lChapterTag,
					AppConstants.statusKeyPresent))
					.equals(AppConstants.statusValueFalse));
			if (!lHidden) {
				lChapterIds.add(lChapterTag.getAttribute(AppConstants.keyId));
				String lChapterLabel = CDesktopComponents.sSpring().unescapeXML(lChapterTag.getChildValue("title"));
				lChapterLabels.add(lChapterLabel);
				boolean lDisabled = ((CDesktopComponents.sSpring().getCurrentTagStatus(lChapterTag,
						AppConstants.statusKeyAccessible))
						.equals(AppConstants.statusValueFalse));
				if (!lIsCurrentChapter && !lDisabled) {
					if (lIsBeforeCurrentChapter && !previouspagesaccessible) {
						lDisabled = true;
					}
					if (!lIsBeforeCurrentChapter && !nextpagesaccessible) {
						lDisabled = true;
					}
				}
				lChapterDisableds.add("" + lDisabled);
				boolean lFinished = ((CDesktopComponents.sSpring().getCurrentTagStatus(lChapterTag,
						AppConstants.statusKeyFinished))
						.equals(AppConstants.statusValueTrue));
				lChapterFinisheds.add("" + lFinished);

/*				List<String> lTempPageIds = new ArrayList<String>();
				lPageIds.add(lTempPageIds);
				List<String> lTempPageLabels = new ArrayList<String>();
				lPageLabels.add(lTempPageLabels);
				List<String> lTempPageDisableds = new ArrayList<String>();
				lPageDisableds.add(lTempPageDisableds);
				List<String> lTempPageFinisheds = new ArrayList<String>();
				lPageFinisheds.add(lTempPageFinisheds);
				List<IXMLTag> lPageTags = lChapterTag.getChilds("page");
				for (IXMLTag lPageTag : lPageTags) {
					lHidden = ((CDesktopComponents.sSpring().getCurrentTagStatus(lPageTag,
							AppConstants.statusKeyPresent))
							.equals(AppConstants.statusValueFalse));
					if (!lHidden) {
						lTempPageIds.add(lPageTag.getAttribute(AppConstants.keyId));
						String lPageLabel = CDesktopComponents.sSpring().unescapeXML(lPageTag.getChildValue("shorttitle"));
						lTempPageLabels.add(lPageLabel);
						lDisabled = ((CDesktopComponents.sSpring().getCurrentTagStatus(lPageTag,
								AppConstants.statusKeyAccessible))
								.equals(AppConstants.statusValueFalse));
						lTempPageDisableds.add("" + lDisabled);
						lFinished = ((CDesktopComponents.sSpring().getCurrentTagStatus(lPageTag,
								AppConstants.statusKeyFinished))
								.equals(AppConstants.statusValueTrue));
						lTempPageFinisheds.add("" + lFinished);
					}
				} */
			}
		}

		// Get current chapter, current chapter pages and current page
		currentChapterId = aChapterTag.getAttribute(AppConstants.keyId);
		List<String> lCurrentPageIds = new ArrayList<String>();
		currentPageId = "";
		for (String lChapterId : lChapterIds) {
			String lPageId = "";
			if (currentpagetags.containsKey(lChapterId)) {
				lPageId = currentpagetags.get(lChapterId).getAttribute(AppConstants.keyId);
			}
			lCurrentPageIds.add(lPageId);
			if (lChapterId.equals(currentChapterId)) {
				currentPageId = lPageId;
			}
		}

		// Get page data. Current page id is needed.
		boolean lIsBeforeCurrentPage = true; 
		boolean lIsCurrentPage = false;
		List<List<String>> lPageIds = new ArrayList<List<String>>();
		List<List<String>> lPageLabels = new ArrayList<List<String>>();
		List<List<String>> lPageDisableds = new ArrayList<List<String>>();
		List<List<String>> lPageFinisheds = new ArrayList<List<String>>();
		for (IXMLTag lChapterTag : chaptertags) {
			boolean lHidden = ((CDesktopComponents.sSpring().getCurrentTagStatus(lChapterTag,
					AppConstants.statusKeyPresent))
					.equals(AppConstants.statusValueFalse));
			if (!lHidden) {
				List<String> lTempPageIds = new ArrayList<String>();
				lPageIds.add(lTempPageIds);
				List<String> lTempPageLabels = new ArrayList<String>();
				lPageLabels.add(lTempPageLabels);
				List<String> lTempPageDisableds = new ArrayList<String>();
				lPageDisableds.add(lTempPageDisableds);
				List<String> lTempPageFinisheds = new ArrayList<String>();
				lPageFinisheds.add(lTempPageFinisheds);
				List<IXMLTag> lPageTags = lChapterTag.getChilds("page");
				for (IXMLTag lPageTag : lPageTags) {
					lIsCurrentPage = lPageTag.getAttribute(AppConstants.keyId).equals(currentPageId);
					if (lIsCurrentPage) {
						lIsBeforeCurrentPage = false;
					}
					lHidden = ((CDesktopComponents.sSpring().getCurrentTagStatus(lPageTag,
							AppConstants.statusKeyPresent))
							.equals(AppConstants.statusValueFalse));
					if (!lHidden) {
						lTempPageIds.add(lPageTag.getAttribute(AppConstants.keyId));
						String lPageLabel = CDesktopComponents.sSpring().unescapeXML(lPageTag.getChildValue("shorttitle"));
						lTempPageLabels.add(lPageLabel);
						boolean lDisabled = ((CDesktopComponents.sSpring().getCurrentTagStatus(lPageTag,
								AppConstants.statusKeyAccessible))
								.equals(AppConstants.statusValueFalse));
						if (!lIsCurrentPage && !lDisabled) {
							if (lIsBeforeCurrentPage && !previouspagesaccessible) {
								lDisabled = true;
							}
							if (!lIsBeforeCurrentPage && !nextpagesaccessible) {
								lDisabled = true;
							}
						}
						lTempPageDisableds.add("" + lDisabled);
						boolean lFinished = ((CDesktopComponents.sSpring().getCurrentTagStatus(lPageTag,
								AppConstants.statusKeyFinished))
								.equals(AppConstants.statusValueTrue));
						lTempPageFinisheds.add("" + lFinished);
					}
				}
			}
		}

		List<Object> lTabData = new ArrayList<Object>();
		for (int i=0;i<lChapterIds.size();i++) {
			List<Object> data = new ArrayList<Object>();
			data.add(lChapterIds.get(i));
			data.add(lChapterLabels.get(i));
			data.add(lChapterDisableds.get(i));
			data.add(lChapterFinisheds.get(i));
			lTabData.add(data);
		}

		List<Object> lTabpanelData = new ArrayList<Object>();
		for (int i=0;i<lChapterIds.size();i++) {
			List<Object> data = new ArrayList<Object>();
			data.add(lChapterIds.get(i));
			List<Object> data2 = new ArrayList<Object>();
			for (int j=0;j<lPageIds.get(i).size();j++) {
				List<Object> data3 = new ArrayList<Object>();
				data3.add(lPageIds.get(i).get(j));
				data3.add(lPageLabels.get(i).get(j));
				data3.add(lPageDisableds.get(i).get(j));
				data3.add(lPageFinisheds.get(i).get(j));
				data2.add(data3);
			}
			data.add(data2);
			lTabpanelData.add(data);
		}

		Map<String,Object> propertyMap = new HashMap<String,Object>();
		propertyMap.put("currentEmergoComponent", this);
		propertyMap.put("tutorial_tabData", lTabData);
		propertyMap.put("tutorial_tabpanelData", lTabpanelData);
		((HtmlMacroComponent)getRunContentComponent()).setDynamicProperty("a_propertyMap", propertyMap);
		((HtmlMacroComponent)getRunContentComponent()).setMacroURI(VView.v_run_tutorial_fr);
		getRunContentComponent().setVisible(true);
	}

	/**
	 * Handle chapter.
	 *
	 * @param aChapterTagId the chapter tag id
	 */
	public void handleChapter(String aChapterTagId) {
		IXMLTag lChapterTag = getChapterTag(aChapterTagId);
		if (lChapterTag == null) {
			return;
		}
		setChapterTag(lChapterTag);
	}

	/**
	 * Sets current chapter tag and saves opened state.
	 *
	 * @param aChapterTag the a chapter tag
	 */
	public void setChapterTag(IXMLTag aChapterTag) {
		if (aChapterTag != null) {
			if (currentchaptertag == null || !aChapterTag.getAttribute(AppConstants.keyId).equals(currentchaptertag.getAttribute(AppConstants.keyId))) {
//				maybe add something for shared component
				previouschaptertag = currentchaptertag;
				currentchaptertag = aChapterTag;
				nextchaptertag = getNextChapterTag(aChapterTag);

				// save chapter first time started
				boolean lStarted = CDesktopComponents.sSpring().getCurrentTagStatus(currentchaptertag,
						AppConstants.statusKeyStarted)
						.equals(AppConstants.statusValueTrue);
				if (!lStarted) {
					setRunTagStatus(caseComponent, currentchaptertag, AppConstants.statusKeyStarted, AppConstants.statusValueTrue, true);
				}
				
				setRunTagStatus(caseComponent, currentchaptertag, AppConstants.statusKeySelected, AppConstants.statusValueTrue, true);
				setRunTagStatus(caseComponent, currentchaptertag, AppConstants.statusKeyOpened, AppConstants.statusValueTrue, true);
				openChapter(currentchaptertag);
			}
		}
	}

	/**
	 * Returns next chapter tag.
	 *
	 * @param aChapterTag the a chapter tag
	 *
	 * @return next chapter tag
	 */
	protected IXMLTag getNextChapterTag(IXMLTag aChapterTag) {
		if (aChapterTag == null) {
			return null;
		}
		IXMLTag lCurrentChapterTag = null;
		for (IXMLTag lChapterTag : chaptertags) {
			if (lCurrentChapterTag != null) {
				boolean lHidden = ((CDesktopComponents.sSpring().getCurrentTagStatus(lChapterTag,
						AppConstants.statusKeyPresent))
						.equals(AppConstants.statusValueFalse));
				if (!lHidden) {
					boolean lDisabled = ((CDesktopComponents.sSpring().getCurrentTagStatus(lChapterTag,
							AppConstants.statusKeyAccessible))
							.equals(AppConstants.statusValueFalse));
					if (!lDisabled) {
						return lChapterTag;
					}
				}
			}
			if (lChapterTag == aChapterTag) {
				lCurrentChapterTag = lChapterTag;
			}
		}
		return null;
	}

	/**
	 * Sets current chapter tag and saves opened state.
	 *
	 * @param aChapterTagId the a chapter tag id
	 */
	public void setChapter(String aChapterTagId) {
		IXMLTag lCurrentChapterTag = getChapterTag(aChapterTagId);
		setChapterTag(lCurrentChapterTag);
	}

	/**
	 * Gets current chapter tag.
	 *
	 * @param aChapterTagId the a chapter tag id
	 */
	public IXMLTag getChapterTag(String aChapterTagId) {
		if (aChapterTagId == null) {
			return null;
		}
		if (chaptertags.size() == 0) {
			return null;
		}
		IXMLTag lCurrentChapterTag = null;
		for (IXMLTag lChapterTag : chaptertags) {
			if (lChapterTag.getAttribute(AppConstants.keyId).equals(aChapterTagId)) {
				lCurrentChapterTag = lChapterTag;
			}
		}
		return lCurrentChapterTag;
	}

	/**
	 * open page.
	 *
	 * @param aPageTag the page tag
	 */
	public void openPage(IXMLTag aPageTag) {
		currentinstructiontag = null;
		currentreporttag = null;
		Label lLabel = (Label)CDesktopComponents.vView().getComponent(labelTitleId);
		if (lLabel == null) {
			return;
		}
		lLabel.setValue(CDesktopComponents.sSpring().unescapeXML(aPageTag.getChildValue("title")));
		Vbox lVbox = (Vbox)CDesktopComponents.vView().getComponent(vboxContentId);
		if (lVbox == null) {
			return;
		}
		lVbox.getChildren().clear();
		List<IXMLTag> lChildTags = aPageTag.getChildTags(AppConstants.defValueNode);
		if (lChildTags.size() == 0) {
			//NOTE for refconversation and refreferences only one child is possible
			//For report every version is saved as a childtag so there can be more than one childtag.
			return;
		}
		IXMLTag lChildTag = lChildTags.get(0);

		IECaseComponent lCaseComponent = null;
		IXMLTag lXmlTag = null;
		if (lChildTag.getName().equals("refconversation") ||
				lChildTag.getName().equals("refreferences") ||
				lChildTag.getName().equals("report")) {
			List<Object> result = getCaseComponentAndTag(lChildTag.getChild("ref"));
			if (result != null && result.size() > 0) {
				lCaseComponent = (IECaseComponent)result.get(0);
			}
			if (result != null && result.size() > 1) {
				lXmlTag = (IXMLTag)result.get(1);
			}
		}
		boolean lFinished = CDesktopComponents.sSpring().getCurrentTagStatus(currentpagetag,
				AppConstants.statusKeyFinished)
				.equals(AppConstants.statusValueTrue);
		if (lChildTag.getName().equals("instruction")) {
			currentinstructiontag = lChildTag;

			HtmlMacroComponent lMacro = new CDefMacro();
			lVbox.appendChild(lMacro);
			lMacro.setDynamicProperty("a_tutorial_instruction", getInstruction(lChildTag));
			lMacro.setMacroURI(VView.v_run_tutorialinstruction_fr);
		}
		else if (lChildTag.getName().equals("refconversation")) {
			if (lCaseComponent == null) {
				return;
			}
			CDesktopComponents.sSpring().setRunComponentStatus(lCaseComponent,
					AppConstants.statusKeySelected, AppConstants.statusValueTrue,
					true, getRunStatusType(), true);
			CRunConversationInteraction lRunConversationInteraction = getNewRunConversationInteraction();
			CRunComponent lRunComponent = getNewRunConversations(lCaseComponent, lXmlTag, lRunConversationInteraction);
			lVbox.appendChild(lRunComponent);
			//NOTE Conversation interaction must be added as child after conversation! Otherwise no conversation action is shown.
			lVbox.appendChild(lRunConversationInteraction);
			CDesktopComponents.sSpring().setRunComponentStatus(lCaseComponent,
					AppConstants.statusKeyOpened, AppConstants.statusValueTrue,
					true, getRunStatusType(), true);
		}
		else if (lChildTag.getName().equals("refreferences")) {
			if (lCaseComponent == null) {
				return;
			}
			CDesktopComponents.sSpring().setRunComponentStatus(lCaseComponent,
					AppConstants.statusKeySelected, AppConstants.statusValueTrue,
					true, getRunStatusType(), true);
			CRunComponent lRunComponent = getNewRunReferences(lCaseComponent);
			lVbox.appendChild(lRunComponent);
			CDesktopComponents.sSpring().setRunComponentStatus(lCaseComponent,
					AppConstants.statusKeyOpened, AppConstants.statusValueTrue,
					true, getRunStatusType(), true);
		}
		else if (lChildTag.getName().equals("report")) {
			currentreporttag = lChildTag;

			//NOTE get report tag again, because it isn't updated within the currently used data tag tree of this component
			lChildTag = CDesktopComponents.sSpring().getXmlDataPlusRunStatusTag(caseComponent, getRunStatusType(), lChildTag.getAttribute(AppConstants.keyId));
			
			//get title of previous report tag page.
			String lPreviousReportPageTitle = "";
			if (lXmlTag != null) {
				if (lXmlTag.getParentTag() != null) {
					lPreviousReportPageTitle = CDesktopComponents.sSpring().unescapeXML(lXmlTag.getParentTag().getChildValue("shorttitle"));
				}
			}
			
			//NOTE lChildTag is a reference to the first report tag, the original report tag.
			//But we must have the last report tag, a status tag, because it contains the last report.
			List<IXMLTag> lAllReportTags = lChildTag.getParentTag().getChilds("report");
			if (lAllReportTags.size() > 0) {
				lChildTag = lAllReportTags.get(lAllReportTags.size() - 1);
			}
			//NOTE lXmlTag is a reference to the first report tag, the original report tag.
			//But we must have the last report tag, a status tag, because it contains the last report.
			if (lXmlTag != null) {
				lAllReportTags = lXmlTag.getParentTag().getChilds("report");
				if (lAllReportTags.size() > 0) {
					lXmlTag = lAllReportTags.get(lAllReportTags.size() - 1);
				}
			}
			HtmlMacroComponent lMacro = new CDefMacro();
			lVbox.appendChild(lMacro);
			lMacro.setDynamicProperty("a_tutorial_showpreviousreport", (lXmlTag != null));
			lMacro.setDynamicProperty("a_tutorial_previousreportpagetitle", CDesktopComponents.vView().getLabel("run_tutorial.previousreportpagetitleprefix") + " " + lPreviousReportPageTitle);
			lMacro.setDynamicProperty("a_tutorial_previousreport", getReport(lXmlTag));
			lMacro.setDynamicProperty("a_tutorial_caneditreport", !lFinished);
			lMacro.setDynamicProperty("a_tutorial_report", getReport(lChildTag));
			String fckStyle = "width:600px;";
			String fckHeight = "";
			if (lXmlTag != null) {
				fckStyle += "position:relative;top:20px;height:150px";
				fckHeight = "210px";
			}
			else {
				fckStyle += "height:300px";
				fckHeight = "380px";
			}
			lMacro.setDynamicProperty("a_tutorial_fckStyle", fckStyle);
			lMacro.setDynamicProperty("a_tutorial_fckHeight", fckHeight);
			lMacro.setMacroURI(VView.v_run_tutorialreport_fr);
		}
		Window lButton = (Window)CDesktopComponents.vView().getComponent(buttonReadyId);
		if (lButton == null) {
			return;
		}
		lButton.setVisible(!lFinished);
		
		if (!previouspagesaccessible) {
			rerenderPreviousPageAndChapterTab(true);
		}
		if (!nextpagesaccessible && lFinished) {
			rerenderNextPageAndChapterTab(false);
		}

		if (runWnd != null)
			runWnd.setNoteTag(caseComponent, currentpagetag);
	}

	/**
	 * Rerenders previous page and chapter tab.
	 *
	 * @param aDisabled the a disabled
	 */
	public void rerenderPreviousPageAndChapterTab(boolean aDisabled) {
		if (previouspagetag != null) {
			// if previous page not yet finished don't disable it
			boolean lFinished = CDesktopComponents.sSpring().getCurrentTagStatus(previouspagetag,
					AppConstants.statusKeyFinished)
					.equals(AppConstants.statusValueTrue);
			if (!lFinished) {
				return;
			}
		}
		if (previouspagetag != null && currentpagetag != previouspagetag) {
			rerenderPageTab(previouspagetag, aDisabled);
		}
		if (previouschaptertag != null && currentchaptertag != previouschaptertag) {
			rerenderChapterTab(previouschaptertag, aDisabled);
		}
	}

	/**
	 * Rerenders next page and chapter tab.
	 *
	 * @param aDisabled the a disabled
	 */
	public void rerenderNextPageAndChapterTab(boolean aDisabled) {
		if (nextpagetag != null) {
			// if next page not yet finished (if finished the page and chapter tabs already are enabled)
			boolean lFinished = CDesktopComponents.sSpring().getCurrentTagStatus(nextpagetag,
					AppConstants.statusKeyFinished)
					.equals(AppConstants.statusValueTrue);
			if (lFinished) {
				return;
			}
		}
		if (nextpagetag != null && nextpagetag != currentpagetag) {
			rerenderPageTab(nextpagetag, aDisabled);
		}
		if (islastchapterpage && nextchaptertag != null && nextchaptertag != currentchaptertag) {
			rerenderChapterTab(nextchaptertag, aDisabled);
		}
	}

	/**
	 * Rerenders page tab.
	 *
	 * @param aPageTag the a page tag
	 * @param aDisabled the a disabled
	 */
	public void rerenderPageTab(IXMLTag aPageTag, boolean aDisabled) {
		if (aPageTag == null) {
			return;
		}
		Hbox hbox = (Hbox)CDesktopComponents.vView().getComponent(listitembackgroundId + aPageTag.getAttribute(AppConstants.keyId));
		if (hbox != null) {
			hbox.setAttribute("disabled", "" + aDisabled);
			Events.postEvent("onEmUpdateStyle", hbox, currentpagetag.getAttribute(AppConstants.keyId));
		}
		Label label = (Label)CDesktopComponents.vView().getComponent(listitemId + aPageTag.getAttribute(AppConstants.keyId));
		if (label != null) {
			label.setAttribute("disabled", "" + aDisabled);
			Events.postEvent("onEmUpdateStyle", label, currentpagetag.getAttribute(AppConstants.keyId));
		}
	}

	/**
	 * Rerenders chapter tab.
	 *
	 * @param aChapterTag the a page tag
	 * @param aDisabled the a disabled
	 */
	public void rerenderChapterTab(IXMLTag aChapterTag, boolean aDisabled) {
		if (aChapterTag == null) {
			return;
		}
		Div div = (Div)CDesktopComponents.vView().getComponent(tabId + aChapterTag.getAttribute(AppConstants.keyId));
		if (div != null) {
			div.setAttribute("disabled", "" + aDisabled);
			Events.postEvent("onEmUpdateStyle", div, currentchaptertag.getAttribute(AppConstants.keyId));
		}
		Label label = (Label)CDesktopComponents.vView().getComponent(tablabelId + aChapterTag.getAttribute(AppConstants.keyId));
		if (label != null) {
			label.setAttribute("disabled", "" + aDisabled);
			Events.postEvent("onEmUpdateStyle", label, currentchaptertag.getAttribute(AppConstants.keyId));
		}
	}

	/**
	 * get instruction.
	 *
	 * @param aInstructionTag the instruction tag
	 */
	public String getInstruction(IXMLTag aInstructionTag) {
		if (aInstructionTag == null) {
			return "";
		}
		return CDesktopComponents.sSpring().unescapeXML(aInstructionTag.getChildValue("richtext"));
	}

	/**
	 * Gets referenced case component and tag.
	 *
	 * @param aRefTag the a ref tag
	 *
	 * @return list of case component and tag
	 */
	public List<Object> getCaseComponentAndTag(IXMLTag aRefTag) {
		if (aRefTag == null) {
			return null;
		}
		List<Object> result = new ArrayList<Object>();
		IECaseComponent lCaseComponent = null;
		IXMLTag lXmlTag = null;
		String lReftype = aRefTag.getDefAttribute(AppConstants.defKeyReftype);
		CCaseHelper cCaseHelper = new CCaseHelper();
		IECaseRole lCaseRole = CDesktopComponents.sSpring().getSCaseRoleHelper().getCaseRole();
		List<String> lRefIds = cCaseHelper.getRefTagIds(lReftype, "" + AppConstants.statusKeySelectedIndex, lCaseRole, caseComponent, aRefTag.getParentTag());
		if (lRefIds.size() > 0) {
			String lRefId = (String)lRefIds.get(0);
			if ((lRefId != null) && (!lRefId.equals(""))) {
					String[] lIdArr = lRefId.split(",");
				if (lIdArr.length == 3) {
					String lCacId = lIdArr[1];
					lCaseComponent = CDesktopComponents.sSpring().getCaseComponent(lCacId);
					String lTagId = lIdArr[2];
					lXmlTag = CDesktopComponents.sSpring().getXmlDataPlusRunStatusTag(CDesktopComponents.sSpring().getCaseComponent(Integer.parseInt(lCacId)), getRunStatusType(), lTagId);
				}
			}
		}
		result.add(lCaseComponent);
		result.add(lXmlTag);
		return result;
	}

	/**
	 * Get new run conversation interaction.
	 *
	 * @return the new run conversation interaction
	 */
	public CRunConversationInteraction getNewRunConversationInteraction() {
		return new CRunConversationInteraction("runConversationInteraction", null, "runConversations");
	}

	/**
	 * Get new run conversations.
	 *
	 * @param aCaseComponent the a case component
	 * @param aXmlTag the a xml tag
	 * @param aRunConversationInteraction the a run conversation interaction
	 * @return the new run conversations
	 */
	public CRunComponent getNewRunConversations(IECaseComponent aCaseComponent, IXMLTag aXmlTag, CRunConversationInteraction aRunConversationInteraction) {
		return new CRunConversations("runConversations", aCaseComponent, aXmlTag, aRunConversationInteraction);
	}

	/**
	 * Get new run references.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return the new run references
	 */
	public CRunComponent getNewRunReferences(IECaseComponent aCaseComponent) {
		return new CRunReferences("runReferences", aCaseComponent);
	}

	/**
	 * handle page.
	 *
	 * @param aPageTagId the page tag id
	 */
	public void handlePage(String aPageTagId) {
		IXMLTag lPageTag = getPageTag(aPageTagId);
		if (lPageTag == null) {
			return;
		}
		setPageTag(lPageTag);
	}

	/**
	 * Sets current page tag and saves opened state.
	 *
	 * @param aPageTag the a page tag
	 */
	public void setPageTag(IXMLTag aPageTag) {
		if (aPageTag != null) {
			if (currentpagetag == null || !aPageTag.getAttribute(AppConstants.keyId).equals(currentpagetag.getAttribute(AppConstants.keyId))) {
//				maybe add something for shared component
				previouspagetag = currentpagetag;
				currentpagetag = aPageTag;
				currentpagetags.put(currentchaptertag.getAttribute(AppConstants.keyId), currentpagetag);
				nextpagetag = getNextPageTag(aPageTag);

				// save page first time started
				boolean lStarted = CDesktopComponents.sSpring().getCurrentTagStatus(currentpagetag,
						AppConstants.statusKeyStarted)
						.equals(AppConstants.statusValueTrue);
				if (!lStarted) {
					setRunTagStatus(caseComponent, currentpagetag, AppConstants.statusKeyStarted, AppConstants.statusValueTrue, true);
				}
				
				setRunTagStatus(caseComponent, currentpagetag, AppConstants.statusKeySelected, AppConstants.statusValueTrue, true);
				setRunTagStatus(caseComponent, currentpagetag, AppConstants.statusKeyOpened, AppConstants.statusValueTrue, true);
				openPage(currentpagetag);
			}
		}
	}

	/**
	 * Returns next page tag.
	 *
	 * @param aPageTag the a page tag
	 *
	 * @return next page tag
	 */
	protected IXMLTag getNextPageTag(IXMLTag aPageTag) {
		if (aPageTag == null) {
			return null;
		}
		islastchapterpage = true;
		IXMLTag lCurrentPageTag = null;
		for (IXMLTag lChapterTag : chaptertags) {
			boolean lHidden = ((CDesktopComponents.sSpring().getCurrentTagStatus(lChapterTag,
					AppConstants.statusKeyPresent))
					.equals(AppConstants.statusValueFalse));
			if (!lHidden) {
				boolean lDisabled = ((CDesktopComponents.sSpring().getCurrentTagStatus(lChapterTag,
						AppConstants.statusKeyAccessible))
						.equals(AppConstants.statusValueFalse));
				if (!lDisabled) {
					List<IXMLTag> lPageTags = lChapterTag.getChilds("page");
					for (IXMLTag lPageTag : lPageTags) {
						if (lCurrentPageTag != null) {
							lHidden = ((CDesktopComponents.sSpring().getCurrentTagStatus(lPageTag,
									AppConstants.statusKeyPresent))
									.equals(AppConstants.statusValueFalse));
							if (!lHidden) {
								lDisabled = ((CDesktopComponents.sSpring().getCurrentTagStatus(lPageTag,
										AppConstants.statusKeyAccessible))
										.equals(AppConstants.statusValueFalse));
								if (!lDisabled) {
									islastchapterpage = lPageTag.getParentTag() != aPageTag.getParentTag();
									return lPageTag;
								}
							}
						}
						if (lPageTag == aPageTag) {
							lCurrentPageTag = lPageTag;
						}
					}
				}
			}
		}
		return null;
	}

	/**
	 * Sets current page tag and saves opened state.
	 *
	 * @param aPageTagId the a page tag id
	 */
	public void setPage(String aPageTagId) {
		IXMLTag lCurrentPageTag = getPageTag(aPageTagId);
		setPageTag(lCurrentPageTag);
	}

	/**
	 * Gets current page tag.
	 *
	 * @param aPageTagId the a page tag id
	 */
	public IXMLTag getPageTag(String aPageTagId) {
		if (aPageTagId == null) {
			return null;
		}
		if (chaptertags == null || chaptertags.size() == 0) {
			return null;
		}
		IXMLTag lCurrentPageTag = null;
		for (IXMLTag lChapterTag : chaptertags) {
			List<IXMLTag> lPageTags = lChapterTag.getChilds("page");
			for (IXMLTag lPageTag : lPageTags) {
				if (lPageTag.getAttribute(AppConstants.keyId).equals(aPageTagId)) {
					lCurrentPageTag = lPageTag;
				}
			}
		}
		return lCurrentPageTag;
	}

	/**
	 * handle report.
	 *
	 * @param aReport the report
	 */
	public void handleReport(String aReport) {
		saveReport(aReport);
	}

	/**
	 * get report.
	 *
	 * @param aReportTag the report tag
	 */
	public String getReport(IXMLTag aReportTag) {
		if (aReportTag == null) {
			return "";
		}
		return CDesktopComponents.sSpring().unescapeXML(aReportTag.getStatusChildValue("richtext"));
	}

	/**
	 * save report.
	 *
	 * @param aReport the report
	 */
	public void saveReport(String aReport) {
		if (currentreporttag == null) {
			return;
		}
		aReport = CDesktopComponents.sSpring().escapeXML(aReport);
		IXMLTag lDataTag = currentreporttag;
		IXMLTag lStatusTag = CDesktopComponents.sSpring().newRunNodeTag(lDataTag.getName(), lDataTag.getAttribute(AppConstants.keyId), null, null);
		addReport(lStatusTag, aReport);
		SStatustagEnrichment lStatustagEnrichment = new SStatustagEnrichment(); 
		lStatustagEnrichment.setStatusChildTags(lStatusTag.getChild(AppConstants.statusElement).getChildTags());
		lStatusTag = CDesktopComponents.sSpring().setRunTagStatus(caseComponent,
				lDataTag.getAttribute(AppConstants.keyId), "", AppConstants.statusKeySent,
				AppConstants.statusValueTrue, lStatustagEnrichment, true, null, getRunStatusType(), true, false);
	}

	/**
	 * Adds report child to aStatusTag.
	 *
	 * @param aStatusTag the a status tag
	 * @param aReport the a body
	 */
	private void addReport(IXMLTag aStatusTag, String aReport) {
		// add richtext as childtag
		IXMLTag lStatusStatusTag = aStatusTag.getChild(AppConstants.statusElement);
		List<IXMLTag> lXmlChildTags = lStatusStatusTag.getChildTags();
		IXMLTag lChildTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag("richtext", aReport);
		lChildTag.setParentTag(lStatusStatusTag);
		lXmlChildTags.add(lChildTag);
	}

	/**
	 * handle page ready.
	 */
	public void handlePageReady() {
		boolean lFinished = CDesktopComponents.sSpring().getCurrentTagStatus(currentpagetag,
				AppConstants.statusKeyFinished)
				.equals(AppConstants.statusValueTrue);
		if (lFinished) {
			return;
		}
		setRunTagStatus(caseComponent, currentpagetag, AppConstants.statusKeyFinished, AppConstants.statusValueTrue, true);
		Label label = (Label)CDesktopComponents.vView().getComponent(listitemId + currentpagetag.getAttribute(AppConstants.keyId));
		if (label != null) {
			label.setAttribute("finished", "true");
			Events.postEvent("onEmUpdateStyle", label, currentpagetag.getAttribute(AppConstants.keyId));
		}
		if (currentreporttag != null) {
			//NOTE if report, rerender page so report cannot be edited anymore.
			openPage(currentpagetag);
		}
		if (!nextpagesaccessible) {
			rerenderNextPageAndChapterTab(false);
		}

		// loop through all chapter pages, if all finished, finish chapter
		List<IXMLTag> lPageTags = currentchaptertag.getChilds("page");
		for (IXMLTag lPageTag : lPageTags) {
			lFinished = CDesktopComponents.sSpring().getCurrentTagStatus(lPageTag,
					AppConstants.statusKeyFinished)
					.equals(AppConstants.statusValueTrue);
			if (!lFinished) {
				return;
			}
		}
		lFinished = CDesktopComponents.sSpring().getCurrentTagStatus(currentchaptertag,
				AppConstants.statusKeyFinished)
				.equals(AppConstants.statusValueTrue);
		if (lFinished) {
			return;
		}
		setRunTagStatus(caseComponent, currentchaptertag, AppConstants.statusKeyFinished, AppConstants.statusValueTrue, true);
		label = (Label)CDesktopComponents.vView().getComponent(tablabelId + currentchaptertag.getAttribute(AppConstants.keyId));
		if (label != null) {
			label.setAttribute("finished", "true");
			Events.postEvent("onEmUpdateStyle", label, currentchaptertag.getAttribute(AppConstants.keyId));
		}

		// loop through all chapters, if all finished, finish tutorial
		for (IXMLTag tag : chaptertags) {
			lFinished = CDesktopComponents.sSpring().getCurrentTagStatus(tag,
					AppConstants.statusKeyFinished)
					.equals(AppConstants.statusValueTrue);
			if (!lFinished) {
				return;
			}
		}
		lFinished = CDesktopComponents.sSpring().getCurrentRunComponentStatus(caseComponent,
				AppConstants.statusKeyFinished, AppConstants.statusTypeRunGroup)
				.equals(AppConstants.statusValueTrue);
		if (lFinished) {
			return;
		}
		setRunComponentStatus(caseComponent, AppConstants.statusKeyFinished, AppConstants.statusValueTrue, true);
	}

	@Override
	public void cleanup() {
		super.cleanup();
	}

}
