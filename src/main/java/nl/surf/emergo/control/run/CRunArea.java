/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.HtmlBasedComponent;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.view.VView;

/**
 * The Class CRunArea is the ancestor of all areas (ZK windows, in html divs) within the
 * Emergo player.
 * Every run area has the possibility to notify observers with a certain eventaction
 * and eventactionstatus. The observer pattern is implemented within the CDefWindow class.
 * And every run area can have a status which for instance is an indication for the type
 * of content it contains or the status of that content.
 */
public class CRunArea extends CRunPlayWnd {

	private static final long serialVersionUID = -8993247242816331749L;

	/** The run wnd. */
	protected CRunWnd runWnd = (CRunWnd) CDesktopComponents.vView().getComponent(CControl.runWnd);

	/** The event action, a string. */
	protected String eventAction = "";

	/** The event action status, an object, mostly a string, containing status info. */
	protected Object eventActionStatus = null;

	/** The status. */
	protected String status = "";

	public String getClassName() {
		return CDesktopComponents.vView().getRunClassName(className);
	}

	/**
	 * Instantiates a new c run area.
	 */
	public CRunArea() {
		setBorder("none");
	}

	/**
	 * Instantiates a new c run area.
	 *
	 * @param aId the a id
	 */
	public CRunArea(String aId) {
		if (!aId.equals(""))
			setId(aId);
		setBorder("none");
	}

	/**
	 * Instantiates a new c run area.
	 *
	 * @param aId the a id
	 * @param aStatus the a status
	 */
	public CRunArea(String aId, String aStatus) {
		setId(aId);
		setBorder("none");
		setStatus(status);
	}

	/**
	 * Sets the event action.
	 *
	 * @param aEventAction the new event action
	 */
	public void setEventAction(String aEventAction) {
		eventAction = aEventAction;
	}

	/**
	 * Sets the event action status.
	 *
	 * @param aEventActionStatus the new event action status
	 */
	public void setEventActionStatus(Object aEventActionStatus) {
		eventActionStatus = aEventActionStatus;
	}

	/**
	 * Notifies observers.
	 */
	public void notifyObservers() {
		notifyObservers(eventAction, eventActionStatus);
	}

	/**
	 * Checks if status is aStatus.
	 *
	 * @param aStatus the a status
	 *
	 * @return true, if is status
	 */
	public boolean isStatus(String aStatus) {
		return (aStatus.equals(status));
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param aStatus the new status
	 */
	public void setStatus(String aStatus) {
		status = aStatus;
	}

	@Override
	public String getStyle() {
		String lStyle = super.getStyle();
		if (lStyle == null) {
			return "";
		}
		return lStyle;
	}
	
	/**
	 * Decorates location object.
	 *
	 * @param aHtmlBasedComponent the html based component
	 * @param aTag the a tag
	 */
	public void decorateLocationObject(HtmlBasedComponent aHtmlBasedComponent, IXMLTag aTag) {
		String lToolTip = CDesktopComponents.sSpring().unescapeXML(aTag.getChildValue("hovertext"));
		if (!lToolTip.equals("")) { 
//			aHtmlBasedComponent.setTooltip("toolTip" + VView.initTooltip);
//			aHtmlBasedComponent.setAttribute("tooltip", lToolTip);
			aHtmlBasedComponent.setTooltiptext(lToolTip);
		}
		if (aTag.getCurrentStatusAttribute(AppConstants.statusKeyDrawAttention).equals(AppConstants.statusValueTrue)) {
			runWnd.drawAttentionToComponent("location" + aTag.getAttribute(AppConstants.keyId) + "Object", true);
		}
	}

	/**
	 * Get zul file used to render (part of) component content.
	 * 
	 * @param aTag the a tag
	 * 
	 * @return the zul file
	 */
	public String getZulfile(IXMLTag aTag) {
		String lZulfile = aTag.getChildAttribute(AppConstants.statusElement, "zulfile");
		if (lZulfile.equals("")) {
			return "";
		}
		return ".." + VView.getInitParameter("emergo.cases.path") + lZulfile;
	}

	/**
	 * Get path to zul file used to render (part of) component content.
	 * 
	 * @param aTag the a tag
	 * 
	 * @return the zul file path
	 */
	public String getZulfilePath(IXMLTag aTag) {
		String lZulfile = aTag.getChildAttribute(AppConstants.statusElement, "zulfile");
		if (lZulfile.equals("")) {
			return "";
		}
		String lZulfilePath = ".." + VView.getInitParameter("emergo.cases.path");
		int lIndex = lZulfile.lastIndexOf("/");
		if (lIndex >= 0) {
			lZulfilePath += lZulfile.substring(0, lIndex + 1);
		}
		return lZulfilePath;
	}

	public void onAction(String sender, String action, Object status) {
		//override
	}
	
}