/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Progressmeter;
import org.zkoss.zul.Timer;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.utilities.PropsValues;
import nl.surf.emergo.view.VView;

public class CRunConversationInterventionWebcamDiv extends CDefDiv {

	private static final long serialVersionUID = -6454349464648990255L;

	protected VView vView = CDesktopComponents.vView();

	protected String runComponentId = "";

	protected CRunConversations runConversations;

	protected CRunConversationInteraction runConversationInteraction = null;

	protected long numberOfResits = 0;

	protected boolean canBeStarted = false;

	protected boolean canBeResited = false;

	protected boolean hasPreviousFragment = false;

	protected boolean hasNextFragment = false;

	protected long maxDuration = -1;

	protected boolean showProgress = false;

	protected long timerCount = 0;

	protected boolean seeSelf = false;

	protected String movieName = "";

	protected long maxInitWebcamAttempts = 12;

	protected long initWebcamAttemptsCount = 0;

	protected long maxSaveInterventionsAttempts = 12;

	protected long saveInterventionAttemptsCount = 0;

	protected String movieExtension = "";

	protected String interventionBlobId = "";

	public void onCreate(CreateEvent aEvent) {
		Events.postEvent("onInitWebcam", this, null);
	}

	public void onInitWebcam(Event aEvent) {
		runComponentId = (String) VView.getParameter("runComponentId", this, "runConversations");

		runConversations = (CRunConversations) vView.getComponent(runComponentId);
		runConversationInteraction = null;
		if (runConversations == null) {
			runConversationInteraction = (CRunConversationInteraction) vView.getComponent("runConversationInteraction");
		} else {
			runConversationInteraction = runConversations.getRunConversationInteraction();
		}

		movieExtension = PropsValues.STREAMING_RECORDER_FILEEXTENSION;
		interventionBlobId = (String) VView.getParameter("intervention_blob_id", this, "");

		List<List<Object>> interventionData = (List<List<Object>>) VView.getParameter("intervention_data", this, null);
		if (interventionData == null) {
			// If zul file is started standalone use dummy data.
			interventionData = new ArrayList<List<Object>>();
			for (int i = 1; i <= 2; i++) {
				List<Object> data = new ArrayList<Object>();
				// numberOfResits
				data.add(new Long(2));
				// canBeStarted
				data.add(true);
				// hasPreviousFragment
				data.add(true);
				// hasNextFragment
				data.add(true);
				// maxDuration
				data.add(new Long(10));
				// seeSelf
				data.add(false);
				// base file name
				data.add("webcam" + i);
				interventionData.add(data);
			}
		}
		if (interventionData != null) {
			numberOfResits = (Long) interventionData.get(0).get(0);
			canBeStarted = (Boolean) interventionData.get(0).get(1);
			canBeResited = numberOfResits > 0;
			hasPreviousFragment = (Boolean) interventionData.get(0).get(2);
			hasNextFragment = (Boolean) interventionData.get(0).get(3);
			maxDuration = (Long) interventionData.get(0).get(4);
			showProgress = maxDuration > -1;
			timerCount = 0;
			seeSelf = (Boolean) interventionData.get(0).get(5);
			movieName = (String) interventionData.get(0).get(6);
			// movieName = Paths.get(movieName + movieExtension).toString();
			// Clients.evalJavaScript("alert('" + movieName + "');");
			movieName = movieName + movieExtension;
			if (interventionBlobId.equals("")) {
				getFellow("startWebcamBtn").setVisible(canBeStarted);
				getFellow("stopWebcamBtn").setVisible(false);
				getFellow("viewVignetBtn").setVisible(false);
				getFellow("viewWebcamBtn").setVisible(false);
				getFellow("resitWebcamBtn").setVisible(false);
				getFellow("nextFragmentBtn").setVisible(false);
				getFellow("webcamProgressmeter").setVisible(false);
				Clients.evalJavaScript(
						"showMessage('" + vView.getLabel("run_conversations.message.waitforinitialization") + "');");
				initWebcamAttemptsCount = 0;
				((Timer) getFellow("initWebcamTimer")).start();
			} else {
				if (runConversationInteraction != null) {
					runConversationInteraction.setStatus("interventionBlobReady");
				}
				getFellow("startWebcamBtn").setVisible(false);
				getFellow("stopWebcamBtn").setVisible(false);
				getFellow("viewVignetBtn").setVisible(true);
				getFellow("viewWebcamBtn").setVisible(true);
				getFellow("resitWebcamBtn").setVisible(true);
				getFellow("nextFragmentBtn").setVisible(true);
				getFellow("webcamProgressmeter").setVisible(false);
				setVisible(true);
			}
			Clients.evalJavaScript("connect('');");
			Clients.evalJavaScript("startRec('" + movieName + "', '" + seeSelf + "');");
		}
	}

	public void onShowRecord(Event aEvent) {
		initWebcamAttemptsCount = 0;
		((Timer) getFellow("initWebcamTimer")).stop();
		if (!canBeStarted) {
			Events.postEvent("onStartWebcam", this, null);
		}
	}

	public void onStartWebcam(Event aEvent) {
		getFellow("startWebcamBtn").setVisible(false);
		if (showProgress) {
			timerCount = 0;
			((Timer) getFellow("webcamTimer")).start();
		}
		getFellow("stopWebcamBtn").setVisible(true);
		if (showProgress) {
			getFellow("webcamProgressmeter").setVisible(true);
		}
		Clients.evalJavaScript("startRec('" + movieName + "', '" + seeSelf + "');");
	}

	public void onStopWebcam(Event aEvent) {
		getFellow("stopWebcamBtn").setVisible(false);
		if (showProgress) {
			((Timer) getFellow("webcamTimer")).stop();
			timerCount = 0;
			((Progressmeter) getFellow("webcamProgressmeter")).setValue(0);
			getFellow("webcamProgressmeter").setVisible(false);
		}
		Clients.evalJavaScript("stopRec();");
		Clients.evalJavaScript("showMessage('" + vView.getLabel("run_conversations.message.waitforrecording") + "');");
	}

	public void onRecordComplete(Event aEvent) {
		saveInterventionAttemptsCount = 0;
		((Timer) getFellow("saveInterventionTimer")).start();
	}

	public void onResitWebcam(Event aEvent) {
		getFellow("resitWebcamBtn").setVisible(false);
		((Timer) getFellow("webcamTimer")).stop();
		timerCount = 0;
		((Progressmeter) getFellow("webcamProgressmeter")).setValue(0);
		getFellow("webcamProgressmeter").setVisible(false);
		getFellow("stopWebcamBtn").setVisible(false);
		getFellow("viewVignetBtn").setVisible(false);
		getFellow("viewWebcamBtn").setVisible(false);
		getFellow("nextFragmentBtn").setVisible(false);
		numberOfResits -= 1;
		// determine canBeResited
		canBeResited = numberOfResits > 0;
		((Timer) getFellow("saveInterventionTimer")).stop();
		Clients.evalJavaScript("initWebcam(false);");
		if (canBeStarted) {
			getFellow("startWebcamBtn").setVisible(true);
		} else {
			setVisible(false);
			Clients.evalJavaScript(
					"showMessage('" + vView.getLabel("run_conversations.message.waitforinitialization") + "');");
			Events.postEvent("onStartWebcam", this, null);
		}
	}

	public void onPreviousFragment(Event aEvent) {
		setVisible(false);
		Clients.evalJavaScript("showMessage('');");
		((Timer) getFellow("webcamTimer")).stop();
		((Timer) getFellow("saveInterventionTimer")).stop();
		// Clients.evalJavaScript("disconnect();");
		runConversations.playPreviousInterventionFragment();
	}

	public void onNextFragment(Event aEvent) {
		setVisible(false);
		Clients.evalJavaScript("showMessage('');");
		((Timer) getFellow("webcamTimer")).stop();
		((Timer) getFellow("saveInterventionTimer")).stop();
		// Clients.evalJavaScript("disconnect();");
		runConversations.playNextInterventionFragment();
	}

}