/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.prepare;

import java.util.ArrayList;
import java.util.List;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitSubStepsDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTag;

public class CRunPVToolkitPrepareSubStepsDiv extends CRunPVToolkitSubStepsDiv {

	private static final long serialVersionUID = 91586446871246207L;

	@Override
	protected List<IXMLTag> getSubStepTags(CRunPVToolkitCacAndTag runPVToolkitCacAndTag) {
		//NOTE always progress of prepare step in first cycle is used. However, in next cycles availability of sub step buttons may depend on present of sub step.
		//So check present of sub steps in current cycle.
		List<IXMLTag> currentSubStepTags = pvToolkit.getCurrentStepTag().getChilds("substep");
		List<IXMLTag> subStepTags = pvToolkit.getFirstStepTag(CRunPVToolkit.prepareStepType).getChilds("substep");
		List<IXMLTag> stepTagsToUse = new ArrayList<IXMLTag>();
		for (IXMLTag subStepTag : subStepTags) {
			int index = subStepTags.indexOf(subStepTag);
			if (index >= 0 && index < currentSubStepTags.size()) {
				if (!currentSubStepTags.get(index).getCurrentStatusAttribute(AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse)) {
					stepTagsToUse.add(subStepTag);
				}
			}
		}
		return stepTagsToUse;
	}
	
	@Override
	protected IXMLTag getStepTagForSubSteps() {
		//NOTE always use progress of first prepare step, because progress of prepare steps is shared over cycles
		return pvToolkit.getFirstStepTag(CRunPVToolkit.prepareStepType);
	}
	
	@Override
	protected List<String> getComponentToNavigateToIds(List<IXMLTag> subStepTags) {
		List<String> componentIds = new ArrayList<String>();
		for (IXMLTag subStepTag : subStepTags) {
			String subSteptype = subStepTag.getChildValue("substeptype");
			if (subSteptype.equals(CRunPVToolkit.prepareStudyRubricSubStepType)) {
				componentIds.add("prepareSubStepDiv1");
			}
			else if (subSteptype.equals(CRunPVToolkit.prepareAnalyzeVideoSubStepType)) {
				componentIds.add("prepareSubStepDiv2");
			}
			else if (subSteptype.equals(CRunPVToolkit.prepareCompareWithExpertsSubStepType)) {
				componentIds.add("prepareSubStepDiv3");
			}
		}
		return componentIds;
	}
	
	@Override
	protected List<String> getComponentToInitIds(List<IXMLTag> subStepTags) {
		List<String> componentIds = new ArrayList<String>();
		for (IXMLTag subStepTag : subStepTags) {
			String subSteptype = subStepTag.getChildValue("substeptype");
			if (subSteptype.equals(CRunPVToolkit.prepareStudyRubricSubStepType)) {
				componentIds.add("prepareSubStepDiv1");
			}
			else if (subSteptype.equals(CRunPVToolkit.prepareAnalyzeVideoSubStepType)) {
				componentIds.add("prepareSubStepDiv2");
			}
			else if (subSteptype.equals(CRunPVToolkit.prepareCompareWithExpertsSubStepType)) {
				componentIds.add("prepareSubStepDiv3");
			}
		}
		return componentIds;
	}
	
	@Override
	protected boolean updateSubSteps(List<IXMLTag> subStepTags) {
		CRunPVToolkitCacAndTag practice = pvToolkit.getPractice(_actor, getStepTagForSubSteps());
		//NOTE no change if no practice tags
		if (practice == null || practice.getXmlTag() == null) {
			return false;
		}
		List<IXMLTag> skillExampleTags = pvToolkit.getPresentChildTags(new CRunPVToolkitCacAndTag(pvToolkit.getCurrentRubricsCaseComponent(), pvToolkit.getCurrentSkillTag()), "videoskillexample");
		List<Boolean> subStepsDone = getSubStepsDone(subStepTags, skillExampleTags);
		boolean lChanged = false;
		for (IXMLTag subStepTag : subStepTags) {
			int lInd = subStepTags.indexOf(subStepTag);
			if (updateSubStepAccessible(subStepTags, subStepsDone, lInd)) {
				lChanged = true;
			}
			if (updateSubStepFinished(subStepTags, subStepsDone, lInd)) {
				lChanged = true;
			}
		}
		return lChanged;
	}

	protected List<Boolean> getSubStepsDone(List<IXMLTag> subStepTags, List<IXMLTag> skillExampleTags) {
		return pvToolkit.getPrepareSubStepsDone(_actor, getFeedbackStepTag(), subStepTags, skillExampleTags);
	}

	protected IXMLTag getFeedbackStepTag() {
		//NOTE for prepare recordings the feedback step is a next step.
		return pvToolkit.getNextStepTagInCurrentCycle(getStepTagForSubSteps(), CRunPVToolkit.feedbackStepType);
	}
	
}
