/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import java.util.List;

import org.zkoss.zk.ui.util.Clients;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunUnityWnd. The Emergo player window used for Unity player demo.
 */
public class CRunUnityWndClassic extends CRunWndClassic
 {

	private static final long serialVersionUID = 1585310536437957590L;

	/**
	 * Instantiates a new c run unity wnd.
	 */
	public CRunUnityWndClassic() {
		super();
	}

	@Override
	public void onAction(String sender, String action, Object status) {
		if (action.equals("showNote")) {
			super.onAction(sender, action, status);
			emergoEvent("open note");
		} else
		if (action.equals("endNote")) {
			super.onAction(sender, action, status);
			emergoEvent("close note");
		} else
		if (action.equals("onEmNotify")) {
			if (sender.equals("unityplayer")) {
				String[] lStatus = (String[]) status;
				String lDataKey = lStatus[1];
				String lDataValue = lStatus[2];
				if (lDataKey.equals("scaredCount")) {
					String lScaredCount = lDataValue;
					int lCount = Integer.parseInt(lScaredCount);
					if (lCount > 4) {
						lCount = 4;
						emergoEvent("exiled");
					}
					sendAlert("birdscared" + lScaredCount);
				}
				if (lDataKey.equals("startOfGame")) {
					if (lDataValue.equals(AppConstants.statusValueTrue)) {
						sendAlert("startofgame");
						IXMLTag lLocationTag = getLocation(currentLocationTagId);
						if (lLocationTag != null) {
							String lPid = lLocationTag.getChildValue("pid");
							if (lPid.equals("Unity start"))
								emergoEvent(sSpring.getActiveRunGroupNames());
							else
								emergoEvent(lLocationTag.getChildValue("pid"));
						}
					}
				}
			} else
				super.onAction(sender, action, status);
		} else
			super.onAction(sender, action, status);
	}

	/**
	 * Sends notification to client, meant for Unity web player.
	 * 
	 * @param data the data
	 */
	public void emergoEvent(String data) {
		Clients.evalJavaScript("emergoEvent('" + data + "');");
	}

	/**
	 * Sends alert.
	 * 
	 * @param aAlertPid pid of alert tag
	 */
	public void sendAlert(String aAlertPid) {
		IECaseComponent lCaseComponent = sSpring.getCaseComponent("alerts", "");
		IXMLTag lTag = null;
		if (lCaseComponent != null) {
			List<IXMLTag> lTags = getCaseComponentTags(lCaseComponent);
			for (IXMLTag lAlert : lTags) {
				String lKey = lAlert.getChildValue("pid");
				if (lKey.equals(aAlertPid))
					lTag = lAlert;
			}
		}
		if (lTag != null) {
			sSpring.setRunTagStatus(lCaseComponent, lTag, AppConstants.statusKeySent, AppConstants.statusValueTrue, true, AppConstants.statusTypeRunGroup, true, false);
			emergoEvent(sSpring.unescapeXML(lTag.getChildValue("richtext")));
		}
	}

}
