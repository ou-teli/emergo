/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CIObserved;
import nl.surf.emergo.control.def.CDefLabel;
import nl.surf.emergo.control.def.CDefTextbox;
import nl.surf.emergo.control.run.CRunCloseBtn;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunNote is used to show the note window within the run choice area of the
 * Emergo player. Notes can be edited within the logbook too, so this class has a public
 * method to save content.
 */
public class CRunNoteClassic extends CRunVboxClassic {

	private static final long serialVersionUID = -5097954733051800638L;

	/** The run component. */
	protected CRunComponentClassic runComponent = null;

	/** The case component, the note casecomponent. */
	protected IECaseComponent caseComponent = null;

	/** The case component for notes, the case component for which to take notes. */
	protected IECaseComponent caseComponentForNotes = null;

	/** The tag for notes, the tag for which to take notes. */
	protected IXMLTag tagForNotes = null;

	/**
	 * The tag container for notes, the tag container for which to take notes.
	 * For instance for a question it is the conversation.
	 */
	protected IXMLTag tagContainerForNotes = null;

	/** The note title. */
	protected String noteTitle = null;

	/**
	 * Instantiates a new c run conversation interaction.
	 * Creates title area with close box and edit field for note.
	 * 
	 * @param aId the a id
	 * @param aRunComponent the a run component
	 */
	public CRunNoteClassic(String aId, CRunComponentClassic aRunComponent) {
		super();
		setId(aId);
		runComponent = aRunComponent;
		// there is only one note component allowed
		caseComponent = CDesktopComponents.sSpring().getCaseComponent("note", "");
		String lType = "Min";
		if (runWnd.runChoiceAreaMed)
			lType = "Med";
		if (runWnd.runChoiceAreaMax)
			lType = "Max";
		setZclass(className + lType);

		CRunHboxClassic lHbox = new CRunHboxClassic();
		appendChild(lHbox);
		createTitleArea(lHbox);
		createTextArea(this);
	}

	/**
	 * Creates title area, a title label and close box.
	 * 
	 * @param aParent the a parent
	 */
	protected void createTitleArea(Component aParent) {
		String lType = "Min";
		if (runWnd.runChoiceAreaMed)
			lType = "Med";
		if (runWnd.runChoiceAreaMax)
			lType = "Max";
		CRunAreaClassic lArea = new CRunAreaClassic();
		aParent.appendChild(lArea);
		lArea.setZclass(getZclass()+"_title_area_left");
		CRunHboxClassic lHbox = new CRunHboxClassic();
		lArea.appendChild(lHbox);
		Label lLabel = new CDefLabel();
		lLabel.setId(getId() + "Title");
		lLabel.setValue(CDesktopComponents.vView().getLabel("run_note.title"));
		lLabel.setZclass(className + "_title_area_label");
		lHbox.appendChild(lLabel);

		lArea = new CRunAreaClassic();
		aParent.appendChild(lArea);
		lArea.setZclass(className+"_title_area_right");
//		CRunHoverBtnClassic lButton = new CRunHoverBtnClassic(getId() + "CloseBtn",
//				"active","endNote", null, "close", "", "");
		CRunCloseBtn lButton = new CRunCloseBtn(getId() + "CloseBtn",
				"active","endNote", null, "close", "", "");
		lButton.setCanHaveStatusSelected(false);
		lButton.registerObserver("runNote");
		lButton.registerObserver(CControl.runWnd);
		lArea.appendChild(lButton);
	}

	/**
	 * Creates text area for note.
	 * 
	 * @param aParent the a parent
	 * 
	 * @return the c run area
	 */
	protected CRunAreaClassic createTextArea(Component aParent) {
		String lType = "Min";
		if (runWnd.runChoiceAreaMed)
			lType = "Med";
		if (runWnd.runChoiceAreaMax)
			lType = "Max";
		CRunAreaClassic lTextArea = new CRunAreaClassic();
		aParent.appendChild(lTextArea);
		lTextArea.setZclass(getZclass() + "_text_area");
		createText(lTextArea);
		return lTextArea;
	}

	/**
	 * Creates richtext field for note.
	 * 
	 * @param aParent the a parent
	 */
	protected void createText(Component aParent) {
		Textbox lTextbox = new CDefTextbox();
		aParent.appendChild(lTextbox);
		lTextbox.setId(getId()+"NoteField");
		lTextbox.setZclass(getZclass() + "_text_area_div");
		lTextbox.setRows(7);
		lTextbox.setAttribute("changed", AppConstants.statusValueFalse);
	}

	/**
	 * Gets the content, the saved content for the current note case component and tag.
	 * 
	 * @return the content
	 */
	protected String getContent() {
		String lContent = "";
		if (caseComponent != null) {
			// notes are private
			IXMLTag lStatusRootTag = CDesktopComponents.sSpring().getXmlRunStatusTree(caseComponent, AppConstants.statusTypeRunGroup);
			if (lStatusRootTag != null) {
				IXMLTag lStatusContentTag = lStatusRootTag.getChild(AppConstants.contentElement);
				if ((lStatusContentTag != null) && (caseComponentForNotes != null)) {
					String lCacId = "" + caseComponentForNotes.getCacId();
					boolean lTagForNotes = (tagForNotes != null); 
					String lDataTagId = "";
					String lStatusTagId = "";
					if (lTagForNotes) {
						lStatusTagId = tagForNotes.getAttribute(AppConstants.keyRefstatusid);
						lDataTagId = tagForNotes.getAttribute(AppConstants.keyId);
					}
					else {
						lDataTagId = "-1";
						lStatusTagId = "-1";
					}
					for (IXMLTag lTempStatusTag : lStatusContentTag.getChilds("note")) {
						if (lTempStatusTag.getAttribute(AppConstants.keyRefcacid).equals(lCacId)) {
							boolean lOk = ((!lTagForNotes) &&
								(lTempStatusTag.getAttribute(AppConstants.keyRefstatusid).equals("-1")) &&
								(lTempStatusTag.getAttribute(AppConstants.keyRefdataid).equals("-1")));
							if (!lOk) {
								lOk = ((!lStatusTagId.equals("")) &&
									(lTempStatusTag.getAttribute(AppConstants.keyRefstatusid).equals(lStatusTagId)));
							}
							if (!lOk) {
								lOk = ((!lDataTagId.equals("")) &&
									(lTempStatusTag.getAttribute(AppConstants.keyRefdataid).equals(lDataTagId)));
							}
							if (lOk)
								lContent = CDesktopComponents.sSpring().unescapeXML(lTempStatusTag.getChildValue("text"));
						}
					}
				}
			}
		}
		return lContent;
	}

	/**
	 * Saves the content for the given parameters.
	 * 
	 * @param aNoteCacId the a note cac id
	 * @param aNoteDataTagId the a note data tag id
	 * @param aNoteStatusTagId the a note status tag id
	 * @param aContent the a content
	 */
	public void saveContent(String aNoteCacId, String aNoteDataTagId, String aNoteStatusTagId, String aContent) {
		if (caseComponent != null) {
			// notes are private
			IXMLTag lStatusRootTag = CDesktopComponents.sSpring().getXmlRunStatusTree(caseComponent, AppConstants.statusTypeRunGroup);
			if (lStatusRootTag != null) {
				IXMLTag lStatusContentTag = lStatusRootTag.getChild(AppConstants.contentElement);
				if (lStatusContentTag != null) {
					String lCacId = aNoteCacId;
					boolean lTagForNotes = ((!aNoteDataTagId.equals("")) || (!aNoteStatusTagId.equals(""))); 
					String lDataTagId = "";
					String lStatusTagId = "";
					if (lTagForNotes) {
						lDataTagId = aNoteDataTagId;
						lStatusTagId = aNoteStatusTagId;
					}
					else {
						lDataTagId = "-1";
						lStatusTagId = "-1";
					}
					IXMLTag lStatusTag = null;
					for (IXMLTag lTempStatusTag : lStatusContentTag.getChildTags()) {
						if (lTempStatusTag.getAttribute(AppConstants.keyRefcacid).equals(lCacId)) {
							boolean lOk = ((!lTagForNotes) &&
								(lTempStatusTag.getAttribute(AppConstants.keyRefstatusid).equals("-1")) &&
								(lTempStatusTag.getAttribute(AppConstants.keyRefdataid).equals("-1")));
							if (!lOk) {
								lOk = ((!lStatusTagId.equals("")) &&
									(lTempStatusTag.getAttribute(AppConstants.keyRefstatusid).equals(lStatusTagId)));
							}
							if (!lOk) {
								lOk = ((!lDataTagId.equals("")) &&
									(lTempStatusTag.getAttribute(AppConstants.keyRefdataid).equals(lDataTagId)));
							}
							if (lOk)
								lStatusTag = lTempStatusTag;
						}
					}
					if (lStatusTag != null) {
						lStatusTag.getChild("text").setValue(CDesktopComponents.sSpring().escapeXML(aContent));
						// save status
						CDesktopComponents.sSpring().setSaveRunGroupCaseComponentStatusRootTag(CDesktopComponents.sSpring().getRunGroup(), caseComponent, lStatusRootTag);
					}
				}
			}
		}
	}

	/**
	 * Sets the content for the current note case component and tag.
	 * 
	 * @param aContent the a content
	 */
	protected void setContent(String aContent) {
		if (caseComponent != null) {
			// notes are private
			IXMLTag lStatusRootTag = CDesktopComponents.sSpring().getXmlRunStatusTree(caseComponent, AppConstants.statusTypeRunGroup);
			if (lStatusRootTag != null) {
				IXMLTag lStatusContentTag = lStatusRootTag.getChild(AppConstants.contentElement);
				if (lStatusContentTag != null) {
					String lCacId = "" + caseComponentForNotes.getCacId();
					boolean lTagForNotes = (tagForNotes != null); 
					String lDataTagId = "";
					String lStatusTagId = "";
					if (lTagForNotes) {
						if (tagForNotes.getAttribute(AppConstants.keyExtended).equals(AppConstants.statusValueTrue))
							lStatusTagId = tagForNotes.getAttribute(AppConstants.keyRefstatusid);
						else
							lDataTagId = tagForNotes.getAttribute(AppConstants.keyId);
					}
					else {
						lDataTagId = "-1";
						lStatusTagId = "-1";
					}
					IXMLTag lStatusTag = null;
					for (IXMLTag lTempStatusTag : lStatusContentTag.getChildTags()) {
						if (lTempStatusTag.getAttribute(AppConstants.keyRefcacid).equals(lCacId)) {
							boolean lOk = ((!lTagForNotes) &&
								(lTempStatusTag.getAttribute(AppConstants.keyRefstatusid).equals("-1")) &&
								(lTempStatusTag.getAttribute(AppConstants.keyRefdataid).equals("-1")));
							if (!lOk) {
								lOk = ((!lStatusTagId.equals("")) &&
									(lTempStatusTag.getAttribute(AppConstants.keyRefstatusid).equals(lStatusTagId)));
							}
							if (!lOk) {
								lOk = ((!lDataTagId.equals("")) &&
									(lTempStatusTag.getAttribute(AppConstants.keyRefdataid).equals(lDataTagId)));
							}
							if (lOk)
								lStatusTag = lTempStatusTag;
						}
					}
					if (lStatusTag == null) {
						lStatusTag = CDesktopComponents.sSpring().getXmlManager().newNodeTag(caseComponent.getEComponent().getXmldefinition(), "note");
						lStatusTag.setAttribute(AppConstants.keyRefcacid, lCacId);
						lStatusTag.setAttribute(AppConstants.keyRefdataid, lDataTagId);
						lStatusTag.setAttribute(AppConstants.keyRefstatusid, lStatusTagId);
						if (tagContainerForNotes != null) {
							lStatusTag.setAttribute("refcontainerid", tagContainerForNotes.getAttribute(AppConstants.keyId));
						}
						lStatusTag.getChild("pid").setValue(noteTitle);
						lStatusTag.getChild("text").setValue(CDesktopComponents.sSpring().escapeXML(aContent));
						List<String[]> lErrors = new ArrayList<String[]>();
						CDesktopComponents.sSpring().getXmlManager().newChildNode(lStatusRootTag, lStatusTag, lStatusContentTag, lErrors);
					}
					else {
						lStatusTag.getChild("pid").setValue(noteTitle);
						lStatusTag.getChild("text").setValue(CDesktopComponents.sSpring().escapeXML(aContent));
					}
					// save status
					CDesktopComponents.sSpring().setSaveRunGroupCaseComponentStatusRootTag(CDesktopComponents.sSpring().getRunGroup(), caseComponent, lStatusRootTag);
				}
			}
		}
	}

	/**
	 * Sets the content of the note component for the given parameters.
	 * 
	 * @param aCaseComponentForNotes the a case component for notes
	 * @param aTagForNotes the a tag for notes
	 */
	public void setContent(IECaseComponent aCaseComponentForNotes, IXMLTag aTagForNotes) {
		saveContent();
		caseComponentForNotes = aCaseComponentForNotes;
		tagForNotes = aTagForNotes;
		tagContainerForNotes = null;
		noteTitle = "";
		Label lLabel = (Label) CDesktopComponents.vView().getComponent(getId()+"Title");
		if (lLabel != null) {
			String lValue = CDesktopComponents.vView().getLabel("run_note.title");
			if (caseComponentForNotes != null) {
				String lComponentCode = caseComponentForNotes.getEComponent().getCode();
				if (lComponentCode.equals("locations")) {
					if (tagForNotes == null)
						// show navigator name
						noteTitle = caseComponentForNotes.getName();
					else
						// only show location name
						noteTitle = noteTitle + CDesktopComponents.sSpring().getXmlManager().getTagKeyValues(tagForNotes, tagForNotes.getDefTag().getAttribute(AppConstants.defKeyKey));
				}
				else {
					if (((lComponentCode.equals("conversations")) || (lComponentCode.equals("assessments"))) && (tagForNotes != null)) {
						String lTagName = "";
						if (lComponentCode.equals("conversations"))
							lTagName = "conversation";
						if (lComponentCode.equals("assessments"))
							lTagName = "assessment";
						IXMLTag lParentTag = tagForNotes;
						while ((lParentTag != null) && (!lParentTag.getName().equals(lTagName)))
							lParentTag = lParentTag.getParentTag();
						if (lParentTag != null) {
							if (tagForNotes.getName().equals(lTagName)) {
								// only show conversation or assessment name
								noteTitle = tagForNotes.getChildValue("name");
							}
							else {
								// show conversation or assessment name and tag name
								tagContainerForNotes = lParentTag;
								noteTitle = tagContainerForNotes.getChildValue("name");
								noteTitle = noteTitle + ": " + CDesktopComponents.sSpring().getXmlManager().getTagKeyValues(tagForNotes, tagForNotes.getDefTag().getAttribute(AppConstants.defKeyKey));
							}
						}
						else
							noteTitle = caseComponentForNotes.getName();
					}
					else {
						// show case component name
						noteTitle = caseComponentForNotes.getName();
						if (tagForNotes != null) {
							// show tag name too
							noteTitle = noteTitle + ": " + CDesktopComponents.sSpring().getXmlManager().getTagKeyValues(tagForNotes, tagForNotes.getDefTag().getAttribute(AppConstants.defKeyKey));
						}
					}
				}				
			}
			lValue = lValue + ": " + noteTitle;
			lLabel.setValue(lValue);
		}
		Textbox lTextbox = (Textbox) CDesktopComponents.vView().getComponent(getId()+"NoteField");
		if (lTextbox != null) {
			lTextbox.setValue(getContent());
		}
	}

	/**
	 * Saves the current content of the note component.
	 */
	public void saveContent() {
		// if content changed, save it
		Textbox lTextbox = (Textbox) CDesktopComponents.vView().getComponent(getId()+"NoteField");
		if (lTextbox != null) {
			String lOldContent = getContent();
			String lNewContent = lTextbox.getValue();
			if (!lNewContent.equals(lOldContent))
				setContent(lNewContent);
		}
	}

	/**
	 * Is called by end note close box, saves the note and restores the status of the run choice area. 
	 * 
	 * @param aObserved the observed object, it should have interface CIObserved implemented
	 * @param aAction the action
	 * @param aStatus the status object, can be String or other class
	 */
	@Override
	public void observedNotify(CIObserved aObserved, String aAction,
			Object aStatus) {
		if (aAction.equals("endNote")) {
			saveContent();
			runWnd.runChoiceArea.restoreStatus();
		}
	}

}
