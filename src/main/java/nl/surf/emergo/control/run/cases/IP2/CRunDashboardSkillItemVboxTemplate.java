/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.IP2;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefVbox;

public class CRunDashboardSkillItemVboxTemplate extends CDefVbox {

	private static final long serialVersionUID = 8623015524119518895L;

	public void onInit(Event aEvent) {
		CRunDashboardDiv runDashboardDiv = (CRunDashboardDiv)CDesktopComponents.vView().getComponent("dashboardDiv");
		if (runDashboardDiv == null) {
			return;
		}
		double maxWidth = 254;
		int number = ((int[])aEvent.getData())[0];
		int offsetx = ((int[])aEvent.getData())[1];
		int offsety = ((int[])aEvent.getData())[2];
		//
		setAttribute("skillNumber", number);
		setStyle("position:absolute;left:" + offsetx + "px;top:" + offsety + "px;cursor:pointer;");
		//
		Label label = (Label)getChildren().get(0).getChildren().get(0).getChildren().get(0);
		label.setValue(runDashboardDiv.getSkillData(number, "label"));
		//
		label = (Label)getChildren().get(0).getChildren().get(1).getChildren().get(0);
		double maxNumberOfPoints = Double.parseDouble(runDashboardDiv.getSkillData(number, "maxNumberOfPoints"));
		double numberOfPoints = Double.parseDouble(runDashboardDiv.getSkillData(number, "numberOfPoints"));
		label.setValue("" + Math.round((100 * numberOfPoints) / maxNumberOfPoints) + "%");
		//
		Div div = (Div)getChildren().get(2);
		if (number < 4) {
			div.setClass("skillsliderC");
		}
		else {
			div.setClass("skillsliderG");
		}
		String divWidth = "" + Math.round(maxWidth * (numberOfPoints/maxNumberOfPoints)); 
		div.setStyle("position:relative;left:3px;top:-11px;width:" + divWidth + "px;");
		//
		setVisible(true);
	}

	public void onClick(Event aEvent) {
		Component component = CDesktopComponents.vView().getComponent("skilldetails");
		Events.postEvent("onInit", component, aEvent.getTarget().getAttribute("skillNumber"));
	}

}
