/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.IP2;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.CRunArea;
import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.control.run.CRunEditforms;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.zkspring.SSpringHelper;

public class CRunKP4AfsluitendeBriefDiv extends CRunArea {

	private static final long serialVersionUID = 7972095563874709088L;

	protected SSpringHelper sSpringHelper = new SSpringHelper();

	public List<String> headers = new ArrayList<String>();
	public List<String> sections = new ArrayList<String>();
		
	public CRunKP4AfsluitendeBriefDiv() {
		super();
		init();
	}

	public void init() {
		Events.postEvent("onInitZulfile", this, null);
	}


	/**
	 * Methods originally used in ZUL file.
	 */

	public void onInitZulfile(Event aEvent) {
	}

	public void initLetter() {
		headers.clear();
		sections.clear();
		//Get current case component
		HtmlMacroComponent macro = (HtmlMacroComponent)CRunComponent.getMacroParent(this);
		Map<String,Object> propertyMap = (Map<String,Object>)macro.getDynamicProperty("a_propertyMap");
		CRunEditforms currentEmergoComponent = (CRunEditforms)propertyMap.get("currentEmergoComponent");
		IECaseComponent caseComponent = currentEmergoComponent.getCaseComponent();
		if (caseComponent != null) {
			//Get all sections
			List<IXMLTag> nodeTags = CDesktopComponents.cScript().getNodeTags(caseComponent, "section");
			//Get headers from sections whose id contains '_kop_'
			for (IXMLTag nodeTag : nodeTags) {
				if (nodeTag.getChildValue("pid").matches("O_4_([0-9]+)_kop_(.*)")) {
					headers.add(sSpringHelper.getTagChildValue(nodeTag, "title", ""));
				}
			}
			//Get sections whose id contains '_invulveld_'
			for (int i=0;i<headers.size();i++) {
				sections.add(sSpringHelper.getCurrentStatusTagStatusChildAttribute(caseComponent, "section", "O_4_"+ (i + 1) + "_invulveld_1", "string", ""));
			}
		}
	}

	public void onCreate(CreateEvent aEvent) {
		Events.postEvent("onUpdate", this, null);
	}

	public void onUpdate(Event aEvent) {
		initLetter();
		Events.postEvent("onUpdate", getChildren().get(0), null);
	}

}
