/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.Hashtable;

import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.DropEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Html;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefBox;

public class CRunDragdropformItemboxTemplate extends CDefBox {

	private static final long serialVersionUID = -7467084711129025413L;

	public void onInit(Event aEvent) {
		Hashtable<String,Object> item = (Hashtable<String,Object>)aEvent.getData();
		CRunDragdropformItemHelper.onInit((CRunDragdropforms)CDesktopComponents.vView().getComponent("runDragdropforms"), this, item);
		Html htmlChild = (Html)getChildren().get(0);
		htmlChild.setContent((String)item.get("content"));
		Events.postEvent("onUpdate", this, null);
	}

	public void onUpdate(Event aEvent) {
		boolean hideRightWrong = aEvent.getData() != null && aEvent.getData().equals("hideRightWrong");
		CRunDragdropforms runDragdropforms = (CRunDragdropforms)CDesktopComponents.vView().getComponent("runDragdropforms");
		String sclass = "CRunDragdropformContainerItemBox";
		if (getDraggable().equals("true")) {
			sclass += "Draggable";
			if (!runDragdropforms.containerDoNotShowRightWrong((IXMLTag)getAttribute("tag"))) {
				if (runDragdropforms.isShowIfRightOrWrong((IXMLTag)getAttribute("tag")) && !hideRightWrong) {
					if (runDragdropforms.isRightlyPlaced((IXMLTag)getAttribute("tag")).equals("true")) {
						sclass += "Right";
					}
					else if (runDragdropforms.isRightlyPlaced((IXMLTag)getAttribute("tag")).equals("false")) {
						sclass += "Wrong";
					}
				}
			}
		}
		if (getSclass() == null || !getSclass().equals(sclass)) {
			setSclass(sclass);
		}
		sclass = "CRunDragdropformContainerItemBox";
		if ((Boolean)getAttribute("isDocument")) {
			sclass += "Document";
		}
		else {
			sclass += "Text";
			if (getDraggable().equals("true")) {
				sclass += "Draggable";
			}
		}
		Html htmlChild = (Html)getChildren().get(0);
		if (htmlChild.getSclass() == null || !htmlChild.getSclass().equals(sclass)) {
			htmlChild.setSclass(sclass);
		}
		//alternative for drag and drop
		setStyle("");
	}

	public void onClick(Event aEvent) {
		CRunDragdropformItemHelper.onClick((CRunDragdropforms)CDesktopComponents.vView().getComponent("runDragdropforms"), this);
	}

	public void onDisable(Event aEvent) {
		CRunDragdropformItemHelper.onDisable(this);
	}

	public void onEnable(Event aEvent) {
		CRunDragdropformItemHelper.onEnable(this);
	}

	public void onDrop(DropEvent aEvent) {
		CRunDragdropformItemHelper.onDrop((CRunDragdropforms)CDesktopComponents.vView().getComponent("runDragdropforms"), (HtmlBasedComponent)aEvent.getDragged(), this);
	}

}
