/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.view.VView;

public class CRunIspotInterventionDiv extends CDefDiv {

	private static final long serialVersionUID = -2644281750627395100L;

	protected VView vView = CDesktopComponents.vView();

	protected CRunIspot runIspot = null;
	
	protected String interventionType = "";

	public void onCreate(CreateEvent aEvent) {
		runIspot = (CRunIspot)vView.getComponent("runIspot");
		//set attributes so they always have a value
		setAttribute("previous", false);
		setAttribute("next", false);
		setAttribute("showInstruction", false);
		setAttribute("showVignette", false);
		setAttribute("showFeedback", false);
		setAttribute("showReaction", false);
		setAttribute("showComment", false);
		setAttribute("showRecording", false);
		setAttribute("startRecording", false);
		setAttribute("stopRecording", false);
		setAttribute("redoRecording", false);
		setAttribute("sendRecording", false);
	}

	
	public void onIspotShowInstruction(Event aEvent) {
		runIspot.logMessage("iSpot.onIspotShowInstruction - " + getClass().getName());
		Events.sendEvent("onIspotShowText", this, ((CRunIspotIntervention)getAttribute("dataElement")).getVignetteInstruction());
		Events.sendEvent("onIspotHideButton", this, vView.getComponent("ispotShowInstructionButton" + interventionType));
	}

	public void onIspotShowVignette(Event aEvent) {
		runIspot.logMessage("iSpot.onIspotShowVignette - " + getClass().getName());
		Events.sendEvent("onIspotShowPlayer", this, ((CRunIspotIntervention)getAttribute("dataElement")).getVignetteUrl());
		Events.sendEvent("onIspotHideButton", this, vView.getComponent("ispotShowVignetteButton" + interventionType));
	}

	public void onIspotShowWebcamSettings(Event aEvent) {
		Events.sendEvent("onIspotShowWebcamInstruction", vView.getComponent("ispotViewers"), null);
	}

	public void onIspotRecordingStarted(Event aEvent) {
		runIspot.setComponentVisible(vView.getComponent("ispotStopRecordingButton" + interventionType), (Boolean)getAttribute("stopRecording"));
		runIspot.setComponentVisible(vView.getComponent("ispotPauseRecordingButton" + interventionType), (Boolean)getAttribute("canInterruptRecording"));
		Events.sendEvent("onIspotRecordingStarted", vView.getComponent("ispotViewers"), getAttribute("dataElement"));
		Events.sendEvent("onIspotHideWebcamInstruction", vView.getComponent("ispotViewers"), null);
	}

	public void onIspotRedoRecording(Event aEvent) {
		Events.sendEvent("onIspotStartRecording", this, null);
	}

	public void onIspotCancelRecording(Event aEvent) {
		Events.sendEvent("onIspotBreakRecording", this, null);
		Events.sendEvent("onIspotHideViewers", vView.getComponent("ispotViewers"), null);
		Events.sendEvent("onIspotShowButtons", this, null);
		runIspot.setComponentVisible(vView.getComponent("ispotStartRecordingButton" + interventionType), (Boolean)getAttribute("startRecording"));
		runIspot.setComponentVisible(vView.getComponent("ispotRedoRecordingButton" + interventionType), (Boolean)getAttribute("redoRecording"));
		runIspot.setComponentVisible(vView.getComponent("ispotSendRecordingButton" + interventionType), (Boolean)getAttribute("sendRecording"));
	}

	protected void _IspotKillRecording(boolean aSave) {
		if (vView.getComponent(runIspot.webcamBoxId) != null) {
			if (aSave)
				runIspot.logMessage("iSpot.onIspotStopRecording - " + getClass().getName());
			else
				runIspot.logMessage("iSpot.onIspotCancelRecording - " + getClass().getName());
			((CRunWebcamInitBox)vView.getComponent(runIspot.webcamBoxId)).stopRecording(aSave);
		}
		runIspot.setComponentVisible(vView.getComponent("ispotStopRecordingButton" + interventionType), false);
		runIspot.setComponentVisible(vView.getComponent("ispotPauseRecordingButton" + interventionType), false);
		runIspot.setComponentVisible(vView.getComponent("ispotResumeRecordingButton" + interventionType), false);
		Events.sendEvent("onIspotHideWebcamProgress", vView.getComponent("ispotViewers"), null);
		runIspot.releaseRecording();
	}
	
	protected String getInterventionId() {
		return "";
	}

	public void onIspotStartRecording(Event aEvent) {
		runIspot.logMessage("iSpot.onIspotStartRecording - " + getClass().getName());
		Events.sendEvent("onIspotShowWebcam", this, getAttribute("dataElement"));
		runIspot.setComponentVisible(vView.getComponent("ispotShowRecordingButton" + interventionType), false);
		runIspot.setComponentVisible(vView.getComponent("ispotStartRecordingButton" + interventionType), false);
		runIspot.setComponentVisible(vView.getComponent("ispotRedoRecordingButton" + interventionType), false);
		runIspot.setComponentVisible(vView.getComponent("ispotSendRecordingButton" + interventionType), false);
		if (!(Boolean)getAttribute("canInterruptRecording")) {
			runIspot.setComponentVisible(vView.getComponent("ispotShowInstructionButton" + interventionType), false);
			runIspot.setComponentVisible(vView.getComponent("ispotShowVignetteButton" + interventionType), false);
			runIspot.setComponentVisible(vView.getComponent("ispotShowFeedbackButton" + interventionType), false);
			Events.sendEvent("onEnableMenu", vView.getComponent("ispotMenu"), false);
		}
		//set current recording type
		runIspot.setCurrentRecordingId(getInterventionId());
	}

	public void onIspotPauseRecording(Event aEvent) {
		if (vView.getComponent(runIspot.webcamBoxId) != null) {
			((CRunWebcamInitBox)vView.getComponent(runIspot.webcamBoxId)).pauseRecording();
		}
	}

	public void onIspotRecordingPaused(Event aEvent) {
		Events.sendEvent("onIspotRecordingPaused", vView.getComponent("ispotViewers"), getAttribute("dataElement"));
		runIspot.setComponentVisible(vView.getComponent("ispotPauseRecordingButton" + interventionType), false);
		runIspot.setComponentVisible(vView.getComponent("ispotResumeRecordingButton" + interventionType), true);
	}

	public void onIspotResumeRecording(Event aEvent) {
		if (vView.getComponent(runIspot.webcamBoxId) != null) {
			((CRunWebcamInitBox)vView.getComponent(runIspot.webcamBoxId)).resumeRecording();
		}
	}

	public void onIspotRecordingResumed(Event aEvent) {
		Events.sendEvent("onIspotRecordingResumed", vView.getComponent("ispotViewers"), getAttribute("dataElement"));
		runIspot.setComponentVisible(vView.getComponent("ispotPauseRecordingButton" + interventionType), true);
		runIspot.setComponentVisible(vView.getComponent("ispotResumeRecordingButton" + interventionType), false);
	}

	public void onIspotStopRecording(Event aEvent) {
		_IspotKillRecording(true);
	}

	public void onIspotBreakRecording(Event aEvent) {
		_IspotKillRecording(false);
	}

	public void onIspotShowText(Event aEvent) {
		Events.sendEvent("onIspotCancelRecording", this, null);
		Events.sendEvent("onIspotShowText", vView.getComponent("ispotViewers"), aEvent.getData());
	}

	public void onIspotShowPlayer(Event aEvent) {
		Events.sendEvent("onIspotCancelRecording", this, null);
		Events.sendEvent("onIspotShowPlayer", vView.getComponent("ispotViewers"), aEvent.getData());
	}

	public void onIspotShowWebcam(Event aEvent) {
		Events.sendEvent("onIspotCancelRecording", this, null);
		Events.sendEvent("onIspotShowWebcam", vView.getComponent("ispotViewers"), aEvent.getData());
	}

	public void onIspotShowFeedbackButtons(Event aEvent) {
		Events.sendEvent("onIspotCancelRecording", this, null);
		Events.sendEvent("onIspotShowFeedbackButtons", vView.getComponent("ispotViewers"), null);
	}

	public void onIspotShowButtons(Event aEvent) {
	}

	public void onIspotHideButton(Event aEvent) {
		Events.sendEvent("onIspotShowButtons", this, null);
		runIspot.setComponentVisible((Component)aEvent.getData(), false);
	}

}
