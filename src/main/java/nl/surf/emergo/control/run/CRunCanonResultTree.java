/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Treecol;
import org.zkoss.zul.Treecols;
import org.zkoss.zul.Treeitem;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefTreecol;
import nl.surf.emergo.control.def.CDefTreecols;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunCanonResultTree.
 */
public class CRunCanonResultTree extends CRunTree {

	private static final long serialVersionUID = 100928429080845074L;

	/**
	 * Instantiates a new c run Canon Result tree. The canon result uses the content of
	 * the canon component.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the canon result case component
	 * @param aRunComponent the a run component, the ZK canon result component
	 */
	public CRunCanonResultTree(String aId, IECaseComponent aCaseComponent, CRunComponent aRunComponent) {
		super(aId, aCaseComponent, aRunComponent);
		tagopenednames = "piece";
		setRows(23);
		IXMLTag lRootTag = CDesktopComponents.sSpring().getSReferencedDataTagHelper().determineReferencedRootTag(caseComponent, getRunStatusType(), "refcanon");
		update(lRootTag);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTree#appendTreecols(org.zkoss.zk.ui.Component)
	 */
	protected void appendTreecols(Component aParent) {
		Treecols lTreecols = new CDefTreecols();
		Treecol lTreecol = new CDefTreecol();
		lTreecols.appendChild(lTreecol);
		lTreecol = new CDefTreecol();
		lTreecols.appendChild(lTreecol);
		aParent.appendChild(lTreecols);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTree#getRunComponentHelper()
	 */
	@Override
	public CRunComponentHelper getRunComponentHelper() {
		return new CRunCanonResultHelper(this, tagopenednames, caseComponent, runComponent);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTree#setTreeitemStatus(org.zkoss.zul.Treeitem)
	 */
	public Treeitem setTreeitemStatus(Treeitem aTreeitem) {
		// do nothing till it is clear what to do.
		// should selected and opened be saved as private status or as shared status
		// and if shared it should be saved per rungroupaccount, like ratings
		return aTreeitem;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTree#doTreeitemAction(org.zkoss.zul.Treeitem)
	 */
	public void doTreeitemAction(Treeitem aTreeitem) {
//		if item clicked notify runCanonResult
		CRunCanonResult lComp = (CRunCanonResult) CDesktopComponents.vView().getComponent("runCanonResult");
		if (lComp != null) {
			lComp.doContentItemAction(aTreeitem);
		}
	}
}
