/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;

public class CRunEditform extends CDefDiv {

	private static final long serialVersionUID = -2999700508215911473L;

	public void onUpdate(Event aEvent) {
		//update own plugins
		//get all editform items of all forms
		List<Component> editformItems = CDesktopComponents.vView().getComponentsByPrefix("editformItem_");
		for (Component editformItem : editformItems) {
			//update only own editform items of type plugin
			if (editformItem.getAttribute("tag") != null &&
				((IXMLTag)editformItem.getAttribute("tag")).getParentTag() == (IXMLTag)getAttribute("formTag") &&
				((IXMLTag)editformItem.getAttribute("tag")).getName().equals("plugin")) {
				Events.postEvent("onUpdate", editformItem, null);
			}
		}
	}

}
