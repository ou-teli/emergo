/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zul.Div;
import org.zkoss.zul.Html;
import org.zkoss.zul.Label;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.def.CDefHtml;
import nl.surf.emergo.control.def.CDefLabel;
import nl.surf.emergo.view.VView;

public class CRunDescriptionDiv extends CDefDiv {

	private static final long serialVersionUID = 774556158896087620L;

	public void onCreate(CreateEvent aEvent) {
		VView vView = CDesktopComponents.vView();

		//NOTE get position relative to position of parent CRunConversations
		String left = (String)VView.getParameter("fragment_position_left", this, "0px");
		String top = (String)VView.getParameter("fragment_position_top", this, "0px");
		//NOTE fragment size may overwrite size of parent CRunConversations
		String desc_width = (String)VView.getParameter("fragment_size_width", this, (String)VView.getParameter("video_size_width", this, (String)getDesktop().getAttribute("runWndWidth")));
		String desc_height = (String)VView.getParameter("fragment_size_height", this, (String)VView.getParameter("video_size_height", this, (String)getDesktop().getAttribute("runWndHeight")));

		Div lDiv = new CDefDiv();
		appendChild(lDiv);
		lDiv.setZclass("CRunDescription");
		lDiv.setStyle("position:absolute;left:" + left + ";top:" + top + ";width:" + desc_width + ";height:" + desc_height + ";");
		
		Div lSubDiv = new CDefDiv();
		lDiv.appendChild(lSubDiv);
		lSubDiv.setZclass("CRunDescriptionText");
		
		Label lLabel = new CDefLabel();
		lSubDiv.appendChild(lLabel);
		lLabel.setZclass("CRunDescription_title");
		lLabel.setValue(vView.getCLabel(VView.childtagLabelKeyPrefix + "description"));
		
		lSubDiv = new CDefDiv();
		lDiv.appendChild(lSubDiv);
		lSubDiv.setZclass("CRunDescriptionText");
		
		Html lHtml = new CDefHtml();
		lSubDiv.appendChild(lHtml);
		lHtml.setZclass("CRunDescription_content");
		lHtml.setContent((String)VView.getParameter("description", this, ""));
	}

}