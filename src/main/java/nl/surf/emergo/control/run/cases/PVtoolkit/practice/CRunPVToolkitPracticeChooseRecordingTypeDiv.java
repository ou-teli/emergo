/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.practice;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Div;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitInitBox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.util.CRunPVToolkitCustomAttributeHelper;

public class CRunPVToolkitPracticeChooseRecordingTypeDiv extends CDefDiv {

	private static final long serialVersionUID = -5550529573833157356L;

	public String zulfilepath = ((CRunPVToolkitInitBox)CDesktopComponents.vView().getComponent("PV-toolkit_initBox")).getZulfilepath();
		
	protected String _idPrefix = "practice";
	protected String _classPrefix = "practiceChooseRecordingType";
	
	public void onCreate(CreateEvent aEvent) {
		_idPrefix = CRunPVToolkitCustomAttributeHelper.handleCustomAttribute(this, "_idPrefix", _idPrefix);
		_classPrefix = CRunPVToolkitCustomAttributeHelper.handleCustomAttribute(this, "_classPrefix", _classPrefix);
	}
	
	public void init() {
		
		setClass("popupDiv");
		
		getChildren().clear();
		
		new CRunPVToolkitDefImage(this, 
				new String[]{"class"}, 
				new Object[]{"popupBackground"}
		);

		Div popupDiv = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix}
		);

		new CRunPVToolkitDefImage(popupDiv, 
				new String[]{"class", "src"}, 
				new Object[]{_classPrefix + "Background", zulfilepath + "practice-choose-recording-type.svg"}
		);
		
		Component btn = new CRunPVToolkitDefButton(popupDiv, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font pvtoolkitButton " + _classPrefix + "WebcamButton", "PV-toolkit-practice.button.webcam"}
				);
		addPracticeOnClickEventListener(btn, "webcam");
		btn = new CRunPVToolkitDefButton(popupDiv, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font pvtoolkitButton " + _classPrefix + "UploadButton", "PV-toolkit-practice.button.upload"}
				);
		addPracticeOnClickEventListener(btn, "upload");
		
		setVisible(true);

	}
	
	protected void addPracticeOnClickEventListener(Component component, String recordingState) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				setVisible(false);
				CRunPVToolkitPracticeNewRecording newRecording = (CRunPVToolkitPracticeNewRecording)CDesktopComponents.vView().getComponent(_idPrefix + "NewRecording");
				if (newRecording != null) {
					newRecording.init(recordingState);
					newRecording.setVisible(true);
				}
			}
		});
	}

}
