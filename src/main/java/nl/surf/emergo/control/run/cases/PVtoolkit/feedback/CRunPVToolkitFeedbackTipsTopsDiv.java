/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.feedback;

import org.zkoss.zk.ui.Component;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitInitBox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTag;

public class CRunPVToolkitFeedbackTipsTopsDiv extends CRunPVToolkitDefDiv {
	
	private static final long serialVersionUID = -82212033815262911L;

	public String zulfilepath = ((CRunPVToolkitInitBox)CDesktopComponents.vView().getComponent("PV-toolkit_initBox")).getZulfilepath();
	
	protected CRunPVToolkitCacAndTag _runPVToolkitCacAndTag;
	protected int _level;
	protected String _feedbackRubricId;
	protected String _idPrefix = "";

	private CRunPVToolkitFeedbackRubricDiv _feedbackRubric;

	public CRunPVToolkitFeedbackTipsTopsDiv(Component parent, String[] keys, Object[] values) {
		super(parent, keys, values);
	}
	
	public void init(CRunPVToolkitCacAndTag runPVToolkitCacAndTag, int level, String feedbackRubricId, String idPrefix) {
		_runPVToolkitCacAndTag = runPVToolkitCacAndTag;
		_feedbackRubricId = feedbackRubricId;

		_feedbackRubric = (CRunPVToolkitFeedbackRubricDiv)CDesktopComponents.vView().getComponent(_feedbackRubricId);
		
		_idPrefix = idPrefix;

		update(level);
	}
	
	public void update() {
		update(_level);
	}
	
	public void update(int level) {
		_level = level;

		getChildren().clear();

		boolean hasTipsOrTops = !(_feedbackRubric.getTips(_runPVToolkitCacAndTag.getXmlTag()).equals("") && _feedbackRubric.getTops(_runPVToolkitCacAndTag.getXmlTag()).equals(""));
		boolean inOverview = _feedbackRubric.inOverview();
		String check = hasTipsOrTops ? "-check" : "";
		String blue = (inOverview || _level <= 0) ? "-blue" : "";
		String src = zulfilepath + "comment-alt" + check + blue + ".svg";
		new CRunPVToolkitDefImage(this, 
				new String[]{"class", "src"}, 
				new Object[]{"feedbackSubSkillTipsTops", src}
		);

	}

	public void onClick() {
		CRunPVToolkitFeedbackScoreLevelTipsTopsDiv feedbackScoreLevelTipsTops = (CRunPVToolkitFeedbackScoreLevelTipsTopsDiv)CDesktopComponents.vView().getComponent(_idPrefix + "ScoreLevelTipsTopsDiv");
		if (feedbackScoreLevelTipsTops != null) {
			feedbackScoreLevelTipsTops.setAttribute("skillclusterCounter", getAttribute("skillclusterCounter"));
			feedbackScoreLevelTipsTops.setAttribute("subskillCounter", getAttribute("subskillCounter"));
			feedbackScoreLevelTipsTops.init(_runPVToolkitCacAndTag, _level, _feedbackRubricId);
			feedbackScoreLevelTipsTops.setVisible(true);
		}
	}

}
