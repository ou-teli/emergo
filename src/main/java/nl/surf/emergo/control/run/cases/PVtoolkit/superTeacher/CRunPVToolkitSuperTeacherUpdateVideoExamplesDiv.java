/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.superTeacher;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Button;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitVideoExamplesDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.practice.CRunPVToolkitPracticeChooseRecordingTypeDiv;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpring;

public class CRunPVToolkitSuperTeacherUpdateVideoExamplesDiv extends CRunPVToolkitVideoExamplesDiv {

	private static final long serialVersionUID = 2963582583669612168L;

	protected VView vView = CDesktopComponents.vView();
	protected SSpring sSpring = CDesktopComponents.sSpring();

	protected StringBuffer examples;

	@Override
	public void showInput(Component parent, List<IXMLTag> exampleTags) {
		Button btn = new CRunPVToolkitDefButton(parent, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font pvtoolkitButton updateRubricRubricVideoExamplesNewRecordingButton", "PV-toolkit-superTeacher.updaterubric.button.newExample"}
		);
		addNewRecordingButtonOnClickEventListener(btn);
		
		initExamples();
	}

	public void initExamples() {
		initExamples(_runPVToolkitCacAndTag.getXmlTag());
		Clients.evalJavaScript("initExamples('" + examples.toString() + "');");
	}

	protected void addNewRecordingButtonOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				CRunPVToolkitPracticeChooseRecordingTypeDiv practiceChooseRecordingType = (CRunPVToolkitPracticeChooseRecordingTypeDiv)CDesktopComponents.vView().getComponent(_idPrefix + "ChooseRecordingTypeDiv");
				if (practiceChooseRecordingType != null) {
					Clients.evalJavaScript("pauseVideo('" + _idPrefix + "VideoVideo" + "');");
					practiceChooseRecordingType.init();
				}
			}
		});
	}

	protected void initExamples(IXMLTag parentTag) {
		String childTagName = "";
		if (_performanceExamples) {
			childTagName = "videoperformancelevelexample";
		}
		else {
			childTagName = "videoskillexample";
		}
		examples = new StringBuffer();
		for (IXMLTag exampleTag : parentTag.getChilds(childTagName)) {
			addExample(exampleTag);
		}
		examples.insert(0, "[");
		examples.append("]");
	}
		
	protected void addExample(IXMLTag nodeTag) {
		addExample(nodeTag.getParentTag(), nodeTag);
	}

	protected void addExample(IXMLTag parentTag, IXMLTag nodeTag) {
		StringBuffer element = new StringBuffer();
		addElementPart(element, "parentTagId", parentTag.getAttribute(AppConstants.keyId), true);
		addElementPart(element, "parentTagName", parentTag.getName(), true);
		addElementPart(element, "tagId", nodeTag.getAttribute(AppConstants.keyId), true);
		addElementPart(element, "tagName", nodeTag.getName(), true);
		addElementPart(element, "name", sSpring.unescapeXML(nodeTag.getChildValue("name")), true);
		addElementPart(element, "description", sSpring.unescapeXML(nodeTag.getChildValue("description")).replace("\n", ""), false);
		if (examples.length() > 0) {
			examples.append(",");
		}
		examples.append("{");
		examples.append(element);
		examples.append("}");
	}

	protected void addElementPart(StringBuffer element, String key, String value, boolean separator) {
		element.append("\"");
		element.append(key);
		element.append("\":\"");
		element.append(value);
		element.append("\"");
		if (separator) {
			element.append(",");
		}
	}

	public void onNotify (Event event) {
		JSONObject jsonObject = (JSONObject)event.getData();
		String action = (String)jsonObject.get("action");
		CRunPVToolkitSuperTeacherDiv superTeacherDiv = (CRunPVToolkitSuperTeacherDiv)vView.getComponent("superTeacherUpdateRubricDiv");
		if (superTeacherDiv == null) {
			return;
		}
		String elementType = "rubric";
		if (action.equals("new_element")) {
			String blobId = (String)jsonObject.get("blobId");
			String name = (String)jsonObject.get("name");
			IXMLTag parentTag = sSpring.getTag(_runPVToolkitCacAndTag.getCaseComponent(), _runPVToolkitCacAndTag.getXmlTag().getAttribute(AppConstants.keyId));
			Map<String,String> childTagNameValueMap = new HashMap<String,String>();
			childTagNameValueMap.put("pid", name);
			childTagNameValueMap.put("name", name);
			childTagNameValueMap.put("blob", blobId);
			String childTagName = "";
			if (_performanceExamples) {
				childTagName = "videoperformancelevelexample";
			}
			else {
				childTagName = "videoskillexample";
			}
			superTeacherDiv.addElementTag(parentTag.getAttribute(AppConstants.keyId), elementType, childTagName, childTagNameValueMap);
			initExamples();
		}
		else if (action.equals("select_element")) {
			String tagId = (String)jsonObject.get("tagId");
			IXMLTag elementTag = sSpring.getTag(_runPVToolkitCacAndTag.getCaseComponent(), tagId);
			if (elementTag != null) {
				showVideo(elementTag);
			}
		}
		else if (action.equals("edit_element")) {
			String tagId = (String)jsonObject.get("tagId");
			String elementName = (String)jsonObject.get("elementName");
			IXMLTag elementTag = sSpring.getTag(_runPVToolkitCacAndTag.getCaseComponent(), tagId);
			if (elementTag != null) {
				superTeacherDiv.editElementTag(elementType, elementTag, elementName);
			}
		}
		else if (action.equals("delete_element")) {
			String tagId = (String)jsonObject.get("tagId");
			IXMLTag elementTag = sSpring.getTag(_runPVToolkitCacAndTag.getCaseComponent(), tagId);
			if (elementTag != null) {
				superTeacherDiv.deleteElementTag(elementType, elementTag);
				pvToolkit.clearVideo(_idPrefix);
			}
		}
	}
	
}