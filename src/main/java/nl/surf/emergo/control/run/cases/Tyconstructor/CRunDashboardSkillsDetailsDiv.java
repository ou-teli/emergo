/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.Tyconstructor;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Div;
import org.zkoss.zul.Vbox;

import nl.surf.emergo.control.def.CDefDiv;

public class CRunDashboardSkillsDetailsDiv extends CDefDiv {

	private static final long serialVersionUID = -9214412511631123035L;
	
	private Map<String,Object> propertyMap = null;

	private Div detailsPerCountryDiv = null;
	
	private List<String> skill_labels = null;

	private List<String> country_labels = null;
	
	private double[][] weights_per_country_per_skill = null;
	
	private double[][] weights_per_country_per_level = null;
	
	private double[][][] score_per_country_per_level_per_skill = null;
	
	private List<Div> tabDivs = new ArrayList<Div>();
	
	public void onCreate(CreateEvent aEvent) {
		propertyMap = (Map<String,Object>)aEvent.getArg().get("a_propertyMap");
		skill_labels = (List<String>)propertyMap.get("skill_labels");
		country_labels = (List<String>)propertyMap.get("country_labels");
		weights_per_country_per_skill = (double[][])propertyMap.get("weights_per_country_per_skill");
		weights_per_country_per_level = (double[][])propertyMap.get("weights_per_country_per_level");
		score_per_country_per_level_per_skill = (double[][][])propertyMap.get("score_per_country_per_level_per_skill");

		int initialCountryNumber = 0;
		
		//there are five countries, so five tabs
		for (int i=0;i<5;i++) {
			//country 0 is the default country
			String color = "gray";
			if (i == initialCountryNumber) {
				color = "yellow";
			}
			Div div = new CRunDashboardDetailsTabDiv();
			appendChild(div);
			div.setClass("details_tab details_tab_" + color + " details_tab_" + i);
			div.setAttribute("countryNumber", i);
			CRunDashboardInitBox.appendLabel(div, "country_label", null, country_labels.get(i));

			tabDivs.add(div);
		}
		
		//show initial details
		detailsPerCountryDiv = CRunDashboardInitBox.appendDiv(this, "details_rectangle", null, true);
		showDetailsPerCountry(initialCountryNumber);

		//close details button
		Div div2 = new CRunDashboardCloseDetailsDiv();
		appendChild(div2);
		div2.setClass("close_details");
	} 
	
	public void onShowDetailsPerCountry(Event aEvent) {
		showDetailsPerCountry((int)aEvent.getData());
	}
	
	public void showDetailsPerCountry(int countryNumber) {
		//update tabs
		for (int i=0;i<5;i++) {
			String color = "gray";
			if (i == countryNumber) {
				color = "yellow";
			}
			tabDivs.get(i).setClass("details_tab details_tab_" + color + " details_tab_" + i);
		}

		//set level panels for country
		detailsPerCountryDiv.getChildren().clear();
		
		DecimalFormat decimalFormat = new DecimalFormat("#.#");
		   
		//loop through levels
		for (int levelNumber=0;levelNumber<score_per_country_per_level_per_skill[countryNumber].length;levelNumber++) {
			Div div1 = CRunDashboardInitBox.appendDiv(detailsPerCountryDiv, "details_level details_level_" + levelNumber, null, true);
			Vbox vbox1 = CRunDashboardInitBox.appendVbox(div1);
			Div div2 = CRunDashboardInitBox.appendDiv(vbox1, "level_div", null, true);
			Vbox vbox2 = CRunDashboardInitBox.appendVbox(div2);
			CRunDashboardInitBox.appendLabel(vbox2, "country_label level_country_label", null, country_labels.get(countryNumber));
			CRunDashboardInitBox.appendLabel(vbox2, "level_label", null, ((String)propertyMap.get("level_label")) + " " + (levelNumber + 1));
			//loop through skills
			for (int skillNumber=0;skillNumber<score_per_country_per_level_per_skill[countryNumber][levelNumber].length;skillNumber++) {
				div2 = CRunDashboardInitBox.appendDiv(vbox1, "level_div", null, true);
				vbox2 = CRunDashboardInitBox.appendVbox(div2);
				CRunDashboardInitBox.appendLabel(vbox2, "skill_label level_skill_label", null, skill_labels.get(skillNumber));
				double weightPerCountryPerSkill = weights_per_country_per_skill[countryNumber][skillNumber];
				if (weightPerCountryPerSkill == 0) {
					CRunDashboardInitBox.appendLabel(vbox2, "level_label", null, (String)propertyMap.get("not_applicable_label"));
				}
				else {
					double weightPerCountryPerLevel = weights_per_country_per_level[countryNumber][levelNumber];
					double score = score_per_country_per_level_per_skill[countryNumber][levelNumber][skillNumber];
					//NOTE score is -1 if number
					score = Math.max(score,  0);
					String percentageScore = decimalFormat.format(score * weightPerCountryPerSkill * 100);
					String percentageMaximum = decimalFormat.format(weightPerCountryPerLevel * weightPerCountryPerSkill * 100);
					String scoreLabel = ((String)propertyMap.get("score_label")).replace("%1", percentageScore).replace("%2", percentageMaximum);
					CRunDashboardInitBox.appendLabel(vbox2, "level_label", null, scoreLabel);
				}
			}
		}
	}
	
}
