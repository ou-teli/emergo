/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.IP2;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Label;

import nl.surf.emergo.control.def.CDefDiv;

public class CRunSatisfactionBoxDiv extends CDefDiv {

	private static final long serialVersionUID = 8328102730042765609L;

	public void onInit(Event aEvent) {
		// aEvent.getData()[0] : type of satisfaction
		// aEvent.getData()[1] : satisfaction question
		// aEvent.getData()[2] : satisfaction label left
		// aEvent.getData()[3] : satisfaction label right
		// aEvent.getData()[4] : satisfaction
		setClass("satisfaction" + (String)((Object[])aEvent.getData())[0]);
		((Label)getChildren().get(0).getChildren().get(0)).setValue((String)((Object[])aEvent.getData())[1]);
		((Label)getChildren().get(0).getChildren().get(1).getChildren().get(0).getChildren().get(0)).setValue((String)((Object[])aEvent.getData())[2]);
		((Label)getChildren().get(0).getChildren().get(1).getChildren().get(2).getChildren().get(0)).setValue((String)((Object[])aEvent.getData())[3]);
		Component component = (Component)getChildren().get(0).getChildren().get(1).getChildren().get(1).getChildren().get(0).clone();
		getChildren().get(0).getChildren().get(1).getChildren().get(1).insertBefore(component, null);
		Events.postEvent("onInit", component, new Object[]{((Object[])aEvent.getData())[0], ((Object[])aEvent.getData())[4]});
		setVisible(true);
	}

}
