/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.Tyconstructor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Vbox;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefBox;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.def.CDefHbox;
import nl.surf.emergo.control.def.CDefLabel;
import nl.surf.emergo.control.def.CDefVbox;
import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.zkspring.SSpring;

public class CRunDashboardInitBox extends CDefBox {

	private static final long serialVersionUID = 5396116213038809684L;
	
	/*		

	Competence x Case lead	1 (taking initiative)	2 (learning experience)	3 (mobilising others)	4 (eth.&sust. thinking)	5 (motivat. & perseverance)	Total
	1 (IT)					0.70					0.10					0.00					0.10					0.10						1
	2 (SLO)					0.10					0.60					0.10					0.10					0.10						1
	3 (UK)					0.10					0.00					0.80					0.10					0.00						1
	4 (SP)					0.10					0.20					0.00					0.70					0.00						1
	5 (GE)					0.05					0.05					0.05					0.15					0.70						1
	Total					1.05					0.95					0.95					1.15					0.90						5

	Country					Weight level 1			Weight level 2			Weight level 3
	1 (IT)					0.25					0.25					0.50
	2 (SLO)					1.00					-						-
	3 (UK)					0.25					0.25					0.50
	4 (SP)					0.50					0.50					-
	5 (GE)					0.25					0.25					0.50
	 
	Country					Level					Minimum score			Maximum score
	IT						1						4						1
	IT						2						0						8
	IT						3						-75						105
	SLO						1						1						10
	UK						1						0						10
	UK						2						0						11
	UK						3						0						5						
	SP						1						-16						72
	SP						2						0						30
	GE						1						0						20
	GE						2						0						60
	GE						3						0						60
			
	*/

	final static double[][] weightsPerCountryPerSkill = new double[][]
			{
				{0.70, 0.10, 0.00, 0.10, 0.10},
				{0.10, 0.60, 0.10, 0.10, 0.10},
				{0.10, 0.00, 0.80, 0.10, 0.00},
				{0.10, 0.20, 0.00, 0.70, 0.00},
				{0.05, 0.05, 0.05, 0.15, 0.70}
			};
	
	final static double[][] weightsPerCountryPerLevel = new double[][]
			{
				{0.25, 0.25, 0.50},
				{1.00},
				{0.25, 0.25, 0.50},
				{0.50, 0.50},
				{0.25, 0.25, 0.50}
			};
				
	final static double[][][] minimumAndMaximumScoresPerCountryPerLevel = new double[][][]
			{
				{
					{4, 1},
					{0, 8},
					{-75, 105}
				},
				{
					{1, 10}
				},
				{
					{0, 10},
					{0, 11},
					{0, 5}
				},
				{
					{-16, 72},
					{0, 30}
				},
				{
					{0, 20},
					{0, 60},
					{0, 60}
				}
			};
			
	final static String[][] cacNamesPerCountryPerLevel = new String[][]
			{
				{"M_A1_states", "M_A2_states", "M_A3_states"},
				{"M_B1_states"},
				{"M_C1_states", "M_C2_states", "M_C3_states"},
				{"M_D1_states", "M_D2_states"},
				{"M_E1_states", "M_E2_states", "M_E3_states"}
			};

	final static String[][] tagKeysPerCountryPerLevel = new String[][]
			{
				{"A1_score", "A2_score", "A3_score"},
				{"B1_score"},
				{"C1_score", "C2_score", "C3_score"},
				{"D1_score", "D2_score"},
				{"E1_score", "E2_score", "E3_score"}
			};
			
	final static int maxSliderWidth = 344;

	private SSpring sSpring = CDesktopComponents.sSpring();

	private List<String> slider_style_per_skill = new ArrayList<String>();

	private double[][][] score_per_country_per_level_per_skill = new double[][][]
	{
		{
			{-1, -1, -1, -1, -1},
			{-1, -1, -1, -1, -1},
			{-1, -1, -1, -1, -1}
		},
		{
			{-1, -1, -1, -1, -1}
		},
		{
			{-1, -1, -1, -1, -1},
			{-1, -1, -1, -1, -1},
			{-1, -1, -1, -1, -1}
		},
		{
			{-1, -1, -1, -1, -1},
			{-1, -1, -1, -1, -1}
		},
		{
			{-1, -1, -1, -1, -1},
			{-1, -1, -1, -1, -1},
			{-1, -1, -1, -1, -1}
		}
	};

	public void onCreate() {
		//NOTE use echoEvent, otherwise macro parent does not yet exist, if code is used within zscript
	  	Events.echoEvent("onInit", this, null);
	}
	
	public void onInit() {
		Map<String,Object> propertyMap = new HashMap<String,Object>();
		propertyMap.put("casecomponent", ((HtmlMacroComponent)CRunComponent.getMacroParent(this)).getDynamicProperty("a_casecomponent"));
		propertyMap.put("tag", ((HtmlMacroComponent)CRunComponent.getMacroParent(this)).getDynamicProperty("a_tag"));

		//NOTE label keys are equal to required pids of node tags
		String[] labelKeys = new String[] {
				"header_label",
				"left_label",
				"button_details_label",
				"level_label",
				"score_label",
				"not_applicable_label"
			};
		String skillLabelsKey = "skill_labels";
		String[] skillLabelKeys = new String[] {
				"skill_label_1",
				"skill_label_2",
				"skill_label_3",
				"skill_label_4",
				"skill_label_5"
			};
		String countryLabelsKey = "country_labels";
		String[] countryLabelKeys = new String[] {
				"country_label_1",
				"country_label_2",
				"country_label_3",
				"country_label_4",
				"country_label_5"
			};
		IECaseComponent caseComponent = CDesktopComponents.sSpring().getCaseComponent(CDesktopComponents.sSpring().getCase(), "", "game_navigation");
		if (caseComponent != null) {
			List<IXMLTag> nodeTags = CDesktopComponents.cScript().getRunGroupNodeTags(caseComponent, "piece");
			fillPropertyMapWithLabels(nodeTags, labelKeys, propertyMap);
			propertyMap.put(skillLabelsKey, getLabelList(nodeTags, skillLabelKeys));
			propertyMap.put(countryLabelsKey, getLabelList(nodeTags, countryLabelKeys));
		}

		propertyMap.put("weights_per_country_per_skill", weightsPerCountryPerSkill);

		propertyMap.put("weights_per_country_per_level", weightsPerCountryPerLevel);
		
		//There are five skills
		for (int i=1;i<=5;i++) {
			fillDashboardData(i);
		}
		propertyMap.put("slider_style_per_skill", slider_style_per_skill);
		propertyMap.put("score_per_country_per_level_per_skill", score_per_country_per_level_per_skill);
				
		String zulfilepath = (String)((HtmlMacroComponent)CRunComponent.getMacroParent(this)).getDynamicProperty("a_zulfilepath");
		propertyMap.put("zulfilepath", zulfilepath);
	
		HtmlMacroComponent macro = new HtmlMacroComponent();
		appendChild(macro);
		macro.setDynamicProperty("a_propertyMap", propertyMap);
		macro.setMacroURI(zulfilepath + "run_dashboard_view_macro.zul");
	}
	
	protected void fillPropertyMapWithLabels(List<IXMLTag> nodeTags, String[] labelKeys, Map<String,Object> labelPropertyMap) {
		List<String> labelValues  = getLabelValues(nodeTags, labelKeys);
		for (int i=0;i<labelKeys.length;i++) {
			labelPropertyMap.put(labelKeys[i], labelValues.get(i));
		}
	}
	
	protected List<String> getLabelList(List<IXMLTag> nodeTags, String[] labelKeys) {
		List<String> labelList = new ArrayList<String>();
		List<String> labelValues  = getLabelValues(nodeTags, labelKeys);
		for (int i=0;i<labelKeys.length;i++) {
			labelList.add(labelValues.get(i));
		}
		return labelList;
	}
	
	protected List<String> getLabelValues(List<IXMLTag> nodeTags, String[] labelKeys) {
		List<String> labelValues = new ArrayList<String>();
		for (int i=0;i<labelKeys.length;i++) {
			labelValues.add(getLabelValue(nodeTags, labelKeys[i]));
		}
		return labelValues;
	}
	
	protected String getLabelValue(List<IXMLTag> nodeTags, String labelKey) {
		String labelValue = "";
		for (IXMLTag nodeTag : nodeTags) {
			String nodeTagPid = nodeTag.getChildValue("pid");
			//NOTE label key is equal to 'dashboard_' + pid of node tag
			if (nodeTagPid.equals("dashboard_" + labelKey)) {
				return CDesktopComponents.sSpring().unescapeXML(nodeTag.getChildValue("name"));
			}
		}
		return labelValue;
	}
	
	protected void fillDashboardData(int skillNumber) {
		if (skillNumber < 1 || skillNumber > weightsPerCountryPerSkill[0].length) {
			return;
		}
		
		//will be value within 0 and 1
		double skillScore = 0;
		
		//to store skill weights per country
		double[] skillWeightsPerCountry = new double[weightsPerCountryPerSkill.length];
		//to determine the sum of skill weights per country, because it not always will be 1.00
		double sumOfSkillWeightsPerCountry = 0;
		//loop through weights per country per skill ...
		for (int i=0;i<weightsPerCountryPerSkill.length;i++) {
			//... to get the weight for skill given by skill number and ...
			skillWeightsPerCountry[i] = weightsPerCountryPerSkill[i][skillNumber - 1];
			//... to add this weight to the sum of skill weights per country
			sumOfSkillWeightsPerCountry += skillWeightsPerCountry[i];
		}
		//loop through weights per country per level
		for (int i=0;i<weightsPerCountryPerLevel.length;i++) {
			//loop through weights per level
			for (int j=0;j<weightsPerCountryPerLevel[i].length;j++) {
				double levelWeight = weightsPerCountryPerLevel[i][j];
				//now get level score
				String statusValue = getCurrentRunTagStatus(cacNamesPerCountryPerLevel[i][j], "state", tagKeysPerCountryPerLevel[i][j], "value", AppConstants.statusTypeRunGroup);
				if (statusValue.length() > 0) {
					double levelValue = Double.parseDouble(statusValue);
					double minLevelValue = minimumAndMaximumScoresPerCountryPerLevel[i][j][0];
					double maxLevelValue = minimumAndMaximumScoresPerCountryPerLevel[i][j][1];
					//NOTE level score will be value between 0 and 1
					double levelScore = (levelValue - minLevelValue) / (maxLevelValue - minLevelValue);
					//NOTE correct score for sum of skill weights per country not always being 1.00
					levelScore = (skillWeightsPerCountry[i] * levelWeight * levelScore) / sumOfSkillWeightsPerCountry;
					score_per_country_per_level_per_skill[i][j][skillNumber - 1] = levelScore;
					skillScore += levelScore;
				}
			}
		}

		//NOTE due to rounding errors, the score may be slightly less than 0 or slightly more than 1
		skillScore = Math.max(0, skillScore);
		skillScore = Math.min(1, skillScore);
		
		long sliderWidth = Math.round(skillScore * maxSliderWidth);
		
		slider_style_per_skill.add("width:" + sliderWidth + "px;");
	}

	public String getCurrentRunTagStatus(String aCaseComponentName, String aTagName, String aTagKey, String aStatusKey, String aStatusType) {
		IECaseComponent lCaseComponent = sSpring.getCaseComponent(sSpring.getCase(), "", aCaseComponentName);
		if (lCaseComponent == null) {
			return "";
		}
		IXMLTag lRootTag = sSpring.getXmlDataPlusRunStatusTree(lCaseComponent, aStatusType);
		if (lRootTag == null) {
			return "";
		}
		IXMLTag lTag = getRunTagByNameAndKey(lRootTag, aTagName, aTagKey);
		//NOTE if no status tag yet, the value is never set, so return empty string. Otherwise default value is returned which is 0
		if (lTag == null || lTag.getStatusTag() == null) {
			return "";
		}
		return CDesktopComponents.sSpring().getCurrentTagStatus(lTag, aStatusKey);
	}

	protected IXMLTag getRunTagByNameAndKey(IXMLTag aRootTag, String aTagName, String aTagKey) {
		List<IXMLTag> lNodeTags = CDesktopComponents.cScript().getNodeTags(aRootTag);
		for (IXMLTag lNodeTag : lNodeTags) {
			if (lNodeTag.getName().equals(aTagName) && lNodeTag.getChildValue(lNodeTag.getDefAttribute(AppConstants.defKeyKey)).equals(aTagKey)) {
				return lNodeTag;
			}
		}
		return null;
	}
	
	public static Div appendDiv(Component parent, String styleClass, String style, boolean visible) {
		Div div = new CDefDiv();
		parent.appendChild(div);
		if (styleClass != null) div.setClass(styleClass);
		if (style != null) div.setStyle(style);
		div.setVisible(visible);
		return div;
	}
	
	public static Hbox appendHbox(Component parent) {
		Hbox box = new CDefHbox();
		parent.appendChild(box);
		box.setSpacing("0");
		return box;
	}
	
	public static Vbox appendVbox(Component parent) {
		Vbox box = new CDefVbox();
		parent.appendChild(box);
		box.setSpacing("0");
		return box;
	}
	
	public static Label appendLabel(Component parent, String styleClass, String style, String labelValue) {
		Label label = new CDefLabel();
		parent.appendChild(label);
		if (styleClass != null) label.setClass(styleClass);
		if (style != null) label.setStyle(style);
		if (labelValue != null) label.setValue(labelValue);
		return label;
	}
	
}
