/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.IP2;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Html;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefVbox;

public class CRunKP4AfsluitendeBriefVbox extends CDefVbox {

	private static final long serialVersionUID = -7520761216938089344L;
	
	protected CRunKP4AfsluitendeBriefDiv letterDiv = null;

	public void onUpdate(Event aEvent) {
		letterDiv = (CRunKP4AfsluitendeBriefDiv)CDesktopComponents.vView().getComponent("letterDiv");
		if (letterDiv == null) {
			return;
		}
		getChildren().clear();
		Events.postEvent("onCreateTitle", this, null);
		for (int i=0;i<letterDiv.headers.size();i++) {
			Events.postEvent("onCreateHeader", this, i);
			Events.postEvent("onCreateSection", this, i);
		}
	}

	public void onCreateTitle(Event aEvent) {
		Html html = new Html("Afsluitende brief");
		html.setStyle("color:blue;font-size:14pt;");
		appendChild(html);
	}

	public void onCreateHeader(Event aEvent) {
		Html html = new Html(letterDiv.headers.get((int)aEvent.getData()));
		html.setStyle("font-size:13pt;");
		appendChild(html);
	}

	public void onCreateSection(Event aEvent) {
		Html html = new Html(letterDiv.sections.get((int)aEvent.getData()).replace("\n", "<br/>"));
		html.setStyle("position:relative;left:10px;top:-4px;");
		appendChild(html);
	}

}
