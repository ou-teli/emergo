/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.feedback;

import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vbox;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitCloseImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitInitBox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefHtml;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefTextbox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefVbox;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTag;
import nl.surf.emergo.control.run.cases.PVtoolkit.util.CRunPVToolkitCustomAttributeHelper;

public class CRunPVToolkitFeedbackScoreLevelTipsTopsDiv extends CDefDiv {

	private static final long serialVersionUID = 6063031918033192733L;

	public String zulfilepath = ((CRunPVToolkitInitBox)CDesktopComponents.vView().getComponent("PV-toolkit_initBox")).getZulfilepath();
		
	protected CRunPVToolkitCacAndTag _runPVToolkitCacAndTag;
	protected int _level;
	protected String _feedbackRubricId;
	
	protected CRunPVToolkitFeedbackRubricDiv _feedbackRubric;

	protected String _idPrefix = "feedback";
	protected String _classPrefix = "feedbackScoreLevelTipsTops";
	
	public void onCreate(CreateEvent aEvent) {
		_idPrefix = CRunPVToolkitCustomAttributeHelper.handleCustomAttribute(this, "_idPrefix", _idPrefix);
	}
	
	public void init(CRunPVToolkitCacAndTag runPVToolkitCacAndTag, int level, String feedbackRubricId) {
		_runPVToolkitCacAndTag = runPVToolkitCacAndTag;
		_level = level;
		_feedbackRubricId = feedbackRubricId;
		_feedbackRubric = (CRunPVToolkitFeedbackRubricDiv)CDesktopComponents.vView().getComponent(_feedbackRubricId);
		
		setClass("popupDiv");

		update();
	}
	
	public void update() {
		getChildren().clear();
		
		if (_runPVToolkitCacAndTag == null) {
			return;
		}
		
		if (_feedbackRubric == null) {
			return;
		}

		Div popupDiv = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix}
		);

		new CRunPVToolkitDefImage(popupDiv, 
				new String[]{"class", "src"}, 
				new Object[]{_classPrefix + "Background", zulfilepath + "score-level-tips-tops-background.svg"}
		);

		Div div = new CRunPVToolkitDefDiv(popupDiv, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "Title"}
		);

		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "cacAndTag", "tagChildName"}, 
				new Object[]{"font " + _classPrefix + "Title", _runPVToolkitCacAndTag, "name"}
		);

		List<IXMLTag> subSkillTags = _runPVToolkitCacAndTag.getXmlTag().getChilds("subskill");
		if (subSkillTags.size() > 0) {
			String value = "";
			CRunPVToolkit pvToolkit = (CRunPVToolkit)CDesktopComponents.vView().getComponent("pvToolkit");
			for (IXMLTag subSkillTag : subSkillTags) {
				if (!value.equals("")) {
					value += "<br/>";
				}
				value += pvToolkit.getTagChildValue(new CRunPVToolkitCacAndTag(_runPVToolkitCacAndTag.getCaseComponent(), subSkillTag), "name");
			}
			div = new CRunPVToolkitDefDiv(popupDiv, 
					new String[]{"class"}, 
					new Object[]{_classPrefix + "SubSkills"}
			);
	
			new CRunPVToolkitDefHtml(div, 
					new String[]{"class", "value"}, 
					new Object[]{"font " + _classPrefix + "SubSkills", value}
			);
		}

		CRunPVToolkitCloseImage closeImage = new CRunPVToolkitCloseImage(popupDiv, 
				new String[]{"class", "src"}, 
				new Object[]{_classPrefix + "CloseImage", zulfilepath + "close.svg"}
		);
		closeImage.init(_idPrefix + "ScoreLevelTipsTopsDiv");

		if (_feedbackRubric != null && _feedbackRubric.feedbackIsEditable()) {
			Button saveButton = new CRunPVToolkitDefButton(popupDiv,
					new String[]{"class", "cLabelKey"}, 
					new Object[]{"font pvtoolkitButton " + _classPrefix + "SaveButton", "PV-toolkit.save"}
					);
			addScoreLevelTipsTopsSaveButtonOnClickEventListener(saveButton, this);
		}
		
		div = new CRunPVToolkitDefDiv(popupDiv, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "Div"}
		);

		Vbox vbox = new CRunPVToolkitDefVbox(div, null, null);
		

		div = new CRunPVToolkitDefDiv(vbox, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "LabelTops"}
		);

		new CRunPVToolkitDefImage(div, 
				new String[]{"class", "src"}, 
				new Object[]{_classPrefix + "LabelTops", zulfilepath + "thumbs-up-gray.svg"}
		);

		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font " + _classPrefix + "LabelTops", "PV-toolkit-feedback.header.tops"}
		);
		
		Textbox tbox = new CRunPVToolkitDefTextbox(vbox, 
				new String[]{"id", "class", "rows", "width", "text"},
				new Object[]{_idPrefix + "ScoreLevelTipsTopsTextboxTops", "font " + _classPrefix + "TextboxTops", "6", "450px", _feedbackRubric.getTops(_runPVToolkitCacAndTag.getXmlTag())}
		);
		tbox.setReadonly((_feedbackRubric == null) || !_feedbackRubric.feedbackIsEditable());


		div = new CRunPVToolkitDefDiv(vbox, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "LabelTips"}
		);

		new CRunPVToolkitDefImage(div, 
				new String[]{"class", "src"}, 
				new Object[]{_classPrefix + "LabelTips", zulfilepath + "map-marker-exclamation-gray.svg"}
		);

		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font " + _classPrefix + "LabelTips", "PV-toolkit-feedback.header.tips"}
		);

		tbox = new CRunPVToolkitDefTextbox(vbox, 
				new String[]{"id", "class", "rows", "width", "text"},
				new Object[]{_idPrefix + "ScoreLevelTipsTopsTextboxTips", "font " + _classPrefix + "TextboxTips", "6", "450px", _feedbackRubric.getTips(_runPVToolkitCacAndTag.getXmlTag())}
		);
		tbox.setReadonly((_feedbackRubric == null) || !_feedbackRubric.feedbackIsEditable());

	}
	
	protected void addScoreLevelTipsTopsSaveButtonOnClickEventListener(Component component, Component notifyComponent) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				((HtmlBasedComponent)component).focus();
			  	Events.echoEvent("onSave", notifyComponent, null);
			}
		});
	}

	public void onSave(Event event) {
		saveTipsAndTops();
		Component feedbackScoreLevelTipsTops = CDesktopComponents.vView().getComponent(_idPrefix + "ScoreLevelTipsTopsDiv");
		if (feedbackScoreLevelTipsTops != null) {
			feedbackScoreLevelTipsTops.setVisible(false);
		}
	}

	public void saveTipsAndTops() {
		_feedbackRubric.saveTips(_runPVToolkitCacAndTag.getXmlTag(), ((Textbox)CDesktopComponents.vView().getComponent(_idPrefix + "ScoreLevelTipsTopsTextboxTips")).getValue());
		_feedbackRubric.saveTops(_runPVToolkitCacAndTag.getXmlTag(), ((Textbox)CDesktopComponents.vView().getComponent(_idPrefix + "ScoreLevelTipsTopsTextboxTops")).getValue());
	}

}
