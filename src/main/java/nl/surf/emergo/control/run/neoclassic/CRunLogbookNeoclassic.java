/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.neoclassic;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Label;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CContentHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefLabel;
import nl.surf.emergo.control.def.CDefTextbox;
import nl.surf.emergo.control.run.CRunLogbook;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunLogbookNeoclassic is used to show the logbook within the run view area of the
 * Emergo player.
 */
public class CRunLogbookNeoclassic extends CRunLogbook {

	private static final long serialVersionUID = -8135625135322426222L;

	/**
	 * Instantiates a new c run logbook.
	 */
	public CRunLogbookNeoclassic() {
		super();
	}

	/**
	 * Instantiates a new c run logbook.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the logbook case component
	 */
	public CRunLogbookNeoclassic(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
	}

	/**
	 * Creates new content component, the logbook tree.
	 * 
	 * @return the component
	 */
	@Override
	protected Component newContentComponent() {
		return (new CRunLogbookTreeNeoclassic("runLogbookTree", caseComponent, this));
	}

	/**
	 * Shows note within item area.
	 *
	 * @param aParent the a parent
	 * @param aItem the a item
	 */
	@Override
	protected void createNote(Component aParent,IXMLTag aItem) {
		String lNoteTitle = aItem.getChild("pid").getValue();
		Label lLabel = new CDefLabel();
		aParent.appendChild(lLabel);
		lLabel.setSclass(getClassName() + "_s_margin " + getClassName() + "_s_item_label");
		lLabel.setValue(CContentHelper.getNodeTagLabel(caseComponent.getEComponent().getCode(), "note") + ": " + lNoteTitle);
		String lNote = "";
		lNote = CDesktopComponents.sSpring().unescapeXML(aItem.getChild("text").getValue());
		CDefTextbox lTextbox = new CDefTextbox();
		aParent.appendChild(lTextbox);
		lTextbox.setZclass(getClassName() + "_s_item_text");
		lTextbox.setId("runLogbookNote");
		lTextbox.setValue(lNote);
		lTextbox.setAttribute("changed", "false");
		lTextbox.setRows(runLogbookNoteRows);
	}

}
