/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.DropEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.MouseEvent;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;

public class CRunGraphicalformPointDiv extends CDefDiv {

	private static final long serialVersionUID = 3119279101381922084L;

	public void onCreate(CreateEvent aEvent) {
		String defaultStyle = "";
		if (!getAttribute("description").equals("")) {
			defaultStyle += "cursor:pointer;";
		}
		else if (getDraggable().equals("true")) {
			defaultStyle += "cursor:move;";
		}
		setAttribute("defaultStyle", defaultStyle); 
		Events.postEvent("onUpdatePosition", this, null);
	}

	public void onUpdatePosition(Event aEvent) {
		CRunGraphicalforms runGraphicalforms = (CRunGraphicalforms)CDesktopComponents.vView().getComponent("runGraphicalforms");
		if (runGraphicalforms == null) {
			return;
		}
		//set position
		setStyle(getAttribute("defaultStyle") + runGraphicalforms.getStylePosition((String)getAttribute("position"), (String)getAttribute("size")));
		Events.postEvent("onUpdate", this, (runGraphicalforms.isHideRightWrong((IXMLTag)getAttribute("tag")) ? "hideRightWrong" : ""));
	}

	public void onUpdate(Event aEvent) {
		CRunGraphicalforms runGraphicalforms = (CRunGraphicalforms)CDesktopComponents.vView().getComponent("runGraphicalforms");
		if (runGraphicalforms == null) {
			return;
		}
		boolean hideRightWrong = aEvent.getData() != null && aEvent.getData().equals("hideRightWrong");
		String sclass = "PointDraggable CRunGraphicalformPoint";
		if (getAttribute("placeable").equals("true") && runGraphicalforms.isShowIfRightOrWrong((IXMLTag)getAttribute("tag")) && !hideRightWrong) {
			if (runGraphicalforms.isRightlyPositioned((IXMLTag)getAttribute("tag"))) {
				sclass += "Right";
			}
			else {
				sclass += "Wrong";
			}
		}
		if (getSclass() == null || !getSclass().equals(sclass)) {
			setSclass(sclass);
		}
	}

	public void onClick(MouseEvent aEvent) {
		CRunGraphicalforms runGraphicalforms = (CRunGraphicalforms)CDesktopComponents.vView().getComponent("runGraphicalforms");
		if (runGraphicalforms == null) {
			return;
		}
		if (!getAttribute("description").equals("")) {
			runGraphicalforms.showAlert((String)getAttribute("title"), (String)getAttribute("description"));
		}
		else {
			//NOTE aEvent.getX() and aEvent.getY() are relative to the top left corner of the point
			//So get current position of point stored in attribute 'point', to be able to determine absolute x and y
			//get current absolute position
			String currentPosition = (String)getAttribute("position");
			String[] positionArr = currentPosition.split(",");
			int xOffset = 0;
			int yOffset = 0;
			boolean ok = true;
			if (positionArr.length == 2) {
				try {
					xOffset = Integer.parseInt(positionArr[0]);
				} catch (NumberFormatException e) {
					ok = false;
				}
				if (ok) {
					try {
						yOffset = Integer.parseInt(positionArr[1]);
					} catch (NumberFormatException e) {
						ok = false;
					}
				}
			}
			if (ok) {
				//calculate new absolute position
				String newPosition = "" + (xOffset + aEvent.getX()) + "," + (yOffset + aEvent.getY());
				//center point on the new position
				Integer left = runGraphicalforms.getLeft(newPosition, (String)getAttribute("size"));
				Integer top = runGraphicalforms.getTop(newPosition, (String)getAttribute("size"));
				if (left != null && top != null) {
					runGraphicalforms.showItem(left.intValue(), top.intValue());
				}
			}
		}
	}

	public void onDrop(DropEvent aEvent) {
		CRunGraphicalforms runGraphicalforms = (CRunGraphicalforms)CDesktopComponents.vView().getComponent("runGraphicalforms");
		if (runGraphicalforms == null) {
			return;
		}
		runGraphicalforms.dropItem(aEvent.getDragged(), this, aEvent.getX(), aEvent.getY());
	}

}
