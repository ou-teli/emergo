/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.LegalPresenting;

import java.util.HashMap;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefBox;
import nl.surf.emergo.control.script.CScript;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpring;

/**
 * This class is the ancestor of all Legal Presenting classes that are responsible for handling macros defined within the navigation component of the Legal Presenting case.
 * Every one of these classes is used within the ZUL file that is entered in the corresponding macro tag within the navigation component.
 */
public class CRunLegalPresentingBox extends CDefBox {

	private static final long serialVersionUID = -8712419828619967206L;

	protected VView vView = CDesktopComponents.vView();
	protected CControl cControl = CDesktopComponents.cControl();
	protected SSpring sSpring = CDesktopComponents.sSpring();
	protected CScript cScript = CDesktopComponents.cScript();
	
	/** The current case component. */
	IECaseComponent currentCaseComponent;
	/** The current tag. */
	protected IXMLTag currentTag;
	/** The current macro gathers data and puts it in a property map that is used by a child macro, with its own ZUL file, to render the data. */
	protected Map<String,Object> propertyMap = new HashMap<String,Object>();
	/** The macro. */
	protected HtmlMacroComponent macro;
	/** The child macro. */
	protected HtmlMacroComponent childMacro;
	/** The path to the ZUL file of the current macro. It is used by the child macro to be able to reference to other files like assets or style sheets. */
	protected String zulfilepath = "";
	
	public String getZulfilepath() {
		return zulfilepath;
	}

	public void setZulfilepath(String zulfilepath) {
		this.zulfilepath = zulfilepath;
	}

	public void onCreate() {
		//NOTE use echoEvent, otherwise macro parent does not yet exist, if code is used within zscript
	  	Events.echoEvent("onInit", this, null);
	}
	
	/** Init macro. */
	public void onInit() {
		macro = (HtmlMacroComponent)getMacroParent(this);
		propertyMap.put("macro", macro);
		
		//NOTE normally current case component is the navigation component and current tag is a macro tag.
		//However if the macro zul file is used within the junior scientist references component, the case component is a references component and the tag is a piece tag
		currentCaseComponent = (IECaseComponent)macro.getDynamicProperty("a_casecomponent");
		currentTag = (IXMLTag)macro.getDynamicProperty("a_tag");

		propertyMap.put("casecomponent", currentCaseComponent);
		propertyMap.put("tag", currentTag);
		zulfilepath = (String)macro.getDynamicProperty("a_zulfilepath");
		propertyMap.put("zulfilepath", zulfilepath);
		
		propertyMap.put("macroBoxUuid", getUuid());
	}

	protected void addChildMacro(String childMacroZulfile) {
		childMacro = new HtmlMacroComponent();
		appendChild(childMacro);
		childMacro.setDynamicProperty("a_propertyMap", propertyMap);
		childMacro.setMacroURI(zulfilepath + childMacroZulfile);
	}
	
	
	/** Update macro. */
	public void onUpdate() {
		//NOTE may be overwritten by descendant if macro has to be updated, e.g., due to user action
	}

	protected Component getMacroParent(Component component) {
		while (component != null && !(component instanceof HtmlMacroComponent)) {
			component = component.getParent();
		}
		return component;
	}
	
	/** Wrappers for SSpring methods. */
	protected void setRunComponentStatus(IECaseComponent caseComponent, String statusKey, String statusValue) {
		sSpring.setRunComponentStatus(caseComponent, statusKey, statusValue, true, AppConstants.statusTypeRunGroup, true);
	}

	protected IXMLTag setRunTagStatus(IECaseComponent caseComponent, IXMLTag tag, String statusKey, String statusValue, boolean fromScript) {
		return sSpring.setRunTagStatus(caseComponent, tag, statusKey, statusValue, null, true, AppConstants.statusTypeRunGroup, true, fromScript, true);
	}

}
