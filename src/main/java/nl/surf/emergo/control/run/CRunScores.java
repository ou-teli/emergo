/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Div;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefMacro;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SDatatagReference;
import nl.surf.emergo.zkspring.STriggeredReference;

/**
 * The Class CRunScores is used to show score display within the run view area of the emergo player.
 */
public class CRunScores extends CRunComponent {
	
	private static final long serialVersionUID = 4269023319462587342L;
	
	protected boolean init = false;
	
	protected Hashtable<String,String> scoreTagIdsForStateTagReferences = null; 

	public final static String runScoresId = "runScores";
	public final static String macroScoresDatasId = "macroScoresDatas";
	public final static String macroScoresOverviewDatasId = "macroScoresOverviewDatas";
	public final static String divScoresOverviewId = "divScoresOverview";
	public final static String imageScoresScoreId = "imageScoresScore";
	public final static String labelScoresScoreId = "labelScoresScore";
	public final static String imageScoresOverviewScoreId = "imageScoresOverviewScore";
	public final static String labelScoresOverviewScoreId = "labelScoresOverviewScore";
	public int rowNumber = 1;
	
	/**
	 * Instantiates a new c run scores.
	 */
	public CRunScores() {
		super("runScores", null);
		init();
	}

	/**
	 * Instantiates a new c run scores.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the case component
	 */
	public CRunScores(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
		init();
	}

	/**
	 * Creates title area, content area to show scores,
	 * and buttons area with close button.
	 */
	@Override
	protected void createComponents() {
		CRunVbox lVbox = new CRunVbox();

		createTitleArea(lVbox);
		createContentArea(lVbox);
		createButtonsArea(lVbox);

		appendChild(lVbox);
	}

	/**
	 * Creates new content component, the scores view.
	 *
	 * @return the component
	 */
	@Override
	protected Component newContentComponent() {
		HtmlMacroComponent scoresView = new CDefMacro();
		scoresView.setId("runScoresView");
		scoresView.setZclass("");
		return scoresView;
	}

	/**
	 * Creates buttons area and adds close button.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run hbox
	 */
	@Override
	protected CRunHbox createButtonsArea(Component aParent) {
		return null;
	}

	/**
	 * Init.
	 */
	public void init() {
		if (!init) {
			init = true;
			CDesktopComponents.sSpring().setRunComponentStatus(caseComponent, AppConstants.statusKeyOpened, AppConstants.statusValueTrue, true, getRunStatusType(), true);
		}

		Events.postEvent("onInitZulfile", this, null);
	}

	public void update() {
 		String divScoresId = "divScores";
 		Div divScores = (Div)CDesktopComponents.vView().getComponent(divScoresId);
 		if (divScores != null) {
 			Events.postEvent("onUpdate", divScores, null);
 		}
	}

	public String getScoresAccessible() {
		IXMLTag lRootTag = getDataPlusStatusRootTag();
		IXMLTag lComponentTag = lRootTag.getChild(AppConstants.componentElement);
		boolean lAccessible = !lComponentTag.getCurrentStatusAttribute(AppConstants.statusKeyAccessible)
				.equals(AppConstants.statusValueFalse);
		return "" + lAccessible;
	}

	public String getScoresPositionTop() {
		IXMLTag lRootTag = getDataPlusStatusRootTag();
		IXMLTag lComponentTag = lRootTag.getChild(AppConstants.componentElement);
		return getScoresPositionTop(lComponentTag);
	}

	public String getScoresPositionLeft() {
		IXMLTag lRootTag = getDataPlusStatusRootTag();
		IXMLTag lComponentTag = lRootTag.getChild(AppConstants.componentElement);
		return getScoresPositionLeft(lComponentTag);
	}

	public List<List<Object>> getScoresDatasLevel1() {
		IXMLTag lRootTag = getDataPlusStatusRootTag();
		return getScoresDatas(lRootTag, 1);
	}

	public String getScoresPositionLeft(IXMLTag aComponentTag) {
		return getScoresPosition(aComponentTag, 0);
	}

	public String getScoresPositionTop(IXMLTag aComponentTag) {
		return getScoresPosition(aComponentTag, 1);
	}

	protected String getScoresPosition(IXMLTag aComponentTag, int aIndex) {
		String lPosition = aComponentTag.getChildAttribute(AppConstants.statusElement, "position");
		if (lPosition.equals("")) {
			return "";
		}
		String[] lPositionArr = lPosition.split(",");
		if (lPositionArr == null || aIndex > (lPositionArr.length - 1)) {
			return "";
		}
		try {
			int lValue = Integer.parseInt(lPositionArr[aIndex]);
			return "" + lValue;
		} catch (NumberFormatException e) {
			return "";
		}
	}

	public List<List<Object>> getScoresDatas(int aViewRelevance) {
		return getScoresDatas(null, aViewRelevance);
	}
	
	public List<List<Object>> getScoresDatas(IXMLTag aRootTag, int aViewRelevance) {
		scoreTagIdsForStateTagReferences = new Hashtable<String,String>(); 
		List<List<Object>> lDataList = new ArrayList<List<Object>>();
		List<IXMLTag> lXmlTags = getContentChildTagsByComponentCode(aRootTag, "scores", "score");
		if (lXmlTags == null) {
			return lDataList;
		}
		for (IXMLTag lXmlTag : lXmlTags) {
			int lViewRelevance = Integer.parseInt(lXmlTag.getChildValue("viewrelevance"));
			boolean lPresent = !lXmlTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse);
			if (lViewRelevance <= aViewRelevance) {
				List<Object> lData = new ArrayList<Object>();
				lData.add(lXmlTag);
				lData.add(lXmlTag.getAttribute(AppConstants.keyId));
				//get sprite number
				String lSpriteNumber = lXmlTag.getChildValue("sprite");
				lData.add(CDesktopComponents.sSpring().getStyleImgSrc("scores-sprite" + lSpriteNumber));
				lData.add(CDesktopComponents.sSpring().getStyleImgSrc("scores-sprite-animation" + lSpriteNumber));
				lData.add(CDesktopComponents.sSpring().getStyleImgSrc("scores-sprite-big" + lSpriteNumber));
				String lScore = "";
				//get related state value
				SDatatagReference lReference = CDesktopComponents.sSpring().getSReferencedDataTagHelper().getDatatagReference(caseComponent, lXmlTag, "refstate");
				IXMLTag lStateTag = lReference.getDataTag();
				if (lStateTag != null) {
					if (lStateTag.getChildValue("keytype").equals("time")) {
						//minutes
						lScore = "" + Math.round(CDesktopComponents.sSpring().getCaseTime() / 60);
					}
					else if (lStateTag.getName().equals("formula")) {
						lScore = CDesktopComponents.sSpring().getFormulaValue(lStateTag);
					}
					else {
						lScore = lStateTag.getCurrentStatusAttribute(AppConstants.statusKeyValue);
					}
					String lReferenceIds = "" + lReference.getCaseComponent().getCacId() + "," +
						lStateTag.getAttribute(AppConstants.keyId);
					scoreTagIdsForStateTagReferences.put(lReferenceIds, lXmlTag.getAttribute(AppConstants.keyId));
				}
				lData.add(lScore);
				lData.add(CDesktopComponents.sSpring().unescapeXML(lXmlTag.getChildValue("label")));
				lData.add(CDesktopComponents.sSpring().unescapeXML(lXmlTag.getChildValue("unit")));
				lData.add("" + lPresent);
				lDataList.add(lData);
			}
		}
		return lDataList;
	}

	protected List<IXMLTag> getContentChildTagsByComponentCode(IXMLTag aRootTag, String aComponentCode, String aTagName) {
		List<IXMLTag> lXmlTags = new ArrayList<IXMLTag>();
		IXMLTag lRootTag = aRootTag;
		if (lRootTag == null) {
			lRootTag = getDataPlusStatusRootTag();
		}
		if (lRootTag != null) {
			IXMLTag lContentRootTag = lRootTag.getChild(AppConstants.contentElement);
			if (lContentRootTag != null) {
				lXmlTags = lContentRootTag.getChilds(aTagName);
			}
		}
		return lXmlTags;
	}

	@Override
	public void handleStatusChange(STriggeredReference aTriggeredReference) {
		//NOTE There is only one scores component
		if (aTriggeredReference.getDataTag() == null) {
			if (aTriggeredReference.getCaseComponent().getEComponent().getCode().equals("scores") &&
					aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyPresent)) {
				//present change for scores component
				boolean lPresent = aTriggeredReference.getStatusValue().equals(AppConstants.statusValueTrue);
				setVisible(lPresent);
			}
			return;
		}
		if (aTriggeredReference.getCaseComponent().getEComponent().getCode().equals("states") &&
				(aTriggeredReference.getDataTag().getName().equals("state") || aTriggeredReference.getDataTag().getName().equals("formula")) &&
				aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyValue)) {
			//change of state
			//rerender corresponding score
			String lReferenceIds = "" + aTriggeredReference.getCaseComponent().getCacId() + "," +
				aTriggeredReference.getDataTag().getAttribute(AppConstants.keyId);
			if (scoreTagIdsForStateTagReferences != null && scoreTagIdsForStateTagReferences.containsKey(lReferenceIds)) {
				reRenderScore(scoreTagIdsForStateTagReferences.get(lReferenceIds), aTriggeredReference.getStatusValue());
			}
		}
		else if (caseComponent.getCacId() == aTriggeredReference.getCaseComponent().getCacId()) {
			if (aTriggeredReference.getDataTag().getName().equals("score") &&
				aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyHighlight)) {
				//highlight score
				highlightScore(aTriggeredReference.getDataTag().getAttribute(AppConstants.keyId));
			}
			else {
				//possibly score is set present, so rerender all scores
				update();
			}
		}
	}

	protected void reRenderScore(String aTagId, String aScore) {
		String macroScoresScoreId = "macroScoresScore" + aTagId;
		HtmlMacroComponent macroScoresScore = (HtmlMacroComponent)CDesktopComponents.vView().getComponent(macroScoresScoreId);
		if (macroScoresScore != null) {
			macroScoresScore.setDynamicProperty("a_score", aScore);
		}
		String divScoresScoreId = "divScoresScore" + aTagId;
		Div divScoresScore = (Div)CDesktopComponents.vView().getComponent(divScoresScoreId);
		if (divScoresScore != null) {
			Events.postEvent("onRender", divScoresScore, null);
		}
		String macroScoresOverviewScoreId = "macroScoresOverviewScore" + aTagId;
		HtmlMacroComponent macroScoresOverviewScore = (HtmlMacroComponent)CDesktopComponents.vView().getComponent(macroScoresOverviewScoreId);
		if (macroScoresOverviewScore != null) {
			macroScoresOverviewScore.setDynamicProperty("a_score", aScore);
		}
		String divScoresOverviewScoreId = "divScoresOverviewScore" + aTagId;
		Div divScoresOverviewScore = (Div)CDesktopComponents.vView().getComponent(divScoresOverviewScoreId);
		if (divScoresOverviewScore != null) {
			Events.postEvent("onRender", divScoresOverviewScore, null);
		}
	}

	protected void highlightScore(String aTagId) {
		String divScoresScoreId = "divScoresScore" + aTagId;
		Div divScoresScore = (Div)CDesktopComponents.vView().getComponent(divScoresScoreId);
		if (divScoresScore != null) {
			Events.postEvent("onHighlight", divScoresScore, null);
		}
	}

	public void reRenderTimeScores() {
		List<IXMLTag> lXmlTags = getContentChildTagsByComponentCode(null, "scores", "score");
		if (lXmlTags == null) {
			return;
		}
		//minutes
		String lTime = "" + Math.round(CDesktopComponents.sSpring().getCaseTime() / 60);
		for (IXMLTag lXmlTag : lXmlTags) {
			int lViewRelevance = Integer.parseInt(lXmlTag.getChildValue("viewrelevance"));
			boolean lPresent = !lXmlTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse);
			if (lViewRelevance <= 1 && lPresent) {
				//get related state value
				IXMLTag lStateTag = CDesktopComponents.sSpring().getSReferencedDataTagHelper().getReferencedTag(caseComponent, lXmlTag, "refstate");
				if (lStateTag != null) {
					if (lStateTag.getChildValue("keytype").equals("time")) {
						//minutes
						reRenderScore(lXmlTag.getAttribute(AppConstants.keyId), lTime);
					}
				}
			}
		}
	}

	/**
	 * Methods originally used in ZUL file.
	 */

	public void onInitZulfile(Event aEvent) {
		Map<String,Object> propertyMap = new HashMap<String,Object>();
		propertyMap.put("currentEmergoComponent", this);
		propertyMap.put("scores_datas", getScoresDatasLevel1());
		((HtmlMacroComponent)getRunContentComponent()).setDynamicProperty("a_propertyMap", propertyMap);
		((HtmlMacroComponent)getRunContentComponent()).setMacroURI(VView.v_run_scores_fr);
		getRunContentComponent().setVisible(true);
	}

}
