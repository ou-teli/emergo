/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Treeitem;

import nl.surf.emergo.control.CTree;
import nl.surf.emergo.control.def.CDefCheckbox;

/**
 * The Class CRunTaskFinishedCb used to check a task.
 */
public class CRunTaskFinishedCb extends CDefCheckbox {

	private static final long serialVersionUID = 2942130988353463362L;

	/**
	 * Instantiates a new c run task finished cb.
	 * 
	 * @param aTreeitem the a treeitem, the parent
	 */
	public CRunTaskFinishedCb(Treeitem aTreeitem) {
		setAttribute("treeitem", aTreeitem);
	}

	/**
	 * On check notify parent tree.
	 * 
	 * @param aEvent the a event
	 */
	public void onCheck(Event aEvent) {
		String lDisabled = (String)getAttribute("disabled");
		if ((lDisabled != null) && (lDisabled.equals("true"))) {
			setChecked(!isChecked());
			return;
		}
		Treeitem lTreeitem = (Treeitem) getAttribute("treeitem");
		CTree lTree = (CTree) lTreeitem.getTree();
		lTree.contentItemFinished(lTreeitem, aEvent.getTarget(), "" + this.isChecked());
	}
}
