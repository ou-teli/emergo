/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.JuniorScientist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CContentHelper;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.zkspring.STriggeredReference;

/**
 * This class is used to show mails in a pop-up. It uses content entered within a mail case component.
 * The used mail case component should have present=false and present should stay false, to prevent rendering of the original mail component on the tablet.
 * Mail content entered determines the layout of the mail. Only sent mails are shown. Mails are sent by game script. 
 * For mails the explanation child tag may be used to set certain parameters to be used to render the mail.
 * 
 * Mails may have parameters that indicate how to decorate the mail. These parameters are stored in the explanation child tag of the mail.
 * E.g., 'to=John, Steven&cc=Alice, Juliette':
 * - 'to' (optional), overwrites default receiver of mail (the current user)
 * - 'cc' (optional), enables showing cc that default is not possible 
 * - 'actionText' (optional), shows a button with label 'actionText' directly below the mail which is used to close the mail macro and to open another macro when clicked
 * - 'macro-tag-pid' (optional), the pid of the macro to be shown 
 */
public class CRunJuniorScientistMailBox extends CRunJuniorScientistBox {

	private static final long serialVersionUID = -5267492049051942915L;

	protected final static String mailCaseComponentName = "game_mail";
	protected IECaseComponent mailCaseComponent;
	
	protected String mailMacroId;
	protected String mailActionMacroId;

	protected List<IXMLTag> mailTags = new ArrayList<IXMLTag>();
	protected Integer mailToShowIndex = 0;
	
	protected boolean initialized = false;

	protected final static int shortMailBodyLength = 50;

	/** Constants needed to get to and cc for mail. */
	protected final static String paramKeyTo = "to";
	protected final static String paramKeyCc = "cc";

	protected final static String paramKeyActionText = "actionText";
	protected final static String paramKeyMacroTagPid = "macro-tag-pid";

	@Override
	public void onInit() {
		super.onInitMacro();
		
		mailMacroId = getUuid() + "_mail_macro";
		mailActionMacroId = getUuid() + "_mail_action_macro";

		mailCaseComponent = getMailComponent();
		//NOTE super ancestor CRunComponent needs case component to be set
		setCaseComponent(mailCaseComponent);

		//just like for the original mail component set selected and opened
		setRunComponentStatusJS(mailCaseComponent, AppConstants.statusKeySelected, AppConstants.statusValueTrue);
		setRunComponentStatusJS(mailCaseComponent, AppConstants.statusKeyOpened, AppConstants.statusValueTrue);

		//determine mail properties to be used within the child macro
		setMailProperties();
		
		//add the child macro
		addChildMacro("JuniorScientist_mail_view_macro.zul");
		
		initialized = true;
	}
	
	@Override
	public void onUpdate() {
		//rerender child macro with adjusted properties
		childMacro.recreate();
	}

	protected void setMailProperties() {
		propertyMap.put("mailsTitle", sSpring.unescapeXML(sSpring.getCaseComponentRoleName("", mailCaseComponent.getName())));
		
		List<Map<String,Object>> mails = new ArrayList<Map<String,Object>>();
		propertyMap.put("mails", mails);
		
		mailTags.clear();
		List<IXMLTag> nodeTags = cScript.getRunGroupNodeTags(mailCaseComponent);
		//NOTE newer mails on top
		int index = 0;
		IXMLTag activeMailTag = null;
		CContentHelper cHelper = new CContentHelper(null);
		for (int i=(nodeTags.size()-1);i>=0;i--) {
			IXMLTag nodeTag = nodeTags.get(i);
			if (nodeTag.getName().equals("inmailpredef")) {
				boolean sent = nodeTag.getCurrentStatusAttribute(AppConstants.statusKeySent).equals(AppConstants.statusValueTrue);
				boolean isOriginalTag = !nodeTag.getAttribute(AppConstants.keyRefstatusids).equals("");
				if (sent && !isOriginalTag) {
					mailTags.add(nodeTag);
					
					Map<String,Object> hMailData = new HashMap<String,Object>();
					mails.add(hMailData);
				
					hMailData.put("mailIndex", new Integer(index));
					hMailData.put("new", !nodeTag.getCurrentStatusAttribute(AppConstants.statusKeyOpened).equals(AppConstants.statusValueTrue));
					boolean active = index == mailToShowIndex.intValue();
					if (active) {
						activeMailTag = nodeTag;
					}
					hMailData.put("active", active);

					hMailData.put("from", sSpring.unescapeXML(nodeTag.getStatusChildValue("sendername")));
					
					hMailData.put("subject", sSpring.unescapeXML(nodeTag.getStatusChildValue("title")));

					String body = sSpring.unescapeXML(nodeTag.getStatusChildValue("richtext").replaceAll("<p>", "<p class=\"js-p\">"));
					//NOTE shortBody is one line so strip html codes
					String shortBody = cHelper.stripHtmlFromCKeditorString(cHelper.stripCKeditorString(body));
					if (shortBody.length() > shortMailBodyLength) {
						shortBody = shortBody.substring(0, shortMailBodyLength) + "...";
					}
					hMailData.put("shortBody", shortBody);

					index++;
				}
			}
		}
		
		setMailProperties(activeMailTag);

		if (activeMailTag != null) {
			setRunTagStatusJS(mailCaseComponent, activeMailTag, AppConstants.statusKeyOpened, AppConstants.statusValueTrue, true);
		}
	}
	
	protected void setMailProperties(IXMLTag mailTag) {
		Map<String,Object> mail = new HashMap<String,Object>();
		propertyMap.put("mailData", mail);

		mail.put("mailTag", mailTag);
		if (mailTag != null) {
			mail.put("from", sSpring.unescapeXML(mailTag.getStatusChildValue("sendername")));
			mail.put(paramKeyTo, getTo(mailTag));
			mail.put(paramKeyCc, getCc(mailTag));
			mail.put("subject", sSpring.unescapeXML(mailTag.getStatusChildValue("title")));
			mail.put("body", sSpring.unescapeXML(mailTag.getStatusChildValue("richtext")).replaceAll("<p>", "<p class=\"js-p\">"));
			mail.put(paramKeyActionText, getActionText(mailTag));

			List<Map<String,Object>> attachments = new ArrayList<Map<String,Object>>();
			mail.put("attachments", attachments);
					
			for (IXMLTag attachmentTag : mailTag.getStatusChilds("attachment")) {
				Map<String,Object> hAttachmentData = new HashMap<String,Object>();
				attachments.add(hAttachmentData);
	
				String attachmentName = attachmentTag.getChildValue("name");
				if (attachmentName.equals("")) {
					attachmentName = attachmentTag.getChildValue("pid");
				}
				attachmentName = sSpring.unescapeXML(attachmentName);
				hAttachmentData.put("attachmentName", attachmentName);
	
				String attachmentUrl = sSpring.getSBlobHelper().getUrl(attachmentTag, "blob");
				if (!vView.isAbsoluteUrl(attachmentUrl)) {
					attachmentUrl = vView.getAbsoluteUrl(attachmentUrl);
				}
				hAttachmentData.put("attachmentUrl", attachmentUrl);
			}
		}
	}
	
	protected IECaseComponent getMailComponent() {
		return sSpring.getCaseComponent("", mailCaseComponentName);
	}
	
	/** If a button is clicked within the child macro file, following event is triggered. */
	public void onButtonClick(Event event) {
		if (event.getData().equals("close_macro")) {
			//NOTE the mail pop-up is closed, so hide it

			//just like for the original tasks component set opened for the case component
			setRunComponentStatusJS(mailCaseComponent, AppConstants.statusKeyOpened, AppConstants.statusValueFalse);

			//hide macro component
			setRunTagStatusJS(getNavigationCaseComponent(), currentTag, AppConstants.statusKeyPresent, AppConstants.statusValueFalse, false);
		}
		else if (event.getData() instanceof Integer) {
			//mail is chosen
			Integer index = (Integer)event.getData();
			if (index.intValue() != mailToShowIndex.intValue()) {
				//mail other than current active is chosen
				Component mailDiv = vView.getComponent(getUuid() + "_mail_div_" + mailToShowIndex);
				Events.sendEvent("onSetActive", mailDiv, false);

				mailToShowIndex = (Integer)event.getData();

				mailDiv = vView.getComponent(getUuid() + "_mail_div_" + mailToShowIndex);
				Events.sendEvent("onSetActive", mailDiv, true);

				IXMLTag mailTag = mailTags.get(mailToShowIndex);
				setMailProperties(mailTag);
				
				HtmlMacroComponent mailMacro = (HtmlMacroComponent)vView.getComponent(mailMacroId);
				mailMacro.setDynamicProperty("a_mailData", propertyMap.get("mailData"));
				mailMacro.recreate();

				HtmlMacroComponent mailActionMacro = (HtmlMacroComponent)vView.getComponent(mailActionMacroId);
				mailActionMacro.setDynamicProperty("a_macroBoxUuid", getUuid());
				mailActionMacro.setDynamicProperty("a_mailData", propertyMap.get("mailData"));
				mailActionMacro.recreate();

				setRunTagStatusJS(mailCaseComponent, mailTag, AppConstants.statusKeySelected, AppConstants.statusValueTrue, true);
				setRunTagStatusJS(mailCaseComponent, mailTag, AppConstants.statusKeyOpened, AppConstants.statusValueTrue, true);
			}
		}
	}
	
	protected String getTo(IXMLTag mailTag) {
		String to = getParamValue(mailTag, paramKeyTo);
		if (to.equals("")) {
			//default return pc case role
			return sSpring.getSCaseRoleHelper().getCaseRole().getName();
		}
		return to;
	}
	
	protected String getCc(IXMLTag mailTag) {
		return getParamValue(mailTag, paramKeyCc);
	}
	
	protected String getActionText(IXMLTag mailTag) {
		return getParamValue(mailTag, paramKeyActionText);
	}
	
	protected String getMacroTagPid(IXMLTag macroTag) {
		return getParamValue(macroTag, paramKeyMacroTagPid);
	}
	
	/** If a button is clicked within the child macro file, following event is triggered. */
	public void onMailAction(Event event) {
		IXMLTag mailTag = (IXMLTag)event.getData();
		String macroTagPid = getMacroTagPid(mailTag);
		if (!macroTagPid.equals("")) {
			hideMacro(currentTag);
			showMacro(macroTagPid);
		}
	}
	
	@Override
	public void handleStatusChange(STriggeredReference aTriggeredReference) {
	}

}
