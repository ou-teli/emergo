/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.Treerow;
import org.zkoss.zul.impl.XulElement;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CTree;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunMemosHelper. Helps rendering memos.
 */
public class CRunMemosHelper extends CRunComponentHelper {

	/** The max string length. */
	protected int maxStringLength = 40;

	/**
	 * Instantiates a new c run Memos helper.
	 *
	 * @param aTree the ZK memos tree
	 * @param aShowTagNames the a show tag names, comma separated, only tags with these names are shown
	 * @param aCaseComponent the memos case component
	 * @param aRunComponent the a run component, the ZK memos component
	 */
	public CRunMemosHelper(CTree aTree, String aShowTagNames,	IECaseComponent aCaseComponent, CRunComponent aRunComponent) {
		super(aTree, aShowTagNames, aCaseComponent, aRunComponent);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTreeHelper#renderTreecell(org.zkoss.zul.Treeitem, org.zkoss.zul.Treerow, nl.surf.emergo.business.IXMLTag, java.lang.String, boolean)
	 */
	@Override
	protected Treecell renderTreecell(Treeitem aTreeitem, Treerow aTreerow,
			IXMLTag aItem, String aKeyValues, boolean aAccIsAuthor) {
		Treecell lTreecell = getTreerowTreecell(aTreerow, 0);
		String lLabelStr = aItem.getChild("pid").getValue();
		if (lLabelStr.length() > maxStringLength) {
			lTreecell.setTooltiptext(lLabelStr);
			lLabelStr = lLabelStr.substring(0, maxStringLength) + "...";
		}
		lTreecell.setLabel(lLabelStr);
		String lStatus = "active";
		if (isOpened(aItem))
			lStatus = "opened";
		if (!isAccessible(aItem))
			lStatus = "inactive";
		lTreecell.setZclass(runComponent.getClassName() + "_treecell_"+lStatus);

		return lTreecell;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunComponentHelper#setContextMenu(org.zkoss.zk.ui.Component)
	 */
	public void setContextMenu(Component aObject) {
		if (extendable)
			((XulElement) aObject).setContext("runMenuPopup");
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.CContentHelper#editContentItem(org.zkoss.zk.ui.Component, org.zkoss.zk.ui.Component, nl.surf.emergo.business.IXMLTag)
	 */
	public Component editContentItem(Component aParent, Component aTreeitem, IXMLTag aItem) {
		Component lTreeitem = super.editContentItem(aParent, aTreeitem, aItem);
		// Redraw item data by simulating click on it. But only for owner. Other rgas maybe rating it.
		if (aItem.getAttribute(AppConstants.keyOwner_rgaid).equals(""+CDesktopComponents.sSpring().getRunGroupAccount().getRgaId()))
			runComponent.doContentItemAction((Treeitem)lTreeitem);
		return lTreeitem;
	}

}