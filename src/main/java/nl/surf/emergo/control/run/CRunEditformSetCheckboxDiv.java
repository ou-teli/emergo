/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;

public class CRunEditformSetCheckboxDiv extends CDefDiv {

	private static final long serialVersionUID = 4557131313240112415L;

	public void onClick(Event aEvent) {
		CRunEditforms runEditforms = (CRunEditforms)CDesktopComponents.vView().getComponent("runEditforms");
		if (runEditforms != null && getAttribute("disabled").equals("false")) {
			if (getAttribute("selected").equals("false")) {
				setAttribute("selected", "true");
			}
			else {
				setAttribute("selected", "false");
			}
			runEditforms.saveAlternativeSelection((IXMLTag)getAttribute("tag"), (String)getAttribute("selected"));
			Events.postEvent("onUpdate", this, null);
			//get all checkboxes
			List<Component> editformItemCheckboxes = CDesktopComponents.vView().getComponentsByPrefix("editformItemCheckbox_");
			IXMLTag setTag = ((IXMLTag)getAttribute("tag")).getParentTag();
			//filter checkboxes on current set
			for (int i=(editformItemCheckboxes.size()-1);i>=0;i--) {
				Component editformItemCheckbox = editformItemCheckboxes.get(i);
				IXMLTag tag = (IXMLTag)editformItemCheckbox.getAttribute("tag");
				if (tag.getParentTag() != setTag) {
					editformItemCheckboxes.remove(i);
				}
			}
			if (!runEditforms.isSetMultiselect(setTag)) {
				for (Component editformItemCheckbox : editformItemCheckboxes) {
					if (editformItemCheckbox != this && editformItemCheckbox.getAttribute("selected").equals("true")) {
						editformItemCheckbox.setAttribute("selected", "false");
						Events.postEvent("onUpdate", editformItemCheckbox, null);
					}
				}
			}
			if (runEditforms.isSetMaxNumberOfAlternativesSelected(setTag)) {
				for (Component editformItemCheckbox : editformItemCheckboxes) {
					if (editformItemCheckbox.getAttribute("disabled").equals("false") && editformItemCheckbox.getAttribute("selected").equals("false")) {
						editformItemCheckbox.setAttribute("disabled", "true");
						Events.postEvent("onUpdate", editformItemCheckbox, null);
					}
				}
			}
			else {
				for (Component editformItemCheckbox : editformItemCheckboxes) {
					if (editformItemCheckbox.getAttribute("disabled").equals("true")) {
						editformItemCheckbox.setAttribute("disabled", "false");
						Events.postEvent("onUpdate", editformItemCheckbox, null);
					}
				}
			}
		}
	}

	public void onUpdate(Event aEvent) {
		String sclass = "CRunEditformAltCheckbox";
		if (getAttribute("disabled").equals("true")) {
			sclass += "Disabled";
		}
		if (getAttribute("selected").equals("true")) {
			sclass += "Selected";
		}
		if (!getSclass().equals(sclass)) {
			setSclass(sclass);
		}
		if (getAttribute("selected").equals("false")) {
			CRunEditforms runEditforms = (CRunEditforms)CDesktopComponents.vView().getComponent("runEditforms");
			if (runEditforms != null) {
				Events.postEvent("onUpdate",
						CDesktopComponents.vView().getComponent("editformItem_" + ((IXMLTag)getAttribute("tag")).getAttribute("id")), 
						(runEditforms.isHideRightWrong((IXMLTag)getAttribute("tag")) ? "hideRightWrong" : ""));
			}
		}
	}

}
