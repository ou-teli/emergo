/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.util;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.json.parser.JSONParser;
import org.zkoss.json.parser.ParseException;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.zkspring.SSpring;

public class CRunPVToolkitJsonHelper {
	
	protected SSpring sSpring;
	
	public CRunPVToolkitJsonHelper(SSpring sSpring) {
		this.sSpring = sSpring;
	}

	public String getStatusChildTagAttribute(IXMLTag tag, String attributeKey) {
		return sSpring.unescapeXML(tag.getChildAttribute(AppConstants.statusElement, attributeKey)).replace(AppConstants.statusCommaReplace, ",");
	}
	
	public IXMLTag getTagByNameAndKey(List<IXMLTag> nodeTags, String tagName, String tagKey) {
		for (IXMLTag nodeTag : nodeTags) {
			if (nodeTag.getName().equals(tagName) && nodeTag.getChildValue(nodeTag.getDefAttribute(AppConstants.defKeyKey)).equals(tagKey)) {
				return nodeTag;
			}
		}
		return null;
	}
	
	public JSONArray getJsonArray(String pJsonArrayStr) {
		JSONArray lJsonArray = null;
		if (pJsonArrayStr.equals("")) {
			lJsonArray = new JSONArray();
		}
		else {
			JSONParser jsonParser = new JSONParser();
			lJsonArray = (JSONArray)jsonParser.parse(pJsonArrayStr);
		}
		return lJsonArray;
	}
	
	public JSONArray getJsonArray(IECaseComponent statesCaseComponent, List<IXMLTag> stateTags, String stateTagName, String stateTagKey) {
		IXMLTag stateTag = getTagByNameAndKey(stateTags, stateTagName, stateTagKey);
		String jsonArrayStr = getStatusChildTagAttribute(stateTag, AppConstants.statusKeyValue);
		JSONArray jsonArray = null;
		if (jsonArrayStr.equals("")) {
			jsonArray = new JSONArray();
			sSpring.setRunTagStatus(statesCaseComponent, stateTag, AppConstants.statusKeyValue, sSpring.escapeXML("" + jsonArray).replace(",", AppConstants.statusCommaReplace), true, AppConstants.statusTypeRunGroup, true, false);
		}
		else {
			JSONParser jsonParser = new JSONParser();
			jsonArray = (JSONArray)jsonParser.parse(jsonArrayStr);
		}
		return jsonArray;
	}
	
	public JSONObject getJsonObject(JSONArray jsonArray, String jsonObjectToFindId) {
		JSONObject foundJsonObject = null;
		for (int i=0;i<jsonArray.size();i++) {
			JSONObject jsonObject = (JSONObject)jsonArray.get(i);
			if (jsonObject.get("id").equals(jsonObjectToFindId)) {
				foundJsonObject = jsonObject;
				break;
			}
		}
		return foundJsonObject;
	}
	
	public JSONObject getJsonObject(JSONArray jsonArray, JSONObject jsonObjectToFind) {
		return getJsonObject(jsonArray, (String)jsonObjectToFind.get("id"));
	}
	
	public String[] getStringArrayFromJSON(String pJsonObjectStr, String pJsonId) {
		JSONParser parser = new JSONParser();
		String[] lStringArray = new String[0];
		try {
			JSONObject lJsonObject = (JSONObject) parser.parse(pJsonObjectStr);
			JSONArray lJsonArray = (JSONArray) lJsonObject.get(pJsonId);
			lStringArray = new String[lJsonArray.size()];
			for (int i = 0; i < lJsonArray.size(); i++) {
				lStringArray[i] = (String) lJsonArray.get(i);
			}
		} catch (ParseException e) {
			sSpring.getSLogHelper().logAction("No valid JSON: " + pJsonObjectStr);
		}
		return lStringArray;
	}
	
	protected JSONArray getRunJsonArrayFromAccountExtraData(IERunGroupAccount pRGAccount) {
		String lExtraData = pRGAccount.getEAccount().getExtradata();
		String lCode = pRGAccount.getERunGroup().getERun().getCode();
		JSONArray lJsonArray = null;
		if (!StringUtils.isEmpty(lExtraData)) {
			try {
				JSONParser lParser = new JSONParser();
				JSONObject lJsonObject = (JSONObject) lParser.parse(lExtraData);
				lJsonArray = (JSONArray) lJsonObject.get(lCode);
			} catch (ParseException e) {
				sSpring.getSLogHelper().logAction("No valid JSON: " + lExtraData);
			}
		}
		return lJsonArray;
	}

	public String[] getStringArrayFromExtraData(IERunGroupAccount pRGAccount, String pJsonId) {
		JSONArray lRunJson = getRunJsonArrayFromAccountExtraData(pRGAccount);
		String[] lStringArray = new String[0];
		if (null != lRunJson) {
			JSONArray lResultArray = null;
			for (Object lObj : lRunJson) {
				JSONObject lPVJson = (JSONObject) lObj;
				if (lPVJson.containsKey(pJsonId)) {
					lResultArray = (JSONArray) lPVJson.get(pJsonId);
					break;
				}
			}
			if (lResultArray != null) {
				lStringArray = new String[lResultArray.size()];
				for (int i = 0; i < lResultArray.size(); i++) {
					lStringArray[i] = (String) lResultArray.get(i);
				}
			} else {
				sSpring.getSLogHelper().logAction("run code key and/or sub key " + pJsonId + " not found in account JSON extra data for account: " + pRGAccount.getEAccount().getAccId());
			}
		}
		return lStringArray;
	}

	public String getStringFromExtraData(IERunGroupAccount pRGAccount, String pJsonId) {
		JSONArray lRunJson = getRunJsonArrayFromAccountExtraData(pRGAccount);
		String lValueString = "";
		if (null != lRunJson) {
			for (Object lObj : lRunJson) {
				JSONObject lPVJson = (JSONObject) lObj;
				if (lPVJson.containsKey(pJsonId)) {
					lValueString = (String) lPVJson.get(pJsonId);
					break;
				}
			}
			if (StringUtils.isEmpty(lValueString)) {
				sSpring.getSLogHelper().logAction("run code key and/or sub key " + pJsonId + " not found in account JSON extra data for account: " + pRGAccount.getEAccount().getAccId());
			}
		}
		return lValueString;
	}

}
