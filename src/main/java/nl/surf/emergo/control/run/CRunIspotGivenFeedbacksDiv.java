/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Listbox;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.view.VView;

public class CRunIspotGivenFeedbacksDiv extends CDefDiv {

	private static final long serialVersionUID = 5876528980130314235L;

	protected VView vView = CDesktopComponents.vView();

	protected CRunIspot runIspot = null;

	public void onCreate(CreateEvent aEvent) {
		runIspot = (CRunIspot)vView.getComponent("runIspot");
	}

	public void onUpdate(Event aEvent) {
		Events.postEvent("onUpdate", vView.getComponent("ispotGivenFeedbacksListbox"), null);
	}

	public void onGivenFeedbacks(Event aEvent) {
		Events.postEvent("onIspotCancelRecording", vView.getComponent("ispotGivenFeedback"), null);
		runIspot.setComponentVisible(vView.getComponent("ispotGivenFeedbacksButton"), false);
		runIspot.setComponentVisible(vView.getComponent("ispotViewers"), false);
		runIspot.setComponentVisible(vView.getComponent("ispotGivenFeedback"), false);
		//clear selection so same listbox item can be chosen again
		((Listbox)vView.getComponent("ispotGivenFeedbacksListbox")).clearSelection();
		//NOTE update listbox if it is shown, so possibly new data elements are added
		Events.postEvent("onUpdate", vView.getComponent("ispotGivenFeedbacksListbox"), null);
		setVisible(true);
	}

}
