/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Include;

import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefInclude;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.STriggeredReference;

/**
 * The Class CRunDashboard is used to show the dashboard apps.
 */
public class CRunDashboard extends CRunComponent {

	private static final long serialVersionUID = -3805294200404374916L;

	protected String zulComponent = "dashboardDiv";
	
	/**
	 * Instantiates a new c run dashboard.
	 */
	public CRunDashboard() {
		super("runDashboard", null);
		init();
	}

	/**
	 * Instantiates a new c run dashboard.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the dashboard case component
	 */
	public CRunDashboard(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
		init();
	}

	/**
	 * Creates title area, content area to show apps,
	 * and buttons area with close button.
	 */
	@Override
	protected void createComponents() {
		CRunVbox lVbox = new CRunVbox();

		createTitleArea(lVbox);
		createContentArea(lVbox);
		createButtonsArea(lVbox);

		appendChild(lVbox);
	}

	/**
	 * Creates new content component, the apps panel.
	 *
	 * @return the component
	 */
	@Override
	protected Component newContentComponent() {
		Include lView = new CDefInclude();
		lView.setId("runDashboardView");
		lView.setVisible(false);
		return lView;
	}

	/**
	 * Creates buttons area and adds close button.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run hbox
	 */
	@Override
	protected CRunHbox createButtonsArea(Component aParent) {
		CRunHbox lButtonsHbox = super.createButtonsArea(aParent);
		createCloseButton(lButtonsHbox);
		return lButtonsHbox;
	}

	/**
	 * Creates view.
	 */
	protected void init() {
		((Include)getRunContentComponent()).setAttribute("emergoComponent", this);
		((Include)getRunContentComponent()).setSrc(VView.v_run_dashboard_fr);
		getRunContentComponent().setVisible(true);
	}
	
	@Override
	public boolean setVisible(boolean visible) {
		if (visible == true) {
			//rerender dashboard because content may have to be updated
			getRunContentComponent().setVisible(false);
			((Include)getRunContentComponent()).setSrc("");
			((Include)getRunContentComponent()).setSrc(VView.v_run_dashboard_fr);
			getRunContentComponent().setVisible(true);
		}
		return super.setVisible(visible);
	}
	
	/**
	 * Handles status change due to firing of script actions.
	 *
	 * @param aTriggeredReference the a triggered reference
	 */
	@Override
	public void handleStatusChange(STriggeredReference aTriggeredReference) {
		boolean lUpdateDashboard = (aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyUpdate) && aTriggeredReference.getDataTag() == null);
		if (lUpdateDashboard) {
			Component lZulComponent = CDesktopComponents.vView().getComponent(zulComponent);
			if (lZulComponent != null) {
				Events.postEvent("onHandleStatusChange", lZulComponent, aTriggeredReference);
			}
		}
	}

}
