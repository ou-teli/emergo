/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Image;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

import nl.surf.emergo.business.IRunGroupCaseComponentUpdateManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CCaseHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefMacro;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IECaseComponentRole;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.domain.IERunGroupCaseComponentUpdate;
import nl.surf.emergo.view.VView;

/**
 * The Class CRunProfile is used to show profile component within the run view area of the emergo player.
 */
public class CRunProfile extends CRunComponent {
	
	// TODO Is toggle ok? Also related to other layout. Or should every window have a close button?
	// TODO Save opening of profiles? In what status? rungroup? but other profiles are opened. Not necessary yet.
	// TODO Implement event queues asynchronously. See work/async_eventqueue.zul or http://books.zkoss.org/wiki/ZK_Developer%27s_Reference/UI_Patterns/Long_Operations/Use_Event_Queues
	
	private static final long serialVersionUID = 3886821563368268896L;

	/** Stores runGroupAccounts. */
	protected List<IERunGroupAccount> runGroupAccounts = null;

	/** The has new chats. */
	protected boolean hasNewChats = false;

	public static final String runProfileId = "runProfile";
	public static final String divProfilesId = "divProfiles";
	public static final String divProfileId = "divProfile";
	public static final String divChatId = "divChat";
	public static final String textboxProfileMoodId = "textboxProfileMood";
	public static final String textboxProfileChatId = "textboxProfileChat";
	public static final String imageProfileAvatarId = "imageProfileAvatar";
	public static final String imagePlayerAvatarId = "imagePlayerAvatar";

	/**
	 * Instantiates a new c run profile.
	 */
	public CRunProfile() {
		super("runProfile", null);
		init();
	}

	/**
	 * Instantiates a new c run profile.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the case component
	 */
	public CRunProfile(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
		init();
	}

	/**
	 * Creates title area, content area to show tab structure,
	 * and buttons area with close button.
	 */
	@Override
	protected void createComponents() {
		CRunVbox lVbox = new CRunVbox();

		createTitleArea(lVbox);
		createContentArea(lVbox);
		createButtonsArea(lVbox);

		appendChild(lVbox);
	}

	/**
	 * Creates new content component, the profile view.
	 *
	 * @return the component
	 */
	@Override
	protected Component newContentComponent() {
		HtmlMacroComponent profileView = new CDefMacro();
		profileView.setId("runProfileView");
		profileView.setZclass("");
		return profileView;
	}

	/**
	 * Creates buttons area and adds close button.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run hbox
	 */
	@Override
	protected CRunHbox createButtonsArea(Component aParent) {
		return null;
	}

	/**
	 * Creates view.
	 */
	public void init() {
		
		// NOTE Add pending group chats send when the rga was offline
		updateGroupChatTags();
		
		runWnd.eventQueueOpenPlayer.subscribe( new EventListener<Event>() {
			public void onEvent(Event evt) {
				handleOpenPlayerEvent(evt.getData().toString());
			}
		});
		runWnd.eventQueueClosePlayer.subscribe( new EventListener<Event>() {
			public void onEvent(Event evt) {
				handleClosePlayerEvent(evt.getData().toString());
			}
		});
		
		// NOTE only subscribe to chat queues concerned with sending/getting chats from other rgas in run or team. 
		for (IERunGroupAccount lRunGroupAccountFrom : getRunGroupAccounts()) {
			// Subscribe to group chats
			getChatEventQueue("" + lRunGroupAccountFrom.getRgaId(), "").subscribe( new EventListener<Event>() {
				public void onEvent(Event evt) {
					handleChatEvent((IXMLTag)evt.getData());
				}
			});
			for (IERunGroupAccount lRunGroupAccountTo : getRunGroupAccounts()) {
				if (lRunGroupAccountTo.getRgaId() != lRunGroupAccountFrom.getRgaId()) {
					int lCurrentRgaId = CDesktopComponents.sSpring().getRunGroupAccount().getRgaId();
					if (lRunGroupAccountFrom.getRgaId() == lCurrentRgaId || lRunGroupAccountTo.getRgaId() == lCurrentRgaId) {
						// Subscribe to private chats
						getChatEventQueue("" + lRunGroupAccountFrom.getRgaId(), "" + lRunGroupAccountTo.getRgaId()).subscribe( new EventListener<Event>() {
							public void onEvent(Event evt) {
								handleChatEvent((IXMLTag)evt.getData());
							}
						});
					}
				}
			}
		}

		Events.postEvent("onInitZulfile", this, null);
	}

	protected EventQueue<Event> getChatEventQueue(String aFromRgaId, String aToRgaId) {
		return EventQueues.lookup("chat_" + aFromRgaId + "_" + aToRgaId, EventQueues.APPLICATION, true);
	}
	
	protected void handleOpenPlayerEvent(String aRgaId) {
		if (isKnownRunGroupAccount(aRgaId)) {
			// NOTE every rga gets event, so filter on rgas in run or team
			renderNumberOfOnlinePlayers();
			renderProfiles();
		}
	}

	protected void handleClosePlayerEvent(String aRgaId) {
		if (isKnownRunGroupAccount(aRgaId)) {
			// NOTE every rga gets event, so filter on rgas in run or team
			renderNumberOfOnlinePlayers();
			renderProfiles();
		}
	}

	public void toggleVisibility(boolean restore) {
		Component profilesDiv = CDesktopComponents.vView().getComponent(CRunProfile.divProfilesId);
		Component profileDiv = CDesktopComponents.vView().getComponent(CRunProfile.divProfileId);
		Component chatDiv = CDesktopComponents.vView().getComponent(CRunProfile.divChatId);
		if (profilesDiv == null || profileDiv == null || chatDiv == null) {
			return;
		}
		if (!profilesDiv.isVisible()) {
			profilesDiv.setVisible(true);
			if (!restore) {
				profilesIsOpened();
			}
			renderProfiles();
		}
		else {
			if (!restore) {
				profilesIsClosed();
			}
			chatDiv.setVisible(false);
			profileDiv.setVisible(false);
			profilesDiv.setVisible(false);
		}

	}
	
	public void restore() {
	  	toggleVisibility(true);
	}

	protected void handleChatEvent(IXMLTag aChatTag) {
		// NOTE Commented out, should not be necessary anymore because chats are only send to the ones who should get them.
/*		String lSenderRgaId = aChatTag.getChild(AppConstants.statusElement).getChildValue("senderid");
		String lReceiverRgaId = aChatTag.getChild(AppConstants.statusElement).getChildValue("receiverid");
		boolean lIsChatForCurrentRga = lReceiverRgaId.equals("") ||
			lSenderRgaId.equals("" + CDesktopComponents.sSpring().getRunGroupAccount().getRgaId()) || 
			lReceiverRgaId.equals("" + CDesktopComponents.sSpring().getRunGroupAccount().getRgaId()); 
		if (isKnownRunGroupAccount(lSenderRgaId) && lIsChatForCurrentRga) {
			// NOTE every rga gets event, so filter on rgas in run or team and if chat is for current rga
			// NOTE get chat tag from saveChat because saveChat makes copy so every rga gets its own copy
			IXMLTag lChatTag = saveChat(aChatTag);
			renderChat(lChatTag);
		} */
		IXMLTag lChatTag = saveChat(aChatTag);
		renderChat(lChatTag);
	}

	protected List<IERunGroupAccount> getOnlineRunGroupAccounts() {
		List<IERunGroupAccount> onlineRunGroupAccounts = new ArrayList<IERunGroupAccount>();
		if (!getContentShared() && !getContentSharedbyteam()) {
			// if profile is private return current player
			onlineRunGroupAccounts.add(CDesktopComponents.sSpring().getRunGroupAccount());
			return onlineRunGroupAccounts;
		}
		else if (getContentShared()) {
			// if profile is shared return number of online players in run
			onlineRunGroupAccounts = CDesktopComponents.sSpring().getActiveRunGroupAccountsWithinRun();
		}
		else {
			// if profile is shared within team return number of online players in team
			onlineRunGroupAccounts = CDesktopComponents.sSpring().getActiveRunGroupAccountsWithinRunTeam();
		}
		return onlineRunGroupAccounts;
	}

	protected List<IERunGroupAccount> getRunGroupAccounts() {
		if (runGroupAccounts == null) {
			runGroupAccounts = new ArrayList<IERunGroupAccount>();
			if (!getContentShared() && !getContentSharedbyteam()) {
				// if profile is private return current player
				runGroupAccounts.add(CDesktopComponents.sSpring().getRunGroupAccount());
				return runGroupAccounts;
			}
			else if (getContentShared()) {
				// if profile is shared return number of online players in run
				runGroupAccounts = CDesktopComponents.sSpring().getRunGroupAccountsWithinRun();
			}
			else {
				// if profile is shared within team return number of online players in team
				runGroupAccounts = CDesktopComponents.sSpring().getRunGroupAccountsWithinRunTeam();
			}
		}
		return runGroupAccounts;
	}

	protected List<IERunGroupAccount> getOfflineRunGroupAccounts() {
		List<IERunGroupAccount> offlineRunGroupAccounts = new ArrayList<IERunGroupAccount>();
		List<IERunGroupAccount> onlineRunGroupAccounts = getOnlineRunGroupAccounts();
		List<IERunGroupAccount> runGroupAccounts = getRunGroupAccounts();
		for (IERunGroupAccount lRunGroupAccount : runGroupAccounts) {
			boolean lFound = false;
			for (IERunGroupAccount lOnlineRunGroupAccount : onlineRunGroupAccounts) {
				if (lOnlineRunGroupAccount.getRgaId() == lRunGroupAccount.getRgaId()) {
					lFound = true;
				}
			}
			if (!lFound) {
				offlineRunGroupAccounts.add(lRunGroupAccount);
			}
		}
		return offlineRunGroupAccounts;
	}

	protected int getNumberOfOnlinePlayers() {
		return getOnlineRunGroupAccounts().size();
	}

	protected String getNumberOfOnlinePlayersStr() {
		int lNumberOfOnlinePlayers = getNumberOfOnlinePlayers();
		String lStr = "" + lNumberOfOnlinePlayers + " ";
		if (lNumberOfOnlinePlayers == 1) {
			lStr += CDesktopComponents.vView().getLabel("run_profile.player_online");
		}
		else {
			lStr += CDesktopComponents.vView().getLabel("run_profile.players_online");
		}
		return lStr;
	}

	protected boolean isChatComponentUsed() {
		// NOTE Check if chat component is used and present
		boolean lIsComponentUsed = runWnd.isComponentUsed("chat");
		boolean lPresent = false;
		if (lIsComponentUsed) {
			IECaseComponent lCaseComponent = CDesktopComponents.sSpring().getCaseComponent("chat", "");
			if (lCaseComponent != null) {
				lPresent = ((CDesktopComponents.sSpring().getCurrentRunComponentStatus(
						lCaseComponent, AppConstants.statusKeyPresent, AppConstants.statusTypeRunGroup)).equals(AppConstants.statusValueTrue));
			}
		}
		return lIsComponentUsed && lPresent;
	}

	protected String[] getPlayer() {
		String[] lData = new String[4];
		lData[0] = "" + CDesktopComponents.sSpring().getRunGroupAccount().getRgaId();
		lData[1] = getAvatarSrc(CDesktopComponents.sSpring().getRunGroupAccount());
		lData[2] = CDesktopComponents.sSpring().getRunGroup().getName(); 
		lData[3] = CDesktopComponents.sSpring().getSCaseRoleHelper().getCaseRole().getName(); 
		return lData;
	}

	protected List<String[]> getOtherOnlinePlayers() {
		List<String[]> lDataList = new ArrayList<String[]>();
		List<IERunGroupAccount> onlineRunGroupAccounts = getOnlineRunGroupAccounts();
		for (IERunGroupAccount lRunGroupAccount : onlineRunGroupAccounts) {
			if (lRunGroupAccount.getRgaId() != CDesktopComponents.sSpring().getRunGroupAccount().getRgaId()) {
				String[] lData = new String[5];
				lData[0] = "" + lRunGroupAccount.getRgaId(); 
				lData[1] = getAvatarSrc(lRunGroupAccount);
				lData[2] = lRunGroupAccount.getERunGroup().getName(); 
				lData[3] = lRunGroupAccount.getERunGroup().getECaseRole().getName();
				lData[4] = "" + hasNewChats("" + lRunGroupAccount.getRgaId());
				lDataList.add(lData);
			}
		}
		return lDataList;
	}

	protected List<String[]> getOtherOfflinePlayers() {
		List<String[]> lDataList = new ArrayList<String[]>();
		List<IERunGroupAccount> offlineRunGroupAccounts = getOfflineRunGroupAccounts();
		for (IERunGroupAccount lRunGroupAccount : offlineRunGroupAccounts) {
			if (lRunGroupAccount.getRgaId() != CDesktopComponents.sSpring().getRunGroupAccount().getRgaId()) {
				String[] lData = new String[4];
				lData[0] = "" + lRunGroupAccount.getRgaId(); 
				lData[1] = getAvatarSrc(lRunGroupAccount);
				lData[2] = lRunGroupAccount.getERunGroup().getName(); 
				lData[3] = lRunGroupAccount.getERunGroup().getECaseRole().getName();
				lDataList.add(lData);
			}
		}
		return lDataList;
	}

	protected boolean isMultiRole() {
		return CDesktopComponents.sSpring().getSCaseRoleHelper().getCaseRoles(false).size() > 1;
	}
	
	public String[] getProfileInfo(String aRgaId) {
		String[] lData = new String[4];
		int lRgaId = 0;
		try {
			lRgaId = Integer.parseInt(aRgaId);
		} catch (NumberFormatException e) {
		}
		if (lRgaId == 0) {
			return lData;
		}
		List<IERunGroupAccount> runGroupAccounts = getRunGroupAccounts();
		for (IERunGroupAccount lRunGroupAccount : runGroupAccounts) {
			if (lRunGroupAccount.getRgaId() == lRgaId) {
				lData[0] = "" + lRunGroupAccount.getRgaId(); 
				lData[1] = getAvatarSrc(lRunGroupAccount);
				lData[2] = lRunGroupAccount.getERunGroup().getName(); 
				lData[3] = getMood(lRunGroupAccount);
			}
		}
		return lData;
	}

	public List<String[]> getProfileDatas(String aRgaId) {
		List<String[]> lDataList = new ArrayList<String[]>();
		int lRgaId = 0;
		try {
			lRgaId = Integer.parseInt(aRgaId);
		} catch (NumberFormatException e) {
		}
		if (lRgaId == 0) {
			return lDataList;
		}
		List<IERunGroupAccount> runGroupAccounts = getRunGroupAccounts();
		for (IERunGroupAccount lRunGroupAccount : runGroupAccounts) {
			if (lRunGroupAccount.getRgaId() == lRgaId) {
				// get score keys, and units from casecomponent data
				// get score values from data of rungroupaccount
				List<IXMLTag> lXmlTags = getContentChildTagsByComponentCode(lRunGroupAccount.getERunGroup().getRugId(), "profile", "score");
				if (lXmlTags != null) {
					for (IXMLTag lXmlTag : lXmlTags) {
						boolean lPresent = !lXmlTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse);;
						if (lPresent) {
							List<Object> lCaseComponentAndTag = getCaseComponentAndTag(lRunGroupAccount, this.caseComponent, lXmlTag.getChild("refstate"));
							if (lCaseComponentAndTag != null) {
								IXMLTag lStateTag = (IXMLTag)lCaseComponentAndTag.get(1);
								String lStateValue = "";
								if (lStateTag != null) {
									lStateValue = lStateTag.getCurrentStatusAttribute(AppConstants.statusKeyValue);
								}
								String[] lData = new String[2];
								lData[0] = CDesktopComponents.sSpring().unescapeXML(lXmlTag.getChildValue("label"));
								lData[1] = lStateValue; 
								String lUnit = CDesktopComponents.sSpring().unescapeXML(lXmlTag.getChildValue("unit"));
								if (!lUnit.equals("")) {
									lData[1] += " " + lUnit;
								}
								lDataList.add(lData);
							}
						}
					}
				}
				
				// get time value from data of rungroupaccount
				String[] lTimeData = new String[2];
				lTimeData[0] = CDesktopComponents.vView().getLabel("run_profile.time");
				String lTime = "";
				double lDoubleTime = CDesktopComponents.sSpring().getCurrentRunComponentStatusTime(lRunGroupAccount.getERunGroup().getRugId(), CDesktopComponents.sSpring().getCaseComponent("case", null), AppConstants.statusKeyCurrentTime);
				if (lDoubleTime >= 0) {
					lTime = "" + Math.round(lDoubleTime / 60);
				}
				lTimeData[1] = lTime + " " + CDesktopComponents.vView().getLabel("run_profile.time.unit"); 
				lDataList.add(lTimeData);
				if (isMultiRole()) {
					// get role
					lTimeData = new String[2];
					lTimeData[0] = CDesktopComponents.vView().getLabel("run_profile.role"); 
					lTimeData[1] = lRunGroupAccount.getERunGroup().getECaseRole().getName(); 
					lDataList.add(lTimeData);
				}
			}
		}
		return lDataList;
	}

	public String getAvatarSrc(IERunGroupAccount aRunGroupAccount) {
		List<IXMLTag> lXmlTags = getContentChildTagsByComponentCode(aRunGroupAccount.getERunGroup().getRugId(), "profile", "avatar");
		// NOTE First avatar tags can be entered by author. Then opened can be set to preselect an avatar.
		// NOTE Latter avatar tags can be added by student. Then the last one is the current avatar.
		IXMLTag lFoundXmlTag = null;
		if (lXmlTags != null) {
			for (IXMLTag lXmlTag : lXmlTags) {
				boolean lPresent = !lXmlTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse);
				boolean lOpened = lXmlTag.getCurrentStatusAttribute(AppConstants.statusKeyOpened).equals(AppConstants.statusValueTrue);
				if ((lPresent && lOpened) ||
						lXmlTag.getAttribute(AppConstants.keyExtended).equals(AppConstants.statusValueTrue)) {
					lFoundXmlTag = lXmlTag;
				}
			}
		}
		if (lFoundXmlTag != null) {
			String lSrc = CDesktopComponents.sSpring().getSBlobHelper().getBlobSrc(lFoundXmlTag.getStatusChildValue("picture"));
			if (!lSrc.equals("")) {
				return lSrc;
			}
		}
		return CDesktopComponents.sSpring().getStyleImgSrc("profile-default-avatar");
	}

	public void saveAvatar(Media aMedia) {
		if (caseComponent == null || aMedia == null) {
			return;
		}
		// avatar is private
		IXMLTag lStatusRootTag = CDesktopComponents.sSpring().getXmlRunStatusTree(caseComponent, AppConstants.statusTypeRunGroup);
		if (lStatusRootTag == null) {
			return;
		}
		IXMLTag lStatusContentTag = lStatusRootTag.getChild(AppConstants.contentElement);
		if (lStatusContentTag == null) {
			return;
		}
		
		IXMLTag lStatusTag = CDesktopComponents.sSpring().getXmlManager().newNodeTag(caseComponent.getEComponent().getXmldefinition(), "avatar");
		IXMLTag lStatusStatusTag = lStatusTag.getChild(AppConstants.statusElement);
		IXMLTag lStatusStatusChildTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag("pid", "avatar");
		lStatusStatusTag.getChildTags().add(lStatusStatusChildTag);
		String aBloId = CDesktopComponents.sSpring().getSBlobHelper().setBlobMedia("", aMedia);
		lStatusStatusChildTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag("picture", aBloId);
		lStatusStatusTag.getChildTags().add(lStatusStatusChildTag);
		List<String[]> lErrors = new ArrayList<String[]>();
		// NOTE Avatar is saved without calling CDesktopComponents.sSpring().setRunTagStatus, because it is user generated content,
		// not related to any data tag. setRunTagStatus is meant to check script, but script does not work on
		// user generated content.
		CDesktopComponents.sSpring().setRunTagStatusValue(lStatusTag, AppConstants.statusKeySaved, AppConstants.statusValueTrue);
		CDesktopComponents.sSpring().getXmlManager().newChildNode(lStatusRootTag, lStatusTag, lStatusContentTag, lErrors);
		CDesktopComponents.sSpring().getSLogHelper().logSetRunTagAction("setruntagstatus", AppConstants.statusTypeRunGroup, CDesktopComponents.sSpring().getXmlManager().unescapeXML(caseComponent.getName()), lStatusTag.getName(),
				lStatusTag.getChildValue("pid"), AppConstants.statusKeySaved, AppConstants.statusValueTrue, false);
		// save status
		CDesktopComponents.sSpring().setSaveRunGroupCaseComponentStatusRootTag(CDesktopComponents.sSpring().getRunGroup(), caseComponent, lStatusRootTag);
	}

	public String getMood(IERunGroupAccount aRunGroupAccount) {
		List<IXMLTag> lXmlTags = getContentChildTagsByComponentCode(aRunGroupAccount.getERunGroup().getRugId(), "profile", "mood");
		// NOTE First mood tags can be entered by author. Then opened can be set to preselect a mood.
		// NOTE Latter mood tags can be added by student. Then the last one is the current mood.
		IXMLTag lFoundXmlTag = null;
		if (lXmlTags != null) {
			for (IXMLTag lXmlTag : lXmlTags) {
				boolean lPresent = !lXmlTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse);
				boolean lOpened = lXmlTag.getCurrentStatusAttribute(AppConstants.statusKeyOpened).equals(AppConstants.statusValueTrue);
				if ((lPresent && lOpened) ||
						lXmlTag.getAttribute(AppConstants.keyExtended).equals(AppConstants.statusValueTrue)) {
					lFoundXmlTag = lXmlTag;
				}
			}
		}
		if (lFoundXmlTag != null) {
			return CDesktopComponents.sSpring().unescapeXML(lFoundXmlTag.getStatusChildValue("text"));
		}
		return CDesktopComponents.vView().getLabel("run_profile.mood");
	}

	public void saveMood(String aMood) {
		if (caseComponent == null) {
			return;
		}
		// mood is private
		IXMLTag lStatusRootTag = CDesktopComponents.sSpring().getXmlRunStatusTree(caseComponent, AppConstants.statusTypeRunGroup);
		if (lStatusRootTag == null) {
			return;
		}
		IXMLTag lStatusContentTag = lStatusRootTag.getChild(AppConstants.contentElement);
		if (lStatusContentTag == null) {
			return;
		}
		IXMLTag lStatusTag = CDesktopComponents.sSpring().getXmlManager().newNodeTag(caseComponent.getEComponent().getXmldefinition(), "mood");
		IXMLTag lStatusStatusTag = lStatusTag.getChild(AppConstants.statusElement);
		IXMLTag lStatusStatusChildTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag("pid", "mood");
		lStatusStatusTag.getChildTags().add(lStatusStatusChildTag);
		lStatusStatusChildTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag("text", CDesktopComponents.sSpring().escapeXML(aMood));
		lStatusStatusTag.getChildTags().add(lStatusStatusChildTag);
		List<String[]> lErrors = new ArrayList<String[]>();
		// NOTE Mood is saved without calling CDesktopComponents.sSpring().setRunTagStatus, because it is user generated content,
		// not related to any data tag. setRunTagStatus is meant to check script, but script does not work on
		// user generated content.
		CDesktopComponents.sSpring().setRunTagStatusValue(lStatusTag, AppConstants.statusKeySaved, AppConstants.statusValueTrue);
		CDesktopComponents.sSpring().getXmlManager().newChildNode(lStatusRootTag, lStatusTag, lStatusContentTag, lErrors);
		CDesktopComponents.sSpring().getSLogHelper().logSetRunTagAction("setruntagstatus", AppConstants.statusTypeRunGroup, CDesktopComponents.sSpring().getXmlManager().unescapeXML(caseComponent.getName()), lStatusTag.getName(),
				lStatusTag.getChildValue("pid"), AppConstants.statusKeySaved, AppConstants.statusValueTrue, false);
		// save status
		CDesktopComponents.sSpring().setSaveRunGroupCaseComponentStatusRootTag(CDesktopComponents.sSpring().getRunGroup(), caseComponent, lStatusRootTag);
	}

	public String[] getChatInfo(String aRgaId) {
		if (aRgaId.equals("")) {
	 		String imageGroupchat = CDesktopComponents.sSpring().getStyleImgSrc("profile-groupchat");
			String[] lData = new String[4];
			lData[0] = ""; 
			lData[1] = imageGroupchat;
			lData[2] = CDesktopComponents.vView().getLabel("run_profile.group_chat"); 
			lData[3] = "";
			return lData;
		}
		else {
			// NOTE is same as profile info
			return getProfileInfo(aRgaId);
		}
	}

	public boolean hasNewChats() {
		IECaseComponent lCaseComponent = CDesktopComponents.sSpring().getCaseComponent("chat", "");
		if (lCaseComponent == null) {
			return false;
		}
		// chat is private
		IXMLTag lStatusRootTag = CDesktopComponents.sSpring().getXmlRunStatusTree(lCaseComponent, AppConstants.statusTypeRunGroup);
		if (lStatusRootTag == null) {
			return false;
		}
		IXMLTag lStatusContentTag = lStatusRootTag.getChild(AppConstants.contentElement);
		if (lStatusContentTag == null) {
			return false;
		}
		List<IXMLTag> lChatTags = lStatusContentTag.getChilds("chat");
		boolean lHasNewChats = false;
		for (IXMLTag lChatTag : lChatTags) {
			IXMLTag lChatStatusTag = lChatTag.getChild(AppConstants.statusElement);
			String lSenderRgaId = lChatStatusTag.getChildValue("senderid");
			int lRgaId = 0;
			try {
				lRgaId = Integer.parseInt(lSenderRgaId);
			} catch (NumberFormatException e) {
			}
			if (lRgaId > 0) {
				boolean lOpened = lChatTag.getCurrentStatusAttribute(AppConstants.statusKeyOpened).equals(AppConstants.statusValueTrue);
				if (!lOpened) {
					lHasNewChats = true;
				}
			}
		}
		return lHasNewChats;
	}

	public boolean hasNewChats(String aChatWithRgaId) {
		boolean lGetGroupChats = aChatWithRgaId.equals("");
		IECaseComponent lCaseComponent = CDesktopComponents.sSpring().getCaseComponent("chat", "");
		if (lCaseComponent == null) {
			return false;
		}
		// chat is private
		IXMLTag lStatusRootTag = CDesktopComponents.sSpring().getXmlRunStatusTree(lCaseComponent, AppConstants.statusTypeRunGroup);
		if (lStatusRootTag == null) {
			return false;
		}
		IXMLTag lStatusContentTag = lStatusRootTag.getChild(AppConstants.contentElement);
		if (lStatusContentTag == null) {
			return false;
		}
		List<IXMLTag> lChatTags = lStatusContentTag.getChilds("chat");
		boolean lHasNewChats = false;
		for (IXMLTag lChatTag : lChatTags) {
			IXMLTag lChatStatusTag = lChatTag.getChild(AppConstants.statusElement);
			String lSenderRgaId = lChatStatusTag.getChildValue("senderid");
			int lRgaId = 0;
			try {
				lRgaId = Integer.parseInt(lSenderRgaId);
			} catch (NumberFormatException e) {
			}
			if (lRgaId > 0) {
				String lReceiverRgaId = lChatStatusTag.getChildValue("receiverid");
				boolean lIsGroupChat = lGetGroupChats && lReceiverRgaId.equals("");
				boolean lIsFromRgaIdToChatWithRgaId =
					!lIsGroupChat && lReceiverRgaId.equals("" + CDesktopComponents.sSpring().getRunGroupAccount().getRgaId()) && lSenderRgaId.equals(aChatWithRgaId);
				boolean lIsFromChatWithRgaIdToRgaId =
					!lIsGroupChat && lReceiverRgaId.equals(aChatWithRgaId) && lSenderRgaId.equals("" + CDesktopComponents.sSpring().getRunGroupAccount().getRgaId());
				if (lIsGroupChat || lIsFromRgaIdToChatWithRgaId || lIsFromChatWithRgaIdToRgaId) {
					boolean lOpened = lChatTag.getCurrentStatusAttribute(AppConstants.statusKeyOpened).equals(AppConstants.statusValueTrue);
					if (!lOpened) {
						lHasNewChats = true;
					}
				}
			}
		}
		return lHasNewChats;
	}

	public List<String[]> getChatDatas(String aChatWithRgaId) {
		boolean lGetGroupChats = aChatWithRgaId.equals("");
		List<String[]> lDataList = new ArrayList<String[]>();
		IECaseComponent lCaseComponent = CDesktopComponents.sSpring().getCaseComponent("chat", "");
		if (lCaseComponent == null) {
			return lDataList;
		}
		// chat is private
		IXMLTag lStatusRootTag = CDesktopComponents.sSpring().getXmlRunStatusTree(lCaseComponent, AppConstants.statusTypeRunGroup);
		if (lStatusRootTag == null) {
			return lDataList;
		}
		IXMLTag lStatusContentTag = lStatusRootTag.getChild(AppConstants.contentElement);
		if (lStatusContentTag == null) {
			return lDataList;
		}
		List<IXMLTag> lChatTags = lStatusContentTag.getChilds("chat");
		boolean lIsStatusChanged = false;
		for (IXMLTag lChatTag : lChatTags) {
			IXMLTag lChatStatusTag = lChatTag.getChild(AppConstants.statusElement);
			String lSenderRgaId = lChatStatusTag.getChildValue("senderid");
			int lRgaId = 0;
			try {
				lRgaId = Integer.parseInt(lSenderRgaId);
			} catch (NumberFormatException e) {
			}
			if (lRgaId > 0) {
				String lReceiverRgaId = lChatStatusTag.getChildValue("receiverid");
				boolean lIsGroupChat = lGetGroupChats && lReceiverRgaId.equals("");
				boolean lIsFromRgaIdToChatWithRgaId =
					!lIsGroupChat && lSenderRgaId.equals("" + CDesktopComponents.sSpring().getRunGroupAccount().getRgaId()) && lReceiverRgaId.equals(aChatWithRgaId);
				boolean lIsFromChatWithRgaIdToRgaId =
					!lIsGroupChat && lSenderRgaId.equals(aChatWithRgaId) && lReceiverRgaId.equals("" + CDesktopComponents.sSpring().getRunGroupAccount().getRgaId());
				if (lIsGroupChat || lIsFromRgaIdToChatWithRgaId || lIsFromChatWithRgaIdToRgaId) {
					String[] lData = new String[3];
					lData[0] = CDesktopComponents.sSpring().getRunGroupAccount(lRgaId).getERunGroup().getName();
					String lTimeStr = lChatStatusTag.getChildValue("time");
					long lTime = 0;
					try {
						lTime = Long.parseLong(lTimeStr);
					} catch (NumberFormatException e) {
					}
					if (lTime == 0) {
						lTimeStr = "";
					}
					else {
						Date lDate = new Date(lTime);
						SimpleDateFormat lFormatter = new SimpleDateFormat("dd-MM-yyyy");
						String lFDateStr = lFormatter.format(lDate);
						lFormatter = new SimpleDateFormat("HH:mm");
						String lFTimeStr = lFormatter.format(lDate);
						lTimeStr = lFDateStr;
						lTimeStr += " " + CDesktopComponents.vView().getLabel("run_profile.at") + " ";
						lTimeStr += lFTimeStr;
						lTimeStr += CDesktopComponents.vView().getLabel("run_profile.h");
					}
					lData[1] = lTimeStr; 
					lData[2] = CDesktopComponents.sSpring().unescapeXML(lChatStatusTag.getChildValue("text"));
					lData[2] = lData[2].replaceAll("\n", "<br/>");
					lDataList.add(lData);

					// If opened not set, set it because all chats are opened at the same time
					boolean lOpened = lChatTag.getCurrentStatusAttribute(AppConstants.statusKeyOpened).equals(AppConstants.statusValueTrue);
					if (!lOpened) {
						CDesktopComponents.sSpring().setRunTagStatusValue(lChatTag, AppConstants.statusKeyOpened, AppConstants.statusValueTrue);
						lIsStatusChanged = true;
					}
				}
			}
		}
		if (lIsStatusChanged) {
			// save status
			CDesktopComponents.sSpring().setSaveRunGroupCaseComponentStatusRootTag(CDesktopComponents.sSpring().getRunGroup(), lCaseComponent, lStatusRootTag);
		}
		return lDataList;
	}

	public void sendChat(String aReceiverRgaId, String aMessage) {
		IECaseComponent lCaseComponent = CDesktopComponents.sSpring().getCaseComponent("chat", "");
		if (lCaseComponent == null) {
			return;
		}
		// create chat xml tag
		IXMLTag lStatusTag = CDesktopComponents.sSpring().getXmlManager().newNodeTag(lCaseComponent.getEComponent().getXmldefinition(), "chat");
		IXMLTag lStatusStatusTag = lStatusTag.getChild(AppConstants.statusElement);
		IXMLTag lStatusStatusChildTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag("pid", "chat");
		lStatusStatusTag.getChildTags().add(lStatusStatusChildTag);
		lStatusStatusChildTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag("senderid", "" + CDesktopComponents.sSpring().getRunGroupAccount().getRgaId());
		lStatusStatusTag.getChildTags().add(lStatusStatusChildTag);
		lStatusStatusChildTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag("receiverid", aReceiverRgaId);
		lStatusStatusTag.getChildTags().add(lStatusStatusChildTag);
		lStatusStatusChildTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag("text", CDesktopComponents.sSpring().escapeXML(aMessage));
		lStatusStatusTag.getChildTags().add(lStatusStatusChildTag);
		lStatusStatusChildTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag("time", "" + System.currentTimeMillis());
		lStatusStatusTag.getChildTags().add(lStatusStatusChildTag);
		String lSentValue = AppConstants.statusValueTrue + "," + CDesktopComponents.sSpring().getCaseTime();
		lStatusStatusTag.setAttribute(AppConstants.statusKeySent, lSentValue);
		
		// publish chat xml tag
		getChatEventQueue("" + CDesktopComponents.sSpring().getRunGroupAccount().getRgaId(), aReceiverRgaId).publish(new Event("onChat", null, lStatusTag));
		
		if (aReceiverRgaId.equals("")) {
			// offline players have to get group chats too but are not subscribed to the chat queue 
			publishForOfflinePlayers(lStatusTag);
		}
	}
	
	protected void publishForOfflinePlayers(IXMLTag aStatusTag) {
		IXMLTag lClonedStatusTag = CDesktopComponents.sSpring().getXmlManager().copyTag(aStatusTag, null);
		lClonedStatusTag.setAttribute(AppConstants.processLocally, AppConstants.statusValueTrue);
		List<IERunGroupAccount> offlineRunGroupAccounts = getOfflineRunGroupAccounts();
		for (IERunGroupAccount lRunGroupAccount : offlineRunGroupAccounts) {
			if (lRunGroupAccount.getRgaId() != CDesktopComponents.sSpring().getRunGroupAccount().getRgaId()) {
				int lCacIdForReceiver = pGetCacIdForReceiver(caseComponent, lRunGroupAccount.getERunGroup());
				if (lCacIdForReceiver != 0)
					CDesktopComponents.sSpring().getSUpdateHelper().addUpdateRunTag(lRunGroupAccount.getERunGroup().getRugId(), lCacIdForReceiver, lClonedStatusTag, AppConstants.statusTypeRunGroup, false);
			}
		}
	}
	
	private int pGetCacIdForReceiver(IECaseComponent aCaseComponentForSender, IERunGroup aRunGroupReceiver) {
		String lComponentCode = aCaseComponentForSender.getEComponent().getCode();
		List<IECaseComponentRole> lCaseComponentRolesForReceiver = 
				CDesktopComponents.sSpring().getCaseComponentRolesByCarId(aRunGroupReceiver.getECaseRole().getCarId());
		for (IECaseComponentRole lCaseComponentRoleForSender : lCaseComponentRolesForReceiver) {
			IECaseComponent lCaseComponent = CDesktopComponents.sSpring().getCaseComponent(lCaseComponentRoleForSender.getCacCacId());
			if (lCaseComponent.getEComponent().getCode().equals(lComponentCode))
				// only one e-messages per case role!
				return lCaseComponentRoleForSender.getCacCacId();
		}
		return 0;
	}

	//NOTE updateGroupChatTags() can be called from within updateGroupChatTag(rgu) resulting in stack overflow
	//So to prevent this code below is changed
	private int updateCounter = 0;
	private boolean updateBusy = false;
	
	public void updateGroupChatTags() {
		updateCounter ++;
		if (!updateBusy) {
			updateBusy = true;
			pUpdateGroupChatTags();
			updateBusy = false;
			updateCounter --;
			if (updateCounter > 0) {
				updateCounter --;
				updateGroupChatTags();
			}
		}
	}

	protected void pUpdateGroupChatTags() {
		IERunGroup lRunGroup = CDesktopComponents.sSpring().getRunGroup();
		if (lRunGroup == null)
			return;
//		get not processed records
		IRunGroupCaseComponentUpdateManager rguManager = (IRunGroupCaseComponentUpdateManager)CDesktopComponents.sSpring().getBean("runGroupCaseComponentUpdateManager");
		List<IERunGroupCaseComponentUpdate> rgus = rguManager.getAllRunGroupCaseComponentUpdatesByRugId(lRunGroup.getRugId(), false);
		if (rgus.size() == 0)
//			no not processed records
			return;
		for (IERunGroupCaseComponentUpdate rgu : rgus) {
			if (updateGroupChatTag(rgu)) {
				rgu.setProcessed(true);
				rguManager.updateRunGroupCaseComponentUpdate(rgu);
			}
		}
	}

	public boolean updateGroupChatTag(IERunGroupCaseComponentUpdate rgu) {
		boolean lOk = false;
		IXMLTag lStatusUpdateRootTag = CDesktopComponents.sSpring().getSUpdateHelper().getXmlRunGroupStatusUpdateTree(rgu);
		if (lStatusUpdateRootTag != null) {
			IXMLTag lContentTag = lStatusUpdateRootTag.getChild(AppConstants.contentElement);
			if (lContentTag != null) {
				List<IXMLTag> lChildTags = lContentTag.getChildTags();
				if (lChildTags.size() == 1) {
					IXMLTag lStatusUpdateTag = (IXMLTag)lChildTags.get(0);
					boolean lProcessLocally = lStatusUpdateTag.getAttribute(AppConstants.processLocally).equals(AppConstants.statusValueTrue);
					if (lProcessLocally) {
						// NOTE if processLocally is set a component itself takes care of updating run tags
						IECaseComponent lCaseComponent = CDesktopComponents.sSpring().getCaseComponentInRun(rgu.getCacCacId());
						String lTagName = lStatusUpdateTag.getName();
						if (lCaseComponent.getCacId() == caseComponent.getCacId() && lTagName.equals("chat")) {
							handleChatEvent(lStatusUpdateTag);
							lOk = true;
						}
					}
				}
			}
		}
		return lOk;
	}

	protected IXMLTag saveChat(IXMLTag aChatTag) {
		IECaseComponent lCaseComponent = CDesktopComponents.sSpring().getCaseComponent("chat", "");
		if (lCaseComponent == null) {
			return aChatTag;
		}
		// chat is private
		IXMLTag lStatusRootTag = CDesktopComponents.sSpring().getXmlRunStatusTree(lCaseComponent, AppConstants.statusTypeRunGroup);
		if (lStatusRootTag == null) {
			return aChatTag;
		}
		IXMLTag lStatusContentTag = lStatusRootTag.getChild(AppConstants.contentElement);
		if (lStatusContentTag == null) {
			return aChatTag;
		}
		List<String[]> lErrors = new ArrayList<String[]>();
		// NOTE copy chat tag so each rga gets his own one
		aChatTag = CDesktopComponents.sSpring().getXmlManager().copyTag(aChatTag, null);
		CDesktopComponents.sSpring().getXmlManager().newChildNode(lStatusRootTag, aChatTag, lStatusContentTag, lErrors);
		// save status
		CDesktopComponents.sSpring().setSaveRunGroupCaseComponentStatusRootTag(CDesktopComponents.sSpring().getRunGroup(), lCaseComponent, lStatusRootTag);
		return aChatTag;
	}


	/**
	 * Gets the run component helper.
	 *
	 * @return the run component helper
	 */
	@Override
	public CRunComponentHelper getRunComponentHelper() {
		return new CRunComponentHelper(null, "score", caseComponent, this);
	}

	/**
	 * Gets all scores.
	 */
	public List<IXMLTag> getScores() {
		CRunComponentHelper cComponent = (CRunComponentHelper)getRunComponentHelper();
		IXMLTag lXmlTree = cComponent.getXmlDataPlusStatusTree();
		if (lXmlTree != null) {
			return lXmlTree.getChild(AppConstants.contentElement).getChilds("score");
		}
		else {
			return new ArrayList<IXMLTag>();
		}
	}

	/**
	 * Gets all formulas.
	 */
	public List<IXMLTag> getFormulas() {
		CRunComponentHelper cComponent = (CRunComponentHelper)getRunComponentHelper();
		IXMLTag lXmlTree = cComponent.getXmlDataPlusStatusTree();
		if (lXmlTree != null) {
			return lXmlTree.getChild(AppConstants.contentElement).getChilds("formula");
		}
		else {
			return new ArrayList<IXMLTag>();
		}
	}

	/**
	 * Gets referenced case component and tag.
	 *
	 * @param aCaseComponent the a case component
	 * @param aRunGroupAccount the a run group account
	 * @param aRefTag the a ref tag
	 *
	 * @return list of case component and tag
	 */
	public List<Object> getCaseComponentAndTag(IERunGroupAccount aRunGroupAccount, IECaseComponent aCaseComponent, IXMLTag aRefTag) {
		if (aRefTag == null) {
			return null;
		}
		List<Object> result = new ArrayList<Object>();
		IECaseComponent lCaseComponent = null;
		IXMLTag lXmlTag = null;
		
		IERunGroupAccount lRunGroupAccount = aRunGroupAccount;
		if (lRunGroupAccount == null) {
			lRunGroupAccount = CDesktopComponents.sSpring().getRunGroupAccount();
		}
		if (lRunGroupAccount == null) {
			return null;
		}
		
		String lReftype = aRefTag.getDefAttribute(AppConstants.defKeyReftype);
		CCaseHelper cCaseHelper = new CCaseHelper();
		IECaseRole lCaseRole = lRunGroupAccount.getERunGroup().getECaseRole();
		List<String> lRefIds = cCaseHelper.getRefTagIds(lReftype, "" + AppConstants.statusKeySelectedIndex, lCaseRole, aCaseComponent, aRefTag.getParentTag());
		if (lRefIds.size() > 0) {
			String lRefId = (String)lRefIds.get(0);
			if ((lRefId != null) && (!lRefId.equals(""))) {
					String[] lIdArr = lRefId.split(",");
				if (lIdArr.length == 3) {
					String lCacId = lIdArr[1];
					lCaseComponent = CDesktopComponents.sSpring().getCaseComponent(lCacId);
					String lTagId = lIdArr[2];
					// NOTE Referenced casecomponent states is of type statusTypeRunGroup
					lXmlTag = CDesktopComponents.sSpring().getXmlDataPlusRunStatusTag(lRunGroupAccount.getERunGroup().getRugId(), CDesktopComponents.sSpring().getCaseComponent(Integer.parseInt(lCacId)), lTagId);
				}
			}
		}
		result.add(lCaseComponent);
		result.add(lXmlTag);
		return result;
	}

	public List<IXMLTag> getContentChildTagsByComponentCode(int aRugId, String aComponentCode, String aTagName) {
		List<IXMLTag> lXmlTags = new ArrayList<IXMLTag>();
		IXMLTag lRootTag = CDesktopComponents.sSpring().getXmlDataPlusRunGroupStatusTree(aRugId, CDesktopComponents.sSpring().getCaseComponent(aComponentCode, null));
		if (lRootTag != null) {
			IXMLTag lContentRootTag = lRootTag.getChild(AppConstants.contentElement);
			if (lContentRootTag != null) {
				lXmlTags = lContentRootTag.getChilds(aTagName);
			}
		}
		return lXmlTags;
	}

	public List<IXMLTag> getContentChildTags(int aRugId, IECaseComponent aCaseComponent, String aTagName) {
		List<IXMLTag> lXmlTags = new ArrayList<IXMLTag>();
		IXMLTag lRootTag = CDesktopComponents.sSpring().getXmlDataPlusRunGroupStatusTree(aRugId, aCaseComponent);
		if (lRootTag != null) {
			IXMLTag lContentRootTag = lRootTag.getChild(AppConstants.contentElement);
			if (lContentRootTag != null) {
				lXmlTags = lContentRootTag.getChilds(aTagName);
			}
		}
		return lXmlTags;
	}

	protected void renderHasNewChats(boolean aHasNewChats) {
 		String imageHasNewChatsId = "imageHasNewChats";
 		Image imageHasNewChats = (Image)CDesktopComponents.vView().getComponent(imageHasNewChatsId);
 		if (imageHasNewChats != null) {
 	 		String imageHasNewChatsSrc = "";
 	 		if (aHasNewChats) {
 	 			imageHasNewChatsSrc = CDesktopComponents.sSpring().getStyleImgSrc("profile-newchats");
 	 		}
 	 		else {
 	 			imageHasNewChatsSrc = CDesktopComponents.sSpring().getStyleImgSrc("profile-no-newchats");
 	 		}
 			imageHasNewChats.setSrc(imageHasNewChatsSrc);
 		}
	}

	protected void renderNumberOfOnlinePlayers() {
 		String labelNumberOfOnlinePlayersId = "labelNumberOfOnlinePlayers";
 		Label labelNumberOfOnlinePlayers = (Label)CDesktopComponents.vView().getComponent(labelNumberOfOnlinePlayersId);
 		if (labelNumberOfOnlinePlayers != null) {
 			labelNumberOfOnlinePlayers.setValue(getNumberOfOnlinePlayersStr());
 		}
	}

	protected boolean isKnownRunGroupAccount(String aRgaId) {
		int lRgaId = 0;
		try {
			lRgaId = Integer.parseInt(aRgaId);
		} catch (NumberFormatException e) {
		}
		for (IERunGroupAccount lRunGroupAccount : getRunGroupAccounts()) {
			if (lRunGroupAccount.getRgaId() == lRgaId) {
				return true;
			}
		}
		return false;
	}

	protected boolean isCurrentRunGroupAccount(String aRgaId) {
		int lRgaId = 0;
		try {
			lRgaId = Integer.parseInt(aRgaId);
		} catch (NumberFormatException e) {
		}
		return lRgaId == CDesktopComponents.sSpring().getRunGroupAccount().getRgaId();
	}

	protected boolean isWithinCurrentRun(String aRgaId) {
		int lRgaId = 0;
		try {
			lRgaId = Integer.parseInt(aRgaId);
		} catch (NumberFormatException e) {
		}
		IERunGroupAccount lRunGroupAccount = CDesktopComponents.sSpring().getRunGroupAccount(lRgaId);
		return (lRunGroupAccount != null && lRunGroupAccount.getERunGroup().getERun().getRunId() == CDesktopComponents.sSpring().getRunGroupAccount().getERunGroup().getERun().getRunId());
	}

	protected boolean isWithinCurrentRunTeam(String aRgaId) {
		int lRgaId = 0;
		try {
			lRgaId = Integer.parseInt(aRgaId);
		} catch (NumberFormatException e) {
		}
		List<IERunGroupAccount> lRunGroupAccountsWithinCurrentRunTeam = CDesktopComponents.sSpring().getRunGroupAccountsWithinRunTeam();
		for (IERunGroupAccount lRunGroupAccountWithinCurrentRunTeam : lRunGroupAccountsWithinCurrentRunTeam) {
			if (lRunGroupAccountWithinCurrentRunTeam.getRgaId() == lRgaId) {
				return true;
			}
		}
		return false;
	}

	protected void renderOnlineProfiles() {
 		String macroOnlineProfilesId = "macroOnlineProfiles";
		HtmlMacroComponent macroOnlineProfiles = (HtmlMacroComponent)CDesktopComponents.vView().getComponent(macroOnlineProfilesId);
		macroOnlineProfiles.setDynamicProperty("a_visible", getOtherOnlinePlayers().size() > 0);
		macroOnlineProfiles.setDynamicProperty("a_profile_otheronlineplayers", getOtherOnlinePlayers());
		macroOnlineProfiles.setDynamicProperty("a_has_chatcomponent", isChatComponentUsed());
		macroOnlineProfiles.recreate();
	}

	protected void renderOfflineProfiles() {
 		String macroOfflineProfilesId = "macroOfflineProfiles";
 		HtmlMacroComponent macroOfflineProfiles = (HtmlMacroComponent)CDesktopComponents.vView().getComponent(macroOfflineProfilesId);
		macroOfflineProfiles.setDynamicProperty("a_visible", getOtherOfflinePlayers().size() > 0);
		macroOfflineProfiles.setDynamicProperty("a_profile_otherofflineplayers", getOtherOfflinePlayers());
		macroOfflineProfiles.recreate();
	}

	protected void renderGroupChat() {
 		String divGroupChatContainerId = "divGroupChatContainer";
 		Div divGroupChatContainer = (Div)CDesktopComponents.vView().getComponent(divGroupChatContainerId);
 		if (divGroupChatContainer != null) {
 			divGroupChatContainer.setVisible(isChatComponentUsed() && getOtherOnlinePlayers().size() > 0);
 		}
 		String imageGroupChatIconId = "imageGroupChatIcon";
 		Image imageGroupChatIcon = (Image)CDesktopComponents.vView().getComponent(imageGroupChatIconId);
 		if (imageGroupChatIcon != null) {
 			imageGroupChatIcon.setVisible(hasNewChats(""));
 		}
	}

	public void profilesIsOpened() {
		CDesktopComponents.sSpring().setRunComponentStatus(caseComponent,
				AppConstants.statusKeySelected,
				AppConstants.statusValueTrue, true, AppConstants.statusTypeRunGroup, true);
		CDesktopComponents.sSpring().setRunComponentStatus(caseComponent,
				AppConstants.statusKeyOpened,
				AppConstants.statusValueTrue, true, AppConstants.statusTypeRunGroup, true);
	}
	
	public void profilesIsClosed() {
		CDesktopComponents.sSpring().setRunComponentStatus(caseComponent,
				AppConstants.statusKeyOpened,
				AppConstants.statusValueFalse, true, AppConstants.statusTypeRunGroup, true);
	}
	
	public void renderProfiles() {
		// NOTE Add pending group chats send when the rga was offline.
		// Could be he missed one, if it was sent just (less then 30 seconds) before the rga started the player.
		// So to be sure do update here too.
		updateGroupChatTags();

		String divProfilesId = "divProfiles";
 		Div divProfiles = (Div)CDesktopComponents.vView().getComponent(divProfilesId);
 		if (divProfiles == null || !divProfiles.isVisible()) {
 			return;
 		}
		renderOnlineProfiles();
		renderOfflineProfiles();
		renderGroupChat();
	}

	public void renderProfile(String aRgaId) {
 		String divProfileId = "divProfile";
 		Div divProfile = (Div)CDesktopComponents.vView().getComponent(divProfileId);
 		if (divProfile == null || !divProfile.isVisible()) {
 			return;
 		}
 		String macroProfileInfoId = "macroProfileInfo";
 		HtmlMacroComponent macroProfileInfo = (HtmlMacroComponent)CDesktopComponents.vView().getComponent(macroProfileInfoId);
 		macroProfileInfo.setDynamicProperty("a_profile_info", getProfileInfo(aRgaId));
 		macroProfileInfo.recreate();
 		String macroProfileDatasId = "macroProfileDatas";
 		HtmlMacroComponent macroProfileDatas = (HtmlMacroComponent)CDesktopComponents.vView().getComponent(macroProfileDatasId);
 		macroProfileDatas.setDynamicProperty("a_profile_datas", getProfileDatas(aRgaId));
 		macroProfileDatas.recreate();
 		
 		boolean isCurrentRunGroupAccount = isCurrentRunGroupAccount(aRgaId);
 		String buttonProfileAvatarUploadId = "buttonProfileAvatarUpload";
 		Button buttonProfileAvatarUpload = (Button)CDesktopComponents.vView().getComponent(buttonProfileAvatarUploadId);
 		if (buttonProfileAvatarUpload != null) {
 			buttonProfileAvatarUpload.setVisible(isCurrentRunGroupAccount);
 		}
 		String labelProfileMoodId = "labelProfileMood";
 		Label labelProfileMood = (Label)CDesktopComponents.vView().getComponent(labelProfileMoodId);
 		if (labelProfileMood != null) {
 			labelProfileMood. setVisible(!isCurrentRunGroupAccount);
 		}
 		String textboxProfileMoodId = "textboxProfileMood";
 		Textbox textboxProfileMood = (Textbox)CDesktopComponents.vView().getComponent(textboxProfileMoodId);
 		if (textboxProfileMood != null) {
 			textboxProfileMood.setVisible(isCurrentRunGroupAccount);
 		}
	}

	public void renderChats(String aChatWithRgaId) {
 		String divChatId = "divChat";
 		Div divChat = (Div)CDesktopComponents.vView().getComponent(divChatId);
 		if (divChat == null) {
 			return;
 		}
 		if (!divChat.isVisible()) {
 			return;
 		}
 		if (!divChat.getAttribute("rgaId").equals("") && !divChat.getAttribute("rgaId").equals(aChatWithRgaId)) {
 			return;
 		}
 		String macroChatInfoId = "macroChatInfo";
 		HtmlMacroComponent macroChatInfo = (HtmlMacroComponent)CDesktopComponents.vView().getComponent(macroChatInfoId);
 		macroChatInfo.setDynamicProperty("a_chat_info", getChatInfo(aChatWithRgaId));
 		macroChatInfo.recreate();
 		String macroChatDatasId = "macroChatDatas";
 		HtmlMacroComponent macroChatDatas = (HtmlMacroComponent)CDesktopComponents.vView().getComponent(macroChatDatasId);
 		macroChatDatas.setDynamicProperty("a_chat_datas", getChatDatas(aChatWithRgaId));
 		macroChatDatas.recreate();

 		// scrolls div containing chats to last chat
 		Clients.evalJavaScript("scrollToLastChat();");
 		
 		// if not new chats left render new chat image false
 		if (hasNewChats && !hasNewChats()) {
 			hasNewChats = false;
			renderHasNewChats(hasNewChats);
 		}
	}

	protected void renderChat(IXMLTag aChatTag) {
		// NOTE If receiverid is empty render group chat otherwise render private chat
		String lSenderRgaId = aChatTag.getChild(AppConstants.statusElement).getChildValue("senderid");
		String lReceiverRgaId = aChatTag.getChild(AppConstants.statusElement).getChildValue("receiverid");
		boolean lIsSenderIsCurrentRga = lSenderRgaId.equals("" + CDesktopComponents.sSpring().getRunGroupAccount().getRgaId());

		// show chat notify?
		if (!lIsSenderIsCurrentRga) {
			// render new chat audio
			renderHasNewChatsAudio();
			// render new chat image true if not already true
			if (!hasNewChats) {
				hasNewChats = true;
				renderHasNewChats(hasNewChats);
			}
	 		// rerender profiles if new chat
	 		renderProfiles();
		}
		else {
			// set opened for current rga
			String lOpenedValue = AppConstants.statusValueTrue + "," + CDesktopComponents.sSpring().getCaseTime();
			aChatTag.getChild(AppConstants.statusElement).setAttribute(AppConstants.statusKeyOpened, lOpenedValue);
		}
		
		// add chat to chat vbox
		String lChatWithRgaId = "";
		if (lReceiverRgaId.equals("")) {
			// NOTE group chat
			lChatWithRgaId = "";
		}
		else {
			if (lIsSenderIsCurrentRga) {
				// if current rga, receiver is receiving
				lChatWithRgaId = lReceiverRgaId;
			}
			else {
				// else sender is receiving
				lChatWithRgaId = lSenderRgaId;
			}
		}
		renderChats(lChatWithRgaId);
	}
	
	protected void renderHasNewChatsAudio() {
 		String includeHasNewChatsId = "includeHasNewChats";
		Include includeHasNewChats = (Include)CDesktopComponents.vView().getComponent(includeHasNewChatsId);
		if (includeHasNewChats == null) {
			return;
		}
		// NOTE have to empty src otherwise no reload
		includeHasNewChats.setSrc("");
		includeHasNewChats.setSrc((String)includeHasNewChats.getAttribute("src"));
	}
	
	/**
	 * Methods originally used in ZUL file.
	 */

	public void onInitZulfile(Event aEvent) {
		Map<String,Object> propertyMap = new HashMap<String,Object>();
		propertyMap.put("currentEmergoComponent", this);
		propertyMap.put("profile_number_of_online_players", getNumberOfOnlinePlayersStr());
		propertyMap.put("profile_has_chatcomponent", "" + isChatComponentUsed());
		hasNewChats = hasNewChats();
		if (hasNewChats) {
			propertyMap.put("profile_has_new_chats_imagesrc", CDesktopComponents.sSpring().getStyleImgSrc("profile-newchats"));
		}
		else {
			propertyMap.put("profile_has_new_chats_imagesrc", CDesktopComponents.sSpring().getStyleImgSrc("profile-no-newchats"));
		}
		propertyMap.put("profile_has_new_group_chats", "" + hasNewChats(""));
		propertyMap.put("profile_player", getPlayer());
		propertyMap.put("profile_otheronlineplayers", getOtherOnlinePlayers());
		propertyMap.put("profile_otherofflineplayers", getOtherOfflinePlayers());
		propertyMap.put("profile_multirole", "" + isMultiRole());
		propertyMap.put("profile_datas", getProfileDatas("0"));
		((HtmlMacroComponent)getRunContentComponent()).setDynamicProperty("a_propertyMap", propertyMap);
		((HtmlMacroComponent)getRunContentComponent()).setMacroURI(VView.v_run_profile_fr);
		getRunContentComponent().setVisible(true);
	}

}
