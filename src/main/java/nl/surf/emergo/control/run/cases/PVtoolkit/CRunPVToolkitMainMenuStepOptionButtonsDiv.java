/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTag;
import nl.surf.emergo.domain.IECaseComponent;

public class CRunPVToolkitMainMenuStepOptionButtonsDiv extends CDefDiv {

	private static final long serialVersionUID = -454885057174560495L;

	public String zulfilepath = ((CRunPVToolkitInitBox)CDesktopComponents.vView().getComponent("PV-toolkit_initBox")).getZulfilepath();
	
	protected CRunPVToolkit pvToolkit;
	
	protected final static int stepOffset = 20;
	protected final static int stepHeight = 40;

	public void onCreate() {
		update();
	}

	public void update() {
		getChildren().clear();
		
		pvToolkit = (CRunPVToolkit)CDesktopComponents.vView().getComponent("pvToolkit");
		
		List<IXMLTag> stepTags = pvToolkit.getCurrentStepTags();
		IECaseComponent caseComponent = pvToolkit.getCurrentProjectsCaseComponent();
		
		int top = stepOffset;
		
		if (pvToolkit.getCurrentStepTag() != null) {
			appendCurrentStepMenuOptionButton(
					this, 
					caseComponent, 
					pvToolkit.getCurrentStepTag(), 
					pvToolkit.getCurrentStepNumber(), 
					pvToolkit.getCurrentStep(),
					false,
					true,
					true,
					top);
			top += stepHeight;
		}
 
		//NOTE only show step menu buttons if role is adm or demo mode is true
		boolean showStepMenuButtons = pvToolkit.isAdmin() || pvToolkit.isDemoMode(); 
		if (!showStepMenuButtons) {
			return;
		}
		
		if (pvToolkit.hasStudentRole || pvToolkit.hasTeacherRole || pvToolkit.hasStudentAssistantRole || pvToolkit.hasPeerStudentRole) {
			Label label = new CRunPVToolkitDefLabel(this, 
					new String[]{"class", "labelKey"}, 
					new Object[]{"font stepMenuOptionsHeaderLabel", "PV-toolkit.mainmenulabel.steps"}
					);
			label.setStyle("top:" + top + "px;");
		}

		top = stepOffset + stepHeight;
		int stepNumber = 1;
		for (IXMLTag stepTag : stepTags) {
			String steptype = stepTag.getChildValue("steptype");
			if (pvToolkit.hasStudentRole || (steptype.equals(CRunPVToolkit.feedbackStepType) && (pvToolkit.hasTeacherRole || pvToolkit.hasStudentAssistantRole || pvToolkit.hasPeerStudentRole))) {
				//NOTE for teachers and peerstudents only show feedback steps
				String step = pvToolkit.getPanelId(steptype);
				if (!step.equals("")) {
					appendStepMenuOptionButton(
							this, 
							caseComponent, 
							stepTag, 
							stepNumber, 
							step,
							true,
							false,
							false,
							top);
				}
			}
			stepNumber++;
		}
		
	}
	
	public Div appendCurrentStepMenuOptionButton(Div parentDiv, IECaseComponent caseComponent, IXMLTag stepTag, int stepNumber, String step, boolean resetPanel, boolean updatePanel, boolean resetOnFirstClick, int top) {
		Div div = appendStepMenuOptionButton(parentDiv, stepTag, stepNumber, step, resetPanel, updatePanel, resetOnFirstClick, top);
		new CRunPVToolkitDefImage(div, 
				new String[]{"class", "src"}, 
				new Object[]{"stepMenuOptionsButtonIcon", zulfilepath + "ballot-white.svg"}
				);
		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "cacAndTag", "labelKey", "maxLabelLength"}, 
				new Object[]{"font stepMenuOptionsButtonLabel", new CRunPVToolkitCacAndTag(caseComponent, stepTag), "PV-toolkit.mainmenulabel.currentstep", 20}
				);
		return div;
	}
	
	public Div appendStepMenuOptionButton(Div parentDiv, IECaseComponent caseComponent, IXMLTag stepTag, int stepNumber, String step, boolean resetPanel, boolean updatePanel, boolean resetOnFirstClick, int top) {
		Div div = appendStepMenuOptionButton(parentDiv, stepTag, stepNumber, step, resetPanel, updatePanel, resetOnFirstClick, top);
		new CRunPVToolkitDefImage(div, 
				new String[]{"class", "src"}, 
				new Object[]{"stepMenuOptionsButtonIcon", zulfilepath + "number_" + stepNumber + "_gray.svg"}
				);
		IXMLTag learningActivityTag = pvToolkit.getLearningActivityChildTag(stepTag);
		if (learningActivityTag != null) {
			Label label = new CRunPVToolkitDefLabel(div, 
					new String[]{"class", "cacAndTag", "tagChildName", "maxLabelLength"}, 
					new Object[]{"font stepMenuOptionsButtonLabel", new CRunPVToolkitCacAndTag(caseComponent, learningActivityTag), "name", 20}
					);
			if (!pvToolkit.hasStudentRole) {
				//NOTE if role is not student remove 'peer'
				label.setValue(pvToolkit.removePeerInString(label.getValue()));
				if (!StringUtils.isEmpty(label.getTooltiptext())) {
					label.setTooltiptext(pvToolkit.removePeerInString(label.getTooltiptext()));
				}
			}
		}
		
		return div;
	}
	
	public Div appendStepMenuOptionButton(Div parentDiv, IXMLTag stepTag, int stepNumber, String step, boolean resetPanel, boolean updatePanel, boolean resetOnFirstClick, int top) {
		Div div = new CRunPVToolkitMainMenuOptionButton();
		parentDiv.appendChild(div);
		Map<String,Object> keyValueMap = new HashMap<String,Object>();
		keyValueMap.put("stepTag", stepTag);
		keyValueMap.put("stepNumber", stepNumber);
		keyValueMap.put("step", step);
		//NOTE resetPanel=true means that the panel will show its first page, as if you visit the panel for the first time 
		//NOTE resetPanel=false means that the panel will show the page on which the user was working the last time he visited the panel during the current session 
		keyValueMap.put("resetPanel", resetPanel);
		keyValueMap.put("updatePanel", updatePanel);
		keyValueMap.put("resetOnFirstClick", resetOnFirstClick);
		div.setAttribute("keyValueMap", keyValueMap);

		div.setClass(step + "Button");
		div.setStyle("top:" + top + "px;");
		return div;
	}
	
}
