/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.IP2;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;

public class CRunDashboardSatisfactionDiv extends CDefDiv {

	private static final long serialVersionUID = 6068514222553140626L;
	
	protected int numberOfMiniGames = 16;
	protected int xOffset = 20;
	protected int yOffset = 70;
	protected int columnWidth = 245;
	protected int rowHeight = 125;

	public void onInit(Event aEvent) {
		CRunDashboardDiv runDashboardDiv = (CRunDashboardDiv)CDesktopComponents.vView().getComponent("dashboardDiv");
		if (runDashboardDiv == null) {
			return;
		}
		if (getAttribute("initialized") == null) {
			int x = xOffset;
			int y = yOffset;
			for (int i=0;i<numberOfMiniGames;i++) {
				if (i == 4) {
					x += columnWidth;
					y = yOffset;
				}
				if (i == 8) {
					x += columnWidth;
					y = yOffset;
				}
				if (i == 12) {
					x += columnWidth;
					y = yOffset;
				}
				if (((double[])runDashboardDiv.getMinigamesatisfactionData(i, "satisfaction"))[0] > 0) {
					Component component = (Component)CDesktopComponents.vView().getComponent("templateMinigameSatisfaction_").clone();
					component.setId("");
					insertBefore(component, null);
					Events.postEvent("onInit", component, new int[]{i,x,y});
				}
				y += rowHeight;
			}
			setAttribute("initialized", true);
		}
		setVisible(true);
	}

}
