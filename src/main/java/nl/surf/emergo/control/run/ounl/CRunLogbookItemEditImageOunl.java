/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Html;
import org.zkoss.zul.Textbox;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefImage;
import nl.surf.emergo.view.VView;

public class CRunLogbookItemEditImageOunl extends CDefImage {

	private static final long serialVersionUID = -1399316375728177597L;

	protected VView vView = CDesktopComponents.vView();

	public void onClick(Event aEvent) {
		CRunLogbookOunl logbook = (CRunLogbookOunl)vView.getComponent("runLogbook");
		setVisible(false);
		Html html = (Html)vView.getComponent(logbook.htmlLogbookNoteId + getAttribute("itemTagId"));
		Textbox textbox = (Textbox)vView.getComponent(logbook.textboxLogbookNoteId + getAttribute("itemTagId"));
		Component button = vView.getComponent(logbook.buttonLogbookNoteSaveId + getAttribute("itemTagId"));
		if (html != null) {
			html.setVisible(false);
		}
		if (textbox != null) {
			if (html != null) {
				textbox.setValue(html.getContent().replaceAll("<br/>", "\n"));
			}
			textbox.setVisible(true);
		}
		if (button != null) {
			button.setVisible(true);
		}
	}

}
