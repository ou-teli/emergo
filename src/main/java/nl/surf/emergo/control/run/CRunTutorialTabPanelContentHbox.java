/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefHbox;
import nl.surf.emergo.view.VView;

public class CRunTutorialTabPanelContentHbox extends CDefHbox {

	private static final long serialVersionUID = -3452913600826029020L;

	public void onCreate(CreateEvent aEvent) {
		CRunTutorial runTutorial = (CRunTutorial)CDesktopComponents.vView().getComponent("runTutorial");
		setAttribute("counter", "" + runTutorial.layoutnumber);
		Events.postEvent("onEmUpdateStyle", this, null);
	}

	public void onEmUpdateStyle(Event aEvent) {
		CRunTutorial runTutorial = (CRunTutorial)CDesktopComponents.vView().getComponent("runTutorial");
		boolean isSelected = ((String)getAttribute("pageId")).equals(runTutorial.currentPageId);
		String sclassName = "CRunTutorial_tabpanelhbox" + getAttribute("counter"); 
		if (isSelected) {
			sclassName += "_selected";
		}
		setSclass(sclassName);
	}

	public void onClick(Event aEvent) {
		CRunTutorial runTutorial = (CRunTutorial)CDesktopComponents.vView().getComponent("runTutorial");
		boolean isDisabled = ((String)getAttribute("disabled")).equals("true");
		boolean isSelected = ((String)getAttribute("pageId")).equals(runTutorial.currentPageId);
		if (!isDisabled && !isSelected) {
			String previousPageId = runTutorial.currentPageId;
			runTutorial.currentPageId = (String)getAttribute("pageId");

			VView vView = CDesktopComponents.vView();
			// deselect previous 'listitem'
			Events.postEvent("onEmUpdateStyle", vView.getComponent(CRunTutorial.listitemId + previousPageId), null);
			Events.postEvent("onEmUpdateStyle", vView.getComponent(CRunTutorial.listitembackgroundId + previousPageId), null);

			// select new 'listitem'
			Events.postEvent("onEmUpdateStyle", vView.getComponent(CRunTutorial.listitemId + runTutorial.currentPageId), null);
			Events.postEvent("onEmUpdateStyle", vView.getComponent(CRunTutorial.listitembackgroundId + runTutorial.currentPageId), null);

			runTutorial.handlePage((String)getAttribute("pageId"));
		}
	}

}
