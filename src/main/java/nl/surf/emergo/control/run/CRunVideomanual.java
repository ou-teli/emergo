/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Include;
import org.zkoss.zul.Treeitem;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefInclude;
import nl.surf.emergo.control.def.CDefMacro;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;

/**
 * The Class CRunVideomanual is used to show the videomanual within the run view area of the
 * Emergo player.
 */
public class CRunVideomanual extends CRunComponent {

	private static final long serialVersionUID = -59811495798386346L;
	
	/** The video view to show the video being played. It's an include so src can be changed. */
	protected Include videoView = null;

	/** The current video tag. */
	protected IXMLTag currentvideotag = null;

	public int rowNumber = 1;
	public HtmlBasedComponent selectedLabel = null;
	
	/**
	 * Instantiates a new c run videomanual.
	 */
	public CRunVideomanual() {
		super("runVideomanual", null);
		init();
	}

	/**
	 * Instantiates a new c run videomanual.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the videomanual case component
	 */
	public CRunVideomanual(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
		init();
	}

	/**
	 * Creates title area, content area to show videomanual and buttons area with close button.
	 */
	@Override
	protected void createComponents() {
		CRunVbox lVbox = new CRunVbox();
		appendChild(lVbox);

		createTitleArea(lVbox);
		CRunHbox lHbox = new CRunHbox();
		lVbox.appendChild(lHbox);
		createContentArea(lHbox);
		CRunArea lItemArea = createItemArea(lHbox);
		lItemArea.setId(getId()+"ItemArea");
		createButtonsArea(lVbox);
	}

	/**
	 * Creates new content component, the videomanual include.
	 *
	 * @return the component
	 */
	@Override
	protected Component newContentComponent() {
		HtmlMacroComponent videomanualView = new CDefMacro();
		videomanualView.setId("runVideomanualView");
		return videomanualView;
	}

	/**
	 * Creates buttons area and adds close button.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run hbox
	 */
	@Override
	protected CRunHbox createButtonsArea(Component aParent) {
		CRunHbox lButtonsHbox = super.createButtonsArea(aParent);
		createCloseButton(lButtonsHbox);
		return null;
	}

	/**
	 * Does contentitem action, clicking on an videomanual item.
	 * Shows item within item area.
	 *
	 * @param aContentItem the a contentitem, the videomanual item clicked
	 */
	@Override
	public void doContentItemAction(Component aContentItem) {
		Treeitem lTreeitem = (Treeitem)aContentItem;
		IXMLTag lVideoTag = (IXMLTag)lTreeitem.getAttribute("item");
		if ((lVideoTag == null) || (!lVideoTag.getName().equals("piece")))
			return;
		playVideo(lVideoTag);
	}

	/**
	 * Plays video. Only plays flv.
	 *
	 * @param aVideoTag the a video tag
	 */
	public void playVideo(IXMLTag aVideoTag) {
		if (aVideoTag == null) {
			return;
		}
		currentvideotag = aVideoTag;
		setRunTagStatus(caseComponent, aVideoTag, AppConstants.statusKeySelected, AppConstants.statusValueTrue, true);
		setRunTagStatus(caseComponent, aVideoTag, AppConstants.statusKeyOpened, AppConstants.statusValueTrue, true);
		initVideoSize();
		playFragment(aVideoTag);
	}

	/**
	 * Gets video size for current video and, if set, sets size of video.
	 */
	protected void initVideoSize() {
		// NOTE Get size of video. If not null, it overrules default values in style class.
		String lVideoSizeWidth = getVideoTagChildValue("size", 0);
		String lVideoSizeHeight = getVideoTagChildValue("size", 1);
		if (lVideoSizeWidth.equals("")) {
			lVideoSizeWidth = getComponentTagStatusValue("size", 0);
		}
		if (lVideoSizeHeight.equals("")) {
			lVideoSizeHeight = getComponentTagStatusValue("size", 1);
		}

		// NOTE Set videoView attributes to be used by video player
		videoView.setAttribute("video_size_width", lVideoSizeWidth);
		videoView.setAttribute("video_size_height", lVideoSizeHeight);
	}

	protected String getVideoTagChildValue(String aVideoTagChildName, int index) {
		if (currentvideotag == null) {
			return "";
		}
		return getValueByIndex(currentvideotag.getChildValue(aVideoTagChildName), index);
	}

	protected String getComponentTagStatusValue(String aStatusKey, int index) {
		return getValueByIndex(CDesktopComponents.sSpring().getCurrentRunComponentStatus(caseComponent, aStatusKey, AppConstants.statusTypeRunGroup), index);
	}

	protected String getValueByIndex(String aValues, int index) {
		if (aValues.equals("")) {
			return "";
		}
		int value = 0;
		String[] arr = aValues.split(",");
		if (index >= arr.length) {
			return "";
		}
		try {
			value = Integer.parseInt(arr[index]);
		} catch (NumberFormatException e) {
			return "";
		}
		return "" + value + "px";
	}

	/**
	 * Creates view for video.
	 * Gets current setting, sets selected and opened of it to true and starts it.
	 */
	public void init() {
		Events.postEvent("onInitZulfile", this, null);
	}

	protected List<List<Object>> getVideomanualDatas() {
		List<List<Object>> lDataList = new ArrayList<List<Object>>();
		
		List<IXMLTag> lVideoTags = getVideoTags();
		for (IXMLTag lVideoTag : lVideoTags) {
			List<Object> data = new ArrayList<Object>();
			data.add(lVideoTag);
			data.add(CDesktopComponents.sSpring().unescapeXML(lVideoTag.getChildValue("name")));
			lDataList.add(data);
		}
		return lDataList;
	}

	/**
	 * Gets all video tags.
	 */
	public List<IXMLTag> getVideoTags() {
		IXMLTag lRootTag = determineRootTag();
		if (lRootTag != null) {
			return lRootTag.getChild(AppConstants.contentElement).getChildTags(AppConstants.defValueNode);
		}
		else {
			return new ArrayList<IXMLTag>();
		}
	}

	/**
	 * Determine root tag of videomanual component to be used to render logbook.
	 *
	 * @return the XML tag
	 */
	protected IXMLTag determineRootTag() {
		if (caseComponent == null) {
			return null;
		}
		String lCacId = "" + caseComponent.getCacId();
		IXMLTag lRootTag = CDesktopComponents.sSpring().getXmlDataPlusRunStatusTree(lCacId, getRunStatusType());
		return lRootTag;
	}
	
	/**
	 * Creates video view.
	 *
	 * @param aParent the a parent
	 */
	protected void createVideoView(Component aParent) {
		videoView = new CDefInclude();
		videoView.setId("runVideomanualVideoView");
		videoView.setVisible(false);
		aParent.appendChild(videoView);
	}

	/**
	 * Gets the run component helper.
	 *
	 * @return the run component helper
	 */
	@Override
	public CRunComponentHelper getRunComponentHelper() {
		return new CRunComponentHelper(null, "piece", caseComponent, this);
	}

	/**
	 * Plays fragment. Only plays flv.
	 *
	 * @param aFragmentTag the a fragment tag
	 */
	public void playFragment(IXMLTag aFragmentTag) {
		videoView.setSrc(VView.v_run_empty_fr);
		videoView.setVisible(false);
		if (aFragmentTag == null) {
			return;
		}
		String lUrl = CDesktopComponents.sSpring().getSBlobHelper().getUrl(aFragmentTag);
		if (!lUrl.equals("")) {
			IXMLTag lBlobChild = aFragmentTag.getChild("blob");
			String lBlobtype = lBlobChild.getAttribute(AppConstants.keyBlobtype);
			if (!(lBlobtype.equals(AppConstants.blobtypeExtUrl))) {
				// put emergo path before url for player
				if (!CDesktopComponents.vView().isAbsoluteUrl(lUrl))
					lUrl = CDesktopComponents.vView().getEmergoRootUrl() + CDesktopComponents.vView().getEmergoWebappsRoot() + lUrl;
			}
		}
		if (lUrl.equals("")) {
			return;
		}
		videoView.setAttribute("url", lUrl);
		videoView.setSrc(VView.v_run_flash_videomanual_fr);

		videoView.setVisible(true);

		if (runWnd != null) {
			runWnd.setNoteTag(caseComponent, aFragmentTag);
		}
	}

	/**
	 * Methods originally used in ZUL file.
	 */

	public void onInitZulfile(Event aEvent) {
		Map<String,Object> propertyMap = new HashMap<String,Object>();
		propertyMap.put("currentEmergoComponent", this);
		propertyMap.put("videomanual_datas", getVideomanualDatas());
		((HtmlMacroComponent)getRunContentComponent()).setDynamicProperty("a_propertyMap", propertyMap);
		((HtmlMacroComponent)getRunContentComponent()).setMacroURI(VView.v_run_videomanual_fr);
		createVideoView(this);
		getRunContentComponent().setVisible(true);
	}

}
