/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Div;
import org.zkoss.zul.Include;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CIObserved;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.def.CDefImage;
import nl.surf.emergo.control.def.CDefInclude;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.utilities.PropsValues;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SDatatagReference;
import nl.surf.emergo.zkspring.SSpring;
import nl.surf.emergo.zkspring.STriggeredReference;

/**
 * The Class CRunConversations is used to show conversation fragments and images within the run view area of the
 * Emergo player. Conversation interaction is shown within the run choice area of the player.
 */
public class CRunConversations extends CRunComponent {

	private static final long serialVersionUID = 3227292980077429552L;

	/** The run conversation image view is used to show background images during conversation. */
	protected CRunConversationImageView imageView = null;

	/** The objects view is used to show objects during conversation. */
	protected Div objectsView = null;

	/** The fragment view is used to show the fragment being played. It's an include so src can be changed. */
	protected Include fragmentView = null;

	/** The current conversation tag. */
	protected IXMLTag currentconversationtag = null;

	/** The current startfragment tag. */
	protected IXMLTag currentstartfragmenttag = null;

	/** The current fragment tag. */
	protected IXMLTag currentfragmenttag = null;

	protected String runConversationInteractionId = "runConversationInteraction";
	
	/** The run conversation interaction. */
	protected CRunConversationInteraction runConversationInteraction = null;

	protected CRunArea onActionObserver = null;

	/** The show interaction after introduction. */
	protected boolean showInteractionAfterIntroduction = false;

	/** The there is interaction to show. */
	protected boolean thereIsInteractionToShow = false;

	/** The there is intervention after a fragment is being played. */
	protected boolean thereIsInterventionToShow = false;

	/** The show intervention. */
	protected boolean showIntervention = false;

	/** The intervention file name. */
	protected String interventionFileName = "";

	/** The intervention blob id. */
	protected String interventionBlobId = "";

	/** The x-value of the conversation video area position. */
	protected String conversationVideoLeftPosition = "";

	/** The y-value of the conversation fragment area position. */
	protected String conversationVideoTopPosition = "";
	
	/** The original style. */
	protected String originalStyle = "";

	public String getConversationInteractionId() {
		return runConversationInteractionId;
	}

	public boolean isShowInteractionAfterIntroduction() {
		return showInteractionAfterIntroduction;
	}

	public void setShowInteractionAfterIntroduction(
			boolean showInteractionAfterIntroduction) {
		this.showInteractionAfterIntroduction = showInteractionAfterIntroduction;
	}

	public IXMLTag getCurrentInterventionTag() {
		if (currentfragmenttag == null) {
			return null;
		}
		return currentfragmenttag.getChild("intervention");
	}

	public String getInterventionFileName() {
		return interventionFileName;
	}

	public String getInterventionBlobId() {
		return interventionBlobId;
	}

	/**
	 * Instantiates a new c run conversations.
	 */
	public CRunConversations() {
		super("runConversations", null);
		onActionObserver = runWnd;
		init();
	}

	/**
	 * Instantiates a new c run conversations.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the a case component
	 */
	public CRunConversations(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
		onActionObserver = runWnd;
		init();
	}

	/**
	 * Instantiates a new c run conversations, without directly playing fragment
	 *
	 * @param aId the a id
	 * @param aRunConversationInteractionId the a run conversation interaction id
	 * @param aOnActionObserver the a on action observer
	 */
	public CRunConversations(String aId, String aRunConversationInteractionId, CRunComponent aOnActionObserver) {
		super(aId, null);
		runConversationInteractionId = aRunConversationInteractionId;
		onActionObserver = aOnActionObserver;
		initViews();
	}

	/**
	 * Instantiates a new c run conversations.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the a case component
	 * @param aConversationTag the a conversation tag
	 * @param aRunConversationInteraction the a run conversation interaction
	 */
	public CRunConversations(String aId, IECaseComponent aCaseComponent, IXMLTag aConversationTag, CRunConversationInteraction aRunConversationInteraction) {
		super(aId, aCaseComponent);
		onActionObserver = runWnd;
		currentconversationtag = aConversationTag;
		runConversationInteraction = aRunConversationInteraction;
		init();
	}

	public IXMLTag getCurrentConversationTag() {
		return currentconversationtag;
	}

	public IXMLTag getCurrentFragmentTag() {
		return currentfragmenttag;
	}

	public Include getFragmentView() {
		if (fragmentView == null) {
			createFragmentView(this);
		}
		return fragmentView;
	}

	public CRunConversationInteraction getRunConversationInteraction() {
		if (runConversationInteraction == null) {
			return (CRunConversationInteraction) CDesktopComponents.vView().getComponent(runConversationInteractionId);
		}
		else {
			return runConversationInteraction;
		}
	}

	/**
	 * Creates views for image, objects and fragment.
	 */
	public void initViews() {
		createImageView(this);
		createObjectsView(this);
		createFragmentView(this);
	}

	/**
	 * Get start conversation parameter.
	 */
	protected Object getStartConversationParameter() {
		return this;
	}

	/**
	 * Creates views for image, objects and fragment.
	 * Gets current conversation.
	 */
	public void init() {
		if (onActionObserver != null) {
			onActionObserver.onAction(getId(), "startConversation", getStartConversationParameter());
		}

		initViews();
		
		// get conversation to play
		CRunConversationsHelper cComponent = (CRunConversationsHelper)getRunComponentHelper();
		if (currentconversationtag == null) {
			IXMLTag lXmlTree = cComponent.getXmlDataPlusStatusTree();
			if (lXmlTree != null) {
				currentconversationtag = getCurrentConversation(lXmlTree.getChild(AppConstants.contentElement).getChildTags(AppConstants.defValueNode));
			}
		}

		init(currentconversationtag, null);
	}

	/**
	 * Sets selected and opened of current conversation to true and starts it.
	 */
	public void init(IXMLTag aConversationTag, IXMLTag aFragmentTag) {
		originalStyle = getStyle();
		if (aConversationTag == null)
			return;
		if (onActionObserver != null && aConversationTag != currentconversationtag) {
			onActionObserver.onAction(getId(), "startConversation", getStartConversationParameter());
		}
		currentconversationtag = aConversationTag;
		initStyle(currentconversationtag);
		initShowInteractionAfterIntroduction();

		// set conversation status
		setRunTagStatus(getCaseComponent(), currentconversationtag, AppConstants.statusKeySelected, AppConstants.statusValueTrue, true);
		setRunTagStatus(getCaseComponent(), currentconversationtag, AppConstants.statusKeyOpened, AppConstants.statusValueTrue, true);
//		maybe add something for shared component

		Component lRunConversationInteractionInteractionArea = CDesktopComponents.vView().getComponent(getRunConversationInteraction().getId() + "InteractionArea");
		lRunConversationInteractionInteractionArea.getChildren().clear();
		
		addConversationElementChilds(currentconversationtag);

		CRunConversationInteraction lInteraction = getRunConversationInteraction();
		if (lInteraction != null)
			lInteraction.isInteractionShown = false;

		// check if conversation must start with fragment or not
		if (aFragmentTag == null) {
			currentstartfragmenttag = getCurrentFragment(currentconversationtag);
		}
		else {
			currentstartfragmenttag = aFragmentTag;
		}
		if (currentstartfragmenttag != null) {
			playFragment(currentconversationtag, currentstartfragmenttag, true);
		}
		else {
			showInteraction(currentconversationtag);
			showObjects(currentconversationtag, null);
		}

		if (onActionObserver instanceof CRunWnd && isChangeNoteContext()) {
			runWnd.setNoteTag(getCaseComponent(), currentconversationtag);
		}
	}

	protected int pixelStrToInt(String aPixelStr, int aDefaultValue) {
		if (!StringUtils.isEmpty(aPixelStr)) {
			aPixelStr = aPixelStr.replace("px", "");
			try {
				return Integer.parseInt(aPixelStr);
			} catch (NumberFormatException e) {
				return aDefaultValue;
			}
		}
		return aDefaultValue;
	}

	/**
	 * Restricts size of conversations component to size of case skin.
	 */
	protected void restrictComponentSize() {
		//NOTE get width and size of case skin
		int lSkinWidth = CDesktopComponents.sSpring().getSCaseSkinHelper().getCaseSkinWidth(getCaseComponent().getECase());
		int lSkinHeight = CDesktopComponents.sSpring().getSCaseSkinHelper().getCaseSkinHeight(getCaseComponent().getECase());
		int lLeft = pixelStrToInt(getLeft(), 0);
		int lTop = pixelStrToInt(getTop(), 0);
		int lWidth = pixelStrToInt(getWidth(), lSkinWidth);
		int lHeight = pixelStrToInt(getHeight(), lSkinHeight);
		if ((lLeft + lWidth) > lSkinWidth) {
			setWidth("" + (lSkinWidth - lLeft) + "px");
		}
		if ((lTop + lHeight) > lSkinHeight) {
			setHeight("" + (lSkinHeight - lTop) + "px");
		}
	}

	/**
	 * Gets video position and size for a tag and, if set, sets position and size of conversation and video frame.
	 * 
	 * @param aConversation the a conversation
	 */
	protected void initStyle(IXMLTag aConversation) {
		// NOTE Get position and size of conversation. If not null, it overrules default values in style class.
		String lVideoPositionLeft = getTagPixelChildValue(aConversation, "position", 0);
		String lVideoPositionTop = getTagPixelChildValue(aConversation, "position", 1);
		String lVideoSizeWidth = getTagPixelChildValue(aConversation, "size", 0);
		String lVideoSizeHeight = getTagPixelChildValue(aConversation, "size", 1);

		// NOTE Set position and size of conversation
		setLeft(lVideoPositionLeft);
		setTop(lVideoPositionTop);
		setWidth(lVideoSizeWidth);
		setHeight(lVideoSizeHeight);
		if (!lVideoPositionLeft.equals("")) {
			conversationVideoLeftPosition = lVideoPositionLeft;
		}
		else {
			conversationVideoLeftPosition = getLeft();
		}
		if (!lVideoPositionTop.equals("")) {
			conversationVideoTopPosition = lVideoPositionTop;
		}
		else {
			conversationVideoTopPosition = getTop();
		}
		// NOTE Set attributes to be used by video player
		getFragmentView().setAttribute("video_position_left", lVideoPositionLeft);
		getFragmentView().setAttribute("video_position_top", lVideoPositionTop);
		getFragmentView().setAttribute("video_size_width", lVideoSizeWidth);
		getFragmentView().setAttribute("video_size_height", lVideoSizeHeight);
		getFragmentView().setAttribute("video_overlay", getConversationVideoOverlay());
		restrictComponentSize();
	}

	/**
	 * Inits showInteractionAfterIntroduction.
	 */
	protected void initShowInteractionAfterIntroduction() {
		if (currentconversationtag == null) return;
		String lValue = currentconversationtag.getChildValue("showquestionsafterintroduction");
		showInteractionAfterIntroduction = lValue.equals(AppConstants.statusValueTrue); 
	}

	protected IXMLTag getTagChildTag(IXMLTag aTag, String aTagChildName) {
		if (aTag == null) return null;
		return aTag.getChild(aTagChildName);
	}

	protected String getTagPixelChildValue(IXMLTag aTag, String aTagChildName, int index) {
		int value = getTagIntegerChildValue(aTag, aTagChildName, index);
		if (value == Integer.MAX_VALUE) return "";
		return "" + value + "px";
	}

	protected int getTagIntegerChildValue(IXMLTag aTag, String aTagChildName, int index) {
		IXMLTag childTag = getTagChildTag(aTag, aTagChildName);
		if (childTag == null) {
			return Integer.MAX_VALUE;
		}
		String values = childTag.getValue();
		if (values.equals("")) {
			return Integer.MAX_VALUE;
		}
		int value = 0;
		String[] arr = values.split(",");
		if (index >= arr.length) {
			return Integer.MAX_VALUE;
		}
		try {
			value = Integer.parseInt(arr[index]);
		} catch (NumberFormatException e) {
			return Integer.MAX_VALUE;
		}
		return value;
	}

	protected String getConversationVideoOverlay() {
		String overlaySrc = CDesktopComponents.sSpring().getSBlobHelper().getUrl(currentconversationtag);
		overlaySrc = overlaySrc.replace("\\", "/");
		if (overlaySrc.indexOf("/") == 0) {
			overlaySrc = overlaySrc.substring(1);
		}
		if (!overlaySrc.equals("") && !CDesktopComponents.vView().isAbsoluteUrl(overlaySrc)) {
			overlaySrc = CDesktopComponents.vView().getEmergoWebappsRoot() + "/" + overlaySrc;
		}
		return overlaySrc;
	}

	/**
	 * Gets the run component helper.
	 *
	 * @return the run component helper
	 */
	@Override
	public CRunComponentHelper getRunComponentHelper() {
		return new CRunConversationsHelper(null, "question,map,alternative,field,button", getCaseComponent(), this);
	}

	/**
	 * Creates image view.
	 *
	 * @param aParent the a parent
	 */
	protected void createImageView(Component aParent) {
		CRunHbox lHbox = new CRunHbox();
		imageView = new CRunConversationImageView(getId() + "ImageView");
		imageView.setVisible(false);
		lHbox.appendChild(imageView);
		aParent.appendChild(lHbox);
	}

	/**
	 * Creates objects view.
	 *
	 * @param aParent the a parent
	 */
	protected void createObjectsView(Component aParent) {
		objectsView = new CDefDiv();
		objectsView.setVisible(false);
		aParent.appendChild(objectsView);
	}

	/**
	 * Creates fragment view.
	 *
	 * @param aParent the a parent
	 */
	protected void createFragmentView(Component aParent) {
		fragmentView = new CDefInclude();
		fragmentView.setId(getId() + "FragmentView");
		fragmentView.setVisible(false);
		fragmentView.setAttribute("runComponentId", getId());
		aParent.appendChild(fragmentView);
	}

	/**
	 * Is called when run conversations component is closed or user wants to ask a question.
	 *
	 * @param aObserved the observed object, it should have interface CIObserved implemented
	 * @param aAction the action
	 * @param aStatus the status object, can be String or other class
	 */
	@Override
	public void observedNotify(CIObserved aObserved, String aAction, Object aStatus) {
		if (aAction.equals("endComponent")) {
			endCurrentConversation();
		}
		if (aAction.equals("askQuestion")) {
			showInteraction(currentconversationtag);
		}
	}

	/**
	 * Gets the current conversation.
	 *
	 * @param aConversationTags the a conversation tags
	 *
	 * @return the conversation tag
	 */
	public IXMLTag getCurrentConversation(List<IXMLTag> aConversationTags) {
		if (runWnd == null) return null;
		if (onActionObserver instanceof CRunWnd) {
			IXMLTag lActTag = runWnd.getPlayerStatusXmlTag("ActCaseComponentTag", getId());
			if (lActTag == null)
				return null;
			return CDesktopComponents.sSpring().getXmlManager().getTagById(aConversationTags, lActTag.getAttribute(AppConstants.keyId));
		}
		else {
			return currentconversationtag;
		}
	}

	/**
	 * Gets the current fragment of a parent tag.
	 *
	 * @param aParentTag the a parent tag
	 *
	 * @return the current fragment tag or null if none
	 */
	public IXMLTag getCurrentFragment(IXMLTag aParentTag) {
		if (aParentTag == null)
			return null;
		for (IXMLTag lChildTag : aParentTag.getChildTags(AppConstants.defValueNode)) {
			String lName = lChildTag.getName();
			if (lName.equals("fragment")) {
				boolean lPresent = lChildTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent).equals(AppConstants.statusValueTrue);
				boolean lOpened = lChildTag.getCurrentStatusAttribute(AppConstants.statusKeyOpened).equals(AppConstants.statusValueTrue);
				if (lPresent && lOpened)
					// get first opened
					return lChildTag;
			}
		}
		return null;
	}

	/**
	 * Tries to get real size of current fragment and sets Include attributes to be used by fragment player to scale fragment conserving aspect ratio
	 * Include attribute is set to size of fragment fragment, or '0,0' if audio or empty file name, or '-1,-1' if size cannot be determined (in
	 * which case the conversation video area size should be used)
	 * 
	 * @param aFragment the a fragment
	 */
	protected void initFragmentPositionAndSize(IXMLTag aFragment) {
		getFragmentView().setAttribute("fragment_position_left", getTagPixelChildValue(aFragment, "position", 0));
		getFragmentView().setAttribute("fragment_position_top", getTagPixelChildValue(aFragment, "position", 1));
		getFragmentView().setAttribute("fragment_size_width", getTagPixelChildValue(aFragment, "size", 0));
		getFragmentView().setAttribute("fragment_size_height", getTagPixelChildValue(aFragment, "size", 1));
		getFragmentView().setAttribute("fragment_position_mode", "position:absolute;");
	}

	/**
	 * Adjusts video position for current fragment and, if set, overrules position of conversation and video frame.
	 */
	protected void adjustVideoPosition(IXMLTag aFragment) {
		// NOTE Get position of fragment. If not null, it overrules conversation position.
		String lVideoPositionLeft = getTagPixelChildValue(aFragment, "position", 0);
		String lVideoPositionTop = getTagPixelChildValue(aFragment, "position", 1);
		String lCurrentLeft = getLeft();
		String lCurrentTop = getTop();
		// NOTE Set position of conversation
		if (!lVideoPositionLeft.equals("") && !lVideoPositionLeft.equals(lCurrentLeft))
			setLeft(lVideoPositionLeft);
		else
			if (!conversationVideoLeftPosition.equals(lCurrentLeft))
				setLeft(conversationVideoLeftPosition);
		if (!lVideoPositionTop.equals("") && !lVideoPositionTop.equals(lCurrentTop))
			setTop(lVideoPositionTop);
		else
			if (!conversationVideoTopPosition.equals(lCurrentTop))
				setTop(conversationVideoTopPosition);
		restrictComponentSize();
	}

	/**
	 * Plays fragment and sets status of runchoicearea according to conversation type (with or without childs).
	 * Fragment formats can be flv, swf, wmv, mpg, wma, wav.
	 * If the format isn't equal to one of these the following applies.
	 * If the fragment is an url and it contains youtube.com, the youtube video is played embedded within an iframe.
	 * All other urls are shown within an embedded iframe.
	 *
	 * @param aConversation the a conversation
	 * @param aFragment the a fragment
	 * @param aSetOpened the a set opened
	 */
	public void playFragment(IXMLTag aConversation, IXMLTag aFragment, boolean aSetOpened) {
		if (runWnd == null || aFragment == null)
			return;

		if (aConversation != null) {
			getFragmentView().setAttribute("showCloseButton", "" + aConversation.getDefChildValue("showclosebutton").equals(AppConstants.statusValueTrue));
			getFragmentView().setAttribute("showControls", "" + aConversation.getDefChildValue("showcontrols").equals(AppConstants.statusValueTrue));
			String zindex = aConversation.getDefChildValue("z-index");
			if (!zindex.equals("")) {
				setStyle(originalStyle + "z-index:" + zindex + ";");
			}
		}

		// save current fragment
		currentfragmenttag = aFragment;
		if (currentfragmenttag.getParentTag().getName().equals("conversation")) {
			currentstartfragmenttag = currentfragmenttag; 
		}
		// is the fragment followed by an intervention?
		thereIsInterventionToShow = isIntervention(aFragment);
		showIntervention = false;
		if (thereIsInterventionToShow) {
			IXMLTag lInterventionStatusTag = getCurrentInterventionStatusTag();
			if (lInterventionStatusTag != null) {
				IXMLTag lStatusStatusTag = lInterventionStatusTag.getChild(AppConstants.statusElement);
				String lBlobId = lStatusStatusTag.getChildValue("blob");
				if (!lBlobId.equals("")) {
					//NOTE intervention has been saved so show intervention instead of fragment
					showIntervention = true;
					getFragmentView().setSrc(VView.v_run_empty_fr);
					initIntervention(lBlobId);
					getFragmentView().setVisible(true);
					return;
				} 
			}
		}
		// save SSpring instance in fragmentView attribute so media players possibly can use it
		getFragmentView().setAttribute("sspring", CDesktopComponents.sSpring());
		// save visibility of imageView to restore it later on
		boolean lIsImageViewVisible = imageView.isVisible();
		// hide image
		imageView.setVisible(false);

		// set opened status of fragment
		//NOTE If opened is set true (due to script action), don't set opened true, which is indicated by aSetOpened.
		if (aSetOpened) {
			setRunTagStatus(getCaseComponent(), aFragment, AppConstants.statusKeyOpened, AppConstants.statusValueTrue, true);
		}

		// set status of conversation interaction to video
		CRunConversationInteraction lInteraction = getRunConversationInteraction();
		if (lInteraction != null) {
			lInteraction.setStatus("video");
			// set status of buttons
			lInteraction.setButtonsStatus(areFragmentChilds(aConversation));
			lInteraction.setButtonsAction("");
		}

		showCurrentBackgroundImage(aConversation);
		showObjects(aConversation, aFragment);

		// get url of fragment
		String lUrl = CDesktopComponents.sSpring().getSBlobHelper().getUrl(aFragment);

		// if empty url show fragment text or description
		if (lUrl.equals("")) {
			String lText = CDesktopComponents.sSpring().unescapeXML(aFragment.getChildValue("text"));
			if (!lText.equals("")) {
				initFragmentPositionAndSize(aFragment);
				playText(getFragmentView(), aFragment, lText);
				imageView.setVisible(lIsImageViewVisible);
			}
			else {
				playDescription(getFragmentView(), aFragment, CDesktopComponents.sSpring().unescapeXML(aFragment.getChildValue("description")));
			}
			if (onActionObserver != null) {
				onActionObserver.onAction(getId(), "playFragment", "");
			}
			if (lText.equals("")) {
				// set finished to true because description is shown
				setRunTagStatus(getCaseComponent(), aFragment, AppConstants.statusKeyFinished, AppConstants.statusValueTrue, true);
			}

			deleteConversationElementChilds("fragment");

			addConversationElementChilds(currentfragmenttag);

			return;
		}
		
		// make url absolute if necessary
		String lBlobtype = aFragment.getChildAttribute("blob", AppConstants.keyBlobtype);
		if (!(lBlobtype.equals(AppConstants.blobtypeExtUrl))) {
			if (!CDesktopComponents.vView().isAbsoluteUrl(lUrl)) {
				// put emergo path before url
				lUrl = CDesktopComponents.vView().getEmergoRootUrl() +
						CDesktopComponents.vView().getEmergoWebappsRoot() + 
						lUrl;
			}
		}
		
		// get file extension
		String lFileExtension = CDesktopComponents.vView().getFileExtension(lUrl);

		// don't remove player in case of flv|mp4|webm|ogv|fla|mp3, so player is not unnecessary reloaded
		if (!lFileExtension.matches("flv|mp4|webm|ogv|fla|mp3")) {
			getFragmentView().setSrc(VView.v_run_empty_fr);
		}
		
		if (onActionObserver instanceof CRunWnd) {
			// show media control buttons for windows media player
			// for flash the buttons are within flash player
			runWnd.setPlayerStatus("TitleAreaMediaBtnsVisible", "false");
			if (lFileExtension.equals("wmv") || lFileExtension.equals("mpg")) {
				runWnd.setPlayerStatus("TitleAreaMediaBufferingVisible", "false");
			}
			runWnd.setPlayerStatus("TitleAreaMediaBtnsAction", "");
		}
		
		initFragmentPositionAndSize(aFragment);
		
		boolean lIsFragmentViewVisible = false;
		boolean lAdjustVideoPosition = true;
		
		if (lFileExtension.matches("wmv|mpg|wma|wav")) {
			if (onActionObserver instanceof CRunWnd) {
				runWnd.setPlayerStatus("TitleAreaMediaBtnsAction", lFileExtension);
			}
			if (lInteraction != null) {
				lInteraction.setButtonsAction(lFileExtension);
			}
			lIsFragmentViewVisible = playInWindowsMediaPlayer(runWnd, getFragmentView(), aFragment, lUrl);
		}
		else {
			if (lFileExtension.matches("flv|mp4|swf|unity3d|webm|ogv|fla|mp3")) {
				if (onActionObserver instanceof CRunWnd) {
					runWnd.setPlayerStatus("TitleAreaMediaBtnsAction", lFileExtension);
				}
				if (lInteraction != null) {
					lInteraction.setButtonsAction(lFileExtension);
				}
				// show player frame with correct url, will autoplay, if background it is shown
				String lBackgroundUrl = getBackgroundUrl(aConversation);
				// cannot handle error messages for flash with video-js player, so stick to jwplayer for flash
				if (lFileExtension.matches("flv|mp4|webm|ogv|fla|mp3")) {
					lIsFragmentViewVisible = playInJWPlayer(runWnd, getFragmentView(), aFragment, lUrl, lBackgroundUrl);
					lAdjustVideoPosition = false;   //position adjustment is done on ontime event of jwplayer
				}
				if (lFileExtension.equals("swf")) {
					lIsFragmentViewVisible = playInJWPlayerSwf(getFragmentView(), aFragment, lUrl);
				}
				if (lFileExtension.equals("unity3d")) {
					lIsFragmentViewVisible = playInUnityPlayer(getFragmentView(), aFragment, lUrl);
				}
			}
			else {
				lIsFragmentViewVisible = playWebsite(getFragmentView(), aFragment, lUrl);
			}
		}
		

		getFragmentView().setVisible(lIsFragmentViewVisible);
		if (!lIsFragmentViewVisible) {
			// if no media played restore imageView
			imageView.setVisible(lIsImageViewVisible);
		}

		if (onActionObserver != null) {
			onActionObserver.onAction(getId(), "playFragment", lFileExtension);
		}
		
		if (lAdjustVideoPosition) {
			//adjustVideoPosition(aFragment);
		}

		deleteConversationElementChilds("fragment");

		addConversationElementChilds(currentfragmenttag);
	}

	/**
	 * Adds conversation element childs.
	 * 
	 * @param aConversationElement the a conversation element
	 */
	public void addConversationElementChilds(IXMLTag aConversationElement) {
		CRunConversationsHelper lHelper = (CRunConversationsHelper)getRunComponentHelper();
		String lSourceElementName = "";
		if (aConversationElement.getName().equals("conversation")) {
			lSourceElementName = "conversation";
			//NOTE according to XML definition a conversation may have button childs
			addConversationElementChilds(aConversationElement, lHelper, lSourceElementName, "button");
		}
		else if (aConversationElement.getName().equals("fragment")) {
			lSourceElementName = "fragment";
			//NOTE according to XML definition a fragment may have field and button childs
			addConversationElementChilds(aConversationElement, lHelper, lSourceElementName, "field");
			addConversationElementChilds(aConversationElement, lHelper, lSourceElementName, "button");
		}

		if (!lSourceElementName.equals("")) {
			//NOTE both conversation and fragment may have sets as child and these sets may have buttons as childs
			List<IXMLTag> lSetTags = aConversationElement.getChilds("set");
			for (IXMLTag lSetTag : lSetTags) {
				if (!lSetTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse)) {
					addConversationElementChilds(lSetTag, lHelper, lSourceElementName, "button");
				}
			}
		}
	}

	/**
	 * Adds conversation element childs.
	 * 
	 * @param aConversationElement the a conversation element
	 * @param aHelper the a helper
	 * @param aSourceElementName the a source element name
	 * @param aChildTagName the a child tag name
	 */
	public void addConversationElementChilds(IXMLTag aConversationElement, CRunConversationsHelper aHelper, String aSourceElementName, String aChildTagName) {
		//NOTE conversation element may have button or field child tags. If so render them.
		List<IXMLTag> lChildTags = aConversationElement.getChilds(aChildTagName);
		for (IXMLTag lChildTag : lChildTags) {
			if (!lChildTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse)) {
				if (aChildTagName.equals("field")) {
					Component lComponent = aHelper.renderField(lChildTag);
					lComponent.setAttribute("sourceElementName", aSourceElementName);
				}
				else if (aChildTagName.equals("button")) {
					Component lComponent = aHelper.renderButton(lChildTag);
					lComponent.setAttribute("sourceElementName", aSourceElementName);
				}
			}
		}
	}

	/**
	 * Deletes conversation element childs.
	 * 
	 * @param aSourceElementName the a source element name
	 */
	public void deleteConversationElementChilds(String aSourceElementName) {
		Component lRunConversationInteractionInteractionArea = CDesktopComponents.vView().getComponent(getRunConversationInteraction().getId() + "InteractionArea");
		List<Component> lChildsToDelete = new ArrayList<Component>();
		for (Component lComponent : lRunConversationInteractionInteractionArea.getChildren()) {
			if (((String)lComponent.getAttribute("sourceElementName")).equals(aSourceElementName)) {
				lChildsToDelete.add(lComponent);
			}
		}
		for (Component lComponent : lChildsToDelete) {
			lComponent.detach();
		}
	}

	/**
	 * Plays text.
	 *
	 * @param aFragmentView the a fragment view
	 * @param aFragmentTag the a fragment tag
	 * @param aText the a text
	 *
	 * @return if fragment view should be shown
	 */
	public boolean playText(Include aFragmentView, IXMLTag aFragmentTag, String aText) {
		aFragmentView.setVisible(false);
		if (!aText.equals("")) {
			// show text if not empty
			aFragmentView.setAttribute("tag", aFragmentTag);
			aFragmentView.setAttribute("text", aText);
			aFragmentView.setAttribute("textstyle", aFragmentTag.getChildValue("textstyle"));
			SDatatagReference lNextFragment = getReferencedFragment(getCaseComponent(), aFragmentTag);
			//NOTE try to get referenced fragment of fragment tag. If no referenced fragment, try to get referenced fragment of the parent tag that might be an alternative 
			if (lNextFragment == null) {
				lNextFragment = getReferencedFragment(getCaseComponent(), aFragmentTag.getParentTag());
			}
			aFragmentView.setAttribute("hasNextFragment", "" + (lNextFragment != null));
			Component runTextDiv = CDesktopComponents.vView().getComponent("runTextDiv_" + aFragmentView.getAttribute("runComponentId"));
			if (runTextDiv == null) {
				aFragmentView.setSrc(VView.v_run_empty_fr);
				aFragmentView.setSrc(VView.v_run_text_fr);
			}
			else {
				Events.postEvent("onPlayText", runTextDiv, null);
			}
			aFragmentView.setVisible(true);
			// set started status of fragment
			setRunTagStatus(getCaseComponent(), aFragmentTag, AppConstants.statusKeyStarted, AppConstants.statusValueTrue, true);
		}
		else {
			aFragmentView.setSrc(VView.v_run_empty_fr);
		}
		return !aText.equals("");
	}

	/**
	 * Plays description.
	 *
	 * @param aFragmentView the a fragment view
	 * @param aFragmentTag the a fragment tag
	 * @param aDescription the a description
	 *
	 * @return if fragment view should be shown
	 */
	public boolean playDescription(Include aFragmentView, IXMLTag aFragmentTag, String aDescription) {
		if (aDescription.equals("")) {
			aFragmentView.setSrc(VView.v_run_empty_fr);
			aFragmentView.setVisible(false);
		}
		else {
			// show description if not empty
			aFragmentView.setAttribute("tag", aFragmentTag);
			aFragmentView.setAttribute("description", aDescription);
			aFragmentView.setSrc(VView.v_run_description_fr);
			aFragmentView.setVisible(true);
			// set started status of fragment
			setRunTagStatus(getCaseComponent(), aFragmentTag, AppConstants.statusKeyStarted, AppConstants.statusValueTrue, true);
		}
		return !aDescription.equals("");
	}

	/**
	 * Play in windows media player.
	 *
	 * @param aRunWnd the a run wnd
	 * @param aFragmentView the a fragment view
	 * @param aFragmentTag the a fragment tag
	 * @param aUrl the a url
	 *
	 * @return if fragment view should be shown
	 */
	public boolean playInWindowsMediaPlayer(CRunWnd aRunWnd, Include aFragmentView, IXMLTag aFragmentTag, String aUrl) {
		aFragmentView.setAttribute("tag", aFragmentTag);
		aFragmentView.setAttribute("url", aUrl);
		String lFileExtension = CDesktopComponents.vView().getFileExtension(aUrl);
		if (lFileExtension.equals("wmv") || lFileExtension.equals("wma")) {
			if (aRunWnd.showMediaControl) {
				aFragmentView.setSrc(VView.v_run_windowsmedia_with_control_fr);
			}
			else {
				aFragmentView.setSrc(VView.v_run_windowsmedia_fr);
			}
		}
		else {
			aFragmentView.setSrc(VView.v_run_windowsmedia_mpg_fr);
		}
		// set started status of fragment
		setRunTagStatus(getCaseComponent(), aFragmentTag, AppConstants.statusKeyStarted, AppConstants.statusValueTrue, true);
		return lFileExtension.equals("wmv") || lFileExtension.equals("mpg");
	}

	/**
	 * Play in jw player.
	 *
	 * @param aRunWnd the a run wnd
	 * @param aFragmentView the a fragment view
	 * @param aFragmentTag the a fragment tag
	 * @param aUrl the a url
	 * @param aBackgroundUrl the a background url
	 *
	 * @return if fragment view should be shown
	 */
	public boolean playInJWPlayer(CRunWnd aRunWnd, Include aFragmentView, IXMLTag aFragmentTag, String aUrl, String aBackgroundUrl) {
		aFragmentView.setAttribute("tag", aFragmentTag);
		aFragmentView.setAttribute("url", aUrl);
		aFragmentView.setAttribute("image", aBackgroundUrl);
		if (aFragmentView.getSrc() == null || !aFragmentView.getSrc().equals(VView.v_run_flash_fr)) {
			// load player
			aFragmentView.setSrc(VView.v_run_flash_fr);
		}
		else {
			// load new media using javascript
			// deal with special characters in file name because they can lead to distorted name after transfer to client side
			HtmlMacroComponent lPlayerMacro = (HtmlMacroComponent)getMacroChildById(aFragmentView, "playerMacro_" + aFragmentView.getAttribute("runComponentId"));
			if (lPlayerMacro != null) {
				List<List<String>> lStreamingurls = new ArrayList<List<String>>();
				if(!aUrl.equals("")) {
					aUrl = CDesktopComponents.vView().getUrlEncodedFileName(aUrl);
					lStreamingurls = CDesktopComponents.vView().getHtml5StreamingUrls(aUrl);
					if(!aUrl.equals(""))
						aUrl = CDesktopComponents.vView().uniqueView(aUrl);
				}
				lPlayerMacro.setDynamicProperty("a_streamingUrls", lStreamingurls);
				// NOTE url may contain back slashes. Replace by slashes
				aUrl = aUrl.replaceAll(PropsValues.STREAMING_PLAYER_PATH_SEPARATOR, "/");
				lPlayerMacro.setDynamicProperty("a_url", aUrl);
				
				String lLeft = getTagPixelChildValue(aFragmentTag, "position", 0);
				if ((lLeft == null) || (lLeft.equals("")))
					lLeft = "0px";
				lPlayerMacro.setDynamicProperty("a_left", lLeft);
				String lTop = getTagPixelChildValue(aFragmentTag, "position", 1);
				if ((lTop == null) || (lTop.equals("")))
					lTop = "0px";
				lPlayerMacro.setDynamicProperty("a_top", lTop);
				lPlayerMacro.setDynamicProperty("a_coverTop", lTop);
				String lWidth = getTagPixelChildValue(aFragmentTag, "size", 0);
				if ((lWidth == null) || (lWidth.equals(""))) {
					lWidth = getTagPixelChildValue(currentconversationtag, "size", 0);
					if ((lWidth == null) || (lWidth.equals(""))) {
						lWidth = (String)getDesktop().getAttribute("runWndWidth");
					}
				}
				String lHeight = getTagPixelChildValue(aFragmentTag, "size", 1);
				if ((lHeight == null) || (lHeight.equals(""))) {
					lHeight = getTagPixelChildValue(currentconversationtag, "size", 1);
					if ((lHeight == null) || (lHeight.equals(""))) {
						lHeight = (String)getDesktop().getAttribute("runWndHeight");
					}
				}

				lPlayerMacro.setDynamicProperty("a_width", lWidth);
				lPlayerMacro.setDynamicProperty("a_height", lHeight);

				lPlayerMacro.recreate();
				String lData = aBackgroundUrl +
						"&&" + aFragmentView.getAttribute("fragment_size_width") + 
						"&&" + aFragmentView.getAttribute("fragment_size_height");
				aRunWnd.emergoEventToClient("jwplayer", "runFlash_" + (String)fragmentView.getAttribute("runComponentId"), "load", lData);
			}
		}
		// set started status of fragment
		setRunTagStatus(getCaseComponent(), aFragmentTag, AppConstants.statusKeyStarted, AppConstants.statusValueTrue, true);
		return true;
	}

	/**
	 * Plays in jw player swf.
	 *
	 * @param aFragmentView the a fragment view
	 * @param aFragmentTag the a fragment tag
	 * @param aUrl the a url
	 *
	 * @return if fragment view should be shown
	 */
	public boolean playInJWPlayerSwf(Include aFragmentView, IXMLTag aFragmentTag, String aUrl) {
		aFragmentView.setAttribute("tag", aFragmentTag);
		aFragmentView.setAttribute("url", aUrl);
		aFragmentView.setSrc(VView.v_run_flash_swf_fr);
		// set started status of fragment
		setRunTagStatus(getCaseComponent(), aFragmentTag, AppConstants.statusKeyStarted, AppConstants.statusValueTrue, true);
		return true;
	}

	/**
	 * Plays in unity player.
	 *
	 * @param aFragmentView the a fragment view
	 * @param aFragmentTag the a fragment tag
	 * @param aUrl the a url
	 *
	 * @return if fragment view should be shown
	 */
	public boolean playInUnityPlayer(Include aFragmentView, IXMLTag aFragmentTag, String aUrl) {
		aFragmentView.setAttribute("tag", aFragmentTag);
		aFragmentView.setAttribute("url", aUrl);
		aFragmentView.setSrc(VView.v_run_unity_fr);
		// set started status of fragment
		setRunTagStatus(getCaseComponent(), aFragmentTag, AppConstants.statusKeyStarted, AppConstants.statusValueTrue, true);
		return true;
	}

	/**
	 * Plays web site.
	 *
	 * @param aFragmentView the a fragment view
	 * @param aFragmentTag the a fragment tag
	 * @param aUrl the a url
	 *
	 * @return if fragment view should be shown
	 */
	public boolean playWebsite(Include aFragmentView, IXMLTag aFragmentTag, String aUrl) {
		if (!CDesktopComponents.vView().isAbsoluteUrl(aUrl)) {
			// add root otherwise iframe will add root and '/emergo' so
			// aUrl will contain '/emergo/emergo' and resource will not be found
			aUrl = CDesktopComponents.vView().getEmergoRootUrl() + aUrl;
		}
		// if url on emergo server then get relative path
	  	aUrl = CDesktopComponents.vView().getRelativeUrl(aUrl);
		aFragmentView.setAttribute("tag", aFragmentTag);
		aFragmentView.setAttribute("url", aUrl);
		aFragmentView.setSrc(VView.v_run_website_fr);
		// set started of fragment
		setRunTagStatus(getCaseComponent(), aFragmentTag, AppConstants.statusKeyStarted, AppConstants.statusValueTrue, true);
		return true;
	}

	/**
	 * Shows interaction for conversation within runchoicearea and shows current background image of conversation, can be non existing.
	 *
	 * @param aConversation the a conversation
	 */
	public void showInteraction(IXMLTag aConversation) {
		if (onActionObserver instanceof CRunWnd)
			// hide media control buttons
			runWnd.setPlayerStatus("TitleAreaMediaBtnsVisible", "false");
		// set status of conversation interaction to conversation
		CRunConversationInteraction lInteraction = getRunConversationInteraction();
		if (lInteraction == null)
			return;
		lInteraction.setStatus("conversation");
		// show interaction
		CRunInteractionTree lTree = getRunConversationInteraction().runInteractionTree;
		lTree.showInteraction(getCaseComponent(), aConversation);
		// hide fragment and set src empty, so player will stop
		getFragmentView().setVisible(false);
		getFragmentView().setSrc(VView.v_run_empty_fr);
		// show image
		showCurrentBackgroundImage(aConversation);
	}

	/**
	 * Checks if interaction is present, after status update. If not, hide interaction, else show interaction.
	 *
	 * @param aConversation the a conversation
	 */
	public void checkOnInteraction(IXMLTag aConversation) {
		CRunConversationInteraction lInteraction = getRunConversationInteraction();
		if (lInteraction == null)
			return;
		//show interaction
		CRunInteractionTree lTree = lInteraction.getRunInteractionTree();
		lTree.showInteraction(getCaseComponent(), aConversation);
		Boolean lOldThereIsInteractionToShow = thereIsInteractionToShow;
		thereIsInteractionToShow = isThereInteractionToShow(aConversation);
		if (thereIsInteractionToShow != lOldThereIsInteractionToShow) {
			lInteraction.setVisible(thereIsInteractionToShow && !isShowInteractionAfterIntroduction() && !lInteraction.isInteractionHidden());
			lInteraction.setIsInteractionShown(thereIsInteractionToShow);
		}
	}

	/**
	 * Does aTag have present xml child tags with aTagName?
	 *
	 * @param aTag the a tag
	 * @param aTagName the a tag name
	 *
	 * @return boolean
	 */
	protected boolean hasPresentXmlChildTags(IXMLTag aTag, String aTagName) {
		if (aTag == null) {
			return false;
		}
		for (IXMLTag lChildTag : aTag.getChilds(aTagName)) {
			if (lChildTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent).equals(AppConstants.statusValueTrue)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Does aTag have present xml child tags?
	 *
	 * @param aTag the a tag
	 *
	 * @return boolean
	 */
	protected boolean hasPresentXmlChildTags(IXMLTag aTag) {
		if (hasPresentXmlChildTags(aTag, "question")) {
			return true;
		}
		else if (hasPresentXmlChildTags(aTag, "map")) {
			return true;
		}
		else if (hasPresentXmlChildTags(aTag, "alternative")) {
			return true;
		}
		else if (hasPresentXmlChildTags(aTag, "set")) {
			return true;
		}
		else if (hasPresentXmlChildTags(aTag, "field")) {
			return true;
		}
		else if (hasPresentXmlChildTags(aTag, "button")) {
			return true;
		}
		return false;
	}

	/**
	 * Is there interaction to show?
	 *
	 * @param aConversation the a conversation
	 *
	 * @return boolean
	 */
	public boolean isThereInteractionToShow(IXMLTag aConversation) {
		CRunConversationInteraction lInteraction = getRunConversationInteraction();
		if (lInteraction == null)
			return false;
		CRunInteractionTree lTree = lInteraction.getRunInteractionTree();
		IXMLTag lTag = lTree.getRootTag();
		if (hasPresentXmlChildTags(lTag)) {
			return true;
		}
		// if there are no 'root' elements, just fragments with sub elements, show only elements for currently played fragment
		IXMLTag lCurrentFragment = getCurrentFragmentTag();
		if (lCurrentFragment == null) {
			return false;
		}
		if (lCurrentFragment.getCurrentStatusAttribute(AppConstants.statusKeyFinished).equals(AppConstants.statusValueTrue) ||
				lCurrentFragment.getCurrentStatusAttribute(AppConstants.statusKeyShowinteraction).equals(AppConstants.statusValueTrue)) {
			if (hasPresentXmlChildTags(lCurrentFragment)) {
				return true;
			}
			// also show element history (for parent fragment(s))
			IXMLTag lParentTag = lCurrentFragment.getParentTag();
			while (lParentTag != null && !lParentTag.getName().equals("conversation")) {
				if (lParentTag.getName().equals("fragment") &&
						(lParentTag.getCurrentStatusAttribute(AppConstants.statusKeyFinished).equals(AppConstants.statusValueTrue) ||
								lCurrentFragment.getCurrentStatusAttribute(AppConstants.statusKeyShowinteraction).equals(AppConstants.statusValueTrue))) {
					if (hasPresentXmlChildTags(lParentTag)) {
						return true;
					}
				}
				lParentTag = lParentTag.getParentTag();
			}
		}
		return false;
	}

	/**
	 * Shows current background image for tag.
	 *
	 * @param aTag the a tag
	 */
	public void showCurrentBackgroundImage(IXMLTag aTag) {
		// show image
		imageView.setVisible(false);
		CRunBackgroundImage lImage = imageView.getRunBackgroundImage();
		lImage.showImage(aTag);
		imageView.setVisible(!StringUtils.isEmpty(lImage.getSrc()));
	}

	/**
	 * Shows objects.
	 *
	 * @param aConversationTag the a conversation tag
	 * @param aFragmentTag the a fragment tag
	 */
	public void showObjects(IXMLTag aConversationTag, IXMLTag aFragmentTag) {
		objectsView.setVisible(false);
		//remove old objects
		objectsView.getChildren().clear();
		boolean lVisible = false;
		
		List<IXMLTag> lTags = new ArrayList<IXMLTag>();
		//first add objects of background tag
		IXMLTag lCurrentBackgroundTag = CDesktopComponents.sSpring().getSBlobHelper().getBackgroundTag(aConversationTag);
		if (lCurrentBackgroundTag != null) {
			lTags.addAll(lCurrentBackgroundTag.getChilds("object"));
		}
		//then add objects of fragment tag
		if (aFragmentTag != null) {
			lTags.addAll(aFragmentTag.getChilds("object"));
		}
		// show objects
		for (IXMLTag lTag : lTags) {
			if (!lTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse)) {
				String lUrl = CDesktopComponents.sSpring().getSBlobHelper().getUrl(lTag).replace("\\", "/");
				if (!lUrl.equals("")) {
					CDefImage lImage = new CDefImage();
					lImage.setSrc(lUrl);
					String lStyle = "";
					String lPositionLeft = getTagPixelChildValue(lTag, "position", 0);
					lStyle = (!lPositionLeft.equals("") ? lStyle + "left:" + lPositionLeft + ";" : lStyle);

					String lPositionTop = getTagPixelChildValue(lTag, "position", 1);
					lStyle = (!lPositionTop.equals("") ? lStyle + "top:" + lPositionTop + ";" : lStyle);

					String lSizeWidth = getTagPixelChildValue(lTag, "size", 0);
					lStyle = (!lSizeWidth.equals("") ? lStyle + "width:" + lSizeWidth + ";" : lStyle);
					
					String lSizeHeight = getTagPixelChildValue(lTag, "size", 1);
					lStyle = (!lSizeHeight.equals("") ? lStyle + "height:" + lSizeHeight + ";" : lStyle);

					lStyle = (!lStyle.equals("") ? "position:absolute;" + lStyle : lStyle);

					lImage.setStyle(lStyle);
					objectsView.appendChild(lImage);
					lVisible = true;
				}
			}
		}
		objectsView.setVisible(lVisible);
	}

	/**
	 * Ends current conversation and sets opened to false and finished to true.
	 */
	public void endCurrentConversation() {
		closeConversation();
		// save status of conversation
		setRunTagStatus(getCaseComponent(), currentconversationtag, AppConstants.statusKeyOpened, AppConstants.statusValueFalse, true);
		setRunTagStatus(getCaseComponent(), currentconversationtag, AppConstants.statusKeyFinished, AppConstants.statusValueTrue, true);
//		maybe add something for shared component
	}

	/**
	 * Close conversation and restores runchoicearea status.
	 */
	public void closeConversation() {
		if (onActionObserver instanceof CRunWnd) {
			// hide media control buttons
			runWnd.setPlayerStatus("TitleAreaMediaBtnsVisible", "false");
		}
		if (onActionObserver != null) {
			onActionObserver.onAction(getId(), "endConversation", this);
		}
		removeFragmentView();
		// hide image
		imageView.setVisible(false);
	}

	/**
	 * Remove fragment view. To stop fragment from playing.
	 */
	public void removeFragmentView() {
		if (fragmentView != null) {
			// hide fragment and set src empty, so player will stop
			fragmentView.setVisible(false);
			fragmentView.setSrc(VView.v_run_empty_fr);
			fragmentView.detach();
			fragmentView = null;
		}
	}

	/**
	 * Checks if there are childs for the fragment.
	 *
	 * @param aFragment the a fragment
	 *
	 * @return true, if successful
	 */
	public boolean areFragmentChilds(IXMLTag aFragment) {
		if (aFragment == null)
			return false;
		return aFragment.getChilds("map").size() > 0 ||
				aFragment.getChilds("question").size() > 0 ||
				aFragment.getChilds("alternative").size() > 0 ||
				aFragment.getChilds("set").size() > 0 ||
				aFragment.getChilds("field").size() > 0 ||
				aFragment.getChilds("button").size() > 0;
	}

	/**
	 * Checks if there is an intervention for the the fragment.
	 *
	 * @param aFragment the a fragment
	 *
	 * @return true, if successful
	 */
	public boolean isIntervention(IXMLTag aFragment) {
		if (aFragment == null)
			return false;
		// if intervention
		return (aFragment.getChild("intervention") != null);
	}

	/**
	 * Gets current intervention data, so for current fragment.
	 *
	 * @return list of intervention data
	 */
	protected List<List<Object>> getInterventionData() {
		if (!isIntervention(getCurrentFragmentTag())) {
			return null;
		}
		List<List<Object>> lDataList = new ArrayList<List<Object>>();
		IXMLTag lInterventionTag = getCurrentInterventionTag();
		List<Object> data = new ArrayList<Object>();
 		//numberOfResits
		long numberOfResits = 0;
		try {
			numberOfResits = Long.parseLong(lInterventionTag.getChild("numberofresits").getDefValue());
		} catch (NumberFormatException e) {
		}
 		data.add(numberOfResits);
 		//canBeStarted
 		data.add(!lInterventionTag.getChild(AppConstants.statusElement).getDefAttribute("autostart").equals(AppConstants.statusValueTrue));
 		//hasPreviousFragment
 		boolean lHasPrevious = false;
 		if (getCurrentFragmentTag() != null && getCurrentFragmentTag().getParentTag() != null) {
 			List<IXMLTag> lFragments = getCurrentFragmentTag().getParentTag().getChilds("fragment");
 			lHasPrevious = lFragments.size() > 0 && !lFragments.get(0).getAttribute(AppConstants.keyId).equals(getCurrentFragmentTag().getAttribute(AppConstants.keyId));
 		}
 		data.add(lHasPrevious);
 		//hasNextFragment
 		data.add(CDesktopComponents.sSpring().getSReferencedDataTagHelper().getReferencedTag(getCaseComponent(), lInterventionTag, "reffragment") != null);
 		//maxDuration
		long maxDuration = -1;
		try {
			maxDuration = Long.parseLong(lInterventionTag.getChild("maxduration").getDefValue());
		} catch (NumberFormatException e) {
		}
 		data.add(maxDuration);
 		//seeSelf
 		data.add(lInterventionTag.getChild("seeself").getDefValue().equals(AppConstants.statusValueTrue));
 		//base name of recorded webcam file:
 		/*
		String lSpecFileName = Paths.get(String.valueOf(CDesktopComponents.sSpring().getCase().getCasId()), 
							String.valueOf(CDesktopComponents.sSpring().getRun().getRunId()), 
							String.valueOf(CDesktopComponents.sSpring().getAccount().getAccId()),
							lInterventionTag.getChild("pid").getDefValue()).toString();
							*/
		String lSpecFileName = String.valueOf(CDesktopComponents.sSpring().getCase().getCasId()) + "/" + 
				String.valueOf(CDesktopComponents.sSpring().getRun().getRunId()) + "/" +  
				String.valueOf(CDesktopComponents.sSpring().getAccountInRun().getAccId()) + "/" + 
				lInterventionTag.getChild("pid").getDefValue();
 		data.add(lSpecFileName);
		lDataList.add(data);
		return lDataList;
	}

	/**
	 * Stores name of intervention saved on streaming server into blob.
	 *
	 * @return true, if successful
	 */
	public boolean saveStreamingServerIntervention() {
		String lFileName = interventionFileName;
		/*NOTE still have to add check if file exists on streaming server (use xuggler??)
		if (!CDesktopComponents.sSpring().getFileManager().fileExists(lFileName)) {
			return false;
		}
		*/
		return interventionBlobReady(saveStreamingServerIntervention(lFileName));
	}
	
	/**
	 * Init intervention.
	 *
	 * @param aBlobId the a blob id
	 */
	protected void initIntervention(String aBlobId) {
		CRunConversationInteraction lInteraction = getRunConversationInteraction();
		if (lInteraction != null && thereIsInterventionToShow) {
			Include lIntervention = (Include)CDesktopComponents.vView().getComponent(lInteraction.getId() + "Intervention");
			if (lIntervention != null) {
				lIntervention.setAttribute("intervention_data", getInterventionData());
				lIntervention.setAttribute("intervention_blob_id", aBlobId);
			}
			lInteraction.setVisible(true);
			lInteraction.setStatus("intervention");
			IXMLTag lInterventionTag = getCurrentInterventionTag();
			if (lInterventionTag != null) {
				setRunTagStatus(getCaseComponent(), lInterventionTag, AppConstants.statusKeySelected, AppConstants.statusValueTrue, true);
				setRunTagStatus(getCaseComponent(), lInterventionTag, AppConstants.statusKeyOpened, AppConstants.statusValueTrue, true);
			}
		}
	}
	
	/**
	 * Intervention blob ready.
	 *
	 * @param aBlobId the a blob id
	 *
	 * @return true, if successful
	 */
	protected boolean interventionBlobReady(String aBlobId) {
		interventionBlobId = aBlobId;
		if (!aBlobId.equals("")) {
			CRunConversationInteraction lInteraction = getRunConversationInteraction();
			if (lInteraction != null) {
				lInteraction.setStatus("interventionBlobReady");
			}
			return true;
		}
		return false;
	}
	
	/**
	 * Gets intervention status tag.
	 *
	 * @return intervention status tag
	 */
	public IXMLTag getCurrentInterventionStatusTag() {
		if (getCaseComponent() == null) {
			return null;
		}
		IXMLTag lInterventionTag = getCurrentInterventionTag();
		if (lInterventionTag == null) {
			return null;
		}
		IXMLTag lStatusRootTag = CDesktopComponents.sSpring().getXmlRunStatusTree(getCaseComponent(), AppConstants.statusTypeRunGroup);
		if (lStatusRootTag == null) {
			return null;
		}
		IXMLTag lStatusContentTag = lStatusRootTag.getChild(AppConstants.contentElement);
		if (lStatusContentTag == null) {
			return null;
		}
		String lInterventionTagId = lInterventionTag.getAttribute(AppConstants.keyId);
		IXMLTag lInterventionStatusTag = null;
		//NOTE intervention status tag exists because selected and opened are set before for intervention
		for (IXMLTag lStatusTag : lStatusContentTag.getChilds("intervention")) {
			if (lStatusTag.getAttribute(AppConstants.keyRefdataid).equals(lInterventionTagId)) {
				lInterventionStatusTag = lStatusTag;
			}
		}
		return lInterventionStatusTag;
	}

	/**
	 * Saves intervention.
	 *
	 * @param aFileName the a file name
	 *
	 * @return blo id, if successful, else empty string
	 */
	public String saveIntervention(String aFileName) {
		if (getCaseComponent() == null) {
			return "";
		}
		IXMLTag lInterventionTag = getCurrentInterventionTag();
		if (lInterventionTag == null) {
			return "";
		}
		IXMLTag lInterventionStatusTag = getCurrentInterventionStatusTag();
		if (lInterventionStatusTag == null) {
			return "";
		}
		IXMLTag lStatusStatusTag = lInterventionStatusTag.getChild(AppConstants.statusElement);
		IXMLTag lStatusStatusChildTag = lStatusStatusTag.getChild("blob"); 
		if (lStatusStatusChildTag == null) {
			//no blob child yet
			lStatusStatusChildTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag("blob", "");
			lStatusStatusTag.getChildTags().add(lStatusStatusChildTag);
		}
		String lBlobId = lStatusStatusChildTag.getValue();
		//don't remove from ftp folder in localhost, because ftp upload does not work on localhost.
		lBlobId = CDesktopComponents.sSpring().getSBlobHelper().replaceFileBlob(lBlobId, aFileName, !(CDesktopComponents.vView().getEmergoRootUrl().indexOf("localhost") >= 0));
		lStatusStatusChildTag.setValue(lBlobId);

		// NOTE Intervention is saved without calling CDesktopComponents.sSpring().setRunTagStatus, because it is user generated content,
		// not related to any data tag. setRunTagStatus is meant to check script, but script does not work on
		// user generated content.
		CDesktopComponents.sSpring().setRunTagStatusValue(lInterventionStatusTag, AppConstants.statusKeySaved, AppConstants.statusValueTrue);
		CDesktopComponents.sSpring().getSLogHelper().logSetRunTagAction("setruntagstatus", AppConstants.statusTypeRunGroup, CDesktopComponents.sSpring().getXmlManager().unescapeXML(getCaseComponent().getName()), lInterventionTag.getName(),
				lInterventionTag.getChildValue("pid"), AppConstants.statusKeySaved, AppConstants.statusValueTrue, false);
		// save status
		IXMLTag lStatusRootTag = CDesktopComponents.sSpring().getXmlRunStatusTree(getCaseComponent(), AppConstants.statusTypeRunGroup);
		CDesktopComponents.sSpring().setSaveRunGroupCaseComponentStatusRootTag(CDesktopComponents.sSpring().getRunGroup(), getCaseComponent(), lStatusRootTag);
		return lBlobId;
	}

	/**
	 * Stores name of intervention saved on streaming server into blob.
	 *
	 * @param aFileName the a file name
	 *
	 * @return blo id, if successful, else empty string
	 */
	public String saveStreamingServerIntervention(String aFileName) {
		if (getCaseComponent() == null) {
			return "";
		}
		IXMLTag lInterventionTag = getCurrentInterventionTag();
		if (lInterventionTag == null) {
			return "";
		}
		IXMLTag lInterventionStatusTag = getCurrentInterventionStatusTag();
		if (lInterventionStatusTag == null) {
			return "";
		}
		IXMLTag lStatusStatusTag = lInterventionStatusTag.getChild(AppConstants.statusElement);
		IXMLTag lStatusStatusChildTag = lStatusStatusTag.getChild("blob"); 
		if (lStatusStatusChildTag == null) {
			//no blob child yet
			lStatusStatusChildTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag("blob", "");
			lStatusStatusChildTag.setAttribute(AppConstants.keyBlobtype, AppConstants.blobtypeExtUrl);
			lStatusStatusTag.getChildTags().add(lStatusStatusChildTag);
		}
		String lBlobId = lStatusStatusChildTag.getValue();
		//don't remove from ftp folder in localhost, because ftp upload does not work on localhost.
		lBlobId = CDesktopComponents.sSpring().getSBlobHelper().replaceUrlBlob(lBlobId, aFileName);
		lStatusStatusChildTag.setValue(lBlobId);

		// NOTE Intervention is saved without calling CDesktopComponents.sSpring().setRunTagStatus, because it is user generated content,
		// not related to any data tag. setRunTagStatus is meant to check script, but script does not work on
		// user generated content.
		CDesktopComponents.sSpring().setRunTagStatusValue(lInterventionStatusTag, AppConstants.statusKeySaved, AppConstants.statusValueTrue);
		CDesktopComponents.sSpring().getSLogHelper().logSetRunTagAction("setruntagstatus", AppConstants.statusTypeRunGroup, CDesktopComponents.sSpring().getXmlManager().unescapeXML(getCaseComponent().getName()), lInterventionTag.getName(),
				lInterventionTag.getChildValue("pid"), AppConstants.statusKeySaved, AppConstants.statusValueTrue, false);
		// save status
		IXMLTag lStatusRootTag = CDesktopComponents.sSpring().getXmlRunStatusTree(getCaseComponent(), AppConstants.statusTypeRunGroup);
		CDesktopComponents.sSpring().setSaveRunGroupCaseComponentStatusRootTag(CDesktopComponents.sSpring().getRunGroup(), getCaseComponent(), lStatusRootTag);
		return lBlobId;
	}

	/**
	 * Plays previous intervention fragment.
	 */
	public void playPreviousInterventionFragment() {
		CRunConversationInteraction lInteraction = getRunConversationInteraction();
		if (lInteraction != null) {
			lInteraction.setStatus("previousintervention");
			lInteraction.setVisible(false);
		}
		if (!isIntervention(getCurrentFragmentTag())) {
			return;
		}
		IXMLTag lFragmentTag = null;
 		if (getCurrentFragmentTag() != null && getCurrentFragmentTag().getParentTag() != null) {
 			IXMLTag lPreviousXmlTag = null;
 			for (IXMLTag lXmlTag : getCurrentFragmentTag().getParentTag().getChilds("fragment")) {
 	 			if (lPreviousXmlTag != null && 
 	 					lXmlTag.getAttribute(AppConstants.keyId).equals(getCurrentFragmentTag().getAttribute(AppConstants.keyId))) {
 	 				lFragmentTag = lPreviousXmlTag;
 	 				break;
 	 			}
 	 			lPreviousXmlTag = lXmlTag;
 			}
 		}
 		if (lFragmentTag != null) {
 			playFragment(currentconversationtag, lFragmentTag, true);
 		}
	}

	/**
	 * Plays next intervention fragment.
	 */
	public void playNextInterventionFragment() {
		CRunConversationInteraction lInteraction = getRunConversationInteraction();
		if (lInteraction != null) {
			lInteraction.setStatus("nextintervention");
			lInteraction.setVisible(false);
		}
		if (!isIntervention(getCurrentFragmentTag())) {
			return;
		}
		IXMLTag lInterventionTag = getCurrentInterventionTag();
 		playFragment(currentconversationtag, CDesktopComponents.sSpring().getSReferencedDataTagHelper().getReferencedTag(getCaseComponent(), lInterventionTag, "reffragment"), true);
	}

	/**
	 * On action. Is used to show a buffering alert and to show play/pause buttons for the windows media player.
	 *
	 * @param sender the sender
	 * @param action the action
	 * @param status the status
	 */
	@Override
	public void onAction(String sender, String action, Object status) {
		if (action.equals("onExternalEvent")) {
			String[] lStatus = (String[]) status;
			String lSender = lStatus[0];
			String lReceiver = lStatus[1];
			String lEvent = lStatus[2];
			String lEventData = lStatus[3];
			String[] lEventDataValues = lEventData.split(",");
			SSpring lSSpring = CDesktopComponents.sSpring(); 
			if (sender.matches("jwplayer|videojsplayer|textplayer")) {
				if (lEvent.equals("onTime")) {
					// if flv has started playing by flash player the player sends an onTime, in run_flash_fr.zul
					//adjustVideoPosition(currentfragmenttag);
				} 
				else if (lEvent.equals("onBuffer")) {
					// if flv is buffered by flash player the player sends an onBuffer, in run_flash_fr.zul
				} 
				else if (lEvent.equals("onStartOfLastBalloon") || lEvent.equals("onComplete")) {
					if (lEvent.equals("onStartOfLastBalloon")) {
						setRunTagStatus(getCaseComponent(), currentfragmenttag, AppConstants.statusKeyShowinteraction, AppConstants.statusValueTrue, true);
					}
					else if (lEvent.equals("onComplete")) {
						setRunTagStatus(getCaseComponent(), currentfragmenttag, AppConstants.statusKeyFinished, AppConstants.statusValueTrue, true);
					} 
					CRunConversationInteraction lInteraction = getRunConversationInteraction();
					if (lInteraction != null &&  !lInteraction.isVisible() && isShowInteractionAfterIntroduction() && thereIsInteractionToShow) {
						lInteraction.setVisible(true);
						lInteraction.runInteractionTree.restoreSelectedTagId();
					}
					setShowInteractionAfterIntroduction(false);
					if (lEvent.equals("onComplete")) {
						initIntervention("");
					} 
				} 
				else if (lEvent.equals("onError")) {
					// if flv cannot be played by flash player the player sends an onError, in run_flash_fr.zul only one onTime is sent
					String lFileName = (String)getFragmentView().getAttribute("url");
					lSSpring.getSLogHelper().logAction("error in " + sender + ": file " + lFileName + ": " + lEventDataValues[1]);
					getFragmentView().setSrc(VView.v_run_empty_fr);
					// show description if not empty and no streaming media found
					String lDescription = lSSpring.unescapeXML(currentfragmenttag.getChildValue("description"));
					if (lDescription.equals("")) {
						lDescription = "error in " + sender + ": " + lEventDataValues[1];
					}
					getFragmentView().setAttribute("description", lDescription);
					getFragmentView().setSrc(VView.v_run_description_fr);
					imageView.setVisible(false);
					getFragmentView().setVisible(true);
					if (lSSpring.isReadOnlyRun() && lSSpring.isTestRun()) {
						//NOTE If error in video, only set fragment to finished when testing the game in read only mode. This allows testing the 
						//game flow when video's are not yet present. Students should only be able to proceed when the video has reached the end, 
						//or else should receive a warning they can send to the EMERGO help desk.
						//When testing in read/write mode, use the preview window interface to set fragment status if needed.
						setRunTagStatus(getCaseComponent(), currentfragmenttag, AppConstants.statusKeyFinished, AppConstants.statusValueTrue, true);
						CRunConversationInteraction lInteraction = getRunConversationInteraction();
						if (lInteraction != null && isShowInteractionAfterIntroduction() && thereIsInteractionToShow) {
							lInteraction.setVisible(true);
						}
						setShowInteractionAfterIntroduction(false);
					}
				} 
			}
			if (sender.matches("webcam")) {
				if (lEvent.equals("onShowRecord")) {
					interventionFileName = "";
					interventionBlobId = "";
					CRunConversationInteraction lInteraction = getRunConversationInteraction();
					if (lInteraction != null) {
						lInteraction.setStatus("interventionShowRecord");
					}
				} 
				else if (lEvent.equals("onRecordComplete")) {
					interventionFileName = lEventDataValues[1];
					CRunConversationInteraction lInteraction = getRunConversationInteraction();
					if (lInteraction != null) {
						lInteraction.setStatus("interventionRecordComplete");
					}
				} else if (lEvent.equals("onError")) {
					lSSpring.getSLogHelper().logAction("error in recording from webcam: " + lEventDataValues[1]);
					Clients.evalJavaScript("showMessage('" + CDesktopComponents.vView().getLabel("run_ispot.error.webcam." + lEventDataValues[1]) + "');");
				}
			}
		}
	}

	/**
	 * Get conversation. If aTag is no conversation tag, then loops through the parents of a tag to find the conversation tag.
	 *
	 * @param aTag tag the a tag
	 */
	private IXMLTag getConversation (IXMLTag aTag) {
		IXMLTag lConversationTag = null;
		if (aTag != null) {
			if (aTag.getName().equals("conversation")) {
				return aTag;
			}
			lConversationTag = aTag.getParentTag();
			while (!(lConversationTag == null || lConversationTag.getName().equals("conversation"))) {
				lConversationTag = lConversationTag.getParentTag();
			}
		}
		return lConversationTag;
	}

	/**
	 * Is tag equal to one of his parents?.
	 *
	 * @param aTag the a tag
	 * @param aOtherTag the a other tag
	 *
	 * @return boolean
	 */
	protected boolean isTagEqualToParentOfOtherTag(IXMLTag aTag, IXMLTag aOtherTag) {
		IXMLTag lParentTag = aOtherTag.getParentTag();
		while (!lParentTag.getName().equals(AppConstants.contentElement)) {
			if (aTag == lParentTag) {
				return true;
			}
			lParentTag = lParentTag.getParentTag();
		}
		return false;
	}

	/**
	 * Gets referenced fragment.
	 *
	 * @param aCaseComponent the a case component
	 * @param aTag the a tag
	 *
	 * @return boolean
	 */
	public SDatatagReference getReferencedFragment(IECaseComponent aCaseComponent, IXMLTag aTag) {
		SDatatagReference lReference = CDesktopComponents.sSpring().getSReferencedDataTagHelper().getDatatagReference(aCaseComponent, aTag, "reffragment");
		if (lReference == null || lReference.getCaseComponent() == null || lReference.getDataTag() == null) {
			return null;
		}
		return lReference;
	}

	/**
	 * If aCaseComponent and aTag has a referenced fragment then play it.
	 *
	 * @param aCaseComponent the a case component
	 * @param aTag the a tag
	 *
	 * @return boolean
	 */
	public boolean playReferencedFragment(IECaseComponent aCaseComponent, IXMLTag aTag) {
		SDatatagReference lReference = getReferencedFragment(aCaseComponent, aTag);
		if (lReference == null) {
			return false;
		}
		IECaseComponent lRefCaseComponent = lReference.getCaseComponent();
		IXMLTag lRefFragmentTag = lReference.getDataTag();

		//if referenced fragment in other case component or in other conversation of current case component
		
		//NOTE if a conversations component is adjusted within the authoring environment and is previewed within the player environment,
		//refreshing the player environment results in lRefCaseComponent being unequal to the result of the getCaseComponent() method, although their cacid's are equal.
		//This probably has to do with caching of case components within application memory, but I'm not able to figure out the problem (yet). According to the code it should work.
		//Refreshing a second time solves the problem.
		//However to prevent the problem at all, below is checked on id's of case components and tags.
		
		boolean lCaseComponentsAreUnequal = lRefCaseComponent == null || getCaseComponent() == null ||
				lRefCaseComponent.getCacId() != getCaseComponent().getCacId();
		IXMLTag lRefConversationTag = getConversation(lRefFragmentTag);
		IXMLTag lConversationTag = getConversation(aTag);
		boolean conversationTagsAreUnequal = lRefConversationTag == null || lConversationTag == null ||
				!lRefConversationTag.getAttribute(AppConstants.keyId).equals(lConversationTag.getAttribute(AppConstants.keyId));
		if (lCaseComponentsAreUnequal || conversationTagsAreUnequal) {
			if (currentconversationtag != null) {
				//close current conversation
				setRunTagStatus(getCaseComponent(), currentconversationtag, AppConstants.statusKeyOpened, AppConstants.statusValueFalse, true);
				setRunTagStatus(getCaseComponent(), currentconversationtag, AppConstants.statusKeyFinished, AppConstants.statusValueTrue, true);
			}
			//if referenced fragment in other case component
			if (lCaseComponentsAreUnequal) {
				//close current case component
				CDesktopComponents.sSpring().setRunComponentStatus(getCaseComponent(),
						AppConstants.statusKeyOpened, AppConstants.statusValueFalse,
						true, AppConstants.statusTypeRunGroup, true);
				//set case component of CRunConversations to the other case component
				setCaseComponent(lRefCaseComponent);
				//open the other case component
				CDesktopComponents.sSpring().setRunComponentStatus(lRefCaseComponent,
						AppConstants.statusKeyOpened, AppConstants.statusValueTrue,
						true, AppConstants.statusTypeRunGroup, true);
			}
			//set conversation to other conversation and play fragment
			init(getConversation(lRefFragmentTag), lRefFragmentTag);
			return true;
		}
		//hide conversation interaction if referenced tag is unequal to aTag or one of its parents, for instance if a new 'mc question' has to be shown
		//if an alternative is to repeat the same question, conversation interaction should not be hidden.
		if (!isTagEqualToParentOfOtherTag(lRefFragmentTag, aTag)) {
			CRunConversationInteraction lInteraction = getRunConversationInteraction();
			if (lInteraction != null) {
				lInteraction.setVisible(false);
			}
		}
		//play fragment
		playFragment(currentconversationtag, lReference.getDataTag(), true);
		return true;
	}

	/**
	 * Hides component.
	 */
	public void Hide() {
		CRunConversationInteraction lInteraction = getRunConversationInteraction();
		if (lInteraction != null) {
			lInteraction.setVisible(false);
		}
		setVisible(false);
		setRunTagStatus(getCaseComponent(), currentconversationtag, AppConstants.statusKeyOpened, AppConstants.statusValueFalse, true);
	}

	/**
	 * Update. If status of an element of the shown conversation has changed, re-render it.
	 *
	 * @param aTriggeredReference the a triggered reference
	 */
	public void update(STriggeredReference aTriggeredReference) {
		IECaseComponent lCaseComponent = aTriggeredReference.getCaseComponent();
		String lTagId = aTriggeredReference.getDataTag().getAttribute(AppConstants.keyId);
		String lStatusKey = aTriggeredReference.getStatusKey();
		String lStatusValue = aTriggeredReference.getStatusValue();
		boolean lSetFromScript = aTriggeredReference.getSetFromScript();
		if (currentconversationtag == null || getCaseComponent().getCacId() != lCaseComponent.getCacId()) {
			return;
		}
		IXMLTag lTag = CDesktopComponents.sSpring().getXmlDataPlusRunStatusTag(getCaseComponent(), runStatusType, lTagId);
		if (lTag == null) {
			return;
		}
		IXMLTag lChangedConversationTag = getConversation(lTag);
		// no update if unequal to current conversation
		if (lChangedConversationTag == null || 
				!lChangedConversationTag.getAttribute(AppConstants.keyId).equals(currentconversationtag.getAttribute(AppConstants.keyId))) {
			return;
		}
		String lTagName = lTag.getName();
		boolean lIsConversationTag = lTagName.equals("conversation");
		boolean lIsBackgroundTag = lTagName.equals("background");
		boolean lIsFragmentTag = lTagName.equals("fragment");
		boolean lIsOtherStartFragmentTag = lIsFragmentTag && lTag.getParentTag().getName().equals("conversation") &&
				(currentstartfragmenttag == null ||
				!lTag.getAttribute(AppConstants.keyId).equals(currentstartfragmenttag.getAttribute(AppConstants.keyId)));
		boolean lIsQuestionTag = lTagName.equals("question");
		boolean lIsMapTag = lTagName.equals("map");
		boolean lIsAlternativeTag = lTagName.equals("alternative");
		boolean lIsButtonTag = lTagName.equals("button");
		boolean lIsSetTag = lTagName.equals("set");
		boolean lIsFeedbackconditionTag = lTagName.equals("feedbackcondition");
		boolean lIsPresentChanged = lStatusKey.equals(AppConstants.statusKeyPresent);
		boolean lIsAccessibleChanged = lStatusKey.equals(AppConstants.statusKeyAccessible);
		boolean lIsOpenedChanged = lStatusKey.equals(AppConstants.statusKeyOpened);
		boolean lIsFinishedChanged = lStatusKey.equals(AppConstants.statusKeyFinished);
		boolean lIsShowinteractionChanged = lStatusKey.equals(AppConstants.statusKeyShowinteraction);

		CRunConversationInteraction lInteraction = getRunConversationInteraction();
		// if present of conversation is set to false, end conversation
		if (lIsConversationTag && lIsPresentChanged && lStatusValue.equals(AppConstants.statusValueFalse)) {
			if (lInteraction != null) {
				lInteraction.setVisible(false);
			}
			endCurrentConversation();
			return;
		}
		// else update the status:
		CRunConversationsHelper lHelper = (CRunConversationsHelper)getRunComponentHelper();
		IXMLTag lXmlTree = lHelper.getXmlDataPlusStatusTree();
		lHelper.setItemAttributes(this, lXmlTree.getChild(AppConstants.contentElement));
		IXMLTag lCurrentConversationTag = getCurrentConversation(lXmlTree.getChild(AppConstants.contentElement).getChildTags(AppConstants.defValueNode));
		// NOTE current conversation tag can be null, then don't overwrite it
		if (lCurrentConversationTag != null) {
			boolean lConversationTagChanged = lCurrentConversationTag != currentconversationtag;
			currentconversationtag = lCurrentConversationTag;
			if (lConversationTagChanged) {
				initStyle(currentconversationtag);
			}
		}
		if (lIsBackgroundTag && lIsOpenedChanged) {
			// show (other) background
			showCurrentBackgroundImage(currentconversationtag);
			showObjects(currentconversationtag, currentfragmenttag);
		}
		
		//TODO Hub changed
		//if (lIsOtherStartFragmentTag || (!isVisible() || lSetFromScript) && lIsOpenedChanged && lStatusValue.equals(AppConstants.statusValueTrue)) {
		//to
		//if (lIsOtherStartFragmentTag && (!isVisible() || lSetFromScript) && lIsOpenedChanged && lStatusValue.equals(AppConstants.statusValueTrue)) {
		//Probably playing same fragment does not work anymore!!
		
		if (lIsOtherStartFragmentTag && (!isVisible() || lSetFromScript) && lIsOpenedChanged && lStatusValue.equals(AppConstants.statusValueTrue)) {
			// play start fragment
			setVisible(true);
			currentstartfragmenttag = lTag;
			//NOTE If opened is set true (due to script action), don't set opened true within playFragment, which is indicated by 'false'.
			playFragment(currentconversationtag, currentstartfragmenttag, false);

			if (lInteraction != null) {
				thereIsInteractionToShow = isThereInteractionToShow(currentconversationtag);
				boolean lIsVisible = thereIsInteractionToShow && !isShowInteractionAfterIntroduction() && !lInteraction.isInteractionHidden();
				if (lInteraction.isVisible() != lIsVisible) {
					lInteraction.setVisible(lIsVisible);
				}
			}
		}
		if ((lIsFragmentTag || lIsQuestionTag || lIsMapTag || lIsSetTag) && (lIsPresentChanged || lIsAccessibleChanged || lIsFinishedChanged || lIsShowinteractionChanged)) {
			if (lInteraction != null) {
				if (lIsFragmentTag && (lIsFinishedChanged || lIsShowinteractionChanged)) {
					boolean lHasNodeTags = getPresentChildTags(lTag).size() > 0;
					if (lStatusValue.equals(AppConstants.statusValueTrue) && lHasNodeTags) {
						//determine if conversation interaction should be visible
						thereIsInteractionToShow = isThereInteractionToShow(currentconversationtag);
						boolean lIsVisible = thereIsInteractionToShow && !isShowInteractionAfterIntroduction() && !lInteraction.isInteractionHidden();
						if (!lInteraction.isVisible() && lIsVisible) {
							CRunInteractionTree lTree = getRunConversationInteraction().runInteractionTree;
							//NOTE have to call method below, otherwise conversation within tree is empty and update method below generates null pointer exception
							lTree.showInteraction(getCaseComponent(), currentconversationtag);
						}
						if (lInteraction.isVisible() != lIsVisible) {
							//NOTE if conversation is closed don't handle pending finished or showinteraction changes
							boolean lIsConversationOpened = currentconversationtag.getCurrentStatusAttribute(AppConstants.statusKeyOpened).equals(AppConstants.statusValueTrue);
							if (lIsConversationOpened) {
								lInteraction.setVisible(lIsVisible);
							}
						}
						lInteraction.setIsInteractionShown(thereIsInteractionToShow);
						if (lIsFinishedChanged) {
							//update children of fragment tag in interaction tree
							lInteraction.update(lTag);
						}
					}
					if (lIsFinishedChanged) {
						//NOTE try to play referenced fragment of tag. If no referenced fragment, try to play referenced fragment of the parent tag that might be an alternative 
						if (!playReferencedFragment(getCaseComponent(), lTag)) {
							playReferencedFragment(getCaseComponent(), lTag.getParentTag());
						}
					}
				}
				else {
					// update complete interaction to be sure
					lInteraction.update(currentconversationtag);
				}
			}
			if ((lIsQuestionTag || lIsMapTag) && lIsPresentChanged) {
				checkOnInteraction(currentconversationtag);
			}
		}
		if ((lIsAlternativeTag || lIsButtonTag || lIsFeedbackconditionTag) && lIsOpenedChanged && lStatusValue.equals(AppConstants.statusValueTrue)) {
			if (lIsAlternativeTag || lIsButtonTag) {
				//NOTE for alternative and button score etc. still have to be set
				//for feedbackcondition this is already done by method getFeedbackConditionTagAndChangeStatus, see method showFeedback below
				setRunTagStatus(getCaseComponent(), lTag, AppConstants.statusKeyScore, lTag.getChildValue("score"), true);
				setRunTagStatus(getCaseComponent(), lTag, AppConstants.statusKeyCorrect, "" + lTag.getChildValue("correct").equals(AppConstants.statusValueTrue), true);
				if (lTag.getParentTag().getName().equals("set")) {
					setRunTagStatus(getCaseComponent(), lTag.getParentTag(), AppConstants.statusKeyScore, lTag.getChildValue("score"), true);
				}
			}
			//if alternative tag does not have child fragment tag, play reffragment right away, otherwise it will be played after the child fragment tag is played, see above
			if (getCurrentFragment(lTag) == null) {
				playReferencedFragment(getCaseComponent(), lTag);
			}
		}
	}

	/**
	 * Saves field text.
	 *
	 * @param aFieldTag the a field tag
	 * @param aText the a text
	 */
	public void saveFieldText(IXMLTag aFieldTag, String aText) {
		//NOTE because the text is saved as an attribute value \n has to be replaced by a string constant.
		//Otherwise the text is not reproduced correctly, crlf's are then replaced by spaces.
		aText = aText.replace("\n", AppConstants.statusCrLfReplace);
		setRunTagStatus(getCaseComponent(), aFieldTag, AppConstants.statusKeyString, CDesktopComponents.sSpring().escapeXML(aText), true);
	}

	/**
	 * Show feedback.
	 *
	 * @param aFieldTag the a field tag
	 * 
	 * @return the feedback condition tag
	 */
	public IXMLTag showFeedback(IXMLTag aFieldTag) {
		IXMLTag lFeedbackConditionTag = getFeedbackConditionTagAndChangeStatus(aFieldTag);
		if (lFeedbackConditionTag != null) {
			IXMLTag lCurrentFragmentTag = getCurrentFragment(lFeedbackConditionTag); 
			if (lCurrentFragmentTag == null) {
				//if feedbackcondition tag does not have child fragment tag, play reffragment right away
				playReferencedFragment(getCaseComponent(), lFeedbackConditionTag);
			}
			else {
				//else play child fragment tag first and possible reffragment tag will be played thereafter, in method update, see above
				playFragment(currentconversationtag, lCurrentFragmentTag, true);
			}
		}
		return lFeedbackConditionTag;
	}

	/**
	 * Called when button corresponding to button tag is clicked.
	 */
	public void onButtonClicked(Event aEvent) {
		IXMLTag tag = (IXMLTag)aEvent.getData();
		if (tag == null || !tag.getName().equals("button"))
			return;

		setRunTagStatus(caseComponent, tag, AppConstants.statusKeySelected, AppConstants.statusValueTrue, true);
		setRunTagStatus(caseComponent, tag, AppConstants.statusKeyOpened, AppConstants.statusValueTrue, true);

		for (IXMLTag childtag : tag.getChilds("fragment")) {
			if (childtag.getName().equals("fragment")) {
				boolean lPresent = false;
				boolean lOpened = false;
				lPresent = (CDesktopComponents.sSpring().getCurrentTagStatus(childtag, AppConstants.statusKeyPresent).equals(AppConstants.statusValueTrue));
				lOpened = (CDesktopComponents.sSpring().getCurrentTagStatus(childtag, AppConstants.statusKeyOpened).equals(AppConstants.statusValueTrue));
				if (lPresent && lOpened) {
					playFragment(currentconversationtag, childtag, true);
					return;
				}
			}
		}
		playFragment(currentconversationtag, null, true);
	}

}
