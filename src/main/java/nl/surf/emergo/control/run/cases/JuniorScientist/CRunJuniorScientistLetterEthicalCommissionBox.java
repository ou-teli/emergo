/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.JuniorScientist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.event.Event;

import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.zkspring.STriggeredReference;

/**
 * This class is used to show a letter of the ethical commission where certain paragraphs may be present or not. It uses four states that each contains an answer given to one out of four dilemma's.
 * An empty editforms case component is used to store the opening and closing of the letter of the ethical commission.
 * The used editforms case component should have present=false and present should stay false, to prevent rendering of the original editforms component on the tablet.
 */
public class CRunJuniorScientistLetterEthicalCommissionBox extends CRunJuniorScientistBox {

	private static final long serialVersionUID = -2515432486877494358L;

	protected final static String ethicalCommissionCaseComponentName = "phase_1_ethical_commission";

	protected IECaseComponent ethicalCommissionAppCaseComponent;
	
	/** Prefix of pids of states entered by a case developer. */
	protected static final String newsDilemmaAnswerPrefix = "game_1_news_dilemma_answer_";
	
	@Override
	public void onInit() {
		super.onInitMacro();
		
		ethicalCommissionAppCaseComponent = getEthicalCommissionAppCaseComponent();
		//NOTE super ancestor CRunComponent needs case component to be set
		setCaseComponent(ethicalCommissionAppCaseComponent);
		
		//just like for the original editforms component set selected and opened
		setRunComponentStatusJS(ethicalCommissionAppCaseComponent, AppConstants.statusKeySelected, AppConstants.statusValueTrue);
		setRunComponentStatusJS(ethicalCommissionAppCaseComponent, AppConstants.statusKeyOpened, AppConstants.statusValueTrue);

		//determine ethical commission app properties to be used within the child macro
		setEthicalCommissionAppProperties();

		//add the child macro
		addChildMacro("JuniorScientist_ethical_commission_view_macro.zul");
	}
	
	@Override
	public void onUpdate() {
		//NOTE not used yet
		//rerender child macro with adjusted properties
		childMacro.recreate();
	}

	public void setEthicalCommissionAppProperties() {
		//NOTE there are sections that are shown depending on if all four dillemma's are answered correctly or not and there are sections that are shown if the corresponding dillemma answer is wrong.
		
		//NOTE get answers of student to dilemma's, If answer is empty, no answer is given yet. If answer is 1 correct choice is made. If answer is 2 incorrect choice is made.
		//depending on answers paragraphs are shown or not.
		List<Boolean> parPresent = new ArrayList<Boolean>();
		boolean allAnswersCorrect = true;
		for (int i=1;i<=4;i++) {
			//answers are stored as state tag value
			String answer = getStateTagValue(newsDilemmaAnswerPrefix + i);
			boolean answerCorrect = answer.equals("1");
			//NOTE every answer has one paragraph
			parPresent.add(!answerCorrect);
			allAnswersCorrect = allAnswersCorrect && answerCorrect;
		}

		propertyMap.put("approved", allAnswersCorrect);
		
		//NOTE fill paragraph data
		List<Map<String,Object>> paragraphs = new ArrayList<Map<String,Object>>();
		propertyMap.put("paragraphs", paragraphs);
		
		if (!allAnswersCorrect) {
			for (int i=0;i<parPresent.size();i++) {
				Map<String,Object> hParagraphData = new HashMap<String,Object>();
				paragraphs.add(hParagraphData);
				hParagraphData.put("present", parPresent.get(i));
			}
		}
		
	}
	
	protected IECaseComponent getEthicalCommissionAppCaseComponent() {
		return sSpring.getCaseComponent("", ethicalCommissionCaseComponentName);
	}
	
	public void onButtonClick(Event event) {
		//NOTE user clicks button to continue. This macro is used several times in a row.
		boolean approved = (boolean)event.getData();

		//just like for the original editforms component set opened
		setRunComponentStatusJS(ethicalCommissionAppCaseComponent, AppConstants.statusKeyOpened, AppConstants.statusValueFalse);
		if (currentTag.getName().equals("macro")) {
			//NOTE if macro zul file is used within the junior scientist references component, the case component is a references component and the tag is a piece tag, so don't set present
			//hide ethical commission macro
			setRunTagStatusJS(getNavigationCaseComponent(), currentTag, AppConstants.statusKeyPresent, AppConstants.statusValueFalse, false);
		}

		if (!approved) {
			setGameStateValue(1, 5);
		}
		else {
			setGameStateValue(1, 6);
		}
	}
	
	@Override
	public void handleStatusChange(STriggeredReference aTriggeredReference) {
	}

}
