/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.Component;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;

/**
 * The Class CRunConversationInteraction is used for interaction during conversations
 * and is shown within the run choice area.
 */
public class CRunConversationInteraction extends CRunArea {

	private static final long serialVersionUID = -3915236800203971036L;

	/** The run component, the conversations component. */
	protected CRunComponent runComponent = null;

	protected boolean notifyRunWnd = true;

	protected String runConversationsId = "runConversations";
	
	/** The conversation. */
	protected IXMLTag conversation = null;

	/** The is interaction shown. */
	protected boolean isInteractionShown = false;

	/** The run interaction tree. */
	protected CRunInteractionTree runInteractionTree = null;

	/** The interaction hidden. */
	protected boolean interactionHidden = false;

	public boolean isIsInteractionShown() {
		return isInteractionShown;
	}

	public void setIsInteractionShown(boolean isInteractionShown) {
		this.isInteractionShown = isInteractionShown;
	}

	public CRunInteractionTree getRunInteractionTree() {
		return runInteractionTree;
	}

	public void setRunInteractionTree(CRunInteractionTree runInteractionTree) {
		this.runInteractionTree = runInteractionTree;
	}

	public boolean isInteractionHidden() {
		return interactionHidden;
	}

	public void setInteractionHidden(boolean interactionHidden) {
		this.interactionHidden = interactionHidden;
	}

	/**
	 * Instantiates a new c run conversation interaction.
	 * Creates button area with close and ask question button.
	 * And creates content area.
	 */
	public CRunConversationInteraction() {
		super();
		setId("runConversationInteraction");
		init();
	}

	/**
	 * Instantiates a new c run conversation interaction.
	 * Creates button area with close and ask question button.
	 * And creates content area.
	 *
	 * @param aId the a id
	 * @param aRunComponent the a run component
	 * @param aRunConversationsId the a run conversations id
	 */
	public CRunConversationInteraction(String aId, CRunComponent aRunComponent, String aRunConversationsId) {
		super();
		setId(aId);
		runComponent = aRunComponent;
		runConversationsId = aRunConversationsId;
		init();
	}

	/**
	 * Instantiates a new c run conversation interaction.
	 * Creates button area with close and ask question button.
	 * And creates content area.
	 *
	 * @param aId the a id
	 * @param aRunComponent the a run component
	 * @param aRunConversationsId the a run conversations id
	 * @param aNotifyRunWnd the a notify run wnd
	 */
	public CRunConversationInteraction(String aId, CRunComponent aRunComponent, String aRunConversationsId, boolean aNotifyRunWnd) {
		super();
		setId(aId);
		runComponent = aRunComponent;
		runConversationsId = aRunConversationsId;
		notifyRunWnd = aNotifyRunWnd;
		init();
	}

	/**
	 * Creates views for image and for video.
	 * Gets current conversation, sets selected and opened of it to true and starts it.
	 */
	public void init() {
		String lType = "";
		if (runWnd != null && notifyRunWnd)
			lType = runWnd.getPlayerStatusString("ChoiceAreaLayoutStatus", null);
		setZclass(getClassName() + lType);

		createButtonsArea(this);
		createContentArea(this);
	}

	/**
	 * Creates new content component, the interaction tree.
	 *
	 * @return the component
	 */
	protected Component newContentComponent() {
		return (new CRunInteractionTree("runInteractionTree", runComponent, runConversationsId));
	}

	/**
	 * Creates button area with close and ask question button.
	 *
	 * @param aParent the a parent
	 */
	protected CRunHbox createButtonsArea(Component aParent) {
		CRunArea lButtonsArea = new CRunArea();
		CRunHbox lHbox = new CRunHbox();
		lButtonsArea.appendChild(lHbox);
		aParent.appendChild(lButtonsArea);
		createCloseButton(lHbox);
		createAskQuestionButton(lHbox);
		return lHbox;
	}

	/**
	 * Creates close button.
	 *
	 * @param aParent the a parent
	 */
	protected CRunConversationBtn createCloseButton(Component aParent) {
		CRunConversationBtn lButton = new CRunConversationBtn("runConversationEndBtn",
				"active","endComponent", runComponent,
				CDesktopComponents.vView().getLabel("run_conversations.button.endconversation.prefix"), "");
		lButton.setTooltiptext(CDesktopComponents.vView().getLabel("run_conversations.button.endconversation.tooltip"));
//		set to false, so button can be clicked multiple times
		lButton.setCanHaveStatusSelected(false);
		lButton.registerObserver(runConversationsId);
		if (notifyRunWnd) {
			lButton.registerObserver(CControl.runWnd);
		}
		aParent.appendChild(lButton);
		return lButton;
	}

	/**
	 * Creates ask question button.
	 *
	 * @param aParent the a parent
	 */
	protected CRunConversationBtn createAskQuestionButton(Component aParent) {
		CRunConversationBtn lButton = new CRunConversationBtn("runConversationAskQuestionBtn",
				"active","askQuestion", "",
				CDesktopComponents.vView().getLabel("run_conversations.button.askquestion.prefix"), "");
//		set to false, so button can be clicked multiple times
		lButton.setCanHaveStatusSelected(false);
		lButton.registerObserver(runConversationsId);
		aParent.appendChild(lButton);
		return lButton;
	}

	/**
	 * Creates content area and interaction within it.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run area
	 */
	protected CRunArea createContentArea(Component aParent) {
		String lType = "";
		CRunArea lContentArea = new CRunArea();
		lContentArea.setZclass(getClassName() + lType + "_content_area");
		runInteractionTree = (CRunInteractionTree)newContentComponent();
		// NOTE set sclass in stead of zclass
		// setting zclass destroys tree structure
		runInteractionTree.setSclass(getClassName() + lType + "_s_content");
		lContentArea.appendChild(runInteractionTree);
		aParent.appendChild(lContentArea);
		return lContentArea;
	}

	/**
	 * Sets the status. Either the close button and the interaction tree or
	 * the close button and the ask questions button.
	 *
	 * @param aStatus the new status
	 */
	public void setStatus(String aStatus) {
		boolean lStatusIsConversation = aStatus.equals("conversation");
		// if interaction is shown, don't hide them anymore and don't show ask questions button anymore
		if (lStatusIsConversation)
			isInteractionShown = lStatusIsConversation;
		if (runInteractionTree != null) {
			if (isInteractionShown) {
				if (!runInteractionTree.isVisible())
					runInteractionTree.setVisible(true);
			}
			else {
				if (runInteractionTree.isVisible())
					runInteractionTree.setVisible(false);
			}
		}
		if (isInteractionShown) {
			CRunHoverBtn lButton = (CRunHoverBtn) CDesktopComponents.vView().getComponent("runConversationAskQuestionBtn");
			if (lButton != null)
				lButton.setVisible(false);
		}
	}

	/**
	 * Sets the buttons status depending on if there are questions and if so, if they are shown.
	 *
	 * @param aAreQuestions the a are questions status
	 */
	public void setButtonsStatus(boolean aAreQuestions) {
		CRunHoverBtn lButton = (CRunHoverBtn) CDesktopComponents.vView().getComponent("runConversationAskQuestionBtn");
		// if questions are shown, don't show ask questions button anymore
		if (lButton != null)
			lButton.setVisible((aAreQuestions) && (!isInteractionShown));
	}

	/**
	 * Sets the buttons action depending on aFileExt. If media player is used client action is set to
	 * stop media player playing if one of the buttons is clicked.
	 *
	 * @param aFileExt the new btns action
	 */
	public void setButtonsAction(String aFileExt) {
		String lAction = "";
		if ((aFileExt.equals("wmv")) || (aFileExt.equals("mpg"))) {
			if (runWnd != null && notifyRunWnd)
				lAction = runWnd.getMediaplayerStopAction();
		}
		CRunHoverBtn lButton = (CRunHoverBtn) CDesktopComponents.vView().getComponent("runConversationEndBtn");
		if (lButton != null)
			lButton.setWidgetListener("onClick", lAction);
		lButton = (CRunHoverBtn) CDesktopComponents.vView().getComponent("runConversationAskQuestionBtn");
		if (lButton != null)
			lButton.setWidgetListener("onClick", lAction);
	}

	/**
	 * Re-renders tree items if necessary.
	 *
	 * @param aTag the changed element
	 */
	public void update(IXMLTag aTag) {
		if (isInteractionShown)
			runInteractionTree.update(aTag);
	}

}
