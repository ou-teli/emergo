/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.JuniorScientist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.zkoss.zk.ui.event.Event;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.zkspring.STriggeredReference;

/**
 * This class is used to show tweets during phase 3, first feedback to Sven.
 * The same editforms component is used as for CRunJuniorScientistFeedbackSvenBox1. It contains the tweets to be shown if answers are incorrect. There is a tweet for each question.
 */
public class CRunJuniorScientistFeedbackSvenTweetsBox1 extends CRunJuniorScientistBox {

	private static final long serialVersionUID = -8467611447972587603L;

	protected String feedbackSvenCaseComponentName = "";
	protected IECaseComponent feedbackSvenAppCaseComponent;
	
	/** Prefixes of pids entered by a case developer. */
	protected static final String selectorPidPrefix = "selector_";
	protected static final String tweetPidPrefix = "tweet_";
	
	@Override
	public void onInit() {
		super.onInitMacro();
		
		feedbackSvenCaseComponentName = "phase_3_feedback_sven_" + getVersion();
		feedbackSvenAppCaseComponent = getFeedbackSvenAppCaseComponent();

		//determine references properties to be used within the child macro
		setFeedbackSvenProperties();
		
		//add the child macro
		addChildMacro("JuniorScientist_feedback_sven_tweets_view_macro.zul");
	}
	
	protected int getVersion() {
		return 1;
	}
	
	protected void setFeedbackSvenProperties() {
		propertyMap.put("version", getVersion());

		List<Map<String,Object>> tweets = new ArrayList<Map<String,Object>>();
		propertyMap.put("tweets", tweets);
		
		//put all node tags in a map per pid to be able to get them by number later on
		Map<String,IXMLTag> nodeTagsByPid = getNodeTagsByPid();
		int counter = 1;
		while (nodeTagsByPid.containsKey(selectorPidPrefix + counter)) {
			//content to be rendered is entered in twoe content elements per question
			IXMLTag selectorTag = nodeTagsByPid.get(selectorPidPrefix + counter);
			IXMLTag tweetTag = nodeTagsByPid.get(tweetPidPrefix + counter);
			
			//get default or given answer
			String answer = getAnswer(selectorTag);
			boolean isAnswerOk = isAnswerOk(selectorTag, answer);
			if (!isAnswerOk) {
				Map<String,Object> hData = new HashMap<String,Object>();
				hData.put("tweet", sSpring.unescapeXML(tweetTag.getChildValue("defaulttext")));
				tweets.add(hData);
			}

			counter++;
		}
	}
	
	protected Map<String,IXMLTag> getNodeTagsByPid() {
		//put all node tags in a map per pid to be able to get them by number later on
		Map<String,IXMLTag> nodeTagsByPid = new HashMap<String,IXMLTag>();
		IXMLTag rootTag = sSpring.getXmlDataPlusRunStatusTree(feedbackSvenAppCaseComponent, AppConstants.statusTypeRunGroup);
		if (rootTag == null) {
			return nodeTagsByPid;
		}
		IXMLTag contentTag = rootTag.getChild(AppConstants.contentElement);
		if (contentTag == null) {
			return nodeTagsByPid;
		}
		IXMLTag formTag = contentTag.getChild("form");
		if (formTag == null) {
			return nodeTagsByPid;
		}
		for (IXMLTag nodeTag : cScript.getChildNodeTags(formTag)) {
			nodeTagsByPid.put(nodeTag.getChildValue("pid"), nodeTag);
		}
		
		return nodeTagsByPid;
	}
	
	protected String getAnswer(IXMLTag selectorTag) {
		return selectorTag.getCurrentStatusAttribute(AppConstants.statusKeyAnswer);
	}	

	protected boolean isAnswerOk(IXMLTag selectorTag, String answer) {
		//correct options are entered in selector tag
		Pattern p = Pattern.compile(selectorTag.getChildValue("correctoptions"), Pattern.DOTALL);
		Matcher m = p.matcher(answer);
		return m.matches();
	}

	protected IECaseComponent getFeedbackSvenAppCaseComponent() {
		return sSpring.getCaseComponent("", feedbackSvenCaseComponentName);
	}
	
	@Override
	public void handleStatusChange(STriggeredReference aTriggeredReference) {
	}

	public void onTryAgain(Event event) {
		hideMacro(currentTag);
		showMacro("feedback_sven_macro_" + getVersion());
	}
	
}
