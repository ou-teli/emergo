/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.viewfeedback;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitSubStepsDiv;
import nl.surf.emergo.domain.IERunGroup;

public class CRunPVToolkitViewFeedbackSubStepsDiv extends CRunPVToolkitSubStepsDiv {

	private static final long serialVersionUID = -4361727506283172039L;

	@Override
	protected List<String> getComponentToNavigateToIds(List<IXMLTag> subStepTags) {
		List<String> componentIds = new ArrayList<String>();
		for (IXMLTag subStepTag : subStepTags) {
			String subSteptype = subStepTag.getChildValue("substeptype");
			if (subSteptype.equals(CRunPVToolkit.viewFeedbackViewFeedbackSubStepType)) {
				componentIds.add("viewFeedbackSubStepDiv");
			}
		}
		return componentIds;
	}
	
	@Override
	protected List<String> getComponentToInitIds(List<IXMLTag> subStepTags) {
		List<String> componentIds = new ArrayList<String>();
		for (IXMLTag subStepTag : subStepTags) {
			String subSteptype = subStepTag.getChildValue("substeptype");
			if (subSteptype.equals(CRunPVToolkit.viewFeedbackViewFeedbackSubStepType)) {
				componentIds.add("viewFeedbackSubStepDiv");
			}
		}
		return componentIds;
	}
	
	@Override
	protected boolean updateSubSteps(List<IXMLTag> subStepTags) {
		List<Boolean> subStepsDone = getSubStepsDone(subStepTags);
		boolean lChanged = false;
		for (IXMLTag subStepTag : subStepTags) {
			int lInd = subStepTags.indexOf(subStepTag);
			if (updateSubStepAccessible(subStepTags, subStepsDone, lInd)) {
				lChanged = true;
			}
			if (updateSubStepFinished(subStepTags, subStepsDone, lInd)) {
				lChanged = true;
			}
		}
		return lChanged;
	}

	protected List<Boolean> getSubStepsDone(List<IXMLTag> subStepTags) {
		return pvToolkit.getViewFeedbackSubStepsDone(subStepTags);
	}

	@Override
	protected boolean specificStepFinished() {
		if (!pvToolkit.mustRateGivenFeedback(_actor)) {
			return true;
		}
		IXMLTag lPracticeTag = pvToolkit.getSharedPracticeTagsPerFeedbackStep(_actor).get(pvToolkit.getCurrentCycleNumber() - 1);
		List<IXMLTag> lRatingTags = pvToolkit.getStatusChildTag(lPracticeTag).getChilds(CRunPVToolkit._feedbackRatingKeyStr);
		List<IERunGroup> lUsersThatReceiveFeedbackRating = pvToolkit.getUsersThatReceiveFeedbackRating(_actor);
		if (lRatingTags.size() < lUsersThatReceiveFeedbackRating.size())
			return false;
		for (IXMLTag lRatingTag : lRatingTags) {
			String lLevel = pvToolkit.getStatusChildTagAttribute(lRatingTag, CRunPVToolkit._feedbackRatingLevelStr);
			if (StringUtils.isEmpty(lLevel) || lLevel.equals("0"))
				return false;
		}
		return true;

	}
	
	@Override
	public void setStepFinished(boolean value) {
		if (value) {
			if (pvToolkit.mustRateGivenFeedback(_actor)) {
				pvToolkit.mailNotification(CRunPVToolkit.onFeedbackRated, pvToolkit.getCurrentStepTag(), pvToolkit.getSharedPracticeTagsPerFeedbackStep(_actor).get(pvToolkit.getCurrentCycleNumber() - 1));
			}
		}
		super.setStepFinished(value);
	}
	
}
