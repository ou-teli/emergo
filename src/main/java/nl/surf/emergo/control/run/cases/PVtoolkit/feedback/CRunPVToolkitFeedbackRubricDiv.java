/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.feedback;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Vbox;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitVideoExamplesDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefHbox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefVbox;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTag;
import nl.surf.emergo.domain.IERunGroup;

public class CRunPVToolkitFeedbackRubricDiv extends CRunPVToolkitFeedbackDiv {

	private static final long serialVersionUID = -4095095702665323673L;

	/** skill cluster buttons show radio behavior, so keep track of previous subskills div */
	protected Component previousSubskillsDiv;
	
	protected int _numberOfSkillClusters = 0;
	
	protected int _numberOfPerformanceLevels = 0;
	protected int _numberOfPerformanceLevelsToShow  = 0;

	protected final static int buttonWidth = 160;
	protected final static int buttonLeftOffset = 230;

	public void init(IERunGroup actor, IXMLTag currentStepTag, IXMLTag currentTag, boolean editable, boolean unfolded) {
		_actor = actor;
		_currentStepTag = currentStepTag;
		_currentTag = currentTag;
		_editable = editable;
		_unfolded = unfolded;
		
		pvToolkit = (CRunPVToolkit)CDesktopComponents.vView().getComponent("pvToolkit");

		setClass(_classPrefix + "Rubric");

		caseComponent = pvToolkit.getCurrentRubricsCaseComponent();
		skillTag = pvToolkit.getCurrentSkillTag();
		if (skillTag == null) {
			return;
		}
		
		_numberOfSkillClusters = pvToolkit.getNumberOfSkillClusters(pvToolkit.getCurrentRubricsCaseComponent(), skillTag);

		_numberOfPerformanceLevels = pvToolkit.getNumberOfPerformanceLevels();
		_numberOfPerformanceLevelsToShow = pvToolkit.getNumberOfPerformanceLevelsToShow();

		update();
		_initialized = true;
	}
	
	public void update() {
		getChildren().clear();
		
		pvToolkit.setMemoryCaching(true);
		
		String feedbackLevelOfScores = pvToolkit.getFeedbackLevelOfScores(pvToolkit.getCurrentMethodTag(), pvToolkit.getCurrentCycleTag());
		String feedbackLevelOfTipsAndTops = pvToolkit.getFeedbackLevelOfTipsAndTops(pvToolkit.getCurrentMethodTag(), pvToolkit.getCurrentCycleTag());
		boolean mayScoreSkill = pvToolkit.getMayScoreSkill(pvToolkit.getCurrentMethodTag(), pvToolkit.getCurrentCycleTag());
		boolean mayTipsAndTopsSkill = pvToolkit.getMayTipsAndTopsSkill(pvToolkit.getCurrentMethodTag(), pvToolkit.getCurrentCycleTag());
		
		_skillclusterHasScore = feedbackLevelOfScores.equals("skillcluster") && !inRubric();
		_skillclusterHasTipsTops = feedbackLevelOfTipsAndTops.equals("skillcluster") && !inRubric();
		_subskillHasScore = feedbackLevelOfScores.equals("subskill") && !inRubric();
		_subskillHasTipsTops = feedbackLevelOfTipsAndTops.equals("subskill") && !inRubric();
		_skillHasScore = (feedbackLevelOfScores.equals("skill") || mayScoreSkill) && !inRubric();
		_skillHasTipsTops = (feedbackLevelOfTipsAndTops.equals("skill") || mayTipsAndTopsSkill) && !inRubric();

		if (_showPreviousOverview) {
			new CRunPVToolkitDefDiv(this, 
					new String[]{"class"}, 
					new Object[]{_classPrefix + "Background"}
			);
		}
		
		if (inRubric()) {
			//show skill
			new CRunPVToolkitDefLabel(this, 
					new String[]{"class", "cacAndTag", "tagChildName"}, 
					new Object[]{"font " + _classPrefix + "Skill", new CRunPVToolkitCacAndTag(caseComponent, skillTag), "name"}
			);
			//show link to video examples
			CRunPVToolkitCacAndTag runPVToolkitCacAndTag = new CRunPVToolkitCacAndTag(caseComponent, skillTag);
			List<IXMLTag> skillExampleTags = pvToolkit.getPresentChildTags(runPVToolkitCacAndTag, "videoskillexample");
			if (skillExampleTags.size() > 0) {
				String labelKey = "PV-toolkit-rubric.button.videoexample";
				if (skillExampleTags.size() > 1) {
					labelKey += "s";
				}
				Label link = new CRunPVToolkitDefLabel(this, 
						new String[]{"class", "labelKey"}, 
						new Object[]{"font gridLink " + _classPrefix + "SkillVideoExamples", labelKey}
						);
				addRubricVideoExamplesLinkOnClickEventListener(link, runPVToolkitCacAndTag, "videoskillexample", false);
			}
			//show link to rubric pdf
			pvToolkit.showDataLink(pvToolkit.getCurrentRubricTag(), "blob", this, "", "font gridLink " + _classPrefix + "ShowRubricAsPdf", CDesktopComponents.vView().getLabel("PV-toolkit-rubric.button.openrubricaspdf"));
		}

		Div rubricDiv = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "RubricDiv"}
		);

		Hbox hbox = new CRunPVToolkitDefHbox(rubricDiv, null, null);

		Vbox vbox1 = new CRunPVToolkitDefVbox(hbox, null, null);
		vbox1.setClass(_classPrefix + "RubricHbox1");
		Vbox vbox2 = null;
		if (inOverview()) {
			vbox2 = new CRunPVToolkitDefVbox(hbox, null, null);
			vbox2.setClass(_classPrefix + "RubricHbox2");
		}
		
		Vbox currentVbox = vbox1;
		int totalNumberOfSubSkills = getNumberOfSubSkills();
		int numberOfSubSkills = 0;
		int skillclusterCounter = 1;

		List<IXMLTag> parentTags = new ArrayList<IXMLTag>();
		//NOTE a rubric contains either skill clusters, directly only the skill tag, and sub skills within them or only sub skills directly under the skill tag. A mixture is not allowed.
		if (_numberOfSkillClusters > 0) {
			parentTags = skillTag.getChilds("skillcluster");
		}
		else {
			parentTags.add(skillTag);
		}
		
		for (IXMLTag parentTag : parentTags) {
			if (pvToolkit.isRunSpecificTagPresent(new CRunPVToolkitCacAndTag(caseComponent, parentTag))) {
				
				if (inOverview() && (numberOfSubSkills > totalNumberOfSubSkills / 2)) {
					currentVbox = vbox2;
				}
				
				List<Integer> subskillLevels = getSubSkillLevelsPerSkillCluster(parentTag);
				
				if (_numberOfSkillClusters > 0 || inRubric()) {
					//NOTE if no skill clusters and class is used to show rubric misuse skill cluster component to show stars that indicate performance levels
					int level = getSkillClusterLevel(parentTag);
					createSkillClusterComponent(
							currentVbox, 
							parentTag, 
							skillclusterCounter, 
							level, 
							subskillLevels);
				}

				Div div = new CRunPVToolkitDefDiv(currentVbox, 
						new String[]{"class", "visible"}, 
						new Object[]{_classPrefix + "SubSkillsDiv", "" + (_unfolded || _numberOfSkillClusters == 0)}
				);
	
				int subskillCounter = 1;
				List<IXMLTag> subskilltags = parentTag.getChilds("subskill");
				for (IXMLTag subskillTag : subskilltags) {
					if (pvToolkit.isRunSpecificTagPresent(new CRunPVToolkitCacAndTag(caseComponent, subskillTag))) {
						createSubSkillComponent(
								div, 
								subskillTag, 
								skillclusterCounter, 
								subskillCounter, 
								subskillLevels);

						subskillCounter++;
						numberOfSubSkills++;
					}
				}

				skillclusterCounter++;
			}
		}

		Div div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "SkillDiv"}
		);

		if (!inRubric()) {
			createSkillComponent(
					div, 
					skillTag, 
					getSkillLevel(skillTag));
		}

		pvToolkit.setMemoryCaching(false);
		
	}

	public void addRubricVideoExamplesLinkOnClickEventListener(Component component, CRunPVToolkitCacAndTag runPVToolkitCacAndTag, String childTagName, boolean performanceExamples) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				CRunPVToolkitVideoExamplesDiv feedbackScoreLevelVideoExamples = (CRunPVToolkitVideoExamplesDiv)CDesktopComponents.vView().getComponent(_idPrefix + "VideoExamplesDiv");
				if (feedbackScoreLevelVideoExamples != null) {
					feedbackScoreLevelVideoExamples.init(runPVToolkitCacAndTag, childTagName, performanceExamples);
					feedbackScoreLevelVideoExamples.setVisible(true);
				}
			}
		});
	}
	
	protected Component createSkillClusterComponent(
			Component parent, 
			IXMLTag skillclusterTag, 
			int skillclusterCounter, 
			int level, 
			List<Integer> subskillLevels) {
		CRunPVToolkitFeedbackSkillClusterButton button = new CRunPVToolkitFeedbackSkillClusterButton(parent,
				new String[]{"id", "class"},
				new Object[]{_idPrefix + "SkillClusterButton_" + skillclusterTag.getAttribute(AppConstants.keyId), _classPrefix + "SkillClusterButton"}
		);
		button.setAttribute("skillclusterCounter", skillclusterCounter);
		button.init(new CRunPVToolkitCacAndTag(caseComponent, skillclusterTag), "name", level, subskillLevels, getId(), _stepPrefix, _classPrefix + "SkillCluster");
		return button;
	}
	
	public Component getPreviousSubskillsDiv() {
		return previousSubskillsDiv;
	}

	public void setPreviousSubskillsDiv(Component previousSubskillsDiv) {
		this.previousSubskillsDiv = previousSubskillsDiv;
	}

	protected Component createSubSkillComponent(
			Component parent, 
			IXMLTag subskillTag, 
			int skillclusterCounter, 
			int subskillCounter, 
			List<Integer> subskillLevels) {
		String classPrefix = "";
		if (_numberOfSkillClusters > 0) {
			classPrefix = _classPrefix + "SubSkill";
		}
		else {
			//NOTE if no skill clusters use class of skill component so position and width of sub skill components is correct
			classPrefix = _classPrefix + "Skill";
		}
		CRunPVToolkitFeedbackSubSkillButton button = new CRunPVToolkitFeedbackSubSkillButton(parent,
				new String[]{"id", "class"}, 
				new Object[]{_idPrefix + "SubSkillButton_" + subskillTag.getAttribute(AppConstants.keyId), classPrefix + "Button"}
				);
		button.setAttribute("skillclusterCounter", skillclusterCounter);
		button.setAttribute("subskillCounter", subskillCounter);
		button.init(new CRunPVToolkitCacAndTag(caseComponent, subskillTag), new String[]{"name", "description"}, subskillLevels.get(subskillCounter - 1), getId(), _stepPrefix, classPrefix);
		return button;
	}
	
	protected Component createSkillComponent(
			Component parent, 
			IXMLTag skillTag, 
			Integer skillLevel) {
		CRunPVToolkitFeedbackSkillButton button = new CRunPVToolkitFeedbackSkillButton(parent,
				new String[]{"id", "class"}, 
				new Object[]{_idPrefix + "SkillButton_" + skillTag.getAttribute(AppConstants.keyId), _classPrefix + "SkillButton"}
				);
		button.setAttribute("skillclusterCounter", 0);
		button.setAttribute("subskillCounter", 0);
		button.init(new CRunPVToolkitCacAndTag(caseComponent, skillTag), new String[]{"name", "description"}, skillLevel, getId(), _stepPrefix, _classPrefix + "Skill");
		return button;
	}
	
	public void saveLevel(IXMLTag skillTag, int level) {
		super.saveLevel(skillTag, level);
		//update interface
		updateInterface(skillTag, level);
	}
	
	public void saveTips(IXMLTag skillTag, String tips) {
		super.saveTips(skillTag, tips);
		//update interface
		updateInterface(skillTag, 0);
	}

	public void saveTops(IXMLTag skillTag, String tops) {
		super.saveTops(skillTag, tops);
		//update interface
		updateInterface(skillTag, 0);
	}

	public void updateInterface(IXMLTag skillTag, int level) {
		//NOTE if skill tag is updated, only skill button has to be updated
		//if skill cluster tag is updated, only skill cluster button has to be updated
		//if sub skill tag is updated, both skill cluster and sub skill button have to be updated, because skill cluster button indicates which sub skills are scored
		if (skillTag.getName().equals("skill")) {
			CRunPVToolkitFeedbackSkillButton skillButton = (CRunPVToolkitFeedbackSkillButton)CDesktopComponents.vView().getComponent(_idPrefix + "SkillButton_" + skillTag.getAttribute(AppConstants.keyId));
			if (skillButton != null) {
				if (level > 0) {
					skillButton.update(level);
				}
				else {
					skillButton.update();
				}
			}
			return;
		}
		IXMLTag skillclusterTag = skillTag;
		if (skillTag.getName().equals("subskill")) {
			skillclusterTag = skillTag.getParentTag();
		}
		CRunPVToolkitFeedbackSkillClusterButton skillClusterButton = (CRunPVToolkitFeedbackSkillClusterButton)CDesktopComponents.vView().getComponent(_idPrefix + "SkillClusterButton_" + skillclusterTag.getAttribute(AppConstants.keyId));
		if (skillClusterButton != null) {
			skillClusterButton.update(getSkillClusterLevel(skillclusterTag), getSubSkillLevelsPerSkillCluster(skillclusterTag));
		}
		if (skillTag.getName().equals("subskill")) { 
			CRunPVToolkitFeedbackSubSkillButton subskillButton = (CRunPVToolkitFeedbackSubSkillButton)CDesktopComponents.vView().getComponent(_idPrefix + "SubSkillButton_" + skillTag.getAttribute(AppConstants.keyId));
			if (subskillButton != null) {
				if (level > 0) {
					subskillButton.update(level);
				}
				else {
					subskillButton.update();
				}
			}
		}
	}
	
	@Override
	public boolean feedbackIsReady() {
		//NOTE during giving feedback, a user is ready if feedback has been set ready before, or if all accessible sub skills and the skill are scored.
		return super.feedbackIsReady() || (getNumberOfGivenSubSkillScores() == getNumberOfAccessibleSubSkills() && getSkillLevel(skillTag) > 0);
	}
	
}
