/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.superTeacher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.util.Clients;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.domain.IECaseComponent;

public class CRunPVToolkitSuperTeacherUpdateMethodCyclesDiv extends CRunPVToolkitSuperTeacherNewMethodDiv {

	private static final long serialVersionUID = -4522501640293840196L;
	
	protected static final String methodTagName = "method";
	protected static final String cycleTagName = "cycle";
	protected static final String stepTagName = "step";
	protected static final String substepTagName = "substep";
	
	protected StringBuffer methodElements;

	public void update() {
		//NOTE rubric is always stored in XML data and status that is cached within the SSpring class, regardless if preview is read only or not.
		IXMLTag nodeTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get("cacId")), _idMap.get("tagId"));
		if (nodeTag != null) {
			initMethodElements(nodeTag);
			
			Clients.evalJavaScript("initUpdateMethodCycles('" + methodElements.toString() + "');");
		}
	}
		
	protected void initMethodElements(IXMLTag methodTag) {
		methodElements = new StringBuffer();
		
		int counter = 1;
		for (IXMLTag cycleTag : methodTag.getChilds(cycleTagName)) {
			addMethodElement(cycleTag.getParentTag(), cycleTag, counter);
			counter++;
		}
		
		methodElements.insert(0, "[");
		methodElements.append("]");
	}
		
	protected void addMethodElement(IXMLTag parentTag, IXMLTag nodeTag, int counter) {
		StringBuffer element = new StringBuffer();
		addElementPart(element, "parentTagId", parentTag.getAttribute(AppConstants.keyId), true);
		addElementPart(element, "parentTagName", parentTag.getName(), true);
		addElementPart(element, "tagId", nodeTag.getAttribute(AppConstants.keyId), true);
		addElementPart(element, "tagName", nodeTag.getName(), true);
		String name = sSpring.unescapeXML(nodeTag.getChildValue("name"));
		name = name.equals("") ? vView.getLabel("PV-toolkit-superTeacher.updatemethodcycles.label.cycle") + counter : name;
		addElementPart(element, "name", name, true);
		addElementPart(element, "referenceidsforstudent", getStatusAttributeValue(nodeTag, "referenceidsforstudent").replace(AppConstants.statusCommaReplace, ","), true);
		addElementPart(element, "referenceidsforteacher", getStatusAttributeValue(nodeTag, "referenceidsforteacher").replace(AppConstants.statusCommaReplace, ","), true);
		addElementPart(element, "referenceidsforpeerstudent", getStatusAttributeValue(nodeTag, "referenceidsforpeerstudent").replace(AppConstants.statusCommaReplace, ","), true);
		StringBuffer stepOrSubstepElements = new StringBuffer();
		int i = 0;
		for (IXMLTag stepTag : nodeTag.getChilds(stepTagName)) {
			addStepOrSubstepElement(stepTag.getParentTag(), stepTag, i, -1, stepOrSubstepElements);
			List<IXMLTag> substepTags = stepTag.getChilds(substepTagName);
			if (substepTags.size() > 1) {
				int j = 0;
				for (IXMLTag substepTag : substepTags) {
					addStepOrSubstepElement(substepTag.getParentTag(), substepTag, i, j, stepOrSubstepElements);
					j++;
				}
			}
			i++;
		}
		stepOrSubstepElements.insert(0, "[");
		stepOrSubstepElements.append("]");
		addElementPart(element, "stepOrSubsteps", stepOrSubstepElements, false);
		if (methodElements.length() > 0) {
			methodElements.append(",");
		}
		methodElements.append("{");
		methodElements.append(element);
		methodElements.append("}");
	}

	protected void addStepOrSubstepElement(IXMLTag parentTag, IXMLTag nodeTag, int i, int j, StringBuffer stepOrSubstepElements) {
		StringBuffer element = new StringBuffer();
		addElementPart(element, "parentTagId", parentTag.getAttribute(AppConstants.keyId), true);
		addElementPart(element, "parentTagName", parentTag.getName(), true);
		addElementPart(element, "tagId", nodeTag.getAttribute(AppConstants.keyId), true);
		addElementPart(element, "tagName", nodeTag.getName(), true);
		addElementPart(element, "stepNumber", "" + (i + 1), true);
		String typeKey = nodeTag.getName() + "type";
		String typeValue = sSpring.unescapeXML(nodeTag.getChildValue(nodeTag.getName() + "type"));
		addElementPart(element, typeKey, typeValue, true);
		String[] keys = new String[] {"mandatoryforstudent", "mandatoryforteacher", "mandatoryforpeerstudent",
				"presentforstudent", "presentforteacher", "presentforpeerstudent"};
		if (nodeTag.getName().equals(stepTagName)) {
			addElementPart(element, "mandatoryprevioussteptype", CRunPVToolkit.stepTypes[i].getMandatoryPreviousStepType(), true);
			boolean[] values = new boolean[] {CRunPVToolkit.stepTypes[i].isMandatoryForStudent(),
					CRunPVToolkit.stepTypes[i].isMandatoryForTeacher(),
					CRunPVToolkit.stepTypes[i].isMandatoryForPeerStudent(),
					CRunPVToolkit.stepTypes[i].isDefaultPresentForStudent(),
					CRunPVToolkit.stepTypes[i].isDefaultPresentForTeacher(),
					CRunPVToolkit.stepTypes[i].isDefaultPresentForPeerStudent()};
			for (int k=0;k<keys.length;k++) {
				addElementPart(element, keys[k], !nodeTag.getCurrentStatusAttribute(keys[k]).equals("") ? nodeTag.getCurrentStatusAttribute(keys[k]) : "" + values[k], true);
			}
		}
		else if (nodeTag.getName().equals(substepTagName)) {
			addElementPart(element, "mandatoryprevioussubsteptype", CRunPVToolkit.subStepTypes[i][j].getMandatoryPreviousSubStepType(), true);
			boolean[] values = new boolean[] {CRunPVToolkit.subStepTypes[i][j].isMandatoryForStudent(),
					CRunPVToolkit.subStepTypes[i][j].isMandatoryForTeacher(),
					CRunPVToolkit.subStepTypes[i][j].isMandatoryForPeerStudent(),
					CRunPVToolkit.subStepTypes[i][j].isDefaultPresentForStudent(),
					CRunPVToolkit.subStepTypes[i][j].isDefaultPresentForTeacher(),
					CRunPVToolkit.subStepTypes[i][j].isDefaultPresentForPeerStudent()};
			for (int k=0;k<keys.length;k++) {
				addElementPart(element, keys[k], !nodeTag.getCurrentStatusAttribute(keys[k]).equals("") ? nodeTag.getCurrentStatusAttribute(keys[k]) : "" + values[k], true);
			}
		}
		String name = sSpring.unescapeXML(nodeTag.getChildValue("name"));
		name = name.equals("") ? vView.getLabel("PV-toolkit." + typeKey + "." + typeValue) : name;
		addElementPart(element, "name", name, true);
		name = sSpring.unescapeXML(nodeTag.getChildValue("nameforteacher"));
		name = name.equals("") ? vView.getLabel("PV-toolkit." + typeKey + "." + typeValue + ".forteacher") : name;
		addElementPart(element, "nameforteacher", name, true);
		name = sSpring.unescapeXML(nodeTag.getChildValue("nameforpeerstudent"));
		name = name.equals("") ? vView.getLabel("PV-toolkit." + typeKey + "." + typeValue + ".forpeerstudent") : name;
		addElementPart(element, "nameforpeerstudent", name, true);
		addElementPart(element, "referenceidsforstudent", getStatusAttributeValue(nodeTag, "referenceidsforstudent").replace(AppConstants.statusCommaReplace, ","), true);
		addElementPart(element, "referenceidsforteacher", getStatusAttributeValue(nodeTag, "referenceidsforteacher").replace(AppConstants.statusCommaReplace, ","), true);
		addElementPart(element, "referenceidsforpeerstudent", getStatusAttributeValue(nodeTag, "referenceidsforpeerstudent").replace(AppConstants.statusCommaReplace, ","), false);
		if (stepOrSubstepElements.length() > 0) {
			stepOrSubstepElements.append(",");
		}
		stepOrSubstepElements.append("{");
		stepOrSubstepElements.append(element);
		stepOrSubstepElements.append("}");
	}

	public void onNotify (Event event) {
		JSONObject jsonObject = (JSONObject)event.getData();
		String action = (String)jsonObject.get("action");
		if (action.equals("select_step_or_substep")) {
			String tagId = (String)jsonObject.get("tagId");
	    	String role = (String)jsonObject.get("role");
	    	String selected = (String)jsonObject.get("selected");
			IXMLTag elementTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get("cacId")), tagId);
			if (elementTag != null) {
				editElementTagStatusAttributeValue(elementTag, "method", "presentfor" + role, selected);
			}
		}
		else if (action.equals("edit_method_part")) {
			String tagId = (String)jsonObject.get("tagId");
	    	String childTagName = (String)jsonObject.get("childTagName");
	    	String childTagValue = (String)jsonObject.get("childTagValue");
			IXMLTag elementTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get("cacId")), tagId);
			if (elementTag != null) {
				editElementTagChildValue(elementTag, "method", childTagName, childTagValue);
			}
		}
		else {
			super.onNotify(event);
		}
	}
	
	public List<IXMLTag> getSkillTagBeforeAndAfterTagForCreate(IXMLTag skillTag, String tagName) {
		IXMLTag tagBeforeTag = null;
		IXMLTag tagAfterTag = null;
		List<IXMLTag> subskillTags = skillTag.getChilds(substepTagName);
		List<IXMLTag> skillclusterTags = skillTag.getChilds(stepTagName);
		//if skill element is sub skill add it after the last sub skill located directly under the skill tag, or if no sub skills add it after the last skill cluster tag
		if (tagName.equals(substepTagName)) {
			if (subskillTags.size() > 0) {
				tagBeforeTag = subskillTags.get(subskillTags.size() - 1); 
			}
			else if (skillclusterTags.size() > 0) {
				tagBeforeTag = skillclusterTags.get(skillclusterTags.size() - 1);
			}
		}
		//if skill element is skill cluster add it just after the last skill cluster or if no skill cluster just before the first sub skill located directly under the skill tag 
		else if (tagName.equals(stepTagName)) {
			if (skillclusterTags.size() > 0) {
				tagBeforeTag = skillclusterTags.get(skillclusterTags.size() - 1);
			}
			else if (subskillTags.size() > 0) {
				tagAfterTag = subskillTags.get(0); 
			}
		}
		List<IXMLTag> beforeAndAfterTag = new ArrayList<IXMLTag>();
		beforeAndAfterTag.add(tagBeforeTag);
		beforeAndAfterTag.add(tagAfterTag);
		return beforeAndAfterTag;
	}
	
	public List<IXMLTag> getSkillTagBeforeAndAfterTagForMove(IXMLTag skillTag, IXMLTag draggedElementTag, IXMLTag targetElementTag, boolean dropBelowTargetElement) {
		IXMLTag tagBeforeTag = null;
		IXMLTag tagAfterTag = null;
		List<IXMLTag> subskillTags = skillTag.getChilds(substepTagName);
		List<IXMLTag> skillclusterTags = skillTag.getChilds(stepTagName);
		//if skill element is sub skill move it after the last sub skill located directly under the skill tag, or if no sub skills add it before the first skill cluster tag
		if (draggedElementTag.getName().equals(substepTagName)) {
			if (subskillTags.size() > 0) {
				tagBeforeTag = subskillTags.get(subskillTags.size() - 1); 
			}
			else if (skillclusterTags.size() > 0) {
				tagAfterTag = skillclusterTags.get(0);
			}
		}
		//if skill element is skill cluster move it just below or above the target element, which always will be a skill cluster 
		else if (draggedElementTag.getName().equals(stepTagName)) {
			if (dropBelowTargetElement) {
				//move just below target skill cluster
				tagBeforeTag = targetElementTag;
			}
			else {
				//move just above target skill cluster
				tagAfterTag = targetElementTag;
			}
		}
		List<IXMLTag> beforeAndAfterTag = new ArrayList<IXMLTag>();
		beforeAndAfterTag.add(tagBeforeTag);
		beforeAndAfterTag.add(tagAfterTag);
		return beforeAndAfterTag;
	}
	
	protected IXMLTag addSkillElementTag(IXMLTag parentTag, IXMLTag tagBeforeNewTag, IXMLTag tagAfterNewTag, String tagName, String elementName) {
		IXMLTag elementTag = null;
		IECaseComponent caseComponent = sSpring.getCaseComponent(_idMap.get("cacId"));
		Map<String,String> childTagNameValueMap = new HashMap<String,String>();
		childTagNameValueMap.put("pid", elementName);
		childTagNameValueMap.put("name", elementName);
		if (!sSpring.isReadOnlyRun()) {
			//NOTE if not read only run update data tree. It is updated in application memory as well.
			IXMLTag dataRootTag = sSpring.getXmlDataTree(caseComponent);
			parentTag = sSpring.getXmlManager().getTagById(dataRootTag, parentTag.getAttribute(AppConstants.statusKeyId));
			IXMLTag nodeTag = null;
			if (tagBeforeNewTag != null) {
				tagBeforeNewTag = sSpring.getXmlManager().getTagById(dataRootTag, tagBeforeNewTag.getAttribute(AppConstants.statusKeyId));
				nodeTag = addNodeTagAfterTag(parentTag, tagBeforeNewTag, tagName, childTagNameValueMap);
			}
			else if (tagAfterNewTag != null) {
				tagAfterNewTag = sSpring.getXmlManager().getTagById(dataRootTag, tagAfterNewTag.getAttribute(AppConstants.statusKeyId));
				nodeTag = addNodeTagBeforeTag(parentTag, tagAfterNewTag, tagName, childTagNameValueMap);
			}
			else {
				nodeTag = addNodeTag(parentTag, tagName, childTagNameValueMap);
			}
			if (nodeTag != null) {
				addPerformancelevelTags(nodeTag);
			}
			sSpring.setCaseComponentData("" + caseComponent.getCacId(), sSpring.getXmlManager().xmlTreeToDoc(dataRootTag));
		}
		//NOTE always update data plus status tree cached as SSpring property. This will work for both preview and preview read-only mode. 
		IXMLTag dataPlusStatusRootTag = sSpring.getXmlDataPlusRunStatusTree(caseComponent, AppConstants.statusTypeRunGroup);
		parentTag = sSpring.getXmlManager().getTagById(dataPlusStatusRootTag, parentTag.getAttribute(AppConstants.statusKeyId));
		if (tagBeforeNewTag != null) {
			tagBeforeNewTag = sSpring.getXmlManager().getTagById(dataPlusStatusRootTag, tagBeforeNewTag.getAttribute(AppConstants.statusKeyId));
			elementTag = addNodeTagAfterTag(parentTag, tagBeforeNewTag, tagName, childTagNameValueMap);
		}
		else if (tagAfterNewTag != null) {
			tagAfterNewTag = sSpring.getXmlManager().getTagById(dataPlusStatusRootTag, tagAfterNewTag.getAttribute(AppConstants.statusKeyId));
			elementTag = addNodeTagBeforeTag(parentTag, tagAfterNewTag, tagName, childTagNameValueMap);
		}
		else {
			elementTag = addNodeTag(parentTag, tagName, childTagNameValueMap);
		}
		if (elementTag != null) {
			addPerformancelevelTags(elementTag);
		}
		
		//NOTE return data plus status element because it is always set
		return elementTag;
	}

	protected IXMLTag moveSkillElementTag(IXMLTag parentTag, IXMLTag draggedTag, IXMLTag tagBeforeTag, IXMLTag tagAfterTag) {
		IXMLTag elementTag = null;
		IECaseComponent caseComponent = sSpring.getCaseComponent(_idMap.get("cacId"));
		if (!sSpring.isReadOnlyRun()) {
			//NOTE if not read only run update data tree. It is updated in application memory as well.
			IXMLTag dataRootTag = sSpring.getXmlDataTree(caseComponent);
			parentTag = sSpring.getXmlManager().getTagById(dataRootTag, parentTag.getAttribute(AppConstants.statusKeyId));
			draggedTag = sSpring.getXmlManager().getTagById(dataRootTag, draggedTag.getAttribute(AppConstants.statusKeyId));
			if (tagBeforeTag != null) {
				tagBeforeTag = sSpring.getXmlManager().getTagById(dataRootTag, tagBeforeTag.getAttribute(AppConstants.statusKeyId));
				moveNodeTagAfterTag(parentTag, draggedTag, tagBeforeTag);
			}
			else if (tagAfterTag != null) {
				tagAfterTag = sSpring.getXmlManager().getTagById(dataRootTag, tagAfterTag.getAttribute(AppConstants.statusKeyId));
				moveNodeTagBeforeTag(parentTag, draggedTag, tagAfterTag);
			}
			else {
				moveNodeTag(parentTag, draggedTag);
			}
			sSpring.setCaseComponentData("" + caseComponent.getCacId(), sSpring.getXmlManager().xmlTreeToDoc(dataRootTag));
		}
		//NOTE always update data plus status tree cached as SSpring property. This will work for both preview and preview read-only mode. 
		IXMLTag dataPlusStatusRootTag = sSpring.getXmlDataPlusRunStatusTree(caseComponent, AppConstants.statusTypeRunGroup);
		parentTag = sSpring.getXmlManager().getTagById(dataPlusStatusRootTag, parentTag.getAttribute(AppConstants.statusKeyId));
		draggedTag = sSpring.getXmlManager().getTagById(dataPlusStatusRootTag, draggedTag.getAttribute(AppConstants.statusKeyId));
		if (tagBeforeTag != null) {
			tagBeforeTag = sSpring.getXmlManager().getTagById(dataPlusStatusRootTag, tagBeforeTag.getAttribute(AppConstants.statusKeyId));
			elementTag = moveNodeTagAfterTag(parentTag, draggedTag, tagBeforeTag);
		}
		else if (tagAfterTag != null) {
			tagAfterTag = sSpring.getXmlManager().getTagById(dataPlusStatusRootTag, tagAfterTag.getAttribute(AppConstants.statusKeyId));
			elementTag = moveNodeTagBeforeTag(parentTag, draggedTag, tagAfterTag);
		}
		else {
			elementTag = moveNodeTag(parentTag, draggedTag);
		}
		
		//NOTE return data plus status element because it is always set
		return elementTag;
	}

	protected IXMLTag deleteSkillElementTag(IXMLTag elementTag, boolean ifSkillClusterTagKeepSubSkillTags) {
		IECaseComponent caseComponent = getCaseComponent("rubric");
		if (!sSpring.isReadOnlyRun()) {
			//NOTE if not read only run update data tree. It is updated in application memory as well.
			IXMLTag rootTag = sSpring.getXmlDataTree(caseComponent);
			if (ifSkillClusterTagKeepSubSkillTags) {
				elementTag = sSpring.getXmlManager().getTagById(rootTag, elementTag.getAttribute(AppConstants.statusKeyId));
				ifSkillClusterTagKeepSubSkillTags(elementTag);
			}
			deleteNodeTag(rootTag, elementTag);
			sSpring.setCaseComponentData("" + caseComponent.getCacId(), sSpring.getXmlManager().xmlTreeToDoc(rootTag));
		}
		//NOTE always update data plus status tree cached as SSpring property. This will work for both preview and preview read-only mode. 
		IXMLTag rootTag = sSpring.getXmlDataPlusRunStatusTree(caseComponent, AppConstants.statusTypeRunGroup);
		if (ifSkillClusterTagKeepSubSkillTags) {
			elementTag = sSpring.getXmlManager().getTagById(rootTag, elementTag.getAttribute(AppConstants.statusKeyId));
			ifSkillClusterTagKeepSubSkillTags(elementTag);
		}
		elementTag = deleteNodeTag(rootTag, elementTag);
		//NOTE return data plus status element because it is always set
		return elementTag;
	}
	
	protected void ifSkillClusterTagKeepSubSkillTags(IXMLTag elementTag) {
		if (!elementTag.getName().equals(stepTagName)) {
			return;
		}
		IXMLTag skillTag = elementTag.getParentTag();
		List<IXMLTag> subskillTagsOfSkillTag = skillTag.getChilds(substepTagName);
		IXMLTag tagBeforeNewTag = null; 
		if (subskillTagsOfSkillTag.size() > 0) {
			tagBeforeNewTag = subskillTagsOfSkillTag.get(subskillTagsOfSkillTag.size() - 1);
		}
		List<IXMLTag> subskillTags = elementTag.getChilds(substepTagName);
		for (IXMLTag subSkillTag : subskillTags) {
			subSkillTag.getParentTag().getChildTags().remove(subSkillTag);
			subSkillTag.setParentTag(skillTag);
			int index = 0;
			if (tagBeforeNewTag != null) {
				index = skillTag.getChildTags().indexOf(tagBeforeNewTag) + 1;
			}
			skillTag.getChildTags().add(index, subSkillTag);
			tagBeforeNewTag = subSkillTag;
		}
	}
	
}