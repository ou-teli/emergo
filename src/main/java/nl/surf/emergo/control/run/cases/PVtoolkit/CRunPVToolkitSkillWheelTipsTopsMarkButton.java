/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit;

import java.util.List;

import org.zkoss.zk.ui.Component;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitFeedbackTipOrTop;
import nl.surf.emergo.domain.IERunGroup;

public class CRunPVToolkitSkillWheelTipsTopsMarkButton extends CRunPVToolkitDefImage {
	
	private static final long serialVersionUID = 4193424264788544303L;
	
	public String zulfilepath = ((CRunPVToolkitInitBox)CDesktopComponents.vView().getComponent("PV-toolkit_initBox")).getZulfilepath();
	
	protected CRunPVToolkit pvToolkit;
	
	protected IERunGroup _actor;
	protected boolean _editable;
	protected CRunPVToolkitFeedbackTipOrTop _feedback;
	public IXMLTag _skillTag;
	protected String _idPrefix;
	protected String _idMarkButtonPart = "_TipsTopsMarkButton_";

	public CRunPVToolkitSkillWheelTipsTopsMarkButton(Component parent, String[] keys, Object[] values) {
		super(parent, keys, values);
	}
	
	public void init(IERunGroup actor, boolean editable, CRunPVToolkitFeedbackTipOrTop feedback, String idPrefix, int presentationLevel) {
		_actor = actor;
		_editable = editable;
		_feedback = feedback;
		_idPrefix = idPrefix;
		
		pvToolkit = (CRunPVToolkit)CDesktopComponents.vView().getComponent("pvToolkit");
		
		IXMLTag skillTag = pvToolkit.getFeedbackTipsOrTopsSkillTag(feedback);

		String idPostfix = _idMarkButtonPart + presentationLevel + "_" + skillTag.getAttribute(AppConstants.keyId) + "_" + feedback.getPeer().getRgaId() + "_";
		if (feedback.isTip()) {
			idPostfix += "tip";
		}
		else if (feedback.isTop()) {
			idPostfix += "top";
		}
		setId(idPrefix + idPostfix);
		_skillTag = skillTag;
		
		setImage(feedback.isMark());
		if (!_editable) {
			setStyle("cursor:default;");
		}
	}

	protected void setImage(boolean mark) {
		String markImage = "";
		if (mark) {
			markImage = "star-filled-blue.svg";
		}
		else {
			markImage = "star.svg";
		}
		setSrc(zulfilepath + markImage);
	}

	public void onClick() {
		if (!_editable) {
			return;
		}
		_feedback.setMark(!_feedback.isMark());
		setImage(_feedback.isMark());
		pvToolkit.saveMarkedFeedbackTipsOrTops(_actor, _feedback);
		
		//NOTE mark button may be used in other panels.
		//so check if it is existing in other panels and if so update the feedback and the image
		List<Component> components = CDesktopComponents.vView().getComponentsByMatch("(.*)" + _idMarkButtonPart + "(.*)");
		for (Component component : components) {
			if (component != this && component instanceof CRunPVToolkitSkillWheelTipsTopsMarkButton) {
				CRunPVToolkitSkillWheelTipsTopsMarkButton markButton = (CRunPVToolkitSkillWheelTipsTopsMarkButton)component;
				if (markButton._skillTag == this._skillTag &&
						markButton._feedback.getPeer().getRgaId() == this._feedback.getPeer().getRgaId() && 
						markButton._feedback.isTip() == this._feedback.isTip() &&
						markButton._feedback.isTop() == this._feedback.isTop()) {
					markButton._feedback.setMark(_feedback.isMark());
					markButton.setImage(_feedback.isMark());
				}
			}
		}
	}

}
