/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.myRecordings;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.def.CDefHelper;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitInitBox;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitVideoRecordingLink;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefHbox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.cases.PVtoolkit.feedback.CRunPVToolkitFeedbackPreviousOverviewRubricDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTag;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTags;
import nl.surf.emergo.control.run.cases.PVtoolkit.practice.CRunPVToolkitPracticeFeedbackDiv;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.view.VView;

public class CRunPVToolkitMyRecordingsRecordingsDiv extends CDefDiv {

	private static final long serialVersionUID = 7883986700440575558L;

	public String zulfilepath = ((CRunPVToolkitInitBox)CDesktopComponents.vView().getComponent("PV-toolkit_initBox")).getZulfilepath();
	
	protected CRunPVToolkit pvToolkit;
		
	protected IERunGroup _actor;
	protected boolean _editable;
	protected boolean _hasSelfFeedbackSubSteps = false;
	
	protected CRunPVToolkitCacAndTags myRecordings;

	protected String _stepPrefix = "myRecordings";
	protected String _idPrefix = "myRecordings";
	protected String _classPrefix = "myRecordings";
	
	public void init(IERunGroup actor, boolean editable) {
		_actor = actor;
		_editable = editable;
		_hasSelfFeedbackSubSteps = hasSelfFeedbackSubSteps();
		
		//NOTE always initialize, because recordings my be added during a session
		setClass(_classPrefix + "Recordings");

		pvToolkit = (CRunPVToolkit)CDesktopComponents.vView().getComponent("pvToolkit");
		
		update();
	}
	
	public void update() {
		getChildren().clear();
		
		pvToolkit.setMemoryCaching(true);
		
		Div div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{"titleRight"}
		);
		
		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font titleRight", "PV-toolkit-myRecordings.header.myRecordings"}
		);
		
		div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "RecordingsDiv"}
		);
		
		Rows rows = appendRecordingsGrid(div);
		int rowNumber = 1;
		rowNumber = appendRecordingsToGrid(rows, rowNumber);
		
		pvToolkit.setMemoryCaching(false);
	}

	public void update(IXMLTag practiceTag) {
		String practiceTagUuid = practiceTag.getAttribute(practiceTag.getName() + "uuid");
		VView vView = CDesktopComponents.vView();

		setPracticeLabelVariableProperties((Label)vView.getComponent(getId() + "GridRowPracticeLabel_" + practiceTagUuid), practiceTag);
	}
	
	private boolean showSelfFeedbackColumn = false;

    protected Rows appendRecordingsGrid(Component parent) {
   		showSelfFeedbackColumn = _hasSelfFeedbackSubSteps;
    	return pvToolkit.appendGrid(
    			parent, 
    			getId() + "Grid",
    			"position:absolute;left:0px;top:0px;width:1060px;height:480px;overflow:auto;", 
				new boolean[] {true, true, true, true, pvToolkit.hasCycleTags(), true, true, true, showSelfFeedbackColumn, _editable},
    			new String[] {"3%", "29%", "7%", "10%", "10%", "10%", "13%", "15%", "15%", "10%"}, 
    			new boolean[] {false, true, false, true, true, true, true, false, false, false},
    			null,
    			"PV-toolkit-myRecordings.column.label.", 
    			new String[] {"", "name", "edit", "date", "cycle", "duration", "shared", "recording", "selffeedback", "delete"});
    }

	protected int appendRecordingsToGrid(Rows rows, int rowNumber) {
		return appendRecordingsToGrid(
				rows, 
				rowNumber,
				new boolean[] {true, true, true, true, pvToolkit.hasCycleTags(), true, true, true, showSelfFeedbackColumn, _editable} 
				);
	}

	protected int appendRecordingsToGrid(
			Rows rows, 
			int rowNumber,
    		boolean[] showColumn 
			) {
		myRecordings = pvToolkit.getMyRecordings(_actor);
		if (myRecordings == null) {
			return rowNumber;
		}

		for (IXMLTag runStepTagStatusChildTag : myRecordings.getXmlTags()) {
			IXMLTag practiceStepTag = CDesktopComponents.sSpring().getCaseComponentXmlDataTagById(pvToolkit.getCurrentProjectsCaseComponent(), runStepTagStatusChildTag.getParentTag().getAttribute(AppConstants.keyRefdataid));
			int cycleNumber = 0;
			if (practiceStepTag != null && practiceStepTag.getParentTag().getName().equals("cycle")) {
				cycleNumber = pvToolkit.getCycleNumber(practiceStepTag.getParentTag());
			}

			for (IXMLTag practiceTag : runStepTagStatusChildTag.getChilds("practice")) {
				boolean practiceHasSelfFeedback = practiceHasSelfFeedback(practiceTag);
				boolean practiceIsShared = practiceIsShared(practiceTag);
				//NOTE editable if _editable and not shared
				boolean editable = _editable && !practiceIsShared;

				String practiceTagUuid = practiceTag.getAttribute(practiceTag.getName() + "uuid");
				
				Row row = new Row();
				rows.appendChild(row);
				row.setAttribute("tag", practiceTag);
				
				int columnNumber = 0;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					new CRunPVToolkitDefImage(row, 
							new String[]{"class", "src"}, 
							new Object[]{"gridImage", zulfilepath + "video.svg"}
							);
				}
	
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					Label label = new CRunPVToolkitDefLabel(row, 
							new String[]{"id", "class"}, 
							new Object[]{getId() + "GridRowPracticeLabel_" + practiceTagUuid, "font gridLabel"}
							);
					setPracticeLabelVariableProperties(label, practiceTag);
				}
	
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					Image image = new CRunPVToolkitDefImage(row,
							new String[]{"class", "src"}, 
							new Object[]{"gridImageClickable", zulfilepath + "edit.svg"}
					);
					addEditOnClickEventListener(image, practiceTag);
					image.setVisible(editable);
				}

				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					String recordingDate = pvToolkit.getStatusChildTagAttribute(practiceTag, "recordingdate");
					CRunPVToolkitDefLabel label = new CRunPVToolkitDefLabel(row, 
							new String[]{"class", "value"}, 
							new Object[]{"font gridLabel", recordingDate}
							);
					//NOTE for ordering use ymd format
					if (!StringUtils.isEmpty(recordingDate)) {
						Date date = CDefHelper.getDateFromStrDMY(recordingDate);
						label.setAttribute("orderValue", CDefHelper.getDateStrAsYMD(date));
					}
				}
				
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					new CRunPVToolkitDefLabel(row, 
							new String[]{"class", "value"}, 
							new Object[]{"font gridLabel", "" + cycleNumber}
							);
				}
	
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					String value = "";
					int webcamRecordingLength = pvToolkit.getWebcamRecordingLength(practiceTag);
					if (webcamRecordingLength >= 0) {
						value = CDefHelper.getTimeStrAsHMS(webcamRecordingLength);
					}
					new CRunPVToolkitDefLabel(row, 
							new String[]{"class", "value"}, 
							new Object[]{"font gridLabel", value}
							);
				}
	
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					String labelKey = "";
					if (pvToolkit.getStatusChildTagAttribute(practiceTag, "share").equals("true")) {
						labelKey = "PV-toolkit-myRecordings.row.label.shared.yes";
					}
					else {
						labelKey = "PV-toolkit-myRecordings.row.label.shared.no";
					}
					new CRunPVToolkitDefLabel(row, 
							new String[]{"class", "labelKey"}, 
							new Object[]{"font gridLabel", labelKey}
							);
				}
	
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					CRunPVToolkitVideoRecordingLink link = new CRunPVToolkitVideoRecordingLink(row, 
							new String[]{"class", "labelKey"}, 
							new Object[]{"font gridLink", "PV-toolkit-myRecordings.row.button.recording.view"}
							);
					link.init(cycleNumber, practiceTag, getId(), _idPrefix + "RecordingDiv", _idPrefix);
				}
	
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					Hbox hbox = new CRunPVToolkitDefHbox(row, null, null);
					Label link = new CRunPVToolkitDefLabel(hbox, 
							new String[]{"class", "labelKey"}, 
							new Object[]{"font gridLink", "PV-toolkit-practice.row.button.selffeedback.view"}
							);
					link.setVisible(_hasSelfFeedbackSubSteps && practiceHasSelfFeedback);
					addSelfFeedbackOnClickEventListener(link, _actor, practiceStepTag, practiceTag, editable);
				}

				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					Image image = new CRunPVToolkitDefImage(row,
							new String[]{"class", "src"}, 
							new Object[]{"gridImageClickable", zulfilepath + "trash.svg"}
					);
					addDeleteOnClickEventListener(image, practiceTag);
					image.setVisible(editable);
				}

				rowNumber ++;
	
			}			
		}

		return rowNumber;
	}

	public void setPracticeLabelVariableProperties(Label label, IXMLTag practiceTag) {
		if (label == null || practiceTag == null) {
			return;
		}
		label.setValue(pvToolkit.getStatusChildTagAttribute(practiceTag, "name"));
	}
	
	protected void addEditOnClickEventListener(Component component, IXMLTag practiceTag) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				CRunPVToolkitMyRecordingsEditRecording popup = (CRunPVToolkitMyRecordingsEditRecording)CDesktopComponents.vView().getComponent(_idPrefix + "EditRecording");
				if (popup != null) {
					popup.init(practiceTag);
				}
			}
		});
	}
	
	protected void addSelfFeedbackOnClickEventListener(Component component, IERunGroup actor, IXMLTag practiceStepTag, IXMLTag practiceTag, boolean editable) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				if (practiceStepTag == null) {
					return;
				}
				
				CDesktopComponents.vView().getComponent(_stepPrefix + "RecordingsDiv").setVisible(false);
				
				List<IXMLTag> previousPracticeStepTags = pvToolkit.getStepTagsInPreviousCycles(practiceStepTag, CRunPVToolkit.practiceStepType);
				boolean showPreviousFeedback = previousPracticeStepTags.size() > 0;
				if (showPreviousFeedback) {
					CRunPVToolkitFeedbackPreviousOverviewRubricDiv previousOverviewRubricDiv = (CRunPVToolkitFeedbackPreviousOverviewRubricDiv)CDesktopComponents.vView().getComponent(_stepPrefix + "PreviousOverviewRubricDiv");
					if (previousOverviewRubricDiv != null) {
						previousOverviewRubricDiv.init(actor, practiceStepTag, practiceTag, false, true);
						previousOverviewRubricDiv.setVisible(true);
					}
				}
				else {
					CRunPVToolkitPracticeFeedbackDiv feedbackDiv = (CRunPVToolkitPracticeFeedbackDiv)CDesktopComponents.vView().getComponent(_stepPrefix + "FeedbackDiv");
					if (feedbackDiv != null) {
						feedbackDiv.init(actor, practiceStepTag, practiceTag, editable);
						feedbackDiv.setVisible(true);
					}
				}
			}
		});
	}
	
	protected void addDeleteOnClickEventListener(Component component, IXMLTag practiceTag) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				VView vView = CDesktopComponents.vView();
				int choice = vView.showMessagebox(getRoot(), vView.getCLabel("PV-toolkit.delete.confirmation"), vView.getCLabel("PV-toolkit.delete"), Messagebox.CANCEL | Messagebox.OK, Messagebox.QUESTION);
				if (choice == Messagebox.OK) {
					deletePractice(practiceTag);
				}
			}
		});
	}
	
	public boolean practiceHasSelfFeedback(IXMLTag practiceTag) {
		return pvToolkit.getPracticeStatus(_actor, practiceTag, "selffeedback").equals(AppConstants.statusValueTrue);
	}
	
	public boolean practiceIsShared(IXMLTag practiceTag) {
		return pvToolkit.getPracticeStatus(_actor, practiceTag, "share").equals(AppConstants.statusValueTrue);
	}
	
	public void updatePractice(IXMLTag practiceTag, String name) {
		if (pvToolkit.updatePractice(practiceTag, name)) {
			update(practiceTag);
		}
	}
	
	public void deletePractice(IXMLTag practiceTag) {
		if (pvToolkit.deletePractice(practiceTag)) {
			pvToolkit.deleteGridRow(getId() + "Grid", practiceTag);
		}
	}
	
	public boolean hasSelfFeedbackSubSteps() {
		if (pvToolkit == null) {
			pvToolkit = (CRunPVToolkit)CDesktopComponents.vView().getComponent("pvToolkit");
		}
		for (IXMLTag practiceStepTag : pvToolkit.getStepTags(CRunPVToolkit.practiceStepType)) {
			CRunPVToolkitCacAndTag runPVToolkitCacAndTag = new CRunPVToolkitCacAndTag(pvToolkit.getCurrentProjectsCaseComponent(), practiceStepTag);
			for (IXMLTag subStepTag : pvToolkit.getSubStepTags(runPVToolkitCacAndTag)) {
				if (subStepTag.getChildValue("substeptype").equals(CRunPVToolkit.practiceSelfFeedbackSubStepType)) {
					return true;
				}
			}
		}
		return false;
	}
	
}
