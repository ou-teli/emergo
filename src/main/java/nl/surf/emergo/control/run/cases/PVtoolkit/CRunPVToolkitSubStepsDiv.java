/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.lang.Strings;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefHtml;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTag;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitNotification;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.view.VView;

public class CRunPVToolkitSubStepsDiv extends CDefDiv {

	private static final long serialVersionUID = 4202033651557206488L;

	public String zulfilepath = ((CRunPVToolkitInitBox)CDesktopComponents.vView().getComponent("PV-toolkit_initBox")).getZulfilepath();
	
	protected CRunPVToolkit pvToolkit;
	
	protected CRunPVToolkitCacAndTag runPVToolkitCacAndTag;
		
	protected List<IXMLTag> subStepTags;
	
	protected IERunGroup _actor;
	protected boolean _reset;
	protected boolean _editable;
	
	public boolean _initialized = false;
	
	protected boolean isPeerGroupMemberStudent;
	
	protected String _classPrefix = "subSteps";
	
	protected final static int subStepOffset = 0;
	protected final static int subStepHeight = 100;

	public void init(IERunGroup actor, boolean reset, boolean editable) {
		_actor = actor;
		
		setClass(_classPrefix);

		pvToolkit = (CRunPVToolkit)CDesktopComponents.vView().getComponent("pvToolkit");
		
		runPVToolkitCacAndTag = new CRunPVToolkitCacAndTag(pvToolkit.getCurrentProjectsCaseComponent(), getStepTagForSubSteps());
		subStepTags = getSubStepTags(runPVToolkitCacAndTag);

		isPeerGroupMemberStudent = pvToolkit.hasStudentRole;

		_reset = reset;
		if (reset) {
			reset();
		}
		_editable = editable;
		
		update();
		
		_initialized = true;
	}
	
	protected List<IXMLTag> getSubStepTags(CRunPVToolkitCacAndTag runPVToolkitCacAndTag) {
		return pvToolkit.getSubStepTags(runPVToolkitCacAndTag);
	}
	
	protected IXMLTag getStepTagForSubSteps() {
		//NOTE step tag to be used for sub steps may be different, e.g., for prepare steps
		return pvToolkit.getCurrentStepTag();
	}
	
	public void reset() {
		//NOTE following components may be visible if prepare step is shown before, so hide them
		for (String componentToNavigateTo: getComponentToNavigateToIds(subStepTags)) {
			if (!componentToNavigateTo.equals("")) {
				Component component = CDesktopComponents.vView().getComponent(componentToNavigateTo);
				if (component != null) {
					component.setVisible(false);
				}
			}
		}
		setVisible(true);
	}
	
	public void update() {
		getChildren().clear();
		
		//NOTE to be sure determine pvToolkit, due to null pointer exception
		pvToolkit = (CRunPVToolkit)CDesktopComponents.vView().getComponent("pvToolkit");

		pvToolkit.setMemoryCaching(true);
		
		//NOTE sub steps might be finished in the mean time
		if (updateSubSteps(subStepTags)) {
			pvToolkit.updateStatusProperties();
			
			runPVToolkitCacAndTag = new CRunPVToolkitCacAndTag(pvToolkit.getCurrentProjectsCaseComponent(), getStepTagForSubSteps());
			subStepTags = getSubStepTags(runPVToolkitCacAndTag);
		}

		//step
		Div div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "StepDiv"}
		);
		
		Div subDiv = new CRunPVToolkitDefDiv(div, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "LearningActivity"}
		);

		IXMLTag learningActivityTag = pvToolkit.getLearningActivityChildTag(pvToolkit.getCurrentStepTag());
		if (learningActivityTag != null) {
			new CRunPVToolkitDefHtml(subDiv, 
					new String[]{"class", "cacAndTag", "tagChildName"}, 
					new Object[]{"font " + _classPrefix + "LearningActivity", new CRunPVToolkitCacAndTag(runPVToolkitCacAndTag.getCaseComponent(), learningActivityTag), "richtext"}
					);
		}
		
		
		boolean studentMayCompleteStep = studentMayCompleteStep();
		
		Label label = new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font " + _classPrefix + "FinishStepTitle", "PV-toolkit.finish.step.header"}
		);
		label.setVisible(studentMayCompleteStep);
		
		boolean isStepFinished = isStepFinished();
		boolean areSubStepsFinished = areAllSubStepsFinished(subStepTags);
		boolean areAllSubStepsDoneForAllUsers = true;
		if (allUsersMustCompleteAllSubSteps()) {
			areAllSubStepsDoneForAllUsers = areSubStepsFinished && areAllSubStepsDoneForAllPeers(subStepTags);
		}
		boolean isAtLeastOneSubStepStarted = isAtLeastOneSubStepStarted(subStepTags);
		boolean specificStepFinished = specificStepFinished();

		boolean isEnabled = !isStepFinished && areSubStepsFinished && areAllSubStepsDoneForAllUsers && isAtLeastOneSubStepStarted && specificStepFinished; 
		String checkboxClass = "font " + _classPrefix + "FinishStepCheckbox";
		if (isEnabled) {
			checkboxClass += "Active";
		}
		else {
			checkboxClass += "Inactive";
		}
		CRunPVToolkitStepFinishedCheckbox checkbox = new CRunPVToolkitStepFinishedCheckbox(div, 
				new String[]{"class", "labelKey", "checked"}, 
				new Object[]{checkboxClass, "PV-toolkit.finish.step.checkbox", isStepFinished}
		);
		checkbox.init(this);
		checkbox.setDisabled(!isEnabled);
		checkbox.setVisible(studentMayCompleteStep);
		
		boolean showMessageWaitingForOthers = !isStepFinished && (!specificStepFinished || !areAllSubStepsDoneForAllUsers);

		if (showMessageWaitingForOthers) {
			String stepType = pvToolkit.getCurrentStepTag().getChildValue("steptype"); 
			if (stepType.equals(CRunPVToolkit.feedbackStepType)) {
				String value = CDesktopComponents.vView().getLabel("PV-toolkit-feedback.done.waitingforothers");

				String deadlineDate = pvToolkit.getDeadlineDateStr(_actor, pvToolkit.getCurrentStepTag());
				String deadlineTime =  pvToolkit.getDeadlineTimeStr(_actor, pvToolkit.getCurrentStepTag());
				if (!deadlineDate.equals("")) {
					if (deadlineTime.equals("")) {
						value += " " + CDesktopComponents.vView().getLabel("PV-toolkit-feedback.done.waitingforothers2").replace("%1", deadlineDate);
					}
					else {
						value += " " + CDesktopComponents.vView().getLabel("PV-toolkit-feedback.done.waitingforothers3").replace("%1", deadlineDate).replace("%2", deadlineTime);
					}
				}
				new CRunPVToolkitDefLabel(div, 
						new String[]{"class", "value"}, 
						new Object[]{"font " + _classPrefix + "FinishStepWaitingForOthersLabel", value}
						);
			}
		}
		
		//sub steps
		//buttons for sub steps
		div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "SubStepsDiv"}
		);
		
		List<String> componentToNavigateToIds = getComponentToNavigateToIds(subStepTags);
		List<String> componentToInitIds = getComponentToInitIds(subStepTags);
		
		int top = subStepOffset;
		int subStepNumber = 1;
		for (IXMLTag subStepTag : subStepTags) {
			String subSteptype = subStepTag.getChildValue("substeptype");
			if (!subSteptype.equals("")) {
				appendSubStepButton(
						div, 
						runPVToolkitCacAndTag.getCaseComponent(), 
						subStepTag, 
						subStepNumber, 
						subSteptype,
						componentToNavigateToIds.get(subStepNumber - 1),
						componentToInitIds.get(subStepNumber - 1),
						top);
				top += subStepHeight;
				subStepNumber++;
			}
		}
		
		pvToolkit.setMemoryCaching(false);
		
	}
	
	public boolean studentMayCompleteStep() {
		return !pvToolkit.getCurrentStepTag().getCurrentStatusAttribute(AppConstants.statusKeyStudentmaycompletestep).equals(AppConstants.statusValueFalse);
	}
	
	public boolean allUsersMustCompleteAllSubSteps() {
		return pvToolkit.hasStudentRole && pvToolkit.getCurrentStepTag().getCurrentStatusAttribute(AppConstants.statusKeyAllusersmustcompleteallsubsteps).equals(AppConstants.statusValueTrue);
	}

	public boolean isStepFinished() {
		return pvToolkit.getCurrentStepTag().getCurrentStatusAttribute(AppConstants.statusKeyFinished).equals(AppConstants.statusValueTrue);
	}
	
	public void setStepFinished(boolean value) {
		CDesktopComponents.sSpring().setRunTagStatus(runPVToolkitCacAndTag.getCaseComponent(), pvToolkit.getCurrentStepTag(), AppConstants.statusKeyFinished, "" + value, true, AppConstants.statusTypeRunGroup, true, false);
		
		if (value) {
			pvToolkit.publishNotification(new Event(CRunPVToolkit.onStepFinished, null, new CRunPVToolkitNotification(_actor, runPVToolkitCacAndTag.getXmlTag(), null)));

			/* START EXPERIMENTAL CODE */
			if (pvToolkit.isUsedInExperiment()) {
				String currentStepType = pvToolkit.getCurrentStepType();
				int currentCycleNumber = pvToolkit.getCurrentCycleNumber();
				int lCurrentStepNumber = pvToolkit.getStepOrderNumber(currentStepType);

				String assessmentType = pvToolkit.getExperimentAssessmentType(currentCycleNumber, lCurrentStepNumber, _actor);
				if (CRunPVToolkit.gExpAssNamesNotInStates.equals(assessmentType)) {
					//NOTE legacy
					assessmentType = "";

					//NOTE show possible assessment for step type 4 or 5
					if (currentStepType.equals(CRunPVToolkit.viewFeedbackStepType)) {
						if (pvToolkit.getCurrentRubricCode().equals("JP")) {
							//NOTE check for not variant R2R4, because this is the default behavior, which accounts for older runs as well
							if ((!pvToolkit.participatesInVariantR2R4 && (currentCycleNumber == 1 || currentCycleNumber == 3)) ||
									(pvToolkit.participatesInVariantR2R4 && currentCycleNumber == 2)) {
								if (pvToolkit.hasPeersWithTeacherRole && (pvToolkit.mayAskTeacherFeedbackCount(_actor) < 0)) {
									//NOTE show feedback acceptance questionnaire including items that refer to teachers
									assessmentType = "TPS";
								}
								else {
									//NOTE show feedback acceptance questionnaire without items that refer to teachers
									//NOTE if student may ask for feedback also don't include items that refer to teachers, because it is not known if teacher has given feedback
									assessmentType = "PS";
								}
							}
							//NOTE check for not variant R2R4, because this is the default behavior, which accounts for older runs as well
							else if (currentCycleNumber == 4 && !pvToolkit.participatesInVariantR2R4) {
								//NOTE show TAM and SUS questionnaires
								if (pvToolkit.mayAskTeacherFeedbackCount(_actor) < 0)
									assessmentType = "N";
								else {
									//NOTE until now, only in U/D experiment student may ask for teacher feedback
									if (pvToolkit.participatesInVariantTFBU(_actor))
										assessmentType = "U_N";
									else
										assessmentType = "D_N";
								}
							}
							else if (currentCycleNumber == 4 && pvToolkit.participatesInVariantR2R4) {
								//NOTE if variant R2R4 then only TPS
								//NOTE show feedback acceptance questionnaire including items that refer to teachers, and TAM and SUS questionnaires
								assessmentType = "TPS_N";
							}
						}
						else if (pvToolkit.getCurrentRubricCode().equals("PP")) {
							if (currentCycleNumber == 1) {
								if (pvToolkit.hasPeersWithTeacherRole) {
									//NOTE show feedback acceptance questionnaire including items that refer to teachers
									assessmentType = "TPS";
								}
								else {
									//NOTE show feedback acceptance questionnaire without items that refer to teachers
									assessmentType = "PS";
								}
							}
							else if (currentCycleNumber == 2) {
								//NOTE show TAM and SUS questionnaires
								assessmentType = "N";
							}
						}
					}
					else if (currentStepType.equals(CRunPVToolkit.defineGoalsStepType)) {
						if (pvToolkit.getCurrentRubricCode().equals("JP")) {
							if (currentCycleNumber == 1 || currentCycleNumber == 3) {
								//NOTE show IMI enjoyment questionnaire
								assessmentType = "N";
							}
						}
						else if (pvToolkit.getCurrentRubricCode().equals("PP")) {
							if (currentCycleNumber == 1) {
								//NOTE show IMI enjoyment questionnaire
								assessmentType = "N";
							}
						}
					}
				}
				if (!Strings.isEmpty(assessmentType)) {
					Events.echoEvent("onShowAssessment", pvToolkit, new String[]{"" + currentCycleNumber, "" + lCurrentStepNumber, assessmentType });
				}

			}
			/* END EXPERIMENTAL CODE */

			boolean hasNextPanel = pvToolkit.showNextStep(pvToolkit.getCurrentStepTag());
			if (!hasNextPanel) {
				pvToolkit.mailNotification(CRunPVToolkit.onCertificateOfParticipation, pvToolkit.getCurrentStepTag(), null);

				VView vView = CDesktopComponents.vView();
				vView.showMessagebox(getRoot(), pvToolkit.getRubricLabelText("PV-toolkit.finish.method.finish.confirmation"), vView.getLabel("PV-toolkit.finish.method.finish"), Messagebox.OK, Messagebox.INFORMATION);

				List<String> errorMessages = new ArrayList<String>();
				CDesktopComponents.sSpring().setCurrentRunTagStatus(
						CRunPVToolkit.gCaseComponentNameStatesExperiment,
						"state",
						"beam_to_PV_tool_outtro",
						AppConstants.statusKeyValue,
						AppConstants.statusValueTrue,
						AppConstants.statusTypeRunGroup,
						true,
						errorMessages);
			}
		}
	}
	
	protected boolean updateSubSteps(List<IXMLTag> subStepTags) {
		//NOTE can be overwritten
		return false;
	}

	protected boolean isSubStepAccessible(IXMLTag subStepTag) {
		return subStepTag.getCurrentStatusAttribute(AppConstants.statusKeyAccessible).equals(AppConstants.statusValueTrue);
	}
	
	protected boolean isSubStepStarted(IXMLTag subStepTag) {
		return subStepTag.getCurrentStatusAttribute(AppConstants.statusKeyStarted).equals(AppConstants.statusValueTrue);
	}
	
	protected boolean isSubStepFinished(IXMLTag subStepTag) {
		return subStepTag.getCurrentStatusAttribute(AppConstants.statusKeyFinished).equals(AppConstants.statusValueTrue);
	}
	
	protected void setSubStepAccessible(IXMLTag subStepTag, boolean value) {
		CDesktopComponents.sSpring().setRunTagStatus(runPVToolkitCacAndTag.getCaseComponent(), subStepTag, AppConstants.statusKeyAccessible, "" + value, true, AppConstants.statusTypeRunGroup, true, false);
	}
	
	protected void setSubStepStarted(IXMLTag subStepTag, boolean value) {
		CDesktopComponents.sSpring().setRunTagStatus(runPVToolkitCacAndTag.getCaseComponent(), subStepTag, AppConstants.statusKeyStarted, "" + value, true, AppConstants.statusTypeRunGroup, true, false);
	}
	
	protected void setSubStepFinished(IXMLTag subStepTag, boolean value) {
		CDesktopComponents.sSpring().setRunTagStatus(runPVToolkitCacAndTag.getCaseComponent(), subStepTag, AppConstants.statusKeyFinished, "" + value, true, AppConstants.statusTypeRunGroup, true, false);

		if (value) {
			pvToolkit.publishNotification(new Event(CRunPVToolkit.onSubStepFinished, null, new CRunPVToolkitNotification(_actor, subStepTag, null)));
		}
	}
	
	protected boolean isAtLeastOneSubStepStarted(List<IXMLTag> subStepTags) {
		if (subStepTags.size() == 0) {
			return true;
		}
		for (IXMLTag subStepTag : subStepTags) {
			if (isSubStepStarted(subStepTag)) {
				return true;
			}
		}
		return false;
	}
	
	protected boolean areAllSubStepsFinished(List<IXMLTag> subStepTags) {
		for (IXMLTag subStepTag : subStepTags) {
			if (!isSubStepFinished(subStepTag)) {
				return false;
			}
		}
		if (allUsersMustCompleteAllSubSteps()) {
			//NOTE notify other peer group members so they might be able to complete a step
			pvToolkit.publishNotification(new Event(CRunPVToolkit.onSubStepsFinished, null, new CRunPVToolkitNotification(_actor, runPVToolkitCacAndTag.getXmlTag(), null)));
		}
		return true;
	}
	
	protected boolean areAllSubStepsDoneForAllPeers(List<IXMLTag> subStepTags) {
		return pvToolkit.areAllSubStepsDoneForAllPeers(_actor, getStepTagForSubSteps(), subStepTags);
	}
	
	protected boolean specificStepFinished() {
		//NOTE may be overwritten
		return true;
	}
	
	protected List<String> getComponentToNavigateToIds(List<IXMLTag> subStepTags) {
		//NOTE is overwritten by sub class
		List<String> componentIds = new ArrayList<String>();
		for (IXMLTag subStepTag : subStepTags) {
			componentIds.add("");
		}
		return componentIds;
	}
	
	protected List<String> getComponentToInitIds(List<IXMLTag> subStepTags) {
		//NOTE is overwritten by sub class
		List<String> componentIds = new ArrayList<String>();
		for (IXMLTag subStepTag : subStepTags) {
			componentIds.add("");
		}
		return componentIds;
	}
	
	protected Button appendSubStepButton(Div parentDiv, IECaseComponent caseComponent, IXMLTag subStepTag, int subStepNumber, String subStepType, String componentToNavigateToId, String componentToInitId, int top) {
		boolean accessible = isSubStepAccessible(subStepTag);
		String buttonClass = "font pvtoolkitButton " + _classPrefix + "Button";
		if (accessible) {
			buttonClass += "Active";
		}
		else {
			buttonClass += "Inactive";
		}
		CRunPVToolkitSubStepNavigationButton button = new CRunPVToolkitSubStepNavigationButton(this, 
				new String[]{"class", "cacAndTag", "tagChildName"}, 
				new Object[]{buttonClass, new CRunPVToolkitCacAndTag(caseComponent, subStepTag), "name"}
		);
		if (!isPeerGroupMemberStudent) {
			//NOTE if role is teacher or peerstudent remove 'peer'
			button.setLabel(pvToolkit.removePeerInString(button.getLabel()));
		}
		parentDiv.appendChild(button);
		button.setDisabled(!accessible);
		Map<String,Object> keyValueMap = new HashMap<String,Object>();
		keyValueMap.put("reset", _reset);
		keyValueMap.put("editable", _editable);
		keyValueMap.put("subStepTag", subStepTag);
		keyValueMap.put("subStepNumber", subStepNumber);
		keyValueMap.put("subStepType", subStepType);
		keyValueMap.put("runPVToolkitSubStepsDiv", this);
		button.setAttribute("keyValueMap", keyValueMap);
		button.init(_actor, getId(), componentToNavigateToId, componentToInitId);

		button.setClass(subStepType + "Button");
		button.setStyle("top:" + top + "px;");
		return button;
	}
	
	protected boolean updateSubStepAccessible(List<IXMLTag> subStepTags, List<Boolean> subStepsDone, int index) {
		IXMLTag subStepTag = subStepTags.get(index);
		boolean previousSubStepDone = false;
		boolean lChanged = false;
		if (index <= 0) {
			previousSubStepDone = true;
		}
		else { 
			previousSubStepDone = subStepsDone.get(index - 1).booleanValue();
		}
		//NOTE current sub step is only accessible if previous sub step is done
		if (previousSubStepDone) {
			if (!isSubStepAccessible(subStepTag)) {
				lChanged = true;
				setSubStepAccessible(subStepTag, true);
			}
		}
		return lChanged;
	}

	protected boolean updateSubStepFinished(List<IXMLTag> subStepTags, List<Boolean> subStepsDone, int index) {
		IXMLTag subStepTag = subStepTags.get(index);
		boolean subStepDone = subStepsDone.get(index).booleanValue();
		boolean lChanged = false;
		if (subStepDone) {
			if (!isSubStepFinished(subStepTag)) {
				lChanged = true;
				setSubStepFinished(subStepTag, true);
			}
		}
		return lChanged;
	}

}
