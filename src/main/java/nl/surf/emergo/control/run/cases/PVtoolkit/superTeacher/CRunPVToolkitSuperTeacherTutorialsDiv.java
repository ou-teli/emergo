/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.superTeacher;

import org.zkoss.zhtml.Iframe;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;

public class CRunPVToolkitSuperTeacherTutorialsDiv extends CRunPVToolkitSuperTeacherDiv {

	private static final long serialVersionUID = -7194271724961106594L;

	public void update() {
		pvToolkit.setMemoryCaching(true);
		
		//NOTE put all elements in a specific div and if it exists remove it.
		//Other elements are defined in the ZUL file and may not be removed 
		Div specificDiv = pvToolkit.getSpecificDiv(this);

		Div div = new CRunPVToolkitDefDiv(specificDiv, 
				new String[]{"class"}, 
				new Object[]{"titleCenter"}
		);
		
		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font titleCenter", "PV-toolkit.mainmenuoption.superTeacher"}
		);
		
		Button btn = new CRunPVToolkitDefButton(specificDiv, 
				new String[]{"class", "cLabelKey"}, 
				new Object[]{"font pvtoolkitButton " + _classPrefix + "BackButton", "PV-toolkit.back"}
		);
		addBackToAuthoringEnvironmentOnClickEventListener(btn, this, CDesktopComponents.vView().getComponent("superTeacherAuthoringEnvironmentDiv"));

		//pvToolkit.showVideoUrl("https://youtu.be/SDUVIQa21Lc?list=TLGGcqo4syIMuJEwOTAzMjAyMg", -1, _idPrefix, _classPrefix);

		Iframe iframe = new Iframe();
		iframe.setParent(specificDiv);
		iframe.setStyle("position:absolute;left:100px;top:40px;width:1060px;height:530px");
		iframe.setClientAttribute("frameborder", "0");
		iframe.setClientAttribute("allow", "accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture");
		iframe.setClientAttribute("allowfullscreen", "true");
		iframe.setSrc("https://www.youtube.com/embed/SDUVIQa21Lc?list=TLGGcqo4syIMuJEwOTAzMjAyMg");
		
		pvToolkit.setMemoryCaching(false);
	}

	protected void addBackToAuthoringEnvironmentOnClickEventListener(Component component, Component fromComponent, Component toComponent) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				if (fromComponent != null) {
					fromComponent.getChildren().clear();
					fromComponent.setVisible(false);
				}
				if (toComponent != null) {
					toComponent.setVisible(true);
				}
			}
		});
	}

}
