/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.IP2;

import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Vbox;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;

public class CRunSatisfactionBoxOptionsDiv extends CDefDiv {

	private static final long serialVersionUID = -8842374665867255960L;

	protected CRunSatisfactionDiv satisfactionDiv = null;

	public void onInit(Event aEvent) {
		satisfactionDiv = (CRunSatisfactionDiv)CDesktopComponents.vView().getComponent("satisfactionDiv");
		if (satisfactionDiv == null) {
			return;
		}

		String typeOfSatisfaction = (String)((Object[])aEvent.getData())[0];
		int satisfactionValue = (int)((Object[])aEvent.getData())[1];
		//
		setClass(typeOfSatisfaction + "valuediv");
		//
		HtmlBasedComponent hbox = (HtmlBasedComponent)getChildren().get(0);
		hbox.setClass(typeOfSatisfaction + "value");
		//
		for (int i=0;i<6;i++) {
			Vbox vbox = new Vbox();
			hbox.appendChild(vbox);
			Image image = (Image)hbox.getChildren().get(0).clone();
			vbox.appendChild(image);
			image.setAttribute("typeOfSatisfaction", typeOfSatisfaction);
			image.setAttribute("number", i);
			if (i > (Math.round(satisfactionValue) - 1)) {
				image.setSrc(satisfactionDiv.relativePath + "bolletje-leeg.png");
			}
			else {
				image.setSrc(satisfactionDiv.relativePath + "bolletje-" + typeOfSatisfaction + ".png");
			}
			image.setWidth("18px");
			image.setVisible(true);
			Label label = new Label("" + i);
			vbox.appendChild(label);
			label.setClass("optionlabel");
		}
		//
		setVisible(true);
	}

}
