/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.Tyconstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.domain.IECaseComponent;

public class CRunSpainLevel1_1ScoreInitBox extends CRunScoreInitBox {

	private static final long serialVersionUID = 3646856636097629671L;

	public void onCreate() {
		//NOTE use echoEvent, otherwise macro parent does not yet exist, if code is used within zscript
	  	Events.echoEvent("onInit", this, null);
	}
	
	public void onInit() {
		Map<String,Object> propertyMap = new HashMap<String,Object>();
		propertyMap.put("casecomponent", ((HtmlMacroComponent)CRunComponent.getMacroParent(this)).getDynamicProperty("a_casecomponent"));
		propertyMap.put("tag", ((HtmlMacroComponent)CRunComponent.getMacroParent(this)).getDynamicProperty("a_tag"));

		caseComponentName = "M_D1_define_groups_app";
		IECaseComponent caseComponent = CDesktopComponents.sSpring().getCaseComponent(CDesktopComponents.sSpring().getCase(), "", caseComponentName);
		List<String[]> answerData = getAnswersData(caseComponent, "text", new String[]{
				"target_group1",
				"target_group2",
				"target_group3",
				"target_group4",
				"target_group5",
				"target_group6",
				"target_group7"
			}); 
		List<String[]> answerDataOther = getAnswersData(caseComponent, "text", new String[]{
				"other_group1",
				"other_group2",
				"other_group3",
				"other_group4",
				"other_group5",
				"other_group6",
				"other_group7",
			});
		for (int i=0;i<answerData.size();i++) {
			answerData.get(i)[2] = answerDataOther.get(i)[0]; 
		}
		propertyMap.put("answersdata", answerData);

		String headerleft = "";
		String headerright = "";
		caseComponent = CDesktopComponents.sSpring().getCaseComponent(CDesktopComponents.sSpring().getCase(), "", "game_navigation");
		if (caseComponent != null) {
			List<IXMLTag> nodeTags = CDesktopComponents.cScript().getRunGroupNodeTags(caseComponent, "piece");
			for (IXMLTag nodeTag : nodeTags) {
				String nodeTagPid = nodeTag.getChildValue("pid");
				if (nodeTagPid.equals("D1_1_score_header_left")) {
					headerleft = CDesktopComponents.sSpring().unescapeXML(nodeTag.getChildValue("name"));
				}
				if (nodeTagPid.equals("D1_1_score_header_right")) {
					headerright = CDesktopComponents.sSpring().unescapeXML(nodeTag.getChildValue("name"));
				}
			}
		}
		propertyMap.put("headerleft", headerleft);
		propertyMap.put("headerright", headerright);

		String zulfilepath = (String)((HtmlMacroComponent)CRunComponent.getMacroParent(this)).getDynamicProperty("a_zulfilepath");
		propertyMap.put("zulfilepath", zulfilepath);
	
		HtmlMacroComponent macro = new HtmlMacroComponent();
		appendChild(macro);
		macro.setDynamicProperty("a_propertyMap", propertyMap);
		macro.setMacroURI(zulfilepath + "run_spain_level1_1_score_view_macro.zul");
	}

	@Override
	public String[] getAnswerData(IECaseComponent caseComponent, String nodeTagName, String matchString) {
		String[] answerdata = new String[3];
		List<IXMLTag> nodeTags = getNodeTags(caseComponent, nodeTagName, matchString);
		for (IXMLTag nodeTag : nodeTags) {
			answerdata[0] = CDesktopComponents.sSpring().unescapeXML(nodeTag.getChildValue("richtext"));
			answerdata[1] = "thumbs-up.png";
			answerdata[2] = "";
		}
		return answerdata;
	}

}
