/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.Treerow;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CTree;
import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.control.run.CRunConversationsHelper;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunConversationsHelperOunl. Helps rendering conversation interaction tree.
 */
public class CRunConversationsHelperOunl extends CRunConversationsHelper {

	/** The last selected element id. */
	protected String lastSelectedElementId = "";

	/** The tree item count. */
	protected int treeitemCount = 0;

	/**
	 * Instantiates a new c run conversations helper ounl.
	 *
	 * @param aTree the ZK conversations tree
	 * @param aShowTagNames the a show tag names, comma separated, only tags with these names are shown
	 * @param aCaseComponent the conversations case component
	 * @param aRunComponent the a run component, the ZK conversations component
	 */
	public CRunConversationsHelperOunl(CTree aTree, String aShowTagNames, IECaseComponent aCaseComponent, CRunComponent aRunComponent) {
		super(aTree, aShowTagNames, aCaseComponent, aRunComponent);
		maxStringLength = 75;
	}


	@Override
	public Treeitem renderTreeitem(Component aParent, Component aInsertBefore,
			IXMLTag aItem) {
		treeitemCount += 1;
		return super.renderTreeitem(aParent, aInsertBefore, aItem);
	}

	@Override
	protected Treecell renderTreecell(Treeitem aTreeitem, Treerow aTreerow,
			IXMLTag aItem, String aKeyValues, boolean aAccIsAuthor) {
		Treecell lTreecell = super.renderTreecell(aTreeitem, aTreerow, aItem, aKeyValues, aAccIsAuthor);
		boolean lEven = false;
		if (lTreecell.getAttribute("style_even") == null) {
			lEven = (treeitemCount % 2 == 0);
			lTreecell.setAttribute("style_even", "" + lEven);
		}
		else {
			lEven = ((String)lTreecell.getAttribute("style_even")).equals("true"); 
		}

		if (lEven) {
			lTreecell.setZclass(lTreecell.getZclass() + "_even");
		}
		else {
			lTreecell.setZclass(lTreecell.getZclass() + "_odd");
		}
		String lImg = "";
		if (lastSelectedElementId.equals("")) {
			lastSelectedElementId = CDesktopComponents.sSpring().getCurrentRunComponentStatus(
				getCaseComponent(), AppConstants.statusKeySelectedTagId, AppConstants.statusTypeRunGroup);
		}
		if (aItem.getAttribute(AppConstants.keyId).equals(lastSelectedElementId)) {
			lImg = CDesktopComponents.sSpring().getStyleImgSrc("icon_selected_question");
		}
		if (lImg.equals("")) {
			//NOTE setImage("") does not clear image within Java. It does within zscript. So set image to empty image
			lImg = CDesktopComponents.sSpring().getStyleImgSrc("icon_empty");
		}
		lTreecell.setImage(lImg);
		return lTreecell;
	}

}
