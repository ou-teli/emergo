/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import java.util.List;

import org.zkoss.zul.Label;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listhead;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefListbox;
import nl.surf.emergo.control.def.CDefListcell;
import nl.surf.emergo.control.def.CDefListhead;
import nl.surf.emergo.control.def.CDefListheader;
import nl.surf.emergo.control.def.CDefListitem;

/**
 * The Class CRunMailSubjectContactsListbox. Used to show all possible subject receiver combinations of a mail
 * within the new mail dialogue.
 */
public class CRunMailSubjectContactsListboxClassic extends CDefListbox {

	private static final long serialVersionUID = 4326961510190214270L;

	/** The run wnd. */
	protected CRunWndClassic runWnd = (CRunWndClassic) CDesktopComponents.vView().getComponent(CControl.runWnd);

	/**
	 * Instantiates a new c run mail subjects combo.
	 * 
	 * @param aId the a id
	 */
	public CRunMailSubjectContactsListboxClassic(String aId) {
		setId(aId);
		setCheckmark(true);
		setSclass(this.className);
//		setRows(5);
	}

	/**
	 * Shows all subject contact combinations.
	 * 
	 * @param aTags the a tags
	 * @param aTagId the a tag id
	 * @param aContactsStrings the a contacts strings
	 */
	public void showItems(List<IXMLTag> aTags, List<String> aContactsStrings, String aTagId) {
		getChildren().clear();
		if (aTags == null)
			return;
		Listhead lListhead = new CDefListhead();
		lListhead.setSclass(this.className + "_listhead");
		appendChild(lListhead);
		Listheader lListheader = new CDefListheader(CDesktopComponents.vView().getLabel("run_new_mail.prompt.subject"));
		lListhead.appendChild(lListheader);
		lListheader.setStyle("color:black;background-color:#EEEEEE;");
		if (CDesktopComponents.sSpring().isTutorRun())
			lListheader = new CDefListheader(CDesktopComponents.vView().getLabel("run_new_mail.prompt.sender"));
		else
			lListheader = new CDefListheader(CDesktopComponents.vView().getLabel("run_new_mail.prompt.contacts"));
		lListhead.appendChild(lListheader);
		lListheader.setStyle("color:black;background-color:#EEEEEE;");
		for (int i=0;i<aTags.size();i++) {
			IXMLTag lTag = aTags.get(i);
			String lTagId = lTag.getAttribute(AppConstants.keyId);
			String lSubject = CDesktopComponents.sSpring().unescapeXML(lTag.getChildValue("title"));
			String lSubjectToShow = lSubject;
			if (lSubject.equals(""))
				lSubjectToShow = "...";
			String lContacts = "";
			if (i < aContactsStrings.size())
				lContacts = aContactsStrings.get(i);
			if (!lContacts.equals("")) {
				Listitem lListitem = new CDefListitem();
				// set value of listitem to item
				lListitem.setValue(lTagId);
				lListitem.setAttribute("subject", lSubject);
				lListitem.setAttribute("contacts", lContacts);
				Listcell lListcell = new CDefListcell();
				Label lLabel = new Label(lSubjectToShow);
				lLabel.setZclass(this.className + "_listcell_label");
				lListcell.appendChild(lLabel);
				lListitem.appendChild(lListcell);
				lListcell = new CDefListcell();
				lLabel = new Label(lContacts);
				lLabel.setZclass(this.className + "_listcell_label");
				lListcell.appendChild(lLabel);
				lListitem.appendChild(lListcell);
				appendChild(lListitem);
				if (lTagId.equals(aTagId))
					setSelectedItem(lListitem);
			}
		}
	}

	/**
	 * On select notify new mail dialogue.
	 */
	@Override
	public void onSelect() {
		Listitem lListitem = getSelectedItem();
		String lTagId = (String) lListitem.getValue();
		String lSubject = (String) lListitem.getAttribute("subject");
		String lContacts = (String) lListitem.getAttribute("contacts");
		CRunNewMailClassic runNewMail = (CRunNewMailClassic) CDesktopComponents.vView().getComponent("runNewMail");
		if (runNewMail != null)
			runNewMail.subjectContactsSelected(lTagId, lSubject, lContacts);
	}
}
