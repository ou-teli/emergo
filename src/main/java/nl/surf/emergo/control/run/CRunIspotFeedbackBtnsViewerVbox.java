/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.Macro;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefVbox;
import nl.surf.emergo.view.VView;

public class CRunIspotFeedbackBtnsViewerVbox extends CDefVbox {

	private static final long serialVersionUID = 8171926685731457425L;

	protected VView vView = CDesktopComponents.vView();

	protected CRunIspot runIspot = null;

	public void onCreate(CreateEvent aEvent) {
		runIspot = (CRunIspot)vView.getComponent("runIspot");
	}

	public void onInit(Event aEvent) {
		Events.sendEvent("onIspotShowButtons", this, null);
		getParent().setVisible(true);
	}

	public void onUpdate(Event aEvent) {
		CRunIspotIntervention dataElement = (CRunIspotIntervention)aEvent.getData();
		List<Object[]> buttonsData = new ArrayList<Object[]>();
		List<String> buttonIds = new ArrayList<String>();
		for (CRunIspotFeedbackText feedbackText : dataElement.getFeedbackTexts()) {
			String buttonId = "ispotFeedbackBtn" + feedbackText.getTagId();
			buttonIds.add(buttonId);
			buttonsData.add(new Object[]{buttonId, "ShowText", feedbackText, feedbackText.getTitle(), "DarkBlue"});
		}
		for (CRunIspotFeedbackFragment feedbackFragment : dataElement.getFeedbackFragments()) {
			String buttonId = "ispotFeedbackBtn" + feedbackFragment.getTagId();
			buttonIds.add(buttonId);
			buttonsData.add(new Object[]{buttonId, "ShowPlayer", feedbackFragment, feedbackFragment.getTitle(), "DarkBlue"});
		}
		setAttribute("buttonIds", buttonIds);
		Macro macro = (Macro)vView.getComponent("ispotFeedbackBtnsMacro");
		if (macro != null) {
			macro.setDynamicProperty("a_buttons_data", buttonsData);
			macro.recreate();
		}
	}

	
	public void onIspotShowText(Event aEvent) {
		Events.sendEvent("onIspotShowText", vView.getComponent("ispotViewers"), ((CRunIspotFeedbackText)aEvent.getData()).getText());
		Events.sendEvent("onIspotHideButton", this, ((CRunIspotFeedbackText)aEvent.getData()).getTagId());
		getParent().setVisible(true);
	}

	public void onIspotShowPlayer(Event aEvent) {
		Events.sendEvent("onIspotShowPlayer", vView.getComponent("ispotViewers"), ((CRunIspotFeedbackFragment)aEvent.getData()).getUrl());
		Events.sendEvent("onIspotHideButton", this, ((CRunIspotFeedbackFragment)aEvent.getData()).getTagId());
		getParent().setVisible(true);
	}

	
	public void onIspotShowButtons(Event aEvent) {
		for (String buttonId : (List<String>)getAttribute("buttonIds")) {
			runIspot.setComponentVisible(vView.getComponent(buttonId), true);
		}
	}

	public void onIspotHideButton(Event aEvent) {
		Events.sendEvent("onIspotShowButtons", this, null);
		runIspot.setComponentVisible(vView.getComponent("ispotFeedbackBtn" + aEvent.getData()), false);
	}

}
