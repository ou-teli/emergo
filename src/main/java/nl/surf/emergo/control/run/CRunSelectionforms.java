/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.STriggeredReference;

/**
 * The Class CRunSelectionforms is used to show selectionforms component within the run view area of the emergo player..
 */
public class CRunSelectionforms extends CRunForms {

	private static final long serialVersionUID = -4326285689303711111L;

	protected String containerChildTagNames = "text";
	
	protected String zulComponent = "selectionformsDiv";

	public final static String runSelectionformsId = "runSelectionforms";

	/**
	 * Instantiates a new c run selectionforms.
	 */
	public CRunSelectionforms() {
		super("runSelectionforms", null);
		conversationsFeedbackIdSuffix = "Selectionforms";
		init();
	}

	/**
	 * Instantiates a new c run selectionforms.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the case component
	 */
	public CRunSelectionforms(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
		conversationsFeedbackIdSuffix = "Selectionforms";
		init();
	}

	/**
	 * Creates title area, content area to show tab structure,
	 * and buttons area with close button.
	 */
	@Override
	protected void createComponents() {
		CRunVbox lVbox = new CRunVbox();

		createTitleArea(lVbox);
		createContentArea(lVbox);
		createButtonsArea(lVbox);

		appendChild(lVbox);
	}

	/**
	 * Creates new content component, the tree.
	 *
	 * @return the component
	 */
	@Override
	protected Component newContentComponent() {
		return newContentComponent("runSelectionformsView");
	}

	/**
	 * Creates buttons area and adds close button.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run hbox
	 */
	@Override
	protected CRunHbox createButtonsArea(Component aParent) {
		CRunHbox lButtonsHbox = super.createButtonsArea(aParent);
		createCloseButton(lButtonsHbox);
		return lButtonsHbox;
	}

	/**
	 * Creates view.
	 */
	public void init() {
		Events.postEvent("onInitZulfile", this, null);
	}

	/**
	 * Gets the run component helper.
	 *
	 * @return the run component helper
	 */
	@Override
	public CRunComponentHelper getRunComponentHelper() {
		return new CRunComponentHelper(null, "form", caseComponent, this);
	}

	/**
	 * Gets form tags.
	 *
	 * @return the form tags
	 */
	public List<IXMLTag> getFormTags() {
		return getPresentChildTags(getDataPlusStatusRootTag().getChild(AppConstants.contentElement), "form");
	}

	/**
	 * Gets piece and refpiece tags.
	 *
	 * @param aParentTag the a parent tag
	 * 
	 * @return the piece and refpiece tags
	 */
	public List<IXMLTag> getPieceAndRefpieceTags(IXMLTag aParentTag) {
		if (aParentTag == null) {
			return new ArrayList<IXMLTag>();
		}
		return aParentTag.getChilds("piece,refpiece");
	}
	
	/**
	 * Gets container tags.
	 *
	 * @param aParentTag the a parent tag
	 * 
	 * @return the container tags
	 */
	public List<IXMLTag> getContainerTags(IXMLTag aParentTag) {
		return getPresentChildTags(aParentTag, "container");
	}
	
	/**
	 * Gets item tags.
	 *
	 * @param aParentTag the a parent tag
	 * 
	 * @return the item tags
	 */
	public List<IXMLTag> getItemTags(IXMLTag aParentTag) {
		return getPresentChildTags(aParentTag, containerChildTagNames);
		
	}
	
	/**
	 * Gets all item tags, that is also tags that are not present.
	 *
	 * @param aParentTag the a parent tag
	 * 
	 * @return the item tags
	 */
	public List<IXMLTag> getAllItemTags(IXMLTag aParentTag) {
		return aParentTag.getChilds(containerChildTagNames);
	}
	
	/**
	 * Select item.
	 *
	 * @param aTag the a tag
	 * @param aSelected the a selected
	 *
	 * @return if successful
	 */
	public boolean selectItem(IXMLTag aTag, boolean aSelected) {
		if (aTag == null) {
			return false;
		}
		setRunTagStatus(getCaseComponent(), aTag, AppConstants.statusKeySelected, "" + aSelected, true);
		return true;
	}

	/**
	 * Get triggered feedback condition tag. aParentTags are possible parents of feedback condition tags.
	 *
	 * @return the feedback condition tag
	 */
	public IXMLTag getTriggeredFeedbackConditionTag() {
		return super.getTriggeredFeedbackConditionTag(getFormTags());
	}

	/**
	 * Show feedback.
	 *
	 * @return the feedback condition tag
	 */
	public IXMLTag showFeedback() {
		IXMLTag lFeedbackCondition = super.showFeedback(getFormTags());
	  	if (lFeedbackCondition != null) {
		  	//NOTE score -1 means 'no relevant score yet' because tool instruction is shown, so don'nt increase number of attempts.
		  	if (!lFeedbackCondition.getChildValue("score").equals("-1")) {
				//increase numberofattempts for all form tags
				List<IXMLTag> lFormTags = getPresentChildTags(getDataPlusStatusRootTag().getChild(AppConstants.contentElement), "form");
				for (IXMLTag lFormTag : lFormTags) {
					int lNumberOfAttempts = 0;
					try {
						lNumberOfAttempts = Integer.parseInt(lFormTag.getCurrentStatusAttribute(AppConstants.statusKeyNumberofattempts));
					} catch (NumberFormatException e) {
						lNumberOfAttempts = 0;
					}
					lNumberOfAttempts++;
					setRunTagStatus(getCaseComponent(), lFormTag, AppConstants.statusKeyNumberofattempts, "" + lNumberOfAttempts, true);
				}
		  	}
	  	}
		return lFeedbackCondition;
	}

	/**
	 * Hide feedback.
	 */
	public void hideFeedback() {
		super.hideFeedback();
	}

	@Override
	public String getFeedbackTitle() {
		return CDesktopComponents.vView().getLabel("run_selectionforms.alert.feedback.title");
	}

	@Override
	public String getDefaultFeedbackText() {
		return CDesktopComponents.vView().getLabel("run_selectionforms.alert.feedback.defaulttext");
	}

	@Override
	public void handleStatusChange(STriggeredReference aTriggeredReference) {
		Events.postEvent("onHandleStatusChange", this, aTriggeredReference);
	}

	@Override
	public void cleanup() {
		// if conversations exists, remove it
		removeFeedbackConversations();
		super.cleanup();
	}


	/**
	 * Methods originally used in ZUL file.
	 */

	public void onInitZulfile(Event aEvent) {
		String componentStyle = "";
		String url = getBackgroundUrl();
		if (!url.equals("")) {
			componentStyle = "background-image:url('" + url + "');";
		}
		
		IXMLTag triggeredFeedbackConditionTag = getTriggeredFeedbackConditionTag();
		if (triggeredFeedbackConditionTag != null) {
			//NOTE only show feedback if it is an instruction, which is indicated by a score of -1
			if (sSpringHelper.getTagChildValue(triggeredFeedbackConditionTag, "score", "-1").equals("-1")) {
				Events.postEvent("onShowFeedback", this, null);
			}
		}

		//NOTE showFeedback and showScore are for all forms!
		showFeedback = false;
		showScore = false;
		dataElements = new ArrayList<Hashtable<String,Object>>();

		List<IXMLTag> formTags = getFormTags();
		for (int i=0;i<formTags.size();i++) {
			IXMLTag formTag = formTags.get(i);
			Hashtable<String,Object> hFormDataElements = new Hashtable<String,Object>();
			hFormDataElements.put("tag", formTag);
			hFormDataElements.put("title", sSpringHelper.getTagChildValue(formTag, "title", ""));
			hFormDataElements.put("hovertext", sSpringHelper.getTagChildValue(formTag, "hovertext", ""));

			//NOTE following list is needed so you can do nested foreach looping! see below.
			List<Hashtable<String,Object>> pieceOrRefpieceDataElementsList = new ArrayList<Hashtable<String,Object>>();
			List<IXMLTag> pieceOrRefpieceTags = getPieceAndRefpieceTags(formTag);
			handlepieceOrRefpieces(pieceOrRefpieceDataElementsList, pieceOrRefpieceTags); 
			hFormDataElements.put("pieces", pieceOrRefpieceDataElementsList);

			if (sSpringHelper.getTagStatusChildAttribute(formTag, "showfeedback", "true").equals("true")) {
				showFeedback = true;
			}
			if (sSpringHelper.getTagStatusChildAttribute(formTag, "showscore", "false").equals("true")) {
				showScore = true;
			}
			boolean isShowIfRightOrWrong = isShowIfRightOrWrong(formTag);

			//NOTE following list is needed so you can do nested foreach looping! see below.
			List<Hashtable<String,Object>> containerDataElementsList = new ArrayList<Hashtable<String,Object>>();
			List<IXMLTag> containerTags = getAllContainerTags(formTag);
			for (int j=0;j<containerTags.size();j++) {
				IXMLTag containerTag = containerTags.get(j);
				Hashtable<String,Object> hContainerDataElements = new Hashtable<String,Object>();
				hContainerDataElements.put("tag", containerTag);
				hContainerDataElements.put("title", sSpringHelper.getTagChildValue(containerTag, "title", ""));
				hContainerDataElements.put("hovertext", sSpringHelper.getTagChildValue(containerTag, "hovertext", ""));
				String childOrientation = sSpringHelper.getTagChildValue(containerTag, "childorientation", "vertical");
				hContainerDataElements.put("childorientation", childOrientation);
				hContainerDataElements.put("styleposition", runWnd.getStylePosition(containerTag));
				hContainerDataElements.put("stylesize", runWnd.getStyleSize(containerTag));
				hContainerDataElements.put("stylewidth", runWnd.getStyleWidth(containerTag));
				List<Hashtable<String,Object>> itemList = getItemList(containerTag, isShowIfRightOrWrong, childOrientation);
				hContainerDataElements.put("items", itemList);
				hContainerDataElements.put("maxitemstoselect", getMaxitemsToSelect(itemList));
				containerDataElementsList.add(hContainerDataElements);
			}
			hFormDataElements.put("containers", containerDataElementsList);

			//NOTE following list is needed so you can do nested foreach looping! see below.
			pieceOrRefpieceDataElementsList = new ArrayList<Hashtable<String,Object>>();
			IXMLTag feedbackConditionTag = getCurrentFeedbackConditionTag(formTag);
			pieceOrRefpieceTags = getPieceAndRefpieceTags(feedbackConditionTag);
			handlepieceOrRefpieces(pieceOrRefpieceDataElementsList, pieceOrRefpieceTags); 
			hFormDataElements.put("feedbackpieces", pieceOrRefpieceDataElementsList);

			dataElements.add(hFormDataElements);
		}

		Map<String,Object> propertyMap = new HashMap<String,Object>();
		propertyMap.put("currentEmergoComponent", this);
		propertyMap.put("dataElements", dataElements);
		propertyMap.put("componentStyle", componentStyle);
		propertyMap.put("feedbackButtonUrl", sSpringHelper.stripEmergoPath(getFeedbackButtonUrl()));
		propertyMap.put("tooltipId", VView.tooltipId);
		((HtmlMacroComponent)getRunContentComponent()).setDynamicProperty("a_propertyMap", propertyMap);
		((HtmlMacroComponent)getRunContentComponent()).setMacroURI(VView.v_run_selectionforms_fr);
		getRunContentComponent().setVisible(true);
	}

	public void onShowFeedback(Event aEvent) {
		onShowFeedback(aEvent, "selectionformItem_", "onFeedbackUpdate");
	}

	public void onHandleStatusChange(Event aEvent) {
		//NOTE to handle pieces and refpieces call super
		super.onHandleStatusChange(aEvent, "selectionformItemDiv_");
	}
		
	protected List<Hashtable<String,Object>> getItemList(IXMLTag parentTag, boolean isShowIfRightOrWrong, String childOrientation) {
		//NOTE following list is needed so you can do nested foreach looping! see below.
		List<Hashtable<String,Object>> itemDataElementsList = new ArrayList<Hashtable<String,Object>>();
		//NOTE get all item tags, also non present ones
		List<IXMLTag> itemTags = getAllItemTags(parentTag);
		for (int k=0;k<itemTags.size();k++) {
			IXMLTag itemTag = itemTags.get(k);
			Hashtable<String,Object> hItemDataElements = new Hashtable<String,Object>();
			hItemDataElements.put("tag", itemTag);
			hItemDataElements.put("content", sSpringHelper.getTagChildValue(itemTag, "richtext", ""));
			hItemDataElements.put("accessible", sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, AppConstants.statusKeyAccessible, "true").equals("true"));
			hItemDataElements.put("selected", sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, AppConstants.statusKeySelected, "true").equals("true"));
			if (isShowIfRightOrWrong) {
				hItemDataElements.put("isrightlyplaced", "" + isRightlySelected(itemTag));
			}
			else {
				hItemDataElements.put("isrightlyplaced", "");
			}
			hItemDataElements.put("present", sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, AppConstants.statusKeyPresent, "true"));
			itemDataElementsList.add(hItemDataElements);
		}
		return itemDataElementsList;
	}

	protected String getMaxitemsToSelect(List<Hashtable<String,Object>> itemList) {
		int maxitemsToSelect = 0;
		for (Hashtable<String,Object> item : itemList) {
			IXMLTag itemTag = (IXMLTag)item.get("tag");
			if (sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, AppConstants.statusKeyCorrect, "true").equals("true")) {
				maxitemsToSelect++;
			}
		}
		return "" + maxitemsToSelect;
	}

	boolean isRightlySelected(IXMLTag itemTag) {
		boolean correct = sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, AppConstants.statusKeyCorrect, "true").equals("true");
		boolean selected = sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, AppConstants.statusKeySelected, "true").equals("true");
		return correct && selected;
	}

}
