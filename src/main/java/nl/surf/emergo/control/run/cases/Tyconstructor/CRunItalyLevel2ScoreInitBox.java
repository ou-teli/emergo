/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.Tyconstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.domain.IECaseComponent;

public class CRunItalyLevel2ScoreInitBox extends CRunScoreInitBox {

	private static final long serialVersionUID = -8457322710625346255L;
	
	public void onCreate() {
		//NOTE use echoEvent, otherwise macro parent does not yet exist, if code is used within zscript
	  	Events.echoEvent("onInit", this, null);
	}
	
	public void onInit() {
		caseComponentName = "M_A2_conversations";
		
		Map<String,Object> propertyMap = new HashMap<String,Object>();
		propertyMap.put("casecomponent", ((HtmlMacroComponent)CRunComponent.getMacroParent(this)).getDynamicProperty("a_casecomponent"));
		propertyMap.put("tag", ((HtmlMacroComponent)CRunComponent.getMacroParent(this)).getDynamicProperty("a_tag"));

		IECaseComponent caseComponent = CDesktopComponents.sSpring().getCaseComponent(CDesktopComponents.sSpring().getCase(), "", caseComponentName);
		propertyMap.put("answersdata", getAnswersData(caseComponent, "button", new String[]{
				"interview_investor_1_(.?)",
				"interview_investor_2_(.?)",
				"interview_investor_3_(.?)",
				"interview_investor_4_(.?)",
				"interview_investor_5_(.?)",
				"interview_investor_6_(.?)",
				"interview_investor_7_(.?)",
				"interview_investor_8_(.?)"
			}));

		String headerleft = "";
		String headerright = "";
		caseComponent = CDesktopComponents.sSpring().getCaseComponent(CDesktopComponents.sSpring().getCase(), "", "game_navigation");
		if (caseComponent != null) {
			List<IXMLTag> nodeTags = CDesktopComponents.cScript().getRunGroupNodeTags(caseComponent, "piece");
			for (IXMLTag nodeTag : nodeTags) {
				String nodeTagPid = nodeTag.getChildValue("pid");
				if (nodeTagPid.equals("A2_score_header_left")) {
					headerleft = CDesktopComponents.sSpring().unescapeXML(nodeTag.getChildValue("name"));
				}
				if (nodeTagPid.equals("A2_score_header_right")) {
					headerright = CDesktopComponents.sSpring().unescapeXML(nodeTag.getChildValue("name"));
				}
			}
		}
		propertyMap.put("headerleft", headerleft);
		propertyMap.put("headerright", headerright);

		String zulfilepath = (String)((HtmlMacroComponent)CRunComponent.getMacroParent(this)).getDynamicProperty("a_zulfilepath");
		propertyMap.put("zulfilepath", zulfilepath);
	
		HtmlMacroComponent macro = new HtmlMacroComponent();
		appendChild(macro);
		macro.setDynamicProperty("a_propertyMap", propertyMap);
		macro.setMacroURI(zulfilepath + "run_italy_level2_score_view_macro.zul");
	}

}
