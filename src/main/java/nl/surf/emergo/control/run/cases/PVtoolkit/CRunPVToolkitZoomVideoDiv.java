/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;

public class CRunPVToolkitZoomVideoDiv extends CRunPVToolkitDefDiv {

	private static final long serialVersionUID = 271185477661370730L;

	public String zulfilepath = ((CRunPVToolkitInitBox)CDesktopComponents.vView().getComponent("PV-toolkit_initBox")).getZulfilepath();
	
	protected String _idPrefix = "";
	protected String _classPrefix = "";
	
	protected int _pos = 0;
	protected final static int _initialPos = 0;
	protected final static int _minPos = 0;
	protected final static int _maxPos = 100;
	
	public CRunPVToolkitZoomVideoDiv(Component parent, String[] keys, Object[] values) {
		super(parent, keys, values);
	}

	public void init(String idPrefix, String classPrefix) {
		_idPrefix = idPrefix;
		_classPrefix = classPrefix;
		_pos = _initialPos;
		
		new CRunPVToolkitDefLabel(this, 
				new String[]{"class", "cLabelKey"}, 
				new Object[]{"font " + _classPrefix + "ZoomHeader", "PV-toolkit.zoom"}
		);
		
		Component image = new CRunPVToolkitDefImage(this, 
				new String[]{"class", "src"}, 
				new Object[]{_classPrefix + "ZoomInImage", zulfilepath + "icon_plus.svg"}
		);
		addZoomInImageOnClickEventListener(image);

		image = new CRunPVToolkitDefImage(this, 
				new String[]{"class", "src"}, 
				new Object[]{_classPrefix + "ZoomOutImage", zulfilepath + "icon_minus.svg"}
		);
		addZoomOutImageOnClickEventListener(image);

		new CRunPVToolkitDefLabel(this, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font " + _classPrefix + "ZoomResetHeader", "PV-toolkit.zoom.reset"}
		);
		
		image = new CRunPVToolkitDefImage(this, 
				new String[]{"class", "src"}, 
				new Object[]{_classPrefix + "ZoomResetImage", zulfilepath + "icon_reset.svg"}
		);
		addZoomResetImageOnClickEventListener(image);

		Events.echoEvent("onZoomTimed", this, null);
	}
	
	protected void addZoomInImageOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				int newPos = _pos + 20;
				if (newPos <= _maxPos) {
					_pos = newPos;
					onZoom();
				}
			}
		});
	}

	protected void addZoomOutImageOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				int newPos = _pos - 20;
				if (newPos >= _minPos) {
					_pos = newPos;
					onZoom();
				}
			}
		});
	}

	protected void addZoomResetImageOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				_pos = _initialPos;
				onZoom();
			}
		});
	}

	public void onZoom() {
		zoom(false);
	}
	
	public void onZoomTimed() {
		zoom(true);
	}
	
	public void zoom(boolean timed) {
		Component videoDiv = CDesktopComponents.vView().getComponent(_idPrefix + "VideoDiv");
		Component video = CDesktopComponents.vView().getComponent(_idPrefix + "VideoVideo");
		if (videoDiv != null && video != null) {
			String javaScriptFunction = "sizeVideo";
			if (timed) {
				javaScriptFunction += "Timed";
			}
			Clients.evalJavaScript(javaScriptFunction + "('" + videoDiv.getUuid() + "','" + video.getUuid() + "',"+ _pos + ");");
		}
	}
	
}
