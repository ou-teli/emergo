/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkmax.zul.Camera;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefBox;
import nl.surf.emergo.utilities.PropsValues;
import nl.surf.emergo.view.VView;

public class CRunWebcamInitBox extends CDefBox {

	private static final long serialVersionUID = 3653913194505182631L;

	// NOTE streamingWebcam is true if webcam image is streamed to server, and then
	// assembled to video
	// streamingWebcam is false if webcam video is assembled client side, and then
	// sent to server
	protected boolean streamingWebcam = false;

	protected VView vView = CDesktopComponents.vView();

	public void onCreate(CreateEvent aEvent) {
		CDesktopComponents.sSpring().initDesktopAttributes(getDesktop());

		setId((String) VView.getParameter("boxId", this, "webcamBoxId"));
		HtmlMacroComponent macro = new HtmlMacroComponent();
		appendChild(macro);
		macro.setDynamicProperty("a_emergoRootUrl", vView.getAbsoluteWebappsRoot());
		macro.setDynamicProperty("a_runComponentId", VView.getParameter("runComponentId", this, "runConversations"));
		macro.setDynamicProperty("a_parentComponentId", getId());
		macro.setDynamicProperty("a_webcamContainerId",
				VView.getParameter("webcamContainerId", this, "webcamContainerId"));
		streamingWebcam = PropsValues.STREAMING_SERVER_STORE;
		if (streamingWebcam) {
			macro.setDynamicProperty("a_rtcWsUrl", vView.getHtml5RecorderUrl());
			macro.setDynamicProperty("a_rtcWsApp", vView.getHtml5RecorderApplication());
			macro.setDynamicProperty("a_movieAutoExtension", PropsValues.STREAMING_RECORDER_AUTOFILEEXTENSION);
			macro.setMacroURI("../run_webcam_streaming_view_fr_macro.zul");
			Clients.evalJavaScript("startRec('" + (String) (VView.getParameter("recordingName", this, "")) + "', '"
					+ (String) (VView.getParameter("seeSelf", this, "true")) + "');");
		} else {
			CDesktopComponents.cControl().setRunSessAttr("iSpotRecordingName",
					(VView.getParameter("recordingName", this, "")));
			macro.setDynamicProperty("a_seeSelf", (VView.getParameter("seeSelf", this, "true")));
			macro.setMacroURI("../run_webcam_local_view_fr_macro.zul");
		}
	}

	public void pauseRecording() {
		if (streamingWebcam) {
			Clients.evalJavaScript("pauseRec()");
		} else {
			Camera camera = (Camera) vView.getComponent("webcamRecordingCameraCamera");
			if (camera != null) {
				camera.pause();
			}
		}
	}

	public void resumeRecording() {
		if (streamingWebcam) {
			Clients.evalJavaScript("resumeRec()");
		} else {
			Camera camera = (Camera) vView.getComponent("webcamRecordingCameraCamera");
			if (camera != null) {
				camera.resume();
			}
		}
	}

	public void stopRecording(boolean aSave) {
		if (streamingWebcam) {
			Clients.evalJavaScript("stopRec(" + aSave + ")");
		} else {
			Camera camera = (Camera) vView.getComponent("webcamRecordingCameraCamera");
			if (camera != null) {
				camera.stop();
			}
		}
	}

}
