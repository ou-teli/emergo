/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.lang.Strings;

import eu.rageproject.asset.manager.IBridge;
import eu.rageproject.asset.manager.IDataStorage;
import eu.rageproject.asset.manager.ILog;
import eu.rageproject.asset.manager.Severity;
import eu.rageproject.assets.hatasset.HATAsset;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.utilities.RageAssetsLogger;

/**
 * {@link eu.rageproject.asset.manager.IBridge} implementation
 */
public class CRunAdaptationsHelper implements IBridge, IDataStorage, ILog {

	/** The case component to be used by the run component. */
	private IECaseComponent caseComponent = null;

	/** The parent run adaptations instance. */
	private CRunAdaptations runAdaptation;

	private static final Logger _log = LogManager.getLogger(CRunAdaptationsHelper.class);
	private static final String settingsRageId = HATAsset.getSettingsStorageId();
	private static final String settingsEmergoId = AppConstants.statusKeyGameSettings;
	private static final String playLogRageId = HATAsset.getLogStorageId();
	private static final String playLogEmergoId = AppConstants.statusKeyGameLogging;

	private static final Map<String, String> statusIdsMap;
	
	static {
		statusIdsMap = new HashMap<String, String>();
		statusIdsMap.put(settingsRageId, settingsEmergoId);
		statusIdsMap.put(playLogRageId, playLogEmergoId);
	}
	
	public  CRunAdaptationsHelper() {
	}

	/**
	 * Sets connection to current case component
	 *
	 * @param aCaseComponent	The case component.
	 */
	public void setCaseComponent(IECaseComponent aCaseComponent) {
		caseComponent = aCaseComponent;
	}

	/**
	 * Sets connection to the run adaptation instance
	 *
	 * @param aRunAdaptation	The run adaptation.
	 */
	public void setRunAdaptation(CRunAdaptations aRunAdaptation) {
		runAdaptation = aRunAdaptation;
	}

	public boolean settingsExist() {
		return exists(settingsRageId);
	}

	@Override
	public boolean exists(String fileId) {
		if (statusIdsMap.containsKey(fileId)) {
		    // NOTE for the time being scenario rating is player dependent, so no need to use run status in stead of run group status
		    String lSavedStatus = CDesktopComponents.sSpring().getCurrentRunComponentStatus(caseComponent, statusIdsMap.get(fileId), AppConstants.statusTypeRunGroup);
		    if (!Strings.isEmpty(lSavedStatus))
		    	return true;
		}
		return false;
	}

	@Override
	public String[] files() {
		return null;
	}

	@Override
	public void save(String fileId, String fileData) {
		if (statusIdsMap.containsKey(fileId)) {
		    // NOTE for the time being scenario rating is player dependent, so no need to use run status in stead of run group status
			runAdaptation.setRunComponentStatus(caseComponent, statusIdsMap.get(fileId), CDesktopComponents.sSpring().escapeXML(fileData), true);
		}
	}

	@Override
	public String load(String fileId) {
	    String lSavedStatus = "";
		if (statusIdsMap.containsKey(fileId)) {
		    // NOTE for the time being scenario rating is player dependent, so no need to use run status in stead of run group status
		    lSavedStatus = CDesktopComponents.sSpring().unescapeXML(CDesktopComponents.sSpring().getCurrentRunComponentStatus(caseComponent, statusIdsMap.get(fileId), AppConstants.statusTypeRunGroup));
		}
		return lSavedStatus;
	}

	@Override
	public boolean delete(String fileId) {
		return false;
	}

	/**
	 * Logs
	 *
	 * @param loglevel	The loglevel.
	 * @param format  	Describes the format to use.
	 * @param args	  	Variable arguments providing the arguments.
	 */
	public void Log(Severity loglevel, String format, Object... args) {
		Log(loglevel,String.format(Locale.ROOT, format, args));
	}

	@Override
	public void Log(Severity severity, String msg) {
		RageAssetsLogger.Log(_log, severity, msg);
	}

	/**
	 * Gets the file name used in the RAGE asset to store settings.
	 * 
	 * @return the settings file name
	 */
	public String getRageSettingsId() {
		return settingsRageId;
	}

	/**
	 * Converts string to double value.
	 * 
	 * @param aDStr the string representing the double value
	 * @param aDefDble the double value to return in case aDStr is not valid
	 *
	 * @return double value
	 */
	public double doubleFromString(String aDStr, double aDefDble) {
		/*
		final String Digits = "(\\p{Digit}+)";
		final String HexDigits = "(\\p{XDigit}+)";
		// an exponent is 'e' or 'E' followed by an optionally
		// signed decimal integer.
		final String Exp = "[eE][+-]?" + Digits;
		final String fpRegex = ("[\\x00-\\x20]*" + // Optional leading "whitespace"
				"[+-]?(" + // Optional sign character
				"NaN|" + // "NaN" string
				"Infinity|" + // "Infinity" string

				// A decimal floating-point string representing a finite positive
				// number without a leading sign has at most five basic pieces:
				// Digits . Digits ExponentPart FloatTypeSuffix
				//
				// Since this method allows integer-only strings as input
				// in addition to strings of floating-point literals, the
				// two sub-patterns below are simplifications of the grammar
				// productions from section 3.10.2 of
				// The Java Language Specification.

				// Digits ._opt Digits_opt ExponentPart_opt FloatTypeSuffix_opt
				"(((" + Digits + "(\\.)?(" + Digits + "?)(" + Exp + ")?)|" +

				// . Digits ExponentPart_opt FloatTypeSuffix_opt
				"(\\.(" + Digits + ")(" + Exp + ")?)|" +

				// Hexadecimal strings
				"((" +
				// 0[xX] HexDigits ._opt BinaryExponent FloatTypeSuffix_opt
				"(0[xX]" + HexDigits + "(\\.)?)|" +

				// 0[xX] HexDigits_opt . HexDigits BinaryExponent FloatTypeSuffix_opt
				"(0[xX]" + HexDigits + "?(\\.)" + HexDigits + ")" +

				")[pP][+-]?" + Digits + "))" + "[fFdD]?))" + "[\\x00-\\x20]*");// Optional trailing "whitespace"
		if (Pattern.matches(fpRegex, aDStr)) {
			// Will not throw NumberFormatException
			return Double.parseDouble(aDStr);
		}
		*/
		try {
			return Double.parseDouble(aDStr);
		} catch (NumberFormatException e) {
	        Log(Severity.Error, "cannot convert string to double: '%s'", aDStr);
		}
		return aDefDble;
	}
	
	/**
	 * Converts string to integer value.
	 * 
	 * @param aIStr the string representing the integer value
	 * @param aDefInt the integer value to return in case aIStr is not valid
	 *
	 * @return int value
	 */
	public int intFromString(String aIStr, int aDefInt) {
		try {
			return Integer.parseInt(aIStr);
		} catch (NumberFormatException e) {
	        Log(Severity.Error, "cannot convert string to integer: '%s'", aIStr);
		}
		return aDefInt;
	}
	
}
