/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Label;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.Treerow;
import org.zkoss.zul.impl.XulElement;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CContentHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CTree;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;

/**
 * The Class CRunCanonResultHelper. Helps rendering canon result pieces.
 */
public class CRunCanonResultHelper extends CRunComponentHelper {

	/**
	 * Instantiates a new c run canon result helper.
	 *
	 * @param aTree the ZK canon result tree
	 * @param aShowTagNames the a show tag names, comma separated, only tags with these names are shown
	 * @param aCaseComponent the canon result case component
	 * @param aRunComponent the a run component, the ZK canon result component
	 */
	public CRunCanonResultHelper(CTree aTree, String aShowTagNames, IECaseComponent aCaseComponent, CRunComponent aRunComponent) {
		super(aTree, aShowTagNames, aCaseComponent, aRunComponent);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTreeHelper#renderTreecell(org.zkoss.zul.Treeitem, org.zkoss.zul.Treerow, nl.surf.emergo.business.IXMLTag, java.lang.String, boolean)
	 */
	@Override
	protected Treecell renderTreecell(Treeitem aTreeitem, Treerow aTreerow, IXMLTag aItem, String aKeyValues, boolean aAccIsAuthor) {
		Treecell lTreecell = getTreerowTreecell(aTreerow, 0);
		String lLabelStr = aKeyValues;
		String lOwnerRgaId = aItem.getAttribute(AppConstants.keyOwner_rgaid);
		if (!lOwnerRgaId.equals(""))
			lLabelStr = lLabelStr + " (" +CDesktopComponents.sSpring().getRunGroupAccount(Integer.parseInt(lOwnerRgaId)).getERunGroup().getName() +")";
		lLabelStr = lLabelStr + " (" + aItem.getAttribute(AppConstants.statusKeyRating) + (")");
		lTreecell.setLabel(lLabelStr);
		String lStatus = "active";
		if (isOpened(aItem))
			lStatus = "opened";
		if (!isAccessible(aItem))
			lStatus = "inactive";
		lTreecell.setZclass(runComponent.getClassName() + "_treecell_"+lStatus);
		if (aItem.getName().equals("map")) {
			String lImg = CDesktopComponents.sSpring().getStyleImgSrc("icon_folder_" + lStatus);
			lTreecell.setImage(lImg);
		}
		if (aItem.getName().equals("piece")) {
			String lMediatype = aItem.getChildAttribute("blob", AppConstants.keyMediatype);
			if (lMediatype.equals("text"))
				lMediatype = "document";
			if (lMediatype.equals(""))
				lMediatype = "other";
			String lImg = CDesktopComponents.sSpring().getStyleImgSrc("icon_" + lMediatype + "_" + lStatus);
			lTreecell.setImage(lImg);
		}
		if (aItem.getName().equals(AppConstants.contentElement)) {
			String lTagName = CContentHelper.getNodeTagLabel(caseComponent.getEComponent().getCode(), aItem.getName());
			Label lLabel = getComponentLabel(lTreecell, "contentelement", lTagName + " ");
			lLabel.setZclass(runComponent.getClassName() + "_treecell_"+lStatus);
		}

		String lToolTip = CDesktopComponents.sSpring().unescapeXML(aItem.getChildValue("motivation"));
		String lBloId = aItem.getChildValue("picture");
		if (lBloId.equals("")) {
			lBloId = aItem.getChildValue("illustration");
		}
		if (((lToolTip != null) && (!lToolTip.equals("")) || (!lBloId.equals("")))) {
			lTreecell.setTooltip("toolTip" + VView.initTooltip);
			lTreecell.setAttribute("tooltip", lToolTip);
			lTreecell.setAttribute("item", aItem);
			lTreecell.setAttribute("bloid", lBloId);
		}

		return lTreecell;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunComponentHelper#setContextMenu(org.zkoss.zk.ui.Component)
	 */
	public void setContextMenu(Component aObject) {
		if (extendable)
			((XulElement) aObject).setContext("runMenuPopup");
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.CContentHelper#editContentItem(org.zkoss.zk.ui.Component, org.zkoss.zk.ui.Component, nl.surf.emergo.business.IXMLTag)
	 */
	public Component editContentItem(Component aParent, Component aTreeitem, IXMLTag aItem) {
		Component lTreeitem = super.editContentItem(aParent, aTreeitem, aItem);
		// Redraw item data by simulating click on it. But only for owner. Other rgas maybe rating it.
		if (aItem.getAttribute(AppConstants.keyOwner_rgaid).equals(""+CDesktopComponents.sSpring().getRunGroupAccount().getRgaId()))
			runComponent.doContentItemAction((Treeitem)lTreeitem);
		return lTreeitem;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunComponentHelper#xmlChildsToContentItems(java.util.List, org.zkoss.zk.ui.Component, org.zkoss.zk.ui.Component)
	 */
	public List<Component> xmlChildsToContentItems(List<IXMLTag> aChildTags, Component aParent, Component aInsertBefore) {
		// put childtags in order of rating
		// render rating in treecell
		// render number in treecell
		if ((aChildTags == null) || (aChildTags.size() == 0))
			return null;
		// canon items don't have children
	    TreeMap<String,IXMLTag> lSort = new TreeMap<String,IXMLTag>();
	    int i = 0;
		for (IXMLTag xmltag : aChildTags) {
			boolean lHidden = ((CDesktopComponents.sSpring().getCurrentTagStatus(xmltag,
						AppConstants.statusKeyPresent))
						.equals(AppConstants.statusValueFalse));
			boolean lDeleted = ((CDesktopComponents.sSpring().getCurrentTagStatus(xmltag,
						AppConstants.statusKeyDeleted))
						.equals(AppConstants.statusValueTrue));
			if (!(lHidden || lDeleted)) {
				// determine rating of item
				int lRating = 0;
				IXMLTag lStatusChild = xmltag.getChild(AppConstants.statusElement);
				if (lStatusChild != null) {
					for (IXMLTag lRgaChild : lStatusChild.getChilds("rga")) {
						List<IXMLTag> lRatingChilds = lRgaChild.getChilds(AppConstants.statusKeyRating);
						if (lRatingChilds.size()>0) {
							// get last rating
							String lRatingStr = ((IXMLTag)lRatingChilds.get(lRatingChilds.size()-1)).getValue();
							if (!lRatingStr.equals(""))
								lRating = lRating + Integer.parseInt(lRatingStr);
						}
					}
				}
				xmltag.setAttribute(AppConstants.statusKeyRating, ""+lRating);
				DecimalFormat lFormatter = new DecimalFormat("000000");
				lSort.put(""+lFormatter.format(10000-lRating)+i,xmltag);
				i++;
			}
		}
		List<IXMLTag> lChildTags = new ArrayList<IXMLTag>();
	    Iterator<Map.Entry<String,IXMLTag>> iterator = lSort.entrySet().iterator();
	    while(iterator.hasNext() )
	    {
	    	Map.Entry<String,IXMLTag> entry = iterator.next();
	      	IXMLTag xmltag = (IXMLTag)entry.getValue();
	      	lChildTags.add(xmltag);
	    }
		return super.xmlChildsToContentItems(lChildTags, aParent, aInsertBefore);
	}
}