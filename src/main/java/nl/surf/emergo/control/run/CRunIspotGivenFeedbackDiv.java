/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.Date;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;

import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.def.CDefHelper;
import nl.surf.emergo.utilities.PropsValues;

public class CRunIspotGivenFeedbackDiv extends CRunIspotInterventionDiv {

	private static final long serialVersionUID = 7593915583160072800L;

	@Override
	public void onCreate(CreateEvent aEvent) {
		super.onCreate(aEvent);
		interventionType = runIspot.interventionTypeGivenFeedback;
	}

	public void onUpdate(Event aEvent) {
		setAttribute("dataElement", aEvent.getData());
		Events.sendEvent("onIspotInit", this, null);
		// default show instruction
		Events.sendEvent("onIspotShowInstruction", this, null);
	}

	public void onIspotInit(Event aEvent) {
		Events.sendEvent("onIspotInitHeader", this, null);

		CRunIspotGivenFeedback dataElement = (CRunIspotGivenFeedback) getAttribute("dataElement");
		((Image) vView.getComponent("ispotGivenFeedbackState"))
				.setSrc(runIspot.stylePath + "ispot-" + dataElement.getGivenFeedbackState() + ".png");
		((Image) vView.getComponent("ispotGivenFeedbackState")).setTooltiptext(
				vView.getLabel("run_ispot.listcell.given_feedback." + dataElement.getGivenFeedbackState()));
		((Label) vView.getComponent("ispotGivenFeedbackTitle"))
				.setValue(dataElement.getReactionRga().getERunGroup().getName() + ": " + dataElement.getSubjectName()
						+ ": " + dataElement.getVignetteName());

		boolean done = dataElement.getGivenFeedbackState().equals("done");
		// NOTE feedback can be given multiple times
		int numberOfResits = -1;
		boolean recordingAvailable = dataElement.getGivenFeedbackUrl() != null
				&& !dataElement.getGivenFeedbackUrl().equals("");

		// NOTE set global variables for webcam
		// NOTE recording never starts automatically for feedbacks
		runIspot.recordingCanBeStarted = true;
		// NOTE set maximum recording duration for feedbacks to 10 minutes,
		// so recordings will not get too long if user forgets to stop recording
		runIspot.recordingMaxDuration = 600;
		runIspot.recordingShowProgress = runIspot.recordingMaxDuration > -1;
		runIspot.recordingTimerCount = 0;

		setAttribute("previous", dataElement.getPrevious());
		setAttribute("next", dataElement.getNext());
		setAttribute("showInstruction", !dataElement.getVignetteInstruction().equals(""));
		setAttribute("showVignette", !dataElement.getVignetteUrl().equals(""));
		setAttribute("showFeedback",
				dataElement.getFeedbackTexts().size() > 0 || dataElement.getFeedbackFragments().size() > 0);
		setAttribute("showReaction", true);
		setAttribute("showRecording", recordingAvailable);
		// NOTE no start recording button if auto start
		setAttribute("startRecording", runIspot.recordingCanBeStarted && !done && !recordingAvailable);
		setAttribute("canInterruptRecording", true);
		setAttribute("stopRecording", true);
		// NOTE redo recording button even if auto start
		setAttribute("redoRecording", !done && recordingAvailable && numberOfResits == -1);
		setAttribute("sendRecording", !done && recordingAvailable);

		runIspot.setComponentVisible(vView.getComponent("ispotPreviousButton" + interventionType),
				getAttribute("previous") != null);
		runIspot.setComponentVisible(vView.getComponent("ispotNextButton" + interventionType),
				getAttribute("next") != null);
		Events.sendEvent("onIspotHideViewers", vView.getComponent("ispotViewers"), null);
		Events.sendEvent("onIspotShowButtons", this, null);
		runIspot.setComponentVisible(vView.getComponent("ispotStartRecordingButton" + interventionType),
				(Boolean) getAttribute("startRecording"));
		runIspot.setComponentVisible(vView.getComponent("ispotPauseRecordingButton" + interventionType), false);
		runIspot.setComponentVisible(vView.getComponent("ispotResumeRecordingButton" + interventionType), false);
		runIspot.setComponentVisible(vView.getComponent("ispotStopRecordingButton" + interventionType), false);
		runIspot.setComponentVisible(vView.getComponent("ispotRedoRecordingButton" + interventionType),
				(Boolean) getAttribute("redoRecording"));
		runIspot.setComponentVisible(vView.getComponent("ispotSendRecordingButton" + interventionType),
				(Boolean) getAttribute("sendRecording"));

		Events.postEvent("onUpdate", vView.getComponent("ispotFeedbackBtnsViewer"), dataElement);
	}

	public void onIspotInitHeader(Event aEvent) {
		CRunIspotGivenFeedback dataElement = (CRunIspotGivenFeedback) getAttribute("dataElement");
		((Image) vView.getComponent("ispotGivenFeedbackState"))
				.setSrc(runIspot.stylePath + "ispot-" + dataElement.getGivenFeedbackState() + ".png");
		((Label) vView.getComponent("ispotGivenFeedbackTitle"))
				.setValue(dataElement.getReactionRga().getERunGroup().getName() + ": " + dataElement.getSubjectName()
						+ ": " + dataElement.getVignetteName());
	}

	public void onIspotPreviousGivenFeedback(Event aEvent) {
		Events.sendEvent("onIspotCancelRecording", this, null);
		Events.postEvent("onPrevious", vView.getComponent("ispotGivenFeedbacksListbox"), null);
	}

	public void onIspotNextGivenFeedback(Event aEvent) {
		Events.sendEvent("onIspotCancelRecording", this, null);
		Events.postEvent("onNext", vView.getComponent("ispotGivenFeedbacksListbox"), null);
	}

	public void onIspotShowFeedback(Event aEvent) {
		runIspot.logMessage("iSpot.onIspotShowFeedback - " + getClass().getName());
		Events.sendEvent("onIspotShowFeedbackButtons", this, null);
		Events.sendEvent("onIspotHideButton", this, vView.getComponent("ispotShowFeedbackButton" + interventionType));
	}

	public void onIspotShowReaction(Event aEvent) {
		runIspot.logMessage("iSpot.onIspotShowReaction - " + getClass().getName());
		Events.sendEvent("onIspotShowPlayer", this,
				((CRunIspotGivenFeedback) getAttribute("dataElement")).getReactionUrl());
		Events.sendEvent("onIspotHideButton", this, vView.getComponent("ispotShowReactionButton" + interventionType));
	}

	public void onIspotShowRecording(Event aEvent) {
		runIspot.logMessage("iSpot.onIspotShowRecording - " + getClass().getName());
		Events.sendEvent("onIspotShowPlayer", this,
				((CRunIspotGivenFeedback) getAttribute("dataElement")).getGivenFeedbackUrl());
		Events.sendEvent("onIspotHideButton", this, vView.getComponent("ispotShowRecordingButton" + interventionType));
	}

	@Override
	protected String getInterventionId() {
		return runIspot.getFeedbackInterventionId();
	}

	public void onIspotSaveRecording(Event aEvent) {
		runIspot.logMessage("iSpot.onIspotSaveRecording - " + getClass().getName());
		CRunIspotGivenFeedback dataElement = (CRunIspotGivenFeedback) getAttribute("dataElement");
		String lRecName = "";
		if (PropsValues.STREAMING_SERVER_STORE) {
			lRecName = runIspot.getAbsoluteRecordingUrl((String) aEvent.getData());
		} else {
			// NOTE if recording is saved on emergo server, use relative url.
			lRecName = (String) aEvent.getData();
		}
		runIspot.logMessage("iSpot - name of feedback recording: " + lRecName);
		dataElement.setGivenFeedbackUrl(lRecName);
		// save feedback blob for dataElement
		runIspot.saveInterventionFeedback(dataElement, (String) aEvent.getData());
		Events.sendEvent("onIspotInit", this, null);
		Clients.evalJavaScript("showWebcamMessage('');");
	}

	public void onIspotSendRecording(Event aEvent) {
		runIspot.logMessage("iSpot.onIspotSendRecording - " + getClass().getName());
		CRunIspotGivenFeedback dataElement = (CRunIspotGivenFeedback) getAttribute("dataElement");
		dataElement.setGivenFeedbackState("done");
		dataElement.setGivenFeedbackDoneDate(CDefHelper.getDateStrAsYMD(new Date()));
		// set finished state of dataElement
		runIspot.setGivenFeedbackStatus(dataElement, AppConstants.statusKeyFinished, AppConstants.statusValueTrue);
		runIspot.setGivenFeedbackStatus(dataElement, "finisheddate", dataElement.getGivenFeedbackDoneDate());
		Events.postEvent("onUpdateState", vView.getComponent("ispotGivenFeedbacksListbox"), dataElement);
		Events.sendEvent("onIspotInit", this, null);
	}

	@Override
	public void onIspotShowButtons(Event aEvent) {
		runIspot.setComponentVisible(vView.getComponent("ispotShowInstructionButton" + interventionType),
				(Boolean) getAttribute("showInstruction"));
		runIspot.setComponentVisible(vView.getComponent("ispotShowVignetteButton" + interventionType),
				(Boolean) getAttribute("showVignette"));
		runIspot.setComponentVisible(vView.getComponent("ispotShowFeedbackButton" + interventionType),
				(Boolean) getAttribute("showFeedback"));
		runIspot.setComponentVisible(vView.getComponent("ispotShowReactionButton" + interventionType),
				(Boolean) getAttribute("showReaction"));
		runIspot.setComponentVisible(vView.getComponent("ispotShowRecordingButton" + interventionType),
				(Boolean) getAttribute("showRecording"));
	}

}
