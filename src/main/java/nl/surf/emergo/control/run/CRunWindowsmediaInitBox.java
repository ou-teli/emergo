/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.CreateEvent;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.utilities.PropsValues;
import nl.surf.emergo.view.VView;

public class CRunWindowsmediaInitBox extends CRunStreamingMediaInitBox {

	private static final long serialVersionUID = 4452034809449169554L;

	protected String defaultUrl = "http://emergo.ou.nl/emergo/streaming/test.wmv";
	protected String uiMode = "none";
	protected String windowlessVideo = "true";
	protected String showcontrols = "0";

	public void onCreate(CreateEvent aEvent) {
		init();
		
		CDesktopComponents.sSpring().initDesktopAttributes(getDesktop());

		String wmed_width = (String)VView.getParameter("video_size_width", this, (String)getDesktop().getAttribute("runWndWidth"));
		String wmed_height = (String)VView.getParameter("video_size_height", this, (String)getDesktop().getAttribute("runWndHeight"));

		HtmlMacroComponent macro = new HtmlMacroComponent();
		appendChild(macro);
		macro.setDynamicProperty("a_wmed_width", wmed_width);
		macro.setDynamicProperty("a_wmed_height", wmed_height);
		// NOTE url may contain back slashes. Replace by slashes
		url = url.replaceAll(PropsValues.STREAMING_PLAYER_PATH_SEPARATOR, "/");
		macro.setDynamicProperty("a_url", url);
		macro.setDynamicProperty("a_uiMode", uiMode);
		macro.setDynamicProperty("a_windowlessVideo", windowlessVideo);
		macro.setDynamicProperty("a_showcontrols", showcontrols);
		macro.setMacroURI("../run_website_view_fr_macro.zul");
	}

}
