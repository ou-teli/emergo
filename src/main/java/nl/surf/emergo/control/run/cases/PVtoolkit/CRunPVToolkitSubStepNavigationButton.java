/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit;

import java.util.Map;

import org.zkoss.zk.ui.Component;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IERunGroup;

public class CRunPVToolkitSubStepNavigationButton extends CRunPVToolkitNavigationButton {

	private static final long serialVersionUID = 4509351715403384555L;

	protected IERunGroup _actor;
	protected String _componentToInitId = "";

	public CRunPVToolkitSubStepNavigationButton(Component parent, String[] keys, Object[] values) {
		super(parent, keys, values);
	}
	
	public void init(IERunGroup actor, String componentFromId, String componentToId) {
		_actor = actor;
		super.init(componentFromId, componentToId);
	}

	public void init(IERunGroup actor, String componentFromId, String componentToId, String componentToInitId) {
		_actor = actor;
		super.init(componentFromId, componentToId);
		_componentToInitId = componentToInitId;
	}

	public void onClick() {
		super.onClick();
		CRunPVToolkitSubStepDiv componentToInit = (CRunPVToolkitSubStepDiv)CDesktopComponents.vView().getComponent(_componentToInitId);
		if (componentToInit != null) {
			boolean reset = false;
			boolean editable = false;
			String subStepType = "";
			if (getAttribute("keyValueMap") != null) {
				Map<String,Object> attrKeyValueMap = (Map<String,Object>)getAttribute("keyValueMap");
				if (attrKeyValueMap.containsKey("reset")) {
					reset = (boolean)attrKeyValueMap.get("reset");
				}
				if (attrKeyValueMap.containsKey("editable")) {
					editable = (boolean)attrKeyValueMap.get("editable");
				}
				if (attrKeyValueMap.containsKey("subStepType")) {
					subStepType = (String)attrKeyValueMap.get("subStepType");
				}
				if (attrKeyValueMap.containsKey("runPVToolkitSubStepsDiv") && attrKeyValueMap.containsKey("subStepTag")) {
					CRunPVToolkitSubStepsDiv runPVToolkitSubStepsDiv = (CRunPVToolkitSubStepsDiv)attrKeyValueMap.get("runPVToolkitSubStepsDiv");
					IXMLTag subStepTag = (IXMLTag)attrKeyValueMap.get("subStepTag");
					runPVToolkitSubStepsDiv.setSubStepStarted(subStepTag, true);
					runPVToolkitSubStepsDiv.setAttribute("divTitle", getLabel());
				}
			}
			componentToInit.init(_actor, reset, editable, subStepType);
		}
	}

}
