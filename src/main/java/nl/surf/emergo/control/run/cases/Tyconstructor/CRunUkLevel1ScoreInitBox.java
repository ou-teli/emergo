/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.Tyconstructor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.domain.IECaseComponent;

public class CRunUkLevel1ScoreInitBox extends CRunScoreInitBox {

	private static final long serialVersionUID = 4443686794429714556L;

	public void onCreate() {
		//NOTE use echoEvent, otherwise macro parent does not yet exist, if code is used within zscript
	  	Events.echoEvent("onInit", this, null);
	}
	
	public void onInit() {
		caseComponentName = "M_C1_planning_tool_app";
		
		Map<String,Object> propertyMap = new HashMap<String,Object>();
		propertyMap.put("casecomponent", ((HtmlMacroComponent)CRunComponent.getMacroParent(this)).getDynamicProperty("a_casecomponent"));
		propertyMap.put("tag", ((HtmlMacroComponent)CRunComponent.getMacroParent(this)).getDynamicProperty("a_tag"));

		IECaseComponent caseComponent = CDesktopComponents.sSpring().getCaseComponent(CDesktopComponents.sSpring().getCase(), "", caseComponentName);
		List<String[]> answerData = getAnswersData(caseComponent, "selector", new String[]{
				"selector1",
				"selector2",
				"selector3",
				"selector4",
				"selector5",
				"selector6",
				"selector7",
				"selector8",
				"selector9",
				"selector10"
			}); 
		List<String[]> answerDataSection = getAnswersData(caseComponent, "section", new String[]{
				"preliminary_budget"
			});
		answerData.add(5, answerDataSection.get(0));
		propertyMap.put("answersdata", answerData);

		String zulfilepath = (String)((HtmlMacroComponent)CRunComponent.getMacroParent(this)).getDynamicProperty("a_zulfilepath");
		propertyMap.put("zulfilepath", zulfilepath);
	
		HtmlMacroComponent macro = new HtmlMacroComponent();
		appendChild(macro);
		macro.setDynamicProperty("a_propertyMap", propertyMap);
		macro.setMacroURI(zulfilepath + "run_uk_level1_score_view_macro.zul");
	}

	@Override
	public String[] getAnswerData(IECaseComponent caseComponent, String nodeTagName, String matchString) {
		String[] answerdata = new String[]{"", ""};
		List<IXMLTag> nodeTags = getNodeTags(caseComponent, nodeTagName, matchString);
		if (nodeTagName.equals("selector")) {
			for (IXMLTag nodeTag : nodeTags) {
				String options = CDesktopComponents.sSpring().unescapeXML(nodeTag.getChildValue("options"));
				String[] optionsArr = options.split("\n");
				String correctOptions = nodeTag.getChildValue("correctoptions");
				int correctOption = 0;
				try {
					correctOption = Integer.parseInt(correctOptions);
				} catch (NumberFormatException e) {
				}
				if (correctOption > 0 && correctOption <= optionsArr.length) {
					answerdata[0] = optionsArr[correctOption - 1];
				}
				if (nodeTag.getCurrentStatusAttribute(AppConstants.statusKeyCorrect).equals(AppConstants.statusValueTrue) ) {
					answerdata[1] = "thumbs-up-small.png";
				}
				else {
					answerdata[1] = "thumbs-down-small.png";
				}
			}
		}
		if (nodeTagName.equals("section")) {
			for (IXMLTag nodeTag : nodeTags) {
				answerdata[0] = CDesktopComponents.sSpring().unescapeXML(nodeTag.getChildValue("title"));
				answerdata[1] = "thumbs-up-small.png";
			}
		}
		return answerdata;
	}

	@Override
	public List<String[]> getAnswersData(IECaseComponent caseComponent, String nodeTagName, String[] matchStrings) {
		List<String[]> answersdata = new ArrayList<String[]>();
		for (int i=0;i<matchStrings.length;i++) {
			String[] answerdata = getAnswerData(caseComponent, nodeTagName, matchStrings[i]);
			answersdata.add(answerdata);
		}
		return answersdata;
	}

}
