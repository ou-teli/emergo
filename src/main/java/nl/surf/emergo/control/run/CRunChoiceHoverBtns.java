/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Div;
import org.zkoss.zul.Image;

import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.def.CDefImage;
/**
 * The Class CRunChoiceHoverBtns. Ancestor of alle sets of hover buttons used
 * within the run choice area.
 */
public class CRunChoiceHoverBtns extends CRunArea {

	private static final long serialVersionUID = -5869530972578569441L;

	/** The ZK parent. */
	protected Component parent = null;

	/** The button hbox containing the hover buttons. */
	protected CRunHbox buttonHbox = null;

	protected List<Component> buttonHboxList = new ArrayList<Component>();

	/** The item count, the number of hover buttons. */
	protected int itemCount = 0;

	/** The first update, used by inherited classes. */
	protected boolean firstUpdate = true;

	/** Use arrows. */
	protected boolean useArrows = true;

	/**
	 * Instantiates a new c run choice hover btns.
	 *
	 * @param aParent the ZK parent
	 * @param aId the a id
	 */
	public CRunChoiceHoverBtns(Component aParent, String aId) {
		super();
		parent = aParent;
		setId(aId);
		init();
	}

	/**
	 * Instantiates a new c run choice hover btns.
	 *
	 * @param aParent the ZK parent
	 * @param aId the a id
	 * @param aUseArrows the a use arrows
	 */
	public CRunChoiceHoverBtns(Component aParent, String aId, boolean aUseArrows) {
		super();
		parent = aParent;
		setId(aId);
		useArrows = aUseArrows;
		init();
	}

	protected void init() {
		String lType = "";
		if (runWnd != null)
			lType = runWnd.getPlayerStatusString("ChoiceAreaLayoutStatus", null);
		setZclass(getClassName() + lType);
		buttonHbox = newButtonHbox();
		if (useArrows)
			createArrows();
		update();
	}

	protected CRunHbox newButtonHbox() {
		CRunHbox hbox = new CRunHbox();
		appendChild(hbox);
		hbox.setZclass(getClassName() + "_hbox");
		buttonHboxList.add(hbox);
		return hbox;
	}

	/**
	 * Creates arrows to scroll the hover buttons.
	 */
	protected void createArrows() {
		String lLeftArrowId = getId() + "LeftArrow";
		String lRightArrowId = getId() + "RightArrow";

		Div lLeftArrow = new CDefDiv();
		appendChild(lLeftArrow);
		lLeftArrow.setId(lLeftArrowId);
		lLeftArrow.setZclass(getClassName() + "_left_arrow");
		lLeftArrow.setTooltiptext(CDesktopComponents.vView().getLabel("run_empack.left_arrow.tooltip"));
		lLeftArrow.setVisible(true);
		String lSrc = "";
		lSrc = CDesktopComponents.sSpring().getStyleImgSrc("left_arrow");
		Image lLeftImage = new CDefImage(lSrc);
		lLeftArrow.appendChild(lLeftImage);

		Div lRightArrow = new CDefDiv();
		appendChild(lRightArrow);
		lRightArrow.setId(lRightArrowId);
		lRightArrow.setZclass(getClassName() + "_right_arrow");
		lRightArrow.setTooltiptext(CDesktopComponents.vView().getLabel("run_empack.right_arrow.tooltip"));
		lRightArrow.setVisible(false);
		lSrc = "";
		lSrc = CDesktopComponents.sSpring().getStyleImgSrc("right_arrow");
		Image lRightImage = new CDefImage(lSrc);
		lRightArrow.appendChild(lRightImage);

		String lLeftAction = "move('" + getUuid() + "', '" + buttonHbox.getUuid() + "', '" + lLeftArrow.getUuid() + "', '" + lRightArrow.getUuid() + "', true);";
		lLeftImage.setWidgetListener("onMouseOver", lLeftAction);
		lLeftImage.setWidgetListener("onMouseOut", "stopscroll();");

		String lRightAction = "move('" + getUuid() + "', '" + buttonHbox.getUuid() + "', '" + lLeftArrow.getUuid() + "', '" + lRightArrow.getUuid() + "', false);";
		lRightImage.setWidgetListener("onMouseOver", lRightAction);
		lRightImage.setWidgetListener("onMouseOut", "stopscroll();");
	}

	/**
	 * Updates hover buttons.
	 */
	public void update() {
	}

	/**
	 * Sets the number of hover buttons.
	 *
	 * @param aItemCount the new item count
	 */
	public void setItemCount(int aItemCount) {
		itemCount = aItemCount;
	}

	/**
	 * Shows scroll arrows depending on aShow and number of hover buttons.
	 *
	 * @param aShow the a show
	 */
	public void showArrows(boolean aShow) {
		if (!useArrows)
			return;
		String lLeftId = getId() + "LeftArrow";
		Component lLeftArrow = (Component)CDesktopComponents.vView().getComponent(lLeftId);
		if (lLeftArrow != null) {
			Image lImage = (Image)lLeftArrow.getChildren().get(0);
			lImage.setVisible(aShow);
			lLeftArrow.setVisible(true);
		}
		String lRightId = getId() + "RightArrow";
		Component lRightArrow = (Component)CDesktopComponents.vView().getComponent(lRightId);
		if (lRightArrow != null) {
			int lNumber = 6;
			String lType = runWnd.getPlayerStatusString("ChoiceAreaLayoutStatus", null);
			if (runWnd != null && (lType.equals("Left") || lType.equals("Right")))
				lNumber = 8;
			if (runWnd != null && lType.equals("Max"))
				lNumber = 10;
			lRightArrow.setVisible((aShow) && (itemCount > lNumber));
			boolean lShow = ((aShow) && (itemCount > lNumber));
			lRightArrow.setVisible(lShow);
			Image lImage = (Image)lRightArrow.getChildren().get(0);
			lImage.setVisible(aShow);
		}
	}

	/**
	 * Creates new hover btn.
	 *
	 * @param aId the a id
	 * @param aStatus the a status the button should get
	 * @param aAction the a action to be send when clicked
	 * @param aActionStatus the a action status of the action
	 * @param aPosition the a position, where on the screen the button appears for layout
	 * @param aImgPrefix the a img prefix of the hover images
	 * @param aLabel the a label to be shown above the button
	 *
	 * @return the c run hover btn
	 */
	protected CRunHoverBtn newHoverBtn(String aId, String aStatus, String aAction,
			Object aActionStatus, String aImgPrefix, String aLabel) {
		CRunHoverBtn cHoverBtn = createHoverBtn(aId, aStatus, aAction, aActionStatus, aImgPrefix, aLabel);
		cHoverBtn.registerObserver(CControl.runWnd);
		cHoverBtn.setVisible(!aStatus.equals("empty"));
		if (runWnd != null)
			cHoverBtn.setWidgetListener("onClick", runWnd.getMediaplayerStopAction());
		return cHoverBtn;
	}

	/**
	 * Creates new hover btn.
	 *
	 * @param aId the a id
	 * @param aStatus the a status the button should get
	 * @param aAction the a action to be send when clicked
	 * @param aActionStatus the a action status of the action
	 * @param aPosition the a position, where on the screen the button appears for layout
	 * @param aImgPrefix the a img prefix of the hover images
	 * @param aLabel the a label to be shown above the button
	 *
	 * @return the c run hover btn
	 */
	protected CRunHoverBtn createHoverBtn(String aId, String aStatus, String aAction,
			Object aActionStatus, String aImgPrefix, String aLabel) {
		return new CRunHoverBtn(aId, aStatus, aAction, aActionStatus, aImgPrefix, aLabel, "");
	}

	/**
	 * Creates new hover btn.
	 *
	 * @param aParams the a params
	 *
	 * @return the c run hover btn
	 */
	protected CRunHoverBtn newHoverBtn(Map<String,Object> aParams) {
		CRunHoverBtn cHoverBtn = createHoverBtn(aParams);
		cHoverBtn.registerObserver(CControl.runWnd);
		cHoverBtn.setVisible(!((String)aParams.get("status")).equals("empty"));
		if (runWnd != null)
			cHoverBtn.setWidgetListener("onClick", runWnd.getMediaplayerStopAction());
		return cHoverBtn;
	}

	/**
	 * Creates new hover btn.
	 *
	 * @param aParams the a params
	 *
	 * @return the c run hover btn
	 */
	protected CRunHoverBtn createHoverBtn(Map<String,Object> aParams) {
		return new CRunHoverBtn(aParams);
	}

	/**
	 * Gets the btn status as string.
	 *
	 * @param aVisible the a visible
	 * @param aEnabled the a enabled
	 * @param aSelected the a selected
	 *
	 * @return the btn status
	 */
	public String getBtnStatus(boolean aVisible, boolean aEnabled,
			boolean aSelected) {
		String lStatus = "active";
		if (!aVisible)
			lStatus = "empty";
		else {
			if (!aEnabled)
				lStatus = "inactive";
			else {
				if (aSelected)
					lStatus = "selected";
			}
		}
		return lStatus;
	}

	/**
	 * Selects button given by selectId.
	 *
	 * @param selectId the select id
	 */
	public void selectBtn(String selectId) {
		for (Component lButtonHbox : buttonHboxList) {
			for (Component lComp : lButtonHbox.getChildren()) {
				if (lComp instanceof CRunHoverBtn) {
					CRunHoverBtn lBtn = (CRunHoverBtn)lComp;
					if ((lBtn.getId().equals(selectId)) && (!lBtn.isStatus("selected")))
						lBtn.setStatus("selected");
				}
			}
		}
	}

	/**
	 * Deselects the previously selected button if it's id is unequal to notDeselectId
	 *
	 * @param notDeselectId the not deselect id
	 */
	public void deselectBtn(String notDeselectId) {
		for (Component lButtonHbox : buttonHboxList) {
			for (Component lComp : lButtonHbox.getChildren()) {
				if (lComp instanceof CRunHoverBtn) {
					CRunHoverBtn lBtn = (CRunHoverBtn)lComp;
					if ((lBtn.isStatus("selected")) && (!lBtn.getId().equals(notDeselectId)))
						lBtn.deselect();
				}
			}
		}
	}

	/**
	 * Updates existing btn given by aBtnName with status given by aStatusKey and
	 * aStatusValue.
	 *
	 * @param aBtnName the a btn name
	 * @param aStatusKey the a status key
	 * @param aStatusValue the a status value
	 */
	public void updateBtn(String aBtnName, String aStatusKey, String aStatusValue) {
		CRunHoverBtn lBtn = (CRunHoverBtn) CDesktopComponents.vView().getComponent(aBtnName);
		if (lBtn == null)
			return;
		boolean lBool = (aStatusValue.equals(AppConstants.statusValueTrue));
		if (aStatusKey.equals(AppConstants.statusKeyPresent)) {
			if ((!lBtn.isVisible()) && (lBool))
				itemCount = itemCount + 1;
			if ((lBtn.isVisible()) && (!lBool))
				itemCount = itemCount - 1;
			lBtn.setBtnVisible(lBool);
			if (lBool) {
				boolean lAccessible = true;
				String lAccStr = (String)lBtn.getAttribute("accessible");
				if ((lAccStr != null) && (!lAccStr.equals("")) && (!lAccStr.equals("true")))
					lAccessible = false;
				lBtn.setBtnEnabled(lAccessible);
			}
			showArrows(true);
		}
		if (aStatusKey.equals(AppConstants.statusKeyAccessible)) {
			lBtn.setBtnEnabled(lBool);
			lBtn.setAttribute("accessible", "" + lBool);
		}
		if (aStatusKey.equals(AppConstants.statusKeySelected))
			lBtn.setBtnSelected(lBool);
	}
}
