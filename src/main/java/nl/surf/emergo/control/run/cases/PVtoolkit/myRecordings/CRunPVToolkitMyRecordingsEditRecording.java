/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.myRecordings;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Image;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitInitBox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefTextbox;
import nl.surf.emergo.view.VView;

public class CRunPVToolkitMyRecordingsEditRecording extends CDefDiv {

	private static final long serialVersionUID = -8253928126726848506L;

	protected VView vView = CDesktopComponents.vView();
	
	protected String zulfilepath = ((CRunPVToolkitInitBox)vView.getComponent("PV-toolkit_initBox")).getZulfilepath();
	
	protected CRunPVToolkit pvToolkit;
	
	protected CRunPVToolkitMyRecordingsRecordingsDiv myRecordingsRecordingsDiv;

	protected IXMLTag _practiceTag;
	
	protected String _idPrefix = "myRecordingsEdit";
	protected String _classPrefix = "practiceEdit";
	
	public void init(IXMLTag practiceTag) {
		_practiceTag = practiceTag;
		
		pvToolkit = (CRunPVToolkit)vView.getComponent("pvToolkit");
		
		myRecordingsRecordingsDiv = (CRunPVToolkitMyRecordingsRecordingsDiv)vView.getComponent("myRecordingsRecordingsDiv");
		
		setClass("popupDiv");

		update();
		
		setVisible(true);
	}
	
	public void update() {
		
		getChildren().clear();
		
		pvToolkit.setMemoryCaching(true);
		
		new CRunPVToolkitDefImage(this, 
				new String[]{"class"}, 
				new Object[]{"popupBackground"}
		);

		new CRunPVToolkitDefImage(this, 
				new String[]{"class", "src"}, 
				new Object[]{_classPrefix + "Background", zulfilepath + "edit-recording-background.svg"}
		);

		Div div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "Title"}
		);
		
		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font " + _classPrefix + "Title", "PV-toolkit-practice.header.edit.recording"}
		);
		
		Image closeImage = new CRunPVToolkitDefImage(this, 
				new String[]{"class", "src"}, 
				new Object[]{_classPrefix + "CloseButton", zulfilepath + "close.svg"}
		);
		addCloseButtonOnClickEventListener(closeImage);

		div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "Div"}
		);
		
		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font " + _classPrefix + "GiveNameTitle", "PV-toolkit-practice.column.label.name"}
		);

		String name = "";
		if (_practiceTag != null) {
			name = pvToolkit.getStatusChildTagAttribute(_practiceTag, "name");
		}
		new CRunPVToolkitDefTextbox(div, 
				new String[]{"id", "class", "text"}, 
				new Object[]{_idPrefix + "GiveNameTextbox", "font " + _classPrefix + "GiveNameTextbox", name}
		);

		Button btn = new CRunPVToolkitDefButton(div, 
				new String[]{"id", "class", "cLabelKey"}, 
				new Object[]{_idPrefix + "SaveButton", "font pvtoolkitButton " + _classPrefix + "SaveButton", "PV-toolkit.save"}
		);
		addSaveButtonOnClickEventListener(btn, this);

		pvToolkit.setMemoryCaching(false);
	}

	protected void addCloseButtonOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				Component componentToHide = vView.getComponent(_idPrefix + "Recording");
				if (componentToHide != null) {
					componentToHide.setVisible(false);
				}
			}
		});
	}

	protected void addSaveButtonOnClickEventListener(Component component, Component notifyComponent) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				String name = ((Textbox)vView.getComponent(_idPrefix + "GiveNameTextbox")).getValue().trim();
				if (name.equals("")) {
					//NOTE empty name, show warning
					vView.showMessagebox(getRoot(), vView.getLabel("PV-toolkit-practice.edit.recording.save.emptyname.warning"), vView.getLabel("PV-toolkit-practice.edit.recording.save"), Messagebox.OK, Messagebox.EXCLAMATION);
					return;
				}
				myRecordingsRecordingsDiv.updatePractice(_practiceTag, name);
				
				setVisible(false);
			}
		});
	}

}
