/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.feedback;

import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Vbox;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitCloseImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitInitBox;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitVideoExamplesDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefHbox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefHtml;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefVbox;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTag;
import nl.surf.emergo.control.run.cases.PVtoolkit.util.CRunPVToolkitCustomAttributeHelper;

public class CRunPVToolkitFeedbackScoreLevelsDiv extends CDefDiv {

	private static final long serialVersionUID = -7371003065811862353L;

	public String zulfilepath = ((CRunPVToolkitInitBox)CDesktopComponents.vView().getComponent("PV-toolkit_initBox")).getZulfilepath();
		
	protected int _numberOfPerformanceLevels = 0;
	protected int _numberOfPerformanceLevelsToShow  = 0;
	protected int[] _performanceLevels;
	
	protected CRunPVToolkitCacAndTag _runPVToolkitCacAndTag;
	protected int	_level;
	protected boolean _hasTipsTops;
	protected String _feedbackRubricId;
	protected CRunPVToolkitFeedbackRubricDiv _feedbackRubric;
	
	protected boolean _accessible = true;

	protected CRunPVToolkit pvToolkit;
	
	protected String _idPrefix = "feedback";
	protected String _classPrefix = "feedback";
	
	protected final static int specificDivWidth = 200;
	protected final static int scoreLevelDivWidth = 200;
	protected final static int scoreLevelVboxWidth = 180;
	protected final static int scoreLevelStarsDivWidth = 174;
	protected final static int scoreLevelDescriptionDivWidth = 176;

	public void onCreate(CreateEvent aEvent) {
		_idPrefix = CRunPVToolkitCustomAttributeHelper.handleCustomAttribute(this, "_idPrefix", _idPrefix);
	}
	
	public void init(CRunPVToolkitCacAndTag runPVToolkitCacAndTag, int level, boolean hasTipsTops, String feedbackRubricId) {
		_runPVToolkitCacAndTag = runPVToolkitCacAndTag;
		_level = level;
		_hasTipsTops = hasTipsTops;
		_feedbackRubricId = feedbackRubricId;
		_feedbackRubric = (CRunPVToolkitFeedbackRubricDiv)CDesktopComponents.vView().getComponent(_feedbackRubricId);
		
		IXMLTag tag = _runPVToolkitCacAndTag.getXmlTag();
		//NOTE if sub skill tag then possibly skill cluster tag should be accessible as well. Skill tag always is accessible.
		_accessible = !tag.getCurrentStatusAttribute(AppConstants.statusKeyAccessible).equals(AppConstants.statusValueFalse);
		if (tag.getParentTag().getName().equals("skillcluster")) {
			_accessible	= _accessible && !tag.getParentTag().getCurrentStatusAttribute(AppConstants.statusKeyAccessible).equals(AppConstants.statusValueFalse);
		}

		IXMLTag subSkillTag = _runPVToolkitCacAndTag.getXmlTag();
		//NOTE sub skill and possibly skill cluster are accessible
		_accessible = !subSkillTag.getCurrentStatusAttribute(AppConstants.statusKeyAccessible).equals(AppConstants.statusValueFalse);
		if (subSkillTag.getParentTag().getName().equals("skillcluster")) {
			_accessible	= _accessible && !subSkillTag.getParentTag().getCurrentStatusAttribute(AppConstants.statusKeyAccessible).equals(AppConstants.statusValueFalse);
		}

		
		pvToolkit = (CRunPVToolkit)CDesktopComponents.vView().getComponent("pvToolkit");
		
		setClass("popupDiv");

		_numberOfPerformanceLevels = pvToolkit.getNumberOfPerformanceLevels();
		_numberOfPerformanceLevelsToShow = pvToolkit.getNumberOfPerformanceLevelsToShow();
		_performanceLevels = pvToolkit.getPerformanceLevels();
		
		update();
	}
	
	public void update() {
		getChildren().clear();
		
		if (_runPVToolkitCacAndTag == null) {
			return;
		}
		
		new CRunPVToolkitDefImage(this, 
				new String[]{"class"}, 
				new Object[]{"popupBackground"}
		);

		Div popupDiv = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "ScoreLevels"}
		);

		new CRunPVToolkitDefImage(popupDiv, 
				new String[]{"class", "src"}, 
				new Object[]{_classPrefix + "ScoreLevelsBackground", zulfilepath + "popup-large-background.svg"}
		);

		Div div = new CRunPVToolkitDefDiv(popupDiv, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "ScoreLevelsTitle"}
		);

		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "cacAndTag", "tagChildName"}, 
				new Object[]{"font " + _classPrefix + "ScoreLevelsTitle", _runPVToolkitCacAndTag, "name"}
		);

		CRunPVToolkitCloseImage closeImage = new CRunPVToolkitCloseImage(popupDiv, 
				new String[]{"class", "src"}, 
				new Object[]{_classPrefix + "ScoreLevelCloseImage", zulfilepath + "close.svg"}
		);
		closeImage.init(getId());

		div = new CRunPVToolkitDefDiv(popupDiv, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "ScoreLevelsDiv"}
		);

		Hbox hbox = new CRunPVToolkitDefHbox(div, null, null);

		List<IXMLTag> performancelevelTags = _runPVToolkitCacAndTag.getXmlTag().getChilds("performancelevel");
		for (IXMLTag performancelevelTag : performancelevelTags) {
			int performanceLevel = Integer.parseInt(performancelevelTag.getChildValue("level"));
			if (_performanceLevels[performanceLevel-1] > 0) {
				boolean selected = _level == performanceLevel;
	
				Div specificDiv = new CRunPVToolkitDefDiv(hbox,
						new String[]{"id", "class"}, 
						new Object[]{_idPrefix + "ScoreLevel_" + performanceLevel, _classPrefix + "ScoreLevel"}
				);
				//NOTE width of div depends on number of performance levels
				double width = 5 * specificDivWidth;
				width = width / _numberOfPerformanceLevelsToShow;
				String style = "width:" + width + "px;";
				if (!_feedbackRubric.feedbackIsEditable() || !_accessible) {
					style += "cursor:default;";
				}
				specificDiv.setStyle(style);
				addFeedbackScoreLevelDivOnClickEventListener(specificDiv, performanceLevel);
	
				//NOTE width of stars depends on number of performance levels
				width = 5 * scoreLevelDivWidth;
				width = width / _numberOfPerformanceLevelsToShow;
				style = "width:" + width + "px;";
				Div div3 = new CRunPVToolkitDefDiv(specificDiv, 
						new String[]{"class", "style"}, 
						new Object[]{_classPrefix + "ScoreLevelDiv", style}
				);
	
				//NOTE width of stars depends on number of performance levels
				width = 5 * scoreLevelVboxWidth;
				width = width / _numberOfPerformanceLevelsToShow;
				style = "width:" + width + "px;";
				Vbox vbox = new CRunPVToolkitDefVbox(div3, 
						new String[]{"class", "style"}, 
						new Object[]{_classPrefix + "ScoreLevelVbox", style}
				);
	
				Div div4 = new CRunPVToolkitDefDiv(vbox, 
						new String[]{"id"}, 
						new Object[]{_idPrefix + "ScoreLevelStars_" + performanceLevel}
				);
				//NOTE width of stars depends on number of performance levels
				width = 5 * scoreLevelStarsDivWidth;
				width = width / _numberOfPerformanceLevelsToShow;
				style = "width:" + width + "px;";
				if (!_feedbackRubric.feedbackIsEditable() || !_accessible) {
					style += "cursor:default;";
				}
				div4.setStyle(style);
				addFeedbackScoreLevelStarsDivOnClickEventListener(specificDiv, performanceLevel);
	
				pvToolkit.renderStars(div4, _performanceLevels[performanceLevel-1], selected, "");
	
				CRunPVToolkitCacAndTag runPVToolkitCacAndPerformancelevelTag = new CRunPVToolkitCacAndTag(_runPVToolkitCacAndTag.getCaseComponent(), performancelevelTag);
				
				List<IXMLTag> performanceLevelExampleTags = pvToolkit.getPresentChildTags(runPVToolkitCacAndPerformancelevelTag, "videoperformancelevelexample");
				if (performanceLevelExampleTags.size() > 0) {
					String labelKey = "PV-toolkit-feedback.button.videoexample";
					if (performanceLevelExampleTags.size() > 1) {
						labelKey += "s";
					}
					Label link = new CRunPVToolkitDefLabel(vbox, 
							new String[]{"class", "labelKey"}, 
							new Object[]{"font gridLink " + _classPrefix + "ScoreLevelVideoExamples", labelKey}
							);
					addFeedbackVideoExamplesLinkOnClickEventListener(link, runPVToolkitCacAndPerformancelevelTag);
				}
	
				//NOTE width of description depends on number of performance levels
				width = 5 * scoreLevelDescriptionDivWidth;
				width = width / _numberOfPerformanceLevelsToShow;
				style = "width:" + width + "px;";
				div = new CRunPVToolkitDefDiv(vbox, 
						new String[]{"class", "style"}, 
						new Object[]{_classPrefix + "ScoreLevelDescription", style}
				);
	
				new CRunPVToolkitDefHtml(div,
						new String[]{"class", "cacAndTag", "tagChildName"}, 
						new Object[]{"font " + _classPrefix + "ScoreLevelDescription", runPVToolkitCacAndPerformancelevelTag, "description"}
				);
			}
		}

		if (_hasTipsTops) {
			Button tipsTopsButton = new CRunPVToolkitDefButton(popupDiv,
					new String[]{"class", "labelKey"}, 
					new Object[]{"font pvtoolkitButton " + _classPrefix + "ScoreLevelTipsTopsButton", "PV-toolkit-feedback.button.tipstops"}
					);
			addFeedbackTipsTopsButtonOnClickEventListener(tipsTopsButton, getAttribute("skillclusterCounter"), getAttribute("subskillCounter"));
		}

		if (_feedbackRubric != null && _feedbackRubric.feedbackIsEditable() && _accessible) {
			Button saveButton = new CRunPVToolkitDefButton(popupDiv,
					new String[]{"id", "class", "cLabelKey"}, 
					new Object[]{_idPrefix + "ScoreLevelSaveButton", "font pvtoolkitButton " + _classPrefix + "ScoreLevelSaveButton", "PV-toolkit.save"}
					);
			saveButton.setAttribute("level", _level);
			addFeedbackScoreLevelSaveButtonOnClickEventListener(saveButton);
		}

	}

	protected void addFeedbackScoreLevelDivOnClickEventListener(Component component, int performanceLevel) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				if (_feedbackRubric != null && _feedbackRubric.feedbackIsEditable() && _accessible) {
					CRunPVToolkitFeedbackScoreLevelsDiv feedbackScoreLevels = (CRunPVToolkitFeedbackScoreLevelsDiv)CDesktopComponents.vView().getComponent(_idPrefix + "ScoreLevelsDiv");
					if (feedbackScoreLevels != null) {
						feedbackScoreLevels.updateSelection(_runPVToolkitCacAndTag, performanceLevel);
					}
				}
			}
		});
	}

	protected void addFeedbackScoreLevelStarsDivOnClickEventListener(Component component, int performanceLevel) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				if (_feedbackRubric != null && _feedbackRubric.feedbackIsEditable() && _accessible) {
					CRunPVToolkitFeedbackScoreLevelsDiv feedbackScoreLevels = (CRunPVToolkitFeedbackScoreLevelsDiv)CDesktopComponents.vView().getComponent(_idPrefix + "ScoreLevelsDiv");
					if (feedbackScoreLevels != null) {
						feedbackScoreLevels.updateSelection(_runPVToolkitCacAndTag, performanceLevel);
					}
				}
			}
		});
	}

	protected void addFeedbackVideoExamplesLinkOnClickEventListener(Component component, CRunPVToolkitCacAndTag runPVToolkitCacAndTag) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				CRunPVToolkitVideoExamplesDiv feedbackScoreLevelVideoExamples = (CRunPVToolkitVideoExamplesDiv)CDesktopComponents.vView().getComponent(_idPrefix + "ScoreLevelVideoExamplesDiv");
				if (feedbackScoreLevelVideoExamples != null) {
					//NOTE for unknown reason video examples div does not appear on top of score levels div,
					//although in ZUL file video examples div is defined after score levels div
					setVisible(false);
					//NOTE set attribute so video examples div may show top score levels div again when closed
					feedbackScoreLevelVideoExamples.setAttribute("componentIdToShow", getId());
					feedbackScoreLevelVideoExamples.init(runPVToolkitCacAndTag, "videoperformancelevelexample", true);
					feedbackScoreLevelVideoExamples.setVisible(true);
				}
			}
		});
	}
	
	protected void addFeedbackTipsTopsButtonOnClickEventListener(Component component, Object skillclusterCounter, Object subskillCounter) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				CRunPVToolkitFeedbackScoreLevelTipsTopsDiv feedbackScoreLevelTipsTops = (CRunPVToolkitFeedbackScoreLevelTipsTopsDiv)CDesktopComponents.vView().getComponent(_idPrefix + "ScoreLevelTipsTopsDiv");
				if (feedbackScoreLevelTipsTops != null) {
					feedbackScoreLevelTipsTops.setAttribute("skillclusterCounter", skillclusterCounter);
					feedbackScoreLevelTipsTops.setAttribute("subskillCounter", subskillCounter);
					feedbackScoreLevelTipsTops.init(_runPVToolkitCacAndTag, _level, _feedbackRubricId);
					feedbackScoreLevelTipsTops.setVisible(true);
				}
			}
		});
	}
	
	protected void addFeedbackScoreLevelSaveButtonOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				Component feedbackScoreLevels = CDesktopComponents.vView().getComponent(_idPrefix + "ScoreLevelsDiv");
				if (feedbackScoreLevels != null) {
					feedbackScoreLevels.setVisible(false);
				}
				CRunPVToolkitFeedbackRubricDiv feedbackRubric = (CRunPVToolkitFeedbackRubricDiv)CDesktopComponents.vView().getComponent(_feedbackRubricId);
				if (feedbackRubric != null) {
					feedbackRubric.saveLevel(_runPVToolkitCacAndTag.getXmlTag(), (int)component.getAttribute("level"));
				}
			}
		});
	}
	
	public void updateSelection(CRunPVToolkitCacAndTag runPVToolkitCacAndTag, int level) {
		if (level != _level) {
			if (_level > 0) {
				//deselect previous level
				((HtmlBasedComponent)CDesktopComponents.vView().getComponent(_idPrefix + "ScoreLevel_" + _level)).setClass(_classPrefix + "ScoreLevel");
				pvToolkit.renderStars(CDesktopComponents.vView().getComponent(_idPrefix + "ScoreLevelStars_" + _level), _performanceLevels[_level-1], false, "");
			}
			_level = level;
			//select level
			((HtmlBasedComponent)CDesktopComponents.vView().getComponent(_idPrefix + "ScoreLevel_" + _level)).setClass(_classPrefix + "ScoreLevelSelected");
			pvToolkit.renderStars(CDesktopComponents.vView().getComponent(_idPrefix + "ScoreLevelStars_" + _level), _performanceLevels[_level-1], true, "");

			//update save button
			Component feedbackScoreLevelSaveButton = CDesktopComponents.vView().getComponent(_idPrefix + "ScoreLevelSaveButton");
			if (feedbackScoreLevelSaveButton != null) {
				feedbackScoreLevelSaveButton.setAttribute("level", _level);
			}
		}
	}

}
