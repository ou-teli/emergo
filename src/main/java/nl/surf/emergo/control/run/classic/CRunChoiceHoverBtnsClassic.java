/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Div;
import org.zkoss.zul.Include;

import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.view.VView;
/**
 * The Class CRunChoiceHoverBtns. Ancestor of alle sets of hover buttons used
 * within the run choice area.
 */
public class CRunChoiceHoverBtnsClassic extends CRunAreaClassic {

	private static final long serialVersionUID = -2516952724277201521L;

	/** The button hbox containing the hover buttons. */
	protected CRunHboxClassic buttonHbox = null;

	/** The button area containing the hover buttons. */
	protected CRunAreaClassic buttonArea = null;

	/** The run parallax. Used for parallax effect. */
	protected Include runParallax = null;

	protected List<Component> buttonHboxList = new ArrayList<Component>();

	/** The item count, the number of hover buttons. */
	protected int itemCount = 0;

	/** The first update, used by inherited classes. */
	protected boolean firstUpdate = true;

	/**
	 * Instantiates a new c run choice hover btns.
	 * 
	 * @param aParent the ZK parent
	 * @param aId the a id
	 */
	public CRunChoiceHoverBtnsClassic(Component aParent, String aId) {
		super();
		setId(aId);
		init();
	}

	protected void init() {
		String lType = "";
		if (runWnd != null)
			lType = ((CRunWndClassic)runWnd).getPlayerStatusString("ChoiceAreaLayoutStatus", null);
		setZclass(getClassName() + lType);
		setStyle("overflow:hidden;position:relative;");
		if (runParallax == null) {
			runParallax = (Include)CDesktopComponents.vView().getComponent("runParallax");
			((CRunWndClassic)runWnd).runChoiceArea.appendChild(runParallax);
		}
		String lLayerId = "runLocationLayer1";
		Div runLocationLayer = (Div)CDesktopComponents.vView().getComponent(lLayerId);
		if (runLocationLayer == null) {
			runParallax.setSrc(VView.v_run_parallax_fr);
			runLocationLayer = (Div)CDesktopComponents.vView().getComponent(lLayerId);
		}
		buttonArea = new CRunAreaClassic();
		
		buttonArea.setStyle("overflow:hidden;position:relative;");
		buttonArea.setId(getId() + "ButtonHbox");
		buttonHbox = new CRunHboxClassic();
//		buttonHbox.setSclass(className + "HBox");
		buttonHbox.setSclass(getClassName() + "_hbox");

		buttonArea.appendChild(buttonHbox);
		if (runLocationLayer != null) {
			runLocationLayer.appendChild(buttonArea);
		}
		buttonHboxList.add(buttonHbox);
		update();
	}

	/**
	 * Updates hover buttons.
	 */
	public void update() {
	}

	/**
	 * Sets the number of hover buttons.
	 * 
	 * @param aItemCount the new item count
	 */
	public void setItemCount(int aItemCount) {
		itemCount = aItemCount;
	}

	/**
	 * Sets visibility of choice buttons and size of parallax div depending on aShow.
	 * 
	 * @param aShow the a show
	 */
	public void setParallax(boolean aShow) {
		if (runParallax == null) {
			runParallax = (Include)CDesktopComponents.vView().getComponent("runParallax");
		}
		if (runParallax != null) {
			buttonArea.setVisible(aShow);
			if (aShow) {
				runParallax.setVisible(aShow);
				runWnd.emergoEventToClient("parallax", "", "checksize", buttonHbox.getSclass());
			}
		}
	}
	
	/**
	 * Creates new hover btn.
	 *
	 * @param aId the a id
	 * @param aStatus the a status the button should get
	 * @param aAction the a action to be send when clicked
	 * @param aActionStatus the a action status of the action
	 * @param aPosition the a position, where on the screen the button appears for layout
	 * @param aImgPrefix the a img prefix of the hover images
	 * @param aLabel the a label to be shown above the button
	 *
	 * @return the c run hover btn
	 */
	/**
	protected CRunHoverBtn newHoverBtn(String aId, String aStatus, String aAction,
			Object aActionStatus, String aImgPrefix, String aLabel) {
		CRunHoverBtn cHoverBtn = createHoverBtn(aId, aStatus, aAction, aActionStatus, aImgPrefix, aLabel);
		cHoverBtn.registerObserver(CControl.runWnd);
		cHoverBtn.setVisible(!aStatus.equals("empty"));
		if (runWnd != null)
			cHoverBtn.setWidgetListener("onClick", runWnd.getMediaplayerStopAction());
		return cHoverBtn;
	}
	 */

	/**
	 * Creates new hover btn.
	 *
	 * @param aId the a id
	 * @param aStatus the a status the button should get
	 * @param aAction the a action to be send when clicked
	 * @param aActionStatus the a action status of the action
	 * @param aPosition the a position, where on the screen the button appears for layout
	 * @param aImgPrefix the a img prefix of the hover images
	 * @param aLabel the a label to be shown above the button
	 *
	 * @return the c run hover btn
	 */
		/**
	protected CRunHoverBtn createHoverBtn(String aId, String aStatus, String aAction,
			Object aActionStatus, String aImgPrefix, String aLabel) {
		CRunHoverBtn lBtn = new CRunHoverBtn(aId, aStatus, aAction, aActionStatus, aImgPrefix, aLabel, "");
		lBtn.setDir("reverse");
		return lBtn;
	}
	 */

	/**
	 * Gets the btn status as string.
	 *
	 * @param aVisible the a visible
	 * @param aEnabled the a enabled
	 * @param aSelected the a selected
	 *
	 * @return the btn status
	 */
	public String getBtnStatus(boolean aVisible, boolean aEnabled,
			boolean aSelected) {
		String lStatus = "active";
		if (!aVisible)
			lStatus = "empty";
		else {
			if (!aEnabled)
				lStatus = "inactive";
			else {
				if (aSelected)
					lStatus = "selected";
			}
		}
		return lStatus;
	}

	/**
	 * Selects button given by selectId.
	 *
	 * @param selectId the select id
	 */
	public void selectBtn(String selectId) {
		for (Component lButtonHbox : buttonHboxList) {
			for (Component lComp : lButtonHbox.getChildren()) {
				if (lComp instanceof CRunHoverBtnClassic) {
					CRunHoverBtnClassic lBtn = (CRunHoverBtnClassic)lComp;
					if ((lBtn.getId().equals(selectId)) && (!lBtn.isStatus("selected")))
						lBtn.setStatus("selected");
				}
			}
		}
	}

	/**
	 * Deselects the previously selected button if it's id is unequal to notDeselectId
	 *
	 * @param notDeselectId the not deselect id
	 */
	public void deselectBtn(String notDeselectId) {
		for (Component lButtonHbox : buttonHboxList) {
			for (Component lComp : lButtonHbox.getChildren()) {
				if (lComp instanceof CRunHoverBtnClassic) {
					CRunHoverBtnClassic lBtn = (CRunHoverBtnClassic)lComp;
					if ((lBtn.isStatus("selected")) && (!lBtn.getId().equals(notDeselectId)))
						lBtn.deselect();
				}
			}
		}
	}

	/**
	 * Updates existing btn given by aBtnName with status given by aStatusKey and
	 * aStatusValue.
	 *
	 * @param aBtnName the a btn name
	 * @param aStatusKey the a status key
	 * @param aStatusValue the a status value
	 */
	public void updateBtn(String aBtnName, String aStatusKey, String aStatusValue) {
		CRunHoverBtnClassic lBtn = (CRunHoverBtnClassic) CDesktopComponents.vView().getComponent(aBtnName);
		if (lBtn == null)
			return;
		boolean lBool = (aStatusValue.equals(AppConstants.statusValueTrue));
		if (aStatusKey.equals(AppConstants.statusKeyPresent)) {
			if ((!lBtn.isVisible()) && (lBool))
				itemCount = itemCount + 1;
			if ((lBtn.isVisible()) && (!lBool))
				itemCount = itemCount - 1;
			lBtn.setBtnVisible(lBool);
			if (lBool) {
				boolean lAccessible = true;
				String lAccStr = (String)lBtn.getAttribute("accessible");
				if ((lAccStr != null) && (!lAccStr.equals("")) && (!lAccStr.equals("true")))
					lAccessible = false;
				lBtn.setBtnEnabled(lAccessible);
			}
			setParallax(true);
		}
		if (aStatusKey.equals(AppConstants.statusKeyAccessible)) {
			lBtn.setBtnEnabled(lBool);
			lBtn.setAttribute("accessible", "" + lBool);
		}
		if (aStatusKey.equals(AppConstants.statusKeySelected))
			lBtn.setBtnSelected(lBool);
	}
	
}
