/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zul.Div;
import org.zkoss.zul.Include;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CCaseHelper;
import nl.surf.emergo.control.CContentHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefInclude;
import nl.surf.emergo.control.run.CRunArea;
import nl.surf.emergo.control.run.CRunAssessments;
import nl.surf.emergo.control.run.CRunButton;
import nl.surf.emergo.control.run.CRunComponentHelper;
import nl.surf.emergo.control.run.CRunHbox;
import nl.surf.emergo.control.run.CRunVbox;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.STriggeredReference;

/**
 * The Class CRunAssessmentsOunl is used to show assessments within the run view area of the
 * Emergo player.
 */
public class CRunAssessmentsOunl extends CRunAssessments {

	private static final long serialVersionUID = 5584118707664094343L;

	/** The content tag. */
	protected CRunComponentHelper cRunComponentHelper = null;

	/** The content tag. */
	protected IXMLTag contenttag = null;

	/** The assessment tags. */
	protected List<IXMLTag> assessmenttags = new ArrayList<IXMLTag>();

	/** The current assessment tag. */
	protected IXMLTag currentassessmenttag = null;

	/** The current assessment ref item tags. */
	protected List<IXMLTag> currentassessmentrefitemtags = new ArrayList<IXMLTag>();

	/** The assessments active ref item tags. */
	protected Hashtable<String, List<IXMLTag>> assessmentsactiverefitemtags = new Hashtable<String, List<IXMLTag>>();

	/** The assessments active ref item tags. */
	protected Hashtable<String, Hashtable<String, List<IXMLTag>>> assessmentsrefitemalttags = new Hashtable<String, Hashtable<String, List<IXMLTag>>>();

	/** The current assessment ref item tag. */
	protected IXMLTag currentassessmentrefitemtag = null;

	/** The case helper. */
	protected CCaseHelper caseHelper = null;

	/** The on tablet tag. */
	protected boolean onTablet = true;
	
	/** The label for overview button, overwrites default if not empty. */
	protected String labelforoverviewbutton = "";
	
	/** The number of assessments, determines if overview button must be shown. */
	protected int presentassessmentssize = 0;
	
	public class ItemTagData {
		
		private IECaseRole caseRole;
		private IECaseComponent caseComponent;
		private IXMLTag itemTag;
		
		public IECaseRole getCaseRole() {
			return caseRole;
		}
		public void setCaseRole(IECaseRole caseRole) {
			this.caseRole = caseRole;
		}
		public IECaseComponent getCaseComponent() {
			return caseComponent;
		}
		public void setCaseComponent(IECaseComponent caseComponent) {
			this.caseComponent = caseComponent;
		}
		public IXMLTag getItemTag() {
			return itemTag;
		}
		public void setItemTag(IXMLTag itemTag) {
			this.itemTag = itemTag;
		}

	}
	
	/**
	 * Gets the case helper.
	 *
	 * @return the case helper
	 */
	protected CCaseHelper getCaseHelper() {
		if (caseHelper == null)
			caseHelper = new CCaseHelper();
		return caseHelper;
	}

	/**
	 * Instantiates a new c run assessments.
	 */
	public CRunAssessmentsOunl() {
		super();
		labelforoverviewbutton = CDesktopComponents.sSpring().getCurrentRunComponentStatus(caseComponent, "labelforoverviewbutton", getRunStatusType());
		init();
	}

	/**
	 * Instantiates a new c run assessments.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the assessments case component
	 * @param aOnTablet the a on tablet
	 */
	public CRunAssessmentsOunl(String aId, IECaseComponent aCaseComponent, boolean aOnTablet) {
		super(aId, aCaseComponent);
		onTablet = aOnTablet;
		labelforoverviewbutton = CDesktopComponents.sSpring().getCurrentRunComponentStatus(caseComponent, "labelforoverviewbutton", getRunStatusType());
		init();
	}

	/**
	 * Creates title area, content area to show tab structure,
	 * and buttons area with close button.
	 */
	@Override
	protected void createComponents() {
		CRunVbox lVbox = new CRunVbox();

		createTitleArea(lVbox);
		createContentArea(lVbox);
		createButtonsArea(lVbox);

		appendChild(lVbox);
	}

	/**
	 * Creates new content component, the references tree.
	 *
	 * @return the component
	 */
	@Override
	protected Component newContentComponent() {
		Include assessmentsView = new CDefInclude();
		assessmentsView.setId("runAssessmentsView");
		//NOTE Set width and height to 0, because they default are 100%, which might cause problems when rendering an include, because it horizontally becomes to wide.
		assessmentsView.setWidth("0");
		assessmentsView.setHeight("0");
		return assessmentsView;
	}

	/**
	 * Creates buttons area and adds close button.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run hbox
	 */
	@Override
	protected CRunHbox createButtonsArea(Component aParent) {
		return null;
	}

	/**
	 * Creates view for assessments.
	 * Gets current assessments, sets selected and opened of it to true and opens it.
	 */
	public void init() {
		CRunComponentDecoratorOunl decorator = createDecorator();
		if (onTablet) {
			decorator.createCloseArea(caseComponent, (Component)getAttribute("runTitleAreaHbox"), getClassName(), getId(), this);
		}
		else {
			decorator.createCloseAreaWithoutCloseBtn(caseComponent, (Component)getAttribute("runTitleAreaHbox"), getClassName());
		}

		contenttag = getContentTag();
		setAssessmentTags();

		getRunContentComponent().setAttribute("on_tablet", onTablet);
		getRunContentComponent().setAttribute("assessments_title", getAssessmentsTitle());
		getRunContentComponent().setAttribute("assessments_instruction", getAssessmentsInstruction());
		List<List<Object>> assessmentsDatas = getAssessmentsDatas();
		getRunContentComponent().setAttribute("assessments_datas", assessmentsDatas);
		if (assessmentsDatas.size() == 1) {
			String assessmentTagId = (String)assessmentsDatas.get(0).get(0);
			currentassessmenttag = getAssessmentTag(assessmentTagId);
			getRunContentComponent().setAttribute("assessment_datas", getAssessmentDatas(currentassessmenttag));
		}
		((Include)getRunContentComponent()).setAttribute("emergoComponent", this);
		((Include)getRunContentComponent()).setSrc(VView.v_run_assessments_fr);
	}

	public String getXMLTagId(IXMLTag aXMLTag) {
		if (aXMLTag == null) {
			return "";
		}
		return aXMLTag.getAttribute(AppConstants.keyId);
	}
	
	/**
	 * Update assessment.
	 */
	@Override
	public void updateAssessments() {
		renderAssessmentsMacro();
	}

	protected void renderAssessmentsMacro() {
		String macroAssessmentsId = "macroAssessments";
		HtmlMacroComponent macroAssessment = (HtmlMacroComponent)CDesktopComponents.vView().getComponent(macroAssessmentsId);
		macroAssessment.setDynamicProperty("a_assessments_title", getAssessmentsTitle());
		macroAssessment.setDynamicProperty("a_assessments_instruction", getAssessmentsInstruction());
		List<List<Object>> assessmentsDatas = getAssessmentsDatas();
		macroAssessment.setDynamicProperty("a_assessments_datas", assessmentsDatas);
		// NOTE if assessment div visible, close it if present of current assessment is set to false
		Boolean assessmentTagChanged = true;
		if (currentassessmenttag != null) {
			currentassessmenttag = getAssessmentTag(getAssessmentId(currentassessmenttag));
			if (currentassessmenttag != null) {
				assessmentTagChanged = false;
			}
			else {
				String runAssessmentDivId = "runAssessmentDiv";
				Div lRunAssessmentDiv = (Div)CDesktopComponents.vView().getComponent(runAssessmentDivId);
				if (lRunAssessmentDiv != null) {
					lRunAssessmentDiv.setVisible(false);
				}
			}
		}
		macroAssessment.recreate();
		int assessmentsSize = assessmentsDatas.size();
		Boolean showOverview = false;
		if (assessmentsSize != presentassessmentssize) {
			if ((presentassessmentssize == 1) && (assessmentsSize > 1)) {
				showOverview = true;
			}
			presentassessmentssize = assessmentsSize; 
		}
		if (showOverview || ((assessmentsSize == 1) && (assessmentTagChanged))) {
			String assessmentTagId = "";
			// NOTE update shown assessment
			if (assessmentTagChanged) {
				assessmentTagId = (String)assessmentsDatas.get(0).get(0);
				currentassessmenttag = getAssessmentTag(assessmentTagId);
			}
			else
				assessmentTagId = getAssessmentId(currentassessmenttag);
			List<List<Object>> assessmentDatas = getAssessmentDatas(currentassessmenttag);
			macroAssessment.setDynamicProperty("a_assessment_datas", assessmentDatas);
			Boolean multipleQuestions = (assessmentDatas.size() > 1 || !assessmentsDatas.get(0).get(5).equals("") || assessmentsDatas.get(0).get(9).equals("false"));
			if (!multipleQuestions) {
				prepairAssessmentItem(assessmentTagId);
				handleAssessmentItem((String)assessmentDatas.get(0).get(0));
			}
			else
				handleAssessment(assessmentTagId);
		}
	}
	
	/**
	 * Gets the run component helper.
	 *
	 * @return the run component helper
	 */
	@Override
	public CRunComponentHelper getRunComponentHelper() {
		if (cRunComponentHelper == null) {
			cRunComponentHelper = new CRunComponentHelper(null, "chapter", caseComponent, this);
		}
		return cRunComponentHelper;
	}

	/**
	 * Gets content tag.
	 */
	public IXMLTag getContentTag() {
		IXMLTag lXmlTree = getRunComponentHelper().getXmlDataPlusStatusTree();
		if (lXmlTree != null) {
			return lXmlTree.getChild(AppConstants.contentElement); 
		}
		return null;
	}

	/**
	 * sets the assessment tags property: list of all present and not-deleted assessments.
	 */
	protected void setAssessmentTags() {
		assessmenttags = getAssessmentTags();
	}

	/**
	 * Gets all assessments.
	 */
	public List<IXMLTag> getAssessmentTags() {
		if (contenttag != null) {
			List<IXMLTag> lAssessmentTags = contenttag.getChilds("assessment"); 
			List<IXMLTag> lVisibleAssessmentTags = new ArrayList<IXMLTag>();
			for (IXMLTag lAssessmentTag : lAssessmentTags) {
				boolean lHidden = ((CDesktopComponents.sSpring().getCurrentTagStatus(lAssessmentTag,
						AppConstants.statusKeyPresent))
						.equals(AppConstants.statusValueFalse));
				boolean lDeleted = ((CDesktopComponents.sSpring().getCurrentTagStatus(lAssessmentTag,
						AppConstants.statusKeyDeleted))
						.equals(AppConstants.statusValueTrue));
				if (!(lHidden || lDeleted)) {
					lVisibleAssessmentTags.add(lAssessmentTag);
				}
			}
			return lVisibleAssessmentTags;
		}
		else {
			return new ArrayList<IXMLTag>();
		}
	}

	protected String getAssessmentsTitle() {
		String lTitle = labelforoverviewbutton;
		if (lTitle.equals("")) {
			lTitle = CDesktopComponents.vView().getLabel("run_assessments.button.overview");
		}
		return lTitle;
	}

	protected String getAssessmentsInstruction() {
		IXMLTag lInstructionTag = getInstructionTag(contenttag);
		if (lInstructionTag != null) {
			String lRichtext = CDesktopComponents.sSpring().unescapeXML(lInstructionTag.getChildValue("richtext").trim());
			return getRunComponentHelper().stripCKeditorString(lRichtext);
		}
		return "";
	}

	protected IXMLTag getInstructionTag(IXMLTag aContentTag) {
		if (aContentTag == null) {
			return null;
		}
		List<IXMLTag> lInstructionTags = aContentTag.getChilds("instruction");
		for (IXMLTag lInstructionTag : lInstructionTags) {
			boolean lOpened = lInstructionTag.getCurrentStatusAttribute(AppConstants.statusKeyOpened).equals(AppConstants.statusValueTrue);
			if (lOpened) {
				boolean lPresent = !lInstructionTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse);
				if (lOpened && lPresent) {
					// get first opened and present
					return lInstructionTag;
				}
			}
		}
		return null;
	}

	protected List<List<Object>> getAssessmentsDatas() {
		List<List<Object>> lDataList = new ArrayList<List<Object>>();
		
		List<IXMLTag> lAssessmentTags = getAssessmentTags();
		for (IXMLTag lAssessmentTag : lAssessmentTags) {
			List<Object> data = new ArrayList<Object>();
			data.add(getAssessmentId(lAssessmentTag));
			String instruction = getAssessmentInstruction(lAssessmentTag);
			String assessmentItemId = "";
			List<IXMLTag> assessmentRefItemTags = getAssessmentActualRefItemTags(lAssessmentTag);
			if (assessmentRefItemTags.size() == 1 && instruction.equals("")) {
				// if only 1 item and assessment instruction empty show assessment item when choosing assessment
				assessmentItemId = getRefItemId(assessmentRefItemTags.get(0)); 
			}
			data.add(assessmentItemId);
			data.add("" + isAssessmentStarted(lAssessmentTag));
			data.add("" + isAssessmentFinished(lAssessmentTag));
			data.add(getAssessmentTitle(lAssessmentTag));
			data.add(instruction);
			data.add("" + getAssessmentNumberOfItems(lAssessmentTag));
			data.add("" + isAssessmentScoreVisible(lAssessmentTag));
			data.add("" + getAssessmentScore(lAssessmentTag));
			data.add("" + isAutostartAssessment(lAssessmentTag));
			IXMLTag lFeedbackTag = getAssessmentFeedbackCondition(lAssessmentTag);
			data.add(getAssessmentFeedback(lFeedbackTag));
			data.add("" + isAssessmentFeedbackVisible(lAssessmentTag));
			data.add(getPiecesDatas(null, null, lFeedbackTag));
			lDataList.add(data);
		}

		return lDataList;
	}

	protected String getAssessmentId(IXMLTag aAssessmentTag) {
		return getXMLTagId(aAssessmentTag);
	}
	
	public String getAssessmentTitle(IXMLTag aAssessmentTag) {
		return CDesktopComponents.sSpring().unescapeXML(aAssessmentTag.getChild("name").getValue());
	}
	
	public String getAssessmentInstruction(IXMLTag aAssessmentTag) {
		String lRichtext = CDesktopComponents.sSpring().unescapeXML(aAssessmentTag.getChildValue("hovertext").trim());
		return getRunComponentHelper().stripCKeditorString(lRichtext);
	}
	
	protected List<IXMLTag> getAssessmentRefItemTags(IXMLTag aAssessmentTag) {
		return aAssessmentTag.getChilds("refitem");
	}
	
	public List<IXMLTag> getAssessmentActualRefItemTags(IXMLTag aAssessmentTag) {
		//NOTE could be that assessment is reset from script; check it here
		if (isAssessmentReset(aAssessmentTag))
			resetAssessment(aAssessmentTag);
		List<IXMLTag> lActualRefItemTags = assessmentsactiverefitemtags.get(getAssessmentId(aAssessmentTag));
		if (lActualRefItemTags == null) {
			prepairRefItemTags(aAssessmentTag);
			lActualRefItemTags = assessmentsactiverefitemtags.get(getAssessmentId(aAssessmentTag));
		}
		return lActualRefItemTags;
	}
	
	protected List<IXMLTag> getAssessmentVisibleRefItemTags(IXMLTag aAssessmentTag) {
		List<IXMLTag> lVisibleRefItemTags = new ArrayList<IXMLTag>();
		List<IXMLTag> lRefItemTags = getAssessmentRefItemTags(aAssessmentTag);
		//NOTE for running assessment, return only the actual items (possibly randomly selected at start of assessment)
		//for example, at session restart assessment items must be equal to selected items for running assessments, and for finished assessments that are not
		//yet restarted (perhaps presented in different order if randomized is set, but that doesn't matter)
		boolean lStarted = isAssessmentEditable(aAssessmentTag);
		boolean lFinished = isAssessmentFinished(aAssessmentTag);
		for (IXMLTag lRefItemTag : lRefItemTags) {
			boolean lPresent = true;
			boolean lDeleted = false;
			lPresent = !CDesktopComponents.sSpring().getCurrentTagStatus(lRefItemTag, AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse);
			lDeleted = CDesktopComponents.sSpring().getCurrentTagStatus(lRefItemTag, AppConstants.statusKeyDeleted).equals(AppConstants.statusValueTrue);
			if (lPresent && !(lDeleted && (lStarted || lFinished))) {
				lVisibleRefItemTags.add(lRefItemTag);
			}
		}
		return lVisibleRefItemTags;
	}
	
	protected int getAssessmentTotalNumberOfVisibleItems(IXMLTag aAssessmentTag) {
		return getAssessmentVisibleRefItemTags(aAssessmentTag).size();
	}
	
	protected int getAssessmentNumberOfItems(IXMLTag aAssessmentTag) {
		int lTotalNumber = getAssessmentTotalNumberOfVisibleItems(aAssessmentTag);
		int lWantedNumber = lTotalNumber;
		try {
			lWantedNumber = Integer.parseInt(aAssessmentTag.getChildValue("numberofitems"));
		} catch (NumberFormatException e) {
			lWantedNumber = lTotalNumber;
		}
		if (lWantedNumber > lTotalNumber) {
			lWantedNumber = lTotalNumber;
		}
		return lWantedNumber;
	}
	
	protected int getAssessmentNumberOfResits(IXMLTag aAssessmentTag) {
		int lNumberOfResits = -1;
		try {
			lNumberOfResits = Integer.parseInt(aAssessmentTag.getChildValue("numberofresits"));
		} catch (NumberFormatException e) {
			lNumberOfResits = -1;
		}
		return lNumberOfResits;
	}
	
	protected int getAssessmentScore(IXMLTag aAssessmentTag) {
		int lScore = 0;
		String lScoreStr = CDesktopComponents.sSpring().getCurrentRunTagStatus(caseComponent, getXMLTagId(aAssessmentTag), AppConstants.statusKeyScore, getRunStatusType());
		if (!lScoreStr.equals("")) {
			lScore = Integer.parseInt(lScoreStr);
		}
		return lScore;
	}
	
	protected void setAssessmentScore(IXMLTag aAssessmentTag, int aScore) {
		setRunTagStatus(caseComponent, aAssessmentTag, AppConstants.statusKeyScore, "" + aScore, true);
	}

	protected boolean isAssessmentPreviewed(IXMLTag aAssessmentTag) {
		boolean lPreviewed = false;
		lPreviewed = CDesktopComponents.sSpring().getCurrentTagStatus(aAssessmentTag, AppConstants.statusKeyPreviewed).equals(AppConstants.statusValueTrue);
		return lPreviewed;
	}

	protected void setAssessmentPreviewed(IXMLTag aAssessmentTag, String aPreviewedBooleanStr) {
		setRunTagStatus(caseComponent, aAssessmentTag, AppConstants.statusKeyPreviewed, aPreviewedBooleanStr, true);
	}

	protected boolean isAssessmentStarted(IXMLTag aAssessmentTag) {
		return CDesktopComponents.sSpring().getCurrentTagStatus(aAssessmentTag, AppConstants.statusKeyStarted).equals(AppConstants.statusValueTrue);
	}

	protected void setAssessmentStarted(IXMLTag aAssessmentTag, String aStartedBooleanStr) {
		setRunTagStatus(caseComponent, aAssessmentTag, AppConstants.statusKeyStarted, aStartedBooleanStr, true);
	}

	protected boolean isAssessmentFinished(IXMLTag aAssessmentTag) {
		return CDesktopComponents.sSpring().getCurrentTagStatus(aAssessmentTag, AppConstants.statusKeyFinished).equals(AppConstants.statusValueTrue);
	}

	protected boolean isAssessmentActive(IXMLTag aAssessmentTag) {
		return !CDesktopComponents.sSpring().getCurrentTagStatus(aAssessmentTag, AppConstants.statusKeyActive).equals(AppConstants.statusValueFalse);
	}

	public void setAssessmentFinished(IXMLTag aAssessmentTag, String aFinishedBooleanStr) {
		setRunTagStatus(caseComponent, aAssessmentTag, AppConstants.statusKeyFinished, aFinishedBooleanStr, true);
	}

	protected boolean isAssessmentEverFinished(IXMLTag aAssessmentTag) {
		return aAssessmentTag.getStatusAttributeCount(AppConstants.statusKeyFinished, AppConstants.statusValueTrue) > 0;
	}

	protected void setAssessmentReset(IXMLTag aAssessmentTag, String aResetBooleanStr) {
		setRunTagStatus(caseComponent, aAssessmentTag, AppConstants.statusKeyReset, aResetBooleanStr, true);
	}

	protected boolean isAssessmentReset(IXMLTag aAssessmentTag) {
		return CDesktopComponents.sSpring().getCurrentTagStatus(aAssessmentTag, AppConstants.statusKeyReset).equals(AppConstants.statusValueTrue);
	}

	protected boolean isAutostartAssessment(IXMLTag aAssessmentTag) {
		boolean lAutostart = false;
		lAutostart = (CDesktopComponents.sSpring().getCurrentRunTagStatus(caseComponent, aAssessmentTag, "autostart", getRunStatusType()).equals(AppConstants.statusValueTrue));
		return lAutostart;
	}

	protected boolean isAssessmentEditable(IXMLTag aAssessmentTag) {
		return isAssessmentStarted(aAssessmentTag) && !isAssessmentFinished(aAssessmentTag) && isAssessmentActive(aAssessmentTag);
	}

	protected boolean hasAssessmentSit(IXMLTag aAssessmentTag) {
		int lNumberOfResits = getAssessmentNumberOfResits(aAssessmentTag);
		if (lNumberOfResits < 0) {
			return true;
		}
		aAssessmentTag = CDesktopComponents.sSpring().getDataStatusTag(caseComponent, aAssessmentTag, getRunStatusType());
		int lNumberOfSits = aAssessmentTag.getStatusAttributeCount(AppConstants.statusKeyFinished, AppConstants.statusValueTrue);
		return lNumberOfSits <= lNumberOfResits; 
	}

	protected boolean isAssessmentScoreVisible(IXMLTag aAssessmentTag) {
		boolean lShowScore = false;
		lShowScore = (CDesktopComponents.sSpring().getCurrentRunTagStatus(caseComponent, aAssessmentTag, AppConstants.statusKeyShowscore, getRunStatusType()).equals(AppConstants.statusValueTrue));
		return lShowScore && ((isAutostartAssessment(aAssessmentTag) && isAssessmentStarted(aAssessmentTag)) || isAssessmentFinished(aAssessmentTag));
	}

	protected boolean isAssessmentFeedbackVisible(IXMLTag aAssessmentTag) {
		boolean lShowFeedback = !(CDesktopComponents.sSpring().getCurrentRunTagStatus(caseComponent, aAssessmentTag, AppConstants.statusKeyShowassfeedback, getRunStatusType()).equals(AppConstants.statusValueFalse));
		return lShowFeedback && ((isAutostartAssessment(aAssessmentTag) && isAssessmentStarted(aAssessmentTag)) || isAssessmentFinished(aAssessmentTag));
	}

	protected boolean mayUserFinishAssessment(IXMLTag aAssessmentTag) {
		return !CDesktopComponents.sSpring().getCurrentTagStatus(aAssessmentTag, AppConstants.statusKeyUsernotabletofinish).equals(AppConstants.statusValueTrue);
	}

	/**
	 * Gets the feedback condition tag applicable for the given aAssessment.
	 * 
	 * @param aAssessment the a assessment tag
	 * 
	 * @return the feedback condition
	 */
	protected IXMLTag getAssessmentFeedbackCondition(IXMLTag aAssessment) {
		for (IXMLTag lFbTag : aAssessment.getChilds("feedbackcondition")) {
			if (CDesktopComponents.sSpring().getSScriptHelper().evaluateConditionTag(null, lFbTag, AppConstants.statusTypeRunGroup, false, false)) {
				String lTagPid = "";
				if (lFbTag != null) {
					lTagPid = lFbTag.getChildValue("name");
					if (lTagPid.equals(""))
						lTagPid = lFbTag.getChildValue("pid");
				}
				lTagPid = CDesktopComponents.sSpring().unescapeXML(lTagPid);
				CDesktopComponents.sSpring().getSLogHelper().logSetFeedbackAction("feedback condition triggered", CDesktopComponents.sSpring().unescapeXML(caseComponent.getName()), lFbTag.getName(), lTagPid);
				return lFbTag;
			}
		}
		return null;
	}

	/**
	 * Preview assessment.
	 *
	 * @param aAssessmentTag the a assessment tag
	 */
	public void previewAssessment(IXMLTag aAssessmentTag) {
		if (isAssessmentPreviewed(aAssessmentTag)) {
			return;
		}
		setAssessmentPreviewed(aAssessmentTag, AppConstants.statusValueTrue);
		if (isAssessmentEverFinished(aAssessmentTag)) {
			//never reached??
			resetRefItemTags(aAssessmentTag);
		}
		prepairRefItemTags(aAssessmentTag);
	}

	protected void resetRefItemTags(IXMLTag aAssessmentTag) {
		List<IXMLTag> lRefItemTags = getAssessmentRefItemTags(aAssessmentTag);
//		clear answers of previously used refitems
		for (IXMLTag lRefItemTag : lRefItemTags) {
			boolean lPresent = !CDesktopComponents.sSpring().getCurrentTagStatus(lRefItemTag,AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse);
			// NOTE If assessment with randomly selected items, deleted for not selected items was set to true
			boolean lDeleted = CDesktopComponents.sSpring().getCurrentTagStatus(lRefItemTag,AppConstants.statusKeyDeleted).equals(AppConstants.statusValueTrue);
			if (lPresent && !lDeleted) {
				//updateAllExistingRefStatusChildTags(currentassessmentrefitemtag, "refalternative", AppConstants.statusKeyOpened, "" + AppConstants.statusValueFalse);
				//updateAllExistingRefStatusChildTags(currentassessmentrefitemtag, "reffield", AppConstants.statusKeyAnswer, "");
				//updateAllExistingRefStatusChildTags(currentassessmentrefitemtag, "reffeedbackcondition", AppConstants.statusKeyOpened, AppConstants.statusValueFalse);
				updateAllExistingRefStatusChildTags(lRefItemTag, "refalternative", AppConstants.statusKeyOpened, "" + AppConstants.statusValueFalse);
				updateAllExistingRefStatusChildTags(lRefItemTag, "refalternative", AppConstants.statusKeySequencenumber, "");
				updateAllExistingRefStatusChildTags(lRefItemTag, "reffield", AppConstants.statusKeyAnswer, "");
				updateAllExistingRefStatusChildTags(lRefItemTag, "reffield", AppConstants.statusKeySequencenumber, "");
				updateAllExistingRefStatusChildTags(lRefItemTag, "reffeedbackcondition", AppConstants.statusKeyOpened, AppConstants.statusValueFalse);
				
				if (!((CDesktopComponents.sSpring().getCurrentTagStatus(lRefItemTag, AppConstants.statusKeyAnswer) == null) ||
						(CDesktopComponents.sSpring().getCurrentTagStatus(lRefItemTag, AppConstants.statusKeyAnswer).equals(""))))
					setRunTagStatus(caseComponent, lRefItemTag, AppConstants.statusKeyAnswer, "", true);
				if (!((CDesktopComponents.sSpring().getCurrentTagStatus(lRefItemTag, AppConstants.statusKeyFeedbackConditionId) == null) ||
						(CDesktopComponents.sSpring().getCurrentTagStatus(lRefItemTag, AppConstants.statusKeyFeedbackConditionId).equals(""))))
					setRunTagStatus(caseComponent, lRefItemTag, AppConstants.statusKeyFeedbackConditionId, "", true);
				if (!((CDesktopComponents.sSpring().getCurrentTagStatus(lRefItemTag, AppConstants.statusKeyScore) == null) ||
						(CDesktopComponents.sSpring().getCurrentTagStatus(lRefItemTag, AppConstants.statusKeyScore).equals("0"))))
					setRunTagStatus(caseComponent, lRefItemTag, AppConstants.statusKeyScore, "0", true);
				if (!((CDesktopComponents.sSpring().getCurrentTagStatus(lRefItemTag, AppConstants.statusKeyOpened) == null) ||
						(CDesktopComponents.sSpring().getCurrentTagStatus(lRefItemTag, AppConstants.statusKeyOpened).equals(AppConstants.statusValueFalse))))
					setRunTagStatus(caseComponent, lRefItemTag, AppConstants.statusKeyOpened, AppConstants.statusValueFalse, true);
				setRunTagStatus(caseComponent, lRefItemTag, AppConstants.statusKeySequencenumber, "", true);
			}
			if (lDeleted) {
				setRunTagStatus(caseComponent, lRefItemTag, AppConstants.statusKeyDeleted, AppConstants.statusValueFalse, true);
			}
		}
		String lAssId = getAssessmentId(aAssessmentTag);
		assessmentsactiverefitemtags.remove(lAssId);
		assessmentsrefitemalttags.remove(lAssId);
	}

	protected List<IXMLTag> selectRandomizedTags(List<IXMLTag> aTags, int aSelectionSize, boolean aReturnRandomized, IECaseComponent aCaseComponent) {
		List<IXMLTag> lRandomizedTags = new ArrayList<IXMLTag>();
		if ((aTags == null) || (aTags.size() == 0))
			return lRandomizedTags;
		int lTotalNumber = aTags.size();
//		check if wanted number of items is smaller than total number of items, so random draw of items
		boolean lRandomDraw = aSelectionSize < lTotalNumber;
//		and return in random order if status randomizeitems is set
		if (lRandomDraw || aReturnRandomized) {
//			if so randomize items and hide items not used
			//NOTE if subset of possible items, then select the items chosen the least number of times in earlier assessments
			List<IXMLTag> lSubSetTags = new ArrayList<IXMLTag>();
			int lSubSetSize = 0;
			if (lRandomDraw) {
				Hashtable<Integer, List<IXMLTag>> lNrChosenTags = new Hashtable<Integer, List<IXMLTag>>();
				int lMax = 0;
				for (IXMLTag lTag : aTags) {
					String lChosen = CDesktopComponents.sSpring().getCurrentTagStatus(lTag, AppConstants.statusKeyChosennumber);
					int lChosenNr = 0;
					if (!lChosen.equals("")) {
						lChosenNr += Integer.parseInt(lChosen);
					}
					if (lChosenNr > lMax)
						lMax = lChosenNr;
					List<IXMLTag> lTmpTags = new ArrayList<IXMLTag>();
					if (!((lNrChosenTags.get(lChosenNr) == null) || (lNrChosenTags.get(lChosenNr).size() == 0)))
						lTmpTags.addAll(lNrChosenTags.get(lChosenNr));
					lTmpTags.add(lTag);
					lNrChosenTags.put(lChosenNr, lTmpTags);
				}
				for (int i=0; i <= lMax; i++) {
					if (lSubSetSize < aSelectionSize) {
						List<IXMLTag> lTmpTags = lNrChosenTags.get(i);
						if (!((lTmpTags == null) || (lTmpTags.size() == 0))) {
							lSubSetSize += lTmpTags.size();
							lSubSetTags.addAll(lTmpTags);
						}
					}
				}
			} else {
				lSubSetTags.addAll(aTags);
				lSubSetSize = lTotalNumber;
			}
			
//			get random wanted numbers out of totalnumber
			List<Integer> lNumbersToUse = new ArrayList<Integer>();
			while (lNumbersToUse.size() < aSelectionSize) {
				int lRandomInt = Integer.parseInt("" + Math.round(Math.random() * lSubSetSize));
				if (lRandomInt == lSubSetSize)
					lRandomInt = 0;
				if (!lNumbersToUse.contains(lRandomInt))
					lNumbersToUse.add(lRandomInt);
			}
			
			int lCounter = 0;
			for (IXMLTag lTag : lSubSetTags) {
				if (!lNumbersToUse.contains(lCounter))
					setRunTagStatus(aCaseComponent, lTag, AppConstants.statusKeyDeleted, AppConstants.statusValueTrue, true);
				else if (!aReturnRandomized)
					lRandomizedTags.add(lTag);
				lCounter = lCounter + 1;
			}
			if (aReturnRandomized) {
				//randomize lVisibleRefItemTags
				for (int i = 0; i < aSelectionSize; i++) {
					lRandomizedTags.add(lSubSetTags.get(lNumbersToUse.get(i)));
				}
			}
		} else
			lRandomizedTags.addAll(aTags);
		
		int lCounter = 1;
		for (IXMLTag lTag : lRandomizedTags) {
			setRunTagStatus(aCaseComponent, lTag, AppConstants.statusKeySequencenumber, "" + lCounter, true);
			String lChosen = CDesktopComponents.sSpring().getCurrentTagStatus(lTag, AppConstants.statusKeyChosennumber);
			int lChosenNr = 1;
			if (!lChosen.equals("")) {
				lChosenNr += Integer.parseInt(lChosen);
			}
			setRunTagStatus(aCaseComponent, lTag, AppConstants.statusKeyChosennumber, "" + lChosenNr, true);
			lCounter++;
		}
		return lRandomizedTags;
	}
	/**
	 *
	 * @param aAssessmentTag the assessment tag
	 */
	protected boolean checkIfItemsPrepaired(IXMLTag aAssessmentTag) {
		if (isAssessmentReset(aAssessmentTag))
			resetAssessment(aAssessmentTag);

		List<IXMLTag> lActualRefItemTags = assessmentsactiverefitemtags.get(getAssessmentId(aAssessmentTag));
		if ((lActualRefItemTags == null) || (lActualRefItemTags.size() == 0)) {
			List<IXMLTag> lRefItemTags = getAssessmentRefItemTags(aAssessmentTag);
			Hashtable<Integer, IXMLTag> lRandomRefItemTags = new Hashtable<Integer, IXMLTag>();
			String lAssId = getAssessmentId(aAssessmentTag);
			Hashtable<String, List<IXMLTag>> lClearITems = new Hashtable<String, List<IXMLTag>>();
			assessmentsrefitemalttags.put(lAssId, lClearITems);
			for (IXMLTag lTag : lRefItemTags) {
				//NOTE only present assessment items will be shown
				boolean lPresent = !CDesktopComponents.sSpring().getCurrentTagStatus(lTag, AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse);
				if (lPresent) {
					String lSeq = CDesktopComponents.sSpring().getCurrentTagStatus(lTag, AppConstants.statusKeySequencenumber);
					int lSeqNr = 0;
					if (!lSeq.equals("")) {
						lSeqNr = Integer.parseInt(lSeq);
					}
					if (lSeqNr > 0) {
						lRandomRefItemTags.put(lSeqNr, lTag);
						IXMLTag lItemTag = getItemTagData(lTag).getItemTag();
						List<IXMLTag> lRefAltTags = lItemTag.getChilds("alternative,field");
						//List<IXMLTag> lRandomRefAltTags = new ArrayList<IXMLTag>();
						Hashtable<Integer, IXMLTag> lRandomRefAltTags = new Hashtable<Integer, IXMLTag>();
						for (IXMLTag lAltTag : lRefAltTags) {
							String lAltSeq = CDesktopComponents.sSpring().getCurrentTagStatus(lAltTag, AppConstants.statusKeySequencenumber);
							int lAltSeqNr = 0;
							if (!lAltSeq.equals("")) {
								lAltSeqNr = Integer.parseInt(lAltSeq);
							}
							if (lAltSeqNr > 0)
								lRandomRefAltTags.put(lAltSeqNr, lAltTag);
						}
						if (lRandomRefAltTags.size() > 0) {
							List<IXMLTag> lRRATags = new ArrayList<IXMLTag>();
							lRandomRefAltTags.forEach((lKey, lValue)
									-> lRRATags.add(lValue));
							assessmentsrefitemalttags.get(lAssId).put(getRefItemId(lTag), lRRATags);
						}
					}
				}
			}
			if (lRandomRefItemTags.size() > 0) {
				List<IXMLTag> lRRITags = new ArrayList<IXMLTag>();
				lRandomRefItemTags.forEach((lKey, lValue)
						-> lRRITags.add(lValue));
				assessmentsactiverefitemtags.put(lAssId, lRRITags);
				return true;
			}
			return false;
		}
		return true;
	}

	/**
	 * Determines which items in the assessment will be presented in the run, and in which order.
	 * Also the list of answer alternatives may be randomized. 
	 *
	 * @param aAssessmentTag the assessment tag
	 */
	protected void prepairRefItemTags(IXMLTag aAssessmentTag) {
		if (checkIfItemsPrepaired(aAssessmentTag))
			return;
		int lWantedNumber = getAssessmentNumberOfItems(aAssessmentTag);
		List<IXMLTag> lRefItemTags = getAssessmentVisibleRefItemTags(aAssessmentTag);
		boolean lRandomizeItems = CDesktopComponents.sSpring().getCurrentTagStatus(aAssessmentTag,
				AppConstants.statusKeyRandomizeitems).equals(AppConstants.statusValueTrue);
		List<IXMLTag> lRandomizedRefItemTags = selectRandomizedTags(lRefItemTags, lWantedNumber, lRandomizeItems, caseComponent);
		String lAssId = getAssessmentId(aAssessmentTag);
		assessmentsactiverefitemtags.put(lAssId, lRandomizedRefItemTags);
		Hashtable<String, List<IXMLTag>> lClearITems = new Hashtable<String, List<IXMLTag>>();
		assessmentsrefitemalttags.put(lAssId, lClearITems);
		for (IXMLTag lRefTag : lRandomizedRefItemTags) {
			ItemTagData lItemTagData = getItemTagData(lRefTag);
			IXMLTag lItemTag = lItemTagData.getItemTag();
			IECaseComponent lCaseComponent = lItemTagData.getCaseComponent();
			List<IXMLTag> lRefAltTags = lItemTag.getChilds("alternative,field");
			lRandomizeItems = CDesktopComponents.sSpring().getCurrentTagStatus(lRefTag,
					AppConstants.statusKeyRandomizeitems).equals(AppConstants.statusValueTrue);
			List<IXMLTag> lRandomizedRefAltTags = selectRandomizedTags(lRefAltTags, lRefAltTags.size(), lRandomizeItems, lCaseComponent);
			assessmentsrefitemalttags.get(lAssId).put(getRefItemId(lRefTag), lRandomizedRefAltTags);
		}
	}

	protected List<List<Object>> getAssessmentDatas(IXMLTag aAssessmentTag) {
		List<List<Object>> lDataList = new ArrayList<List<Object>>();
		currentassessmentrefitemtags = getAssessmentActualRefItemTags(aAssessmentTag);
		int lItemNumber = 1;
		for (IXMLTag lRefItemTag : currentassessmentrefitemtags) {
			List<Object> data = new ArrayList<Object>();
			data.add(getRefItemId(lRefItemTag));
			data.add("" + isRefItemFinished(lRefItemTag));
			data.add(getRefItemTitle(lRefItemTag, lItemNumber));
			lDataList.add(data);
			lItemNumber ++;
		}

		return lDataList;
	}

	public String getRefItemId(IXMLTag aRefItemTag) {
		return getXMLTagId(aRefItemTag);
	}
	
	protected String getRefItemTitle(IXMLTag aRefItemTag, int aItemNumber) {
		return getItemTitle(getItemTagData(aRefItemTag).getItemTag(), aItemNumber);
	}
	
	public String getRefItemAnswer(IXMLTag aRefItemTag) {
		return CDesktopComponents.sSpring().getCurrentTagStatus(aRefItemTag, AppConstants.statusKeyAnswer);
	}
	
	protected String getRefItemFeedbackConditionId(IXMLTag aRefItemTag) {
		return CDesktopComponents.sSpring().getCurrentTagStatus(aRefItemTag, AppConstants.statusKeyFeedbackConditionId);
	}
	
	public boolean isRefItemFinished(IXMLTag aRefItemTag) {
		String lAnswer = getRefItemAnswer(aRefItemTag);
		return !lAnswer.equals(""); 
	}
	
	public List<IXMLTag> getRefItemAltTags(IXMLTag aAssessmentTag, IXMLTag aRefItemTag) {
		List<IXMLTag> lRefItemAltTags = new ArrayList<IXMLTag>();
		String lRefItemId = getRefItemId(aRefItemTag);
		String lAssId = getAssessmentId(aAssessmentTag);
		if (StringUtils.isEmpty(lRefItemId) || StringUtils.isEmpty(lAssId))
			return lRefItemAltTags;
		Hashtable<String, List<IXMLTag>> lAssRefItemAltTags = assessmentsrefitemalttags.get(lAssId);
		if ((lAssRefItemAltTags == null) || (!lAssRefItemAltTags.containsKey(lRefItemId))) {
			prepairRefItemTags(aAssessmentTag);
			lAssRefItemAltTags = assessmentsrefitemalttags.get(lAssId);
		}
		if ((lAssRefItemAltTags == null) || (!lAssRefItemAltTags.containsKey(lRefItemId)))
			return lRefItemAltTags;
		return lAssRefItemAltTags.get(lRefItemId);
	}
	
	public ItemTagData getItemTagData(IXMLTag aRefItemTag) {
		ItemTagData lItemTagData = new ItemTagData();
	//	get item from associated itemtool
		IXMLTag lRefChildTag = aRefItemTag.getChild("ref");
		String lReftype = lRefChildTag.getDefAttribute(AppConstants.defKeyReftype);
		IECaseRole lCaseRole = CDesktopComponents.sSpring().getSCaseRoleHelper().getCaseRole();
		List<String> lRefIds = getCaseHelper().getRefTagIds(lReftype, "" + AppConstants.statusKeySelectedIndex, lCaseRole, caseComponent, aRefItemTag);
	//	only one ref
		String lRefId = (String)lRefIds.get(0);
		if ((lRefId != null) && (!lRefId.equals(""))) {
			String[] lIdArr = lRefId.split(",");
			if (lIdArr.length == 3) {
				String lCarId = lIdArr[0];
				String lCacId = lIdArr[1];
				String lTagId = lIdArr[2];
				lItemTagData.setCaseRole(CDesktopComponents.sSpring().getSCaseRoleHelper().getCaseRole(Integer.parseInt(lCarId)));
				lItemTagData.setCaseComponent(CDesktopComponents.sSpring().getCaseComponent(Integer.parseInt(lCacId)));
				lItemTagData.setItemTag(CDesktopComponents.sSpring().getTag(lItemTagData.getCaseComponent(), lTagId));
			}
		}
		return lItemTagData;
	}

	/**
	 * Creates title area and shows name of case component within it.
	 * And adds close button
	 *
	 * @param aParent the ZK parent
	 *
	 * @return the c run area
	 */
	@Override
	protected CRunArea createTitleArea(Component aParent) {
		CRunHbox lHbox = new CRunHbox();
		aParent.appendChild(lHbox);
		CRunComponentDecoratorOunl decorator = createDecorator();
		CRunArea lTitleArea = decorator.createTitleArea(caseComponent, lHbox, getClassName());
		decorator.createCloseArea(caseComponent, lHbox, getClassName(), getId(), this);
		setAttribute("runTitleAreaHbox", lHbox);
		return lTitleArea;
	}

	/**
	 * Creates decorator.
	 *
	 * @return the decorator
	 */
	protected CRunComponentDecoratorOunl createDecorator() {
		return new CRunComponentDecoratorOunl();
	}

	/**
	 * Creates close button.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run button
	 */
	@Override
	protected CRunButton createCloseButton(Component aParent) {
		// NOTE Don't create close button
		return null;
	}

	/**
	 * Handles status change due to firing of script actions.
	 *
	 * @param aTriggeredReference the a triggered reference
	 */
	@Override
	public void handleStatusChange(STriggeredReference aTriggeredReference) {
		if (aTriggeredReference.getDataTag() == null) {
			return;
		}
		if (caseComponent.getCacId() != aTriggeredReference.getCaseComponent().getCacId()) {
			// only handle status change on own case component
			return;
		}
		boolean lResetAssessment = (aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyReset) && 
				aTriggeredReference.getStatusValue().equals(AppConstants.statusValueTrue));
		boolean lRerenderAssessment = aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyPresent) || 
				aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyFinished) || lResetAssessment;
		if (lRerenderAssessment) {
			// get tag out of buffered assessment data and setTagStatus
			String lTagId = getXMLTagId(aTriggeredReference.getDataTag());
			List<IXMLTag> lNodeTags = CDesktopComponents.cScript().getNodeTags(contenttag.getParentTag());
			IXMLTag lTag = null;
			for (IXMLTag lNodeTag : lNodeTags) {
				if (getXMLTagId(lNodeTag).equals(lTagId)) {
					lTag = lNodeTag;
				}
			}
			// NOTE setTagStatus already done for statusKeyFinished and statusKeyReset!
			if (!aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyFinished) && !lResetAssessment && (lTag != null)) {
				CDesktopComponents.sSpring().setRunTagStatusValue(CDesktopComponents.sSpring().getStatusTagOfDataTag(lTag), aTriggeredReference.getStatusKey(), aTriggeredReference.getStatusValue());
			}
			// NOTE if present of assessment is changed when using the assessments component, 'assessmenttags' property must be updated
			if (aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyPresent) && aTriggeredReference.getDataTag().getName().equals("assessment"))
				setAssessmentTags();
			boolean assessmentFinished = (aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyFinished) && aTriggeredReference.getDataTag().getName().equals("assessment"));
			if (assessmentFinished && lTag != null && lTag.getName().equals("assessment")) {
				saveAssessmentScore(lTag);
			}
			if (!lResetAssessment)
				if (assessmentFinished || aTriggeredReference.getDataTag().getName().equals("instruction") || aTriggeredReference.getDataTag().getName().equals("assessment")) {
					// rerender assessmentsoverview if visible
			   		String runAssessmentsDivId = "runAssessmentsDiv";
					Div lRunAssessmentsDiv = (Div)CDesktopComponents.vView().getComponent(runAssessmentsDivId);
					if (lRunAssessmentsDiv != null && lRunAssessmentsDiv.isVisible()) {
						updateAssessments();
					}
				}
			if (assessmentFinished || aTriggeredReference.getDataTag().getName().equals("refitem")) {
				// rerender assessment if visible
		   		String runAssessmentDivId = "runAssessmentDiv";
				Div lRunAssessmentDiv = (Div)CDesktopComponents.vView().getComponent(runAssessmentDivId);
				if (lRunAssessmentDiv != null && lRunAssessmentDiv.isVisible()) {
					updateAssessment();
				}
			}
			if (assessmentFinished) {
				// rerender assessmentitem if visible
		   		String runAssessmentItemDivId = "runAssessmentItemDiv";
				Div lRunAssessmentItemDiv = (Div)CDesktopComponents.vView().getComponent(runAssessmentItemDivId);
				if (lRunAssessmentItemDiv != null && lRunAssessmentItemDiv.isVisible()) {
					updateAssessmentItem();
				}
			}
			if ((lResetAssessment) && (lTag != null))
				//NOTE check on actual value of 'reset' state; could already be handled if assessment is present at session start
				if (isAssessmentReset(lTag))
					restartAssessment();
		}
	}

	/**
	 * Creates feedback button within item area.
	 *
	 * @param aParent the a parent
	 * @param aItem the a item
	 *
	 * @return the component
	 */
	@Override
	protected Component createShowFeedbackButton (Component aParent, IXMLTag aItem) {
		CRunArea lButton = createButton("","showFeedback",aItem,CDesktopComponents.vView().getLabel("run_assessments.button.show_feedback"),"Small","");
		lButton.registerObserver("runAssessments");
		aParent.appendChild(lButton);
		return lButton;
	}

	protected CRunArea createButton (String aId, String aEventAction,
			Object aEventActionStatus, String aLabel, String aZclassExtension,
			String aClientOnClickAction) {
		return new CRunLabelButtonOunl(aId, aEventAction, aEventActionStatus, aLabel, aZclassExtension, aClientOnClickAction);
	}

	/**
	 * Closes assessments.
	 */
	@Override
	public void close() {
		if (runWnd != null) {
			((CRunWndOunl)runWnd).onAction(getId(), "endComponent", this);
			((CRunWndOunl)runWnd).endApp(this);
		}
	}

	/**
	 * Handle assessment.
	 *
	 * @param aAssessmentTagId the assessment tag id
	 */
	@Override
	public void handleAssessment(String aAssessmentTagId) {
		IXMLTag lAssessmentTag = getAssessmentTag(aAssessmentTagId);
		if (lAssessmentTag == null) {
			return;
		}
		currentassessmenttag = lAssessmentTag;
		showFeedback = CDesktopComponents.sSpring().getCurrentRunTagStatus(caseComponent, currentassessmenttag, AppConstants.statusKeyShowfeedback, getRunStatusType()).equals(AppConstants.statusValueTrue);
		previewAssessment(lAssessmentTag);
		currentassessmentrefitemtags = getAssessmentActualRefItemTags(lAssessmentTag);
   		String runAssessmentItemDivId = "runAssessmentItemDiv";
		Div lRunAssessmentItemDiv = (Div)CDesktopComponents.vView().getComponent(runAssessmentItemDivId);
		if (lRunAssessmentItemDiv != null) {
			// assessment item div can still be visible if breadcrumb is used to jump from level 3 to 1
			lRunAssessmentItemDiv.setVisible(false);
		}
   		String runAssessmentDivId = "runAssessmentDiv";
		Div lRunAssessmentDiv = (Div)CDesktopComponents.vView().getComponent(runAssessmentDivId);
		if (lRunAssessmentDiv != null) {
			// update assessment
			renderAssessmentMacro(lAssessmentTag);
//			Events.postEvent("onEmUpdate", lRunAssessmentDiv, aAssessmentTagId);
			lRunAssessmentDiv.setVisible(true);
		}
	}

	/**
	 * Update assessment.
	 */
	@Override
	public void updateAssessment() {
		renderAssessmentMacro(currentassessmenttag);
	}

	protected void renderAssessmentMacro(IXMLTag aAssessmentTag) {
		String macroAssessmentId = "macroAssessment";
		HtmlMacroComponent macroAssessment = (HtmlMacroComponent)CDesktopComponents.vView().getComponent(macroAssessmentId);
		macroAssessment.setDynamicProperty("a_assessment_title", getAssessmentTitle(aAssessmentTag));
		macroAssessment.setDynamicProperty("a_assessment_instruction", getAssessmentInstruction(aAssessmentTag));
		boolean isAutostartAssessment = isAutostartAssessment(aAssessmentTag) && !isAssessmentEverFinished(aAssessmentTag);
		boolean lCanBeStarted = !isAutostartAssessment && !isAssessmentStarted(aAssessmentTag) && !isAssessmentFinished(aAssessmentTag) && isAssessmentActive(aAssessmentTag);
		macroAssessment.setDynamicProperty("a_assessment_can_be_started", "" + lCanBeStarted);
		boolean lCanBeRestarted = !isAutostartAssessment && isAssessmentFinished(aAssessmentTag) && hasAssessmentSit(aAssessmentTag) && !isAssessmentEditable(aAssessmentTag) && isAssessmentActive(aAssessmentTag);
		macroAssessment.setDynamicProperty("a_assessment_can_be_restarted", "" + lCanBeRestarted);
		boolean lCanBeResumed = !isAutostartAssessment && hasAssessmentSit(aAssessmentTag) && isAssessmentEditable(aAssessmentTag);
		macroAssessment.setDynamicProperty("a_assessment_can_be_resumed", "" + lCanBeResumed);
		boolean lReadOnlyItems = !isAutostartAssessment && isAssessmentFinished(aAssessmentTag) && !isAssessmentEditable(aAssessmentTag);
		boolean lCanBeInspected = lReadOnlyItems && !CDesktopComponents.sSpring().getCurrentRunTagStatus(caseComponent, aAssessmentTag, AppConstants.statusKeyHideinspection, getRunStatusType()).equals(AppConstants.statusValueTrue);
		macroAssessment.setDynamicProperty("a_assessment_can_be_inspected", "" + lCanBeInspected);
		boolean lShowItemButtons = !lReadOnlyItems || lCanBeInspected;
		macroAssessment.setDynamicProperty("a_assessment_show_item_buttons", "" + lShowItemButtons);
		boolean lCanBeFinished = lCanBeResumed;
		macroAssessment.setDynamicProperty("a_assessment_can_be_finished", "" + lCanBeFinished);
		macroAssessment.setDynamicProperty("a_assessment_show_score", "" + isAssessmentScoreVisible(aAssessmentTag));
		macroAssessment.setDynamicProperty("a_assessment_score", "" + getAssessmentScore(aAssessmentTag));
		IXMLTag lFeedbackTag = getAssessmentFeedbackCondition(aAssessmentTag);
		macroAssessment.setDynamicProperty("a_assessment_assfeedback_text", getAssessmentFeedback(lFeedbackTag));
		macroAssessment.setDynamicProperty("a_assessment_assfeedback_pieces", getPiecesDatas(null, null, lFeedbackTag));
		macroAssessment.setDynamicProperty("a_assessment_showfeedback", "" + isAssessmentFeedbackVisible(aAssessmentTag));
		macroAssessment.setDynamicProperty("a_assessment_datas", getAssessmentDatas(aAssessmentTag));
		macroAssessment.setDynamicProperty("a_assessment_multiple_assessments", (getAssessmentsDatas().size() > 1));
		macroAssessment.recreate();
	}
	
	/**
	 * Gets assessment tag.
	 *
	 * @param aAssessmentTagId the a assessment tag id
	 *
	 * @return the xml tag
	 */
	public IXMLTag getAssessmentTag(String aAssessmentTagId) {
		if (aAssessmentTagId == null) {
			return null;
		}
		if (assessmenttags.size() == 0) {
			return null;
		}
		for (IXMLTag lAssessmentTag : assessmenttags) {
			if (getXMLTagId(lAssessmentTag).equals(aAssessmentTagId)) {
				return lAssessmentTag;
			}
		}
		return null;
	}

	/**
	 * Gets first ref item tag.
	 *
	 * @return the xml tag
	 */
	protected IXMLTag getFirstRefItemTag() {
		if (currentassessmentrefitemtags.size() == 0) {
			return null;
		}
		return currentassessmentrefitemtags.get(0);
	}

	/**
	 * Gets previous ref item tag.
	 *
	 * @return the xml tag
	 */
	protected IXMLTag getPreviousRefItemTag() {
		IXMLTag lPreviousRefItemTag = null;
		for (IXMLTag lRefItemTag : currentassessmentrefitemtags) {
			if (lRefItemTag == currentassessmentrefitemtag) {
				return lPreviousRefItemTag;
			}
			lPreviousRefItemTag = lRefItemTag; 
		}
		return lPreviousRefItemTag;
	}

	/**
	 * Gets next ref item tag.
	 *
	 * @return the xml tag
	 */
	protected IXMLTag getNextRefItemTag() {
		IXMLTag lPreviousRefItemTag = null;
		for (IXMLTag lRefItemTag : currentassessmentrefitemtags) {
			if (lPreviousRefItemTag == currentassessmentrefitemtag) {
				return lRefItemTag;
			}
			lPreviousRefItemTag = lRefItemTag; 
		}
		return null;
	}

	/**
	 * Gets ref item tag.
	 *
	 * @param aRefItemTagId the a ref item tag id
	 *
	 * @return the xml tag
	 */
	protected IXMLTag getRefItemTag(String aRefItemTagId) {
		if (aRefItemTagId == null) {
			return null;
		}
		if (currentassessmentrefitemtags.size() == 0) {
			return null;
		}
		for (IXMLTag lRefItemTag : currentassessmentrefitemtags) {
			if (getXMLTagId(lRefItemTag).equals(aRefItemTagId)) {
				return lRefItemTag;
			}
		}
		return null;
	}

	/**
	 * Gets ref item tag number.
	 *
	 * @param aRefItemTag the a ref item tagd
	 *
	 * @return the number
	 */
	protected int getRefItemNumber(IXMLTag aRefItemTag) {
		if (aRefItemTag == null) {
			return -1;
		}
		if (currentassessmentrefitemtags.size() == 0) {
			return -1;
		}
		int lIndex = currentassessmentrefitemtags.indexOf(aRefItemTag);
		if (lIndex >= 0) {
			lIndex++;
		}
		return lIndex;
	}

	/**
	 * Start assessment.
	 */
	@Override
	public void startAssessment() {
		// set started to true
		setAssessmentStarted(currentassessmenttag, AppConstants.statusValueTrue);
		// open item panel with first item, editable
		renderFirstAssessmentItem(true);
	}

	/**
	 * Reset assessment.
	 */
	protected void resetAssessment(IXMLTag aAssessmentTag) {
		setAssessmentReset(aAssessmentTag, AppConstants.statusValueFalse);
		if (isAssessmentActive (aAssessmentTag)) {
			if (isAssessmentEditable(aAssessmentTag)) {
				saveAssessmentScore(aAssessmentTag);
				setAssessmentStarted(aAssessmentTag, AppConstants.statusValueFalse);
			} else
				// set finished to false
				setAssessmentFinished(aAssessmentTag, AppConstants.statusValueFalse);
			resetRefItemTags(aAssessmentTag);
		}
	}

	/**
	 * Restart assessment.
	 */
	@Override
	public void restartAssessment() {
		resetAssessment(currentassessmenttag);
		prepairRefItemTags(currentassessmenttag);
		// set started to true
		setAssessmentStarted(currentassessmenttag, AppConstants.statusValueTrue);
		currentassessmentrefitemtags = getAssessmentActualRefItemTags(currentassessmenttag);
		// open item panel with first item, editable
		renderFirstAssessmentItem(true);
	}

	/**
	 * Render assessment items.
	 *
	 * @param aEditable the a editable
	 */
	protected void renderFirstAssessmentItem(boolean aEditable) {
		// open item panel with first item, editable
		IXMLTag lRefItemTag = getFirstRefItemTag();
		if (lRefItemTag == null) {
			return;
		}
		handleAssessmentItem(getRefItemId(lRefItemTag));
	}

	/**
	 * Render assessment.
	 *
	 * @param aAssessmentRefItemTag the a assessment ref item tag
	 * @param aEditable the a editable
	 */
	protected void renderAssessmentItem(IXMLTag aAssessmentRefItemTag, boolean aEditable) {
   		String runAssessmentItemDivId = "runAssessmentItemDiv";
		Div lRunAssessmentItemDiv = (Div)CDesktopComponents.vView().getComponent(runAssessmentItemDivId);
		if (lRunAssessmentItemDiv != null) {
			// update assessment item
			renderAssessmentRefItemMacro(aAssessmentRefItemTag, aEditable);
			lRunAssessmentItemDiv.setVisible(true);
		}
	}

	/**
	 * Resume assessment.
	 */
	@Override
	public void resumeAssessment() {
		// open item panel with first item, editable
		renderFirstAssessmentItem(true);
	}

	/**
	 * Inspect assessment.
	 */
	@Override
	public void inspectAssessment() {
		// open item panel with first item, not editable
		renderFirstAssessmentItem(false);
	}

	/**
	 * Finish assessment.
	 */
	@Override
	public void finishAssessment() {
		// save assessment score
		saveAssessmentScore(currentassessmenttag);
		setAssessmentStarted(currentassessmenttag, AppConstants.statusValueFalse);
		// set finished to true
		setAssessmentFinished(currentassessmenttag, AppConstants.statusValueTrue);
		// rerender current panel to show score
		updateAssessment();
	}

	protected void saveAssessmentScore(IXMLTag aAssessmentTag) {
//		determine assessment score and save it
		List<IXMLTag> lCurrentAssessmentRefitemTags = null;
		if (getXMLTagId(aAssessmentTag).equals(getXMLTagId(currentassessmenttag))) {
			lCurrentAssessmentRefitemTags = currentassessmentrefitemtags;
		}
		else {
			//lCurrentAssessmentRefitemTags = getAssessmentRefItemTags(aAssessmentTag);
			lCurrentAssessmentRefitemTags = getAssessmentActualRefItemTags(aAssessmentTag);
		}
		int lScore = 0;
		for (IXMLTag lRefItemTag : lCurrentAssessmentRefitemTags) {
			IXMLTag lFeedbackConditionTag = getFeedbackCondition(lRefItemTag, getItemTagData(lRefItemTag).getItemTag());
			String lFeedbackItemScore = "";
			if (lFeedbackConditionTag != null) {
				lFeedbackItemScore = lFeedbackConditionTag.getChildValue("score");
			}
			// TODO What to do if score is other than saved in ref item. This can be the case if a feedback condition
			// belonging to an item checks values of alternatives of other items. The feedback condition only is saved
			// in ref item if an alternative of the same item is chosen.
/*			String lItemScore = CDesktopComponents.sSpring().getCurrentTagStatus(lRefItemTag,AppConstants.statusKeyScore);
			if (!lItemScore.equals(lFeedbackItemScore)) {
			} */
			String lItemWeighting = lRefItemTag.getChildValue("weighting");
			if ((!lFeedbackItemScore.equals("")) && (!lItemWeighting.equals(""))) {
				lScore += Integer.parseInt(lFeedbackItemScore)*Integer.parseInt(lItemWeighting);
			}
		}
		setAssessmentScore(aAssessmentTag, lScore);
	}

	/**
	 * Prepair assessment item.
	 *
	 * @param aAssessmentTagId the ref item tag id
	 */
	@Override
	public void prepairAssessmentItem(String aAssessmentTagId) {
		IXMLTag lAssessmentTag = getAssessmentTag(aAssessmentTagId);
		if (lAssessmentTag == null) {
			return;
		}
		currentassessmenttag = lAssessmentTag;
		//currentassessmentrefitemtags = getAssessmentVisibleRefItemTags(lAssessmentTag);
		previewAssessment(lAssessmentTag);
		currentassessmentrefitemtags = getAssessmentActualRefItemTags(lAssessmentTag);
   		String runAssessmentItemDivId = "runAssessmentItemDiv";
		Div lRunAssessmentItemDiv = (Div)CDesktopComponents.vView().getComponent(runAssessmentItemDivId);
		if (lRunAssessmentItemDiv != null) {
			// assessment item div can still be visible if breadcrumb is used to jump from level 3 to 1
			lRunAssessmentItemDiv.setVisible(false);
		}
   		String runAssessmentDivId = "runAssessmentDiv";
		Div lRunAssessmentDiv = (Div)CDesktopComponents.vView().getComponent(runAssessmentDivId);
		if (lRunAssessmentDiv != null) {
			lRunAssessmentDiv.setVisible(false);
		}
	}
	
	/**
	 * Handle assessment item.
	 *
	 * @param aRefItemTagId the ref item tag id
	 */
	@Override
	public void handleAssessmentItem(String aRefItemTagId) {
		IXMLTag lRefItemTag = getRefItemTag(aRefItemTagId);
		if (lRefItemTag == null) {
			return;
		}
		currentassessmentrefitemtag = lRefItemTag; 
		// set started to true
		if (!isAssessmentStarted(currentassessmenttag) && !isAssessmentFinished(currentassessmenttag)) {
			setAssessmentStarted(currentassessmenttag, AppConstants.statusValueTrue);
		}
		showFeedback = CDesktopComponents.sSpring().getCurrentRunTagStatus(caseComponent, currentassessmenttag, AppConstants.statusKeyShowfeedback, getRunStatusType()).equals(AppConstants.statusValueTrue);
		if (isAssessmentFinished(currentassessmenttag))
			showFeedback = showFeedback || CDesktopComponents.sSpring().getCurrentRunTagStatus(caseComponent, currentassessmenttag, AppConstants.statusKeyAlwaysinspectfeedback, getRunStatusType()).equals(AppConstants.statusValueTrue);
		boolean lEditable = isAssessmentEditable(currentassessmenttag);
		renderAssessmentItem(lRefItemTag, lEditable);
		
		IXMLTag lItemTag = getItemTagData(lRefItemTag).getItemTag();
		if (lItemTag == null) {
			return;
		}
		if (showFeedback && itemHasDirectFeedback(lItemTag)) {
			String lFeedbackConditionId = getRefItemFeedbackConditionId(currentassessmentrefitemtag);
			if (!lFeedbackConditionId.equals("")) {
				IXMLTag lFeedbackConditionTag = getFeedbackCondition(currentassessmentrefitemtag, lItemTag);
				renderFeedback(lFeedbackConditionTag, false);
			}
		}
	}
	
	/**
	 * Updatet assessment item.
	 */
	public void updateAssessmentItem() {
		IXMLTag lRefItemTag = currentassessmentrefitemtag;
		if (lRefItemTag != null) {
			handleAssessmentItem(getXMLTagId(lRefItemTag));
		}
	}

	/**
	 * Previous assessment item.
	 */
	@Override
	public void handlePreviousAssessmentItem() {
		IXMLTag lRefItemTag = getPreviousRefItemTag();
		if (lRefItemTag != null) {
			handleAssessmentItem(getXMLTagId(lRefItemTag));
		}
	}

	/**
	 * Next assessment item.
	 */
	@Override
	public void handleNextAssessmentItem() {
		IXMLTag lRefItemTag = getNextRefItemTag();
		if (lRefItemTag != null) {
			handleAssessmentItem(getXMLTagId(lRefItemTag));
		}
	}

	protected void renderAssessmentRefItemMacro(IXMLTag aAssessmentRefItemTag, boolean aEditable) {
		ItemTagData lItemTagData = getItemTagData(aAssessmentRefItemTag);
		IXMLTag lItemTag = lItemTagData.getItemTag();
		if (lItemTag == null) {
			return;
		}
		IECaseRole lItemTagCaseRole = lItemTagData.getCaseRole();
		IECaseComponent lItemTagCaseComponent = lItemTagData.getCaseComponent();
		String macroAssessmentItemId = "macroAssessmentItem";
		HtmlMacroComponent macroAssessment = (HtmlMacroComponent)CDesktopComponents.vView().getComponent(macroAssessmentItemId);
		int lItemNumber = getRefItemNumber(aAssessmentRefItemTag);
		String lAnswer = getRefItemAnswer(aAssessmentRefItemTag);
		macroAssessment.setDynamicProperty("a_assessment_title", getAssessmentTitle(currentassessmenttag));
		macroAssessment.setDynamicProperty("a_assessmentitem_title", getItemTitle(lItemTag, lItemNumber));
		macroAssessment.setDynamicProperty("a_assessmentitem_instruction", getItemInstruction(lItemTag));
		macroAssessment.setDynamicProperty("a_assessmentitem_pieces", getPiecesDatas(lItemTagCaseRole, lItemTagCaseComponent, lItemTag));
		String lItemType = getItemType(lItemTag);
		macroAssessment.setDynamicProperty("a_assessmentitem_directfeedback", "" + itemHasDirectFeedback(lItemTag));
		macroAssessment.setDynamicProperty("a_assessmentitem_type", lItemType);
		macroAssessment.setDynamicProperty("a_assessmentitem_alt_orientation", getAltOrientation(lItemTag));
		macroAssessment.setDynamicProperty("a_assessmentitem_has_fields", "" + (getPresentChildTags(lItemTag, "field").size() > 0));
		macroAssessment.setDynamicProperty("a_assessmentitems_editable", "" + aEditable);
		macroAssessment.setDynamicProperty("a_current_assessmentitem_number", lItemNumber);
		macroAssessment.setDynamicProperty("a_number_of_assessmentitems", currentassessmentrefitemtags.size());
		macroAssessment.setDynamicProperty("a_assessmentitem_datas", getItemDatas(lItemTagCaseRole, lItemTagCaseComponent, aAssessmentRefItemTag, lItemTag, lItemType, lAnswer));
		//NOTE always show 'finish assessment' button at last question, when editable and user may finish assessment:
		macroAssessment.setDynamicProperty("a_assessment_can_be_finished", aEditable && mayUserFinishAssessment(currentassessmenttag));
		macroAssessment.recreate();

		renderFeedback(getFeedbackCondition(aAssessmentRefItemTag, lItemTag), false);
	}
	
	protected String getItemId(IXMLTag aItemTag) {
		return getXMLTagId(aItemTag);
	}
	
	protected String getItemTitle(IXMLTag aItemTag, int aItemNumber) {
		String lTitle = "";
		if (aItemTag != null) {
			lTitle = CDesktopComponents.sSpring().unescapeXML(aItemTag.getChildValue("shorttitle"));
			if (lTitle.equals("")) {
				lTitle = CContentHelper.getNodeTagLabel(caseComponent.getEComponent().getCode(), "question");
				if (aItemNumber > 0) {
					lTitle += " " + aItemNumber;
				}
			}
		}
		return lTitle;
	}
	
	public String getItemInstruction(IXMLTag aItemTag) {
		String lRichtext = CDesktopComponents.sSpring().unescapeXML(aItemTag.getChildValue("richtext").trim());
		return getRunComponentHelper().stripCKeditorString(lRichtext);
	}
	
	public String getItemType(IXMLTag aItemTag) {
		return aItemTag.getChildValue("type");
	}
	
	public String getAltOrientation(IXMLTag aItemTag) {
		return aItemTag.getChildValue("altorientation");
	}
	
	protected List<List<Object>> getItemDatas(IECaseRole aItemTagCaseRole, IECaseComponent aItemTagCaseComponent, IXMLTag aRefItemTag, IXMLTag aItemTag, String aItemType, String aAnswer) {
		List<List<Object>> lDataList = new ArrayList<List<Object>>();
		List<IXMLTag> lItemAltTags = getRefItemAltTags(currentassessmenttag, aRefItemTag);
		for (IXMLTag lChildTag : lItemAltTags) {
			List<Object> data = new ArrayList<Object>();
			data.add(getXMLTagId(lChildTag));
			data.add(lChildTag.getName());
			if (lChildTag.getName().equals("alternative")) {
				//alternative text
				data.add(getAlternativeText(lChildTag));
				//number of lines, not relevant
				data.add("-1");
				//default answer
				data.add(AppConstants.statusValueFalse);
				//answer
				data.add("" + isAlternativeChecked(lChildTag, aItemType, aAnswer));
			}
			else if (lChildTag.getName().equals("field")) {
				//header
				data.add(getAlternativeText(lChildTag));
				//number of lines
				data.add(getFieldNumberOfLines(lChildTag));
				//default answer
				data.add(getFieldText(lChildTag));
				//answer
				data.add(CDesktopComponents.sSpring().unescapeXMLAlt(getFieldAnswer(aRefItemTag, getXMLTagId(lChildTag)).replace(AppConstants.statusCrLfReplace, "\n")));
			}
			data.add(getPiecesDatas(aItemTagCaseRole, aItemTagCaseComponent, lChildTag));
			lDataList.add(data);
		}
		return lDataList;
	}

	protected String getAlternativeId(IXMLTag aAlternativeTag) {
		return getXMLTagId(aAlternativeTag);
	}
	
	public boolean isAlternativeChecked(IXMLTag aAlternativeTag, String aItemType, String aAnswer) {
		if (aItemType.equals("mkea")) {
			return getAlternativeId(aAlternativeTag).equals(aAnswer);
		}
		else if (aItemType.equals("mkma")) {
			return (AppConstants.statusValueSeparator + aAnswer + AppConstants.statusValueSeparator).indexOf(AppConstants.statusValueSeparator + getAlternativeId(aAlternativeTag) + AppConstants.statusValueSeparator) >= 0;

		}
		return false;
	}
	
	public String getAlternativeText(IXMLTag aAlternativeTag) {
		if (aAlternativeTag != null) {
			String lRichtext = CDesktopComponents.sSpring().unescapeXML(aAlternativeTag.getChildValue("richtext").trim());
			return getRunComponentHelper().stripCKeditorString(lRichtext);
		}
		return "";
	}
	
	public String getFieldNumberOfLines(IXMLTag aFieldTag) {
		if (aFieldTag != null) {
			return CDesktopComponents.sSpring().unescapeXML(aFieldTag.getChildValue("numberoflines").trim());
			}
		return "";
	}
	
	public String getFieldText(IXMLTag aFieldTag) {
		if (aFieldTag != null) {
			return CDesktopComponents.sSpring().unescapeXML(aFieldTag.getChildValue("text").trim());
			}
		return "";
	}
	
	public String getFieldAnswer(IXMLTag aRefItemTag, String aFieldTagId) {
		IXMLTag lStatusStatusTag = getStatusStatusTag(aRefItemTag);
		if (lStatusStatusTag == null) {
			return "";
		}
		IXMLTag lStatusStatusChildTag = getStatusStatusChildTag(lStatusStatusTag, "reffield", aFieldTagId);
		if (lStatusStatusChildTag != null) {
			return CDesktopComponents.sSpring().getCurrentTagStatus(lStatusStatusChildTag, AppConstants.statusKeyAnswer);
		}
		return "";
	}
	
	public List<List<Object>> getPiecesDatas(IECaseRole aTagCaseRole, IECaseComponent aTagCaseComponent, IXMLTag aTag) {
		List<List<Object>> lDataList = new ArrayList<List<Object>>();
		if (aTag == null) {
			return lDataList;
		}
		List<IXMLTag> lPieceTags = aTag.getChilds("piece");
		for (IXMLTag lPieceTag : lPieceTags) {
			List<Object> data = new ArrayList<Object>();
			data.add(CDesktopComponents.sSpring().unescapeXML(lPieceTag.getChildValue("name")));
			data.add(getPieceUrl(lPieceTag));
			lDataList.add(data);
		}
		List<IXMLTag> lRefPieceTags = aTag.getChilds("refpiece");
		for (IXMLTag lRefPieceTag : lRefPieceTags) {
			String lReftype = lRefPieceTag.getChild("ref").getDefTag().getAttribute(AppConstants.defKeyReftype);
			List<IXMLTag> lRefTags = new ArrayList<IXMLTag>();
			lRefTags = caseHelper.getRefTags(lReftype, "" + AppConstants.statusKeySelectedIndex, aTagCaseRole, aTagCaseComponent, lRefPieceTag);
//			only one ref
			if (lRefTags.size() == 1) {
				List<Object> data = new ArrayList<Object>();
				data.add(CDesktopComponents.sSpring().unescapeXML(lRefPieceTag.getChildValue("name")));
				data.add(getPieceUrl(lRefTags.get(0)));
				lDataList.add(data);
			}
		}
		return lDataList;
	}

	public String getPieceUrl(IXMLTag aPieceTag) {
		String lUrl = CDesktopComponents.sSpring().getSBlobHelper().convertHrefForCertainMediaTypes(CDesktopComponents.sSpring().getSBlobHelper().getUrl(aPieceTag));
		if (!CDesktopComponents.vView().isAbsoluteUrl(lUrl)) {
			if (!lUrl.equals("") && lUrl.indexOf("/") != 0) {
				lUrl = "/" + lUrl;
			}
			lUrl = CDesktopComponents.vView().getEmergoWebappsRoot() + lUrl;
		}
		return lUrl;
	}

	/**
	 * Handle assessment item alt.
	 *
	 * @param aAssessmentItemAltTagId the assessment item alt tag id
	 * @param aChecked the a checked
	 */
	@Override
	public void handleAssessmentItemAlt(String aAssessmentItemAltTagId, boolean aChecked) {
		ItemTagData lItemTagData = getItemTagData(currentassessmentrefitemtag);
		IXMLTag lItemTag = lItemTagData.getItemTag();
		if (lItemTag == null) {
			return;
		}
		IECaseComponent lItemsCaseComponent = lItemTagData.getCaseComponent();
		if (lItemsCaseComponent == null) {
			return;
		}
		IXMLTag lAltTag = getItemChildTag(lItemTag, aAssessmentItemAltTagId);
		if (lAltTag == null) {
			return;
		}
		if (getItemType(lItemTag).equals("mkea")) {
			handleAssessmentItemAltRadio(lItemsCaseComponent, lItemTag, aAssessmentItemAltTagId, aChecked);
		}
		else if (getItemType(lItemTag).equals("mkma")) {
			handleAssessmentItemAltCheck(lItemsCaseComponent, lItemTag, aAssessmentItemAltTagId, aChecked);
		}
	}

	/**
	 * Handle assessment item field.
	 *
	 * @param aAssessmentItemFieldTagId the assessment item field tag id
	 * @param aText the a text
	 */
	@Override
	public void handleAssessmentItemField(String aAssessmentItemFieldTagId, String aText) {
		ItemTagData lItemTagData = getItemTagData(currentassessmentrefitemtag);
		IXMLTag lItemTag = lItemTagData.getItemTag();
		if (lItemTag == null) {
			return;
		}
		IECaseComponent lItemsCaseComponent = lItemTagData.getCaseComponent();
		if (lItemsCaseComponent == null) {
			return;
		}
		IXMLTag lItemChildTag = getItemChildTag(lItemTag, aAssessmentItemFieldTagId);
		if (lItemChildTag == null) {
			return;
		}
		handleAssessmentItemField(lItemsCaseComponent, lItemTag, aAssessmentItemFieldTagId, aText);
	}

	/**
	 * Gets item child tag.
	 *
	 * @param aTagId the a tag id
	 *
	 * @return the xml tag
	 */
	protected IXMLTag getItemChildTag(IXMLTag aItemTag, String aTagId) {
		if (aItemTag == null || aTagId == null) {
			return null;
		}
		for (IXMLTag lChildTag : aItemTag.getChildTags()) {
			if (getXMLTagId(lChildTag).equals(aTagId)) {
				return lChildTag;
			}
		}
		return null;
	}

	protected void handleAssessmentItemAltRadio(IECaseComponent aItemsCaseComponent, IXMLTag aItemTag, String aAlternativeId, boolean aChecked) {
		if (currentassessmentrefitemtag == null)
			return;
		// overwrite previous answer.
		if (!aChecked) {
			// no alternative chosen
			aAlternativeId = "";
		}
		// update refitem status child tags
		String lPreviousAlternativeId = CDesktopComponents.sSpring().getCurrentTagStatus(currentassessmentrefitemtag, AppConstants.statusKeyAnswer);
		addOrUpdateRefStatusChildTags(currentassessmentrefitemtag, aAlternativeId, "refalternative", AppConstants.statusKeyOpened, "" + aChecked);
		if (aChecked) {
			addOrUpdateRefStatusChildTags(currentassessmentrefitemtag, lPreviousAlternativeId, "refalternative", AppConstants.statusKeyOpened, "" + !aChecked);
		}

		// update refitem answer
		if (!aAlternativeId.equals("") && !lPreviousAlternativeId.equals("")) {
			//NOTE first clear answer if previous answer is noth empty and other answer is chosen
			setRunTagStatus(caseComponent, currentassessmentrefitemtag, AppConstants.statusKeyAnswer, "", true);
		}
		setRunTagStatus(caseComponent, currentassessmentrefitemtag, AppConstants.statusKeyAnswer, aAlternativeId, true);

		// update item
		setRunTagStatus(aItemsCaseComponent, getItemChildTag(aItemTag, aAlternativeId), AppConstants.statusKeyOpened,  "" + aChecked, true);
		if (aChecked) {
			setRunTagStatus(aItemsCaseComponent, getItemChildTag(aItemTag, lPreviousAlternativeId), AppConstants.statusKeyOpened, "" + !aChecked, true);
		}

		handleFeedbackAndScore(aItemsCaseComponent, aItemTag);
	}

	public void handleAssessmentItemAltRadio(IXMLTag aRefItemTag, IXMLTag aItemTag, String aAlternativeId, boolean aChecked) {
		ItemTagData lItemTagData = getItemTagData(aRefItemTag);
		IECaseComponent lItemsCaseComponent = lItemTagData.getCaseComponent();
		// overwrite previous answer.
		if (!aChecked) {
			// no alternative chosen
			aAlternativeId = "";
		}
		// update refitem status child tags
		String lPreviousAlternativeId = CDesktopComponents.sSpring().getCurrentTagStatus(aRefItemTag, AppConstants.statusKeyAnswer);
		addOrUpdateRefStatusChildTags(aRefItemTag, aAlternativeId, "refalternative", AppConstants.statusKeyOpened, "" + aChecked);
		if (aChecked) {
			addOrUpdateRefStatusChildTags(aRefItemTag, lPreviousAlternativeId, "refalternative", AppConstants.statusKeyOpened, "" + !aChecked);
		}

		// update refitem answer
		if (!aAlternativeId.equals("") && !lPreviousAlternativeId.equals("")) {
			//NOTE first clear answer if previous answer is noth empty and other answer is chosen
			setRunTagStatus(caseComponent, aRefItemTag, AppConstants.statusKeyAnswer, "", true);
		}
		setRunTagStatus(caseComponent, aRefItemTag, AppConstants.statusKeyAnswer, aAlternativeId, true);

		// update item
		setRunTagStatus(lItemsCaseComponent, getItemChildTag(aItemTag, aAlternativeId), AppConstants.statusKeyOpened,  "" + aChecked, true);
		if (aChecked) {
			setRunTagStatus(lItemsCaseComponent, getItemChildTag(aItemTag, lPreviousAlternativeId), AppConstants.statusKeyOpened, "" + !aChecked, true);
		}
	}

	protected void handleAssessmentItemAltCheck(IECaseComponent aItemsCaseComponent, IXMLTag aItemTag, String aAlternativeId, boolean aChecked) {
		if (currentassessmentrefitemtag == null)
			return;
		// update refitem status child tags
		addOrUpdateRefStatusChildTags(currentassessmentrefitemtag, aAlternativeId, "refalternative", AppConstants.statusKeyOpened, "" + aChecked);

		// update refitem answer
		String lAlternativeIds = CDesktopComponents.sSpring().getCurrentTagStatus(currentassessmentrefitemtag, AppConstants.statusKeyAnswer);
		if (aChecked) {
			lAlternativeIds = addStringItemIfPossible(lAlternativeIds, aAlternativeId);
		}
		else {
			lAlternativeIds = removeStringItemIfPossible(lAlternativeIds, aAlternativeId);
		}
		setRunTagStatus(caseComponent, currentassessmentrefitemtag, AppConstants.statusKeyAnswer, lAlternativeIds, true);

		// update item
		setRunTagStatus(aItemsCaseComponent, getItemChildTag(aItemTag, aAlternativeId), AppConstants.statusKeyOpened,  "" + aChecked, true);

		handleFeedbackAndScore(aItemsCaseComponent, aItemTag);
	}

	protected void handleAssessmentItemField(IECaseComponent aItemsCaseComponent, IXMLTag aItemTag, String aFieldId, String aText) {
		handleAssessmentItemField(aItemsCaseComponent, currentassessmentrefitemtag, aItemTag, aFieldId, aText, true);
	}

	public void handleAssessmentItemField(IXMLTag aRefItemTag, IXMLTag aItemTag, String aFieldId, String aText, boolean aHandleFeedback) {
		ItemTagData lItemTagData = getItemTagData(aRefItemTag);
		IECaseComponent lItemsCaseComponent = lItemTagData.getCaseComponent();
		handleAssessmentItemField(lItemsCaseComponent, aRefItemTag, aItemTag, aFieldId, aText, aHandleFeedback);
	}

	protected void handleAssessmentItemField(IECaseComponent aItemsCaseComponent, IXMLTag aRefItemTag, IXMLTag aItemTag, String aFieldId, String aText, boolean aHandleFeedback) {
		if (aRefItemTag == null)
			return;
		//NOTE because the text is saved as an attribute value \n has to be replaced by a string constant.
		//Otherwise the text is not reproduced correctly, crlf's are then replaced by spaces.
		aText = aText.replace("\n", AppConstants.statusCrLfReplace);
		aText = CDesktopComponents.sSpring().escapeXMLAlt(aText);

		
		// update refitem status child tags
		addOrUpdateRefStatusChildTags(aRefItemTag, aFieldId, "reffield", AppConstants.statusKeyAnswer, aText);

		// update refitem answer
		String lFieldIds = CDesktopComponents.sSpring().getCurrentTagStatus(aRefItemTag, AppConstants.statusKeyAnswer);
		//NOTE only add field id, do not remove it like for radio or checkbox question, because an answer is always given and not checked out
		lFieldIds = addStringItemIfPossible(lFieldIds, aFieldId);
		setRunTagStatus(caseComponent, aRefItemTag, AppConstants.statusKeyAnswer, lFieldIds, true);

		// update item
		setRunTagStatus(aItemsCaseComponent, getItemChildTag(aItemTag, aFieldId), AppConstants.statusKeyAnswer,  aText, true);

		if (aHandleFeedback)
			handleFeedbackAndScore(aItemsCaseComponent, aItemTag);
	}

	protected boolean addOrUpdateRefStatusChildTags(IXMLTag aRefItemTag, String aNodeChildTagId, String aNodeChildTagName, String aStatusKey, String aStatusValue) {
		if (aRefItemTag == null || StringUtils.isEmpty(aNodeChildTagId) || StringUtils.isEmpty(aStatusKey) || aStatusValue == null)
			return false;
		IXMLTag lStatusStatusTag = getStatusStatusTag(aRefItemTag);
		if (lStatusStatusTag == null) {
			return false;
		}
		IXMLTag lStatusStatusChildTag = getStatusStatusChildTag(lStatusStatusTag, aNodeChildTagName, aNodeChildTagId);
		if (lStatusStatusChildTag != null) {
			CDesktopComponents.sSpring().setRunTagStatusValue(lStatusStatusChildTag, aStatusKey, aStatusValue);
			return true;
		}
		return false;
	}

	protected boolean updateAllExistingRefStatusChildTags(IXMLTag aRefItemTag, String aNodeChildTagName, String aStatusKey, String aStatusValue) {
		if (aRefItemTag == null || StringUtils.isEmpty(aStatusKey) || aStatusValue == null)
			return false;
		IXMLTag lStatusStatusTag = getStatusStatusTag(aRefItemTag);
		if (lStatusStatusTag == null) {
			return false;
		}
		List<IXMLTag> lStatusStatusChildTags = lStatusStatusTag.getChilds(aNodeChildTagName);
		for (IXMLTag lStatusStatusChildTag : lStatusStatusChildTags) {
			CDesktopComponents.sSpring().setRunTagStatusValue(lStatusStatusChildTag, aStatusKey, aStatusValue);
		}
		return true;
	}

	protected IXMLTag getStatusStatusTag(IXMLTag aDataTag) {
		if (aDataTag == null)
			return null;
		IXMLTag lStatusTag = getStatusTag(aDataTag);
		if (lStatusTag == null) {
			return null;
		}
		return lStatusTag.getChild(AppConstants.statusElement);
	}

	protected IXMLTag getStatusStatusChildTag(IXMLTag aStatusStatusTag, String aNodeChildTagName, String aRefDataId) {
		if (aStatusStatusTag == null || StringUtils.isEmpty(aRefDataId))
			return null;
		List<IXMLTag> lRefChildTags = aStatusStatusTag.getChilds(aNodeChildTagName);
		IXMLTag lStatusStatusChildTag = null;
		for (IXMLTag lRefChildTag : lRefChildTags) {
			if (lRefChildTag.getAttribute(AppConstants.keyRefdataid).equals(aRefDataId)) {
				lStatusStatusChildTag = lRefChildTag;
				break;
			}
		}
		if (lStatusStatusChildTag == null) {
			lStatusStatusChildTag = CDesktopComponents.sSpring().newRunNodeTag(aNodeChildTagName, aRefDataId, null, null);
		}
		if (lStatusStatusChildTag != null) {
			lStatusStatusChildTag.setParentTag(aStatusStatusTag);
			aStatusStatusTag.getChildTags().add(lStatusStatusChildTag);
		}
		return lStatusStatusChildTag;
	}

	protected IXMLTag getStatusTag(IXMLTag aDataTag) {
		if (aDataTag == null)
			return null;
		String lStatusType = AppConstants.statusTypeRunGroup;
		IXMLTag lStatusRootTag = CDesktopComponents.sSpring().getXmlRunStatusTree(getCaseComponent(), lStatusType);
		if (lStatusRootTag == null)
			return null;
		String lStatusTagId = aDataTag.getAttribute(AppConstants.keyRefstatusid);
		IXMLTag lStatusTag = null;
		if (!lStatusTagId.equals("")) {
			lStatusTag = CDesktopComponents.sSpring().getXmlStatusTagById(getCaseComponent(), lStatusTagId, lStatusType);
		}
		if (lStatusTag == null) {
			lStatusTag = CDesktopComponents.sSpring().newRunNodeTag(aDataTag.getName(), getXMLTagId(aDataTag), null, null);
			CDesktopComponents.sSpring().getXmlManager().newChildNodeSimple(lStatusRootTag, lStatusTag, null);
			if (lStatusTag != null) {
				aDataTag.setAttribute(AppConstants.keyRefstatusid, getXMLTagId(lStatusTag));
				aDataTag.addStatusTag(lStatusTag);
				CDesktopComponents.sSpring().setXmlStatusTagById(getCaseComponent(), lStatusTag, lStatusType);
			}
		}
		return lStatusTag;
	}


	protected void handleFeedbackAndScore(IECaseComponent aItemsCaseComponent, IXMLTag aItemTag) {
		IXMLTag lFeedbackConditionTag = getFeedbackCondition(currentassessmentrefitemtag, aItemTag);
		String lPreviousFeedbackConditionId = getRefItemFeedbackConditionId(currentassessmentrefitemtag);
		String lFeedbackConditionId = getXMLTagId(lFeedbackConditionTag);
		List<IXMLTag> lFeedbackConditionTags = aItemTag.getChilds("feedbackcondition");
		for (IXMLTag lTag : lFeedbackConditionTags) {
			String lId = getXMLTagId(lTag);
			if (lId.equals(lFeedbackConditionId))
				// set opened of feedback condition to true
				setRunTagStatus(aItemsCaseComponent, lTag, AppConstants.statusKeyOpened, AppConstants.statusValueTrue, true);
		}
		for (IXMLTag lTag : lFeedbackConditionTags) {
			String lId = getXMLTagId(lTag);
			if (lId.equals(lPreviousFeedbackConditionId))
				// set opened of previous feedback condition to false
				//NOTE feedback condition does not have singleopen within definition because in principle multiple feedback conditions
				//could become true, and their feedback texts could be aggregated. But in Java code, we presume only condition can
				//become true, so we set opened to false.
				setRunTagStatus(aItemsCaseComponent, lTag, AppConstants.statusKeyOpened, AppConstants.statusValueFalse, true);
		}
		addOrUpdateRefStatusChildTags(currentassessmentrefitemtag, lFeedbackConditionId, "reffeedbackcondition", AppConstants.statusKeyOpened, AppConstants.statusValueTrue);
		setRunTagStatus(caseComponent, currentassessmentrefitemtag, AppConstants.statusKeyFeedbackConditionId, lFeedbackConditionId, true);
		if (lFeedbackConditionTag != null) {
			setRunTagStatus(caseComponent, currentassessmentrefitemtag, AppConstants.statusKeyScore, lFeedbackConditionTag.getChildValue("score"), true);
			setRunTagStatus(caseComponent, currentassessmentrefitemtag, AppConstants.statusKeyCorrect, lFeedbackConditionTag.getChildValue("correct"), true);
		}
		else {
			setRunTagStatus(caseComponent, currentassessmentrefitemtag, AppConstants.statusKeyScore, "", true);
			setRunTagStatus(caseComponent, currentassessmentrefitemtag, AppConstants.statusKeyCorrect, "", true);
		}
		if (isAutostartAssessment(currentassessmenttag)) {
			//NOTE also adjust assessment score because user cannot end assessment himself
			saveAssessmentScore(currentassessmenttag);
		}
		renderFeedback(lFeedbackConditionTag, false);
	}


	/**
	 * Calls super to get generated alternative tags, to keep supporting old cases and enrich this hashtable with reffield status childs of aRefItem.
	 * 
	 * @param aRefItem the a ref item within the assessments case component
	 * @param aItem the a item within the items case component
	 * @param aChildName the a child name
	 *
	 * @return hashtable
	 */
	@Override
	protected Hashtable<String, IXMLTag> getItemChildTagsFromRefItem(IXMLTag aRefItem, IXMLTag aItem, String aChildName) {
		//NOTE call super to get alternative tags, to support old cases.
		Hashtable<String, IXMLTag> hTags = super.getItemChildTagsFromRefItem(aRefItem, aItem, aChildName);
		//NOTE enrich with reffield status childs of aRefItem
		IXMLTag lStatusStatusTag = getStatusStatusTag(aRefItem);
		if (lStatusStatusTag != null) {
			for (IXMLTag lRefFieldTag : lStatusStatusTag.getChilds("reffield")) {
				hTags.put(lRefFieldTag.getAttribute(AppConstants.keyRefdataid), lRefFieldTag);
			}
		}
		return hTags;
	}

	
	protected void renderFeedback(IXMLTag aFeedbackConditionTag, boolean aFeedbackButtonClicked) {
		ItemTagData lItemTagData = getItemTagData(currentassessmentrefitemtag);
		IXMLTag lItemTag = lItemTagData.getItemTag();
		if (lItemTag == null) {
			return;
		}
		IECaseRole lItemTagCaseRole = lItemTagData.getCaseRole();
		IECaseComponent lItemTagCaseComponent = lItemTagData.getCaseComponent();
		String macroAssessmentFeedbackId = "macroAssessmentFeedback";
		HtmlMacroComponent macroAssessment = (HtmlMacroComponent)CDesktopComponents.vView().getComponent(macroAssessmentFeedbackId);

		String lFeedbackInstruction = getAssessmentFeedback(aFeedbackConditionTag);
		List<List<Object>> lFeedbackPieces = getPiecesDatas(lItemTagCaseRole, lItemTagCaseComponent, aFeedbackConditionTag);
		
		boolean lRenderFirstTime = currentassessmentrefitemtag.getStatusAttribute(AppConstants.statusKeyAnswer).size() == 0 ||
				currentassessmentrefitemtag.getCurrentStatusAttribute(AppConstants.statusKeyAnswer).equals("-1");
		boolean lThereIsFeedback = showFeedback && aFeedbackConditionTag != null &&
				(!StringUtils.isEmpty(lFeedbackInstruction) || lFeedbackPieces.size() > 0);
		boolean lItemHasDirectFeedback = itemHasDirectFeedback(lItemTag);
		boolean lItemHasFields = getPresentChildTags(lItemTag, "field").size() > 0;
		boolean lAssessmentCanBeInspected = !isAutostartAssessment(currentassessmenttag) && isAssessmentFinished(currentassessmenttag) && !isAssessmentEditable(currentassessmenttag);
		boolean lFeedbackButtonVisible = lThereIsFeedback && ((!lItemHasDirectFeedback && !lRenderFirstTime) || lItemHasFields) && !lAssessmentCanBeInspected;  
		boolean lFeedbackInstructionVisible = lThereIsFeedback && (!lFeedbackButtonVisible || aFeedbackButtonClicked) && !lRenderFirstTime;
		boolean lFeedbackTitleVisible = lThereIsFeedback && !lFeedbackButtonVisible && !lRenderFirstTime;  

		macroAssessment.setDynamicProperty("a_assessmentitem_showfeedback", "" + lThereIsFeedback);
		macroAssessment.setDynamicProperty("a_feedback_button_visible", "" + lFeedbackButtonVisible);
		macroAssessment.setDynamicProperty("a_feedback_title_visible", "" + lFeedbackTitleVisible);
		macroAssessment.setDynamicProperty("a_feedback_instruction_visible", "" + lFeedbackInstructionVisible);
		macroAssessment.setDynamicProperty("a_assessment_feedback_instruction", lFeedbackInstruction);
		macroAssessment.setDynamicProperty("a_assessment_feedback_pieces", lFeedbackPieces);
		
		macroAssessment.recreate();
	}
	
	protected String getAssessmentFeedback(IXMLTag aFeedbackCondition) {
		if (aFeedbackCondition == null) {
			return "";
		}
		String lRichtext = CDesktopComponents.sSpring().unescapeXML(aFeedbackCondition.getChildValue("richtext").trim());
		return getRunComponentHelper().stripCKeditorString(lRichtext);
	}
	
	/**
	 * Handle assessment feedback.
	 */
	@Override
	public void handleAssessmentItemFeedback() {
		ItemTagData lItemTagData = getItemTagData(currentassessmentrefitemtag);
		IXMLTag lItemTag = lItemTagData.getItemTag();
		if (lItemTag == null) {
			return;
		}
		IXMLTag lFeedbackConditionTag = getFeedbackCondition(currentassessmentrefitemtag, lItemTag);
		renderFeedback(lFeedbackConditionTag, true);
	}

}
