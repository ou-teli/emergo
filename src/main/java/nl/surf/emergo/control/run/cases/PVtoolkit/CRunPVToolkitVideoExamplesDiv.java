/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit;

import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Div;
import org.zkoss.zul.Image;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefCombobox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefComboitem;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefHtml;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTag;
import nl.surf.emergo.control.run.cases.PVtoolkit.util.CRunPVToolkitCustomAttributeHelper;

public class CRunPVToolkitVideoExamplesDiv extends CDefDiv {

	private static final long serialVersionUID = 2831729321721113676L;

	public String zulfilepath = ((CRunPVToolkitInitBox)CDesktopComponents.vView().getComponent("PV-toolkit_initBox")).getZulfilepath();
		
	protected CRunPVToolkitCacAndTag _runPVToolkitCacAndTag;
	protected String _childTagName;
	
	protected CRunPVToolkit pvToolkit;
	
	protected String _idPrefix = "videoExamples";
	protected String _classPrefix = "videoExamples";
	protected boolean _performanceExamples = false;
	
	public void onCreate(CreateEvent aEvent) {
		_idPrefix = CRunPVToolkitCustomAttributeHelper.handleCustomAttribute(this, "_idPrefix", _idPrefix);
		_classPrefix = CRunPVToolkitCustomAttributeHelper.handleCustomAttribute(this, "_classPrefix", _classPrefix);
	}
	
	public void init(CRunPVToolkitCacAndTag runPVToolkitCacAndTag, String childTagName, boolean performanceExamples) {
		_runPVToolkitCacAndTag = runPVToolkitCacAndTag;
		_childTagName = childTagName;
		_performanceExamples = performanceExamples;
		
		pvToolkit = (CRunPVToolkit)CDesktopComponents.vView().getComponent("pvToolkit");
		
		setClass("popupDiv");

		update();
	}
	
	public void update() {
		//NOTE put all elements in a specific div and if it exists remove it.
		//Other elements are defined in the ZUL file and may not be removed 
		Div specificDiv = pvToolkit.getSpecificDiv(this);

		new CRunPVToolkitDefImage(specificDiv, 
				new String[]{"class"}, 
				new Object[]{"popupBackground"}
		);

		Div popupDiv = new CRunPVToolkitDefDiv(specificDiv, 
				new String[]{"class"}, 
				new Object[]{_classPrefix}
		);

		new CRunPVToolkitDefImage(popupDiv, 
				new String[]{"class", "src"}, 
				new Object[]{_classPrefix + "Background", zulfilepath + "popup-large-background.svg"}
		);
		
		//NOTE parent of performance level tag skill, skill cluster or sub skill. Show its name
		IXMLTag skillTag = _runPVToolkitCacAndTag.getXmlTag().getParentTag();
		new CRunPVToolkitDefLabel(popupDiv, 
				new String[]{"class", "cacAndTag", "tagChildName"}, 
				new Object[]{"font " + _classPrefix + "Title", new CRunPVToolkitCacAndTag(_runPVToolkitCacAndTag.getCaseComponent(), skillTag), "name"}
		);

		if (_performanceExamples) {
			//number of stars
			Div div = new CRunPVToolkitDefDiv(popupDiv, 
					new String[]{"class"}, 
					new Object[]{_classPrefix + "Stars"}
					);

			int performanceLevel = Integer.parseInt(_runPVToolkitCacAndTag.getXmlTag().getChildValue("level"));
			pvToolkit.renderStars(div, performanceLevel, false, "");

			//level description
			div = new CRunPVToolkitDefDiv(popupDiv, 
					new String[]{"class"}, 
					new Object[]{_classPrefix + "Description"}
					);

			new CRunPVToolkitDefHtml(div,
					new String[]{"class", "cacAndTag", "tagChildName"}, 
					new Object[]{"font " + _classPrefix + "Description", _runPVToolkitCacAndTag, "description"}
					);
		}

		List<IXMLTag> exampleTags = pvToolkit.getPresentChildTags(_runPVToolkitCacAndTag, _childTagName);
		showInput(specificDiv, exampleTags);
		
		//video interaction div
		new CRunPVToolkitDefDiv(popupDiv, 
				new String[]{"id"}, 
				new Object[]{_idPrefix + "VideoInteractionDiv"}
		);

		//video
		new CRunPVToolkitDefDiv(popupDiv, 
				new String[]{"id", "class"}, 
				new Object[]{_idPrefix + "VideoDiv", _classPrefix + "VideoDiv"}
		);
		if (exampleTags.size() > 0) {
			showVideo(exampleTags.get(0));
		}

		Image closeImage = new CRunPVToolkitDefImage(popupDiv, 
				new String[]{"class", "src"}, 
				new Object[]{_classPrefix + "CloseImage", zulfilepath + "close.svg"}
		);
		addCloseImageOnClickEventListener(closeImage, getId());

	}
	
	public void showInput(Component parent, List<IXMLTag> exampleTags) {
		if (exampleTags.size() == 1) {
			//label for video
			IXMLTag exampleTag = exampleTags.get(0);
			String name = pvToolkit.getTagChildValue(new CRunPVToolkitCacAndTag(_runPVToolkitCacAndTag.getCaseComponent(), exampleTag), "name");
			new CRunPVToolkitDefLabel(parent, 
					new String[]{"class", "value"}, 
					new Object[]{"font " + _classPrefix + "Header", name}
					);
		}
		else {
			//combobox with videos
			Combobox combobox = new CRunPVToolkitDefCombobox(parent, 
					new String[]{"width"}, 
					new Object[]{"680px"}
			);
			combobox.setClass("font " + _classPrefix + "Combobox");
			for (IXMLTag exampleTag : exampleTags) {
				String name = pvToolkit.getTagChildValue(new CRunPVToolkitCacAndTag(_runPVToolkitCacAndTag.getCaseComponent(), exampleTag), "name");
				Comboitem comboitem = new CRunPVToolkitDefComboitem(combobox, 
						new String[]{"value"}, 
						new Object[]{name}
				);
				comboitem.setClass("font " + _classPrefix + "Comboitem");
				comboitem.setAttribute("exampleTag", exampleTag);
			}
			if (exampleTags.size() > 0) {
				combobox.setSelectedIndex(0);
			}
			combobox.addEventListener("onSelect", new EventListener<Event>() {
				public void onEvent(Event event) {
					if (combobox.getSelectedItem() != null) {
						showVideo((IXMLTag)combobox.getSelectedItem().getAttribute("exampleTag"));
					}
				}
			});
		}
	}
	
	protected void showVideo(IXMLTag exampleTag) {
		pvToolkit.showDataVideo(exampleTag , "blob", _idPrefix, _classPrefix);
	}

	protected void addCloseImageOnClickEventListener(Component component, String componentToCloseId) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				pvToolkit.clearVideo(_idPrefix);
				Component componentFrom = CDesktopComponents.vView().getComponent(componentToCloseId);
				if (componentFrom != null) {
					componentFrom.setVisible(false);
				}
				//NOTE for unknown reason video examples div sometimes does not appear on top of other div,
				//attribute to show other div when closed
				String componentIdToShow = (String)getAttribute("componentIdToShow");
				if (componentIdToShow != null) {
					Component componentToShow = CDesktopComponents.vView().getComponent(componentIdToShow);
					if (componentToShow != null) {
						componentToShow.setVisible(true);
					}
				}
			}
		});
	}

}
