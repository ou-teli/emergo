/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Div;
import org.zkoss.zul.Include;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CIObserved;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.run.CRunAlert;
import nl.surf.emergo.control.run.CRunArea;
import nl.surf.emergo.control.run.CRunChoiceHoverBtns;
import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.control.run.CRunConversationInteraction;
import nl.surf.emergo.control.run.CRunConversations;
import nl.surf.emergo.control.run.CRunDashboard;
import nl.surf.emergo.control.run.CRunHoverBtn;
import nl.surf.emergo.control.run.CRunIspot;
import nl.surf.emergo.control.run.CRunLocationReference;
import nl.surf.emergo.control.run.CRunMemo;
import nl.surf.emergo.control.run.CRunMemoBtn;
import nl.surf.emergo.control.run.CRunNote;
import nl.surf.emergo.control.run.CRunNotifications;
import nl.surf.emergo.control.run.CRunProfile;
import nl.surf.emergo.control.run.CRunScores;
import nl.surf.emergo.control.run.CRunStatusChange;
import nl.surf.emergo.control.run.CRunTitleArea;
import nl.surf.emergo.control.run.CRunVideosceneselector;
import nl.surf.emergo.control.run.CRunWnd;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IEComponent;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.STriggeredReference;

/**
 * The Class CRunWndOunl. The Ounl Emergo player window.
 */
public class CRunWndOunl extends CRunWnd
 {

	private static final long serialVersionUID = -2552938104058775843L;

	/** The run tablet btn to show the tablet. */
	public CRunHoverBtn runTabletBtn = null;

	/** The run note btn to show the note. */
	public CRunHoverBtn runNoteBtn = null;

	/** The run memo btn to start the memo recorder. */
	public CRunHoverBtn runMemoBtn = null;

	/** The run dashboard btn to show the dashboard. */
	public CRunHoverBtn runDashboardBtn = null;

	/** The current location tag id. The tagid of the current location. */
	public String currentLocationTagId = "";

	/** The navigation component. Used for buffering in memory. */
	protected IECaseComponent navigationComponent = null;

	/** The tablet act run component, the current run component shown within tablet. */
	protected CRunComponent tabletActRunComponent = null;

	/** The act run component, the current run component shown. */
	protected CRunComponent actRunComponent = null;

	/** The location act run component, the current run component shown on location. */
	protected CRunComponent locationActRunComponent = null;

	/** The conversation interaction, used to ask questions during conversations. */
	protected CRunConversationInteraction conversationInteraction = null;

	/** The possible case components on location. Used for buffering in memory. */
	protected List<IECaseComponent> possibleCaseComponentsOnLocation = null;

	/** The conversation started. */
	protected boolean conversationStarted = false;

	/** The init location components. */
	protected boolean initLocationComponents = false;

	/** The init location. */
	protected boolean initLocation = false;

	/**
	 * Instantiates a new c run wnd.
	 * Reads google maps key out of .properties file and set it as session var,
	 * so google maps will work within the player.
	 */
	public CRunWndOunl() {
		super();
	}

	/**
	 * Creates tablet button.
	 *
	 * @param aCaseComponent the a case component
	 * @param aStatus the a status
	 *
	 * @return run hover btn
	 */
	protected CRunHoverBtn createTabletBtn(IECaseComponent aCaseComponent, String aStatus) {
		Map<String,Object> lParams = new HashMap<String,Object>();
		lParams.put("id", "runTabletBtn");
		lParams.put("status", aStatus);
		lParams.put("action", "showTablet");
		lParams.put("actionstatus", "");
		lParams.put("imgprefix", "tablet");
		lParams.put("label", "");
		lParams.put("clientonclickaction", "");
		lParams.putAll(getCaseComponentBtnData(aCaseComponent));
		return createTabletBtn(lParams);
	}

	/**
	 * Creates tablet button.
	 *
	 * @param aParams the a params
	 *
	 * @return run hover btn
	 */
	protected CRunHoverBtn createTabletBtn(Map<String,Object> aParams) {
		return new CRunTabletBtnOunl(aParams);
	}

	/**
	 * Creates note button.
	 *
	 * @param aCaseComponent the a case component
	 * @param aStatus the a status
	 *
	 * @return run hover btn
	 */
	protected CRunHoverBtn createNoteBtn(IECaseComponent aCaseComponent, String aStatus) {
		Map<String,Object> lParams = new HashMap<String,Object>();
		lParams.put("id", "runNoteBtn");
		lParams.put("status", aStatus);
		lParams.put("action", "showNote");
		lParams.put("actionstatus", "");
		lParams.put("imgprefix", "note");
		lParams.put("label", "");
		lParams.put("clientonclickaction", "");
		lParams.putAll(getCaseComponentBtnData(aCaseComponent));
		return createNoteBtn(lParams);
	}

	/**
	 * Creates note button.
	 *
	 * @param aParams the a params
	 *
	 * @return run hover btn
	 */
	protected CRunHoverBtn createNoteBtn(Map<String,Object> aParams) {
		return new CRunNoteBtnOunl(aParams);
	}

	/**
	 * Creates memo button.
	 *
	 * @param aCaseComponent the a case component
	 * @param aStatus the a status
	 *
	 * @return run hover btn
	 */
	protected CRunHoverBtn createMemoBtn(IECaseComponent aCaseComponent, String aStatus) {
		Map<String,Object> lParams = new HashMap<String,Object>();
		lParams.put("id", "runMemoBtn");
		lParams.put("status", aStatus);
		lParams.put("action", "toggleMemo");
		lParams.put("actionstatus", "");
		lParams.put("imgprefix", "memo");
		lParams.put("label", "");
		lParams.put("clientonclickaction", "");
		lParams.putAll(getCaseComponentBtnData(aCaseComponent));
		return createMemoBtn(lParams);
	}

	/**
	 * Creates memo button.
	 *
	 * @param aParams the a params
	 *
	 * @return run hover btn
	 */
	protected CRunHoverBtn createMemoBtn(Map<String,Object> aParams) {
		return new CRunMemoBtnOunl(aParams);
	}

	/**
	 * Creates dashboard button.
	 *
	 * @param aCaseComponent the a case component
	 * @param aStatus the a status
	 *
	 * @return run hover btn
	 */
	protected CRunHoverBtn createDashboardBtn(IECaseComponent aCaseComponent, String aStatus) {
		Map<String,Object> lParams = new HashMap<String,Object>();
		lParams.put("id", "runDashboardBtn");
		lParams.put("status", aStatus);
		lParams.put("action", "showDashboard");
		lParams.put("actionstatus", "");
		lParams.put("imgprefix", "dashboard");
		lParams.put("label", "");
		lParams.put("clientonclickaction", "");
		lParams.putAll(getCaseComponentBtnData(aCaseComponent));
		return createDashboardBtn(lParams);
	}

	/**
	 * Creates dashboard button.
	 *
	 * @param aParams the a params
	 *
	 * @return run hover btn
	 */
	protected CRunHoverBtn createDashboardBtn(Map<String,Object> aParams) {
		return new CRunDashboardBtnOunl(aParams);
	}

	/**
	 * Creates conversations component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createConversationsComponent(IECaseComponent aCaseComponent) {
		return new CRunConversationsOunl("runConversations", aCaseComponent);
	}
	
	/**
	 * Creates conversation interaction.
	 *
	 * @param aRunComponent the a run component
	 *
	 * @return run conversation interaction
	 */
	protected CRunConversationInteraction createConversationInteraction(CRunComponent aRunComponent) {
		return new CRunConversationInteractionOunl("runConversationInteraction", aRunComponent, "runConversations");
	}
	
	/**
	 * Creates tablet component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createTabletComponent(IECaseComponent aCaseComponent) {
		return new CRunTabletOunl("runTablet", aCaseComponent);
	}
	
	/**
	 * Creates dashboard component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createDashboardComponent(IECaseComponent aCaseComponent) {
		return new CRunDashboardOunl("runDashboard", aCaseComponent);
	}
	
	/**
	 * Creates references component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createReferencesComponent(IECaseComponent aCaseComponent) {
		return new CRunReferencesOunl("runReferences", aCaseComponent);
	}
	
	/**
	 * Creates mail component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createMailComponent(IECaseComponent aCaseComponent) {
		return new CRunMailOunl("runMail", aCaseComponent);
	}
	
	/**
	 * Creates assessments component.
	 *
	 * @param aCaseComponent the a case component
	 * @param aOnTablet the a on tablet
	 *
	 * @return run component
	 */
	protected CRunComponent createAssessmentsComponent(IECaseComponent aCaseComponent, boolean aOnTablet) {
		return new CRunAssessmentsOunl("runAssessments", aCaseComponent, aOnTablet);
	}
	
	/**
	 * Creates logbook component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createLogbookComponent(IECaseComponent aCaseComponent) {
		return new CRunLogbookOunl("runLogbook", aCaseComponent);
	}
	
	/**
	 * Creates google maps component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createGooglemapsComponent(IECaseComponent aCaseComponent) {
		return new CRunGooglemapsOunl("runGooglemaps", aCaseComponent);
	}
	
	/**
	 * Creates canon component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createCanonComponent(IECaseComponent aCaseComponent) {
		return new CRunCanonOunl("runCanon", aCaseComponent);
	}
	
	/**
	 * Creates canon result component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createCanonResultComponent(IECaseComponent aCaseComponent) {
		return new CRunCanonResultOunl("runCanonResult", aCaseComponent);
	}
	
	/**
	 * Creates tasks component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createTasksComponent(IECaseComponent aCaseComponent) {
		return new CRunTasksOunl("runTasks", aCaseComponent);
	}
	
	/**
	 * Creates directing component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createDirectingComponent(IECaseComponent aCaseComponent) {
		return new CRunDirectingOunl("runDirecting", aCaseComponent);
	}
	
	/**
	 * Creates memos component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createMemosComponent(IECaseComponent aCaseComponent) {
		return new CRunMemosOunl("runMemos", aCaseComponent);
	}
	
	/**
	 * Creates video manual component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createVideomanualComponent(IECaseComponent aCaseComponent) {
		return new CRunVideomanualOunl("runVideomanual", aCaseComponent);
	}
	
	/**
	 * Creates tutorial component.
	 *
	 * @param aCaseComponent the a case component
	 * @param aChapterId the a chapter id
	 *
	 * @return run component
	 */
	protected CRunComponent createTutorialComponent(IECaseComponent aCaseComponent, String aChapterId) {
		return new CRunTutorialOunl("runTutorial", aCaseComponent, aChapterId);
	}
	
	/**
	 * Creates profile component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createProfileComponent(IECaseComponent aCaseComponent) {
		return new CRunProfileOunl("runProfile", aCaseComponent);
	}
	
	/**
	 * Creates scores component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunScores createScoresComponent(IECaseComponent aCaseComponent) {
		return new CRunScoresOunl("runScores", aCaseComponent);
	}
	
	/**
	 * Creates notifications component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunNotifications createNotificationsComponent(IECaseComponent aCaseComponent) {
		//NOTE There can be multiple notifications components so add case component id within notifications id.
		return new CRunNotificationsOunl("runNotifications_" + aCaseComponent.getCacId(), aCaseComponent);
	}
	
	/**
	 * Creates note component.
	 *
	 * @param aRunComponent the a run component
	 *
	 * @return run component
	 */
	protected CRunArea createNoteComponent(CRunComponent aRunComponent) {
		return new CRunNoteOunl("runNote", aRunComponent);
	}
	
	/**
	 * Creates memo component.
	 *
	 * @return run component
	 */
	protected CRunMemo createMemoComponent() {
		return new CRunMemoOunl("runMemo");
	}
	
	/**
	 * Creates alert component.
	 *
	 * @param aRunComponent the a run component
	 *
	 * @return run component
	 */
	protected CRunAlert createAlertComponent(CRunComponent aRunComponent) {
		return new CRunAlertOunl("runAlert", aRunComponent);
	}
	
	/**
	 * Creates ispot component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createIspotComponent(IECaseComponent aCaseComponent) {
		return new CRunIspotOunl("runIspot", aCaseComponent);
	}
	
	/**
	 * Creates editforms component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createEditformsComponent(IECaseComponent aCaseComponent) {
		return new CRunEditformsOunl("runEditforms", aCaseComponent);
	}
	
	/**
	 * Creates dragdropforms component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createDragdropformsComponent(IECaseComponent aCaseComponent) {
		return new CRunDragdropformsOunl("runDragdropforms", aCaseComponent);
	}
	
	/**
	 * Creates graphicalforms component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createGraphicalformsComponent(IECaseComponent aCaseComponent) {
		return new CRunGraphicalformsOunl("runGraphicalforms", aCaseComponent);
	}
	
	/**
	 * Creates selectionforms component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createSelectionformsComponent(IECaseComponent aCaseComponent) {
		return new CRunSelectionformsOunl("runSelectionforms", aCaseComponent);
	}
	
	/**
	 * Creates videosceneselector component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createVideosceneselectorComponent(IECaseComponent aCaseComponent) {
		return new CRunVideosceneselectorOunl("runVideosceneselector", aCaseComponent);
	}
	
	/**
	 * Creates tests component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createTestsComponent(IECaseComponent aCaseComponent) {
		return new CRunTestsOunl("runTests", aCaseComponent);
	}
	
	/**
	 * Creates run location area.
	 *
	 * @return area
	 */
	protected CRunLocationAreaOunl createLocationArea() {
		return new CRunLocationAreaOunl("runLocationArea");
	}
	
	/**
	 * Creates breadcrumb area.
	 *
	 * @return area
	 */
	protected CRunBreadcrumbsAreaOunl createBreadcrumbsArea() {
		return new CRunBreadcrumbsAreaOunl("runBreadcrumbsArea");
	}
	
	/**
	 * Creates close area.
	 *
	 * @return area
	 */
	protected CRunArea createCloseArea() {
		return new CRunCloseAreaOunl("runCloseArea");
	}
	

	/**
	 * On create:
	 * - save ZK desktop id to use it when ZK desktop is closed
	 * - get request parameter runstatus, see IAppManager
	 * - get request parameter rgaId, the id of the user opening the player
	 * - get request parameter rutId, the possible runteam the user belongs to
	 * - get request parameter cacId, the possible casecomponent to preselect
	 * - get request parameter tagId, the possible tag to preselect
	 * - get request parameter mediacontrol, if windows media player will show controls
	 * - get request parameters for language to use
	 * - start CRunMainTimer
	 * - add rungroupaccount to rungroupaccounts who have an open player, so who are
	 *   playing right now
	 *
	 * @param aEvent the a event
	 */
	public void onCreate(Event aEvent) {
		super.onCreate(aEvent);
	}

	/**
	 * Init player.
	 */
	public void init() {
		super.init();

		initProfile();
		initScores();
		//init scores before init formulas otherwise formula values will not be rendered in scores
		sSpring.initStatesFormulas();
		initDashboard();
	}

	/**
	 * Init location components.
	 */
	public void initLocationComponents(IXMLTag aLocationTag) {
		if (initLocationComponents) {
			return;
		}
		initLocationComponents = true;
		initNotifications(aLocationTag);
	}

	/**
	 * Cleanup. Is called when the ZK desktop (the current browser instance)
	 * is closed.
	 * - saves note if note window is opened
	 * - saves all pending status for the user
	 * - removes current rungroupaccount from list of rungroupaccounts who are
	 *   playing right now
	 */
	public synchronized void cleanup(){
		if (saveAtCleanup()) {
			CRunNote lRunNote = (CRunNote)CDesktopComponents.vView().getComponent("runNote");
			if (lRunNote != null)
				lRunNote.saveContent();
		}
		CRunIspot lRunIspot = (CRunIspot)CDesktopComponents.vView().getComponent("runIspot");
		if (lRunIspot != null)
			lRunIspot.cleanup();
		super.cleanup();
	}

	/**
	 * Creates child components of player.
	 */
	public void createComponents() {
		updateViewAreas();
	}

	/**
	 * Updates view areas. The areas beneath the run view area.
	 */
	protected void updateViewAreas() {
		String lStatus = "empty";
		// update tablet button
		if (isComponentUsed("empack")) {
			runTabletBtn = (CRunHoverBtn)CDesktopComponents.vView().getComponent("runTabletBtn");
			if (runTabletBtn == null) {
				runTabletBtn = createTabletBtn(sSpring.getCaseComponent("empack", ""), lStatus);
				appendChild(runTabletBtn);
				runTabletBtn.registerObserver(getId());
			}
		}
		// update note button
		if (isComponentUsed("note")) {
			runNoteBtn = (CRunHoverBtn)CDesktopComponents.vView().getComponent("runNoteBtn");
			if (runNoteBtn == null) {
				runNoteBtn = createNoteBtn(sSpring.getCaseComponent("note", ""), lStatus);
				appendChild(runNoteBtn);
				runNoteBtn.registerObserver(getId());
			}
		}
		// update memo button
		if (isComponentUsed("memo")) {
			runMemoBtn = (CRunHoverBtn)CDesktopComponents.vView().getComponent("runMemoBtn");
			if (runMemoBtn == null) {
				runMemoBtn = createMemoBtn(sSpring.getCaseComponent("memo", ""), lStatus);
				runMemoBtn.setToggleStatus(true);
				appendChild(runMemoBtn);
				runMemoBtn.registerObserver(getId());
			}
			boolean memoUsedAndVisible = isComponentUsedAndVisible("memo");
			if (!memoUsedAndVisible) {
				runMemoBtn.setVisible(false);
			}
		}
		// update dashboard button
		if (isComponentUsed("dashboard")) {
			runDashboardBtn = (CRunHoverBtn)CDesktopComponents.vView().getComponent("runDashboardBtn");
			if (runDashboardBtn == null) {
				runDashboardBtn = createDashboardBtn(sSpring.getCaseComponent("dashboard", ""), lStatus);
				appendChild(runDashboardBtn);
				runDashboardBtn.registerObserver(getId());
			}
		}
	}

	/**
	 * Shows components status.
	 * Is called when the player is started. Restores player status for current user
	 * and depending on runstatus. If preview, preselected casecomponent and tag are
	 * used to set the player in a certain state. Otherwise location is restored to
	 * last location visited during previous session.
	 */
	@Override
	public void showComponentsStatus() {
		if (runTabletBtn != null) {
			runTabletBtn.setStatus(getCaseComponentRenderStatus("empack", ""));
		}
		if (runNoteBtn != null) {
			runNoteBtn.setStatus(getCaseComponentRenderStatus("note", ""));
		}
		if (runMemoBtn != null) {
			runMemoBtn.setStatus(getCaseComponentRenderStatus("memo", ""));
			if (runMemoBtn.getStatus().equals("active")) {
				// NOTE Memo button only can be clicked during conversations
				runMemoBtn.setStatus("inactive");
			}
		}
		if (runDashboardBtn != null) {
			runDashboardBtn.setStatus(getCaseComponentRenderStatus("dashboard", ""));
		}

		IECaseComponent lNavigationComponent = null;
		IXMLTag lSelectedLocationTag = null;
		IECaseComponent lTabletComponent = null;
		IECaseComponent lNoteComponent = null;
		IECaseComponent lTabletAppComponent = null;
		IECaseComponent lTutorialComponent = null;
		IECaseComponent lIspotComponent = null;
		IECaseComponent lDashboardComponent = null;
		if (isRunStatus(AppConstants.runStatusPreview) || isRunStatus(AppConstants.runStatusPreviewReadOnly) || isRunStatus(AppConstants.runStatusTutorRun)) {
			//NOTE if case component other than navigation component is previewed, no "showLocation" action is sent!
			//This would interfere with for instance previewing a conversation because "showLocation" could also result in "showLocationAction" of another case component.
			if (preselectedCaseComponent == null) {
				// no case component selected, so open navigation
				// there is only one instance of the navigation component
				lNavigationComponent = getNavigationComponent();
			}
			else {
				// get possibly selected navigation component
				lNavigationComponent = getPreselectedCaseComponent("navigation");
			}
			// get possibly selected location tag
			lSelectedLocationTag = getPreselectedLocationTag();
			if (lSelectedLocationTag == null && lNavigationComponent != null) {
				// if no selected location tag and preview of navigation component, get first or saved location tag
				lSelectedLocationTag = getLocationTag();
			}
			// get possibly selected note component
			lNoteComponent = getPreselectedCaseComponent("note");
			// get possibly selected empack action component
			lTabletAppComponent = getPreselectedTabletApp();
			lTutorialComponent = getPreselectedCaseComponent("tutorial");
			lIspotComponent = getPreselectedCaseComponent("ispot");
			lDashboardComponent = getPreselectedCaseComponent("dashboard");
			if (lTabletAppComponent == null) {
				// get possibly selected tablet component
				lTabletComponent = getPreselectedCaseComponent("empack");
			}
			else {
				runTabletBtn.simulateOnClick();
				CRunHoverBtn lBtn = (CRunHoverBtn) CDesktopComponents.vView().getComponent(getTabletAppBtnId(lTabletAppComponent));
				if (lBtn != null) {
					lBtn.simulateOnClick();
				}
				else {
					List<String> lCacIdAndTagId = new ArrayList<String>();
					lCacIdAndTagId.add("" + lTabletAppComponent.getCacId());
					lCacIdAndTagId.add("0");
					onAction(getId(), "showTabletApp", lCacIdAndTagId);
				}
			}
			if (lTutorialComponent != null) {
				List<String> status = new ArrayList<String>();
				status.add("" + lTutorialComponent.getCacId());
				String lTagId = "";
				if (preselectedTag != null) {
					lTagId = preselectedTag.getAttribute(AppConstants.keyId);
				}
				status.add(lTagId);
				onAction(getId(), "showApp", status);
			}
			if (lIspotComponent != null) {
				List<String> status = new ArrayList<String>();
				status.add("" + lIspotComponent.getCacId());
				onAction(getId(), "showApp", status);
			}
			if (lTabletComponent == null &&
				lTabletAppComponent == null &&
				lNoteComponent == null &&
				lTutorialComponent == null &&
				lIspotComponent == null) {
				// get possibly selected location actions
				List<List<Object>> lLocationActions = getPreselectedLocationActions();
				// show only the one selected if so
				if (lLocationActions.size() > 0) {
					if (lLocationActions.size() == 1) {
						onAction(getId(), "showLocationAction", lLocationActions.get(0));
					}
					else {
						onAction(getId(), "showLocationActions", null);
					}
				}
			}
			if (lSelectedLocationTag == null) {
				//NOTE no preview from navigation component, so show runWnd which means fading in runWnd
				onTimedAction(getId(), "showRunWnd", null);
			}
		}
		if (isRunStatus(AppConstants.runStatusRun)) {
			// there is only one instance of the navigation component
			lNavigationComponent = getNavigationComponent();
			// get first or saved location tag
			lSelectedLocationTag = getLocationTag();
		}
		if (isRunStatus("demo")) {
		}
		if (lNavigationComponent != null || lTabletComponent != null || lNoteComponent != null || lDashboardComponent != null || lSelectedLocationTag != null) {
			// show navigation bar
			if (lNavigationComponent != null) {
				if (lSelectedLocationTag != null) {
					// prohibit adding location actions to prevent multiple occurences of the same action
					pAddActions = false;
				}
			}
			pAddActions = true;
			// show tablet
			if (runTabletBtn != null && lTabletComponent != null) {
				runTabletBtn.simulateOnClick();
			}
			// show location
			if (lSelectedLocationTag != null) {
				String lSelectedTagId = lSelectedLocationTag.getAttribute(AppConstants.keyId);
				List<IXMLTag> lLocations = getNestedLocationTags();
				if (lLocations != null) {
					for (IXMLTag lLocation : lLocations) {
						String lTagId = lLocation.getAttribute(AppConstants.keyId);
						if (lTagId != null && lTagId.equals(lSelectedTagId))
							onTimedAction(getId(), "showLocation", lTagId);
					}
				}
			}
			// show note
			if (runNoteBtn != null && lNoteComponent != null) {
				runNoteBtn.simulateOnClick();
			}
			// show dashboard
			if (runDashboardBtn != null && lDashboardComponent != null) {
				runDashboardBtn.simulateOnClick();
			}
		}
	}

	/**
	 * Init profile.
	 */
	public void initProfile() {
		if (isComponentUsed("profile")) {
			CRunComponent runProfile = (CRunComponent)CDesktopComponents.vView().getComponent("runProfile");
			if (runProfile == null) {
				IECaseComponent lCaseComponent = sSpring.getCaseComponent("profile", "");
				if (lCaseComponent != null) {
					runProfile = createProfileComponent(lCaseComponent);
					appendChild(runProfile);
					boolean lPresent = ((sSpring.getCurrentRunComponentStatus(
							lCaseComponent, AppConstants.statusKeyPresent, AppConstants.statusTypeRunGroup)).equals(AppConstants.statusValueTrue));
					runProfile.setVisible(lPresent);
				}
			}
		}
	}

	/**
	 * Init scores.
	 */
	public void initScores() {
		if (isComponentUsed("scores")) {
			CRunComponent runScores = (CRunComponent)CDesktopComponents.vView().getComponent("runScores");
			if (runScores == null) {
				IECaseComponent lCaseComponent = sSpring.getCaseComponent("scores", "");
				if (lCaseComponent != null) {
					runScores = createScoresComponent(lCaseComponent);
					appendChild(runScores);
					IXMLTag lRootTag = runScores.getDataPlusStatusRootTag();
					IXMLTag lComponentTag = lRootTag.getChild(AppConstants.componentElement);
					boolean lPresent = lComponentTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent)
							.equals(AppConstants.statusValueTrue);
					runScores.setVisible(lPresent);
				}
			}
		}
	}

	/**
	 * Init dashboard.
	 */
	public void initDashboard() {
		if (isComponentUsed("dashboard")) {
			CRunComponent runDashboard = (CRunComponent)CDesktopComponents.vView().getComponent("runDashboard");
			if (runDashboard == null) {
				IECaseComponent lCaseComponent = sSpring.getCaseComponent("dashboard", "");
				if (lCaseComponent != null) {
					runDashboard = createDashboardComponent(lCaseComponent);
					appendChild(runDashboard);
					//initially hide. User has to click dashboard icon to open it.
					runDashboard.setVisible(false);
				}
			}
		}
	}

	/**
	 * Init notifications.
	 */
	public void initNotifications(IXMLTag aLocationTag) {
		List<IECaseComponent> lCaseComponents = sSpring.getCaseComponents("notifications");
		for (IECaseComponent lCaseComponent : lCaseComponents) {
			appendNotificationsComponent(lCaseComponent, aLocationTag);
		}
	}

	/**
	 * Append notifications component.
	 * 
	 * @param aCaseComponent the a case component
	 *
	 * @return notifications component
	 */
	protected CRunComponent appendNotificationsComponent(IECaseComponent aCaseComponent, IXMLTag aLocationTag) {
		CRunComponent runNotifications = (CRunComponent)CDesktopComponents.vView().getComponent("runNotifications_" + aCaseComponent.getCacId());
		if (runNotifications == null) {
			//if notifications not on current location don't append
			if (isLocationComponentOnLocation(aCaseComponent, aLocationTag)) {
				runNotifications = createNotificationsComponent(aCaseComponent);
				appendChild(runNotifications);
				IXMLTag lRootTag = runNotifications.getDataPlusStatusRootTag();
				IXMLTag lComponentTag = lRootTag.getChild(AppConstants.componentElement);
				boolean lPresent = lComponentTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent)
						.equals(AppConstants.statusValueTrue);
				runNotifications.setVisible(lPresent);
			}
		}
		return runNotifications;
	}

	/**
	 * Gets player status string.
	 *
	 * @param aKey the a key
	 * @param aStatus the a status
	 *
	 * @return status string
	 */
	protected String getPlayerStatusString(String aKey, Object aStatus) {
		return "";
	}

	/**
	 * Gets player status xml tag.
	 *
	 * @param aKey the a key
	 * @param aStatus the a status
	 *
	 * @return status xml tag
	 */
	protected IXMLTag getPlayerStatusXmlTag(String aKey, Object aStatus) {
		if (aKey.equals("ActCaseComponentTag")) {
			return getActCaseComponentTag((String)aStatus);
		}
		return null;
	}

	/**
	 * Gets player status list.
	 *
	 * @param aKey the a key
	 * @param aStatus the a status
	 *
	 * @return status list
	 */
	public List getPlayerStatusList(String aKey, Object aStatus) {
		if (aKey.equals("TabletAppComponents"))
			return getTabletAppComponents();
		else if (aKey.equals("LocationTags"))
			return getNestedLocationTags();
		return new ArrayList();
	}

	/**
	 * Sets player status.
	 *
	 * @param aKey the a key
	 * @param aStatus the a status
	 */
	protected void setPlayerStatus(String aKey, Object aStatus) {
	}

	/**
	 * Sets the current case component for notes. Clears current tag for notes.
	 *
	 * @param aCurrentCaseComponentForNotes the current case component for notes
	 */
	public void setNoteCaseComponent(IECaseComponent aCurrentCaseComponentForNotes) {
		deselectNoteBtn(aCurrentCaseComponentForNotes);
		super.setNoteCaseComponent(aCurrentCaseComponentForNotes);
	}

	/**
	 * Sets the current tag for notes and current case component for notes.
	 *
	 * @param aCurrentCaseComponentForNotes the current case component for notes
	 * @param aCurrentTagForNotes the a current tag for notes
	 */
	public void setNoteTag(IECaseComponent aCurrentCaseComponentForNotes, IXMLTag aCurrentTagForNotes) {
		deselectNoteBtn(aCurrentCaseComponentForNotes);
		super.setNoteTag(aCurrentCaseComponentForNotes, aCurrentTagForNotes);
	}

	/**
	 * Deselects note btn.
	 *
	 * @param aCaseComponentForNotes the case component for notes
	 */
	protected void deselectNoteBtn(IECaseComponent aCaseComponentForNotes) {
	}

	/**
	 * Is called by several other components for notification.
	 * Calls method onAction, so see this method for possible aActions.
	 *
	 * @param aObserved the observed object, it should have interface CIObserved implemented
	 * @param aAction the action
	 * @param aStatus the status object, can be String or other class
	 */
	@Override
	public void observedNotify(CIObserved aObserved, String aAction, Object aStatus) {
		if (aAction.equals("endComponent")) {
			if (!(aStatus instanceof CRunDashboard)) {
				endTabletApp(aStatus);
			}
		}
		if (aAction.equals("endComponentAndTablet")) {
			if (!(aStatus instanceof CRunDashboard)) {
				endTabletApp(aStatus);
				IECaseComponent lCaseComponent = sSpring.getCaseComponent("empack", "");
				//NOTE set opened to false, because it is a result of a user action
				sSpring.setRunComponentStatus(lCaseComponent, AppConstants.statusKeyOpened, AppConstants.statusValueFalse, true, AppConstants.statusTypeRunGroup, true);
				onAction(getId(), "endTablet", null);
			}
		}
		if (aAction.equals("endTasks")) {
			endTabletApp(aStatus);
		}
		if (aAction.equals("endApp")) {
			endApp(aStatus);
		}
		super.observedNotify(aObserved, aAction, aStatus);
	}

	/**
	 * Ends tablet app.
	 *
	 * @param aRunComponent the a run component
	 */
	@Override
	public void endTabletApp(Object aRunComponent) {
		CRunComponent lRunComponent = (CRunComponent)aRunComponent;
		if (lRunComponent != null) {
			sSpring.setRunComponentStatus(lRunComponent.getCaseComponent(), AppConstants.statusKeyOpened, AppConstants.statusValueFalse, true, AppConstants.statusTypeRunGroup, true);
			lRunComponent.cleanup();
			lRunComponent.detach();
			tabletActRunComponent = null;
			// maybe add something for shared component
		}
	}

	/**
	 * Ends app.
	 *
	 * @param aRunComponent the a run component
	 */
	@Override
	public void endApp(Object aRunComponent) {
		CRunComponent lRunComponent = (CRunComponent)aRunComponent;
		if (lRunComponent != null) {
			sSpring.setRunComponentStatus(lRunComponent.getCaseComponent(), AppConstants.statusKeyOpened, AppConstants.statusValueFalse, true, AppConstants.statusTypeRunGroup, true);
			lRunComponent.detach();
			actRunComponent = null;
			// maybe add something for shared component
		}
	}

	/**
	 * On action. Is called by several components change the state of the player.
	 * Possible actions are:
	 * - showLocations: shows location buttons within run choice area
	 * - showLocation: shows location image within run view area
	 * - beamLocation: simulates click on location button and shows location image
	 * - showEmpack: shows tablet app buttons within run choice area
	 * - showTabletApp: shows content of corresponding case component within run view area
	 * - showLocationActions: shows popup window with possible location actions
	 * - showLocationAction: shows location action within run view area
	 * - endComponent: restores tablet app button state when a component shown within the run view area is closed
	 * - showTasks: shows tasks within popup window
	 * - endTasks: restores tasks button state
	 * - showNote: shows note window within run choice area
	 * - endNote: restores note button state
	 * - onExternalEvent: used to notify conversations component when user clicks pause/play
	 * - close: saves all pending status for current user
	 *
	 * @param sender the sender
	 * @param action the action
	 * @param status the status
	 */
	@Override
	public void onAction(String sender, String action, Object status) {
		if (action.equals("showRunWnd")) {
			fadeInComponent(CControl.runWnd);
		}
		else if (action.equals("showLocations")) {
			// shows location selectors within popup window above view area
			// if only one action selector, the popup window isn't shown. the
			// location is shown immediately

			String lTagIds;
			if (status instanceof CRunLocationReference) {
				//NOTE locations in other navigation component
				CRunLocationReference lReference = (CRunLocationReference)status;
				String lCacId = lReference.getCacId();
				if (!lCacId.equals("")) {
					setNavigationComponent(sSpring.getCaseComponent(lCacId));
				}
				lTagIds = lReference.getLocationIds();
			}
			else {
				lTagIds = (String)status;
			}
			// get location tags, status is comma separated list of ids
			List<IXMLTag> lLocationTags = getNestedLocationTags(lTagIds);
			if (lLocationTags != null) {
				if (lLocationTags.size() == 1) {
					onAction(getId(), "showLocation", lLocationTags.get(0).getAttribute(AppConstants.keyId));
				}
				if (lLocationTags.size() > 1) {
					Map<String,Object> lParams = new HashMap<String,Object>();
					lParams.put("item", lLocationTags);
					CDesktopComponents.vView().modalPopupWithoutWaiting(VView.v_run_locations, null, lParams, "center");
				}
			}
		}
		else if (action.equals("beamLocation")) {
			IXMLTag lLocation = getLocation((String) status);
			String lTagId = lLocation.getAttribute(AppConstants.keyId);
			if ((lTagId != null) && (lTagId.equals(status))) {
				// allow beaming to current location
				if (lTagId == currentLocationTagId)
					currentLocationTagId = null;
				onAction(getId(), "showLocation", lTagId);
			}
		}
		else if (action.equals("showLocation") || action.equals("fadeInLocation")) {
			IXMLTag lCurrentLocationTag = getLocation(currentLocationTagId);
			String lTagId;
			if (status instanceof CRunLocationReference) {
				//NOTE location in other navigation component
				CRunLocationReference lReference = (CRunLocationReference)status;
				String lCacId = lReference.getCacId();
				if (!lCacId.equals("")) {
					setNavigationComponent(sSpring.getCaseComponent(lCacId));
				}
				lTagId = lReference.getLocationIds();
			}
			else {
				lTagId = (String)status;
			}
			IXMLTag lLocationTag = getLocation(lTagId);
			
			if (lLocationTag != null && lTagId != currentLocationTagId) {
				
				boolean lShowTransitionEffect = false;
				CRunLocationAreaOunl lComponent = (CRunLocationAreaOunl)CDesktopComponents.vView().getComponent("runLocationArea");
				if (lComponent != null) {
					lShowTransitionEffect = lComponent.doShowTransitionEffect(lCurrentLocationTag, lLocationTag);
				}

				if (action.equals("showLocation") && lShowTransitionEffect) {
					fadeOutLocation(lTagId);
					IECaseComponent lCaseComponent = getNavigationComponent();
					sSpring.setRunTagStatus(lCaseComponent, lLocationTag.getAttribute(AppConstants.keyId), lLocationTag.getName(),
							AppConstants.statusKeyStartOfFadeOut, AppConstants.statusValueTrue, null, true, null, AppConstants.statusTypeRunGroup, true, false);
					// postpone showing location till fadeInLocation is sent
				}
				else {
	//				emergoEventToClient("parallax", "", "on", "true");

					// remove conversation because conversation is on location
					onAction(sender, "finishConversation", null);

					// remove tablet because student wants to see location
					onAction(sender, "endTablet", null);

					// remove note because note is contextualized, endNote saves current note
					onAction(sender, "endNote", null);

					// remove dashboard because student wants to see location
					onAction(sender, "endDashboard", null);

					// end possibly started tutorial
					CRunTutorialOunl runTutorial = (CRunTutorialOunl)CDesktopComponents.vView().getComponent("runTutorial");
					if (runTutorial != null) {
						endApp(runTutorial);
					}

					// end possibly started ispot
					CRunIspotOunl runIspot = (CRunIspotOunl)CDesktopComponents.vView().getComponent("runIspot");
					if (runIspot != null) {
						endApp(runIspot);
					}
	
					// NOTE don't remove alerts, because an alert can be given when changing location
	
					IECaseComponent lCaseComponent = getNavigationComponent();
					if ((currentLocationTagId != null) && (!currentLocationTagId.equals(""))) {
						// deselect old location 
						// don't set 'opened' to false because location is singleopen for passage
						IXMLTag lLocationTagOld = getLocation(currentLocationTagId);
						String[] lKeysOld = new String[] { AppConstants.statusKeySelected};
						String[] lValuesOld = new String[] { AppConstants.statusValueFalse};
						sSpring.setRunTagStatus(lCaseComponent, lLocationTagOld.getAttribute(AppConstants.keyId), lLocationTagOld.getName(),
								lKeysOld[0], lValuesOld[0], null, true, null, AppConstants.statusTypeRunGroup, true, false);
					}
	
					currentLocationTagId = lTagId;
					String[] lKeys = new String[] { AppConstants.statusKeySelected, AppConstants.statusKeyOpened };
					String[] lValues = new String[] { AppConstants.statusValueTrue, AppConstants.statusValueTrue };
					sSpring.setRunTagStatus(lCaseComponent, lLocationTag.getAttribute(AppConstants.keyId), lLocationTag.getName(),
							lKeys[0], lValues[0], null, true, null, AppConstants.statusTypeRunGroup, true, false);
					sSpring.setRunTagStatus(lCaseComponent, lLocationTag.getAttribute(AppConstants.keyId), lLocationTag.getName(),
							lKeys[1], lValues[1], null, true, null, AppConstants.statusTypeRunGroup, true, false);
	
					// show location background
					setViewAreaStatus(sender, "showLocation", lLocationTag);
	
					// update breadcrumbs
					if (breadcrumbsarea != null && getNestedLocationTags().size() > 1)
						breadcrumbsarea.showBreadcrumb(lLocationTag);
					
					setNoteTag(lCaseComponent, lLocationTag);
	
					// show location actions
					onAction(getId(), "showLocationActions", lLocationTag);
	
	//				emergoEventToClient("client", action, lLocationTag.getChildValue("pid"));
	
					initLocationComponents(lLocationTag);
	
					hideShowLocationComponents(lLocationTag);
	
					if (lCurrentLocationTag == null || (action.equals("fadeInLocation") && lShowTransitionEffect)) {
						//NOTE fade in runWnd if first location is shown or if transition effect should be shown
						fadeInLocation(currentLocationTagId);
						sSpring.setRunTagStatus(lCaseComponent, lLocationTag.getAttribute(AppConstants.keyId), lLocationTag.getName(),
								AppConstants.statusKeyStartOfFadeIn, AppConstants.statusValueTrue, null, true, null, AppConstants.statusTypeRunGroup, true, false);
					}
				}
			}
			if (!initLocation) {
				initLocation = true;

				//restore dashboard if opened and present
				IECaseComponent lCaseComponent = sSpring.getCaseComponent("dashboard", "");
				if (getCurrentCaseComponentStatusValue(lCaseComponent, AppConstants.statusKeyOpened).equals(AppConstants.statusValueTrue) &&
						!getCurrentCaseComponentStatusValue(lCaseComponent, AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse)) {				
					onTimedAction(sender, "restoreDashboard", null);
				}

				//restore profile if opened and present
				lCaseComponent = sSpring.getCaseComponent("profile", "");
				if (getCurrentCaseComponentStatusValue(lCaseComponent, AppConstants.statusKeyOpened).equals(AppConstants.statusValueTrue) &&
						!getCurrentCaseComponentStatusValue(lCaseComponent, AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse)) {
					onTimedAction(sender, "restoreProfile", null);
				}

				//restore tablet if opened and present
				IECaseComponent lTabletCaseComponent = sSpring.getCaseComponent("empack", "");
				boolean lTabletCaseComponentIsOpened = false;
				if (getCurrentCaseComponentStatusValue(lTabletCaseComponent, AppConstants.statusKeyOpened).equals(AppConstants.statusValueTrue) &&
						!getCurrentCaseComponentStatusValue(lTabletCaseComponent, AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse)) {				
					lTabletCaseComponentIsOpened = true;
					onTimedAction(sender, "restoreTablet", null);
				}

				//restore tablet apps if opened and present
				String[] lTabletAppsCodes = new String[]
						{
								"assessments",
								"directing",
								"dragdropforms",
								"editforms",
								"googlemaps",
								"graphicalforms",
								"logbook",
								"mail",
								"memos",
								"references",
								"selectionforms",
								"tasks",
								"tests",
								"videomanual",
								"videosceneselector"
						};
				String lAction = "";
				if (lTabletCaseComponentIsOpened) {
					lAction = "restoreTabletApp";
				}
				else {
					lAction = "restoreApp";
				}
				for (int i=0;i<lTabletAppsCodes.length;i++) {
					List<IECaseComponent> lTabletAppCaseComponents = sSpring.getCaseComponentsByComponentCode(lTabletAppsCodes[i]);
					for (IECaseComponent lTabletAppCaseComponent : lTabletAppCaseComponents) {
						if (getCurrentCaseComponentStatusValue(lTabletAppCaseComponent, AppConstants.statusKeyOpened).equals(AppConstants.statusValueTrue) &&
								!getCurrentCaseComponentStatusValue(lTabletAppCaseComponent, AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse)) {				
							List<String> lCacIdAndTagId = new ArrayList<String>();
							lCacIdAndTagId.add("" + lTabletAppCaseComponent.getCacId());
							lCacIdAndTagId.add("0");
							onTimedAction(sender, lAction, lCacIdAndTagId);
						}
					}
				}

				//restore note if opened and present
				lCaseComponent = sSpring.getCaseComponent("note", "");
				if (getCurrentCaseComponentStatusValue(lCaseComponent, AppConstants.statusKeyOpened).equals(AppConstants.statusValueTrue) &&
						!getCurrentCaseComponentStatusValue(lCaseComponent, AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse)) {				
					onTimedAction(sender, "restoreNote", null);
				}

			}
		}
		else if (action.equals("showLocationActions")) {
			// shows location action selectors within popup window above view area
			// if only one action selector, the popup window isn't shown. the
			// location action is shown immediately

			// get location actions
			List<Object> lLocationActions = getLocationActions((IXMLTag) status);
			if (lLocationActions.size() == 1) {
				List<Object> lLocationAction = (List) lLocationActions.get(0);
				onAction(getId(), "showLocationAction", lLocationAction);
			}
			if (lLocationActions.size() > 1) {
				Map<String,Object> lParams = new HashMap<String,Object>();
				lParams.put("item", lLocationActions);
				CDesktopComponents.vView().modalPopup(VView.v_run_location_actions, null, lParams, "center");
			}
		}
		else if (action.equals("showLocationAction")) {
			IECaseComponent lCaseComponent = (IECaseComponent) ((List) status).get(0);
			String lCode = lCaseComponent.getEComponent().getCode();
			if (lCode.equals("conversations")) {
				// kill pending fragment
				CRunConversations lComponent = (CRunConversations)CDesktopComponents.vView().getComponent("runConversations");
				if (lComponent != null) {
					lComponent.removeFragmentView();
				}
			}
			else {
				// kill conversation
				onAction(sender, "endConversation", null);
			}
			// remove location app
			if (locationActRunComponent != null) {
				locationActRunComponent.detach();
				locationActRunComponent = null;
			}
			// conversation interaction can exist too
			if (conversationInteraction != null) {
				conversationInteraction.detach();
				conversationInteraction = null;
			}
			// shows location app
			viewAreaActCaseComponent = (IECaseComponent) ((List) status).get(0);
			viewAreaActCaseComponentTag = (IXMLTag) ((List) status).get(1);

			sSpring.setRunComponentStatus(lCaseComponent,
					AppConstants.statusKeySelected, AppConstants.statusValueTrue,
					true, AppConstants.statusTypeRunGroup, true);

			locationActRunComponent = null;
			if (lCode.equals("conversations")) {
				locationActRunComponent = createConversationsComponent(lCaseComponent);
			}
			else if (lCode.equals("references")) {
				locationActRunComponent = createReferencesComponent(lCaseComponent);
				locationActRunComponent.setSclass(getClassName() + "_app");
			}
			else if (lCode.equals("assessments")) {
				//NOTE no difference in tablet or location app anymore
				locationActRunComponent = createAssessmentsComponent(lCaseComponent, true);
				locationActRunComponent.setSclass(getClassName() + "_app");
			}
			else if (lCode.equals("tutorial")) {
				locationActRunComponent = createTutorialComponent(lCaseComponent, "");
			}
			else if (lCode.equals("ispot")) {
				locationActRunComponent = createIspotComponent(lCaseComponent);
			}
			else if (lCode.equals("editforms")) {
				locationActRunComponent = createEditformsComponent(lCaseComponent);
				locationActRunComponent.setSclass(getClassName() + "_app");
			}
			else if (lCode.equals("dragdropforms")) {
				locationActRunComponent = createDragdropformsComponent(lCaseComponent);
				locationActRunComponent.setSclass(getClassName() + "_app");
			}
			else if (lCode.equals("selectionforms")) {
				locationActRunComponent = createSelectionformsComponent(lCaseComponent);
				locationActRunComponent.setSclass(getClassName() + "_app");
			}
			else if (lCode.equals("videosceneselector")) {
				locationActRunComponent = createVideosceneselectorComponent(lCaseComponent);
				locationActRunComponent.setSclass(getClassName() + "_app");
			}
			else if (lCode.equals("tests")) {
				locationActRunComponent = createTestsComponent(lCaseComponent);
				locationActRunComponent.setSclass(getClassName() + "_app");
			}
			else if (lCode.equals("graphicalforms")) {
				locationActRunComponent = createGraphicalformsComponent(lCaseComponent);
				locationActRunComponent.setSclass(getClassName() + "_app");
			}
			if (locationActRunComponent != null)
				appendChild(locationActRunComponent);

			sSpring.setRunComponentStatus(lCaseComponent,
					AppConstants.statusKeyOpened, AppConstants.statusValueTrue,
					true, AppConstants.statusTypeRunGroup, true);
			if (!lCode.equals("conversations"))
				setNoteCaseComponent(lCaseComponent);
		}

		else if (action.equals("showTablet") || action.equals("restoreTablet")) {
			// set tablet button status
			if (runTabletBtn != null && runTabletBtn.getStatus().equals("selected")) {
				runTabletBtn.setVisible(false);
			}
			// show tablet component
			String lCode = "empack";
			IECaseComponent lCaseComponent = sSpring.getCaseComponent(lCode, "");
			CRunComponent lRunComponent = createTabletComponent(lCaseComponent);
			appendChild(lRunComponent);

			if (action.equals("showTablet")) {
				sSpring.setRunComponentStatus(lCaseComponent,
						AppConstants.statusKeySelected, AppConstants.statusValueTrue,
						true, AppConstants.statusTypeRunGroup, true);
				sSpring.setRunComponentStatus(lCaseComponent,
						AppConstants.statusKeyOpened, AppConstants.statusValueTrue,
						true, AppConstants.statusTypeRunGroup, true);
			}
			
			setNoteCaseComponent(lCaseComponent);
			
//			emergoEventToClient("parallax", "", "on", "false");
		}

		else if (action.equals("endTablet")) {
			// remove tablet app
			if (tabletActRunComponent != null) {
				tabletActRunComponent.cleanup();
				tabletActRunComponent.detach();
				tabletActRunComponent = null;
			}
			// remove tablet
			CRunComponent lRunComponent = (CRunComponent)CDesktopComponents.vView().getComponent("runTablet");
			if (lRunComponent != null)
				lRunComponent.detach();
			// restore tablet button
			if (runTabletBtn != null && runTabletBtn.getStatus().equals("selected")) {
				runTabletBtn.setStatus("active");
				runTabletBtn.setVisible(true);
			}
			// restore note context
			onAction(sender, "restoreNoteContext", null);
		}

		else if (action.equals("showTabletApp") || action.equals("restoreTabletApp")) {
			// remove tablet app
			if (tabletActRunComponent != null) {
				tabletActRunComponent.detach();
				tabletActRunComponent = null;
			}
			// shows tablet app in tablet
			CRunChoiceHoverBtns lTabBtns = (CRunChoiceHoverBtns) CDesktopComponents.vView().getComponent("runTabletAppBtns");
			if (lTabBtns != null)
				lTabBtns.deselectBtn("");
			else {
				// tabletapp started from clickable object on location
				Object lClickBtn = CDesktopComponents.vView().getComponent(sender);
				if ((lClickBtn != null) && (lClickBtn.getClass() == CRunHoverBtn.class))
					((CRunHoverBtn)lClickBtn).deselect();
			}
			IECaseComponent lCaseComponent = sSpring.getCaseComponent(Integer.parseInt(((List<String>)status).get(0)));
			setNoteCaseComponent(lCaseComponent);
			if (action.equals("showTabletApp")) {
				sSpring.setRunComponentStatus(lCaseComponent,
						AppConstants.statusKeySelected, AppConstants.statusValueTrue,
						true, AppConstants.statusTypeRunGroup, true);
			}

			String lCode = lCaseComponent.getEComponent().getCode();
			tabletActRunComponent = null;
			if (lCode.equals("references"))
				tabletActRunComponent = createReferencesComponent(lCaseComponent);
			else if (lCode.equals("mail"))
				tabletActRunComponent = createMailComponent(lCaseComponent);
			else if (lCode.equals("assessments"))
				tabletActRunComponent = createAssessmentsComponent(lCaseComponent, true);
			else if (lCode.equals("logbook"))
				tabletActRunComponent = createLogbookComponent(lCaseComponent);
			else if (lCode.equals("googlemaps"))
				tabletActRunComponent = createGooglemapsComponent(lCaseComponent);
			else if (lCode.equals("canon"))
				tabletActRunComponent = createCanonComponent(lCaseComponent);
			else if (lCode.equals("canonresult"))
				tabletActRunComponent = createCanonResultComponent(lCaseComponent);
			else if (lCode.equals("tasks"))
				tabletActRunComponent = createTasksComponent(lCaseComponent);
			else if (lCode.equals("directing"))
				tabletActRunComponent = createDirectingComponent(lCaseComponent);
			else if (lCode.equals("memos"))
				tabletActRunComponent = createMemosComponent(lCaseComponent);
			else if (lCode.equals("videomanual"))
				tabletActRunComponent = createVideomanualComponent(lCaseComponent);
			else if (lCode.equals("editforms"))
				tabletActRunComponent = createEditformsComponent(lCaseComponent);
			else if (lCode.equals("dragdropforms"))
				tabletActRunComponent = createDragdropformsComponent(lCaseComponent);
			else if (lCode.equals("selectionforms"))
				tabletActRunComponent = createSelectionformsComponent(lCaseComponent);
			else if (lCode.equals("videosceneselector"))
				tabletActRunComponent = createVideosceneselectorComponent(lCaseComponent);
			else if (lCode.equals("tests"))
				tabletActRunComponent = createTestsComponent(lCaseComponent);
			else if (lCode.equals("graphicalforms"))
				tabletActRunComponent = createGraphicalformsComponent(lCaseComponent);

			if (tabletActRunComponent != null) {
				appendChild(tabletActRunComponent);
				tabletActRunComponent.setSclass(getClassName() + "_tablet_app");
			}

			if (action.equals("showTabletApp")) {
				sSpring.setRunComponentStatus(lCaseComponent,
						AppConstants.statusKeyOpened, AppConstants.statusValueTrue,
						true, AppConstants.statusTypeRunGroup, true);
			}
		}

		else if (action.equals("showApp") || action.equals("restoreApp")) {
			// kill conversation
			onAction(sender, "endConversation", null);
			// remove app
			if (actRunComponent != null) {
				actRunComponent.detach();
				actRunComponent = null;
			}
			IECaseComponent lCaseComponent = sSpring.getCaseComponent(Integer.parseInt(((List<String>)status).get(0)));
			if (lCaseComponent != null) {
				setNoteCaseComponent(lCaseComponent);
				if (action.equals("showApp")) {
					sSpring.setRunComponentStatus(lCaseComponent,
							AppConstants.statusKeySelected, AppConstants.statusValueTrue,
							true, AppConstants.statusTypeRunGroup, true);
				}

				String lCode = lCaseComponent.getEComponent().getCode();
				actRunComponent = null;
				if (lCode.equals("tutorial"))
					actRunComponent = createTutorialComponent(lCaseComponent, ((List<String>)status).get(1));
				else if (lCode.equals("ispot"))
					actRunComponent = createIspotComponent(lCaseComponent);
				else if (lCode.equals("references")) {
					actRunComponent = createReferencesComponent(lCaseComponent);
					actRunComponent.setSclass(getClassName() + "_tablet_app");
				}
				else if (lCode.equals("assessments")) {
					//NOTE no difference in tablet or location app anymore
					actRunComponent = createAssessmentsComponent(lCaseComponent, true);
					actRunComponent.setSclass(getClassName() + "_tablet_app");
				}
				else if (lCode.equals("editforms")) {
					//NOTE no difference in tablet or location app anymore
					actRunComponent = createEditformsComponent(lCaseComponent);
					actRunComponent.setSclass(getClassName() + "_tablet_app");
				}
				else if (lCode.equals("dragdropforms")) {
					//NOTE no difference in tablet or location app anymore
					actRunComponent = createDragdropformsComponent(lCaseComponent);
					actRunComponent.setSclass(getClassName() + "_tablet_app");
				}
				else if (lCode.equals("selectionforms")) {
					//NOTE no difference in tablet or location app anymore
					actRunComponent = createSelectionformsComponent(lCaseComponent);
					actRunComponent.setSclass(getClassName() + "_tablet_app");
				}
				else if (lCode.equals("videosceneselector")) {
					//NOTE no difference in tablet or location app anymore
					actRunComponent = createVideosceneselectorComponent(lCaseComponent);
					actRunComponent.setSclass(getClassName() + "_tablet_app");
				}
				else if (lCode.equals("tests")) {
					//NOTE no difference in tablet or location app anymore
					actRunComponent = createTestsComponent(lCaseComponent);
					actRunComponent.setSclass(getClassName() + "_tablet_app");
				}
				else if (lCode.equals("graphicalforms")) {
					//NOTE no difference in tablet or location app anymore
					actRunComponent = createGraphicalformsComponent(lCaseComponent);
					actRunComponent.setSclass(getClassName() + "_tablet_app");
				}
				if (actRunComponent != null) {
					appendChild(actRunComponent);
					// app possibly started from clickable object on location
					Object lClickBtn = CDesktopComponents.vView().getComponent(sender);
					if ((lClickBtn != null) && (lClickBtn.getClass() == CRunHoverBtn.class))
						((CRunHoverBtn)lClickBtn).deselect();
				}
				if (action.equals("showApp")) {
					sSpring.setRunComponentStatus(lCaseComponent,
							AppConstants.statusKeyOpened, AppConstants.statusValueTrue,
							true, AppConstants.statusTypeRunGroup, true);
				}
			}
		}

		else if (action.equals("endComponent")) {
			CRunComponent lRunComponent = (CRunComponent)status;
			if (lRunComponent != null) {
				if (lRunComponent.getId().equals("runTablet")) {
					// tablet closed
					onAction(sender, "endTablet", null);
				}
				else if (lRunComponent.getId().equals("runDashboard")) {
					// dashboard closed
					onAction(sender, "endDashboard", null);
				}
				else {
					// cleanup component
					lRunComponent.cleanup();
					// app on tablet closed, set note case component to tablet
					lRunComponent = (CRunComponent)CDesktopComponents.vView().getComponent("runTablet");
					if (lRunComponent != null) {
						// NOTE that is, if tablet exists
						IECaseComponent lCaseComponent = sSpring.getCaseComponent("empack", "");
						setNoteCaseComponent(lCaseComponent);
					}
					else {
						// NOTE if tablet does noet exist the component was opened as a location action, so set note context to current location
						IECaseComponent lCaseComponent = getNavigationComponent();
						if (lCaseComponent != null) {
							IXMLTag lLocationTag = getLocation(currentLocationTagId);
							if (lLocationTag == null)
								setNoteCaseComponent(lCaseComponent);
							else
								setNoteTag(lCaseComponent, lLocationTag);
						}
					}
				}
			}
		}
		else if (action.equals("endTasks")) {
			// tasks is now shown on tablet, so treat it as a tablet app
			action = "endComponent";
			onAction(sender, action, status);
		}

		else if (action.equals("showNote") || action.equals("restoreNote")) {
			// set note button status
			if (runNoteBtn != null && runNoteBtn.getStatus().equals("selected")) {
				runNoteBtn.setVisible(false);
			}
			// show note component
			CRunArea lRunComponent = (CRunNote)CDesktopComponents.vView().getComponent("runNote");
			if (lRunComponent == null) {
				lRunComponent = createNoteComponent(null);
				appendChild(lRunComponent);
			}
			lRunComponent.setVisible(true);
			IECaseComponent lCaseComponent = getActiveCaseComponent("note");
			if (lCaseComponent != null) {
				if (action.equals("showNote")) {
					sSpring.setRunComponentStatus(lCaseComponent,
							AppConstants.statusKeySelected,
							AppConstants.statusValueTrue, true, AppConstants.statusTypeRunGroup, true);
					sSpring.setRunComponentStatus(lCaseComponent,
							AppConstants.statusKeyOpened,
							AppConstants.statusValueTrue, true, AppConstants.statusTypeRunGroup, true);
				}
				setNoteContent();
			}
		}
		else if (action.equals("createNote")) {
			CRunArea lRunComponent = (CRunNote)CDesktopComponents.vView().getComponent("runNote");
			if (lRunComponent == null) {
				lRunComponent = createNoteComponent(null);
				appendChild(lRunComponent);
			}
			lRunComponent.setVisible(false);
		}
		else if (action.equals("endNote")) {
			// hide note
			CRunArea lRunArea = (CRunArea)CDesktopComponents.vView().getComponent("runNote");
			if (lRunArea != null && lRunArea.isVisible()) {
				lRunArea.setVisible(false);
				IECaseComponent lCaseComponent = getActiveCaseComponent("note");
				if (lCaseComponent != null)
					sSpring.setRunComponentStatus(lCaseComponent,
							AppConstants.statusKeyOpened,
							AppConstants.statusValueFalse, true, AppConstants.statusTypeRunGroup, true);
			}
			// restore note button
			if (runNoteBtn != null && runNoteBtn.getStatus().equals("selected")) {
				runNoteBtn.setStatus("active");
				runNoteBtn.setVisible(true);
			}
		}
		else if (action.equals("restoreNoteContext")) {
			// restore note context
			CRunConversations lComponent = (CRunConversations)CDesktopComponents.vView().getComponent("runConversations");
			if (lComponent != null && lComponent.getCurrentConversationTag() != null) {
				// NOTE if conversation is running, set note context to current conversation tag.
				setNoteTag(lComponent.getCaseComponent(), lComponent.getCurrentConversationTag());
			}
			else {
				// NOTE else set it to current location
				IECaseComponent lCaseComponent = getNavigationComponent();
				if (lCaseComponent != null) {
					IXMLTag lLocationTag = getLocation(currentLocationTagId);
					if (lLocationTag == null)
						setNoteCaseComponent(lCaseComponent);
					else
						setNoteTag(lCaseComponent, lLocationTag);
				}
			}
		}

		else if (action.equals("showDashboard") || action.equals("restoreDashboard")) {
			// set dashboard button status
			if (runDashboardBtn != null && runDashboardBtn.getStatus().equals("selected")) {
				runDashboardBtn.setVisible(false);
			}
			String lCode = "dashboard";
			IECaseComponent lCaseComponent = sSpring.getCaseComponent(lCode, "");
			CRunComponent lRunComponent = (CRunComponent)CDesktopComponents.vView().getComponent("runDashboard");
			if (lRunComponent == null) {
				// create dashboard component
				lRunComponent = createDashboardComponent(lCaseComponent);
				appendChild(lRunComponent);
			}
			else {
				// show dashboard component
				lRunComponent.setVisible(true);
			}
			
			if (action.equals("showDashboard")) {
				sSpring.setRunComponentStatus(lCaseComponent,
						AppConstants.statusKeySelected, AppConstants.statusValueTrue,
						true, AppConstants.statusTypeRunGroup, true);
				sSpring.setRunComponentStatus(lCaseComponent,
						AppConstants.statusKeyOpened, AppConstants.statusValueTrue,
						true, AppConstants.statusTypeRunGroup, true);
			}
			setNoteCaseComponent(lCaseComponent);
			
//			emergoEventToClient("parallax", "", "on", "false");
		}

		else if (action.equals("endDashboard")) {
			// remove dashboard
			CRunComponent lRunComponent = (CRunComponent)CDesktopComponents.vView().getComponent("runDashboard");
			if (lRunComponent != null && lRunComponent.isVisible()) {
				//NOTE don't detach dashboard component but hide it, so it stays available to handle status changes
				lRunComponent.setVisible(false);
				String lCode = "dashboard";
				IECaseComponent lCaseComponent = sSpring.getCaseComponent(lCode, "");
				sSpring.setRunComponentStatus(lCaseComponent,
						AppConstants.statusKeyOpened, AppConstants.statusValueFalse,
						true, AppConstants.statusTypeRunGroup, true);
			}
			// restore dashboard button
			if (runDashboardBtn != null && runDashboardBtn.getStatus().equals("selected")) {
				runDashboardBtn.setStatus("active");
				runDashboardBtn.setVisible(true);
			}
			// restore note context
			onAction(sender, "restoreNoteContext", null);
		}

		else if (action.equals("showAlert")) {
		}
		else if (action.equals("endAlert")) {
			CRunAlert lRunAlert = (CRunAlert)status;
			if (lRunAlert != null) {
				if (lRunAlert.hasPreviousContent())
					lRunAlert.restoreContent();
				else
					lRunAlert.detach();
			}
		}
		else if (action.equals("startConversation")) {
			conversationStarted = true;
			// if conversation interaction is shown, remove it
			onAction(sender, "endConversation", null);
			// show conversation interaction
			// NOTE for tutorial, conversation interaction already is created because it has to have another parent as runWnd
			// Then status is equal to "noConversationInteraction"
			if (!(status != null && status instanceof String && ((String)status).equals("noConversationInteraction"))) {
				CRunConversations lConversations = null;
				if (status != null && status instanceof CRunConversations) {
					lConversations = (CRunConversations)status;
				}
				conversationInteraction = createConversationInteraction(lConversations);
				appendChild(conversationInteraction);
			}
			if (conversationInteraction != null) {
				conversationInteraction.setVisible(false);
			}
			boolean memoUsedAndVisible = isComponentUsedAndVisible("memo");
			runMemoBtn = (CRunMemoBtn)CDesktopComponents.vView().getComponent("runMemoBtn");
			if (memoUsedAndVisible && runMemoBtn != null) {
				if (getCurrentCaseComponentStatusValue("memo", "", AppConstants.statusKeyAlwaysrecord).equals(AppConstants.statusValueTrue)) {
					runMemoBtn.setStatus("selected");
				}
				else {
					runMemoBtn.setStatus("active");
				}
			}
		}
		else if (action.equals("finishConversation")) {
			// set status of current conversation to finished 
			CRunConversations lComponent = (CRunConversations)CDesktopComponents.vView().getComponent("runConversations");
			if (lComponent != null) {
				lComponent.endCurrentConversation();
			}
			Div lRunExitLocation = (Div)CDesktopComponents.vView().getComponent("runExitLocation");
			if (lRunExitLocation != null) {
				// NOTE If finish conversation hide exit location button.
				lRunExitLocation.setVisible(false);
			}
		}
		else if (action.equals("playFragment")) {
			boolean toggleOn = false;
			runMemoBtn = (CRunMemoBtn)CDesktopComponents.vView().getComponent("runMemoBtn");
			if (runMemoBtn != null) {
				toggleOn = runMemoBtn.isStatus("selected");
			}
			if (toggleOn) {
				//  run conversations should be present, so timed action
				onTimedAction(sender, "saveMemo", status);
			}
			String lFileExtension = (String)status;
			if ((lFileExtension.equals("flv") || lFileExtension.equals("mp4")) && conversationStarted) {
				// if flv is played by flash player and conversation is started
				doBeforePlayingFlv();
			}
			conversationStarted = false;
		}
		else if (action.equals("endConversation")) {
			// kill pending fragment and remove conversation component
			CRunConversations lComponent = (CRunConversations)CDesktopComponents.vView().getComponent("runConversations");
			if (lComponent != null) {
				lComponent.removeFragmentView();
				lComponent.detach();
			}

			// if conversation interaction is shown, remove it
			CRunConversationInteraction lConversationInteraction = (CRunConversationInteraction)CDesktopComponents.vView().getComponent("runConversationInteraction");
			if (lConversationInteraction != null)
				lConversationInteraction.detach();
			conversationInteraction = null;
			runMemoBtn = (CRunMemoBtn)CDesktopComponents.vView().getComponent("runMemoBtn");
			if (runMemoBtn != null) {
				runMemoBtn.setStatus("inactive");
			}
		}
		else if (action.equals("toggleMemo")) {
			boolean toggleOn = false;
			runMemoBtn = (CRunMemoBtn)CDesktopComponents.vView().getComponent("runMemoBtn");
			if (runMemoBtn != null) {
				toggleOn = runMemoBtn.isStatus("selected");
			}
			String lCode = "memo";
			IECaseComponent lCaseComponent = sSpring.getCaseComponent(lCode, "");
			if (lCaseComponent != null) {
				if (toggleOn) {
					sSpring.setRunComponentStatus(lCaseComponent,
							AppConstants.statusKeyStarted,
							AppConstants.statusValueTrue, true, AppConstants.statusTypeRunGroup, true);
				}
				else {
					sSpring.setRunComponentStatus(lCaseComponent,
							AppConstants.statusKeyFinished,
							AppConstants.statusValueTrue, true, AppConstants.statusTypeRunGroup, true);
				}
			}
			if (toggleOn) {
				onAction(sender, "saveMemo", status);
			}
		}
		else if (action.equals("saveMemo")) {
			CRunConversations lComponent = (CRunConversations)CDesktopComponents.vView().getComponent("runConversations");
			if (lComponent != null) {
				IECaseComponent lCac = lComponent.getCaseComponent();
				IXMLTag lConversationTag = lComponent.getCurrentConversationTag();
				IXMLTag lFragmentTag = lComponent.getCurrentFragmentTag();
				CRunMemo lRunMemo = createMemoComponent();
				lRunMemo.saveMemo(lCac, lConversationTag, lFragmentTag);
			}
		}
		else if (action.equals("restoreProfile")) {
			CRunProfile runProfile = (CRunProfile)CDesktopComponents.vView().getComponent("runProfile");
			if (runProfile != null) {
				runProfile.restore();
			}
		}
		else if (action.equals("onExternalEvent")) {
			String[] lStatus = (String[]) status;
			String lSender = lStatus[0];
			String lReceiver = lStatus[1];
			String lEvent = lStatus[2];
			String lEventData = lStatus[3];
			if (sender.matches("jwplayer|videojsplayer|textplayer")) {
				if (lReceiver.equals("runIspot")) {
					CRunIspot lComponent = (CRunIspot)CDesktopComponents.vView().getComponent(lReceiver);
					if (lComponent != null) {
						lComponent.onAction(sender, action, status);
					}
				}
				else if (lReceiver.equals("runVideosceneselector")) {
					CRunVideosceneselector lComponent = (CRunVideosceneselector)CDesktopComponents.vView().getComponent(lReceiver);
					if (lComponent != null) {
						lComponent.onAction(sender, action, status);
					}
				}
				else if (lReceiver.equals("pvToolkit")) {
					CRunPVToolkit lComponent = (CRunPVToolkit)CDesktopComponents.vView().getComponent(lReceiver);
					if (lComponent != null) {
						lComponent.onAction(sender, action, status);
					}
				}
				else {
					CRunComponent lComponent = (CRunComponent)CDesktopComponents.vView().getComponent(lReceiver);
					if (lComponent != null) {
						lComponent.onAction(sender, action, status);
					}
				}
				if (lEvent.equals("onTime")) {
					// if flv is played by flash player the player sends an onTime, in run_flash_fr.zul only one onTime is sent
					doAfterPlayingFlv();
				} 
				if (lEvent.equals("onError")) {
					// if flv cannot be played by flash player the player sends an onError, in run_flash_fr.zul only one onTime is sent
					doAfterPlayingFlv();
				} 
			}
			else if (sender.matches("scriptcam")) {
				if (lEvent.equals("onShowRecord") ||
						lEvent.equals("onRecordComplete")) {
					CRunConversations lComponent = (CRunConversations)CDesktopComponents.vView().getComponent(lReceiver);
					if (lComponent != null) {
						lComponent.onAction(sender, action, status);
					}
				} 
			}
			else if (sender.matches("webcam")) {
				Component lComponent = CDesktopComponents.vView().getComponent(lReceiver);
				if (lComponent != null) {
					if (lComponent instanceof CRunIspot) {
						((CRunIspot)lComponent).onAction(sender, action, status);
					}
					if (lComponent instanceof CRunConversations) {
						((CRunConversations)lComponent).onAction(sender, action, status);
					}
				}
			}
			if (sender.matches("runWnd")) {
				if (lReceiver.equals("runWnd")) {
					if (lEvent.equals("onFinishedFadeOut")) {
						//if fade out of previous location is finished fade in next location whose id is given by lEventData
						onAction(getId(), "fadeInLocation", lEventData);
					}
					else if (lEvent.equals("onFinishedFadeIn")) {
						//do nothing
					}
				}
				super.onAction(sender, action, status);
			}
			else {
				super.onAction(sender, action, status);
			}
		}
		else if (action.equals("close")) {
			saveAllStatus();
		}
		else {
			super.onAction(sender, action, status);
		}
	}
	
	/**
	 * Hide location etc till flash player starts playing.
	 */
	protected void doBeforePlayingFlv() {
		setStyle("cursor:wait;");
		if (locationarea != null) {
			locationarea.setVisible(false);
		}
		if (locationobjects != null) {
			locationobjects.setVisible(false);
		}
		if (conversationInteraction != null) {
			conversationInteraction.setInteractionHidden(true); 
			conversationInteraction.setVisible(false);
		}
	}

	/**
	 * Show location etc if flash player starts playing.
	 */
	protected void doAfterPlayingFlv() {
		if (locationarea != null && !locationarea.isVisible()) {
			locationarea.setVisible(true);
		}
		if (locationobjects != null) {
			locationobjects.setVisible(true);
		}
		if (conversationInteraction != null && !conversationInteraction.isVisible()) {
			CRunConversations lComponent = (CRunConversations)CDesktopComponents.vView().getComponent("runConversations");
			if (lComponent != null) {
				conversationInteraction.setVisible(conversationInteraction.isIsInteractionShown() && conversationInteraction.isInteractionHidden() && !lComponent.isShowInteractionAfterIntroduction());
			}
		}

		if (conversationInteraction != null) {
			conversationInteraction.setInteractionHidden(false);
		}

		setStyle("cursor:default;");
	}

	/**
	 * Status change. Is called by SSpring class if script actions have changed status. This status change should
	 * be reflected within the player. Status changes are saved in a buffer and handled by method checkStatusChange.
	 * Only status changes of statusKeyPresent, statusKeyAccessible, statusKeyOpened, statusKeySent and
	 * statusKeyStarted are buffered.
	 *
	 * @param aCaseComponent the a case component
	 * @param aDataTag the a data tag
	 * @param aStatusTag the a status tag
	 * @param aTriggeredReference.getStatusValue() the a status key
	 * @param aStatusValue the a status value
	 * @param aFromScript the a from script, whether this method is called from within script, so is a result of a script action
	 */
	@Override
	public void statusChange(IECaseComponent aCaseComponent, IXMLTag aDataTag, IXMLTag aStatusTag, String aStatusKey, String aStatusValue, boolean aFromScript) {
		if (aStatusKey.equals(AppConstants.statusKeyPresent) ||
				aStatusKey.equals(AppConstants.statusKeyAccessible) ||
				aStatusKey.equals(AppConstants.statusKeyOpened) ||
				aStatusKey.equals(AppConstants.statusKeySelected) ||
				aStatusKey.equals(AppConstants.statusKeySent) ||
				aStatusKey.equals(AppConstants.statusKeyStarted) ||
				aStatusKey.equals(AppConstants.statusKeyFinished) ||
				aStatusKey.equals(AppConstants.statusKeyCorrect) ||
				aStatusKey.equals(AppConstants.statusKeyValue) ||
				aStatusKey.equals(AppConstants.statusKeyHighlight) ||
				aStatusKey.equals(AppConstants.statusKeyDrawAttention) ||
				aStatusKey.equals(AppConstants.statusKeyDeleted) ||
				aStatusKey.equals(AppConstants.statusKeyUpdate) ||
				aStatusKey.equals(AppConstants.statusKeyShowcontrols) ||
				aStatusKey.equals(AppConstants.statusKeyShowinteraction) ||
				aStatusKey.equals(AppConstants.statusKeyDraggable) ||
				aStatusKey.equals(AppConstants.statusKeyDroppable) ||
				aStatusKey.equals(AppConstants.statusKeyReset) ||
				aStatusKey.equals(AppConstants.statusKeyXposition) ||
				aStatusKey.equals(AppConstants.statusKeyYposition)) {
			CRunStatusChange lRunStatusChange = new CRunStatusChange();
			lRunStatusChange.setCaseComponent(aCaseComponent);
			lRunStatusChange.setDataTag(aDataTag);
			lRunStatusChange.setStatusTag(aStatusTag);
			lRunStatusChange.setStatusKey(aStatusKey);
			lRunStatusChange.setStatusValue(aStatusValue);
			lRunStatusChange.setFromScript(aFromScript);
			if (this.getDesktop().getId().equalsIgnoreCase(Executions.getCurrent().getDesktop().getId()))
				Events.postEvent("onStatusChange", this, lRunStatusChange);
		}
	}

	/**
	 * On status change.
	 *
	 * @param aEvent the a event
	 */
	public void onStatusChange(Event aEvent) {
		checkStatusChange((CRunStatusChange)aEvent.getData());
	}
	
	/**
	 * Saves all pending status.
	 */
	public void saveAllStatus() {
		sSpring.saveRunCaseComponentsStatus();
	}

	/**
	 * Checks status change. This method reads the status changes buffer and updates the player accordingly,
	 * by updating the different components of the player. Such as navigator, empack, tasks or note button, or
	 * location or tablet app buttons, or the content of the run view area.
	 * This method should be made more general in such a way that new Emergo components could be more easily added.
	 * NOTE Always use tag id's as parameters of component methods. Data tag does not have to be enriched with status change yet. 
	 */
	public void checkStatusChange() {
		if (!pIdle || pRunStatusChangeList.size() < 1)
			return;
		pIdle = false;
		while (pRunStatusChangeList.size() >= 1) {
			CRunStatusChange lRunStatusChange = pRunStatusChangeList.get(0);
			pRunStatusChangeList.remove(0);
			checkStatusChange(lRunStatusChange);
		}
		pIdle = true;
	}

	/**
	 * Checks status change.
	 *
	 * @param aRunStatusChange the a run status change
	 */
	protected void checkStatusChange(CRunStatusChange aRunStatusChange) {
		super.checkStatusChange(aRunStatusChange);
		STriggeredReference lTriggeredReference = new STriggeredReference(
				aRunStatusChange.getCaseComponent(),
				aRunStatusChange.getDataTag(),
				aRunStatusChange.getStatusTag(),
				aRunStatusChange.getStatusKey(),
				aRunStatusChange.getStatusValue(),
				aRunStatusChange.isFromScript());
		String lCode = lTriggeredReference.getCaseComponent().getEComponent().getCode();

		if (lCode.equals("case")) {
			handleCase(lTriggeredReference);
		}
		else if (lCode.equals("navigation")) {
			handleNavigation(lTriggeredReference);
		}
		else if (lCode.equals("conversations")) {
			handleConversations(lTriggeredReference);
		}
		else if (lCode.equals("alerts")) {
			handleAlert(lTriggeredReference);
		}
		else if (lCode.equals("mail")) {
			handleMail(lTriggeredReference);
		}
		else if (lCode.equals("empack")) {
			handleTablet(lTriggeredReference);
		}
		else if (lCode.equals("dashboard")) {
			handleDashboard(lTriggeredReference);
		}
		else if (lCode.equals("note")) {
			handleNote(lTriggeredReference);
		}
		else if (lCode.equals("memo")) {
			handleMemo(lTriggeredReference);
		}
		else if (lCode.equals("assessments")) {
			handleAssessments(lTriggeredReference);
		}
		else if (lCode.equals("states")) {
			handleStates(lTriggeredReference);
		}
		else if (lCode.equals("scores")) {
			handleScores(lTriggeredReference);
		}
		else if (lCode.equals("notifications")) {
			handleNotifications(lTriggeredReference);
		}
		else if (lCode.equals("editforms")) {
			handleEditforms(lTriggeredReference);
		}
		else if (lCode.equals("dragdropforms")) {
			handleDragdropforms(lTriggeredReference);
		}
		else if (lCode.equals("selectionforms")) {
			handleSelectionforms(lTriggeredReference);
		}
		else if (lCode.equals("videosceneselector")) {
			handleVideosceneselector(lTriggeredReference);
		}
		else if (lCode.equals("tests")) {
			handleTests(lTriggeredReference);
		}
		else if (lCode.equals("graphicalforms")) {
			handleGraphicalforms(lTriggeredReference);
		}
		else if (lCode.equals(CRunPVToolkit.gCaseComponentNameMessages) ||
				lCode.equals(CRunPVToolkit.gCaseComponentNameProjects) ||
				lCode.equals(CRunPVToolkit.gCaseComponentNameRubrics)) {
			handlePVToolkit(lTriggeredReference);
		}
		else if (caseComponentIsActionOn(lTriggeredReference.getCaseComponent(), "empack")) {
			handleTabletApp(lTriggeredReference);
		}
	}

	protected void handleCase(STriggeredReference aTriggeredReference) {
		if (aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyReset) && aTriggeredReference.getStatusValue().equals(AppConstants.statusValueTrue)) {
			if (isRunStatus(AppConstants.runStatusRun) || isRunStatus(AppConstants.runStatusPreview)) {
				//If in run reset status, also in database
				resetAllStatus();
			}
			//reload run page
			Clients.evalJavaScript("location.reload();");
		}
	}

	protected void handleNavigation(STriggeredReference aTriggeredReference) {
		//NOTE check selected and opened as well, because (older?) script sometimes uses selected and sometimes opened to beam to another location. 
		boolean lLocationSelectedOrOpened = 
				((aTriggeredReference.getStatusKey().equals(AppConstants.statusKeySelected) || aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyOpened)) &&
						aTriggeredReference.getStatusValue().equals(AppConstants.statusValueTrue) &&
						aTriggeredReference.getDataTag() != null &&
						aTriggeredReference.getDataTag().getName().equals("location"));
		boolean lLocationObjectOpened = 
				(aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyOpened) &&
						aTriggeredReference.getStatusValue().equals(AppConstants.statusValueTrue) &&
						aTriggeredReference.getDataTag() != null &&
						(aTriggeredReference.getDataTag().getName().equals("passage") || aTriggeredReference.getDataTag().getName().equals("object") ||
								aTriggeredReference.getDataTag().getName().equals("clickableobject") || aTriggeredReference.getDataTag().getName().equals("panel")));
		boolean lBeamToLocation = lLocationSelectedOrOpened || lLocationObjectOpened;
		boolean lOtherNavigationComponentPresent = 
				(aTriggeredReference.getDataTag() == null &&
				aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyPresent) &&
					aTriggeredReference.getStatusValue().equals(AppConstants.statusValueTrue) &&
					aTriggeredReference.getCaseComponent().getCacId() != getNavigationComponent().getCacId());
		boolean lBeamToOtherNavigationComponent = 
				(lBeamToLocation &&
						aTriggeredReference.getCaseComponent().getCacId() != getNavigationComponent().getCacId());
		boolean lSwitchNavigationComponent = lOtherNavigationComponentPresent || lBeamToOtherNavigationComponent;
		if (lSwitchNavigationComponent) {
			setNavigationComponent(aTriggeredReference.getCaseComponent());
			if (lOtherNavigationComponentPresent) {
				onAction(getId(), "beamLocation", getLocationTag().getAttribute(AppConstants.keyId));
			}
			else if (lBeamToOtherNavigationComponent) {
				onAction(getId(), "beamLocation", aTriggeredReference.getDataTag().getAttribute(AppConstants.keyId));
			}
		}
		else if (lBeamToLocation) {
			// NOTE handle within this class because no navigation component is involved and it may not exist (anymore).
			if (aTriggeredReference.getSetFromScript() && !currentLocationTagId.equals(aTriggeredReference.getDataTag().getAttribute(AppConstants.keyId)))
				// Only beam if it is triggered from script
				onAction(getId(), "beamLocation", aTriggeredReference.getDataTag().getAttribute(AppConstants.keyId));
		}
		else {
			// NOTE delegate because status change may rerender part of component
			CRunLocationAreaOunl lComponent = (CRunLocationAreaOunl)CDesktopComponents.vView().getComponent("runLocationArea");
			if (lComponent != null) {
				lComponent.handleStatusChange(aTriggeredReference);
			}
			delegateHandling(aTriggeredReference, "runNavigation");
		}
	}

	protected void handleConversations(STriggeredReference aTriggeredReference) {
		boolean lStartConversation = aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyStarted) && aTriggeredReference.getStatusValue().equals(AppConstants.statusValueTrue) && aTriggeredReference.getDataTag().getName().equals("conversation");
		if (lStartConversation) {
			// NOTE handle within this class because no conversations component is involved and it may not exist (anymore).
			IXMLTag lLocationTag = getLocation(currentLocationTagId);
			// conversation can be triggered by another conversation in
			// which case the former is not yet finished, so use timed action
			onAction(getId(), "showLocationActions", lLocationTag);
		} else {
			// NOTE delegate because status change may rerender part of component
			delegateHandling(aTriggeredReference, "runConversations");
		}
	}

	public void showAlert(String aAlert) {
		showAlert("", aAlert);
	}

	public void showAlert(IECaseComponent aCaseComponent, IXMLTag aAlertTag) {
		showAlert(aCaseComponent, aAlertTag, "");
	}

	@Override
	public void showAlert(String aTitle, String aAlert) {
		getAlert().setContent(aTitle, aAlert);
		onAction(getId(), "showAlert", aAlert);
	}

	@Override
	public void showAlert(IECaseComponent aCaseComponent, IXMLTag aAlertTag, String aTitle) {
		getAlert().setContent(aCaseComponent, aAlertTag, aTitle);
		onAction(getId(), "showAlert", aAlertTag);
	}

	protected CRunAlert getAlert() {
		// NOTE handle within this class because the alert component may not exist (anymore).
		CRunAlert lRunAlert = (CRunAlert)CDesktopComponents.vView().getComponent("runAlert");
		if (lRunAlert == null) {
			lRunAlert = createAlertComponent(null);
			appendChild(lRunAlert);
		}
		return lRunAlert;
	}

	public void showAlertFragment(IXMLTag aAudioAlertTag) {
		String lUrl = sSpring.getSBlobHelper().getUrl(aAudioAlertTag);
 		String includeAlertFragmentId = "includeAlertFragment";
		Include includeAlertFragment = (Include)CDesktopComponents.vView().getComponent(includeAlertFragmentId);
		if (includeAlertFragment == null) {
			return;
		}
		if (!CDesktopComponents.vView().isAbsoluteUrl(lUrl)) {
			// put emergo path before url
			lUrl = CDesktopComponents.vView().getEmergoRootUrl() +
					CDesktopComponents.vView().getEmergoWebappsRoot() + 
					lUrl;
		}
		includeAlertFragment.setAttribute("url", lUrl);
		// NOTE have to empty src otherwise no reload
		includeAlertFragment.setSrc("");
		includeAlertFragment.setSrc((String)includeAlertFragment.getAttribute("src"));
	}
	
	protected void handleAlert(STriggeredReference aTriggeredReference) {
		if (aTriggeredReference.getDataTag() == null) {
			return;
		}
		// NOTE handle within this class because the alert component may not exist (anymore).
		if (aTriggeredReference.getStatusKey().equals(AppConstants.statusKeySent) && aTriggeredReference.getStatusValue().equals(AppConstants.statusValueTrue)) {
			if (aTriggeredReference.getDataTag().getName().equals("alert")) {
				showAlert(aTriggeredReference.getCaseComponent(), aTriggeredReference.getDataTag());
			}
			else if (aTriggeredReference.getDataTag().getName().equals("audioalert")) {
				showAlertFragment(aTriggeredReference.getDataTag());
			}
		}
	}

	protected void handleMail(STriggeredReference aTriggeredReference) {
		if (aTriggeredReference.getStatusTag() == null) {
			handleTabletApp(aTriggeredReference);
		}
		// lDataTag = null for outmails becoming inmails for other rug's
		boolean lCaseComponentIsPresent = sSpring.getCurrentRunComponentStatus(aTriggeredReference.getCaseComponent(), AppConstants.statusKeyPresent, AppConstants.statusTypeRunGroup).equals(AppConstants.statusValueTrue);
		//only show alert if mail case component is present
		boolean lShowMailAlert = lCaseComponentIsPresent && aTriggeredReference.getStatusKey().equals(AppConstants.statusKeySent) && 
			(aTriggeredReference.getStatusTag().getName().equals("inmailpredef") || aTriggeredReference.getStatusTag().getName().equals("inmailhelp"));
		if (lShowMailAlert) {
			// NOTE handle within this class because the alert component may not exist (anymore).
			showAlert(CDesktopComponents.vView().getLabel("run_alert.text.mail_alert"));
		}
		// NOTE delegate because status change may rerender part of component
		delegateHandling(aTriggeredReference, "runMail");
	}

	protected void handleTablet(STriggeredReference aTriggeredReference) {
		if (aTriggeredReference.getDataTag() != null) {
			return;
		}
		// NOTE handle within this class because the tablet component is not involved, only the button to open it
		boolean lRerenderTabletBtn = aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyPresent) || aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyAccessible);
		boolean lStartTablet = aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyStarted);
		boolean lHighlightElement = aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyHighlight);
		boolean lDrawAttentionToElement = aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyDrawAttention);
		String lBtnId = "runTabletBtn";
		if (lRerenderTabletBtn) {
			updateBtn(lBtnId, aTriggeredReference.getStatusKey(), aTriggeredReference.getStatusValue(), aTriggeredReference.getCaseComponent());
		}
		else if (lStartTablet) {
			onAction(getId(), "showTablet", null);
		}
		else if (lHighlightElement) {
			highlightComponent(lBtnId);
		}
		else if (lDrawAttentionToElement) {
			drawAttentionToComponent(lBtnId, aTriggeredReference.getStatusValue().equals(AppConstants.statusValueTrue));
		}
	}

	protected void handleDashboard(STriggeredReference aTriggeredReference) {
		if (aTriggeredReference.getDataTag() != null) {
			return;
		}
		// NOTE handle within this class because the dashboard component is not involved, only the button to open it
		boolean lRerenderDashboardBtn = aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyPresent) || aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyAccessible);
		boolean lHighlightElement = aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyHighlight);
		boolean lDrawAttentionToElement = aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyDrawAttention);
		String lBtnId = "runDashboardBtn";
		if (lRerenderDashboardBtn) {
			updateBtn(lBtnId, aTriggeredReference.getStatusKey(), aTriggeredReference.getStatusValue(), aTriggeredReference.getCaseComponent());
		}
		else if (lHighlightElement) {
			highlightComponent(lBtnId);
		}
		else if (lDrawAttentionToElement) {
			drawAttentionToComponent(lBtnId, aTriggeredReference.getStatusValue().equals(AppConstants.statusValueTrue));
		}
		else {
			// NOTE delegate because status change may rerender part of component
			delegateHandling(aTriggeredReference, "runDashboard");
		}
	}

	protected void handleNote(STriggeredReference aTriggeredReference) {
		if (aTriggeredReference.getDataTag() != null) {
			return;
		}
		// NOTE handle within this class because the note component is not involved, only the button to open it
		boolean lRerenderNoteBtn = aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyPresent) || aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyAccessible);
		boolean lHighlightElement = aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyHighlight);
		boolean lDrawAttentionToElement = aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyDrawAttention);
		String lBtnId = "runNoteBtn";
		if (lRerenderNoteBtn) {
			updateBtn(lBtnId, getCaseComponentRenderStatus("note", ""));
		}
		if (lHighlightElement) {
			highlightComponent(lBtnId);
		}
		else if (lDrawAttentionToElement) {
			drawAttentionToComponent(lBtnId, aTriggeredReference.getStatusValue().equals(AppConstants.statusValueTrue));
		}
		else {
			// NOTE delegate because status change may rerender part of component
			delegateHandling(aTriggeredReference, "runNote");
		}
	}

	protected void handleMemo(STriggeredReference aTriggeredReference) {
		if (aTriggeredReference.getDataTag() != null) {
			return;
		}
		// NOTE handle within this class because the memo component is not involved, only the button to open it
		boolean lRerenderMemoBtn = aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyPresent) || aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyAccessible);
		boolean lHighlightElement = aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyHighlight);
		boolean lDrawAttentionToElement = aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyDrawAttention);
		String lBtnId = "runMemoBtn";
		if (lRerenderMemoBtn) {
			updateBtn(lBtnId, aTriggeredReference.getStatusKey(), aTriggeredReference.getStatusValue(), aTriggeredReference.getCaseComponent());
			runMemoBtn = (CRunMemoBtn)CDesktopComponents.vView().getComponent(lBtnId);
			if (runMemoBtn != null) {
				runMemoBtn.setVisible(false);
				boolean memoUsedAndVisible = isComponentUsedAndVisible("memo");
				if (memoUsedAndVisible) {
					runMemoBtn.setVisible(true);
					CRunConversations lComponent = (CRunConversations)CDesktopComponents.vView().getComponent("runConversations");
					if (lComponent != null) {
						// NOTE Only clickable if within conversation
						if (getCurrentCaseComponentStatusValue("memo", "", AppConstants.statusKeyAlwaysrecord).equals(AppConstants.statusValueTrue)) {
							runMemoBtn.setStatus("selected");
						}
						else {
							runMemoBtn.setStatus("active");
						}
					}
					else {
						runMemoBtn.setStatus("inactive");
					}
				}
			}
		}
		else if (lHighlightElement) {
			highlightComponent(lBtnId);
		}
		else if (lDrawAttentionToElement) {
			drawAttentionToComponent(lBtnId, aTriggeredReference.getStatusValue().equals(AppConstants.statusValueTrue));
		}
	}

	protected void handleAssessments(STriggeredReference aTriggeredReference) {
		if (aTriggeredReference.getDataTag() == null) {
			handleTabletApp(aTriggeredReference);
		}
		else {
			// NOTE delegate because status change may rerender part of component
			delegateHandling(aTriggeredReference, "runAssessments");
		}
	}

	protected void handleStates(STriggeredReference aTriggeredReference) {
		//NOTE there is no general Emergo states component existing for the player environment.
		//However, macros or plug-ins might offer a component based on states
		delegateHandling(aTriggeredReference, "runStates");
		//NOTE changes in state have to be rendered on score display
		delegateHandling(aTriggeredReference, "runScores");
	}

	protected void handleScores(STriggeredReference aTriggeredReference) {
		delegateHandling(aTriggeredReference, "runScores");
	}

	protected void handleNotifications(STriggeredReference aTriggeredReference) {
		//NOTE There can be multiple notifications components so get the one which corresponds to aTriggeredReference.getCaseComponent()
		CRunComponent lComponent = (CRunComponent)CDesktopComponents.vView().getComponent("runNotifications_" + aTriggeredReference.getCaseComponent().getCacId());
		if (lComponent == null) {
			if (aTriggeredReference.getStatusKey().equals(AppConstants.statusKeySent)) {
				lComponent = appendNotificationsComponent(aTriggeredReference.getCaseComponent(), getLocation(currentLocationTagId));
			}
		}
		// NOTE delegate because status change may rerender part of component
		delegateHandling(aTriggeredReference, "runNotifications");
	}

	protected void handleEditforms(STriggeredReference aTriggeredReference) {
		if (aTriggeredReference.getStatusTag() == null) {
			handleTabletApp(aTriggeredReference);
		}
		// NOTE delegate because status change may rerender part of component
		delegateHandling(aTriggeredReference, "runEditforms");
	}

	protected void handleDragdropforms(STriggeredReference aTriggeredReference) {
		if (aTriggeredReference.getStatusTag() == null) {
			handleTabletApp(aTriggeredReference);
		}
		// NOTE delegate because status change may rerender part of component
		delegateHandling(aTriggeredReference, "runDragdropforms");
	}

	protected void handleSelectionforms(STriggeredReference aTriggeredReference) {
		if (aTriggeredReference.getStatusTag() == null) {
			handleTabletApp(aTriggeredReference);
		}
		// NOTE delegate because status change may rerender part of component
		delegateHandling(aTriggeredReference, "runSelectionforms");
	}

	protected void handleVideosceneselector(STriggeredReference aTriggeredReference) {
		if (aTriggeredReference.getStatusTag() == null) {
			handleTabletApp(aTriggeredReference);
		}
		// NOTE delegate because status change may rerender part of component
		delegateHandling(aTriggeredReference, "runVideosceneselector");
	}

	protected void handleTests(STriggeredReference aTriggeredReference) {
		if (aTriggeredReference.getStatusTag() == null) {
			handleTabletApp(aTriggeredReference);
		}
		// NOTE delegate because status change may rerender part of component
		delegateHandling(aTriggeredReference, "runTests");
	}

	protected void handleGraphicalforms(STriggeredReference aTriggeredReference) {
		if (aTriggeredReference.getStatusTag() == null) {
			handleTabletApp(aTriggeredReference);
		}
		// NOTE delegate because status change may rerender part of component
		delegateHandling(aTriggeredReference, "runGraphicalforms");
	}

	protected void handlePVToolkit(STriggeredReference aTriggeredReference) {
		// NOTE delegate because status change may rerender part of component
		delegateHandling(aTriggeredReference, "pvToolkit");
	}

	protected void handleTabletApp(STriggeredReference aTriggeredReference) {
		if (aTriggeredReference.getDataTag() != null) {
			//NOTE changes in tags of tablet apps are not rendered until a tablet app is (re)opened.
			return;
		}
		boolean lStartTabletApp = aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyStarted);
		boolean lHighlightElement = aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyHighlight);
		boolean lDrawAttentionToElement = aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyDrawAttention);
		String lBtnId = getTabletAppBtnId(aTriggeredReference.getCaseComponent());
		if (lStartTabletApp) {
			// NOTE handle within this class because no component is not involved.
			List<String> lCacIdAndTagId = new ArrayList<String>();
			lCacIdAndTagId.add("" + aTriggeredReference.getCaseComponent().getCacId());
			lCacIdAndTagId.add("0");
			onAction(getId(), "showTabletApp", lCacIdAndTagId);
		}
		else if (lHighlightElement) {
			highlightComponent(lBtnId);
		}
		else if (lDrawAttentionToElement) {
			drawAttentionToComponent(lBtnId, aTriggeredReference.getStatusValue().equals(AppConstants.statusValueTrue));
		}
		else {
			// NOTE delegate because status change may rerender part of component
			delegateHandling(aTriggeredReference, "runTablet");
		}
	}

	protected void delegateHandling(STriggeredReference aTriggeredReference, String aComponentIdPrefix) {
		//NOTE delegate because status change may rerender part of component
		//NOTE get all components with a certain component id prefix.
		//E.g., there might be multiple conversations and notifications be simultaneously present within the Emergo player environment.
		//And macros or plug-ins might implement their own version of a component as well
		List<Component> lComponents = CDesktopComponents.vView().getComponentsByPrefix(aComponentIdPrefix);
		for (Component lComponent : lComponents) {
			if (lComponent instanceof CRunComponent) {
				CRunComponent lRunComponent = (CRunComponent)lComponent;
				if (lRunComponent.getCaseComponent() != null && lRunComponent.getCaseComponent() == aTriggeredReference.getCaseComponent()) {
					lRunComponent.handleStatusChange(aTriggeredReference);
				}
			}
		}
	}


	/**
	 * Gets the location tags of the navigation component. There is only one navigation component within a case.
	 *
	 * @return the location tags
	 */
	public List<IXMLTag> getLocationTags() {
		// only one location case component per case per caserole
		IECaseComponent lCaseComponent = getNavigationComponent();
		if (lCaseComponent == null)
			return null;
		return getCaseComponentRootTagChildTags(lCaseComponent, "location");
	}

	/**
	 * Gets the nested location tags of the navigation component. There is only one navigation component within a case.
	 *
	 * @return the neste location tags
	 */
	public List<IXMLTag> getNestedLocationTags() {
		// only one location case component per case per caserole
		IECaseComponent lCaseComponent = getNavigationComponent();
		if (lCaseComponent == null)
			return null;
		return getCaseComponentRootTagNestedChildTags(lCaseComponent, "location");
	}

	/**
	 * Gets the location tags of the navigation component. There is only one navigation component within a case.
	 *
	 * @return the location tags
	 */
	public List<IXMLTag> getNestedLocationTags(String aLocationTagIds) {
		if (aLocationTagIds == null || aLocationTagIds.equals(""))
			return null;
		List<IXMLTag> lAllLocationTags = getNestedLocationTags();
		if (lAllLocationTags == null)
			return null;
		String[] lIds = aLocationTagIds.split(",");
		List<IXMLTag> lLocationTags = new ArrayList<IXMLTag>();
		for (int i=0;i<lAllLocationTags.size();i++) {
			IXMLTag lLocationTag = (IXMLTag)lAllLocationTags.get(i);
			String lId = lLocationTag.getAttribute(AppConstants.keyId);
			boolean lFound = false;
			for (int j=0;j<lIds.length;j++) {
				if (lId.equals(lIds[j]))
					lFound = true;
			}
			if (lFound)
				lLocationTags.add(lLocationTag);
		}
		return lLocationTags;
	}

	/**
	 * Gets the preselected location tag. Is used for preview option. If preview should start with a certain case component tag.
	 *
	 * @return the preselected location tag
	 */
	protected IXMLTag getPreselectedLocationTag() {
		IXMLTag lTag = preselectedTag;
		if ((lTag != null) && lTag.getName().equals("location"))
			return lTag;
		return null;
	}

	/**
	 * Gets the selected location tag. Is used for preview option. If preview should start with a certain case component tag.
	 *
	 * @return the selected location tag
	 */
	protected IXMLTag getLocationTag() {
		IXMLTag lTag = null;
		List<IXMLTag> lNestedLocationTags = getNestedLocationTags();
		if (lNestedLocationTags != null && lNestedLocationTags.size() > 0) {
			for (IXMLTag lLocationTag : lNestedLocationTags) {
				// NOTE if selected, user has selected it, so it was the last location the user was in
				boolean lSelected = sSpring.getCurrentTagStatus(lLocationTag,
						AppConstants.statusKeySelected)
						.equals(AppConstants.statusValueTrue);
				if (lSelected) {
					lTag = lLocationTag;
				}
			}
			if (lTag == null) {
				List<IXMLTag> lLocationTags = getLocationTags();
				for (IXMLTag lLocationTag : lLocationTags) {
					// NOTE get the first location tag which is opened
					boolean lOpened = !sSpring.getCurrentTagStatus(lLocationTag,
							AppConstants.statusKeyOpened)
							.equals(AppConstants.statusValueFalse);
					if (lOpened) {
						return lLocationTag;
					}
				}
			}
			if (lTag == null || !lTag.getName().equals("location")) {
				// NOTE If not found or no location tag, return first;
				lTag = lNestedLocationTags.get(0);
			}
			return lTag;
		}
		return null;
	}

	/**
	 * Gets the location given by aTagId.
	 *
	 * @param aTagId the a tag id
	 *
	 * @return the location
	 */
	protected IXMLTag getLocation(String aTagId) {
		if (aTagId == null)
			return null;
		List<IXMLTag> lLocations = getNestedLocationTags();
		if (lLocations == null)
			return null;
		for (IXMLTag lLocation : lLocations) {
			if (aTagId.equals(lLocation.getAttribute(AppConstants.keyId)))
				return lLocation;
		}
		return null;
	}

	/**
	 * Has case component location actions. Checks if aCaseComponent has location actions.
	 *
	 * @param aCaseComponent the case component
	 *
	 * @return true, if succesfully
	 */
	protected boolean hasCaseComponentLocationActions(IECaseComponent aCaseComponent) {
		IECaseComponent lComponent = getNavigationComponent();
		if (lComponent == null)
			// if no locations then no actions on location
			return false;
		List<Object> lLocActions = new ArrayList<Object>();
		List<IXMLTag> lLocationTags = getNestedLocationTags();
		for (IXMLTag lLocationTag : lLocationTags) {
			getCaseComponentLocationActions(aCaseComponent, (lLocationTag != null), lLocationTag, lLocActions);
		}
		return (lLocActions.size() > 0);
	}

	/**
	 * Has case component location actions for current location. Checks if aCaseComponent has location actions
	 * for current location.
	 *
	 * @param aCaseComponent the case component
	 *
	 * @return true, if succesfully
	 */
	protected boolean hasCaseComponentLocationActionsForCurrentLocation(IECaseComponent aCaseComponent) {
		IECaseComponent lComponent = getNavigationComponent();
		if (lComponent == null)
			// if no locations then no actions on location
			return false;
		IXMLTag lLocationTag = getLocation(currentLocationTagId);
		List<Object> lLocActions = new ArrayList<Object>();
		getCaseComponentLocationActions(aCaseComponent, (lLocationTag != null), lLocationTag, lLocActions);
		return (lLocActions.size() > 0);
	}

	/**
	 * Case component is action on case component given by aParentCode.
	 *
	 * @param eCaseComponent the e case component
	 * @param aParentCode the a parent code
	 *
	 * @return true, if successful
	 */
	protected boolean caseComponentIsActionOn(IECaseComponent eCaseComponent, String aParentCode) {
		IEComponent lComponent = eCaseComponent.getEComponent();
		IEComponent lParent = null;
		int parentComId = lComponent.getComComId();
		if (parentComId > 0)
			lParent = sSpring.getComponent(""+parentComId);
		if ((lParent != null) && (lParent.getCode().equals(aParentCode)))
			return true;
		if (aParentCode.equals(""))
			return true;
		return false;
	}

	/**
	 * Gets the preselected location actions. Is used for preview option. If preview should start with certain location actions.
	 *
	 * @return the preselected location actions
	 */
	protected List<List<Object>> getPreselectedLocationActions() {
		List<List<Object>> lActions = new ArrayList<List<Object>>();
		// is used for previewing. previewing is done on only 1 casecomponent,
		// saved in session
		IECaseComponent lCaseComponent = preselectedCaseComponent;
		if (lCaseComponent == null) {
			return lActions;
		}
		//NOTE all actions should be possible, even if present for them is false, so next code is commented out
/*		if (!caseComponentIsActionOn(lCaseComponent, "navigation")) {
			if (!hasCaseComponentLocationActionsForCurrentLocation(lCaseComponent))
				return lActions;
		} */
		// possibly to use casecomponent tag is saved in session
		IXMLTag lTag = preselectedTag;
		// if tag is not null is must be a previewed conversation tag
		if (lTag != null && !lTag.getName().equals("conversation") && !lTag.getName().equals(AppConstants.contentElement))
			return lActions;
		List<Object> lObjects = new ArrayList<Object>();
		lObjects.add(lCaseComponent);
		lObjects.add(lTag);
		lActions.add(lObjects);
		return lActions;
	}

	/**
	 * Gets the navigation component.
	 *
	 * @return the navigation component
	 */
	protected IECaseComponent getNavigationComponent() {
		if (navigationComponent == null)
			navigationComponent = getActiveCaseComponent("navigation");
		return navigationComponent;
	}

	/**
	 * Sets the navigation component.
	 *
	 * @param aCaseComponent the e case component
	 */
	protected void setNavigationComponent(IECaseComponent aCaseComponent) {
		navigationComponent = aCaseComponent;
		breadcrumbsarea.clear();
	}

	/**
	 * Gets the tablet app components, a list of case components which are in the empack.
	 *
	 * @return the tablet app components
	 */
	public List<IECaseComponent> getTabletAppComponents() {
		IECase lCase = sSpring.getCase();
		if (lCase == null)
			return null;
		List<IECaseComponent> lCaseComponents = sSpring.getCaseComponents(lCase);
		if ((lCaseComponents == null) || (lCaseComponents.size() == 0))
			return null;
		List<IECaseComponent> lTabletCaseComponents = new ArrayList<IECaseComponent>();
		for (IECaseComponent lCaseComponent : lCaseComponents) {
			if (caseComponentIsActionOn(lCaseComponent, "empack")) {
				if (!hasCaseComponentLocationActions(lCaseComponent))
					lTabletCaseComponents.add(lCaseComponent);
			}
			// TODO tasks should have parent empack, but is within db, so new component has to be created 'rtasks' which has empack as parent.
			// TODO maybe better new component tablet in which components are defined
			else if (lCaseComponent.getEComponent().getCode().equals("tasks")) {
				lTabletCaseComponents.add(lCaseComponent);
			}
		}
		return lTabletCaseComponents;
	}

	/**
	 * Is a component a tablet component?
	 *
	 * @param aCacId the a cac id
	 * 
	 * @return boolean
	 */
	public boolean isTabletAppComponent(String aCacId) {
		List<IECaseComponent> lCaseComponents = getTabletAppComponents();
		for (IECaseComponent lCaseComponent : lCaseComponents) {
			if (("" + lCaseComponent.getCacId()).equals(aCacId)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Gets the preselected tablet app. Is used for preview option. If preview should start with a certain case component.
	 *
	 * @return the preselected tablet app or null if none
	 */
	protected IECaseComponent getPreselectedTabletApp() {
		IECaseComponent lCaseComponent = preselectedCaseComponent;
		if (lCaseComponent != null) {
			if (caseComponentIsActionOn(lCaseComponent, "empack")) {
				if (!hasCaseComponentLocationActions(lCaseComponent))
					return lCaseComponent;
			}
			// TODO tasks should have parent empack, but is within db, so new component has to be created 'rtasks' which has empack as parent.
			// TODO maybe better new component tablet in which components are defined
			else if (lCaseComponent.getEComponent().getCode().equals("tasks")) {
				return lCaseComponent;
			}
		}
		return null;
	}

	/**
	 * Gets the tablet app, a case component, given by aCacId.
	 *
	 * @param aCacId the a cac id
	 *
	 * @return the tablet app or null if none
	 */
	protected IECaseComponent getTabletApp(String aCacId) {
		if (aCacId == null)
			return null;
		List<IECaseComponent> lTabletApps = getTabletAppComponents();
		if (lTabletApps == null)
			return null;
		for (IECaseComponent lCaseComponent : lTabletApps) {
			if (aCacId.equals("" + lCaseComponent.getCacId())) {
				return lCaseComponent;
			}
		}
		return null;
	}

	/**
	 * Hide or shows location components, if they are not present on all locations. For instance notifications components.
	 *
	 * @param aLocationTag the a location tag
	 */
	protected void hideShowLocationComponents(IXMLTag aLocationTag) {
		if (aLocationTag == null) {
			return;
		}
		IECase lCase = sSpring.getCase();
		if (lCase == null) {
			return;
		}
		List<IECaseComponent> lPossibleLocationComponents = sSpring.getCaseComponents(lCase, "notifications");
		for (IECaseComponent lCaseComponent : lPossibleLocationComponents) {
			//NOTE There can be multiple notifications components so get the one which corresponds to aCaseComponent
			CRunComponent lComponent = appendNotificationsComponent(lCaseComponent, aLocationTag);
			if (lComponent != null) {
				lComponent.setVisible(((CRunNotifications)lComponent).isNotificationsComponentVisible());
			}
		}
	}

	/**
	 * Hide or shows location component, if it is not present on all locations. For instance a notifications component.
	 *
	 * @param aCaseComponent the case component
	 * @param aLocationTag the location tag
	 *
	 * @return visible
	 */
	protected boolean isLocationComponentVisible(IECaseComponent aCaseComponent, IXMLTag aLocationTag) {
		if (!isLocationComponentOnLocation(aCaseComponent, aLocationTag)) {
			return false;
		}
		IXMLTag lRootTag = sSpring.getXmlDataPlusRunStatusTree(aCaseComponent, AppConstants.statusTypeRunGroup);
		if (lRootTag == null) {
			return false;
		}
		// casecomponent has to be present
		IXMLTag lComponentTag = lRootTag.getChild(AppConstants.componentElement);
		boolean lCacPresent = !lComponentTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent)
				.equals(AppConstants.statusValueFalse);
		boolean lCacOpened = lComponentTag.getCurrentStatusAttribute(AppConstants.statusKeyOpened)
				.equals(AppConstants.statusValueTrue);
		if (!lCacPresent || !lCacOpened) {
			return false;
		}
		return true;
	}

	protected boolean isLocationComponentOnLocation(IECaseComponent aCaseComponent, IXMLTag aLocationTag) {
		IXMLTag lRootTag = sSpring.getXmlDataTree(aCaseComponent);
		if (lRootTag == null) {
			return false;
		}
		IXMLTag lContentTag = lRootTag.getChild(AppConstants.contentElement);
		String lCrossReferenceId = "locationtags_componentroot";
		// get tag ids coupled to location
		List<String> lTagIds = cCaseHelper.getTagRefIds(lCrossReferenceId, "" + AppConstants.statusKeySelectedIndex,
				sSpring.getSCaseRoleHelper().getCaseRole(), getNavigationComponent(), aLocationTag, false);
		String lTagId = lContentTag.getAttribute(AppConstants.keyId);
		for (String lIds : lTagIds) {
			String[] lIdArr = lIds.split(",");
			// TagId and CacId equal!
			if (lIdArr[2].equals(lTagId) && lIdArr[1].equals("" + aCaseComponent.getCacId())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Hide or shows location component for current location, if it is not present on all locations. For instance a notifications component.
	 *
	 * @param aCaseComponent the case component
	 *
	 * @return visible
	 */
	@Override
	public boolean isLocationComponentVisible(IECaseComponent aCaseComponent) {
		return isLocationComponentVisible(aCaseComponent, getLocation(currentLocationTagId));
	}

	/**
	 * Gets current location actions.
	 *
	 * @param aLocationTag the a location tag
	 *
	 * @return the location actions
	 */
	protected List<Object> getLocationActions(IXMLTag aLocationTag) {
		List<Object> lActions = new ArrayList<Object>();
		IECase lCase = sSpring.getCase();
		if (lCase == null)
			return lActions;
		if (possibleCaseComponentsOnLocation == null) {
			possibleCaseComponentsOnLocation = sSpring.getCaseComponents(lCase, "conversations");
			possibleCaseComponentsOnLocation.addAll(sSpring.getCaseComponents(lCase, "references"));
			possibleCaseComponentsOnLocation.addAll(sSpring.getCaseComponents(lCase, "assessments"));
			possibleCaseComponentsOnLocation.addAll(sSpring.getCaseComponents(lCase, "tutorial"));
			possibleCaseComponentsOnLocation.addAll(sSpring.getCaseComponents(lCase, "ispot"));
			possibleCaseComponentsOnLocation.addAll(sSpring.getCaseComponents(lCase, "editforms"));
			possibleCaseComponentsOnLocation.addAll(sSpring.getCaseComponents(lCase, "dragdropforms"));
			possibleCaseComponentsOnLocation.addAll(sSpring.getCaseComponents(lCase, "selectionforms"));
			possibleCaseComponentsOnLocation.addAll(sSpring.getCaseComponents(lCase, "videosceneselector"));
			possibleCaseComponentsOnLocation.addAll(sSpring.getCaseComponents(lCase, "tests"));
			possibleCaseComponentsOnLocation.addAll(sSpring.getCaseComponents(lCase, "graphicalforms"));
		}
		if (possibleCaseComponentsOnLocation == null)
			return lActions;
		boolean lFilterOnLocation = (aLocationTag != null);
		// if no filtering on location get all presentable location actions
		// in case one does not use a location component and for previewing
		// location actions
		for (IECaseComponent lCaseComponent : possibleCaseComponentsOnLocation) {
			getCaseComponentLocationActions(lCaseComponent, lFilterOnLocation, aLocationTag, lActions);
		}
		return lActions;
	}

	/**
	 * Gets the case component location actions.
	 *
	 * @param aCaseComponent the case component
	 * @param aFilterOnLocation filter on location, if not give all
	 * @param aLocationTag the location tag
	 * @param aActions used to return the actions
	 */
	protected void getCaseComponentLocationActions(IECaseComponent aCaseComponent, boolean aFilterOnLocation, IXMLTag aLocationTag, List<Object> aActions) {
		boolean lConversationTags = aCaseComponent.getEComponent().getCode().equals("conversations");
		boolean lComponentRootTag = aCaseComponent.getEComponent().getCode().equals("references") ||
				aCaseComponent.getEComponent().getCode().equals("assessments") ||
				aCaseComponent.getEComponent().getCode().equals("tutorial") ||
				aCaseComponent.getEComponent().getCode().equals("ispot") ||
				aCaseComponent.getEComponent().getCode().equals("editforms") ||
				aCaseComponent.getEComponent().getCode().equals("dragdropforms") ||
				aCaseComponent.getEComponent().getCode().equals("selectionforms") ||
				aCaseComponent.getEComponent().getCode().equals("videosceneselector") ||
				aCaseComponent.getEComponent().getCode().equals("tests") ||
				aCaseComponent.getEComponent().getCode().equals("graphicalforms");
		if (!(lConversationTags || lComponentRootTag))
			return;
		// casecomponent has to be present
		boolean lCacPresent = (!(sSpring.getCurrentRunComponentStatus(
				aCaseComponent, AppConstants.statusKeyPresent, AppConstants.statusTypeRunGroup))
				.equals(AppConstants.statusValueFalse));
		if (!lCacPresent)
			return;
		String lCrossReferenceId = "";
		if (lConversationTags)
			lCrossReferenceId = "locationtags_conversationtags";
		if (lComponentRootTag)
			lCrossReferenceId = "locationtags_componentroot";
		List<String> lTagIds = null;
		if (aFilterOnLocation) {
			// get tag ids coupled to location
			lTagIds = cCaseHelper.getTagRefIds(lCrossReferenceId, "" + AppConstants.statusKeySelectedIndex,
					sSpring.getSCaseRoleHelper().getCaseRole(), getNavigationComponent(), aLocationTag, false);
		}
		List<IXMLTag> lTags = null;
		if (lConversationTags)
			lTags = getCaseComponentRootTagChildTags(aCaseComponent);
		if (lComponentRootTag) {
			IXMLTag lRootTag = getCaseComponentRootTag(aCaseComponent);
			lTags = new ArrayList<IXMLTag>();
			lTags.add(lRootTag);
		}
		if (lTags != null) {
			for (IXMLTag lTag : lTags) {
				String lTagId = lTag.getAttribute(AppConstants.keyId);
				String lStatus = sSpring.getCurrentTagStatus(lTag, AppConstants.statusKeyPresent);
				boolean lPresent = true;
				if (lConversationTags)
					// conversation tag has to be present
					lPresent = (lStatus.equals(AppConstants.statusValueTrue));
				if (lComponentRootTag)
					// root tag is always present
					;
				boolean lOnLocation = false;
				if (aFilterOnLocation) {
					if (lPresent) {
						for (String lIds : lTagIds) {
							String[] lIdArr = lIds.split(",");
							// TagId and CacId equal!
							if ((lIdArr[2].equals(lTagId)) && (lIdArr[1].equals("" + aCaseComponent.getCacId())))
								lOnLocation = true;
						}
					}
				} else
					lOnLocation = true;
				if (lPresent && lOnLocation) {
					List<Object> lObjects = new ArrayList<Object>();
					lObjects.add(aCaseComponent);
					if (lConversationTags) {
						lObjects.add(lTag);
						String lName = sSpring.unescapeXML(lTag.getChildValue("name"));
						if (lName.equals("")) {
							lName = sSpring.getXmlManager().getTagKeyValues(lTag, lTag.getDefAttribute(AppConstants.defKeyKey));
						}
						lObjects.add(lName);
					}
					if (lComponentRootTag) {
						lObjects.add(null);
						lObjects.add(sSpring.getCaseComponentRoleName(aCaseComponent));
					}
					aActions.add(lObjects);
				}
			}
		}
	}





	/** The view area status. */
	protected String viewAreaStatus = "";

	/** The locationarea, used to show location image. */
	protected CRunLocationAreaOunl locationarea = null;

	/** The locationobjects, used to show location objects. */
	protected Div locationobjects = null;

	/** The breadcrumbsarea, used to show breadcrumbs. */
	protected CRunBreadcrumbsAreaOunl breadcrumbsarea = null;

	/** The view area actcasecomponent, the current casecomponent for which content is shown. */
	protected IECaseComponent viewAreaActCaseComponent = null;

	/** The view area actcasecomponenttag, the current tag of the casecomponent for which content is shown. */
	protected IXMLTag viewAreaActCaseComponentTag = null;

	public IECaseComponent getActCaseComponent(String senderId) {
		return viewAreaActCaseComponent;
	}

	public IXMLTag getActCaseComponentTag(String senderId) {
		return viewAreaActCaseComponentTag;
	}

	public void setViewAreaStatus(String senderId, String status) {
		viewAreaStatus = status;
		if ((status.equals("location")) && (locationarea == null)) {
			locationarea = (CRunLocationAreaOunl)CDesktopComponents.vView().getComponent("runLocationArea");
			if (locationarea == null) {
				locationarea = createLocationArea();
				appendChild(locationarea);
			}
			else {
				// TODO init
			}
			locationobjects = (Div)CDesktopComponents.vView().getComponent("runLocationObjects");
			if (locationobjects == null) {
				locationobjects = new CDefDiv();
				locationobjects.setId("runLocationObjects");
				locationobjects.setZclass("CRunLocationObjects");
				appendChild(locationobjects);
			}
			else {
				// TODO init
			}
			breadcrumbsarea = (CRunBreadcrumbsAreaOunl)CDesktopComponents.vView().getComponent("runBreadcrumbsArea");
			if (breadcrumbsarea == null) {
				breadcrumbsarea = createBreadcrumbsArea();
				appendChild(breadcrumbsarea);
			}
			CRunArea lRunCloseArea = (CRunArea)CDesktopComponents.vView().getComponent("runCloseArea");
			if (lRunCloseArea == null) {
				Component lCloseAreaParent = this;
				Component lRunWndSurrounding = (Component)CDesktopComponents.vView().getComponent("runWndSurrounding");
				if (lRunWndSurrounding != null) {
					lCloseAreaParent = lRunWndSurrounding; 
				}
				lRunCloseArea = createCloseArea();
				lCloseAreaParent.appendChild(lRunCloseArea);
			}
			else {
				// TODO init
			}
		}
		if (locationarea != null)
			locationarea.setVisible(status.equals("location"));
	}

	public void setViewAreaStatus(String senderId, String action, Object status) {
		viewAreaActCaseComponentTag = null;
		CRunTitleArea lTitleArea = (CRunTitleArea) CDesktopComponents.vView().getComponent("runTitleArea");
		if (lTitleArea != null) {
			lTitleArea.setMediaBufferingVisible(false);
			lTitleArea.setMediaBtnsVisible(false);
		}
		if (action.equals("showLocation")) {
			setViewAreaStatus(senderId, "location");
			viewAreaActCaseComponent = getNavigationComponent();
			locationarea.showLocation(status);
		}
		if ((action.equals("showTabletApp")) || (action.equals("showLocationAction")) || (action.equals("showApp"))) {
			setViewAreaStatus(senderId, "component");
			if (action.equals("showLocationAction")) {
				viewAreaActCaseComponent = (IECaseComponent) ((List) status).get(0);
				viewAreaActCaseComponentTag = (IXMLTag) ((List) status).get(1);
			} else
				viewAreaActCaseComponent = sSpring.getCaseComponent(Integer.parseInt(((List<String>)status).get(0)));;
		}
	}

}
