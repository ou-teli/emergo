/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.cases.PVtoolkit.feedback.CRunPVToolkitFeedbackRatingHbox;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;

public class CRunPVToolkitSkillWheelLevel2Div extends CRunPVToolkitSkillWheelLevel1Div {

	private static final long serialVersionUID = -4529829370813482512L;

	protected Map<String,String> _levelRatings = new HashMap<String,String>();
	protected Map<String,IXMLTag> _levelRatingTags = new HashMap<String,IXMLTag>();
	protected IXMLTag _practiceTag;
	protected int initialCycleNumber;
	protected int initialSkillClusterNumber;
	
	@Override
	public void init(IERunGroup actor, boolean editable, int cycleNumber, int skillClusterNumber) {
		_skillWheelZoomFactor = 1.40;
		_presentationLevel = 2;
		_classPrefix = "skillWheelLevel2";
		initialSkillClusterNumber = skillClusterNumber;
		initialCycleNumber = cycleNumber;
		
		super.init(actor, editable, cycleNumber, skillClusterNumber);
	}
	
	public void init(IERunGroup actor, boolean editable, int cycleNumber, int skillClusterNumber, List<Integer> pOtherRgaIdsToFilterOn) {
		_skillWheelZoomFactor = 1.40;
		_presentationLevel = 2;
		_classPrefix = "skillWheelLevel2";
		initialOtherRgaIdsToFilterOn = new ArrayList<>(pOtherRgaIdsToFilterOn);
		otherRgaIdsToFilterOn = pOtherRgaIdsToFilterOn;
		initialSkillClusterNumber = skillClusterNumber;
		initialCycleNumber = cycleNumber;
		
		super.init(actor, editable, cycleNumber, skillClusterNumber);
	}
	
	@Override
	public void update(int cycleNumber) {
		super.update(cycleNumber);

		if (_hasBackButton) {
			Component btn = new CRunPVToolkitDefButton(this, 
					new String[]{"class", "cLabelKey"}, 
					new Object[]{"font pvtoolkitButton " + _classPrefix + "BackButton", "PV-toolkit.back"}
					);
			addBackOnClickEventListener(btn, _idPrefix + "Level2Div", _backToComponentId, _backToComponentInitId);
		}

		if (mayRateFeedback()) {
			_levelRatings.clear();
			_levelRatingTags.clear();
			if (_cycleNumber > 0) {
				Component lBtn = new CRunPVToolkitDefButton(this, 
						new String[]{"class", "labelKey"}, 
						new Object[]{"font pvtoolkitButton " + _classPrefix + "RateFeedbackButton", "PV-toolkit-skillwheel.button.ratefeedback"}
						);
				addShowRateFeedbackOnClickEventListener(lBtn, this);
				
				_practiceTag = _sharedPracticeTagsPerFeedbackStep.get(_cycleNumber - 1);
				for (IXMLTag lFeedbackRatingTag : pvToolkit.getStatusChildTag(_practiceTag).getChilds(CRunPVToolkit._feedbackRatingKeyStr)) {
					String lLevel = pvToolkit.getStatusChildTagAttribute(lFeedbackRatingTag, CRunPVToolkit._feedbackRatingLevelStr);
					String lFBRgaId = pvToolkit.getStatusChildTagAttribute(lFeedbackRatingTag, CRunPVToolkit._feedbackRatingRgaIdStr);
					_levelRatings.put(lFBRgaId, lLevel);
					_levelRatingTags.put(lFBRgaId, lFeedbackRatingTag);
				}
			}
		}
		
		if (_compareWithExperts || (_cycleNumber > 0 && _cycleNumber <= _sharedPracticeTagsPerFeedbackStep.size())) {
			if (!_compareWithExperts) {
				if (_mayFilter) {
					String labelKey = "";
					if (!_compareFeedbackGivers) {
						labelKey = "PV-toolkit-skillwheel.button.filter";
					}
					else {
						labelKey = "PV-toolkit-skillwheel.button.groups";
					}
					Component btn = new CRunPVToolkitDefButton(this, 
							new String[]{"class", "labelKey"}, 
							new Object[]{"font pvtoolkitButton " + _classPrefix + "FilterButton", labelKey}
							);
					if (!_compareFeedbackGivers) {
						addShowFilterOnClickEventListener(btn, this);
					}
					else {
						addShowGroupsOnClickEventListener(btn, this);
					}
				}
			}

			if (!_compareFeedbackGivers) {
				Component btn = new CRunPVToolkitDefButton(this, 
						new String[]{"class", "labelKey"}, 
						new Object[]{"font pvtoolkitButton " + _classPrefix + "RecordingButton", "PV-toolkit-skillwheel.button.recording"}
						);
				addToRecordingOnClickEventListener(btn, this);
		
				btn = new CRunPVToolkitDefButton(this, 
						new String[]{"class", "labelKey"}, 
						new Object[]{"font pvtoolkitButton " + _classPrefix + "OverviewTipsTopsButton", "PV-toolkit-skillwheel.button.overviewtipstops"}
						);
				addToOverViewTipsTopsOnClickEventListener(btn, this);

				if (!_compareWithExperts) {
					btn = new CRunPVToolkitDefButton(this, 
							new String[]{"class", "labelKey"}, 
							new Object[]{"font pvtoolkitButton " + _classPrefix + "ComparePeersButton", "PV-toolkit-skillwheel.button.compare"}
							);
					addToComparePeersOnClickEventListener(btn, this);
				}
			}
		}
		
	}

	protected Boolean mayRateFeedback() {
		return false;
	}

	protected void addBackOnClickEventListener(Component component, String componentFromId, String componentToId, String componentToInitId) {
		// NOTE only used to go back to level 1
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				CRunPVToolkitSkillWheelLevel2Div componentFrom = (CRunPVToolkitSkillWheelLevel2Div)CDesktopComponents.vView().getComponent(componentFromId);
				IERunGroup lActor = null;
				boolean lEditable = false;
				int lCycleNumber = initialCycleNumber;
				int lSkillClusterNumberToFilter = initialSkillClusterNumber;
				List<Integer> lOtherRgaIdsToFilterOn = initialOtherRgaIdsToFilterOn;
				if (componentFrom != null) {
					lActor = componentFrom._actor;
					lEditable = componentFrom._editable;
					lCycleNumber = componentFrom._cycleNumber;
					lSkillClusterNumberToFilter = componentFrom._skillClusterNumberToFilter;
					lOtherRgaIdsToFilterOn = componentFrom.otherRgaIdsToFilterOn;
					componentFrom.setVisible(false);
				}
				toComponent(componentToId, componentToInitId, lActor, lEditable, lCycleNumber, lSkillClusterNumberToFilter, lOtherRgaIdsToFilterOn);
			}
		});
	}

	protected void toComponent(String componentToId, String componentToInitId, IERunGroup pActor, boolean pEditable, int pCycleNumber, int pSkillClusterNumberToFilter, List<Integer> pOtherRgaIdsToFilterOn) {
		Component componentTo = CDesktopComponents.vView().getComponent(componentToId);
		if (componentTo != null) {
			Component componentToInit = !StringUtils.isEmpty(componentToInitId) ? CDesktopComponents.vView().getComponent(componentToInitId) : null;
			if (componentToInit == null) {
				componentToInit = componentTo;
			}
			if (componentToInit instanceof CRunPVToolkitSkillWheelLevel1Div) {
				boolean lRgaIdsChanged = (((CRunPVToolkitSkillWheelLevel1Div)componentToInit).initialOtherRgaIdsToFilterOn != null) && !((CRunPVToolkitSkillWheelLevel1Div)componentToInit).initialOtherRgaIdsToFilterOn.equals(pOtherRgaIdsToFilterOn);
				// NOTE: check for componentTo._cycleNumber, because initialCycleNumber can be reset by OverviewTipsTopsDiv:
				if (lRgaIdsChanged || (((CRunPVToolkitSkillWheelLevel1Div)componentToInit)._cycleNumber != pCycleNumber)) {
					// NOTE: componentToInit.otherRgaIdsToFilterOn is already equal to componentFrom.otherRgaIdsToFilterOn if set in componentFrom, because both point to same ArrayList
					// NOTE: if pOtherRgaIdsToFilterOn = null we have 'compare' situation; we must not overrule pOtherRgaIdsToFilterOn of parent
					// NOTE: set componentToInit._cycleNumber, because parameter in init method will be ignored for level 1 and level 2:
					((CRunPVToolkitSkillWheelLevel1Div)componentToInit)._cycleNumber = pCycleNumber;
					((CRunPVToolkitSkillWheelLevel1Div)componentToInit).init(pActor, pEditable, pCycleNumber, pSkillClusterNumberToFilter);
				}
			}
			componentTo.setVisible(true);
		}
	}

	protected void addShowRateFeedbackOnClickEventListener(Component pComponent, CRunPVToolkitSkillWheelLevel2Div pSkillWheelLevelDiv) {
		pComponent.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				showRateFeedbackDiv(pSkillWheelLevelDiv);
			}
		});
	}

	protected void showRateFeedbackDiv(CRunPVToolkitSkillWheelLevel2Div pSkillWheelLevelDiv) {
		Div parentDiv = new CRunPVToolkitDefDiv(pSkillWheelLevelDiv, 
				new String[]{"class"}, 
				new Object[]{"popupDiv"}
		);

		new CRunPVToolkitDefImage(parentDiv, 
				new String[]{"class"}, 
				new Object[]{"popupBackground"}
		);
		
		String classPrefix = "skillWheelRateFeedback";

		Div popupDiv = new CRunPVToolkitDefDiv(parentDiv, 
				new String[]{"class"}, 
				new Object[]{classPrefix}
		);

		if (_cycleNumber == _maxCycleNumber) {
			new CRunPVToolkitDefImage(popupDiv, 
					new String[]{"class", "src"}, 
					new Object[]{classPrefix + "Background", zulfilepath + "filter-background.svg"}
			);
		} else {
			new CRunPVToolkitDefImage(popupDiv, 
					new String[]{"class", "src"}, 
					new Object[]{classPrefix + "Background_geel", zulfilepath + "skillWheelLevel2_geel.svg"}
			);
		}

		Div div = new CRunPVToolkitDefDiv(popupDiv, 
				new String[]{"class"}, 
				new Object[]{classPrefix + "Title"}
		);

		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font " + classPrefix + "Title", "PV-toolkit-skillwheel.button.ratefeedback"}
		);

		Div peersDiv = new CRunPVToolkitDefDiv(popupDiv, 
				new String[]{"class"}, 
				new Object[]{classPrefix + "Grid"}
		);
		
		if (_cycleNumber == _maxCycleNumber) {
			Button saveButton = new CRunPVToolkitDefButton(popupDiv,
					new String[]{"class", "cLabelKey"}, 
					new Object[]{"font pvtoolkitButton " + classPrefix + "SaveButton", "PV-toolkit.ok"}
					);
			addRateFeedbackSaveButtonOnClickEventListener(saveButton, parentDiv, this);
		} else {
			Button seenButton = new CRunPVToolkitDefButton(popupDiv,
					new String[]{"class", "cLabelKey"}, 
					new Object[]{"font pvtoolkitButton " + classPrefix + "SeenButton", "PV-toolkit.seen"}
					);
			addRateFeedbackSeenButtonOnClickEventListener(seenButton, parentDiv);
		}

		filterRows = appendRateFeedbackPeersGrid(peersDiv, "PeersGrid");
		fillRateFeedbackRows(filterRows, getCycleFeedbackTags(), _idPrefix + "FeedbackRateBox_");

	}

	protected void addRateFeedbackSaveButtonOnClickEventListener(Component pComponent, Component pComponentToDetach, CRunPVToolkitSkillWheelLevel2Div pRunPVToolkitSkillWheelLevel2Div) {
		pComponent.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				
				List<Component> lRateBoxes = CDesktopComponents.vView().getComponentsByPrefix(_idPrefix + "FeedbackRateBox_");
				for (Component lRateBox : lRateBoxes) {
					if (lRateBox instanceof CRunPVToolkitFeedbackRatingHbox) {
						int lRgaId = (int)lRateBox.getAttribute("rgaId");
						int lLevel = ((CRunPVToolkitFeedbackRatingHbox) lRateBox).getLevel();
						IXMLTag lFBRatingTag = _levelRatingTags.get(lRgaId + "");
						if (lFBRatingTag == null)
							lFBRatingTag = pvToolkit.addPracticeFeedbackRating(_practiceTag, lRgaId + "", lLevel + "");
						pvToolkit.setPracticeStatus(_actor, lFBRatingTag, CRunPVToolkit._feedbackRatingLevelStr, lLevel + "");
					}
				}

				pComponentToDetach.detach();
				pRunPVToolkitSkillWheelLevel2Div.update(_cycleNumber);
			}
		});
	}

	protected void addRateFeedbackSeenButtonOnClickEventListener(Component pComponent, Component pComponentToDetach) {
		pComponent.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				pComponentToDetach.detach();
			}
		});
	}

    protected Rows appendRateFeedbackPeersGrid(Component parent, String gridIdPostfix) {
    	return pvToolkit.appendGrid(
    			parent,
    			getId() + gridIdPostfix,
    			"position:absolute;left:5px;top:0px;width:400px;height:300px;overflow:auto;",
    			null,
    			new String[] {"65%", "35%"}, 
    			new boolean[] {true, true, true},
    			null,
    			"PV-toolkit-skillwheel.ratefeedback.column.label.", 
    			new String[] {"peer", "show"});
	}

	protected void fillRateFeedbackRows(Rows rows, List<IXMLTag> feedbackTags, String pRateBoxIdPrefix) {
		appendRateFeedbackPeersToGrid(rows, feedbackTags, pRateBoxIdPrefix);
	}

	protected void appendRateFeedbackPeersToGrid(
			Rows rows, 
    		List<IXMLTag> feedbackTags,
			String pRateBoxIdPrefix
			) {
		rows.getChildren().clear();
		
		//NOTE get all feedback givers from feedback tags.
		List<IERunGroupAccount> runGroupAccounts = new ArrayList<IERunGroupAccount>();
		for (IXMLTag feedbackTag : feedbackTags) {
			IERunGroupAccount runGroupAccount = pvToolkit.getRunGroupAccount(feedbackTag.getAttribute("feedbackrgaid"));
			if (runGroupAccount != null && !runGroupAccounts.contains(runGroupAccount)) {
				runGroupAccounts.add(runGroupAccount);
			}
		}
		List<Integer> allFeedbackGiverRgaIds = pvToolkit.getRgaIds(runGroupAccounts);
		
		//List<IXMLTag> commonPeerGroupTags = pvToolkit.getCommonPeerGroupTags(_actor, null, pvToolkit.getRolesThatReceiveFeedbackRating(), null, true);
		// NOTE: allow showing and giving feedback rating to feedback givers that now belong to other peer groups
		List<IXMLTag> commonPeerGroupTags = pvToolkit.getPeerGroupTags(_actor).getXmlTags();

		//NOTE same rga ids might be existing in different peer groups. Only show one row per rga id. 
		List<Integer> handledRgaIds = new ArrayList<Integer>();
		for (IXMLTag peerGroupTag : commonPeerGroupTags) {
			List<IXMLTag> memberTags = pvToolkit.getStatusChildTag(peerGroupTag).getChilds("member");
			for (IXMLTag memberTag : memberTags) {
				IERunGroupAccount runGroupAccount = pvToolkit.getRunGroupAccount(pvToolkit.getStatusChildTagAttribute(memberTag, "rgaid"));
				//NOTE if student not (yet) active in run (has not yet enrolled) runGroupAccount is null  
				if (runGroupAccount != null) {
					if ((runGroupAccount.getERunGroup().getRugId() != _actor.getRugId()) && pvToolkit.getRolesThatReceiveFeedbackRating().contains(pvToolkit.getStatusChildTagAttribute(memberTag, "role"))) {
						if (!handledRgaIds.contains(runGroupAccount.getRgaId())) {
							if (allFeedbackGiverRgaIds.contains(runGroupAccount.getRgaId())) {
								appendRateFeedbackRowToGrid(rows, runGroupAccount, pRateBoxIdPrefix);
							}
						}
					}
					handledRgaIds.add(runGroupAccount.getRgaId());
				}
			}
		}
	}

	protected void appendRateFeedbackRowToGrid(
			Rows rows, 
			IERunGroupAccount runGroupAccount,
			String pRateBoxIdPrefix
			) {

		Row row = new Row();
		rows.appendChild(row);
						
		String name = "";
		if (isPreviewRun) {
			name = runGroupAccount.getERunGroup().getName();
		}
		else {
			name = accountManager.getAccountName(runGroupAccount.getEAccount());
		}
		new CRunPVToolkitDefLabel(row, 
				new String[]{"class", "value"}, 
				new Object[]{"font gridLabel", name}
				);

		int lLevel = 0;
		if (!StringUtils.isEmpty(_levelRatings.get(runGroupAccount.getRgaId() + ""))) {
			try {
				lLevel = Integer.parseInt(_levelRatings.get(runGroupAccount.getRgaId() + ""));
			} catch (NumberFormatException e) {
			}
		}
		CRunPVToolkitFeedbackRatingHbox hbox = new CRunPVToolkitFeedbackRatingHbox(row,
				new String[]{"id", "class"}, 
				new Object[]{pRateBoxIdPrefix + runGroupAccount.getRgaId(), "gridRatebox"});
		hbox.setAttribute("rgaId", runGroupAccount.getRgaId());
		hbox.init(lLevel, _cycleNumber == _maxCycleNumber);
		
	}

}
