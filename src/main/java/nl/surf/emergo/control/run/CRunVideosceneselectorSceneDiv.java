/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Label;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.view.VView;

public class CRunVideosceneselectorSceneDiv extends CDefDiv {

	private static final long serialVersionUID = 3954232453700208982L;

	protected VView vView = CDesktopComponents.vView();

	protected CRunVideosceneselector runVideosceneselector = (CRunVideosceneselector)CDesktopComponents.vView().getComponent(CRunVideosceneselector.runVideosceneselectorId);

	public void onInit(Event aEvent) {
		long duration = (long)((Object[])aEvent.getData())[0];
		int videoWidth = (int)((Object[])aEvent.getData())[1];
		String color = (String)((Object[])aEvent.getData())[2];
		setAttribute("color", color);
		double factor = 0 + videoWidth;
		factor = factor/duration;
		String[] range = ((String)getAttribute("range")).split(",");
		double leftPos = 0 + Integer.parseInt(range[0]); 
		leftPos = leftPos * factor;
		double rightPos = 0 + Integer.parseInt(range[1]); 
		rightPos = rightPos * factor;
		double width = -(0 + Math.round(leftPos));
		width += rightPos;

		String style = "";
		style += "left:" + Math.round(leftPos) + "px;";
		style += "width:" + Math.round(width) + "px;";
		setStyle(style);
		setAttribute("style", style);
	}

	public void onSetSceneCounter(Event aEvent) {
		Label label = (Label)vView.getComponent("videosceneselectorSceneLabel_" + ((IXMLTag)getAttribute("tag")).getAttribute("id"));
		if (label != null) {
			label.setValue((String)aEvent.getData());
		}
	}

	public void onClick(Event aEvent) {
		Events.postEvent("onPlayScene", runVideosceneselector, ((String)getAttribute("range")).split(","));
	}

	public void onFeedbackUpdate(Event aEvent) {
		boolean correct = runVideosceneselector.isCorrect((IXMLTag)getAttribute("tag"));
		if (!correct) {
			//deselect if not correct
			boolean selected = (boolean)getAttribute("selected");
			if (selected) {
				setAttribute("selected", false);
				setAttribute("color", "");
				Events.postEvent("onUpdate", this, null);
			}
		}
		else {
			Events.postEvent("onUpdate", this, null);
		}
	}

	public void onSetSelectedAndColor(Event aEvent) {
		String color = (String)aEvent.getData();
		String previousColor = (String)getAttribute("color");
		boolean selected = (boolean)getAttribute("selected");
		if (selected) {
			if (previousColor.equals(color)) {
				selected = !selected;
				setAttribute("selected", selected);
				color = "";
			}
		}
		else {
			selected = !selected;
			setAttribute("selected", selected);
		}
		setAttribute("color", color);
	}

	public void onUpdate(Event aEvent) {
		boolean selected = (boolean)getAttribute("selected");
		String color = (String)getAttribute("color");
		String style = (String)getAttribute("style");
		if (selected && color != null && !color.equals("")) {
			style += "background-color:" + color + ";";
		}
		setStyle(style);
		String sclass = "CRunVideosceneselectorCategorySceneSelectable";
		if (runVideosceneselector.isShowIfRightOrWrong((IXMLTag)getAttribute("tag"))) {
			if (runVideosceneselector.isRightlySelected((IXMLTag)getAttribute("tag"))) {
				sclass += "Right";
			}
			else {
				sclass += "Wrong";
			}
		}
		if (getSclass() == null || !getSclass().equals(sclass)) {
			setSclass(sclass);
		}
	}

}
