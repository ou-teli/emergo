/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.JuniorScientist;

import java.util.List;

import org.zkoss.zk.ui.event.Event;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.zkspring.STriggeredReference;

/**
 * This class is used to show a certain notification within a certain notifications case component.
 * Notifications case components should have present=false and present should stay false, to prevent the original notifications component to render the notification.
 * Because this class is location independent, locations entered within a notifications case component are ignored.
 * 
 * Notifications may have parameters that indicate how to decorate the mail. These parameters are stored in the explanation child tag of the mail:
 * - 'cover-background' (optional), see macro ZUL file for its operation
 * - 'notification-style' (optional), see macro ZUL file for its operation 
 * - 'line-style' (optional), see macro ZUL file for its operation
 * - 'cover-style' (optional), see macro ZUL file for its operation
 */
public class CRunJuniorScientistNotificationsBox extends CRunJuniorScientistBox {

	private static final long serialVersionUID = -2502186621927024608L;

	/** The current notification is defined by a notifications case component and a notification tag. */
	protected IECaseComponent notificationsCaseComponent;
	protected IXMLTag notificationTag;
	
	@Override
	public void onInit() {
		super.onInitMacro();
		
		initNotification();
		
		//determine fragment properties to be used within the child macro
		setNotificationProperties();
		
		//add the child macro
		addChildMacro("JuniorScientist_notifications_view_macro.zul");
	}
	
	public void initNotification() {
		//NOTE get notifications case component from a state that contains its name
		notificationsCaseComponent = getNotificationsComponent();
		if (notificationsCaseComponent == null) {
			return;
		}
		//NOTE get notification tag from a state that contains its pid
		notificationTag = getNotificationTag();
		if (notificationTag == null) {
			return;
		}

		//NOTE super ancestor CRunComponent needs case component to be set
		setCaseComponent(notificationsCaseComponent);
		
		//just like for the original notifications component set sent and opened for the notification
		setRunTagStatusJS(notificationsCaseComponent, notificationTag, AppConstants.statusKeySent, AppConstants.statusValueTrue, false);
		setRunTagStatusJS(notificationsCaseComponent, notificationTag, AppConstants.statusKeyOpened, AppConstants.statusValueTrue, false);
	}
	
	@Override
	public void onUpdate() {
		//rerender child macro with adjusted properties
		childMacro.recreate();
	}

	protected void setNotificationProperties() {
		propertyMap.put("notificationTag", notificationTag);
		propertyMap.put("notificationText", getNotificationText(notificationTag));
		List<String[]> lExplanationKeyValues = getParamKeyValues(notificationTag);
		for (String[] lKeyValue : lExplanationKeyValues) {
			propertyMap.put(lKeyValue[0], lKeyValue[1]);
		}
	}
	
	protected IECaseComponent getNotificationsComponent() {
		return sSpring.getCaseComponent("", getNotificationsComponentName());
	}
	
	protected IXMLTag getNotificationTag(String tagPid) {
		return getTagByPid(notificationsCaseComponent, tagPid);
	}
	
	protected IXMLTag getNotificationTag() {
		return getNotificationTag(getNotificationTagPid());
	}
	
	protected String getNotificationText(IXMLTag notificationTag) {
		if (notificationTag == null) {
			return "";
		}
		return sSpring.unescapeXML(notificationTag.getChildValue("richtext").replaceAll("<p>", "<p class=\"js-p\">"));
	}
	
	/** If a button is clicked within the child macro file, following event is triggered. */
	public void onButtonClick(Event event) {
		if (event.getData().equals("close_macro")) {
			//just like for the original notifications component set opened for the notification
			setRunTagStatusJS(notificationsCaseComponent, notificationTag, AppConstants.statusKeyOpened, AppConstants.statusValueFalse, false);
			//hide macro component
			setRunTagStatusJS(getNavigationCaseComponent(), currentTag, AppConstants.statusKeyPresent, AppConstants.statusValueFalse, false);
		}
	}
	
	@Override
	public void handleStatusChange(STriggeredReference aTriggeredReference) {
	}

}
