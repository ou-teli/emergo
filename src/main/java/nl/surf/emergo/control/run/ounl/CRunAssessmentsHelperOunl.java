/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.Treerow;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CTree;
import nl.surf.emergo.control.run.CRunArea;
import nl.surf.emergo.control.run.CRunAssessmentsHelper;
import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunAssessmentsHelperOunl. Helps rendering assessments.
 */
public class CRunAssessmentsHelperOunl extends CRunAssessmentsHelper {

	/** The last selected question id. */
	protected String lastSelectedItemId = "";

	/**
	 * Instantiates a new c run assessments helper.
	 *
	 * @param aTree the ZK assessments tree
	 * @param aShowTagNames the a show tag names, comma separated, only tags with these names are shown
	 * @param aCaseComponent the assessments case component
	 * @param aRunComponent the a run component, the ZK assessments component
	 */
	public CRunAssessmentsHelperOunl(CTree aTree, String aShowTagNames,	IECaseComponent aCaseComponent, CRunComponent aRunComponent) {
		super(aTree, aShowTagNames, aCaseComponent, aRunComponent);
	}

	@Override
	protected Component createStartButton (Component aParent, IXMLTag aItem) {
		CRunArea lButton = createButton("","startAssessment",aItem,CDesktopComponents.vView().getLabel("run_assessments.button.start_assessment"),"Small","");
		lButton.registerObserver("runAssessments");
		aParent.appendChild(lButton);
		return lButton;
	}

	@Override
	protected Component createEndButton (Component aParent, IXMLTag aItem) {
		CRunArea lButton = createButton("","endAssessment",aItem,CDesktopComponents.vView().getLabel("run_assessments.button.end_assessment"),"Small","");
		lButton.registerObserver("runAssessments");
		aParent.appendChild(lButton);
		return lButton;
	}

	protected CRunArea createButton (String aId, String aEventAction,
			Object aEventActionStatus, String aLabel, String aZclassExtension,
			String aClientOnClickAction) {
		return new CRunLabelButtonOunl(aId, aEventAction, aEventActionStatus, aLabel, aZclassExtension, aClientOnClickAction);
	}

	@Override
	protected Treecell renderTreecell(Treeitem aTreeitem, Treerow aTreerow,
			IXMLTag aItem, String aKeyValues, boolean aAccIsAuthor) {
		Treecell lTreecell = super.renderTreecell(aTreeitem, aTreerow, aItem, aKeyValues, aAccIsAuthor);
		String lImg = "";
		if (lastSelectedItemId.equals("")) {
			lastSelectedItemId = CDesktopComponents.sSpring().getCurrentRunComponentStatus(
				getCaseComponent(), AppConstants.statusKeySelectedTagId, AppConstants.statusTypeRunGroup);
		}
		if (aItem.getAttribute(AppConstants.keyId).equals(lastSelectedItemId)) {
			lImg = CDesktopComponents.sSpring().getStyleImgSrc("icon_selected_item");
		}
		if (lImg.equals("")) {
			//NOTE setImage("") does not clear image within Java. It does within zscript. So set image to empty image
			lImg = CDesktopComponents.sSpring().getStyleImgSrc("icon_empty");
		}
		lTreecell.setImage(lImg);
		return lTreecell;
	}

}