/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.feedback;

import org.zkoss.zk.ui.Component;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitInitBox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefHbox;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTag;

public class CRunPVToolkitFeedbackScoreHbox extends CRunPVToolkitDefHbox {
	
	private static final long serialVersionUID = 456507054823971884L;

	public String zulfilepath = ((CRunPVToolkitInitBox)CDesktopComponents.vView().getComponent("PV-toolkit_initBox")).getZulfilepath();
	
	protected int _numberOfPerformanceLevels = 0;
	protected int[] _performanceLevels;
	protected int[] _performanceLevelValues;
	
	protected CRunPVToolkitCacAndTag _runPVToolkitCacAndTag;
	protected int _level;
	protected String _feedbackRubricId;
	protected CRunPVToolkitFeedbackRubricDiv _feedbackRubric;
	
	protected String _classPrefix = "feedback";
	
	protected CRunPVToolkit pvToolkit;
	
	public CRunPVToolkitFeedbackScoreHbox(Component parent, String[] keys, Object[] values) {
		super(parent, keys, values);
	}
	
	public void init(CRunPVToolkitCacAndTag runPVToolkitCacAndTag, int level, String feedbackRubricId) {
		_runPVToolkitCacAndTag = runPVToolkitCacAndTag;
		_feedbackRubricId = feedbackRubricId;
		
		_feedbackRubric = (CRunPVToolkitFeedbackRubricDiv)CDesktopComponents.vView().getComponent(_feedbackRubricId);

		pvToolkit = (CRunPVToolkit)CDesktopComponents.vView().getComponent("pvToolkit");
		
		_numberOfPerformanceLevels = pvToolkit.getNumberOfPerformanceLevels();
		_performanceLevels = pvToolkit.getPerformanceLevels();
		_performanceLevelValues = pvToolkit.getPerformanceLevelValues();
		
		update(level);
	}
	
	public void update() {
		update(_level);
	}
	
	public void update(int level) {
		_level = level;

		getChildren().clear();

		for (int i=1;i<=_numberOfPerformanceLevels;i++) {
			if (_performanceLevels[i - 1] > 0) {
				appendStar(this, i, _level);
			}
		}

	}

	public void appendStar(Component parent, int level, int chosenLevel) {
		String src = "";
		//NOTE if performance levels are filtered, e.g., showing levels 1, 3 and 5 and omitting levels 2 and 4, given level scores have to be converted.
		//_numberOfPerformanceLevelsToShow then will be 3 instead of 5 and _performanceLevelValues contains the converted labels.
		//E.g., levels 1,2,3,4,5 will be converted to levels 1,2,2,2,3
		if (chosenLevel > 0 && _performanceLevelValues[chosenLevel - 1] >= _performanceLevelValues[level - 1]) {
			if (_feedbackRubric.inOverview()) {
				src = zulfilepath + "star-filled-blue.svg";
			}
			else {
				src = zulfilepath + "star-white.svg";
			}
		}
		else {
			src = zulfilepath + "star-gray.svg";
		}
		CRunPVToolkitFeedbackScoreImage image = new CRunPVToolkitFeedbackScoreImage(parent, 
				new String[]{"class", "src"}, 
				new Object[]{_classPrefix + "SubSkillLevel", src}
		);
		image.init(_runPVToolkitCacAndTag, level, _feedbackRubricId);
	}

}
