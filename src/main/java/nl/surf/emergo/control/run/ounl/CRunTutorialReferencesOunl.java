/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Tree;

import nl.surf.emergo.control.run.CRunArea;
import nl.surf.emergo.control.run.CRunHbox;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunTutorialReferencesOunl is used to show references within the run view area of the emergo player.
 */
public class CRunTutorialReferencesOunl extends CRunReferencesOunl {

	private static final long serialVersionUID = 6209100199031474889L;

	/**
	 * Instantiates a new c run references.
	 */
	public CRunTutorialReferencesOunl() {
		super();
	}

	/**
	 * Instantiates a new c run references.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the references case component
	 */
	public CRunTutorialReferencesOunl(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
	}

	/**
	 * Creates new content component, the references tree.
	 *
	 * @return the component
	 */
	@Override
	protected Component newContentComponent() {
		Tree tree = new CRunTutorialReferencesTreeOunl("runReferencesTree", caseComponent, this);
		// change rows
		tree.setRows(18);
		return tree;
	}

	/**
	 * Creates title area and shows name of case component within it.
	 * And adds close button
	 *
	 * @param aParent the ZK parent
	 *
	 * @return the c run area
	 */
	@Override
	protected CRunArea createTitleArea(Component aParent) {
		// NOTE Don't create title area
		return null;
	}

	/**
	 * Creates buttons area and adds close button.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run hbox
	 */
	@Override
	protected CRunHbox createButtonsArea(Component aParent) {
		// NOTE Don't create buttons area
		return null;
	}

}
