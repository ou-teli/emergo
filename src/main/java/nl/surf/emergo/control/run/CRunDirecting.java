/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Include;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefInclude;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;

/**
 * The Class CRunDirecting is used to show directing component within the run view area of the emergo player.
 */
public class CRunDirecting extends CRunComponent {

	private static final long serialVersionUID = -1507787257131960043L;

	/** The video view to show the video being played. It's an include so src can be changed. */
	protected Include videoView = null;

	/** The setting tags. */
	protected List<IXMLTag> settingtags = new ArrayList<IXMLTag>();

	/** The current setting tag. */
	protected IXMLTag currentsettingtag = null;

	/** The current view tag. */
	protected IXMLTag currentviewtag = null;

	/**
	 * Instantiates a new c run directing.
	 */
	public CRunDirecting() {
		super("runDirecting", null);
		init();
	}

	/**
	 * Instantiates a new c run directing.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the directing case component
	 */
	public CRunDirecting(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
		init();
	}

	public IXMLTag getCurrentSettingTag() {
		return currentsettingtag;
	}

	public IXMLTag getCurrentViewTag() {
		return currentviewtag;
	}

	/**
	 * Creates title area, content area to show directing tree,
	 * and buttons area with close button.
	 */
	@Override
	protected void createComponents() {
		CRunVbox lVbox = new CRunVbox();

		createTitleArea(lVbox);
		createContentArea(lVbox);
		createButtonsArea(lVbox);

		appendChild(lVbox);
	}

	/**
	 * Creates new content component, the directing tree.
	 *
	 * @return the component
	 */
	@Override
	protected Component newContentComponent() {
		return null;
	}

	/**
	 * Creates buttons area and adds close button.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run hbox
	 */
	@Override
	protected CRunHbox createButtonsArea(Component aParent) {
		CRunHbox lButtonsHbox = super.createButtonsArea(aParent);
		createCloseButton(lButtonsHbox);
		return lButtonsHbox;
	}

	/**
	 * Creates view for video.
	 * Gets current setting, sets selected and opened of it to true and starts it.
	 */
	public void init() {
		createVideoView(this);

		// get setting to play
		settingtags = getSettings();
		currentsettingtag = getCurrentSetting();

		// set setting status
		if (!(currentsettingtag == null)) {
			setRunTagStatus(caseComponent, currentsettingtag, AppConstants.statusKeySelected, AppConstants.statusValueTrue, true);
			setRunTagStatus(caseComponent, currentsettingtag, AppConstants.statusKeyOpened, AppConstants.statusValueTrue, true);
//			maybe add something for shared component
		}

		startSetting(currentsettingtag);
	}

	/**
	 * Creates video view.
	 *
	 * @param aParent the a parent
	 */
	protected void createVideoView(Component aParent) {
		videoView = new CDefInclude();
		videoView.setId("runSettingsVideoView");
		// set class or is sizing player enough?
		videoView.setZclass("");
		videoView.setWidth("256px");
		videoView.setHeight("144px");
		videoView.setVisible(false);
		aParent.appendChild(videoView);
	}

	/**
	 * Gets the run component helper.
	 *
	 * @return the run component helper
	 */
	@Override
	public CRunComponentHelper getRunComponentHelper() {
		return new CRunComponentHelper(null, "setting", caseComponent, this);
	}

	/**
	 * Gets all settings.
	 */
	public List<IXMLTag> getSettings() {
		CRunComponentHelper cComponent = (CRunComponentHelper)getRunComponentHelper();
		IXMLTag lXmlTree = cComponent.getXmlDataPlusStatusTree();
		if (lXmlTree != null) {
			return lXmlTree.getChild(AppConstants.contentElement).getChildTags(AppConstants.defValueNode);
		}
		else {
			return new ArrayList<IXMLTag>();
		}
	}

	/**
	 * Gets the current setting tag.
	 *
	 * @return the setting tag
	 */
	public IXMLTag getCurrentSetting() {
		if (settingtags == null || settingtags.size() == 0) {
			return null;
		}

		IXMLTag lCurrentSettingTag = null;
		if (settingtags.size() == 1) {
			//if only one setting and it is present and accessible than preselect this setting
			for (IXMLTag lSettingTag : settingtags) {
				boolean lPresent = CDesktopComponents.sSpring().getCurrentTagStatus(lSettingTag, AppConstants.statusKeyPresent).equals(AppConstants.statusValueTrue);
				boolean lAccessible = CDesktopComponents.sSpring().getCurrentTagStatus(lSettingTag, AppConstants.statusKeyAccessible).equals(AppConstants.statusValueTrue);
				if (lPresent && lAccessible && (lCurrentSettingTag == null)) {
					lCurrentSettingTag = lSettingTag;
				}
			}
		}
		return lCurrentSettingTag;
	}

	/**
	 * Gets video size for current video and, if set, sets size of video.
	 */
	protected void initVideoSize(IXMLTag aSettingTag) {
		// NOTE Get size of video. If not null, it overrules default values in style class.
		String lVideoSizeWidth = getVideoTagChildValue(aSettingTag, "size", 0);
		String lVideoSizeHeight = getVideoTagChildValue(aSettingTag, "size", 1);

		// NOTE Set videoView attributes to be used by video player
		videoView.setAttribute("video_size_width", lVideoSizeWidth);
		videoView.setAttribute("video_size_height", lVideoSizeHeight);
	}

	protected String getVideoTagChildValue(IXMLTag aSettingTag, String aVideoTagChildName, int index) {
		if (aSettingTag == null) {
			return "";
		}
		String values = aSettingTag.getChildValue(aVideoTagChildName);
		if (values.equals("")) {
			return "";
		}
		int value = 0;
		String[] arr = values.split(",");
		if (index >= arr.length) {
			return "";
		}
		try {
			value = Integer.parseInt(arr[index]);
		} catch (NumberFormatException e) {
			return "";
		}
		return "" + value;
	}

	public void startSetting(IXMLTag aSettingTag) {
		videoView.setSrc(VView.v_run_empty_fr);
		videoView.setVisible(false);
		if (settingtags == null) {
			return;
		}
		String lUrl = "";
		List<String> lViewLabels = new ArrayList<String>();
		if (aSettingTag != null) {
			initVideoSize(aSettingTag);
			lUrl = CDesktopComponents.sSpring().getSBlobHelper().getUrl(aSettingTag);
			IXMLTag lBlobChild = aSettingTag.getChild("blob");
			String lBlobtype = lBlobChild.getAttribute(AppConstants.keyBlobtype);
			if (!(lBlobtype.equals(AppConstants.blobtypeExtUrl))) {
				// put emergo path before url for player
				if (!CDesktopComponents.vView().isAbsoluteUrl(lUrl))
					lUrl = CDesktopComponents.vView().getEmergoRootUrl() + CDesktopComponents.vView().getEmergoWebappsRoot() + lUrl;
			}
			List<IXMLTag> lViewTags = aSettingTag.getChilds("view");
			if (lViewTags.size() != 0) {
				int i = 0;
				for (IXMLTag lViewTag : lViewTags) {
					String lViewLabel = CDesktopComponents.sSpring().unescapeXML(lViewTag.getChildValue("name"));
					lViewLabels.add(lViewLabel);
					if (i == 0) {
						setViewTag(lViewTag);
					}
					i++;
				}
			}
		}
		List<String> lSettingLabels = new ArrayList<String>();
		List<Boolean> lSettingsPresent = new ArrayList<Boolean>();
		List<Boolean> lSettingsAccessible = new ArrayList<Boolean>();
		for (IXMLTag lSettingTag : settingtags) {
			String lSettingLabel = CDesktopComponents.sSpring().unescapeXML(lSettingTag.getChildValue("name"));
			lSettingLabels.add(lSettingLabel);
			lSettingsPresent.add(CDesktopComponents.sSpring().getCurrentTagStatus(lSettingTag, AppConstants.statusKeyPresent).equals(AppConstants.statusValueTrue));
			lSettingsAccessible.add(CDesktopComponents.sSpring().getCurrentTagStatus(lSettingTag, AppConstants.statusKeyAccessible).equals(AppConstants.statusValueTrue));
		}
		videoView.setAttribute("directing_settinglabels", lSettingLabels);
		videoView.setAttribute("directing_settingspresent", lSettingsPresent);
		videoView.setAttribute("directing_settingsaccessible", lSettingsAccessible);
		videoView.setAttribute("directing_settingnumber", new Integer(getSettingNumber(aSettingTag)));
		videoView.setAttribute("directing_url", lUrl);
		videoView.setAttribute("directing_viewlabels", lViewLabels);
		videoView.setSrc(VView.v_run_flash_directing_fr);

		videoView.setVisible(true);

		if (runWnd != null)
			runWnd.setNoteTag(caseComponent, currentsettingtag);
	}

	/**
	 * Sets current setting tag and saves opened state.
	 *
	 * @param aSettingTag the a setting tag
	 */
	public void setSettingTag(IXMLTag aSettingTag) {
		if (aSettingTag != null) {
			if (currentsettingtag == null || !aSettingTag.getAttributeAsList(AppConstants.keyId).equals(currentsettingtag.getAttributeAsList(AppConstants.keyId))) {
//				maybe add something for shared component
				currentsettingtag = aSettingTag;
				setRunTagStatus(caseComponent, currentsettingtag, AppConstants.statusKeySelected, AppConstants.statusValueTrue, true);
				setRunTagStatus(caseComponent, currentsettingtag, AppConstants.statusKeyOpened, AppConstants.statusValueTrue, true);
				startSetting(currentsettingtag);
			}
		}
	}

	/**
	 * Sets current setting tag and.
	 *
	 * @param aSettingNumber the a setting number
	 */
	public void setSetting(String aSettingNumber) {
		IXMLTag lCurrentSettingTag = getSettingTag(aSettingNumber);
		setSettingTag(lCurrentSettingTag);
	}

	/**
	 * Gets current setting tag.
	 *
	 * @param aSettingNumber the a setting number
	 */
	public IXMLTag getSettingTag(String aSettingNumber) {
		if (aSettingNumber == null) {
			return null;
		}
		if (settingtags.size() == 0) {
			return null;
		}
		IXMLTag lCurrentSettingTag = null;
		int i = 0;
		for (IXMLTag lSettingTag : settingtags) {
			if (("" + (i + 1)).equals(aSettingNumber)) {
				lCurrentSettingTag = lSettingTag;
			}
			i++;
		}
		return lCurrentSettingTag;
	}

	/**
	 * Gets setting number.
	 *
	 * @param aSettingTag the a setting tag
	 */
	public int getSettingNumber(IXMLTag aSettingTag) {
		if (aSettingTag == null) {
			return 0;
		}
		if (settingtags.size() == 0) {
			return 0;
		}
		int lSettingNumber = 0;
		for (IXMLTag lSettingTag : settingtags) {
			lSettingNumber ++;
			if (aSettingTag.getAttributeAsList(AppConstants.keyId).equals(lSettingTag.getAttributeAsList(AppConstants.keyId))) {
				return lSettingNumber;
			}
		}
		return lSettingNumber;
	}

	/**
	 * Sets current view tag and saves opened state.
	 *
	 * @param aViewTag the a view tag
	 */
	public void setViewTag(IXMLTag aViewTag) {
		if (aViewTag != null) {
			if (currentviewtag == null || !aViewTag.getAttributeAsList(AppConstants.keyId).equals(currentviewtag.getAttributeAsList(AppConstants.keyId))) {
//				maybe add something for shared component
				currentviewtag = aViewTag;
				setRunTagStatus(caseComponent, currentviewtag, AppConstants.statusKeySelected, AppConstants.statusValueTrue, true);
				setRunTagStatus(caseComponent, currentviewtag, AppConstants.statusKeyOpened, AppConstants.statusValueTrue, true);
			}
		}
	}

	/**
	 * Sets current view tag and saves opened state.
	 *
	 * @param aViewNumber the a view number
	 */
	public void setView(String aViewNumber) {
		IXMLTag lCurrentViewTag = getViewTag(aViewNumber);
		setViewTag(lCurrentViewTag);
	}

	/**
	 * Gets current view tag.
	 *
	 * @param aViewNumber the a view number
	 */
	public IXMLTag getViewTag(String aViewNumber) {
		if (currentsettingtag == null || aViewNumber == null) {
			return null;
		}
		List<IXMLTag> lViewTags = currentsettingtag.getChilds("view");
		if (lViewTags.size() == 0) {
			return null;
		}
		IXMLTag lCurrentViewTag = null;
		int i = 0;
		for (IXMLTag lViewTag : lViewTags) {
			if (("" + (i + 1)).equals(aViewNumber)) {
				lCurrentViewTag = lViewTag;
			}
			i++;
		}
		return lCurrentViewTag;
	}

}
