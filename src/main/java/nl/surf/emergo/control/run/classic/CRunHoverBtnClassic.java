/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefButton;

/**
 * The Class CRunHoverBtn. Used to show a hover button, which show different images
 * when mouseover, mouseout, selected or disabled.
 */
public class CRunHoverBtnClassic extends CDefButton {

	private static final long serialVersionUID = -3471513527239037673L;

	/** The action to be send to observers when clicked. */
	protected String action = "";

	/** The actionstatus to be send to observers when clicked. */
	protected Object actionstatus = "";

	/** The imgprefix of the images used to show the different states. */
	protected String imgprefix = "";

	/** The label of the button. */
	protected String label = "";

	/** The client action to be executed when clicked. */
	protected String clientOnClickAction = "";

	/** The can have status selected. */
	protected boolean can_have_status_selected = true;

	/** The toggle status. */
	protected boolean toggle_status = false;

	/** The status. */
	protected String status = "";

	/** The run wnd. */
	protected CRunWndClassic runWnd = (CRunWndClassic) CDesktopComponents.vView().getComponent(CControl.runWnd);

	public String getClassName() {
		return CDesktopComponents.vView().getRunClassName(className);
	}
	
	/**
	 * Instantiates a new c run hover btn.
	 *
	 * @param aId the a id
	 * @param aStatus the a status
	 * @param aAction the a action
	 * @param aActionStatus the a action status
	 * @param aImgPrefix the a img prefix
	 * @param aClientOnClickAction the a client on click action
	 */
	public CRunHoverBtnClassic(String aId, String aStatus, String aAction,
			Object aActionStatus, String aImgPrefix, String aClientOnClickAction) {
		super();
		init(aId, aStatus, aAction, aActionStatus, aImgPrefix, "", aClientOnClickAction);
	}

	/**
	 * Instantiates a new c run hover btn classic.
	 *
	 * @param aId the a id
	 * @param aStatus the a status
	 * @param aAction the a action
	 * @param aActionStatus the a action status
	 * @param aImgPrefix the a img prefix
	 * @param aLabel the a label
	 * @param aClientOnClickAction the a client on click action
	 */
	public CRunHoverBtnClassic(String aId, String aStatus, String aAction,
			Object aActionStatus, String aImgPrefix, String aLabel, String aClientOnClickAction) {
		super();
		init(aId, aStatus, aAction, aActionStatus, aImgPrefix, aLabel, aClientOnClickAction);
	}


	/**
	 * Instantiates a new c run hover btn classic.
	 *
	 * @param aId the a id
	 * @param aStatus the a status
	 * @param aAction the a action
	 * @param aActionStatus the a action status
	 * @param aImgPrefix the a img prefix
	 * @param aLabel the a label
	 * @param aClientOnClickAction the a client on click action
	 */
	public void init(String aId, String aStatus, String aAction,
			Object aActionStatus, String aImgPrefix, String aLabel, String aClientOnClickAction) {
		setId(aId);
		setEventAction(aAction);
		setEventActionStatus(aActionStatus);
		imgprefix = aImgPrefix;
		label = aLabel;
		setLabel(label);
		if ((aClientOnClickAction != null) && (aClientOnClickAction.equals("")))
			aClientOnClickAction = null;
		clientOnClickAction = aClientOnClickAction;
		setStatus(aStatus);
	}

	/**
	 * Sets the has status selected.
	 *
	 * @param aStatus the new has status selected
	 */
	public void setCanHaveStatusSelected(boolean aStatus) {
		can_have_status_selected = aStatus;
	}

	/**
	 * Sets the toggle status.
	 *
	 * @param aStatus the new toggle status
	 */
	public void setToggleStatus(boolean aStatus) {
		toggle_status = aStatus;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Checks if status is aStatus.
	 *
	 * @param aStatus the a status
	 *
	 * @return true, if is status
	 */
	public boolean isStatus(String aStatus) {
		return (aStatus.equals(status));
	}

	public void setStatus(String aStatus) {
		setStatus(getClassName(), aStatus);
	}

	public void setStatus(String aClassName, String aStatus) {
		if ((aStatus == null) || (aStatus.equals("")))
			aStatus = "empty";
		status = aStatus;

		setDisabled(!status.equals("active") && !toggle_status);

		if (status.equals("empty"))
			setLabel("");
		else
			setLabel(label);

		setZclass(aClassName + "_" + status);

		if (isDisabled()) {
			setWidgetListener("onClick", null);
		}
		else {
			setWidgetListener("onClick", clientOnClickAction);
		}

		setHoverImage();
		setImage();
	}

	/**
	 * Sets hover image.
	 */
	protected void setHoverImage() {
		if (isDisabled()) {
			setHoverImage(null);
		}
		else {
			if (imgprefix != null && !imgprefix.equals("")) {
				String imgafterfix = "_mouseover";
				if (toggle_status && status.equals("selected")) {
					imgafterfix = "_mouseoverselected";
				}
				setHoverImage(CDesktopComponents.sSpring().getStyleImgSrc(imgprefix + imgafterfix));
			}
		}
	}

	/**
	 * Sets image.
	 */
	protected void setImage() {
		if (imgprefix != null && !imgprefix.equals("")) {
			setImage(CDesktopComponents.sSpring().getStyleImgSrc(imgprefix + "_" + status));
		}
	}

	/**
	 * Sets the btn visible.
	 *
	 * @param aVisible the new btn visible
	 */
	public void setBtnVisible(boolean aVisible) {
		if (aVisible)
//			setting visible implies active is true. that is if status is empty.
			if (isStatus("empty"))
				setStatus("active");
		setVisible(aVisible);
	}

	/**
	 * Sets the btn empty.
	 *
	 * @param aEmpty the new btn empty
	 */
	public void setBtnEmpty(boolean aEmpty) {
		if (aEmpty)
			setStatus("empty");
		else
			setStatus("active");
	}

	/**
	 * Sets the btn enabled.
	 *
	 * @param aEnabled the new btn enabled
	 */
	public void setBtnEnabled(boolean aEnabled) {
		if (aEnabled)
			setStatus("active");
		else
			setStatus("inactive");
	}

	/**
	 * Sets the btn selected.
	 *
	 * @param aSelected the new btn selected
	 */
	public void setBtnSelected(boolean aSelected) {
		if (aSelected)
			setStatus("selected");
		else
			setStatus("active");
	}

	/**
	 * Deselects the button.
	 */
	public void deselect() {
		if (isStatus("selected"))
			setStatus("active");
	}

	/**
	 * Sets the event action.
	 *
	 * @param aEventAction the new event action
	 */
	public void setEventAction(String aEventAction) {
		action = aEventAction;
	}

	/**
	 * Sets the event action status.
	 *
	 * @param aEventActionStatus the new event action status
	 */
	public void setEventActionStatus(Object aEventActionStatus) {
		actionstatus = aEventActionStatus;
	}

	public void onClick() {
		boolean lAccessible = getAttribute("accessible") == null || !getAttribute("accessible").equals(AppConstants.statusValueFalse);
		if (!lAccessible || status.equals("") || status.equals("inactive")) {
			return;
		}
		if (!status.equals("active") && !toggle_status)
			return;
		if (status.equals("active")) {
			if (can_have_status_selected) {
				setStatus("selected");
			}
		}
		else if (status.equals("selected")) {
			if (toggle_status) {
				setStatus("active");
			}
		}
		notifyObservers(action, actionstatus);
	}

	/**
	 * Simulates clicking on button.
	 */
	public void simulateOnClick() {
		this.onClick();
	}

	/**
	 * Gets the action status.
	 *
	 * @return the action status
	 */
	public Object getActionStatus() {
		return actionstatus;
	}
	
}
