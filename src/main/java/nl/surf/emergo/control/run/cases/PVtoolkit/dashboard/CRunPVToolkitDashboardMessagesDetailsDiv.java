/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.dashboard;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Div;
import org.zkoss.zul.Html;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.def.CDefHelper;
import nl.surf.emergo.control.def.CDefListcell;
import nl.surf.emergo.control.def.CDefListitem;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitInitBox;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitNavigationButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefHtml;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitMessage;
import nl.surf.emergo.control.run.cases.PVtoolkit.util.CRunPVToolkitCustomAttributeHelper;
import nl.surf.emergo.domain.IERunGroup;

public class CRunPVToolkitDashboardMessagesDetailsDiv extends CDefDiv {

	private static final long serialVersionUID = 4107795773073121387L;

	public String zulfilepath = ((CRunPVToolkitInitBox)CDesktopComponents.vView().getComponent("PV-toolkit_initBox")).getZulfilepath();
	
	protected CRunPVToolkit pvToolkit;
		
	protected IERunGroup _actor;
	protected boolean _editable;
	
	public boolean _initialized = false;
	
	protected boolean _showBackButton = true;

	protected Html messageBodyHtml;

	protected String _idPrefix = "dashboard";
	protected String _classPrefix = "dashboardMessagesDetails";
	
	public void onCreate(CreateEvent aEvent) {
		_showBackButton = CRunPVToolkitCustomAttributeHelper.handleCustomAttribute(this, "_showBackButton", _showBackButton);
	
		_idPrefix = CRunPVToolkitCustomAttributeHelper.handleCustomAttribute(this, "_idPrefix", _idPrefix);
	}
	
	public void init(IERunGroup actor, boolean editable) {
		_actor = actor;
		_editable = editable;
		
		//NOTE always initialize, because recordings my be added during a session
		setClass(_classPrefix);

		pvToolkit = (CRunPVToolkit)CDesktopComponents.vView().getComponent("pvToolkit");
		
		update();
		
		_initialized = true;
	}
	
	public void update() {
		getChildren().clear();
		
		pvToolkit.setMemoryCaching(true);
		
		Div div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{"dashboardBackground"}
		);
		
		div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{"titleRight"}
		);
		
		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font titleRight", "PV-toolkit-dashboard.header.messages"}
		);
		
		if (_showBackButton) {
			CRunPVToolkitNavigationButton btn = new CRunPVToolkitNavigationButton(this, 
					new String[]{"class", "cLabelKey"}, 
					new Object[]{"font pvtoolkitButton " + _classPrefix + "BackButton", "PV-toolkit.back"}
					);
			btn.init(getId(), _idPrefix + "Div");
		}

		div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "Div"}
		);
		
		Listbox listbox = appendMessagesListbox(div);
		int rowNumber = 1;
		rowNumber = appendMessagesToListbox(listbox, rowNumber);
		
		addListboxOnSelectEventListener(listbox);
		
		div = new CRunPVToolkitDefDiv(div, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "BodyDiv"}
		);
		
		messageBodyHtml = new CRunPVToolkitDefHtml(div, 
				new String[]{"class", "visible"}, 
				new Object[]{"font " + _classPrefix + "BodyHtml", "false"}
				);

		pvToolkit.setMemoryCaching(false);
	}

    protected Listbox appendMessagesListbox(Component parent) {
    	//title, sender, date/time, group, cycle, step state
    	return pvToolkit.appendListbox(
    			parent, 
    			getId() + "Listbox",
    			"position:absolute;left:0px;top:0px;width:1060px;height:320px;overflow:auto;", 
				new boolean[] {true, true, true, true, true, pvToolkit.hasCycleTags(), true, true}, 
    			new String[] {"3%", "30%", "20%", "18%", "13%", "7%", "15%", "10%"}, 
    			new boolean[] {false, true, true, true, true, true, true, true},
    			null,
    			"PV-toolkit-dashboard.column.label.", 
    			new String[] {"", "title", "sender", "datetime", "group", "cycle", "step", "status"});
    }

	protected int appendMessagesToListbox(Listbox listbox, int rowNumber) {
		return appendMessagesToListbox(
				listbox, 
				rowNumber,
				new boolean[] {true, true, true, true, true, pvToolkit.hasCycleTags(), true, true} 
				);
	}

	protected int appendMessagesToListbox(
			Listbox listbox, 
			int rowNumber,
    		boolean[] showColumn 
			) {
		for (CRunPVToolkitMessage message : pvToolkit.getMessages(_actor, true)) {
			Listitem listitem = new CDefListitem();
			listbox.appendChild(listitem);
			listitem.setAttribute("message", message);
			listitem.setAttribute("rowNumber", rowNumber);
			
			int columnNumber = 0;
			if (showColumn.length > columnNumber && showColumn[columnNumber]) {
				Listcell listcell = new CDefListcell();
				listitem.appendChild(listcell);
				new CRunPVToolkitDefImage(listcell, 
						new String[]{"class", "src"}, 
						new Object[]{"gridImage", zulfilepath + "envelope.svg"}
						);
			}
	
			columnNumber++;
			if (showColumn.length > columnNumber && showColumn[columnNumber]) {
				Listcell listcell = new CDefListcell();
				listitem.appendChild(listcell);
				new CRunPVToolkitDefLabel(listcell, 
						new String[]{"class", "value"}, 
						new Object[]{"font gridLabel", CDesktopComponents.sSpring().unescapeXML(message.getMessageTag().getChildValue("shorttitle"))}
						);
			}
	
			columnNumber++;
			if (showColumn.length > columnNumber && showColumn[columnNumber]) {
				Listcell listcell = new CDefListcell();
				listitem.appendChild(listcell);
				new CRunPVToolkitDefLabel(listcell, 
						new String[]{"class", "value"}, 
						new Object[]{"font gridLabel", (message.getSender() != null ? message.getSender().getERunGroup().getName() : CDesktopComponents.vView().getLabel("PV-toolkit"))}
						);
			}
	
			columnNumber++;
			if (showColumn.length > columnNumber && showColumn[columnNumber]) {
				Listcell listcell = new CDefListcell();
				listitem.appendChild(listcell);
				String sendDate = message.getSendDate();
				String sendTime = message.getSendTime();
				Label label = new CRunPVToolkitDefLabel(listcell, 
						new String[]{"class", "value"}, 
						new Object[]{"font gridLabel", sendDate + " " + sendTime}
						);
				label.setAttribute("orderValue", CDefHelper.getDateStrAsYMD(CDefHelper.getDateFromStrDMY(sendDate)) + sendTime);
			}
	
			columnNumber++;
			if (showColumn.length > columnNumber && showColumn[columnNumber]) {
				Listcell listcell = new CDefListcell();
				listitem.appendChild(listcell);
				new CRunPVToolkitDefLabel(listcell, 
						new String[]{"class", "value"}, 
						new Object[]{"font gridLabel", (message.getSender() != null ? pvToolkit.getPeerGroupName(message.getSender().getERunGroup(), _actor) : "")}
						);
			}
	
			columnNumber++;
			if (showColumn.length > columnNumber && showColumn[columnNumber]) {
				Listcell listcell = new CDefListcell();
				listitem.appendChild(listcell);
				int cycleNumber = 0;
				if (message.getStepTag() != null) {
					cycleNumber = pvToolkit.getCycleNumber(message.getStepTag().getParentTag());
				}
				Label label = new CRunPVToolkitDefLabel(listcell, 
						new String[]{"class", "value"}, 
						new Object[]{"font gridLabel", (cycleNumber > 0 ? "" + cycleNumber : "") }
						);
	
				String orderValue = "" + cycleNumber;
				while (orderValue.length() < 4) {
					orderValue = "0" + orderValue;
				}
				label.setAttribute("orderValue", orderValue);
			}
	
			columnNumber++;
			if (showColumn.length > columnNumber && showColumn[columnNumber]) {
				Listcell listcell = new CDefListcell();
				listitem.appendChild(listcell);
				String step = "";
				if (message.getStepTag() != null) {
					step = CDesktopComponents.vView().getLabel("PV-toolkit.steptype." + message.getStepTag().getChildValue("steptype"));
				}
				new CRunPVToolkitDefLabel(listcell, 
						new String[]{"class", "value"}, 
						new Object[]{"font gridLabel", step}
						);
			}
	
			columnNumber++;
			if (showColumn.length > columnNumber && showColumn[columnNumber]) {
				Listcell listcell = new CDefListcell();
				listitem.appendChild(listcell);
				Label label = new CRunPVToolkitDefLabel(listcell, 
						new String[]{"id", "class", "value"}, 
						new Object[]{getId() + "Listitem_" + rowNumber, "font gridLabel", "Nieuw"}
						);
				setMessageLabelVariableProperties(label, message);
			}
	
			rowNumber ++;

		}

		return rowNumber;
	}

	protected void setMessageLabelVariableProperties(Label label, CRunPVToolkitMessage message) {
		if (label == null || message == null) {
			return;
		}
		String labelKey = "";
		if (message.isRead()) {
			labelKey = "PV-toolkit-dashboard.row.label.read";
		}
		else {
			labelKey = "PV-toolkit-dashboard.row.label.new";
		}
		label.setValue(CDesktopComponents.vView().getLabel(labelKey));
	}
	
	protected void addListboxOnSelectEventListener(Listbox listbox) {
		listbox.addEventListener("onSelect", new EventListener<Event>() {
			public void onEvent(Event event) {
				Listitem listitem = listbox.getSelectedItem();
				if (listitem != null) {
					CRunPVToolkitMessage message = (CRunPVToolkitMessage)listitem.getAttribute("message");
					
					//replace all references to PV-tool content in message body
					Map<String,String> messageVarKeyValues = new HashMap<String,String>();
					String cycleNumber = "";
					if (message.getStepTag() != null) {
						cycleNumber = "" + pvToolkit.getCycleNumber(message.getStepTag().getParentTag());
					}
					messageVarKeyValues.put(CRunPVToolkit.messageVarCycleNumber, cycleNumber);
					if (message.getSender() != null) {
						messageVarKeyValues.put(CRunPVToolkit.messageVarSenderName, message.getSender().getERunGroup().getName());
					}
					if (message.getReceiver() != null) {
						messageVarKeyValues.put(CRunPVToolkit.messageVarReceiverName, message.getReceiver().getERunGroup().getName());
					}
					messageVarKeyValues.put(CRunPVToolkit.messageVarNewLine, "<br/>");
					
					// extra content
					IXMLTag messagedetailsTag = message.getMessagedetailsTag();
					String lExtraVarKeys = pvToolkit.getStatusChildTagAttribute(messagedetailsTag, "extrakeys");
					
					if (!StringUtils.isEmpty(lExtraVarKeys)) {
						String[] lExtras = lExtraVarKeys.split(",");
						int lNr = 1;
						for (String lExtra : lExtras) {
							String lVal = pvToolkit.getStatusChildTagAttribute(messagedetailsTag, "extravar_" + lNr);
							messageVarKeyValues.put(lExtra, lVal);
							lNr++;
						}
					}
					
					messageBodyHtml.setContent(CRunPVToolkit.replaceMessageVars(CDesktopComponents.sSpring().unescapeXML(message.getMessageTag().getChildValue("text").replace("\n", "<br/>")), messageVarKeyValues));
					messageBodyHtml.setVisible(true);
					
					if (!message.isRead()) {
						message.setRead(true);
						Label label = (Label)CDesktopComponents.vView().getComponent(getId() + "Listitem_" + listitem.getAttribute("rowNumber"));
						setMessageLabelVariableProperties(label, message);
						pvToolkit.setMessageRead(message);
					}
				}
			}
		});
	}
	
}
