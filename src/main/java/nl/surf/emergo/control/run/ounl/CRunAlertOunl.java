/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl;

import nl.surf.emergo.control.run.CRunAlert;
import nl.surf.emergo.control.run.CRunCloseBtn;
import nl.surf.emergo.control.run.CRunComponent;

/**
 * The Class CRunAlertOunl is used to show alerts within the run choice area of the Emergo player.
 * If an alert is shown on top of another alert, the previous alert is saved temporarily.
 */
public class CRunAlertOunl extends CRunAlert {

	private static final long serialVersionUID = -6647253236280215527L;

	/**
	 * Instantiates a new c run conversation interaction, a title and a text area.
	 */
	public CRunAlertOunl() {
		super();
	}

	/**
	 * Instantiates a new c run conversation interaction, a title and a text area.
	 *
	 * @param aId the a id
	 * @param aRunComponent the a run component
	 */
	public CRunAlertOunl(String aId, CRunComponent aRunComponent) {
		super();
	}

	/**
	 * Creates new close button.
	 *
	 * @return the button
	 */
	protected CRunCloseBtn newCloseBtn() {
		return new CRunCloseBtn(getId() + "CloseBtn", "active","endAlert", this, "", "");
	}
}
