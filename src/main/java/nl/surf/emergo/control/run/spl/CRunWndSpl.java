/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.spl;

import nl.surf.emergo.control.run.CRunAlert;
import nl.surf.emergo.control.run.CRunArea;
import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.control.run.CRunConversationInteraction;
import nl.surf.emergo.control.run.CRunHoverBtn;
import nl.surf.emergo.control.run.CRunMemo;
import nl.surf.emergo.control.run.CRunNotifications;
import nl.surf.emergo.control.run.CRunScores;
import nl.surf.emergo.control.run.ounl.CRunBreadcrumbsAreaOunl;
import nl.surf.emergo.control.run.ounl.CRunLocationAreaOunl;
import nl.surf.emergo.control.run.ounl.CRunWndOunl;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunWndSpl. The Spl Emergo player window.
 */
public class CRunWndSpl extends CRunWndOunl
 {

	private static final long serialVersionUID = 3115008833354141500L;

	/**
	 * Instantiates a new c run wnd.
	 * Reads google maps key out of .properties file and set it as session var,
	 * so google maps will work within the player.
	 */
	public CRunWndSpl() {
		super();
	}

	/**
	 * Creates tablet button.
	 *
	 * @param aStatus the a status
	 *
	 * @return run hover btn
	 */
	protected CRunHoverBtn createTabletBtn(String aStatus) {
		return new CRunTabletBtnSpl("runTabletBtn", aStatus, "showTablet", "", "tablet", "", "");
	}

	/**
	 * Creates note button.
	 *
	 * @param aStatus the a status
	 *
	 * @return run hover btn
	 */
	protected CRunHoverBtn createNoteBtn(String aStatus) {
		return new CRunNoteBtnSpl("runNoteBtn", aStatus, "showNote", "", "note", "", "");
	}

	/**
	 * Creates memo button.
	 *
	 * @param aStatus the a status
	 *
	 * @return run hover btn
	 */
	protected CRunHoverBtn createMemoBtn(String aStatus) {
		return new CRunMemoBtnSpl("runMemoBtn", aStatus, "toggleMemo", "", "memo", "", "");
	}

	/**
	 * Creates conversations component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createConversationsComponent(IECaseComponent aCaseComponent) {
		return new CRunConversationsSpl("runConversations", aCaseComponent);
	}
	
	/**
	 * Creates conversation interaction.
	 *
	 * @param aRunComponent the a run component
	 *
	 * @return run conversation interaction
	 */
	protected CRunConversationInteraction createConversationInteraction(CRunComponent aRunComponent) {
		return new CRunConversationInteractionSpl("runConversationInteraction", aRunComponent, "runConversations");
	}
	
	/**
	 * Creates tablet component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createTabletComponent(IECaseComponent aCaseComponent) {
		return new CRunTabletSpl("runTablet", aCaseComponent);
	}
	
	/**
	 * Creates references component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createReferencesComponent(IECaseComponent aCaseComponent) {
		return new CRunReferencesSpl("runReferences", aCaseComponent);
	}
	
	/**
	 * Creates mail component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createMailComponent(IECaseComponent aCaseComponent) {
		return new CRunMailSpl("runMail", aCaseComponent);
	}
	
	/**
	 * Creates assessments component.
	 *
	 * @param aCaseComponent the a case component
	 * @param aOnTablet the a on tablet
	 *
	 * @return run component
	 */
	protected CRunComponent createAssessmentsComponent(IECaseComponent aCaseComponent, boolean aOnTablet) {
		return new CRunAssessmentsSpl("runAssessments", aCaseComponent, aOnTablet);
	}
	
	/**
	 * Creates logbook component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createLogbookComponent(IECaseComponent aCaseComponent) {
		return new CRunLogbookSpl("runLogbook", aCaseComponent);
	}
	
	/**
	 * Creates google maps component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createGooglemapsComponent(IECaseComponent aCaseComponent) {
		return new CRunGooglemapsSpl("runGooglemaps", aCaseComponent);
	}
	
	/**
	 * Creates canon component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createCanonComponent(IECaseComponent aCaseComponent) {
		return new CRunCanonSpl("runCanon", aCaseComponent);
	}
	
	/**
	 * Creates canon result component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createCanonResultComponent(IECaseComponent aCaseComponent) {
		return new CRunCanonResultSpl("runCanonResult", aCaseComponent);
	}
	
	/**
	 * Creates tasks component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createTasksComponent(IECaseComponent aCaseComponent) {
		return new CRunTasksSpl("runTasks", aCaseComponent);
	}
	
	/**
	 * Creates directing component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createDirectingComponent(IECaseComponent aCaseComponent) {
		return new CRunDirectingSpl("runDirecting", aCaseComponent);
	}
	
	/**
	 * Creates memos component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createMemosComponent(IECaseComponent aCaseComponent) {
		return new CRunMemosSpl("runMemos", aCaseComponent);
	}
	
	/**
	 * Creates video manual component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createVideomanualComponent(IECaseComponent aCaseComponent) {
		return new CRunVideomanualSpl("runVideomanual", aCaseComponent);
	}
	
	/**
	 * Creates tutorial component.
	 *
	 * @param aCaseComponent the a case component
	 * @param aChapterId the a chapter id
	 *
	 * @return run component
	 */
	protected CRunComponent createTutorialComponent(IECaseComponent aCaseComponent, String aChapterId) {
		return new CRunTutorialSpl("runTutorial", aCaseComponent, aChapterId);
	}
	
	/**
	 * Creates profile component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createProfileComponent(IECaseComponent aCaseComponent) {
		return new CRunProfileSpl("runProfile", aCaseComponent);
	}
	
	/**
	 * Creates scores component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunScores createScoresComponent(IECaseComponent aCaseComponent) {
		return new CRunScoresSpl("runScores", aCaseComponent);
	}
	
	/**
	 * Creates notifications component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunNotifications createNotificationsComponent(IECaseComponent aCaseComponent) {
		//NOTE There can be multiple notifications components so add case component id within notifications id.
		return new CRunNotificationsSpl("runNotifications_" + aCaseComponent.getCacId(), aCaseComponent);
	}
	
	/**
	 * Creates note component.
	 *
	 * @param aRunComponent the a run component
	 *
	 * @return run component
	 */
	protected CRunArea createNoteComponent(CRunComponent aRunComponent) {
		return new CRunNoteSpl("runNote", aRunComponent);
	}
	
	/**
	 * Creates memo component.
	 *
	 * @return run component
	 */
	protected CRunMemo createMemoComponent() {
		return new CRunMemoSpl("runMemo");
	}
	
	/**
	 * Creates alert component.
	 *
	 * @param aRunComponent the a run component
	 *
	 * @return run component
	 */
	protected CRunAlert createAlertComponent(CRunComponent aRunComponent) {
		return new CRunAlertSpl("runAlert", aRunComponent);
	}
	
	/**
	 * Creates run location area.
	 *
	 * @return area
	 */
	protected CRunLocationAreaOunl createLocationArea() {
		return new CRunLocationAreaSpl("runLocationArea");
	}
	
	/**
	 * Creates breadcrumb area.
	 *
	 * @return area
	 */
	protected CRunBreadcrumbsAreaOunl createBreadcrumbsArea() {
		return new CRunBreadcrumbsAreaSpl("runBreadcrumbsArea");
	}
	
	/**
	 * Creates close area.
	 *
	 * @return area
	 */
	protected CRunArea createCloseArea() {
		return new CRunCloseAreaSpl("runCloseArea");
	}
	
}
