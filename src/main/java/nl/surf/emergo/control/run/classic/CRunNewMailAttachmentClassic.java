/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import org.zkoss.util.media.Media;
import org.zkoss.zul.Label;

import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CIObserved;

/**
 * The Class CRunNewMailAttachment. Is used to show a new mail attachtment
 */
public class CRunNewMailAttachmentClassic extends CRunHboxClassic {

	private static final long serialVersionUID = 957437133243320001L;

	/** The run wnd. */
	protected CRunWndClassic runWnd = (CRunWndClassic) CDesktopComponents.vView().getComponent(CControl.runWnd);

	/**
	 * Instantiates a new c run new mail attachment.
	 * 
	 * @param aMedia the a media
	 */
	public CRunNewMailAttachmentClassic(Media aMedia) {
		super();
		setAttribute("content", aMedia);
		// create all necessary components within window
		createComponents();
	}

	/**
	 * Creates components. A clickable label to open the attachment and a delete
	 * attachment button.
	 */
	public void createComponents() {
		// create label
		CRunAreaClassic lArea = new CRunAreaClassic();
		appendChild(lArea);
		lArea.setZclass(className + "_area");
		lArea.setStyle("width:400px;");
		Label lLabel = new CRunBlobLabelClassic((Media) getAttribute("content"));
		lArea.appendChild(lLabel);
		setId(getUuid());
		// create delete button
//		CRunHoverBtnClassic lButton = new CRunHoverBtnClassic("deleteMailAttachment_"+((Media) getAttribute("content")).getName(),
//				"active","deleteMailAttachment", null, "close", "close", "");
		CRunButtonClassic lButton = new CRunButtonClassic("deleteMailAttachment_"+((Media) getAttribute("content")).getName(),
				"deleteMailAttachment", null, CDesktopComponents.vView().getCLabel("delete"), "_component_100", "");
		lButton.registerObserver(this.getId());
		appendChild(lButton);
	}

	/**
	 * Is called by delete mail attachment button.
	 * 
	 * @param aObserved the observed object, it should have interface CIObserved implemented
	 * @param aAction the action
	 * @param aStatus the status object, can be String or other class
	 */
	@Override
	public void observedNotify(CIObserved aObserved, String aAction, Object aStatus) {
		if (aAction.equals("deleteMailAttachment"))
			delete();
	}

	/**
	 * Deletes this component.
	 */
	public void delete() {
		this.detach();
	}

}
