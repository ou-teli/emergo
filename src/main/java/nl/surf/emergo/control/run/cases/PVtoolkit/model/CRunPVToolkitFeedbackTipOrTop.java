/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.model;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.domain.IERunGroupAccount;

public class CRunPVToolkitFeedbackTipOrTop {

	IXMLTag skillTag;
	IXMLTag skillClusterTag;
	int skillClusterNumber;
	IXMLTag subSkillTag;
	IXMLTag stepTag;
	IXMLTag feedbackTag;
	boolean feedbackTagIsStatusTag;
	IERunGroupAccount peer;
	String feedbackName;
	String feedbackRole;
	String shareDate;
	String shareTime;
	boolean skillHasTipsTops;
	boolean skillclusterHasTipsTops;
	boolean subskillHasTipsTops;
	boolean tip;
	boolean top;
	String value;
	boolean mark;
	
	public IXMLTag getSkillTag() {
		return skillTag;
	}
	public void setSkillTag(IXMLTag skillTag) {
		this.skillTag = skillTag;
	}
	public IXMLTag getSkillClusterTag() {
		return skillClusterTag;
	}
	public void setSkillClusterTag(IXMLTag skillClusterTag) {
		this.skillClusterTag = skillClusterTag;
	}
	public int getSkillClusterNumber() {
		return skillClusterNumber;
	}
	public void setSkillClusterNumber(int skillClusterNumber) {
		this.skillClusterNumber = skillClusterNumber;
	}
	public IXMLTag getSubSkillTag() {
		return subSkillTag;
	}
	public void setSubSkillTag(IXMLTag subSkillTag) {
		this.subSkillTag = subSkillTag;
	}
	public IXMLTag getStepTag() {
		return stepTag;
	}
	public void setStepTag(IXMLTag stepTag) {
		this.stepTag = stepTag;
	}
	public IXMLTag getFeedbackTag() {
		return feedbackTag;
	}
	public void setFeedbackTag(IXMLTag feedbackTag) {
		this.feedbackTag = feedbackTag;
	}
	public boolean isFeedbackTagIsStatusTag() {
		return feedbackTagIsStatusTag;
	}
	public void setFeedbackTagIsStatusTag(boolean feedbackTagIsStatusTag) {
		this.feedbackTagIsStatusTag = feedbackTagIsStatusTag;
	}
	public IERunGroupAccount getPeer() {
		return peer;
	}
	public void setPeer(IERunGroupAccount peer) {
		this.peer = peer;
	}
	public String getFeedbackName() {
		return feedbackName;
	}
	public void setFeedbackName(String feedbackName) {
		this.feedbackName = feedbackName;
	}
	public String getFeedbackRole() {
		return feedbackRole;
	}
	public void setFeedbackRole(String feedbackRole) {
		this.feedbackRole = feedbackRole;
	}
	public String getShareDate() {
		return shareDate;
	}
	public void setShareDate(String shareDate) {
		this.shareDate = shareDate;
	}
	public String getShareTime() {
		return shareTime;
	}
	public void setShareTime(String shareTime) {
		this.shareTime = shareTime;
	}
	public boolean isSkillHasTipsTops() {
		return skillHasTipsTops;
	}
	public void setSkillHasTipsTops(boolean skillHasTipsTops) {
		this.skillHasTipsTops = skillHasTipsTops;
	}
	public boolean isSkillclusterHasTipsTops() {
		return skillclusterHasTipsTops;
	}
	public void setSkillclusterHasTipsTops(boolean skillclusterHasTipsTops) {
		this.skillclusterHasTipsTops = skillclusterHasTipsTops;
	}
	public boolean isSubskillHasTipsTops() {
		return subskillHasTipsTops;
	}
	public void setSubskillHasTipsTops(boolean subskillHasTipsTops) {
		this.subskillHasTipsTops = subskillHasTipsTops;
	}
	public boolean isTip() {
		return tip;
	}
	public void setTip(boolean tip) {
		this.tip = tip;
	}
	public boolean isTop() {
		return top;
	}
	public void setTop(boolean top) {
		this.top = top;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public boolean isMark() {
		return mark;
	}
	public void setMark(boolean mark) {
		this.mark = mark;
	}

}
