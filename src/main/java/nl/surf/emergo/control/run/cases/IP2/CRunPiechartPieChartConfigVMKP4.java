package nl.surf.emergo.control.run.cases.IP2;


public class CRunPiechartPieChartConfigVMKP4 {
	
    double value1 = 0.0D;
    double value2 = 0.0D;
    double value3 = 0.0D;
    double value4 = 0.0D;
	double value5 = 0.0D;
	double value6 = 0.0D;
 
    String category1 = "Category 1";
	String category2 = "Category 2";
	String category3 = "Category 3";
	String category4 = "Category 4";
	String category5 = "Category 5";
	String category6 = "Category 6";

	public String getCategory1() {
		return category1;
	}

	public void setCategory1(String category1) {
		this.category1 = category1;
	}

	public String getCategory2() {
		return category2;
	}

	public void setCategory2(String category2) {
		this.category2 = category2;
	}

	public String getCategory3() {
		return category3;
	}

	public void setCategory3(String category3) {
		this.category3 = category3;
	}

	public String getCategory4() {
		return category4;
	}

	public void setCategory4(String category4) {
		this.category4 = category4;
	}

	public String getCategory5() {
		return category5;
	}

	public void setCategory5(String category5) {
		this.category5 = category5;
	}

	public String getCategory6() {
		return category6;
	} 

	public void setCategory6(String category6) {
		this.category6 = category6;
	}

	public double getValue1() {
		return value1;
	}

	public void setValue1(double value1) {
		this.value1 = value1;
	}

	public double getValue2() {
		return value2;
	}

	public void setValue2(double value2) {
		this.value2 = value2;
	}

	public double getValue3() {
		return value3;
	}

	public void setValue3(double value3) {
		this.value3 = value3;
	}

	public double getValue4() {
		return value4;
	}

	public void setValue4(double value4) {
		this.value4 = value4;
	}

	public double getValue5() {
		return value5;
	}

	public void setValue5(double value5) {
		this.value5 = value5;
	}

	public double getValue6() {
		return value6;
	}

	public void setValue6(double value6) {
		this.value6 = value6;
	}

}
