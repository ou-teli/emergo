/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl;

import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.UploadEvent;

//import nl.surf.emergo.control.def.CDefFCKeditor;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefButton;
import nl.surf.emergo.control.def.CDefTextbox;
import nl.surf.emergo.control.run.CRunArea;
import nl.surf.emergo.control.run.CRunHbox;
import nl.surf.emergo.control.run.CRunNewMail;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunNewMailOunl. Is used to show input elements for new mail.
 */
public class CRunNewMailOunl extends CRunNewMail {

	private static final long serialVersionUID = -2347557385148580311L;

	/**
	 * Instantiates a new c run new mail.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the a case component
	 */
	public CRunNewMailOunl(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
	}

	/**
	 * Creates new button.
	 *
	 * @param aId the a id
	 * @param aAction the a action
	 * @param aActionStatus the a action status
	 * @param aLabel the a label
	 *
	 * @return the c run area
	 */
	protected CRunArea createNewNewMailButton(String aId, String aAction, Object aActionStatus, String aLabel) {
		return new CRunLabelButtonOunl(aId, aAction, aActionStatus, aLabel, "", "");
	}

	protected Component createNewMailAttachmentButton(Component aParent) {
		//NOTE Problem was: CRunArea cannot be used for uploading files
		//NOTE Solution: use hidden upload button to do the job
		//NOTE create the visible button, but don't register observers, so onClick event will have no effect
		CRunArea lButton = createNewNewMailButton("", "", null, CDesktopComponents.vView().getLabel("run_new_mail.button.new_attachment"));
		//NOTE a hidden upload button with the following id will do the job
		String lHiddenUploadButtonId = "runNewMailAttachmentBtn";
		//NOTE on the client a click on the visible button will generate a click on the hidden upload button
		lButton.setWidgetListener("onClick", "zk.Widget.$('$" + lHiddenUploadButtonId + "').$n().click();");
		aParent.appendChild(lButton);
		//NOTE now add the hidden upload button
		CDefButton lUploadButton = new CDefButton();
		lUploadButton.setId(lHiddenUploadButtonId);
		//hide the upload button
		lUploadButton.setVisible(false);
		//register observer
		lUploadButton.registerObserver(getId());
		//upload of one file with unlimited size is allowed
		lUploadButton.setUpload("true,maxsize=-1,native,multiple=false");
		lUploadButton.addEventListener("onUpload",
			new EventListener<UploadEvent>() {
				public void onEvent(UploadEvent event) {
					//in case of UploadEvent notify observers and provide media
					lUploadButton.notifyObservers("newMailAttachment", event.getMedia());
				}
			});
		aParent.appendChild(lUploadButton);
		return lButton;
	}

	protected Component createSendMailButton (Component aParent) {
		CRunArea lButton = createNewNewMailButton("runSendMailBtn", "sendMail", null,
				CDesktopComponents.vView().getLabel("send"));
		lButton.registerObserver(getId());
		aParent.appendChild(lButton);
		setButtonDisabled(lButton, true);
		return lButton;
	}

	protected void setButtonDisabled(Component aButton, boolean aDisabled) {
		((CRunLabelButtonOunl)aButton).setDisabled(aDisabled);
	}

	protected void setButtonVisible(Component aButton, boolean aDisabled) {
		((CRunLabelButtonOunl)aButton).setVisible(aDisabled);
	}

	protected Component createCancelMailButton (Component aParent) {
		CRunArea lButton = createNewNewMailButton("", "cancelMail", null, CDesktopComponents.vView()
				.getLabel("cancel"));
		lButton.registerObserver(getId());
		aParent.appendChild(lButton);
		return lButton;
	}

	/**
	 * Creates fck editor.
	 *
	 * @return fck editor
	 */
	protected CDefTextbox getFCKEditor() {
		CDefTextbox lEditor = super.getFCKEditor();
		lEditor.setWidth("624px");
		return lEditor;
	}

	/**
	 * Get new mail attachment.
	 *
	 * @return new mail attachment
	 */
	protected CRunHbox getNewMailAttachment(Media aMedia) {
		return new CRunNewMailAttachmentOunl(aMedia);
	}

}