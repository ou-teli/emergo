/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.Treerow;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CTree;
import nl.surf.emergo.control.def.CDefInclude;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;

/**
 * The Class CRunConversationsHelper. Helps rendering conversation tree.
 */
public class CRunConversationsHelper extends CRunComponentHelper {

	/** The run wnd. */
	protected CRunWnd runWnd = (CRunWnd)CDesktopComponents.vView().getComponent(CControl.runWnd);

	/** The max string length. */
	protected int maxStringLength = 80;

	/**
	 * Instantiates a new c run conversations helper.
	 *
	 * @param aTree the ZK conversations tree
	 * @param aShowTagNames the a show tag names, comma separated, only tags with these names are shown
	 * @param aCaseComponent the conversations case component
	 * @param aRunComponent the a run component, the ZK conversations component
	 */
	public CRunConversationsHelper(CTree aTree, String aShowTagNames, IECaseComponent aCaseComponent, CRunComponent aRunComponent) {
		super(aTree, aShowTagNames, aCaseComponent, aRunComponent);
	}

	@Override
	public String setItemAttributes(Component aObject, IXMLTag aItem) {
		// save xml tag
		aObject.setAttribute("item", aItem);
		String lKey = aItem.getDefAttribute(AppConstants.defKeyKey);
		String lKeyValues = CDesktopComponents.sSpring().unescapeXML(CDesktopComponents.sSpring().getXmlManager().getTagKeyValues(aItem, lKey));
		// save key values
		aObject.setAttribute("keyvalues", lKeyValues);
		if (aItem.getName().equals("alternative")) {
			return CDesktopComponents.sSpring().unescapeXML(aItem.getChildValue("text"));
		}
		return lKeyValues;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CContentHelper#renderTreerow(org.zkoss.zul.Treeitem, nl.surf.emergo.business.IXMLTag, java.lang.String, boolean)
	 */
	@Override
	protected Treerow renderTreerow(Treeitem aTreeitem, IXMLTag aItem,
			String aKeyValues, boolean aAccIsAuthor) {
		Treerow lTreerow = super.renderTreerow(aTreeitem, aItem, aKeyValues, aAccIsAuthor);
		CRunConversationInteraction lInteraction = null;
		if (getRunComponent() != null) {
			lInteraction = ((CRunConversations)getRunComponent()).getRunConversationInteraction();
		}
		if (lInteraction != null) {
			lTreerow.setZclass(lInteraction.getClassName() + "_treerow");
		}
		return lTreerow;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTreeHelper#renderTreecell(org.zkoss.zul.Treeitem, org.zkoss.zul.Treerow, nl.surf.emergo.business.IXMLTag, java.lang.String, boolean)
	 */
	@Override
	protected Treecell renderTreecell(Treeitem aTreeitem, Treerow aTreerow,
			IXMLTag aItem, String aKeyValues, boolean aAccIsAuthor) {
		Treecell lTreecell = getTreerowTreecell(aTreerow, 0);
		renderLabel(lTreecell, aItem, aKeyValues);
		String lStatus = "active";
		if (isOpened(aItem))
			lStatus = "opened";
		if (!isAccessible(aItem))
			lStatus = "inactive";
		CRunConversationInteraction lInteraction = null;
		if (getRunComponent() != null) {
			lInteraction = ((CRunConversations)getRunComponent()).getRunConversationInteraction();
		}
		if (lInteraction != null) {
			if (aItem.getName().equals("alternative")) {
				lTreecell.setZclass(lInteraction.getClassName() + "_treecellalt_"+lStatus);
			}
			else {
				lTreecell.setZclass(lInteraction.getClassName() + "_treecell_"+lStatus);
			}
		}
		return lTreecell;
	}

	protected void renderLabel(Treecell aTreecell, IXMLTag aItem, String aKeyValues) {
		if (aKeyValues.length() > maxStringLength) {
			aTreecell.setTooltiptext(aKeyValues);
			aKeyValues = aKeyValues.substring(0, maxStringLength) + "...";
		}
		aTreecell.setLabel(aKeyValues);
	}

	protected Component renderField(IXMLTag aItem) {
		Component lRunInteractionArea = CDesktopComponents.vView().getComponent(((CRunConversations)runComponent).getRunConversationInteraction().getId() + "InteractionArea");
		CDefInclude lInclude = new CDefInclude(); 
		lRunInteractionArea.appendChild(lInclude);
		lInclude.setAttribute("emergoComponent", runComponent);
		lInclude.setAttribute("tag", aItem);
		lInclude.setSrc(VView.v_run_conversation_editfield_fr);
		return lInclude;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunContentHelper#xmlContentToContentItems(nl.surf.emergo.business.IXMLTag, org.zkoss.zk.ui.Component)
	 */
	@Override
	public void xmlContentToContentItems(IXMLTag aXmlTree, Component aComponent) {
		xmlChildsToContentItems(aXmlTree.getChildTags(AppConstants.defValueNode), aComponent, null);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunComponentHelper#xmlChildsToContentItems(java.util.List, org.zkoss.zk.ui.Component, org.zkoss.zul.Treeitem)
	 */
	@Override
	public List<Component> xmlChildsToContentItems(List<IXMLTag> aChildTags, Component aParent,
			Component aInsertBefore) {
		// use internal method
		return xmlChildsToContentItems(aChildTags, aParent, aInsertBefore, false);
	}

	/**
	 * Converts xml child tags to content items. Nested.
	 * Content items are shown depending on their current status.
	 *
	 * @param aChildTags the xml child tags
	 * @param aParent the ZK parent component
	 * @param aInsertBefore the content item to insert before, if null it will be appended
	 * @param aShowChildren the show children
	 *
	 * @return the new content items
	 */
	protected List<Component> xmlChildsToContentItems(List<IXMLTag> aChildTags, Component aParent,
			Component aInsertBefore, boolean aShowChildren) {
		//aShowChildren is a property of a question or alternative, if it is true sub questions/alternatives (that are children of an answer fragment) should be rendered
		//right away and not just after a question/alternative is asked/chosen
		if (aChildTags == null || aChildTags.size() == 0) {
			return null;
		}
		List<Component> lContentItems = new ArrayList<Component>();
		for (IXMLTag lXMLTag : aChildTags) {
			//don't show hidden content
			boolean lHidden = CDesktopComponents.sSpring().getCurrentTagStatus(lXMLTag,
					AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse);
			if (!lHidden) {
				String lName = lXMLTag.getName();
				if (lName.equals("fragment")) {
					//opened determines active fragment, there can be other non-active fragments
					boolean lOpened = !CDesktopComponents.sSpring().getCurrentTagStatus(lXMLTag,
							AppConstants.statusKeyOpened).equals(AppConstants.statusValueFalse);
					//fragment has been played
					boolean lFinished = CDesktopComponents.sSpring().getCurrentTagStatus(lXMLTag,
							AppConstants.statusKeyFinished).equals(AppConstants.statusValueTrue);
					//interaction must be shown
					boolean lShowinteraction = CDesktopComponents.sSpring().getCurrentTagStatus(lXMLTag,
							AppConstants.statusKeyShowinteraction).equals(AppConstants.statusValueTrue);
					//if active fragment and parent question or alternative wants child questions/alternatives to be shown right away, or fragment is finished or interaction must be shown
					//then show sub questions/alternatives
					if (lOpened && (aShowChildren || lFinished || lShowinteraction)) {
						xmlChildsToContentItems(lXMLTag.getChildTags(AppConstants.defValueNode), aParent, null);
					}
				}
				else if (lName.equals("map") || lName.equals("question") || lName.equals("alternative") || lName.equals("set") || lName.equals("field") || lName.equals("button")) {
					List<IXMLTag> lTagsToRender = new ArrayList<IXMLTag>();
					if (lName.equals("set")) {
						//depending on property of set node children are randomized or not
						lTagsToRender = getNodeChildTagsPossiblyRandomized(lXMLTag);
					}
					else {
						lTagsToRender.add(lXMLTag);
					}
					for (IXMLTag lTagToRender : lTagsToRender) {
						if (lTagToRender.getName().equals("field")) {
							//NOTE do nothing. Rendering of field is handled in playFragment method of CRunConversations
						}
						else if (lTagToRender.getName().equals("button")) {
							//NOTE do nothing. Rendering of button is handled in playFragment method of CRunConversations
						}
						else {
							Component lContentItem = renderTreeitem(aParent, null, lTagToRender);
							if (lContentItem != null) {
								lContentItems.add(lContentItem);
								//is question or map or alternative outfolded
								boolean Outfolded = CDesktopComponents.sSpring().getCurrentTagStatus(
										lTagToRender, AppConstants.statusKeyOutfolded).equals(AppConstants.statusValueTrue);
								//if false fold it in, if true do nothing, default is true
								if (!Outfolded) {
									((Treeitem)lContentItem).setOpen(false);
								}
								//does question or alternative wants children to be shown, NOTE boolean will be true for maps
								boolean lShowChildren = !CDesktopComponents.sSpring().getCurrentTagStatus(
										lTagToRender, AppConstants.statusKeyOutfoldable).equals(AppConstants.statusValueFalse) ||
										lName.equals("set");
								if (lShowChildren)
									xmlChildsToContentItems(lTagToRender.getChildTags(AppConstants.defValueNode),
											lContentItem, null, lShowChildren);
								if (runWnd != null && ((Treeitem)lContentItem).getTreerow() != null) {
									((Treeitem)lContentItem).getTreerow().setWidgetListener("onClick", runWnd.getMediaplayerStopAction());
								}
							}
						}
					}
				}
			}
		}
		return lContentItems;
	}

	/**
	 * If no randomize returns node child tags.
	 * If randomize returns node child tags randomized. Only randomizes the first time, random order is saved in status.
	 *
	 * @param aParentTag the parent tag
	 *
	 * @return the (randomized) node child tags
	 */
	protected List<IXMLTag> getNodeChildTagsPossiblyRandomized(IXMLTag aParentTag) {
		boolean lRandomizeitems = CDesktopComponents.sSpring().getCurrentTagStatus(aParentTag,
				AppConstants.statusKeyRandomizeitems).equals(AppConstants.statusValueTrue);
		List<IXMLTag> lNodeChildTags = aParentTag.getChildTags(AppConstants.defValueNode);
		List<IXMLTag> lPresentNodeChildTags = new ArrayList<IXMLTag>();
		for (IXMLTag lNodeChildTag : lNodeChildTags) {
			if (!CDesktopComponents.sSpring().getCurrentTagStatus(lNodeChildTag,
					AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse)) {
				lPresentNodeChildTags.add(lNodeChildTag);
			}
		}
		if (!lRandomizeitems || lPresentNodeChildTags.size() < 2) {
			//only randomize if more than one item
			return lPresentNodeChildTags;
		}
		boolean lRandomized = CDesktopComponents.sSpring().getCurrentTagStatus(aParentTag,
				AppConstants.statusKeyRandomized).equals(AppConstants.statusValueTrue);

		// return randomized node child tags
		List<Integer> lIndexList = new ArrayList<Integer>();
		boolean lUseIndexList = true;
		if (!lRandomized) {
			//randomize lPresentNodeChildTags
			int lMaxNumber = lPresentNodeChildTags.size();
			while (lIndexList.size() < lMaxNumber) {
				int lIndex = Integer.parseInt("" + Math.round(Math.random() * lMaxNumber));
				if (lIndex == lMaxNumber) {
					lIndex = 0;
				}
				lIndex ++;
				if (!lIndexList.contains(lIndex)) {
					lIndexList.add(lIndex);
				}
			}
			for (int i=0;i<lMaxNumber;i++) {
				IXMLTag lNodeChildTag = lPresentNodeChildTags.get(i);
				//set index
				CDesktopComponents.sSpring().setRunTagStatus(getCaseComponent(), lNodeChildTag, AppConstants.statusKeyIndex, "" + lIndexList.get(i), true, getRunStatusType(), true, false);
			}
			CDesktopComponents.sSpring().setRunTagStatus(getCaseComponent(), aParentTag, AppConstants.statusKeyRandomized, AppConstants.statusValueTrue, true, getRunStatusType(), true, false);
		}
		else {
			//NOTE sometimes randomized is true, but indexes are not set, so are (partly) 0.
			//Probably because in previous class version indexes were extracted from XML tags just after they were set and aParentTag should have been updated before extracted the indexes
			//If one or more indexes are 0, don't use them
			for (IXMLTag lNodeChildTag : lPresentNodeChildTags) {
				int lIndex = Integer.parseInt(lNodeChildTag.getCurrentStatusAttribute(AppConstants.statusKeyIndex));
				if (lIndex < 1 || lIndex > lPresentNodeChildTags.size()) {
					lUseIndexList = false;
					break;
				}
				else {
					lIndexList.add(lIndex);
				}
			}
		}

		if (!lUseIndexList) {
			return lPresentNodeChildTags;
		}
		IXMLTag[] lRandomizedNodeChildTagsArr = new IXMLTag[lPresentNodeChildTags.size()];
		for (IXMLTag lNodeChildTag : lPresentNodeChildTags) {
			int lListIndex = lIndexList.get(lNodeChildTags.indexOf(lNodeChildTag)) - 1;
			lRandomizedNodeChildTagsArr[lListIndex] = lNodeChildTag;
		}
		List<IXMLTag> lRandomizedNodeChildTags = new ArrayList<IXMLTag>();
		for (int i=0;i<lRandomizedNodeChildTagsArr.length;i++) {
			lRandomizedNodeChildTags.add(lRandomizedNodeChildTagsArr[i]);
		}
		return lRandomizedNodeChildTags;
	}

	protected Component renderButton(IXMLTag aItem) {
		Component lRunInteractionArea = CDesktopComponents.vView().getComponent(((CRunConversations)runComponent).getRunConversationInteraction().getId() + "InteractionArea");
		HtmlMacroComponent macro = new HtmlMacroComponent();
		lRunInteractionArea.appendChild(macro);
		macro.setStyle(getStylePosition(aItem));
		macro.setDynamicProperty("a_observer", runComponent.getId());
		macro.setDynamicProperty("a_event", "onButtonClicked");
		macro.setDynamicProperty("a_eventdata", aItem);
		macro.setDynamicProperty("a_label", CDesktopComponents.sSpring().unescapeXML(aItem.getChildValue("text")));
		macro.setDynamicProperty("a_classtype", "DarkGreenVertical");
		macro.setDynamicProperty("a_textstyle", aItem.getChildValue("textstyle"));
		//get image scr
		String lImageSrc = CDesktopComponents.sSpring().getSBlobHelper().getUrl(aItem);
		if (!lImageSrc.equals("")) {
			//convert to UTF URL
			lImageSrc = CDesktopComponents.sSpring().getSBlobHelper().toUTFUrl(lImageSrc);
		}
		macro.setDynamicProperty("a_imagesrc", lImageSrc);
		macro.setMacroURI("../run_input_labelbtn_macro.zul");
		return macro;
	}

	protected String getStylePosition(IXMLTag tag) {
		String stylePosition = "";
		String[] position = tag.getChildValue("position").split(",");
		if (position.length == 2) {
			String left = getNumberStr(position[0], "");
			String top = getNumberStr(position[1], "");
			if (!left.equals("") && !top.equals("")) {
				stylePosition += "left:" + left + "px;top:" + top + "px;";
			}
		}
		if (!stylePosition.equals("")) {
			stylePosition = "position:absolute;" + stylePosition;
		}
		return stylePosition;
	}

	protected String getNumberStr(String aNumber, String aDefaultStr) {
		String number = aDefaultStr;
		try {
			number = "" + Integer.parseInt(aNumber);
		} catch (NumberFormatException e) {
			number = aDefaultStr;
		}
		return number;
	}

	protected int getNumber(String aNumber, int aDefaultNumber) {
		int number = aDefaultNumber;
		try {
			number = Integer.parseInt(aNumber);
		} catch (NumberFormatException e) {
			number = aDefaultNumber;
		}
		return number;
	}

}
