/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.superTeacher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.util.Clients;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.domain.IECaseComponent;

public class CRunPVToolkitSuperTeacherUpdateRubricSkillDiv extends CRunPVToolkitSuperTeacherNewRubricDiv {

	private static final long serialVersionUID = -4522501640293840196L;
	
	protected static final String skillTagName = "skill";
	protected static final String skillclusterTagName = "skillcluster";
	protected static final String subskillTagName = "subskill";
	protected static final String performancelevelTagName = "performancelevel";
	protected static final String performancelevelexampleTagName = "videoperformancelevelexample";
	
	protected StringBuffer skillElements;

	public void update() {
		//NOTE rubric is always stored in XML data and status that is cached within the SSpring class, regardless if preview is read only or not.
		IXMLTag nodeTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get("cacId")), _idMap.get("tagId"));
		if (nodeTag != null) {
			initSkillElements(nodeTag, true, false, true);
			
			Clients.evalJavaScript("initUpdateRubricSkill('" + skillElements.toString() + "','" + 
					getColorsAsJsonStr(pvToolkit.getSkillClusterColors()) + "','" +
					getColorsAsJsonStr(pvToolkit.getPerformanceLevelSkillClusterColors()) + "');");
		}
	}
		
	protected void initSkillElements(IXMLTag rubricTag, boolean addSkillElement, boolean addChildNodeTags, boolean addAddSkillElements) {
		skillElements = new StringBuffer();
		IXMLTag skillTag = rubricTag.getChild(skillTagName);
		if (addSkillElement) {
			addSkillElement(skillTag, addChildNodeTags);
		}
		//NOTE skill tag may have skill cluster tags
		for (IXMLTag skillclusterTag : skillTag.getChilds(skillclusterTagName)) {
			addSkillElement(skillclusterTag, addChildNodeTags);
			//NOTE skill cluster tag may have sub skill tags
			for (IXMLTag subskillTag : skillclusterTag.getChilds(subskillTagName)) {
				addSkillElement(subskillTag, addChildNodeTags);
			}
			if (addAddSkillElements) {
				//NOTE sub skill tag may be added to skill cluster tag
				addAddSkillElement(skillclusterTag, subskillTagName);
			}
		}
		if (addAddSkillElements) {
			//NOTE skill cluster tag may be added to skill tag
			addAddSkillElement(skillTag, skillclusterTagName);
		}
		//NOTE skill tag may have sub skill tags
		for (IXMLTag subskillTag : skillTag.getChilds(subskillTagName)) {
			addSkillElement(subskillTag, addChildNodeTags);
		}
		//NOTE sub skill tag may be added to skill tag
		if (addAddSkillElements) {
			addAddSkillElement(skillTag, subskillTagName);
		}
		skillElements.insert(0, "[");
		skillElements.append("]");
	}
		
	protected void addSkillElement(IXMLTag nodeTag, boolean addChildNodeTags) {
		addSkillElement(nodeTag.getParentTag(), nodeTag, "", addChildNodeTags);
	}

	protected void addAddSkillElement(IXMLTag parentTag, String tagName) {
		addSkillElement(parentTag, null, tagName, false);
	}

	protected void addSkillElement(IXMLTag parentTag, IXMLTag nodeTag, String tagName, boolean addChildNodeTags) {
		StringBuffer element = new StringBuffer();
		addElementPart(element, "parentTagId", parentTag.getAttribute(AppConstants.keyId), true);
		addElementPart(element, "parentTagName", parentTag.getName(), true);
		addElementPart(element, "tagId", nodeTag != null ? nodeTag.getAttribute(AppConstants.keyId) : "", true);
		addElementPart(element, "tagName", nodeTag != null ? nodeTag.getName() : tagName, true);
		addElementPart(element, "name", nodeTag != null ? sSpring.unescapeXML(nodeTag.getChildValue("name")) : "", true);
		addElementPart(element, "description", nodeTag != null ? sSpring.unescapeXML(nodeTag.getChildValue("description")).replace("\n", "") : "", addChildNodeTags);
		if (addChildNodeTags) {
			StringBuffer performancelevelElements = new StringBuffer();
			for (IXMLTag performancelevelTag : nodeTag.getChilds(performancelevelTagName)) {
				addPerformanceLevel(nodeTag, performancelevelTag, addChildNodeTags, performancelevelElements);
			}			
			performancelevelElements.insert(0, "[");
			performancelevelElements.append("]");
			addElementPart(element, "performanceLevels", performancelevelElements, false);
		}
		if (skillElements.length() > 0) {
			skillElements.append(",");
		}
		skillElements.append("{");
		skillElements.append(element);
		skillElements.append("}");
	}

	protected void addPerformanceLevel(IXMLTag parentTag, IXMLTag nodeTag, boolean addChildNodeTags, StringBuffer performancelevelElements) {
		StringBuffer element = new StringBuffer();
		addElementPart(element, "parentTagId", parentTag.getAttribute(AppConstants.keyId), true);
		addElementPart(element, "tagId", nodeTag.getAttribute(AppConstants.keyId), true);
		addElementPart(element, "tagName", nodeTag.getName(), true);
		addElementPart(element, "level", nodeTag.getChildValue("level"), true);
		addElementPart(element, "description", sSpring.unescapeXML(nodeTag.getChildValue("description")).replace("\n", ""), addChildNodeTags);
		if (addChildNodeTags) {
			StringBuffer performancelevelexampleElements = new StringBuffer();
			for (IXMLTag performancelevelexampleTag : nodeTag.getChilds(performancelevelexampleTagName)) {
				addPerformanceLevelExample(nodeTag, performancelevelexampleTag, addChildNodeTags, performancelevelexampleElements);
			}			
			performancelevelexampleElements.insert(0, "[");
			performancelevelexampleElements.append("]");
			addElementPart(element, "performancelevelexamples", performancelevelexampleElements, false);
		}
		if (performancelevelElements.length() > 0) {
			performancelevelElements.append(",");
		}
		performancelevelElements.append("{");
		performancelevelElements.append(element);
		performancelevelElements.append("}");
	}

	protected void addPerformanceLevelExample(IXMLTag parentTag, IXMLTag nodeTag, boolean addChildNodeTags, StringBuffer performancelevelexampleElements) {
		StringBuffer element = new StringBuffer();
		addElementPart(element, "parentTagId", parentTag.getAttribute(AppConstants.keyId), true);
		addElementPart(element, "tagId", nodeTag.getAttribute(AppConstants.keyId), true);
		addElementPart(element, "tagName", nodeTag.getName(), true);
		addElementPart(element, "blobId", nodeTag.getChildValue("blob"), true);
		addElementPart(element, "description", sSpring.unescapeXML(nodeTag.getChildValue("description")).replace("\n", ""), false);
		if (performancelevelexampleElements.length() > 0) {
			performancelevelexampleElements.append(",");
		}
		performancelevelexampleElements.append("{");
		performancelevelexampleElements.append(element);
		performancelevelexampleElements.append("}");
	}

	protected String getColorsAsJsonStr(String[] colorArr) {
		StringBuffer elements = new StringBuffer();
		for (int i=0;i<colorArr.length;i++) {
			addColor(colorArr[i], elements);
		}
		elements.insert(0, "[");
		elements.append("]");
		return elements.toString();
	}
	
	protected void addColor(String color, StringBuffer elements) {
		StringBuffer element = new StringBuffer();
		addElementPart(element, "color", color, false);
		if (elements.length() > 0) {
			elements.append(",");
		}
		elements.append("{");
		elements.append(element);
		elements.append("}");
	}

	public void onNotify (Event event) {
		JSONObject jsonObject = (JSONObject)event.getData();
		String action = (String)jsonObject.get("action");
		if (action.equals("create_skill_element")) {
			String parentTagId = (String)jsonObject.get("parentTagId");
			String tagName = (String)jsonObject.get("tagName");
			String elementName = (String)jsonObject.get("elementName");
			IXMLTag parentTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get("cacId")), parentTagId);
			if (parentTag.getName().equals(skillTagName)) {
				//NOTE insert skill element on right spot
				List<IXMLTag> beforeAndAfterTag = getSkillTagBeforeAndAfterTagForCreate(parentTag, tagName);
				//if skill element is sub skill add it after the last sub skill located directly under the skill tag, or if no sub skills add it before the first skill cluster tag
				IXMLTag tagBeforeNewTag = beforeAndAfterTag.get(0);
				IXMLTag tagAfterNewTag = beforeAndAfterTag.get(1);
				addSkillElementTag(parentTag, tagBeforeNewTag, tagAfterNewTag, tagName, elementName);
			}
			else {
				//NOTE parent is skill cluster, so just add to child tags of skill cluster
				addSkillElementTag(parentTag, null, null, tagName, elementName);
			}
			//rerendering content is more easy then updating client using javascript
			update();
		}
		else if (action.equals("edit_skill_element")) {
			String tagId = (String)jsonObject.get("tagId");
			String elementName = (String)jsonObject.get("elementName");
			IXMLTag elementTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get("cacId")), tagId);
			if (elementTag != null) {
				editElementTagChildValue(elementTag, "rubric", "pid", elementName);
				editElementTagChildValue(elementTag, "rubric", "name", elementName);
			}
		}
		else if (action.equals("delete_skill_element")) {
			String tagId = (String)jsonObject.get("tagId");
			boolean keepSubSkills = ((String)jsonObject.get("keepSubSkills")).equals("true");
			IXMLTag elementTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get("cacId")), tagId);
			if (elementTag != null) {
				deleteSkillElementTag(elementTag, keepSubSkills);
			}
			if (elementTag.getName().equals(skillclusterTagName)) {
				//rerendering content is more easy then updating client using javascript
				update();
			}
		}
		else if (action.equals("move_skill_element")) {
			String draggedElementTagId = (String)jsonObject.get("draggedElementTagId");
			String targetElementTagId = (String)jsonObject.get("targetElementTagId");
			boolean dropBelowTargetElement = ((String)jsonObject.get("dropBelowTargetElement")).equals("true");
			IXMLTag draggedElementTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get("cacId")), draggedElementTagId);
			IXMLTag targetElementTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get("cacId")), targetElementTagId);
			String draggedElementTagName = draggedElementTag.getName(); 
			String targetElementTagName = targetElementTag.getName();
			if (targetElementTagName.equals(skillTagName) && draggedElementTagName.equals(subskillTagName)) {
				//NOTE skill may only receive sub skills.
				List<IXMLTag> beforeAndAfterTag = getSkillTagBeforeAndAfterTagForMove(targetElementTag, draggedElementTag, targetElementTag, dropBelowTargetElement);
				IXMLTag tagBeforeTag = beforeAndAfterTag.get(0);
				IXMLTag tagAfterTag = beforeAndAfterTag.get(1);
				moveSkillElementTag(targetElementTag, draggedElementTag, tagBeforeTag, tagAfterTag);
			}
			else if (targetElementTagName.equals(skillclusterTagName)) {
				//NOTE skill cluster may receive skill clusters and sub skills 
				if (draggedElementTagName.equals(skillclusterTagName)) {
					List<IXMLTag> beforeAndAfterTag = getSkillTagBeforeAndAfterTagForMove(targetElementTag.getParentTag(), draggedElementTag, targetElementTag, dropBelowTargetElement);
					IXMLTag tagBeforeTag = beforeAndAfterTag.get(0);
					IXMLTag tagAfterTag = beforeAndAfterTag.get(1);
					if (dropBelowTargetElement) {
						//move just below target skill cluster
						tagAfterTag = null;
					}
					else {
						//move just above target skill cluster
						tagBeforeTag = null;
						tagAfterTag = targetElementTag;
					}
					moveSkillElementTag(targetElementTag.getParentTag(), draggedElementTag, tagBeforeTag, tagAfterTag);
				}
				else if (draggedElementTagName.equals(subskillTagName)) {
					//move as last child of target
					moveSkillElementTag(targetElementTag, draggedElementTag, null, null);
				}
			}
			else if (targetElementTagName.equals(subskillTagName) && draggedElementTagName.equals(subskillTagName)) {
				//NOTE sub skill may only receive sub skills.
				IXMLTag tagBeforeTag = null;
				IXMLTag tagAfterTag = null;
				if (dropBelowTargetElement) {
					//move just below target sub skill
					tagBeforeTag = targetElementTag;
				}
				else {
					//move just above target sub skill
					tagAfterTag = targetElementTag;
				}
				moveSkillElementTag(targetElementTag.getParentTag(), draggedElementTag, tagBeforeTag, tagAfterTag);
			}
			//rerendering content is more easy then updating client using javascript
			update();
		}
		else {
			super.onNotify(event);
		}
	}
	
	public List<IXMLTag> getSkillTagBeforeAndAfterTagForCreate(IXMLTag skillTag, String tagName) {
		IXMLTag tagBeforeTag = null;
		IXMLTag tagAfterTag = null;
		List<IXMLTag> subskillTags = skillTag.getChilds(subskillTagName);
		List<IXMLTag> skillclusterTags = skillTag.getChilds(skillclusterTagName);
		//if skill element is sub skill add it after the last sub skill located directly under the skill tag, or if no sub skills add it after the last skill cluster tag
		if (tagName.equals(subskillTagName)) {
			if (subskillTags.size() > 0) {
				tagBeforeTag = subskillTags.get(subskillTags.size() - 1); 
			}
			else if (skillclusterTags.size() > 0) {
				tagBeforeTag = skillclusterTags.get(skillclusterTags.size() - 1);
			}
		}
		//if skill element is skill cluster add it just after the last skill cluster or if no skill cluster just before the first sub skill located directly under the skill tag 
		else if (tagName.equals(skillclusterTagName)) {
			if (skillclusterTags.size() > 0) {
				tagBeforeTag = skillclusterTags.get(skillclusterTags.size() - 1);
			}
			else if (subskillTags.size() > 0) {
				tagAfterTag = subskillTags.get(0); 
			}
		}
		List<IXMLTag> beforeAndAfterTag = new ArrayList<IXMLTag>();
		beforeAndAfterTag.add(tagBeforeTag);
		beforeAndAfterTag.add(tagAfterTag);
		return beforeAndAfterTag;
	}
	
	public List<IXMLTag> getSkillTagBeforeAndAfterTagForMove(IXMLTag skillTag, IXMLTag draggedElementTag, IXMLTag targetElementTag, boolean dropBelowTargetElement) {
		IXMLTag tagBeforeTag = null;
		IXMLTag tagAfterTag = null;
		List<IXMLTag> subskillTags = skillTag.getChilds(subskillTagName);
		List<IXMLTag> skillclusterTags = skillTag.getChilds(skillclusterTagName);
		//if skill element is sub skill move it after the last sub skill located directly under the skill tag, or if no sub skills add it before the first skill cluster tag
		if (draggedElementTag.getName().equals(subskillTagName)) {
			if (subskillTags.size() > 0) {
				tagBeforeTag = subskillTags.get(subskillTags.size() - 1); 
			}
			else if (skillclusterTags.size() > 0) {
				tagAfterTag = skillclusterTags.get(0);
			}
		}
		//if skill element is skill cluster move it just below or above the target element, which always will be a skill cluster 
		else if (draggedElementTag.getName().equals(skillclusterTagName)) {
			if (dropBelowTargetElement) {
				//move just below target skill cluster
				tagBeforeTag = targetElementTag;
			}
			else {
				//move just above target skill cluster
				tagAfterTag = targetElementTag;
			}
		}
		List<IXMLTag> beforeAndAfterTag = new ArrayList<IXMLTag>();
		beforeAndAfterTag.add(tagBeforeTag);
		beforeAndAfterTag.add(tagAfterTag);
		return beforeAndAfterTag;
	}
	
	protected IXMLTag addSkillElementTag(IXMLTag parentTag, IXMLTag tagBeforeNewTag, IXMLTag tagAfterNewTag, String tagName, String elementName) {
		IXMLTag elementTag = null;
		IECaseComponent caseComponent = sSpring.getCaseComponent(_idMap.get("cacId"));
		Map<String,String> childTagNameValueMap = new HashMap<String,String>();
		childTagNameValueMap.put("pid", elementName);
		childTagNameValueMap.put("name", elementName);
		if (!sSpring.isReadOnlyRun()) {
			//NOTE if not read only run update data tree. It is updated in application memory as well.
			IXMLTag dataRootTag = sSpring.getXmlDataTree(caseComponent);
			parentTag = sSpring.getXmlManager().getTagById(dataRootTag, parentTag.getAttribute(AppConstants.statusKeyId));
			IXMLTag nodeTag = null;
			if (tagBeforeNewTag != null) {
				tagBeforeNewTag = sSpring.getXmlManager().getTagById(dataRootTag, tagBeforeNewTag.getAttribute(AppConstants.statusKeyId));
				nodeTag = addNodeTagAfterTag(parentTag, tagBeforeNewTag, tagName, childTagNameValueMap);
			}
			else if (tagAfterNewTag != null) {
				tagAfterNewTag = sSpring.getXmlManager().getTagById(dataRootTag, tagAfterNewTag.getAttribute(AppConstants.statusKeyId));
				nodeTag = addNodeTagBeforeTag(parentTag, tagAfterNewTag, tagName, childTagNameValueMap);
			}
			else {
				nodeTag = addNodeTag(parentTag, tagName, childTagNameValueMap);
			}
			if (nodeTag != null) {
				addPerformancelevelTags(nodeTag);
			}
			sSpring.setCaseComponentData("" + caseComponent.getCacId(), sSpring.getXmlManager().xmlTreeToDoc(dataRootTag));
		}
		//NOTE always update data plus status tree cached as SSpring property. This will work for both preview and preview read-only mode. 
		IXMLTag dataPlusStatusRootTag = sSpring.getXmlDataPlusRunStatusTree(caseComponent, AppConstants.statusTypeRunGroup);
		parentTag = sSpring.getXmlManager().getTagById(dataPlusStatusRootTag, parentTag.getAttribute(AppConstants.statusKeyId));
		if (tagBeforeNewTag != null) {
			tagBeforeNewTag = sSpring.getXmlManager().getTagById(dataPlusStatusRootTag, tagBeforeNewTag.getAttribute(AppConstants.statusKeyId));
			elementTag = addNodeTagAfterTag(parentTag, tagBeforeNewTag, tagName, childTagNameValueMap);
		}
		else if (tagAfterNewTag != null) {
			tagAfterNewTag = sSpring.getXmlManager().getTagById(dataPlusStatusRootTag, tagAfterNewTag.getAttribute(AppConstants.statusKeyId));
			elementTag = addNodeTagBeforeTag(parentTag, tagAfterNewTag, tagName, childTagNameValueMap);
		}
		else {
			elementTag = addNodeTag(parentTag, tagName, childTagNameValueMap);
		}
		if (elementTag != null) {
			addPerformancelevelTags(elementTag);
		}
		
		//NOTE return data plus status element because it is always set
		return elementTag;
	}

	protected IXMLTag moveSkillElementTag(IXMLTag parentTag, IXMLTag draggedTag, IXMLTag tagBeforeTag, IXMLTag tagAfterTag) {
		IXMLTag elementTag = null;
		IECaseComponent caseComponent = sSpring.getCaseComponent(_idMap.get("cacId"));
		if (!sSpring.isReadOnlyRun()) {
			//NOTE if not read only run update data tree. It is updated in application memory as well.
			IXMLTag dataRootTag = sSpring.getXmlDataTree(caseComponent);
			parentTag = sSpring.getXmlManager().getTagById(dataRootTag, parentTag.getAttribute(AppConstants.statusKeyId));
			draggedTag = sSpring.getXmlManager().getTagById(dataRootTag, draggedTag.getAttribute(AppConstants.statusKeyId));
			if (tagBeforeTag != null) {
				tagBeforeTag = sSpring.getXmlManager().getTagById(dataRootTag, tagBeforeTag.getAttribute(AppConstants.statusKeyId));
				moveNodeTagAfterTag(parentTag, draggedTag, tagBeforeTag);
			}
			else if (tagAfterTag != null) {
				tagAfterTag = sSpring.getXmlManager().getTagById(dataRootTag, tagAfterTag.getAttribute(AppConstants.statusKeyId));
				moveNodeTagBeforeTag(parentTag, draggedTag, tagAfterTag);
			}
			else {
				moveNodeTag(parentTag, draggedTag);
			}
			sSpring.setCaseComponentData("" + caseComponent.getCacId(), sSpring.getXmlManager().xmlTreeToDoc(dataRootTag));
		}
		//NOTE always update data plus status tree cached as SSpring property. This will work for both preview and preview read-only mode. 
		IXMLTag dataPlusStatusRootTag = sSpring.getXmlDataPlusRunStatusTree(caseComponent, AppConstants.statusTypeRunGroup);
		parentTag = sSpring.getXmlManager().getTagById(dataPlusStatusRootTag, parentTag.getAttribute(AppConstants.statusKeyId));
		draggedTag = sSpring.getXmlManager().getTagById(dataPlusStatusRootTag, draggedTag.getAttribute(AppConstants.statusKeyId));
		if (tagBeforeTag != null) {
			tagBeforeTag = sSpring.getXmlManager().getTagById(dataPlusStatusRootTag, tagBeforeTag.getAttribute(AppConstants.statusKeyId));
			elementTag = moveNodeTagAfterTag(parentTag, draggedTag, tagBeforeTag);
		}
		else if (tagAfterTag != null) {
			tagAfterTag = sSpring.getXmlManager().getTagById(dataPlusStatusRootTag, tagAfterTag.getAttribute(AppConstants.statusKeyId));
			elementTag = moveNodeTagBeforeTag(parentTag, draggedTag, tagAfterTag);
		}
		else {
			elementTag = moveNodeTag(parentTag, draggedTag);
		}
		
		//NOTE return data plus status element because it is always set
		return elementTag;
	}

	protected IXMLTag deleteSkillElementTag(IXMLTag elementTag, boolean ifSkillClusterTagKeepSubSkillTags) {
		IECaseComponent caseComponent = getCaseComponent("rubric");
		if (!sSpring.isReadOnlyRun()) {
			//NOTE if not read only run update data tree. It is updated in application memory as well.
			IXMLTag rootTag = sSpring.getXmlDataTree(caseComponent);
			if (ifSkillClusterTagKeepSubSkillTags) {
				elementTag = sSpring.getXmlManager().getTagById(rootTag, elementTag.getAttribute(AppConstants.statusKeyId));
				ifSkillClusterTagKeepSubSkillTags(elementTag);
			}
			deleteNodeTag(rootTag, elementTag);
			sSpring.setCaseComponentData("" + caseComponent.getCacId(), sSpring.getXmlManager().xmlTreeToDoc(rootTag));
		}
		//NOTE always update data plus status tree cached as SSpring property. This will work for both preview and preview read-only mode. 
		IXMLTag rootTag = sSpring.getXmlDataPlusRunStatusTree(caseComponent, AppConstants.statusTypeRunGroup);
		if (ifSkillClusterTagKeepSubSkillTags) {
			elementTag = sSpring.getXmlManager().getTagById(rootTag, elementTag.getAttribute(AppConstants.statusKeyId));
			ifSkillClusterTagKeepSubSkillTags(elementTag);
		}
		elementTag = deleteNodeTag(rootTag, elementTag);
		//NOTE return data plus status element because it is always set
		return elementTag;
	}
	
	protected void ifSkillClusterTagKeepSubSkillTags(IXMLTag elementTag) {
		if (!elementTag.getName().equals(skillclusterTagName)) {
			return;
		}
		IXMLTag skillTag = elementTag.getParentTag();
		List<IXMLTag> subskillTagsOfSkillTag = skillTag.getChilds(subskillTagName);
		IXMLTag tagBeforeNewTag = null; 
		if (subskillTagsOfSkillTag.size() > 0) {
			tagBeforeNewTag = subskillTagsOfSkillTag.get(subskillTagsOfSkillTag.size() - 1);
		}
		List<IXMLTag> subskillTags = elementTag.getChilds(subskillTagName);
		for (IXMLTag subSkillTag : subskillTags) {
			subSkillTag.getParentTag().getChildTags().remove(subSkillTag);
			subSkillTag.setParentTag(skillTag);
			int index = 0;
			if (tagBeforeNewTag != null) {
				index = skillTag.getChildTags().indexOf(tagBeforeNewTag) + 1;
			}
			skillTag.getChildTags().add(index, subSkillTag);
			tagBeforeNewTag = subSkillTag;
		}
	}
	
}