/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;

public class CRunAssessmentItemAltButton extends CDefDiv {

	private static final long serialVersionUID = 816718663833939225L;

	public void onCreate(CreateEvent aEvent) {
		setAttribute("observer", aEvent.getArg().get("a_observer"));
		setAttribute("event", aEvent.getArg().get("a_event"));
		setAttribute("assessmentItemAltTagId", aEvent.getArg().get("a_assessmentitem_alt_tag_id"));
		setAttribute("assessmentItemType", aEvent.getArg().get("a_assessmentitem_type"));
		setAttribute("checked", aEvent.getArg().get("a_assessmentitem_alt_answer"));
		setAttribute("disabled", aEvent.getArg().get("a_is_assessmentitem_editable").equals("false"));

		setId("alt" + getAttribute("assessmentItemAltTagId"));
		if (getAttribute("assessmentItemType").equals("mkea")) {
			if (getAttribute("checked").equals("true")) {
				Component lParent = CDesktopComponents.vView().getComponent("runAssessmentItemAlts");
				lParent.setAttribute("previouslyCheckedOption", this);
			}
		}
		Events.postEvent("onRender", this, null);
	}

	public void onRender() {
		// render checked or not
		String sclass = "CRunAssessmentAltCheckbox";
		if (getAttribute("assessmentItemType").equals("mkea"))
			sclass = "CRunAssessmentAltRadio";
		if ((Boolean)getAttribute("disabled")) {
			sclass += "Disabled";
		}
		if (getAttribute("checked").equals("true")) {
			sclass += "Selected";
		}
		else {
			sclass += "Active";
		}
		setSclass(sclass);
		HtmlBasedComponent lParent = (HtmlBasedComponent)CDesktopComponents.vView().getComponent("runAssessmentItemAlt" + getAttribute("assessmentItemAltTagId"));
		if (getAttribute("checked").equals("true")) {
			lParent.setStyle("border: 1px solid #1967b9;");
		}
		else {
			lParent.setStyle("border: 1px solid transparent");
		}
	}

	public void onClick() {
		if (!(Boolean)getAttribute("disabled")) {
			// set checked or not
			if (getAttribute("checked").equals("true")) {
				setAttribute("checked", "false");
			}
			else {
				setAttribute("checked", "true");
			}
			Events.postEvent("onRender", this, null);
			if (getAttribute("assessmentItemType").equals("mkea")) {
				Component lParent = CDesktopComponents.vView().getComponent("runAssessmentItemAlts");
				if (getAttribute("checked").equals("true")) {
					// uncheck previous option
					Component lCheckbox = (Component)lParent.getAttribute("previouslyCheckedOption");
					if (lCheckbox != null) {
						lCheckbox.setAttribute("checked", "false");
						Events.postEvent("onRender", lCheckbox, null);
					}
					lParent.setAttribute("previouslyCheckedOption", this);
				}
				else {
					lParent.setAttribute("previouslyCheckedOption", null);
				}
			}
			Object[] eventData = new Object[2];
			eventData[0] = getAttribute("assessmentItemAltTagId");
			eventData[1] = getAttribute("checked").equals("true");
			Events.postEvent((String)getAttribute("event"), CDesktopComponents.vView().getComponent((String)getAttribute("observer")), eventData);
		}
	}

}
