/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;

public class CRunSelectionformContainerDiv extends CDefDiv {

	private static final long serialVersionUID = 4477888670417824436L;

	public void onCreate(CreateEvent aEvent) {
		Events.postEvent("onUpdate", this, null);
	}

	public void onUpdate(Event aEvent) {
		CRunSelectionforms runSelectionforms = (CRunSelectionforms)CDesktopComponents.vView().getComponent(CRunSelectionforms.runSelectionformsId);
		if (runSelectionforms == null) {
			return;
		}
		List<Component> selectionformItems = CDesktopComponents.vView().getComponentsByPrefix("selectionformItem_");
		int numberOfSelected = 0;
		int numberOfRightSelected = 0;
		for (Component selectionformItem : selectionformItems) {
			if ((boolean)selectionformItem.getAttribute("selected")) {
				numberOfSelected++;
				if (runSelectionforms.isRightlySelected((IXMLTag)selectionformItem.getAttribute("tag"))) {
					numberOfRightSelected++;
				}
			}
		}
		setAttribute("numberofselecteditems", "" + numberOfSelected);
		Component labelNumberOfSelected = CDesktopComponents.vView().getComponent("selectionformItemBoxNumberOfSelected_" + ((IXMLTag)getAttribute("tag")).getAttribute("id"));
		if (labelNumberOfSelected != null) {
			Events.postEvent("onUpdate", labelNumberOfSelected, "" + numberOfSelected);
			boolean highlightright = numberOfRightSelected == Integer.parseInt((String)getAttribute("maxitemstoselect"));
			boolean highlight = Integer.parseInt((String)getAttribute("numberofselecteditems")) == Integer.parseInt((String)getAttribute("maxitemstoselect"));
			if (highlightright) {
				Events.postEvent("onSetSclassExtension", labelNumberOfSelected, "HighlightRight");
			}
			else if (highlight) {
				Events.postEvent("onSetSclassExtension", labelNumberOfSelected, "Highlight");
			}
			else {
				Events.postEvent("onSetSclassExtension", labelNumberOfSelected, "");
			}
		}

	}

}
