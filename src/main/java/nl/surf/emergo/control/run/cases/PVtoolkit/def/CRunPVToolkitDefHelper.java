/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.def;

import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.impl.InputElement;

import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTag;

public class CRunPVToolkitDefHelper {
	
	private static final String pvToolkitComponentId = "pvToolkit";
	private static final String labelKeyId = "labelKey";
	private static final String cLabelKeyId = "cLabelKey";

	private CRunPVToolkitDefHelper() {
		throw new IllegalStateException("Utility class");
	}

	public static void fillKeyValueMap(Component component, String[] keys, Object[] values, Map<String,Object> keyValueMap) {
		if (keys != null && values != null) {
			for (int i=0;i<keys.length;i++) {
				if (i < values.length) {
					keyValueMap.put(keys[i], values[i]);
				}
			}
			component.setAttribute("keyValueMap", keyValueMap);
		}
	}
	
	public static void fillKeyValueMap(Component component, Map<String,Object> keyValueMap) {
		String labelKey = CDesktopComponents.vView().getAttributeString(component, labelKeyId);
		if (!labelKey.equals("")) {
			boolean labelCapitalizeFirstChar = CDesktopComponents.vView().getAttributeString(component, "labelCapitalizeFirstChar").equals(AppConstants.statusValueTrue);
			if (labelCapitalizeFirstChar) {
				keyValueMap.put(cLabelKeyId, labelKey);
			}
			else {
				keyValueMap.put(labelKeyId, labelKey);
			}
			component.setAttribute("keyValueMap", keyValueMap);
		}
	}
	
	public static void decorateComponent(Component component, Map<String,Object> keyValueMap) {
		if (keyValueMap.containsKey("id")) {
			component.setId((String)keyValueMap.get("id"));
		}
		if (keyValueMap.containsKey("class")) {
			if (component instanceof HtmlBasedComponent) {
				((HtmlBasedComponent)component).setZclass((String)keyValueMap.get("class"));
			}
		}
		if (keyValueMap.containsKey("style")) {
			if (component instanceof HtmlBasedComponent) {
				((HtmlBasedComponent)component).setStyle((String)keyValueMap.get("style"));
			}
		}
		if (keyValueMap.containsKey("width")) {
			if (component instanceof HtmlBasedComponent) {
				((HtmlBasedComponent)component).setWidth((String)keyValueMap.get("width"));
			}
		}
		if (keyValueMap.containsKey("height")) {
			if (component instanceof HtmlBasedComponent) {
				((HtmlBasedComponent)component).setHeight((String)keyValueMap.get("height"));
			}
		}
		if (keyValueMap.containsKey("tooltiptext")) {
			if (component instanceof HtmlBasedComponent) {
				((HtmlBasedComponent)component).setTooltiptext((String)keyValueMap.get("tooltiptext"));
			}
		}
		if (keyValueMap.containsKey("text")) {
			if (component instanceof InputElement) {
				((InputElement)component).setText((String)keyValueMap.get("text"));
			}
		}
		if (keyValueMap.containsKey("visible")) {
			component.setVisible(!((String)keyValueMap.get("visible")).equals("false"));
		}
	}

	public static String[] getValueAndTooltipText(Map<String,Object> keyValueMap) {
		String value = "";
		String tooltiptext = "";
		CRunPVToolkit lPVTK = (CRunPVToolkit)CDesktopComponents.vView().getComponent(pvToolkitComponentId);
		if (keyValueMap.containsKey("value")) {
			value = (String)keyValueMap.get("value");
		}
		else if (keyValueMap.containsKey(labelKeyId)) {
			if (lPVTK != null)
				value = lPVTK.getRubricLabelText((String)keyValueMap.get(labelKeyId));
			else
				value = (String)keyValueMap.get(labelKeyId);
		}
		else if (keyValueMap.containsKey(cLabelKeyId)) {
			if (lPVTK != null)
				value = lPVTK.getRubricCLabelText((String)keyValueMap.get(cLabelKeyId));
			else
				value = (String)keyValueMap.get(cLabelKeyId);
		}
		else if (keyValueMap.containsKey("cacAndTag") && (keyValueMap.get("cacAndTag") instanceof CRunPVToolkitCacAndTag) && keyValueMap.containsKey("tagChildName")) {
			if (lPVTK != null)
				value = lPVTK.getTagChildValue((CRunPVToolkitCacAndTag)keyValueMap.get("cacAndTag"), (String)keyValueMap.get("tagChildName"));
			else
				value = (String)keyValueMap.get("tagChildName");
		}
		if (keyValueMap.containsKey("maxLabelLength")) {
			int maxLabelLength = (int)keyValueMap.get("maxLabelLength");
			if (value.length() > maxLabelLength) {
				tooltiptext = value;
				value = value.substring(0, maxLabelLength) + "...";
			}
		}
		return new String[]{value, tooltiptext};
	}

	public static String editContentOrLabel(Component component) {
		if (component == null) {
			return "";
		}
		Map<String,Object> keyValueMap = (Map<String,Object>)component.getAttribute("keyValueMap");
		if (keyValueMap == null) {
			return "";
		}
		//For content
		if (keyValueMap.containsKey("cacAndTag") && (keyValueMap.get("cacAndTag") instanceof CRunPVToolkitCacAndTag) && keyValueMap.containsKey("tagChildName")) {
			String value = ((CRunPVToolkit)CDesktopComponents.vView().getComponent(pvToolkitComponentId)).getTagChildValue((CRunPVToolkitCacAndTag)keyValueMap.get("cacAndTag"), (String)keyValueMap.get("tagChildName"));
			CDesktopComponents.vView().showMessagebox(null, value, "Bewerk Inhoud", Messagebox.OK, Messagebox.INFORMATION);
			//TODO show pop-up to edit content depending on type of child tag. It probably will be more or less a copy of the pop-up used within the authoring environment
			//NOTE store value in content, so in XML data
			((CRunPVToolkit)CDesktopComponents.vView().getComponent(pvToolkitComponentId)).setTagChildValue((CRunPVToolkitCacAndTag)keyValueMap.get("cacAndTag"), (String)keyValueMap.get("tagChildName"), value);
			return value;
		}
		//for labels
		else if (keyValueMap.containsKey(labelKeyId)) {
			//TODO enable personalizing of labels
			String value = ((CRunPVToolkit)CDesktopComponents.vView().getComponent(pvToolkitComponentId)).getRubricLabelText((String)keyValueMap.get(labelKeyId));
			CDesktopComponents.vView().showMessagebox(null, value, "Bewerk label", Messagebox.OK, Messagebox.INFORMATION);
			//TODO show pop-up to edit label value
			//TODO probably store value in run status status
			return value;
		}
		else if (keyValueMap.containsKey(cLabelKeyId)) {
			//TODO enable personalizing of labels
			String value = ((CRunPVToolkit)CDesktopComponents.vView().getComponent(pvToolkitComponentId)).getRubricCLabelText((String)keyValueMap.get(cLabelKeyId));
			CDesktopComponents.vView().showMessagebox(null, value, "Bewerk label", Messagebox.OK, Messagebox.INFORMATION);
			//TODO show pop-up to edit label value
			//TODO probably store value in run status status
			return value;
		}
		return "";
	}
	
}