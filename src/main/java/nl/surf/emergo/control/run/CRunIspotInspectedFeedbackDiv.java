/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.Date;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;

import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.def.CDefHelper;

public class CRunIspotInspectedFeedbackDiv extends CRunIspotInterventionDiv {

	private static final long serialVersionUID = 7590745474569523767L;

	public void onCreate(CreateEvent aEvent) {
		super.onCreate(aEvent);
		interventionType = runIspot.interventionTypeInspectedFeedback;
	}

	public void onUpdate(Event aEvent) {
		setAttribute("dataElement", aEvent.getData());
		Events.sendEvent("onIspotInit", this, null);
		//default show instruction
		Events.sendEvent("onIspotShowInstruction", this, null);
	}

	public void onIspotInit(Event aEvent) {
		Events.sendEvent("onIspotInitHeader", this, null);

		CRunIspotInspectedFeedback dataElement = (CRunIspotInspectedFeedback)getAttribute("dataElement");
		setAttribute("previous", dataElement.getPrevious());
		setAttribute("next", dataElement.getNext());
		setAttribute("showInstruction", !dataElement.getVignetteInstruction().equals(""));
		setAttribute("showVignette", !dataElement.getVignetteUrl().equals(""));
		setAttribute("showFeedback", dataElement.getFeedbackTexts().size() > 0 || dataElement.getFeedbackFragments().size() > 0);
		setAttribute("showReaction", true);
		setAttribute("showComment", true);

		runIspot.setComponentVisible(vView.getComponent("ispotPreviousButton" + interventionType), getAttribute("previous") != null);
		runIspot.setComponentVisible(vView.getComponent("ispotNextButton" + interventionType), getAttribute("next") != null);
		Events.sendEvent("onIspotHideViewers", vView.getComponent("ispotViewers"), null);
		Events.sendEvent("onIspotShowButtons", this, null);
		
		Events.postEvent("onUpdate", vView.getComponent("ispotFeedbackBtnsViewer"), dataElement);
	}

	public void onIspotInitHeader(Event aEvent) {
		CRunIspotInspectedFeedback dataElement = (CRunIspotInspectedFeedback)getAttribute("dataElement");
		((Image)vView.getComponent("ispotInspectedFeedbackState")).setSrc(runIspot.stylePath + "ispot-"+ dataElement.getInspectedFeedbackState() + ".png");
		((Image)vView.getComponent("ispotInspectedFeedbackState")).setTooltiptext(vView.getLabel("run_ispot.listcell.inspected_feedback." + dataElement.getInspectedFeedbackState()));
		((Label)vView.getComponent("ispotInspectedFeedbackTitle")).setValue(
				(runIspot.canCurrentRgaSeeFeedbackOnOthers ? dataElement.getReactionRga().getERunGroup().getName() + ": " : "") +
				dataElement.getSubjectName() + ": " +
				dataElement.getVignetteName());
		((Label)vView.getComponent("ispotInspectedFeedbackGiverName")).setValue(
				vView.getLabel("run_ispot.header.feedback_given_rga_name") + ": " +
				dataElement.getGivenFeedbackRga().getERunGroup().getName());
		
	}

	
	public void onIspotPreviousInspectedFeedback(Event aEvent) {
		Events.postEvent("onPrevious", vView.getComponent("ispotInspectedFeedbacksListbox"), null);
	}

	public void onIspotNextInspectedFeedback(Event aEvent) {
		Events.postEvent("onNext", vView.getComponent("ispotInspectedFeedbacksListbox"), null);
	}

	
	public void onIspotShowFeedback(Event aEvent) {
		Events.sendEvent("onIspotShowFeedbackButtons", this, null);
		Events.sendEvent("onIspotHideButton", this, vView.getComponent("ispotShowFeedbackButton" + interventionType));
	}

	public void onIspotShowReaction(Event aEvent) {
		Events.sendEvent("onIspotShowPlayer", this, ((CRunIspotInspectedFeedback)getAttribute("dataElement")).getReactionUrl());
		Events.sendEvent("onIspotHideButton", this, vView.getComponent("ispotShowReactionButton" + interventionType));
	}

	public void onIspotShowComment(Event aEvent) {
		Events.sendEvent("onIspotShowPlayer", vView.getComponent("ispotViewers"), ((CRunIspotInspectedFeedback)getAttribute("dataElement")).getGivenFeedbackUrl());
		Events.sendEvent("onIspotHideButton", this, vView.getComponent("ispotShowCommentButton" + interventionType));
	}

	public void onIspotStreamCompleted(Event aEvent) {
		if (vView.getComponent("ispotInspectedFeedback").isVisible() && !vView.getComponent("ispotShowCommentButton" + interventionType).isVisible()) {
			//NOTE if inspected feedback div is visible and the show comment button not, the last user action must have been a click on
			//the show comment button, so the comment stream is completed.
			CRunIspotInspectedFeedback dataElement = (CRunIspotInspectedFeedback)getAttribute("dataElement");
			if (!dataElement.getInspectedFeedbackState().equals("done")) {
				dataElement.setInspectedFeedbackState("done");
				dataElement.setInspectedFeedbackDoneDate(CDefHelper.getDateStrAsYMD(new Date()));
				//set finished state of dataElement
				runIspot.setInspectedFeedbackStatus(dataElement, AppConstants.statusKeyFinished, AppConstants.statusValueTrue);
				runIspot.setInspectedFeedbackStatus(dataElement, "finisheddate", dataElement.getInspectedFeedbackDoneDate());
				Events.postEvent("onUpdateState", vView.getComponent("ispotInspectedFeedbacksListbox"), dataElement);
				Events.sendEvent("onIspotInitHeader", this, null);
			}
		}
	}


	@Override
	public void onIspotShowButtons(Event aEvent) {
		runIspot.setComponentVisible(vView.getComponent("ispotShowInstructionButton" + interventionType), (Boolean)getAttribute("showInstruction"));
		runIspot.setComponentVisible(vView.getComponent("ispotShowVignetteButton" + interventionType), (Boolean)getAttribute("showVignette"));
		runIspot.setComponentVisible(vView.getComponent("ispotShowFeedbackButton" + interventionType), (Boolean)getAttribute("showFeedback"));
		runIspot.setComponentVisible(vView.getComponent("ispotShowReactionButton" + interventionType), (Boolean)getAttribute("showReaction"));
		runIspot.setComponentVisible(vView.getComponent("ispotShowCommentButton" + interventionType), (Boolean)getAttribute("showComment"));
	}

}
