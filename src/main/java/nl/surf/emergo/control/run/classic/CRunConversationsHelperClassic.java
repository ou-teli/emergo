/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.Treerow;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CTree;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunConversationsHelper. Helps rendering conversation questions tree.
 */
public class CRunConversationsHelperClassic extends CRunComponentHelperClassic {

	/** The run wnd. */
	CRunWndClassic runWnd = (CRunWndClassic)CDesktopComponents.vView().getComponent(CControl.runWnd);
	
	/**
	 * Instantiates a new c run conversations helper.
	 * 
	 * @param aTree the ZK conversations tree
	 * @param aShowTagNames the a show tag names, comma separated, only tags with these names are shown
	 * @param aCaseComponent the conversations case component
	 * @param aRunComponent the a run component, the ZK conversations component
	 */
	public CRunConversationsHelperClassic(CTree aTree, String aShowTagNames, IECaseComponent aCaseComponent, CRunComponentClassic aRunComponent) {
		super(aTree, aShowTagNames, aCaseComponent, aRunComponent);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTreeHelper#renderTreecell(org.zkoss.zul.Treeitem, org.zkoss.zul.Treerow, nl.surf.emergo.business.IXMLTag, java.lang.String, boolean, java.lang.String)
	 */
	protected Treecell renderTreecell(Treeitem aTreeitem, Treerow aTreerow,
			IXMLTag aItem, String aKeyValues, boolean aAccIsAuthor) {
		Treecell lTreecell = getTreerowTreecell(aTreerow, 0);
		if (aKeyValues.length() > 80) {
			lTreecell.setTooltiptext(aKeyValues);
			aKeyValues = aKeyValues.substring(0, 80) + "...";
		}
		lTreecell.setLabel(aKeyValues);
		String lStatus = "active";
		if (isOpened(aItem))
			lStatus = "opened";
		if (!isAccessible(aItem))
			lStatus = "inactive";
		lTreecell.setZclass("CRunConversationInteraction_treecell_"+lStatus);
		lTreecell.setStyle(CDesktopComponents.vView().getLabel("CRunComponent_treecell_style"));
		// showStatus(lTreecell,aItem);
		return lTreecell;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunContentHelper#xmlContentToContentItems(nl.surf.emergo.business.IXMLTag, org.zkoss.zk.ui.Component, java.lang.String)
	 */
	@Override
	public void xmlContentToContentItems(IXMLTag aXmlTree, Component aComponent) {
		xmlChildsToContentItems(aXmlTree.getChildTags(AppConstants.defValueNode), aComponent, null);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunComponentHelper#xmlChildsToContentItems(java.util.List, org.zkoss.zk.ui.Component, org.zkoss.zul.Treeitem, java.lang.String)
	 */
	@Override
	public List<Component> xmlChildsToContentItems(List<IXMLTag> aChildTags, Component aParent,
			Component aInsertBefore) {
		if ((aChildTags == null) || (aChildTags.size() == 0))
			return null;
		List<Component> lContentItems = new ArrayList<Component>();
		for (IXMLTag xmltag : aChildTags) {
			IXMLTag xmlchild = xmltag.getChild(AppConstants.statusElement);
			boolean lOk = (xmlchild != null);
			String lName = xmltag.getName();
			lOk = (lOk && ((lName.equals("map")) || (lName.equals("question")) || (lName.equals("fragment"))));
			if (lOk) {
				if (lName.equals("fragment"))
					xmlChildsToContentItems(xmltag.getChildTags(AppConstants.defValueNode), aParent,
							null);
				else {
					boolean lHidden = ((CDesktopComponents.sSpring().getCurrentTagStatus(xmltag,
							AppConstants.statusKeyPresent))
							.equals(AppConstants.statusValueFalse));
					if (!lHidden) {
						Component lContentItem = renderTreeitem(aParent, null,
								xmltag);
						if (lContentItem != null) {
							lContentItems.add(lContentItem);
							boolean lOpened = ((CDesktopComponents.sSpring().getCurrentTagStatus(
									xmltag, AppConstants.statusKeyOutfolded))
									.equals(AppConstants.statusValueTrue));
							if (!lOpened)
								((Treeitem)lContentItem).setOpen(false);
							boolean lChildrenHidden = ((CDesktopComponents.sSpring()
									.getCurrentTagStatus(xmltag,
											AppConstants.statusKeyOutfoldable))
											.equals(AppConstants.statusValueFalse));
							if (!lChildrenHidden)
								xmlChildsToContentItems(xmltag.getChildTags(AppConstants.defValueNode),
										lContentItem, null);
//							if ((runWnd != null) && (((Treeitem)lContentItem).getTreerow() != null))
//								((Treeitem)lContentItem).getTreerow().setAction(runWnd.getMediaplayerStopAction());
							if ((runWnd != null) && (((Treeitem)lContentItem).getTreerow() != null))
								((Treeitem)lContentItem).getTreerow().setWidgetListener("onClick", runWnd.getMediaplayerStopAction());
						}
					}
				}
			}
		}
		return lContentItems;
	}
}
