/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.IP2;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefBox;

public class CRunSatisfactionBox extends CDefBox {

	private static final long serialVersionUID = -4476647956947506641L;

	protected CRunSatisfactionDiv satisfactionDiv = null;

	public void onCreate(CreateEvent aEvent) {
		satisfactionDiv = (CRunSatisfactionDiv)CDesktopComponents.vView().getComponent("satisfactionDiv");
		if (satisfactionDiv == null) {
			return;
		}
		String[] satisfactionTypes = new String[]{"nice", "difficult"};
		String[] satisfactionQuestions = new String[]{"Hoe leuk vond u de opdracht?", "Hoe moeilijk vond u de opdracht?"}; 
		String[] satisfactionLabelLeft = new String[]{"Helemaal niet leuk", "Helemaal niet moeilijk"}; 
		String[] satisfactionLabelRight = new String[]{"Heel erg leuk", "Heel erg moeilijk"}; 
		int[] satisfaction = satisfactionDiv.getSatisfaction();
		for (int i=0;i<satisfactionTypes.length;i++) {
			Component component = (Component)getChildren().get(0).clone();
			insertBefore(component, null);
			Events.postEvent("onInit", component, new Object[]{satisfactionTypes[i], satisfactionQuestions[i], satisfactionLabelLeft[i], satisfactionLabelRight[i], satisfaction[i]});
		}
	}

}
