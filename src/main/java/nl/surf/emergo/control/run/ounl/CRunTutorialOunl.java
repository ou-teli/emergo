/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl;

import org.zkoss.zk.ui.Component;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.run.CRunArea;
import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.control.run.CRunConversationInteraction;
import nl.surf.emergo.control.run.CRunHbox;
import nl.surf.emergo.control.run.CRunTutorial;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunTutorialOunl is used to show tutorial component within the run view area of the emergo player.
 */
public class CRunTutorialOunl extends CRunTutorial {

	private static final long serialVersionUID = -858811953775018263L;

	/**
	 * Instantiates a new c run tutorial.
	 */
	public CRunTutorialOunl() {
		super();
	}

	/**
	 * Instantiates a new c run tutorial.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the tutorial case component
	 * @param aChapterTagId the chapter tag id
	 */
	public CRunTutorialOunl(String aId, IECaseComponent aCaseComponent, String aChapterTagId) {
		super(aId, aCaseComponent, aChapterTagId);
	}

	/**
	 * Creates title area and shows name of case component within it.
	 * And adds close button
	 *
	 * @param aParent the ZK parent
	 *
	 * @return the c run area
	 */
	@Override
	protected CRunArea createTitleArea(Component aParent) {
		return null;
	}

	/**
	 * Creates buttons area and adds close button.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run hbox
	 */
	@Override
	protected CRunHbox createButtonsArea(Component aParent) {
		// NOTE Don't create buttons area
		return null;
	}

	/**
	 * Get new run conversation interaction.
	 *
	 * @return the new run conversation interaction
	 */
	@Override
	public CRunConversationInteraction getNewRunConversationInteraction() {
		return new CRunTutorialConversationInteractionOunl("runConversationInteraction", null, "runConversations");
	}

	/**
	 * Get new run conversations.
	 *
	 * @param aCaseComponent the a case component
	 * @param aXmlTag the a xml tag
	 * @param aRunConversationInteraction the a run conversation interaction
	 *
	 * @return the new run conversations
	 */
	@Override
	public CRunComponent getNewRunConversations(IECaseComponent aCaseComponent, IXMLTag aXmlTag, CRunConversationInteraction aRunConversationInteraction) {
		return new CRunTutorialConversationsOunl("runConversations", aCaseComponent, aXmlTag, aRunConversationInteraction);
	}

	/**
	 * Get new run references.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return the new run references
	 */
	@Override
	public CRunComponent getNewRunReferences(IECaseComponent aCaseComponent) {
		return new CRunTutorialReferencesOunl("runReferences", aCaseComponent);
	}

}
