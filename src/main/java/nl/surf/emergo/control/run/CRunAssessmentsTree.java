/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Treecol;
import org.zkoss.zul.Treecols;
import org.zkoss.zul.Treeitem;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefTreecol;
import nl.surf.emergo.control.def.CDefTreecols;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunAssessmentsTree.
 */
public class CRunAssessmentsTree extends CRunTree {

	private static final long serialVersionUID = 4524619733291610676L;

	/**
	 * Instantiates a new c run assessments tree.
	 * 
	 * @param aId the a id
	 * @param aCaseComponent the assessments case component
	 * @param aRunComponent the a run component, the ZK assessments component
	 */
	public CRunAssessmentsTree(String aId, IECaseComponent aCaseComponent, CRunComponent aRunComponent) {
		super(aId, aCaseComponent, aRunComponent);
		tagopenednames = "refitem";
		setRows(20);
		update();
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTree#appendTreecols(org.zkoss.zk.ui.Component)
	 */
	@Override
	protected void appendTreecols(Component aParent) {
		Treecols lTreecols = new CDefTreecols();
		Treecol lTreecol = new CDefTreecol();
		lTreecols.appendChild(lTreecol);
		lTreecol = new CDefTreecol();
		lTreecols.appendChild(lTreecol);
		aParent.appendChild(lTreecols);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTree#getRunComponentHelper()
	 */
	@Override
	public CRunComponentHelper getRunComponentHelper() {
		return new CRunAssessmentsHelper(this, tagopenednames, caseComponent, runComponent);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTree#doTreeitemAction(org.zkoss.zul.Treeitem)
	 */
	@Override
	public void doTreeitemAction(Treeitem aTreeitem) {
//		if item clicked notify runAssessments
		CRunAssessments lComp = (CRunAssessments) CDesktopComponents.vView().getComponent("runAssessments");
		if (lComp != null) {
			lComp.doContentItemAction(aTreeitem);
		}
	}
}
