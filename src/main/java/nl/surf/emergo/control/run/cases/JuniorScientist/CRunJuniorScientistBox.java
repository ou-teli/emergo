/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.JuniorScientist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.control.script.CScript;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpring;

/**
 * This class is the ancestor of all Junior Scientist classes that are responsible for handling macros defined within the navigation component of the Junior Scientist case.
 * Every one of these classes is used within the ZUL file that is entered in the corresponding macro tag within the navigation component.
 */
public class CRunJuniorScientistBox extends CRunComponent {

	private static final long serialVersionUID = 4805334229097631014L;

	protected VView vView = CDesktopComponents.vView();
	protected CControl cControl = CDesktopComponents.cControl();
	protected SSpring sSpring = CDesktopComponents.sSpring();
	protected CScript cScript = CDesktopComponents.cScript();
	
	/** These case components may be used in several macros. */
	protected final static String statesCaseComponentName = "game_states";
	protected final static String navigationCaseComponentName = "game_navigation";
	protected IECaseComponent statesCaseComponent;
	protected IECaseComponent navigationCaseComponent;
	
	/** The conversations, tasks and notifications macro tags are defined within the root of the navigation component, because they may be present on multiple locations and/or used/changed by other location specific macros. */
	protected final static String conversationsMacroTagPid = "conversations_macro";
	protected final static String tasksMacroTagPid = "tasks_macro";
	protected final static String notificationsMacroTagPid = "notifications_macro";

	/** The state of the conversations macro is stored in three states tag defined within the states component. Conversations may be used within other macros, i.e., to give feedback. */
	protected final static String conversationsCaseComponentNameStateKeyPid = "conversations_name"; 
	protected final static String conversationTagPidStateKeyPid = "conversation_pid";
	/** Fragment pid may be used to overwrite the default fragment. If not empty is used and set to empty again. */
	protected final static String fragmentTagPidStateKeyPid = "fragment_pid";

	/** Keys used by passage and clickable object macros. */
	protected final static String paramKeyExtraMacroClass = "extra-macro-class";
	protected final static String paramKeyIconClass = "icon-class";
	protected final static String paramKeyPlaySoundOnClick = "playSoundOnClick";
	protected final static String paramKeySound = "sound";
	protected final static String soundOnClickStateKey = "sound_onclick";

	/** The state of the notifications macro is stored in two states tag defined within the states component. Notifications may be used within other macros. */
	protected final static String notificationsCaseComponentNameStateKeyPid = "notifications_name"; 
	protected final static String notificationTagPidStateKeyPid = "notification_pid";

	/** The state of the employee card macro is stored in three states tag defined within the states component. Employee card states may be used within other macros. */
	protected static final String employeeCardOpenedStateKey = "employee_card_opened";
	protected static final String employeeCardNameStateKey = "employee_card_name";
	protected static final String employeeCardImageBlobIdStateKey = "employee_card_image_blobid";
	
	/** The default employee card name */
	protected static final String defaultEmployeeCardName = "Junior Scientist";

	/** If the value of this state is true, answer buttons in phase 3, 4 and 5 will be shown randomized. This is handled in the zul files. */
	protected static final String randomizeAnswersStateKey = "randomize_answers";

	/** Pid may contain parameters starting after paramStart. */
	protected final static String paramsStart = "$";
	protected final static String paramSeparator = "&";

	/** An event queue to notify other macros in case of certain events. */
	public EventQueue<Event> eventQueueJuniorScientistEvents = EventQueues.lookup("JuniorScientistEvents", EventQueues.DESKTOP, true);
	public static final String onMacroEvent = "onMacroEvent";

	/** The current case component. */
	IECaseComponent currentCaseComponent;
	/** The current tag. */
	protected IXMLTag currentTag;
	/** The current macro gathers data and puts it in a property map that is used by a child macro, with its own ZUL file, to render the data. */
	protected Map<String,Object> propertyMap = new HashMap<String,Object>();
	/** The macro. */
	protected HtmlMacroComponent macro;
	/** The child macro. */
	protected HtmlMacroComponent childMacro;
	/** The path to the ZUL file of the current macro. It is used by the child macro to be able to reference to other files like assets or style sheets. */
	protected String zulfilepath = "";
	
	/** If true the macro is shown statically, without interaction possibility. */
	protected boolean showstatic = false;

	/** The relative sub path to the Junior Scientist asset files. It is relative to zulFilepath. */
	protected final static String assetSubPath = "assets/";
	/** The relative sub path to the Junior Scientist sound files. It is relative to zulFilepath. */
	protected final static String soundsSubPath = assetSubPath + "html/sound/";
	
	public IECaseComponent getStatesCaseComponent() {
		if (statesCaseComponent == null) {
			statesCaseComponent = sSpring.getCaseComponent("", statesCaseComponentName);
		}
		return statesCaseComponent;
	}

	public IECaseComponent getNavigationCaseComponent() {
		if (navigationCaseComponent == null) {
			navigationCaseComponent = sSpring.getCaseComponent("", navigationCaseComponentName);
		}
		return navigationCaseComponent;
	}

	public String getZulfilepath() {
		return zulfilepath;
	}

	public void setZulfilepath(String zulfilepath) {
		this.zulfilepath = zulfilepath;
	}

	public boolean isShowstatic() {
		return showstatic;
	}

	public void setShowstatic(boolean showstatic) {
		this.showstatic = showstatic;
	}

	public void onCreate(CreateEvent aEvent) {
		super.onCreate(aEvent);
		
		initRunWnd();
		
		//subscribe to event queue to be able to get notifications by macros
		eventQueueJuniorScientistEvents.subscribe(new EventListener<Event>() {
			public void onEvent(Event event) {
				handleEvent(event);
			}
		});

		//NOTE use echoEvent, otherwise macro parent does not yet exist, if code is used within zscript
	  	Events.echoEvent("onInit", this, null);
	}
	
	protected void initRunWnd() {
		//NOTE default layout of runWnd has to be adjusted for Junior Scientist
		if (Executions.getCurrent() == null) {
			return;
		}
		if (Executions.getCurrent().getDesktop().getAttribute("JSrunWndInitialized") != null) {
			//NOTE this class is ancestor of many Junior Scientist classes, meaning onCreate method above is called many times. Initialize runWnd only once.
			return;
		}
		HtmlBasedComponent runWnd = (HtmlBasedComponent)vView.getComponent(CControl.runWnd);
		String style = runWnd.getStyle() == null ? "" : runWnd.getStyle();
		style += ";background-color:white;overflow:hidden;border-radius:0.5rem;"; 
		runWnd.setStyle(style);
		vView.getComponent("runWndCorners").setVisible(false);
		
		Executions.getCurrent().getDesktop().setAttribute("JSrunWndInitialized", "true");
	}
	
	protected void handleEvent(Event event) {
		//NOTE may be overwritten by descendant if it needs to use events of other macros
	}
	
	/** Init macro. */
	public void onInit() {
		onInitMacro();
	}

	/** On init macro. */
	protected void onInitMacro() {
		macro = (HtmlMacroComponent)getMacroParent(this);
		propertyMap.put("macro", macro);
		
		//NOTE normally current case component is the navigation component and current tag is a macro tag.
		//However if the macro zul file is used within the junior scientist references component, the case component is a references component and the tag is a piece tag
		currentCaseComponent = (IECaseComponent)macro.getDynamicProperty("a_casecomponent");
		currentTag = (IXMLTag)macro.getDynamicProperty("a_tag");

		propertyMap.put("casecomponent", currentCaseComponent);
		propertyMap.put("tag", currentTag);
		zulfilepath = (String)macro.getDynamicProperty("a_zulfilepath");
		propertyMap.put("zulfilepath", zulfilepath);

		showstatic = macro.getDynamicProperty("a_showstatic") == null ? false : (boolean)macro.getDynamicProperty("a_showstatic");
		propertyMap.put("showstatic", showstatic);
		
		propertyMap.put("macroBoxUuid", getUuid());
		
		propertyMap.put("randomizeAnswers", getStateTagValue(randomizeAnswersStateKey).equals(AppConstants.statusValueTrue));
	}

	protected void addChildMacro(String childMacroZulfile) {
		childMacro = new HtmlMacroComponent();
		appendChild(childMacro);
		childMacro.setDynamicProperty("a_propertyMap", propertyMap);
		childMacro.setMacroURI(zulfilepath + childMacroZulfile);
	}
	
	
	/** Update macro. */
	public void onUpdate() {
		//NOTE may be overwritten by descendant if macro has to be updated, e.g., due to user action
	}
/*
	protected Component getMacroParent(Component component) {
		while (component != null && !(component instanceof HtmlMacroComponent)) {
			component = component.getParent();
		}
		return component;
	}
*/	
	protected IXMLTag getMacroTag(String macroTagPid) {
		//NOTE macro tags are located within navigation component
		List<IXMLTag> nodeTags = cScript.getRunGroupNodeTags(getNavigationCaseComponent());
		for (IXMLTag nodeTag : nodeTags) {
			if (sSpring.unescapeXML(nodeTag.getChildValue("pid")).equals(macroTagPid)) {
				return nodeTag;
			}
		}
		return null;
	}
	
	protected IXMLTag getStateTag(String key) {
		//NOTE state tags are located within states component
		List<IXMLTag> nodeTags = cScript.getRunGroupNodeTags(getStatesCaseComponent());
		for (IXMLTag nodeTag : nodeTags) {
			if (sSpring.unescapeXML(nodeTag.getChildValue("key")).equals(key)) {
				return nodeTag;
			}
		}
		return null;
	}
	
	protected String getStateTagValue(String key) {
		IXMLTag stateTag = getStateTag(key);
		if (stateTag == null) {
			return "";
		}
		else {
			return stateTag.getCurrentStatusAttribute(AppConstants.statusKeyValue);
		}
	}
	
	protected void setStateTagValue(String key, String value) {
		IXMLTag stateTag = getStateTag(key);
		if (stateTag == null) {
			return;
		}
		setRunTagStatusJS(getStatesCaseComponent(), stateTag, AppConstants.statusKeyValue, value, false);
	}

	/** Used to get game state for certain phase. */
	protected int getGameStateValue(int phase) {
		int value = 0;
		String stateTagValue = getStateTagValue("game_" + phase + "_state");
		try {
			value = Integer.parseInt(stateTagValue);
		} catch (NumberFormatException e) {
		}
		return value;
	}


	/** Used to set game state for certain phase. */
	protected void setGameStateValue(int phase, int value) {
		IXMLTag gameStateTag = getStateTag("game_" + phase + "_state");
		setRunTagStatusJS(getStatesCaseComponent(), gameStateTag, AppConstants.statusKeyValue, "" + value, false);
	}


	/** Wrappers for SSpring methods. */
	protected void setRunComponentStatusJS(IECaseComponent caseComponent, String statusKey, String statusValue) {
		sSpring.setRunComponentStatus(caseComponent, statusKey, statusValue, true, AppConstants.statusTypeRunGroup, true);
	}

	protected IXMLTag setRunTagStatusJS(IECaseComponent caseComponent, IXMLTag tag, String statusKey, String statusValue, boolean fromScript) {
		return sSpring.setRunTagStatus(caseComponent, tag, statusKey, statusValue, null, true, AppConstants.statusTypeRunGroup, true, fromScript, true);
	}


	protected IXMLTag getTagByPid(IECaseComponent caseComponent, String tagPid) {
		for (IXMLTag nodeTag : cScript.getRunGroupNodeTags(caseComponent)) {
			String pid = sSpring.unescapeXML(nodeTag.getChildValue("pid"));
			if (pid.equals(tagPid)) {
				return nodeTag;
			}
		}
		return null;
	}


	/** Because conversations can be used by other macros, methods below are available to alle macros. */
	protected IXMLTag getConversationsMacroTag() {
		return getMacroTag(conversationsMacroTagPid);
	}
	
	protected IXMLTag getTasksMacroTag() {
		return getMacroTag(tasksMacroTagPid);
	}
	
	protected IXMLTag getNotificationsMacroTag() {
		return getMacroTag(notificationsMacroTagPid);
	}
	
	protected IXMLTag getConversationsComponentNameStateTag() {
		return getStateTag(conversationsCaseComponentNameStateKeyPid);
	}
	
	protected IXMLTag getConversationTagPidStateTag() {
		return getStateTag(conversationTagPidStateKeyPid);
	}
	
	protected IXMLTag getFragmentTagPidStateTag() {
		return getStateTag(fragmentTagPidStateKeyPid);
	}
	
	protected IXMLTag getNotificationsComponentNameStateTag() {
		return getStateTag(notificationsCaseComponentNameStateKeyPid);
	}
	
	protected IXMLTag getNotificationTagPidStateTag() {
		return getStateTag(notificationTagPidStateKeyPid);
	}
	
	protected String getEmployeeCardUrl() {
		String employeeCardImageBlobId = getStateTagValue(employeeCardImageBlobIdStateKey);
		if (!employeeCardImageBlobId.equals("")) {
			return sSpring.getSBlobHelper().getUrl(employeeCardImageBlobId, AppConstants.blobtypeDatabase);
		}
		return "";
	}

	protected String getConversationsComponentName() {
		IXMLTag stateTag = getConversationsComponentNameStateTag();
		if (stateTag != null) {
			return stateTag.getCurrentStatusAttribute(AppConstants.statusKeyValue);
		}
		return "";
	}
	
	protected String getConversationTagPid() {
		IXMLTag stateTag = getConversationTagPidStateTag();
		if (stateTag != null) {
			return stateTag.getCurrentStatusAttribute(AppConstants.statusKeyValue);
		}
		return "";
	}
	
	protected String getFragmentTagPid() {
		IXMLTag stateTag = getFragmentTagPidStateTag();
		if (stateTag != null) {
			return stateTag.getCurrentStatusAttribute(AppConstants.statusKeyValue);
		}
		return "";
	}
	
	protected String getNotificationsComponentName() {
		IXMLTag stateTag = getNotificationsComponentNameStateTag();
		if (stateTag != null) {
			return stateTag.getCurrentStatusAttribute(AppConstants.statusKeyValue);
		}
		return "";
	}
	
	protected String getNotificationTagPid() {
		IXMLTag stateTag = getNotificationTagPidStateTag();
		if (stateTag != null) {
			return stateTag.getCurrentStatusAttribute(AppConstants.statusKeyValue);
		}
		return "";
	}
	
	protected void setConversationsComponentName(String value) {
		IXMLTag stateTag = getConversationsComponentNameStateTag();
		if (stateTag != null) {
			setRunTagStatusJS(getStatesCaseComponent(), stateTag, AppConstants.statusKeyValue, value, false);
		}
	}
	
	protected void setConversationTagPid(String value) {
		IXMLTag stateTag = getConversationTagPidStateTag();
		if (stateTag != null) {
			setRunTagStatusJS(getStatesCaseComponent(), stateTag, AppConstants.statusKeyValue, value, false);
		}
	}
	
	protected void setFragmentTagPid(String value) {
		IXMLTag stateTag = getFragmentTagPidStateTag();
		if (stateTag != null) {
			setRunTagStatusJS(getStatesCaseComponent(), stateTag, AppConstants.statusKeyValue, value, false);
		}
	}
	
	protected void setNotificationsComponentName(String value) {
		IXMLTag stateTag = getNotificationsComponentNameStateTag();
		if (stateTag != null) {
			setRunTagStatusJS(getStatesCaseComponent(), stateTag, AppConstants.statusKeyValue, value, false);
		}
	}
	
	protected void setNotificationTagPid(String value) {
		IXMLTag stateTag = getNotificationTagPidStateTag();
		if (stateTag != null) {
			setRunTagStatusJS(getStatesCaseComponent(), stateTag, AppConstants.statusKeyValue, value, false);
		}
	}
	

	protected void showConversation(String conversationsName, String conversationPid, String fragmentPid) {
		hideConversation();
		setConversationsComponentName(conversationsName);
		setConversationTagPid(conversationPid);
		setFragmentTagPid(fragmentPid);
		//update conversations macro component, in case the macro already exists
		setRunTagStatusJS(getNavigationCaseComponent(), getConversationsMacroTag(), AppConstants.statusKeyUpdate, AppConstants.statusValueTrue, false);
		//show conversations macro component
		setRunTagStatusJS(getNavigationCaseComponent(), getConversationsMacroTag(), AppConstants.statusKeyPresent, AppConstants.statusValueTrue, false);
	}

	protected void showConversation(String conversationName, String conversationPid) {
		showConversation(conversationName, conversationPid, "");
	}

	protected void hideConversation() {
		//hide conversations macro component
		setRunTagStatusJS(getNavigationCaseComponent(), getConversationsMacroTag(), AppConstants.statusKeyPresent, AppConstants.statusValueFalse, false);
	}

	protected void showNotification(String notificationsName, String notificationPid) {
		setNotificationsComponentName(notificationsName);
		setNotificationTagPid(notificationPid);
		//update notifications macro component, in case the macro already exists
		setRunTagStatusJS(getNavigationCaseComponent(), getNotificationsMacroTag(), AppConstants.statusKeyUpdate, AppConstants.statusValueTrue, false);
		//show notifications macro component
		setRunTagStatusJS(getNavigationCaseComponent(), getNotificationsMacroTag(), AppConstants.statusKeyPresent, AppConstants.statusValueTrue, false);
	}


	protected void toLocation(String locationPid) {
		//get location tag
		IXMLTag locationTag = getTagByPid(getNavigationCaseComponent(), locationPid);
		if (locationTag != null) {
			//NOTE last parameter fromScript is false, otherwise selected results in beaming to location as well. Only opened should trigger beaming.
			setRunTagStatusJS(getNavigationCaseComponent(), locationTag, AppConstants.statusKeySelected, AppConstants.statusValueTrue, false);
			//set location tag opened true (is beaming to location)
			//NOTE second last parameter is true, which indicates that from script is true. If false beaming is not functioning
			setRunTagStatusJS(getNavigationCaseComponent(), locationTag, AppConstants.statusKeyOpened, AppConstants.statusValueTrue, true);
		}
	}
	
	/** Methods used by passage and clickable object macros. */
	protected String getExtraMacroClass(IXMLTag macroTag) {
		return getParamValue(macroTag, paramKeyExtraMacroClass);
	}
	
	protected String getIconClass(IXMLTag macroTag) {
		return getParamValue(macroTag, paramKeyIconClass);
	}
	
	protected boolean getPlaySoundOnClick(IXMLTag macroTag) {
		return getParamValue(macroTag, paramKeyPlaySoundOnClick).equals(AppConstants.statusValueTrue);
	}
	
	protected String getSound(IXMLTag macroTag) {
		String sound = getStateTagValue(soundOnClickStateKey);
		if (sound.equals("")) {
			sound = getParamValue(macroTag, paramKeySound);
		}
		return sound;
	}

	protected void possiblyPlaySound() {
		if (getPlaySoundOnClick(currentTag)) {
			String sound = getSound(currentTag);
			if (!sound.equals("")) {
				sound = getZulfilepath() + soundsSubPath + sound;
				Clients.evalJavaScript("playAudio('" + sound + "');");
			}
		}
	}
	
	
	protected void showMacro(IXMLTag macroTag) {
		if (macroTag != null) {
			setRunTagStatusJS(getNavigationCaseComponent(), macroTag, AppConstants.statusKeyPresent, AppConstants.statusValueTrue, true);
		}
	}
	
	protected void showMacro(String macroPid) {
		showMacro(getTagByPid(getNavigationCaseComponent(), macroPid));
	}
	
	protected void hideMacro(IXMLTag macroTag) {
		if (macroTag != null) {
			setRunTagStatusJS(getNavigationCaseComponent(), macroTag, AppConstants.statusKeyPresent, AppConstants.statusValueFalse, true);
		}
	}
	
	protected void hideMacro(String macroPid) {
		hideMacro(getTagByPid(getNavigationCaseComponent(), macroPid));
	}
	


	protected List<String[]> getParamKeyValues(IXMLTag tag) {
		List<String[]> paramKeyValues = new ArrayList<String[]>();
		if (tag == null) {
			return paramKeyValues;
		}
		String paramsStr = sSpring.unescapeXML(tag.getChildValue("explanation"));
		if (paramsStr.length() == 0) {
			return paramKeyValues;
		}

		String[] paramStrs = paramsStr.split(paramSeparator);
		for (int i=0;i<paramStrs.length;i++) {
			String paramStr = paramStrs[i];
			if (paramStr.length() > 0) {
				String[] paramKeyValue = paramStr.split("=");
				if (paramKeyValue.length == 2) {
					paramKeyValues.add(new String[]{paramKeyValue[0], paramKeyValue[1]});
				}
			}
		}
		return paramKeyValues;
	}

	protected String getParamValue(IXMLTag tag, String key) {
		List<String[]> paramKeyValues = getParamKeyValues(tag);
		for (String[] param : paramKeyValues) {
			if (param[0].equals(key)) {
				return param[1];
			}
		}
		return "";
	}
	
	protected List<String> getParamValues(IXMLTag tag, String key) {
		List<String[]> paramKeyValues = getParamKeyValues(tag);
		List<String> paramValues = new ArrayList<String>();
		for (String[] param : paramKeyValues) {
			if (param[0].equals(key)) {
				paramValues.add(param[1]);
			}
		}
		return paramValues;
	}
	
}
