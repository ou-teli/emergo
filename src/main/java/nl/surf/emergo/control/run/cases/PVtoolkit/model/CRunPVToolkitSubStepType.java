/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.model;

public class CRunPVToolkitSubStepType {

	String subStepType;
	String mandatoryPreviousSubStepType;
	boolean mandatoryForStudent;
	boolean mandatoryForTeacher;
	boolean mandatoryForPeerStudent;
	boolean defaultPresentForStudent; 
	boolean defaultPresentForTeacher;
	boolean defaultPresentForPeerStudent;
	
	public CRunPVToolkitSubStepType(
			String subStepType,
			String mandatoryPreviousSubStepType,
			boolean mandatoryForStudent,
			boolean mandatoryForTeacher,
			boolean mandatoryForPeerStudent,
			boolean defaultPresentForStudent,
			boolean defaultPresentForTeacher,
			boolean defaultPresentForPeerStudent) {
		setSubStepType(subStepType);
		setMandatoryPreviousSubStepType(mandatoryPreviousSubStepType);
		setMandatoryForStudent(mandatoryForStudent);
		setMandatoryForTeacher(mandatoryForTeacher);
		setMandatoryForPeerStudent(mandatoryForPeerStudent);
		setDefaultPresentForStudent(defaultPresentForStudent);
		setDefaultPresentForTeacher(defaultPresentForTeacher);
		setDefaultPresentForPeerStudent(defaultPresentForPeerStudent);
	}
	
	public String getSubStepType() {
		return subStepType;
	}
	public void setSubStepType(String subStepType) {
		this.subStepType = subStepType;
	}
	public String getMandatoryPreviousSubStepType() {
		return mandatoryPreviousSubStepType;
	}
	public void setMandatoryPreviousSubStepType(String mandatoryPreviousSubStepType) {
		this.mandatoryPreviousSubStepType = mandatoryPreviousSubStepType;
	}
	public boolean isMandatoryForStudent() {
		return mandatoryForStudent;
	}
	public void setMandatoryForStudent(boolean mandatoryForStudent) {
		this.mandatoryForStudent = mandatoryForStudent;
	}
	public boolean isMandatoryForTeacher() {
		return mandatoryForTeacher;
	}
	public void setMandatoryForTeacher(boolean mandatoryForTeacher) {
		this.mandatoryForTeacher = mandatoryForTeacher;
	}
	public boolean isMandatoryForPeerStudent() {
		return mandatoryForPeerStudent;
	}
	public void setMandatoryForPeerStudent(boolean mandatoryForPeerStudent) {
		this.mandatoryForPeerStudent = mandatoryForPeerStudent;
	}
	public boolean isDefaultPresentForStudent() {
		return defaultPresentForStudent;
	}
	public void setDefaultPresentForStudent(boolean defaultPresentForStudent) {
		this.defaultPresentForStudent = defaultPresentForStudent;
	}
	public boolean isDefaultPresentForTeacher() {
		return defaultPresentForTeacher;
	}
	public void setDefaultPresentForTeacher(boolean defaultPresentForTeacher) {
		this.defaultPresentForTeacher = defaultPresentForTeacher;
	}
	public boolean isDefaultPresentForPeerStudent() {
		return defaultPresentForPeerStudent;
	}
	public void setDefaultPresentForPeerStudent(boolean defaultPresentForPeerStudent) {
		this.defaultPresentForPeerStudent = defaultPresentForPeerStudent;
	}

}
