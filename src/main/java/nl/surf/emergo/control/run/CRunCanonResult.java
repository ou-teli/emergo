/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Html;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Treeitem;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefHtml;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunCanonResult is used to show a canon result within the run view area of the emergo player.
 */
public class CRunCanonResult extends CRunComponent {

	private static final long serialVersionUID = -5901296209369403787L;

	/**
	 * Instantiates a new c run canon result.
	 */
	public CRunCanonResult() {
		super("runCanonResult", null, false);
		createComponents();
	}

	/**
	 * Instantiates a new c run canon result.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the canon result case component
	 */
	public CRunCanonResult(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent, false);
		createComponents();
	}

	/**
	 * Creates title area, content area to show canon result tree, item area to show
	 * selected canon result piece and buttons area with close button.
	 */
	@Override
	protected void createComponents() {
		CRunVbox lVbox = new CRunVbox();
		appendChild(lVbox);

		createTitleArea(lVbox);
		CRunHbox lHbox = new CRunHbox();
		lVbox.appendChild(lHbox);
		createContentArea(lHbox);
		CRunArea lItemArea = createItemArea(lHbox);
		lItemArea.setId(getId()+"ItemArea");
		createButtonsArea(lVbox);
	}

	/**
	 * Creates new content component, the canon result tree.
	 *
	 * @return the component
	 */
	@Override
	protected Component newContentComponent() {
		return (new CRunCanonResultTree("runCanonResultTree", caseComponent, this));
	}

	/**
	 * Creates buttons area and adds close button.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run hbox
	 */
	@Override
	protected CRunHbox createButtonsArea(Component aParent) {
		CRunHbox lButtonsHbox = super.createButtonsArea(aParent);
		createCloseButton(lButtonsHbox);
		return lButtonsHbox;
	}

	/**
	 * Does contentitem action, clicking on an canon result item.
	 * Shows item within item area.
	 *
	 * @param aContentItem the a contentitem, the canon result item clicked
	 */
	@Override
	public void doContentItemAction(Component aContentItem) {
		Treeitem lTreeitem = (Treeitem)aContentItem;
		IXMLTag tag = (IXMLTag)lTreeitem.getAttribute("item");
		if ((tag == null) || (!tag.getName().equals("piece")))
			return;
		CRunArea lItemArea = (CRunArea)CDesktopComponents.vView().getComponent(getId()+"ItemArea");
		if (lItemArea != null)
			createItem(lItemArea,tag);
	}

	/**
	 * Shows item within item area. First removes previous item.
	 *
	 * @param aParent the ZK parent
	 * @param aItem the piece within the canon case component
	 *
	 * @return the c run vbox
	 */
	protected CRunVbox createItem(Component aParent,IXMLTag aItem) {
		removeItem(aParent);
		CRunArea lArea = new CRunArea();
		aParent.appendChild(lArea);
		lArea.setZclass(getClassName()+"_item");
		CRunVbox lVbox = new CRunVbox();
		lArea.appendChild(lVbox);
//		append link
		createLink(lVbox,aItem);
//		append picture
		createPicture(lVbox,aItem);
//		append motivation
		createMotivation(lVbox,aItem);
//		append rating and rating motivation per rga
		createRatings(lVbox,aItem);
		return lVbox;
	}

	/**
	 * Removes item from item area.
	 *
	 * @param aParent the a parent
	 */
	protected void removeItem(Component aParent) {
		if (aParent.getChildren() != null)
			aParent.getChildren().clear();
	}

	/**
	 * Shows link within item area.
	 *
	 * @param aParent the a parent
	 * @param aItem the a item
	 */
	protected void createLink(Component aParent,IXMLTag aItem) {
		Label lLabel = new CRunBlobLabel(aItem);
		aParent.appendChild(lLabel);
	}

	/**
	 * Shows picture within item area.
	 *
	 * @param aParent the a parent
	 * @param aItem the a item, the tag containing the picture tag as child
	 */
	protected void createPicture(Component aParent,IXMLTag aItem) {
		Image lImage = new CRunBlobImage(aItem);
		if (((lImage.getSrc() != null) && (!lImage.getSrc().equals(""))) || (lImage.getContent() != null)) {
			aParent.appendChild(lImage);
			lImage.setWidth("150px");
		}
	}

	/**
	 * Shows motivation within item area.
	 *
	 * @param aParent the a parent
	 * @param aItem the a item
	 */
	protected void createMotivation(Component aParent,IXMLTag aItem) {
		Html lHtml = new CDefHtml(CDesktopComponents.sSpring().unescapeXML(aItem.getChildValue("motivation")));
		aParent.appendChild(lHtml);
		lHtml.setZclass(getClassName() + "_item_html");
	}

	/**
	 * Shows ratings and rating motivations within item area.
	 *
	 * @param aParent the a parent
	 * @param aItem the a item
	 */
	protected void createRatings(Component aParent,IXMLTag aItem) {
		String lRating = "";
		String lMotivation = "";
		List<IXMLTag> lRgaChilds = aItem.getChild(AppConstants.statusElement).getChilds("rga");
		for (IXMLTag lRgaChild: lRgaChilds) {
			lRating = "";
			lMotivation = "";
			String lRgaId = lRgaChild.getValue();
			List<IXMLTag> lRatingChilds = lRgaChild.getChilds(AppConstants.statusKeyRating);
			if (lRatingChilds.size()>0)
				// get last rating
				lRating = ((IXMLTag)lRatingChilds.get(lRatingChilds.size()-1)).getValue();
			lRatingChilds = lRgaChild.getChilds("ratingmotivation");
			if (lRatingChilds.size()>0)
				// get last rating
				lMotivation = ((IXMLTag)lRatingChilds.get(lRatingChilds.size()-1)).getValue();
			if (((!lRating.equals("")) && (!lRating.equals("0"))) || (!lMotivation.equals(""))) {
				createBreak(aParent);
				Html lHtml = new CDefHtml(CDesktopComponents.sSpring().getRunGroupAccount(Integer.parseInt(lRgaId)).getERunGroup().getName() + " (" + lRating + ")");
				aParent.appendChild(lHtml);
				lHtml.setZclass(getClassName() + "_item_html_bold");
				lHtml = new CDefHtml(CDesktopComponents.sSpring().unescapeXML(lMotivation));
				aParent.appendChild(lHtml);
				lHtml.setZclass(getClassName() + "_item_html");
			}
		}
	}

	/**
	 * Creates break within item area.
	 *
	 * @param aParent the a parent
	 */
	protected void createBreak(Component aParent) {
		Html lHtml = new CDefHtml("<br/>");
		aParent.appendChild(lHtml);
	}

}
