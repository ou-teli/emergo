/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.OpenEvent;
import org.zkoss.zul.Tree;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CContentHelper;
import nl.surf.emergo.control.CContentItemMenuPopup;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;

/**
 * The Class CRunContentItemMenuPopup is used to show a contextualised popup menu within the Emergo player environment.
 * Popup menu items are mainly crud operations and depend on the XML tag referenced by the ZK component right clicked. 
 */
public class CRunContentItemMenuPopupClassic extends CContentItemMenuPopup {

	private static final long serialVersionUID = -5007766190452419069L;

	/** The run wnd. */
	protected CRunWndClassic runWnd = null;
	
	/**
	 * The accisauthor. Within the player a case component author within the role case developer will not
	 * be able to edit the case component data, so value is false.
	 */
	protected boolean accisauthor = false;

	/**
	 * Gets the run wnd.
	 * 
	 * @return the run wnd
	 */
	public CRunWndClassic getRunWnd() {
		if (runWnd == null)
			runWnd = (CRunWndClassic) CDesktopComponents.vView().getComponent(CControl.runWnd);
		return runWnd;
	}

	/**
	 * Instantiates a new c run contentitem menu popup.
	 */
	public CRunContentItemMenuPopupClassic() {
		super();
		renderEditRoot = false;
		renderInspect = false;
		renderPreview = false;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.CContentItemMenuPopup#getContentHelper()
	 */
	protected CContentHelper getContentHelper() {
		if (getComponent() == null)
			return null;
		if (getComponent() instanceof CRunTreeClassic)
			return ((CRunTreeClassic)getComponent()).getRunComponentHelper();
		if (getComponent() instanceof CRunGmapsClassic)
			return ((CRunGmapsClassic)getComponent()).getContentHelper();
		return null;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.CContentItemMenuPopup#accIsAuthor()
	 */
	public boolean accIsAuthor() {
		return accisauthor;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.CContentItemMenuPopup#onOpen(org.zkoss.zk.ui.event.OpenEvent)
	 */
	public void onOpen(OpenEvent aEvent) {
		if (!aEvent.isOpen())
			return;
		if (CDesktopComponents.sSpring().getRunGroupAccount() == null) return;
		Component lTarget = aEvent.getReference();
		if (lTarget == null) return;
		setComponent(lTarget);
		if ((component instanceof Tree) && (!(lTarget instanceof Tree)))
			// reference is treerow!
			lTarget = lTarget.getParent();
		IXMLTag lItem = (IXMLTag) lTarget.getAttribute("item");
		if (lItem == null) return;
		CRunContentHelperClassic lRunContentHelper = (CRunContentHelperClassic)getContentHelper();
		boolean lContentOwnership = lRunContentHelper.getRunComponent().getContentOwnership();
		String lOwnerRugId = lItem.getAttribute(AppConstants.keyOwner_rgaid);
//		boolean lRoot = (lItem.getName().equals(AppConstants.contentElement));
		boolean lOwner = (lOwnerRugId.equals(""+CDesktopComponents.sSpring().getRunGroupAccount().getRgaId()));
		boolean lEditDelete = ((!lContentOwnership) || (lOwner));
//		accisauthor = ((lEditDelete) || (lRoot));
		accisauthor = true;
		renderEdit = lEditDelete;
		renderDelete = lEditDelete;
		super.onOpen(aEvent);
	}
}
