/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.spl;

import nl.surf.emergo.control.run.ounl.CRunDirectingLabelButtonOunl;


/**
 * The Class CRunLabelButton, is ancestor of all label buttons within the Emergo player.
 */
public class CRunDirectingLabelButtonSpl extends CRunDirectingLabelButtonOunl {

	private static final long serialVersionUID = -8894022051218166374L;

	/**
	 * Instantiates a new c run label button.
	 */
	public CRunDirectingLabelButtonSpl() {
		super();
	}

	/**
	 * Instantiates a new c run label button.
	 *
	 * @param aId the a id
	 * @param aEventAction the a event action, to be send when button is clicked
	 * @param aEventActionStatus the a event action status, when button is clicked
	 * @param aLabel the a label of the button
	 * @param aZclassExtension the a s class extension for the style class
	 * @param aClientOnClickAction the a client action to be executed when clicked
	 */
	public CRunDirectingLabelButtonSpl(String aId, String aEventAction,
			Object aEventActionStatus, String aLabel, String aZclassExtension,
			String aClientOnClickAction) {
		super(aId, aEventAction, aEventActionStatus, aLabel, aZclassExtension, aClientOnClickAction);
	}

}
