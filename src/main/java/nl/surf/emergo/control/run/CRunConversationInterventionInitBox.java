/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.CreateEvent;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefBox;
import nl.surf.emergo.utilities.PropsValues;
import nl.surf.emergo.view.VView;

public class CRunConversationInterventionInitBox extends CDefBox {

	private static final long serialVersionUID = 3024912215018492549L;

	public void onCreate(CreateEvent aEvent) {
		VView vView = CDesktopComponents.vView();

		CDesktopComponents.sSpring().initDesktopAttributes(getDesktop());

		HtmlMacroComponent macro = new HtmlMacroComponent();
		appendChild(macro);
		macro.setDynamicProperty("a_emergoRootUrl", CDesktopComponents.vView().getAbsoluteWebappsRoot());
		macro.setDynamicProperty("a_runComponentId", VView.getParameter("runComponentId", this, "runConversations"));
		macro.setDynamicProperty("a_webcamAppName", vView.getStreamingRecorderApplication());
		macro.setDynamicProperty("a_movieAutoExtension", PropsValues.STREAMING_RECORDER_AUTOFILEEXTENSION);
		macro.setDynamicProperty("a_alwaysAskPermission",
				getDesktop().getAttribute("desktop_wc_PermissionAsked") == null);
		getDesktop().setAttribute("desktop_wc_PermissionAsked", "true");
		macro.setDynamicProperty("a_movieName", "");
		macro.setMacroURI("../run_conversation_interaction_view_fr_macro.zul");
	}

}
