/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.feedback;

import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitInitBox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefHbox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTag;

public class CRunPVToolkitFeedbackSkillClusterButton extends CRunPVToolkitDefDiv {

	private static final long serialVersionUID = 4317697284721930553L;

	public String zulfilepath = ((CRunPVToolkitInitBox)CDesktopComponents.vView().getComponent("PV-toolkit_initBox")).getZulfilepath();

	protected int _numberOfPerformanceLevels = 0;
	protected int _numberOfPerformanceLevelsToShow  = 0;
	
	protected int _maxLabelLength = 35;
	protected int _dotWidth = 16;

	protected CRunPVToolkitCacAndTag _runPVToolkitCacAndTag;
	protected String _childName;
	protected int _level;
	protected List<Integer> _subskillLevels;
	protected boolean _hasScore;
	protected boolean _hasTipsTops;
	protected String _feedbackRubricId;

	protected CRunPVToolkitFeedbackRubricDiv _feedbackRubric;

	protected boolean _accessible = true;

	protected String _stepPrefix;
	protected String _classPrefix;
	
	protected CRunPVToolkit pvToolkit;
	
	protected final static int buttonWidth = 160;
	protected final static int buttonLeftOffset = 230;

	public CRunPVToolkitFeedbackSkillClusterButton(Component parent, String[] keys, Object[] values) {
		super(parent, keys, values);
	}
	
	public void init(CRunPVToolkitCacAndTag cacAndTag, String childName, int level, List<Integer> subskillLevels, String feedbackRubricId, String stepPrefix, String classStr) {
		_runPVToolkitCacAndTag = cacAndTag;
		_childName = childName;
		_feedbackRubricId = feedbackRubricId;
		_stepPrefix = stepPrefix;
		_classPrefix = classStr;

		_feedbackRubric = (CRunPVToolkitFeedbackRubricDiv)CDesktopComponents.vView().getComponent(_feedbackRubricId);
		_hasScore = _feedbackRubric.skillclusterHasScore();
		_hasTipsTops = _feedbackRubric.skillclusterHasTipsTops();

		IXMLTag skillClusterTag = _runPVToolkitCacAndTag.getXmlTag();
		//NOTE skill cluster is accessible. Skill tag always is accessible.
		_accessible = !skillClusterTag.getCurrentStatusAttribute(AppConstants.statusKeyAccessible).equals(AppConstants.statusValueFalse);
		
		if (_feedbackRubric.inOverview() || _feedbackRubric.inRubric()) {
			setStyle("cursor:default;");
		}
		if (_feedbackRubric.inOverview()) {
			_maxLabelLength = 47;
		}
		if (_feedbackRubric.inRubric()) {
			_maxLabelLength = 26;
		}
		
		pvToolkit = (CRunPVToolkit)CDesktopComponents.vView().getComponent("pvToolkit");
		
		_numberOfPerformanceLevels = pvToolkit.getNumberOfPerformanceLevels();
		_numberOfPerformanceLevelsToShow = pvToolkit.getNumberOfPerformanceLevelsToShow();
		
		update(level, subskillLevels);
	}

	public void update(int level, List<Integer> subskillLevels) {
		//NOTE if can be scored on skill cluster level, level is 0 or contains the last chose level. Stars will be shown to be able to score the skill cluster
		//if not can be scored on skill cluster level but on sub skill level, subskillLevels contains the scores for the sub skills. Dots will be shown to indicate which sub skills already are scored
		//NOTE either stars or dots are shown
		//TODO implement scoring for skill cluster
		_level = level;
		_subskillLevels = subskillLevels;

		getChildren().clear();

		//NOTE there are only 10 colors defined for skill clusters
		int skillclusterCounter = ((int)getAttribute("skillclusterCounter")) - 1;
		skillclusterCounter = skillclusterCounter % 10;
		skillclusterCounter++;
		
		if (!(_feedbackRubric.inOverview() || _feedbackRubric.inRubric())) {
			new CRunPVToolkitDefImage(this, 
					new String[]{"class", "src"}, 
					new Object[]{_classPrefix + "Background", zulfilepath + "cluster" + skillclusterCounter + ".svg"}
			);
		}
		
		Div div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "Label"}
		);

		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "cacAndTag", "tagChildName", "maxLabelLength"}, 
				new Object[]{"font " + _classPrefix + "Label", _runPVToolkitCacAndTag, _childName, _maxLabelLength}
		);
		
		String style = ""; 

		if (_hasTipsTops) {
			style = "width:" + (_dotWidth * _subskillLevels.size()) + "px;right:30px;";
		}
		else {
			style = "width:" + (_dotWidth * _subskillLevels.size()) + "px;right:10px;";
		}
		div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class", "style"}, 
				new Object[]{_classPrefix + "Status", style}
		);

		Hbox hbox = new CRunPVToolkitDefHbox(div, null, null);
		if (!(_feedbackRubric.inOverview() || _feedbackRubric.inRubric())) {
			for (int subskillLevel : _subskillLevels) {
				appendDot(hbox, subskillLevel);
			}
		}

		if (_hasTipsTops && _accessible) {
			CRunPVToolkitFeedbackTipsTopsDiv tipsTopsDiv = new CRunPVToolkitFeedbackTipsTopsDiv(this, 
					new String[]{"class"}, 
					new Object[]{_classPrefix + "TipsTops"}
			);
			tipsTopsDiv.init(_runPVToolkitCacAndTag, _level, _feedbackRubricId, _stepPrefix);
		}

		if (_feedbackRubric.inRubric()) {
			//NOTE left and width of stars depend on number of performance levels
			double width = 5 * buttonWidth;
			width = width / _numberOfPerformanceLevelsToShow;
			String widthStyle = "width:" + width + "px;";
			double left = buttonLeftOffset;
			if (pvToolkit.getCurrentRubricTag().getChildValue("performancelevelsdescending").equals(AppConstants.statusValueTrue)) {
				for (int i=_numberOfPerformanceLevelsToShow;i>0;i--) {
					left = showStars(left, width, i, widthStyle);
				}
			}
			else {
				for (int i=1;i<=_numberOfPerformanceLevelsToShow;i++) {
					left = showStars(left, width, i, widthStyle);
				}
			}
		}
	}

	protected double showStars(double left, double width, int i, String widthStyle) {
		String leftStyle = "left:" + left + "px;";
		Div div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class", "style"}, 
				new Object[]{_classPrefix + "ButtonLevel" + (i + 5 - _numberOfPerformanceLevels), leftStyle + widthStyle}
				);
		div = new CRunPVToolkitDefDiv(div, 
				new String[]{"class", "style"}, 
				new Object[]{_classPrefix + "ButtonLevelStars", widthStyle}
				);
		pvToolkit.renderStars(div, i, false, _classPrefix + "Score");
	
		left += width;
		return left;
	}

	public void appendDot(Component parent, int level) {
		String src = "";
		if (level > 0) {
			src = zulfilepath + "dot-blue.svg";
		}
		else if (level == CRunPVToolkit.noLevelYet) {
			src = zulfilepath + "dot-gray.svg";
		}
		else if (level == CRunPVToolkit.noLevelPossible) {
			src = zulfilepath + "dot-lightgray.svg";
		}
		new CRunPVToolkitDefImage(parent, 
				new String[]{"class", "src"}, 
				new Object[]{_classPrefix + "Status", src}
		);
	}

	public void onClick() {
		if (_feedbackRubric.inOverview() || _feedbackRubric.inRubric()) {
			return;
		}
		Component previousSubskillsDiv = _feedbackRubric.getPreviousSubskillsDiv();
		int indexOfThis = getParent().getChildren().indexOf(this);
		int indexOfSubskillsDiv = indexOfThis + 1; 
		if (indexOfSubskillsDiv < getParent().getChildren().size()) {
			Component subskillsDiv = getParent().getChildren().get(indexOfSubskillsDiv);
			if (subskillsDiv != null && !(subskillsDiv instanceof CRunPVToolkitFeedbackSkillClusterButton)) {
				if (subskillsDiv.isVisible()) {
					subskillsDiv.setVisible(false);
					previousSubskillsDiv = null;
				}
				else {
					if (previousSubskillsDiv != null) {
						previousSubskillsDiv.setVisible(false);
					}
					subskillsDiv.setVisible(true);
					previousSubskillsDiv = subskillsDiv;
				}
			}
		}
		_feedbackRubric.setPreviousSubskillsDiv(previousSubskillsDiv);
	}

}
