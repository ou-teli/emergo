/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefImage;

/**
 * The Class CRunImage.
 */
public class CRunImage extends CDefImage {

	private static final long serialVersionUID = -2895786861992103169L;

	/** The event action to be send to observers when clicked.. */
	protected String eventAction = "";

	/** The event action status to be send to observers when clicked.. */
	protected Object eventActionStatus = null;

	public String getClassName() {
		return CDesktopComponents.vView().getRunClassName(className);
	}

	/**
	 * Instantiates a new c run image.
	 */
	public CRunImage() {
	}

	/**
	 * Instantiates a new c run image. Either aBlobId or aSrc is used to get
	 * the image.
	 *
	 * @param aId the a id
	 * @param aEventAction the a event action
	 * @param aEventActionStatus the a event action status
	 * @param aBlobId the a blob id of the image to be shown
	 * @param aSrc the a src of the image to be shown
	 * @param aZclassExtension the a s class extension used to get correct style class
	 */
	public CRunImage(String aId, String aEventAction,
			Object aEventActionStatus, String aBlobId, String aSrc, String aZclassExtension) {
		if (!aId.equals(""))
			setId(aId);
		setEventAction(aEventAction);
		setEventActionStatus(aEventActionStatus);
		setVisible(false);
		if (!aBlobId.equals(""))
			showBlobImage(aBlobId);
		if (!aSrc.equals(""))
			showSrcImage(aSrc);
		if (!aZclassExtension.equals(""))
			setZclass(getClassName() + aZclassExtension);
	}

	/**
	 * Sets the event action.
	 *
	 * @param aEventAction the new event action
	 */
	public void setEventAction(String aEventAction) {
		eventAction = aEventAction;
	}

	/**
	 * Sets the event action status.
	 *
	 * @param aEventActionStatus the new event action status
	 */
	public void setEventActionStatus(Object aEventActionStatus) {
		eventActionStatus = aEventActionStatus;
	}

	/**
	 * Shows blob image.
	 *
	 * @param aBlobId the a blob id
	 */
	public void showBlobImage(String aBlobId) {
		setVisible(false);
		setContent((org.zkoss.image.Image) null);
		setSrc(CDesktopComponents.sSpring().getSBlobHelper().getImageSrc(aBlobId));
		setVisible(true);
	}

	/**
	 * Shows blob image.
	 *
	 * @param aBackgroundTag the a background tag containing the blob id
	 */
	public void showBlobImage(IXMLTag aBackgroundTag) {
		setVisible(false);
		if (aBackgroundTag == null)
			return;
		String lBlobId = aBackgroundTag.getChildValue("picture");
		String lBlobtype = aBackgroundTag.getChildAttribute("picture", AppConstants.keyBlobtype);
		setContent((org.zkoss.image.Image) null);
		String lSrc = "";
		if (lBlobtype.equals(AppConstants.blobtypeIntUrl))
			// url
			lSrc = CDesktopComponents.sSpring().getSBlobHelper().getUrl(aBackgroundTag);
		else
			// blob
			lSrc = CDesktopComponents.sSpring().getSBlobHelper().getImageSrc(lBlobId);
		if (getSrc() == null || !lSrc.equals(getSrc())) {
			setSrc(lSrc);
		}
		setVisible(true);
	}

	/**
	 * Shows src image.
	 *
	 * @param aSrc the a src
	 */
	public void showSrcImage(String aSrc) {
		setVisible(false);
		setContent((org.zkoss.image.Image) null);
		setSrc(aSrc);
		setVisible(true);
	}

	/**
	 * On click notify observers.
	 */
	public void onClick() {
		if (!isDoubleClick()) {
			this.notifyObservers(eventAction, eventActionStatus);
		}
	}
}
