/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl;

import org.zkoss.zk.ui.Component;

import nl.surf.emergo.control.run.CRunComponent;

/**
 * The Class CRunTutorialConversationInteractionOunl is used for interaction during conversations
 * and is shown within the run choice area.
 */
public class CRunTutorialConversationInteractionOunl extends CRunConversationInteractionOunl {

	private static final long serialVersionUID = 6005403613093140403L;

	/**
	 * Instantiates a new c run conversation interaction.
	 * And creates content area.
	 */
	public CRunTutorialConversationInteractionOunl() {
		super();
	}

	/**
	 * Instantiates a new c run conversation interaction.
	 * And creates content area.
	 *
	 * @param aId the a id
	 * @param aRunComponent the a run component
	 * @param aRunConversationsId the a run conversations id
	 */
	public CRunTutorialConversationInteractionOunl(String aId, CRunComponent aRunComponent, String aRunConversationsId) {
		super(aId, aRunComponent, aRunConversationsId);
	}

	/**
	 * Creates new content component, the interaction tree.
	 *
	 * @return the component
	 */
	@Override
	protected Component newContentComponent() {
		return (new CRunTutorialInteractionTreeOunl("runInteractionTree", runComponent, runConversationsId));
	}

	/**
	 * Creates title area, a title label and close box.
	 *
	 * @param aParent the a parent
	 */
	protected void createTitleArea(Component aParent) {
		// no title area
	}

}
