/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefListbox;

public class CRunEditformSelectorListbox extends CDefListbox {

	private static final long serialVersionUID = 880723803897198950L;

	public void onCreate(CreateEvent aEvent) {
		//create listitems
		String options = (String)getAttribute("options");
		String[] optionArr = options.split("\n");
		String answer = "," + (String)getAttribute("answer") + ",";
		String[] answerArr = answer.split(",");
		for (int i=0;i<optionArr.length;i++) {
			Listitem listitem = new Listitem();
			listitem.setLabel(optionArr[i]);
			listitem.setValue("" + (i + 1));
			appendChild(listitem);
			if (answer.indexOf("," + (i + 1) + ",") > -1) {
				addItemToSelection(listitem);
			}
		}
		int numberofrows = Integer.parseInt((String)getAttribute("numberofrows"));
		if (numberofrows <= -1) {
			numberofrows = getChildren().size();
		}
		if (numberofrows == 0) {
			numberofrows = 1;
		}
		setRows(numberofrows);
		if (isMultiple()) {
			setCheckmark(true);
		}
		else {
			setMold("select");
		}
		Events.postEvent("onUpdate", this, null);
	}

	public void onSelect(Event aEvent) {
		CRunEditforms runEditforms = (CRunEditforms)CDesktopComponents.vView().getComponent("runEditforms");
		if (runEditforms != null) {
			Events.postEvent("onHideFeedback", runEditforms, null);
			String answer = "";
			for (Listitem listitem : getSelectedItems()) {
				if (!answer.equals("")) {
					answer += ",";
				}
				answer += (String)listitem.getValue();
			}
			runEditforms.saveSelection((IXMLTag)getAttribute("tag"), answer);
			Events.postEvent("onUpdate", this, (runEditforms.isHideRightWrong((IXMLTag)getAttribute("tag")) ? "hideRightWrong" : ""));
		}
	}

	public void onUpdate(Event aEvent) {
		CRunEditforms runEditforms = (CRunEditforms)CDesktopComponents.vView().getComponent("runEditforms");
		if (runEditforms != null) {
			runEditforms.updateEditformItem(this, "CRunEditformSectionOverwrite", (String)aEvent.getData());
		}
	}

}
