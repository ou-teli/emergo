/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefMacro;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.STriggeredReference;

/**
 * The Class CRunNotifications is used to show notifications within or outside the run view area of the emergo player.
 */
public class CRunNotifications extends CRunComponent {
	
	private static final long serialVersionUID = -3077011650363278140L;

	protected boolean init = false;
	
	public static final String divNotificationsId = "divNotifications";
	public int notWidth = -1;
	public int notHeight = -1;
	public int notificationsRowNumber = 1;
	public static final String macroNotificationsItemsId = "macroNotificationsItems";

	/**
	 * Instantiates a new c run notifications.
	 */
	public CRunNotifications() {
		super("runNotifications", null);
		init();
	}

	/**
	 * Instantiates a new c run notifications.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the case component
	 */
	public CRunNotifications(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
		init();
	}

	/**
	 * Creates title area, content area to show notifications,
	 * and buttons area with close button.
	 */
	@Override
	protected void createComponents() {
		CRunVbox lVbox = new CRunVbox();

		createTitleArea(lVbox);
		createContentArea(lVbox);
		createButtonsArea(lVbox);

		appendChild(lVbox);
	}

	/**
	 * Creates new content component, the notifications view.
	 *
	 * @return the component
	 */
	@Override
	protected Component newContentComponent() {
		HtmlMacroComponent notificationsView = new CDefMacro();
		String lId = "runNotificationsView";
		if (caseComponent != null) {
			lId += "" + caseComponent.getCacId();
		}
		notificationsView.setId(lId);
		notificationsView.setZclass("");
		return notificationsView;
	}

	/**
	 * Creates buttons area and adds close button.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run hbox
	 */
	@Override
	protected CRunHbox createButtonsArea(Component aParent) {
		return null;
	}

	/**
	 * Is notifications component visible.
	 *
	 * @return if visible
	 */
	public boolean isNotificationsComponentVisible() {
		return runWnd.isLocationComponentVisible(caseComponent) && hasNotifications();
	}

	/**
	 * Creates view for notifications.
	 */
	public void init() {
		if (!init) {
			init = true;
			CDesktopComponents.sSpring().setRunComponentStatus(caseComponent, AppConstants.statusKeyOpened, AppConstants.statusValueTrue, true, getRunStatusType(), true);
		}

		Events.postEvent("onInitZulfile", this, null);
	}
	
	public boolean setVisible(boolean aVisible) {
		boolean lVisible = super.setVisible(aVisible);
		if (aVisible) {
			scrollToLastNotification();
		}
		return lVisible;
	}
	
	public void scrollToLastNotification() {
		// scrolls div containing notifications to last notification
 		Clients.evalJavaScript("scrollToLastNotification('" + getId() + "');");
	}

	public void update() {
 		String macroNotificationsItemsId = "macroNotificationsItems" + getId();
		HtmlMacroComponent macroNotificationsItems = (HtmlMacroComponent)CDesktopComponents.vView().getComponent(macroNotificationsItemsId);
		if (macroNotificationsItems != null) {
			macroNotificationsItems.setDynamicProperty("a_notifications_datas", getNotificationsDatas());
			macroNotificationsItems.recreate();
			if (runWnd != null) {
				setVisible(isNotificationsComponentVisible());
			}
		}
		else {
			//NOTE If no macro yet do init.
			//This can happen if one script condition has two actions sending a notification.
			//The first notification will create CRunNotifications (within run wnd), but before
			//the notifications component is rendered on screen, the second notification comes in.
			init();
		}
	}

	protected boolean getNotificationsClosable() {
		IXMLTag lRootTag = CDesktopComponents.sSpring().getXmlDataTree(getCaseComponent());
		IXMLTag lComponentTag = lRootTag.getChild(AppConstants.componentElement);
		return lComponentTag.getCurrentStatusAttribute("closable")
				.equals(AppConstants.statusValueTrue);
	}

	public String getNotificationsPositionLeft() {
		return getNotificationsPositionOrSize("position", 0);
	}

	public String getNotificationsPositionTop() {
		return getNotificationsPositionOrSize("position", 1);
	}

	public String getNotificationsSizeWidth() {
		return getNotificationsPositionOrSize("size", 0);
	}

	public String getNotificationsSizeHeight() {
		return getNotificationsPositionOrSize("size", 1);
	}

	public String getNotificationsBackgroundUrl() {
		IXMLTag lRootTag = CDesktopComponents.sSpring().getXmlDataTree(getCaseComponent());
		if (lRootTag != null) {
			String lUrl = CDesktopComponents.sSpring().getSBlobHelper().getUrl(lRootTag.getChild(AppConstants.contentElement));
			if (!lUrl.equals("")) {
				lUrl = CDesktopComponents.vView().getEmergoWebappsRoot() + lUrl;  
			}
			return lUrl;
		}
		return "";
	}

	public String getNotificationsTextcolor() {
		IXMLTag lRootTag = CDesktopComponents.sSpring().getXmlDataTree(getCaseComponent());
		if (lRootTag != null) {
			return lRootTag.getChild(AppConstants.contentElement).getChildValue("textcolor");
		}
		return "";
	}

	protected String getNotificationsPositionOrSize(String aAttributedName, int aIndex) {
		IXMLTag lRootTag = CDesktopComponents.sSpring().getXmlDataTree(getCaseComponent());
		IXMLTag lComponentTag = lRootTag.getChild(AppConstants.componentElement);
		String lPosition = lComponentTag.getChildAttribute(AppConstants.statusElement, aAttributedName);
		if (lPosition.equals("")) {
			return "";
		}
		String[] lPositionArr = lPosition.split(",");
		if (lPositionArr == null || aIndex > (lPositionArr.length - 1)) {
			return "";
		}
		try {
			int lValue = Integer.parseInt(lPositionArr[aIndex]);
			return "" + lValue;
		} catch (NumberFormatException e) {
			return "";
		}
	}

	protected boolean getNotificationsAccumulate() {
		IXMLTag lRootTag = CDesktopComponents.sSpring().getXmlDataTree(getCaseComponent());
		return lRootTag.getChild(AppConstants.componentElement).getCurrentStatusAttribute("accumulate")
				.equals(AppConstants.statusValueTrue);
	}

	protected boolean hasNotifications() {
		List<IXMLTag> lXmlTags = getStatusTags("notification");
		if (lXmlTags == null) {
			return false;
		}
		return lXmlTags.size() > 0;
	}

	protected List<List<Object>> getNotificationsDatas() {
		List<List<Object>> lDataList = new ArrayList<List<Object>>();
		List<IXMLTag> lStatusTags = getStatusTags("notification");
		if (lStatusTags == null) {
			return lDataList;
		}
		if (lStatusTags.size() > 0) {
			if (getNotificationsAccumulate()) {
				// accumulate all notifications
				for (IXMLTag lStatusTag : lStatusTags) {
					addNotificationsData(lDataList, lStatusTag);
				}
			}
			else {
				//NOTE only add last notification
				for (int i=lStatusTags.size()-1;i>=0;i--) {
					IXMLTag lStatusTag = lStatusTags.get(i);
					String lDataTagId = addNotificationsData(lDataList, lStatusTag);
					if (!lDataTagId.equals("")) {
						break;
					}
				}
			}
		}
		return lDataList;
	}

	protected String addNotificationsData(List<List<Object>> aDataList, IXMLTag aStatusTag) {
		boolean lDeleted = CDesktopComponents.sSpring().getCurrentTagStatus(aStatusTag, AppConstants.statusKeyDeleted).equals(AppConstants.statusValueTrue);
		if (!lDeleted) {
			IXMLTag lDataTag = aStatusTag.getDataTag();
			if (lDataTag != null) {
				List<Object> lData = new ArrayList<Object>();
				lData.add(lDataTag);
				lData.add(lDataTag.getAttribute(AppConstants.keyId));
				//get notification
				lData.add(CDesktopComponents.sSpring().unescapeXML(lDataTag.getChildValue("richtext")));
				aDataList.add(lData);
				return aStatusTag.getAttribute(AppConstants.keyRefdataid);				
			}
		}
		return "";
	}

	protected List<IXMLTag> getStatusTags(String aTagName) {
		List<IXMLTag> lXmlTags = new ArrayList<IXMLTag>();
		IXMLTag lRootTag = CDesktopComponents.sSpring().getXmlRunGroupStatusTree(CDesktopComponents.sSpring().getRunGroup().getRugId(), getCaseComponent());
		if (lRootTag != null) {
			IXMLTag lContentRootTag = lRootTag.getChild(AppConstants.contentElement);
			if (lContentRootTag != null) {
				lXmlTags = lContentRootTag.getChilds(aTagName);
			}
		}
		return lXmlTags;
	}

	protected boolean setStatusKeyDeletedForCurrentNotifications(String aDataTagId, boolean aDelete) {
		IXMLTag lStatusRootTag = CDesktopComponents.sSpring().getXmlRunStatusTree(caseComponent, AppConstants.statusTypeRunGroup);
		if (lStatusRootTag == null) {
			return false;
		}
		IXMLTag lStatusContentRootTag = lStatusRootTag.getChild(AppConstants.contentElement);
		if (lStatusContentRootTag == null) {
			return false;
		}
		List<IXMLTag> lStatusTags = lStatusContentRootTag.getChilds("notification");

		boolean lChanged = false;
		for (IXMLTag lStatusTag : lStatusTags) {
			if (lStatusTag.getAttribute(AppConstants.keyRefdataid).equals(aDataTagId)) {
				boolean lDeleted = CDesktopComponents.sSpring().getCurrentTagStatus(lStatusTag, AppConstants.statusKeyDeleted).equals(AppConstants.statusValueTrue);
				String lStatusValue = "";
				if (aDelete && !lDeleted) {
					lStatusValue = AppConstants.statusValueTrue;
				}
				else if (!aDelete && lDeleted) {
					lStatusValue = AppConstants.statusValueFalse;
				}
				if (!lStatusValue.equals("")) {
					CDesktopComponents.sSpring().setRunTagStatusValue(lStatusTag, AppConstants.statusKeyDeleted, lStatusValue);
					CDesktopComponents.sSpring().getSLogHelper().logSetRunTagAction("setruntagstatus", AppConstants.statusTypeRunGroup, CDesktopComponents.sSpring().getXmlManager().unescapeXML(caseComponent.getName()), lStatusTag.getName(),
							lStatusTag.getChildValue("pid"), AppConstants.statusKeyDeleted, lStatusValue, false);
					lChanged = true;
				}
			}
		}
		if (lChanged) {
			// save status
			CDesktopComponents.sSpring().setSaveRunGroupCaseComponentStatusRootTag(CDesktopComponents.sSpring().getRunGroup(), caseComponent, lStatusRootTag);
		}
		return lChanged;
	}

	@Override
	public void handleStatusChange(STriggeredReference aTriggeredReference) {
		if (aTriggeredReference.getDataTag() == null) {
			if (aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyPresent)) {
				if (runWnd != null) {
					setVisible(isNotificationsComponentVisible());
				}
			}
		}
		else {
			//notification is set present or sent
			if (aTriggeredReference.getDataTag().getName().equals("notification")) {
				if (aTriggeredReference.getStatusKey().equals(AppConstants.statusKeySent)) {
					update();
				}
				else if (aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyDeleted)) {
					//NOTE Set statusKeyDeleted for all status tags that have a reference to data tag
					setStatusKeyDeletedForCurrentNotifications(aTriggeredReference.getDataTag().getAttribute(AppConstants.keyId), aTriggeredReference.getStatusValue().equals(AppConstants.statusValueTrue));
					update();
				}
			}
		}
	}

	/**
	 * Closes notifications.
	 */
	public void closeNotifications() {
		CDesktopComponents.sSpring().setRunComponentStatus(caseComponent, AppConstants.statusKeyOpened, AppConstants.statusValueFalse, true, AppConstants.statusTypeRunGroup, true);
		detach();
	}


	/**
	 * Methods originally used in ZUL file.
	 */

	public void onInitZulfile(Event aEvent) {
		String notificationsId = getId();
		Boolean closable = getNotificationsClosable();
		String notificationsWidth = getNotificationsSizeWidth();
		String notificationsHeight = getNotificationsSizeHeight();
		String notificationsBackground = getNotificationsBackgroundUrl();
		String notificationsTextcolor = getNotificationsTextcolor();
		List<List<Object>> notificationsDatas = getNotificationsDatas();

		if (notificationsDatas == null) {
			// fill with test data
			notificationsId = "notifications";
			closable = true;
			notificationsWidth = "300";
			notificationsHeight = "300";
			notificationsBackground = "";
			notificationsTextcolor = "";
			String notification = "notification";
			notificationsDatas = new ArrayList<List<Object>>();
			for (int i=1;i<=20;i++) {
				List<Object> data = new ArrayList<Object>();
				data.add(null);
				data.add("" + i);
				data.add(notification);
				notificationsDatas.add(data);
			}
		}

		notWidth = -1;
		if (!notificationsWidth.equals("")) {
			notWidth = Integer.parseInt(notificationsWidth);
			notWidth -= 12;
		}
		notHeight = -1;
		if (!notificationsHeight.equals("")) {
			notHeight = Integer.parseInt(notificationsHeight);
			notHeight -= 12;
			if (closable) {
				notHeight -= 24;
			}
		}
		String styleTransparent = "";
		if (!notificationsBackground.equals("")) {
			styleTransparent = "Transparent";
		}
		String htmlStyle = "";
		if (!notificationsTextcolor.equals("")) {
			htmlStyle = "color:" + notificationsTextcolor + ";";
		}
		
		Map<String,Object> propertyMap = new HashMap<String,Object>();
		propertyMap.put("currentEmergoComponent", this);
		propertyMap.put("notifications_notificationsId", notificationsId);
		propertyMap.put("notifications_closable", closable);
		propertyMap.put("notificationsDatas", notificationsDatas);
		propertyMap.put("notifications_styleTransparent", styleTransparent);
		propertyMap.put("notifications_htmlStyle", htmlStyle);
		propertyMap.put("macroNotificationsItemsId", macroNotificationsItemsId);
		((HtmlMacroComponent)getRunContentComponent()).setDynamicProperty("a_propertyMap", propertyMap);
		((HtmlMacroComponent)getRunContentComponent()).setMacroURI(VView.v_run_notifications_fr);
		getRunContentComponent().setVisible(true);
	}

}
