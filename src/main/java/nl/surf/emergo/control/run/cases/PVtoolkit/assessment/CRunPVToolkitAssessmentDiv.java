/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.assessment;

import java.util.Hashtable;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.CheckEvent;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Column;
import org.zkoss.zul.Columns;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CIObserved;
import nl.surf.emergo.control.def.CDefColumn;
import nl.surf.emergo.control.def.CDefGrid;
import nl.surf.emergo.control.run.CRunArea;
import nl.surf.emergo.control.run.CRunWnd;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefHbox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefHtml;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.ounl.CRunAssessmentsOunl;
import nl.surf.emergo.control.run.ounl.CRunAssessmentsOunl.ItemTagData;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;

public class CRunPVToolkitAssessmentDiv extends CRunArea {

	private static final long serialVersionUID = 4823146814842021525L;

	protected String relativePath = "";

	protected CRunAssessmentsOunl runAssessments;
	protected List<List<Object>>assessmentsDatas;
	protected IXMLTag currentAssessmentTag;
	protected String assessmentTitle;
	protected String assessmentInstruction;
	protected List<IXMLTag> assessmentRefItemTags;
	
	protected String _classPrefix = "PVassessment";

	protected Hashtable<String, IXMLTag> altParentItems = new Hashtable<String, IXMLTag>();
	protected Hashtable<String, IXMLTag> altRefItems = new Hashtable<String, IXMLTag>();
	
	@Override
	public void observedNotify(CIObserved aObserved, String aAction, Object aStatus) {
	}

	
	public void onCreate(CreateEvent aEvent) {
		//NOTE if PV-tool is used in game, hide game tablet
		Component runTablet = CDesktopComponents.vView().getComponent("runTablet");
		if (runTablet != null) {
			runTablet.setVisible(false);
		}
		//NOTE change style of original assessments component, so PV-tool assessment will be shown full-screen.
		Component parent = getParent();
		while (parent != null) {
			String zClass = ((HtmlBasedComponent)parent).getZclass();
			boolean setFullScreen = zClass != null && (zClass.equals("CRunAssessments_content_area") || zClass.equals("CRunWnd_tablet_app"));
			String sClass = ((HtmlBasedComponent)parent).getSclass();
			setFullScreen = setFullScreen || (sClass != null && (sClass.equals("CRunAssessments_content_area") || sClass.equals("CRunWnd_tablet_app")));
			if (setFullScreen) {
				((HtmlBasedComponent)parent).setStyle("left:0px;top:0px;width:1200px;height:650px;z-index:800;");
			}
			parent = parent.getParent();
		}

		Events.postEvent("onInitZulfile", this, null);
	}

	/**
	 * Methods originally used in ZUL file.
	 */

	public void onInitZulfile(Event aEvent) {

		runAssessments = (CRunAssessmentsOunl)CDesktopComponents.vView().getComponent("runAssessments");
		
		relativePath = runAssessments.getZulfilePath();

		Component runContentComponent = runAssessments.getRunContentComponent();
		assessmentsDatas = (List<List<Object>>)runContentComponent.getAttribute("assessments_datas");
		String assessmentTagId = "";
		if (assessmentsDatas.size() == 1) {
			assessmentTagId = (String)assessmentsDatas.get(0).get(0);
		}
		if (assessmentTagId.equals("")) {
			return;
		}
		currentAssessmentTag = runAssessments.getAssessmentTag(assessmentTagId);
		assessmentTitle = runAssessments.getAssessmentTitle(currentAssessmentTag);
		assessmentInstruction = runAssessments.getAssessmentInstruction(currentAssessmentTag);
		assessmentRefItemTags = runAssessments.getAssessmentActualRefItemTags(currentAssessmentTag);
		
		renderTitle();
		renderInstruction();
		renderItems();

		Button readyButton = new CRunPVToolkitDefButton(this,
				new String[]{"class", "cLabelKey"}, 
				new Object[]{_classPrefix + "Font " + _classPrefix + "ReadyButton", "PV-toolkit.ready"}
				);
		addReadyButtonOnClickEventListener(readyButton);
	}
	
	protected void renderTitle() {
		new CRunPVToolkitDefLabel(this, 
				new String[]{"class", "value"}, 
				new Object[]{_classPrefix + "Font " + _classPrefix + "TitleLabel", assessmentTitle}
				);
	}

	protected void renderInstruction() {
		new CRunPVToolkitDefHtml(this, 
				new String[]{"class", "value"}, 
				new Object[]{_classPrefix + "Font " + _classPrefix + "InstructionHtml", assessmentInstruction}
				);
	}

	protected void renderItems() {
		Rows rows = appendItemsGrid(this);
		
		for (IXMLTag assessmentRefItemTag : assessmentRefItemTags) {
			ItemTagData itemTagData = runAssessments.getItemTagData(assessmentRefItemTag);
			IXMLTag itemTag = itemTagData.getItemTag();
			if (itemTag != null) {
				String score = runAssessments.getRefItemAnswer(assessmentRefItemTag);

				Row row = new Row();
				rows.appendChild(row);

				String itemInstruction = runAssessments.getItemInstruction(itemTag);
				String itemType = runAssessments.getItemType(itemTag);
				String altOrientation = runAssessments.getAltOrientation(itemTag);
				new CRunPVToolkitDefHtml(row, 
						new String[]{"class", "value"}, 
						new Object[]{_classPrefix + "Font " + _classPrefix + "ItemInstructionHtml", itemInstruction}
						);
				
				Div div = new CRunPVToolkitDefDiv(row, 
						new String[]{"class"}, 
						new Object[]{_classPrefix + "RadioDiv"}
						);
				String classPostfix = "";
				if (altOrientation.equals("horizontal")) {
					classPostfix = "RadioHbox";
				}
				else {
					classPostfix = "RadioVbox";
				}
				Hbox hbox = new CRunPVToolkitDefHbox(div, 
						new String[]{"class"}, 
						new Object[]{_classPrefix + classPostfix}
						);
				Radiogroup radioGroup = new Radiogroup();
				hbox.appendChild(radioGroup);
				radioGroup.setSclass(_classPrefix + "RadioGroup");
				radioGroup.setOrient(altOrientation);
				radioGroup.setAttribute("refItemTag", assessmentRefItemTag);
				radioGroup.setAttribute("itemTag", itemTag);
				addAltOnCheckEventListener(radioGroup);
				for (IXMLTag assessmentRefItemAltTag : runAssessments.getRefItemAltTags(currentAssessmentTag, assessmentRefItemTag)) {
					String altTagId = runAssessments.getXMLTagId(assessmentRefItemAltTag);
					altParentItems.put(altTagId, itemTag);
					altRefItems.put(altTagId, assessmentRefItemTag);
					if (assessmentRefItemAltTag.getName().equals("alternative")) {
						boolean isAltChecked = runAssessments.isAlternativeChecked(assessmentRefItemAltTag, itemType, score);

						Radio radio = new Radio();
						radioGroup.appendChild(radio);
						radio.setSclass(_classPrefix + "Radio");
						radio.setAttribute("altTagId", altTagId);
						radio.setChecked(isAltChecked);
						if (altOrientation.equals("vertical")) {
							radio.setLabel(runAssessments.getAlternativeText(assessmentRefItemAltTag));
						}
					} else {
						if (assessmentRefItemAltTag.getName().equals("field")) {
							row = new Row();
							rows.appendChild(row);

							HtmlMacroComponent macro = new HtmlMacroComponent();
							row.appendChild(macro);
							macro.setDynamicProperty("a_observer", this.getId());
							macro.setDynamicProperty("a_event", "onHandleAssessmentItemField");
							macro.setDynamicProperty("a_assessmentitem_alt_tag_id", altTagId);
							macro.setDynamicProperty("a_assessmentitem_alt_text", runAssessments.getAlternativeText(assessmentRefItemAltTag));
							macro.setDynamicProperty("a_assessmentitem_alt_numberoflines", runAssessments.getFieldNumberOfLines(assessmentRefItemAltTag));
							macro.setDynamicProperty("a_assessmentitem_alt_default_answer", runAssessments.getFieldText(assessmentRefItemAltTag));
							macro.setDynamicProperty("a_assessmentitem_alt_answer", CDesktopComponents.sSpring().unescapeXMLAlt(runAssessments.getFieldAnswer(assessmentRefItemTag, runAssessments.getXMLTagId(assessmentRefItemAltTag)).replace(AppConstants.statusCrLfReplace, "\n")));
							macro.setDynamicProperty("a_assessmentitem_alt_pieces", runAssessments.getPiecesDatas(itemTagData.getCaseRole(), itemTagData.getCaseComponent(), assessmentRefItemAltTag));
							macro.setDynamicProperty("a_is_assessmentitem_editable", "true");
							macro.setMacroURI("../run_assessmentitemaltfield_macro.zul");
							
						}
					}
				}
			}
		}
	}

	public void onHandleAssessmentItemField(Event aEvent) {
		String assessmentItemAltTagId = (String)((Object[])aEvent.getData())[0];
		String value = (String)((Object[])aEvent.getData())[1];
		IXMLTag itemTag = altParentItems.get(assessmentItemAltTagId);
		IXMLTag refItemTag = altRefItems.get(assessmentItemAltTagId);
		runAssessments.handleAssessmentItemField(refItemTag, itemTag, assessmentItemAltTagId, value, false);
	}

	protected void addAltOnCheckEventListener(Component component) {
		component.addEventListener("onCheck", new EventListener<CheckEvent>() {
			public void onEvent(CheckEvent event) {
				Radiogroup radioGroup = (Radiogroup)component;
				runAssessments.handleAssessmentItemAltRadio((IXMLTag)radioGroup.getAttribute("refItemTag"), (IXMLTag)radioGroup.getAttribute("itemTag"), (String)radioGroup.getSelectedItem().getAttribute("altTagId"), true);
			}
		});
	}
	
	protected Rows appendItemsGrid(Component parent) {
    	return appendItemsGrid(
    			parent,
    			"position:absolute;left:20px;top:150px;width:1160px;height:430px;overflow:auto;", 
    			new String[] {"80%", "20%"},
    			"PV-toolkit-assessment.column.label.", 
    			new String[] {"proposition", "score"});
    }

    public Rows appendItemsGrid(
    		Component parent, 
    		String gridStyle, 
    		String[] columnWidths,
    		String labelKeyPrefix, 
    		String[] labelKeyPostfixes) {
		Grid grid = new CDefGrid();
		parent.appendChild(grid);
		grid.setStyle(gridStyle);
		
		Columns columns = new Columns();
		grid.appendChild(columns);
		
		for (int i=0;i<columnWidths.length;i++) {
			Column column = new CDefColumn();
			columns.appendChild(column);
			column.setWidth(columnWidths[i]);
			column.setLabel(CDesktopComponents.vView().getLabel(labelKeyPrefix + labelKeyPostfixes[i]));
		}
		
		Rows rows = new Rows();
		grid.appendChild(rows);
		
		return rows;
	}

	protected void addReadyButtonOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				boolean finished = true;
				for (IXMLTag assessmentRefItemTag : assessmentRefItemTags) {
					finished = finished && runAssessments.isRefItemFinished(assessmentRefItemTag);
				}
				if (!finished) {
					VView vView = CDesktopComponents.vView();
					vView.showMessagebox(getRoot(), vView.getCLabel("PV-toolkit-assessment.notready.confirmation"), vView.getCLabel("PV-toolkit-assessment.notready"), Messagebox.OK, Messagebox.EXCLAMATION);
				}
				else {
					runAssessments.setAssessmentFinished(currentAssessmentTag, AppConstants.statusValueTrue);
					CRunWnd runWnd = (CRunWnd) CDesktopComponents.vView().getComponent(CControl.runWnd);
					runWnd.endTabletApp(runAssessments);
					Component runTablet = CDesktopComponents.vView().getComponent("runTablet");
					if (runTablet != null) {
						IECaseComponent caseComponent = CDesktopComponents.sSpring().getCaseComponent("empack", "");
						CDesktopComponents.sSpring().setRunComponentStatus(caseComponent, AppConstants.statusKeyOpened, AppConstants.statusValueFalse, true, AppConstants.statusTypeRunGroup, true);
						runWnd.onAction(getId(), "endTablet", null);
					}
				}
			}
		});
	}

}
