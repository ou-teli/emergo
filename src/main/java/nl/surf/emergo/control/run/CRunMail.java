/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CCaseHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CIObserved;
import nl.surf.emergo.control.script.CScript;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.view.VView;

/**
 * The Class CRunMail is used to show the mail component within the run view area of the
 * Emergo player.
 */
public class CRunMail extends CRunComponent {

	private static final long serialVersionUID = -4984583667833552722L;

	/** The case helper. */
	protected CCaseHelper caseHelper = null;

	/**
	 * Instantiates a new c run mail.
	 */
	public CRunMail() {
		super("runMail", null);
	}

	/**
	 * Instantiates a new c run mail.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the mail case component
	 */
	public CRunMail(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
	}

	/**
	 * Gets the case helper.
	 *
	 * @return the case helper
	 */
	protected CCaseHelper getCaseHelper() {
		if (caseHelper == null)
			caseHelper = new CCaseHelper();
		return caseHelper;
	}

	/**
	 * Creates title area, content area to show logbook, and buttons area
	 * with send new mail button and close button.
	 */
	@Override
	protected void createComponents() {
		CRunVbox lVbox = new CRunVbox();

		createTitleArea(lVbox);
		createContentArea(lVbox);
		createButtonsArea(lVbox);

		appendChild(lVbox);
	}

	/**
	 * Creates new content component, the mail tree.
	 *
	 * @return the component
	 */
	@Override
	protected Component newContentComponent() {
		return (new CRunMailTree("runMailTree", caseComponent, this));
	}

	/**
	 * Creates buttons area and adds send new mail and close button.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run hbox
	 */
	@Override
	protected CRunHbox createButtonsArea(Component aParent) {
		CRunHbox lButtonsHbox = super.createButtonsArea(aParent);
		createNewMailButton(lButtonsHbox);
		createCloseButton(lButtonsHbox);
		return lButtonsHbox;
	}

	/**
	 * Creates new mail button.
	 *
	 * @param aParent the a parent
	 */
	protected void createNewMailButton(Component aParent) {
		aParent.appendChild(newNewMailButton());
	}

	/**
	 * Creates new new mail button.
	 *
	 * @return the c run button
	 */
	protected CRunButton newNewMailButton() {
		String lLabel = "";
		if (CDesktopComponents.sSpring().isTutorRun())
			lLabel = CDesktopComponents.vView().getLabel("run_mails.button.new_mail.tutor");
		else
			lLabel = CDesktopComponents.vView().getLabel("run_mails.button.new_mail");
		CRunButton lButton = new CRunButton("newMailButton", "newMail", "", lLabel, "_component_200", "");
		lButton.registerObserver(getId());

		setNewMailButtonStatus(lButton);
		return lButton;
	}

	/**
	 * Sets status of new mail button.
	 */
	public void setNewMailButtonStatus(CRunButton aButton) {
		aButton.setDisabled(getMailTemplateTags().size() == 0);
	}

	/**
	 * Sets status of new mail button.
	 */
	public void setNewMailButtonStatus() {
		CRunButton lButton = (CRunButton)CDesktopComponents.vView().getComponent("newMailButton");
		if (lButton != null) {
			setNewMailButtonStatus(lButton);
		}
	}

	/**
	 * Is called by new mail button and shows new mail dialogue.
	 *
	 * @param aObserved the observed object, it should have interface CIObserved implemented
	 * @param aAction the action
	 * @param aStatus the status object, can be String or other class
	 */
	@Override
	public void observedNotify(CIObserved aObserved, String aAction, Object aStatus) {
		if (aAction.equals("newMail")) {
			if (CDesktopComponents.sSpring().isTutorRun()) {
				Map<String,Object> lParams = new HashMap<String,Object>();
				lParams.put("item", CDesktopComponents.vView().getLabel("run_mails.button.new_mail.tutor.alert"));
				CDesktopComponents.vView().modalPopupWithoutWaiting(VView.v_run_alert, null, lParams, "center");
			}
			// modalpopup in parentwindow because of timing problems in Firefox (sometimes mail opened in "alert" window)
			Map<String,Object> lParams = new HashMap<String,Object>();
			lParams.put("item", caseComponent);
			CDesktopComponents.vView().modalPopupWithoutWaiting(VView.v_run_new_mail, getRoot(), lParams, "center,top");
		}
		else if (aAction.equals("attachmentClicked")) {
			IXMLTag lDataTag = (IXMLTag)aStatus;
			//NOTE for in mails, the attachment maybe a copy of the original attachment added by a case developer,
			//because mail tags are copied including attachment child tags, if data is restored.
			//for out mails a user generated attachment may be clicked.
			if (lDataTag != null) {
				//get original attachment
				IXMLTag lReferencedDataTag = CDesktopComponents.sSpring().getXmlManager().getTagById(
						CDesktopComponents.sSpring().getXmlDataPlusRunStatusTree(caseComponent, getRunStatusType()),
						lDataTag.getAttribute(AppConstants.keyReforiginalid));
				//if it exists use it (lDataTag will be unequal to the data tag entered by the case developer)
				if (lReferencedDataTag != null) {
					lDataTag = lReferencedDataTag;
				}
			}
			if (lDataTag != null) {
				//NOTE click on user generated attachment is not saved!
				setRunTagStatus(caseComponent, lDataTag, AppConstants.statusKeySelected, AppConstants.statusValueTrue, true);
				setRunTagStatus(caseComponent, lDataTag, AppConstants.statusKeyOpened, AppConstants.statusValueTrue, true);
			}
		}
	}

	/**
	 * Gets the mail template tags, all current mails that can be send by Emergo user.
	 *
	 * @return the mail template tags
	 */
	public List<IXMLTag> getMailTemplateTags() {
		IECaseComponent lCaseComponent = caseComponent;
		CScript cScript = CDesktopComponents.cScript();
		List<IXMLTag> lMailTags = new ArrayList<IXMLTag>();
		List<IXMLTag> lMailTags2 = null;
		if (CDesktopComponents.sSpring().isTutorRun()) {
			// get all predefined in mails
			lMailTags = cScript.getRunGroupNodeTags("" + lCaseComponent.getCacId(), "inmailpredef");
			// get all predefined in help mails
			lMailTags2 = cScript.getRunGroupNodeTags("" + lCaseComponent.getCacId(), "inmailhelp");
			lMailTags.addAll(lMailTags2);
		}
		else {
			if (lCaseComponent != null) {
				// get all predefined out mails
				lMailTags = cScript.getRunGroupNodeTags("" + lCaseComponent.getCacId(), "outmailpredef");
				// get all predefined out help mails
				lMailTags2 = cScript.getRunGroupNodeTags("" + lCaseComponent.getCacId(), "outmailhelp");
				lMailTags.addAll(lMailTags2);
			}
		}
		// get only mail tag templates, not already sent mail tags

		filterOnPresentTemplateTags(lMailTags);
		return lMailTags;
	}

	/**
	 * Gets all possible current contact ids.
	 *
	 * @param aCaseRole the a case role
	 * @param aCaseComponent the a case component
	 * @param aMailTags the a mail tags
	 * @param aContactIds the a contact ids to be returned
	 */
	public void getContactIds(IECaseRole aCaseRole,
			IECaseComponent aCaseComponent, List<IXMLTag> aMailTags,
			Hashtable<String,String> aContactIds) {
		// loop through mailtags to get contact ids
		for (IXMLTag lMailTag : aMailTags) {
			boolean lPresent = (!(CDesktopComponents.sSpring().getCurrentTagStatus(lMailTag,AppConstants.statusKeyPresent)).equals(AppConstants.statusValueFalse));
			if (lPresent) {
				IXMLTag lDefChildTag = null;
				if (CDesktopComponents.sSpring().isTutorRun())
					lDefChildTag = lMailTag.getDefChild("refsendernpc");
				else
					lDefChildTag = lMailTag.getDefChild("refreceiversnpc");
				String lReftype = lDefChildTag.getDefAttribute(AppConstants.defKeyReftype);
				List<String> lRefIds = getCaseHelper().getRefTagIds(lReftype, ""
						+ AppConstants.statusKeySelectedIndex, aCaseRole,
						aCaseComponent, lMailTag);
				for (String lRefId : lRefIds) {
					aContactIds.put(lRefId, "");
				}
			}
		}
	}

	/**
	 * Filters aStatusTags on present attribute true.
	 *
	 * @param aStatusTags the status tags
	 */
	public void filterOnPresentTemplateTags (List<IXMLTag> aStatusTags) {
		for (int i=(aStatusTags.size()-1);i>=0;i--) {
			IXMLTag lStatusTag = (IXMLTag)aStatusTags.get(i);
			String lRefStatusIds = lStatusTag.getAttribute(AppConstants.keyRefstatusids);
			boolean lHasRefStatusIds = !lRefStatusIds.equals("");
			String lVersion = CDesktopComponents.sSpring().getCurrentTagStatus(lStatusTag,AppConstants.statusKeyVersion);
			boolean lHasVersion = !(lVersion.equals("") || lVersion.equals("0"));
			boolean lTemplateTag = (lHasRefStatusIds || (!lHasVersion));
			if (lTemplateTag) {
				boolean lPresent = (!(CDesktopComponents.sSpring().getCurrentTagStatus(lStatusTag,AppConstants.statusKeyPresent)).equals(AppConstants.statusValueFalse));
				if (!lPresent)
					aStatusTags.remove(i);
			}
			else
				aStatusTags.remove(i);
		}
	}

	/**
	 * Gets the contact strings to be shown to the user.
	 *
	 * @param aMailTags the a mail tags
	 *
	 * @return the contacts
	 */
	public List<String> getContactStrings(List<IXMLTag> aMailTags) {
		IECaseComponent lCaseComponent = caseComponent;
		IECase lCase = CDesktopComponents.sSpring().getRun().getECase();
		IECaseRole lCaseRole = CDesktopComponents.sSpring().getSCaseRoleHelper().getCaseRole();
		List<List<String>> lContactIds = new ArrayList<List<String>>();
		// get contact strings
		getContactIds(lCaseRole, lCaseComponent, aMailTags, lContactIds);
		List<String> lContactStrings = new ArrayList<String>();
		getContactStrings(lCase, lCaseRole, lContactIds, lContactStrings);
		return lContactStrings;
	}

	/**
	 * Gets the contact ids.
	 *
	 * @param aCaseRole the a case role
	 * @param aCaseComponent the a case component
	 * @param aMailTags the a mail tags
	 * @param aContactIds the a contact ids to be returned
	 */
	public void getContactIds(IECaseRole aCaseRole,
			IECaseComponent aCaseComponent, List<IXMLTag> aMailTags, List<List<String>> aContactIds) {
		// loop through mailtags to get contact ids
		for (IXMLTag lMailTag : aMailTags) {
			boolean lPresent = (!(CDesktopComponents.sSpring().getCurrentTagStatus(lMailTag,AppConstants.statusKeyPresent)).equals(AppConstants.statusValueFalse));
			if (lPresent) {
				IXMLTag lDefChildTag = null;
				if (CDesktopComponents.sSpring().isTutorRun())
					lDefChildTag = lMailTag.getDefChild("refsendernpc");
				else
					lDefChildTag = lMailTag.getDefChild("refreceiversnpc");
				String lReftype = lDefChildTag.getDefAttribute(AppConstants.defKeyReftype);
				List<String> lRefIds = getCaseHelper().getRefTagIds(lReftype, ""
						+ AppConstants.statusKeySelectedIndex, aCaseRole,
						aCaseComponent, lMailTag);
				List<String> lContactIds = new ArrayList<String>();
				for (String lRefId : lRefIds) {
					lContactIds.add(lRefId);
				}
				lDefChildTag = lMailTag.getDefChild("refreceiverspc");
				if (lDefChildTag != null) {
					lReftype = lDefChildTag.getDefAttribute(AppConstants.defKeyReftype);
					lRefIds = getCaseHelper().getRefTagIds(lReftype, ""
							+ AppConstants.statusKeySelectedIndex, aCaseRole,
							aCaseComponent, lMailTag);
					for (String lRefId : lRefIds) {
						lContactIds.add(lRefId);
					}
				}
				aContactIds.add(lContactIds);
			}
			else
				aContactIds.add(new ArrayList<String>());
		}
	}

	/**
	 * Gets the contact strings.
	 *
	 * @param aCase the a case
	 * @param aCaseRole the a case role
	 * @param aContactIds the a contact ids
	 * @param aContactStrings the a contact strings to be returned
	 */
	private void getContactStrings(IECase aCase, IECaseRole aCaseRole,
			List<List<String>> aContactIds, List<String> aContactStrings) {
		List<IECaseComponent> lCaseComponents = CDesktopComponents.sSpring().getCaseComponents(aCase, "persons");
		List<IECaseRole> lPcCaseRoles = CDesktopComponents.sSpring().getSCaseRoleHelper().getCaseRoles(false);
		List<IECaseRole> lNpcCaseRoles = CDesktopComponents.sSpring().getSCaseRoleHelper().getCaseRoles(true);
		List<IERunGroup> lRunGroups = CDesktopComponents.sSpring().getRunGroups();
		IERunGroup lCurrentRunGroup = CDesktopComponents.sSpring().getRunGroup();
		String lCarId = "" + aCaseRole.getCarId();
		for (List<String> lContactIds : aContactIds) {
			String lContactsString = "";
			for (IECaseComponent lCaseComponent : lCaseComponents) {
				String lCacId = "" + lCaseComponent.getCacId();
				List<IXMLTag> lPersonTags = CDesktopComponents.cScript().getRunGroupNodeTags(lCacId, "person");
				for (IXMLTag lPersonTag : lPersonTags) {
					boolean lPresent = (!(CDesktopComponents.sSpring().getCurrentTagStatus(lPersonTag,AppConstants.statusKeyPresent)).equals(AppConstants.statusValueFalse));
					if (lPresent) {
						String lContactId = lCarId + "," + lCacId + ","
							+ lPersonTag.getAttribute(AppConstants.keyId);
						if (lContactIds.contains(lContactId)) {
							if (!lContactsString.equals(""))
								lContactsString = lContactsString + ", ";
							lContactsString = lContactsString + CDesktopComponents.sSpring().unescapeXML(lPersonTag.getChildValue("name"));
						}
					}
				}
			}
			for (IECaseRole lCaseRole : lPcCaseRoles) {
				for (IERunGroup lRunGroup : lRunGroups) {
					if ((lRunGroup.getECaseRole().getCarId() == lCaseRole.getCarId()) &&
						(lRunGroup.getRugId() != lCurrentRunGroup.getRugId())) {
						String lContactId = "" + lCaseRole.getCarId() + "," + "0" + "," + "0";
						if (lContactIds.contains(lContactId)) {
							if (!lContactsString.equals(""))
								lContactsString = lContactsString + ", ";
							lContactsString = lContactsString + lRunGroup.getName();
						}
					}
				}
			}
			for (IECaseRole lCaseRole : lNpcCaseRoles) {
				String lContactId = "" + lCaseRole.getCarId() + "," + "0" + "," + "0";
				if (lContactIds.contains(lContactId)) {
					if (!lContactsString.equals(""))
						lContactsString = lContactsString + ", ";
					lContactsString = lContactsString + lCaseRole.getName();
				}
			}
			aContactStrings.add(lContactsString);
		}
	}

	/**
	 * Gets the to rungroups. The rungroups that should receive the mail.
	 *
	 * @param aMailTag the a mail tag
	 *
	 * @return the contacts
	 */
	public List<IERunGroup> getToRunGroups(IXMLTag aMailTag) {
		IECaseComponent lCaseComponent = caseComponent;
		IECase lCase = CDesktopComponents.sSpring().getRun().getECase();
		IECaseRole lCaseRole = CDesktopComponents.sSpring().getSCaseRoleHelper().getCaseRole();
		List<List<String>> lContactIds = new ArrayList<List<String>>();
		// get contact strings
		List<IXMLTag> lMailTags = new ArrayList<IXMLTag>();
		lMailTags.add(aMailTag);
		getContactIds(lCaseRole, lCaseComponent, lMailTags, lContactIds);
		List<IERunGroup> lToRunGroups = new ArrayList<IERunGroup>();
		getToRunGroups(lCase, lCaseRole, lContactIds, lToRunGroups);
		return lToRunGroups;
	}

	/**
	 * Gets the to rungroups. The rungroups that should receive the mail.
	 *
	 * @param aCase the a case
	 * @param aCaseRole the a case role
	 * @param aContactIds the a contact ids
	 * @param aToRunGroups the a to rungroups to be returned
	 */
	private void getToRunGroups(IECase aCase, IECaseRole aCaseRole,
			List<List<String>> aContactIds, List<IERunGroup> aToRunGroups) {
		List<IECaseRole> lCaseRoles = CDesktopComponents.sSpring().getSCaseRoleHelper().getCaseRoles(false);
		List<IERunGroup> lRunGroups = CDesktopComponents.sSpring().getRunGroups();
		IERunGroup lCurrentRunGroup = CDesktopComponents.sSpring().getRunGroup();
		for (List<String> lContactIds : aContactIds) {
			for (IECaseRole lCaseRole : lCaseRoles) {
				for (IERunGroup lRunGroup : lRunGroups) {
					if ((lRunGroup.getECaseRole().getCarId() == lCaseRole.getCarId()) &&
						(lRunGroup.getRugId() != lCurrentRunGroup.getRugId())) {
						String lContactId = "" + lCaseRole.getCarId() + "," + "0" + "," + "0";
						if (lContactIds.contains(lContactId)) {
							aToRunGroups.add(lRunGroup);
						}
					}
				}
			}
		}
	}

}
