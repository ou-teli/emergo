/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zul.Label;
import org.zkoss.zul.Window;

import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefLabel;

/**
 * The Class CRunTitleArea shows current location, logged on user and close box.
 * If wmv files are played it shows pause and play buttons too.
 */
public class CRunTitleArea extends CRunArea {

	private static final long serialVersionUID = -3026885962553958302L;

	/**
	 * Instantiates a new c run title area.
	 */
	public CRunTitleArea() {
		super("runTitleArea");
		init();
	}

	/**
	 * Instantiates a new c run title area.
	 *
	 * @param aId the a id
	 */
	public CRunTitleArea(String aId) {
		super(aId);
		init();
	}

	/**
	 * Init.
	 */
	public void init() {
		setZclass(getClassName());

		CRunHbox lMainHbox = new CRunHbox();
		appendChild(lMainHbox);
		CRunArea lArea = new CRunArea();
		lMainHbox.appendChild(lArea);
		lArea.setZclass(getClassName()+"_left");
		CRunHbox lHbox = new CRunHbox();
		lArea.appendChild(lHbox);

		Label lLabel = new CDefLabel();
		lLabel.setId(getId() + "LocationName");
		lLabel.setValue("");
		lLabel.setZclass(getClassName() + "_label");
		lHbox.appendChild(lLabel);
		lLabel = new CDefLabel();
		lLabel.setId(getId() + "CaseTime");
		lLabel.setValue("");
		lLabel.setZclass(getClassName() + "_label");
		lHbox.appendChild(lLabel);

		CRunHoverBtn lButton = new CRunPausePlayBtn(getId() + "MediaPauseBtn", "active","", null, "pause", "");
//		set to false, so button can be clicked multiple times
		lButton.setCanHaveStatusSelected(false);
		lButton.setVisible(false);
		lHbox.appendChild(lButton);

		lButton = new CRunPausePlayBtn(getId() + "MediaPlayBtn", "active","", null, "play", "");
		lButton.setCanHaveStatusSelected(false);
		lButton.setVisible(false);
		lHbox.appendChild(lButton);

		lArea = new CRunArea();
		lMainHbox.appendChild(lArea);
		lArea.setZclass(getClassName()+"_right");
		String lAction = "if (confirm('" + CDesktopComponents.vView().getLabel("run.button.close.confirm") + "')) {window.close();};";
		lButton = new CRunCloseBtn(getId() + "CloseBtn", "active","close", null, "close", "onClick:"+lAction);
		lButton.setWidgetListener("onClick", lAction);
		lButton.setCanHaveStatusSelected(false);
		lButton.registerObserver(CControl.runWnd);
		lArea.appendChild(lButton);
	}

	/**
	 * Sets the location name.
	 *
	 * @param aName the new location name
	 */
	public void setLocationName(String aName) {
		Label lLabel = (Label) CDesktopComponents.vView().getComponent(getId() + "LocationName");
		if (lLabel == null)
			return;
		lLabel.setValue(aName);
	}

	/**
	 * Sets the case time.
	 *
	 * @param aTime the new case time
	 */
	public void setCaseTime(String aTime) {
		Label lLabel = (Label) CDesktopComponents.vView().getComponent(getId() + "CaseTime");
		if (lLabel == null)
			return;
		long lTime = Math.round(Double.parseDouble(aTime));
		long lHours = Math.round(lTime / 3600);
		long lMins = Math.round(lTime / 60) - (lHours * 60);
		lLabel.setValue(""+lHours+" uur "+lMins+" min");
	}

	/**
	 * Sets the media btns pause and play visible.
	 *
	 * @param aVisible the new media btns visible
	 */
	public void setMediaBtnsVisible(boolean aVisible) {
		CRunHoverBtn lButton = (CRunHoverBtn) CDesktopComponents.vView().getComponent(getId() + "MediaPauseBtn");
		lButton.setVisible(aVisible);
		lButton = (CRunHoverBtn) CDesktopComponents.vView().getComponent(getId() + "MediaPlayBtn");
		lButton.setVisible(aVisible);
	}

	/**
	 * Sets the media buffering visible.
	 *
	 * @param aVisible the new media buffering visible
	 */
	public void setMediaBufferingVisible(boolean aVisible) {
		Window lWindow = (Window) CDesktopComponents.vView().getComponent("bufferingWnd");
		if (lWindow != null)
			lWindow.setVisible(aVisible);
	}

	/**
	 * Sets the media btns action depending on file extension.
	 *
	 * @param aFileExt the new media btns action
	 */
	public void setMediaBtnsAction(String aFileExt) {
		CRunHoverBtn lButton = (CRunHoverBtn) CDesktopComponents.vView().getComponent(getId() + "MediaPauseBtn");
		String lAction = "";
		if ((aFileExt.equals("wmv")) || (aFileExt.equals("mpg")) || (aFileExt.equals("wav")))
			lAction = "if (document.mediaplayer != null) document.mediaplayer.controls.pause();";
		lButton.setWidgetListener("onClick", lAction);
		lButton = (CRunHoverBtn) CDesktopComponents.vView().getComponent(getId() + "MediaPlayBtn");
		lAction = "";
		if ((aFileExt.equals("wmv")) || (aFileExt.equals("mpg")) || (aFileExt.equals("wav")))
			lAction = "if (document.mediaplayer != null) document.mediaplayer.controls.play();";
		lButton.setWidgetListener("onClick", lAction);
	}

}
