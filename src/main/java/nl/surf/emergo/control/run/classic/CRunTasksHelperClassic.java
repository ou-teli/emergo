/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.Treerow;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CContentHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CTree;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;

/**
 * The Class CRunTasksHelper. Helps rendering tasks.
 */
public class CRunTasksHelperClassic extends CRunComponentHelperClassic {

	/** The last selected task id, only one task can be selected. */
	private String lastSelectedTaskId = "";

	/** The tasks checkable by user. */
	private boolean tasksCheckable = false;

	/**
	 * Instantiates a new c run tasks helper.
	 * 
	 * @param aTree the ZK tasks tree
	 * @param aShowTagNames the a show tag names, comma separated, only tags with these names are shown
	 * @param aCaseComponent the tasks case component
	 * @param aRunComponent the a run component, the ZK tasks component
	 */
	public CRunTasksHelperClassic(CTree aTree, String aShowTagNames, IECaseComponent aCaseComponent, CRunComponentClassic aRunComponent) {
		super(aTree, aShowTagNames, aCaseComponent, aRunComponent);
		tasksCheckable = ((CDesktopComponents.sSpring().getCurrentRunComponentStatus(
				aCaseComponent, "taskscheckable", AppConstants.statusTypeRunGroup)).equals(AppConstants.statusValueTrue));
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTreeHelper#renderTreecell(org.zkoss.zul.Treeitem, org.zkoss.zul.Treerow, nl.surf.emergo.business.IXMLTag, java.lang.String, boolean, java.lang.String)
	 */
	@Override
	protected Treecell renderTreecell(Treeitem aTreeitem, Treerow aTreerow,
			IXMLTag aItem, String aKeyValues, boolean aAccIsAuthor) {
		Treecell lTreecell = getTreerowTreecell(aTreerow, 0);
		// showStatus(lTreecell,aItem);
		String lStatus = "active";
		if (isOpened(aItem))
			lStatus = "active";
		boolean lFinished = isFinished(aItem);
		if (lFinished)
			lStatus = "active";
		if (isRequired(aItem))
			lStatus = "active";
		if (!isAccessible(aItem))
			lStatus = "inactive";
		if (isCanceled(aItem))
			lStatus = "active";
		String lImg = VView.getInitParameter("emergo.style.path") + "icon_task_" + lStatus + ".gif";
		lTreecell.setImage(lImg);

		String lToolTip = CDesktopComponents.sSpring().unescapeXML(aItem.getChildValue("hovertext"));
		if ((lToolTip != null) & (!lToolTip.equals(""))) {
			lTreecell.setTooltip("toolTip" + VView.initTooltip);
			lTreecell.setAttribute("tooltip", lToolTip);
		}
		if (lastSelectedTaskId.equals("")) {
			IECaseComponent lCaseComponent = CDesktopComponents.sSpring().getCaseComponent(
					"tasks", "");
			lastSelectedTaskId = CDesktopComponents.sSpring().getCurrentRunComponentStatus(
					lCaseComponent, AppConstants.statusKeySelectedTagId, AppConstants.statusTypeRunGroup);
		}
		// show run task finished checkbox and populate with status
		Checkbox lCheckbox = getRunTaskFinishedCb(lTreecell, aTreeitem, "taskfinished");
		lCheckbox.setChecked(lFinished);
		if ((!isAccessible(aItem)) || (!tasksCheckable))
			lCheckbox.setDisabled(true);
		// get last selected task from tasks component status. if task selected
		// set style to bold.
		if (aItem.getAttribute(AppConstants.keyId).equals(lastSelectedTaskId))
			lTreecell.setZclass("CRunTasks_treecell_selected");
		else
			lTreecell.setZclass("CRunTasks_treecell_"+lStatus);
		lTreecell.setStyle(CDesktopComponents.vView().getLabel("CRunComponent_treecell_style"));
		String lLabelStr = "";
		if (aItem.getName().equals(AppConstants.contentElement))
			lLabelStr = CContentHelper.getNodeTagLabel(caseComponent.getEComponent().getCode(), aItem.getName());
		else
			lLabelStr = aKeyValues;
		String lTime = aItem.getChildValue("time");
		if (!lTime.equals(""))
			lLabelStr = lLabelStr + " (" + lTime + ")";
		Label lLabel = getComponentLabel(lTreecell, "tasklabel", lLabelStr);
		lLabel.setValue(lLabelStr);
		lLabel.setZclass(runComponent.getClassName() + "_treecell_"+lStatus);
		lLabel.setStyle(CDesktopComponents.vView().getLabel("CRunComponent_treecell_style"));
		return lTreecell;
	}

	/**
	 * Gets run task finished checkbox.
	 *
	 * @param aComponent the component parent
	 * @param aTreeitem the a treeitem
	 * @param aId the a id
	 *
	 * @return the checkbox
	 */
	protected CRunTaskFinishedCbClassic getRunTaskFinishedCb(Component aComponent, Treeitem aTreeitem, String aId) {
		CRunTaskFinishedCbClassic lCheckbox = null;
		for (Component lComponent : aComponent.getChildren()) {
			if (lComponent instanceof CRunTaskFinishedCbClassic && ((String)lComponent.getAttribute("id")).equals(aId)) {
				return (CRunTaskFinishedCbClassic)lComponent;
			}
		}
		lCheckbox = new CRunTaskFinishedCbClassic(aTreeitem);
		lCheckbox.setAttribute("id", aId);
		aComponent.appendChild(lCheckbox);
		return lCheckbox;
	}

}