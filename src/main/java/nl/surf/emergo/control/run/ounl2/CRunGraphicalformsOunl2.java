/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl2;

import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.control.run.CRunConversationInteraction;
import nl.surf.emergo.control.run.CRunConversations;
import nl.surf.emergo.control.run.ounl.CRunComponentDecoratorOunl;
import nl.surf.emergo.control.run.ounl.CRunGraphicalformsOunl;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunGraphicalformsOunl2 is used to show graphicalforms component within the run view area of the emergo player.
 */
public class CRunGraphicalformsOunl2 extends CRunGraphicalformsOunl {

	private static final long serialVersionUID = 3244815813268277850L;

	/**
	 * Instantiates a new c run graphicalforms.
	 */
	public CRunGraphicalformsOunl2() {
		super();
	}

	/**
	 * Instantiates a new c run graphicalforms.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the case component
	 */
	public CRunGraphicalformsOunl2(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
	}

	/**
	 * Creates decorator.
	 *
	 * @return the decorator
	 */
	protected CRunComponentDecoratorOunl createDecorator() {
		return new CRunComponentDecoratorOunl2();
	}

	@Override
	protected CRunConversations getNewRunFeedbackConversations(String aConversationsId, String aConversationInteractionId) {
		return new CRunConversationsOunl2(aConversationsId, aConversationInteractionId, this);
	}

	@Override
	protected CRunConversationInteraction getNewRunFeedbackConversationInteraction(String aConversationInteractionId, CRunComponent aConversations, String aConversationsId) {
		return  new CRunConversationInteractionOunl2(aConversationInteractionId, aConversations, aConversationsId, notifyRunWnd);
	}
	
}
