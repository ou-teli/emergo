/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl2;

import java.util.Map;

import org.zkoss.zk.ui.Component;

import nl.surf.emergo.control.run.CRunHoverBtn;
import nl.surf.emergo.control.run.ounl.CRunTabletAppBtnsOunl;

/**
 * The Class CRunTabletAppBtnsOunl2. Used to show tablet app buttons within tablet.
 * Clicking a tablet app button, opens the corresponding case component within the tablet.
 */
public class CRunTabletAppBtnsOunl2 extends CRunTabletAppBtnsOunl {

	private static final long serialVersionUID = -6612655904781930772L;

	/**
	 * Instantiates a new c run tablet app btns.
	 */
	public CRunTabletAppBtnsOunl2() {
		super();
	}

	/**
	 * Instantiates a new c run tablet app btns.
	 *
	 * @param aId the a id
	 * @param aParent the a parent
	 */
	public CRunTabletAppBtnsOunl2(Component aParent, String aId) {
		super(aParent, aId);
	}

	@Override
	protected int getMaxButtonsPerRow() {
		return 6;
	}

	@Override
	protected CRunHoverBtn createHoverBtn(String aId, String aStatus, String aAction,
			Object aActionStatus, String aImgPrefix, String aLabel) {
		return new CRunTabletAppBtnOunl2(aId, aStatus, aAction, aActionStatus, aImgPrefix, aLabel, "");
	}

	@Override
	protected CRunHoverBtn createHoverBtn(Map<String,Object> aParams) {
		return new CRunTabletAppBtnOunl2(aParams);
	}

}
