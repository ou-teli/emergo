/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.IP2;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zul.Include;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.IAppManager;
import nl.surf.emergo.business.IRunAccountManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefListbox;
import nl.surf.emergo.domain.ECaseComponent;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunAccount;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpring;

public class CRunMonitorRunsLb extends CDefListbox {

	private static final long serialVersionUID = -5699662146726598243L;

	protected VView vView = CDesktopComponents.vView();

	protected CControl cControl = CDesktopComponents.cControl();

	protected SSpring sSpring = CDesktopComponents.sSpring();

	protected List<IERun> runs = new ArrayList<IERun>();

	protected List<List<String>> jspResult = new ArrayList<List<String>>();
	
	protected String gameStateAfgerondCacName = "Game state GameAfgerond"; 

	protected String gameStateAfgerondTagKeyValue = "GameAfgerond"; 

	public void onCreate(CreateEvent aEvent) {
		getItems().clear();
		List<IERunAccount> lRunAccounts = ((IRunAccountManager)CDesktopComponents.sSpring().getBean("runAccountManager")).getAllRunAccountsByAccIdActiveAsTutor(CDesktopComponents.cControl().getAccId());
		int i = 0;
		for (IERunAccount lRunAccount : lRunAccounts) {
			IERun lRun = lRunAccount.getERun();
			if (isRunning(lRun)) {
				runs.add(lRun);
				Listitem lListitem = new Listitem();
				lListitem.setValue(lRun);
				lListitem.appendChild(new Listcell(lRun.getName()));
				this.appendChild(lListitem);
				if (i == 0) {
					this.addItemToSelection(lListitem);
				}
				i++;
			}
		}
		restoreSettings();
		onSelect();
	}

	private boolean isRunning(IERun aRun) {
		if (aRun.getStatus() != AppConstants.run_status_runnable) {
			return false;
		}
		return true;
	}

	public void onSelect() {
		initJspResult();
		fillJspResult(runs);
	}

	void initJspResult() {
		jspResult.clear();
		List<String> headerResult = new ArrayList<String>();
		headerResult.add("naam");
		headerResult.add("studentnummer");
		headerResult.add("datum van afronding");
		headerResult.add("run code");
		jspResult.add(headerResult);
	}

	void fillJspResult(List<IERun> runs) {
		//NOTE don't use listbox to select run, but show data for all runs
		/*
 		if (vView.getComponent("runs").getSelectedItem() == null) {
			return;
		}
		*/
		if (runs.size() == 0) {
			return;
		}
		//NOTE don't use listbox to select run, but show data for all runs
		//IERun run = (IERun)((Listbox)vView.getComponent("runs")).getSelectedItem().getValue();
		//NOTE use first run to get casId
		IERun firstRun = runs.get(0);

		//get all case components. In this case only one with name 'Game state GameAfgerond'.
		List<IECaseComponent> tutCaseComponents = new ArrayList<IECaseComponent>();
		List<String> errors = new ArrayList<String>();
		List<Object> sqlCaseComponents = ((IAppManager)sSpring.getBean("appManager")).getSqlResult("SELECT * FROM `casecomponents` WHERE casCasId=" + firstRun.getECase().getCasId() + " AND name='" + gameStateAfgerondCacName + "';", ECaseComponent.class, errors);
		if (sqlCaseComponents != null && sqlCaseComponents.size() == 1) {
			tutCaseComponents.add((IECaseComponent)sqlCaseComponents.get(0));
		}

		//determine status to retrieve
		List<String> tutTagNames = new ArrayList<String>();
		tutTagNames.add("state");
		String tutTagKeyValue = gameStateAfgerondTagKeyValue;
		List<String> tutKeys = new ArrayList<String>();
		tutKeys.add("value");

		List<Object> sqlResults = new ArrayList<Object>();
		List<Integer> rugIds = new ArrayList<Integer>();
		for (IERun run : runs) {
			//get rugIds for all runs
			StringBuffer lSqlPart = new StringBuffer();
			lSqlPart.append(run.getRunId());
			lSqlPart.insert(0, "SELECT DISTINCT rugId FROM `rungroups` WHERE runRunId in (").append(");");
			List<Object> sqlRugIds = ((IAppManager)sSpring.getBean("appManager")).getSqlResult(lSqlPart.toString(), errors);
			lSqlPart.setLength(0);
			for (Object sqlRugId : sqlRugIds) {
				rugIds.add((Integer)sqlRugId);
				if (lSqlPart.length() != 0) {
					lSqlPart.append(",");
				}
				lSqlPart.append(sqlRugId);
			}

			//get sql data that will be presented
			if (lSqlPart.length() != 0) {
				lSqlPart.insert(0, "AND rungroups.rugId in (").append(")");
			}
			lSqlPart.insert(0, "SELECT rungroups.rugId,accounts.userid,accounts.lastname,accounts.nameprefix,accounts.initials,runs.code "
					+ "FROM `accounts` "
					+ "INNER JOIN `rungroupaccounts` "
					+ "INNER JOIN `rungroups` "
					+ "INNER JOIN `runs` "
					+ "WHERE rungroupaccounts.accAccId=accounts.accId "
					+ "AND rungroups.rugId=rungroupaccounts.rugRugId "
					+ "AND rungroups.active=1 "
					+ "AND runs.runId=rungroups.runRunId ").append(" ORDER BY accounts.lastname;");
			sqlResults.addAll(((IAppManager)sSpring.getBean("appManager")).getSqlResult(lSqlPart.toString(), errors));
		}
		
		//render sql data
		CRunMonitorRunsHelper monitorHelper = new CRunMonitorRunsHelper();
		//TODO possibility of stack overflow error when passing sqlResults to renderItems method
		monitorHelper.renderItems(tutCaseComponents, tutTagNames, tutTagKeyValue, tutKeys, rugIds, sqlResults, jspResult);
		Executions.getCurrent().setAttribute("jspResult", jspResult);
		((Include)vView.getComponent("excelOverview")).setSrc("");
		((Include)vView.getComponent("excelOverview")).setSrc("tut_runs_finished.jsp");
	}

}
