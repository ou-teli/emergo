/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Tree;

import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.CRunArea;
import nl.surf.emergo.control.run.CRunButton;
import nl.surf.emergo.control.run.CRunHbox;
import nl.surf.emergo.control.run.CRunMail;
import nl.surf.emergo.control.run.CRunMailTree;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.zkspring.STriggeredReference;

/**
 * The Class CRunMailOunl is used to show the mail component within the run view area of the
 * Emergo player.
 */
public class CRunMailOunl extends CRunMail {

	private static final long serialVersionUID = 8455804534083746269L;

	/**
	 * Instantiates a new c run mail.
	 */
	public CRunMailOunl() {
		super();
	}

	/**
	 * Instantiates a new c run mail.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the mail case component
	 */
	public CRunMailOunl(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
	}

	/**
	 * Creates new content component, the references tree.
	 *
	 * @return the component
	 */
	@Override
	protected Component newContentComponent() {
		Tree tree = (Tree)super.newContentComponent();
		// change rows
		tree.setRows(18);
		return tree;
	}

	/**
	 * Creates title area and shows name of case component within it.
	 * And adds close button
	 *
	 * @param aParent the ZK parent
	 *
	 * @return the c run area
	 */
	@Override
	protected CRunArea createTitleArea(Component aParent) {
		CRunHbox lHbox = new CRunHbox();
		aParent.appendChild(lHbox);
		CRunComponentDecoratorOunl decorator = createDecorator();
		CRunArea lTitleArea = decorator.createTitleArea(caseComponent, lHbox, getClassName());
		decorator.createCloseArea(caseComponent, lHbox, getClassName(), getId(), this);
		return lTitleArea;
	}

	/**
	 * Creates decorator.
	 *
	 * @return the decorator
	 */
	protected CRunComponentDecoratorOunl createDecorator() {
		return new CRunComponentDecoratorOunl();
	}

	/**
	 * Creates new mail button.
	 *
	 * @param aParent the a parent
	 */
	protected void createNewMailButton(Component aParent) {
		aParent.appendChild(newNewMailButtonOunl());
	}

	/**
	 * Creates new new mail button.
	 *
	 * @return the c run button
	 */
	protected CRunArea newNewMailButtonOunl() {
		String lLabel = "";
		if (CDesktopComponents.sSpring().isTutorRun())
			lLabel = CDesktopComponents.vView().getLabel("run_mails.button.new_mail.tutor");
		else
			lLabel = CDesktopComponents.vView().getLabel("run_mails.button.new_mail");
		CRunArea lButton = createNewMailButton("newMailButton", "newMail", lLabel);
		lButton.registerObserver(getId());

		setNewMailButtonStatus(lButton);
		return lButton;
	}

	/**
	 * Creates new mail button.
	 *
	 * @param aId the a id
	 * @param aEventAction the a event action
	 * @param aLabel the a label
	 *
	 * @return the c run button
	 */
	protected CRunArea createNewMailButton(String aId, String aEventAction, String aLabel) {
		return new CRunLabelButtonOunl(aId, aEventAction, "", aLabel, "", "");
	}

	/**
	 * Sets status of new mail button.
	 */
	public void setNewMailButtonStatus(CRunArea aButton) {
		aButton.setVisible(getMailTemplateTags().size() > 0);
	}

	/**
	 * Sets status of new mail button.
	 */
	public void setNewMailButtonStatus() {
		CRunArea lButton = (CRunArea)CDesktopComponents.vView().getComponent("newMailButton");
		if (lButton != null) {
			setNewMailButtonStatus(lButton);
		}
	}

	/**
	 * Creates close button.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run button
	 */
	@Override
	protected CRunButton createCloseButton(Component aParent) {
		// NOTE Don't create close button
		return null;
	}

	/**
	 * Handles status change due to firing of script actions.
	 *
	 * @param aTriggeredReference the a triggered reference
	 */
	@Override
	public void handleStatusChange(STriggeredReference aTriggeredReference) {
		if (aTriggeredReference.getStatusTag() == null) {
			return;
		}
		CRunWndOunl lRunWnd = (CRunWndOunl)runWnd;
		if (lRunWnd == null) {
			return;
		}
		// lDataTag = null for outmails becoming inmails for other rug's
		boolean lShowMailAlert = aTriggeredReference.getStatusKey().equals(AppConstants.statusKeySent) && 
			(aTriggeredReference.getStatusTag().getName().equals("inmailpredef") || aTriggeredReference.getStatusTag().getName().equals("inmailhelp"));
		if (lShowMailAlert) {
			CRunMailTree lRunMailTree = (CRunMailTree)getRunContentComponent();
			if (lRunMailTree != null)
				lRunMailTree.update();
		}
		else {
			boolean lPresentChangedForMailsToSend = aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyPresent) &&  
				(aTriggeredReference.getStatusTag().getName().equals("outmailpredef") || aTriggeredReference.getStatusTag().getName().equals("outmailhelp"));
			if (lPresentChangedForMailsToSend) {
				setNewMailButtonStatus();
			}
		}
	}

}
