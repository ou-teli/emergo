/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl2;

import java.util.Map;

import nl.surf.emergo.control.run.ounl.CRunTabletAppBtnOunl;

/**
 * The Class CRunTabletAppBtnOunl2.
 */
public class CRunTabletAppBtnOunl2 extends CRunTabletAppBtnOunl {

	private static final long serialVersionUID = 6031024647741083945L;
	
	protected int maxChars = 32;

	/**
	 * Instantiates a new c run tablet app btn.
	 */
	public CRunTabletAppBtnOunl2() {
		super();
	}

	/**
	 * Instantiates a new c run tablet app btn.
	 *
	 * @param aId the a id
	 * @param aStatus the a status
	 * @param aAction the a action
	 * @param aActionStatus the a action status
	 * @param aImgPrefix the a img prefix
	 * @param aLabel the a label
	 * @param aClientOnClickAction the a client on click action
	 */
	public CRunTabletAppBtnOunl2(String aId, String aStatus, String aAction,
			Object aActionStatus, String aImgPrefix, String aLabel, String aClientOnClickAction) {
		super(aId, aStatus, aAction, aActionStatus, aImgPrefix, aLabel, aClientOnClickAction);
		if (aLabel.length() > maxChars) {
			setTooltiptext(aLabel);
			setLabel(aLabel.substring(0, maxChars) + "...");
		}
	}

	/**
	 * Instantiates a new c run tablet app btn.
	 *
	 * @param aParams the a params
	 */
	public CRunTabletAppBtnOunl2(Map<String,Object> aParams) {
		super(aParams);
		String lLabel = (String)aParams.get("label");
		if (lLabel.length() > maxChars) {
			setTooltiptext(lLabel);
			setLabel(lLabel.substring(0, maxChars) + "...");
		}
	}

}
