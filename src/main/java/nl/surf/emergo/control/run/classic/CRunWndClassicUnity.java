/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.CRunTablet;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;

/**
 * The Class CRunWndClassicUnity. The Emergo player window used for Unity player demo.
 */
public class CRunWndClassicUnity extends CRunWndClassic
 {

	private static final long serialVersionUID = 2934795385913771517L;

	/**
	 * Instantiates a new c run unity wnd.
	 */
	public CRunWndClassicUnity() {
		super();
	}

	@Override
	public void onAction(String sender, String action, Object status) {
		// unity player is always on top, so hide conversation if unity player is loaded, when tablet is opened.
		// and show it again if the tablet is closed.
		if (action.equals("showAlert")) {
			hideUnityPlayer();
			super.onAction(sender, action, status);
		}
		else if (action.equals("endAlert")) {
			super.onAction(sender, action, status);
			showUnityPlayer();
		}
		else if (action.equals("showEmpackAction")) {
			hideUnityPlayer();
			super.onAction(sender, action, status);
		}
		else if (action.equals("endComponent")) {
			super.onAction(sender, action, status);
			showUnityPlayer();
		}

		// communication with unity player
		if (action.equals("showNote")) {
			super.onAction(sender, action, status);
			emergoEventToClient("unityplayer", "", "open note", "");
		} else
		if (action.equals("endNote")) {
			super.onAction(sender, action, status);
			emergoEventToClient("unityplayer", "", "close note", "");
		} else
		if (action.equals("onExternalEvent")) {
			if (sender.equals("unityplayer")) {
				String[] lStatus = (String[]) status;
				String lSender = lStatus[0];
				String lReceiver = lStatus[1];
				String lEvent = lStatus[2];
				String lEventData = lStatus[3];
				String[] lEventDataValues = lEventData.split(",");
				String lDataKey = lEventDataValues[0];
				String lDataValue = lEventDataValues[1];
				if (lDataKey.equals("scaredCount")) {
					String lScaredCount = lDataValue;
					int lCount = Integer.parseInt(lScaredCount);
					if (lCount > 4) {
						lCount = 4;
						emergoEventToClient("unityplayer", "", "exiled", "");
					}
					sendAlert("birdscared" + lScaredCount);
				}
				if (lDataKey.equals("startOfGame")) {
					if (lDataValue.equals(AppConstants.statusValueTrue)) {
						sendAlert("startofgame");
						IXMLTag lLocationTag = getLocation(currentLocationTagId);
						if (lLocationTag != null) {
							String lPid = lLocationTag.getChildValue("pid");
							if (lPid.equals("Unity start")) {
								if (sSpring != null)
									emergoEventToClient("unityplayer", "", sSpring.getActiveRunGroupNames(), "");
							}
							else
								emergoEventToClient("unityplayer", "", lLocationTag.getChildValue("pid"), "");
						}
					}
				}
			};
			super.onAction(sender, action, status);
		} else
			super.onAction(sender, action, status);
	}

	protected void hideUnityPlayer() {
		CRunConversationsClassic lConversations = (CRunConversationsClassic)CDesktopComponents.vView().getComponent("runConversations");
		if (lConversations != null && lConversations.getVideoView() != null && lConversations.getVideoView().getSrc().equals(VView.v_run_unity_fr)) {
			emergoEventToClient("unityplayer", "", "hideplayer", "");
		}
	}

	// TO DO
	// doesn't work. CRunConversationsClassic has been removed: 
	//		- videoview in CRunWndClassic.onAction
	//		- CRunConversationsClassic in CRunComponentViewClassic.showEmpackAction
	protected void showUnityPlayer() {
		CRunTablet lRunTablet = (CRunTablet)CDesktopComponents.vView().getComponent("runTablet");
//		CRunAlertClassic lRunAlert = (CRunAlertClassic)CDesktopComponents.vView().getComponent("runAlert");
//		if (lRunTablet == null && lRunAlert == null) {
		if (lRunTablet == null) {
			CRunConversationsClassic lConversations = (CRunConversationsClassic)CDesktopComponents.vView().getComponent("runConversations");
//			if (lConversations != null && lConversations.getVideoView() != null && lConversations.getVideoView().getSrc().equals(VView.v_run_unity_fr)) {
				emergoEventToClient("unityplayer", "", "showplayer", "");
//			}
		}
	}

	/**
	 * Sends alert.
	 *
	 * @param aAlertPid pid of alert tag
	 */
	public void sendAlert(String aAlertPid) {
		if (sSpring == null)
			return;
		IECaseComponent lCaseComponent = sSpring.getCaseComponent("alerts", "");
		IXMLTag lTag = null;
		if (lCaseComponent != null) {
			for (IXMLTag lAlert : getCaseComponentRootTagChildTags(lCaseComponent)) {
				String lKey = lAlert.getChildValue("pid");
				if (lKey.equals(aAlertPid))
					lTag = lAlert;
			}
		}
		if (lTag != null) {
			sSpring.setRunTagStatus(lCaseComponent, lTag, AppConstants.statusKeySent, AppConstants.statusValueTrue, true, AppConstants.statusTypeRunGroup, true, false);
			emergoEventToClient("unityplayer", "", sSpring.unescapeXML(lTag.getChildValue("richtext")), "");
		}
	}

}
