/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Treechildren;
import org.zkoss.zul.Treeitem;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunTasksTree.
 */
public class CRunTasksTreeClassic extends CRunTreeClassic {

	private static final long serialVersionUID = 2381881580436094572L;

	/**
	 * Instantiates a new c run tasks tree.
	 * 
	 * @param aId the a id
	 * @param aCaseComponent the tasks case component
	 * @param aRunComponent the a run component, the ZK tasks component
	 */
	public CRunTasksTreeClassic(String aId, IECaseComponent aCaseComponent, CRunComponentClassic aRunComponent) {
		super(aId, aCaseComponent, aRunComponent);
		tagopenednames = "task";
//		setRows(10);
		update();
		selectedtagid = CDesktopComponents.sSpring().getCurrentRunComponentStatus(caseComponent, AppConstants.statusKeySelectedTagId, AppConstants.statusTypeRunGroup);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTree#getRunComponentHelper()
	 */
	@Override
	public CRunComponentHelperClassic getRunComponentHelper() {
		return new CRunTasksHelperClassic(this, tagopenednames, caseComponent, runComponent);
	}

	/**
	 * Deselects previous clicked task.
	 * 
	 * @param aTagId the a tag id
	 * 
	 * @return the treeitem
	 */
	public Treeitem deselectPreviousClicked(String aTagId) {
		if (aTagId.equals(""))
			return null;
		Treeitem lTreeitem = getContentItem(aTagId);
		if (lTreeitem == null)
			return null;
		// replace old treeitem to show new selected status
		CRunComponentHelperClassic cComponent = getRunComponentHelper();
		Treeitem lNewTreeitem = cComponent.renderTreeitem(lTreeitem.getParent(), lTreeitem, (IXMLTag) lTreeitem.getAttribute("item"));
		// Add existing treechildren to new tree item.
		Treechildren lTreechildren = lTreeitem.getTreechildren();
		if (lTreechildren != null)
			lNewTreeitem.appendChild(lTreechildren);
		lTreeitem.detach();
		return lNewTreeitem;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTree#contentItemClicked(org.zkoss.zk.ui.Component)
	 */
	@Override
	public Component contentItemClicked(Component aClickedComponent) {
		if (aClickedComponent instanceof CRunTaskFinishedCbClassic)
			return null;
		String lSelectedTagId = selectedtagid;
		Treeitem lTreeitem = getContentItemClicked(aClickedComponent);
		IXMLTag tag = getContentItemTag(lTreeitem);
//		check if accessible
		if (!getRunComponentHelper().isAccessible(tag))
			return null;
		if (tag != null)
			lSelectedTagId = tag.getAttribute(AppConstants.keyId);
		lTreeitem = (Treeitem)super.contentItemClicked(aClickedComponent);
		if (!lSelectedTagId.equals(selectedtagid))
			deselectPreviousClicked(selectedtagid);
		selectedtagid = lSelectedTagId;
		return lTreeitem;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTree#setTreeitemStatus(org.zkoss.zul.Treeitem)
	 */
	@Override
	public Treeitem setTreeitemStatus(Treeitem aTreeitem) {
		Treeitem lTreeitem = aTreeitem;
		IXMLTag tag = getContentItemTag(lTreeitem);
		if (tag != null) {
			String lSelectedTagId = tag.getAttribute(AppConstants.keyId);
			CDesktopComponents.sSpring().setRunComponentStatus(caseComponent,
					AppConstants.statusKeySelectedTagId, lSelectedTagId, false, AppConstants.statusTypeRunGroup, true);
//			maybe add something for shared component
		}
		lTreeitem = super.setTreeitemStatus(lTreeitem);
		return lTreeitem;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTree#doTreeitemAction(org.zkoss.zul.Treeitem)
	 */
	@Override
	public void doTreeitemAction(Treeitem aTreeitem) {
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.CTree#contentItemFinished(org.zkoss.zk.ui.Component, org.zkoss.zk.ui.Component, java.lang.String)
	 */
	@Override
	public void contentItemFinished(Component aTreeitem, Component aClickedComponent, String aValue) {
		if (aTreeitem != null) {
			IXMLTag tag = (IXMLTag) aTreeitem.getAttribute("item");
			if (tag != null) {
				aTreeitem = setRunTagStatus(aTreeitem, tag,
						AppConstants.statusKeyFinished, aValue, tagopenednames, true);
//				maybe add something for shared component
			}
		}
	}
}
