/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl;

import org.zkoss.zk.ui.Component;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefLabel;
import nl.surf.emergo.control.run.CRunArea;
import nl.surf.emergo.control.run.CRunCloseBtn;
import nl.surf.emergo.control.run.CRunHoverBtn;
import nl.surf.emergo.control.run.CRunWnd;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunComponentDecoratorOunl is used to decorate OUNL run components like CRunReferencesOunl.
 */
public class CRunComponentDecoratorOunl {

	/**
	 * Creates title area.
	 *
	 * @param aCaseComponent the a case component
	 * @param aParent the ZK parent
	 * @param aClassNamePrefix the a class name prefix
	 *
	 * @return the c run area
	 */
	public CRunArea createTitleArea(IECaseComponent aCaseComponent, Component aParent, String aClassNamePrefix) {
		CRunArea lTitleArea = new CRunArea();
		lTitleArea.setZclass(aClassNamePrefix + "_title_area");
		//NOTE determine if alternative logo is entered. If so, set style.
		String lLogoStyle = "";
		CRunWnd runWnd = (CRunWnd)CDesktopComponents.vView().getComponent(CControl.runWnd);
		if (runWnd != null) {
			IXMLTag lContentTag = runWnd.getCaseComponentRootTag(aCaseComponent);
			if (lContentTag != null) {
				lLogoStyle = runWnd.getStyleBackgroundLogo(lContentTag);
			}
		}
		if (!lLogoStyle.equals("")) {
			lTitleArea.setStyle(lLogoStyle);
		}
		String lTitle = "";
		if (aCaseComponent != null) {
			String lName = aCaseComponent.getName();
			lTitle = CDesktopComponents.sSpring().getCaseComponentRoleName("", lName);
		}
		lTitleArea.appendChild(createTitleArea(lTitleArea, aClassNamePrefix, lTitle));
		aParent.appendChild(lTitleArea);
		return lTitleArea;
	}

	/**
	 * Creates title.
	 *
	 * @param aParent the ZK parent
	 * @param aClassNamePrefix the a class name prefix
	 * @param aLabel the a label
	 *
	 * @return the component
	 */
	public Component createTitleArea(Component aParent, String aClassNamePrefix, String aTitle) {
		CDefLabel lLabel = new CDefLabel(aTitle);
		aParent.appendChild(lLabel);
		lLabel.setZclass(aClassNamePrefix + "_title_area_label");
		return lLabel;
	}

	/**
	 * Creates close area and without close button.
	 *
	 * @param aCaseComponent the a case component
	 * @param aParent the ZK parent
	 * @param aClassNamePrefix the a class name prefix
	 *
	 * @return the c run area
	 */
	public CRunArea createCloseAreaWithoutCloseBtn(IECaseComponent aCaseComponent, Component aParent, String aClassNamePrefix) {
		CRunArea lCloseArea = new CRunArea();
		lCloseArea.setZclass(aClassNamePrefix + "_close_area");
		aParent.appendChild(lCloseArea);
		return lCloseArea;
	}

	/**
	 * Creates close area and shows close button within it.
	 *
	 * @param aCaseComponent the a case component
	 * @param aParent the ZK parent
	 * @param aClassNamePrefix the a class name prefix
	 * @param aIdPrefix the a id prefix
	 * @param aActionStatus the a ActionStatus
	 *
	 * @return the c run area
	 */
	public CRunArea createCloseArea(IECaseComponent aCaseComponent, Component aParent, String aClassNamePrefix, String aIdPrefix, Object aActionStatus) {
		return createCloseArea(aCaseComponent, aParent, aClassNamePrefix, aIdPrefix, aActionStatus, CDesktopComponents.vView().getLabel("close"));
	}

	/**
	 * Creates close area and shows close button within it.
	 *
	 * @param aCaseComponent the a case component
	 * @param aParent the ZK parent
	 * @param aClassNamePrefix the a class name prefix
	 * @param aIdPrefix the a id prefix
	 * @param aActionStatus the a ActionStatus
	 * @param aLabel the a label
	 *
	 * @return the c run area
	 */
	public CRunArea createCloseArea(IECaseComponent aCaseComponent, Component aParent, String aClassNamePrefix, String aIdPrefix, Object aActionStatus, String aLabel) {
		CRunArea lCloseArea = new CRunArea();
		lCloseArea.setZclass(aClassNamePrefix + "_close_area");
		CRunCloseBtn lButton = null;
		if (aIdPrefix.equals("runTablet") || aIdPrefix.equals("runDashboard"))
			lButton = createNewTabletCloseButton(aIdPrefix + "CloseBtn",
					"active","endComponent", aActionStatus, aLabel);
		else
			lButton = createNewTabletAppCloseButton(aIdPrefix + "CloseBtn",
					"active","endComponent", aActionStatus, aLabel);
		lButton.registerObserver(CControl.runWnd);
		lCloseArea.appendChild(lButton);
		aParent.appendChild(lCloseArea);
		return lCloseArea;
	}

	/**
	 * Creates report btn and shows report button within it.
	 *
	 * @param aParent the ZK parent
	 * @param aClassNamePrefix the a class name prefix
	 * @param aIdPrefix the a id prefix
	 * @param aActionStatus the a ActionStatus
	 * @param aLabel the a label
	 * @param aNotifyId the a NotifyId
	 *
	 * @return the c run hover btn
	 */
	public CRunArea createReportBtn(Component aParent, String aClassNamePrefix, String aIdPrefix, Object aActionStatus, String aLabel, String aNotifyId) {
		CRunArea lReportArea = new CRunArea();
		lReportArea.setZclass(aClassNamePrefix + "_report_area");
		CRunHoverBtn lButton = new CRunHoverBtn("", "active", "reportComponent", aActionStatus, "", aLabel, "");
		lButton.setSclass(aClassNamePrefix + "_report_btn");
		lButton.setCanHaveStatusSelected(false);
		lButton.registerObserver(aNotifyId);
		lReportArea.appendChild(lButton);
		aParent.appendChild(lReportArea);
		return lReportArea;
	}

	/**
	 * Creates new tablet close button.
	 *
	 * @param aId the a id
	 * @param aStatus the a status
	 * @param aAction the a action
	 * @param aActionStatus the a ActionStatus
	 * @param aLabel the a label
	 *
	 * @return the c run area
	 */
	public CRunCloseBtn createNewTabletCloseButton(String aId, String aStatus, String aAction, Object aActionStatus, String aLabel) {
		CRunCloseBtn lButton = new CRunTabletCloseBtnOunl(aId, aStatus, aAction, aActionStatus, "", aLabel, "");
		lButton.setCanHaveStatusSelected(false);
		return lButton;
	}

	/**
	 * Creates new tablet app close button.
	 *
	 * @param aId the a id
	 * @param aStatus the a status
	 * @param aAction the a action
	 * @param aActionStatus the a ActionStatus
	 * @param aLabel the a label
	 *
	 * @return the c run area
	 */
	public CRunCloseBtn createNewTabletAppCloseButton(String aId, String aStatus, String aAction, Object aActionStatus, String aLabel) {
		CRunCloseBtn lButton = new CRunTabletAppCloseBtnOunl(aId, aStatus, aAction, aActionStatus, "", aLabel, "");
		lButton.setCanHaveStatusSelected(false);
		return lButton;
	}

	/**
	 * Creates new tablet app close button.
	 *
	 * @param aId the a id
	 * @param aStatus the a status
	 * @param aAction the a action
	 * @param aActionStatus the a ActionStatus
	 * @param aLabel the a label
	 *
	 * @return the c run area
	 */
	public CRunCloseBtn createNewTabletAppCloseTabletButton(String aId, String aStatus, String aAction, Object aActionStatus, String aLabel) {
		CRunCloseBtn lButton = new CRunTabletAppCloseTabletBtnOunl(aId, aStatus, aAction, aActionStatus, "", aLabel, "");
		lButton.setCanHaveStatusSelected(false);
		return lButton;
	}

}
