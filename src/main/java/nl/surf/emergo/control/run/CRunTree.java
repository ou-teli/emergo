/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Treechildren;
import org.zkoss.zul.Treecol;
import org.zkoss.zul.Treecols;
import org.zkoss.zul.Treeitem;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CTree;
import nl.surf.emergo.control.def.CDefTreecol;
import nl.surf.emergo.control.def.CDefTreecols;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunTree. Ancestor of all tree components within the player.
 */
public class CRunTree extends CTree {

	private static final long serialVersionUID = -1562013986218640975L;

	/** The run wnd. */
	protected CRunWnd runWnd = (CRunWnd) CDesktopComponents.vView().getComponent(CControl.runWnd);

	/** The case component which content is rendered as a tree. */
	protected IECaseComponent caseComponent = null;

	/** The tagopenednames, comma separated, only content tags with these names are shown. */
	protected String tagopenednames = "";

	/** The selectedtreeitem. */
	protected Treeitem selectedtreeitem = null;

	/** The selectedtagid, corresponds to the selectedtreeitem. */
	protected String selectedtagid = "";

	/** The run component, the ZK parent container. */
	protected CRunComponent runComponent = null;

	/** The run status type, see AppConstants. */
	protected String runStatusType = AppConstants.statusTypeRunGroup;

	/** The selectedtreeitem. */
	protected IXMLTag rootTag = null;

	/** The change Note context. If true, when tree item is clicked, context of note is set to tree item tag. */
	protected boolean changeNoteContext = true;

	/** The previous treeitem. */
	protected Component previousTreeitem = null;

	public String getClassName() {
		return CDesktopComponents.vView().getRunClassName(className);
	}

	/**
	 * Gets the run status type.
	 *
	 * @return the run status type
	 */
	public String getRunStatusType() {
		if (runComponent != null)
			return runComponent.getRunStatusType();
		return runStatusType;
	}

	/**
	 * Sets the run status type.
	 *
	 * @param aRunStatusType the new run status type
	 */
	public void setRunStatusType(String aRunStatusType) {
		runStatusType = aRunStatusType;
	}

	/**
	 * Gets the root tag.
	 *
	 * @return the root tag
	 */
	public IXMLTag getRootTag() {
		return rootTag;
	}

	/**
	 * Gets the run component.
	 *
	 * @return the run component
	 */
	public CRunComponent getRunComponent() {
		return runComponent;
	}

	public boolean isChangeNoteContext() {
		return changeNoteContext;
	}

	public void setChangeNoteContext(boolean aChangeNoteContext) {
		changeNoteContext = aChangeNoteContext;
	}

	/**
	 * Instantiates a new c run tree.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the a case component
	 * @param aRunComponent the a run component
	 */
	public CRunTree(String aId, IECaseComponent aCaseComponent, CRunComponent aRunComponent) {
		setId(aId);
		caseComponent = aCaseComponent;
		runComponent = aRunComponent;
	}

	/**
	 * Updates tree.
	 */
	public void update() {
		update(null);
	}

	/**
	 * Updates tree.
	 *
	 * @param aTag the tag to update
	 */
	public void update(IXMLTag aTag) {
		update(aTag, false);
	}

	/**
	 * Updates tree with a tag referencing another component.
	 *
	 * @param aTag the tag to update
	 */
	public void updateReference(IXMLTag aTag) {
		update(aTag, true);
	}

	/**
	 * Updates tree. If aTag is equal to null, the complete content of case component is rendered.
	 * If aTag is set, it is possible to render a (sub)part of the content. For instance questions
	 * belonging to a particular conversation.
	 *
	 * @param aTag the tag to update
	 * @param aReference true if aTag references another component
	 */
	private void update(IXMLTag aTag, boolean aReference) {
		getChildren().clear();
		invalidate();

		appendTreecols(this);

		CRunComponentHelper cComponent = getRunComponentHelper();
		IXMLTag lRoot = cComponent.getXmlDataPlusStatusTree();
		if (lRoot != null) {
			cComponent.setItemAttributes(this, lRoot.getChild(AppConstants.contentElement));
		}
		if (aTag == null) {
			aTag = lRoot;
		} else {
			if (!aReference) {
				aTag = CDesktopComponents.sSpring().xmlManager.getTagById(lRoot, aTag.getAttribute(AppConstants.keyId));
				if (aTag == null)
					aTag = lRoot;
			}
		}
		cComponent.xmlContentToContentItems(aTag, this);
		rootTag = aTag;
	}

	/**
	 * Updates existing status with external tags. Gets all update tags for current user and
	 * processes them.
	 */
	public void updateWithExternalTags() {
		CRunComponentHelper lTreeHelper = getRunComponentHelper();
		lTreeHelper.updateWithExternalTags();
	}

	/**
	 * Appends treecols.
	 *
	 * @param aParent the a parent
	 */
	protected void appendTreecols(Component aParent) {
		Treecols lTreecols = new CDefTreecols();
		Treecol lTreecol = new CDefTreecol();
		lTreecols.appendChild(lTreecol);
		aParent.appendChild(lTreecols);
	}

	/**
	 * Default no dropping possible within player.
	 *
	 * @param aDragged the dragged contentitem
	 * @param aDropped the contentitem dropped on
	 * @param aCopy the copy state
	 */
	@Override
	public void onDrop(Component aDragged, Component aDropped, boolean aCopy) {
	}

	/**
	 * On open save outfolded status.
	 *
	 * @param aContentItem the contentitem
	 * @param aOpen the open state
	 */
	@Override
	public void onOpen(Component aContentItem, boolean aOpen) {
		updateWithExternalTags();
		if (aContentItem == null) {
			return;
		}
		if (aContentItem.isVisible()) {
			IXMLTag tag = (IXMLTag) aContentItem.getAttribute("item");
			if (tag != null && !tag.getAttribute(AppConstants.keyId).equals("")) {
				String lStatus = "";
				if (aOpen)
					lStatus = AppConstants.statusValueTrue;
				else
					lStatus = AppConstants.statusValueFalse;
				aContentItem = setRunTagStatus(aContentItem, tag, AppConstants.statusKeyOutfolded, lStatus, "", true);
//				add something for shared component
			}
		}
		else {
			//TODO give appropriate alert why action is canceled.
		}
	}

	/**
	 * Gets the run component helper.
	 *
	 * @return the run component helper
	 */
	public CRunComponentHelper getRunComponentHelper() {
		return new CRunComponentHelper(this, tagopenednames, caseComponent, runComponent);
	}

	/**
	 * Sets treeitem status. Selected and opened are set true for corresponding tag.
	 *
	 * @param aTreeitem the a treeitem
	 *
	 * @return the treeitem
	 */
	protected Treeitem setTreeitemStatus(Treeitem aTreeitem) {
		Treeitem lTreeitem = aTreeitem;
		IXMLTag tag = getContentItemTag(lTreeitem);
		if (tag != null && !tag.getAttribute(AppConstants.keyId).equals("")) {
			CRunComponentHelper lTreeHelper = getRunComponentHelper();
			lTreeHelper.updateWithExternalTags();
			lTreeitem = getContentItem(tag.getAttribute(AppConstants.keyId));
			//NOTE only if accessible
			if (lTreeHelper.isAccessible(tag)) {
				lTreeitem = setRunTagStatus(lTreeitem, tag,
						AppConstants.statusKeySelected, AppConstants.statusValueTrue, tagopenednames, true);
				lTreeHelper.addExternalUpdateStatusTag(tag, AppConstants.statusKeySelected, AppConstants.statusValueTrue);
				lTreeitem = setRunTagStatus(lTreeitem, tag,
						AppConstants.statusKeyOpened, AppConstants.statusValueTrue, tagopenednames, true);
				lTreeHelper.addExternalUpdateStatusTag(tag, AppConstants.statusKeyOpened, AppConstants.statusValueTrue);
				if (runWnd != null && isChangeNoteContext()) {
					runWnd.setNoteTag(caseComponent, tag);
				}
			}
		}
		return lTreeitem;
	}

	/**
	 * Does treeitem action. To be overriden by children.
	 * Added for polymorphism.
	 *
	 * @param aTreeitem the a treeitem
	 */
	public void doTreeitemAction(Treeitem aTreeitem) {
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.CTree#contentItemClicked(org.zkoss.zk.ui.Component)
	 */
	@Override
	public Component contentItemClicked(Component aClickedComponent) {
		updateWithExternalTags();
		Treeitem lTreeitem = getContentItemClicked(aClickedComponent);
		if (aClickedComponent.isVisible()) {
			setTreeitemStatus(lTreeitem);
			doTreeitemAction(lTreeitem);
			scrollIntoView(lTreeitem);
		}
		else {
			//TODO give appropriate alert why action is canceled.
		}
		return lTreeitem;
	}

	/**
	 * Sets run group tag status and saves it in db.
	 *
	 * @param aTreeitem the a treeitem
	 * @param aTag the a tag
	 * @param aKey the a key
	 * @param aValue the a value
	 * @param aTagNames the a tag names, comma separated, only content tags with these names are shown
	 * @param aSaveInDb the a save in db
	 *
	 * @return the treeitem
	 */
	public Treeitem setRunTagStatus(Component aTreeitem, IXMLTag aTag,
			String aKey, String aValue, String aTagNames, boolean aSaveInDb) {
		// set RunGroupTagstatus of tag in db
		CDesktopComponents.sSpring().setRunTagStatus(caseComponent, aTag, aKey, aValue, true, getRunStatusType(), aSaveInDb, false);
		String lTagNames = "," + aTagNames + ",";
		boolean lNameInTagNames = ((aTagNames.equals("")) || (lTagNames.indexOf("," + aTag.getName() + ",") >= 0));
		if (lNameInTagNames) {
			// replace treeitem to show new status
			return reRenderTreeitem((Treeitem)aTreeitem,aTag,false);
		}
		return null;
	}

	/**
	 * Rerender same treeitem.
	 *
	 * @param aTreeitem the a treeitem
	 * @param aTag the a tag
	 * @param aReRenderChilds the a re render childs, if childs have to be rendered again too or are taken from aTreeitem
	 *
	 * @return the treeitem
	 */
	public Treeitem reRenderTreeitem(Treeitem aTreeitem, IXMLTag aTag, boolean aReRenderChilds) {
		if (aTreeitem == null)
			return null;
		CRunComponentHelper cComponent = getRunComponentHelper();
		IXMLTag lRoot = cComponent.getXmlDataPlusStatusTree();
		cComponent.setItemAttributes(this, lRoot.getChild(AppConstants.contentElement));
		if (aTag == null) {
			aTag = lRoot;
		} else {
			aTag = CDesktopComponents.sSpring().xmlManager.getTagById(lRoot, aTag.getAttribute(AppConstants.keyId));
			if (aTag == null)
				aTag = lRoot;
		}
		if (cComponent.isPresent (aTag)) {
			aTreeitem = cComponent.reRenderTreeitem(aTreeitem, aTag);
			if (aReRenderChilds) {
				// Remove existing treechildren and rerender
				Treechildren lTreechildren = aTreeitem.getTreechildren();
				if (lTreechildren != null)
					lTreechildren.detach();
				cComponent.xmlChildsToContentItems(aTag.getChildTags(AppConstants.defValueNode), aTreeitem, null);
			}
			return aTreeitem;
		} else {
			if (aTreeitem.getParent() != null)
				// possibly already detached...
				aTreeitem.detach();
			return null;
		}
	}

	/**
	 * Adds treeitem to tree.
	 *
	 * @param aParentTag the a parent tag
	 * @param aTag the a tag
	 *
	 * @return the treeitem
	 */
	public Treeitem addTreeitem(IXMLTag aParentTag,IXMLTag aTag) {
		CRunComponentHelper cComponent = getRunComponentHelper();
		Treeitem lParent = getContentItem(aParentTag.getAttribute(AppConstants.keyId));
		Treeitem lNewTreeitem = cComponent.renderTreeitem(lParent, null, aTag);
		return lNewTreeitem;
	}

	/**
	 * Gets selected tree tag id.
	 *
	 * @return the tag id
	 */
	protected String getSelectedTagId() {
		return CDesktopComponents.sSpring().getCurrentRunComponentStatus(caseComponent, AppConstants.statusKeySelectedTagId, AppConstants.statusTypeRunGroup);
	}
	
	/**
	 * Saves selected tree tag id.
	 *
	 * @param aTag the a tag
	 */
	protected void saveSelectedTagId(IXMLTag aTag) {
		if (aTag != null) {
			CDesktopComponents.sSpring().setRunComponentStatus(caseComponent,
					AppConstants.statusKeySelectedTagId, aTag.getAttribute(AppConstants.keyId), false, AppConstants.statusTypeRunGroup, true);
		}
	}
	
	/**
	 * Restores selected tree tag id.
	 * Uses runMainTimer because Clients.scrollIntoView has to be called and this function only works if ZK is ready rendering.
	 */
	public void restoreSelectedTagId() {
		runWnd.runMainTimer.setRunTreeForScrollIntoView(this);
	}
	
	/**
	 * Scroll ZK component into view. Is called by runMainTimer.
	 * Posts event onEmergoScrollIntoView to runWnd, see run.zul for handling the event.
	 */
	public void scrollIntoView() {
		selectedtagid = CDesktopComponents.sSpring().getCurrentRunComponentStatus(caseComponent, AppConstants.statusKeySelectedTagId, AppConstants.statusTypeRunGroup);
		Component lTreeitem = getContentItem(selectedtagid);
		if (lTreeitem != null) {
			Component lPreviousOrNextTreeitem = getPreviousOrNextTreeitem(lTreeitem,
					lTreeitem.getParent().getChildren().indexOf(previousTreeitem) <= lTreeitem.getParent().getChildren().indexOf(lTreeitem));
			if (lPreviousOrNextTreeitem == null) {
				lPreviousOrNextTreeitem = lTreeitem; 
			}
			Events.postEvent("onEmergoScrollIntoView", runWnd, lPreviousOrNextTreeitem);
			previousTreeitem = lTreeitem;
		}
	}
	
	/**
	 * Scroll into view.
	 *
	 * @param aTreeitem the a treeitem
	 */
	protected void scrollIntoView(Component aTreeitem) {
		Component lPreviousOrNextTreeitem = getPreviousOrNextTreeitem(aTreeitem,
				aTreeitem.getParent().getChildren().indexOf(previousTreeitem) <= aTreeitem.getParent().getChildren().indexOf(aTreeitem));
		if (lPreviousOrNextTreeitem != null) {
			runWnd.emergoScrollIntoView(lPreviousOrNextTreeitem, false);
		}
		previousTreeitem = aTreeitem;
	}

	/**
	 * Gets previous or next treeitem.
	 *
	 * @param aTreeitem the a treeitem
	 * @param aNext the a next
	 *
	 * @return the treeitem or null
	 */
	public Component getPreviousOrNextTreeitem(Component aTreeitem, boolean aNext) {
		if (aTreeitem == null) {
			return null;
		}
		if (aNext) {
			if (aTreeitem.getNextSibling() != null) {
				return aTreeitem.getNextSibling();
			}
		}
		else {
			if (aTreeitem.getPreviousSibling() != null) {
				return aTreeitem.getPreviousSibling();
			}
		}
		return null;
	}
	
}
