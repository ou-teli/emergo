/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Tree;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CIObserved;
import nl.surf.emergo.control.run.CRunArea;
import nl.surf.emergo.control.run.CRunHbox;
import nl.surf.emergo.control.run.CRunReferences;
import nl.surf.emergo.control.run.CRunTree;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;

/**
 * The Class CRunReferencesOunl is used to show references within the run view area of the emergo player.
 */
public class CRunReferencesOunl extends CRunReferences {

	private static final long serialVersionUID = -5700427918135463132L;

	/**
	 * Instantiates a new c run references.
	 */
	public CRunReferencesOunl() {
		super();
	}

	/**
	 * Instantiates a new c run references.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the references case component
	 */
	public CRunReferencesOunl(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
	}

	/**
	 * Creates new content component, the references tree.
	 *
	 * @return the component
	 */
	@Override
	protected Component newContentComponent() {
		Tree tree = (Tree)super.newContentComponent();
		// change rows
		tree.setRows(18);
		return tree;
	}

	protected void getReferencesData(List<IXMLTag> aNodeTags, List<String> aTitles, int aIndent) {
		aIndent ++;
		for (IXMLTag lNodeTag : aNodeTags) {
			String lTitle = CDesktopComponents.sSpring().unescapeXML(lNodeTag.getChildValue("name")).replaceAll("\n", "<br/>");
			for (int i=1;i<aIndent;i++) {
				lTitle = "." + lTitle;
			}
			aTitles.add(lTitle);
			getReferencesData(lNodeTag.getChildTags(AppConstants.defValueNode), aTitles, aIndent);
		}
	}
	
	public void reportReferencesData() {
		Object[][] data = null;
		
		List<String> lTitles = new ArrayList<String>();
		IXMLTag lRootTag = CDesktopComponents.sSpring().getXmlDataPlusRunStatusTree(caseComponent, getRunStatusType());
		if (lRootTag != null) {
			IXMLTag lContentTag = lRootTag.getChild(AppConstants.contentElement);
			if (lContentTag != null) {
				CRunTree lTree = (CRunTree)CDesktopComponents.vView().getComponent("runReferencesTree");
				if (lTree != null) {
					List<IXMLTag> lNodeTags = lTree.getRunComponentHelper().getVisibleXmlNodeTags(lContentTag);
					getReferencesData(lNodeTags, lTitles, 0);
				}
			}
		}
		data = new Object[lTitles.size()][1];
		for (int i=0;i<lTitles.size();i++) {
			Object object = lTitles.get(i);
			if (object instanceof String) {
				object = ((String)object).replaceAll("<br/>", "\n");
			}
			data[i][0] = object; 
		}
		CRunReferencesJRDataSource dataSource = new CRunReferencesJRDataSource();
		dataSource.setData(data);
		
		String lTitle = CDesktopComponents.sSpring().getCaseComponentRoleName("", caseComponent.getName()) + " " + CDesktopComponents.vView().getLabel("run_report.title.last");
		getDesktop().setAttribute("run_reporttitle", lTitle);
		String lUrl = "/" + CDesktopComponents.sSpring().getSCaseSkinHelper().getCaseSkinPath(caseComponent.getECase()) + VView.v_run_references_report_file;
		getDesktop().setAttribute("run_reportjasperfile", lUrl);
		getDesktop().setAttribute("run_reportdatasource", dataSource);

		CDesktopComponents.vView().modalPopupWithoutWaiting(VView.v_run_jasperreport, null, null, "center");
	}
	
	/**
	 * Creates title area and shows name of case component within it.
	 * And adds close button
	 *
	 * @param aParent the ZK parent
	 *
	 * @return the c run area
	 */
	@Override
	protected CRunArea createTitleArea(Component aParent) {
		CRunHbox lHbox = new CRunHbox();
		aParent.appendChild(lHbox);
		CRunComponentDecoratorOunl decorator = createDecorator();
		CRunArea lTitleArea = decorator.createTitleArea(caseComponent, lHbox, getClassName());
		decorator.createCloseArea(caseComponent, lHbox, getClassName(), getId(),this);
		String lCaption = CDesktopComponents.sSpring().getCaseComponentRoleName("", caseComponent.getName()) + " " + CDesktopComponents.vView().getLabel("run_report.button.last");
		decorator.createReportBtn(lHbox, getClassName(), getId(), this, lCaption, getId());
		return lTitleArea;
	}

	/**
	 * Creates decorator.
	 *
	 * @return the decorator
	 */
	protected CRunComponentDecoratorOunl createDecorator() {
		return new CRunComponentDecoratorOunl();
	}

	/**
	 * Creates buttons area and adds close button.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run hbox
	 */
	@Override
	protected CRunHbox createButtonsArea(Component aParent) {
		// NOTE Don't create buttons area
		return null;
	}

	/**
	 * Is called if report has to be generated.
	 *
	 * @param aObserved the observed object, it should have interface CIObserved implemented
	 * @param aAction the action
	 * @param aStatus the status object, can be String or other class
	 */
	@Override
	public void observedNotify(CIObserved aObserved, String aAction, Object aStatus) {
		if (aAction.equals("reportComponent")) {
			reportReferencesData();
		}
	}

}
