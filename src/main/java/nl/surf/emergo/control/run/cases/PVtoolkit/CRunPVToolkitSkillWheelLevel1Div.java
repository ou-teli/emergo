/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Vbox;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.def.CDefHelper;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefCheckbox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefCombobox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefComboitem;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefHbox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefVbox;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTag;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitFeedbackTipOrTop;
import nl.surf.emergo.control.run.cases.PVtoolkit.util.CRunPVToolkitCustomAttributeHelper;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;

public class CRunPVToolkitSkillWheelLevel1Div extends CDefDiv {

	private static final long serialVersionUID = -5038119794106215616L;

	public String zulfilepath = ((CRunPVToolkitInitBox)CDesktopComponents.vView().getComponent("PV-toolkit_initBox")).getZulfilepath();
	
	protected CRunPVToolkit pvToolkit;

	protected IECaseComponent caseComponent;
	protected IXMLTag skillTag;
	
	protected List<IXMLTag> _sharedPracticeTagsPerFeedbackStep = null;
	protected List<List<IXMLTag>> _sharedFeedbackTagsPerFeedbackStep = null;
	public boolean _includeSelfFeedback = false;
	public boolean _onlySelfFeedback = false;
	protected int _numberOfFeedbacks = 0;
	protected int _maxCycleNumber = 0;
	public IERunGroup _actor;
	public boolean _editable;
	public int _cycleNumber = 0;
	protected boolean _showMarks = false;
	protected boolean _compareFeedbackGivers = false;
	protected List<IERunGroupAccount> _compareRunGroupAccounts1;
	protected List<IERunGroupAccount> _compareRunGroupAccounts2;

	protected boolean _compareWithExperts = false;
	protected IXMLTag _skillExampleTag;
	protected List<IXMLTag> _skillExampleExpertTags = null;
	protected IXMLTag _skillExampleFeedbackTag;
	protected List<IXMLTag> _compareExpertTags1;
	protected List<IXMLTag> _compareExpertTags2;
	protected boolean _compareFeedbackTag = true;
	protected boolean _showCycle = true;
	protected boolean _mayFilter = true;
	protected boolean _mayCompare = true;
	protected int _offsetToPreviousCycle = 1;
	
	protected int _numberOfSkillClusters = 0;

	protected int _numberOfPerformanceLevelsToShow = 5;
	protected int[] _performanceLevelValues;
	
	protected double _skillWheelZoomFactor = 1;
	
	protected double _skillWheelRotationDegree = 0;
	
	protected int _skillClusterNumberToFilter = 0;
	
	protected int _presentationLevel = 1;
	
	protected boolean _skillclusterHasScore;
	protected boolean _skillclusterHasTipsTops;
	protected boolean _subskillHasScore;
	protected boolean _subskillHasTipsTops;
	protected boolean _skillHasScore;
	protected boolean _skillHasTipsTops;

	public List<Integer> otherRgaIdsToFilterOn;
	protected List<Integer> initialOtherRgaIdsToFilterOn;
	
	protected Rows filterRows;
	
	protected Rows groupRows1;
	protected Rows groupRows2;

	protected IAccountManager accountManager = (IAccountManager)CDesktopComponents.sSpring().getBean("accountManager");

	protected boolean isPreviewRun = false;

	protected String _stepPrefix = "";
	protected String _idPrefix = "skillWheel";
	protected String _compareIdPrefix = "compareSkillWheel";
	
	protected String _classPrefix = "skillWheelLevel1";
	
	protected String _browseClassPrefix = "skillWheelCycle";
	
	protected boolean _hasBackButton = true;

	protected String _backToComponentId = "";
	protected String _backToComponentInitId = "";
	
	public boolean skillclusterHasScore() {
		return _skillclusterHasScore;
	}
	
	public boolean skillclusterHasTipsTops() {
		return _skillclusterHasTipsTops;
	}
	
	public boolean subskillHasScore() {
		return _subskillHasScore;
	}
	
	public boolean subskillHasTipsTops() {
		return _subskillHasTipsTops;
	}
	
	public boolean skillHasScore() {
		return _skillHasScore;
	}
	
	public boolean skillHasTipsTops() {
		return _skillHasTipsTops;
	}
	
	public void onCreate(CreateEvent aEvent) {
		_compareFeedbackGivers = CRunPVToolkitCustomAttributeHelper.handleCustomAttribute(this, "_compareFeedbackGivers", _compareFeedbackGivers);
		_includeSelfFeedback = CRunPVToolkitCustomAttributeHelper.handleCustomAttribute(this, "_includeSelfFeedback", _includeSelfFeedback);
		_onlySelfFeedback = CRunPVToolkitCustomAttributeHelper.handleCustomAttribute(this, "_onlySelfFeedback", _onlySelfFeedback);
		_compareWithExperts = CRunPVToolkitCustomAttributeHelper.handleCustomAttribute(this, "_compareWithExperts", _compareWithExperts);
		_hasBackButton = CRunPVToolkitCustomAttributeHelper.handleCustomAttribute(this, "_hasBackButton", _hasBackButton);
		_showCycle = CRunPVToolkitCustomAttributeHelper.handleCustomAttribute(this, "_showCycle", _showCycle);
		_mayFilter = CRunPVToolkitCustomAttributeHelper.handleCustomAttribute(this, "_mayFilter", _mayFilter);
	
		_stepPrefix = CRunPVToolkitCustomAttributeHelper.handleCustomAttribute(this, "_stepPrefix", _stepPrefix);
		_idPrefix = CRunPVToolkitCustomAttributeHelper.handleCustomAttribute(this, "_idPrefix", _idPrefix);
		_backToComponentId = CRunPVToolkitCustomAttributeHelper.handleCustomAttribute(this, "_backToComponentId", _backToComponentId);
		_backToComponentInitId = CRunPVToolkitCustomAttributeHelper.handleCustomAttribute(this, "_backToComponentInitId", _backToComponentInitId);
		_compareIdPrefix = CRunPVToolkitCustomAttributeHelper.handleCustomAttribute(this, "_compareIdPrefix", _compareIdPrefix);
	}
	
	protected List <List <IXMLTag>> getFilteredSharedFeedbackSteps() {
		List <List <IXMLTag>> lSharedFeedbackTagsPerFeedbackStep = new ArrayList<>();
		int lCurrentStepNumber = pvToolkit.getStepOrderNumber(pvToolkit.getCurrentStepType());
		boolean lIncludeCurrentPeerFeedback = (pvToolkit.hasTeacherRole || pvToolkit.hasPassiveTeacherRole || (lCurrentStepNumber >= pvToolkit.getStepOrderNumber(CRunPVToolkit.viewFeedbackStepType)));
		for (int lCN=1; lCN < _maxCycleNumber; lCN++) {
			List <List <IXMLTag>> lFBTags = pvToolkit.getSharedFeedbackTagsPerFeedbackStep(_actor, _includeSelfFeedback, getOtherRolesToFilterOn(lCN), otherRgaIdsToFilterOn);
			lSharedFeedbackTagsPerFeedbackStep.add(lFBTags.get(lCN - 1));
		}
		if (_maxCycleNumber > 0) {
			List <Integer> lRgaIdsToFilterOn = new ArrayList<Integer>();
			if (!lIncludeCurrentPeerFeedback) {
				lRgaIdsToFilterOn.add(pvToolkit.getActiveRunGroupAccount(_actor.getRugId()).getRgaId());
			} else {
				lRgaIdsToFilterOn = otherRgaIdsToFilterOn;
			}
			List <List <IXMLTag>> lFBTags = pvToolkit.getSharedFeedbackTagsPerFeedbackStep(_actor, _includeSelfFeedback, getOtherRolesToFilterOn(_maxCycleNumber), lRgaIdsToFilterOn);
			lSharedFeedbackTagsPerFeedbackStep.add(lFBTags.get(_maxCycleNumber - 1));
		}
		return lSharedFeedbackTagsPerFeedbackStep;
	}
	
	public void init(IERunGroup actor, boolean editable, int cycleNumber, int skillClusterNumber) {
		
		int originalCycleNumber = _cycleNumber; 
		
		if (_cycleNumber > 0 && (_presentationLevel == 1 || _presentationLevel == 2)) {
			//NOTE keep _cycleNumber during session, so ignore parameter cycleNumber, if presentation level is 1 or 2.
			//This means that level 3 gets level of level 2 when zooming in
			cycleNumber = _cycleNumber;
		}
		
		//NOTE always initialize, because recordings and feedback my be added during a session
		setClass(_classPrefix);
		
		_actor = actor;
		_editable = editable;

		pvToolkit = (CRunPVToolkit)CDesktopComponents.vView().getComponent("pvToolkit");
		
		caseComponent = pvToolkit.getCurrentRubricsCaseComponent();
		skillTag = pvToolkit.getCurrentSkillTag();
		if (skillTag == null) {
			return;
		}
		
		_numberOfSkillClusters = pvToolkit.getNumberOfSkillClusters(pvToolkit.getCurrentRubricsCaseComponent(), skillTag);

		_numberOfPerformanceLevelsToShow = pvToolkit.getNumberOfPerformanceLevelsToShow();
		_performanceLevelValues = pvToolkit.getPerformanceLevelValues();
		
		pvToolkit.setMemoryCaching(true);
		
		String feedbackLevelOfScores = pvToolkit.getFeedbackLevelOfScores(pvToolkit.getCurrentMethodTag(), pvToolkit.getCurrentCycleTag());
		String feedbackLevelOfTipsAndTops = pvToolkit.getFeedbackLevelOfTipsAndTops(pvToolkit.getCurrentMethodTag(), pvToolkit.getCurrentCycleTag());
		boolean mayScoreSkill = pvToolkit.getMayScoreSkill(pvToolkit.getCurrentMethodTag(), pvToolkit.getCurrentCycleTag());
		boolean mayTipsAndTopsSkill = pvToolkit.getMayTipsAndTopsSkill(pvToolkit.getCurrentMethodTag(), pvToolkit.getCurrentCycleTag());
		
		_skillclusterHasScore = feedbackLevelOfScores.equals("skillcluster");
		_skillclusterHasTipsTops = feedbackLevelOfTipsAndTops.equals("skillcluster");
		_subskillHasScore = feedbackLevelOfScores.equals("subskill");
		_subskillHasTipsTops = feedbackLevelOfTipsAndTops.equals("subskill");
		_skillHasScore = feedbackLevelOfScores.equals("skill") || mayScoreSkill;
		_skillHasTipsTops = feedbackLevelOfTipsAndTops.equals("skill") || mayTipsAndTopsSkill;

		if (!_compareWithExperts) {
			_sharedPracticeTagsPerFeedbackStep = pvToolkit.getSharedPracticeTagsPerFeedbackStep(_actor);
		
			//_sharedFeedbackTagsPerFeedbackStep = pvToolkit.getSharedFeedbackTagsPerFeedbackStep(_actor, _includeSelfFeedback, getOtherRolesToFilterOn(cycleNumber), otherRgaIdsToFilterOn);
			_sharedFeedbackTagsPerFeedbackStep = getFilteredSharedFeedbackSteps();

			//_maxCycleNumber = Math.min(_sharedFeedbackTagsPerFeedbackStep.size(), pvToolkit.getCurrentCycleNumber());
			_maxCycleNumber = pvToolkit.getCurrentCycleNumber();
		}
		
		_compareExpertTags1 = null;
		_compareExpertTags2 = null;
		_compareFeedbackTag = true;
		if (_compareWithExperts) {
			_skillExampleTag = (IXMLTag)CDesktopComponents.cControl().getRunSessAttr("skillExampleTag");
			if (_skillExampleTag == null) {
				return;
			}
			//NOTE there may be no expert tags. Then they will not be shown
			_skillExampleExpertTags = pvToolkit.getPresentChildTags(new CRunPVToolkitCacAndTag(pvToolkit.getCurrentRubricsCaseComponent(), _skillExampleTag), "videoskillexampleexpert");
			_skillExampleFeedbackTag = pvToolkit.getPrepareFeedbackTag(_actor, _skillExampleTag);
			if (_skillExampleFeedbackTag == null) {
				return;
			}
			//TODO
			_compareExpertTags1 = null;
			_compareExpertTags2 = null;
			_compareFeedbackTag = true;
		}
		if (_compareFeedbackGivers && _maxCycleNumber > 0) {
			initCompareRunGroupAccounts();
		} else {
			if (!_compareFeedbackGivers) {
				initFilterRunGroupAccounts();
			}
		}

		_skillClusterNumberToFilter = skillClusterNumber;

		update(cycleNumber);
		
		setVisible(true);

		if (_compareFeedbackGivers && _maxCycleNumber > 0 && originalCycleNumber == 0) {
			//NOTE show settings in groups div if feedback givers are compared and compare option is used the first time during the session
			Events.echoEvent("onShowGroupsDiv", this, null);
		}
	}
	
	/* START EXPERIMENTAL CODE */
	public List<String> getOtherRolesToFilterOn(int cycleNumber) {
		List<String> otherRolesToFilterOnPerCycle = null; 
		if (pvToolkit.excludeStudentAssistantsFeedback(_actor)) {
			otherRolesToFilterOnPerCycle = new ArrayList<String>();
			otherRolesToFilterOnPerCycle.add(CRunPVToolkit.peerGroupStudentRole);
			if (cycleNumber > 0 && (!Boolean.TRUE.equals(pvToolkit.getHideTeacherFeedbackPerCycle(_actor).get(cycleNumber - 1)))) {
				otherRolesToFilterOnPerCycle.add(CRunPVToolkit.peerGroupTeacherRole);
			}
			otherRolesToFilterOnPerCycle.add(CRunPVToolkit.peerGroupPeerStudentRole);
		}
		else if (cycleNumber > 0 && Boolean.TRUE.equals(pvToolkit.getHideTeacherFeedbackPerCycle(_actor).get(cycleNumber - 1))) {
			//add student roles
			otherRolesToFilterOnPerCycle = new ArrayList<String>();
			otherRolesToFilterOnPerCycle.add(CRunPVToolkit.peerGroupStudentRole);
			otherRolesToFilterOnPerCycle.add(CRunPVToolkit.peerGroupPeerStudentRole);
			otherRolesToFilterOnPerCycle.add(CRunPVToolkit.peerGroupStudentAssistantRole);
		}
		//NOTE if otherRolesToFilterOnPerCycle is null, no filtering is applied so all roles are available
		return otherRolesToFilterOnPerCycle;
	}
	/* END EXPERIMENTAL CODE */
	
	public void initCompareRunGroupAccounts() {
		//NOTE default compare self with all feedback givers.
		//keep compare groups during session
		if (_compareRunGroupAccounts1 == null) {
			//get self
			_compareRunGroupAccounts1 = pvToolkit.getPeerRungroupAccounts(_actor, true, null, null);
		}
		if (_compareRunGroupAccounts2 == null) {
			//get all feedbackgivers
			_compareRunGroupAccounts2 = pvToolkit.getPeerRungroupAccounts(_actor, false, null, null);
		}
	}
	
	public void initFilterRunGroupAccounts() {
		//NOTE default all other peers in current peergroup included in filter 
		if (otherRgaIdsToFilterOn == null) {
			otherRgaIdsToFilterOn = new ArrayList<Integer>();
			//get all feedbackgivers
			for (IERunGroupAccount lRunGroupAccount : pvToolkit.getRunGroupAccounts(pvToolkit.getPeerRungroups(_actor, false, null, null, false))) {
				otherRgaIdsToFilterOn.add(lRunGroupAccount.getRgaId());
			}
		}
		initialOtherRgaIdsToFilterOn = new ArrayList<>(otherRgaIdsToFilterOn);
	}
	
	public void onShowGroupsDiv(Event event) {
		showGroupsDiv((CRunPVToolkitSkillWheelLevel1Div)event.getTarget());
	}

	public void browseSkillWheel(String notifyAction) {
		int cycleNumber = -1;
		if (notifyAction.equals("first")) {
			cycleNumber = 1;
		}
		else if (notifyAction.equals("previous")) {
			cycleNumber = _cycleNumber - 1;
		}
		else if (notifyAction.equals("next")) {
			cycleNumber = _cycleNumber + 1;
		}
		else if (notifyAction.equals("last")) {
			cycleNumber = _maxCycleNumber;
		}

		update(cycleNumber);
	};

	public void update(int cycleNumber) {
		getChildren().clear();
		
		_cycleNumber = cycleNumber;
		if (_cycleNumber < 1) {
			_cycleNumber = _maxCycleNumber;
		}
		setEditableAndMarkable();
		
		pvToolkit.setMemoryCaching(true);
		
		//_sharedFeedbackTagsPerFeedbackStep = pvToolkit.getSharedFeedbackTagsPerFeedbackStep(_actor, _includeSelfFeedback, getOtherRolesToFilterOn(_cycleNumber), otherRgaIdsToFilterOn);
		_sharedFeedbackTagsPerFeedbackStep = getFilteredSharedFeedbackSteps();

		isPreviewRun = CDesktopComponents.sSpring().getRun().getStatus() == AppConstants.run_status_test;

		String src = "";
		if (_cycleNumber == _maxCycleNumber) {
			src = zulfilepath + _classPrefix + "_wit.svg";
		}
		if (_cycleNumber < _maxCycleNumber) {
			src = zulfilepath + _classPrefix + "_geel.svg";
		}
		new CRunPVToolkitDefImage(this, 
				new String[]{"class", "src"}, 
				new Object[]{_classPrefix + "Background", src}
		);
		
		if (_presentationLevel == 1) {
			new CRunPVToolkitDefLabel(this, 
					new String[]{"class", "labelKey"}, 
					new Object[]{"font " + _classPrefix + "Title", "PV-toolkit-skillwheel.header"}
			);

			Component btn = new CRunPVToolkitDefButton(this, 
					new String[]{"class", "cLabelKey"}, 
					new Object[]{"font pvtoolkitButton " + _classPrefix + "DetailsButton", "PV-toolkit-dashboard.button.details"}
					);
			addDetailsOnClickEventListener(btn);
		}
		else {
			Div div = new CRunPVToolkitDefDiv(this, 
					new String[]{"class"}, 
					new Object[]{"titleRight"}
			);
			if (!_compareWithExperts) {
				new CRunPVToolkitDefLabel(div, 
						new String[]{"class", "labelKey"}, 
						new Object[]{"font titleRight", "PV-toolkit-skillwheel.header"}
						);
			}
			else {
				new CRunPVToolkitDefLabel(div, 
						new String[]{"class", "labelKey"}, 
						new Object[]{"font titleRight", "PV-toolkit-skillwheel.header.compareWithExperts"}
						);
			}
		}
		
		Div div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "Div"}
		);
		
		if (pvToolkit.hasCycleTags() && !_compareWithExperts) {
			_numberOfFeedbacks = getNumberOfCycleFeedbacks(_cycleNumber, _sharedFeedbackTagsPerFeedbackStep);
			
			showCycle(this, _numberOfFeedbacks);
		}
		
		showSkillWheel(div);
		
		pvToolkit.setMemoryCaching(false);
		
	}
	
	protected void addDetailsOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				CDesktopComponents.vView().getComponent("dashboardDiv").setVisible(false);
				CRunPVToolkitSkillWheelLevel1Div level1Div = (CRunPVToolkitSkillWheelLevel1Div)CDesktopComponents.vView().getComponent(_idPrefix + "Level1Div");
				CRunPVToolkitSkillWheelLevel2Div level2Div = (CRunPVToolkitSkillWheelLevel2Div)CDesktopComponents.vView().getComponent(_idPrefix + "Level2Div");
				// NOTE: set componentTo._cycleNumber, because parameter in init method will be ignored for level 1 and level 2:
				level1Div.initialOtherRgaIdsToFilterOn = new ArrayList<>(level1Div.otherRgaIdsToFilterOn);
				level2Div._cycleNumber = level1Div._cycleNumber;
				level2Div.init(level1Div._actor, level1Div._editable, level1Div._cycleNumber, 0, level1Div.otherRgaIdsToFilterOn);
				CDesktopComponents.vView().getComponent(_idPrefix + "Level2Div").setVisible(true);
			}
		});
	}

	protected void addToRecordingOnClickEventListener(Component component, CRunPVToolkitSkillWheelLevel1Div skillWheelLevelDiv) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				skillWheelLevelDiv.setVisible(false);
				CRunPVToolkitSkillWheelRecordingDiv recordingDiv = (CRunPVToolkitSkillWheelRecordingDiv)CDesktopComponents.vView().getComponent(_idPrefix + "RecordingDiv");
				// NOTE: set recordingDiv._cycleNumber, because parameter in init method will be ignored for level 1 and level 2:
				recordingDiv._cycleNumber = skillWheelLevelDiv._cycleNumber;
				recordingDiv.init(skillWheelLevelDiv._actor, skillWheelLevelDiv._editable, skillWheelLevelDiv._cycleNumber, skillWheelLevelDiv._skillClusterNumberToFilter, skillWheelLevelDiv.getId());
				recordingDiv.setVisible(true);
			}
		});
	}

	protected void addToOverViewTipsTopsOnClickEventListener(Component component, CRunPVToolkitSkillWheelLevel1Div skillWheelLevelDiv) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				skillWheelLevelDiv.setVisible(false);
				CRunPVToolkitSkillWheelOverviewTipsTopsDiv overviewTipsTopsDiv = (CRunPVToolkitSkillWheelOverviewTipsTopsDiv)CDesktopComponents.vView().getComponent(_idPrefix + "OverviewTipsTopsDiv");
				// NOTE: set overviewTipsTopsDiv._cycleNumber, because parameter in init method will be ignored for level 1 and level 2:
				overviewTipsTopsDiv._cycleNumber = skillWheelLevelDiv._cycleNumber;
				overviewTipsTopsDiv.init(skillWheelLevelDiv._actor, skillWheelLevelDiv._editable, skillWheelLevelDiv._cycleNumber, skillWheelLevelDiv._skillClusterNumberToFilter, skillWheelLevelDiv.getId(), otherRgaIdsToFilterOn);
				overviewTipsTopsDiv.setVisible(true);
			}
		});
	}

	protected void addToComparePeersOnClickEventListener(Component component, CRunPVToolkitSkillWheelLevel1Div skillWheelLevelDiv) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				skillWheelLevelDiv.setVisible(false);
				CRunPVToolkitSkillWheelLevel2Div compareSkillWheelLevelDiv = (CRunPVToolkitSkillWheelLevel2Div)CDesktopComponents.vView().getComponent(_compareIdPrefix + "Level2Div");
				// NOTE: set compareSkillWheelLevelDiv._cycleNumber, because parameter in init method will be ignored for level 1 and level 2:
				compareSkillWheelLevelDiv._cycleNumber = skillWheelLevelDiv._cycleNumber;
				compareSkillWheelLevelDiv.init(skillWheelLevelDiv._actor, skillWheelLevelDiv._editable, skillWheelLevelDiv._cycleNumber, 0);
				compareSkillWheelLevelDiv.setVisible(true);
			}
		});
	}
	
	protected void addShowFilterOnClickEventListener(Component component, CRunPVToolkitSkillWheelLevel1Div skillWheelLevelDiv) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				showFilterDiv(skillWheelLevelDiv);
			}
		});
	}

	protected void addShowGroupsOnClickEventListener(Component component, CRunPVToolkitSkillWheelLevel1Div skillWheelLevelDiv) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				showGroupsDiv(skillWheelLevelDiv);
			}
		});
	}

	protected List<IXMLTag> getCycleFeedbackTags() {
		//NOTE get all feedback tags from shared feedback. Only show feedback given.
		List<List<IXMLTag>> sharedFeedbackTagsPerFeedbackStep = pvToolkit.getSharedFeedbackTagsPerFeedbackStep(_actor, true, getOtherRolesToFilterOn(_cycleNumber), null);
		return getCycleFeedbackTags(_cycleNumber, sharedFeedbackTagsPerFeedbackStep);
	}

	protected void showFilterDiv(CRunPVToolkitSkillWheelLevel1Div skillWheelLevelDiv) {
		Div parentDiv = new CRunPVToolkitDefDiv(skillWheelLevelDiv, 
				new String[]{"class"}, 
				new Object[]{"popupDiv"}
		);

		new CRunPVToolkitDefImage(parentDiv, 
				new String[]{"class"}, 
				new Object[]{"popupBackground"}
		);
		
		String classPrefix = "skillWheelFilter";

		Div popupDiv = new CRunPVToolkitDefDiv(parentDiv, 
				new String[]{"class"}, 
				new Object[]{classPrefix}
		);

		new CRunPVToolkitDefImage(popupDiv, 
				new String[]{"class", "src"}, 
				new Object[]{classPrefix + "Background", zulfilepath + "filter-background.svg"}
		);

		Div div = new CRunPVToolkitDefDiv(popupDiv, 
				new String[]{"class"}, 
				new Object[]{classPrefix + "Title"}
		);

		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font " + classPrefix + "Title", "PV-toolkit-skillwheel.button.filter"}
		);

		Image closeImage = new CRunPVToolkitDefImage(popupDiv, 
				new String[]{"class", "src"}, 
				new Object[]{classPrefix + "CloseImage", zulfilepath + "close.svg"}
		);
		addCloseFilterOnClickEventListener(closeImage, parentDiv);

		Div peersDiv = new CRunPVToolkitDefDiv(popupDiv, 
				new String[]{"class"}, 
				new Object[]{classPrefix + "Grid"}
		);
		
		Button resetButton = new CRunPVToolkitDefButton(popupDiv,
				new String[]{"class", "cLabelKey"}, 
				new Object[]{"font pvtoolkitButton " + classPrefix + "ResetButton", "PV-toolkit.reset"}
				);
		addFilterResetButtonOnClickEventListener(resetButton);

		Button saveButton = new CRunPVToolkitDefButton(popupDiv,
				new String[]{"class", "cLabelKey"}, 
				new Object[]{"font pvtoolkitButton " + classPrefix + "SaveButton", "PV-toolkit.save"}
				);
		addFilterSaveButtonOnClickEventListener(saveButton, parentDiv, this);

		filterRows = appendPeersGrid(peersDiv, "PeersGrid");
		fillRows(filterRows, otherRgaIdsToFilterOn, getCycleFeedbackTags(), _idPrefix + "FilterCheckbox_");

		if (_maxCycleNumber > 1 ) {
			new CRunPVToolkitDefLabel(popupDiv,
					new String[]{"class", "value"}, 
					new Object[]{"font " + classPrefix + "CompareCyclesLabel", CDesktopComponents.vView().getLabel("PV-toolkit-skillwheel.filter.label.compareCycles")}
			);

			Combobox combobox = new CRunPVToolkitDefCombobox(popupDiv, 
					new String[]{"id", "width"}, 
					new Object[]{_idPrefix + "CompareCycleCombobox", "50px"}
			);
			combobox.setClass("font " + classPrefix + "CompareCycleCombobox");
			for (int i=1;i<=_maxCycleNumber;i++) {
				if (i == 1 || i < _maxCycleNumber) {
					Comboitem comboitem = new CRunPVToolkitDefComboitem(combobox, 
							new String[]{"value"}, 
							new Object[]{"" + i}
							);
					comboitem.setValue(i);
					comboitem.setClass("font " + classPrefix + "CompareCycleComboitem");
					if (i == _offsetToPreviousCycle) {
						combobox.setSelectedItem(comboitem);
					}
				}
			}
		}

	}

	protected void fillRows(Rows rows, List<Integer> rgaIdsToFilterOn, List<IXMLTag> feedbackTags, String checkboxIdPrefix) {
		appendPeersToGrid(rows, rgaIdsToFilterOn, 1, feedbackTags, checkboxIdPrefix);
	}

	protected void addCloseFilterOnClickEventListener(Component component, Component componentToDetach) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				componentToDetach.detach();
			}
		});
	}

	protected void addFilterResetButtonOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				// NOTE temporary store otherRgaIdsToFilterOn, because it is reset in appendPeersToGrid
				List<Integer> lSaveRgaIdsToFilterOn = otherRgaIdsToFilterOn;
				fillRows(filterRows, null, getCycleFeedbackTags(), _idPrefix + "FilterCheckbox_");
				otherRgaIdsToFilterOn = lSaveRgaIdsToFilterOn;
				_offsetToPreviousCycle = 1;
				Combobox combobox = (Combobox)CDesktopComponents.vView().getComponent(_idPrefix + "CompareCycleCombobox");
				if (combobox != null) {
					//NOTE if only 1 cycle present, compare cycle combobox is not added
					for (Component comboitem : combobox.getChildren()) {
						if ((int)((Comboitem)comboitem).getValue() == _offsetToPreviousCycle) {
							combobox.setSelectedItem((Comboitem)comboitem);
							break;
						}
					}
				}
			}
		});
	}

	protected void addFilterSaveButtonOnClickEventListener(Component component, Component componentToDetach, CRunPVToolkitSkillWheelLevel1Div runPVToolkitSkillWheelLevel1Div) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				if (otherRgaIdsToFilterOn == null) {
					otherRgaIdsToFilterOn = new ArrayList<Integer>();
				}
				else {
					otherRgaIdsToFilterOn.clear();
				}
				
				_includeSelfFeedback = false;
				List<Component> filterCheckboxes = CDesktopComponents.vView().getComponentsByPrefix(_idPrefix + "FilterCheckbox_");
				for (Component filterCheckbox : filterCheckboxes) {
					if (filterCheckbox instanceof Checkbox) {
						if (((Checkbox)filterCheckbox).isChecked()) {
							IERunGroupAccount runGroupAccount = (IERunGroupAccount)filterCheckbox.getAttribute("runGroupAccount");
							if (runGroupAccount != null) {
								otherRgaIdsToFilterOn.add(runGroupAccount.getRgaId());
								if (runGroupAccount.getERunGroup().getRugId() == _actor.getRugId()) {
									_includeSelfFeedback = true;
								}
							}
						}
					}
				}
				if (otherRgaIdsToFilterOn.size() == 0) {
					//otherRgaIdsToFilterOn = null;
				}
				Combobox combobox = (Combobox)CDesktopComponents.vView().getComponent(_idPrefix + "CompareCycleCombobox");
				if (combobox != null) {
					//NOTE if only 1 cycle present, compare cycle combobox is not added
					if (combobox.getSelectedItem() != null) {
						_offsetToPreviousCycle = (int)combobox.getSelectedItem().getValue();
					}
				}

				componentToDetach.detach();
				runPVToolkitSkillWheelLevel1Div.update(_cycleNumber);
			}
		});
	}

	protected void showGroupsDiv(CRunPVToolkitSkillWheelLevel1Div skillWheelLevelDiv) {
		Div parentDiv = new CRunPVToolkitDefDiv(skillWheelLevelDiv, 
				new String[]{"class"}, 
				new Object[]{"popupDiv"}
		);

		new CRunPVToolkitDefImage(parentDiv, 
				new String[]{"class"}, 
				new Object[]{"popupBackground"}
		);
		
		String classPrefix = "skillWheelGroups";

		Div popupDiv = new CRunPVToolkitDefDiv(parentDiv, 
				new String[]{"class"}, 
				new Object[]{classPrefix}
		);

		new CRunPVToolkitDefImage(popupDiv, 
				new String[]{"class", "src"}, 
				new Object[]{classPrefix + "Background", zulfilepath + "compare-filter-background.svg"}
		);

		Div div = new CRunPVToolkitDefDiv(popupDiv, 
				new String[]{"class"}, 
				new Object[]{classPrefix + "Title"}
		);

		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font " + classPrefix + "Title", "PV-toolkit-skillwheel.button.groups"}
		);

		Image closeImage = new CRunPVToolkitDefImage(popupDiv, 
				new String[]{"class", "src"}, 
				new Object[]{classPrefix + "CloseImage", zulfilepath + "close.svg"}
		);
		addCloseGroupsOnClickEventListener(closeImage, parentDiv);

		div = new CRunPVToolkitDefDiv(popupDiv, 
				new String[]{"class"}, 
				new Object[]{classPrefix + "Group1Title"}
		);

		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font " + classPrefix + "Group1Title", "PV-toolkit-skillwheel.label.group1"}
		);

		Div peersDiv1 = new CRunPVToolkitDefDiv(popupDiv, 
				new String[]{"class"}, 
				new Object[]{classPrefix + "Grid1"}
		);
		
		div = new CRunPVToolkitDefDiv(popupDiv, 
				new String[]{"class"}, 
				new Object[]{classPrefix + "Group2Title"}
		);

		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font " + classPrefix + "Group2Title", "PV-toolkit-skillwheel.label.group2"}
		);

		Div peersDiv2 = new CRunPVToolkitDefDiv(popupDiv, 
				new String[]{"class"}, 
				new Object[]{classPrefix + "Grid2"}
		);
		
		Button resetButton = new CRunPVToolkitDefButton(popupDiv,
				new String[]{"class", "cLabelKey"}, 
				new Object[]{"font pvtoolkitButton " + classPrefix + "ResetButton", "PV-toolkit.reset"}
				);
		addGroupsResetButtonOnClickEventListener(resetButton);

		Button saveButton = new CRunPVToolkitDefButton(popupDiv,
				new String[]{"class", "cLabelKey"}, 
				new Object[]{"font pvtoolkitButton " + classPrefix + "SaveButton", "PV-toolkit.save"}
				);
		addGroupsSaveButtonOnClickEventListener(saveButton, parentDiv, this);

		List<IXMLTag> feedbackTags = getCycleFeedbackTags();
		
		groupRows1 = appendPeersGrid(peersDiv1, "PeersGrid1");
		fillRows(groupRows1, pvToolkit.getRgaIds(_compareRunGroupAccounts1), feedbackTags, _idPrefix + "FilterCheckbox1_");

		groupRows2 = appendPeersGrid(peersDiv2, "PeersGrid2");
		fillRows(groupRows2, pvToolkit.getRgaIds(_compareRunGroupAccounts2), feedbackTags, _idPrefix + "FilterCheckbox2_");

	}

	protected void addCloseGroupsOnClickEventListener(Component component, Component componentToDetach) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				componentToDetach.detach();
			}
		});
	}

	protected void addGroupsResetButtonOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				_compareRunGroupAccounts1 = null;
				_compareRunGroupAccounts2 = null;
				initCompareRunGroupAccounts();
				List<IXMLTag> feedbackTags = getCycleFeedbackTags();
				fillRows(groupRows1, pvToolkit.getRgaIds(_compareRunGroupAccounts1), feedbackTags, _idPrefix + "FilterCheckbox1_");
				fillRows(groupRows2, pvToolkit.getRgaIds(_compareRunGroupAccounts2), feedbackTags, _idPrefix + "FilterCheckbox2_");
			}
		});
	}

	protected void addGroupsSaveButtonOnClickEventListener(Component component, Component componentToDetach, CRunPVToolkitSkillWheelLevel1Div runPVToolkitSkillWheelLevel1Div) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				if (_compareRunGroupAccounts1 == null) {
					_compareRunGroupAccounts1 = new ArrayList<IERunGroupAccount>();
				}
				else {
					_compareRunGroupAccounts1.clear();
				}
				if (_compareRunGroupAccounts2 == null) {
					_compareRunGroupAccounts2 = new ArrayList<IERunGroupAccount>();
				}
				else {
					_compareRunGroupAccounts2.clear();
				}
				
				List<Component> filterCheckboxes = CDesktopComponents.vView().getComponentsByPrefix(_idPrefix + "FilterCheckbox1_");
				for (Component filterCheckbox : filterCheckboxes) {
					if (filterCheckbox instanceof Checkbox) {
						if (((Checkbox)filterCheckbox).isChecked()) {
							IERunGroupAccount runGroupAccount = (IERunGroupAccount)filterCheckbox.getAttribute("runGroupAccount");
							if (runGroupAccount != null) {
								_compareRunGroupAccounts1.add(runGroupAccount);
							}
						}
					}
				}

				filterCheckboxes = CDesktopComponents.vView().getComponentsByPrefix(_idPrefix + "FilterCheckbox2_");
				for (Component filterCheckbox : filterCheckboxes) {
					if (filterCheckbox instanceof Checkbox) {
						if (((Checkbox)filterCheckbox).isChecked()) {
							IERunGroupAccount runGroupAccount = (IERunGroupAccount)filterCheckbox.getAttribute("runGroupAccount");
							if (runGroupAccount != null) {
								_compareRunGroupAccounts2.add(runGroupAccount);
							}
						}
					}
				}

				componentToDetach.detach();
				runPVToolkitSkillWheelLevel1Div.update(_cycleNumber);
			}
		});
	}

    protected Rows appendPeersGrid(Component parent, String gridIdPostfix) {
    	return pvToolkit.appendGrid(
    			parent,
    			getId() + gridIdPostfix,
    			"position:absolute;left:0px;top:0px;width:460px;height:400px;overflow:auto;",
    			null,
    			new String[] {"60%", "30%", "10%"}, 
    			new boolean[] {true, true, true},
    			null,
    			"PV-toolkit-skillwheel.filter.column.label.", 
    			new String[] {"peer", "role", "show"});
	}

	protected int appendPeersToGrid(Rows rows, List<Integer> rgaIdsToFilterOn, int rowNumber, List<IXMLTag> feedbackTags, String peerCheckboxIdPrefix) {
		return appendPeersToGrid(
				rows, 
				rowNumber,
				new boolean[] {true, true, true},
				rgaIdsToFilterOn,
				feedbackTags,
				peerCheckboxIdPrefix
				);
	}

	protected int appendPeersToGrid(
			Rows rows, 
			int rowNumber,
    		boolean[] showColumn,
    		List<Integer> rgaIdsToFilterOn,
    		List<IXMLTag> feedbackTags,
			String peerCheckboxIdPrefix
			) {
		rows.getChildren().clear();
		
		//NOTE get all feedback givers from feedback tags.
		List<IERunGroupAccount> runGroupAccounts = new ArrayList<IERunGroupAccount>();
		for (IXMLTag feedbackTag : feedbackTags) {
			IERunGroupAccount runGroupAccount = pvToolkit.getRunGroupAccount(feedbackTag.getAttribute("feedbackrgaid"));
			if (runGroupAccount != null && !runGroupAccounts.contains(runGroupAccount)) {
				runGroupAccounts.add(runGroupAccount);
			}
		}
		List<Integer> allFeedbackGiverRgaIds = pvToolkit.getRgaIds(runGroupAccounts);
		
		//List<IXMLTag> commonPeerGroupTags = pvToolkit.getCommonPeerGroupTags(_actor, null, getOtherRolesToFilterOn(_cycleNumber), null, true, true);
		// NOTE: allow filtering and grouping feedback givers that now belong to other peer groups:
		List<IXMLTag> commonPeerGroupTags = pvToolkit.getPeerGroupTags(_actor).getXmlTags();
		
		boolean initializeRgaIdsToFilterOn = false;
		if (!_compareFeedbackGivers) {
			initializeRgaIdsToFilterOn = rgaIdsToFilterOn == null;
			if (initializeRgaIdsToFilterOn) {
				rgaIdsToFilterOn = new ArrayList<Integer>();
			}
		}

		//NOTE same rga ids might be existing in different peer groups. Only show one row per rga id. 
		List<Integer> handledRgaIds = new ArrayList<Integer>();
		for (IXMLTag peerGroupTag : commonPeerGroupTags) {
			List<IXMLTag> memberTags = pvToolkit.getStatusChildTag(peerGroupTag).getChilds("member");
			for (IXMLTag memberTag : memberTags) {
				IERunGroupAccount runGroupAccount = pvToolkit.getRunGroupAccount(pvToolkit.getStatusChildTagAttribute(memberTag, "rgaid"));
				//NOTE if student not (yet) active in run (has not yet enrolled) runGroupAccount is null  
				if (runGroupAccount != null) {
					if (!handledRgaIds.contains(runGroupAccount.getRgaId())) {
						if (allFeedbackGiverRgaIds.contains(runGroupAccount.getRgaId())) {
							String peerRole = pvToolkit.getStatusChildTagAttribute(memberTag, "role");
							if (!_compareFeedbackGivers) {
								if (initializeRgaIdsToFilterOn) {
									if (!rgaIdsToFilterOn.contains(runGroupAccount.getRgaId()) && !(runGroupAccount.getERunGroup().getRugId() == _actor.getRugId())) {
										rgaIdsToFilterOn.add(runGroupAccount.getRgaId());
									}
								}
							}
							appendRowToGrid(rows, showColumn, rgaIdsToFilterOn, runGroupAccount, peerRole, initializeRgaIdsToFilterOn, peerCheckboxIdPrefix);
							rowNumber ++;
						}
					}
					
					handledRgaIds.add(runGroupAccount.getRgaId());
				}
			}
		}
		if (!_compareFeedbackGivers) {
			if (initializeRgaIdsToFilterOn) {
				otherRgaIdsToFilterOn = rgaIdsToFilterOn;
			}
		}
		return rowNumber;
	}

	protected void appendRowToGrid(
			Rows rows, 
			boolean[] showColumn,
			List<Integer> rgaIdsToFilterOn,
			IERunGroupAccount runGroupAccount,
			String peerRole,
			boolean initializeRgaIdsToFilterOn,
			String peerCheckboxIdPrefix
			) {

		Row row = new Row();
		rows.appendChild(row);
						
		int columnNumber = 0;
		if (showColumn.length > columnNumber && showColumn[columnNumber]) {
			String name = "";
			if (isPreviewRun) {
				name = runGroupAccount.getERunGroup().getName();
			}
			else {
				name = accountManager.getAccountName(runGroupAccount.getEAccount());
			}
			new CRunPVToolkitDefLabel(row, 
					new String[]{"class", "value"}, 
					new Object[]{"font gridLabel", name}
					);
		}

		columnNumber++;
		if (showColumn.length > columnNumber && showColumn[columnNumber]) {
			//NOTE student does not see student-assistant in tips/tops but teacher
			if (pvToolkit.hasStudentRole && peerRole.equals(CRunPVToolkit.peerGroupStudentAssistantRole)) {
				peerRole = CRunPVToolkit.peerGroupTeacherRole;
			}
			new CRunPVToolkitDefLabel(row, 
					new String[]{"class", "labelKey"}, 
					new Object[]{"font gridLabel", "PV-toolkit.peergroup.member.role." + peerRole}
					);
		}
		
		columnNumber++;
		if (showColumn.length > columnNumber && showColumn[columnNumber]) {
			boolean selfFeedback = runGroupAccount.getERunGroup().getRugId() == _actor.getRugId();
			boolean checked = rgaIdsToFilterOn.contains(runGroupAccount.getRgaId());
			if (initializeRgaIdsToFilterOn && selfFeedback) {
				checked = false;
			}
			Component checkbox = new CRunPVToolkitDefCheckbox(row, 
					new String[]{"id", "class", "checked"}, 
					new Object[]{peerCheckboxIdPrefix + runGroupAccount.getRgaId(), "gridCheckbox", checked}
					);
			checkbox.setAttribute("runGroupAccount", runGroupAccount);
		}
		
	}

	protected void setEditableAndMarkable() {
		//NOTE _editable depends on _cycleNumber and on current step type
		//only in last cycle and in step number 4 and 5 editing is possible
		_editable = false;
		if (pvToolkit.getCurrentStepTag() != null) {
			String stepType = pvToolkit.getCurrentStepTag().getChildValue("steptype"); 
			_editable = (_cycleNumber == _maxCycleNumber) && (stepType.equals(CRunPVToolkit.viewFeedbackStepType) || stepType.equals(CRunPVToolkit.defineGoalsStepType));
		}
		//NOTE don't show mark buttons when comparing with experts.
		//Only show marks in previous cycles, but then they are not editable, or in view feedback and define goals step in current cycle, and then they are editable
		_showMarks = !_compareWithExperts && ((_cycleNumber < _maxCycleNumber) || _editable);
	}

	public void showCycle(Component parentDiv, int numberOfFeedbacks) {
		if (!_showCycle) {
			return;
		}
		if (_maxCycleNumber < 2)
			return;
		Label label = new CRunPVToolkitDefLabel(parentDiv, 
				new String[]{"class"}, 
				new Object[]{"font " + _classPrefix + "FeedbacksTitle"}
		);
		String labelKey = "PV-toolkit-skillwheel.header.feedback";
		if (numberOfFeedbacks > 1) {
			labelKey += "s";
		}
		label.setValue("" + numberOfFeedbacks + " " + CDesktopComponents.vView().getLabel(labelKey));
		
		Div div = new CRunPVToolkitDefDiv(parentDiv, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "CycleDiv"}
		);
		
		String labelClass = "";
		String labelValue = "";
		if (_cycleNumber > _offsetToPreviousCycle && !_compareFeedbackGivers) {
			//compare cycles
			labelClass = "font " + _browseClassPrefix + "TitleCompare";
			labelValue = CDesktopComponents.vView().getCLabel("PV-toolkit-skillwheel.header.cycleCompare").replace("%1", ("" + _cycleNumber)).replace("%2", ("" + (_cycleNumber - _offsetToPreviousCycle)));
		}
		else {
			//no cycles to compare or comparison of feedback givers
			labelClass = "font " + _browseClassPrefix + "Title";
			labelValue = CDesktopComponents.vView().getCLabel("PV-toolkit-skillwheel.header.cycle").replace("%1", ("" + _cycleNumber));
		}
		label = new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "value"}, 
				new Object[]{labelClass, labelValue}
		);
		
		CRunPVToolkitBrowseImage browseImage = new CRunPVToolkitBrowseImage(div, 
				new String[]{"class", "src"}, 
				new Object[]{getBrowseFirstClass(_cycleNumber, 1), getBrowseFirstSrc(_cycleNumber, 1)}
		);
		browseImage.init(this, _cycleNumber > 1, "first");
		browseImage = new CRunPVToolkitBrowseImage(div, 
				new String[]{"class", "src"}, 
				new Object[]{getBrowsePreviousClass(_cycleNumber, 1), getBrowsePreviousSrc(_cycleNumber, 1)}
		);
		browseImage.init(this, _cycleNumber > 1, "previous");
		browseImage = new CRunPVToolkitBrowseImage(div, 
				new String[]{"class", "src"}, 
				new Object[]{getBrowseNextClass(_cycleNumber, _maxCycleNumber), getBrowseNextSrc(_cycleNumber, _maxCycleNumber)}
		);
		browseImage.init(this, _cycleNumber < _maxCycleNumber, "next");
		browseImage = new CRunPVToolkitBrowseImage(div, 
				new String[]{"class", "src"}, 
				new Object[]{getBrowseLastClass(_cycleNumber, _maxCycleNumber), getBrowseLastSrc(_cycleNumber, _maxCycleNumber)}
		);
		browseImage.init(this, _cycleNumber < _maxCycleNumber, "last");
		label = new CRunPVToolkitDefLabel(div, 
				new String[]{"class"}, 
				new Object[]{"font " + _browseClassPrefix + "MaxCycles"}
		);
		label.setValue("" + _maxCycleNumber);
		
	}

	protected StringBuffer getColorsAsString(String[] colorArr, int skillClusterNumberToFilter) {
		StringBuffer colors = new StringBuffer();
		colors.append("[");
		for (int i=0;i<colorArr.length;i++) {
			if (skillClusterNumberToFilter ==  0 || i == (skillClusterNumberToFilter - 1)) {
				colors.append("\""+ colorArr[i] + "\"");
			}
			else {
				colors.append("\""+ "transparent" + "\"");
			}
			if (i < (colorArr.length - 1)) {
				colors.append(",");
			}
		}
		colors.append("]");
		return colors;
	}
	
	protected StringBuffer getStringListAsString(List<String> stringList) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("[");
		for (int i=0;i<stringList.size();i++) {
			buffer.append("\""+ stringList.get(i) + "\"");
			if (i < (stringList.size() - 1)) {
				buffer.append(",");
			}
		}
		buffer.append("]");
		return buffer;
	}
	
	public void showSkillWheel(Component parentDiv) {
		Map<String,Object> propertyMap = new HashMap<String,Object>();
		propertyMap.put("zulfilepath", zulfilepath);
		
		//number of skill clusters
		int skillclusterCounter = 0;
		//number of sub skills per skill cluster
		List<Integer> subskillCounters = new ArrayList<Integer>();
		//segments = sub skills
		List<Integer> subskillSegments = new ArrayList<Integer>();
		
		new CRunPVToolkitDefLabel(parentDiv, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font " + _classPrefix + "ScoreLevelHeader", "PV-toolkit-skillwheel.header.finalfeedback"}
		);
		List<String> skillPerformanceStates;
		if (_compareWithExperts) {
			skillPerformanceStates = getPerformanceLevelStates(skillTag, _skillExampleExpertTags, _skillExampleFeedbackTag);
		}
		else {
			skillPerformanceStates = getPerformanceLevelStates(_cycleNumber, skillTag, _sharedFeedbackTagsPerFeedbackStep);
		}
		Div div = new CRunPVToolkitDefDiv(parentDiv, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "ScoreLevelDiv"}
		);
		pvToolkit.renderStars(div, skillPerformanceStates);
		
		div = new CRunPVToolkitDefDiv(parentDiv, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "LegendDiv"}
		);
		renderLegend(div);
		
		List<String> skillclusterNames = new ArrayList<String>();;
		List<String> skillclusterAccessibles = new ArrayList<String>();;
		List<String> subskillNames = new ArrayList<String>();;
		List<String> subskillAccessibles = new ArrayList<String>();;
		List<List<List<String>>> performanceColorsPerSkillClusterPerSubSkill = new ArrayList<List<List<String>>>();
		
		List<IXMLTag> parentTags = new ArrayList<IXMLTag>();
		//NOTE a rubric contains either skill clusters, directly only the skill tag, and sub skills within them or only sub skills directly under the skill tag. A mixture is not allowed.
		if (_numberOfSkillClusters > 0) {
			parentTags = skillTag.getChilds("skillcluster");
		}
		else {
			parentTags.add(skillTag);
		}
		
		for (IXMLTag parentTag : parentTags) {
			if (pvToolkit.isRunSpecificTagPresent(new CRunPVToolkitCacAndTag(caseComponent, parentTag))) {
				if (parentTag.getName().equals("skillcluster")) {
					skillclusterNames.add(pvToolkit.getTagChildValue(new CRunPVToolkitCacAndTag(caseComponent, parentTag), "name"));
					skillclusterAccessibles.add("" + !parentTag.getCurrentStatusAttribute(AppConstants.statusKeyAccessible).equals(AppConstants.statusValueFalse));
				}
				List<List<String>> subSkillPerformanceColorsPerSkillCluster = new ArrayList<List<String>>();
				performanceColorsPerSkillClusterPerSubSkill.add(subSkillPerformanceColorsPerSkillCluster);
				//number of sub skills per skill cluster
				int subskillCounter = 0;
				for (IXMLTag subskillTag : parentTag.getChilds("subskill")) {
					if (pvToolkit.isRunSpecificTagPresent(new CRunPVToolkitCacAndTag(caseComponent, subskillTag))) {
						subskillNames.add(pvToolkit.getTagChildValue(new CRunPVToolkitCacAndTag(caseComponent, subskillTag), "name"));
						subskillAccessibles.add("" + !subskillTag.getCurrentStatusAttribute(AppConstants.statusKeyAccessible).equals(AppConstants.statusValueFalse));
						subskillSegments.add(1);
						List<String> subSkillPerformanceColorsPerSubSkill = new ArrayList<String>();
						subSkillPerformanceColorsPerSkillCluster.add(subSkillPerformanceColorsPerSubSkill);
						List<String> performanceLevelColors;
						if (_compareWithExperts) {
							performanceLevelColors = getPerformanceLevelColors(skillclusterCounter, subskillTag, _skillExampleExpertTags, _skillExampleFeedbackTag);
						}
						else {
							performanceLevelColors = getPerformanceLevelColors(_cycleNumber, skillclusterCounter, subskillTag, _sharedFeedbackTagsPerFeedbackStep);
						}
						subSkillPerformanceColorsPerSubSkill.addAll(performanceLevelColors);
						subskillCounter++;
					}
				}
				subskillCounters.add(subskillCounter);
				if (parentTag.getName().equals("skillcluster")) {
					skillclusterCounter++;
				}
			}
		}

		//NOTE to be able to use strings in JavaScript in PV-toolkit-skill-wheel-cluster-subskill.zul they have to be put in an array
		propertyMap.put("SC_names", getStringListAsString(skillclusterNames).toString());
		propertyMap.put("SC_accessibles", getStringListAsString(skillclusterAccessibles).toString());
		propertyMap.put("SS_names", getStringListAsString(subskillNames).toString());
		propertyMap.put("SS_accessibles", getStringListAsString(subskillAccessibles).toString());
		List<String> temp = new ArrayList<String>();
		temp.add(CDesktopComponents.vView().getLabel("PV-toolkit-skillwheel.label.subskill.inaccessible"));
		propertyMap.put("SS_inaccessibleText", getStringListAsString(temp));
		temp = new ArrayList<String>();
		temp.add(CDesktopComponents.vView().getLabel("PV-toolkit-skillwheel.label.subskill.accessible"));
		propertyMap.put("SS_accessibleText", getStringListAsString(temp));
		
		propertyMap.put("SC_colors", getColorsAsString(pvToolkit.getSkillClusterColors(), _skillClusterNumberToFilter).toString());
		propertyMap.put("SC_colors_pastel", getColorsAsString(pvToolkit.getPerformanceLevelSkillClusterColors(), _skillClusterNumberToFilter).toString());
		
		propertyMap.put("SC_number", skillclusterCounter);
		propertyMap.put("SS_numbers", subskillCounters);
		//number of performance levels
		int PL_number = _numberOfPerformanceLevelsToShow;
		propertyMap.put("PL_number", PL_number);

		propertyMap.put("PL_segments", subskillSegments);
		
		StringBuffer PL_colors = new StringBuffer();
		PL_colors.append("[");
		for (int i=0;i<PL_number;i++) {
			PL_colors.append("[");
			int number = 0;
			for (int j=0;j<performanceColorsPerSkillClusterPerSubSkill.size();j++) {
				for (int k=0;k<subskillCounters.get(j);k++) {
					if (_skillClusterNumberToFilter ==  0 || j == (_skillClusterNumberToFilter - 1)) {
						PL_colors.append("\"" + performanceColorsPerSkillClusterPerSubSkill.get(j).get(k).get(i) + "\"");
					}
					else {
						PL_colors.append("\"" + "transparent" + "\"");
					}
					if (number < (subskillSegments.size() - 1)) {
						PL_colors.append(",");
					}
					number ++;
				}
			}
			PL_colors.append("]");
			if (i < (PL_number - 1)) {
				PL_colors.append(",");
			}
		}
		PL_colors.append("]");
		propertyMap.put("PL_colors", PL_colors.toString());
		
		if (_skillClusterNumberToFilter > 0) {
			double totalNumberOfSubskills = subskillSegments.size();
			double numberOfSubSkills = subskillCounters.get(_skillClusterNumberToFilter - 1);
			double numberOfPreviousSubSkills = 0;
			for (int i=0;i<(_skillClusterNumberToFilter - 1);i++) {
				numberOfPreviousSubSkills += subskillCounters.get(i);
			}
			double currentAverageDegree = ((numberOfPreviousSubSkills + (numberOfSubSkills / 2)) / totalNumberOfSubskills)*360;
			_skillWheelRotationDegree = 90 - currentAverageDegree;
		}
		
		
		propertyMap.put("skill_wheel_div_uuid", getUuid());
		propertyMap.put("skill_wheel_unique_postfix", getUuid());
		propertyMap.put("number_of_levels", _numberOfPerformanceLevelsToShow);
		propertyMap.put("presentation_level", _presentationLevel);
		propertyMap.put("zoom_factor", _skillWheelZoomFactor);
		propertyMap.put("rotation_degree", _skillWheelRotationDegree);

		//NOTE for the Gamebrics project the skill wheel has another layout. To enable this the following parameters are added
		propertyMap.put("segmentOffset", 35);
		propertyMap.put("segmentPadding", 10);
		propertyMap.put("padAngle", 0.02);
		propertyMap.put("frameColor", "");
		propertyMap.put("SC_hide", false);
		propertyMap.put("SS_PLs", new ArrayList<Integer>());

		HtmlMacroComponent macro = new HtmlMacroComponent();
		parentDiv.appendChild(macro);
		macro.setZclass(_classPrefix + "Macro");
		macro.setDynamicProperty("a_propertyMap", propertyMap);
		macro.setMacroURI(zulfilepath + "PV-toolkit-skill-wheel-cluster-subskill.zul");
		
		Clients.evalJavaScript("renderSkillWheel" + getUuid() + "();");
		
	}
	
	protected void renderLegend(Component parent) {
		Vbox vbox = new CRunPVToolkitDefVbox(parent, 
				new String[]{}, 
				new Object[]{}
		);
		String labelKeyPrefix = "PV-toolkit-skillwheel.header.";
		if (_compareWithExperts) {
			//NOTE only show experts legend if there are expert scores
			if (_skillExampleExpertTags.size() > 0) {
				renderLegendHbox(vbox, _classPrefix + "LegendEqualButton", labelKeyPrefix + "compare.equal");
				renderLegendHbox(vbox, _classPrefix + "LegendFirstButton", labelKeyPrefix + "compare.experts"); 
			}
			renderLegendHbox(vbox, _classPrefix + "LegendSecondButton", labelKeyPrefix + "compare.self");
		}
		else if (_compareFeedbackGivers) {
			renderLegendHbox(vbox, _classPrefix + "LegendEqualButton", labelKeyPrefix + "compare.equal"); 
			renderLegendHbox(vbox, _classPrefix + "LegendFirstButton", labelKeyPrefix + "compare.first"); 
			renderLegendHbox(vbox, _classPrefix + "LegendSecondButton", labelKeyPrefix + "compare.second");
		}
		else {
			renderLegendHbox(vbox, _classPrefix + "LegendAchievedButton", labelKeyPrefix + "score.achieved");
			//NOTE only in second and higher cycle show improvement or deterioration
			if (_cycleNumber > 1) {
				renderLegendHbox(vbox, _classPrefix + "LegendImprovedButton", labelKeyPrefix + "score.improved"); 
				renderLegendHbox(vbox, _classPrefix + "LegendDeterioratedButton", labelKeyPrefix + "score.deteriorated");
			}
		}
	}

	protected void renderLegendHbox(Component parent, String buttonClass, String labelKey) {
		Hbox hbox = new CRunPVToolkitDefHbox(parent, 
				new String[]{}, 
				new Object[]{}
		);
		new CRunPVToolkitDefButton(hbox, 
				new String[]{"class"}, 
				new Object[]{"font skillWheelLevelLegendButton " + buttonClass}
				);
		new CRunPVToolkitDefLabel(hbox, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font skillWheelLevelLegendLabel", labelKey}
				);
	}

	protected int getNumberOfCycleFeedbacks(int cycleNumber, List<List<IXMLTag>> sharedFeedbackTagsPerFeedbackStep) {
		int numberOfFeedbacks = 0;
		if (cycleNumber < 1 || cycleNumber > sharedFeedbackTagsPerFeedbackStep.size()) {
			return numberOfFeedbacks;
		}
		return sharedFeedbackTagsPerFeedbackStep.get(cycleNumber - 1).size();
	}
	
	protected List<IXMLTag> getCycleFeedbackTags(int cycleNumber, List<List<IXMLTag>> sharedFeedbackTagsPerFeedbackStep) {
		if (cycleNumber < 1 || cycleNumber > sharedFeedbackTagsPerFeedbackStep.size()) {
			return null;
		}
		return sharedFeedbackTagsPerFeedbackStep.get(cycleNumber - 1);
	}
	
	protected String getBrowseClassSuffix(int cycleNumber, int compareCycleNumber, boolean forward) {
		String classSuffix = "";
		if ((forward && cycleNumber < compareCycleNumber) || (!forward && cycleNumber > compareCycleNumber)) {
			classSuffix = "Active";
		}
		else {
			classSuffix = "Inactive";
		}
		return classSuffix;
	}

	protected String getBrowseSrcSuffix(int cycleNumber, int compareCycleNumber, boolean forward) {
		String srcSuffix = "";
		if ((forward && cycleNumber < compareCycleNumber) || (!forward && cycleNumber > compareCycleNumber)) {
			srcSuffix = "active";
		}
		else {
			srcSuffix = "inactive";
		}
		return srcSuffix;
	}

	protected String getBrowseFirstClass(int cycleNumber, int compareCycleNumber) {
		return _browseClassPrefix + "First" + getBrowseClassSuffix(cycleNumber, compareCycleNumber, false);
	}

	protected String getBrowsePreviousClass(int cycleNumber, int compareCycleNumber) {
		return _browseClassPrefix + "Previous" + getBrowseClassSuffix(cycleNumber, compareCycleNumber, false);
	}

	protected String getBrowseNextClass(int cycleNumber, int compareCycleNumber) {
		return _browseClassPrefix + "Next" + getBrowseClassSuffix(cycleNumber, compareCycleNumber, true);
	}

	protected String getBrowseLastClass(int cycleNumber, int compareCycleNumber) {
		return _browseClassPrefix + "Last" + getBrowseClassSuffix(cycleNumber, compareCycleNumber, true);
	}

	protected String getBrowseFirstSrc(int cycleNumber, int compareCycleNumber) {
		return zulfilepath + "arrow-alt-to-first-" + getBrowseSrcSuffix(cycleNumber, compareCycleNumber, false) + ".svg";
	}

	protected String getBrowsePreviousSrc(int cycleNumber, int compareCycleNumber) {
		return zulfilepath + "arrow-alt-left-" + getBrowseSrcSuffix(cycleNumber, compareCycleNumber, false) + ".svg";
	}

	protected String getBrowseNextSrc(int cycleNumber, int compareCycleNumber) {
		return zulfilepath + "arrow-alt-right-" + getBrowseSrcSuffix(cycleNumber, compareCycleNumber, true) + ".svg";
	}

	protected String getBrowseLastSrc(int cycleNumber, int compareCycleNumber) {
		return zulfilepath + "arrow-alt-to-last-" + getBrowseSrcSuffix(cycleNumber, compareCycleNumber, true) + ".svg";
	}

	protected List<String> getPerformanceLevelColors(int cycleNumber, int skillclusterCounter, IXMLTag skillTag, List<List<IXMLTag>> sharedFeedbackTagsPerFeedbackStep) {
		List<String> performanceLevelStates = getPerformanceLevelStates(cycleNumber, skillTag, sharedFeedbackTagsPerFeedbackStep);
		return pvToolkit.getPerformanceLevelColors(performanceLevelStates, skillclusterCounter);
	}

	protected List<String> getPerformanceLevelColors(int skillclusterCounter, IXMLTag skillTag, List<IXMLTag> skillExampleExpertTags, IXMLTag skillExampleFeedbackTag) {
		List<String> performanceLevelStates = getPerformanceLevelStates(skillTag, skillExampleExpertTags, skillExampleFeedbackTag);
		return pvToolkit.getPerformanceLevelColors(performanceLevelStates, skillclusterCounter);
	}

	protected List<String> getPerformanceLevelStates(int cycleNumber, IXMLTag skillTag, List<List<IXMLTag>> sharedFeedbackTagsPerFeedbackStep) {
		double firstLevel = 0;
		double secondLevel = 0;
		if (_compareFeedbackGivers) {
			//NOTE user wants to compare levels of feedback givers, including himself
			//level of first feedback giver group
			firstLevel = pvToolkit.getMeanPerformanceLevel(_actor, cycleNumber, skillTag, sharedFeedbackTagsPerFeedbackStep, _compareRunGroupAccounts1);
			//level of second feedback giver group
			secondLevel = pvToolkit.getMeanPerformanceLevel(_actor, cycleNumber, skillTag, sharedFeedbackTagsPerFeedbackStep, _compareRunGroupAccounts2);
			//NOTE if one of the levels is 0 set both levels to 0 because comparison is not possible
			if (firstLevel == 0 || secondLevel == 0) {
				firstLevel = 0;
				secondLevel = 0;
			}
		}
		else {
			//NOTE user wants to compare levels in different cycles
			//NOTE firstLevel is level within cycle (cycleNumber - 1)
			//NOTE secondLevel is level within cycle cycleNumber
			secondLevel = pvToolkit.getMeanPerformanceLevel(_actor, cycleNumber, skillTag, sharedFeedbackTagsPerFeedbackStep, null);
			int firstCycleNumber = cycleNumber - _offsetToPreviousCycle; 
			if (firstCycleNumber <= 0) {
				//NOTE no previous cycle, so set first level to second level
				firstLevel = secondLevel;
			}
			else {
				firstLevel = pvToolkit.getMeanPerformanceLevel(_actor, firstCycleNumber, skillTag, sharedFeedbackTagsPerFeedbackStep, null);
			}
		}
		//NOTE now the skill wheel can only show discrete values, so convert doubles to longs
		int firstLevelLong = new Long(Math.round(firstLevel)).intValue();
		int secondLevelLong = new Long(Math.round(secondLevel)).intValue();
		return getPerformanceLevelStates(firstLevelLong, secondLevelLong);
	}

	protected List<String> getPerformanceLevelStates(IXMLTag skillTag, List<IXMLTag> skillExampleExpertTags, IXMLTag skillExampleFeedbackTag) {
		double firstLevel = 0;
		double secondLevel = 0;
		if (_compareFeedbackTag) {
			//NOTE user wants to compare experts level with his own level
			//level of experts
			firstLevel = getMeanSkillExampleExpertsPerformanceLevel(skillTag, skillExampleExpertTags, _compareExpertTags1);
			//own level
			secondLevel = getSkillExampleFeedbackPerformanceLevel(skillTag, skillExampleFeedbackTag);
		}
		else {
			//NOTE user wants to compare experts levels
			//level of first expert group
			firstLevel = getMeanSkillExampleExpertsPerformanceLevel(skillTag, skillExampleExpertTags, _compareExpertTags1);
			//level of second expert group
			secondLevel = getMeanSkillExampleExpertsPerformanceLevel(skillTag, skillExampleExpertTags, _compareExpertTags2);
		}
		//NOTE now the skill wheel can only show discrete values, so convert doubles to longs
		int firstLevelLong = new Long(Math.round(firstLevel)).intValue();
		int secondLevelLong = new Long(Math.round(secondLevel)).intValue();
		return getPerformanceLevelStates(firstLevelLong, secondLevelLong);
	}

	protected List<String> getPerformanceLevelStates(int firstLevel, int secondLevel) {
		List<String> performanceLevelStates = new ArrayList<String>();
		//NOTE if performance levels are filtered, e.g., showing levels 1, 3 and 5 and omitting levels 2 and 4, given level scores have to be converted.
		//_numberOfPerformanceLevelsToShow then will be 3 instead of 5 and _performanceLevelValues contains the converted labels.
		//E.g., levels 1,2,3,4,5 will be converted to levels 1,2,2,2,3
		String[] performanceLevelStatesArr = new String[_numberOfPerformanceLevelsToShow];
		for (int i=0;i<_numberOfPerformanceLevelsToShow;i++) {
			performanceLevelStatesArr[i] = "";
		}
		if ((_cycleNumber == 1) || (secondLevel > 0)) {
			//NOTE if first level > 0 it is scored. If 0 it is not scored, so don't show progress or difference
			if (firstLevel > 0) {
				for (int i=0;i<_performanceLevelValues[firstLevel - 1];i++) {
					if (_compareWithExperts || _compareFeedbackGivers) {
						performanceLevelStatesArr[i] = CRunPVToolkit.performanceLevelCompareBothState;
					}
					else {
						performanceLevelStatesArr[i] = CRunPVToolkit.performanceLevelAchievedState;
					}
				}
			}
			//NOTE if second level > 0 it is scored. If 0 it is not scored, so don't show progress or difference
			if (secondLevel > 0) {
				if (firstLevel > 0) {
					for (int i=_performanceLevelValues[secondLevel - 1];i<_performanceLevelValues[firstLevel - 1];i++) {
						if (_compareWithExperts || _compareFeedbackGivers) {
							performanceLevelStatesArr[i] = CRunPVToolkit.performanceLevelCompareFirstState;
						}
						else {
							performanceLevelStatesArr[i] = CRunPVToolkit.performanceLevelDeterioratedState;
						}
					}
					for (int i=_performanceLevelValues[firstLevel - 1];i<_performanceLevelValues[secondLevel - 1];i++) {
						if (_compareWithExperts || _compareFeedbackGivers) {
							performanceLevelStatesArr[i] = CRunPVToolkit.performanceLevelCompareSecondState;
						}
						else {
							performanceLevelStatesArr[i] = CRunPVToolkit.performanceLevelImprovedState;
						}
					}
				} else {
					for (int i=0;i<_performanceLevelValues[secondLevel - 1];i++) {
						if (_compareWithExperts || _compareFeedbackGivers) {
							performanceLevelStatesArr[i] = CRunPVToolkit.performanceLevelCompareBothState;
						}
						else {
							performanceLevelStatesArr[i] = CRunPVToolkit.performanceLevelAchievedState;
						}
					}
				}
			}
		}
		for (int i=0;i<_numberOfPerformanceLevelsToShow;i++) {
			if (performanceLevelStatesArr[i].equals("")) {
				performanceLevelStatesArr[i] = CRunPVToolkit.performanceLevelNoScoreState;
			}
		}
		for (int i=0;i<_numberOfPerformanceLevelsToShow;i++) {
			performanceLevelStates.add(performanceLevelStatesArr[i]);
		}
		return performanceLevelStates;
	}

	protected double getMeanSkillExampleExpertsPerformanceLevel(IXMLTag skillTag, List<IXMLTag> skillExampleExpertTags, List<IXMLTag> filterOnSkillExampleExpertTags) {
		//NOTE return mean level as double to be prepared for showing non integer values within skill wheel
		double meanLevel = 0;
		double numberOfExperts = 0;
		double sumOfLevels = 0;
		for (IXMLTag skillExampleExpertTag : skillExampleExpertTags) {
			if (filterOnSkillExampleExpertTags == null || filterOnSkillExampleExpertTags.contains(skillExampleExpertTag)) {
				double level = pvToolkit.getSkillExampleExpertLevel(skillExampleExpertTag, skillTag);
				if (level >= 0) {
					sumOfLevels += level;
					numberOfExperts ++;
				}
			}
		}
		if (numberOfExperts > 0) {
			return sumOfLevels / numberOfExperts;
		}
		return meanLevel;
	}

	protected double getSkillExampleFeedbackPerformanceLevel(IXMLTag skillTag, IXMLTag skillExampleFeedbackTag) {
		//NOTE return level as double to be prepared for showing non integer values within skill wheel
		//NOTE always use progress of first feedback step, because progress of prepare steps is shared over cycles
		IXMLTag feedbackStepTag = pvToolkit.getFirstStepTag(CRunPVToolkit.feedbackStepType);
		return pvToolkit.getFeedbackLevel(_actor, feedbackStepTag, skillExampleFeedbackTag, true, skillTag);
	}
	
	protected boolean showFeedbackTipsOrTops(CRunPVToolkitFeedbackTipOrTop feedback) {
		if (!_compareWithExperts) {
			boolean showTipsOrTops = otherRgaIdsToFilterOn == null || otherRgaIdsToFilterOn.contains(feedback.getPeer().getRgaId());
			if (showTipsOrTops) {
				if (_onlySelfFeedback && feedback.getPeer().getERunGroup().getRugId() != _actor.getRugId()) {
					showTipsOrTops = false;
				}
			}

			return showTipsOrTops;
		}
		if (_compareWithExperts) {
			if (_compareFeedbackTag) {
				if (_compareExpertTags1 == null) {
					return true;
				}
				if (_compareExpertTags1 != null && _compareExpertTags1.contains(feedback.getFeedbackTag())) {
					return true;
				}
			}
			else {
				if (_compareExpertTags1 == null && _compareExpertTags2 == null) {
					return true;
				}
				if (_compareExpertTags1 != null && _compareExpertTags1.contains(feedback.getFeedbackTag())) {
					return true;
				}
				if (_compareExpertTags2 != null && _compareExpertTags2.contains(feedback.getFeedbackTag())) {
					return true;
				}
			}
		}
		return false;
	}

	protected int appendTipsOrTopsToGrid(
			Rows rows, 
			String tipsOrTops, 
			int rowNumber,
    		boolean[] showColumn 
			) {
		String tipsOrTopsImage = "";
		if (tipsOrTops.equals("tops")) {
			tipsOrTopsImage = "thumbs-up-gray.svg";
		}
		else if (tipsOrTops.equals("tips")) {
			tipsOrTopsImage = "map-marker-exclamation-gray.svg";
		}
		boolean skillclusterHasTipsTops = skillclusterHasTipsTops();
		boolean subskillHasTipsTops = subskillHasTipsTops();
		boolean skillHasTipsTops = skillHasTipsTops();
		for (CRunPVToolkitFeedbackTipOrTop feedback : pvToolkit.getPerformanceTipsOrTops(
				_actor, 
				skillTag, 
				_sharedFeedbackTagsPerFeedbackStep, 
				_skillExampleExpertTags,
				_skillExampleFeedbackTag,
				true, 
				_cycleNumber, 
				tipsOrTops, 
				_compareWithExperts)) {
			if (showFeedbackTipsOrTops(feedback) && (_skillClusterNumberToFilter == 0 || feedback.getSkillClusterNumber() == _skillClusterNumberToFilter)) {
				Row row = new Row();
				rows.appendChild(row);
				
				int columnNumber = 0;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					new CRunPVToolkitDefImage(row, 
							new String[]{"class", "src"}, 
							new Object[]{"gridImage", zulfilepath + tipsOrTopsImage}
							);
				}
	
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					Label label = new CRunPVToolkitDefLabel(row, 
							new String[]{"class", "value"}, 
							new Object[]{"font gridLabel", feedback.getValue()}
							);
					//NOTE if ordered tops will be shown on top
					if (tipsOrTops.equals("tops")) {
						label.setAttribute("orderValue", "1");
					}
					else if (tipsOrTops.equals("tips")) {
						label.setAttribute("orderValue", "2");
					}
				}
	
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					new CRunPVToolkitDefLabel(row, 
							new String[]{"class", "value"}, 
							new Object[]{"font gridLabel", feedback.getFeedbackName()}
							);
				}
				
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					new CRunPVToolkitDefLabel(row, 
							new String[]{"class", "value"}, 
							new Object[]{"font gridLabel", feedback.getFeedbackRole()}
							);
				}
				
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					CRunPVToolkitDefLabel label = new CRunPVToolkitDefLabel(row, 
							new String[]{"class", "value"}, 
							new Object[]{"font gridLabel", feedback.getShareDate()}
							);
					//NOTE for ordering use ymd format
					if (!StringUtils.isEmpty(feedback.getShareDate())) {
						Date date = CDefHelper.getDateFromStrDMY(feedback.getShareDate());
						label.setAttribute("orderValue", CDefHelper.getDateStrAsYMD(date));
					}
				}
				
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					String value = "";
					if (skillclusterHasTipsTops && feedback.getSkillClusterTag() != null) {
						value = pvToolkit.getTagChildValue(new CRunPVToolkitCacAndTag(caseComponent, feedback.getSkillClusterTag()), "name");
					}
					new CRunPVToolkitDefLabel(row, 
							new String[]{"class", "value"}, 
							new Object[]{"font gridLabel", value}
							);
				}
				
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					String value = "";
					if (subskillHasTipsTops && feedback.getSubSkillTag() != null) {
						value = pvToolkit.getTagChildValue(new CRunPVToolkitCacAndTag(caseComponent, feedback.getSubSkillTag()), "name");
					}
					new CRunPVToolkitDefLabel(row, 
							new String[]{"class", "value"}, 
							new Object[]{"font gridLabel", value}
							);
				}
				
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					String value = "";
					if (skillHasTipsTops && feedback.getSkillTag() != null && feedback.getSkillClusterTag() == null && feedback.getSubSkillTag() == null) {
						value = pvToolkit.getTagChildValue(new CRunPVToolkitCacAndTag(caseComponent, feedback.getSkillTag()), "name");
					}
					new CRunPVToolkitDefLabel(row, 
							new String[]{"class", "value"}, 
							new Object[]{"font gridLabel", value}
							);
				}
				
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					String styleClass = "gridImage";
					if (_editable) {
						styleClass += "Clickable";
					}
					CRunPVToolkitSkillWheelTipsTopsMarkButton markButton = new CRunPVToolkitSkillWheelTipsTopsMarkButton(row, 
							new String[]{"class"}, 
							new Object[]{styleClass}
							);
					//NOTE if ordered marked tips or tops will be shown on top
					if (feedback.isMark()) {
						markButton.setAttribute("orderValue", "1");
					}
					else {
						markButton.setAttribute("orderValue", "2");
					}
					if (!_compareWithExperts) {
						markButton.init(_actor, _editable, feedback, _idPrefix, _presentationLevel);
					}
				}
	
				rowNumber ++;
			}
		}
		return rowNumber;
	}

}
