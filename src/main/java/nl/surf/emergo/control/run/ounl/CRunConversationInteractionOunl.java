/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefInclude;
import nl.surf.emergo.control.run.CRunArea;
import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.control.run.CRunConversationInteraction;
import nl.surf.emergo.control.run.CRunConversations;
import nl.surf.emergo.control.run.CRunHbox;
import nl.surf.emergo.control.run.CRunInteractionTree;
import nl.surf.emergo.view.VView;

/**
 * The Class CRunConversationInteractionOunl is used for interaction during conversations
 * and is shown within the run choice area.
 */
public class CRunConversationInteractionOunl extends CRunConversationInteraction {

	private static final long serialVersionUID = -7959191736501321493L;

	/**
	 * Instantiates a new c run conversation interaction.
	 * And creates content area.
	 */
	public CRunConversationInteractionOunl() {
		super();
	}

	/**
	 * Instantiates a new c run conversation interaction.
	 * And creates content area.
	 *
	 * @param aId the a id
	 * @param aRunComponent the a run component
	 * @param aRunConversationsId the a run conversations id
	 */
	public CRunConversationInteractionOunl(String aId, CRunComponent aRunComponent, String aRunConversationsId) {
		super(aId, aRunComponent, aRunConversationsId);
	}

	/**
	 * Instantiates a new c run conversation interaction.
	 * Creates button area with close and ask question button.
	 * And creates content area.
	 *
	 * @param aId the a id
	 * @param aRunComponent the a run component
	 * @param aRunConversationsId the a run conversations id
	 * @param aNotifyRunWnd the a notify run wnd
	 */
	public CRunConversationInteractionOunl(String aId, CRunComponent aRunComponent, String aRunConversationsId, boolean aNotifyRunWnd) {
		super(aId, aRunComponent, aRunConversationsId, aNotifyRunWnd);
	}

	/**
	 * Creates new content component, the interaction tree.
	 *
	 * @return the component
	 */
	@Override
	protected Component newContentComponent() {
		return (new CRunInteractionTreeOunl("runInteractionTree", runComponent, runConversationsId));
	}

	/**
	 * Re-renders tree items if necessary.
	 *
	 * @param aTag the changed element
	 */
	@Override
	public void update(IXMLTag aTag) {
		super.update(aTag);
		// show conversation title
		Label lLabel = (Label) CDesktopComponents.vView().getComponent(getId()+"Title");
		if (lLabel != null) {
			CRunInteractionTree lInteractionTree = (CRunInteractionTree) CDesktopComponents.vView().getComponent("runInteractionTree");
			lLabel.setValue(CDesktopComponents.sSpring().unescapeXML(lInteractionTree.conversation.getChildValue("name")));
		}
	}

	/**
	 * Creates button area with close and ask question button.
	 *
	 * @param aParent the a parent
	 */
	@Override
	protected CRunHbox createButtonsArea(Component aParent) {
		//NOTE no buttons area
		return null;
	}

	/**
	 * Creates content area and interaction within it.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run area
	 */
	@Override
	protected CRunArea createContentArea(Component aParent) {
		CRunArea lContentArea = super.createContentArea(aParent);
		Include lIntervention = new CDefInclude();
		lIntervention.setId(getId() + "Intervention");
		lIntervention.setAttribute("runComponentId", runConversationsId);
		lContentArea.appendChild(lIntervention);
		lIntervention.setVisible(false);
		//NOTE create interaction area to be meant as parent of all conversation tags like fields and buttons, because they are not shown within the (original) tree to show conversation questions.
		CRunArea lInteractionArea = new CRunArea();
		lInteractionArea.setId(getId() + "InteractionArea");
		lContentArea.appendChild(lInteractionArea);
		return lContentArea;
	}

	/**
	 * Sets the status by showing conversation title
	 *
	 * @param aStatus the new status
	 */
	@Override
	public void setStatus(String aStatus) {
		// show conversation title
		Label lLabel = (Label) CDesktopComponents.vView().getComponent(getId()+"Title");
		if (lLabel != null) {
			lLabel.setValue(aStatus + "");
		}
		if (aStatus.equals("intervention") || aStatus.equals("previousintervention") || aStatus.equals("nextintervention")) {
			Include lIntervention = (Include)CDesktopComponents.vView().getComponent(getId() + "Intervention");
			if (lIntervention != null) {
				runInteractionTree.setVisible(false);
				lIntervention.setVisible(aStatus.equals("intervention"));
				lIntervention.setSrc("");
				if (aStatus.equals("intervention")) {
					lIntervention.setSrc(VView.v_run_conversation_intervention_fr);
				}
			}
		}
		else if (aStatus.equals("interventionShowRecord")) {
			Component lWebcamDiv = (Component)CDesktopComponents.vView().getComponent("webcamDiv");
			if (lWebcamDiv != null) {
				Events.postEvent("onShowRecord", lWebcamDiv, null);
				lWebcamDiv.setVisible(true);
			}
		}
		else if (aStatus.equals("interventionRecordComplete")) {
			Component lWebcamDiv = (Component)CDesktopComponents.vView().getComponent("webcamDiv");
			if (lWebcamDiv != null) {
				Events.postEvent("onRecordComplete", lWebcamDiv, null);
			}
		}
		else if (aStatus.equals("interventionBlobReady")) {
			Component lViewVignetBtn = (Component)CDesktopComponents.vView().getComponent("viewVignetBtn");
			if (lViewVignetBtn != null) {
				CRunConversations lRunConversations = (CRunConversations) CDesktopComponents.vView().getComponent(runConversationsId);
				if (lRunConversations != null) {
					lViewVignetBtn.setWidgetListener("onClick", getOnClickWidgetListenerString(lRunConversations.getCurrentFragmentTag()));
					lViewVignetBtn.setVisible(true);
				}
			}
			Component lViewWebcamBtn = (Component)CDesktopComponents.vView().getComponent("viewWebcamBtn");
			if (lViewWebcamBtn != null) {
				CRunConversations lRunConversations = (CRunConversations) CDesktopComponents.vView().getComponent(runConversationsId);
				if (lRunConversations != null) {
					lViewWebcamBtn.setWidgetListener("onClick", getOnClickWidgetListenerString(lRunConversations.getCurrentInterventionStatusTag()));
					lViewWebcamBtn.setVisible(true);
					Events.postEvent("onRecordingAvailable", lViewWebcamBtn, null);
				}
			}
			Component lResitWebcamBtn = (Component)CDesktopComponents.vView().getComponent("resitWebcamBtn");
			if (lResitWebcamBtn != null) {
				Events.postEvent("onRecordingAvailable", lResitWebcamBtn, null);
			}
			Component lPreviousFragmentBtn = (Component)CDesktopComponents.vView().getComponent("previousFragmentBtn");
			if (lPreviousFragmentBtn != null) {
				Events.postEvent("onRecordingAvailable", lPreviousFragmentBtn, null);
			}
			Component lNextFragmentBtn = (Component)CDesktopComponents.vView().getComponent("nextFragmentBtn");
			if (lNextFragmentBtn != null) {
				Events.postEvent("onRecordingAvailable", lNextFragmentBtn, null);
			}
		}
	}
	
	/**
	 * get on click widget listener string.
	 *
	 * @param aBlobContainer the a blob container
	 *
	 * @return the string
	 */
	protected String getOnClickWidgetListenerString(IXMLTag aBlobContainer) {
		if (aBlobContainer == null) {
			return "";
		}
		//first try child of status tag
		String lHref = CDesktopComponents.sSpring().getSBlobHelper().getUrl(aBlobContainer.getChild(AppConstants.statusElement));
		//if empty try child of tag
		if (lHref.equals("")) {
			lHref = CDesktopComponents.sSpring().getSBlobHelper().getUrl(aBlobContainer);
		}
		lHref = CDesktopComponents.sSpring().getSBlobHelper().convertHrefForCertainMediaTypes(lHref);
		if (!CDesktopComponents.vView().isAbsoluteUrl(lHref)) {
			if (!lHref.equals("") && lHref.indexOf("/") != 0)
				lHref = "/" + lHref;
			lHref = CDesktopComponents.vView().getEmergoWebappsRoot() + lHref;
		}
		if (!lHref.equals("")) {
			// avoid javascript unterminated string error
			lHref = lHref.replace("'", "\\'");
			return CDesktopComponents.vView().getJavascriptWindowOpenFullscreen(lHref);
		}
		return "";
	}

}
