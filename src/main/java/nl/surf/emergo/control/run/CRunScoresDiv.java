/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.List;

import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;

public class CRunScoresDiv extends CDefDiv {
	
	private static final long serialVersionUID = -4497007903656019133L;

	public List<List<Object>> scoresDatas = null;
	
	public void onCreate(CreateEvent aEvent) {
		CRunScores runScores = (CRunScores)CDesktopComponents.vView().getComponent(CRunScores.runScoresId);
		if (runScores == null) {
			return;
		}
		scoresDatas = runScores.getScoresDatasLevel1();
		Events.postEvent("onSetStyle", this, null);
	}
	
	public void onSetStyle(Event aEvent) {
		CRunScores runScores = (CRunScores)CDesktopComponents.vView().getComponent(CRunScores.runScoresId);
		if (runScores == null) {
			return;
		}
		String style = "";
		String scoresAccessible = runScores.getScoresAccessible();
		String scoresPositionLeft = runScores.getScoresPositionLeft();
		String scoresPositionTop = runScores.getScoresPositionTop();
		
		if (scoresAccessible.equals("false")) {
			style += "cursor:default;";
		}
		if (!scoresPositionLeft.equals("")) {
			style += "left:" + scoresPositionLeft + "px;";
		}
		if (!scoresPositionTop.equals("")) {
			style += "top:" + scoresPositionTop + "px;";
		}
		else {
			style += "top:"+ (486 - 42 * scoresDatas.size()) + "px;";
		}
		setStyle(style);

	}
	
	public void onUpdate(Event aEvent) {
		CRunScores runScores = (CRunScores)CDesktopComponents.vView().getComponent(CRunScores.runScoresId);
		if (runScores == null) {
			return;
		}
		scoresDatas = runScores.getScoresDatasLevel1();
		HtmlMacroComponent macro = (HtmlMacroComponent)CDesktopComponents.vView().getComponent(CRunScores.macroScoresDatasId);
		macro.setDynamicProperty("a_scores_datas", scoresDatas);
		macro.recreate();
		Events.postEvent("onSetStyle", this, null);
	}
	
	public void onClick(Event aEvent) {
		CRunScores runScores = (CRunScores)CDesktopComponents.vView().getComponent(CRunScores.runScoresId);
		if (runScores == null) {
			return;
		}
		HtmlMacroComponent macro = (HtmlMacroComponent)CDesktopComponents.vView().getComponent(CRunScores.macroScoresOverviewDatasId);
		String scoresAccessible = runScores.getScoresAccessible();
		if (scoresAccessible.equals("true")) {
			//NOTE older author data may have a view relevance of 3, so check voor 3. But essentially 2 and 3 are equal.
			//Now authors may only choose 1 or 2.
			macro.setDynamicProperty("a_scores_datas", runScores.getScoresDatas(3));
			macro.recreate();
		}
		CDesktopComponents.vView().getComponent(CRunScores.divScoresOverviewId).setVisible(true);
	}
	
}
