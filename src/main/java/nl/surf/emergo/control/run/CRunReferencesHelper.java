/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Label;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.Treerow;
import org.zkoss.zul.impl.XulElement;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CCaseHelper;
import nl.surf.emergo.control.CContentHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CTree;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunReferencesHelper. Helps rendering references.
 */
public class CRunReferencesHelper extends CRunComponentHelper {

		/** The max string length. */
	protected int maxStringLength = 80;
	
	/** The c case helper. */
	protected CCaseHelper cCaseHelper = new CCaseHelper();


	/**
	 * Instantiates a new c run references helper.
	 *
	 * @param aTree the ZK references tree
	 * @param aShowTagNames the a show tag names, comma separated, only tags with these names are shown
	 * @param aCaseComponent the references case component
	 * @param aRunComponent the a run component, the ZK references component
	 */
	public CRunReferencesHelper(CTree aTree, String aShowTagNames, IECaseComponent aCaseComponent, CRunComponent aRunComponent) {
		super(aTree, aShowTagNames, aCaseComponent, aRunComponent);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTreeHelper#renderTreecell(org.zkoss.zul.Treeitem, org.zkoss.zul.Treerow, nl.surf.emergo.business.IXMLTag, java.lang.String, boolean)
	 */
	@Override
	protected Treecell renderTreecell(Treeitem aTreeitem, Treerow aTreerow,
			IXMLTag aItem, String aKeyValues, boolean aAccIsAuthor) {
		Treecell lTreecell = getTreerowTreecell(aTreerow, 0);
		String lLabelStr = aKeyValues;
		String lOwnerRgaId = aItem.getAttribute(AppConstants.keyOwner_rgaid);
		if (!lOwnerRgaId.equals("")) {
			lLabelStr = lLabelStr + " (" +CDesktopComponents.sSpring().getRunGroupAccount(Integer.parseInt(lOwnerRgaId)).getERunGroup().getName() +")";
		}
		if (lLabelStr.length() > maxStringLength) {
			lTreecell.setTooltiptext(lLabelStr);
			lLabelStr = lLabelStr.substring(0, maxStringLength) + "...";
		}
		lTreecell.setLabel(lLabelStr);
		String lStatus = "active";
		if (isOpened(aItem)) lStatus = "opened";
		if (!isAccessible(aItem)) lStatus = "inactive";
		lTreecell.setZclass(runComponent.getClassName() + "_treecell_"+lStatus);
		if (aItem.getName().equals("map")) {
			lTreecell.setImage(CDesktopComponents.sSpring().getStyleImgSrc("icon_folder_" + lStatus));
		}
		IXMLTag lItem = aItem;
		if (aItem.getName().equals("refpiece")) {
			String lReftype = aItem.getChild("ref").getDefTag().getAttribute(AppConstants.defKeyReftype);
			List<IXMLTag> lRefTags = new ArrayList<IXMLTag>();
			lRefTags = cCaseHelper.getRefTags(lReftype,""+AppConstants.statusKeySelectedIndex,CDesktopComponents.sSpring().getSCaseRoleHelper().getCaseRole(),caseComponent,aItem);
//			only one ref
			if (lRefTags.size() == 1) {
				lItem = lRefTags.get(0);
			}
		}
		if (aItem.getName().equals("piece") || aItem.getName().equals("refpiece")) {
			//NOTE use getChildAttribute value of CRunContentHelper, because references can have user generated content
			String lMediatype = getNodeChildAttribute(lItem, "blob", AppConstants.keyMediatype);
			if (lMediatype.equals("text")) lMediatype = "document";
			if (lMediatype.equals("")) lMediatype = "other";
			lTreecell.setImage(CDesktopComponents.sSpring().getStyleImgSrc("icon_" + lMediatype + "_" + lStatus));
			lTreecell.setWidgetListener("onClick", null);
			if (aItem != null && isAccessible(aItem)) {
				String lHref = CDesktopComponents.sSpring().getSBlobHelper().convertHrefForCertainMediaTypes(CDesktopComponents.sSpring().getSBlobHelper().getUrl(lItem));
				if (!lHref.equals("") && !CDesktopComponents.vView().isAbsoluteUrl(lHref)) {
					if (lHref.indexOf("/") != 0) lHref = "/" + lHref;
					lHref = CDesktopComponents.vView().getEmergoWebappsRoot() + lHref;
				}
				if (!lHref.equals("")) {
					// avoid javascript unterminated string error
					lHref = lHref.replace("'", "\\'");
					if (lHref.contains(".zul")) {
						lTreecell.setWidgetListener("onClick", CDesktopComponents.vView().getJavascriptWindowOpenFullscreen(lHref));
					} 
					else {
						lTreecell.setWidgetListener("onClick", CDesktopComponents.vView().getJavascriptWindowLocationHref(lHref));
					}
				}
			}
		}
		if (aItem.getName().equals(AppConstants.contentElement)) {
			String lTagName = CContentHelper.getNodeTagLabel(caseComponent.getEComponent().getCode(), aItem.getName());
			Label lLabel = getComponentLabel(lTreecell, "contentelement", lTagName + " ");
			lLabel.setZclass(runComponent.getClassName() + "_treecell_"+lStatus);
		}
		return lTreecell;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunComponentHelper#setContextMenu(org.zkoss.zk.ui.Component)
	 */
	public void setContextMenu(Component aObject) {
		if (extendable)
			((XulElement) aObject).setContext("runMenuPopup");
	}
}