/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zul.Treeitem;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunInteractionTree.
 */
public class CRunInteractionTree extends CRunTree {

	private static final long serialVersionUID = -3264818951563572259L;

	protected String runConversationsId = "runConversations";
	
	/** The conversation the interaction belong to. */
	public IXMLTag conversation = null;

	/**
	 * Instantiates a new c run interaction tree.
	 *
	 * @param aId the a id
	 * @param aRunComponent the a run component, the ZK conversations component
	 * @param aRunConversationsId the a run conversations id
	 */
	public CRunInteractionTree(String aId, CRunComponent aRunComponent, String aRunConversationsId) {
		super(aId, null, aRunComponent);
		runConversationsId = aRunConversationsId;
		tagopenednames = "question,map,alternative";
		setRows(6);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTree#getRunComponentHelper()
	 */
	@Override
	public CRunComponentHelper getRunComponentHelper() {
		return new CRunConversationsHelper(this, tagopenednames, caseComponent, runComponent);
	}

	/**
	 * Shows interaction.
	 *
	 * @param aCaseComponent the a case component
	 * @param aConversation the a conversation
	 */
	public void showInteraction(IECaseComponent aCaseComponent,
			IXMLTag aConversation) {
		caseComponent = aCaseComponent;
		conversation = aConversation;
		update(conversation);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTree#setTreeitemStatus(org.zkoss.zul.Treeitem)
	 */
	@Override
	public Treeitem setTreeitemStatus(Treeitem aTreeitem) {
		Treeitem lTreeitem = aTreeitem;
		IXMLTag tag = getContentItemTag(lTreeitem);
		if (tag != null) {
			lTreeitem = setRunTagStatus(lTreeitem, tag,
					AppConstants.statusKeyOutfoldable,
					AppConstants.statusValueTrue, tagopenednames, true);
//			maybe add something for shared component
		}
		lTreeitem = super.setTreeitemStatus(lTreeitem);
		return lTreeitem;
	}
	
	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTree#doTreeitemAction(org.zkoss.zul.Treeitem)
	 */
	@Override
	public void doTreeitemAction(Treeitem aTreeitem) {
		IXMLTag tag = getContentItemTag(aTreeitem);
		if (tag == null)
			return;
		String lName = tag.getName();
		if (!(lName.equals("question") || lName.equals("alternative")))
			return;
		CRunConversations lComp = (CRunConversations) CDesktopComponents.vView().getComponent(runConversationsId);
		if (lComp == null)
			return;
		for (IXMLTag childtag : tag.getChilds("fragment")) {
			lName = childtag.getName();
			if (lName.equals("fragment")) {
				boolean lPresent = false;
				boolean lOpened = false;
				lPresent = (CDesktopComponents.sSpring().getCurrentTagStatus(childtag, AppConstants.statusKeyPresent).equals(AppConstants.statusValueTrue));
				lOpened = (CDesktopComponents.sSpring().getCurrentTagStatus(childtag, AppConstants.statusKeyOpened).equals(AppConstants.statusValueTrue));
				if (lPresent && lOpened) {
					lComp.playFragment(conversation, childtag, true);
					return;
				}
			}
		}
		lComp.playFragment(conversation, null, true);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTree#reRenderTreeitem(org.zkoss.zul.Treeitem, nl.surf.emergo.business.IXMLTag, boolean)
	 */
	public Treeitem reRenderTreeitem(Treeitem aTreeitem, IXMLTag aTag, boolean aReRenderChilds) {
		return super.reRenderTreeitem(aTreeitem, aTag, aReRenderChilds);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTree#update(nl.surf.emergo.business.IXMLTag)
	 */
	@Override
	public void update(IXMLTag aTag) {
		if (aTag == null) {
			aTag = conversation;
		}
		if (aTag.getAttribute(AppConstants.keyId) == conversation.getAttribute(AppConstants.keyId)) {
			super.update(aTag);
		}
		else {
			CRunComponentHelper cComponent = getRunComponentHelper();
			IXMLTag lRoot = cComponent.getXmlDataPlusStatusTree();
			cComponent.setItemAttributes(this, lRoot.getChild(AppConstants.contentElement));
			if (aTag.getName().equals("fragment")) {
				// rerender question or alternative, because status may be changed
				aTag = aTag.getParentTag();
				if (!(aTag.getName().equals("question") || aTag.getName().equals("alternative"))) {
					//NOTE only rerender questions, maps or alternatives
					return;
				}
			}
			Treeitem lTreeItem = getContentItem(aTag.getAttribute(AppConstants.keyId));
			if (lTreeItem != null) {
				lTreeItem = reRenderTreeitem(lTreeItem, aTag, true);
			}
			else {
				// treeitem not found - possibly removed -> rerender complete conversation
				aTag = conversation;
				super.update(aTag);
			}
		}
	}

}
