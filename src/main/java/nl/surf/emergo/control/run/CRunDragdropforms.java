/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SDatatagReference;
import nl.surf.emergo.zkspring.STriggeredReference;

/**
 * The Class CRunDragdropforms is used to show dragdropforms component within
 * the run view area of the emergo player.
 */
public class CRunDragdropforms extends CRunForms {

	private static final long serialVersionUID = 7076824211797266474L;

	/**
	 * Variables originally only used in JAVA file.
	 */

	protected boolean areItemTagsRestored = false;

	protected Hashtable<IXMLTag, List<IXMLTag>> copyContainerChildTags = new Hashtable<IXMLTag, List<IXMLTag>>();

	protected Hashtable<IXMLTag, List<IXMLTag>> copyContainerItemTags = new Hashtable<IXMLTag, List<IXMLTag>>();

	protected Hashtable<IXMLTag, List<Boolean>> copyContainerItemTagsMoved = new Hashtable<IXMLTag, List<Boolean>>();

	// NOTE used for alternative for drag and drop
	public Component draggedComponent = null;
	public boolean ignoreOnClick = false;

	/**
	 * Methods originally only used in JAVA file.
	 */

	/**
	 * Instantiates a new c run dragdropforms.
	 */
	public CRunDragdropforms() {
		super("runDragdropforms", null);
		conversationsFeedbackIdSuffix = "Dragdropforms";
		init();
	}

	/**
	 * Instantiates a new c run dragdropforms.
	 *
	 * @param aId            the a id
	 * @param aCaseComponent the case component
	 */
	public CRunDragdropforms(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
		conversationsFeedbackIdSuffix = "Dragdropforms";
		init();
	}

	/**
	 * Creates new content component, the tree.
	 *
	 * @return the component
	 */
	@Override
	protected Component newContentComponent() {
		return newContentComponent("runDragdropformsView");
	}

	/**
	 * Creates view.
	 */
	public void init() {
		containerChildTagNames = "text,document";

		Events.postEvent("onInitZulfile", this, null);
	}

	/**
	 * Sets containerok for item tags.
	 *
	 * @param aContainerTag the a container tag
	 */
	protected void setContainerokForItemTags(IXMLTag aContainerTag) {
		// NOTE get all item tags
		List<IXMLTag> lItemTags = getAllItemTags(aContainerTag);
		for (IXMLTag lItemTag : lItemTags) {
			SDatatagReference lDatatagReference = CDesktopComponents.sSpring().getSReferencedDataTagHelper()
					.getDatatagReference(getCaseComponent(), lItemTag, "refcontainer");
			if (lDatatagReference != null && lDatatagReference.getCaseComponent() == getCaseComponent()
					&& lDatatagReference.getDataTag() != null) {
				// cannot be dropped outside current case component
				// determine containerok
				boolean lContainerok = lDatatagReference.getDataTag() == lItemTag.getParentTag();
				setRunTagStatus(getCaseComponent(), lItemTag, AppConstants.statusKeyContainerok, "" + lContainerok,
						false, true);
			}
		}
	}

	/**
	 * Sets containerpos, containerposok and databeforeid for item tags within
	 * aContainerTag.
	 *
	 * @param aContainerTag the a container tag
	 */
	protected void setContainerposAndContainerposokForItemTags(IXMLTag aContainerTag) {
		// NOTE get only present item tags
		List<IXMLTag> lItemTags = getItemTags(aContainerTag);
		int lContainerpos = 1;
		for (IXMLTag lItemTag : lItemTags) {
			String lOldContainerpos = lItemTag.getCurrentStatusAttribute(AppConstants.statusKeyContainerpos);
			if (!("" + lContainerpos).equals(lOldContainerpos)) {
				setRunTagStatus(getCaseComponent(), lItemTag, AppConstants.statusKeyContainerpos, "" + lContainerpos,
						false, true);
				// determine containerposok
				boolean lContainerposok = isContainerposOk(lItemTag);
				setRunTagStatus(getCaseComponent(), lItemTag, AppConstants.statusKeyContainerposok,
						"" + lContainerposok, false, true);
			}
			lContainerpos++;
		}
	}

	/**
	 * Initializes item tags. Item tags are part of a container and might have to be
	 * randomized, and their containerpos attribute has to be set to an initial
	 * value
	 *
	 * @param aFormTags the a form tags
	 */
	protected void initializeItemTags(List<IXMLTag> aFormTags) {
		for (IXMLTag lFormTag : aFormTags) {
			// NOTE get all container tags, also not present ones
			List<IXMLTag> lContainerTags = lFormTag.getChilds("container");
			for (IXMLTag lContainerTag : lContainerTags) {
				// randomize item tags of container if needed
				boolean lRandomizeitems = lContainerTag.getCurrentStatusAttribute(AppConstants.statusKeyRandomizeitems)
						.equals(AppConstants.statusValueTrue);
				if (lRandomizeitems) {
					boolean lRandomized = lContainerTag.getCurrentStatusAttribute(AppConstants.statusKeyRandomized)
							.equals(AppConstants.statusValueTrue);
					if (!lRandomized) {
						// NOTE get all original child tags of container tag, because it is no copy.
						List<IXMLTag> lAllOriginalChildTags = lContainerTag.getChildTags();
						// NOTE get all item tags, also not present ones
						List<IXMLTag> lItemTags = getAllItemTags(lContainerTag);
						int lMaxPosition = lItemTags.size();
						// NOTE values in lPositions are > 0 and <= lMaxPosition
						List<Integer> lPositions = getRandomizedListOfIntegers(lMaxPosition);
						for (Integer lPosition : lPositions) {
							IXMLTag lItemTag = lItemTags.get(lPosition - 1);
							// NOTE use the original child tags to move lItemTag
							lAllOriginalChildTags.remove(lItemTag);
							lAllOriginalChildTags.add(lItemTag);
						}
						setRunTagStatus(getCaseComponent(), lContainerTag, AppConstants.statusKeyRandomized,
								AppConstants.statusValueTrue, true);
					}
				}
				// initialize item tags of container if needed, containerpos is set
				boolean lInitialized = lContainerTag.getCurrentStatusAttribute(AppConstants.statusKeyInitialized)
						.equals(AppConstants.statusValueTrue);
				if (!lInitialized) {
					setContainerokForItemTags(lContainerTag);
					setContainerposAndContainerposokForItemTags(lContainerTag);
					setRunTagStatus(getCaseComponent(), lContainerTag, AppConstants.statusKeyInitialized,
							AppConstants.statusValueTrue, true);
				}
			}
		}
	}

	/**
	 * Restores item tags. Item tags might be dragged into another container or to
	 * another position, so restore this situation.
	 *
	 * @param aFormTags the a form tags
	 */
	protected void restoreItemTags(List<IXMLTag> aFormTags) {
		// NOTE if component is closed the merged data plus status tree still is in
		// memory.
		// When reopening the component, areItemTagsRestored is false so code below is
		// executed while it should not be necessary, but it does no harm.
		// I tried to fix this by setting a component status that is not saved in the
		// database (so is only existing during the current session),
		// but this option (not saving in database) does not work anymore, because data
		// and status are not merged anymore after every status change.
		//
		// TODO Using case start time and opened time could work, but opened is already
		// set before this method is called, so it should be previous opened time.
		if (!areItemTagsRestored) {
			areItemTagsRestored = true;
			Hashtable<String, IXMLTag> lHAllContainerTags = new Hashtable<String, IXMLTag>();
			Hashtable<String, IXMLTag> lHAllItemTags = new Hashtable<String, IXMLTag>();
			List<IXMLTag> lAllItemTags = new ArrayList<IXMLTag>();
			// get all container and item tags
			for (IXMLTag lFormTag : aFormTags) {
				List<IXMLTag> lContainerTags = getContainerTags(lFormTag);
				for (IXMLTag lContainerTag : lContainerTags) {
					lHAllContainerTags.put(lContainerTag.getAttribute(AppConstants.keyId), lContainerTag);
				}
				for (IXMLTag lContainerTag : lContainerTags) {
					List<IXMLTag> lItemTags = getItemTags(lContainerTag);
					for (IXMLTag lItemTag : lItemTags) {
						lHAllItemTags.put(lItemTag.getAttribute(AppConstants.keyId), lItemTag);
					}
					lAllItemTags.addAll(lItemTags);
					// NOTE if copy item container tag store copied item tags in
					// copyContainerItemTags
					// and initialize if item tag is moved
					boolean lCopyItems = lContainerTag.getChild(AppConstants.statusElement)
							.getDefAttribute(AppConstants.statusKeyCopy).equals(AppConstants.statusValueTrue);
					if (lCopyItems) {
						List<IXMLTag> lNewChildTags = new ArrayList<IXMLTag>();
						for (IXMLTag lChildTag : lContainerTag.getChildTags()) {
							lNewChildTags.add(lChildTag);
						}
						copyContainerChildTags.put(lContainerTag, lNewChildTags);
						List<IXMLTag> lNewItemTags = new ArrayList<IXMLTag>();
						List<Boolean> lItemTagsMoved = new ArrayList<Boolean>();
						for (IXMLTag lItemTag : lItemTags) {
							lNewItemTags.add(lItemTag);
							lItemTagsMoved.add(false);
						}
						copyContainerItemTags.put(lContainerTag, lNewItemTags);
						copyContainerItemTagsMoved.put(lContainerTag, lItemTagsMoved);
					}
				}
			}
			// loop through all item tags
			// NOTE first for all items set container tag if it is not equal to original
			// container tag
			for (IXMLTag lItemTag : lAllItemTags) {
				String lOriginalParentTagId = lItemTag.getParentTag().getAttribute(AppConstants.keyId);
				String lParentTagId = lItemTag.getCurrentStatusAttribute(AppConstants.statusKeyDataparentid);
				if (!lParentTagId.equals("")) {
					// if item is dragged to other container, may be original container too if
					// dragged more than once
					IXMLTag lParentTag = null;
					if (!lParentTagId.equals(lOriginalParentTagId) && lHAllContainerTags.containsKey(lParentTagId)) {
						// if parent tag not equal to original parent and parent tag exists
						lParentTag = lHAllContainerTags.get(lParentTagId);
					}
					if (lParentTag != null) {
						// NOTE store that item tag is moved which may happen if a container has
						// statusKeyCopy true
						if (copyContainerItemTags.containsKey(lItemTag.getParentTag())) {
							int lIndex = copyContainerItemTags.get(lItemTag.getParentTag()).indexOf(lItemTag);
							if (lIndex >= 0) {
								copyContainerItemTagsMoved.get(lItemTag.getParentTag()).set(lIndex, true);
							}
						}
						// move item tag to other container if it has another parent tag id
						// remove from old parent
						lItemTag.getParentTag().getChildTags().remove(lItemTag);
						// add to new parent
						lItemTag.setParentTag(lParentTag);
						lParentTag.getChildTags().add(lItemTag);
					}
				}
			}
			// NOTE then for all items restore right position in container tag
			for (IXMLTag lFormTag : aFormTags) {
				List<IXMLTag> lContainerTags = getContainerTags(lFormTag);
				for (IXMLTag lContainerTag : lContainerTags) {
					List<IXMLTag> lItemTags = getItemTags(lContainerTag);
					IXMLTag[] lSortedItemTagsArr = new IXMLTag[lItemTags.size()];
					// NOTE It appears that containerpos sometimes may be out of array range so
					// assemble these item tags in lItemTagsOutOfRange ...
					List<IXMLTag> lItemTagsOutOfRange = new ArrayList<IXMLTag>();
					for (IXMLTag lItemTag : lItemTags) {
						int lContainerpos = Integer
								.parseInt(lItemTag.getCurrentStatusAttribute(AppConstants.statusKeyContainerpos));
						if ((lContainerpos < 1) || (lContainerpos > lItemTags.size())) {
							lItemTagsOutOfRange.add(lItemTag);
						} else {
							lSortedItemTagsArr[lContainerpos - 1] = lItemTag;
						}
					}
					if (lItemTagsOutOfRange.size() > 0) {
						// NOTE ... and put them into open places
						int lNSInd = 0;
						for (int i = 0; i < lSortedItemTagsArr.length; i++) {
							if (lSortedItemTagsArr[i] == null) {
								if (lNSInd < lItemTagsOutOfRange.size()) {
									IXMLTag lItemTag = lItemTagsOutOfRange.get(lNSInd);
									if (lItemTag != null) {
										setRunTagStatus(getCaseComponent(), lItemTag,
												AppConstants.statusKeyContainerpos, "" + (i + 1), false, true);
										// determine containerposok
										boolean lContainerposok = isContainerposOk(lItemTag);
										setRunTagStatus(getCaseComponent(), lItemTag,
												AppConstants.statusKeyContainerposok, "" + lContainerposok, false,
												true);
										lSortedItemTagsArr[i] = lItemTag;
									}
									lNSInd++;
								}
							}
						}
					}
					List<IXMLTag> lSortedItemTags = Arrays.asList(lSortedItemTagsArr);
					lContainerTag.getChildTags().removeAll(lItemTags);
					lContainerTag.getChildTags().addAll(lSortedItemTags);
				}
			}
		}
	}

	/**
	 * Gets form tags. Item tags might be dragged in another container, so put them
	 * there in the right order. Also the initial order of the item tags depends on
	 * the order questions were presented or maybe were asked.
	 *
	 * @return the form tags
	 */
	@Override
	public List<IXMLTag> getFormTags() {
		// NOTE get all form tags, also not present ones
		List<IXMLTag> lFormTags = getDataPlusStatusRootTag().getChild(AppConstants.contentElement).getChilds("form");
		initializeItemTags(lFormTags);
		// NOTE only get present form tags
		lFormTags = getPresentChildTags(getDataPlusStatusRootTag().getChild(AppConstants.contentElement), "form");
		restoreItemTags(lFormTags);
		return lFormTags;
	}

	@Override
	/**
	 * Gets item tags.
	 *
	 * @param aParentTag the a parent tag
	 * 
	 * @return the item tags
	 */
	public List<IXMLTag> getItemTags(IXMLTag aParentTag) {
		return getPresentChildTags(aParentTag, containerChildTagNames);

	}

	/**
	 * Gets all item tags, that is also tags that are not present.
	 *
	 * @param aParentTag the a parent tag
	 * 
	 * @return the item tags
	 */
	@Override
	public List<IXMLTag> getAllItemTags(IXMLTag aParentTag) {
		return aParentTag.getChilds(containerChildTagNames);
	}

	/**
	 * Gets all item tags for zul, that is also tags that are not present. If a
	 * container has statusKeyCopy true original item tags are given
	 *
	 * @param aParentTag the a parent tag
	 * 
	 * @return the item tags
	 */
	public List<IXMLTag> getAllItemTagsForZul(IXMLTag aParentTag) {
		boolean lCopyItems = aParentTag.getChild(AppConstants.statusElement).getDefAttribute(AppConstants.statusKeyCopy)
				.equals(AppConstants.statusValueTrue);
		if (lCopyItems) {
			return copyContainerItemTags.get(aParentTag);
		} else {
			return aParentTag.getChilds(containerChildTagNames);
		}
	}

	/**
	 * Gets info on all item tags moved for zul, If a container has statusKeyCopy
	 * true then the result is not empty
	 *
	 * @param aParentTag the a parent tag
	 * 
	 * @return the boolean list
	 */
	public List<Boolean> getAllItemTagsMovedForZul(IXMLTag aParentTag) {
		return copyContainerItemTagsMoved.get(aParentTag);
	}

	/**
	 * Is containerpos ok.
	 *
	 * @param aItemTag the a item tag
	 * 
	 * @return boolean
	 */
	protected boolean isContainerposOk(IXMLTag aItemTag) {
		// determine containerpos
		int lContainerpos = 1 + getItemTags(aItemTag.getParentTag()).indexOf(aItemTag);
		int lRequiredContainerpos = -1;
		try {
			lRequiredContainerpos = Integer.parseInt(aItemTag.getChildValue("refcontainerpos"));
		} catch (NumberFormatException e) {
			// if no valid position is filled in, position does not matter, so is ok.
			lRequiredContainerpos = lContainerpos;
		}
		return lContainerpos == lRequiredContainerpos;
	}

	/**
	 * Drop item.
	 *
	 * @param aDraggedTag       the a dragged tag
	 * @param aDroppedTag       the a dropped tag
	 * @param aDroppedParentTag the a dropped parent tag
	 *
	 * @return if successful
	 */
	public boolean dropItem(IXMLTag aDraggedTag, IXMLTag aDroppedTag, IXMLTag aDroppedParentTag) {
		if (aDraggedTag == null || aDroppedTag == null) {
			return false;
		}
		// Get the container aDraggedTag should be placed in, the correct one
		SDatatagReference lDatatagReference = CDesktopComponents.sSpring().getSReferencedDataTagHelper()
				.getDatatagReference(getCaseComponent(), aDraggedTag, "refcontainer");
		if (lDatatagReference == null || lDatatagReference.getCaseComponent() != getCaseComponent()) {
			// cannot be dropped outside current case component
			return false;
		}

		// Determine original parent tag
		IXMLTag lOriginalParentTag = aDraggedTag.getParentTag();

		// Possibly move aDraggedTag and determine (new) parent tag and before tag, that
		// is the tag before aDraggedTag is placed
		// Before tag may be null if aDraggedTag will be the last child of the parent
		// tag.
		// NOTE moving dragged tag in xml tree is no problem, because the tree is a copy
		// of the data tree
		IXMLTag lParentTag = null;
		IXMLTag lBeforeTag = null;
		aDraggedTag.getParentTag().getChildTags().remove(aDraggedTag);
		if (aDroppedTag.getName().equals("container")) {
			lParentTag = aDroppedTag;
			lParentTag.getChildTags().add(aDraggedTag);
		} else {
			lParentTag = aDroppedParentTag;
			lBeforeTag = aDroppedTag;
			int lDroppedTagIndex = lParentTag.getChildTags().indexOf(aDroppedTag);
			// NOTE to be sure check if aDroppedTag is a child of lParentTag
			// NOTE if a copied tag (from a container with copyTag true) is moved back,
			// lDroppedTagIndex will be < 0
			// and the tag is not inserted but added, so its position is not right anymore.
			// However, this no problem because for a container with copyTag true always
			// original tags are shown.
			if (lDroppedTagIndex >= 0) {
				// if so add aDraggedTag before aDroppedTag
				lParentTag.getChildTags().add(lDroppedTagIndex, aDraggedTag);
			} else {
				// if not add aDraggedTag as child
				lParentTag.getChildTags().add(aDraggedTag);
			}
		}
		aDraggedTag.setParentTag(lParentTag);
		boolean isDroppedInOtherContainer = lParentTag != lOriginalParentTag;

		// determine parent id and before id, before tag might be null
		String lParentid = lParentTag.getAttribute(AppConstants.keyId);
		String lBeforeid = "";
		if (lBeforeTag != null) {
			lBeforeid = lBeforeTag.getAttribute(AppConstants.keyId);
		}

		// get current containerok value
		boolean lCurrentContainerok = sSpringHelper.getCurrentStatusTagStatusChildAttribute(aDraggedTag,
				AppConstants.statusKeyContainerok, AppConstants.statusValueTrue).equals(AppConstants.statusValueTrue);

		// determine containerok
		boolean lContainerok = lDatatagReference.getDataTag() == lParentTag;

		// determine containerpos
		int lContainerpos = 1 + getItemTags(lParentTag).indexOf(aDraggedTag);

		// determine containerposok
		boolean lContainerposok = isContainerposOk(aDraggedTag);

		setRunTagStatus(getCaseComponent(), aDraggedTag, AppConstants.statusKeyDataparentid, lParentid, true);

		if (lContainerok != lCurrentContainerok) {
			setRunTagStatus(getCaseComponent(), aDraggedTag, AppConstants.statusKeyContainerok, "" + lContainerok,
					true);
		}
		setRunTagStatus(getCaseComponent(), aDraggedTag, AppConstants.statusKeyContainerpos, "" + lContainerpos, true);
		setRunTagStatus(getCaseComponent(), aDraggedTag, AppConstants.statusKeyContainerposok, "" + lContainerposok,
				true);

		// NOTE if dropped in another container the tags originally positioned after
		// aDraggedTag should be set/updated too
		if (isDroppedInOtherContainer) {
			setContainerposAndContainerposokForItemTags(lOriginalParentTag);
		}

		// NOTE if lBeforeTag != null, containerpos of lBeforeTag and tags thereafter
		// have to be set/changed too
		setContainerposAndContainerposokForItemTags(lParentTag);

		return true;
	}

	@Override
	public String getFeedbackTitle() {
		return CDesktopComponents.vView().getLabel("run_dragdropforms.alert.feedback.title");
	}

	@Override
	public String getDefaultFeedbackText() {
		return CDesktopComponents.vView().getLabel("run_dragdropforms.alert.feedback.defaulttext");
	}

	@Override
	public void handleStatusChange(STriggeredReference aTriggeredReference) {
		// NOTE if present of container tag or one of its children changes containerpos
		// and containerposok have to be determined again for the container tag children
		IXMLTag lContainerTag = null;
		if (aTriggeredReference.getDataTag() != null
				&& aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyPresent)) {
			if (aTriggeredReference.getDataTag().getName().equals("container")) {
				lContainerTag = aTriggeredReference.getDataTag();
			} else if (aTriggeredReference.getDataTag().getName().equals("text")
					|| aTriggeredReference.getDataTag().getName().equals("document")) {
				lContainerTag = aTriggeredReference.getDataTag().getParentTag();
			}
		}
		if (lContainerTag != null) {
			setContainerposAndContainerposokForItemTags(lContainerTag);
		}
		Events.postEvent("onHandleStatusChange", this, aTriggeredReference);
	}

	/**
	 * Methods originally used in ZUL file.
	 */

	public void onInitZulfile(Event aEvent) {
		String componentStyle = "";
		String url = getBackgroundUrl();
		if (!url.equals("")) {
			componentStyle = "background-image:url('" + url + "');";
		}

		IXMLTag triggeredFeedbackConditionTag = getTriggeredFeedbackConditionTag();
		if (triggeredFeedbackConditionTag != null) {
			// NOTE only show feedback if it is an instruction, which is indicated by a
			// score of -1
			if (sSpringHelper.getTagChildValue(triggeredFeedbackConditionTag, "score", "-1").equals("-1")) {
				Events.postEvent("onShowFeedback", this, null);
			}
		}

		// NOTE showFeedback and showScore are for all forms!
		showFeedback = false;
		showScore = false;
		dataElements = new ArrayList<Hashtable<String, Object>>();
		List<IXMLTag> formTags = getFormTags();
		for (IXMLTag formTag : formTags) {
			Hashtable<String, Object> hFormDataElements = new Hashtable<String, Object>();
			hFormDataElements.put("tag", formTag);
			hFormDataElements.put("title", sSpringHelper.getTagChildValue(formTag, "title", ""));
			hFormDataElements.put("hovertext", sSpringHelper.getTagChildValue(formTag, "hovertext", ""));

			// NOTE following list is needed so you can do nested foreach looping! see
			// below.
			List<Hashtable<String, Object>> pieceOrRefpieceDataElementsList = new ArrayList<Hashtable<String, Object>>();
			List<IXMLTag> pieceOrRefpieceTags = getPieceAndRefpieceTags(formTag);
			handlepieceOrRefpieces(pieceOrRefpieceDataElementsList, pieceOrRefpieceTags);
			hFormDataElements.put("pieces", pieceOrRefpieceDataElementsList);

			if (sSpringHelper.getTagStatusChildAttribute(formTag, "showfeedback", "true").equals("true")) {
				showFeedback = true;
			}
			if (sSpringHelper.getTagStatusChildAttribute(formTag, "showscore", "false").equals("true")) {
				showScore = true;
			}
			boolean isShowIfRightOrWrong = isShowIfRightOrWrong(formTag);

			// NOTE following list is needed so you can do nested foreach looping! see
			// below.
			List<Hashtable<String, Object>> containerDataElementsList = new ArrayList<Hashtable<String, Object>>();
			List<IXMLTag> containerTags = getAllContainerTags(formTag);
			for (IXMLTag containerTag : containerTags) {
				Hashtable<String, Object> hContainerDataElements = new Hashtable<String, Object>();
				hContainerDataElements.put("tag", containerTag);
				hContainerDataElements.put("title", sSpringHelper.getTagChildValue(containerTag, "title", ""));
				hContainerDataElements.put("hovertext", sSpringHelper.getTagChildValue(containerTag, "hovertext", ""));
				String childOrientation = sSpringHelper.getTagChildValue(containerTag, "childorientation", "vertical");
				hContainerDataElements.put("childorientation", childOrientation);
				hContainerDataElements.put("styleposition", runWnd.getStylePosition(containerTag));
				hContainerDataElements.put("stylesize", runWnd.getStyleSize(containerTag));
				hContainerDataElements.put("styletitlewidth", getStyleTitleWidth(containerTag));
				hContainerDataElements.put("droppable", sSpringHelper.getTagStatusChildAttribute(containerTag,
						AppConstants.statusKeyDroppable, "true"));
				hContainerDataElements.put("copy",
						sSpringHelper.getTagStatusChildAttribute(containerTag, AppConstants.statusKeyCopy, "false"));
				hContainerDataElements.put("present", sSpringHelper
						.getCurrentStatusTagStatusChildAttribute(containerTag, AppConstants.statusKeyPresent, "true"));
				hContainerDataElements.put("items", getItemList(containerTag, isShowIfRightOrWrong, childOrientation));
				containerDataElementsList.add(hContainerDataElements);
			}
			hFormDataElements.put("containers", containerDataElementsList);

			// NOTE following list is needed so you can do nested foreach looping! see
			// below.
			pieceOrRefpieceDataElementsList = new ArrayList<Hashtable<String, Object>>();
			IXMLTag feedbackConditionTag = getCurrentFeedbackConditionTag(formTag);
			pieceOrRefpieceTags = getPieceAndRefpieceTags(feedbackConditionTag);
			handlepieceOrRefpieces(pieceOrRefpieceDataElementsList, pieceOrRefpieceTags);
			hFormDataElements.put("feedbackpieces", pieceOrRefpieceDataElementsList);

			dataElements.add(hFormDataElements);
		}

		Map<String, Object> propertyMap = new HashMap<String, Object>();
		propertyMap.put("currentEmergoComponent", this);
		propertyMap.put("dataElements", dataElements);
		propertyMap.put("componentStyle", componentStyle);
		propertyMap.put("feedbackButtonUrl", sSpringHelper.stripEmergoPath(getFeedbackButtonUrl()));
		propertyMap.put("tooltipId", VView.tooltipId);
		((HtmlMacroComponent) getRunContentComponent()).setDynamicProperty("a_propertyMap", propertyMap);
		((HtmlMacroComponent) getRunContentComponent()).setMacroURI(VView.v_run_dragdropforms_fr);
		getRunContentComponent().setVisible(true);
	}

	public void onShowFeedback(Event aEvent) {
		onShowFeedback(aEvent, "dragdropformItem_", "onUpdate");
	}

	public void onHandleStatusChange(Event aEvent) {
		// NOTE to handle pieces and refpieces call super
		super.onHandleStatusChange(aEvent, "dragdropformItemDiv_");
		STriggeredReference triggeredReference = (STriggeredReference) aEvent.getData();
		if (triggeredReference != null) {
			if (triggeredReference.getDataTag() != null
					&& (triggeredReference.getDataTag().getName().equals("container")
							|| triggeredReference.getDataTag().getName().equals("text")
							|| triggeredReference.getDataTag().getName().equals("document"))
					&& triggeredReference.getStatusKey().equals(AppConstants.statusKeyPresent)) {
				Component lComponent = CDesktopComponents.vView().getComponent(
						"dragdropformItemBox_" + triggeredReference.getDataTag().getAttribute(AppConstants.keyId));
				if (lComponent == null) {
					lComponent = CDesktopComponents.vView().getComponent(
							"dragdropformItem_" + triggeredReference.getDataTag().getAttribute(AppConstants.keyId));
				}
				if (lComponent != null) {
					lComponent.setVisible(triggeredReference.getStatusValue().equals(AppConstants.statusValueTrue));
				}
			}
		}
	}

	protected List<Hashtable<String, Object>> getItemList(IXMLTag parentTag, boolean isShowIfRightOrWrong,
			String childOrientation) {
		// NOTE following list is needed so you can do nested foreach looping! see
		// below.
		List<Hashtable<String, Object>> itemDataElementsList = new ArrayList<Hashtable<String, Object>>();
		// NOTE get all item tags, also non present ones
		List<IXMLTag> itemTags = getAllItemTagsForZul(parentTag);
		List<Boolean> itemTagsMoved = getAllItemTagsMovedForZul(parentTag);
		boolean checkOnMoved = itemTagsMoved != null && itemTagsMoved.size() == itemTags.size();
		for (int k = 0; k < itemTags.size(); k++) {
			IXMLTag itemTag = itemTags.get(k);
			Hashtable<String, Object> hItemDataElements = new Hashtable<String, Object>();
			hItemDataElements.put("tag", itemTag);
			if (itemTag.getName().equals("document")) {
				hItemDataElements.put("content", sSpringHelper.getTagChildValue(itemTag, "title", ""));
			} else {
				hItemDataElements.put("content", sSpringHelper.getTagChildValue(itemTag, "richtext", ""));
			}
			String draggable = sSpringHelper.getTagStatusChildAttribute(itemTag, AppConstants.statusKeyDraggable,
					"false");
			hItemDataElements.put("originallydraggable", draggable);
			if (draggable.equals("true")) {
				if (checkOnMoved && itemTagsMoved.get(k)) {
					draggable = "false";
				}
			}
			hItemDataElements.put("draggable", draggable);
			hItemDataElements.put("droppable",
					sSpringHelper.getTagStatusChildAttribute(itemTag, AppConstants.statusKeyDroppable, "true"));
			if (isShowIfRightOrWrong) {
				hItemDataElements.put("isrightlyplaced", isRightlyPlaced(itemTag));
			} else {
				hItemDataElements.put("isrightlyplaced", "");
			}
			hItemDataElements.put("present", sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag,
					AppConstants.statusKeyPresent, "true"));
			itemDataElementsList.add(hItemDataElements);
		}
		// NOTE list must contain at least one element, otherwise dropping item in other
		// container causes null pointer exception
		if (itemDataElementsList.size() == 0) {
			Hashtable<String, Object> hItemDataElements = new Hashtable<String, Object>();
			hItemDataElements.put("content", "");
			hItemDataElements.put("originallydraggable", "false");
			hItemDataElements.put("draggable", "false");
			hItemDataElements.put("droppable", "false");
			hItemDataElements.put("isrightlyplaced", "false");
			hItemDataElements.put("present", "false");
			itemDataElementsList.add(hItemDataElements);
		}
		return itemDataElementsList;
	}

	protected void dropItem(Component dragged, Component droppedOn) {
		String droppedOnType = (String) droppedOn.getAttribute("type");
		if (droppedOnType == null) {
			return;
		}
		boolean doDrop = false;
		Component originalContainer = null;
		Component targetContainer = null;
		boolean originalCopyItem = false;
		boolean targetCopyItem = false;
		Component beforeComponent = null;

		if (droppedOnType.equals("item")) {
			originalContainer = getMacroParent(dragged).getChildren().get(0);
			targetContainer = getMacroParent(droppedOn).getChildren().get(0);
			originalCopyItem = originalContainer.getAttribute("copyitem").equals("true");
			targetCopyItem = targetContainer.getAttribute("copyitem").equals("true");
			beforeComponent = droppedOn;
		} else if (droppedOnType.equals("container")) {
			originalContainer = getMacroParent(dragged).getChildren().get(0);
			targetContainer = getMacroChild(droppedOn).getChildren().get(0);
			originalCopyItem = originalContainer.getAttribute("copyitem").equals("true");
			targetCopyItem = targetContainer.getAttribute("copyitem").equals("true");
			beforeComponent = null;
		}

		// NOTE check if the maximum number of items for the target container is
		// reached. If so show message and cancel drop of item
		String targetMaxnumberofitemsStr = sSpringHelper.getCurrentStatusTagStatusChildAttribute(
				(IXMLTag) targetContainer.getAttribute("containertag"), AppConstants.statusKeyMaxnumberofitems, "-1");
		int targetMaxnumberofitems = runWnd.getNumber(targetMaxnumberofitemsStr, -1);
		if (targetContainer != originalContainer && targetMaxnumberofitems != -1
				&& getNumberOfContainerItems(targetContainer) >= targetMaxnumberofitems) {
			Messagebox.show(
					vView.getLabel("run_dragdropforms.messagebox.maxnumberofitems.text").replace("%1",
							targetMaxnumberofitemsStr),
					vView.getLabel("run_dragdropforms.messagebox.maxnumberofitems.title"), Messagebox.OK,
					Messagebox.INFORMATION);
			return;
		}

		if (droppedOnType.equals("item") || droppedOnType.equals("container")) {
			if (originalCopyItem) {
				// if original container has copyItem true
				if (!targetCopyItem) {
					// if target container has copyItem false (so it is dragging between two
					// containers)
					// disable original component
					Events.postEvent("onDisable", dragged, null);
					// copy component in target container
					Object[] eventPar = new Object[2];
					eventPar[0] = dragged.getAttribute("item");
					eventPar[1] = beforeComponent;
					Events.postEvent("onInsertComponent", targetContainer, eventPar);
				}
				// if target container has copyItem true, do nothing
			} else {
				// if original container has copyItem false
				if (targetCopyItem) {
					// if target container has copyItem true (so it is dragging between two
					// containers)
					// check if dragged tag is within target container
					Component targetComponent = null;
					IXMLTag draggedTag = (IXMLTag) dragged.getAttribute("tag");
					for (Component component : targetContainer.getChildren()) {
						if (component.getAttribute("tag") instanceof IXMLTag
								&& ((IXMLTag) component.getAttribute("tag")) == draggedTag) {
							targetComponent = component;
						}
					}
					if (targetComponent != null) {
						// remove copy from original container
						Events.postEvent("onRemoveComponent", originalContainer, dragged);
						// if found, enable original component
						Events.postEvent("onEnable", targetComponent, null);
					}
				} else {
					// if target container has copyItem false, move component
					// NOTE if targetContainer and originalContainer both have same orientation use
					// insertBefore otherwise onInsertComponent/onRemoveComponent,
					// because dragged hast ot be rerendered.
					if (targetContainer.getAttribute("orient").equals(originalContainer.getAttribute("orient"))) {
						// move if orient of target container and original container are equal
						targetContainer.insertBefore(dragged, beforeComponent);
					} else {
						// insert dragged in target container and remove in original container, so
						// dragged is rerendered
						// remove dragged from original container
						Events.postEvent("onRemoveComponent", originalContainer, dragged);
						Object[] eventPar = new Object[2];
						eventPar[0] = dragged.getAttribute("item");
						eventPar[1] = beforeComponent;
						Events.postEvent("onInsertComponent", targetContainer, eventPar);
					}
				}
			}
			dropItem((IXMLTag) dragged.getAttribute("tag"), (IXMLTag) droppedOn.getAttribute("tag"),
					(IXMLTag) targetContainer.getAttribute("containertag"));
			doDrop = true;
		}
		if (doDrop) {
			String tagId = getContainerTagId((IXMLTag) droppedOn.getAttribute("tag"));
			Component dragdropformItemBox = vView.getComponent("dragdropformItemBox_" + tagId);
			if (dragdropformItemBox != null) {
				// Events.postEvent("onMaximize", dragdropformItemBox, null);
				Clients.evalJavaScript("highlightRecevingZulElement('" + dragdropformItemBox.getId() + "');");
			}
			Events.postEvent("onUpdate", dragged,
					(isHideRightWrong((IXMLTag) dragged.getAttribute("tag")) ? "hideRightWrong" : ""));
		}
	}

	protected int getNumberOfContainerItems(Component container) {
		int number = 0;
		for (Component item : container.getChildren()) {
			if (item.getId() != null && !item.getId().contains("template")) {
				number++;
			}
		}
		return number;
	}

	protected String isRightlyPlaced(IXMLTag itemTag) {
		boolean containerok = sSpringHelper
				.getCurrentStatusTagStatusChildAttribute(itemTag, AppConstants.statusKeyContainerok, "true")
				.equals("true");
		boolean containerposok = sSpringHelper
				.getCurrentStatusTagStatusChildAttribute(itemTag, AppConstants.statusKeyContainerposok, "true")
				.equals("true");
		return "" + (containerok && containerposok);
	}

}
