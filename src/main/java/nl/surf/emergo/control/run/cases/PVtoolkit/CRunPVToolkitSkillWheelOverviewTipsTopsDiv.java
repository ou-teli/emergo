/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Div;
import org.zkoss.zul.Rows;

import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.domain.IERunGroup;

public class CRunPVToolkitSkillWheelOverviewTipsTopsDiv extends CRunPVToolkitSkillWheelLevel1Div {

	private static final long serialVersionUID = 7579250107007041786L;

	protected String _componentToShowId = "";
	protected int initialCycleNumber;
	protected int initialSkillClusterNumber;

	public void init(IERunGroup actor, boolean editable, int cycleNumber, int skillClusterNumber, String componentToShowId) {
		_classPrefix = "skillWheelOverviewTipsTops";
		_componentToShowId = componentToShowId;
		initialSkillClusterNumber = skillClusterNumber;
		initialCycleNumber = cycleNumber;
		
		super.init(actor, editable, cycleNumber, skillClusterNumber);
	}
	
	public void init(IERunGroup actor, boolean editable, int cycleNumber, int skillClusterNumber, String componentToShowId, List<Integer> pOtherRgaIdsToFilterOn) {
		_classPrefix = "skillWheelOverviewTipsTops";
		_componentToShowId = componentToShowId;
		otherRgaIdsToFilterOn = pOtherRgaIdsToFilterOn;
		initialOtherRgaIdsToFilterOn = new ArrayList<>(pOtherRgaIdsToFilterOn);
		initialSkillClusterNumber = skillClusterNumber;
		initialCycleNumber = cycleNumber;
		
		super.init(actor, editable, cycleNumber, skillClusterNumber);
	}
	
	@Override
	public void update(int cycleNumber) {
		getChildren().clear();
		
		_cycleNumber = cycleNumber;
		if (_cycleNumber < 1) {
			_cycleNumber = _maxCycleNumber;
		}
		setEditableAndMarkable();

		pvToolkit.setMemoryCaching(true);
		
		_sharedFeedbackTagsPerFeedbackStep = getFilteredSharedFeedbackSteps();

		isPreviewRun = CDesktopComponents.sSpring().getRun().getStatus() == AppConstants.run_status_test;

		new CRunPVToolkitDefImage(this, 
				new String[]{"class", "src"}, 
				new Object[]{_classPrefix + "Background", getBackGroundSrc()}
		);
		
		Div div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{"titleRight"}
		);
		
		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "value"}, 
				new Object[]{"font titleRight", getTitle()}
				);
		
		if (!_compareWithExperts) {
			if (_mayFilter) { 
				Component btn = new CRunPVToolkitDefButton(div, 
						new String[]{"class", "labelKey"}, 
						new Object[]{"font pvtoolkitButton " + _classPrefix + "FilterButton", "PV-toolkit-skillwheel.button.filter"}
						);
				addShowFilterOnClickEventListener(btn, this);
			}
		}

		_numberOfFeedbacks = getNumberOfCycleFeedbacks(_cycleNumber, _sharedFeedbackTagsPerFeedbackStep);

		if (!_compareWithExperts) {
			showCycle(this, _numberOfFeedbacks);
		}

		Component btn = new CRunPVToolkitDefButton(this, 
				new String[]{"class", "cLabelKey"}, 
				new Object[]{"font pvtoolkitButton " + _classPrefix + "BackButton", "PV-toolkit.back"}
				);
		addBackOnClickEventListener(btn, getId(), _componentToShowId);

		Div tipsTopsDiv = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "TipsTopsDiv"}
		);
		
		Rows rows = appendTipsTopsGrid(tipsTopsDiv);
		int rowNumber = 1;
		rowNumber = appendTipsOrTopsToGrid(rows, "tops", rowNumber);
		rowNumber = appendTipsOrTopsToGrid(rows, "tips", rowNumber);
		
		pvToolkit.setMemoryCaching(false);
	}

	protected String getBackGroundSrc() {
		String src = "";
		if (_cycleNumber == _maxCycleNumber) {
			src = zulfilepath + _classPrefix + "_wit.svg";
		}
		if (_cycleNumber < _maxCycleNumber) {
			src = zulfilepath + _classPrefix + "_geel.svg";
		}
		return src;
	}

	protected String getTitle() {
		if (!_compareWithExperts) {
			return CDesktopComponents.vView().getLabel("PV-toolkit-skillwheel.header");
		}
		else {
			return CDesktopComponents.vView().getLabel("PV-toolkit-skillwheel.header.compareWithExperts");
		}
	}

    protected Rows appendTipsTopsGrid(Component parent) {
    	String markLabelKey = "mark";
    	if (!_editable) {
    		markLabelKey = "marked";
    	}
    	return pvToolkit.appendGrid(
    			parent, 
    			getId() + "Grid",
    			"position:absolute;left:0px;top:0px;width:1060px;height:450px;overflow:auto;", 
    			new boolean[] {true, true, true, true, true, skillclusterHasTipsTops(), subskillHasTipsTops(), skillHasTipsTops(), _showMarks},
    			new String[] {"3%", "37%", "10%", "10%", "10%", "10%", "10%", "10%", "10%"}, 
    			new boolean[] {false, true, true, true, true, true, true, true, true},
    			null,
    			"PV-toolkit-skillwheel.column.label.", 
    			new String[] {"", "tiptop", "peer", "feedbackrole", "feedbackdate", "skillcluster", "subskill", "skill", markLabelKey});
	}

	protected int appendTipsOrTopsToGrid(Rows rows, String tipsOrTops, int rowNumber) {
		return appendTipsOrTopsToGrid(
				rows, 
				tipsOrTops, 
				rowNumber,
				new boolean[] {true, true, true, true, true, skillclusterHasTipsTops(), subskillHasTipsTops(), skillHasTipsTops(), _showMarks} 
				);
	}

	protected void addBackOnClickEventListener(Component component, String componentFromId, String componentToId) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				CRunPVToolkitSkillWheelOverviewTipsTopsDiv componentFrom = (CRunPVToolkitSkillWheelOverviewTipsTopsDiv)CDesktopComponents.vView().getComponent(componentFromId);
				IERunGroup lActor = null;
				boolean lEditable = false;
				int lCycleNumber = initialCycleNumber;
				// NOTE: cannot be changed here:
				int lSkillClusterNumberToFilter = initialSkillClusterNumber;
				List<Integer> lOtherRgaIdsToFilterOn = initialOtherRgaIdsToFilterOn;
				if (componentFrom != null) {
					lActor = componentFrom._actor;
					lEditable = componentFrom._editable;
					lCycleNumber = componentFrom._cycleNumber;
					lOtherRgaIdsToFilterOn = componentFrom.otherRgaIdsToFilterOn;
					componentFrom.setVisible(false);
				}
				toComponent(componentToId, lActor, lEditable, lCycleNumber, lSkillClusterNumberToFilter, lOtherRgaIdsToFilterOn);
			}
		});
	}

	protected void toComponent(String componentToId, IERunGroup pActor, boolean pEditable, int pCycleNumber, int pSkillClusterNumberToFilter, List<Integer> pOtherRgaIdsToFilterOn) {
		Component componentTo = CDesktopComponents.vView().getComponent(componentToId);
		if (componentTo != null) {
			if (componentTo instanceof CRunPVToolkitSkillWheelLevel1Div) {
				boolean lRgaIdsChanged = initialOtherRgaIdsToFilterOn != null && !initialOtherRgaIdsToFilterOn.equals(pOtherRgaIdsToFilterOn);
				if (lRgaIdsChanged || (initialCycleNumber != pCycleNumber)) {
					// NOTE: componentToInit.otherRgaIdsToFilterOn is already equal to componentFrom.otherRgaIdsToFilterOn if set in componentFrom, because both point to same ArrayList
					// NOTE: set componentTo._cycleNumber, because parameter in init method will be ignored for level 1 and level 2:
					((CRunPVToolkitSkillWheelLevel1Div)componentTo)._cycleNumber = pCycleNumber;
					// NOTE: if pOtherRgaIdsToFilterOn = null we have 'compare' situation; we must not overrule pOtherRgaIdsToFilterOn of parent
					if (lRgaIdsChanged) {
						if (componentTo instanceof CRunPVToolkitSkillWheelLevel2Div) {
							((CRunPVToolkitSkillWheelLevel2Div)componentTo).init(pActor, pEditable, pCycleNumber, pSkillClusterNumberToFilter, pOtherRgaIdsToFilterOn);
						} else {
							((CRunPVToolkitSkillWheelLevel3Div)componentTo).init(pActor, pEditable, pCycleNumber, pSkillClusterNumberToFilter, pOtherRgaIdsToFilterOn);
						}
					} else {
						((CRunPVToolkitSkillWheelLevel1Div)componentTo).init(pActor, pEditable, pCycleNumber, pSkillClusterNumberToFilter);
					}
				}
			}
			componentTo.setVisible(true);
		}
	}

}
