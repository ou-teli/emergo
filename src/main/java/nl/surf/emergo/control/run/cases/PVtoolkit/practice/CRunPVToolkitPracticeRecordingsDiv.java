/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.practice;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefHelper;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitSubStepsDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitVideoRecordingLink;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefHbox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.cases.PVtoolkit.feedback.CRunPVToolkitFeedbackPreviousOverviewRubricDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTag;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.view.VView;

public class CRunPVToolkitPracticeRecordingsDiv extends CRunPVToolkitPracticeDiv {

	private static final long serialVersionUID = -5127391387182197301L;
	
	protected boolean _practice = false;
	protected boolean _selfFeedback = false;
	protected boolean _askFeedback = false;
	protected boolean _hasSelfFeedbackSubStep = false;
	
	protected List<IXMLTag> practiceTags;

	protected String _stepPrefix = "practice";
	protected String _idPrefix = "practice";
	protected String _classPrefix = "practice";
	
	@Override
	public void init(IERunGroup actor, boolean reset, boolean editable, String subStepType) {

		_practice = subStepType.equals(CRunPVToolkit.practicePracticeSubStepType);
		_selfFeedback = subStepType.equals(CRunPVToolkit.practiceSelfFeedbackSubStepType);
		_askFeedback = subStepType.equals(CRunPVToolkit.practiceAskFeedbackSubStepType);
		_hasSelfFeedbackSubStep = hasSelfFeedbackSubStep();

		//NOTE always initialize, because recordings my be changed under my recordings during a session
		setClass(_classPrefix + "Recordings");
		super.init(actor, reset, editable, subStepType);
		setVisible(true);
		_initialized = true;
	}
	
	@Override
	public void reset() {
	}
	
	@Override
	public void update() {
		getChildren().clear();
		
		pvToolkit.setMemoryCaching(true);
		
		Div div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{"titleRight"}
		);
		
		CRunPVToolkitSubStepsDiv runPVToolkitSubStepsDiv = (CRunPVToolkitSubStepsDiv)CDesktopComponents.vView().getComponent("practiceSubStepsDiv");
		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "value"}, 
				new Object[]{"font titleRight", (String)runPVToolkitSubStepsDiv.getAttribute("divTitle")}
		);
		
		if (_practice) {
			Button btn = new CRunPVToolkitDefButton(this, 
					new String[]{"class", "labelKey"}, 
					new Object[]{"font pvtoolkitButton " + _classPrefix + "NewRecordingButton", "PV-toolkit-practice.button.newRecording"}
			);
			addNewRecordingButtonOnClickEventListener(btn);
		}

		div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "RecordingsDiv"}
		);
		
		CRunPVToolkitCacAndTag practice = pvToolkit.getPractice(_actor, pvToolkit.getCurrentStepTag());
		if (practice == null) {
			return;
		}

		Rows rows = appendRecordingsGrid(div);
		int rowNumber = 1;
		rowNumber = appendRecordingsToGrid(rows, rowNumber);
		
		Button btn = new CRunPVToolkitDefButton(this, 
				new String[]{"class", "cLabelKey"}, 
				new Object[]{"font pvtoolkitButton subStepReadyButton", "PV-toolkit.ready"}
		);
		addBackToPracticeSubStepssOnClickEventListener(btn, _stepPrefix + "SubStepDiv");

		pvToolkit.setMemoryCaching(false);
		
	}

	protected void addNewRecordingButtonOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				CRunPVToolkitPracticeChooseRecordingTypeDiv practiceChooseRecordingType = (CRunPVToolkitPracticeChooseRecordingTypeDiv)CDesktopComponents.vView().getComponent(_idPrefix + "ChooseRecordingTypeDiv");
				if (practiceChooseRecordingType != null) {
					practiceChooseRecordingType.init();
				}
			}
		});
	}

	protected void addBackToPracticeSubStepssOnClickEventListener(Component component, String fromComponentId) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				Component fromComponent = CDesktopComponents.vView().getComponent(fromComponentId);
				if (fromComponent != null) {
					fromComponent.setVisible(false);
				}
				CRunPVToolkitSubStepsDiv toComponent = (CRunPVToolkitSubStepsDiv)CDesktopComponents.vView().getComponent(_stepPrefix + "SubStepsDiv");
				if (toComponent != null) {
					toComponent.update();
					toComponent.setVisible(true);
				}
			}
		});
	}

	public void updatePracticeLabel(IXMLTag practiceTag) {
		String practiceTagUuid = practiceTag.getAttribute(practiceTag.getName() + "uuid");
		VView vView = CDesktopComponents.vView();

		setPracticeLabelVariableProperties((Label)vView.getComponent(getId() + "GridRowPracticeLabel_" + practiceTagUuid), practiceTag);
	}
	
	private boolean showSelfFeedbackColumn = false;

	protected Rows appendRecordingsGrid(Component parent) {
   		showSelfFeedbackColumn = _hasSelfFeedbackSubStep && (_selfFeedback || _askFeedback);
    	return pvToolkit.appendGrid(
    			parent, 
    			getId() + "Grid",
    			"position:absolute;left:0px;top:0px;width:1060px;height:430px;overflow:auto;", 
				new boolean[] {true, true, true, true, true, true, showSelfFeedbackColumn, _askFeedback, true}, 
    			new String[] {"3%", "21%", "7%", "10%", "10%", "13%", "13%", "13%", "10%"}, 
    			new boolean[] {false, true, false, true, true, false, false, false, false},
    			null,
    			"PV-toolkit-practice.column.label.",
    			new String[] {"", "name", "edit", "date", "duration", "recording", "selffeedback", "feedback", "delete"});
    }

	protected int appendRecordingsToGrid(Rows rows, int rowNumber) {
		return appendRecordingsToGrid(
				rows, 
				rowNumber,
				new boolean[] {true, true, true, true, true, true, showSelfFeedbackColumn, _askFeedback, true} 
				);
	}

	protected int appendRecordingsToGrid(
			Rows rows, 
			int rowNumber,
    		boolean[] showColumn 
			) {
		CRunPVToolkitCacAndTag practice = pvToolkit.getPractice(_actor, pvToolkit.getCurrentStepTag());
		if (practice == null || practice.getXmlTag() == null) {
			return rowNumber;
		}
		practiceTags = practice.getXmlTag().getChilds("practice");
		if (practiceTags == null) {
			return rowNumber;
		}
		
		boolean atLeastOnepracticeIsShared = atLeastOnepracticeIsShared(practiceTags);

		for (IXMLTag practiceTag : practiceTags) {
			boolean practiceHasSelfFeedback = practiceHasSelfFeedback(practiceTag);
			boolean practiceIsShared = practiceIsShared(practiceTag);
			//NOTE editable if _editable and not in consultation or shared
			boolean editable = _editable && !practiceIsShared;

			String practiceTagUuid = practiceTag.getAttribute(practiceTag.getName() + "uuid");
			
			Row row = new Row();
			rows.appendChild(row);
			row.setAttribute("tag", practiceTag);
			
			int columnNumber = 0;
			if (showColumn.length > columnNumber && showColumn[columnNumber]) {
				new CRunPVToolkitDefImage(row, 
						new String[]{"class", "src"}, 
						new Object[]{"gridImage", zulfilepath + "video.svg"}
						);
			}

			columnNumber++;
			if (showColumn.length > columnNumber && showColumn[columnNumber]) {
				Label label = new CRunPVToolkitDefLabel(row, 
						new String[]{"id", "class"}, 
						new Object[]{getId() + "GridRowPracticeLabel_" + practiceTagUuid, "font gridLabel"}
						);
				setPracticeLabelVariableProperties(label, practiceTag);
			}

			columnNumber++;
			if (showColumn.length > columnNumber && showColumn[columnNumber]) {
				Image image = new CRunPVToolkitDefImage(row,
						new String[]{"class", "src"}, 
						new Object[]{"gridImageClickable", zulfilepath + "edit.svg"}
				);
				addEditOnClickEventListener(image, practiceTag);
				image.setVisible(editable);
			}

			columnNumber++;
			if (showColumn.length > columnNumber && showColumn[columnNumber]) {
				String recordingDate = pvToolkit.getStatusChildTagAttribute(practiceTag, "recordingdate");
				CRunPVToolkitDefLabel label = new CRunPVToolkitDefLabel(row, 
						new String[]{"class", "value"}, 
						new Object[]{"font gridLabel", recordingDate}
						);
				//NOTE for ordering use ymd format
				if (!StringUtils.isEmpty(recordingDate)) {
					Date date = CDefHelper.getDateFromStrDMY(recordingDate);
					label.setAttribute("orderValue", CDefHelper.getDateStrAsYMD(date));
				}
			}

			columnNumber++;
			if (showColumn.length > columnNumber && showColumn[columnNumber]) {
				String value = "";
				int webcamRecordingLength = pvToolkit.getWebcamRecordingLength(practiceTag);
				if (webcamRecordingLength >= 0) {
					value = CDefHelper.getTimeStrAsHMS(webcamRecordingLength);
				}
				new CRunPVToolkitDefLabel(row, 
						new String[]{"class", "value"}, 
						new Object[]{"font gridLabel", value}
						);
			}

			columnNumber++;
			if (showColumn.length > columnNumber && showColumn[columnNumber]) {
				CRunPVToolkitVideoRecordingLink link = new CRunPVToolkitVideoRecordingLink(row, 
						new String[]{"class", "labelKey"}, 
						new Object[]{"font gridLink", "PV-toolkit-practice.row.button.recording.view"}
						);
				link.init(0, practiceTag, getId(), _idPrefix + "RecordingDiv", _idPrefix);
			}

			columnNumber++;
			if (showColumn.length > columnNumber && showColumn[columnNumber]) {
				Hbox hbox = new CRunPVToolkitDefHbox(row, null, null);
				String labelKey = "PV-toolkit-practice.row.button.selffeedback.";
				if (!practiceHasSelfFeedback) {
					labelKey += "give";
				}
				else {
					labelKey += "view";
				}
				Label link = new CRunPVToolkitDefLabel(hbox, 
						new String[]{"class", "labelKey"}, 
						new Object[]{"font gridLink", labelKey}
						);
				link.setVisible(_hasSelfFeedbackSubStep && (_selfFeedback || _askFeedback));
				addSelfFeedbackOnClickEventListener(link, _actor, practiceTag, editable);
				if (practiceHasSelfFeedback) {
					new CRunPVToolkitDefImage(hbox, 
							new String[]{"class", "src"}, 
							new Object[]{"gridImage", zulfilepath + "check-green.svg"}
					);
				}
			}

			columnNumber++;
			if (showColumn.length > columnNumber && showColumn[columnNumber]) {
				Hbox hbox = new CRunPVToolkitDefHbox(row, null, null);
				Label link = new CRunPVToolkitDefLabel(hbox, 
						new String[]{"class", "labelKey"}, 
						new Object[]{"font gridLink", "PV-toolkit-practice.row.button.feedback.ask"}
						);
				link.setVisible(_askFeedback && (!_hasSelfFeedbackSubStep || practiceHasSelfFeedback) && !atLeastOnepracticeIsShared);
				addAskFeedbackOnClickEventListener(link, practiceTag);
				if (practiceIsShared) {
					new CRunPVToolkitDefImage(hbox, 
							new String[]{"class", "src"}, 
							new Object[]{"gridImage", zulfilepath + "check-green.svg"}
					);
				}
			}

			columnNumber++;
			if (showColumn.length > columnNumber && showColumn[columnNumber]) {
				Image image = new CRunPVToolkitDefImage(row,
						new String[]{"class", "src"}, 
						new Object[]{"gridImageClickable", zulfilepath + "trash.svg"}
				);
				addDeleteOnClickEventListener(image, practiceTag);
				image.setVisible(editable);
			}

			rowNumber ++;

		}

		return rowNumber;
	}

	public void setPracticeLabelVariableProperties(Label label, IXMLTag practiceTag) {
		if (label == null || practiceTag == null) {
			return;
		}
		label.setValue(pvToolkit.getStatusChildTagAttribute(practiceTag, "name"));
	}
	
	protected void addEditOnClickEventListener(Component component, IXMLTag practiceTag) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				CRunPVToolkitPracticeEditRecording popup = (CRunPVToolkitPracticeEditRecording)CDesktopComponents.vView().getComponent(_idPrefix + "EditRecording");
				if (popup != null) {
					popup.init(practiceTag);
				}
			}
		});
	}
	
	protected void addSelfFeedbackOnClickEventListener(Component component, IERunGroup actor, IXMLTag practiceTag, boolean editable) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				CDesktopComponents.vView().getComponent(_stepPrefix + "RecordingsDiv").setVisible(false);
				
				IXMLTag currentStepTag = pvToolkit.getCurrentStepTag();
				List<IXMLTag> previousPracticeStepTags = pvToolkit.getStepTagsInPreviousCycles(currentStepTag, CRunPVToolkit.practiceStepType);
				boolean showPreviousFeedback = previousPracticeStepTags.size() > 0;
				if (showPreviousFeedback) {
					CRunPVToolkitFeedbackPreviousOverviewRubricDiv previousOverviewRubricDiv = (CRunPVToolkitFeedbackPreviousOverviewRubricDiv)CDesktopComponents.vView().getComponent(_stepPrefix + "PreviousOverviewRubricDiv");
					if (previousOverviewRubricDiv != null) {
						previousOverviewRubricDiv.init(actor, currentStepTag, practiceTag, false, true);
						previousOverviewRubricDiv.setVisible(true);
					}
				}
				else {
					CRunPVToolkitPracticeFeedbackDiv feedbackDiv = (CRunPVToolkitPracticeFeedbackDiv)CDesktopComponents.vView().getComponent(_stepPrefix + "FeedbackDiv");
					if (feedbackDiv != null) {
						feedbackDiv.init(actor, currentStepTag, practiceTag, editable);
						feedbackDiv.setVisible(true);
					}
				}
			}
		});
	}
	
	protected void addAskFeedbackOnClickEventListener(Component component, IXMLTag practiceTag) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				VView vView = CDesktopComponents.vView();
				String recordingName = pvToolkit.getStatusChildTagAttribute(practiceTag, "name");
				CRunPVToolkitPracticeReadyDiv practiceReadyDiv = (CRunPVToolkitPracticeReadyDiv)vView.getComponent(_stepPrefix + "ReadyDiv");
				if (practiceReadyDiv != null) {
					practiceReadyDiv.init(_actor, practiceTag, recordingName);
				}
			}
		});
	}
	
	protected void addDeleteOnClickEventListener(Component component, IXMLTag practiceTag) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				VView vView = CDesktopComponents.vView();
				int choice = vView.showMessagebox(getRoot(), vView.getCLabel("PV-toolkit.delete.confirmation"), vView.getCLabel("PV-toolkit.delete"), Messagebox.CANCEL | Messagebox.OK, Messagebox.QUESTION);
				if (choice == Messagebox.OK) {
					deletePractice(practiceTag);
				}
			}
		});
	}
	
	public boolean doesRecordingNameExist(IXMLTag practiceTag, String name) {
		if (practiceTags == null) {
			return false;
		}
		for (IXMLTag tempPracticeTag : practiceTags) {
			String tempName = pvToolkit.getStatusChildTagAttribute(tempPracticeTag, "name").trim();
			if (tempName.equals(name) && (practiceTag == null || tempPracticeTag != practiceTag)) {
				return true;
			}
		}
		return false;
	}
	
	protected void sharePractice(IXMLTag practiceTag, boolean pTeacherFbAsked) {
		setPracticeShared(practiceTag, pTeacherFbAsked);
	}
	
	public void addPractice(String name, String blobId, int webcamRecordingLength) {
		if (pvToolkit.addPractice(_actor, pvToolkit.getCurrentStepTag(), name, blobId, webcamRecordingLength)) {
			//TODO add row
			update();
		}
	}
	
	public void updatePractice(IXMLTag practiceTag, String name) {
		if (pvToolkit.updatePractice(practiceTag, name)) {
			updatePracticeLabel(practiceTag);
		}
	}
	
	public void deletePractice(IXMLTag practiceTag) {
		if (pvToolkit.deletePractice(practiceTag)) {
			pvToolkit.deleteGridRow(getId() + "Grid", practiceTag);
		}
	}
	
	public void addPracticePiece(IXMLTag practiceTag, String name, String blobId) {
		pvToolkit.addPracticePiece(_actor, pvToolkit.getCurrentStepTag(), practiceTag, name, blobId);
	}
	
}
