/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.neoclassic;

import org.zkoss.zk.ui.Component;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.run.CRunArea;
import nl.surf.emergo.control.run.CRunChoiceHoverBtns;
import nl.surf.emergo.control.run.CRunHbox;
import nl.surf.emergo.control.run.CRunHoverBtn;

/**
 * The Class CRunChoiceHoverBtnsNeoclassic. Ancestor of alle sets of hover buttons used
 * within the run choice area.
 */
public class CRunChoiceHoverBtnsNeoclassic extends CRunChoiceHoverBtns {

	private static final long serialVersionUID = 4095035789567031409L;

	/** The button hbox containing the hover buttons. */
	protected CRunArea buttonHboxScroll = null;

	/** The right arrow div containing the right arrow button. */
	protected CDefDiv rightArrow = null;

	/** The left arrow div containing the right arrow button. */
	protected CDefDiv leftArrow = null;

	/**
	 * Instantiates a new c run choice hover btns neoclassic.
	 *
	 * @param aParent the ZK parent
	 * @param aId the a id
	 */
	public CRunChoiceHoverBtnsNeoclassic(Component aParent, String aId) {
		super(aParent, aId);
	}

	/**
	 * Instantiates a new c run choice hover btns neoclassic.
	 *
	 * @param aParent the ZK parent
	 * @param aId the a id
	 * @param aUseArrows the a use arrows
	 */
	public CRunChoiceHoverBtnsNeoclassic(Component aParent, String aId, boolean aUseArrows) {
		super(aParent, aId, aUseArrows);
	}

	protected CRunHbox newButtonHbox() {
		buttonHboxScroll = new CRunArea();
		appendChild(buttonHboxScroll);
		buttonHboxScroll.setZclass(getClassName() + "_hbox_scroll");
		CRunHbox hbox = new CRunHbox();
		buttonHboxScroll.appendChild(hbox);
//		appendChild(hbox);
		hbox.setZclass(getClassName() + "_hbox");
		buttonHboxList.add(hbox);
		return hbox;
	}

	/**
	 * Creates arrows to scroll the hover buttons.
	 */
	protected void createArrows() {
		String lLeftArrowId = getId() + "LeftArrow";
		String lRightArrowId = getId() + "RightArrow";

		leftArrow = new CDefDiv();
		appendChild(leftArrow);
		leftArrow.setId(lLeftArrowId);
		leftArrow.setZclass(getClassName() + "_left_arrow");
		leftArrow.setTooltiptext(CDesktopComponents.vView().getLabel("run_empack.left_arrow.tooltip"));
		leftArrow.setVisible(false);

		
		CRunHoverBtn lLeftBtn = newHoverBtn(lLeftArrowId + "Btn", "active", "", "",
				"left_arrow", "");
		lLeftBtn.setZclass(getClassName() + "_arrow_btn");
		lLeftBtn.setCanHaveStatusSelected(false);
		leftArrow.appendChild(lLeftBtn);
		
		
/*		
		String lSrc = "";
		if (sSpring != null)
			lSrc = sSpring.getStyleImgSrc("left_arrow");
		Image lLeftImage = new CDefImage(lSrc);
		lLeftImage.setVisible(false);
		lLeftArrow.appendChild(lLeftImage);
*/
		rightArrow = new CDefDiv();
		appendChild(rightArrow);
		rightArrow.setId(lRightArrowId);
		rightArrow.setZclass(getClassName() + "_right_arrow");
		rightArrow.setTooltiptext(CDesktopComponents.vView().getLabel("run_empack.right_arrow.tooltip"));
		rightArrow.setVisible(false);

		CRunHoverBtn lRightBtn = newHoverBtn(lRightArrowId + "Btn", "active", "", "",
				"right_arrow", "");
		lRightBtn.setZclass(getClassName() + "_arrow_btn");
		lRightBtn.setCanHaveStatusSelected(false);
		rightArrow.appendChild(lRightBtn);
		
/*		
		lSrc = "";
		if (sSpring != null)
			lSrc = sSpring.getStyleImgSrc("right_arrow");
		Image lRightImage = new CDefImage(lSrc);
		lRightImage.setVisible(false);
		lRightArrow.appendChild(lRightImage);
*/

		String lLeftAction = "move('" + buttonHboxScroll.getZclass() + "', '" + buttonHbox.getZclass() + "', '" + leftArrow.getZclass() + "', '" + rightArrow.getZclass() + "', true);";
		lLeftBtn.setWidgetListener("onMouseOver", lLeftAction);
		lLeftBtn.setWidgetListener("onMouseOut", "stopscroll();");
		lLeftBtn.setWidgetListener("onClick", null);

		String lRightAction = "move('" + buttonHboxScroll.getZclass() + "', '" + buttonHbox.getZclass() + "', '" + leftArrow.getZclass() + "', '" + rightArrow.getZclass() + "', false);";
		lRightBtn.setWidgetListener("onMouseOver", lRightAction);
		lRightBtn.setWidgetListener("onMouseOut", "stopscroll();");
		lRightBtn.setWidgetListener("onClick", null);
	}
	
	/**
	 * Shows scroll arrows depending on aShow and number of hover buttons.
	 *
	 * @param aShow the a show
	 */
	public void showArrows(boolean aShow) {
		if (aShow)
			if ((buttonHboxScroll != null) && (buttonHbox != null) && (leftArrow != null) && (rightArrow != null))
				runWnd.emergoEventToClient("arrowscrollarea", "", "checksize", buttonHboxScroll.getZclass() + "," + buttonHbox.getZclass()+ "," + leftArrow.getZclass()+ "," + rightArrow.getZclass());
		return;
	}

}
