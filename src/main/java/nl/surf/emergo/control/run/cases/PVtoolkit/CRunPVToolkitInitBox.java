/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefBox;

public class CRunPVToolkitInitBox extends CDefBox {

	private static final long serialVersionUID = -5134730170101747005L;

	//TODO not only webcam recording length should be stored but also length of uploaded file
	//duration of uploaded file may not exceed maxWebcamRecordingLength (below)
	//also rename length by duration
	
	protected String zulfilepath = "";
	protected boolean hasMainMenu = true;
	protected boolean hasStepTitle = true;

	public String getZulfilepath() {
		return zulfilepath;
	}

	public void setZulfilepath(String zulfilepath) {
		this.zulfilepath = zulfilepath;
	}

	public boolean getHasMainMenu() {
		return hasMainMenu;
	}

	public void setHasMainMenu(boolean hasMainMenu) {
		this.hasMainMenu = hasMainMenu;
	}

	public boolean getHasStepTitle() {
		return hasStepTitle;
	}

	public void setHasStepTitle(boolean hasStepTitle) {
		this.hasStepTitle = hasStepTitle;
	}

	public Component getMacroParent(Component component) {
		while (component != null && !(component instanceof HtmlMacroComponent)) {
			component = component.getParent();
		}
		return component;
	}

	public void onCreate() {
		//NOTE use echoEvent, otherwise macro parent does not yet exist, if code is used within zscript
	  	Events.echoEvent("onInit", this, null);
	}
	
	public void onInit() {
		Map<String,Object> propertyMap = new HashMap<String,Object>();
		propertyMap.put("casecomponent", ((HtmlMacroComponent)getMacroParent(this)).getDynamicProperty("a_casecomponent"));
		propertyMap.put("tag", ((HtmlMacroComponent)getMacroParent(this)).getDynamicProperty("a_tag"));

		zulfilepath = (String)((HtmlMacroComponent)getMacroParent(this)).getDynamicProperty("a_zulfilepath");
		propertyMap.put("zulfilepath", zulfilepath);
			
		List<String> errorMessages = new ArrayList<String>();
		hasMainMenu = !CDesktopComponents.sSpring().getCurrentRunTagStatus(
			CRunPVToolkit.gCaseComponentNameStates,
			"state",
			"no_main_menu",
			AppConstants.statusKeyValue,
			AppConstants.statusTypeRunGroup,
			errorMessages).equals("true");
		propertyMap.put("has_main_menu", hasMainMenu);
		hasStepTitle = !CDesktopComponents.sSpring().getCurrentRunTagStatus(
				CRunPVToolkit.gCaseComponentNameStates,
				"state",
				"no_step_title",
				AppConstants.statusKeyValue,
				AppConstants.statusTypeRunGroup,
				errorMessages).equals("true");
		propertyMap.put("no_step_title", hasStepTitle);
		
		//TODO get state value for maxWebcamRecordingLength

		HtmlMacroComponent macro = new HtmlMacroComponent();
		appendChild(macro);
		macro.setDynamicProperty("a_propertyMap", propertyMap);
		macro.setMacroURI(zulfilepath + "PV-toolkit_view_macro.zul");
	}

}
