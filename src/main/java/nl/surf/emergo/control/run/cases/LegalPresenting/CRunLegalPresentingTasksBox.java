/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.LegalPresenting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.event.Event;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * This class is used to show both a tasks bar and a tasks pop-up. It uses content entered within a tasks case component. There are main tasks (mini games) with one level of sub tasks.
 * The used tasks case component should have present=false and present should stay false, to prevent rendering of the original tasks component on the tablet.
 * Accessible and finished of tasks determine the layout of the tasks bar and pop-up. At the start of the game the first main task and its first sub task should be accessible. The other main and sub tasks should have accessible=false.
 * Game script sets finished of a sub task accessible of the next sub task for its main task, and set finished of main tasks, which results in setting the next main task and its first sub task accessible.
 * 
 * Note that because the tasks macro is defined in the navigation component, it will be rerendered if its location is revisited, i.e., if present of the macro is true.
 * If you want to update the macro when being on its location, use game script to set update of the macro to true.
 */
public class CRunLegalPresentingTasksBox extends CRunLegalPresentingBox {

	private static final long serialVersionUID = -7846191419951846621L;

	protected final static String tasksCaseComponentName = "001_game tasks";
	protected IECaseComponent tasksCaseComponent;
	
	protected String tasksBarId;
	protected String tasksPopupId;
	
	protected boolean tasksBarVisible = true;
	protected boolean tasksPopupVisible = false;

	@Override
	public void onInit() {
		super.onInit();
		
		tasksBarId = getUuid() + "_bar";
		tasksPopupId = getUuid() + "_popup";
		
		tasksCaseComponent = getTasksComponent();

		//NOTE if opened of the tasks case component is true, the pop-up should be shown and the bar should be hidden
		String opened = sSpring.getCurrentRunComponentStatus(tasksCaseComponent, AppConstants.statusKeyOpened, AppConstants.statusTypeRunGroup);
		tasksBarVisible = !opened.equals(AppConstants.statusValueTrue);
		tasksPopupVisible = !tasksBarVisible;
		
		propertyMap.put("tasksBarVisible", tasksBarVisible);
		propertyMap.put("tasksPopupVisible", tasksPopupVisible);

		//determine task properties to be used within the child macro
		setTasksProperties();
		
		//add the child macro
		addChildMacro("LegalPresenting_tasks_view_macro.zul");
	}
	
	@Override
	public void onUpdate() {
		//when updated, e.g., because task is finished, tasks properties have to be set again
		setTasksProperties();
		
		//rerender child macro with adjusted properties
		childMacro.recreate();
	}

	protected void setTasksProperties() {
		//NOTE if opened of the tasks case component is true, the pop-up should be shown and the bar should be hidden
		String accessible = sSpring.getCurrentRunComponentStatus(tasksCaseComponent, AppConstants.statusKeyAccessible, AppConstants.statusTypeRunGroup);
		propertyMap.put("tasksDisabled", accessible.equals(AppConstants.statusValueFalse));

		List<Map<String,Object>> tasks = new ArrayList<Map<String,Object>>();
		propertyMap.put("tasks", tasks);
		IXMLTag rootTag = sSpring.getXmlDataPlusRunStatusTree(tasksCaseComponent, AppConstants.statusTypeRunGroup);
		if (rootTag == null) {
			return;
		}
		IXMLTag contentTag = rootTag.getChild(AppConstants.contentElement);
		if (contentTag == null) {
			return;
		}
		
		//NOTE current main task (= phase) name is shown within tasks bar
		String currentMainTaskName = "";
		int taskCounter = 0;
		//loop through main tasks
		for (IXMLTag taskTag : contentTag.getChilds("task")) {
			Map<String,Object> hTaskData = new HashMap<String,Object>();
			tasks.add(hTaskData);
			hTaskData.put("tag", taskTag);
			hTaskData.put("taskNumber", "" + (taskCounter + 1));
			boolean taskAccessible = !taskTag.getCurrentStatusAttribute(AppConstants.statusKeyAccessible).equals(AppConstants.statusValueFalse);
			boolean taskFinished = taskTag.getCurrentStatusAttribute(AppConstants.statusKeyFinished).equals(AppConstants.statusValueTrue);
			hTaskData.put("taskDone", taskFinished);
			//NOTE active main task has other layout
			boolean taskActive = taskAccessible && !taskFinished;
			hTaskData.put("taskActive", taskActive);
			String taskName = sSpring.unescapeXML(taskTag.getChildValue("name"));
			hTaskData.put("taskName", taskName);
			if (taskActive) {
				currentMainTaskName = taskName;
			}
			else if (taskFinished) {
				//NOTE if no task is active the last finished task will be shown
				currentMainTaskName = taskName;
			}
			List<Map<String,Object>> subTasks = new ArrayList<Map<String,Object>>();
			hTaskData.put("tasks", subTasks);
			for (IXMLTag subTaskTag : taskTag.getChilds("task")) {
				Map<String,Object> hSubTaskData = new HashMap<String,Object>();
				subTasks.add(hSubTaskData);
				hSubTaskData.put("tag", subTaskTag);
				boolean subTaskAccessible = !subTaskTag.getCurrentStatusAttribute(AppConstants.statusKeyAccessible).equals(AppConstants.statusValueFalse);
				boolean subTaskFinished = subTaskTag.getCurrentStatusAttribute(AppConstants.statusKeyFinished).equals(AppConstants.statusValueTrue);
				hSubTaskData.put("subTaskDone", subTaskFinished);
				//NOTE active sub task has other layout
				hSubTaskData.put("subTaskActive", subTaskAccessible && !subTaskFinished);
				hSubTaskData.put("subTaskName", sSpring.unescapeXML(subTaskTag.getChildValue("name")));
			}

			taskCounter++;
		}
		
		propertyMap.put("currentMainTaskName", currentMainTaskName);
	}
	
	protected IECaseComponent getTasksComponent() {
		return sSpring.getCaseComponent("", tasksCaseComponentName);
	}
	
	/** If a button is clicked within the child macro file, following event is triggered. */
	public void onButtonClick(Event event) {
		if (event.getData().equals("show_popup")) {
			//NOTE the tasks bar is clicked, so hide it and show the tasks pop-up

			//just like for the original tasks component set selected and opened for the case component
			setRunComponentStatus(tasksCaseComponent, AppConstants.statusKeySelected, AppConstants.statusValueTrue);
			setRunComponentStatus(tasksCaseComponent, AppConstants.statusKeyOpened, AppConstants.statusValueTrue);

			tasksBarVisible = false;
			tasksPopupVisible = true;
			vView.getComponent(tasksBarId).setVisible(tasksBarVisible);
			vView.getComponent(tasksPopupId).setVisible(tasksPopupVisible);
			propertyMap.put("tasksBarVisible", tasksBarVisible);
			propertyMap.put("tasksPopupVisible", tasksPopupVisible);
		}
		else if (event.getData().equals("show_bar")) {
			//NOTE the tasks pop-up is closed, so hide it and show the tasks bar

			//just like for the original tasks component set opened for the case component
			setRunComponentStatus(tasksCaseComponent, AppConstants.statusKeyOpened, AppConstants.statusValueFalse);

			tasksBarVisible = true;
			tasksPopupVisible = false;
			vView.getComponent(tasksBarId).setVisible(tasksBarVisible);
			vView.getComponent(tasksPopupId).setVisible(tasksPopupVisible);
			propertyMap.put("tasksBarVisible", tasksBarVisible);
			propertyMap.put("tasksPopupVisible", tasksPopupVisible);
		}
	}
	
}
