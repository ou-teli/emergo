/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.JuniorScientist;

import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Image;
import org.zkoss.zul.Textbox;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.utilities.AntiSamyHelper;
import nl.surf.emergo.zkspring.STriggeredReference;

/**
 * This class is used to handle an employee-card. There is a control to initialize the card. If clicked a pop-up is shown to enter employee data. If saved an icon is shown with a picture of the employee or the default icon.
 * If the icon is clicked the pop-up is shown to change the card.  
 * This class uses three states entered within a states case component with name 'game_states'. There is a state tag with pid 'employee_card_opened' to store if the employee-card edit pop-up is opened or not.
 * A state tag with pid 'employee_card_name' is used for the user's name and another with pid 'employee_card_image_blobid' is used to store the blob id of the uploaded user image. NOTE that this blob will not be removed if user status is cleared.
 *  
 * Note that because the employee-card macro is defined under the root element of the navigation component, it will be rerendered for each location, i.e., if present of the macro is true.
 * This way the macro is updated on each location. If you want to update the macro on the same location, use game script to set update of the macro to true.
 */
public class CRunJuniorScientistEmployeeCardBox extends CRunJuniorScientistBox {

	private static final long serialVersionUID = -1556146781872807334L;

	protected String employeeCardControlId;
	protected String employeeCardPopupId;
	protected String employeeIconId;
	
	protected boolean employeeCardControlVisible = true;
	protected boolean employeeCardPopupVisible = false;
	protected boolean employeeIconVisible = false;

	@Override
	public void onInit() {
		super.onInitMacro();
		
		//NOTE super ancestor CRunComponent needs case component to be set
		setCaseComponent(getStatesCaseComponent());

		employeeCardControlId = getUuid() + "_control";
		employeeCardPopupId = getUuid() + "_popup";
		employeeIconId = getUuid() + "_icon";
		
		//determine employee card properties to be used within the child macro
		setEmployeeCardProperties();
		
		//add the child macro
		addChildMacro("JuniorScientist_employee_card_view_macro.zul");
	}
	
	@Override
	public void onUpdate() {
		//when updated, e.g., because employee card is changed, employee card properties have to be set again
		setEmployeeCardProperties();
		
		//rerender child macro with adjusted properties
		childMacro.recreate();
	}

	protected void setEmployeeCardProperties() {
		boolean employeeCardOpened = getStateTagValue(employeeCardOpenedStateKey).equals(AppConstants.statusValueTrue);
		boolean employeeCardEverClosed = false;
		IXMLTag stateTag = getStateTag(employeeCardOpenedStateKey);
		if (stateTag != null) {
			employeeCardEverClosed = stateTag.getStatusAttributeCount(AppConstants.statusKeyValue, AppConstants.statusValueFalse) > 0;
		}

		employeeCardControlVisible = !employeeCardOpened && !employeeCardEverClosed;
		employeeCardPopupVisible = employeeCardOpened;
		employeeIconVisible = employeeCardEverClosed;
		
		propertyMap.put("employeeCardControlVisible", employeeCardControlVisible);
		propertyMap.put("employeeCardPopupVisible", employeeCardPopupVisible);
		propertyMap.put("employeeIconVisible", employeeIconVisible);

		String defaultEmployeeCardName = sSpring.getSCaseRoleHelper().getCaseRole().getName();
		String employeeCardName = getStateTagValue(employeeCardNameStateKey);
		String employeeCardUrl = getEmployeeCardUrl();
		String employeeCardIconStyle = "";
		if (!employeeCardUrl.equals("")) {
			employeeCardIconStyle = "background-image: url('" + vView.getEmergoWebappsRoot() + employeeCardUrl + "');";
		}
		propertyMap.put("defaultEmployeeCardName", defaultEmployeeCardName);
		propertyMap.put("employeeCardName", employeeCardName);
		propertyMap.put("employeeCardUrl", employeeCardUrl);
		propertyMap.put("employeeCardIconStyle", employeeCardIconStyle);
	}
	
	/** If a button is clicked within the child macro file, following event is triggered. */
	public void onButtonClick(Event event) {
		if (event.getData().equals("show_popup")) {
			//NOTE the employee card control or icon is clicked, so hide them and show the employee card pop-up

			setStateTagValue(employeeCardOpenedStateKey, AppConstants.statusValueTrue);

			//rerender macro to show new situation
			Events.echoEvent("onUpdate", this, null);
		}
		else if (event.getData().equals("close_popup")) {
			//NOTE the employee card pop-up is closed, so hide it and show either the employee card control or icon

			setStateTagValue(employeeCardOpenedStateKey, AppConstants.statusValueFalse);
			
			//save employee name and picture
			Textbox textbox = (Textbox)vView.getComponent(getUuid() + "_popup_textbox");
			if (textbox != null) {
				setStateTagValue(employeeCardNameStateKey, AntiSamyHelper.cleanup(textbox.getValue()));
			}
			Image image = (Image)vView.getComponent(getUuid() + "_popup_image");
			if (image != null && image.getContent() != null) {
				String blobId = storeImageAsBlob(getStateTagValue(employeeCardImageBlobIdStateKey), image.getContent());
				setStateTagValue(employeeCardImageBlobIdStateKey, blobId);
			}

			//rerender macro to show new situation
			Events.echoEvent("onUpdate", this, null);
		}
	}
	
	public String storeImageAsBlob(String blobId, Media media) {
		String fileName = media.getName();

		return sSpring.getSBlobHelper().setBlobMedia(blobId, media, fileName, AppConstants.blobtypeDatabase);
	}

	@Override
	public void handleStatusChange(STriggeredReference aTriggeredReference) {
	}

}
