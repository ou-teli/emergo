/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.Tyconstructor;

import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;

import nl.surf.emergo.control.def.CDefDiv;

public class CRunDashboardSkillsDiv extends CDefDiv {

	private static final long serialVersionUID = 6826458779162198351L;

	public void onCreate(CreateEvent aEvent) {
		Map<String,Object> propertyMap = (Map<String,Object>)aEvent.getArg().get("a_propertyMap");
		List<String> skill_labels = (List<String>)propertyMap.get("skill_labels");
		List<String> slider_style_per_skill = (List<String>)propertyMap.get("slider_style_per_skill");
		
		Div div1 = CRunDashboardInitBox.appendDiv(this, "header_label", null, true);
		CRunDashboardInitBox.appendLabel(div1, "header_label", null, (String)propertyMap.get("header_label"));
		CRunDashboardInitBox.appendLabel(this, "left_label", null, (String)propertyMap.get("left_label"));
		div1 = CRunDashboardInitBox.appendDiv(this, "skills", null, true);
		
		//there are five skills, so five sliders
		for (int i=0;i<5;i++) {
			Div div2 = CRunDashboardInitBox.appendDiv(div1, "skill", null, true);
			CRunDashboardInitBox.appendLabel(div2, "skill_label", null, skill_labels.get(i));
			CRunDashboardInitBox.appendLabel(div2, "skill_label skill_label_right", null, "100");
			CRunDashboardInitBox.appendDiv(div2, "skill_slider_background", null, true);
			Hbox hbox = CRunDashboardInitBox.appendHbox(div2);
			CRunDashboardInitBox.appendDiv(hbox, "skill_slider_start", null, true);
			CRunDashboardInitBox.appendDiv(hbox, "skill_slider_middle", slider_style_per_skill.get(i), true);
			CRunDashboardInitBox.appendDiv(hbox, "skill_slider_end", null, true);
		}

		//open details button
		Div div2 = new CRunDashboardOpenDetailsDiv();
		appendChild(div2);
		div2.setClass("open_details");
		CRunDashboardInitBox.appendLabel(div2, "open_details", null, (String)propertyMap.get("button_details_label"));
	}
	
}
