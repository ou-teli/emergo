/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.superTeacher;

import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Image;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitInitBox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTags;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitPeerGroupMember;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.view.VView;

public class CRunPVToolkitSuperTeacherPeerGroupsDiv extends CDefDiv {

	private static final long serialVersionUID = -118047044747128902L;

	public String zulfilepath = ((CRunPVToolkitInitBox)CDesktopComponents.vView().getComponent("PV-toolkit_initBox")).getZulfilepath();
	
	protected CRunPVToolkit pvToolkit;
		
	protected IERunGroup _actor;
	protected boolean _editable;
	protected List<IXMLTag> peerGroupTags;
	
	protected IAccountManager accountManager = (IAccountManager)CDesktopComponents.sSpring().getBean("accountManager");

	protected boolean isPreviewRun = false;

	protected String _idPrefix = "superTeacher";
	protected String _classPrefix = "superTeacher";
	
	public void init(IERunGroup actor, boolean editable) {
		_actor = actor;
		_editable = editable;
		
		setClass(_classPrefix);

		pvToolkit = (CRunPVToolkit)CDesktopComponents.vView().getComponent("pvToolkit");
		
		update();
		
		setVisible(true);
	}
	
	public void update() {
		getChildren().clear();
		
		pvToolkit.setMemoryCaching(true);
		
		isPreviewRun = CDesktopComponents.sSpring().getRun().getStatus() == AppConstants.run_status_test;

		Div div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{"titleRight"}
		);
		
		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font titleRight", "PV-toolkit-superTeacher.header.peerGroups"}
		);
		
		if (_editable) {
			Button btn = new CRunPVToolkitDefButton(this, 
					new String[]{"class", "labelKey"}, 
					new Object[]{"font pvtoolkitButton " + _classPrefix + "NewPeerGroupButton", "PV-toolkit-superTeacher.button.newPeerGroup"}
			);
			addNewPeerGroupButtonOnClickEventListener(btn);
		}

		div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "PeerGroupsDiv"}
		);
		
		Rows rows = appendPeerGroupsGrid(div);
		appendPeerGroupsToGrid(rows, 1);
		
		Button btn = new CRunPVToolkitDefButton(this, 
				new String[]{"class", "cLabelKey"}, 
				new Object[]{"font pvtoolkitButton " + _classPrefix + "BackButton", "PV-toolkit.back"}
		);
		addBackToRunsOnClickEventListener(btn, this, CDesktopComponents.vView().getComponent("superTeacherRunsDiv"));

		pvToolkit.setMemoryCaching(false);
	}

	protected void addNewPeerGroupButtonOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				CRunPVToolkitSuperTeacherNewOrEditPeerGroupDiv popup = (CRunPVToolkitSuperTeacherNewOrEditPeerGroupDiv)CDesktopComponents.vView().getComponent(_idPrefix + "NewOrEditPeerGroupDiv");
				if (popup != null) {
					popup.init(null);
				}
			}
		});
	}

	protected void addBackToRunsOnClickEventListener(Component component, Component fromComponent, Component toComponent) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				if (fromComponent != null) {
					fromComponent.setVisible(false);
				}
				if (toComponent != null) {
					toComponent.setVisible(true);
				}
			}
		});
	}

   protected Rows appendPeerGroupsGrid(Component parent) {
    	return pvToolkit.appendGrid(
    			parent, 
    			getId() + "Grid",
    			"position:absolute;left:0px;top:0px;width:1060px;height:430px;overflow:auto;", 
				new boolean[] {true, true, true, _editable, _editable, true, true}, 
    			new String[] {"3%", "35%", "10%", "7%", "7%", "28%", "10%"}, 
    			new boolean[] {false, true, true, false, false, true, true},
    			null,
    			"PV-toolkit-superTeacher.column.label.", 
    			new String[] {"", "name", "scope", "edit", "delete", "member", "role"});
    }

	protected int appendPeerGroupsToGrid(Rows rows, int rowNumber) {
		return appendPeerGroupsToGrid(
				rows, 
				rowNumber,
				new boolean[] {true, true, true, _editable, _editable, true, true} 
				);
	}

	protected int appendPeerGroupsToGrid(
			Rows rows, 
			int rowNumber,
    		boolean[] showColumn 
			) {
		CRunPVToolkitCacAndTags peerGroups = pvToolkit.getPeerGroupTags(_actor);
		peerGroupTags = peerGroups.getXmlTags();
		if (peerGroupTags == null) {
			return rowNumber;
		}

		for (IXMLTag peerGroupTag : peerGroupTags) {
			List<IXMLTag> memberTags = pvToolkit.getStatusChildTag(peerGroupTag).getChilds("member");
			for (int i=(memberTags.size()-1);i>=0;i--) {
				IXMLTag memberTag = memberTags.get(i);
				if (pvToolkit.getRunGroupAccount(pvToolkit.getStatusChildTagAttribute(memberTag, "rgaid")) == null) {
					memberTags.remove(i);
				}
			}
			if (memberTags.size() == 0) {
				rowNumber = appendPeerGroupRowToGrid(
						rows, 
						rowNumber,
						showColumn,
						peerGroupTag,
						null
						);
			}
			else {
				for (IXMLTag memberTag : memberTags) { 
					rowNumber = appendPeerGroupRowToGrid(
							rows, 
							rowNumber,
							showColumn,
							peerGroupTag,
							memberTag
							);
				}
			}
		}

		return rowNumber;
	}

	protected int appendPeerGroupRowToGrid(
			Rows rows, 
			int rowNumber,
    		boolean[] showColumn,
    		IXMLTag peerGroupTag,
    		IXMLTag memberTag
			) {
		Row row = new Row();
		rows.appendChild(row);
			
		int columnNumber = 0;
		if (showColumn.length > columnNumber && showColumn[columnNumber]) {
			new CRunPVToolkitDefImage(row, 
					new String[]{"class", "src"}, 
					new Object[]{"gridImage", zulfilepath + "user-friends.svg"}
					);
		}

		columnNumber++;
		if (showColumn.length > columnNumber && showColumn[columnNumber]) {
			new CRunPVToolkitDefLabel(row, 
					new String[]{"class", "value"}, 
					new Object[]{"font gridLabel", pvToolkit.getStatusChildTagAttribute(peerGroupTag, "name")}
					);
		}

		columnNumber++;
		if (showColumn.length > columnNumber && showColumn[columnNumber]) {
			new CRunPVToolkitDefLabel(row, 
					new String[]{"class", "labelKey"}, 
					new Object[]{"font gridLabel", "PV-toolkit.peergroup.scope." + pvToolkit.getStatusChildTagAttribute(peerGroupTag, "scope")}
					);
		}

		columnNumber++;
		if (showColumn.length > columnNumber && showColumn[columnNumber]) {
			Image image = new CRunPVToolkitDefImage(row,
					new String[]{"class", "src"}, 
					new Object[]{"gridImageClickable", zulfilepath + "edit.svg"}
			);
			addEditOnClickEventListener(image, peerGroupTag);
			image.setVisible(_editable);
		}

		columnNumber++;
		if (showColumn.length > columnNumber && showColumn[columnNumber]) {
			Image image = new CRunPVToolkitDefImage(row,
					new String[]{"class", "src"}, 
					new Object[]{"gridImageClickable", zulfilepath + "trash.svg"}
			);
			addDeleteOnClickEventListener(image, peerGroupTag);
			image.setVisible(_editable);
		}

		columnNumber++;
		if (showColumn.length > columnNumber && showColumn[columnNumber]) {
			String name = "";
			if (memberTag != null) {
				IERunGroupAccount runGroupAccount = pvToolkit.getRunGroupAccount(pvToolkit.getStatusChildTagAttribute(memberTag, "rgaid"));
				if (runGroupAccount != null) {
					if (isPreviewRun) {
						name = runGroupAccount.getERunGroup().getName();
					}
					else {
						name = accountManager.getAccountName(runGroupAccount.getEAccount());
					}
				}
			}
			new CRunPVToolkitDefLabel(row, 
					new String[]{"class", "value"}, 
					new Object[]{"font gridLabel", name}
					);
		}

		columnNumber++;
		if (showColumn.length > columnNumber && showColumn[columnNumber]) {
			String role = "";
			if (memberTag != null) {
				role = pvToolkit.getStatusChildTagAttribute(memberTag, "role");
			}
			new CRunPVToolkitDefLabel(row, 
					new String[]{"class", "labelKey"}, 
					new Object[]{"font gridLabel", "PV-toolkit.peergroup.member.role." + role}
					);
		}

		rowNumber ++;

		return rowNumber;
	}

	protected void addEditOnClickEventListener(Component component, IXMLTag peerGroupTag) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				CRunPVToolkitSuperTeacherNewOrEditPeerGroupDiv popup = (CRunPVToolkitSuperTeacherNewOrEditPeerGroupDiv)CDesktopComponents.vView().getComponent(_idPrefix + "NewOrEditPeerGroupDiv");
				if (popup != null) {
					popup.init(peerGroupTag);
				}
			}
		});
	}
	
	protected void addDeleteOnClickEventListener(Component component, IXMLTag peerGroupTag) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				VView vView = CDesktopComponents.vView();
				int choice = vView.showMessagebox(getRoot(), vView.getCLabel("PV-toolkit.delete.confirmation"), vView.getCLabel("PV-toolkit.delete"), Messagebox.CANCEL | Messagebox.OK, Messagebox.QUESTION);
				if (choice == Messagebox.OK) {
					deletePeerGroup(peerGroupTag);
				}
			}
		});
	}
	
	public boolean doesPeerGroupNameExist(IXMLTag peerGroupTag, String name) {
		for (IXMLTag tempPeerGroupTag : peerGroupTags) {
			String tempName = pvToolkit.getStatusChildTagAttribute(tempPeerGroupTag, "name").trim();
			if (tempName.equals(name) && (peerGroupTag == null || tempPeerGroupTag != peerGroupTag)) {
				return true;
			}
		}
		return false;
	}
	
	public void savePeergroup(IXMLTag peerGroupTag, String name, String scope, List<CRunPVToolkitPeerGroupMember> peerGroupMembers) {
		if (pvToolkit.savePeerGroup(_actor, peerGroupTag, name, scope, peerGroupMembers)) {
			update();
		}
	}
	
	public void deletePeerGroup(IXMLTag peerGroupTag) {
		if (pvToolkit.deletePeerGroup(peerGroupTag)) {
			update();
		}
	}
	
}
