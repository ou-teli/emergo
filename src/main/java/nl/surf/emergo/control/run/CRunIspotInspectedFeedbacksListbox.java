/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.List;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefListbox;
import nl.surf.emergo.view.VView;

public class CRunIspotInspectedFeedbacksListbox extends CDefListbox {

	private static final long serialVersionUID = 6450541317192458085L;

	protected VView vView = CDesktopComponents.vView();

	protected CRunIspot runIspot = null;

	public void onCreate(CreateEvent aEvent) {
		runIspot = (CRunIspot)vView.getComponent("runIspot");
	}

	public void onUpdate(Event aEvent) {
		//remove listbox items
		getItems().clear();
		//get inspected feedbacks
		List<CRunIspotIntervention> dataElements = runIspot.getInspectedFeedbacks();
		//add listbox items
		for (CRunIspotIntervention element : dataElements) {
			CRunIspotInspectedFeedback dataElement = (CRunIspotInspectedFeedback)element;
			Listitem listitem = new Listitem();
			listitem.setAttribute("dataElement", dataElement);
			appendChild(listitem);
			//0
			Listcell listcell = new Listcell(dataElement.getReactionRga().getERunGroup().getName());
			listcell.setStyle(runIspot.listboxStyle);
			listitem.appendChild(listcell);
			//1
			listcell = new Listcell(dataElement.getGivenFeedbackRga().getERunGroup().getName());
			listcell.setStyle(runIspot.listboxStyle);
			listitem.appendChild(listcell);
			//2
			listcell = new Listcell(dataElement.getSubjectName());
			listcell.setStyle(runIspot.listboxStyle);
			listitem.appendChild(listcell);
			//3
			listcell = new Listcell(dataElement.getVignetteName());
			listcell.setStyle(runIspot.listboxStyle);
			listitem.appendChild(listcell);
			//4
			listcell = new Listcell();
			listcell.setImage(runIspot.stylePath + "ispot-"+ dataElement.getInspectedFeedbackState() + ".png");
			listcell.setTooltiptext(vView.getLabel("run_ispot.listcell.inspected_feedback." + dataElement.getInspectedFeedbackState()));
			listcell.setStyle(runIspot.listboxStyle);
			listitem.setAttribute("stateCell", listcell);
			listitem.appendChild(listcell);
			//5
			listcell = new Listcell();
			listcell.setLabel(dataElement.getInspectedFeedbackDoneDate());
			listcell.setStyle(runIspot.listboxStyle);
			listitem.setAttribute("doneDateCell", listcell);
			listitem.appendChild(listcell);
		}
		restoreSettings();
	}

	public void onSelect(Event aEvent) {
		runIspot.setComponentVisible(vView.getComponent("ispotInspectedFeedbacks"), false);
		runIspot.setComponentVisible(vView.getComponent("ispotInspectedFeedbacksButton"), true);
		CRunIspotInspectedFeedback dataElement = (CRunIspotInspectedFeedback)getSelectedItem().getAttribute("dataElement");
		if (dataElement.getInspectedFeedbackState().equals("empty")) {
			dataElement.setInspectedFeedbackState("busy");
		}
		//set opened state of inspected feedback
		runIspot.setInspectedFeedbackStatus(dataElement, AppConstants.statusKeyOpened, AppConstants.statusValueTrue);
		Events.sendEvent("onUpdateState", this, dataElement);
		Events.postEvent("onUpdate", vView.getComponent("ispotInspectedFeedback"), dataElement);
		runIspot.setComponentVisible(vView.getComponent("ispotViewers"), true);
		runIspot.setComponentVisible(vView.getComponent("ispotInspectedFeedback"), true);
	}

	public void onUpdateState(Event aEvent) {
		CRunIspotInspectedFeedback dataElement = (CRunIspotInspectedFeedback)getSelectedItem().getAttribute("dataElement");
		getSelectedItem().setAttribute("dataElement", dataElement);
		((Listcell)getSelectedItem().getAttribute("stateCell")).setImage(runIspot.stylePath + "ispot-"+ dataElement.getInspectedFeedbackState() + ".png");
		((Listcell)getSelectedItem().getAttribute("stateCell")).setTooltiptext(vView.getLabel("run_ispot.listcell.inspected_feedback." + dataElement.getInspectedFeedbackState()));
		((Listcell)getSelectedItem().getAttribute("doneDateCell")).setLabel(dataElement.getInspectedFeedbackDoneDate());
	}

	public void onPrevious(Event aEvent) {
		setSelectedIndex(getSelectedIndex() - 1);
		if (getSelectedItem() != null) {
			Events.sendEvent("onSelect", this, null);
		}
	}

	public void onNext(Event aEvent) {
		setSelectedIndex(getSelectedIndex() + 1);
		if (getSelectedItem() != null) {
			Events.sendEvent("onSelect", this, null);
		}
	}

}
