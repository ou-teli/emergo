/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.feedback;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitInitBox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefHbox;

public class CRunPVToolkitFeedbackRatingHbox extends CRunPVToolkitDefHbox {
	
	private static final long serialVersionUID = 456507054823971884L;

	public String zulfilepath = ((CRunPVToolkitInitBox)CDesktopComponents.vView().getComponent("PV-toolkit_initBox")).getZulfilepath();
	
	protected int _numberOfPerformanceLevels = 5;
	
	protected int _level;
	protected boolean _editable = true;
	
	protected String _classPrefix = "rating";
	
	protected CRunPVToolkit pvToolkit;
	
	public CRunPVToolkitFeedbackRatingHbox(Component parent, String[] keys, Object[] values) {
		super(parent, keys, values);
	}
	
	public void init(int level, boolean pEditable) {
		
		pvToolkit = (CRunPVToolkit)CDesktopComponents.vView().getComponent("pvToolkit");
		
		update(level, pEditable);
	}
	
	public void update() {
		update(_level, _editable);
	}
	
	public void update(int level, boolean pEditable) {
		_level = level;
		_editable = pEditable;

		getChildren().clear();

		for (int i=1;i<=_numberOfPerformanceLevels;i++) {
			appendStar(this, i, _level, _editable);
		}

	}

	public void appendStar(Component parent, int level, int chosenLevel, boolean pEditable) {
		String src = "";
		//NOTE if performance levels are filtered, e.g., showing levels 1, 3 and 5 and omitting levels 2 and 4, given level scores have to be converted.
		//_numberOfPerformanceLevelsToShow then will be 3 instead of 5 and _performanceLevelValues contains the converted labels.
		//E.g., levels 1,2,3,4,5 will be converted to levels 1,2,2,2,3
		if ((chosenLevel > 0) && (chosenLevel >= level)) {
			if (pEditable) {
				//TODO
				src = zulfilepath + "star-filled-blue.svg";
			}
			else {
				src = zulfilepath + "star-filled-yellow.svg";
			}
		}
		else {
			src = zulfilepath + "star-gray.svg";
		}
		CRunPVToolkitFeedbackRatingImage lImage = new CRunPVToolkitFeedbackRatingImage(parent, 
				new String[]{"class", "src"}, 
				new Object[]{_classPrefix + "RatingLevel", src}
		);
		lImage.init(level, pEditable);
		if (pEditable)
			addStarOnClickEventListener(lImage, this);
	}

	public int getLevel() {
		return _level;
	}
	
	public void setLevel(int pLevel) {
		getChildren().clear();
		update(pLevel, true);
	}

	protected void addStarOnClickEventListener(Component pComponent, CRunPVToolkitFeedbackRatingHbox pSelf) {
		pComponent.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				setLevel(((CRunPVToolkitFeedbackRatingImage)pComponent).getLevel());
			}
		});
	}

}
