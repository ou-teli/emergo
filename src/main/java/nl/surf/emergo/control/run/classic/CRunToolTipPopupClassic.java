/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.OpenEvent;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Html;
import org.zkoss.zul.Image;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefPopup;
import nl.surf.emergo.control.run.CRunBlobImage;

/**
 * The Class CRunToolTipPopup is used to show tool tips.
 */
public class CRunToolTipPopupClassic extends CDefPopup {

	private static final long serialVersionUID = 851500568288043326L;

	public void onOpen(OpenEvent aEvent) {
		setLeft("299px");
		setLeft("300px");
		Component lComponent = (Component)aEvent.getReference();
	    Html lHtml = (Html)CDesktopComponents.vView().getComponent("toolTipLabel");
	    Hbox lHbox = (Hbox)CDesktopComponents.vView().getComponent("toolTipImage");
	    if (lComponent != null && lHtml != null && lHbox != null) {
			String lToolTip = (String)(lComponent.getAttribute("tooltip"));
	    	if (!StringUtils.isEmpty(lToolTip)) {
	    		lHtml.setContent(lToolTip);
	    	}
	    	lHbox.getChildren().clear();
	    	String bloid = (String)lComponent.getAttribute("bloid");
	    	if (bloid != null) {
	    		if (!((String)bloid).equals("")) {
	    			Image lImage = new CRunBlobImage((IXMLTag)lComponent.getAttribute("item"));
	    			lImage.setWidth("150px");
	    			lHbox.appendChild(lImage);
	    		}
	    	}
	    }
	}
	
}