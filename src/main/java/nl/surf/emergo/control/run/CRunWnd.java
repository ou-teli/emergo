/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.zkoss.web.Attributes;
import org.zkoss.zk.ui.AbstractComponent;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.ClientInfoEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CCaseHelper;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CIObserved;
import nl.surf.emergo.control.CXmlHelper;
import nl.surf.emergo.control.crm.CCrmRunGroupAccountsHelper;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.domain.IERunGroupCaseComponentUpdate;
import nl.surf.emergo.utilities.FileHelper;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpring;

/**
 * The Class CRunWnd. The Emergo player window.
 */
public class CRunWnd extends CRunArea
 {

	private static final long serialVersionUID = 163409593572522552L;

	/** The s spring. */
	public SSpring sSpring = CDesktopComponents.sSpring();

	/** The c run main timer. */
	public CRunMainTimer runMainTimer = null;

	/** The c run scroll into view timer. */
	public CRunScrollIntoViewTimer runScrollIntoViewTimer = null;

	/** The run scroll into view counter. */
	protected long runScrollIntoViewCounter = 0;

	/** The p run status change list. Used to buffer status changes due to script actions. */
	protected List<CRunStatusChange> pRunStatusChangeList = new ArrayList<CRunStatusChange>();

	/** The p idle, used to prevent parallel or overlapping method handling. */
	protected boolean pIdle = true;

	/** The p run action list. Used to buffer actions. */
	protected List<CRunAction> pRunActionList = new ArrayList<CRunAction>();

	/** The preselected case component. A case component can be preselected when starting the player,
	 *  for instance when player is started from preview option. */
	protected IECaseComponent preselectedCaseComponent = null;

	/** The preselected tag. A case component tag can be preselected when starting the player,
	 *  for instance when player is started from preview option. */
	protected IXMLTag preselectedTag = null;

	/** The desktop id. Used to save current desktop id. Is used when player is closed and status has to be saved. */
	public String desktopId = "";

	/** The rga id. The rgaid of the current user. */
	public String rgaId = "";

	/** The c case helper. */
	public CCaseHelper cCaseHelper = new CCaseHelper(sSpring);

	/** The p add actions boolean. Used to prevent multiple occurences of the same action within the location actions popup. */
	protected boolean pAddActions = true;

	/** The show media control. Used to show controls for the Windows Media Player. */
	public boolean showMediaControl = false;

	/** The current case component for notes. A note is made contextualised. It belongs to a certain case component. */
	protected IECaseComponent currentCaseComponentForNotes = null;

	/** The current tag for notes. A note is made contextualised. It possibly belongs to a certain tag. */
	protected IXMLTag currentTagForNotes = null;
	
	/** The event queue for rgas opening player. */
	public EventQueue<Event> eventQueueOpenPlayer = EventQueues.lookup("openPlayer", EventQueues.APPLICATION, true);

	/** The event queue for rgas closing player. */
	public EventQueue<Event> eventQueueClosePlayer = EventQueues.lookup("closePlayer", EventQueues.APPLICATION, true);

	/** The event queue for run group case component update sync. */
	public EventQueue<Event> eventQueueRunGroupCaseComponentUpdateSync = EventQueues.lookup("runGroupCaseComponentUpdateSync", EventQueues.APPLICATION, true);

	/** The event queue for run group case component update async. */
	public EventQueue<Event> eventQueueRunGroupCaseComponentUpdateAsync = EventQueues.lookup("runGroupCaseComponentUpdateAsync", EventQueues.APPLICATION, true);

	/** The event for run group case component update. */
	public String eventRunGroupCaseComponentUpdate = "onRunGroupCaseComponentUpdate";
	
	/** The is initialized up. */
	protected boolean isInitialized = false;

	/** The store at cleanup. */
	protected boolean storeAtCleanup = true;

	/** The store at cleanup set. */
	protected boolean storeAtCleanupSet = false;

	/** The is cleaned up. */
	protected boolean isCleanedUp = false;

	/** The is cleaned up. */
	protected boolean clientInfoShown = false;

	/** The navigator user agent. */
	protected String navigatorUserAgent = "unknown";
	
	/** The navigator cookie enabled. */
	protected String navigatorCookieEnabled = "unknown";

	/** The client info event. */
	protected ClientInfoEvent clientInfoEvent;

	/**
	 * Instantiates a new c run wnd.
	 * Reads google maps key out of .properties file and set it as session var,
	 * so google maps will work within the player.
	 * - get request parameter rgaId, the id of the user opening the player
	 * - get request parameter rutId, the possible runteam the user belongs to
	 */
	public CRunWnd() {
		sSpring.setPropRunWnd(this);
		// We have to get the key of Google Maps.
		// This key is used by ZK to generate a proper script
		// To add more keys: http://www.google.com/apis/maps/signup.html
		String gkey = sSpring.getAppManager().getSysvalue(AppConstants.syskey_gmapskey);
		if (gkey != null && !gkey.equals("")) {
			Executions.getCurrent().getDesktop().getSession().setAttribute("gmapsKey", gkey);
		}

		setZclass(getClassName());

		String lRgaId = VView.getReqPar("rgaId");
		String lAccId = VView.getReqPar("accId");
		String lRunId = VView.getReqPar("runId");
		String lCarId = VView.getReqPar("carId");
		String lRutId = VView.getReqPar("rutId");

		String lRunStatus = VView.getReqPar("runstatus");
		sSpring.setRunStatus(lRunStatus);

		rgaId = lRgaId;
		if (rgaId == null || rgaId.equals("") || rgaId.equals("0")) {
			// open run; create new rungroup and rungroupaccount if not already present
			// (could have been created in earlier run in same session)
			if (!((lAccId == null) || (lAccId.equals("")) || (lAccId.equals("0")))) {
				int lAccInt = Integer.parseInt(lAccId);
				int lRunInt = Integer.parseInt(lRunId);
				int lCarInt = Integer.parseInt(lCarId);
				CCrmRunGroupAccountsHelper cHelper = new CCrmRunGroupAccountsHelper();
				IERunGroup lRug = cHelper.addRungroupIfNotExists(lAccInt, lRunInt, lCarInt, true);
				if (lRug != null) {
					IERunGroupAccount lRga = cHelper.addRungroupaccountIfNotExists(lAccInt, lRug.getRugId());
					if (lRga != null) {
						rgaId = "" + lRga.getRgaId();
					}
				}
			}
		}
		if (rgaId != null && !rgaId.equals("") && !rgaId.equals("0")) {
			//sSpring.setRunGroupAccount(Integer.parseInt(rgaId));
			detach();
			
			IERunGroupAccount runGroupAccount = sSpring.getRunGroupAccount(Integer.parseInt(rgaId));
			if (runGroupAccount == null) {
				detach();
				return;
			}
			else {
				sSpring.setRunGroupAccount(runGroupAccount);
				if (!sSpring.isReadOnlyRun() && sSpring.isRunGroupAccountAddedToActiveRunGroupAccounts()) {
					//CDesktopComponents.vView().forbidden();
					sSpring.getSLogHelper().logAction("DOUBLE RECORDS TEST: CRunWnd(): detach();");
					detach();
					return;
				}
				else {
					sSpring.getSLogHelper().logAction("DOUBLE RECORDS TEST: CRunWnd(): sSpring.addRunGroupAccountToActiveRunGroupAccounts();");
					sSpring.addRunGroupAccountToActiveRunGroupAccounts();
				}
			}
		}
		if (lRutId != null && !lRutId.equals("") && !lRutId.equals("0")) {
			sSpring.setRunTeam(Integer.parseInt(lRutId));
		}

		CDesktopComponents.cControl().setSessAttr(Attributes.PREFERRED_LOCALE, null);

		CDesktopComponents.cControl().handleLanguage(null);
	}

	/**
	 * Creates run main timer.
	 *
	 * @return timer
	 */
	protected CRunMainTimer createMainTimer() {
		return new CRunMainTimer("runMainTimer");
	}

	/**
	 * Creates run scroll into view timer.
	 *
	 * @return timer
	 */
	protected CRunScrollIntoViewTimer createScrollIntoViewTimer() {
		return new CRunScrollIntoViewTimer("runScrollIntoViewTimer");
	}

	/**
	 * On create:
	 * - save ZK desktop id to use it when ZK desktop is closed
	 * - get request parameter runstatus, see IAppManager
	 * - init desktop attributes
	 * - execute specific javascript on client
	 * - get request parameter cacId, the possible casecomponent to preselect
	 * - get request parameter tagId, the possible tag to preselect
	 * - get request parameter mediacontrol, if windows media player will show controls
	 * - get request parameters for language to use
	 * - start CRunMainTimer
	 * - create CRunScrollIntoViewTimer
	 * - add rungroupaccount to rungroupaccounts who have an open player, so who are
	 *   playing right now
	 *
	 * @param aEvent the a event
	 */
	public void onCreate(Event aEvent) {
		if (!sSpring.isReadOnlyRun()) {
			if (CDesktopComponents.cControl().getRunSessAttr(CControl.runWnd + "readwrite") != null) {
				CDesktopComponents.cControl().setRunSessAttr(CControl.runWnd + "readwrite", null);
				detach();
				return;
			}
		}
		
		handleAccess();

		//NOTE fade out runWnd, so it can be faded in when first location is shown
		fadeOutComponent(CControl.runWnd, 0);

		// save desktop id to use if desktop is closed
		desktopId = getDesktop().getId();
		// get request parameter for rungroupaccount id and get rungroupaccount
		// rungroupaccount is used within sSpring to get account, case, run,
		// caserole and rungroup

		CDesktopComponents.vView();
		
//		String lRunStatus = VView.getReqPar("runstatus");
//		sSpring.setRunStatus(lRunStatus);

		//TODO Apple devices Safari double records
		sSpring.getSLogHelper().logAction("DOUBLE RECORDS TEST: onCreate(Event aEvent): checkIfPlayerIsAlreadyStartedForCurrentRga()");
		checkIfPlayerIsAlreadyStartedForCurrentRga();
				
		String lCacId = VView.getReqPar("cacId");
		String lTagId = VView.getReqPar("tagId");

		sSpring.initDesktopAttributes(getDesktop());

		executeJavascript();
		
		// get request parameters for casecomponent and tag
		// this parameters are set if preview option is used
		if (lCacId != null && !lCacId.equals("")) {
			preselectedCaseComponent = sSpring.getCaseComponent(Integer.parseInt(lCacId));
		}
		if (preselectedCaseComponent != null) {
			if (lTagId != null) {
				preselectedTag = sSpring.getTag(preselectedCaseComponent, lTagId);
			}
		}
		String lMediaControl = (String)VView.getReqPar("mediacontrol");
		showMediaControl = lMediaControl != null && lMediaControl.equals(AppConstants.statusValueTrue);

		//doOverlapped();
		// (re)create session time
		runMainTimer = createMainTimer();
		appendChild(runMainTimer);
		runMainTimer.start();
		runScrollIntoViewTimer = createScrollIntoViewTimer();
		appendChild(runScrollIntoViewTimer);
		CDesktopComponents.cControl().setRunSessAttr(CControl.runWnd, this);

		if (!sSpring.isReadOnlyRun()) {
			CDesktopComponents.cControl().setRunSessAttr(CControl.runWnd + "readwrite", this);
		}
	}

	/**
	 * Executes java script on client.
	 */
	protected void executeJavascript() {
		Clients.evalJavaScript(
				"window.moveTo(0,0);" +
				"window.resizeTo(" + getDesktop().getAttribute("browserWidth") + "," + getDesktop().getAttribute("browserHeight") + ");" +
				"externalEventToServer('runWnd','runWnd','onNavigatorUserAgent',navigator.userAgent);" +
				"externalEventToServer('runWnd','runWnd','onNavigatorCookieEnabled','' + navigator.cookieEnabled);"
				);
	}

	/**
	 * Check if there are run actions that should be executed. It are actions defined by the case run manager.
	 * Is called by CRunMainTimer if player is opened for first time.
	 */
	protected void executeRunActions() {
		IXMLTag lRootTag = CDesktopComponents.sSpring().getXmlRunDataTree(CDesktopComponents.sSpring().getRun());
		if (lRootTag == null) {
			return;
		}
		IXMLTag lContentTag = lRootTag.getChild(AppConstants.contentElement);
		if (lContentTag == null) {
			return;
		}
		for (IXMLTag lRunActionTag : lContentTag.getChildTags(AppConstants.defValueNode)) {
			IECaseComponent lCaseComponent = getCaseComponent(lRunActionTag);
			if (lCaseComponent != null) {
				IXMLTag lDataTag = getDataTag(lRunActionTag);
				String lStatusKey = getStatusKey(lRunActionTag);
				String lStatusValue = getStatusValue(lRunActionTag);
				//NOTE Don't check script, because it are initial values
				boolean lCheckScript = false;
				if (lDataTag == null) {
					CDesktopComponents.sSpring().setRunComponentStatus(lCaseComponent,
							lStatusKey, lStatusValue, lCheckScript, AppConstants.statusTypeRunGroup, true);
				}
				else {
					CDesktopComponents.sSpring().setRunTagStatus(lCaseComponent, lDataTag,
							lStatusKey, lStatusValue, lCheckScript, AppConstants.statusTypeRunGroup, true, false);
				}
			}
		}
	}

	/**
	 * Gets set tag status tag.
	 *
	 * @param aActionTag the a action tag
	 *
	 * @return set tag status tag
	 */
	protected IXMLTag getMethodTag(IXMLTag aActionTag) {
		if (aActionTag == null) {
			return null;
		}
		IXMLTag lActionstringTag = aActionTag.getChild("actionstring");
		if (lActionstringTag == null) {
			return null;
		}
		if (lActionstringTag.getChildTags().size() != 1) {
			//can have only one child
			return null;
		}
		return lActionstringTag.getChildTags().get(0);
	}
	
	/**
	 * Gets case component.
	 *
	 * @param aActionTag the a action tag
	 *
	 * @return case component
	 */
	protected IECaseComponent getCaseComponent(IXMLTag aActionTag) {
		IXMLTag lMethodTag = getMethodTag(aActionTag);
		if (lMethodTag == null) {
			return null;
		}
		String lCacId = CXmlHelper.getMethodCacId(lMethodTag);
		return CDesktopComponents.sSpring().getCaseComponent(lCacId);
	}
	
	/**
	 * Gets data tag.
	 *
	 * @param aActionTag the a action tag
	 *
	 * @return data tag
	 */
	protected IXMLTag getDataTag(IXMLTag aActionTag) {
		IECaseComponent lCaseComponent = getCaseComponent(aActionTag);
		IXMLTag lMethodTag = getMethodTag(aActionTag);
		if (lMethodTag == null) {
			return null;
		}
		String[] lTagIds = CXmlHelper.getMethodTagIds(lMethodTag);
		if (lTagIds.length != 1) {
			return null;
		}
		return CDesktopComponents.sSpring().getTag(lCaseComponent, lTagIds[0]);
	}
	
	/**
	 * Gets status key.
	 *
	 * @param aActionTag the a action tag
	 *
	 * @return status key
	 */
	protected String getStatusKey(IXMLTag aActionTag) {
		IXMLTag lMethodTag = getMethodTag(aActionTag);
		if (lMethodTag == null) {
			return "";
		}
		String lStatusid = lMethodTag.getChildValue("statusid");
		return CDesktopComponents.sSpring().getAppManager().getStatusKey(Integer.parseInt(lStatusid));
	}
	
	/**
	 * Gets status value.
	 *
	 * @param aActionTag the a action tag
	 *
	 * @return status value
	 */
	protected String getStatusValue(IXMLTag aActionTag) {
		IXMLTag lMethodTag = getMethodTag(aActionTag);
		if (lMethodTag == null) {
			return "";
		}
		String lCacId = CXmlHelper.getMethodCacId(lMethodTag);
		String lTagname = CXmlHelper.getMethodTagName(lMethodTag);
		String lStatusid = CXmlHelper.getMethodStatusId(lMethodTag);
		String lOperatorvalue = CXmlHelper.getMethodOperatorValues(lMethodTag);
		String lStatusValue = lOperatorvalue;
		String lValueType = CDesktopComponents.sSpring().getAppManager().getTagOperatorValueType("", lCacId, lTagname, lStatusid, "");
		if (lValueType.equals("boolean")) {
			lStatusValue = CDesktopComponents.sSpring().getAppManager().getStatusValue(Integer.parseInt(lStatusValue));
		}
		return lStatusValue;
}
	
	/**
	 * Check if player is already started for current rga.
	 * If player for current rga is started read/write, cleanup up all other read/write players for current rga.
	 * Is called within onCreate method.
	 */
	protected synchronized void checkIfPlayerIsAlreadyStartedForCurrentRga() {
		//NOTE Years ago students reported problems in Firefox. When they opened the player they immediately
		//got the message that the player already was opened.
		//NOTE if current opened player window is read/write, check if there are other read/write ones opened still
		if (!sSpring.isReadOnlyRun()) {
			cleanupRunWnds(rgaId, true, false);
		}
	}

	/**
	 * Check for not cleanuped runWnds of other rgas. Because (older versions of) Opera doesn't get a cleanup() and
	 * in case of browser crashes, other runWnds check if there are still runWnds left to clean up.
	 * Is called by CRunMainTimer.
	 */
	protected synchronized void checkForNotCleanupedRunWndsOfOtherRgas() {
		Hashtable<String, List<CRunWnd>> lAllRunWnds = sSpring.getAppPropAllActiveRunWnds();
		//NOTE loop through all rga ids
		for (Enumeration<String> rgaIds = lAllRunWnds.keys(); rgaIds.hasMoreElements();) {
			String rgaId = rgaIds.nextElement();
			cleanupRunWnds(rgaId, false, true);
		}
	}

	/**
	 * Cleanup run wnds.
	 *
	 * @param aRgaId the rga id
	 * @param aCleanupCurrentRga the cleanup current Rga
	 * @param aCheckOnCurrentTime the check on current time
	 */
	protected synchronized void cleanupRunWnds(String aRgaId, boolean aCleanupCurrentRga, boolean aCheckOnCurrentTime) {
		//get all player windows (read-only and read/write) for rgaId
		List<CRunWnd> lRunWnds = sSpring.getAllActiveRunWndsPerRgaId(aRgaId);
		boolean lNewestFound = false;
		//NOTE get last read/write one added, the newest, and clean it up (saving its status)
		//Remove older read/write ones from memory
		for (int i=lRunWnds.size()-1;i>=0;i--) {
			CRunWnd lRunWnd = lRunWnds.get(i);
			//NOTE don't clean up or remove the current player window accidently
			if (lRunWnd != this && lRunWnd.sSpring != null && !lRunWnd.sSpring.isReadOnlyRun()) {
				boolean lIsCurrentRga = lRunWnd.rgaId.equals(this.rgaId);
				boolean lCleanup = (aCleanupCurrentRga && lIsCurrentRga) ||
						(!aCleanupCurrentRga && !lIsCurrentRga);
				if (aCheckOnCurrentTime) {
					//NOTE if other runwnd's timer is not active anymore for more than an hour, cleanup
					//NOTE timer of such a window is still running (isRunning() returns true), but it
					//does not get timer events anymore so currenttime is not increasing anymore. So this is
					//the only way to check if a player window is closed but is not cleaned up.
					long lCurrentTime = runMainTimer.currenttime;
					long lOtherCurrentTime = lRunWnd.runMainTimer.currenttime;
					lCleanup = lCleanup && (lCurrentTime > lOtherCurrentTime + 60000);
				}
				if (lCleanup) {
					if (!lNewestFound) {
						lNewestFound = true;
						sSpring.getSLogHelper().logAction("CLEANING UP RUNWIND for desktop id " + lRunWnd.desktopId);
						lRunWnd.cleanup();
						lRunWnd.runMainTimer.setDetachComponents(true);
					}
					else {
						sSpring.getSLogHelper().logAction("REMOVING OBSOLETE RUNWIND for desktop id " + lRunWnd.desktopId);
						removeRunWndFromMemory(lRunWnd.sSpring, lRunWnd.rgaId);
					} 
				}
			}
		}
	}
	
	/**
	 * saveAtCleanup. If the stored case time in the database exceeds the cached case time, this window doesn't contain
	 * the most up to date player status. Consider it a fake session, and don't store the cached components status.
	 *
	 * @return boolean true if this window represents an up to date player status
	 */
	protected boolean saveAtCleanup() {
		if (storeAtCleanupSet)
			return storeAtCleanup;
		double lOwnCaseTime = sSpring.getCaseTime();
		SSpring lSpring = new SSpring();
		lSpring.setRunGroupAccount(sSpring.getRunGroupAccount().getRgaId());
		long lSavedstarttime = 0;
		long lSavedcurrenttime = 0;
		IECaseComponent lCasecasecomponent = CDesktopComponents.sSpring().getCaseComponent("case", "");
		String lStartTime = lSpring.getCurrentRunComponentStatus(
				lCasecasecomponent, AppConstants.statusKeyStartTime, AppConstants.statusTypeRunGroup);
		if (!lStartTime.equals(""))
			lSavedstarttime = Long.parseLong(lStartTime);
		String lCurrentTime = lSpring.getCurrentRunComponentStatus(
				lCasecasecomponent, AppConstants.statusKeyCurrentTime, AppConstants.statusTypeRunGroup);
		if (!lCurrentTime.equals(""))
			lSavedcurrenttime = Long.parseLong(lCurrentTime);
		double lStoredCaseTime = (lSavedcurrenttime - lSavedstarttime)/1000;
		if (lStoredCaseTime > lOwnCaseTime) {
			storeAtCleanup = false;
			sSpring.getSLogHelper().logAction("runwind cleanup: possibly inactive runwind; don't store cached status: cached case time: " + 
					lOwnCaseTime + "; stored case time: " + lStoredCaseTime);
		}
		storeAtCleanupSet = true;
		return storeAtCleanup;
	}

	/**
	 * Cleanup. Is called when the ZK desktop (the current browser instance)
	 * is closed. But not for (older versions of) Opera! See onCreate method above.
	 * - saves note if note window is opened
	 * - saves all pending status for the user
	 * - removes current rungroupaccount from list of rungroupaccounts who are
	 *   playing right now
	 */
	public synchronized void cleanup() {
		if (isCleanedUp) {
			return;
		}
		
		/* TEST CODE
		runMainTimer.stop();
		*/
		
		boolean lSaveAtCleanup = saveAtCleanup(); 
		if (lSaveAtCleanup) {
			sSpring.setRunComponentStatus(runMainTimer.casecasecomponent,
					AppConstants.statusKeyOpened, AppConstants.statusValueFalse, true, AppConstants.statusTypeRunGroup, true);
		}

		isCleanedUp = true;
		

		//NOTE if cleanup caused by new player window, try to pass rug run status to new desktop
		if (lSaveAtCleanup && !this.getDesktop().getId().equalsIgnoreCase(Executions.getCurrent().getDesktop().getId()))
			if (sSpring.getRunGroupAccount().getRgaId() == CDesktopComponents.sSpring().getRunGroupAccount().getRgaId())
				sSpring.copyRunCaseComponentsStatus(CDesktopComponents.sSpring());

		if (lSaveAtCleanup) {
			saveAllStatus();
			if (isComponentUsed("adaptations")) {
				CRunComponent runAdaptations = (CRunComponent)CDesktopComponents.vView().getComponent("runAdaptations");
				if (runAdaptations != null) {
					runAdaptations.cleanup();
				}
			}
		}

		//TODO Apple devices Safari double records
		sSpring.getSLogHelper().logAction("DOUBLE RECORDS TEST: cleanup(): removeRunWndFromMemory(sSpring, rgaId);");
		removeRunWndFromMemory(sSpring, rgaId);
		
		//NOTE remove temporary files possibly created during uploading files.
		removeTempFiles();

		CDesktopComponents.cControl().setRunSessAttr(CControl.runWnd + "readwrite", null);

		if (sSpring != null) {
			sSpring.getSLogHelper().logAction("Player closes");
		}
		
		/* TEST CODE
		runMainTimer.storeCurrenTime();
		*/
		
	}
	
	/**
	 * Removes run wnd from memory.
	 *
	 * @param aSSpring the SSpring
	 * @param aRgaId the rga id
	 */
	public synchronized void removeRunWndFromMemory(SSpring aSSpring, String aRgaId) {
		// NOTE Only remove run group account if not read only, otherwise a tutor or admin using the read only option
		// could interfere with real users.
		if (!aSSpring.isReadOnlyRun()) {
			// remove run group account and run wnd from application memory
			aSSpring.removeRunWndFromActiveRunWndsPerRgaId();
			// NOTE user can open multiple browser instances.
			if (aSSpring.getAllActiveRunWndsPerRgaId(aRgaId).size() == 0) {
				// if no runwnd left remove it
				eventQueueClosePlayer.publish(new Event("onClosePlayer", null, "" + aSSpring.getRunGroupAccount().getRgaId()));
				aSSpring.removeRunGroupAccountFromActiveRunGroupAccounts();
			}
		}
	}
	
	/**
	 * Removes temp files from 'temp' folder.
	 *
	 */
	public synchronized void removeTempFiles() {
		//every desktop has a unique temp folder, remove this temp folder
		String lSubPath = CDesktopComponents.vView().getUniqueTempSubPath();
		if (!lSubPath.equals("")) {
			(new FileHelper()).deleteDir(sSpring.getAppManager().getAbsoluteTempPath() + lSubPath);
		}
	}
	
	/**
	 * Detach components. Called to clear screen.
	 */
	public void detachComponents() {
		detach();
		String lAction = "window.close()";
		Clients.evalJavaScript(lAction);
	}

	public void onClientInfo(ClientInfoEvent aClientInfoEvent) {
		//timing; wait until all information is collected
		if ((aClientInfoEvent == null) || clientInfoShown)
			return;
		clientInfoEvent = aClientInfoEvent;
		if ("unknown".matches(navigatorUserAgent + "|" + navigatorCookieEnabled))
				return;
		clientInfoShown = true;
		//TODO Apple devices Safari double records
		sSpring.getSLogHelper().logAction("DOUBLE RECORDS TEST: onClientInfo(ClientInfoEvent aClientInfoEvent)");
		String lBrowserName = "";
		String lBrowserVersion = "";
		Execution lExec = Executions.getCurrent();
		if (lExec != null) {
			lBrowserName = lExec.getBrowser();
			if (!lBrowserName.equals(""))
				lBrowserVersion = lExec.getBrowser(lBrowserName).toString();
		}
		String lClientInfo = "navigatorUserAgent=" + navigatorUserAgent + "&" +
			"navigatorCookieEnabled=" + navigatorCookieEnabled + "&" +
			"browsername=" + lBrowserName + "&" +
			"browserversion=" + lBrowserVersion + "&" +
			"screenWidth=" + aClientInfoEvent.getScreenWidth() + "&" +
			"screenHeight=" + aClientInfoEvent.getScreenHeight() + "&" +
			"desktopXOffset=" + aClientInfoEvent.getDesktopXOffset() + "&" +
			"desktopYOffset=" + aClientInfoEvent.getDesktopYOffset() + "&" +
			"desktopWidth=" + aClientInfoEvent.getDesktopWidth() + "&" +
			"desktopHeight=" + aClientInfoEvent.getDesktopHeight() + "&" +
			"devicePixelRatio=" + aClientInfoEvent.getDevicePixelRatio() + "&" +
			"colorDepth=" + aClientInfoEvent.getColorDepth() + "&" +
			"orientation=" + aClientInfoEvent.getOrientation();
		sSpring.getSLogHelper().logClientInfoAction("Player client info", lClientInfo);
	}
	
	/**
	 * On scrollIntoView is an Emergo event.
	 * Used to scroll a certain tree or listbox item into view, meaning it will be made visible if tree or listbox have a scrollbar.
	 * Timer scrollIntoViewTimer is used to be sure scrolling will take place. It could be that event onEmergoScrollIntoView is coming to
	 * early, so the target to scroll is not rendered yet.
	 *
	 * @param aEvent the a event
	 */
	public void onEmergoScrollIntoView(Event aEvent) {
		if (aEvent.getData() != null && aEvent.getData() instanceof Component) {
			emergoScrollIntoView((Component)aEvent.getData(), true);
		}
	}

	/**
	 * ScrollIntoView.
	 * Used to scroll a certain tree or listbox item into view, meaning it will be made visible if tree or listbox have a scrollbar.
	 * Timer scrollIntoViewTimer is used to be sure scrolling will take place. It could be that event onEmergoScrollIntoView is coming to
	 * early, so the target to scroll is not rendered yet.
	 *
	 * @param aComponent the a component
	 * @param aUseTimer the a use timer
	 */
	public void emergoScrollIntoView(Component aComponent, boolean aUseTimer) {
		if (aComponent == null) {
			return;
		}
		if (!aUseTimer) {
			Clients.scrollIntoView(aComponent);
			return;
		}
		if (aComponent.getId().equals("")) {
			//Id of component can be empty, so set it to find the component within timer scrollIntoViewTimer
			runScrollIntoViewCounter++;
			aComponent.setId("scrollIntoView" + runScrollIntoViewCounter);
			//Save that the id originally was empty
			setAttribute("isScrollIntoViewOriginalIdEmpty", "true");
		}
		else {
			//The original id was not empty
			setAttribute("isScrollIntoViewOriginalIdEmpty", "false");
		}
		//Save the id of the component, so it can be used within the timer
		setAttribute("scrollIntoViewId", aComponent.getId());
		//Start timer
		runScrollIntoViewTimer.start();
	}

	/**
	 * Init player.
	 */
	public void init() {
		if (isInitialized) {
			return;
		}
		
		isInitialized = true;
		
		/* TEST CODE
		runMainTimer.init();
		*/
		
		sSpring.getSLogHelper().logAction("Player starts loading");

		// NOTE Only add run group account if not read only, otherwise a tutor or admin using the read only option
		// could interfere with real users.
		if (!sSpring.isReadOnlyRun() && sSpring.getRunGroupAccount() != null) {
			// save run group account and run wnd in application memory
			//TODO Apple devices Safari double records
//			sSpring.logAction("DOUBLE RECORDS TEST: init(): sSpring.addRunGroupAccountToActiveRunGroupAccounts();");
//			sSpring.addRunGroupAccountToActiveRunGroupAccounts();
			sSpring.getSLogHelper().logAction("DOUBLE RECORDS TEST: init(): sSpring.addRunWndToActiveRunWndsPerRgaId();");
			sSpring.addRunWndToActiveRunWndsPerRgaId();
		}
		
		initComponents();
		subscribeToEventQueueRunGroupCaseComponentUpdates();

		eventQueueOpenPlayer.publish(new Event("onOpenPlayer", null, "" + sSpring.getRunGroupAccount().getRgaId()));
		
		//update with external run tags of other users
		onTimedAction(getId(), "onUpdateRunTags", null);
	}

	/**
	 * Init components.
	 */
	public void initComponents() {
		//Adaptations component has no visible elements and is skin-independent, so we can load it here: 
		initAdaptations();
		createComponents();
		sSpring.getSLogHelper().logAction("Player loaded");
		runMainTimer.initCase();
		showComponentsStatus();
		setWindowPosition();
	}

	/**
	 * Init adaptations.
	 */
	public void initAdaptations() {
		if (isComponentUsed("adaptations")) {
			CRunComponent runAdaptations = (CRunComponent)CDesktopComponents.vView().getComponent("runAdaptations");
			if (runAdaptations == null) {
				IECaseComponent lCaseComponent = sSpring.getCaseComponent("adaptations", "");
				if (lCaseComponent != null) {
					runAdaptations = createAdaptationsComponent(lCaseComponent);
					appendChild(runAdaptations);
					runAdaptations.setVisible(false);
				}
			}
		}
	}

	/**
	 * Creates adaptations component.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return run component
	 */
	protected CRunComponent createAdaptationsComponent(IECaseComponent aCaseComponent) {
		return new CRunAdaptations("runAdaptations", aCaseComponent);
	}
	
	/**
	 * Subscribe to event queues that handles data exchange between different roles, synchronous and asynchronous.
	 */
	public void subscribeToEventQueueRunGroupCaseComponentUpdates() {
		eventQueueRunGroupCaseComponentUpdateSync.subscribe(
			new EventListener<Event>() {
				public void onEvent(Event evt) {
					if (eventRunGroupCaseComponentUpdate.equals(evt.getName())) {
						sSpring.getSUpdateHelper().handleRunGroupCaseComponentUpdateEvent((IERunGroupCaseComponentUpdate)evt.getData());
					}
				}
			}
		);
		//NOTE Trick to make async work is using callback method parameter in subscribe and do nothing in first event listener.
		//subscribe(eventlistener, true) generates SpringUtil error.
		eventQueueRunGroupCaseComponentUpdateAsync.subscribe(
			new EventListener<Event>() {
				public void onEvent(Event evt) {
				}
			}
			,
			new EventListener<Event>() {
				public void onEvent(Event evt) {
					if (eventRunGroupCaseComponentUpdate.equals(evt.getName())) {
						sSpring.getSUpdateHelper().handleRunGroupCaseComponentUpdateEvent((IERunGroupCaseComponentUpdate)evt.getData());
					}
				}
			}
		);
	}
	
	/**
	 * Creates child components of player.
	 */
	public void createComponents() {
		// override
	}

	/**
	 * Shows components status.
	 * Is called when the player is started. Restores player status for current user
	 * and depending on runstatus. If preview, preselected casecomponent and tag are
	 * used to set the player in a certain state. Otherwise location is restored to
	 * last location visited during previous session.
	 */
	public void showComponentsStatus() {
		// override
	}

	/**
	 * Set window position on screen.
	 */
	public void setWindowPosition(){
		setPosition("center,center");
	}

	/**
	 * Gets player status string.
	 *
	 * @param aKey the a key
	 * @param aStatus the a status
	 *
	 * @return status string
	 */
	protected String getPlayerStatusString(String aKey, Object aStatus) {
		// override
		return "";
	}

	/**
	 * Gets player status xml tag.
	 *
	 * @param aKey the a key
	 * @param aStatus the a status
	 *
	 * @return status xml tag
	 */
	protected IXMLTag getPlayerStatusXmlTag(String aKey, Object aStatus) {
		// override
		return null;
	}

	/**
	 * Gets player status list.
	 *
	 * @param aKey the a key
	 * @param aStatus the a status
	 *
	 * @return status list
	 */
	public List getPlayerStatusList(String aKey, Object aStatus) {
		// override
		return new ArrayList();
	}

	/**
	 * Sets player status.
	 *
	 * @param aKey the a key
	 * @param aStatus the a status
	 */
	protected void setPlayerStatus(String aKey, Object aStatus) {
		// override
	}

	/**
	 * Checks if run status is equal to aStatus.
	 *
	 * @param aStatus the a status
	 *
	 * @return true, if is run status
	 */
	protected boolean isRunStatus(String aStatus) {
		String lStatus = sSpring.getRunStatus();
		if (lStatus == null)
			return false;
		return lStatus.equals(aStatus);
	}

	/**
	 * Is component used within case?
	 *
	 * @param aComponentCode the component code
	 *
	 * @return boolean
	 */
	protected boolean isComponentUsed(String aComponentCode) {
		String lShowName = sSpring.getCaseComponentRoleName(aComponentCode, "");
		return (!lShowName.equals(""));
	}

	/**
	 * Is component used within case and visible?
	 *
	 * @param aComponentCode the component code
	 *
	 * @return boolean
	 */
	protected boolean isComponentUsedAndVisible(String aComponentCode) {
		if (!isComponentUsed(aComponentCode)) {
			return false;
		}
		String lStatus = getCaseComponentRenderStatus(aComponentCode, "");
		return !(lStatus.equals("") || lStatus.equals("empty"));
	}

	/**
	 * Gets the current case component for notes.
	 *
	 * @return the current case component for notes
	 */
	public IECaseComponent getCurrentCaseComponentForNotes() {
		return currentCaseComponentForNotes;
	}

	/**
	 * Sets the current case component for notes. Clears current tag for notes.
	 *
	 * @param aCurrentCaseComponentForNotes the current case component for notes
	 */
	public void setNoteCaseComponent(IECaseComponent aCurrentCaseComponentForNotes) {
		currentCaseComponentForNotes = aCurrentCaseComponentForNotes;
		currentTagForNotes = null;
		setNoteContent();
	}

	/**
	 * Gets the current tag for notes.
	 *
	 * @return the current tag for notes
	 */
	public IXMLTag getCurrentTagForNotes() {
		return currentTagForNotes;
	}

	/**
	 * Sets the current tag for notes and current case component for notes.
	 *
	 * @param aCurrentCaseComponentForNotes the current case component for notes
	 * @param aCurrentTagForNotes the a current tag for notes
	 */
	public void setNoteTag(IECaseComponent aCurrentCaseComponentForNotes, IXMLTag aCurrentTagForNotes) {
		currentCaseComponentForNotes = aCurrentCaseComponentForNotes;
		currentTagForNotes = aCurrentTagForNotes;
		setNoteContent();
	}

	/**
	 * Sets note content.
	 */
	protected void setNoteContent() {
		CRunNote lRunNote = (CRunNote)CDesktopComponents.vView().getComponent("runNote");
		if (lRunNote != null)
			lRunNote.setContent(getCurrentCaseComponentForNotes(), getCurrentTagForNotes());
	}

	/**
	 * Is called by several other components for notification.
	 * Calls method onAction, so see this method for possible aActions.
	 *
	 * @param aObserved the observed object, it should have interface CIObserved implemented
	 * @param aAction the action
	 * @param aStatus the status object, can be String or other class
	 */
	@Override
	public void observedNotify(CIObserved aObserved, String aAction, Object aStatus) {
		if (aObserved != null)
			onAction(((AbstractComponent) aObserved).getId(), aAction, aStatus);
	}

	/**
	 * On action. Is called by several components change the state of the player.
	 * Possible actions are:
	 * - showLocations: shows location buttons within run choice area
	 * - showLocation: shows location image within run view area
	 * - beamLocation: simulates click on location button and shows location image
	 * - showEmpack: shows empack action buttons within run choice area
	 * - showEmpackAction: shows content of corresponding case component within run view area
	 * - showLocationActions: shows popup window with possible location actions
	 * - showLocationAction: shows location action within run view area
	 * - endComponent: restores empack action button state when a component shown within the run view area is closed
	 * - showTasks: shows tasks within popup window
	 * - endTasks: restores tasks button state
	 * - showNote: shows note window within run choice area
	 * - endNote: restores note button state
	 * - onExternalEvent: used to notify conversations component when user clicks pause/play
	 * - close: saves all pending status for current user
	 *
	 * @param sender the sender
	 * @param action the action
	 * @param status the status
	 */
	@Override
	public void onAction(String sender, String action, Object status) {
		if (action.equals("onUpdateRunTags")) {
			CDesktopComponents.sSpring().getSUpdateHelper().checkUpdateRunTags(AppConstants.statusTypeRunGroup);
		}
		else if (action.equals("onExternalEvent")) {
			String[] lStatus = (String[]) status;
			String lSender = lStatus[0];
			String lReceiver = lStatus[1];
			String lEvent = lStatus[2];
			String lEventData = lStatus[3];
			if (sender.equals(CControl.runWnd)) {
				if (lEvent != null && lEventData != null) {
					if (lEvent.matches("onNavigatorUserAgent|onNavigatorCookieEnabled")) {
						if (lEvent.equals("onNavigatorUserAgent")) {
							navigatorUserAgent = lEventData;
						}
						else if (lEvent.equals("onNavigatorCookieEnabled")) {
							navigatorCookieEnabled = lEventData;
						}
						onClientInfo(clientInfoEvent);
					}
				}
			}
			//Check if internal state has to be updated as a result of the external event
			//NOTE This is used for google forms that set a state when they are loaded and submitted!
			if (lEventData != null) {
				String[] lEventDataValues = lEventData.split(",");
				String lValue = "";
				if ((lEventDataValues.length > 0) && (lEventDataValues[0] != null)) 
					lValue = lEventDataValues[0];
				setExternalState(lEvent, lValue);
			}
		}
	}

	/**
	 * Ends tablet app.
	 *
	 * @param aRunComponent the a run component
	 */
	public void endTabletApp(Object aRunComponent) {
		//override
	}

	/**
	 * Ends app.
	 *
	 * @param aRunComponent the a run component
	 */
	public void endApp(Object aRunComponent) {
		//override
	}

	/**
	 * Sets state key to value, as received from external provider. External key should have equally named corresponding key in a state component.
	 *
	 * @param aKey external key
	 * @param aValue external value
	 * 
	 */
	public void setExternalState(String aKey, String aValue) {
		if (sSpring == null)
			return;
		List<IECaseComponent> lCaseComponents = sSpring.getCaseComponentsByComponentCode("states");
		if (lCaseComponents == null)
			return;
		IXMLTag lTag = null;
		IECaseComponent lCaseComponent = null;
		for (IECaseComponent lCComponent : lCaseComponents) {
			List<IXMLTag> lStates = getCaseComponentRootTagChildTags(lCComponent);
			if (lStates != null) {
				for (IXMLTag lState : getCaseComponentRootTagChildTags(lCComponent)) {
					String lKey = lState.getChildValue("key");
					if (lKey.equals(aKey)) {
						lTag = lState;
						lCaseComponent = lCComponent;
					}
				}
			}
		}
		if (lTag != null) {
			sSpring.setRunTagStatus(lCaseComponent, lTag, AppConstants.statusKeyValue, aValue, true, AppConstants.statusTypeRunGroup, true, false);
		}
	}

	/**
	 * On timed action. Is used to send onActions timed. onActions are saved in a buffer until method checkOnTimedAction is
	 * called by CRunMainTimer. Timed onActions are sometimes necessary.
	 *
	 * @param sender the sender
	 * @param action the action
	 * @param status the status
	 */
	public void onTimedAction(String sender, String action, Object status) {
		if (pAddActions) {
			CRunAction lRunAction = new CRunAction();
			lRunAction.setSender(sender);
			lRunAction.setAction(action);
			lRunAction.setStatus(status);
			Events.postEvent("onTimedAction", this, lRunAction);
		}
	}

	/**
	 * On timed action.
	 *
	 * @param aEvent the a event
	 */
	public void onTimedAction(Event aEvent) {
		checkOnTimedAction((CRunAction)aEvent.getData());
	}

	/**
	 * Checks on timed action.
	 * 
	 * @param aRunAction the a run action
	 */
	protected void checkOnTimedAction(CRunAction aRunAction) {
		onAction(aRunAction.getSender(), aRunAction.getAction(), aRunAction.getStatus());
	}

	/**
	 * Status change. Is called by SSpring class if script actions have changed status. This status change should
	 * be reflected within the player. Status changes are saved in a buffer and handled by method checkStatusChange.
	 * Only status changes of statusKeyPresent, statusKeyAccessible, statusKeyOpened, statusKeySent and
	 * statusKeyStarted are buffered.
	 *
	 * @param aCaseComponent the a case component
	 * @param aDataTag the a data tag
	 * @param aStatusTag the a status tag
	 * @param aStatusKey the a status key
	 * @param aStatusValue the a status value
	 * @param aFromScript the a from script, whether this method is called from within script, so is a result of a script action
	 */
	public void statusChange(IECaseComponent aCaseComponent, IXMLTag aDataTag, IXMLTag aStatusTag, String aStatusKey, String aStatusValue, boolean aFromScript) {
		// override
	}

	/**
	 * Saves all pending status.
	 */
	public void saveAllStatus() {
		sSpring.saveRunCaseComponentsStatus();
	}

	/**
	 * Reset all status.
	 */
	public void resetAllStatus() {
		sSpring.resetRunCaseComponentsStatus();
	}

	/**
	 * Checks status change. This method reads the status changes buffer and updates the player accordingly,
	 * by updating the different components of the player. Such as navigator, empack, tasks or note button, or
	 * location or empack action buttons, or the content of the run view area.
	 * This method should be made more general in such a way that new Emergo components could be more easyly added.
	 */
	public void checkStatusChange() {
		List<CRunStatusChange> lCaseComponentTagList = sSpring.getExternalStatusChangesPerRgaIdAndDesktopId(this.rgaId, this.desktopId);
		while (lCaseComponentTagList.size() > 0) {
			pRunStatusChangeList.add(lCaseComponentTagList.get(0));
			lCaseComponentTagList.remove(0);
		}
	}

	/**
	 * Checks status change.
	 *
	 * @param aRunStatusChange the a run status change
	 */
	protected void checkStatusChange(CRunStatusChange aRunStatusChange) {
		IECaseComponent lCaseComponent = aRunStatusChange.getCaseComponent();
		IXMLTag lDataTag = aRunStatusChange.getDataTag();
		IXMLTag lStatusTag = aRunStatusChange.getStatusTag();
		String lStatusKey = aRunStatusChange.getStatusKey();
		String lStatusValue = aRunStatusChange.getStatusValue();
		String lCode = lCaseComponent.getEComponent().getCode();

		//NOTE Notify all run components that are coupled to lCaseComponent.
		//In principle multiple run components may be coupled to one case component.
		List<CRunComponent> lComponents = CDesktopComponents.vView().getRunComponentsByComponentCode(lCode);
		for (CRunComponent lComponent : lComponents) {
			if (lComponent.getCaseComponent() == lCaseComponent) {
				lComponent.handleBaseStatusChange(lCaseComponent, lDataTag, lStatusTag, lStatusKey, lStatusValue);
			}
		}
	}
	
	/**
	 * Checks update external tags. Is called regularly by CRunMainTimer.
	 * Checks if there are updates of other users meant for the current user and if so updatesthe run view area accordingly.
	 * For instance if another user sends an email to the current user, this email should become visible within the
	 * emessages component of the current user. Or if users share a certain component, contributions of a user should become
	 * visible for the other users.
	 */
	public void checkUpdateExternalTags() {
		// override
	}

	/**
	 * Updates button according to status key and value.
	 *
	 * @param aBtnName the a btn name
	 * @param aStatusKey the a status key
	 * @param aStatusValue the a status value
	 */
	public void updateBtn(String aBtnName, String aStatusKey, String aStatusValue) {
		updateBtn(aBtnName, aStatusKey, aStatusValue, null);
	}

	/**
	 * Updates button according to status key and value and tag.
	 *
	 * @param aBtnName the a btn name
	 * @param aStatusKey the a status key
	 * @param aStatusValue the a status value
	 * @param aCaseComponent the a case component
	 */
	public void updateBtn(String aBtnName, String aStatusKey, String aStatusValue, IECaseComponent aCaseComponent) {
		CRunHoverBtn lBtn = (CRunHoverBtn) CDesktopComponents.vView().getComponent(aBtnName);
		if (lBtn == null)
			return;
		boolean lBool = (aStatusValue.equals(AppConstants.statusValueTrue));
		if (aStatusKey.equals(AppConstants.statusKeyPresent)) {
			if (aCaseComponent != null) {
				if (sSpring.getCurrentRunComponentStatus(aCaseComponent, AppConstants.statusKeyAccessible, AppConstants.statusTypeRunGroup).equals(AppConstants.statusValueFalse)) {
					//NOTE setBtnEnabled shows button, so set enabled before visible is set
					lBtn.setBtnEnabled(false);
				}
			}
			lBtn.setBtnVisible(lBool);
		}
		else if (aStatusKey.equals(AppConstants.statusKeyAccessible)) {
			//NOTE setBtnEnabled shows button, so set enabled before visible is set
			lBtn.setBtnEnabled(lBool);
			if (aCaseComponent != null) {
				if (sSpring.getCurrentRunComponentStatus(aCaseComponent, AppConstants.statusKeyPresent, AppConstants.statusTypeRunGroup).equals(AppConstants.statusValueFalse)) {
					lBtn.setBtnVisible(false);
				}
			}
		}
		else if (aStatusKey.equals(AppConstants.statusKeySelected)) {
			lBtn.setBtnSelected(lBool);
		}
	}

	/**
	 * Updates button according to render status.
	 *
	 * @param aBtnName the a btn name
	 * @param aStatus the a status
	 */
	public void updateBtn(String aBtnName, String aStatus) {
		CRunHoverBtn lBtn = (CRunHoverBtn) CDesktopComponents.vView().getComponent(aBtnName);
		if (lBtn == null)
			return;
		lBtn.setStatus(aStatus);
	}

	/**
	 * Gets tablet app button id.
	 *
	 * @param aCaseComponent the case component
	 *
	 * @return id
	 */
	public String getTabletAppBtnId(IECaseComponent aCaseComponent) {
		return "tabletapp" + aCaseComponent.getCacId() + "Btn";
	}

	/**
	 * Show alert.
	 *
	 * @param aTitle the a title
	 * @param aAlert the a alert
	 */
	public void showAlert(String aTitle, String aAlert) {
		//override
	}

	/**
	 * Show alert.
	 *
	 * @param aCaseComponent the a case component
	 * @param aAlertTag the a alert tag
	 * @param aTitle the a title
	 */
	public void showAlert(IECaseComponent aCaseComponent, IXMLTag aAlertTag, String aTitle) {
		//override
	}

	/**
	 * Hide or shows location component for current location, if it is not present on all locations. For instance a notifications component.
	 *
	 * @param aCaseComponent the case component
	 *
	 * @return visible
	 */
	public boolean isLocationComponentVisible(IECaseComponent aCaseComponent) {
		//override
		return false;
	}


	/**
	 * Gets the case component component tag, either by component code or by case component name.
	 *
	 * @param aComponentCode the a component code of a case component
	 * @param aCaseComponentName the a case component name
	 *
	 * @return the case component component tag
	 */
	protected IXMLTag getCaseComponentComponentTag(String aComponentCode, String aCaseComponentName) {
		return getCaseComponentComponentTag(sSpring.getCaseComponent(aComponentCode, aCaseComponentName));
	}

	/**
	 * Gets the case component component tags component code.
	 *
	 * @param aComponentCode the a component code of a case component
	 *
	 * @return a list of the case component component tags
	 */
	protected List<IXMLTag> getCaseComponentComponentTags(String aComponentCode) {
		List<IXMLTag> lComponentTags = new ArrayList<IXMLTag>();
		List<IECaseComponent> lCaseComponents = sSpring.getCaseComponentsByComponentCode(aComponentCode);
		for (IECaseComponent lCaseComponent : lCaseComponents) {
			lComponentTags.add(getCaseComponentComponentTag(lCaseComponent));
		}
		return lComponentTags;
	}

	/**
	 * Gets the case component component tag.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return the case component component tag
	 */
	protected IXMLTag getCaseComponentComponentTag(IECaseComponent aCaseComponent) {
		if (aCaseComponent == null)
			return null;
		IXMLTag lRootTag = sSpring.getXmlDataPlusRunStatusTree(aCaseComponent, AppConstants.statusTypeRunGroup);
		if (lRootTag == null)
			return null;
		IXMLTag lComponentTag = lRootTag.getChild(AppConstants.componentElement);
		return lComponentTag;
	}

	/**
	 * Gets the case component button data, i.e. alternative images for active/inactive or alternative position.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return a map with data
	 */
	public Map<String,Object>getCaseComponentBtnData(IECaseComponent aCaseComponent) {
		Map<String,Object> lParams = new HashMap<String,Object>();
		IXMLTag lContentTag = getCaseComponentRootTag(aCaseComponent);
		if (lContentTag != null) {
			lParams.put("styleposition", getStylePosition(lContentTag));
			lParams.put("stylesize", getStyleSize(lContentTag));
			lParams.put("stylebackgroundactive", getStyleBackgroundActive(lContentTag));
			lParams.put("stylebackgroundinactive", getStyleBackgroundInactive(lContentTag));
		}
		return lParams;
	}
	
	/**
	 * Gets the absolute position for the tag as style string or empty string if not existing.
	 *
	 * @param aTag the a tag
	 *
	 * @return style string
	 */
	public String getStylePosition(IXMLTag aTag) {
		return getStylePosition(aTag, "position");
	}

	/**
	 * Gets the absolute position for the tag as style string or empty string if not existing.
	 *
	 * @param aTag the a tag
	 * @param aChildTagName the a child tag name
	 *
	 * @return style string
	 */
	public String getStylePosition(IXMLTag aTag, String aChildTagName) {
		String lStylePosition = "";
		String[] lPosition = aTag.getChildValue(aChildTagName).split(",");
		if (lPosition.length == 2) {
			String lLeft = getNumberStr(lPosition[0], "");
			String lTop = getNumberStr(lPosition[1], "");
			if (!lLeft.equals("") && !lTop.equals("")) {
				lStylePosition += "left:" + lLeft + "px;top:" + lTop + "px;";
			}
		}
		if (!lStylePosition.equals("")) {
			lStylePosition = "position:absolute;" + lStylePosition;
		}
		return lStylePosition;
	}

	/**
	 * Gets the size for the tag as style string or empty string if not existing.
	 *
	 * @param aTag the a tag
	 *
	 * @return style string
	 */
	public String getStyleSize(IXMLTag aTag) {
		return getStyleSize(aTag, "size");
	}

	/**
	 * Gets the size for the tag as style string or empty string if not existing.
	 *
	 * @param aTag the a tag
	 * @param aChildTagName the a child tag name
	 *
	 * @return style string
	 */
	public String getStyleSize(IXMLTag aTag, String aChildTagName) {
		String lStyleSize = "";
		String[] lSize = aTag.getChildValue(aChildTagName).split(",");
		if (lSize.length == 2) {
			String lWidth = getNumberStr(lSize[0], "");
			String lHeight = getNumberStr(lSize[1], "");
			if (!lWidth.equals("") && !lHeight.equals("")) {
				lStyleSize += "width:" + lWidth + "px;height:" + lHeight + "px;";
			}
		}
		return lStyleSize;
	}

	/**
	 * Gets the width for the tag as style string or empty string if not existing.
	 *
	 * @param aTag the a tag
	 *
	 * @return style string
	 */
	public String getStyleWidth(IXMLTag aTag) {
		return getStyleWidth(aTag, "size");
	}

	/**
	 * Gets the width for the tag as style string or empty string if not existing.
	 *
	 * @param aTag the a tag
	 * @param aChildTagName the a child tag name
	 *
	 * @return style string
	 */
	public String getStyleWidth(IXMLTag aTag, String aChildTagName) {
		String lStyleWidth = "";
		String[] lSize = aTag.getChildValue(aChildTagName).split(",");
		if (lSize.length == 2) {
			String lWidth = getNumberStr(lSize[0], "");
			if (!lWidth.equals("")) {
				lStyleWidth += "width:" + lWidth + "px;";
			}
		}
		return lStyleWidth;
	}

	/**
	 * Gets the active background for the tag as style string or empty string if not existing.
	 *
	 * @param aTag the a tag
	 *
	 * @return style string
	 */
	public String getStyleBackgroundActive(IXMLTag aTag) {
		String lSrc = getTagActiveImageSrc(aTag);
		if (!lSrc.equals("")) {
			return "background-image:url(" + lSrc + ");background-repeat:no-repeat;";
		}
		return "";
	}

	/**
	 * Gets the inactive background for the tag as style string or empty string if not existing.
	 *
	 * @param aTag the a tag
	 *
	 * @return style string
	 */
	public String getStyleBackgroundInactive(IXMLTag aTag) {
		String lSrc = getTagInactiveImageSrc(aTag);
		if (!lSrc.equals("")) {
			return "background-image:url(" + lSrc + ");background-repeat:no-repeat;";
		}
		return "";
	}

	/**
	 * Gets the logo background for the tag as style string or empty string if not existing.
	 *
	 * @param aTag the a tag
	 *
	 * @return style string
	 */
	public String getStyleBackgroundLogo(IXMLTag aTag) {
		String lSrc = getTagLogoImageSrc(aTag);
		if (!lSrc.equals("")) {
			return "background-image:url(" + lSrc + ");background-repeat:no-repeat;";
		}
		return "";
	}

	/**
	 * Get tag active image src.
	 *
	 * @param aTag the a tag containing the blob id
	 */
	public String getTagActiveImageSrc(IXMLTag aTag) {
		if (aTag == null)
			return "";
		return getTagImageSrc(aTag, "pictureactive");
	}

	/**
	 * Get tag inactive image src.
	 *
	 * @param aTag the a tag containing the blob id
	 */
	public String getTagInactiveImageSrc(IXMLTag aTag) {
		if (aTag == null)
			return "";
		return getTagImageSrc(aTag, "pictureinactive");
	}

	/**
	 * Get tag hover image src.
	 *
	 * @param aTag the a tag containing the blob id
	 */
	public String getTagHoverImageSrc(IXMLTag aTag) {
		if (aTag == null)
			return "";
		return getTagImageSrc(aTag, "picturehover");
	}

	/**
	 * Get tag selected image src.
	 *
	 * @param aTag the a tag containing the blob id
	 */
	public String getTagSelectedImageSrc(IXMLTag aTag) {
		if (aTag == null)
			return "";
		return getTagImageSrc(aTag, "pictureselected");
	}

	/**
	 * Get tag logo image src.
	 *
	 * @param aTag the a tag containing the blob id
	 */
	public String getTagLogoImageSrc(IXMLTag aTag) {
		if (aTag == null)
			return "";
		return getTagImageSrc(aTag, "picturelogo");
	}

	/**
	 * Get image src.
	 *
	 * @param aTag the a tag containing the blob ids
	 * @param aTagPictureName the tag picture name
	 */
	public String getTagImageSrc(IXMLTag aTag, String aTagPictureName) {
		String lBlobId = aTag.getChildValue(aTagPictureName);
		String lBlobtype = aTag.getChildAttribute(aTagPictureName, AppConstants.keyBlobtype);
		String lSrc = "";
		if (lBlobtype.equals(AppConstants.blobtypeIntUrl)) {
			// url
			lSrc = CDesktopComponents.sSpring().getSBlobHelper().getUrl(aTag);
		}
		else {
			// blob
			lSrc = CDesktopComponents.sSpring().getSBlobHelper().getImageSrc(lBlobId);
		}
		if (!lSrc.equals("")) {
			if (!CDesktopComponents.vView().isAbsoluteUrl(lSrc)) {
				String lEMRoot = CDesktopComponents.vView().getEmergoWebappsRoot();
				lSrc = CDesktopComponents.sSpring().getSBlobHelper().toUTFUrl (lEMRoot + lSrc);
			}
		}
		return lSrc;
	}

	/**
	 * Checks if string is integer. If not it returns aDefaultNumber.
	 *
	 * @param aNumber the a number
	 * @param aDefaultNumber the a default number
	 *
	 * @return a number
	 */
	public int getNumber(String aNumber, int aDefaultNumber) {
		int lNumber = aDefaultNumber;
		try {
			lNumber = Integer.parseInt(aNumber);
		} catch (NumberFormatException e) {
			lNumber = aDefaultNumber;
		}
		return lNumber;
	}

	/**
	 * Checks if string is integer. If not it returns aDefaultStr.
	 *
	 * @param aNumber the a number
	 * @param aDefaultStr the a default string
	 *
	 * @return a string
	 */
	public String getNumberStr(String aNumber, String aDefaultStr) {
		String lNumber = aDefaultStr;
		try {
			lNumber = "" + Integer.parseInt(aNumber);
		} catch (NumberFormatException e) {
			lNumber = aDefaultStr;
		}
		return lNumber;
	}

	/**
	 * Gets number as string from comma delimited string using aIndex.
	 *
	 * @param aString the a string
	 * @param aIndex the a index
	 *
	 * @return a string
	 */
	public String getNumberStrInStr(String aString, int aIndex) {
		if (aString == null) {
			return "";
		}
		String[] lStringArr = aString.split(",");
		if (aIndex < 0 || aIndex > (lStringArr.length - 1)) {
			return "";
		}
		return lStringArr[aIndex];
	}

	/**
	 * Gets the current case component status value, either by component code or by case component name.
	 *
	 * @param aComponentCode the a component code of a case component
	 * @param aCaseComponentName the a case component name
	 * @param aStatusKey the a status key
	 *
	 * @return the case component status
	 */
	protected String getCurrentCaseComponentStatusValue(String aComponentCode, String aCaseComponentName, String aStatusKey) {
		IXMLTag lComponentTag = getCaseComponentComponentTag(aComponentCode, aCaseComponentName);
		if (lComponentTag == null)
			return "";
		return lComponentTag.getCurrentStatusAttribute(aStatusKey);
	}

	/**
	 * Gets the current case component status value.
	 *
	 * @param aCaseComponent the a case component
	 * @param aStatusKey the a status key
	 *
	 * @return the case component status
	 */
	protected String getCurrentCaseComponentStatusValue(IECaseComponent aCaseComponent, String aStatusKey) {
		IXMLTag lComponentTag = getCaseComponentComponentTag(aCaseComponent);
		if (lComponentTag == null)
			return "";
		return lComponentTag.getCurrentStatusAttribute(aStatusKey);
	}

	/**
	 * Gets the case component render status, either by component code or by case component name.
	 * If the case component is visible and/or disabled.
	 *
	 * @param aComponentCode the a component code of a case component
	 * @param aCaseComponentName the a case component name
	 *
	 * @return the case component render status
	 */
	protected String getCaseComponentRenderStatus(String aComponentCode, String aCaseComponentName) {
		String lStatus = "empty";
		List<IXMLTag> lComponentTags = new ArrayList<IXMLTag>();
		if (aCaseComponentName.equals("")) {
			lComponentTags = getCaseComponentComponentTags(aComponentCode);
		}
		else {
			IXMLTag lComponentTag = getCaseComponentComponentTag(aComponentCode, aCaseComponentName);
			if (lComponentTag != null) {
				lComponentTags = new ArrayList<IXMLTag>();
				lComponentTags.add(lComponentTag);
			}
		}
		for (IXMLTag lComponentTag : lComponentTags) {
			lStatus = getCaseComponentRenderStatus(lComponentTag);
			if (lStatus.equals("active")) {
				//NOTE if more then one active, take first one
				break;
			}
		}
		return lStatus;
	}

	/**
	 * Gets the (first) active component if there are one or more case components with aComponentCode.
	 *
	 * @param aComponentCode the a component code of a case component
	 *
	 * @return the active case component
	 */
	protected IECaseComponent getActiveCaseComponent(String aComponentCode) {
		List<IECaseComponent> lCaseComponents = sSpring.getCaseComponentsByComponentCode(aComponentCode);
		for (IECaseComponent lCaseComponent : lCaseComponents) {
			if (getCaseComponentRenderStatus(getCaseComponentComponentTag(lCaseComponent)).equals("active")) {
				//NOTE if more then one active, take first one
				return lCaseComponent;
			}
		}
		return null;
	}

	/**
	 * Gets the case component render status.
	 * If the case component is visible and/or disabled.
	 *
	 * @param aComponentTage the a component tag
	 *
	 * @return the case component render status
	 */
	protected String getCaseComponentRenderStatus(IXMLTag aComponentTag) {
		String lStatus = "empty";
		boolean lPresent = !aComponentTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse);
		boolean lAccessible = !aComponentTag.getCurrentStatusAttribute(AppConstants.statusKeyAccessible).equals(AppConstants.statusValueFalse);
		boolean lOpened = aComponentTag.getCurrentStatusAttribute(AppConstants.statusKeyOpened).equals(AppConstants.statusValueTrue);
		if ((lPresent) && (lAccessible)) {
			lStatus = "active";
		}
		if ((lPresent) && (!lAccessible)) {
			lStatus = "inactive";
		}
		return lStatus;
	}

	/**
	 * Gets the preselected case component. Is used for preview option. If preview should start with a certain case component.
	 *
	 * @param aComponentCode the a component code
	 *
	 * @return the preselected case component or null of case component is not preselected
	 */
	protected IECaseComponent getPreselectedCaseComponent(String aComponentCode) {
		IECaseComponent lCaseComponent = preselectedCaseComponent;
		if (lCaseComponent != null) {
			if (lCaseComponent.getEComponent().getCode().equals(aComponentCode))
				return lCaseComponent;
		}
		return null;
	}

	/**
	 * Gets the case component root tag.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return the case component root tag
	 */
	public IXMLTag getCaseComponentRootTag(IECaseComponent aCaseComponent) {
		if (aCaseComponent == null)
			return null;
		IXMLTag lRoot = sSpring.getXmlDataPlusRunStatusTree(aCaseComponent, AppConstants.statusTypeRunGroup);
		if (lRoot == null)
			return null;
		IXMLTag lChild = lRoot.getChild(AppConstants.contentElement);
		return lChild;
	}

	/**
	 * Gets the case component root tag child tags. The child tags of the content tag.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return the case component tags
	 */
	protected List<IXMLTag> getCaseComponentRootTagChildTags(IECaseComponent aCaseComponent) {
		IXMLTag lContentRoot = getCaseComponentRootTag(aCaseComponent);
		if (lContentRoot == null)
			return null;
		return lContentRoot.getChildTags(AppConstants.defValueNode);
	}

	/**
	 * Gets the case component root tag child tags. The child tags of the content tag.
	 *
	 * @param aCaseComponent the a case component
	 * @param aTagName the a tag name
	 *
	 * @return the case component tags
	 */
	protected List<IXMLTag> getCaseComponentRootTagChildTags(IECaseComponent aCaseComponent, String aTagName) {
		IXMLTag lContentRoot = getCaseComponentRootTag(aCaseComponent);
		if (lContentRoot == null)
			return null;
		List<IXMLTag> lTags = lContentRoot.getChildTags(AppConstants.defValueNode);
		List<IXMLTag> lNamedTags = new ArrayList<IXMLTag>();
		for (IXMLTag lTag : lTags) {
			if (lTag.getName().equals(aTagName)) {
				lNamedTags.add(lTag);
			}
		}
		return lNamedTags;
	}

	/**
	 * Gets the case component root tag child tags. The child tags of the content tag.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return the case component tags
	 */
	protected List<IXMLTag> getCaseComponentRootTagNestedChildTags(IECaseComponent aCaseComponent) {
		IXMLTag lContentRoot = getCaseComponentRootTag(aCaseComponent);
		if (lContentRoot == null)
			return null;
		List<IXMLTag> lTags = new ArrayList<IXMLTag>();
		getNestedNodeChilds(lContentRoot.getChildTags(AppConstants.defValueNode), lTags);
		return lTags;
	}

	/**
	 * Gets nested node childs.
	 * Loops through list aTags and makes a new list with the same tags.
	 *
	 * @param aTags the tag list
	 * @param aReturnTags used to return the new tag list
	 */
	public void getNestedNodeChilds(List<IXMLTag> aTags, List<IXMLTag> aReturnTags) {
		if (aTags == null)
			return;
		for (IXMLTag lTag : aTags) {
			aReturnTags.add(lTag);
			getNestedNodeChilds(lTag.getChildTags(AppConstants.defValueNode), aReturnTags);
		}
	}

	/**
	 * Gets the case component root tag nested child tags. The nested child tags of the content tag.
	 *
	 * @param aCaseComponent the a case component
	 * @param aTagName the a tag name
	 *
	 * @return the case component nested tags
	 */
	protected List<IXMLTag> getCaseComponentRootTagNestedChildTags(IECaseComponent aCaseComponent, String aTagName) {
		IXMLTag lContentRoot = getCaseComponentRootTag(aCaseComponent);
		if (lContentRoot == null)
			return null;
		List<IXMLTag> lTags = new ArrayList<IXMLTag>();
		getNestedNodeChilds(lContentRoot.getChildTags(AppConstants.defValueNode), lTags);
		List<IXMLTag> lNamedTags = new ArrayList<IXMLTag>();
		for (IXMLTag lTag : lTags) {
			if (lTag.getName().equals(aTagName)) {
				lNamedTags.add(lTag);
			}
		}
		return lNamedTags;
	}

	/**
	 * Gets the mediaplayer stop action. The javascript to stop mediaplayer from playing.
	 *
	 * @return the mediaplayer stop action
	 */
	public String getMediaplayerStopAction() {
		return "onclick:if (document.mediaplayer != null) document.mediaplayer.controls.pause();";
	}

	/**
	 * Sends notification to client.
	 *
	 * @param data the data
	 */
	public void emergoEventToClient(String receiver, String id, String event, String data) {
		Clients.evalJavaScript("emergoEventFromServer('" + receiver + "','" + id + "','" + event + "','" + data + "');");
	}

	/**
	 * Fade out a component.
	 *
	 * @param aComponentId the component id
	 */
	public void fadeOutComponent(String aComponentId) {
		Clients.evalJavaScript("fadeOutZulElement('" + aComponentId + "');");
	}

	/**
	 * Fade in a component.
	 *
	 * @param aComponentId the component id
	 */
	public void fadeInComponent(String aComponentId) {
		Clients.evalJavaScript("fadeInZulElement('" + aComponentId + "');");
	}

	/**
	 * Fade out a component.
	 *
	 * @param aComponentId the component id
	 * @param aTimeMsecs the time in msecs
	 */
	public void fadeOutComponent(String aComponentId, long aTimeMsecs) {
		Clients.evalJavaScript("fadeOutZulElement('" + aComponentId + "', " + aTimeMsecs + ");");
	}

	/**
	 * Fade in a component.
	 *
	 * @param aComponentId the component id
	 * @param aTimeMsecs the time in msecs
	 */
	public void fadeInComponent(String aComponentId, long aTimeMsecs) {
		Clients.evalJavaScript("fadeInZulElement('" + aComponentId + "', " + aTimeMsecs + ");");
	}

	/**
	 * Fade out a location.
	 *
	 * @param aLocationId the location id
	 */
	public void fadeOutLocation(String aLocationId) {
		Clients.evalJavaScript("fadeOutLocation('" + CControl.runWnd + "', '" + aLocationId + "');");
	}

	/**
	 * Fade in a location.
	 *
	 * @param aLocationId the location id
	 */
	public void fadeInLocation(String aLocationId) {
		Clients.evalJavaScript("fadeInLocation('" + CControl.runWnd + "', '" + aLocationId + "');");
	}

	/**
	 * Highlights a component. A visible component is highlighted.
	 *
	 * @param aComponentId the component id
	 */
	public void highlightComponent(String aComponentId) {
		Component lComponent = (Component)CDesktopComponents.vView().getComponent(aComponentId);
		if (lComponent != null) {
			Clients.evalJavaScript("highlightZulElement('" + lComponent.getId() + "');");
		}
	}

	/**
	 * Highlights a component inverse. A not visible component is highlighted.
	 *
	 * @param aComponentId the component id
	 */
	public void highlightComponentInverse(String aComponentId) {
		Component lComponent = (Component)CDesktopComponents.vView().getComponent(aComponentId);
		if (lComponent != null) {
			Clients.evalJavaScript("highlightZulElementInverse('" + lComponent.getId() + "');");
		}
	}

	/**
	 * Draws attention to a visible component or stop drawwing attention.
	 *
	 * @param aComponentId the component id
	 * @param aDrawAttention the draw attention
	 */
	public void drawAttentionToComponent(String aComponentId, boolean aDrawAttention) {
		HtmlBasedComponent lComponent = (HtmlBasedComponent)CDesktopComponents.vView().getComponent(aComponentId);
		if (lComponent != null) {
			String lZclass = lComponent.getZclass();
			if (lZclass == null) {
				lZclass = "";
			}
			String drawAttentionAnimationClass = "get-attention";
			if (aDrawAttention) {
				if (!lZclass.contains(drawAttentionAnimationClass)) {
					lZclass += " " + drawAttentionAnimationClass;
					lComponent.setZclass(lZclass);
				}
			}
			else {
				if (lZclass.contains(drawAttentionAnimationClass)) {
					lZclass = lZclass.replace(drawAttentionAnimationClass, "");
					lComponent.setZclass(lZclass);
				}
			}
		}
	}

 }
