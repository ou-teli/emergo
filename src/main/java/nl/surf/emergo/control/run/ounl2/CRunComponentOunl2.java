/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl2;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.CRunComponent;

/**
 * The Class CRunComponentOunl2. Needed to access CRunConversationsOunl2 class in a general way.
 */
public class CRunComponentOunl2 extends CRunComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2990803759259862519L;


	/**
	 * Gets feedback conversations component.
	 *
	 * @return run component
	 */
	protected CRunConversationsOunl2 getFeedbackConversationsComponent() {
		if (runWnd != null) {
			String lConversationsId = "runConversations" + conversationsFeedbackIdSuffix;
			String lConversationInteractionId = "runConversationInteraction" + conversationsFeedbackIdSuffix;
			CRunConversationsOunl2 lRunConversations = (CRunConversationsOunl2)CDesktopComponents.vView().getComponent(lConversationsId);
			if (lRunConversations == null) {
				lRunConversations = getNewRunFeedbackConversations(lConversationsId, lConversationInteractionId);
				lRunConversations.setVisible(false);
				runWnd.appendChild(lRunConversations);
			}
			return lRunConversations;
		}
		return null;
	}
	
	/**
	 * Get new run feedback conversations.
	 *
	 * @param aConversationsId the a conversations id
	 * @param aConversationInteractionId the a conversation interaction id
	 * 
	 * @return the new run conversations
	 */
	protected CRunConversationsOunl2 getNewRunFeedbackConversations(String aConversationsId, String aConversationInteractionId) {
		return new CRunConversationsOunl2(aConversationsId, aConversationInteractionId, this);
	}

	/**
	 * Get new run feedback conversation interaction.
	 *
	 * @param aConversationInteractionId the a conversation interaction id
	 * @param aConversations the a conversations
	 * @param aConversationsId the a conversations id
	 * 
	 * @return run component
	 */
	protected CRunConversationInteractionOunl2 getNewRunFeedbackConversationInteraction(String aConversationInteractionId, CRunComponent aConversations, String aConversationsId) {
		return  new CRunConversationInteractionOunl2(aConversationInteractionId, aConversations, aConversationsId, notifyRunWnd);
	}
	
}
