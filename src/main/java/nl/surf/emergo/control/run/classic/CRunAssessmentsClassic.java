/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Html;
import org.zkoss.zul.Label;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.Window;

import nl.surf.emergo.business.IXMLAttributeValueTime;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.business.impl.XMLAttributeValueTime;
import nl.surf.emergo.control.CCaseHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CIObserved;
import nl.surf.emergo.control.def.CDefHtml;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;

/**
 * The Class CRunAssessments is used to show assessments within the run view area of the
 * Emergo player.
 */
public class CRunAssessmentsClassic extends CRunComponentClassic {
	
	private static final long serialVersionUID = -5863947238022126414L;

	/** The current assessment tag. */
	protected IXMLTag currentassessmenttag = null;

	/** The refitem, the selected refitem xml tag within the assessments case component data. */
	protected IXMLTag refitem = null;
	
	/** The itemscarid, the case role id for the items case component. */
	protected String itemscarid = "0";
	
	/** The itemscacid, the case component id of the items case component. */
	protected String itemscacid = "0";
	
	/** The item, the selected item xml tag within the items case component data. */
	protected IXMLTag item = null;
	
	/** The radio button which is checked. */
	protected CRunItemRadioClassic radioChecked = null;
	
	/** The renderAssLock, used to prevent rendering assessments tree if it is already rendered. */
	protected boolean renderAssLock = false;
	
	/** The directFeedback, is feedback is shown directly, if possible. */
	protected boolean directFeedback = false;

	/** The showFeedback, is feedback shown. */
	protected boolean showFeedback = false;

	protected boolean isAssessmentEditable(IXMLTag aAssessmentTag) {
		boolean lEditable = false;
		// statusKeyStarted and statusKeyFinished are not set to false, only to true, so look at times to determine if someone can edit
		String lStartedTime = "" + aAssessmentTag.getCurrentStatusAttributeTime(AppConstants.statusKeyStarted);
		String lFinishedTime = "" + aAssessmentTag.getCurrentStatusAttributeTime(AppConstants.statusKeyFinished);
		if ((lStartedTime != null) && (!lStartedTime.equals(""))) {
			if ((lFinishedTime == null) || (lFinishedTime.equals("")))
				lEditable = true;
			else
				lEditable = Double.parseDouble(lStartedTime) > Double.parseDouble(lFinishedTime);
		}
		return lEditable;
	}

	/**
	 * Instantiates a new c run assessments.
	 * 
	 * @param aId the a id
	 * @param aCaseComponent the assessments case component
	 */
	public CRunAssessmentsClassic(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
		directFeedback = CDesktopComponents.sSpring().getCurrentRunComponentStatus(caseComponent, "directfeedback", getRunStatusType()).equals(AppConstants.statusValueTrue);
	}

	/**
	 * Creates title area, content area to show assessments tree, item area to show
	 * selected item and buttons area with close button.
	 * Renders assessments tree and if there are autostart assessments, starts them.
	 * Non autostart assessments get a start or an end assessment button within the tree.
	 */
	@Override
	protected void createComponents() {
		CRunVboxClassic lVbox = new CRunVboxClassic();
		appendChild(lVbox);

		createTitleArea(lVbox);
		CRunHboxClassic lHbox = new CRunHboxClassic();
		lVbox.appendChild(lHbox);
//		initially only render assessment tags 
//		if one starts assessment by clicking on start button render assessment items
//		check if number of wanted items is smaller then available items
//		if so randomize items to show
//		if assessment is ended by clicking on ready button, determine and save assessment score, and freeze items (using accessible?)
//		if one starts assessment again by clicking on start button again render new assessment items
		createContentArea(lHbox);
		CRunAreaClassic lItemArea = createItemArea(lHbox);
		lItemArea.setId(getId()+"ItemArea");
		lItemArea.setZclass(className+"_itemarea");
		createButtonsArea(lVbox);
		
		CRunTreeClassic lRunTree = (CRunTreeClassic)getRunContentComponent();
		List<IXMLTag> lAutostartAssessments = new ArrayList<IXMLTag>();
		if ((lRunTree != null) && (lRunTree.getTreechildren() != null)) {
			List<Component> lTreeitems = lRunTree.getTreechildren().getChildren();
			for (Component lTreeitem : lTreeitems) {
				IXMLTag lAssessmentTag = lRunTree.getContentItemTag(lTreeitem);
				if (lAssessmentTag != null) {
					boolean lStarted = (CDesktopComponents.sSpring().getCurrentTagStatus(lAssessmentTag,AppConstants.statusKeyStarted).equals(AppConstants.statusValueTrue));
					boolean lAutostart = (CDesktopComponents.sSpring().getCurrentRunTagStatus(caseComponent,lAssessmentTag,"autostart", getRunStatusType()).equals(AppConstants.statusValueTrue));
					if ((!lStarted) && (lAutostart)) {
						lAutostartAssessments.add(lAssessmentTag);
					}
				}
			}
		}
		for (IXMLTag lAssessmentTag : lAutostartAssessments) {
			startAssessment(lAssessmentTag);
		}
	}

	/**
	 * Creates new content component, the assessments tree.
	 * 
	 * @return the component
	 */
	@Override
	protected Component newContentComponent() {
		// clear last selected item
		CDesktopComponents.sSpring().setRunComponentStatus(caseComponent,
				AppConstants.statusKeySelectedTagId, "", false, AppConstants.statusTypeRunGroup, true);
				
		return (new CRunAssessmentsTreeClassic("runAssessmentsTree", caseComponent, this));
	}

	/**
	 * Creates buttons area and adds close button.
	 * 
	 * @param aParent the a parent
	 * 
	 * @return the c run hbox
	 */
	@Override
	protected CRunHboxClassic createButtonsArea(Component aParent) {
		CRunHboxClassic lButtonsHbox = super.createButtonsArea(aParent);
		createCloseButton(lButtonsHbox);
		return lButtonsHbox;
	}

	/**
	 * Does contentitem action, clicking on an assessment item.
	 * Shows item within item area.
	 * 
	 * @param aContentItem the a contentitem, the assessment item clicked
	 */
	@Override
	public void doContentItemAction(Component aContentItem) {
		Treeitem lTreeitem = (Treeitem)aContentItem;
		IXMLTag tag = (IXMLTag)lTreeitem.getAttribute("item");
		CRunAreaClassic lItemArea = (CRunAreaClassic)CDesktopComponents.vView().getComponent(getId()+"ItemArea");
		if (tag.getName().equals("assessment")) {
			if (lItemArea != null) {
				showAssessmentFeedback(lItemArea,tag);
			}
			return;
		}
		String carrefid = (String)lTreeitem.getAttribute("carrefid");
		String cacrefid = (String)lTreeitem.getAttribute("cacrefid");
		IXMLTag tagref = (IXMLTag)lTreeitem.getAttribute("tagref");
		if ((tag == null) || (tagref == null) || (!tag.getName().equals("refitem")))
			return;
		refitem = tag;
		itemscarid = carrefid;
		itemscacid = cacrefid;
		item = tagref;
		if (lItemArea != null)
			createItem(lItemArea,tag,tagref);
	}

	/**
	 * Shows item within item area. First removes previous item.
	 * If assessment is started but not yet finished, the item choice can be changed.
	 * If assessment is ended last choice is shown, but can not be changed. That is also
	 * the case if the item is not accessible anymore.
	 * Item contains item text possibly having references and item options possibly
	 * having references.
	 * If a choice is already made and there should be direct feedback the feedback is shown.
	 * 
	 * @param aParent the ZK parent
	 * @param aRefItem the a ref item within the assessments case component
	 * @param aItem the a item within the items case component
	 * 
	 * @return the c run vbox
	 */
	protected CRunVboxClassic createItem(Component aParent,IXMLTag aRefItem,IXMLTag aItem) {
		radioChecked = null;
		removeItem(aParent);
		IXMLTag lParentTag = aRefItem.getParentTag(); 
		currentassessmenttag = lParentTag;
		boolean lReadOnly = !isAssessmentEditable(lParentTag);
		boolean lBAccessible = (!CDesktopComponents.sSpring().getCurrentRunTagStatus(caseComponent, aRefItem.getAttribute(AppConstants.keyId), AppConstants.statusKeyAccessible, getRunStatusType()).equals(AppConstants.statusValueFalse));
		if (!lBAccessible)
			lReadOnly = true;
		CRunAreaClassic lArea = new CRunAreaClassic();
		aParent.appendChild(lArea);
		lArea.setZclass(className+"_item");
		CRunVboxClassic lVbox = new CRunVboxClassic();
		lArea.appendChild(lVbox);
		CRunVboxClassic lVbox3 = new CRunVboxClassic();
		createRichtext(lVbox3, aItem);
//		append pieces
		createPieces(lVbox3,aItem);
		createBreak(lVbox3);
		lVbox3.setZclass(this.getClassName() + "_item_questiontext");
		lVbox.appendChild(lVbox3);
		String lAnswer = CDesktopComponents.sSpring().getCurrentTagStatus(aRefItem,AppConstants.statusKeyAnswer);
		for (IXMLTag lAltTag : aItem.getChilds("alternative")) {
			CRunHboxClassic lHbox = new CRunHboxClassic();
			lVbox.appendChild(lHbox);
			createRadio(lHbox,lAltTag,lReadOnly,lAnswer);
			CRunVboxClassic lVbox2 = new CRunVboxClassic();
			lVbox2.setZclass(this.getClassName() + "_item_alternativetext");
			lHbox.appendChild(lVbox2);
			createRichtext(lVbox2, lAltTag);
//			append pieces
			createPieces(lVbox2,lAltTag);
		}
		CRunHboxClassic lHbox = null;
//		parent of refitem is assessment
		showFeedback = CDesktopComponents.sSpring().getCurrentRunTagStatus(caseComponent,aRefItem.getParentTag(),"showfeedback", getRunStatusType()).equals(AppConstants.statusValueTrue);
		if (showFeedback) {
			lHbox = createFeedback(lVbox);
			if ((radioChecked != null) && (lHbox != null))
				showFeedback(lHbox,refitem,item);
		}
		return lVbox;
	}

	/**
	 * Removes item from item area.
	 * 
	 * @param aParent the a parent
	 */
	protected void removeItem(Component aParent) {
		if (aParent.getChildren() != null)
			aParent.getChildren().clear();
	}

	/**
	 * Shows richtext within item area.
	 * 
	 * @param aParent the a parent
	 * @param aItem the a item containing the rich text within the items case component
	 */
	protected void createRichtext(Component aParent,IXMLTag aItem) {
		Html lHtml = new CDefHtml(CDesktopComponents.sSpring().unescapeXML(aItem.getChildValue("richtext")));
		aParent.appendChild(lHtml);
		lHtml.setZclass(this.getClassName() + "_richtext");
	}

	/**
	 * Shows break within item area.
	 * 
	 * @param aParent the a parent
	 */
	protected void createBreak(Component aParent) {
		Html lHtml = new CDefHtml("<br/>");
		lHtml.setZclass(getClassName()+"_item_html");
		aParent.appendChild(lHtml);
	}

	/**
	 * Shows radio button within item area. Checks radio button if corresponds to
	 * given answer.
	 * 
	 * @param aParent the a parent
	 * @param aItem the alternative within the items case component
	 * @param aReadOnly the a read only state
	 * @param aAnswer the a answer
	 */
	protected void createRadio(Component aParent,IXMLTag aItem,boolean aReadOnly,String aAnswer) {
		CRunItemRadioClassic lRadio = new CRunItemRadioClassic();
		aParent.appendChild(lRadio);
		String lAltTagId = aItem.getAttribute(AppConstants.keyId);
		lRadio.setAttribute("answer",lAltTagId);
		if (lAltTagId.equals(aAnswer)) {
			lRadio.setChecked(true);
			radioChecked = lRadio;
		}
		lRadio.setDisabled(aReadOnly);
		lRadio.setZclass(getClassName()+"_item_radio");
	}

	/**
	 * Shows pieces within item area. Pieces can be entered within the items
	 * case component, but can also be references to pieces entered within a 
	 * references case component.
	 * 
	 * @param aParent the a parent
	 * @param aItem the item containing the pieces within the items case component
	 */
	protected void createPieces(Component aParent,IXMLTag aItem) {
		for (IXMLTag lPiece : aItem.getChilds("piece")) {
			Label lLabel = new CRunBlobLabelClassic(lPiece);
			aParent.appendChild(lLabel);
			lLabel.setZclass(getClassName() + "_item_reference");
		}
		CCaseHelper cCaseHelper = new CCaseHelper();
		for (IXMLTag lRefPiece : aItem.getChilds("refpiece")) {
			String lReftype = lRefPiece.getChild("ref").getDefTag().getAttribute(AppConstants.defKeyReftype);
			List<IXMLTag> lRefTags = cCaseHelper.getRefTags(lReftype,""+AppConstants.statusKeySelectedIndex,CDesktopComponents.sSpring().getSCaseRoleHelper().getCaseRole(Integer.parseInt(itemscarid)),CDesktopComponents.sSpring().getCaseComponent(Integer.parseInt(itemscacid)),lRefPiece);
//			only one ref
			if (lRefTags.size() == 1) {
				Label lLabel = new CRunBlobLabelClassic(lRefTags.get(0),CDesktopComponents.sSpring().unescapeXML(lRefPiece.getChildValue("name")));
				aParent.appendChild(lLabel);
				lLabel.setZclass(getClassName() + "_item_reference");
			}
		}
	}
	
	/**
	 * Creates feedback area within item area.
	 * 
	 * @param aParent the a parent
	 * 
	 * @return the c run hbox
	 */
	protected CRunHboxClassic createFeedback(Component aParent) {
		CRunHboxClassic lHbox = new CRunHboxClassic();
		aParent.appendChild(lHbox);
		lHbox.setZclass(getClassName() + "_item_feedbackbox");
		lHbox.setId(getId()+"FeedbackBox");
		return lHbox;
	}

	/**
	 * Gets the feedback condition tag applicable for the given aAltId.
	 *
	 * @param aRefItem the a ref item within the assessments case component
	 * @param aItem the a item within the items case component
	 *
	 * @return the feedback condition
	 */
	protected IXMLTag getFeedbackCondition(IXMLTag aRefItem, IXMLTag aItem) {
		List<IXMLTag> lPresentFCTags = getPresentFeedbackconditionTags(CDesktopComponents.sSpring().getRunGroupAccount(), aItem);
		//NOTE!!! lGeneratedAltTags and lGeneratedFCTags are lists of alternatives and feedback conditions tags generated out of refitem status
		//An item is part of an Items component and can be used by multiple assessments, therefore answer and triggered feedback condition id are stored in refitem!
		//To be able to check the condition, that is defined on item alternatives and/or feedback conditions,
		//the history of answers and feedback condition ids is converted to alternative and feedback condition tags with the right status attribute set, namely opened.
		//These tags are then used to check the condition.
		Hashtable<String, IXMLTag> lGeneratedAltTags = getItemChildTagsFromRefItem(aRefItem, aItem, "alternative");
		Hashtable<String, IXMLTag> lGeneratedFCTags = getFeedbackConditionTagsFromRefItem(aRefItem, aItem, "feedbackcondition");
		for (IXMLTag lFCTag : lPresentFCTags) {
			if (CDesktopComponents.sSpring().getSScriptHelper().evaluateAssessmentConditionTag(lFCTag, lGeneratedAltTags, lGeneratedFCTags, AppConstants.statusTypeRunGroup))
				return lFCTag;
		}
		return null;
	}

	/**
	 * Gets a hashtable with per original item child tag id, a generated child tag with a history of status opened set.
	 * Child tags are generated using the history of the answer stored in the refitem status.
	 * For mkea (multiple choice) questions the answer is equal to the chose alternative tag id, e.g. '8'.
	 * For mkma (multiple answer) questions, the answer may be equal to a combination of alternative tag ids, e.g., '2_and_4_and_7'
	 * 
	 * @param aRefItem the a ref item within the assessments case component
	 * @param aItem the a item within the items case component
	 * @param aChildName the a child name
	 *
	 * @return hashtable
	 */
	protected Hashtable<String, IXMLTag> getItemChildTagsFromRefItem(IXMLTag aRefItem, IXMLTag aItem, String aChildName) {
		Hashtable<String, IXMLTag> hTags = new Hashtable<String, IXMLTag>(); 

		//get item child tags
		List<IXMLTag> lItemChildTags = aItem.getChilds(aChildName);
		if (lItemChildTags.size() == 0) {
			return hTags;
		}

		//get refitem answers
		List<IXMLAttributeValueTime> lValueTimes= aRefItem.getStatusAttribute(AppConstants.statusKeyAnswer); 
		if (lValueTimes == null || lValueTimes.size() == 0) {
			return hTags;
		}
		Hashtable<String, List<IXMLAttributeValueTime>> hTagValueTimes = new Hashtable<String, List<IXMLAttributeValueTime>>();
		//init hashtable
		for (IXMLTag lItemChildTag : lItemChildTags) {
			hTagValueTimes.put(lItemChildTag.getAttribute(AppConstants.keyId), new ArrayList<IXMLAttributeValueTime>());
		}
		String[] lIds = null;
		String[] lPreviousIds = new String[0];
		for (IXMLAttributeValueTime lValueTime : lValueTimes) {
			//NOTE value can contain multiple alternative ids in case of multi answer question, so split
			//NOTE value for instance is 'a_and_b', where '_and_' is AppConstants.statusValueSeparator 
			lIds = lValueTime.getValue().split(AppConstants.statusValueSeparator);
			//NOTE check if ids are in previous ids. If not the user has checked an alternative so add value true. 
			for (int i=0;i<lIds.length;i++) {
				//id must be of existing alternative
				if (hTagValueTimes.containsKey(lIds[i])) {
					boolean lFound = false;
					for (int j=0;j<lPreviousIds.length;j++) {
						if (lPreviousIds[j].equals(lIds[i])) {
							lFound = true;
							break;
						}
					}
					if (!lFound) {
						hTagValueTimes.get(lIds[i]).add(new XMLAttributeValueTime(AppConstants.statusValueTrue, lValueTime.getTime()));
					}
				}
			}
			//NOTE check if previous ids are in ids. If not the user has unchecked an alternative so add value false.
			for (int i=0;i<lPreviousIds.length;i++) {
				//id must be of existing alternative
				if (hTagValueTimes.containsKey(lPreviousIds[i])) {
					boolean lFound = false;
					for (int j=0;j<lIds.length;j++) {
						if (lIds[j].equals(lPreviousIds[i])) {
							lFound = true;
							break;
						}
					}
					if (!lFound) {
						hTagValueTimes.get(lPreviousIds[i]).add(new XMLAttributeValueTime(AppConstants.statusValueFalse, lValueTime.getTime()));
					}
				}
			}
			lPreviousIds = lIds;
		}
		
		for (Enumeration<String> lKeys = hTagValueTimes.keys(); lKeys.hasMoreElements();) {
			String lKey = lKeys.nextElement();
			if (hTagValueTimes.get(lKey).size() == 0) {
				// opened is default false within xml definition
				hTagValueTimes.get(lKey).add(new XMLAttributeValueTime(AppConstants.statusValueFalse, -1));
			}
			//NOTE create data tag with aChildName
			IXMLTag lDataTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag(aChildName, "");
			
			IXMLTag lStatusTag = CDesktopComponents.sSpring().newRunNodeTag(aChildName, "", null, null);
			if (lStatusTag != null) {
				lDataTag.addStatusTag(lStatusTag);
				IXMLTag lStatusStatusTag = lStatusTag.getChild(AppConstants.statusElement);
				if (lStatusStatusTag != null) {
					lStatusStatusTag.setAttributeAsList(AppConstants.statusKeyOpened, hTagValueTimes.get(lKey));
				}
			}
			hTags.put(lKey, lDataTag);
		}
		
		return hTags;
	}

	/**
	 * Gets a hashtable with per original item feedback condition child tag id, a generated feedback condition tag with a history of status opened set.
	 * Feedback condition tags are generated using the history of the fired feedback condition id stored in the refitem status.
	 *
	 * @param aRefItem the a ref item within the assessments case component
	 * @param aItem the a item within the items case component
	 * @param aChildName the a child name
	 *
	 * @return hashtable
	 */
	protected Hashtable<String, IXMLTag> getFeedbackConditionTagsFromRefItem(IXMLTag aRefItem, IXMLTag aItem, String aChildName) {
		Hashtable<String, IXMLTag> hTags = new Hashtable<String, IXMLTag>(); 

		//get item feedback conditions
		List<IXMLTag> lItemChildTags = aItem.getChilds(aChildName);
		if (lItemChildTags.size() == 0) {
			return hTags;
		}

		//get refitem feedback condition ids
		List<IXMLAttributeValueTime> lValueTimes= aRefItem.getStatusAttribute(AppConstants.statusKeyFeedbackConditionId); 
		if (lValueTimes == null || lValueTimes.size() == 0) {
			return hTags;
		}
		Hashtable<String, List<IXMLAttributeValueTime>> hTagValueTimes = new Hashtable<String, List<IXMLAttributeValueTime>>();
		//init hashtable
		for (IXMLTag lItemChildTag : lItemChildTags) {
			hTagValueTimes.put(lItemChildTag.getAttribute(AppConstants.keyId), new ArrayList<IXMLAttributeValueTime>());
		}
		String lPreviousId = "";
		String lId = "";
		for (IXMLAttributeValueTime lValueTime : lValueTimes) {
			lId = lValueTime.getValue();
			if (hTagValueTimes.containsKey(lId)) {
				hTagValueTimes.get(lId).add(new XMLAttributeValueTime(AppConstants.statusValueTrue, lValueTime.getTime()));
			}
			if (!lPreviousId.equals("") && hTagValueTimes.containsKey(lPreviousId)) {
				hTagValueTimes.get(lPreviousId).add(new XMLAttributeValueTime(AppConstants.statusValueFalse, lValueTime.getTime()));
			}
			lPreviousId = lId;
		}
		
		for (Enumeration<String> lKeys = hTagValueTimes.keys(); lKeys.hasMoreElements();) {
			String lKey = lKeys.nextElement();
			if (hTagValueTimes.get(lKey).size() == 0) {
				// opened is default false within xml definition
				hTagValueTimes.get(lKey).add(new XMLAttributeValueTime(AppConstants.statusValueFalse, -1));
			}
			//NOTE create data tag with aChildName
			IXMLTag lDataTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag(aChildName, "");
			
			IXMLTag lStatusTag = CDesktopComponents.sSpring().newRunNodeTag(aChildName, "", null, null);
			if (lStatusTag != null) {
				lDataTag.addStatusTag(lStatusTag);
				IXMLTag lStatusStatusTag = lStatusTag.getChild(AppConstants.statusElement);
				if (lStatusStatusTag != null) {
					lStatusStatusTag.setAttributeAsList(AppConstants.statusKeyOpened, hTagValueTimes.get(lKey));
				}
			}
			hTags.put(lKey, lDataTag);
		}
		
		return hTags;
	}

	/**
	 * Shows feedback possibly having references.
	 * 
	 * @param aParent the a parent
	 * @param aRefItem the a ref item
	 * @param aItem the a item within the items case component
	 * 
	 * @return the c run v box
	 */
	protected CRunVboxClassic showFeedback(Component aParent,IXMLTag aRefItem,IXMLTag aItem) {
		aParent.getChildren().clear();
		CRunVboxClassic lVbox = new CRunVboxClassic();
		boolean lEditable = isAssessmentEditable(currentassessmenttag);
		if (!lEditable || (lEditable && directFeedback)) {
//			String lCorrect = CDesktopComponents.sSpring().getCurrentTagStatus(aRefItem, AppConstants.statusKeyCorrect);
			String lCorrect = "";
			String lFeedbackConditionId = CDesktopComponents.sSpring().getCurrentTagStatus(aRefItem,AppConstants.statusKeyFeedbackConditionId);
			String lFeedback = "";
			IXMLTag lFeedbackTag = null;
			for (IXMLTag lFbTag : aItem.getChilds("feedbackcondition")) {
				if (lFbTag.getAttribute(AppConstants.keyId).equals(lFeedbackConditionId)) {
					lFeedback = CDesktopComponents.sSpring().unescapeXML(lFbTag.getChildValue("richtext"));
					lFeedbackTag = lFbTag;
					lCorrect = lFeedbackTag.getChildValue("correct");
				}
			}
			if (!(lFeedbackTag == null)) {
				if (lFeedback.equals(""))
					lFeedback = CDesktopComponents.vView().getLabel("run_assessment_item.feedbacktext." + lCorrect);
				lVbox.setZclass(this.getClassName() + "_item_feedbacktext" + lCorrect);
				aParent.appendChild(lVbox);
				Html lHtml = new CDefHtml("<br/>");
				lVbox.appendChild(lHtml);
				lHtml = new CDefHtml(lFeedback);
				lVbox.appendChild(lHtml);
//				append pieces
				createPieces(lVbox,lFeedbackTag);
			}
		}
		return lVbox;
	}

	/**
	 * Does item action. Is called when radio button aRadio corresponding to
	 * aAnswer is clicked.
	 * The answer is saved. If there is a corresponding feedback condition its id,
	 * its score and its correct status are saved too, and the feedback is shown.
	 * Within the assessments tree the item is rendered again to show its changed
	 * status.
	 * 
	 * @param aRadio the a radio
	 * @param aAnswer the a answer
	 */
	public void doItemAction(CRunItemRadioClassic aRadio, String aAnswer) {
		if (radioChecked != null)
			radioChecked.setChecked(false);
		radioChecked = aRadio;
		if ((refitem == null) || (item == null))
			return;
		setRunTagStatus(caseComponent,refitem,AppConstants.statusKeyAnswer,aAnswer,true);
		IXMLTag lFeedbackCondition = getFeedbackCondition(refitem,item);
		if (lFeedbackCondition != null) {
			setRunTagStatus(caseComponent,refitem,AppConstants.statusKeyFeedbackConditionId,lFeedbackCondition.getAttribute(AppConstants.keyId),true);
			setRunTagStatus(caseComponent,refitem,AppConstants.statusKeyScore,lFeedbackCondition.getChildValue("score"),true);
			setRunTagStatus(caseComponent,refitem,AppConstants.statusKeyCorrect,lFeedbackCondition.getChildValue("correct"),true);
		}

//		maybe add something for shared component
		if (lFeedbackCondition != null) {
			CRunHboxClassic lHbox = (CRunHboxClassic)CDesktopComponents.vView().getComponent(getId()+"FeedbackBox");
			if (lHbox != null)
				showFeedback(lHbox,refitem,item);
		}
		CRunAssessmentsTreeClassic lTree = (CRunAssessmentsTreeClassic)CDesktopComponents.vView().getComponent("runAssessmentsTree");
		if (lTree != null) {
			Treeitem lTreeitem = lTree.getContentItem(refitem.getAttribute(AppConstants.keyId));
			if (lTreeitem != null)
				lTree.reRenderTreeitem(lTreeitem,refitem,false);
		}
	}

	/**
	 * Is called by start or end assessment button and will start or finish the 
	 * current assessment.
	 * 
	 * @param aObserved the observed object, it should have interface CIObserved implemented
	 * @param aAction the action
	 * @param aStatus the status object, can be String or other class
	 */
	@Override
	public void observedNotify(CIObserved aObserved, String aAction, Object aStatus) {
		if (aAction.equals("startAssessment"))
			startAssessment((IXMLTag)aStatus);
		if (aAction.equals("endAssessment"))
			endAssessment((IXMLTag)aStatus);
	}

	/**
	 * Starts and renders assessment.
	 * If not first start confirmation is asked.
	 * Sets started of aAssessment to true and resets items involved within assessment.
	 * 
	 * @param aAssessment the a assessment
	 */
	public void startAssessment(IXMLTag aAssessment) {
		CRunAssessmentsTreeClassic lTree = (CRunAssessmentsTreeClassic)getRunContentComponent();
		if (lTree == null)
			return;
		Treeitem lTreeitem = lTree.getContentItem(aAssessment.getAttribute(AppConstants.keyId));
		if (lTreeitem == null)
			return;
//		ask if sure if not first time
		boolean lStarted = (CDesktopComponents.sSpring().getCurrentTagStatus(aAssessment,AppConstants.statusKeyStarted).equals(AppConstants.statusValueTrue));
		if (lStarted) {
			Map<String,Object> lParams = new HashMap<String,Object>();
			String lValue = (String) lTreeitem.getAttribute("keyvalues");
			lParams.put("title", CDesktopComponents.vView().getLabel("run_assessment_start.title") + " '" + lValue + "'");
			lParams.put("text", CDesktopComponents.vView().getLabel("run_assessment_start.a") + " '" + lValue + "'" + CDesktopComponents.vView().getLabel("run_assessment_start.b"));
			Window lWindow = CDesktopComponents.vView().modalPopup(VView.v_run_assessment_start, null, lParams, "center");
			if (lWindow.getAttribute("ok") == null)
				return;
		}
//		set status of assessment to started
		setRunTagStatus(caseComponent,aAssessment,AppConstants.statusKeyStarted,AppConstants.statusValueTrue, true);
		if (!CDesktopComponents.sSpring().getCurrentRunTagStatus(caseComponent,aAssessment,AppConstants.statusKeyOutfolded, getRunStatusType()).equals(AppConstants.statusValueTrue))
			setRunTagStatus(caseComponent,aAssessment,AppConstants.statusKeyOutfolded,AppConstants.statusValueTrue, true);
//		maybe add something for shared component
//		if already started earlier clear answers of used items
		if (lStarted) {
			for (IXMLTag lTag : aAssessment.getChilds("refitem")) {
				boolean lPresent = (!CDesktopComponents.sSpring().getCurrentTagStatus(lTag,AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse));
				if (lPresent) {
					setRunTagStatus(caseComponent,lTag,AppConstants.statusKeyAnswer,"",true);
					setRunTagStatus(caseComponent,lTag,AppConstants.statusKeyFeedbackConditionId,"",true);
					setRunTagStatus(caseComponent,lTag,AppConstants.statusKeyScore,"",true);
					setRunTagStatus(caseComponent,lTag,AppConstants.statusKeyOpened,AppConstants.statusValueFalse,true);
//					maybe add something for shared component
				}
			}
		}
		renderAssessment(aAssessment);
	}

	/**
	 * Sets item tag status. Item is refitem within assessment.
	 * 
	 * @param aAssessmentId the a assessment id
	 * @param aItemId the a item id
	 * @param aStatusKey the a status key
	 * @param aStatusValue the a status value
	 */
	public void setItemTagStatus(String aAssessmentId, String aItemId, String aStatusKey, String aStatusValue) {
		CRunAssessmentsTreeClassic lTree = (CRunAssessmentsTreeClassic)getRunContentComponent();
		if (lTree == null)
			return;
		Treeitem lTreeitem = lTree.getContentItem(aAssessmentId);
		if (lTreeitem == null)
			return;
		IXMLTag lAssessmentTag = lTree.getContentItemTag(lTreeitem);
		for (IXMLTag lTag : lAssessmentTag.getChilds("refitem")) {
			if (lTag.getAttribute(AppConstants.keyId).equals(aItemId)) {
				CDesktopComponents.sSpring().setRunTagStatusValue(CDesktopComponents.sSpring().getStatusTagOfDataTag(lTag), aStatusKey, aStatusValue);
			}
		}
	}
	
	
	/**
	 * (Re)renders assessment.
	 * 
	 * @param aAssessmentId the a assessment id
	 */
	public void renderAssessment(String aAssessmentId) {
		CRunAssessmentsTreeClassic lTree = (CRunAssessmentsTreeClassic)getRunContentComponent();
		if (lTree == null)
			return;
		Treeitem lTreeitem = lTree.getContentItem(aAssessmentId);
		if (lTreeitem == null)
			return;
		IXMLTag lTag = lTree.getContentItemTag(lTreeitem);
		if (lTag == null)
			return;
		renderAssessment(lTag);
	}
	
	/**
	 * (Re)renders assessment, a subtree with items within the assessmentstree. If a subset of the items should be shown, it
	 * is determined randomly. Removes possible item from item area.
	 * 
	 * @param aAssessment the a assessment
	 */
	public void renderAssessment(IXMLTag aAssessment) {
		if (renderAssLock)
			return;
		renderAssLock = true;
		CRunAssessmentsTreeClassic lTree = (CRunAssessmentsTreeClassic)getRunContentComponent();
		if (lTree == null) {
			renderAssLock = false;
			return;
		}
		Treeitem lTreeitem = lTree.getContentItem(aAssessment.getAttribute(AppConstants.keyId));
		if (lTreeitem == null) {
			renderAssLock = false;
			return;
		}
		List<IXMLTag> lItems = aAssessment.getChilds("refitem");
//		check if wanted number of items is smaller then total number of items
		int lTotalNumber = lItems.size();
		int lWantedNumber = lTotalNumber;
		String lNumberOfItemsStr = aAssessment.getChildValue("numberofitems");
		if (!lNumberOfItemsStr.equals(""))
			lWantedNumber = Integer.parseInt(lNumberOfItemsStr);
		if (lWantedNumber > lTotalNumber)
			lWantedNumber = lTotalNumber;
//		if so randomize items and hide items not used
		List<IXMLTag> lPresentItems = new ArrayList<IXMLTag>();
		if (lWantedNumber < lTotalNumber) {
//			get random wanted numbers out of totalnumber
			List<String> lNumbersToUse = new ArrayList<String>();
			while (lNumbersToUse.size()<lWantedNumber) {
				int rand = Integer.parseInt(""+Math.round(Math.random() * lTotalNumber));
				if (rand == lTotalNumber)
					rand = 0;
				rand = rand + 1;
				if (!lNumbersToUse.contains(""+rand))
					lNumbersToUse.add(""+rand);
			}
			int lCounter = 0;
			for (IXMLTag lTag : lItems) {
				lCounter = lCounter + 1;
				String lPresent = "";
				if (lNumbersToUse.contains(""+lCounter)) {
					lPresent = AppConstants.statusValueTrue;
					lPresentItems.add(lTag);
				}
				else
					lPresent = AppConstants.statusValueFalse;
				setRunTagStatus(caseComponent,lTag,AppConstants.statusKeyPresent,lPresent,true,false);
//				maybe add something for shared component
			}
		}
		else {
			boolean lAutostart = (CDesktopComponents.sSpring().getCurrentRunTagStatus(caseComponent,aAssessment,"autostart", getRunStatusType()).equals(AppConstants.statusValueTrue));
			for (IXMLTag lTag : lItems) {
				boolean lBPresent = (!CDesktopComponents.sSpring().getCurrentTagStatus(lTag,AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse));
				if ((!lAutostart) || (lBPresent)) {
					lPresentItems.add(lTag);
				}
				if (!lAutostart) {
					if (!lBPresent)
						setRunTagStatus(caseComponent,lTag,AppConstants.statusKeyPresent,AppConstants.statusValueTrue,true,false);
				}
//				maybe add something for shared component
			}
		}
//		set question numbers
		int lQCounter = 0;
		for (IXMLTag lTag : lPresentItems) {
			lQCounter = lQCounter + 1;
			setRunTagStatus(caseComponent,lTag,"question_number",""+lQCounter,true,false);
		}
		
//		render assessment items
		lTree.reRenderTreeitem(lTreeitem,aAssessment,true);
//		remove item
		CRunAreaClassic lItemArea = (CRunAreaClassic)CDesktopComponents.vView().getComponent(getId()+"ItemArea");
		if (lItemArea != null)
			removeItem(lItemArea);
		renderAssLock = false;
	}
	
	/**
	 * Extracts the items for the current assessment.
	 * 
	 * @param aTree the a assessmentstree
	 * @param aAssessment the a assessment
	 */
	private List<IXMLTag> getItemsList(CRunAssessmentsTreeClassic aTree, IXMLTag aAssessment) {
		List<IXMLTag> lItems = null;
		IXMLTag lAssTag = (IXMLTag)aTree.getAttribute("item");
		List<IXMLTag> lItems2 = lAssTag.getChildTags();
		for (IXMLTag lTag : lItems2) {
			if (lTag.getAttribute(AppConstants.keyId).equals(aAssessment.getAttribute(AppConstants.keyId))) {
				lItems = lTag.getChilds("refitem");
			}
		}
		return lItems;
	}

	/**
	 * Gets the feedback condition tag applicable for the given aAssessment.
	 * 
	 * @param aAssessment the a assessment tag
	 * 
	 * @return the feedback condition
	 */
	protected IXMLTag getAssessmentFeedbackCondition(IXMLTag aAssessment) {
		for (IXMLTag lFbTag : aAssessment.getChilds("feedbackcondition")) {
			if (CDesktopComponents.sSpring().getSScriptHelper().evaluateConditionTag(null, lFbTag, AppConstants.statusTypeRunGroup, false, false))
				return lFbTag;
		}
		return null;
	}

	/**
	 * Shows assessment end feedback possibly having references.
	 * 
	 * @param aParent the a parent
	 * @param aAssessment the assessment tag
	 * 
	 * @return the c run v box
	 */
	protected CRunVboxClassic showAssessmentFeedback(Component aParent,IXMLTag aAssessment) {
		CRunVboxClassic lVbox = new CRunVboxClassic();
		showFeedback = CDesktopComponents.sSpring().getCurrentRunTagStatus(caseComponent,aAssessment,"showfeedback", getRunStatusType()).equals(AppConstants.statusValueTrue);
		if (showFeedback) {
			IXMLTag lFeedbackTag = getAssessmentFeedbackCondition(aAssessment);
			if (!(lFeedbackTag == null)) {
				removeItem(aParent);
				CRunAreaClassic lArea = new CRunAreaClassic();
				aParent.appendChild(lArea);
				lArea.setZclass(className+"_ass_feedback");
				lVbox.setZclass(getClassName() + "_ass_feedbackbox");
				lArea.appendChild(lVbox);
				String lFeedback = CDesktopComponents.sSpring().unescapeXML(lFeedbackTag.getChildValue("richtext"));
				Html lHtml = new CDefHtml(lFeedback);
				lVbox.appendChild(lHtml);
				lHtml = new CDefHtml("<br/>");
				lVbox.appendChild(lHtml);
//				append pieces
				createPieces(lVbox,lFeedbackTag);
			}
		}
		return lVbox;
	}

	/**
	 * Ends assessment after confirmation.
	 * Determines assessment score and saves it within assessment.
	 * Finished of assessment is set to true.
	 * Removes possible item from item area.
	 * 
	 * @param aAssessment the a assessment
	 */
	public void endAssessment(IXMLTag aAssessment) {
		CRunAssessmentsTreeClassic lTree = (CRunAssessmentsTreeClassic)CDesktopComponents.vView().getComponent("runAssessmentsTree");
		if (lTree == null)
			return;
		Treeitem lTreeitem = lTree.getContentItem(aAssessment.getAttribute(AppConstants.keyId));
		if (lTreeitem == null)
			return;
//		ask if sure
		Map<String,Object> lParams = new HashMap<String,Object>();
		String lValue = (String) lTreeitem.getAttribute("keyvalues");
		lParams.put("title", CDesktopComponents.vView().getLabel("run_assessment_end.title") + " '" + lValue + "'");
		lParams.put("text", CDesktopComponents.vView().getLabel("run_assessment_end.a") + " '" + lValue + "'" + CDesktopComponents.vView().getLabel("run_assessment_end.b"));
		Window lWindow = CDesktopComponents.vView().modalPopup(VView.v_run_assessment_end, null, lParams, "center");
		if (lWindow.getAttribute("ok") == null)
			return;
		List<IXMLTag> lItems = getItemsList(lTree,aAssessment);
//		determine assessment score and save it
		int lScore = 0;
		if (lItems != null) {
			for (IXMLTag lTag : lItems) {
				boolean lPresent = (!CDesktopComponents.sSpring().getCurrentTagStatus(lTag,AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse));
				if (lPresent) {
					String lItemScore = CDesktopComponents.sSpring().getCurrentTagStatus(lTag,AppConstants.statusKeyScore);
					String lItemWeighting = lTag.getChildValue("weighting");
					if ((!lItemScore.equals("")) && (!lItemWeighting.equals(""))) {
						lScore = lScore + (Integer.parseInt(lItemScore)*Integer.parseInt(lItemWeighting));
					}
				}
			}
		}
		setRunTagStatus(caseComponent,aAssessment,AppConstants.statusKeyScore,""+lScore,true);
//		set status of assessment to finished
		setRunTagStatus(caseComponent,aAssessment,AppConstants.statusKeyFinished,AppConstants.statusValueTrue,true);
//		maybe add something for shared component
//		render assessment items
		lTree.reRenderTreeitem(lTreeitem,aAssessment,true);
//		remove item
		CRunAreaClassic lItemArea = (CRunAreaClassic)CDesktopComponents.vView().getComponent(getId()+"ItemArea");
		if (lItemArea != null) {
			removeItem(lItemArea);
			showAssessmentFeedback(lItemArea,aAssessment);
		}
	}

	@Override
	protected boolean updateContent(IXMLTag aDataTag, IXMLTag aStatusTag, String aStatusKey, String aStatusValue) {
		if ((aDataTag != null) && (aStatusKey.equals(AppConstants.statusKeyPresent))) {
			setItemTagStatus(aDataTag.getParentTag().getAttribute(AppConstants.keyId), aDataTag.getAttribute(AppConstants.keyId), aStatusKey, aStatusValue);
			renderAssessment(aDataTag.getParentTag().getAttribute(AppConstants.keyId));
		}
		return true;
	}

}
