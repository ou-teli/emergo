/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CIObserved;
import nl.surf.emergo.control.def.CDefMacro;
import nl.surf.emergo.control.run.CRunArea;
import nl.surf.emergo.control.run.CRunButton;
import nl.surf.emergo.control.run.CRunHbox;
import nl.surf.emergo.control.run.CRunLogbook;
import nl.surf.emergo.control.run.CRunNote;
import nl.surf.emergo.control.run.CRunVbox;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;

/**
 * The Class CRunLogbookOunl is used to show the logbook within the run view area of the
 * Emergo player.
 */
public class CRunLogbookOunl extends CRunLogbook {

	private static final long serialVersionUID = -6750327101547466703L;

	public String htmlLogbookNoteId = "htmlLogbookNote";
	public String textboxLogbookNoteId = "textboxLogbookNote";
	public String buttonLogbookNoteEditId = "buttonLogbookNoteEdit";
	public String buttonLogbookNoteSaveId = "buttonLogbookNoteSave";
	public int logbookRowNumber = 1;

	/**
	 * Instantiates a new c run logbook.
	 */
	public CRunLogbookOunl() {
		super();
		init();
	}

	/**
	 * Instantiates a new c run logbook.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the logbook case component
	 */
	public CRunLogbookOunl(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
		init();
	}

	/**
	 * Creates title area, content area to show tab structure,
	 * and buttons area with close button.
	 */
	@Override
	protected void createComponents() {
		CRunVbox lVbox = new CRunVbox();

		createTitleArea(lVbox);
		createContentArea(lVbox);
		createButtonsArea(lVbox);

		appendChild(lVbox);
	}

	/**
	 * Creates new content component, the profile view.
	 *
	 * @return the component
	 */
	@Override
	protected Component newContentComponent() {
		HtmlMacroComponent logbookView = new CDefMacro();
		logbookView.setId("runLogbookView");
		return logbookView;
	}

	/**
	 * Creates buttons area and adds close button.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run hbox
	 */
	@Override
	protected CRunHbox createButtonsArea(Component aParent) {
		return null;
	}

	/**
	 * Creates view for logbook.
	 * Gets current chapter, sets selected and opened of it to true and opens it.
	 */
	public void init() {
		Events.postEvent("onInitZulfile", this, null);
	}

	protected List<List<Object>> getLogbookDatas() {
		List<List<Object>> lDataList = new ArrayList<List<Object>>();
		
		CRunNote lRunNote = getRunNoteComponent();
		List<IXMLTag> lNoteTags = getNoteTags();
		Hashtable<String,IXMLTag> lHNoteTags = new Hashtable<String,IXMLTag>();
		//NOTE next loop results in hashtable containing last note made with title
		for (IXMLTag lNoteTag : lNoteTags) {
			String lTitle = lNoteTag.getChild("pid").getValue();
			lHNoteTags.put(lTitle, lNoteTag);
		}
		for (Enumeration<String> lTitels = lHNoteTags.keys(); lTitels.hasMoreElements();) {
			String lTitle = lTitels.nextElement();
			IXMLTag lNoteTag = lHNoteTags.get(lTitle);
			List<Object> data = new ArrayList<Object>();
			data.add(lNoteTag);
			data.add(lNoteTag.getAttribute(AppConstants.keyId));
			String lMainTitle = lTitle;
			String lSubTitle = "";
			if (lRunNote != null) {
				lMainTitle = lRunNote.getNoteMainTitle(lTitle);
				lSubTitle = lRunNote.getNoteSubTitle(lTitle);
			}
			data.add(lMainTitle);
			data.add(lSubTitle);
			String lNote = "";
			lNote = CDesktopComponents.sSpring().unescapeXML(lNoteTag.getChild("text").getValue()).replaceAll("\n", "<br/>");
			data.add(lNote);
			lDataList.add(data);
		}

		return lDataList;
	}

	public void reportLogbookData() {
		Object[][] data = null;
		List<List<Object>> logbookDatas = getLogbookDatas();
		if (logbookDatas.size() == 0) {
			return;
		}
		data = new Object[logbookDatas.size()][logbookDatas.get(0).size()-2];
		for (int i=0;i<logbookDatas.size();i++) {
			for (int j=2;j<logbookDatas.get(i).size();j++) {
				Object object = logbookDatas.get(i).get(j);
				if (object instanceof String) {
					object = ((String)object).replaceAll("<br/>", "\n");
				}
				data[i][j-2] = object; 
			}
		}
		CRunLogbookJRDataSource dataSource = new CRunLogbookJRDataSource();
		dataSource.setData(data);
		
		String lTitle = CDesktopComponents.sSpring().getCaseComponentRoleName("", caseComponent.getName()) + " " + CDesktopComponents.vView().getLabel("run_report.title.last");
		getDesktop().setAttribute("run_reporttitle", lTitle);
		String lUrl = "/" + CDesktopComponents.sSpring().getSCaseSkinHelper().getCaseSkinPath(caseComponent.getECase()) + VView.v_run_logbook_report_file;
		getDesktop().setAttribute("run_reportjasperfile", lUrl);
		getDesktop().setAttribute("run_reportdatasource", dataSource);

		CDesktopComponents.vView().modalPopupWithoutWaiting(VView.v_run_jasperreport, null, null, "center");
	}
	
	/**
	 * Gets all note tags.
	 */
	public List<IXMLTag> getNoteTags() {
		IXMLTag lRootTag = CDesktopComponents.sSpring().getSReferencedDataTagHelper().determineReferencedRootTag(caseComponent, getRunStatusType(), "refnote");
		if (lRootTag != null) {
			return lRootTag.getChild(AppConstants.contentElement).getChildTags(AppConstants.defValueNode);
		}
		else {
			return new ArrayList<IXMLTag>();
		}
	}

	/**
	 * Creates title area and shows name of case component within it.
	 * And adds close button
	 *
	 * @param aParent the ZK parent
	 *
	 * @return the c run area
	 */
	@Override
	protected CRunArea createTitleArea(Component aParent) {
		CRunHbox lHbox = new CRunHbox();
		aParent.appendChild(lHbox);
		CRunComponentDecoratorOunl decorator = createDecorator();
		CRunArea lTitleArea = decorator.createTitleArea(caseComponent, lHbox, getClassName());
		decorator.createCloseArea(caseComponent, lHbox, getClassName(), getId(), this);
		String lCaption = CDesktopComponents.sSpring().getCaseComponentRoleName("", caseComponent.getName()) + " " + CDesktopComponents.vView().getLabel("run_report.button.last");
		decorator.createReportBtn(lHbox, getClassName(), getId(), this, lCaption, getId());
		return lTitleArea;
	}

	/**
	 * Creates decorator.
	 *
	 * @return the decorator
	 */
	protected CRunComponentDecoratorOunl createDecorator() {
		return new CRunComponentDecoratorOunl();
	}

	/**
	 * Creates close button.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run button
	 */
	@Override
	protected CRunButton createCloseButton(Component aParent) {
		// NOTE Don't create close button
		return null;
	}

	/**
	 * Is called if report has to be generated.
	 *
	 * @param aObserved the observed object, it should have interface CIObserved implemented
	 * @param aAction the action
	 * @param aStatus the status object, can be String or other class
	 */
	@Override
	public void observedNotify(CIObserved aObserved, String aAction, Object aStatus) {
		if (aAction.equals("reportComponent")) {
			reportLogbookData();
		}
	}

	/**
	 * Methods originally used in ZUL file.
	 */

	public void onInitZulfile(Event aEvent) {
		Map<String,Object> propertyMap = new HashMap<String,Object>();
		propertyMap.put("currentEmergoComponent", this);
		propertyMap.put("logbookDatas", getLogbookDatas());
		propertyMap.put("htmlLogbookNoteId", htmlLogbookNoteId);
		propertyMap.put("textboxLogbookNoteId", textboxLogbookNoteId);
		propertyMap.put("buttonLogbookNoteEditId", buttonLogbookNoteEditId);
		propertyMap.put("buttonLogbookNoteSaveId", buttonLogbookNoteSaveId);
		((HtmlMacroComponent)getRunContentComponent()).setDynamicProperty("a_propertyMap", propertyMap);
		((HtmlMacroComponent)getRunContentComponent()).setMacroURI(VView.v_run_logbook_fr);
		getRunContentComponent().setVisible(true);
	}

}
