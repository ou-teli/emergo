/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl2;

import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.control.run.ounl.CRunNoteOunl;

/**
 * The Class CRunNoteOunl2 is used to show the note window within the run choice area of the
 * Emergo player. Notes can be edited within the logbook too, so this class has a public
 * method to save content.
 */
public class CRunNoteOunl2 extends CRunNoteOunl {

	private static final long serialVersionUID = 3399303408153597065L;

	/**
	 * Instantiates a new c run note.
	 * Creates title area with close box and edit field for note.
	 */
	public CRunNoteOunl2() {
		super();
	}

	/**
	 * Instantiates a new c run note.
	 * Creates title area with close box and edit field for note.
	 *
	 * @param aId the a id
	 * @param aRunComponent the a run component
	 */
	public CRunNoteOunl2(String aId, CRunComponent aRunComponent) {
		super(aId, aRunComponent);
	}

	@Override
	public void init() {
		textboxRows = 5;
		maxStringLength = 70;
		super.init();
	}

	@Override
	protected String getTextboxWidth() {
		return "488px";
	}

}
