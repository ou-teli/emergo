/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.Hashtable;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;

public class CRunDragdropformsItemsDiv extends CDefDiv {

	private static final long serialVersionUID = -6203862533721437326L;

	public void onCreate(CreateEvent aEvent) {
		List<Hashtable<String,Object>> items = (List<Hashtable<String,Object>>)getAttribute("items");
		if (items != null) {
			for (Hashtable<String,Object> item : items) {
				if (item.get("tag") != null) {
					Object[] eventPar = new Object[2];
					eventPar[0] = item;
					eventPar[1] = null;
					Events.postEvent("onInsertComponent", this, eventPar);
				}
			}
		}
	}

	public void onInsertComponent(Event aEvent) {
		Hashtable<String,Object> item = (Hashtable<String,Object>)((Object[])aEvent.getData())[0];
		Component beforeComponent = (Component)((Object[])aEvent.getData())[1];
		Component component = (Component)CDesktopComponents.vView().getComponent("templateDragdropformItem_" + ((IXMLTag)getAttribute("containertag")).getAttribute("id")).clone();
		component.setId("dragdropformItem_" + ((IXMLTag)item.get("tag")).getAttribute("id"));
		insertBefore(component, beforeComponent);
		Events.postEvent("onInit", component, item);
	}

	public void onRemoveComponent(Event aEvent) {
		Component removeComponent = (Component)aEvent.getData();
		removeComponent.detach();
	}

}
