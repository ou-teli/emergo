/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.feedback;

import org.zkoss.zk.ui.Component;

public class CRunPVToolkitFeedbackSkillButton extends CRunPVToolkitFeedbackSubSkillButton {
	
	private static final long serialVersionUID = -5602830195749445199L;

	public CRunPVToolkitFeedbackSkillButton(Component parent, String[] keys, Object[] values) {
		super(parent, keys, values);
		_maxLabelLength = 35;
	}
	
	@Override
	protected boolean hasScore() {
		return _feedbackRubric.skillHasScore();
	}
	
	@Override
	protected boolean hasTipsTops() {
		return _feedbackRubric.skillHasTipsTops();
	}
	
	@Override
	protected String imagePrefix() {
		return "skill";
	}
	
	@Override
	public void update(int level) {
		super.update(level);
		//NOTE for the moment set cursor to default because skill button is used to score skill but skill does not have performance levels
		setStyle("cursor:default;");
	}
	
	@Override
	protected String getLabelKey() {
		return "PV-toolkit-skillwheel.header.finalfeedback";
	}
	
	
	public void onClick() {
		//NOTE for the moment ignore _hasSCore because skill button is used to score skill but skill does not have performance levels
/*		if (!_hasScore || _feedbackRubric.inOverview()) {
			return;
		}
		CRunPVToolkitFeedbackScoreLevelsDiv feedbackScoreLevels = (CRunPVToolkitFeedbackScoreLevelsDiv)CDesktopComponents.vView().getComponent(_stepPrefix + "ScoreLevelsDiv");
		if (feedbackScoreLevels != null) {
			feedbackScoreLevels.setAttribute("skillclusterCounter", getAttribute("skillclusterCounter"));
			feedbackScoreLevels.setAttribute("subskillCounter", getAttribute("subskillCounter"));
			feedbackScoreLevels.init(_runPVToolkitCacAndTag, _level, _hasTipsTops, _feedbackRubricId);
			feedbackScoreLevels.setVisible(true);
		}
*/		
	}

}
