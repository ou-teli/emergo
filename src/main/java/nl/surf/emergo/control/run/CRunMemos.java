/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.Hashtable;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Include;
import org.zkoss.zul.Treeitem;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefInclude;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SStatustagEnrichment;

/**
 * The Class CRunMemos is used to show the memos within the run view area of the
 * Emergo player.
 */
public class CRunMemos extends CRunComponent {

	private static final long serialVersionUID = 877741019159787903L;

	/** The video view to show the video being played. It's an include so src can be changed. */
	protected Include videoView = null;

	/** The current conversation tag. */
	protected IECaseComponent currentconversationcomponent = null;

	/** The current conversation tag. */
	protected IXMLTag currentconversationtag = null;

	/** The current fragment tag. */
	protected IXMLTag currentfragmenttag = null;

	/**
	 * Instantiates a new c run memos.
	 */
	public CRunMemos() {
		super("runMemos", null);
		init();
	}

	/**
	 * Instantiates a new c run memos.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the memos case component
	 */
	public CRunMemos(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
		init();
	}

	/**
	 * Creates title area, content area to show memos and buttons area with close button.
	 */
	@Override
	protected void createComponents() {
		CRunVbox lVbox = new CRunVbox();
		appendChild(lVbox);

		createTitleArea(lVbox);
		CRunHbox lHbox = new CRunHbox();
		lVbox.appendChild(lHbox);
		createContentArea(lHbox);
		CRunArea lItemArea = createItemArea(lHbox);
		lItemArea.setId(getId()+"ItemArea");
		createButtonsArea(lVbox);
	}

	/**
	 * Creates new content component, the memos tree.
	 *
	 * @return the component
	 */
	@Override
	protected Component newContentComponent() {
		return (new CRunMemosTree("runMemosTree", caseComponent, this));
	}

	/**
	 * Creates buttons area and adds close button.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run hbox
	 */
	@Override
	protected CRunHbox createButtonsArea(Component aParent) {
		CRunHbox lButtonsHbox = super.createButtonsArea(aParent);
		createCloseButton(lButtonsHbox);
		return lButtonsHbox;
	}

	/**
	 * Does contentitem action, clicking on an memos item.
	 * Shows item within item area.
	 *
	 * @param aContentItem the a contentitem, the memos item clicked
	 */
	@Override
	public void doContentItemAction(Component aContentItem) {
		Treeitem lTreeitem = (Treeitem)aContentItem;
		IXMLTag lMemoTag = (IXMLTag)lTreeitem.getAttribute("item");
		if ((lMemoTag == null) || (!lMemoTag.getName().equals("memo")))
			return;
		playMemo(lMemoTag);
	}

	/**
	 * Plays memo. Only plays flv.
	 *
	 * @param aMemoTag the a memo tag
	 */
	public void playMemo(IXMLTag aMemoTag) {
		if (aMemoTag == null) {
			return;
		}
		String lCacId = aMemoTag.getAttribute(AppConstants.keyRefcacid);
		IXMLTag lRootTag = CDesktopComponents.sSpring().getXmlDataPlusRunStatusTree(lCacId, getRunStatusType());
		if (lRootTag == null)
			return;
		IXMLTag lContentTag = lRootTag.getChild(AppConstants.contentElement);
		if (lContentTag == null)
			return;
		currentconversationcomponent = CDesktopComponents.sSpring().getCaseComponent(lCacId);
		String lFragmentTagId = aMemoTag.getAttribute(AppConstants.keyRefdataid);
		String lConversationTagId = aMemoTag.getAttribute("refcontainerid");
		currentfragmenttag = CDesktopComponents.sSpring().getXmlManager().getTagById(lContentTag, lFragmentTagId);
		currentconversationtag = CDesktopComponents.sSpring().getXmlManager().getTagById(lContentTag, lConversationTagId);
		if (currentconversationcomponent == null || currentfragmenttag == null || currentconversationtag == null)
			return;
		SStatustagEnrichment lStatustagEnrichment = null; 
		IECaseComponent lReferencedCaseComponent = CDesktopComponents.sSpring().getSReferencedDataTagHelper().determineReferencedCaseComponent(caseComponent, getRunStatusType(), "refmemo");
		if (lReferencedCaseComponent != null) {
			lStatustagEnrichment = new SStatustagEnrichment();
			lStatustagEnrichment.setAttributes(new Hashtable<String, String>());
			lStatustagEnrichment.getAttributes().put(AppConstants.keyRefcacid, "" + lReferencedCaseComponent.getCacId());
		}
		setRunTagStatus(caseComponent, aMemoTag, AppConstants.statusKeySelected, AppConstants.statusValueTrue, lStatustagEnrichment, true);
		setRunTagStatus(caseComponent, aMemoTag, AppConstants.statusKeyOpened, AppConstants.statusValueTrue, lStatustagEnrichment, true);
		initVideoSize();
		playFragment(currentconversationcomponent, currentconversationtag, currentfragmenttag);
	}

	/**
	 * Gets video size for current conversation and, if set, sets size of conversation.
	 */
	protected void initVideoSize() {
		// NOTE Get size of conversation. If not null, it overrules default values in style class.
		String lVideoSizeWidth = getConversationTagChildValue("size", 0);
		String lVideoSizeHeight = getConversationTagChildValue("size", 1);
		if (lVideoSizeWidth.equals("")) {
			lVideoSizeWidth = getComponentTagStatusValue("size", 0);
		}
		if (lVideoSizeHeight.equals("")) {
			lVideoSizeHeight = getComponentTagStatusValue("size", 1);
		}

		// NOTE Set videoView attributes to be used by video player
		videoView.setAttribute("video_size_width", lVideoSizeWidth);
		videoView.setAttribute("video_size_height", lVideoSizeHeight);
	}

	protected String getConversationTagChildValue(String aConversationTagChildName, int index) {
		if (currentconversationtag == null) {
			return "";
		}
		return getValueByIndex(currentconversationtag.getChildValue(aConversationTagChildName), index);
	}

	protected String getComponentTagStatusValue(String aStatusKey, int index) {
		return getValueByIndex(CDesktopComponents.sSpring().getCurrentRunComponentStatus(caseComponent, aStatusKey, AppConstants.statusTypeRunGroup), index);
	}

	protected String getValueByIndex(String aValues, int index) {
		if (aValues.equals("")) {
			return "";
		}
		int value = 0;
		String[] arr = aValues.split(",");
		if (index >= arr.length) {
			return "";
		}
		try {
			value = Integer.parseInt(arr[index]);
		} catch (NumberFormatException e) {
			return "";
		}
		return "" + value + "px";
	}

	/**
	 * Creates view for video.
	 * Gets current setting, sets selected and opened of it to true and starts it.
	 */
	public void init() {
		createVideoView(this);
	}

	/**
	 * Creates video view.
	 *
	 * @param aParent the a parent
	 */
	protected void createVideoView(Component aParent) {
		videoView = new CDefInclude();
		videoView.setId("runMemosVideoView");
		videoView.setVisible(false);
		aParent.appendChild(videoView);
	}

	/**
	 * Gets the run component helper.
	 *
	 * @return the run component helper
	 */
	@Override
	public CRunComponentHelper getRunComponentHelper() {
		return new CRunComponentHelper(null, "memo", caseComponent, this);
	}

	/**
	 * Plays fragment. Only plays flv.
	 *
	 * @param aConversationTag the a conversation tag
	 * @param aFragmentTag the a fragment tag
	 */
	public void playFragment(IECaseComponent aConversationComponent, IXMLTag aConversationTag, IXMLTag aFragmentTag) {
		videoView.setSrc(VView.v_run_empty_fr);
		videoView.setVisible(false);
		if (aConversationTag == null || aFragmentTag == null) {
			return;
		}
		String lUrl = CDesktopComponents.sSpring().getSBlobHelper().getUrl(aFragmentTag);
		if (!lUrl.equals("")) {
			IXMLTag lBlobChild = aFragmentTag.getChild("blob");
			String lBlobtype = lBlobChild.getAttribute(AppConstants.keyBlobtype);
			if (!(lBlobtype.equals(AppConstants.blobtypeExtUrl))) {
				// put emergo path before url for player
				if (!CDesktopComponents.vView().isAbsoluteUrl(lUrl))
					lUrl = CDesktopComponents.vView().getEmergoRootUrl() + CDesktopComponents.vView().getEmergoWebappsRoot() + lUrl;
			}
		}
		if (lUrl.equals("")) {
			return;
		}
		videoView.setAttribute("url", lUrl);
		videoView.setSrc(VView.v_run_flash_memos_fr);

		videoView.setVisible(true);

		if (runWnd != null) {
			// NOTE Set note case component to conversations component in which conversation is played
			IECaseComponent lNoteCaseComponent = aConversationComponent;
			// NOTE If parent tag of fragment tag is conversation tag set note tag to conversation tag, otherwise to fragment tag.
			IXMLTag lNoteTag = null;
			if (aFragmentTag == null || aFragmentTag.getParentTag().getName().equals("conversation")) {
				lNoteTag = aConversationTag;
			}
			else {
				if (aFragmentTag.getParentTag().getName().equals("question")) {
					// NOTE Notes are made related to question
					lNoteTag = aFragmentTag.getParentTag();
				}
			}
			if (lNoteCaseComponent != null && lNoteTag != null) {
				runWnd.setNoteTag(lNoteCaseComponent, lNoteTag);
			}
		}
	}

}
