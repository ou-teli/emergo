/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.practice;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitSubStepsDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.feedback.CRunPVToolkitFeedbackFeedbackDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.feedback.CRunPVToolkitFeedbackOverviewRubricDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.feedback.CRunPVToolkitFeedbackRecordingsDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.myRecordings.CRunPVToolkitMyRecordingsRecordingsDiv;
import nl.surf.emergo.domain.IERunGroup;

public class CRunPVToolkitPracticeFeedbackDiv extends CRunPVToolkitFeedbackFeedbackDiv {

	private static final long serialVersionUID = -2520611493572123478L;

	@Override
	public void init(IERunGroup actor, IXMLTag practiceStepTag, IXMLTag practiceTag, boolean editable) {
		_peerFeedback = false;
		
		super.init(actor, practiceStepTag, practiceTag, editable);
	}

	@Override
	protected String getTitle() {
		CRunPVToolkitSubStepsDiv runPVToolkitSubStepsDiv = (CRunPVToolkitSubStepsDiv)CDesktopComponents.vView().getComponent("practiceSubStepsDiv");
		return (String)runPVToolkitSubStepsDiv.getAttribute("divTitle");
	}
	
	@Override
	protected void addBackButtonOnClickEventListener(Component component, IXMLTag tag) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				clearVideo();
				toRecordings(_idPrefix + "FeedbackDiv");
			}
		});
	}

	public void toRecordings(String fromComponentId) {
		Component fromComponent = CDesktopComponents.vView().getComponent(fromComponentId);
		if (fromComponent != null) {
			fromComponent.setVisible(false);
		}
		Component toComponent = CDesktopComponents.vView().getComponent(_idPrefix + "RecordingsDiv");
		if (toComponent != null) {
			if (toComponent instanceof CRunPVToolkitFeedbackRecordingsDiv) {
				((CRunPVToolkitFeedbackRecordingsDiv)toComponent).update();
				toComponent.setVisible(true);
			}
			else if (toComponent instanceof CRunPVToolkitPracticeRecordingsDiv) {
				((CRunPVToolkitPracticeRecordingsDiv)toComponent).update();
				toComponent.setVisible(true);
			}
			else if (toComponent instanceof CRunPVToolkitMyRecordingsRecordingsDiv) {
				((CRunPVToolkitMyRecordingsRecordingsDiv)toComponent).update();
				toComponent.setVisible(true);
			}
		}
	}

	@Override
	protected void addForwardButtonOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				clearVideo();
				toOverview(_idPrefix + "FeedbackDiv");
			}
		});
	}

	public void toOverview(String fromComponentId) {
		Component fromComponent = CDesktopComponents.vView().getComponent(fromComponentId);
		if (fromComponent != null) {
			fromComponent.setVisible(false);
		}
		CRunPVToolkitFeedbackOverviewRubricDiv toComponent = (CRunPVToolkitFeedbackOverviewRubricDiv)CDesktopComponents.vView().getComponent(_idPrefix + "OverviewRubricDiv");
		if (toComponent != null) {
			toComponent.init(_actor, _currentStepTag, _currentTag, _editable, true);
			toComponent.setVisible(true);
		}
	}

}
