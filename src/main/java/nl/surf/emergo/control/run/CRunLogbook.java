/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.HashMap;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Html;
import org.zkoss.zul.Label;
import org.zkoss.zul.Treeitem;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CIObserved;
import nl.surf.emergo.control.def.CDefHtml;
import nl.surf.emergo.control.def.CDefLabel;
import nl.surf.emergo.control.def.CDefTextbox;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;

/**
 * The Class CRunLogbook is used to show the logbook within the run view area of the
 * Emergo player.
 */
public class CRunLogbook extends CRunComponent {

	private static final long serialVersionUID = 8825520224247055912L;

	protected int runLogbookNoteRows = 14;
	
	protected String runLogbookNoteWidth = "400px";

	/**
	 * Instantiates a new c run logbook.
	 */
	public CRunLogbook() {
		super("runLogbook", null);
	}

	/**
	 * Instantiates a new c run logbook.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the logbook case component
	 */
	public CRunLogbook(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
	}

	/**
	 * Creates title area, content area to show logbook, item area to show
	 * selected logbook note and buttons area with close button.
	 */
	@Override
	protected void createComponents() {
		CRunVbox lVbox = new CRunVbox();
		appendChild(lVbox);

		createTitleArea(lVbox);
		CRunHbox lHbox = new CRunHbox();
		lVbox.appendChild(lHbox);
		createContentArea(lHbox);
		CRunArea lItemArea = createItemArea(lHbox);
		lItemArea.setId(getId()+"ItemArea");
		createButtonsArea(lVbox);
	}

	/**
	 * Creates new content component, the logbook tree.
	 *
	 * @return the component
	 */
	@Override
	protected Component newContentComponent() {
		return (new CRunLogbookTree("runLogbookTree", caseComponent, this));
	}

	/**
	 * Creates buttons area and adds close button.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run hbox
	 */
	@Override
	protected CRunHbox createButtonsArea(Component aParent) {
		CRunHbox lButtonsHbox = super.createButtonsArea(aParent);
		createOverviewButton(lButtonsHbox);
		createCloseButton(lButtonsHbox);
		return lButtonsHbox;
	}

	/**
	 * Creates overview button.
	 *
	 * @param aParent the a parent
	 */
	protected void createOverviewButton(Component aParent) {
		aParent.appendChild(newOverviewButton());
	}

	/**
	 * Creates new overview button.
	 *
	 * @return the c run button
	 */
	protected CRunButton newOverviewButton() {
		String lLabel = CDesktopComponents.vView().getLabel("run_logbook.button.overview");
		CRunButton lButton = new CRunButton("", "overview", "", lLabel, "_component_100", "");
		lButton.registerObserver(getId());
		return lButton;
	}

	/**
	 * Does contentitem action, clicking on an logbook item.
	 * Shows item within item area.
	 *
	 * @param aContentItem the a contentitem, the logbook item clicked
	 */
	@Override
	public void doContentItemAction(Component aContentItem) {
		Treeitem lTreeitem = (Treeitem)aContentItem;
		IXMLTag tag = (IXMLTag)lTreeitem.getAttribute("item");
		if ((tag == null) || (!tag.getName().equals("note")))
			return;
		CRunArea lItemArea = (CRunArea)CDesktopComponents.vView().getComponent(getId()+"ItemArea");
		if (lItemArea != null)
			createItem(lItemArea,tag);
	}

	/**
	 * Shows item within item area. First removes previous item.
	 * Shows note in edit field and save button.
	 *
	 * @param aParent the ZK parent
	 * @param aItem the piece within the canon case component
	 *
	 * @return the c run vbox
	 */
	protected CRunVbox createItem(Component aParent,IXMLTag aItem) {
		removeItem(aParent);
		CRunArea lArea = new CRunArea();
		aParent.appendChild(lArea);
		lArea.setZclass(getClassName()+"_item");
		CRunVbox lVbox = new CRunVbox();
		lArea.appendChild(lVbox);
//		append note
		createNote(lVbox,aItem);
//		append break
		createBreak(lVbox);
//		append note ok button
		createNoteOkBtn(lVbox, aItem);
		return lVbox;
	}

	/**
	 * Removes item.
	 *
	 * @param aParent the a parent
	 */
	protected void removeItem(Component aParent) {
		if (aParent.getChildren() != null)
			aParent.getChildren().clear();
	}

	/**
	 * Shows note within item area.
	 *
	 * @param aParent the a parent
	 * @param aItem the a item
	 */
	protected void createNote(Component aParent,IXMLTag aItem) {
		String lNoteTitle = aItem.getChild("pid").getValue();
		Label lLabel = new CDefLabel();
		aParent.appendChild(lLabel);
		lLabel.setSclass(getClassName() + "_s_margin");
//		lLabel.setValue(VView.getCapitalizeFirstChar(CContentHelper.getNodeTagLabel(caseComponent.getEComponent().getCode(), "note")) + ": " + lNoteTitle);
		lLabel.setValue(lNoteTitle);
		String lNote = "";
		lNote = CDesktopComponents.sSpring().unescapeXML(aItem.getChild("text").getValue());
		CDefTextbox lTextbox = new CDefTextbox();
		aParent.appendChild(lTextbox);
		lTextbox.setZclass(getClassName() + "_text_area_div");
		lTextbox.setId("runLogbookNote");
		lTextbox.setValue(lNote);
		lTextbox.setAttribute("changed", "false");
		lTextbox.setRows(runLogbookNoteRows);
		lTextbox.setWidth(runLogbookNoteWidth);
	}

	/**
	 * Shows note ok btn within item area.
	 *
	 * @param aParent the a parent
	 * @param aItem the a item
	 */
	protected void createNoteOkBtn(Component aParent,IXMLTag aItem) {
		CRunButton lButton = new CRunButton("", "noteOk", aItem, CDesktopComponents.vView().getLabel("save"), "_component_100", "");
		aParent.appendChild(lButton);
		lButton.setSclass(getClassName() + "_s_margin " + getClassName() + "_s_item_save");
		lButton.registerObserver(getId());
	}

	/**
	 * Creates break within item area.
	 *
	 * @param aParent the a parent
	 */
	protected void createBreak(Component aParent) {
		Html lHtml = new CDefHtml("<br/>");
		aParent.appendChild(lHtml);
		lHtml.setSclass(getClassName() + "_s_margin");
	}

	/**
	 * Is called by note ok button and will save the note for the current logbook item.
	 *
	 * @param aObserved the observed object, it should have interface CIObserved implemented
	 * @param aAction the action
	 * @param aStatus the status object, can be String or other class
	 */
	public void observedNotify(CIObserved aObserved, String aAction, Object aStatus) {
		if (aAction.equals("noteOk"))
			saveNote((IXMLTag)aStatus);
		if (aAction.equals("overview"))
			showOverview();
	}

	/**
	 * Saves note within note component.
	 *
	 * @param aItem the a item
	 */
	protected void saveNote(IXMLTag aItem) {
		CDefTextbox lTextbox = (CDefTextbox)CDesktopComponents.vView().getComponent("runLogbookNote");
		String lNote = "";
		boolean lNoteChanged = false;
		if (lTextbox != null) {
			lNoteChanged = (((String)lTextbox.getAttribute("changed")).equals(AppConstants.statusValueTrue));
			if (lNoteChanged) {
				lNote = lTextbox.getValue();
			}
		}
		if (lNoteChanged) {
			saveNote(aItem, lNote);
		}
	}

	/**
	 * Saves note within note component.
	 *
	 * @param aItem the a item
	 * @param aNote the a note
	 */
	public void saveNote(IXMLTag aItem, String aNote) {
		if (aItem == null) {
			return;
		}
		CRunNote lRunNote = getRunNoteComponent();
		if (lRunNote != null) {
			IECaseComponent lNoteCaseComponent = CDesktopComponents.sSpring().getSReferencedDataTagHelper().determineReferencedCaseComponent(caseComponent, getRunStatusType(), "refnote");
			lRunNote.setContent(lNoteCaseComponent, aItem.getAttribute(AppConstants.keyRefcacid), aItem.getAttribute(AppConstants.keyRefdataid), aItem.getAttribute(AppConstants.keyRefstatusid), aNote);
		}
	}

	/**
	 * Gets run note component.
	 *
	 * @return the run note component
	 */
	public CRunNote getRunNoteComponent() {
		CRunNote lRunNote = (CRunNote)CDesktopComponents.vView().getComponent("runNote");
		if (lRunNote == null) {
			if (runWnd != null)
				// create run note component
				runWnd.onAction(getId(), "createNote", null);
			lRunNote = (CRunNote)CDesktopComponents.vView().getComponent("runNote");
		}
		return lRunNote;
	}

	/**
	 * Shows logbook overview.
	 */
	protected void showOverview() {
		CRunTree lRunTree = (CRunTree)CDesktopComponents.vView().getComponent("runLogbookTree");
		if (lRunTree == null)
			return;
		String lBody = "";
		IXMLTag lRootTag = lRunTree.getRootTag();
		if (lRootTag == null)
			return;
		IXMLTag lContentTag = lRootTag.getChild(AppConstants.contentElement);
		if (lContentTag == null)
			return;
		for (IXMLTag lNoteTag : lContentTag.getChilds("note")) {
			String lNoteTitle = lNoteTag.getChild("pid").getValue();
			String lNote = "";
			lNote = CDesktopComponents.sSpring().unescapeXML(lNoteTag.getChild("text").getValue());
			if (!lBody.equals(""))
				lBody = lBody + "<br/>";
			lBody = lBody + "<b>" + lNoteTitle + "</b><br/>" + lNote + "<br/>";
		}
		Map<String,Object> lParams = new HashMap<String,Object>();
		lParams.put("body", lBody);
		CDesktopComponents.vView().modalPopupWithoutWaiting(VView.v_run_logbook_overview, null, lParams, "center");
	}

}
