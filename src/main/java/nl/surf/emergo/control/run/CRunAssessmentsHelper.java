/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Html;
import org.zkoss.zul.Label;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.Treerow;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CCaseHelper;
import nl.surf.emergo.control.CContentHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CTree;
import nl.surf.emergo.control.def.CDefHbox;
import nl.surf.emergo.control.def.CDefLabel;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.view.VView;

/**
 * The Class CRunAssessmentsHelper. Helps rendering assessments.
 */
public class CRunAssessmentsHelper extends CRunComponentHelper {

	/** The case helper. */
	protected CCaseHelper caseHelper = null;

	/**
	 * Instantiates a new c run assessments helper.
	 *
	 * @param aTree the ZK assessments tree
	 * @param aShowTagNames the a show tag names, comma separated, only tags with these names are shown
	 * @param aCaseComponent the assessments case component
	 * @param aRunComponent the a run component, the ZK assessments component
	 */
	public CRunAssessmentsHelper(CTree aTree, String aShowTagNames,	IECaseComponent aCaseComponent, CRunComponent aRunComponent) {
		super(aTree, aShowTagNames, aCaseComponent, aRunComponent);
	}

	/**
	 * Gets the case helper.
	 *
	 * @return the case helper
	 */
	protected CCaseHelper getCaseHelper() {
		if (caseHelper == null)
			caseHelper = new CCaseHelper();
		return caseHelper;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTreeHelper#renderTreecell(org.zkoss.zul.Treeitem, org.zkoss.zul.Treerow, nl.surf.emergo.business.IXMLTag, java.lang.String, boolean)
	 */
	@Override
	protected Treecell renderTreecell(Treeitem aTreeitem, Treerow aTreerow,	IXMLTag aItem, String aKeyValues, boolean aAccIsAuthor) {
		Treecell lTreecell = getTreerowTreecell(aTreerow, 0);
		IXMLTag lItem = null;
		String lCarId = "0";
		String lCacId = "0";
		String lTagId = "0";
		if (aItem.getName().equals("refitem")) {
//			get item from associated itemtool
			String lReftype = aItem.getChild("ref").getDefTag().getAttribute(AppConstants.defKeyReftype);
			IECaseRole lCaseRole = CDesktopComponents.sSpring().getSCaseRoleHelper().getCaseRole();
			String lQuestion = "";
			List<String> lRefIds = getCaseHelper().getRefTagIds(lReftype,""+AppConstants.statusKeySelectedIndex,lCaseRole,caseComponent,aItem);
//			only one ref
			String lRefId = (String)lRefIds.get(0);
			if ((lRefId != null) && (!lRefId.equals(""))) {
				String[] lIdArr = lRefId.split(",");
				if (lIdArr.length == 3) {
					lCarId = lIdArr[0];
					lCacId = lIdArr[1];
					lTagId = lIdArr[2];
					lItem = CDesktopComponents.sSpring().getTag(CDesktopComponents.sSpring().getCaseComponent(Integer.parseInt(lCacId)),lTagId);
					aKeyValues = CDesktopComponents.sSpring().unescapeXML(lItem.getChildValue("richtext"));
					int lOff = aKeyValues.indexOf("<p>");
					if (lOff >= 0)
						aKeyValues = aKeyValues.substring(lOff+3);
					lOff = aKeyValues.indexOf("</p>");
					if (lOff >= 0)
						aKeyValues = aKeyValues.substring(0,lOff);
					lQuestion = CDesktopComponents.sSpring().unescapeXML(lItem.getChildValue("shorttitle"));
				}
			}
			if (lQuestion.equals("")) {
				lQuestion = CContentHelper.getNodeTagLabel(caseComponent.getEComponent().getCode(), "question");
				String lNumber = CDesktopComponents.sSpring().getCurrentTagStatus(aItem, "question_number");
				if (!lNumber.equals(""))
					lQuestion = lQuestion + " " + lNumber;
			}
			Html lHtml = getComponentHtml(lTreecell, "question", lQuestion);
		}
		else
			lTreecell.setLabel(aKeyValues);
		aTreeitem.setAttribute("carrefid",lCarId);
		aTreeitem.setAttribute("cacrefid",lCacId);
		aTreeitem.setAttribute("tagref",lItem);
		String lStatus = "active";
		if (isOpened(aItem))
			lStatus = "opened";
		lTreecell.setZclass(runComponent.getClassName() + "_treecell_"+lStatus);
		if (aItem.getName().equals("assessment")) {
//			show assessment description as hover
			String lToolTip = CDesktopComponents.sSpring().unescapeXML(aItem.getChildValue("hovertext"));
			if ((lToolTip != null) & (!lToolTip.equals(""))) {
				lTreecell.setTooltip("toolTip" + VView.initTooltip);
				lTreecell.setAttribute("tooltip", lToolTip);
			}
//			show assessment status in second column
//			show assessment ready button in second column
		}
		if (aItem.getName().equals("refitem")) {
//			get item from associated itemtool
//			show its name in tree
//			show item status in second column
		}
		return lTreecell;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.CContentHelper#showStatus(org.zkoss.zul.Treecell, nl.surf.emergo.business.IXMLTag)
	 */
	@Override
	protected void showStatus(Treecell aObject, IXMLTag aItem) {
		aObject.getChildren().clear();

		if (aItem.getName().equals("assessment")) {
			boolean lStarted = (aItem.getCurrentStatusAttribute(AppConstants.statusKeyStarted).equals(AppConstants.statusValueTrue));
			String lStartedTime = "" + aItem.getCurrentStatusAttributeTime(AppConstants.statusKeyStarted);
			String lFinishedTime = "" + aItem.getCurrentStatusAttributeTime(AppConstants.statusKeyFinished);
			boolean lAutostart = (CDesktopComponents.sSpring().getCurrentRunTagStatus(caseComponent,aItem,"autostart", getRunStatusType()).equals(AppConstants.statusValueTrue));
			if (!lAutostart) {
				CDefHbox lHbox = new CDefHbox();
				aObject.appendChild(lHbox);
				if (lStarted) {
//					if lStarted show last score
					String lScore = CDesktopComponents.sSpring().getCurrentTagStatus(aItem,AppConstants.statusKeyScore);
					if (!lScore.equals(""))
						lScore = CDesktopComponents.vView().getLabel("run_assessments.score")+":"+lScore;
					Label lLabel = new CDefLabel(lScore);
					lLabel.setZclass(runComponent.getClassName() + "_score");
					lHbox.appendChild(lLabel);
				}
				boolean lBusy = false;
				if ((lStartedTime != null) && (!lStartedTime.equals(""))) {
					if ((lFinishedTime == null) || (lFinishedTime.equals("")))
						lBusy = true;
					else
						lBusy = (Double.parseDouble(lStartedTime) > Double.parseDouble(lFinishedTime));
				}
				if (!lBusy) {
//					if not lBusy show start button
					createStartButton(lHbox, aItem);
				}
				else {
//					else show end button
					createEndButton(lHbox, aItem);
				}
			}
		}
		if (aItem.getName().equals("refitem")) {
			Label lLabel = new CDefLabel("");
			lLabel.setZclass(runComponent.getClassName() + "_answer");
			aObject.appendChild(lLabel);
		}
	}

	protected Component createStartButton (Component aParent, IXMLTag aItem) {
		CRunButton lButton = new CRunButton("","startAssessment",aItem,CDesktopComponents.vView().getLabel("run_assessments.button.start_assessment"),"_assessments_100","");
		lButton.registerObserver("runAssessments");
		aParent.appendChild(lButton);
		return lButton;
	}

	protected Component createEndButton (Component aParent, IXMLTag aItem) {
		CRunButton lButton = new CRunButton("","endAssessment",aItem,CDesktopComponents.vView().getLabel("run_assessments.button.end_assessment"),"_assessments_100","");
		lButton.registerObserver("runAssessments");
		aParent.appendChild(lButton);
		return lButton;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTreeHelper#renderInfoTreecell(org.zkoss.zul.Treeitem, org.zkoss.zul.Treerow, nl.surf.emergo.business.IXMLTag, java.lang.String, boolean)
	 */
	@Override
	protected Treecell renderInfoTreecell(Treeitem aTreeitem, Treerow aTreerow, IXMLTag aItem, String aKeyValues, boolean aAccIsAuthor) {
		Treecell lTreecell = getTreerowTreecell(aTreerow, 1);
		String lStatus = "active";
		if (isOpened(aItem))
			lStatus = "opened";
		lTreecell.setZclass(runComponent.getClassName() + "_treecell_"+lStatus);
		showStatus(lTreecell, aItem);
		return lTreecell;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.CContentHelper#renderTreeitem(org.zkoss.zk.ui.Component, org.zkoss.zk.ui.Component, nl.surf.emergo.business.IXMLTag)
	 */
	public Treeitem renderTreeitem(Component aParent, Component aInsertBefore, IXMLTag aItem) {
		if (isRenderTreeitem(aItem)) {
			return super.renderTreeitem(aParent,aInsertBefore,aItem);
		}
		else {
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.CContentHelper#reRenderTreeitem(org.zkoss.zk.ui.Component, nl.surf.emergo.business.IXMLTag)
	 */
	public Treeitem reRenderTreeitem(Component aComponent, IXMLTag aItem) {
		if (isRenderTreeitem(aItem)) {
			return super.reRenderTreeitem(aComponent, aItem);
		}
		else {
			return null;
		}
	}

	protected boolean isRenderTreeitem(IXMLTag aItem) {
		if (aItem.getName().equals("feedbackcondition"))
			return false;
		IXMLTag lParentTag = aItem.getParentTag();
		if (lParentTag.getName().equals("assessment")) {
			boolean lStarted = (CDesktopComponents.sSpring().getCurrentTagStatus(lParentTag,AppConstants.statusKeyStarted).equals(AppConstants.statusValueTrue));
			if (!lStarted)
				return false;
		}
		if (aItem.getName().equals("refitem")) {
//			get item from associated itemtool
			String lReftype = aItem.getChild("ref").getDefTag().getAttribute(AppConstants.defKeyReftype);
			IECaseRole lCaseRole = CDesktopComponents.sSpring().getSCaseRoleHelper().getCaseRole();
			List<IXMLTag> lRefTags = getCaseHelper().getRefTags(lReftype,""+AppConstants.statusKeySelectedIndex,lCaseRole,caseComponent,aItem);
			if (lRefTags.size() == 0)
				return false;
		}
		return true;
	}

}