/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.CreateEvent;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.CRunStreamingMediaInitBox;
import nl.surf.emergo.utilities.PropsValues;
import nl.surf.emergo.view.VView;

public class CRunVideoReferencesInitBoxClassic extends CRunStreamingMediaInitBox {

	private static final long serialVersionUID = 4360959762574003112L;

	public void onCreate(CreateEvent aEvent) {
		init();

		// NOTE that desktop is null in constructor, therefore following code is in
		// onCreate method
		CDesktopComponents.sSpring().initDesktopAttributes(getDesktop());

		String runComponentId = (String) VView.getParameter("runComponentId", this, "runReferences");

		String generalWidth = (String) CDesktopComponents.cControl().getAccSessAttr("content_area_width");
		if (generalWidth == null) {
			generalWidth = "1024px";
		}
		String generalHeight = (String) CDesktopComponents.cControl().getAccSessAttr("content_area_height");
		if (generalHeight == null) {
			generalHeight = "576px";
			// generalHeight="610px";
		}
		// NOTE Possibly overrule global width and height for player by local values for
		// current conversation
		String localwidth = Executions.getCurrent().getParameter("video_size_width");
		if (localwidth != null && !localwidth.equals("")) {
			generalWidth = localwidth;
		}
		String localheight = Executions.getCurrent().getParameter("video_size_height");
		if (localheight != null && !localheight.equals("")) {
			generalHeight = localheight;
		}

		String width = (String) VView.getParameter("video_size_width", this, generalWidth);
		String height = (String) VView.getParameter("video_size_height", this, generalHeight);

		// NOTE for jw player image may not be null or empty string, so only add image
		// line of not empty.
		// If image is null or empty string jw player reloads run.zul!!!!!
		String image = (String) VView.getParameter("image", this, "");
		String imageLine = "";
		if (!image.equals("")) {
			imageLine = "image: \"" + image + "\",";
		}

		HtmlMacroComponent macro = new HtmlMacroComponent();
		appendChild(macro);
		macro.setDynamicProperty("a_emergoRootUrl", vView.getAbsoluteWebappsRoot());
		macro.setDynamicProperty("a_jwplayerKey", PropsValues.VIDEO_JWPLAYER_KEY);
		macro.setDynamicProperty("a_playerName", "EM_fl_references");
		macro.setDynamicProperty("a_width", width);
		macro.setDynamicProperty("a_height", height);
		macro.setDynamicProperty("a_runComponentId", runComponentId);
		macro.setDynamicProperty("a_streamingUrls", streamingurls);
		// NOTE url may contain back slashes. Replace by slashes
		url = url.replaceAll(PropsValues.STREAMING_PLAYER_PATH_SEPARATOR, "/");
		macro.setDynamicProperty("a_url", url);
		macro.setDynamicProperty("a_imageLine", imageLine);
		macro.setMacroURI("run_" + PropsValues.VIDEO_PLAYER_ID + "_references_view_fr_macro.zul");
	}

}
