/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefMacro;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpringHelper;
import nl.surf.emergo.zkspring.STriggeredReference;

/**
 * The Class CRunForms is the ancestor of all components using forms.
 */
public class CRunForms extends CRunComponent {

	private static final long serialVersionUID = 6673781350103711733L;

	/**
	 * Variables originally only used in JAVA file.
	 */
	
	protected String formChildTagNames = "";
	
	protected String containerChildTagNames = "";
	
	/**
	 * Variables originally used in ZUL file.
	 */
	
	protected VView vView = CDesktopComponents.vView();

	protected SSpringHelper sSpringHelper = new SSpringHelper();

	protected int currentFormIndex = 0;
	
	protected static final String includePath = "../";
	
	protected static final String stylePath = "../style/";

	protected boolean showFeedback = false;

	protected boolean showScore = false;

	protected List<Hashtable<String,Object>> dataElements;
	
	/**
	 * Methods originally only used in JAVA file.
	 */

	/**
	 * Instantiates a new c run forms.
	 */
	public CRunForms() {
		super();
	}

	/**
	 * Instantiates a new c run forms.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the case component
	 */
	public CRunForms(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
	}

	/**
	 * Creates title area, content area to show tab structure,
	 * and buttons area with close button.
	 */
	@Override
	protected void createComponents() {
		CRunVbox lVbox = new CRunVbox();

		createTitleArea(lVbox);
		createContentArea(lVbox);
		createButtonsArea(lVbox);

		appendChild(lVbox);
	}

	/**
	 * Creates new content component.
	 *
	 * @param aId the a id
	 *
	 * @return the component
	 */
	protected Component newContentComponent(String aId) {
		HtmlMacroComponent lView = new CDefMacro();
		lView.setId(aId);
		//NOTE Set width and height to 0, because they default are 100%, which might cause problems when rendering an macro, because it horizontally becomes to wide.
		lView.setWidth("0");
		lView.setHeight("0");
		lView.setVisible(false);
		return lView;
	}

	/**
	 * Creates buttons area and adds close button.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run hbox
	 */
	@Override
	protected CRunHbox createButtonsArea(Component aParent) {
		CRunHbox lButtonsHbox = super.createButtonsArea(aParent);
		createCloseButton(lButtonsHbox);
		return lButtonsHbox;
	}

	/**
	 * Gets the run component helper.
	 *
	 * @return the run component helper
	 */
	@Override
	public CRunComponentHelper getRunComponentHelper() {
		return new CRunComponentHelper(null, "form", caseComponent, this);
	}

	/**
	 * Gets form tags.
	 *
	 * @return the form tags
	 */
	public List<IXMLTag> getFormTags() {
		return getPresentChildTags(getDataPlusStatusRootTag().getChild(AppConstants.contentElement), "form");
	}

	/**
	 * Gets piece and refpiece tags.
	 *
	 * @param aParentTag the a parent tag
	 * 
	 * @return the piece and refpiece tags
	 */
	public List<IXMLTag> getPieceAndRefpieceTags(IXMLTag aParentTag) {
		if (aParentTag == null) {
			return new ArrayList<IXMLTag>();
		}
		return aParentTag.getChilds("piece,refpiece");
	}
	
	/**
	 * Gets container tags.
	 *
	 * @param aParentTag the a parent tag
	 * 
	 * @return the container tags
	 */
	public List<IXMLTag> getContainerTags(IXMLTag aParentTag) {
		return getPresentChildTags(aParentTag, "container");
	}
	
	/**
	 * Gets all container tags.
	 *
	 * @param aParentTag the a parent tag
	 * 
	 * @return the container tags
	 */
	public List<IXMLTag> getAllContainerTags(IXMLTag aParentTag) {
		return aParentTag.getChilds("container");
	}
	
	/**
	 * Gets item tags.
	 *
	 * @param aParentTag the a parent tag
	 * 
	 * @return the item tags
	 */
	public List<IXMLTag> getItemTags(IXMLTag aParentTag) {
		return getPresentChildTags(aParentTag, formChildTagNames);
	}
	
	/**
	 * Gets all item tags, that is also tags that are not present.
	 *
	 * @param aParentTag the a parent tag
	 * @param aChildTagNames the a child tag names, comma separated
	 * 
	 * @return the item tags
	 */
	public List<IXMLTag> getAllItemTags(IXMLTag aParentTag, String aChildTagNames) {
		return aParentTag.getChilds(aChildTagNames);
	}
	
	/**
	 * Gets all item tags, that is also tags that are not present.
	 *
	 * @param aParentTag the a parent tag
	 * 
	 * @return the item tags
	 */
	public List<IXMLTag> getAllItemTags(IXMLTag aParentTag) {
		return getAllItemTags(aParentTag, formChildTagNames);
	}
	
	/**
	 * Get triggered feedback condition tag. aParentTags are possible parents of feedback condition tags.
	 *
	 * @return the feedback condition tag
	 */
	public IXMLTag getTriggeredFeedbackConditionTag() {
		return super.getTriggeredFeedbackConditionTag(getFormTags());
	}

	/**
	 * Get triggered feedback condition tag. aFormTag is possible parent of feedback condition tags.
	 *
	 * @param aFormTag the a form tag
	 *
	 * @return the feedback condition tag
	 */
	public IXMLTag getTriggeredFeedbackConditionTag(IXMLTag aFormTag) {
		List<IXMLTag> lFormTags = new ArrayList<IXMLTag>();
		lFormTags.add(aFormTag);
		return super.getTriggeredFeedbackConditionTag(lFormTags);
	}

	/**
	 * Increase number of attempts.
	 *
	 * @param aTag the a tag
	 *
	 * @return number of attempts
	 */
	public int increaseNumberOfAttempts(IXMLTag aTag) {
		int lNumberOfAttempts = 0;
		try {
			lNumberOfAttempts = Integer.parseInt(aTag.getCurrentStatusAttribute(AppConstants.statusKeyNumberofattempts));
		} catch (NumberFormatException e) {
			lNumberOfAttempts = 0;
		}
		lNumberOfAttempts++;
		setRunTagStatus(getCaseComponent(), aTag, AppConstants.statusKeyNumberofattempts, "" + lNumberOfAttempts, true);
		return lNumberOfAttempts;
	}

	/**
	 * Show feedback.
	 *
	 * @return the feedback condition tag
	 */
	public IXMLTag showFeedback() {
		return showFeedback(getFormTags());
	}

	/**
	 * Show feedback.
	 *
	 * @param aFormTag the a form tag
	 *
	 * @return the feedback condition tag
	 */
	public IXMLTag showFeedback(IXMLTag aFormTag) {
		List<IXMLTag> lFormTags = new ArrayList<IXMLTag>();
		lFormTags.add(aFormTag);
		return showFeedback(lFormTags);
	}

	/**
	 * Show feedback.
	 *
	 * @param aFormTags the a form tags
	 *
	 * @return the feedback condition tag
	 */
	public IXMLTag showFeedback(List<IXMLTag> aFormTags) {
		IXMLTag lFeedbackConditionTag = super.showFeedback(aFormTags);
	  	if (lFeedbackConditionTag != null) {
		  	//NOTE score -1 means 'no relevant score yet' because tool instruction is shown, so don'nt increase number of attempts.
		  	if (!lFeedbackConditionTag.getChildValue("score").equals("-1")) {
				//NOTE increasing number of attempts for current form tag if it is visible
		  		IXMLTag lCurrentFormTag = getFormTag(lFeedbackConditionTag);
				List<IXMLTag> lFormTags = getPresentChildTags(getDataPlusStatusRootTag().getChild(AppConstants.contentElement), "form");
				for (IXMLTag lFormTag : lFormTags) {
					if (lFormTag == lCurrentFormTag) {
						increaseNumberOfAttempts(lFormTag);
					}
				}
		  	}
	  	}
		return lFeedbackConditionTag;
	}

	/**
	 * Hide feedback.
	 */
	public void hideFeedback() {
		super.hideFeedback();
	}

	@Override
	public void cleanup() {
		// if conversations exists, remove it
		removeFeedbackConversations();
		super.cleanup();
	}

	/**
	 * Methods originally used in ZUL file.
	 */

	public void onReady(Event aEvent) {
		Events.postEvent("onShowFeedback", this, null);
	}

	public void onShowFeedback(Event aEvent, String aItemIdPrefix, String aItemUpdateEvent) {
		if (showFeedback) {
			IXMLTag triggeredFeedbackConditionTag = showFeedback();
			//NOTE score -1 means 'no relevant score yet' because tool instruction is shown, so feedback mechanism is misused to show reactions of npc's.
			//TODO rename feedback in reaction in Java and zul
			if (triggeredFeedbackConditionTag != null) {
				if (!sSpringHelper.getTagChildValue(triggeredFeedbackConditionTag, "score", "-1").equals("-1")) {
					List<Component> items = vView.getComponentsByPrefix(aItemIdPrefix);
					for (Component item : items) {
						Events.postEvent(aItemUpdateEvent, item, null);
					}
				}
				for (Hashtable<String,Object> dataElement : dataElements) {
					//for each form
					Events.postEvent("onUpdate", vView.getComponent("feedbackPieces_" + ((IXMLTag)dataElement.get("tag")).getAttribute("id")), triggeredFeedbackConditionTag);
				}
			}
		}
		else {
			showDefaultFeedback();
		}
	}

	public void onHideFeedback(Event aEvent) {
		//NOTE Don't hide feedback, because users can close it themselves
		//hideFeedback();
	}

	public void onHandleStatusChange(Event aEvent, String aItemIdPrefix) {
		STriggeredReference triggeredReference = (STriggeredReference)aEvent.getData();
		if (triggeredReference != null) {
			if (triggeredReference.getDataTag() != null &&
				(triggeredReference.getDataTag().getName().equals("piece") || triggeredReference.getDataTag().getName().equals("refpiece")) &&
				triggeredReference.getStatusKey().equals(AppConstants.statusKeyPresent)) {
				Component lComponent = CDesktopComponents.vView().getComponent(aItemIdPrefix + triggeredReference.getDataTag().getAttribute(AppConstants.keyId));
				if (lComponent != null) {
					lComponent.setVisible(triggeredReference.getStatusValue().equals(AppConstants.statusValueTrue));
				}
			}
		}
	}

	public void handlepieceOrRefpieces(List<Hashtable<String,Object>> pieceOrRefpieceDataElementsList, List<IXMLTag> pieceOrRefpieceTags) {
		for (int i = 0; i < pieceOrRefpieceTags.size(); i++) {
			IXMLTag pieceOrRefpieceTag = pieceOrRefpieceTags.get(i);
			Hashtable<String,Object> hPieceOrRefpieceDataElements = new Hashtable<String,Object>();
			hPieceOrRefpieceDataElements.put("tag", pieceOrRefpieceTag);
			hPieceOrRefpieceDataElements.put("value", sSpringHelper.getTagChildValue(pieceOrRefpieceTag, "name", ""));
			if (pieceOrRefpieceTag.getName().equals("piece")) {
				hPieceOrRefpieceDataElements.put("srctag", pieceOrRefpieceTag);
			}
			else {
				hPieceOrRefpieceDataElements.put("srctag", sSpringHelper.getReferencedTag(getCaseComponent(), pieceOrRefpieceTag, "refpiece"));
			}
			hPieceOrRefpieceDataElements.put("present", sSpringHelper.getCurrentStatusTagStatusChildAttribute(pieceOrRefpieceTag, AppConstants.statusKeyPresent, "true"));
			hPieceOrRefpieceDataElements.put("styleposition", runWnd.getStylePosition(pieceOrRefpieceTag) + runWnd.getStyleSize(pieceOrRefpieceTag));
			pieceOrRefpieceDataElementsList.add(hPieceOrRefpieceDataElements);
		}
	}

	public String getStyleTitleWidth(IXMLTag tag) {
		String styleTitleWidth = "";
		String[] size = sSpringHelper.getTagChildValue(tag, "size", "").split(",");
		if (size.length == 2) {
			int width = runWnd.getNumber(size[0], -1);
			if (width != -1) {
				styleTitleWidth += "width:" + (width - 20) + "px;";
			}
		}
		return styleTitleWidth;
	}

	public String getContainerTagId(IXMLTag tag) {
		while (tag != null && !tag.getName().equals("container")) {
			tag = tag.getParentTag();
		}
		if (tag != null) {
			return tag.getAttribute("id");
		}
		return "";
	}

	public IXMLTag getFormTag(IXMLTag tag) {
		while (tag != null && !tag.getName().equals("form")) {
			tag = tag.getParentTag();
		}
		return tag;
	}

	public IXMLTag getContainerTag(IXMLTag tag) {
		while (tag != null && !tag.getName().equals("container")) {
			tag = tag.getParentTag();
		}
		return tag;
	}

	public boolean isShowIfRightOrWrong(IXMLTag tag) {
		// get form tag
		IXMLTag formTag = getFormTag(tag);
		if (formTag == null) {
			return false;
		}
		int maxNumber = runWnd.getNumber(sSpringHelper.getTagChildValue(formTag, "maxnumberofattempts", "-1"), -1);
		int number = runWnd.getNumber(sSpringHelper.getCurrentStatusTagStatusChildAttribute(formTag, AppConstants.statusKeyNumberofattempts, "0"), 0);
		return maxNumber > -1 && number >= maxNumber;
	}

	public boolean isHideRightWrong(IXMLTag tag) {
		if (containerDoNotShowRightWrong(tag)) {
			return true;
		}
		IXMLTag formTag = getFormTag(tag);
		return formTag == null || sSpringHelper.getTagChildValue(formTag, "showrightwrongonchange", "false").equals("false");
	}

	public boolean containerDoNotShowRightWrong(IXMLTag tag) {
		IXMLTag containerTag = getContainerTag(tag);
		if (containerTag == null) {
			return false;
		}
		return sSpringHelper.getTagChildValue(containerTag, "noshowrightwrong", "false").equals("true");
	}

}
