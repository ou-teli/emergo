/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.neoclassic;

import org.zkoss.zk.ui.Component;

import nl.surf.emergo.control.run.CRunArea;
import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.control.run.CRunHbox;

/**
 * The Class CRunTasksAreaNeoclassic is used to show the tasks area cover within the run choice area of the
 * Emergo player.
 */
public class CRunTasksAreaNeoclassic extends CRunArea {

	private static final long serialVersionUID = 2558557260315481382L;

	/** The run component. */
	protected CRunComponent runComponent = null;
	
	/**
	 * Instantiates a new c run tasks area.
	 * Creates choice area cover.
	 */
	public CRunTasksAreaNeoclassic() {
		super();
		setId("runTasksArea");
		runComponent = null;
		init();
	}

	/**
	 * Instantiates a new c run note.
	 * Creates title area with close box and edit field for note.
	 *
	 * @param aId the a id
	 * @param aRunComponent the a run component
	 */
	public CRunTasksAreaNeoclassic(String aId, CRunComponent aRunComponent) {
		super();
		setId(aId);
		runComponent = aRunComponent;
		init();
	}

	/**
	 * Sets run component.
	 */
	public void setRunComponent(CRunComponent aRunComponent) {
		runComponent = aRunComponent;
	}

	/**
	 * Creates tasks area to cover choise area.
	 */
	public void init() {
		String lType = "";
		if (runWnd != null)
			lType = ((CRunWndNeoclassic)runWnd).getPlayerStatusString("ChoiceAreaLayoutStatus", null);
		setZclass(getClassName() + lType);

		createTitleArea(this);
	}

	/**
	 * Creates title area, a title label and close box.
	 *
	 * @param aParent the a parent
	 */
	protected void createTitleArea(Component aParent) {
		CRunHbox lSuperHbox = new CRunHbox();
		aParent.appendChild(lSuperHbox);
	}

}
