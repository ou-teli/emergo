/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.definegoals;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Image;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Textbox;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefHelper;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitSkillWheelLevel2Div;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitSubStepsDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitSubSubStepDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefTextbox;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTag;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitFeedbackTipOrTop;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.view.VView;

public class CRunPVToolkitDefineGoalsDiv extends CRunPVToolkitSubSubStepDiv {

	private static final long serialVersionUID = -7740166765787060198L;

	protected IECaseComponent caseComponent;
	
	protected boolean _skillclusterHasTipsTops;
	protected boolean _subskillHasTipsTops;
	protected boolean _skillHasTipsTops;
	
	protected String _stepPrefix = "defineGoals";
	protected String _idPrefix = "defineGoals";
	protected String _classPrefix = "defineGoals";
	
	@Override
	public void init(IERunGroup actor, boolean reset, boolean editable, String subStepType) {
		setClass(_classPrefix);
		super.init(actor, reset, editable, subStepType);
		setVisible(true);
		_initialized = true;
	}
	
	@Override
	public void reset() {
	}
	
	@Override
	public void update() {
		getChildren().clear();
		
		pvToolkit.setMemoryCaching(true);
		
		caseComponent = pvToolkit.getCurrentRubricsCaseComponent();

		String feedbackLevelOfTipsAndTops = pvToolkit.getFeedbackLevelOfTipsAndTops(pvToolkit.getCurrentMethodTag(), pvToolkit.getCurrentCycleTag());
		boolean mayTipsAndTopsSkill = pvToolkit.getMayTipsAndTopsSkill(pvToolkit.getCurrentMethodTag(), pvToolkit.getCurrentCycleTag());
		
		_skillclusterHasTipsTops = feedbackLevelOfTipsAndTops.equals("skillcluster");
		_subskillHasTipsTops = feedbackLevelOfTipsAndTops.equals("subskill");
		_skillHasTipsTops = feedbackLevelOfTipsAndTops.equals("skill") || mayTipsAndTopsSkill;

		Div div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{"titleRight"}
		);
		
		CRunPVToolkitSubStepsDiv runPVToolkitSubStepsDiv = (CRunPVToolkitSubStepsDiv)CDesktopComponents.vView().getComponent("defineGoalsSubStepsDiv");
		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "value"}, 
				new Object[]{"font titleRight", (String)runPVToolkitSubStepsDiv.getAttribute("divTitle")}
		);
		
		div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "Div"}
		);

		
		if (pvToolkit.getCurrentCycleNumber() == 1) {
			Textbox textbox = new CRunPVToolkitDefTextbox(div, 
					new String[]{"id", "class", "rows", "width", "text"},
					new Object[]{_idPrefix + "Textbox", "font " + _classPrefix + "Textbox", "24", "500px", getGoals()}
					);
			addDefineGoalsTextOnBlurEventListener(textbox);
		}
		else if (pvToolkit.getCurrentCycleNumber() > 1) {
			new CRunPVToolkitDefLabel(div, 
					new String[]{"class", "labelKey"},
					new Object[]{"font " + _classPrefix + "TitlePreviousCycle", "PV-toolkit-defineGoals.header.previousGoals"}
					);
			Textbox textbox = new CRunPVToolkitDefTextbox(div, 
					new String[]{"class", "rows", "width", "text"},
					new Object[]{"font " + _classPrefix + "TextboxPreviousCycle", "11", "500px", getPreviousGoals()}
					);
			textbox.setDisabled(true);
			new CRunPVToolkitDefLabel(div, 
					new String[]{"class", "labelKey"},
					new Object[]{"font " + _classPrefix + "TitleCurrentCycle", "PV-toolkit-defineGoals.header.currentGoals"}
					);
			textbox = new CRunPVToolkitDefTextbox(div, 
					new String[]{"id", "class", "rows", "width", "text"},
					new Object[]{_idPrefix + "Textbox", "font " + _classPrefix + "TextboxCurrentCycle", "11", "500px", getGoals()}
					);
			addDefineGoalsTextOnBlurEventListener(textbox);
		}

		Button saveButton = new CRunPVToolkitDefButton(div,
				new String[]{"class", "cLabelKey"}, 
				new Object[]{"font pvtoolkitButton " + _classPrefix + "SaveButton", "PV-toolkit.save"}
				);
		addDefineGoalsSaveButtonOnClickEventListener(saveButton, this);

		Image image = new CRunPVToolkitDefImage(div,
				new String[]{"class", "src"}, 
				new Object[]{_classPrefix + "SkillWheelImage", zulfilepath + "icon_skillwheel.svg"}
		);
		addForwardToSkillWheelOnClickEventListener(image, getId());
		
		Div tipsTopsDiv = new CRunPVToolkitDefDiv(div, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "TipsTopsDiv"}
		);
		
		Rows rows = appendTipsTopsGrid(tipsTopsDiv);
		int rowNumber = 1;
		rowNumber = appendTipsOrTopsToGrid(rows, "tops", rowNumber);
		rowNumber = appendTipsOrTopsToGrid(rows, "tips", rowNumber);
		
		Button btn = new CRunPVToolkitDefButton(this, 
				new String[]{"class", "cLabelKey"}, 
				new Object[]{"font pvtoolkitButton " + _classPrefix + "DetailsButton", "PV-toolkit.details"}
		);
		addDetailsButtonOnClickEventListener(btn, getId());

		btn = new CRunPVToolkitDefButton(this, 
				new String[]{"class", "cLabelKey"}, 
				new Object[]{"font pvtoolkitButton subStepReadyButton", "PV-toolkit.ready"}
		);
		addBackToDefineGoalsSubStepssOnClickEventListener(btn, _stepPrefix + "SubStepDiv");

		pvToolkit.setMemoryCaching(false);
		
	}

	protected void addDefineGoalsTextOnBlurEventListener(Component component) {
		component.addEventListener("onBlur", new EventListener<Event>() {
			public void onEvent(Event event) {
				saveGoals();
			}
		});
	}

	protected void addDefineGoalsSaveButtonOnClickEventListener(Component component, Component notifyComponent) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				saveGoals();
				VView vView = CDesktopComponents.vView();
				vView.showMessagebox(getRoot(), vView.getCLabel("PV-toolkit-defineGoals.notification.saved.confirmation"), vView.getCLabel("PV-toolkit-defineGoals.notification.saved"), Messagebox.OK, Messagebox.INFORMATION);
			}
		});
	}

	protected String getPreviousGoals() {
		if (pvToolkit.getCurrentCycleNumber() == 1) {
			return "";
		}
		List<IXMLTag> previousCurrentStepTags = pvToolkit.getStepTagsInPreviousCycles(pvToolkit.getCurrentStepTag(), pvToolkit.getCurrentStepTag().getChildValue("steptype"));
		return pvToolkit.getGoals(_actor, previousCurrentStepTags.get(previousCurrentStepTags.size() - 1));
	}

	protected String getGoals() {
		return pvToolkit.getGoals(_actor, pvToolkit.getCurrentStepTag());
	}

	public void saveGoals() {
		pvToolkit.saveGoals(_actor, pvToolkit.getCurrentStepTag(), ((Textbox)CDesktopComponents.vView().getComponent(_idPrefix + "Textbox")).getValue());
	}

	protected void addForwardToSkillWheelOnClickEventListener(Component component, String fromComponentId) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				Component fromComponent = CDesktopComponents.vView().getComponent(fromComponentId);
				if (fromComponent != null) {
					fromComponent.setVisible(false);
				}
				CRunPVToolkitSkillWheelLevel2Div skillWheel = (CRunPVToolkitSkillWheelLevel2Div)CDesktopComponents.vView().getComponent(_stepPrefix + "SkillWheelLevel2Div");
				if (skillWheel != null) {
					skillWheel.init(_actor, false, 0, 0);
				}
			}
		});
	}

	protected void addDetailsButtonOnClickEventListener(Component component, String fromComponentId) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				Component fromComponent = CDesktopComponents.vView().getComponent(fromComponentId);
				if (fromComponent != null) {
					fromComponent.setVisible(false);
				}
				CRunPVToolkitDefineGoalsOverviewTipsTopsDiv overviewTipsTopsDiv = (CRunPVToolkitDefineGoalsOverviewTipsTopsDiv)CDesktopComponents.vView().getComponent(_stepPrefix + "OverviewTipsTopsDiv");
				if (overviewTipsTopsDiv != null) {
					overviewTipsTopsDiv.init(_actor, false, _editable, _subStepType);
				}
			}
		});
	}

	protected void addBackToDefineGoalsSubStepssOnClickEventListener(Component component, String fromComponentId) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				saveGoals();
				Component fromComponent = CDesktopComponents.vView().getComponent(fromComponentId);
				if (fromComponent != null) {
					fromComponent.setVisible(false);
				}
				CRunPVToolkitSubStepsDiv toComponent = (CRunPVToolkitSubStepsDiv)CDesktopComponents.vView().getComponent(_stepPrefix + "SubStepsDiv");
				if (toComponent != null) {
					toComponent.update();
					toComponent.setVisible(true);
				}
			}
		});
	}

	protected Rows appendTipsTopsGrid(Component parent) {
    	return pvToolkit.appendGrid(
    			parent, 
    			getId() + "Grid",
    			"position:absolute;left:0px;top:0px;width:510px;height:300px;overflow:auto;", 
    			new boolean[] {true, true, _skillclusterHasTipsTops, _subskillHasTipsTops, _skillHasTipsTops},
    			new String[] {"6%", "64%", "30%", "30%", "30%"}, 
    			new boolean[] {true, true, true, true, true},
    			null,
    			"PV-toolkit-skillwheel.column.label.", 
    			new String[] {"", "tiptop", "skillcluster", "subskill", "skill"});
	}

	protected int appendTipsOrTopsToGrid(Rows rows, String tipsOrTops, int rowNumber) {
		return appendTipsOrTopsToGrid(
				rows, 
				tipsOrTops, 
				rowNumber,
				new boolean[] {true, true, false, false, false, _skillclusterHasTipsTops, _subskillHasTipsTops, _skillHasTipsTops} 
				);
	}

	protected int appendTipsOrTopsToGrid(
			Rows rows, 
			String tipsOrTops, 
			int rowNumber,
    		boolean[] showColumn 
			) {
		List<List<IXMLTag>> sharedFeedbackTagsPerFeedbackStep = pvToolkit.getSharedFeedbackTagsPerFeedbackStep(_actor, false, null, null);

		String tipsOrTopsImage = "";
		if (tipsOrTops.equals("tops")) {
			tipsOrTopsImage = "thumbs-up-gray.svg";
		}
		else if (tipsOrTops.equals("tips")) {
			tipsOrTopsImage = "map-marker-exclamation-gray.svg";
		}
		for (CRunPVToolkitFeedbackTipOrTop feedback : pvToolkit.getPerformanceTipsOrTops(_actor, pvToolkit.getCurrentSkillTag(), sharedFeedbackTagsPerFeedbackStep, null, null, true, pvToolkit.getCurrentCycleNumber(), tipsOrTops, false)) {
			if (feedback.isMark()) {
				Row row = new Row();
				rows.appendChild(row);
				
				int columnNumber = 0;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					CRunPVToolkitDefImage image = new CRunPVToolkitDefImage(row, 
							new String[]{"class", "src"}, 
							new Object[]{"gridImage", zulfilepath + tipsOrTopsImage}
							);
					//NOTE if ordered tops will be shown on top
					if (tipsOrTops.equals("tops")) {
						image.setAttribute("orderValue", "1");
					}
					else if (tipsOrTops.equals("tips")) {
						image.setAttribute("orderValue", "2");
					}
				}
	
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					new CRunPVToolkitDefLabel(row, 
							new String[]{"class", "value"}, 
							new Object[]{"font gridLabel", feedback.getValue()}
							);
				}
	
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					new CRunPVToolkitDefLabel(row, 
							new String[]{"class", "value"}, 
							new Object[]{"font gridLabel", feedback.getFeedbackName()}
							);
				}
				
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					new CRunPVToolkitDefLabel(row, 
							new String[]{"class", "value"}, 
							new Object[]{"font gridLabel", feedback.getFeedbackRole()}
							);
				}
				
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					CRunPVToolkitDefLabel label = new CRunPVToolkitDefLabel(row, 
							new String[]{"class", "value"}, 
							new Object[]{"font gridLabel", feedback.getShareDate()}
							);
					//NOTE for ordering use ymd format
					if (!StringUtils.isEmpty(feedback.getShareDate())) {
						Date date = CDefHelper.getDateFromStrDMY(feedback.getShareDate());
						label.setAttribute("orderValue", CDefHelper.getDateStrAsYMD(date));
					}
				}
				
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					String value = "";
					if (_skillclusterHasTipsTops && feedback.getSkillClusterTag() != null) {
						value = pvToolkit.getTagChildValue(new CRunPVToolkitCacAndTag(caseComponent, feedback.getSkillClusterTag()), "name");
					}
					new CRunPVToolkitDefLabel(row, 
							new String[]{"class", "value"}, 
							new Object[]{"font gridLabel", value}
							);
				}
				
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					String value = "";
					if (_subskillHasTipsTops && feedback.getSubSkillTag() != null) {
						value = pvToolkit.getTagChildValue(new CRunPVToolkitCacAndTag(caseComponent, feedback.getSubSkillTag()), "name");
					}
					new CRunPVToolkitDefLabel(row, 
							new String[]{"class", "value"}, 
							new Object[]{"font gridLabel", value}
							);
				}
				
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					String value = "";
					if (_skillHasTipsTops && feedback.getSkillTag() != null && feedback.getSkillClusterTag() == null && feedback.getSubSkillTag() == null) {
						value = pvToolkit.getTagChildValue(new CRunPVToolkitCacAndTag(caseComponent, feedback.getSkillTag()), "name");
					}
					new CRunPVToolkitDefLabel(row, 
							new String[]{"class", "value"}, 
							new Object[]{"font gridLabel", value}
							);
				}
				
				rowNumber ++;
			}
		}
		return rowNumber;
	}

}
