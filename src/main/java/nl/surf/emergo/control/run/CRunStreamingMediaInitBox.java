/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.ArrayList;
import java.util.List;

import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefBox;
import nl.surf.emergo.view.VView;

public class CRunStreamingMediaInitBox extends CDefBox {

	private static final long serialVersionUID = -8567314649051670101L;

	protected VView vView = CDesktopComponents.vView();
	protected CControl cControl = CDesktopComponents.cControl();

	protected String url = "";
	protected String html5url = "";
	protected List<List<String>> streamingurls = new ArrayList<List<String>>();

	protected void init() {
		url = vView.getUrlEncodedFileName((String)VView.getParameter("url", this, ""));
		html5url = vView.getHtml5StreamingUrl(url);
		streamingurls = vView.getHtml5StreamingUrls(url);
		if(!url.equals(""))
			url = vView.uniqueView(url);
		if(!html5url.equals(""))
			html5url = vView.uniqueView(html5url);
	}

}
