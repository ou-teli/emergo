/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.practice;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoField;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkmax.zul.Camera;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Image;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Progressmeter;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Timer;

import com.github.kokorin.jaffree.StreamType;
import com.github.kokorin.jaffree.ffmpeg.FFmpeg;
import com.github.kokorin.jaffree.ffmpeg.FFmpegProgress;
import com.github.kokorin.jaffree.ffmpeg.FFmpegResultFuture;
import com.github.kokorin.jaffree.ffmpeg.NullOutput;
import com.github.kokorin.jaffree.ffmpeg.ProgressListener;
import com.github.kokorin.jaffree.ffmpeg.UrlInput;
import com.github.kokorin.jaffree.ffmpeg.UrlOutput;
import com.github.kokorin.jaffree.ffprobe.FFprobe;
import com.github.kokorin.jaffree.ffprobe.FFprobeResult;
import com.github.kokorin.jaffree.ffprobe.Stream;

import nl.surf.emergo.business.IBlobManager;
import nl.surf.emergo.business.IFileManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitInitBox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefCombobox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefComboitem;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefHbox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefProgressmeter;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefTextbox;
import nl.surf.emergo.control.run.cases.PVtoolkit.util.CRunPVToolkitCustomAttributeHelper;
import nl.surf.emergo.domain.IEBlob;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.utilities.PropsValues;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpring;

public class CRunPVToolkitPracticeNewRecording extends CDefDiv {

	private static final long serialVersionUID = 1657198753508061187L;

	protected VView vView = CDesktopComponents.vView();
	protected SSpring sSpring = CDesktopComponents.sSpring();

	protected String zulfilepath = ((CRunPVToolkitInitBox) vView.getComponent("PV-toolkit_initBox")).getZulfilepath();

	protected String _recordingState = "";

	protected String _recordingBlobId = "";

	protected int _webcamRecordingLength = -1;

	protected CRunPVToolkit pvToolkit;

	protected String _idPrefix = "practiceNewRecording";
	protected String _classPrefix = "practiceNewRecording";
	protected String _logPrefix = "PV-Tool video recording: ";

	protected Boolean locWebcamRecordingStarted;
	protected String locWebcamRecordVideoCodec;
	protected String locWebcamRecordAudioCodec;
	protected Boolean locConvertRecording;
	protected String locWebcamRecordVideoLibrary;
	protected String locWebcamRecordAudioLibrary;
	protected String locWebcamRecordVideoBitrate;
	protected String locWebcamRecordAudioBitrate;
	protected Integer locWebcamRecordMaxBitrate;
	protected Boolean locWebcamCheckSync;
	protected Integer locWebcamSyncDiff;
	protected Boolean locWebcamCheckRotation;

	protected ProgressListener _ProgListener;
	protected int _ProgressPercent;
	protected boolean _StopPWProgressTimer;
	protected boolean _PWProgressTimerFinished;

	protected String _FFOutUrl;
	protected FFprobe _FFProbe;
	protected FFmpegResultFuture _FFmpegResult;
	protected String _ConvertedRecordingName;
	protected boolean _IsWebcamRecording;
	protected String _ShowRecordingName;
	protected CRunPVToolkitDefLabel _PWLoadLabel;
	protected CRunPVToolkitDefLabel _PWProgLabel;

	protected String _progressState = "";
	protected String _progressStateUpload = "upload";
	protected String _progressStateConvert = "convert";
	protected int _progressTimeInSecs = 0;
	protected int _progressUploadLength = 0;

	public void onCreate(CreateEvent aEvent) {
		_idPrefix = CRunPVToolkitCustomAttributeHelper.handleCustomAttribute(this, "_idPrefix", _idPrefix);
		_classPrefix = CRunPVToolkitCustomAttributeHelper.handleCustomAttribute(this, "_classPrefix", _classPrefix);
	}

	public int getMaxWebcamRecordingLength() {
		return pvToolkit.getMaxWebcamRecordingLength();
	}

	public String getRecordingState() {
		return _recordingState;
	}

	public void init(String recordingState) {
		_recordingState = recordingState;

		_recordingBlobId = "";
		_webcamRecordingLength = -1;

		pvToolkit = (CRunPVToolkit) CDesktopComponents.vView().getComponent("pvToolkit");

		update();
	}

	protected void removeSpecificComponents(String divId) {
		// NOTE put all elements in a specific div and if it exists remove it.
		// Other elements are defined in the ZUL file and may not be removed
		Div specificDiv = (Div) vView.getComponent(divId);
		if (specificDiv != null) {
			specificDiv.detach();
		}
	}

	public void update() {

		vView.getComponent(_idPrefix + "Video").setVisible(false);

		// NOTE put all elements in a specific div and if it exists remove it.
		// Other elements are defined in the ZUL file and may not be removed
		String divId = getId() + "SpecificDiv";
		removeSpecificComponents(divId);
		Div specificDiv = new CRunPVToolkitDefDiv(this, new String[] { "id" }, new Object[] { divId });

		new CRunPVToolkitDefImage(specificDiv, new String[] { "class" }, new Object[] { "popupBackground" });

		new CRunPVToolkitDefImage(specificDiv, new String[] { "class", "src" },
				new Object[] { _classPrefix + "Background", zulfilepath + "popup-large-background.svg" });

		Image closeImage = new CRunPVToolkitDefImage(specificDiv, new String[] { "class", "src" },
				new Object[] { _classPrefix + "CloseButton", zulfilepath + "close.svg" });
		addCloseButtonOnClickEventListener(closeImage);

		Div div = new CRunPVToolkitDefDiv(specificDiv, new String[] { "class" }, new Object[] { _classPrefix + "Div" });

		if (_recordingState.equals("webcam")) {
			addWebcamDiv(div);
		} else if (_recordingState.equals("upload")) {
			addUploadDiv(div);
		}

		addPleaseWaitDiv(div);

		addRecordingGiveNameDiv(div);
		addViewRecordingDiv(div, _recordingState.equals("webcam"));
		addSaveRecordingDiv(div);

	}

	protected void addCloseButtonOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			@Override
			public void onEvent(Event event) {

				if (!StringUtils.isEmpty(_recordingBlobId)) {
					boolean isGivingName = vView.getComponent(_idPrefix + "GiveNameDiv").isVisible();
					String labelKey = "PV-toolkit-practice.cancel.confirm";
					if (!isGivingName) {
						labelKey += "1";
					} else {
						labelKey += "2";
					}
					if (vView.showMessagebox(getRoot(), vView.getLabel(labelKey),
							vView.getLabel("PV-toolkit-practice.cancel"), Messagebox.YES + Messagebox.NO,
							Messagebox.QUESTION) != Messagebox.YES) {
						sSpring.getSLogHelper().logAction(_logPrefix + "webcam: recording cancelled");
						return;
					}
				}

				Camera camera = (Camera) vView.getComponent(_idPrefix + "CameraCamera");
				if (camera != null) {
					camera.stop();
					camera.stopStreaming();
					sSpring.getSLogHelper().logAction(_logPrefix + "webcam: recording aborted");
				}
				pvToolkit.clearVideo(_idPrefix);

				if (!StringUtils.isEmpty(_recordingBlobId)) {
					pvToolkit.deleteVideoBlob(_recordingBlobId);
					sSpring.getSLogHelper().logAction(_logPrefix + "webcam/upload: blob deleted: " + _recordingBlobId);
					_recordingBlobId = "";
					_webcamRecordingLength = -1;
				}

				Component practiceNewRecording = vView.getComponent(_idPrefix);
				if (practiceNewRecording != null) {
					removeSpecificComponents(getId() + "SpecificDiv");
					practiceNewRecording.setVisible(false);
				}
			}
		});
	}

	public void addWebcamDiv(Component parent) {
		Div divWebcam = new CRunPVToolkitDefDiv(parent, new String[] { "id" },
				new Object[] { _idPrefix + "WebcamDiv" });

		CRunPVToolkitPracticeNewRecordingCamera camera = new CRunPVToolkitPracticeNewRecordingCamera();
		divWebcam.appendChild(camera);
		camera.setId(_idPrefix + "CameraCamera");
		camera.setClass(_classPrefix + "CameraCamera");
		camera.init(_idPrefix, _classPrefix);

		Div divStartRecording = new CRunPVToolkitDefDiv(divWebcam, new String[] { "id" },
				new Object[] { _idPrefix + "StartRecordingDiv" });

		Button btn = new CRunPVToolkitDefButton(divStartRecording, new String[] { "id", "class", "labelKey" },
				new Object[] { _idPrefix + "StartRecordingButton",
						"font pvtoolkitButton " + _classPrefix + "StartRecordingButton",
						"PV-toolkit-practice.button.startRecording" });
		addStartRecordingButtonOnClickEventListener(btn);

		new CRunPVToolkitDefLabel(divStartRecording, new String[] { "class", "labelKey" },
				new Object[] { "font " + _classPrefix + "TimerTitle", "PV-toolkit-practice.header.timer" });

		Combobox combobox = new CRunPVToolkitDefCombobox(divStartRecording, new String[] { "id", "width" },
				new Object[] { _idPrefix + "TimerCombobox", "50px" });
		combobox.setClass("font " + _classPrefix + "TimerCombobox");
		int[] timerTimes = new int[] { 0, 2, 5, 10 };
		for (int i = 0; i < timerTimes.length; i++) {
			Comboitem comboitem = new CRunPVToolkitDefComboitem(combobox, new String[] { "value" },
					new Object[] { "" + timerTimes[i] });
			comboitem.setValue(timerTimes[i]);
			comboitem.setClass("font " + _classPrefix + "TimerComboitem");
		}
		combobox.setSelectedIndex(0);
		combobox.setReadonly(true);

		btn = new CRunPVToolkitDefButton(divWebcam, new String[] { "id", "class", "labelKey", "visible" },
				new Object[] { _idPrefix + "PauseRecordingButton",
						"font pvtoolkitButton " + _classPrefix + "PauseRecordingButton",
						"PV-toolkit-practice.button.pauseRecording", "false" });
		addPauseRecordingButtonOnClickEventListener(btn);

		btn = new CRunPVToolkitDefButton(divWebcam, new String[] { "id", "class", "labelKey", "visible" },
				new Object[] { _idPrefix + "ResumeRecordingButton",
						"font pvtoolkitButton " + _classPrefix + "ResumeRecordingButton",
						"PV-toolkit-practice.button.resumeRecording", "false" });
		addResumeRecordingButtonOnClickEventListener(btn);

		btn = new CRunPVToolkitDefButton(divWebcam, new String[] { "id", "class", "labelKey", "visible" },
				new Object[] { _idPrefix + "StopRecordingButton",
						"font pvtoolkitButton " + _classPrefix + "StopRecordingButton",
						"PV-toolkit-practice.button.stopRecording", "false" });
		addStopRecordingButtonOnClickEventListener(btn);

		if (pvToolkit.getMayChangeSeeSelf()) {
			boolean seeself = pvToolkit.getSeeSelf();
			btn = new CRunPVToolkitDefButton(divWebcam, new String[] { "id", "class", "labelKey", "visible" },
					new Object[] { _idPrefix + "MirrorOffButton",
							"font pvtoolkitButton " + _classPrefix + "MirrorOffButton",
							"PV-toolkit-practice.button.mirrorOff", "" + seeself });
			addMirrorOffButtonOnClickEventListener(btn);

			btn = new CRunPVToolkitDefButton(divWebcam, new String[] { "id", "class", "labelKey", "visible" },
					new Object[] { _idPrefix + "MirrorOnButton",
							"font pvtoolkitButton " + _classPrefix + "MirrorOnButton",
							"PV-toolkit-practice.button.mirrorOn", "" + !seeself });
			addMirrorOnButtonOnClickEventListener(btn);
		}

		Div timerDiv = new CRunPVToolkitDefDiv(divWebcam, new String[] { "id", "class", "visible" },
				new Object[] { _idPrefix + "TimerDiv", _classPrefix + "TimerDiv", "false" });

		Hbox timerHbox = new CRunPVToolkitDefHbox(timerDiv, null, null);

		Timer timerTimer = new Timer();
		timerHbox.appendChild(timerTimer);
		timerTimer.setId(_idPrefix + "TimerTimer");
		timerTimer.setDelay(1000);
		timerTimer.setRepeats(true);
		timerTimer.setRunning(false);
		addTimerTimerOnTimerEventListener(timerTimer);

		CRunPVToolkitDefProgressmeter timerMeter = new CRunPVToolkitDefProgressmeter();
		timerHbox.appendChild(timerMeter);
		timerMeter.setId(_idPrefix + "TimerMeter");
		timerMeter.setValue(0);
		timerMeter.setWidth("150px");

		if (getMaxWebcamRecordingLength() > 0) {
			Div progressDiv = new CRunPVToolkitDefDiv(divWebcam, new String[] { "id", "class", "visible" },
					new Object[] { _idPrefix + "ProgressDiv", _classPrefix + "ProgressDiv", "false" });

			Hbox progressHbox = new CRunPVToolkitDefHbox(progressDiv, null, null);

			Timer progressTimer = new CRunPVToolkitPracticeNewRecordingProgressTimer();
			progressHbox.appendChild(progressTimer);
			progressTimer.setId(_idPrefix + "ProgressTimer");
			progressTimer.setDelay(1000);
			progressTimer.setRepeats(true);
			progressTimer.setRunning(false);

			Progressmeter progressMeter = new CRunPVToolkitDefProgressmeter();
			progressHbox.appendChild(progressMeter);
			progressMeter.setId(_idPrefix + "ProgressMeter");
			progressMeter.setValue(0);
			progressMeter.setWidth("150px");
		}

	}

	protected void addStartRecordingButtonOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			@Override
			public void onEvent(Event event) {
				int timerTime = 0;
				Combobox timerCombobox = (Combobox) vView.getComponent(_idPrefix + "TimerCombobox");
				if (timerCombobox != null) {
					timerTime = timerCombobox.getSelectedItem() == null ? 0
							: timerCombobox.getSelectedItem().getValue();
				}
				if (timerTime > 0) {
					startTimer(timerTime);
				} else {
					startRecording();
				}
			}
		});
	}

	protected void addTimerTimerOnTimerEventListener(Component component) {
		component.addEventListener("onTimer", new EventListener<Event>() {
			@Override
			public void onEvent(Event event) {
				int timeInSecs = (int) component.getAttribute("timeInSecs");
				int maxTimeInSecs = (int) component.getAttribute("maxTimeInSecs");
				timeInSecs++;
				component.setAttribute("timeInSecs", timeInSecs);
				if (timeInSecs <= maxTimeInSecs) {
					((Progressmeter) CDesktopComponents.vView().getComponent(_idPrefix + "TimerMeter"))
							.setValue((100 * timeInSecs) / maxTimeInSecs);
				} else {
					((Timer) component).stop();
					startRecording();
				}
			}
		});
	}

	protected void addPauseRecordingButtonOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			@Override
			public void onEvent(Event event) {
				pauseRecording();
			}
		});
	}

	protected void addResumeRecordingButtonOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			@Override
			public void onEvent(Event event) {
				resumeRecording();
			}
		});
	}

	protected void addStopRecordingButtonOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			@Override
			public void onEvent(Event event) {
				stopRecording();
			}
		});
	}

	protected void addMirrorOffButtonOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			@Override
			public void onEvent(Event event) {
				mirrorOff();
			}
		});
	}

	protected void addMirrorOnButtonOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			@Override
			public void onEvent(Event event) {
				mirrorOn();
			}
		});
	}

	protected void startTimer(int timerTime) {
		vView.getComponent(_idPrefix + "StartRecordingDiv").setVisible(false);
		vView.getComponent(_idPrefix + "StopRecordingButton").setVisible(false);
		vView.getComponent(_idPrefix + "PauseRecordingButton").setVisible(false);
		vView.getComponent(_idPrefix + "ResumeRecordingButton").setVisible(false);

		vView.getComponent(_idPrefix + "TimerDiv").setVisible(true);
		vView.getComponent(_idPrefix + "TimerTimer").setAttribute("timeInSecs", 0);
		vView.getComponent(_idPrefix + "TimerTimer").setAttribute("maxTimeInSecs", timerTime);

		((Timer) vView.getComponent(_idPrefix + "TimerTimer")).start();
	}

	protected void setWebcamRecordingStarted(boolean pStarted) {
		locWebcamRecordingStarted = pStarted;
	}

	protected boolean getWebcamRecordingStarted() {
		return locWebcamRecordingStarted;
	}

	protected void startRecording() {
		vView.getComponent(_idPrefix + "StartRecordingDiv").setVisible(false);
		vView.getComponent(_idPrefix + "TimerDiv").setVisible(false);
		((Timer) vView.getComponent(_idPrefix + "TimerTimer")).stop();
		((Progressmeter) CDesktopComponents.vView().getComponent(_idPrefix + "TimerMeter")).setValue(0);

		vView.getComponent(_idPrefix + "StopRecordingButton").setVisible(true);
		vView.getComponent(_idPrefix + "PauseRecordingButton").setVisible(true);
		vView.getComponent(_idPrefix + "ResumeRecordingButton").setVisible(false);
		sSpring.getSLogHelper().logAction(_logPrefix + "webcam: start recording");
		setWebcamRecordingStarted(false);
		((Camera) vView.getComponent(_idPrefix + "CameraCamera")).start();
	}

	protected void pauseRecording() {
		if (!getWebcamRecordingStarted()) {
			return;
		}
		vView.getComponent(_idPrefix + "PauseRecordingButton").setVisible(false);

		vView.getComponent(_idPrefix + "ResumeRecordingButton").setVisible(true);
		Camera camera = ((Camera) vView.getComponent(_idPrefix + "CameraCamera"));
		if (camera.isRecording()) {
			camera.pause();
			sSpring.getSLogHelper().logAction(_logPrefix + "webcam: pause recording");
		}
	}

	protected void resumeRecording() {
		vView.getComponent(_idPrefix + "ResumeRecordingButton").setVisible(false);

		vView.getComponent(_idPrefix + "PauseRecordingButton").setVisible(true);
		Camera camera = ((Camera) vView.getComponent(_idPrefix + "CameraCamera"));
		if (camera.isPaused()) {
			sSpring.getSLogHelper().logAction(_logPrefix + "webcam: resume recording");
			camera.resume();
		}
	}

	protected void stopRecording() {
		if (!getWebcamRecordingStarted()) {
			return;
		}
		vView.getComponent(_idPrefix + "StopRecordingButton").setVisible(false);
		((Camera) vView.getComponent(_idPrefix + "CameraCamera")).stop();
		((Camera) vView.getComponent(_idPrefix + "CameraCamera")).stopStreaming();
		vView.getComponent(_idPrefix + "PauseRecordingButton").setVisible(false);
		vView.getComponent(_idPrefix + "ResumeRecordingButton").setVisible(false);
		vView.getComponent(_idPrefix + "StartRecordingDiv").setVisible(true);
		vView.getComponent(_idPrefix + "TimerDiv").setVisible(false);
		((Timer) vView.getComponent(_idPrefix + "TimerTimer")).stop();
		((Progressmeter) CDesktopComponents.vView().getComponent(_idPrefix + "TimerMeter")).setValue(0);
		vView.getComponent(_idPrefix + "WebcamDiv").setVisible(false);
		vView.getComponent(_idPrefix + "PleaseWaitDiv").setVisible(true);
		((Progressmeter) CDesktopComponents.vView().getComponent(_idPrefix + "PWProgressMeter")).setValue(0);
		if (getMaxWebcamRecordingLength() > 0) {
			_progressUploadLength = ((CRunPVToolkitPracticeNewRecordingProgressTimer) CDesktopComponents.vView()
					.getComponent(_idPrefix + "ProgressTimer")).getTimeInSecs();
		}
		if (_progressUploadLength < 1)
			_progressUploadLength = 1;
		_progressTimeInSecs = 0;
		vView.getComponent(_idPrefix + "PWProgressDiv").setVisible(true);
		setProgressState(_progressStateUpload);
		sSpring.getSLogHelper().logAction(_logPrefix + "webcam: stop recording; start of file upload");
		((Timer) vView.getComponent(_idPrefix + "PWProgressTimer")).start();
	}

	protected void mirrorOff() {
		(((Camera) vView.getComponent(_idPrefix + "CameraCamera"))).setVisible(false);
		vView.getComponent(_idPrefix + "MirrorOffButton").setVisible(false);
		vView.getComponent(_idPrefix + "MirrorOnButton").setVisible(true);
	}

	protected void mirrorOn() {
		(((Camera) vView.getComponent(_idPrefix + "CameraCamera"))).setVisible(true);
		vView.getComponent(_idPrefix + "MirrorOnButton").setVisible(false);
		vView.getComponent(_idPrefix + "MirrorOffButton").setVisible(true);
	}

	public void addUploadDiv(Component parent) {
		Div divUpload = new CRunPVToolkitDefDiv(parent, new String[] { "id" },
				new Object[] { _idPrefix + "UploadDiv" });

		Button btn = new CRunPVToolkitDefButton(divUpload, new String[] { "id", "class", "labelKey" },
				new Object[] { _idPrefix + "SelectFileButton",
						"font pvtoolkitButton " + _classPrefix + "SelectFileButton",
						"PV-toolkit-practice.button.selectFile" });
		btn.setUpload("true,maxsize=-1,accept=video/*,multiple=false");
		addSelectFileOnUploadEventListener(btn);
	}

	public void addPleaseWaitDiv(Component parent) {
		Div divPleaseWait = new CRunPVToolkitDefDiv(parent, new String[] { "id", "visible" },
				new Object[] { _idPrefix + "PleaseWaitDiv", "false" });

		new CRunPVToolkitDefLabel(divPleaseWait, new String[] { "class", "labelKey" },
				new Object[] { "font " + _classPrefix + "PleaseWaitLabel", "PV-toolkit-practice.label.pleaseWait" });

		_PWLoadLabel = new CRunPVToolkitDefLabel(divPleaseWait, new String[] { "class", "value" }, new Object[] {
				"font " + _classPrefix + "PleaseWaitProgressLabel",
				CDesktopComponents.vView().getLabel("PV-toolkit-practice.label.pleaseWait.load").replace("%1", "0") });

		_PWProgLabel = new CRunPVToolkitDefLabel(divPleaseWait, new String[] { "class", "value" },
				new Object[] { "font " + _classPrefix + "PleaseWaitProgressLabel", CDesktopComponents.vView()
						.getLabel("PV-toolkit-practice.label.pleaseWait.progress").replace("%1", "0") });
		_PWProgLabel.setVisible(false);

		Div lPWProgressDiv = new CRunPVToolkitDefDiv(divPleaseWait, new String[] { "id", "class", "visible" },
				new Object[] { _idPrefix + "PWProgressDiv", _classPrefix + "PWProgressDiv", "false" });

		Hbox lPWProgressHbox = new CRunPVToolkitDefHbox(lPWProgressDiv, null, null);

		Timer lPWTimer = new Timer();
		lPWProgressHbox.appendChild(lPWTimer);
		lPWTimer.setId(_idPrefix + "PWProgressTimer");
		lPWTimer.setDelay(1000);
		lPWTimer.setRepeats(true);
		lPWTimer.setRunning(false);
		_ProgressPercent = 0;
		addPWTimerOnTimerEventListener(lPWTimer);
		// lPWTimer.start();

		// Progressmeter lPWProgressMeter = new CRunPVToolkitDefProgressmeter();
		Progressmeter lPWProgressMeter = new Progressmeter();
		lPWProgressHbox.appendChild(lPWProgressMeter);
		lPWProgressMeter.setId(_idPrefix + "PWProgressMeter");
		lPWProgressMeter.setValue(0);
		lPWProgressMeter.setWidth("150px");

	}

	protected void addPWTimerOnTimerEventListener(Component component) {
		component.addEventListener("onTimer", new EventListener<Event>() {
			@Override
			public void onEvent(Event event) {
				Div lPD = (Div) CDesktopComponents.vView().getComponent(_idPrefix + "PWProgressDiv");
				if (lPD != null) {
					Progressmeter lPM = ((Progressmeter) CDesktopComponents.vView()
							.getComponent(_idPrefix + "PWProgressMeter"));
					if (lPM != null) {
						if (getProgressState().equals(_progressStateUpload)) {
							_progressTimeInSecs++;
							_ProgressPercent = Math.min(100,
									(int) Math.round(100. * _progressTimeInSecs / _progressUploadLength));
							_PWLoadLabel.setValue(
									CDesktopComponents.vView().getLabel("PV-toolkit-practice.label.pleaseWait.load")
											.replace("%1", "" + _ProgressPercent));
						} else {
							if (getProgressState().equals(_progressStateConvert)) {
								_PWProgLabel.setValue(CDesktopComponents.vView()
										.getLabel("PV-toolkit-practice.label.pleaseWait.progress")
										.replace("%1", "" + _ProgressPercent));
							}
						}
						lPM.setValue(_ProgressPercent);
					}
				}
				if (getProgressState().equals(_progressStateConvert)) {
					checkConvertRecordingProgress();
				}
			}
		});
	}

	protected void setProgressState(String aState) {
		_progressState = aState;
		_ProgressPercent = 0;
		if (aState.equals(_progressStateUpload)) {
			_PWLoadLabel.setVisible(true);
			_PWProgLabel.setVisible(false);
			_PWLoadLabel.setValue(CDesktopComponents.vView().getLabel("PV-toolkit-practice.label.pleaseWait.load")
					.replace("%1", "0"));
		}
		if (aState.equals(_progressStateConvert)) {
			_PWLoadLabel.setVisible(false);
			_PWProgLabel.setVisible(true);
			_PWProgLabel.setValue(CDesktopComponents.vView().getLabel("PV-toolkit-practice.label.pleaseWait.progress")
					.replace("%1", "0"));
		}
		((Progressmeter) CDesktopComponents.vView().getComponent(_idPrefix + "PWProgressMeter")).setValue(0);
	}

	protected String getProgressState() {
		return _progressState;
	}

	protected void addSelectFileOnUploadEventListener(Component component) {
		component.addEventListener("onUpload", new EventListener<UploadEvent>() {
			@Override
			public void onEvent(UploadEvent event) {
				Media media = event.getMedia();
				if (media == null || !(media instanceof Media)) {
					vView.showMessagebox(getRoot(), vView.getCLabel("PV-toolkit-practice.no_file.confirmation"),
							vView.getCLabel("PV-toolkit-practice.no_file"), Messagebox.OK, Messagebox.EXCLAMATION);
					return;
				}
				if (!(media instanceof org.zkoss.video.Video)) {
					vView.showMessagebox(getRoot(), vView.getCLabel("PV-toolkit-practice.no_videofile.confirmation"),
							vView.getCLabel("PV-toolkit-practice.no_videofile"), Messagebox.OK, Messagebox.EXCLAMATION);
					return;
				}
				vView.getComponent(_idPrefix + "UploadDiv").setVisible(false);

				vView.getComponent(_idPrefix + "PleaseWaitDiv").setVisible(true);

				Events.echoEvent("onStoreVideoAsBlobAndShow", vView.getComponent(_idPrefix), media);
			}
		});
	}

	public void onStoreVideoAsBlobAndShow(Event event) {
		Media media = (Media) event.getData();

		storeVideoAsBlobAndShow(media, false, -1);
		/*
		 * String name = media.getName(); name = name.substring(0, name.indexOf("."));
		 * showVideoDiv(name);
		 */
	}

	public void showVideoDiv(String name) {
		vView.getComponent(_idPrefix + "PleaseWaitDiv").setVisible(false);

		vView.getComponent(_idPrefix + "Video").setVisible(true);
		vView.getComponent(_idPrefix + "VideoDiv").setVisible(true);
		vView.getComponent(_idPrefix + "VideoInteractionDiv").setVisible(true);
		vView.getComponent(_idPrefix + "ViewDiv").setVisible(true);
		((Textbox) vView.getComponent(_idPrefix + "GiveNameTextbox")).setValue(name);
	}

	public void addRecordingGiveNameDiv(Component parent) {
		Div div = new CRunPVToolkitDefDiv(parent, new String[] { "id", "visible" },
				new Object[] { _idPrefix + "GiveNameDiv", "false" });

		new CRunPVToolkitDefLabel(div, new String[] { "class", "labelKey" }, new Object[] {
				"font " + _classPrefix + "GiveNameTitle", "PV-toolkit-practice.header.giveRecordingAName" });

		new CRunPVToolkitDefTextbox(div, new String[] { "id", "class" },
				new Object[] { _idPrefix + "GiveNameTextbox", "font " + _classPrefix + "GiveNameTextbox" });
	}

	public void addViewRecordingDiv(Component parent, boolean isWebcamRecording) {
		Div div = new CRunPVToolkitDefDiv(parent, new String[] { "id", "visible" },
				new Object[] { _idPrefix + "ViewDiv", "false" });

		Button btn = new CRunPVToolkitDefButton(div, new String[] { "id", "class", "cLabelKey" },
				new Object[] { _idPrefix + "AgainButton", "font pvtoolkitButton " + _classPrefix + "AgainButton",
						"PV-toolkit.again" });
		if (isWebcamRecording) {
			btn.setAttribute("previousDivId", _idPrefix + "WebcamDiv");
		} else {
			btn.setAttribute("previousDivId", _idPrefix + "UploadDiv");
		}
		addAgainButtonOnClickEventListener(btn);

		btn = new CRunPVToolkitDefButton(div, new String[] { "id", "class", "cLabelKey" },
				new Object[] { _idPrefix + "ForwardButton", "font pvtoolkitButton " + _classPrefix + "ForwardButton",
						"PV-toolkit.forward" });
		addForwardButtonOnClickEventListener(btn);
	}

	protected void addAgainButtonOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			@Override
			public void onEvent(Event event) {
				again();
			}
		});
	}

	public void again() {
		// NOTE pause video
		Clients.evalJavaScript("pauseVideo('" + _idPrefix + "VideoVideo" + "');");
		vView.getComponent(_idPrefix + "Video").setVisible(false);
		vView.getComponent(_idPrefix + "VideoInteractionDiv").setVisible(false);
		vView.getComponent(_idPrefix + "VideoDiv").setVisible(false);
		vView.getComponent(_idPrefix + "ViewDiv").setVisible(false);

		// NOTE remove possibly stored blob
		if (!StringUtils.isEmpty(_recordingBlobId)) {
			pvToolkit.deleteVideoBlob(_recordingBlobId);
			sSpring.getSLogHelper()
					.logAction(_logPrefix + "webcam/upload 'again' button clicked: blob deleted: " + _recordingBlobId);
			_recordingBlobId = "";
			_webcamRecordingLength = -1;
		}

		vView.getComponent(_idPrefix + "PleaseWaitDiv").setVisible(false);
		_ProgressPercent = 0;
		Div lPD = (Div) CDesktopComponents.vView().getComponent(_idPrefix + "PWProgressDiv");
		if (lPD != null) {
			((Timer) vView.getComponent(_idPrefix + "PWProgressTimer")).stop();
			((Progressmeter) CDesktopComponents.vView().getComponent(_idPrefix + "PWProgressMeter"))
					.setValue(_ProgressPercent);
			_PWProgLabel.setValue(CDesktopComponents.vView().getLabel("PV-toolkit-practice.label.pleaseWait.progress")
					.replace("%1", "" + _ProgressPercent));
			lPD.setVisible(false);
		}

		Component againButton = vView.getComponent(_idPrefix + "AgainButton");
		vView.getComponent((String) againButton.getAttribute("previousDivId")).setVisible(true);
		if (againButton.getAttribute("previousDivId").equals(_idPrefix + "WebcamDiv")) {
			((CRunPVToolkitPracticeNewRecordingCamera) vView.getComponent(_idPrefix + "CameraCamera")).init(_idPrefix,
					_classPrefix);
		}
	}

	protected void addForwardButtonOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			@Override
			public void onEvent(Event event) {
				// NOTE pause video
				Clients.evalJavaScript("pauseVideo('" + _idPrefix + "VideoVideo" + "');");
				vView.getComponent(_idPrefix + "Video").setVisible(false);
				vView.getComponent(_idPrefix + "VideoInteractionDiv").setVisible(false);
				vView.getComponent(_idPrefix + "VideoDiv").setVisible(false);
				vView.getComponent(_idPrefix + "ViewDiv").setVisible(false);
				vView.getComponent(_idPrefix + "GiveNameDiv").setVisible(true);
				vView.getComponent(_idPrefix + "SaveDiv").setVisible(true);
			}
		});
	}

	public void addSaveRecordingDiv(Component parent) {
		Div div = new CRunPVToolkitDefDiv(parent, new String[] { "id", "visible" },
				new Object[] { _idPrefix + "SaveDiv", "false" });

		Button btn = new CRunPVToolkitDefButton(div, new String[] { "id", "class", "cLabelKey" }, new Object[] {
				_idPrefix + "BackButton", "font pvtoolkitButton " + _classPrefix + "BackButton", "PV-toolkit.back" });
		addBackButtonOnClickEventListener(btn);

		btn = new CRunPVToolkitDefButton(div, new String[] { "id", "class", "cLabelKey" }, new Object[] {
				_idPrefix + "SaveButton", "font pvtoolkitButton " + _classPrefix + "SaveButton", "PV-toolkit.save" });
		addSaveButtonOnClickEventListener(btn);
	}

	protected void addBackButtonOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			@Override
			public void onEvent(Event event) {
				sSpring.getSLogHelper().logAction(_logPrefix + "webcam/upload: 'back' button clicked");
				vView.getComponent(_idPrefix + "SaveDiv").setVisible(false);
				vView.getComponent(_idPrefix + "PleaseWaitDiv").setVisible(false);
				vView.getComponent(_idPrefix + "GiveNameDiv").setVisible(false);
				vView.getComponent(_idPrefix + "Video").setVisible(true);
				vView.getComponent(_idPrefix + "VideoInteractionDiv").setVisible(true);
				vView.getComponent(_idPrefix + "VideoDiv").setVisible(true);
				vView.getComponent(_idPrefix + "ViewDiv").setVisible(true);
			}
		});
	}

	public void storeVideoAsBlobAndShow(Media media, boolean webcamRecording, int webcamRecordingLength) {
		String fileName = "";
		// NOTE store user id, run id, rga id and cycle number within file name to be
		// able to identify the context of the file
		IERunGroupAccount currentRunGroupAccount = sSpring.getRunGroupAccount();
		String fileNamePrefix = "userid_" + currentRunGroupAccount.getEAccount().getUserid() + "_runid_"
				+ currentRunGroupAccount.getERunGroup().getERun().getRunId() + "_rgaid_"
				+ currentRunGroupAccount.getRgaId() + "_cycle_" + pvToolkit.getCurrentCycleNumber() + "_";
		vView.getComponent(_idPrefix + "PleaseWaitDiv").setVisible(false);
		((Timer) vView.getComponent(_idPrefix + "PWProgressTimer")).stop();

		_StopPWProgressTimer = false;
		_PWProgressTimerFinished = false;

		vView.getComponent(_idPrefix + "PWProgressDiv").setVisible(true);
		setProgressState(_progressStateConvert);
		((Timer) vView.getComponent(_idPrefix + "PWProgressTimer")).start();

		if (webcamRecording) {
			fileName = fileNamePrefix + "webcam." + CRunPVToolkit.webcamRecordFileExtension;
			_ShowRecordingName = ""; // student must enter non-empty name for recording
		} else {
			fileName = media.getName();
			_ShowRecordingName = fileName;
			if (fileName.contains(".")) {
				fileName = fileNamePrefix + "upload" + fileName.substring(fileName.lastIndexOf("."));
			}
		}
		sSpring.getSLogHelper()
				.logAction(_logPrefix + "webcam/upload: file uploaded; start of conversion; file: " + fileName);

		_recordingBlobId = sSpring.getSBlobHelper().setBlobMedia(_recordingBlobId, media, fileName,
				AppConstants.blobtypeDatabase);

		_webcamRecordingLength = webcamRecordingLength;
		String url = sSpring.getSBlobHelper().getUrl(_recordingBlobId, AppConstants.blobtypeDatabase);

		_IsWebcamRecording = webcamRecording;
		int lPos = _ShowRecordingName.indexOf(".");
		if (lPos > 0)
			_ShowRecordingName = _ShowRecordingName.substring(0, lPos);

		String lConvertedUrl = url;
		if (checkConvertRecording())
			lConvertedUrl = convertRecordingIfNecessary(url);
		else
			sSpring.getSLogHelper()
					.logAction(_logPrefix + "webcam/upload: no conversion configured; file: " + lConvertedUrl);

		if (!lConvertedUrl.equals("")) {
			_ConvertedRecordingName = lConvertedUrl;
			_StopPWProgressTimer = true;
		}
	}

	protected String convertRecordingIfNecessary(String aRecordingName) {
		String lConvertVideoCodec = getWebcamRecordVideoCodec();
		String lConvertAudioCodec = getWebcamRecordAudioCodec();
		String lConvertVideoLibrary = getWebcamRecordVideoLibrary();
		String lConvertAudioLibrary = getWebcamRecordAudioLibrary();
		String lConvertVideoBitrate = getWebcamRecordVideoBitrate();
		String lConvertAudioBitrate = getWebcamRecordAudioBitrate();

		if (lConvertVideoCodec.isEmpty() || lConvertAudioCodec.isEmpty() || lConvertVideoLibrary.isEmpty()
				|| lConvertAudioLibrary.isEmpty() || lConvertVideoBitrate.isEmpty() || lConvertAudioBitrate.isEmpty()) {
			sSpring.getSLogHelper()
					.logAction(_logPrefix + "conversion of " + aRecordingName + " aborted: configuration missing: "
							+ lConvertVideoCodec + ":" + lConvertAudioCodec + ":" + lConvertVideoLibrary + ":"
							+ lConvertAudioLibrary + ":" + lConvertVideoBitrate + ":" + lConvertAudioBitrate);
			return aRecordingName;
		}

		if (StringUtils.isEmpty(aRecordingName)) {
			sSpring.getSLogHelper().logAction(_logPrefix + "conversion error: filename empty!");
			_ConvertedRecordingName = "";
			_StopPWProgressTimer = true;
			return "";
		}

		String lFFMpegDir = sSpring.getAppManager().getFFMPegPath();
		if (StringUtils.isEmpty(lFFMpegDir)) {
			sSpring.getSLogHelper().logAction(_logPrefix + "conversion error: ffmpeg path not valid!");
			_ConvertedRecordingName = "";
			_StopPWProgressTimer = true;
			return "";
		}

		Path lFFMpegPath = Paths.get(lFFMpegDir);
		_FFProbe = FFprobe.atPath(lFFMpegPath);
		String lFFUrl = sSpring.getAppManager().getAbsoluteAppPath() + aRecordingName.substring(1);

		FFprobeResult lFFPResult = _FFProbe.setShowStreams(true).setInput(lFFUrl).execute();

		Float lAudioDuration = null;
		Float lVideoDuration = null;
		Integer lAudioBitrate = null;
		Integer lVideoBitrate = null;
		String lAudioCodec = "";
		String lVideoCodec = "";
		String lVideoRotation = "";
		boolean lCheckRotation = checkWebcamRotation();
		for (Stream stream : lFFPResult.getStreams()) {
			String lCodecName = stream.getCodecName();
			Float lStreamDuration = getStreamDuration(stream);
			Integer lStreamBitrate = stream.getBitRate();
			String lSpecInfo = "";
			if (stream.getCodecType().compareTo(StreamType.VIDEO) == 0) {
				if (!(lCodecName == null))
					lVideoCodec = lCodecName;
				lVideoDuration = lStreamDuration;
				lVideoBitrate = lStreamBitrate;
				lSpecInfo = " aspectratio: " + stream.getDisplayAspectRatio();
				if (lCheckRotation) {
					lVideoRotation = getVideoStreamRotation(stream);
					lSpecInfo = lSpecInfo + " rotation: " + lVideoRotation;
				}
			}
			if (stream.getCodecType().compareTo(StreamType.AUDIO) == 0) {
				if (!(lCodecName == null))
					lAudioCodec = lCodecName;
				lAudioDuration = lStreamDuration;
				lAudioBitrate = lStreamBitrate;
			}
			sSpring.getSLogHelper()
					.logAction(_logPrefix + "conversion: original file: " + lFFUrl + ": Stream #" + stream.getIndex()
							+ " type: " + stream.getCodecType() + " codec: " + lCodecName + lSpecInfo + " duration: "
							+ lStreamDuration + " bitrate: " + lStreamBitrate);
		}

		final AtomicLong lAtomDuration = new AtomicLong();
		if (lVideoDuration == null) {
			// alternative way to calculate duration
			FFmpeg.atPath(lFFMpegPath).addInput(UrlInput.fromUrl(lFFUrl)).setOverwriteOutput(true)
					.addOutput(new NullOutput()).setProgressListener(new ProgressListener() {
						@Override
						public void onProgress(FFmpegProgress progress) {
							lAtomDuration.set(progress.getTimeMillis());
						}
					}).execute();
			// lVideoDuration = lAtomDuration.floatValue();
		} else
			lAtomDuration.set(Math.round(lVideoDuration));

		// if (lAudioDuration == null)
		// lAudioDuration = lVideoDuration;
		if ((lAtomDuration == null) || (lAtomDuration.floatValue() < 10))
			lAtomDuration.set(10);

		if (lVideoBitrate == null) {
			// try to estimate overall bit rate depending on file size and duration
			IFileManager lFM = (IFileManager) CDesktopComponents.sSpring().getBean("fileManager");
			lVideoBitrate = Math.round(lFM.getFile(lFFUrl).length() * 8000 / lAtomDuration.floatValue());
		}

		boolean lCheckSync = checkWebcamSync();
		int lSyncDiff = getWebcamSyncDiff();

		if ((lAudioDuration != null) && (lVideoDuration != null)
				&& (!lCheckSync || (Math.abs(lVideoDuration - lAudioDuration) < lSyncDiff))
				&& lAudioCodec.equalsIgnoreCase(lConvertAudioCodec) && lVideoCodec.equalsIgnoreCase(lConvertVideoCodec)
				&& (lVideoBitrate != null) && (lVideoBitrate < getWebcamMaxBitrate())) {
			if (_webcamRecordingLength == -1)
				// NOTE duration in milliseconds
				_webcamRecordingLength = Math.round(lVideoDuration / 1000);
			sSpring.getSLogHelper().logAction(_logPrefix + "conversion not necessary.");
			return aRecordingName;
		}

		_ConvertedRecordingName = aRecordingName.substring(0, aRecordingName.lastIndexOf(".") + 1)
				+ pvToolkit.getWebcamRecordFileExtension();
		if (_ConvertedRecordingName.equals(aRecordingName))
			_ConvertedRecordingName = aRecordingName.substring(0, aRecordingName.lastIndexOf(".")) + "_conv."
					+ pvToolkit.getWebcamRecordFileExtension();

		_FFOutUrl = sSpring.getAppManager().getAbsoluteAppPath() + _ConvertedRecordingName.substring(1);

		FFmpeg lFFMPeg = FFmpeg.atPath(lFFMpegPath);
		lFFMPeg.addInput(UrlInput.fromUrl(lFFUrl));

		if (lCheckSync) {
			if ((lAudioDuration != null) && (lVideoDuration != null)
					&& !(Math.abs(lVideoDuration - lAudioDuration) < lSyncDiff)) {
				int lOutOfSync = Math.round(lAudioDuration - lVideoDuration);
				// TODO account for first frame duration?
				// NOTE take audio from first input, and video from second input; delay second
				// input (video) by lOutOfSync ms.
				// If negative, first lOutOfSync ms wil be cut. Don't delay audio, because
				// 'source duration' will be stored in audio stream, and this value
				// seems to overrule the generated audio duration
				lFFMPeg.addInput(UrlInput.fromUrl(lFFUrl).addArguments("-itsoffset", lOutOfSync + "ms"))
						.addArguments("-map", "0:1").addArguments("-map", "1:0");
			}
		}

		// NOTE video area is 1080x440
		String lFilterString = "scale=w=1080:h=440:force_original_aspect_ratio=decrease";
		if (lCheckRotation && !lVideoRotation.isEmpty()) {
			String lRotFilter = getRotationFilter(lVideoRotation);
			if (!lRotFilter.isEmpty())
				lFilterString = lFilterString + "," + lRotFilter;
		}

		addProgressEventListener(lAtomDuration);

		_FFmpegResult = lFFMPeg.addArguments("-b:v", lConvertVideoBitrate).addArguments("-b:a", lConvertAudioBitrate)
				// .setFilter(StreamType.VIDEO, "rotate=45*PI/180,
				// scale=w=1080:h=440:force_original_aspect_ratio=decrease")
				.addArguments("-movflags", "faststart").addArguments("-map_metadata", "-1").setOverwriteOutput(true)
				.setFilter(StreamType.VIDEO, lFilterString)
				.addOutput(UrlOutput.toUrl(_FFOutUrl).setCodec(StreamType.VIDEO, lConvertVideoLibrary)
						.setCodec(StreamType.AUDIO, lConvertAudioLibrary)
						.setFormat(pvToolkit.getWebcamRecordFileExtension()))
				.setProgressListener(_ProgListener).executeAsync();

		vView.getComponent(_idPrefix + "PleaseWaitDiv").setVisible(true);
		return "";
	}

	protected void addProgressEventListener(AtomicLong pAtomDuration) {
		_ProgListener = new ProgressListener() {
			@Override
			public void onProgress(FFmpegProgress aProgress) {
				_ProgressPercent = Math.min(100,
						(int) Math.round(100. * aProgress.getTimeMillis() / pAtomDuration.get()));
			}
		};
	}

	protected void checkConvertRecordingProgress() {
		if (_StopPWProgressTimer) {
			if (!_PWProgressTimerFinished) {
				_PWProgressTimerFinished = true;
				((Timer) vView.getComponent(_idPrefix + "PWProgressTimer")).stop();
				recordingReadyToShow(_ConvertedRecordingName);
			}
		} else {
			if (_FFmpegResult.isDone()) {
				// NOTE determine duration of streams in converted file
				FFprobeResult lFFPResult = _FFProbe.setShowStreams(true).setInput(_FFOutUrl).execute();

				Float lAudioDuration = null;
				Float lVideoDuration = null;
				for (Stream stream : lFFPResult.getStreams()) {
					if (stream.getCodecType().compareTo(StreamType.VIDEO) == 0)
						lVideoDuration = getStreamDuration(stream);
					else
						lAudioDuration = getStreamDuration(stream);
				}
				if ((_webcamRecordingLength == -1) && (lVideoDuration != null))
					_webcamRecordingLength = Math.round(lVideoDuration / 1000);

				sSpring.getSLogHelper().logAction(_logPrefix + "conversion: converted file: " + _FFOutUrl
						+ ": video duration: " + lVideoDuration + " ms" + "; audioduration: " + lAudioDuration + " ms");
				_StopPWProgressTimer = true;
			}
		}
	}

	protected void recordingReadyToShow(String pRecordingName) {
		// NOTE if recording length was not yet known (for uploaded recording), it is
		// stored by convertRecordingIfNecessary in _webcamRecordingLength
		int webcamRecordingLength = _webcamRecordingLength;
		String lFileName = pRecordingName.substring(pRecordingName.lastIndexOf("/") + 1);
		String url = sSpring.getSBlobHelper().getUrl(_recordingBlobId, AppConstants.blobtypeDatabase);
		if (!url.equals(pRecordingName)) {
			// NOTE adapt reference in blob

			IBlobManager lBM = (IBlobManager) sSpring.getBean("blobManager");
			IEBlob lBlob = lBM.getBlob(Integer.parseInt(_recordingBlobId));
			lBlob.setFilename(lFileName);
			lBM.saveBlob(lBlob);

			// NOTE add new file to content files filter access:
			url = sSpring.getSBlobHelper().getUrl(_recordingBlobId, AppConstants.blobtypeDatabase);
		}

		if (!_IsWebcamRecording) {
			// TODO if webcam recording also upload migrate video to Wowza server
			pvToolkit.copyVideoBlobToWowza(_recordingBlobId, lFileName);
		}

		pvToolkit.showVideoUrl(url, webcamRecordingLength, _idPrefix, _classPrefix);

		showVideoDiv(_ShowRecordingName);

	}

	// NOTE return duration in ms
	protected Float getStreamDuration(Stream aStream) {
		Float lDur = aStream.getDuration();
		if (lDur == null) {
			String lDurStr = aStream.getTag("DURATION");
			if (!((lDurStr == null) || (lDurStr.isEmpty()))) {
				try {
					long lNanoDur = LocalTime.parse(lDurStr, DateTimeFormatter.ofPattern("HH:mm:ss.nnnnnnnnn"))
							.getLong(ChronoField.NANO_OF_DAY);
					lDur = (float) (lNanoDur / 1000000);
				} catch (DateTimeParseException e) {

				}
			}
		} else
			// NOTE stream duration in seconds!
			lDur = lDur * 1000;
		return lDur;
	}

	protected String getVideoStreamRotation(Stream aStream) {
		String lRot = aStream.getTag("rotate");
		if (lRot == null) {
			return "";
		}
		return lRot;
	}

	protected String getRotationFilter(String aRotation) {
		String lFilter = "";
		switch (aRotation) {
		case "90":
			lFilter = "transpose=1";
			break;
		case "180":
			lFilter = "transpose=2,transpose=2";
			break;
		case "270":
			lFilter = "transpose=2";
			break;
		default:
			break;
		}
		return lFilter;
	}

	protected String getWebcamRecordVideoCodec() {
		if (locWebcamRecordVideoCodec == null)
			locWebcamRecordVideoCodec = PropsValues.LOCAL_RECORDER_VIDEO_CODEC;
		return locWebcamRecordVideoCodec;
	}

	protected String getWebcamRecordAudioCodec() {
		if (locWebcamRecordAudioCodec == null)
			locWebcamRecordAudioCodec = PropsValues.LOCAL_RECORDER_AUDIO_CODEC;
		return locWebcamRecordAudioCodec;
	}

	protected boolean checkConvertRecording() {
		if (locConvertRecording == null)
			locConvertRecording = PropsValues.LOCAL_RECORDER_CONVERT;
		return locConvertRecording;
	}

	protected String getWebcamRecordVideoLibrary() {
		if (locWebcamRecordVideoLibrary == null)
			locWebcamRecordVideoLibrary = PropsValues.LOCAL_RECORDER_VIDEO_LIBRARY;
		return locWebcamRecordVideoLibrary;
	}

	protected String getWebcamRecordAudioLibrary() {
		if (locWebcamRecordAudioLibrary == null)
			locWebcamRecordAudioLibrary = PropsValues.LOCAL_RECORDER_AUDIO_LIBRARY;
		return locWebcamRecordAudioLibrary;
	}

	protected String getWebcamRecordVideoBitrate() {
		if (locWebcamRecordVideoBitrate == null)
			locWebcamRecordVideoBitrate = PropsValues.LOCAL_RECORDER_VIDEO_BITRATE;
		return locWebcamRecordVideoBitrate;
	}

	protected String getWebcamRecordAudioBitrate() {
		if (locWebcamRecordAudioBitrate == null)
			locWebcamRecordAudioBitrate = PropsValues.LOCAL_RECORDER_AUDIO_BITRATE;
		return locWebcamRecordAudioBitrate;
	}

	protected int getWebcamMaxBitrate() {
		if (locWebcamRecordMaxBitrate == null) {
			try {
				locWebcamRecordMaxBitrate = PropsValues.LOCAL_RECORDER_MAX_BITRATE;
			} catch (NumberFormatException e) {
				// NOTE default assume 3 Mb/s as maximum bitrate for streaming
				locWebcamRecordMaxBitrate = 3000000;
			}
		}
		return locWebcamRecordMaxBitrate;
	}

	protected boolean checkWebcamSync() {
		if (locWebcamCheckSync == null)
			locWebcamCheckSync = PropsValues.LOCAL_RECORDER_SYNCHRONIZATION_CHECK;
		return locWebcamCheckSync;
	}

	protected int getWebcamSyncDiff() {
		if (locWebcamSyncDiff == null) {
			locWebcamSyncDiff = PropsValues.LOCAL_RECORDER_SYNCHRONIZATION_DIFFERENCE;
			// NOTE assume 20 ms as minimum difference for noticeable out-of-sync issues
			if (locWebcamSyncDiff < 20)
				locWebcamSyncDiff = 200;
		}
		return locWebcamSyncDiff;
	}

	protected boolean checkWebcamRotation() {
		if (locWebcamCheckRotation == null)
			locWebcamCheckRotation = PropsValues.LOCAL_RECORDER_ROTATION_CHECK;
		return locWebcamCheckRotation;
	}

	protected void addSaveButtonOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			@Override
			public void onEvent(Event event) {
				String name = ((Textbox) vView.getComponent(_idPrefix + "GiveNameTextbox")).getValue();
				if (name.equals("")) {
					// show warning
					vView.showMessagebox(getRoot(), vView.getLabel("PV-toolkit-practice.ready.confirmation"),
							vView.getLabel("PV-toolkit-practice.ready"), Messagebox.OK, Messagebox.EXCLAMATION);
					return;
				}

				if (!StringUtils.isEmpty(_recordingBlobId)) {
					saveBlob(name);
				}

				pvToolkit.clearVideo(_idPrefix);

				vView.getComponent(_idPrefix).setVisible(false);
			}
		});
	}

	protected void saveBlob(String name) {
		CRunPVToolkitPracticeRecordingsDiv practiceRecordings = (CRunPVToolkitPracticeRecordingsDiv) vView
				.getComponent("practiceRecordingsDiv");
		practiceRecordings.addPractice(name, _recordingBlobId, _webcamRecordingLength);
		_recordingBlobId = "";
		_webcamRecordingLength = -1;
	}

}
