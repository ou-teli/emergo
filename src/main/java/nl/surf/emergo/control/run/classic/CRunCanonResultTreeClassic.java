/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Treecol;
import org.zkoss.zul.Treecols;
import org.zkoss.zul.Treeitem;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CCaseHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefTreecol;
import nl.surf.emergo.control.def.CDefTreecols;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IECaseRole;

/**
 * The Class CRunCanonResultTree.
 */
public class CRunCanonResultTreeClassic extends CRunTreeClassic {

	private static final long serialVersionUID = 6732242686888300225L;

	/**
	 * Instantiates a new c run Canon Result tree. The canon result uses the content of
	 * the canon component.
	 * 
	 * @param aId the a id
	 * @param aCaseComponent the canon result case component
	 * @param aRunComponent the a run component, the ZK canon result component
	 */
	public CRunCanonResultTreeClassic(String aId, IECaseComponent aCaseComponent, CRunComponentClassic aRunComponent) {
		super(aId, aCaseComponent, aRunComponent);
		tagopenednames = "piece";
		setRows(23);
		IXMLTag lRootTag = determineRootTag();
		update(lRootTag);
	}

	/**
	 * Determine root tag of canon component to be used to render canon result.
	 * 
	 * @return the XML tag
	 */
	protected IXMLTag determineRootTag() {
		String lCacId = "" + caseComponent.getCacId();
		IXMLTag lRootTag = CDesktopComponents.sSpring().getXmlDataPlusRunStatusTree(lCacId, getRunStatusType());
		if (lRootTag != null) {
			// get cacid of referenced casecomponent
			IXMLTag lContentTag = lRootTag.getChild(AppConstants.contentElement);
			if (lContentTag != null) {
				IXMLTag lRefTag = lContentTag.getDefChild("refcanon");
				if (lRefTag != null) {
					String lReftype = lRefTag.getDefAttribute(AppConstants.defKeyReftype);
					CCaseHelper cCaseHelper = new CCaseHelper();
					IECaseRole lCaseRole = CDesktopComponents.sSpring().getSCaseRoleHelper().getCaseRole();
					List<String> lRefIds = cCaseHelper.getRefTagIds(lReftype, "" + AppConstants.statusKeySelectedIndex, lCaseRole, caseComponent, lContentTag);
					if (lRefIds.size() > 0) {
						String lRefId = (String)lRefIds.get(0);
						if ((lRefId != null) && (!lRefId.equals(""))) {
							String[] lIdArr = lRefId.split(",");
							if (lIdArr.length == 3) {
								lCacId = lIdArr[1];
								IECaseComponent lCaseComponent = CDesktopComponents.sSpring().getCaseComponent(lCacId);
								boolean lContentShared = ((CDesktopComponents.sSpring().getCurrentRunComponentStatus(
										lCaseComponent, AppConstants.statusKeyShared, AppConstants.statusTypeRunGroup))
										.equals(AppConstants.statusValueTrue));
								String lRunStatusType = "";
								if (lContentShared)
									lRunStatusType = AppConstants.statusTypeRun;
								else
									lRunStatusType = AppConstants.statusTypeRunGroup;
								lRootTag = CDesktopComponents.sSpring().getXmlDataPlusRunStatusTree(lCacId, lRunStatusType);
							}
						}
					}
				}
			}
		}
		return lRootTag;
	}
	
	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTree#appendTreecols(org.zkoss.zk.ui.Component)
	 */
	protected void appendTreecols(Component aParent) {
		Treecols lTreecols = new CDefTreecols();
		Treecol lTreecol = new CDefTreecol();
		lTreecols.appendChild(lTreecol);
		lTreecol = new CDefTreecol();
		lTreecols.appendChild(lTreecol);
		aParent.appendChild(lTreecols);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTree#getRunComponentHelper()
	 */
	@Override
	public CRunComponentHelperClassic getRunComponentHelper() {
		return new CRunCanonResultHelperClassic(this, tagopenednames, caseComponent, runComponent);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTree#setTreeitemStatus(org.zkoss.zul.Treeitem)
	 */
	public Treeitem setTreeitemStatus(Treeitem aTreeitem) {
		// do nothing till it is clear what to do.
		// should selected and opened be saved as private status or as shared status
		// and if shared it should be saved per rungroupaccount, like ratings
		return aTreeitem;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTree#doTreeitemAction(org.zkoss.zul.Treeitem)
	 */
	public void doTreeitemAction(Treeitem aTreeitem) {
//		if item clicked notify runCanonResult
		CRunCanonResultClassic lComp = (CRunCanonResultClassic) CDesktopComponents.vView().getComponent("runCanonResult");
		if (lComp != null) {
			lComp.doContentItemAction(aTreeitem);
		}
	}
}
