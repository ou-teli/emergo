/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Label;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.Treerow;
import org.zkoss.zul.impl.XulElement;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CContentHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CTree;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;

/**
 * The Class CRunCanonHelper. Helps rendering canon pieces.
 */
public class CRunCanonHelperClassic extends CRunComponentHelperClassic {

	/**
	 * Instantiates a new c run canon helper.
	 * 
	 * @param aTree the ZK canon tree
	 * @param aShowTagNames the a show tag names, comma separated, only tags with these names are shown
	 * @param aCaseComponent the canon case component
	 * @param aRunComponent the a run component, the ZK canon component
	 */
	public CRunCanonHelperClassic(CTree aTree, String aShowTagNames, IECaseComponent aCaseComponent, CRunComponentClassic aRunComponent) {
		super(aTree, aShowTagNames, aCaseComponent, aRunComponent);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTreeHelper#renderTreecell(org.zkoss.zul.Treeitem, org.zkoss.zul.Treerow, nl.surf.emergo.business.IXMLTag, java.lang.String, boolean)
	 */
	@Override
	protected Treecell renderTreecell(Treeitem aTreeitem, Treerow aTreerow, IXMLTag aItem, String aKeyValues, boolean aAccIsAuthor) {
		Treecell lTreecell = getTreerowTreecell(aTreerow, 0);
		String lLabelStr = aKeyValues;
		String lOwnerRgaId = aItem.getAttribute(AppConstants.keyOwner_rgaid);
		if (!lOwnerRgaId.equals(""))
			lLabelStr = lLabelStr + " (" +CDesktopComponents.sSpring().getRunGroupAccount(Integer.parseInt(lOwnerRgaId)).getERunGroup().getName() +")";
		lTreecell.setLabel(lLabelStr);
		String lStatus = "active";
		if (isOpened(aItem))
			lStatus = "opened";
		if (!isAccessible(aItem))
			lStatus = "inactive";
		lTreecell.setZclass("CRunCanon_treecell_"+lStatus);
		lTreecell.setStyle(CDesktopComponents.vView().getLabel("CRunComponent_treecell_style"));
		if (aItem.getName().equals("map")) {
			String lImg = VView.getInitParameter("emergo.style.path") + "icon_folder_" + lStatus + ".png";
			lTreecell.setImage(lImg);
		}
		if (aItem.getName().equals("piece")) {
			String lMediatype = aItem.getChildAttribute("blob", AppConstants.keyMediatype);
			if (lMediatype.equals("text"))
				lMediatype = "document";
			if (lMediatype.equals(""))
				lMediatype = "other";
			String lImg = VView.getInitParameter("emergo.style.path") + "icon_" + lMediatype + "_" + lStatus + ".png";
			lTreecell.setImage(lImg);
		}
		if (aItem.getName().equals(AppConstants.contentElement)) {
			String lTagName = CContentHelper.getNodeTagLabel(caseComponent.getEComponent().getCode(), aItem.getName());
			Label lLabel = getComponentLabel(lTreecell, "contentelement", lTagName + " ");
			lLabel.setZclass(runComponent.getClassName() + "_treecell_"+lStatus);
			lLabel.setStyle(CDesktopComponents.vView().getLabel("CRunComponent_treecell_style"));
		}

		String lOwnerRugId = aItem.getAttribute(AppConstants.keyOwner_rgaid);
		boolean lOwner = (lOwnerRugId.equals(""+CDesktopComponents.sSpring().getRunGroupAccount().getRgaId()));
		if (!lOwner) {
			// don't show tooltips for owners, because it interferes with popupmenu.
			String lToolTip = CDesktopComponents.sSpring().unescapeXML(aItem.getChildValue("motivation"));
			String lBloId = aItem.getChildValue("picture");
			if (lBloId.equals("")) {
				lBloId = aItem.getChildValue("illustration");
			}
			if (((lToolTip != null) && (!lToolTip.equals("")) || (!lBloId.equals("")))) {
				lTreecell.setTooltip("toolTip" + VView.initTooltip);
				lTreecell.setAttribute("tooltip", lToolTip);
				lTreecell.setAttribute("item", aItem);
				lTreecell.setAttribute("bloid", lBloId);
			}
		}

		return lTreecell;
	}
	
	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunComponentHelper#setContextMenu(org.zkoss.zk.ui.Component)
	 */
	public void setContextMenu(Component aObject) {
		if (extendable)
			((XulElement) aObject).setContext("runMenuPopup");
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.CContentHelper#editContentItem(org.zkoss.zk.ui.Component, org.zkoss.zk.ui.Component, nl.surf.emergo.business.IXMLTag)
	 */
	public Component editContentItem(Component aParent, Component aContentItem, IXMLTag aItem) {
		Component lTreeitem = super.editContentItem(aParent, aContentItem, aItem);
		// Redraw item data by simulating click on it. But only for owner. Other rgas maybe rating it.
		if (aItem.getAttribute(AppConstants.keyOwner_rgaid).equals(""+CDesktopComponents.sSpring().getRunGroupAccount().getRgaId()))
			runComponent.doContentItemAction((Treeitem)lTreeitem);
		return lTreeitem;
	}

}