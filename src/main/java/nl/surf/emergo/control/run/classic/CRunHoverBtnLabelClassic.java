/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefLabel;

/**
 * The Class CRunHoverBtnLabel. Used to show the label of a hover button.
 */
public class CRunHoverBtnLabelClassic extends CRunAreaClassic {

	private static final long serialVersionUID = -84780013591650401L;

	/** The btnname of the hover button. */
	protected String btnname = "";

	/** The labelstr to be shown. */
	protected String labelstr = "";

	/** The label component used to show the label. */
	protected CDefLabel label = null;

	/** The position within the player. */
	protected String position = "";

	/**
	 * Instantiates a new c run hover btn label.
	 * 
	 * @param aId the a id
	 * @param aBtnName the a btn name
	 * @param aPosition the a position
	 * @param aLabel the a label
	 */
	public CRunHoverBtnLabelClassic(String aId, String aBtnName, String aPosition, String aLabel) {
		super(aId);
		btnname = aBtnName;
		position = aPosition;
		labelstr = aLabel;
		if ((labelstr == null) || (labelstr.equals("")))
			labelstr = CDesktopComponents.vView().getLabel("run.button." + btnname);
		label = new CDefLabel();
		label.setValue(labelstr);
		label.setZclass(className + "_label");
		setZclass(className + "_" + position + "_normal");
		appendChild(label);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunArea#setStatus(java.lang.String)
	 */
	@Override
	public void setStatus(String aStatus) {
		super.setStatus(aStatus);
		if ((status == null) || (status.equals("")))
			status = "empty";
		if (status.equals("empty"))
			label.setValue("");
		else
			label.setValue(labelstr);
		if (status.equals("selected"))
			setZclass(className + "_" + position + "_selected");
		else
			setZclass(className + "_" + position + "_normal");
	}
}
