/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit;

import java.util.ArrayList;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.ForwardEvent;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefGenericForwardComposer;

public class CRunPVToolkitSkillWheelDivForwardComposer extends CDefGenericForwardComposer {

	private static final long serialVersionUID = 6827868570337591823L;

	public void onWheelClick$skillWheelLevel1Div(Event event) {
		ForwardEvent forwardEvent = (ForwardEvent)event;
		Object[] data = (Object[])forwardEvent.getOrigin().getData();
		if (data != null && data.length == 2 && data[0] != null) {
			if (data[0].equals("onClick")) {
				String _idPrefix = "skillWheel";
				CRunPVToolkitSkillWheelLevel2Div level2Div = (CRunPVToolkitSkillWheelLevel2Div)CDesktopComponents.vView().getComponent(_idPrefix + "Level2Div");
				// NOTE event is fired multiple times
				if (!level2Div.isVisible()) {
					CDesktopComponents.vView().getComponent("dashboardDiv").setVisible(false);
					CRunPVToolkitSkillWheelLevel1Div level1Div = (CRunPVToolkitSkillWheelLevel1Div)CDesktopComponents.vView().getComponent(_idPrefix + "Level1Div");
					level1Div.initialOtherRgaIdsToFilterOn = new ArrayList<>(level1Div.otherRgaIdsToFilterOn);
					// NOTE: set componentTo._cycleNumber, because parameter in init method will be ignored for level 1 and level 2:
					level2Div._cycleNumber = level1Div._cycleNumber;
					if (level1Div.otherRgaIdsToFilterOn != null) {
						level2Div.init(level1Div._actor, level1Div._editable, level1Div._cycleNumber, 0, level1Div.otherRgaIdsToFilterOn);
					} else {
						// NOTE we have 'compare' situation; don't know if this can occur...
						level2Div.init(level1Div._actor, level1Div._editable, level1Div._cycleNumber, 0);
					}
					level2Div.setVisible(true);
				}
			}
		}
	}

	public void wheelClickLevel2(Event event, String _idPrefix) {
		ForwardEvent forwardEvent = (ForwardEvent)event;
		Object[] data = (Object[])forwardEvent.getOrigin().getData();
		if (data != null && data.length == 2 && data[0] != null) {
			if (data[0].equals("onClick")) {
				CRunPVToolkitSkillWheelLevel3Div level3Div = (CRunPVToolkitSkillWheelLevel3Div)CDesktopComponents.vView().getComponent(_idPrefix + "Level3Div");
				// NOTE event is fired multiple times
				if (!level3Div.isVisible()) {
					int skillClusterNumber = (int)data[1];
					CDesktopComponents.vView().getComponent(_idPrefix + "Level2Div").setVisible(false);
					CRunPVToolkitSkillWheelLevel2Div level2Div = (CRunPVToolkitSkillWheelLevel2Div)CDesktopComponents.vView().getComponent(_idPrefix + "Level2Div");
					if (level2Div.otherRgaIdsToFilterOn != null) {
						level3Div.init(level2Div._actor, level2Div._editable, level2Div._cycleNumber, skillClusterNumber, level2Div.otherRgaIdsToFilterOn);
					} else {
						// NOTE we have 'compare' situation
						level3Div.init(level2Div._actor, level2Div._editable, level2Div._cycleNumber, skillClusterNumber);
					}
					level3Div.setVisible(true);
				}
			}
		}
	}

	public void onWheelClick$skillWheelLevel2Div(Event event) {
		wheelClickLevel2(event, "skillWheel");
	}

	public void onWheelClick$compareSkillWheelLevel2Div(Event event){
		wheelClickLevel2(event, "compareSkillWheel");
	}

	public void onWheelClick$progressSkillWheelLevel2Div(Event event){
		wheelClickLevel2(event, "progressSkillWheel");
	}

	public void onWheelClick$progressCompareSkillWheelLevel2Div(Event event){
		wheelClickLevel2(event, "progressCompareSkillWheel");
	}

	public void onWheelClick$teacherDashboardSkillWheelLevel2Div(Event event){
		wheelClickLevel2(event, "teacherDashboardSkillWheel");
	}

	public void onWheelClick$teacherDashboardCompareSkillWheelLevel2Div(Event event){
		wheelClickLevel2(event, "teacherDashboardCompareSkillWheel");
	}

	public void onWheelClick$teacherStudentsProgressSkillWheelLevel2Div(Event event){
		wheelClickLevel2(event, "teacherStudentsProgressSkillWheel");
	}

	public void onWheelClick$teacherStudentsProgressCompareSkillWheelLevel2Div(Event event){
		wheelClickLevel2(event, "teacherStudentsProgressCompareSkillWheel");
	}

	public void onWheelClick$prepareSkillWheelLevel2Div(Event event){
		wheelClickLevel2(event, "prepareSkillWheel");
	}

	public void onWheelClick$prepareCompareSkillWheelLevel2Div(Event event){
		wheelClickLevel2(event, "prepareCompareSkillWheel");
	}

	public void onWheelClick$viewFeedbackSkillWheelLevel2Div(Event event){
		wheelClickLevel2(event, "viewFeedbackSkillWheel");
	}

	public void onWheelClick$viewFeedbackCompareSkillWheelLevel2Div(Event event){
		wheelClickLevel2(event, "viewFeedbackCompareSkillWheel");
	}

	public void onWheelClick$defineGoalsSkillWheelLevel2Div(Event event){
		wheelClickLevel2(event, "defineGoalsSkillWheel");
	}

	public void onWheelClick$defineGoalsCompareSkillWheelLevel2Div(Event event){
		wheelClickLevel2(event, "defineGoalsCompareSkillWheel");
	}

}
