/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl;

import org.zkoss.zk.ui.Component;

import nl.surf.emergo.control.run.CRunArea;
import nl.surf.emergo.control.run.CRunButton;
import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.control.run.CRunConversationInteraction;
import nl.surf.emergo.control.run.CRunConversations;
import nl.surf.emergo.control.run.CRunDragdropforms;
import nl.surf.emergo.control.run.CRunHbox;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunDragdropforms is used to show dragdropforms component within the run view area of the emergo player.
 */
public class CRunDragdropformsOunl extends CRunDragdropforms {

	private static final long serialVersionUID = 9005984633044781027L;

	/**
	 * Instantiates a new c run dragdropforms.
	 */
	public CRunDragdropformsOunl() {
		super();
	}

	/**
	 * Instantiates a new c run dragdropforms.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the case component
	 */
	public CRunDragdropformsOunl(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
	}

	/**
	 * Creates buttons area and adds close button.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run hbox
	 */
	@Override
	protected CRunHbox createButtonsArea(Component aParent) {
		CRunHbox lButtonsHbox = super.createButtonsArea(aParent);
		createCloseButton(lButtonsHbox);
		return null;
	}

	/**
	 * Creates title area and shows name of case component within it.
	 * And adds close button
	 *
	 * @param aParent the ZK parent
	 *
	 * @return the c run area
	 */
	@Override
	protected CRunArea createTitleArea(Component aParent) {
		CRunHbox lHbox = new CRunHbox();
		aParent.appendChild(lHbox);
		CRunComponentDecoratorOunl decorator = createDecorator();
		CRunArea lTitleArea = decorator.createTitleArea(caseComponent, lHbox, getClassName());
		decorator.createCloseArea(caseComponent, lHbox, getClassName(), getId(), this);
		return lTitleArea;
	}

	/**
	 * Creates decorator.
	 *
	 * @return the decorator
	 */
	protected CRunComponentDecoratorOunl createDecorator() {
		return new CRunComponentDecoratorOunl();
	}

	/**
	 * Creates close button.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run button
	 */
	@Override
	protected CRunButton createCloseButton(Component aParent) {
		// NOTE Don't create close button
		return null;
	}

	@Override
	protected CRunConversations getNewRunFeedbackConversations(String aConversationsId, String aConversationInteractionId) {
		return new CRunConversationsOunl(aConversationsId, aConversationInteractionId, this);
	}

	@Override
	protected CRunConversationInteraction getNewRunFeedbackConversationInteraction(String aConversationInteractionId, CRunComponent aConversations, String aConversationsId) {
		return  new CRunConversationInteractionOunl(aConversationInteractionId, aConversations, aConversationsId, notifyRunWnd);
	}
	
}
