/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.superTeacher;

import java.util.HashMap;
import java.util.Map;

import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.util.Clients;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.domain.IECaseComponent;

public class CRunPVToolkitSuperTeacherNewMethodDiv extends CRunPVToolkitSuperTeacherDiv {

	private static final long serialVersionUID = 3597273480171964958L;

	protected StringBuffer method;

	public void update() {
		//NOTE method is always stored in XML data and status that is cached within the SSpring class, regardless if preview is read only or not.
		IXMLTag nodeTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get("cacId")), _idMap.get("tagId"));
		if (nodeTag != null) {
			initMethod(nodeTag);
			
			boolean isNewMethod = true;
			if (isMethodInputOk(nodeTag)) {
				//TODO
				//NOTE if ok, it is an existing method, so number of performance levels may not be changed. Otherwise what to do with removed performance levels?
				isNewMethod = false;
			}
			
			StringBuffer[] rubricsContent = getContent(CRunPVToolkit.gCaseComponentNameRubrics, "rubric");
		
			Clients.evalJavaScript("initNewMethod('" + method.toString() + "'," + isNewMethod + ", '" + rubricsContent[0].toString() + "');");
		}
	}
		
	protected void initMethod(IXMLTag methodTag) {
		method = new StringBuffer();
		addElementPart(method, "name", sSpring.unescapeXML(methodTag.getChildValue("name")), true);
		addElementPart(method, "parentTagId", methodTag.getParentTag().getAttribute(AppConstants.keyId), true);
		addElementPart(method, "parentTagName", methodTag.getParentTag().getName(), true);
		addElementPart(method, "tagId", methodTag.getAttribute(AppConstants.keyId), true);
		addElementPart(method, "tagName", methodTag.getName(), true);
		IXMLTag rubricTag = pvToolkit.getReferencedNodeTag(sSpring.getCaseComponent(_idMap.get("cacId")), methodTag, "refrubric");
		IXMLTag skillTag = rubricTag == null ? null : rubricTag.getChild("skill");
		addElementPart(method, "rubricTagId", rubricTag == null ? "" : rubricTag.getAttribute(AppConstants.keyId), true);
		addElementPart(method, "rubricName", rubricTag == null ? "" : sSpring.unescapeXML(rubricTag.getChildValue("name")), true);
		addElementPart(method, "skillHasClusters", skillTag == null ? "false" : "" + (skillTag.getChilds("skillcluster").size() > 0), true);
		addElementPart(method, "numberOfCycles", "" + methodTag.getChilds("cycle").size(), true);
		addElementPart(method, "mayscoreskill", "" + pvToolkit.getMayScoreSkill(methodTag, "mayscoreskill"), true);
		addElementPart(method, "mayscoreskillforteacher", "" + pvToolkit.getMayScoreSkill(methodTag, "mayscoreskillforteacher"), true);
		addElementPart(method, "mayscoreskillforpeerstudent", "" + pvToolkit.getMayScoreSkill(methodTag, "mayscoreskillforpeerstudent"), true);
		addElementPart(method, "maytipsandtopsskill", "" + pvToolkit.getMayTipsAndTopsSkill(methodTag, "maytipsandtopsskill"), true);
		addElementPart(method, "maytipsandtopsskillforteacher", "" + pvToolkit.getMayTipsAndTopsSkill(methodTag, "maytipsandtopsskillforteacher"), true);
		addElementPart(method, "maytipsandtopsskillforpeerstudent", "" + pvToolkit.getMayTipsAndTopsSkill(methodTag, "maytipsandtopsskillforpeerstudent"), true);
		addElementPart(method, "feedbacklevelofscores", "" + pvToolkit.getFeedbackLevelOfScores(methodTag, "feedbacklevelofscores"), true);
		addElementPart(method, "feedbacklevelofscoresforteacher", "" + pvToolkit.getFeedbackLevelOfScores(methodTag, "feedbacklevelofscoresforteacher"), true);
		addElementPart(method, "feedbacklevelofscoresforpeerstudent", "" + pvToolkit.getFeedbackLevelOfScores(methodTag, "feedbacklevelofscoresforpeerstudent"), true);
		addElementPart(method, "feedbackleveloftipsandtops", "" + pvToolkit.getFeedbackLevelOfTipsAndTops(methodTag, "feedbackleveloftipsandtops"), true);
		addElementPart(method, "feedbackleveloftipsandtopsforteacher", "" + pvToolkit.getFeedbackLevelOfTipsAndTops(methodTag, "feedbackleveloftipsandtopsforteacher"), true);
		addElementPart(method, "feedbackleveloftipsandtopsforpeerstudent", "" + pvToolkit.getFeedbackLevelOfTipsAndTops(methodTag, "feedbackleveloftipsandtopsforpeerstudent"), true);
		addElementPart(method, "referenceidsforstudent", getStatusAttributeValue(methodTag, "referenceidsforstudent").replace(AppConstants.statusCommaReplace, ","), true);
		addElementPart(method, "referenceidsforteacher", getStatusAttributeValue(methodTag, "referenceidsforteacher").replace(AppConstants.statusCommaReplace, ","), true);
		addElementPart(method, "referenceidsforpeerstudent", getStatusAttributeValue(methodTag, "referenceidsforpeerstudent").replace(AppConstants.statusCommaReplace, ","), false);
		method.insert(0, "[{");
		method.append("}]");
	}
	
	public void onNotify (Event event) {
		JSONObject jsonObject = (JSONObject)event.getData();
		String action = (String)jsonObject.get("action");
		if (action.equals("edit_method")) {
			String childTagName = (String)jsonObject.get("childTagName");
			if (childTagName.equals("name")) {
				String elementName = (String)jsonObject.get("elementName");
				IXMLTag elementTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get("cacId")), _idMap.get("tagId"));
				if (elementTag != null) {
					editElementTag("method", elementTag, elementName);
				}
			}
			else {
				String childTagValue = (String)jsonObject.get("childTagValue");
				IXMLTag elementTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get("cacId")), _idMap.get("tagId"));
				if (elementTag != null) {
					editElementTagChildValue(elementTag, "method", childTagName, childTagValue);
				}
			}
		}
		else if (action.equals("select_rubric")) {
			String cacId = (String)jsonObject.get("cacId");
			String tagId = (String)jsonObject.get("tagId");
			if (!(cacId.equals("") || tagId.equals(""))) {
				IECaseComponent rubricsCaseComponent = sSpring.getCaseComponent(cacId);
				IXMLTag rubricTag = sSpring.getTag(rubricsCaseComponent, tagId);
				if (rubricTag != null) {
					IECaseComponent caseComponent = sSpring.getCaseComponent(_idMap.get("cacId"));
					IXMLTag tag = sSpring.getTag(caseComponent, _idMap.get("tagId"));
					pvToolkit.setReferencedNodeTag(caseComponent, tag, "refrubric", rubricsCaseComponent, rubricTag);
					update();
				}
			}
		}
		else if (action.equals("edit_score_or_tiptops_level")) {
			String statusAttributeKey = (String)jsonObject.get("statusAttributeKey");
			String statusAttributeValue = (String)jsonObject.get("statusAttributeValue");
			String childTagName = (String)jsonObject.get("childTagName");
			String childTagValue = (String)jsonObject.get("childTagValue");
			IXMLTag elementTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get("cacId")), _idMap.get("tagId"));
			if (elementTag != null) {
				if (!statusAttributeKey.equals("")) {
					editElementTagStatusAttributeValue(elementTag, "method", statusAttributeKey, statusAttributeValue);
				}
				else if (!childTagName.equals("")) {
					editElementTagChildValue(elementTag, "method", childTagName, childTagValue);
				}
			}
		}
		else if (action.equals("new_method_navigate_forward")) {
			String numberOfCyclesStr = (String)jsonObject.get("numberOfCycles");
			int numberOfCycles = 0;
			try {
				numberOfCycles = Integer.parseInt(numberOfCyclesStr);
			} catch (NumberFormatException e) {
			}
			IXMLTag nodeTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get("cacId")), _idMap.get("tagId"));
			if (numberOfCycles == 0) {
				Clients.evalJavaScript("showDefaultAlertPopup('" + vView.getLabel("PV-toolkit-superTeacher.newmethod.message.body.element.no_cycles_chosen") + "');");
				return;
			}
			addCycleTagsIfNotExisting(nodeTag, numberOfCycles);
			
			String toId = (String)jsonObject.get("toId");
			setVisible(false);
			CRunPVToolkitSuperTeacherDiv div = (CRunPVToolkitSuperTeacherDiv)CDesktopComponents.vView().getComponent(toId);
			if (div != null) {
				div.init(pvToolkit.getCurrentRunGroup(), true, _idMap);
			}
		}
		else if (action.equals("show_method")) {
			String numberOfCyclesStr = (String)jsonObject.get("numberOfCycles");
			int numberOfCycles = 0;
			try {
				numberOfCycles = Integer.parseInt(numberOfCyclesStr);
			} catch (NumberFormatException e) {
			}
			IXMLTag nodeTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get("cacId")), _idMap.get("tagId"));
			if (!isMethodInputOkWhenShowing(nodeTag) || numberOfCycles == 0) {
				Clients.evalJavaScript("showDefaultAlertPopup('" + vView.getLabel("PV-toolkit-superTeacher.newmethod.message.body.element.incomplete") + "');");
				return;
			}
			addCycleTagsIfNotExisting(nodeTag, numberOfCycles);

			setVisible(false);
			CRunPVToolkitSuperTeacherDiv div = (CRunPVToolkitSuperTeacherDiv)CDesktopComponents.vView().getComponent("superTeacherUpdateMethodDiv");
			if (div != null) {
				div.init(pvToolkit.getCurrentRunGroup(), true, _idMap);
			}
		}
		else {
			super.onNotify(event);
		}
	}
	
	protected boolean isMethodInputOkWhenShowing(IXMLTag tag) {
		IXMLTag rubricTag = pvToolkit.getReferencedNodeTag(sSpring.getCaseComponent(_idMap.get("cacId")), tag, "refrubric");
		if (tag == null || tag.getChildValue("name").equals("") || rubricTag == null) {
			//name is empty or no rubric is chosen or number of cycles is 0
			return false;
		}
		return true;
	}
	
	protected void addCycleTagsIfNotExisting(IXMLTag methodTag, int numberOfCycles) {
		if (numberOfCycles > 0 && methodTag.getChilds("cycle").size() == 0) {
			//NOTE if no cycle children yet add cycle tags and step tags within them, and substep tags within the step tags.
			for (int i=0;i<numberOfCycles;i++) {
				Map<String,String> childTagNameValueMap = new HashMap<String,String>();
				childTagNameValueMap.put("pid", "Ronde " + (i + 1));
				childTagNameValueMap.put("name", "Ronde " + (i + 1));
				addElementTag(methodTag.getAttribute(AppConstants.keyId), "method", "cycle", childTagNameValueMap);
			}
		}
	}
	
}
