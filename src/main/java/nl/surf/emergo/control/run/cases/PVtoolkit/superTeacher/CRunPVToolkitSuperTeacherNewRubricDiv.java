/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.superTeacher;

import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.util.Clients;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;

public class CRunPVToolkitSuperTeacherNewRubricDiv extends CRunPVToolkitSuperTeacherDiv {

	private static final long serialVersionUID = -2097983482058412199L;

	protected StringBuffer skill;

	public void update() {
		//NOTE rubric is always stored in XML data and status that is cached within the SSpring class, regardless if preview is read only or not.
		IXMLTag nodeTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get("cacId")), _idMap.get("tagId"));
		if (nodeTag != null) {
			initSkill(nodeTag);
			
			boolean isNewRubric = true;
			if (isRubricInputOk(nodeTag)) {
				//NOTE if ok, it is an existing rubric, so number of performance levels may not be changed. Otherwise what to do with removed performance levels?
				isNewRubric = false;
			}
		
			Clients.evalJavaScript("initNewRubric('" + skill.toString() + "'," + isNewRubric + ");");
		}
	}
		
	protected void initSkill(IXMLTag rubricTag) {
		skill = new StringBuffer();
		addElementPart(skill, "name", sSpring.unescapeXML(rubricTag.getChildValue("name")), true);
		IXMLTag skillTag = rubricTag.getChild("skill");
		boolean hasSkillClustersOrSubSkills = (
				skillTag != null &&
				//it has a skill child
				(skillTag.getChilds("skillcluster").size() > 0 || skillTag.getChilds("subskill").size() > 0)
				//the skill child has skill cluster or sub skill children
				);
		addElementPart(skill, "hasSkillClustersOrSubSkills", "" + hasSkillClustersOrSubSkills, true);
		addElementPart(skill, "numberofperformancelevels", rubricTag.getChildValue("numberofperformancelevels"), true);
		addElementPart(skill, "performancelevelsdescending", "" + rubricTag.getChildValue("performancelevelsdescending").equals(AppConstants.statusValueTrue), false);
		skill.insert(0, "[{");
		skill.append("}]");
	}
	
	public void onNotify (Event event) {
		JSONObject jsonObject = (JSONObject)event.getData();
		String action = (String)jsonObject.get("action");
		if (action.equals("edit_rubric")) {
			String childTagName = (String)jsonObject.get("childTagName");
			if (childTagName.equals("name")) {
				String elementName = (String)jsonObject.get("elementName");
				IXMLTag elementTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get("cacId")), _idMap.get("tagId"));
				if (elementTag != null) {
					editElementTag("rubric", elementTag, elementName);
				}
			}
			else {
				String childTagValue = (String)jsonObject.get("childTagValue");
				IXMLTag elementTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get("cacId")), _idMap.get("tagId"));
				if (elementTag != null) {
					editElementTagChildValue(elementTag, "rubric", childTagName, childTagValue);
				}
			}
		}
		else if (action.equals("show_rubric")) {
			IXMLTag nodeTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get("cacId")), _idMap.get("tagId"));
			if (!isRubricInputOk(nodeTag)) {
				Clients.evalJavaScript("showDefaultAlertPopup('" + vView.getLabel("PV-toolkit-superTeacher.newrubric.message.body.element.incomplete") + "');");
				return;
			}
			setVisible(false);
			CRunPVToolkitSuperTeacherDiv div = (CRunPVToolkitSuperTeacherDiv)CDesktopComponents.vView().getComponent("superTeacherUpdateRubricDiv");
			if (div != null) {
				div.init(pvToolkit.getCurrentRunGroup(), true, _idMap);
			}
		}
		else {
			super.onNotify(event);
		}
	}
	
}
