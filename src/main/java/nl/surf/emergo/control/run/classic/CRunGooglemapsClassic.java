/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Iframe;

import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefIframe;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;

/**
 * The Class CRunGooglemaps is used to show google maps within the run view area of the
 * Emergo player.
 */
public class CRunGooglemapsClassic extends CRunComponentClassic {

	private static final long serialVersionUID = 5133115041686478958L;

	/**
	 * Instantiates a new c run google maps.
	 * 
	 * @param aId the a id
	 * @param aCaseComponent the google maps case component
	 */
	public CRunGooglemapsClassic(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
	}

	/**
	 * Creates title area, content area to show google maps and buttons area with close button.
	 */
	@Override
	protected void createComponents() {
		CRunVboxClassic lVbox = new CRunVboxClassic();

		createTitleArea(lVbox);
		createContentArea(lVbox);
		createButtonsArea(lVbox);

		appendChild(lVbox);
	}

	/**
	 * Creates new content component, the iframe containing google maps.
	 * Set session vars which are used by google maps within the iframe.
	 * 
	 * @return the component
	 */
	@Override
	protected Component newContentComponent() {
		CControl cControl = CDesktopComponents.cControl();
		cControl.setRunSessAttr("casecomponent", caseComponent);
		cControl.setRunSessAttr("runcomponent", this);
		cControl.setRunSessAttr("sspring", CDesktopComponents.sSpring());

		Iframe lIframe = new CDefIframe();
//		lIframe.setStyle("width:950px;height:434px;border:1px solid darkgray;margin: 0px 16px 0px 16px;vertical-align:middle;background-color:white;");
		lIframe.setSrc(VView.v_run_gmaps_fr);
		return lIframe;
	}

	/**
	 * Creates buttons area and adds close button.
	 * 
	 * @param aParent the a parent
	 * 
	 * @return the c run hbox
	 */
	@Override
	protected CRunHboxClassic createButtonsArea(Component aParent) {
		CRunHboxClassic lButtonsHbox = super.createButtonsArea(aParent);
		createCloseButton(lButtonsHbox);
		return lButtonsHbox;
	}

}
