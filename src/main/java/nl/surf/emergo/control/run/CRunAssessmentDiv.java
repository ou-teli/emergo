/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Event;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;

public class CRunAssessmentDiv extends CDefDiv {

	private static final long serialVersionUID = -2816315088541380786L;

	protected CRunAssessmentsContentDiv root;

	public void onInit(Event aEvent) {
		root = (CRunAssessmentsContentDiv)CDesktopComponents.vView().getComponent(CRunAssessmentsContentDiv.runAssessmentsZulId);
		HtmlMacroComponent macro = new HtmlMacroComponent();
		appendChild(macro);
		macro.setId(CRunAssessmentsContentDiv.macroAssessmentId);
		macro.setDynamicProperty("a_observer", CRunAssessmentsContentDiv.runAssessmentDivId);
		macro.setDynamicProperty("a_assessments_title", root.assessmentsTitle);
		macro.setDynamicProperty("a_assessment_title", root.assessmentTitle);
		macro.setDynamicProperty("a_label_style_shift", root.labelStyleShift);
		macro.setDynamicProperty("a_assessment_instruction", root.assessmentInstruction);
		macro.setDynamicProperty("a_assessment_can_be_started", root.assessmentCanBeStarted);
		macro.setDynamicProperty("a_assessment_can_be_restarted", root.assessmentCanBeRestarted);
		macro.setDynamicProperty("a_assessment_can_be_resumed", root.assessmentCanBeResumed);
		macro.setDynamicProperty("a_assessment_can_be_inspected", root.assessmentCanBeInspected);
		macro.setDynamicProperty("a_assessment_can_be_finished", root.assessmentCanBeFinished);
		macro.setDynamicProperty("a_assessment_show_score", root.assessmentShowScore);
		macro.setDynamicProperty("a_assessment_score", root.assessmentScore);
		macro.setDynamicProperty("a_assessment_show_item_buttons", root.assessmentShowItemButtons);
		macro.setDynamicProperty("a_assessment_assfeedback_text", root.assessmentFeedbackText);
		macro.setDynamicProperty("a_assessment_assfeedback_pieces", root.assessmentFeedbackPieces);
		macro.setDynamicProperty("a_assessment_show_feedback", root.assessmentShowFeedback);
		macro.setDynamicProperty("a_assessment_datas", root.assessmentDatas);
		macro.setDynamicProperty("a_assessment_multiple_assessments", root.multipleAssessments);
		macro.setMacroURI("../run_assessmentpanel_macro.zul");
	}

	public void onChooseAssessmentItem(Event aEvent) {
		if (root.currentEmergoComponent != null) {
			root.currentEmergoComponent.handleAssessmentItem((String)aEvent.getData());
		}
		setVisible(false);
	}

	public void onStartAssessment(Event aEvent) {
		if (root.currentEmergoComponent != null) {
			root.currentEmergoComponent.startAssessment();
		}
		setVisible(false);
	}

	public void onRestartAssessment(Event aEvent) {
		if (root.currentEmergoComponent != null) {
			root.currentEmergoComponent.restartAssessment();
		}
		setVisible(false);
	}

	public void onResumeAssessment(Event aEvent) {
		if (root.currentEmergoComponent != null) {
			root.currentEmergoComponent.resumeAssessment();
		}
		setVisible(false);
	}

	public void onInspectAssessment(Event aEvent) {
		if (root.currentEmergoComponent != null) {
			root.currentEmergoComponent.inspectAssessment();
		}
		setVisible(false);
	}

	public void onFinishAssessment(Event aEvent) {
		if (root.currentEmergoComponent != null) {
			root.currentEmergoComponent.finishAssessment();
		}
	}

	public void onUpdateAssessments(Event aEvent) {
		// update assessments div
		if (root.currentEmergoComponent != null) {
			root.currentEmergoComponent.updateAssessments();
		}
		setVisible(false);
		CDesktopComponents.vView().getComponent(CRunAssessmentsContentDiv.runAssessmentItemDivId).setVisible(false);
		CDesktopComponents.vView().getComponent(CRunAssessmentsContentDiv.runAssessmentsDivId).setVisible(true);
	}

}
