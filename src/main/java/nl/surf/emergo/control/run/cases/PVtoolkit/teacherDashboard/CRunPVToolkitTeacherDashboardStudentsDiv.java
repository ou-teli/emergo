/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.teacherDashboard;

import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.dashboard.CRunPVToolkitDashboardPeerGroupsDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefButton;
import nl.surf.emergo.domain.IERunGroup;

public class CRunPVToolkitTeacherDashboardStudentsDiv extends CRunPVToolkitDashboardPeerGroupsDiv {

	private static final long serialVersionUID = 165813294592449329L;

	@Override
	public void init(IERunGroup actor, boolean editable, List<IXMLTag> commonPeerGroupTags) {
		_idPrefix = "teacherDashboard";
		_classPrefix = "dashboardStudents";
		
		super.init(actor, editable, commonPeerGroupTags);
	}

	@Override
	protected boolean excludeTeachersFeedback() {
		return true;
	}

	@Override
	protected boolean excludeStudentAssistantsFeedback() {
		return true;
	}

	@Override
	protected String getTitleLabelKey() {
		return "PV-toolkit-dashboard.header.students";
	}

	@Override
	public void update() {
		super.update();

		Component btn = new CRunPVToolkitDefButton(this, 
				new String[]{"class", "cLabelKey"}, 
				new Object[]{"font pvtoolkitButton " + _classPrefix + "DetailsButton", "PV-toolkit-dashboard.button.details"}
				);
		addDetailsOnClickEventListener(btn);
	}

	protected void addDetailsOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				CDesktopComponents.vView().getComponent(_idPrefix + "Div").setVisible(false);
				CRunPVToolkitTeacherDashboardStudentsDetailsDiv div = (CRunPVToolkitTeacherDashboardStudentsDetailsDiv)CDesktopComponents.vView().getComponent(_idPrefix + "StudentsDetailsDiv");
				//NOTE set visible true before initialization otherwise progress bars will not be filled
				div.setVisible(false);
				div.setVisible(true);
				div.init(_actor, true, _commonPeerGroupTags);
			}
		});
	}

}
