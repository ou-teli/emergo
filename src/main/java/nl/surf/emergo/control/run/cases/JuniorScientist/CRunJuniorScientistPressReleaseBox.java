/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.JuniorScientist;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.zkspring.STriggeredReference;

/**
 * This class is used to show both a press release and a press release app. It uses content entered within an editforms case component: per worry question, a paragraph for the question, a selector and a paragraph for a tooltip.
 * The used editforms case component should have present=false and present should stay false, to prevent rendering of the original editforms component on the tablet.
 * NOTE that for a paragraph only its pid and default text are used and for a selector only its pid, options and correct options. Other input like, e.g., position and size, is ignored, so does not have to be entered by a case developer.
 */
public class CRunJuniorScientistPressReleaseBox extends CRunJuniorScientistBox {

	private static final long serialVersionUID = -4244786148827208530L;

	protected final static String pressReleaseCaseComponentName = "phase_0_press_release";

	protected IECaseComponent pressReleaseAppCaseComponent;
	
	protected List<IXMLTag> selectorTags;
	/** Prefixes of pids entered by a case developer. */
	protected static final String questionPidPrefix = "worry_question_";
	protected static final String selectorPidPrefix = "worry_selector_";
	protected static final String tooltipPidPrefix = "worry_tooltip_";

	protected boolean pressReleaseVisible = true;
	protected boolean pressReleaseAppVisible = false;
	
	/** To keep the number of correctly answered questions. */
	int numberOfCorrectAnswers = 0;

	@Override
	public void onInit() {
		super.onInitMacro();
		
		if (isShowstatic()) {
			pressReleaseVisible = false;
			pressReleaseAppVisible = true;
		}
		
		propertyMap.put("pressReleaseVisible", pressReleaseVisible);
		propertyMap.put("pressReleaseAppVisible", pressReleaseAppVisible);

		pressReleaseAppCaseComponent = getPressReleaseAppCaseComponent();
		//NOTE super ancestor CRunComponent needs case component to be set
		setCaseComponent(pressReleaseAppCaseComponent);
		
		//just like for the original editforms component set selected and opened
		setRunComponentStatusJS(pressReleaseAppCaseComponent, AppConstants.statusKeySelected, AppConstants.statusValueTrue);
		setRunComponentStatusJS(pressReleaseAppCaseComponent, AppConstants.statusKeyOpened, AppConstants.statusValueTrue);

		//determine press release app properties to be used within the child macro
		setPressReleaseAppProperties();

		//add the child macro
		addChildMacro("JuniorScientist_press_release_view_macro.zul");
		
		if (!isShowstatic()) {
			//NOTE a conversation is used for instruction or later on, for feedback
			showConversation("phase_0_conversations", "0_press_release_instruction");
		}
	}
	
	@Override
	public void onUpdate() {
		//NOTE not used yet
		//rerender child macro with adjusted properties
		childMacro.recreate();
	}

	public void setPressReleaseAppProperties() {
		selectorTags = new ArrayList<IXMLTag>();
		
		List<Map<String,Object>> questions = new ArrayList<Map<String,Object>>();
		propertyMap.put("questions", questions);
		IXMLTag rootTag = sSpring.getXmlDataPlusRunStatusTree(pressReleaseAppCaseComponent, AppConstants.statusTypeRunGroup);
		if (rootTag == null) {
			return;
		}
		IXMLTag contentTag = rootTag.getChild(AppConstants.contentElement);
		if (contentTag == null) {
			return;
		}
		IXMLTag formTag = contentTag.getChild("form");
		if (formTag == null) {
			return;
		}

		//put all node tags in a map per pid to be able to get them by number later on
		Map<String,IXMLTag> nodeTagsByPid = new HashMap<String,IXMLTag>();
		for (IXMLTag nodeTag : cScript.getChildNodeTags(formTag)) {
			nodeTagsByPid.put(nodeTag.getChildValue("pid"), nodeTag);
		}
		int counter = 1;
		while (nodeTagsByPid.containsKey(questionPidPrefix + counter)) {
			//content to be rendered is entered in three content elements per worry question
			IXMLTag questionTag = nodeTagsByPid.get(questionPidPrefix + counter);
			IXMLTag selectorTag = nodeTagsByPid.get(selectorPidPrefix + counter);
			IXMLTag tooltipTag = nodeTagsByPid.get(tooltipPidPrefix + counter);
			
			selectorTags.add(selectorTag);
			
			Map<String,Object> hData = new HashMap<String,Object>();
			hData.put("selectorTag", selectorTag);
			hData.put("counter", counter);
			hData.put("question", sSpring.unescapeXML(questionTag.getChildValue("defaulttext")));
			hData.put("tooltip", sSpring.unescapeXML(tooltipTag.getChildValue("defaulttext")));

			//get default or given answer
			String answer = getAnswer(selectorTag);
			hData.put("answer", answer);

			//first option in selector is '-'
			boolean isAnswerEmpty = answer.equals("-");
			//is answer given correct
			boolean isAnswerOk = isAnswerOk(selectorTag, answer);
			if (isAnswerOk) {
				hData.put("ok", true);
				numberOfCorrectAnswers++;
			}
			else if (!isAnswerEmpty) {
				hData.put("notOk", true);
			}

			hData.put("disabled", isAnswerOk);

			List<Map<String,Object>> questionOptions = new ArrayList<Map<String,Object>>();
			hData.put("options", questionOptions);

			//get options within selector
			List<String> options = Arrays.asList(selectorTag.getChildValue("options").split("\n"));
			int optionCounter = 1;
			int selectedIndex = 0;
			for (String option : options) {
				Map<String,Object> hOptions = new HashMap<String,Object>();
				hOptions.put("option", option);
				hOptions.put("optionCounter", optionCounter);
				//preselect given answer
				if (("" + optionCounter).equals(answer)) {
					selectedIndex = optionCounter - 1;
				}
				questionOptions.add(hOptions);
				optionCounter++;
			}
			hData.put("selectedIndex", selectedIndex);

			questions.add(hData);
			
			counter++;
		}
	}
	
	protected String getAnswer (IXMLTag selectorTag) {
		String answer = selectorTag.getCurrentStatusAttribute(AppConstants.statusKeyAnswer);
		if (answer.equals("")) {
			//default answer
			answer = "-";
		}
		return answer;
	}	

	protected boolean isAnswerOk (IXMLTag selectorTag, String answer) {
		//correct options are entered in selector tag
		Pattern p = Pattern.compile(selectorTag.getChildValue("correctoptions"), Pattern.DOTALL);
		Matcher m = p.matcher(answer);
		return m.matches();
	}

	protected IECaseComponent getPressReleaseAppCaseComponent() {
		return sSpring.getCaseComponent("", pressReleaseCaseComponentName);
	}
	
	public void onButtonClick(Event event) {
		//NOTE user clicks button to continue
		if (event.getData().equals("to_press_release_app")) {
			//hide possible conversation
			hideConversation();

			//hide press release and show press release app
			vView.getComponent(getUuid() + "_press_release").setVisible(false);
			vView.getComponent(getUuid() + "_press_release_app").setVisible(true);
			pressReleaseVisible = false;
			pressReleaseAppVisible = true;
			propertyMap.put("pressReleaseVisible", pressReleaseVisible);
			propertyMap.put("pressReleaseAppVisible", pressReleaseAppVisible);

			//show press release app instruction
			showConversation("phase_0_conversations", "0_press_release_app_instruction");
		}
		else if (event.getData().equals("press_release_app_completed")) {
			//hide possible conversation
			hideConversation();
			
			if (numberOfCorrectAnswers < selectorTags.size()) {
				//if not yet ready show feedback
				showConversation("phase_0_conversations", "0_press_release_app_not_ready");
			}
			else {
				//just like for the original editforms component set opened
				setRunComponentStatusJS(pressReleaseAppCaseComponent, AppConstants.statusKeyOpened, AppConstants.statusValueFalse);
				if (currentTag.getName().equals("macro")) {
					//NOTE if macro zul file is used within the junior scientist references component, the case component is a references component and the tag is a piece tag, so don't set present
					//hide press release macro
					setRunTagStatusJS(getNavigationCaseComponent(), currentTag, AppConstants.statusKeyPresent, AppConstants.statusValueFalse, false);
				}

				//set game state for phase 0
				setGameStateValue(0, 5);
				//show conversation after press release
				//NOTE I tried to use game script to show this conversation that was triggered if the game state became 5. However I then had to use a timer to show this conversation, resulting in a blank screen just before the conversation.
				//So better show conversation right away from this component
				showConversation("phase_0_conversations", "0_meeting_end");
			}
		}
	}
	
	public void onListitemSelected(Event event) {
		//a user selects an item in a combobox
		Listitem listitem = (Listitem)event.getData();
		String answer = "" + listitem.getValue();
		Listbox listbox = (Listbox)listitem.getParent();
		IXMLTag selectorTag = (IXMLTag)listbox.getAttribute("selectorTag");
		int counter = (int)listbox.getAttribute("counter");
		boolean isAnswerOk = isAnswerOk(selectorTag, answer);
		//just like for the original editforms component set following states
		setRunTagStatusJS(getPressReleaseAppCaseComponent(), selectorTag, AppConstants.statusKeyAnswer, answer, false);
		setRunTagStatusJS(getPressReleaseAppCaseComponent(), selectorTag, AppConstants.statusKeySelected, AppConstants.statusValueTrue, false);
		setRunTagStatusJS(getPressReleaseAppCaseComponent(), selectorTag, AppConstants.statusKeyCorrect, "" + isAnswerOk, false);
		
		//show the correct icon depending on the answer given
		vView.getComponent(getUuid() + "_press_release_app_ok_icon_" + counter).setVisible(isAnswerOk);
		vView.getComponent(getUuid() + "_press_release_app_not_ok_icon_" + counter).setVisible(!isAnswerOk);
		if (isAnswerOk) {
			numberOfCorrectAnswers++;
			
			IXMLTag gameStateTag = getStateTag("game_0_press_release_app_state");
			if (gameStateTag != null) {
				setRunTagStatusJS(getStatesCaseComponent(), gameStateTag, AppConstants.statusKeyValue, "" + numberOfCorrectAnswers, false);
			}

			if (numberOfCorrectAnswers >= selectorTags.size()) {
				IXMLTag formTag = selectorTag.getParentTag();
				//just like for the original editforms component set correct for the form if all input is correct
				setRunTagStatusJS(pressReleaseAppCaseComponent, formTag, AppConstants.statusKeyCorrect, AppConstants.statusValueTrue, false);
			}

			//disable the current question
			listbox.setDisabled(true);
			if (counter < selectorTags.size()) {
				//enable the next question
				Listbox nextListbox = (Listbox)vView.getComponent(getUuid() + "_press_release_app_listbox_" + (counter + 1));
				nextListbox.setDisabled(false);
			}
			///show feedback for question. It is only given if the answer is correct
			showConversation("phase_0_conversations", "0_press_release_worry_" + counter);
		}
		else {
			///show a default feedback if the answer is wrong
			showConversation("phase_0_conversations", "0_press_release_app_wrong_answer");
		}
	}
	
	@Override
	protected void handleEvent(Event event) {
		STriggeredReference triggeredReference = (STriggeredReference)event.getData();
		if (triggeredReference.getCaseComponent().getName().equals("phase_0_conversations") &&
				triggeredReference.getDataTag().getName().equals("conversation") &&
				triggeredReference.getDataTag().getChildValue("pid").matches("0_press_release_worry_(.*)") &&
				triggeredReference.getStatusKey().equals(AppConstants.statusKeyOpened) &&
				triggeredReference.getStatusValue().equals(AppConstants.statusValueFalse)) {
			//NOTE if worry question correct conversation is closed
			if (numberOfCorrectAnswers >= selectorTags.size()) {
				showConversation("phase_0_conversations", "0_press_release_app_end");
			}			
		}
	}
	
	@Override
	public void handleStatusChange(STriggeredReference aTriggeredReference) {
	}

}
