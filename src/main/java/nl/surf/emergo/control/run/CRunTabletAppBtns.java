/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.zkoss.zk.ui.Component;

import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IEComponent;

/**
 * The Class CRunTabletAppBtns. Used to show tablet app buttons within tablet.
 * Clicking a tablet app button, opens the corresponding case component within the tablet.
 */
public class CRunTabletAppBtns extends CRunChoiceHoverBtns {

	private static final long serialVersionUID = 4306003050684470247L;

	/**
	 * Instantiates a new c run tablet app btns.
	 */
	public CRunTabletAppBtns() {
		super(null, "runTabletAppBtns", false);
	}

	/**
	 * Instantiates a new c run tablet app btns.
	 *
	 * @param aId the a id
	 * @param aParent the a parent
	 */
	public CRunTabletAppBtns(Component aParent, String aId) {
		super(aParent, aId, false);
	}

	protected class CaseComponentSortByName implements Comparator<IECaseComponent>{
		public int compare(IECaseComponent o1, IECaseComponent o2) {
			return o1.getName().compareTo(o2.getName());
		}
	}
	
	/**
	 * Updates hover buttons. Shows all tablet app buttons having the correct status.
	 */
	@Override
	public void update() {
		int counter = 0;
		for (Component lChild : buttonHboxList) {
			for (Component lComp : lChild.getChildren()) {
				if (lComp instanceof CRunHoverBtn) {
					CRunHoverBtn lBtn = (CRunHoverBtn)lComp;
					lBtn.detach();
				}
			}
			if (counter > 0) {
				lChild.detach();
			}
			counter ++;
		}
		if (runWnd == null)
			return;
		List<IECaseComponent> lTabletAppComponents = runWnd.getPlayerStatusList("TabletAppComponents", null);
		if (lTabletAppComponents == null)
			return;
		Collections.sort((List<IECaseComponent>)lTabletAppComponents, new CaseComponentSortByName());

		int lVisibleCount = 0;
		counter = 0;
		for (IECaseComponent lCaseComponent : lTabletAppComponents) {
			String lId = "" + lCaseComponent.getCacId();
			String lName = lCaseComponent.getName();
			String lShowName = CDesktopComponents.sSpring().getCaseComponentRoleName("", lName);
			String lStatus = getBtnStatus(lCaseComponent);
			IEComponent lComponent = lCaseComponent.getEComponent();
			String lCode = lComponent.getCode();
			List<String> lCacIdAndTagId = new ArrayList<String>();
			lCacIdAndTagId.add(lId);
			lCacIdAndTagId.add("0");
			CRunHoverBtn lBtn = newHoverBtn(runWnd.getTabletAppBtnId(lCaseComponent), lStatus, "showTabletApp", lCacIdAndTagId, lCode, lShowName);
			boolean lAccessible = ((CDesktopComponents.sSpring().getCurrentRunComponentStatus(
					lCaseComponent, AppConstants.statusKeyAccessible, AppConstants.statusTypeRunGroup)).equals(AppConstants.statusValueTrue));
			lBtn.setAttribute("accessible", "" + lAccessible);
			if (!useArrows && counter == 8) {
				buttonHbox = newButtonHbox();
				counter = 0;
			}
			buttonHbox.appendChild(lBtn);
			if (!lStatus.equals("empty")) {
				lVisibleCount ++;
				counter ++;
			}
		}
		setItemCount(lVisibleCount);
		showArrows(true);
		firstUpdate = false;
	}

	/**
	 * Creates new hover btn.
	 *
	 * @param aId the a id
	 * @param aStatus the a status the button should get
	 * @param aAction the a action to be send when clicked
	 * @param aActionStatus the a action status of the action
	 * @param aPosition the a position, where on the screen the button appears for layout
	 * @param aImgPrefix the a img prefix of the hover images
	 * @param aLabel the a label to be shown above the button
	 *
	 * @return the c run hover btn
	 */
	protected CRunHoverBtn createHoverBtn(String aId, String aStatus, String aAction,
			Object aActionStatus, String aImgPrefix, String aLabel) {
		return new CRunTabletAppBtn(aId, aStatus, aAction, aActionStatus, aImgPrefix, aLabel, "");
	}

	/**
	 * Gets the btn status for the button identified by aCaseComponent.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return the btn status
	 */
	protected String getBtnStatus(IECaseComponent aCaseComponent) {
		boolean lPresent = ((CDesktopComponents.sSpring().getCurrentRunComponentStatus(
				aCaseComponent, AppConstants.statusKeyPresent, AppConstants.statusTypeRunGroup))
				.equals(AppConstants.statusValueTrue));
		boolean lAccessible = ((CDesktopComponents.sSpring().getCurrentRunComponentStatus(
				aCaseComponent, AppConstants.statusKeyAccessible, AppConstants.statusTypeRunGroup))
				.equals(AppConstants.statusValueTrue));
		boolean lOpened = ((CDesktopComponents.sSpring().getCurrentRunComponentStatus(
				aCaseComponent, AppConstants.statusKeyOpened, AppConstants.statusTypeRunGroup))
				.equals(AppConstants.statusValueTrue));
		// none of the empack action buttons not is pre-selected
		lOpened = false;
		return getBtnStatus(lPresent, lAccessible, lOpened);
	}

}
