/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.superTeacher;

import java.util.Date;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Image;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Timebox;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDatebox;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.def.CDefHelper;
import nl.surf.emergo.control.def.CDefTimebox;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitInitBox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefCombobox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefComboitem;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTag;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTags;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.view.VView;

public class CRunPVToolkitSuperTeacherNewOrEditMessageDiv extends CDefDiv {

	private static final long serialVersionUID = 2143980347693059343L;

	protected VView vView = CDesktopComponents.vView();
	
	protected String zulfilepath = ((CRunPVToolkitInitBox)vView.getComponent("PV-toolkit_initBox")).getZulfilepath();
	
	protected CRunPVToolkit pvToolkit;
	
	protected CRunPVToolkitSuperTeacherMessagesDiv messagesDiv;

	protected IXMLTag _stepMessageTag;
	
	protected List<IERunGroupAccount> runGroupAccounts;
	
	protected IAccountManager accountManager = (IAccountManager)CDesktopComponents.sSpring().getBean("accountManager");

	//NOTE isPreviewRun is true if preview option in authoring environment is used
	protected boolean isPreviewRun = false;
	
	protected String _idPrefix = "superTeacherNewOrEdit";
	protected String _classPrefix = "superTeacherNewOrEdit";
	
	public void init(IXMLTag stepMessageTag) {
		_stepMessageTag = stepMessageTag;
		
		pvToolkit = (CRunPVToolkit)vView.getComponent("pvToolkit");
		
		messagesDiv = (CRunPVToolkitSuperTeacherMessagesDiv)vView.getComponent("superTeacherMessagesDiv");
		
		update();
		
		setVisible(true);
	}
	
	public void update() {
		
		getChildren().clear();
		
		pvToolkit.setMemoryCaching(true);
		
		runGroupAccounts = pvToolkit.getActiveRunGroupAccounts();
		isPreviewRun = CDesktopComponents.sSpring().getRun().getStatus() == AppConstants.run_status_test;

		new CRunPVToolkitDefImage(this, 
				new String[]{"class"}, 
				new Object[]{"popupBackground"}
		);

		new CRunPVToolkitDefImage(this, 
				new String[]{"class", "src"}, 
				new Object[]{_classPrefix + "MessageBackground", zulfilepath + "popup-large-background.svg"}
		);

		Div div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "MessageTitle"}
		);
		
		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font " + _classPrefix + "MessageTitle", "PV-toolkit-superTeacher.header.message"}
		);
		
		Image closeImage = new CRunPVToolkitDefImage(this, 
				new String[]{"class", "src"}, 
				new Object[]{_classPrefix + "MessageCloseButton", zulfilepath + "close.svg"}
		);
		addCloseButtonOnClickEventListener(closeImage);

		Button btn = new CRunPVToolkitDefButton(div, 
				new String[]{"id", "class", "cLabelKey"}, 
				new Object[]{_idPrefix + "MessageSaveButton", "font pvtoolkitButton " + _classPrefix + "MessageSaveButton", "PV-toolkit.save"}
		);
		addSaveButtonOnClickEventListener(btn);


		div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "MessageDiv"}
		);
		
		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font " + _classPrefix + "MessageMessageTitle", "PV-toolkit-superTeacher.column.label.message"}
		);
		
		addMessageCombobox(div);
		
		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font " + _classPrefix + "MessageDateTimeTitle", "PV-toolkit-superTeacher.column.label.messageDateTime"}
		);
		
		addMessageDateTimebox(div);
		
		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font " + _classPrefix + "MessageStepTitle", "PV-toolkit-superTeacher.column.label.step"}
		);
		
		addStepCombobox(div);

		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font " + _classPrefix + "MessageDeadlineTitle", "PV-toolkit-superTeacher.column.label.deadline"}
		);
		
		addDeadlineDateTimebox(div);
		
		pvToolkit.setMemoryCaching(false);
	}

	protected void addCloseButtonOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				Component componentToHide = vView.getComponent(_idPrefix + "MessageDiv");
				if (componentToHide != null) {
					componentToHide.setVisible(false);
				}
			}
		});
	}

	public void addMessageCombobox(Component parent) {
		CRunPVToolkitCacAndTags messages = pvToolkit.getMessages();

		String chosenMessageCacId = "";
		String chosenMessageTagId = "";
		if (_stepMessageTag != null) {
			chosenMessageCacId = pvToolkit.getStatusChildTagAttribute(_stepMessageTag, "messagecacid");
			chosenMessageTagId = pvToolkit.getStatusChildTagAttribute(_stepMessageTag, "messagetagid");
		}
		Combobox combobox = new CRunPVToolkitDefCombobox(parent, 
				new String[]{"id", "width"}, 
				new Object[]{_idPrefix + "MessageMessageCombobox", "300px"}
		);
		combobox.setClass("font " + _classPrefix + "MessageMessageCombobox");
		Comboitem comboitem = new CRunPVToolkitDefComboitem(combobox, 
				new String[]{"labelKey"}, 
				new Object[]{"PV-toolkit.none"}
		);
		comboitem.setClass("font " + _classPrefix + "MessageMessageComboitem");
		int selectedIndex = 0;
		int counter = 1;
		for (IXMLTag messageTag : messages.getXmlTags()) {
			String name = CDesktopComponents.sSpring().unescapeXML(messageTag.getChildValue("pid"));
			comboitem = new CRunPVToolkitDefComboitem(combobox, 
					new String[]{"value"}, 
					new Object[]{name}
			);
			comboitem.setClass("font " + _classPrefix + "MessageMessageComboitem");
			String messageCacId = "" + messages.getCaseComponent().getCacId();
			String messageTagId = messageTag.getAttribute(AppConstants.keyId);
			comboitem.setAttribute("messageCacId", messageCacId);
			comboitem.setAttribute("messageTagId", messageTagId);
			if (messageCacId.equals(chosenMessageCacId) && messageTagId.equals(chosenMessageTagId)) {
				selectedIndex = counter;
			}
			counter++;
		}
		combobox.setSelectedIndex(selectedIndex);
		
		pvToolkit.setMemoryCaching(false);
	}

	public void addStepCombobox(Component parent) {
		Combobox combobox = new CRunPVToolkitDefCombobox(parent, 
				new String[]{"id", "width"}, 
				new Object[]{_idPrefix + "MessageStepCombobox", "300px"}
		);
		combobox.setClass("font " + _classPrefix + "MessageStepCombobox");
		Comboitem comboitem = new CRunPVToolkitDefComboitem(combobox, 
				new String[]{"labelKey"}, 
				new Object[]{"PV-toolkit.none"}
		);
		comboitem.setClass("font " + _classPrefix + "MessageStepComboitem");
		String chosenStepTagId = "";
		if (_stepMessageTag != null) {
			chosenStepTagId = pvToolkit.getStatusChildTagAttribute(_stepMessageTag, "steptagid");
		}
		int selectedIndex = 0;
		int counter = 1;
		for (IXMLTag stepTag : pvToolkit.getCurrentStepTags()) {
			String stepTagId = stepTag.getAttribute(AppConstants.keyId);
			IXMLTag learningActivityTag = pvToolkit.getLearningActivityChildTag(stepTag);
			if (learningActivityTag != null) {
				comboitem = new CRunPVToolkitDefComboitem(combobox, 
						new String[]{"cacAndTag", "tagChildName"}, 
						new Object[]{new CRunPVToolkitCacAndTag(pvToolkit.getCurrentProjectsCaseComponent(), learningActivityTag), "name"}
				);
				comboitem.setClass("font " + _classPrefix + "MessageStepComboitem");
				comboitem.setAttribute("stepTag", stepTag);
				if (chosenStepTagId.equals(stepTagId)) {
					selectedIndex = counter;
				}
				counter++;
			}
		}
		combobox.setSelectedIndex(selectedIndex);

	}

	public void addDatebox(Component parent, String idAndClassPostfix, String statusKeyId) {
		Datebox datebox = new CDefDatebox();
		parent.appendChild(datebox);
		datebox.setId(_idPrefix + idAndClassPostfix);
		datebox.setClass("font " + _classPrefix + idAndClassPostfix);
		if (_stepMessageTag != null) {
			datebox.setValue(CDefHelper.getDateFromStrYMD(pvToolkit.getStatusChildTagAttribute(_stepMessageTag, statusKeyId)));
		}
	}

	public void addTimebox(Component parent, String idAndClassPostfix, String statusKeyId) {
		Timebox timebox = new CDefTimebox();
		parent.appendChild(timebox);
		timebox.setId(_idPrefix + idAndClassPostfix);
		timebox.setClass("font " + _classPrefix + idAndClassPostfix);
		if (_stepMessageTag != null) {
			timebox.setValue(CDefHelper.getDateFromStrHMS(pvToolkit.getStatusChildTagAttribute(_stepMessageTag, statusKeyId)));
		}
	}

	public void addMessageDateTimebox(Component parent) {
		addDatebox(parent, "MessageDatebox", AppConstants.statusKeyReminderDate);
		addTimebox(parent, "MessageTimebox", AppConstants.statusKeyReminderTime);
	}

	public void addDeadlineDateTimebox(Component parent) {
		addDatebox(parent, "MessageDeadlineDatebox", AppConstants.statusKeyDeadlineDate);
		addTimebox(parent, "MessageDeadlineTimebox", AppConstants.statusKeyDeadlineTime);
	}

	protected void addSaveButtonOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				Combobox messageCombobox = (Combobox)vView.getComponent(_idPrefix + "MessageMessageCombobox");
				
				//NOTE message must always be chosen
				String messageCacId = (String)messageCombobox.getSelectedItem().getAttribute("messageCacId");
				String messageTagId = (String)messageCombobox.getSelectedItem().getAttribute("messageTagId");
				if (messageCacId == null || messageTagId == null) {
					//NOTE no message chosen, show warning
					vView.showMessagebox(getRoot(), vView.getLabel("PV-toolkit-superTeacher.message.save.warning"), vView.getLabel("PV-toolkit-superTeacher.message.save"), Messagebox.OK, Messagebox.EXCLAMATION);
					return;
				}

				//NOTE message date has to be stored
				Datebox messageDatebox = (Datebox)vView.getComponent(_idPrefix + "MessageDatebox");
				Timebox messageTimebox = (Timebox)vView.getComponent(_idPrefix + "MessageTimebox");
				//NOTE if no step is chosen it is a general message not related to any step
				//if step is chosen deadliner must be stored as well
				Combobox stepCombobox = (Combobox)vView.getComponent(_idPrefix + "MessageStepCombobox");
				Datebox deadlineDatebox = (Datebox)vView.getComponent(_idPrefix + "MessageDeadlineDatebox");
				Timebox deadlineTimebox = (Timebox)vView.getComponent(_idPrefix + "MessageDeadlineTimebox");
				
				String messageDateStr = "";
				String messageTimeStr = "";
				Date messageDate = messageDatebox.getValue(); 
				Date messageTime = messageTimebox.getValue();
				if (messageDate != null) {
					messageDateStr = CDefHelper.getDateStrAsYMD(messageDate);
				}
				if (messageTime != null) {
					messageTimeStr = CDefHelper.getDateStrAsHMS(messageTime);
				}

				IXMLTag stepTag = (IXMLTag)stepCombobox.getSelectedItem().getAttribute("stepTag");
				String deadlineDateStr = "";
				String deadlineTimeStr = "";
				if (stepTag != null) {
					Date deadlineDate = deadlineDatebox.getValue(); 
					Date deadlineTime = deadlineTimebox.getValue(); 
					if (deadlineDate != null) {
						deadlineDateStr = CDefHelper.getDateStrAsYMD(deadlineDate);
					}
					if (deadlineTime != null) {
						deadlineTimeStr = CDefHelper.getDateStrAsHMS(deadlineTime);
					}
				}

				messagesDiv.saveMessage(_stepMessageTag, messageCacId, messageTagId, messageDateStr, messageTimeStr, stepTag, deadlineDateStr, deadlineTimeStr);
				
				setVisible(false);
			}
		});
	}

}
