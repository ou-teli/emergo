/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Html;
import org.zkoss.zul.Timer;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CXmlHelper;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.view.VView;

public class CRunTextDiv extends CDefDiv {

	private static final long serialVersionUID = 8372350805074970295L;

	protected VView vView = CDesktopComponents.vView();
	
	protected String runComponentId = "";
	protected CRunConversations currentRunComponent = null;
	public boolean showCloseButton = false;
	public String textPlayerName = "";
	public boolean hasNextFragment = false;
	public List<String> balloons = null;
	public int balloonCounter = 0;
	public String textstyle = "";

	public String innerWidth = "";
	public String innerHeight = "";
	public String closeLeft = "";
	public String closeTop = "";
	public String replayLeft = "";
	public String replayTop = "";
	public String previousLeft = "";
	public String previousTop = "";
	public String nextLeft = "";
	public String nextTop = "";

	public int getDelay(String text) {
		return Math.round((CRunTextInitBox.minDelay * CRunTextInitBox.maxBalloonChars + CRunTextInitBox.maxDelay * text.length()) / CRunTextInitBox.maxBalloonChars);
	}
	
	public String getAdjustedText(String text) {
		// remove spaces and new lines in breaks
		text = text.replace("<br />", "<br/>");
		text = text.replace("\n", "");

		//TODO now following only works with external urls. It could be adjusted to work for internal urls too. Within the richtext dialogue a code could be entered to indicate
		//that it is an internal url, e.g. [$EMERGOURL$], and the code below could be adjusted to replace this code by the local emergo path.
		//NOTE clicking a link in richtext on some computers results in showing the url in the Emergo player window!
		//Following code is meant to prevent this
		// adjust possible text links so clicking a link will result in a popup
		String anchorStart = "<a";
		String anchorEnd = "</a>";
		int anchorStartIndex = text.indexOf(anchorStart);
		int anchorEndIndex = text.indexOf(anchorEnd);
		String newText = "";
		String hrefStart = "href=\"";
		String hrefEnd = "\"";
		String targetStart = "target=\"";
		String targetEnd = "\"";
		// adjust all anchors
		while (anchorStartIndex >= 0 && anchorEndIndex > anchorStartIndex) {
			newText += text.substring(0, anchorStartIndex);
			String anchorString = text.substring(anchorStartIndex, anchorEndIndex);
			// get href attribute and get url value of it
			int hrefStartIndex = anchorString.indexOf(hrefStart);
			String url = "";
			if (hrefStartIndex >= 0) {
				url = anchorString.substring(hrefStartIndex + hrefStart.length());
				int hrefEndIndex = url.indexOf(hrefEnd);
				if (hrefEndIndex >= 0) {
					url = url.substring(0, hrefEndIndex); 
				}
				else {
					url = "";
				}
			}
			if (!url.equals("")) {
				// set value of href attribute to #, so it does not work anymore
				anchorString = anchorString.replace(url, "#");
				// add onClick attribute
				anchorString = anchorString.replace(anchorStart, anchorStart + " onClick=\"" + vView.getJavascriptWindowOpenFullscreen(url) + "\"");
			}
			// remove target attribute
			int targetStartIndex = anchorString.indexOf(targetStart);
			if (targetStartIndex >= 0) {
				String target = anchorString.substring(targetStartIndex + targetStart.length());
				int targetEndIndex = target.indexOf(targetEnd);
				if (targetEndIndex >= 0) {
					target = anchorString.substring(targetStartIndex, targetStartIndex + targetStart.length() + targetEndIndex + targetEnd.length());
					anchorString = anchorString.replace(target, "");
				}
			}
			newText += anchorString + anchorEnd;
			text = text.substring(anchorEndIndex + anchorEnd.length());
			anchorStartIndex = text.indexOf(anchorStart);
			anchorEndIndex = text.indexOf(anchorEnd);
		}
		if (!text.equals("")) {
			newText += text;
		}

		return newText;
	}

	public List<String> getTextBalloons(String text) {
		List<String> balloons = new ArrayList<String>();
		text = getAdjustedText(text);
		if (text.equals("")) {
			return balloons;
		}
		// determine sections
		List<String> sections = new ArrayList<String>();
		String sectionStart = "<p>";
		String sectionEnd = "</p>";
		int sectionStartIndex = text.indexOf(sectionStart);
		if (sectionStartIndex < 0) {
			// only one section, <p></p> is removed in method stripCKeditorString in CXmlHelper class
			sections.add(text);
		}
		else {
			// get all sections
			String temp = text;
			int sectionEndIndex = temp.indexOf(sectionEnd);
			while (sectionStartIndex >= 0 && sectionEndIndex >= 0 && sectionEndIndex > (sectionStartIndex + sectionStart.length())) {
				if (sectionStartIndex > 0) {
					// possible content within </p> and <p> tag is added as a section
					sections.add(temp.substring(0, sectionStartIndex));
					temp = temp.substring(sectionStartIndex);
					sectionEndIndex += -sectionStartIndex;
					sectionStartIndex = 0;
				}
				else if (sectionStartIndex == 0) {
					// content within <p> and </p> tag is added as a section
					sections.add(temp.substring(sectionStart.length(), sectionEndIndex));
					temp = temp.substring(sectionEndIndex + sectionEnd.length());
					sectionStartIndex = temp.indexOf(sectionStart);
					sectionEndIndex = temp.indexOf(sectionEnd);
				}
			}
			if (!temp.equals("")) {
				// if content exists after </p> tag, add it as section
				sections.add(temp);
			}
		}
		// determine balloons
		for (String section : sections) {
			if (section.length() <= CRunTextInitBox.maxBalloonChars) {
				// if section fits, add as balloon
				balloons.add(section);
			}
			else {
				CXmlHelper lHelper = new CXmlHelper();
				balloons.addAll(lHelper.splitHtmlStringInChunks(section, CRunTextInitBox.maxBalloonChars));

				/*
				// split section in balloons containing one or more sentences.
				// first split section in words = parts without spaces (punctuation can be part of a 'word')
				String[] words = section.split(" ");
				String balloon = "";
				// loop through the words
				for (int i=0;i<words.length;i++) {
					if (balloon.length() + words[i].length() > maxBalloonChars) {
						String rest = "";
						// if balloon will become too large, search last character that indicates the end of a sentence
						// NOTE . and ? can be part of an anchor tag so search for '. ' and '? '.
						int endIndex = Math.max(Math.max(balloon.lastIndexOf(". "), balloon.lastIndexOf("! ")), balloon.lastIndexOf("? "));
						if (endIndex > 0) {
							// if end of sentence found, strip balloon of part after end of sentence and put the stripped part in the next balloon through rest
							rest += balloon.substring(endIndex + 1);
							balloon = balloon.substring(0, endIndex + 1);
						}
						// check if anchor tag is not broken into two balloons
						String anchorStart = "<a";
						String anchorEnd = "</a>";
						int anchorStartIndex = balloon.lastIndexOf(anchorStart);
						int anchorEndIndex = balloon.lastIndexOf(anchorEnd);
						if (anchorStartIndex > 0 && anchorEndIndex <= anchorStartIndex) {
							// if broken anchor tag, then put it in next balloon
							rest += balloon.substring(anchorStartIndex);
							balloon = balloon.substring(0, anchorStartIndex);
						}  
						// add balloon
						balloons.add(balloon.trim());
						// put the possibly stripped part in the next balloon
						balloon = rest;
					}
					if (balloon.length() > 0) {
						// add space
						balloon += " ";
					}
					balloon += words[i];
				}
				if (balloon.length() > 0) {
					balloons.add(balloon.trim());
				}
				 */


			}
		}
		return balloons;
	}

	public void initText() {
		runComponentId = (String)VView.getParameter("runComponentId", this, "runConversations");

		currentRunComponent = (CRunConversations)vView.getComponent(runComponentId);
		showCloseButton = VView.getParameter("showCloseButton", this, "false").equals("true");

		textPlayerName = (String)VView.getParameter("playerName", this, "EM_te");

		//NOTE get position relative to position of parent CRunConversations
		String left = (String)VView.getParameter("fragment_position_left", this, "0px");
		String top = (String)VView.getParameter("fragment_position_top", this, "0px");
		String lPosMode = (String)VView.getParameter("fragment_position_mode", this, "");
		String style = lPosMode + "left:"+ left + ";top:" + top + ";";
		setStyle(style);

		//NOTE fragment size may overwrite width of parent CRunConversations
		String txt_width = (String)VView.getParameter("fragment_size_width", this, (String)VView.getParameter("video_size_width", this, (String)Executions.getCurrent().getDesktop().getAttribute("runWndWidth")));
		String txt_height = (String)VView.getParameter("fragment_size_height", this, (String)VView.getParameter("video_size_height", this, (String)Executions.getCurrent().getDesktop().getAttribute("runWndHeight")));

		innerWidth = "" + (Integer.parseInt(txt_width.substring(0, txt_width.length() - 2)) - 2 * (CRunTextInitBox.spacing + 1)) + "px";
		innerHeight = "" + (Integer.parseInt(txt_height.substring(0, txt_height.length() - 2)) - 2 * (CRunTextInitBox.spacing + 1)) + "px";

		String text = (String)VView.getParameter("text", this, "");
		hasNextFragment = VView.getParameter("hasNextFragment", this, "false").equals("true");
		balloons = getTextBalloons(text);
		balloonCounter = 0;

		textstyle = (String)VView.getParameter("textstyle", this, "");

		closeLeft = "" + (Integer.parseInt(left.substring(0, left.length() - 2)) + Integer.parseInt(txt_width.substring(0, txt_width.length() - 2)) - 12) + "px";
		closeTop = "" + (Integer.parseInt(top.substring(0, top.length() - 2)) - 2) + "px";

		replayLeft = "" + (Integer.parseInt(left.substring(0, left.length() - 2)) + Integer.parseInt(txt_width.substring(0, txt_width.length() - 2)) - 12) + "px";
		replayTop = "" + (Integer.parseInt(top.substring(0, top.length() - 2)) + Integer.parseInt(txt_height.substring(0, txt_height.length() - 2)) - 12) + "px";

		previousLeft = "" + (Integer.parseInt(left.substring(0, left.length() - 2)) - 62) + "px";
		previousTop = "" + (Integer.parseInt(top.substring(0, top.length() - 2)) + (Integer.parseInt(txt_height.substring(0, txt_height.length() - 2)) / 2) - 2) + "px";

		nextLeft = "" + (Integer.parseInt(left.substring(0, left.length() - 2)) + Integer.parseInt(txt_width.substring(0, txt_width.length() - 2)) + 30) + "px";
		nextTop = previousTop;
	}

	public void playTextBalloon() {
		Timer timer = (Timer)vView.getComponent("contentTimer_" + runComponentId);
		if (balloonCounter < balloons.size()) {
			((Html)vView.getComponent("content_" + runComponentId)).setContent(balloons.get(balloonCounter));
			if (timer != null) {
				timer.setDelay(getDelay(balloons.get(balloonCounter)));
			}
			if (balloonCounter == ((balloons.size() - 1))) {
				Clients.evalJavaScript("onStartOfLastBalloon('" + runComponentId + "','" + textPlayerName + "');");
			}
		}
		Component replayButton = vView.getComponent("replayButton_" + runComponentId);
		if (replayButton != null) {
			replayButton.setVisible(balloons.size() > 1 && balloonCounter > 0);
		}
		Component previousButton = vView.getComponent("previousButton_" + runComponentId);
		if (previousButton != null) {
			previousButton.setVisible(balloons.size() > 1 && balloonCounter > 0);
		}
		Component nextButton = vView.getComponent("nextButton_" + runComponentId);
		if (nextButton != null) {
			nextButton.setVisible((balloonCounter < (balloons.size() - 1)) || hasNextFragment);
		}
		if (balloonCounter == balloons.size()) {
			timer.stop();
			Clients.evalJavaScript("onComplete('" + runComponentId + "','" + textPlayerName + "');");
		}
		balloonCounter++;
	}

	protected void initChildComponents() {
		Events.postEvent("onInit", vView.getComponent("content_" + runComponentId), null);
		Events.postEvent("onInit", vView.getComponent("closeButton_" + runComponentId), null);
		Events.postEvent("onInit", vView.getComponent("replayButton_" + runComponentId), null);
		Events.postEvent("onInit", vView.getComponent("previousButton_" + runComponentId), null);
		Events.postEvent("onInit", vView.getComponent("nextButton_" + runComponentId), null);
	}
	
	protected void playTextBalloonAndStartTimer() {
		if (balloons == null || balloons.size() == 0) {
			return;
		}
		Timer timer = (Timer)vView.getComponent("contentTimer_" + runComponentId);
		timer.stop();
		playTextBalloon();
		timer.start();
	}

	public void playText() {
		initText();
		initChildComponents();
		playTextBalloonAndStartTimer();
	}

	public void resetText() {
		balloonCounter = 0;
		playTextBalloonAndStartTimer();
	}

	public void onCreate(CreateEvent aEvent) {
		Events.postEvent("onInitZulfile", this, null);
	}
	
	public void onInitZulfile(Event aEvent) {
		playText();
	}

	//NOTE onPlayText is used in CRunConversationsClassic and CRunConversations 
	public void onPlayText(Event aEvent) {
		playText();
	}

	public void onPrevious(Event aEvent) {
		//NOTE decrease balloonCounter by 2 because it is increased by 1 in updateText() (see above)
		balloonCounter--;
		balloonCounter--;
		playTextBalloon();
	}

	public void onNext(Event aEvent) {
		playTextBalloon();
	}

	public void onClose(Event aEvent) {
		currentRunComponent.Hide();
	}

}
