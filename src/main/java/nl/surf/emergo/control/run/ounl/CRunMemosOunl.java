/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Label;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefMacro;
import nl.surf.emergo.control.run.CRunArea;
import nl.surf.emergo.control.run.CRunButton;
import nl.surf.emergo.control.run.CRunHbox;
import nl.surf.emergo.control.run.CRunMemos;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;

/**
 * The Class CRunMemosOunl is used to show the memos within the run view area of the
 * Emergo player.
 */
public class CRunMemosOunl extends CRunMemos {

	private static final long serialVersionUID = 6278281203976470801L;

	/** The max string length. */
	protected static final String memoTitleSeparator = ": ";

	public int memosRowNumber = 1;
	public Label selectedLabel = null;
	
	/**
	 * Instantiates a new c run memos.
	 */
	public CRunMemosOunl() {
		super();
	}

	/**
	 * Instantiates a new c run memos.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the memos case component
	 */
	public CRunMemosOunl(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
	}

	/**
	 * Creates view for video.
	 * Gets current setting, sets selected and opened of it to true and starts it.
	 */
	public void init() {
		super.init();

		Events.postEvent("onInitZulfile", this, null);
	}

	protected List<List<Object>> getMemosDatas() {
		List<List<Object>> lDataList = new ArrayList<List<Object>>();
		
		List<IXMLTag> lMemoTags = getMemoTags();
		String lPreviousMainTitle = "";
		List<Object> data = null;
		List<Object> data2 = null;
		for (IXMLTag lMemoTag : lMemoTags) {
			String lTitle = lMemoTag.getChild("pid").getValue();
			String lMainTitle = getMemoMainTitle(lTitle);
			String lSubTitle = getMemoSubTitle(lTitle);
			if (lSubTitle.equals("")) {
				lSubTitle = lMainTitle;
			}
			if (!lMainTitle.equals("")) {
				if (!lMainTitle.equals(lPreviousMainTitle)) {
					data = new ArrayList<Object>();
					lDataList.add(data);
					data.add(lMainTitle);
					data2 = new ArrayList<Object>();
					data.add(data2);
					lPreviousMainTitle = lMainTitle;
				}
				List<Object> data3 = new ArrayList<Object>();
				data3.add(lMemoTag);
				data3.add(lSubTitle);
				data2.add(data3);
			}
		}
		return lDataList;
	}

	/**
	 * Gets all memo tags.
	 */
	public List<IXMLTag> getMemoTags() {
		IXMLTag lRootTag = CDesktopComponents.sSpring().getSReferencedDataTagHelper().determineReferencedRootTag(caseComponent, getRunStatusType(), "refmemo");
		if (lRootTag != null) {
			return lRootTag.getChild(AppConstants.contentElement).getChildTags(AppConstants.defValueNode);
		}
		else {
			return new ArrayList<IXMLTag>();
		}
	}

	/**
	 * Gets the memo main title.
	 * 
	 * @param aMemoTitle the a memo title
	 *
	 * @return the memo main title
	 */
	public String getMemoMainTitle(String aMemoTitle) {
		int lIndex = aMemoTitle.indexOf(memoTitleSeparator);
		if (lIndex < 0) {
			return aMemoTitle;
		}
		return aMemoTitle.substring(0, lIndex);
	}

	/**
	 * Gets the memo sub title.
	 * 
	 * @param aMemoTitle the a memo title
	 *
	 * @return the memo sub title
	 */
	public String getMemoSubTitle(String aMemoTitle) {
		int lIndex = aMemoTitle.indexOf(memoTitleSeparator);
		if (lIndex < 0) {
			return "";
		}
		return aMemoTitle.substring(lIndex + memoTitleSeparator.length());
	}

	/**
	 * Creates new content component, the profile view.
	 *
	 * @return the component
	 */
	@Override
	protected Component newContentComponent() {
		HtmlMacroComponent memosView = new CDefMacro();
		memosView.setId("runMemosView");
		return memosView;
	}

	/**
	 * Creates buttons area and adds close button.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run hbox
	 */
	@Override
	protected CRunHbox createButtonsArea(Component aParent) {
		return null;
	}

	/**
	 * Creates title area and shows name of case component within it.
	 * And adds close button
	 *
	 * @param aParent the ZK parent
	 *
	 * @return the c run area
	 */
	@Override
	protected CRunArea createTitleArea(Component aParent) {
		CRunHbox lHbox = new CRunHbox();
		aParent.appendChild(lHbox);
		CRunComponentDecoratorOunl decorator = createDecorator();
		CRunArea lTitleArea = decorator.createTitleArea(caseComponent, lHbox, getClassName());
		decorator.createCloseArea(caseComponent, lHbox, getClassName(), getId(), this);
		return lTitleArea;
	}

	/**
	 * Creates decorator.
	 *
	 * @return the decorator
	 */
	protected CRunComponentDecoratorOunl createDecorator() {
		return new CRunComponentDecoratorOunl();
	}

	/**
	 * Creates close button.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run button
	 */
	@Override
	protected CRunButton createCloseButton(Component aParent) {
		// NOTE Don't create close button
		return null;
	}


	/**
	 * Methods originally used in ZUL file.
	 */

	public void onInitZulfile(Event aEvent) {
		Map<String,Object> propertyMap = new HashMap<String,Object>();
		propertyMap.put("currentEmergoComponent", this);
		propertyMap.put("memosDatas", getMemosDatas());
		((HtmlMacroComponent)getRunContentComponent()).setDynamicProperty("a_propertyMap", propertyMap);
		((HtmlMacroComponent)getRunContentComponent()).setMacroURI(VView.v_run_memos_fr);
		getRunContentComponent().setVisible(true);
	}

}
