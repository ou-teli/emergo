/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.definegoals;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Div;
import org.zkoss.zul.Rows;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitNavigationButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitSubStepsDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;

public class CRunPVToolkitDefineGoalsOverviewTipsTopsDiv extends CRunPVToolkitDefineGoalsDiv {

	private static final long serialVersionUID = -4817269218203549407L;

	@Override
	public void update() {
		getChildren().clear();
		
		pvToolkit.setMemoryCaching(true);
		
		caseComponent = pvToolkit.getCurrentRubricsCaseComponent();

		String feedbackLevelOfTipsAndTops = pvToolkit.getFeedbackLevelOfTipsAndTops(pvToolkit.getCurrentMethodTag(), pvToolkit.getCurrentCycleTag());
		boolean mayTipsAndTopsSkill = pvToolkit.getMayTipsAndTopsSkill(pvToolkit.getCurrentMethodTag(), pvToolkit.getCurrentCycleTag());
		
		_skillclusterHasTipsTops = feedbackLevelOfTipsAndTops.equals("skillcluster");
		_subskillHasTipsTops = feedbackLevelOfTipsAndTops.equals("subskill");
		_skillHasTipsTops = feedbackLevelOfTipsAndTops.equals("skill") || mayTipsAndTopsSkill;

		Div div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{"titleRight"}
		);
		
		CRunPVToolkitSubStepsDiv runPVToolkitSubStepsDiv = (CRunPVToolkitSubStepsDiv)CDesktopComponents.vView().getComponent("defineGoalsSubStepsDiv");
		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "value"}, 
				new Object[]{"font titleRight", (String)runPVToolkitSubStepsDiv.getAttribute("divTitle")}
		);
		
		Div tipsTopsDiv = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "TipsTopsOverviewDiv"}
		);
		
		Rows rows = appendTipsTopsGrid(tipsTopsDiv);
		int rowNumber = 1;
		rowNumber = appendTipsOrTopsToGrid(rows, "tops", rowNumber);
		rowNumber = appendTipsOrTopsToGrid(rows, "tips", rowNumber);
		
		CRunPVToolkitNavigationButton navBtn = new CRunPVToolkitNavigationButton(this, 
				new String[]{"class", "cLabelKey"}, 
				new Object[]{"font pvtoolkitButton " + _classPrefix + "OverviewTipsTopsBackButton", "PV-toolkit.back"}
		);
		navBtn.init(getId(), _idPrefix + "Div");

		pvToolkit.setMemoryCaching(false);
		
	}

	@Override
    protected Rows appendTipsTopsGrid(Component parent) {
    	return pvToolkit.appendGrid(
    			parent, 
    			getId() + "Grid",
    			"position:absolute;left:0px;top:0px;width:1060px;height:480px;overflow:auto;", 
				new boolean[] {true, true, true, true, true, _skillclusterHasTipsTops, _subskillHasTipsTops, _skillHasTipsTops}, 
    			new String[] {"3%", "47%", "10%", "10%", "10%", "10%", "10%", "10%"}, 
    			new boolean[] {true, true, true, true, true, true, true, true},
    			null,
    			"PV-toolkit-skillwheel.column.label.", 
    			new String[] {"", "tiptop", "peer", "feedbackrole", "feedbackdate", "skillcluster", "subskill", "skill"});
	}

	@Override
	protected int appendTipsOrTopsToGrid(Rows rows, String tipsOrTops, int rowNumber) {
		return appendTipsOrTopsToGrid(
				rows, 
				tipsOrTops, 
				rowNumber,
				new boolean[] {true, true, true, true, true, _skillclusterHasTipsTops, _subskillHasTipsTops, _skillHasTipsTops} 
				);
	}

}
