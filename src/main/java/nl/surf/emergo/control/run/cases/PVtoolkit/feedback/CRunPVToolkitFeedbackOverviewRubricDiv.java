/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.feedback;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Messagebox;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitNavigationButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.view.VView;

public class CRunPVToolkitFeedbackOverviewRubricDiv extends CRunPVToolkitFeedbackRubricDiv {

	private static final long serialVersionUID = 74511605478207467L;
	
	protected String getClassPrefix() {
		return "feedbackOverview";
	}
	
	public void init(IERunGroup peer, IXMLTag currentStepTag, IXMLTag currentTag, boolean editable, boolean unfolded) {
		_showOverview = true;
		_classPrefix = getClassPrefix();
		
		super.init(peer, currentStepTag, currentTag, editable, unfolded);
	}

	public void update() {
		super.update();
		
		if (_showOverview) {
			new CRunPVToolkitDefImage(this, 
					new String[]{"class", "visible"}, 
					new Object[]{"feedbackInfoButton", "false"}
					);
		}
		
		Div div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{"titleRight"}
		);
		
		String labelKey = "";
		String value = "";
		if (!_peerFeedback) {
			labelKey = "PV-toolkit-feedback.header.selffeedbackOverview";
			value = CDesktopComponents.vView().getLabel(labelKey);
		}
		else {
			labelKey = "PV-toolkit-feedback.header.feedbackOverview";
			value = CDesktopComponents.vView().getLabel(labelKey).replace("%1", pvToolkit.getRgaName(_currentTag.getAttribute(CRunPVToolkit.practiceRgaId)));
		}
		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "value"}, 
				new Object[]{"font titleRight", value}
		);
		
		//back button
		Component backBtn = new CRunPVToolkitDefButton(this, 
				new String[]{"class", "cLabelKey"}, 
				new Object[]{"font pvtoolkitButton " + _classPrefix + "BackButton", "PV-toolkit.back"}
		);
		addBackButtonOnClickEventListener(backBtn, _currentStepTag, _currentTag);

		//save or seen button
		if (feedbackIsEditable()) {
			Button saveBtn = new CRunPVToolkitDefButton(this, 
					new String[]{"class", "cLabelKey"}, 
					new Object[]{"font pvtoolkitButton " + _classPrefix + "SaveButton", "PV-toolkit.save"}
					);
			addSaveAndBackToRecordingsOnClickEventListener(saveBtn, _currentTag);
		}
		else {
			CRunPVToolkitNavigationButton seenBtn = new CRunPVToolkitNavigationButton(this, 
					new String[]{"class", "cLabelKey"}, 
					new Object[]{"font pvtoolkitButton " + _classPrefix + "SeenButton", "PV-toolkit.seen"}
					);
			seenBtn.init(getId(), _backToRecordingsId);
		}
		
	}
	
	protected String getTitle() {
		String labelKey = "PV-toolkit-feedback.header.giveFeedback";
		return CDesktopComponents.vView().getLabel(labelKey) + " " + pvToolkit.getRgaName(_currentTag.getAttribute(CRunPVToolkit.practiceRgaId));
	}
	
	protected void addBackButtonOnClickEventListener(Component component, IXMLTag currentStepTag, IXMLTag currentTag) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				toFeedback(_idPrefix + "RubricDiv", _stepPrefix, currentStepTag, currentTag);
			}
		});
	}

	protected void addSaveAndBackToRecordingsOnClickEventListener(Component component, IXMLTag tag) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				VView vView = CDesktopComponents.vView();
				if (!feedbackIsReady()) {
					//show warning
					vView.showMessagebox(getRoot(), vView.getLabel("PV-toolkit-feedback.ready.confirmation"), vView.getLabel("PV-toolkit-feedback.ready"), Messagebox.OK, Messagebox.EXCLAMATION);
				}
				else {
					//NOTE there are three types of peers:
					//self: student gives self feedback within practice step. No dialog: return to his recordings overview where feedback status will become ready
					//student: student gives peer feedback within feedback step. Dialog has two options: set ready for sending or send right away. Return to his feedback recordings overview where feedback status will be updated accordingly
					//teacher: teacher gives peer feedback within feedback step. Dialog has three options: consult other teachers, set ready for batch or send right away. Return to his feedback recordings overview where feedback status will be updated accordingly
					IERunGroupAccount feedbackReceiver = pvToolkit.getRunGroupAccount(_currentTag.getAttribute(CRunPVToolkit.practiceRgaId));
					String peerRole = "";
					if (feedbackReceiver != null) {
						peerRole = pvToolkit.getPeerGroupFeedbackGiverRole(feedbackReceiver.getERunGroup(), _actor);
					}
					else {
						//NOTE if feedbackReceiver is null, class is used in prepare step and tag name is videoskillexample
						peerRole = "assessExpert";
					}
					if (peerRole.equals("self") || peerRole.equals("assessExpert")) {
						setFeedbackReady();
						if (peerRole.equals("self")) {
							setPracticeSelfFeedback();
						}
						toFeedbackRecordings(getId(), _stepPrefix, tag);
						return;
					}
					
					CRunPVToolkitFeedbackReadyDiv feedbackReadyDiv = (CRunPVToolkitFeedbackReadyDiv)vView.getComponent(_stepPrefix + "ReadyDiv");
					if (feedbackReadyDiv != null) {
						feedbackReadyDiv.init(tag, peerRole);
					}
				}
			}
		});
	}

}
