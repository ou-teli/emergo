/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Div;
import org.zkoss.zul.Html;
import org.zkoss.zul.Label;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CIObserved;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.def.CDefHtml;
import nl.surf.emergo.control.def.CDefLabel;
import nl.surf.emergo.control.run.CRunCloseBtn;

/**
 * The Class CRunAlert is used to show alerts within the run choice area of the Emergo player.
 * If an alert is shown on top of another alert, the previous alert is saved temporarily.
 */
public class CRunAlertClassic extends CRunVboxClassic {

	private static final long serialVersionUID = -6881275492960633561L;

	/** The run component. */
	protected CRunComponentClassic runComponent = null;

	/** The previous content, used to temporarily save previous alerts. */
	protected List<String> previousContent = new ArrayList<String>();

	/**
	 * Instantiates a new c run conversation interaction, a title and a text area.
	 * 
	 * @param aId the a id
	 * @param aRunComponent the a run component
	 */
	public CRunAlertClassic(String aId, CRunComponentClassic aRunComponent) {
		super();
		setId(aId);
		runComponent = aRunComponent;
		String lType = "Min";
		if (runWnd.runChoiceAreaMed)
			lType = "Med";
		if (runWnd.runChoiceAreaMax)
			lType = "Max";
		setZclass(className + lType);

		CRunHboxClassic lHbox = new CRunHboxClassic();
		appendChild(lHbox);
		createTitleArea(lHbox);
		createTextArea(this);
	}

	/**
	 * Creates a title area with a fixed title. This area also has a
	 * close button to close the alert.
	 * 
	 * @param aParent the a parent
	 */
	protected void createTitleArea(Component aParent) {
		String lType = "Min";
		if (runWnd.runChoiceAreaMed)
			lType = "Med";
		if (runWnd.runChoiceAreaMax)
			lType = "Max";
		CRunAreaClassic lArea = new CRunAreaClassic();
		aParent.appendChild(lArea);
		lArea.setZclass(getZclass()+"_title_area_left");
		CRunHboxClassic lHbox = new CRunHboxClassic();
		lArea.appendChild(lHbox);
		Label lLabel = new CDefLabel();
		lLabel.setValue(CDesktopComponents.vView().getLabel("run_alert.title"));
		lLabel.setZclass(className + "_title_area_label");
		lHbox.appendChild(lLabel);

		lArea = new CRunAreaClassic();
		aParent.appendChild(lArea);
		lArea.setZclass(className+"_title_area_right");
//		CRunHoverBtnClassic lButton = new CRunHoverBtnClassic(getId() + "CloseBtn",
//				"active","endAlert", null, "close", "", "");
		CRunCloseBtn lButton = new CRunCloseBtn(getId() + "CloseBtn",
				"active","endAlert", null, "close", "", "");
		lButton.setCanHaveStatusSelected(false);
		lButton.registerObserver("runAlert");
		lArea.appendChild(lButton);
	}

	/**
	 * Creates alert text area.
	 * 
	 * @param aParent the a parent
	 * 
	 * @return the area
	 */
	protected CRunAreaClassic createTextArea(Component aParent) {
		String lType = "Min";
		if (runWnd.runChoiceAreaMed)
			lType = "Med";
		if (runWnd.runChoiceAreaMax)
			lType = "Max";
		CRunAreaClassic lTextArea = new CRunAreaClassic();
		lTextArea.setZclass(getZclass() + "_text_area");
		Div lDiv = new CDefDiv();
		lTextArea.appendChild(lDiv);
		lDiv.setZclass(getZclass() + "_text_area_div");
		Html lHtml = new CDefHtml();
		lDiv.appendChild(lHtml);
		lHtml.setId(getId()+"Html");
		lHtml.setStyle(className + "_text_area_label");
		lHtml.setZclass(className + "_text_area_label");
		aParent.appendChild(lTextArea);
		return lTextArea;
	}

	/**
	 * Sets the alert content and adds it to previous state.
	 * 
	 * @param aContent the a content
	 */
	public void setContent(String aContent) {
		previousContent.add(aContent);
		pSetContent(aContent);
	}

	/**
	 * Sets the alert content within the text area.
	 * 
	 * @param aContent the a content
	 */
	private void pSetContent(String aContent) {
		Html lHtml = (Html) CDesktopComponents.vView().getComponent(getId()+"Html");
		if (lHtml != null)
			lHtml.setContent(aContent);
	}

	/**
	 * Restores previous content item within the text area.
	 */
	public void restoreContent() {
		String lPreviousContent = "";
		int lSize = previousContent.size();
		if (lSize > 1) {
			// get previous content
			lPreviousContent = (String)previousContent.get(lSize-2);
			// remove last added content
			previousContent.remove(lSize-1);
			// set previous content
			pSetContent(lPreviousContent);
		}
	}

	/**
	 * Is called by close alert button and will restore previous run choice area status.
	 * 
	 * @param aObserved the observed object, it should have interface CIObserved implemented
	 * @param aAction the action
	 * @param aStatus the status object, can be String or other class
	 */
	@Override
	public void observedNotify(CIObserved aObserved, String aAction, Object aStatus) {
		if (aAction.equals("endAlert")) {
			runWnd.runChoiceArea.restoreStatus();
		}
	}

}
