/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.CreateEvent;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefBox;
import nl.surf.emergo.view.VView;

public class CRunWebsiteInitBox extends CDefBox {
	private static final Logger _log = LogManager.getLogger(CRunWebsiteInitBox.class);
	private static final long serialVersionUID = 4911297334328768697L;

	public void onCreate(CreateEvent aEvent) {
		if (getDesktop() == null)
			return;
		CDesktopComponents.sSpring().initDesktopAttributes(getDesktop());

		VView vView = CDesktopComponents.vView();

		String webs_width = (String) VView.getParameter("video_size_width", this,
				(String) getDesktop().getAttribute("runWndWidth"));
		String webs_height = (String) VView.getParameter("video_size_height", this,
				(String) getDesktop().getAttribute("runWndHeight"));
		String url = (String) VView.getParameter("url", this, "http://www.emergo.cc/");
		boolean isAbsoluteUrl = vView.isAbsoluteUrl(url);
		url = vView.getUrlEncodedFileName(url);

		int lCount = 0;
		String lParam = Executions.getCurrent().getParameter("param" + lCount);
		while (lParam != null) {
			if (lCount == 0)
				url = url + "?" + lParam;
			else
				url = url + "&" + lParam;
			lCount++;
			lParam = Executions.getCurrent().getParameter("param" + lCount);
		}
		if (Executions.getCurrent().getDesktop().getRequestPath().contains("run_website_fr.zul")) {
			// popup window; no need to use iframe, use redirect so protected websites can
			// be shown
			Executions.getCurrent().sendRedirect(url);
			url = null;
		}
		boolean useIframe = false;
		boolean useInclude = false;
		String iframeUrl = "";
		String includeUrl = "";
		String ifDisplay = "none";
		if (url != null) {
			// NOTE don't set source of iframe or include if url already is redirected
			useIframe = isAbsoluteUrl;
			if (!useIframe) {
				// if not absolute url
				String fileExtension = vView.getFileExtension(url);
				_log.info(fileExtension);
				// NOTE show these extensions in an include, so they will be part of the
				// desktop.
				// It enables using the desktop, e.g. communicating with run wnd.
				useInclude = fileExtension.equals("zul") || fileExtension.equals("html") || fileExtension.equals("jsp")
						|| fileExtension.equals("jsf");
				// useInclude = !useInclude;
				useIframe = !useInclude;
			}
			// NOTE if absolute url, then use iframe
			if (useIframe) {
				iframeUrl = url;
				ifDisplay = "block";
			}
			if (useInclude) {
				includeUrl = url;
			}
		}
		String style = "width:" + webs_width + ";height:" + webs_height
				+ ";border:0px solid darkgray;margin: 0px 0px 0px 0px;vertical-align:middle;background-color:transparent;";

		HtmlMacroComponent macro = new HtmlMacroComponent();
		appendChild(macro);
		macro.setDynamicProperty("a_style", style);
		macro.setDynamicProperty("a_ifDisplay", ifDisplay);
		macro.setDynamicProperty("a_iframeUrl", iframeUrl);
		macro.setDynamicProperty("a_useInclude", useInclude);
		macro.setDynamicProperty("a_includeUrl", includeUrl);
		macro.setMacroURI("../run_website_view_fr_macro.zul");
	}

}
