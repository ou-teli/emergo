/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;

/**
 * The Class CRunArea is the ancestor of all areas (ZK windows, in html divs) within the
 * Emergo player.
 * Every run area has the possibility to notify observers with a certain eventaction
 * and eventactionstatus. The observer pattern is implemented within the CDefWindow class.
 * And every run area can have a status which for instance is an indication for the type
 * of content it contains or the status of that content.
 */
public class CRunAreaClassic extends CRunPlayWndClassic {

	private static final long serialVersionUID = -9219841693343274046L;

	/** The run wnd. */
	protected CRunWndClassic runWnd = (CRunWndClassic) CDesktopComponents.vView().getComponent(CControl.runWnd);

	/** The event action, a string. */
	protected String eventAction = "";

	/** The event action status, an object, mostly a string, containing status info. */
	protected Object eventActionStatus = null;

	/** The status. */
	protected String status = "";

	public String getClassName() {
		return CDesktopComponents.vView().getRunClassName(className);
	}

	/**
	 * Instantiates a new c run area.
	 */
	public CRunAreaClassic() {
		setBorder("none");
	}

	/**
	 * Instantiates a new c run area.
	 * 
	 * @param aId the a id
	 */
	public CRunAreaClassic(String aId) {
		if (!aId.equals(""))
			setId(aId);
		setBorder("none");
	}

	/**
	 * Instantiates a new c run area.
	 * 
	 * @param aId the a id
	 * @param aStatus the a status
	 */
	public CRunAreaClassic(String aId, String aStatus) {
		setId(aId);
		setBorder("none");
		setStatus(status);
	}

	/**
	 * Sets the event action.
	 * 
	 * @param aEventAction the new event action
	 */
	public void setEventAction(String aEventAction) {
		eventAction = aEventAction;
	}

	/**
	 * Sets the event action status.
	 * 
	 * @param aEventActionStatus the new event action status
	 */
	public void setEventActionStatus(Object aEventActionStatus) {
		eventActionStatus = aEventActionStatus;
	}

	/**
	 * Notifies observers.
	 */
	public void notifyObservers() {
		notifyObservers(eventAction, eventActionStatus);
	}

	/**
	 * Checks if status is aStatus.
	 * 
	 * @param aStatus the a status
	 * 
	 * @return true, if is status
	 */
	public boolean isStatus(String aStatus) {
		return (aStatus.equals(status));
	}

	/**
	 * Gets the status.
	 * 
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 * 
	 * @param aStatus the new status
	 */
	public void setStatus(String aStatus) {
		status = aStatus;
	}
}
