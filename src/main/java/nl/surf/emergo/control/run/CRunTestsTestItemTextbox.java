/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.InputEvent;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefTextbox;

public class CRunTestsTestItemTextbox extends CDefTextbox {
	
	private static final long serialVersionUID = -866608180421909337L;

	public void onCreate(CreateEvent aEvent) {
		Events.postEvent("onUpdate", this, null);
	}
	
	public void onFocus(Event aEvent) {
		Events.postEvent("onHideFeedback", CDesktopComponents.vView().getComponent(CRunTests.runTestsId), null);
	}
	
	public void onBlur(Event aEvent) {
		CRunTests runTests = (CRunTests)CDesktopComponents.vView().getComponent(CRunTests.runTestsId);
		if (runTests == null) {
			return;
		}
		Events.postEvent("onUpdate", this, (runTests.isHideRightWrong((IXMLTag)getAttribute("tag")) ? "hideRightWrong" : ""));
	}
	
	public void onChange(InputEvent aEvent) {
		CRunTests runTests = (CRunTests)CDesktopComponents.vView().getComponent(CRunTests.runTestsId);
		if (runTests == null) {
			return;
		}
		runTests.saveSectionText((IXMLTag)getAttribute("tag"), getValue());
	}
	
	public void onUpdate(Event aEvent) {
		CRunTests runTests = (CRunTests)CDesktopComponents.vView().getComponent(CRunTests.runTestsId);
		if (runTests == null) {
			return;
		}
		runTests.updateTestItem(this, "CRunEditformSectionOverwrite", (String)aEvent.getData());
	}

}
