/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.Tyconstructor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.domain.IECaseComponent;

public class CRunSpainLevel1_2ScoreInitBox extends CRunScoreInitBox {

	private static final long serialVersionUID = -3056507495300103328L;

	public void onCreate() {
		//NOTE use echoEvent, otherwise macro parent does not yet exist, if code is used within zscript
	  	Events.echoEvent("onInit", this, null);
	}
	
	public void onInit() {
		caseComponentName = "M_D1_explore_groups_app";
		
		Map<String,Object> propertyMap = new HashMap<String,Object>();
		propertyMap.put("casecomponent", ((HtmlMacroComponent)CRunComponent.getMacroParent(this)).getDynamicProperty("a_casecomponent"));
		propertyMap.put("tag", ((HtmlMacroComponent)CRunComponent.getMacroParent(this)).getDynamicProperty("a_tag"));

		IECaseComponent caseComponent = CDesktopComponents.sSpring().getCaseComponent(CDesktopComponents.sSpring().getCase(), "", caseComponentName);
		List<String[]> answerData = getAnswersData(caseComponent, "section", new String[]{
				"impact_statement1",
				"impact_statement2",
				"impact_statement3",
				"impact_statement4",
				"impact_statement5",
				"impact_statement6",
				"impact_statement7",
				"impact_statement8",
				"impact_statement9",
				"impact_statement10"
			}); 
		List<String[]> answerDataSet = getAnswersData(caseComponent, "set", new String[]{
				"set1",
				"set2",
				"set3",
				"set4",
				"set5",
				"set6",
				"set7",
				"set8",
				"set9",
				"set10"
			});
		for (int i=0;i<answerData.size();i++) {
			answerData.get(i)[1] = answerDataSet.get(i)[1]; 
			answerData.get(i)[2] = answerDataSet.get(i)[2]; 
		}
		propertyMap.put("answersdata", answerData);
		String headerleft = "";
		String headerright = "";
		caseComponent = CDesktopComponents.sSpring().getCaseComponent(CDesktopComponents.sSpring().getCase(), "", "game_navigation");
		if (caseComponent != null) {
			List<IXMLTag> nodeTags = CDesktopComponents.cScript().getRunGroupNodeTags(caseComponent, "piece");
			for (IXMLTag nodeTag : nodeTags) {
				String nodeTagPid = nodeTag.getChildValue("pid");
				if (nodeTagPid.equals("D1_2_score_header_left")) {
					headerleft = CDesktopComponents.sSpring().unescapeXML(nodeTag.getChildValue("name"));
				}
				if (nodeTagPid.equals("D1_2_score_header_right")) {
					headerright = CDesktopComponents.sSpring().unescapeXML(nodeTag.getChildValue("name"));
				}
			}
		}
		propertyMap.put("headerleft", headerleft);
		propertyMap.put("headerright", headerright);

		String zulfilepath = (String)((HtmlMacroComponent)CRunComponent.getMacroParent(this)).getDynamicProperty("a_zulfilepath");
		propertyMap.put("zulfilepath", zulfilepath);
	
		HtmlMacroComponent macro = new HtmlMacroComponent();
		appendChild(macro);
		macro.setDynamicProperty("a_propertyMap", propertyMap);
		macro.setMacroURI(zulfilepath + "run_spain_level1_2_score_view_macro.zul");
	}

	public String[] getAnswerData(IECaseComponent caseComponent, String nodeTagName, String matchString) {
		String[] answerdata = new String[3];
		List<IXMLTag> nodeTags = getNodeTags(caseComponent, nodeTagName, matchString);
		List<String> errorMessages = new ArrayList<String>();
		if (nodeTagName.equals("section")) {
			for (IXMLTag nodeTag : nodeTags) {
				answerdata[0] = CDesktopComponents.sSpring().unescapeXML(nodeTag.getChildValue("title"));
				answerdata[1] = "";
				answerdata[2] = "";
			}
		}
		if (nodeTagName.equals("set")) {
			for (IXMLTag nodeTag : nodeTags) {
				answerdata[0] = "";
				answerdata[1] = "";
				answerdata[2] = "";
				boolean correct = true;
				for (IXMLTag alternativeTag : nodeTag.getChilds("alternative")) {
					if (!alternativeTag.getChildValue("pid").contains("_not_affected_")) {
						if (!answerdata[2].equals("")) {
							answerdata[2] += "<br/>";
						}
						answerdata[2] += CDesktopComponents.sSpring().unescapeXML(alternativeTag.getChildValue("richtext"));
						correct = correct && !CDesktopComponents.sSpring().getCurrentRunTagStatus(caseComponentName, "alternative", alternativeTag.getChildValue("pid"), AppConstants.statusKeySelected, AppConstants.statusTypeRunGroup, errorMessages).equals("true");
					}
					else {
						correct = correct && CDesktopComponents.sSpring().getCurrentRunTagStatus(caseComponentName, "alternative", alternativeTag.getChildValue("pid"), AppConstants.statusKeySelected, AppConstants.statusTypeRunGroup, errorMessages).equals("true");
					}
				}
				if (correct) {
					answerdata[1] = "thumbs-up.png";
				}
				else {
					answerdata[1] = "thumbs-down.png";
				}
			}
		}
		return answerdata;
	}

}
