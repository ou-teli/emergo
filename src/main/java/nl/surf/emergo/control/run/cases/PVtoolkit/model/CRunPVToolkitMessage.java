/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.model;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERunGroupAccount;

public class CRunPVToolkitMessage {

	private IECaseComponent messagesCaseComponent;
	private IXMLTag messageTag;
	private IXMLTag messagedetailsTag;
	private IERunGroupAccount sender;
	private IERunGroupAccount receiver;
	private IXMLTag stepTag;
	private String sendDate;
	private String sendTime;
	private boolean read;
	
	public IECaseComponent getMessagesCaseComponent() {
		return messagesCaseComponent;
	}
	public void setMessagesCaseComponent(IECaseComponent messagesCaseComponent) {
		this.messagesCaseComponent = messagesCaseComponent;
	}
	public IXMLTag getMessageTag() {
		return messageTag;
	}
	public void setMessageTag(IXMLTag messageTag) {
		this.messageTag = messageTag;
	}
	public IXMLTag getMessagedetailsTag() {
		return messagedetailsTag;
	}
	public void setMessagedetailsTag(IXMLTag messagedetailsTag) {
		this.messagedetailsTag = messagedetailsTag;
	}
	public IERunGroupAccount getSender() {
		return sender;
	}
	public void setSender(IERunGroupAccount sender) {
		this.sender = sender;
	}
	public IERunGroupAccount getReceiver() {
		return receiver;
	}
	public void setReceiver(IERunGroupAccount receiver) {
		this.receiver = receiver;
	}
	public IXMLTag getStepTag() {
		return stepTag;
	}
	public void setStepTag(IXMLTag stepTag) {
		this.stepTag = stepTag;
	}
	public String getSendDate() {
		return sendDate;
	}
	public void setSendDate(String sendDate) {
		this.sendDate = sendDate;
	}
	public String getSendTime() {
		return sendTime;
	}
	public void setSendTime(String sendTime) {
		this.sendTime = sendTime;
	}

	public boolean isRead() {
		return read;
	}
	public void setRead(boolean read) {
		this.read = read;
	}

}
