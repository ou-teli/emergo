/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import java.util.List;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zul.Label;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefLabel;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunMailAttachments. Used to show mail attachments of a received or
 * sent mail.
 */
public class CRunMailAttachmentsClassic extends CRunVboxClassic {

	private static final long serialVersionUID = -7940329578328390393L;

	/** The mailtag. */
	protected IXMLTag mailtag = null;

	/** The mail case component. */
	protected IECaseComponent caseComponent = null;

	/** The mail run component. */
	protected CRunComponentClassic runComponent = null;

	/**
	 * Gets the mail tag.
	 * 
	 * @return the mail tag
	 */
	public IXMLTag getMailTag() {
		return mailtag;
	}

	/**
	 * Sets the mail tag.
	 * 
	 * @param aMailTag the new mail tag
	 */
	public void setMailTag(IXMLTag aMailTag) {
		mailtag = aMailTag;
	}

	/**
	 * On create fill mailtag and show mail attachments.
	 * 
	 * @param aEvent the a event
	 */
	public void onCreate(CreateEvent aEvent) {
		setMailTag((IXMLTag) (aEvent.getArg()).get("item"));
		// create all necessary components within window
		caseComponent = (IECaseComponent) (aEvent.getArg()).get("casecomponent");
		runComponent = (CRunComponentClassic) (aEvent.getArg()).get("runcomponent");
		createComponents();
		setZclass(className);
	}

	/**
	 * Shows all mail attachments as clickable labels.
	 */
	public void createComponents() {
		if (mailtag != null) {
			List<IXMLTag> lAttachments = mailtag.getStatusChilds("attachment");
			if (lAttachments.size() > 0) {
				Label lLabel = new CDefLabel(CDesktopComponents.vView().getLabel("run_mail.attachments"));
				lLabel.setZclass(className + "_label");
				appendChild(lLabel);
				for (IXMLTag lAttachment : lAttachments) {

					boolean lAllowInline = true;
					IXMLTag lReferencedAttachmentTag = CDesktopComponents.sSpring().getXmlManager().getTagById(
							CDesktopComponents.sSpring().getXmlDataPlusRunStatusTree(caseComponent, runComponent.getRunStatusType()),
							lAttachment.getAttribute(AppConstants.keyReforiginalid));
					if (lReferencedAttachmentTag == null) {
						//if no reference tag, attachment is player resource; don't allow it to be opened inline
						lAllowInline = false;
					}
					
					CRunBlobLabelClassic lRunBlobLabel = new CRunBlobLabelClassic(lAttachment, lAllowInline);
					lRunBlobLabel.setZclass(className + "_attachment");
					appendChild(lRunBlobLabel);
					lRunBlobLabel.setEventAction("attachmentClicked");
					lRunBlobLabel.setEventActionStatus(lAttachment);
					lRunBlobLabel.registerObserver("runMail");
				}
			}
		}
	}
}