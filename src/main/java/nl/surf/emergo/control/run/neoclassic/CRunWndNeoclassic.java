/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.neoclassic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.event.Event;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CIObserved;
import nl.surf.emergo.control.run.CRunAlert;
import nl.surf.emergo.control.run.CRunAssessments;
import nl.surf.emergo.control.run.CRunCanon;
import nl.surf.emergo.control.run.CRunCanonResult;
import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.control.run.CRunConversationInteraction;
import nl.surf.emergo.control.run.CRunConversations;
import nl.surf.emergo.control.run.CRunEmpackBtn;
import nl.surf.emergo.control.run.CRunGooglemaps;
import nl.surf.emergo.control.run.CRunHoverBtn;
import nl.surf.emergo.control.run.CRunLocationArea;
import nl.surf.emergo.control.run.CRunLocationsBtn;
import nl.surf.emergo.control.run.CRunMail;
import nl.surf.emergo.control.run.CRunMailTree;
import nl.surf.emergo.control.run.CRunNoteBtn;
import nl.surf.emergo.control.run.CRunReferences;
import nl.surf.emergo.control.run.CRunStatusChange;
import nl.surf.emergo.control.run.CRunTasksBtn;
import nl.surf.emergo.control.run.CRunTitleArea;
import nl.surf.emergo.control.run.CRunTree;
import nl.surf.emergo.control.run.CRunWnd;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IEComponent;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.STriggeredReference;

/**
 * The Class CRunWndNeoclassic. The neoclassic Emergo player window.
 */
public class CRunWndNeoclassic extends CRunWnd
 {

	private static final long serialVersionUID = -8922453573430761170L;

	/** The run title area to show location, user name and close box. */
	public CRunTitleArea runTitleArea = null;

	/** The run empack btn to show empack action buttons corresponding to casecomponents.
	 *  When clicked it shows the content of a case component within the run view area. */
	public CRunHoverBtn runEmpackBtn = null;

	/** The run locations btn to show location buttons corresponding to locations within the locations component.
	 *  When clicked it shows the current location image within the run view area. */
	public CRunHoverBtn runLocationsBtn = null;

	/** The run note btn to show the note window within the run choice area. */
	public CRunHoverBtn runNoteBtn = null;

	/** The run tasks btn to show the content of the tasks component within a popup window. */
	public CRunHoverBtn runTasksBtn = null;

	/** The run choice area left.  */
	public boolean runChoiceAreaLeft = false;

	/** The run choice area right.  */
	public boolean runChoiceAreaRight = false;

	/** The run choice area max.  */
	public boolean runChoiceAreaMax = false;

	/** The current location tag id. The tagid of the current location. */
	protected String currentLocationTagId = "";

	/** The locations component. Used for buffering in memory. */
	protected IECaseComponent locationsComponent = null;

	/** The possible case components on location. Used for buffering in memory. */
	protected List<IECaseComponent> possibleCaseComponentsOnLocation = null;

	/** The conversation started. */
	protected boolean conversationStarted = false;

	/**
	 * Instantiates a new c run wnd.
	 * Reads google maps key out of .properties file and set it as session var,
	 * so google maps will work within the player.
	 */
	public CRunWndNeoclassic() {
		super();
	}

	/**
	 * On create:
	 * - save ZK desktop id to use it when ZK desktop is closed
	 * - get request parameter runstatus, see IAppManager
	 * - get request parameter rgaId, the id of the user opening the player
	 * - get request parameter rutId, the possible runteam the user belongs to
	 * - get request parameter cacId, the possible casecomponent to preselect
	 * - get request parameter tagId, the possible tag to preselect
	 * - get request parameter mediacontrol, if windows media player will show controls
	 * - get request parameters for language to use
	 * - start CRunMainTimer
	 * - add rungroupaccount to rungroupaccounts who have an open player, so who are
	 *   playing right now
	 *
	 * @param aEvent the a event
	 */
	public void onCreate(Event aEvent) {
		super.onCreate(aEvent);
		determineChoiceAreaLayoutStatus();
	}

	/**
	 * Determines choice area size depending on existance of locations, empack, task and note component
	 * within current case.
	 */
	protected void determineChoiceAreaLayoutStatus() {
		boolean empackUsed = (sSpring.getCaseComponent("empack", "") != null);
		boolean locationsUsed = (sSpring.getCaseComponent("locations", "") != null);
		boolean noteUsed = (sSpring.getCaseComponent("note", "") != null);
		boolean tasksUsed = (sSpring.getCaseComponent("tasks", "") != null);
		if (noteUsed || tasksUsed || (empackUsed && locationsUsed))
			runChoiceAreaRight = true;
		else {
			runChoiceAreaMax = true;
		}
	}

	/**
	 * Gets choice area layout status.
	 */
	protected String getChoiceAreaLayoutStatus() {
		String lStatus = "";
		if (runChoiceAreaLeft)
			lStatus = "Left";
		else if (runChoiceAreaRight)
			lStatus = "Right";
		else if (runChoiceAreaMax)
			lStatus = "Max";
		return lStatus;
	}

	/**
	 * Cleanup. Is called when the ZK desktop (the current browser instance)
	 * is closed.
	 * - saves note if note window is opened
	 * - saves all pending status for the user
	 * - removes current rungroupaccount from list of rungroupaccounts who are
	 *   playing right now
	 */
	public synchronized void cleanup(){
		if (saveAtCleanup()) {
			CRunNoteNeoclassic lRunNote = (CRunNoteNeoclassic)CDesktopComponents.vView().getComponent("runNote");
			if (lRunNote != null)
				lRunNote.saveContent();
		}
		super.cleanup();
	}

	/**
	 * Creates child components of player.
	 */
	public void createComponents() {
		updateViewAreas();
	}

	/**
	 * Updates view areas. The areas beneath the run view area.
	 */
	protected void updateViewAreas() {
		runTitleArea = (CRunTitleArea)CDesktopComponents.vView().getComponent("runTitleArea");
		if (runTitleArea == null) {
			runTitleArea = new CRunTitleArea("runTitleArea");
			appendChild(runTitleArea);
		}

		String lStatus = "empty";
		// update empack button
		String lShowName = sSpring.getCaseComponentRoleName("empack", "");
		boolean empackUsed = (!lShowName.equals(""));
		runEmpackBtn = (CRunEmpackBtn)CDesktopComponents.vView().getComponent("runEmpackBtn");
		if (runEmpackBtn == null) {
			runEmpackBtn = new CRunEmpackBtn("runEmpackBtn", lStatus, "showEmpack", "", "empack", lShowName, "");
			appendChild(runEmpackBtn);
		}
//		runEmpackBtn.setLabel(lShowName);
		runEmpackBtn.registerObserver(getId());
		// update navigation button
		lShowName = sSpring.getCaseComponentRoleName("locations", "");
		boolean locationsUsed = (!lShowName.equals(""));
		runLocationsBtn = (CRunLocationsBtn)CDesktopComponents.vView().getComponent("runLocationsBtn");
		if (runLocationsBtn == null) {
			runLocationsBtn = new CRunLocationsBtn("runLocationsBtn", lStatus, "showLocations", "", "locations", lShowName, "");
			appendChild(runLocationsBtn);
		}
//		runLocationsBtn.setLabel(lShowName);
		runLocationsBtn.registerObserver(getId());
		// update note button
		lShowName = sSpring.getCaseComponentRoleName("note", "");
		boolean noteUsed = (!lShowName.equals(""));
		runNoteBtn = (CRunNoteBtn)CDesktopComponents.vView().getComponent("runNoteBtn");
		if (runNoteBtn == null) {
			runNoteBtn = new CRunNoteBtn("runNoteBtn", lStatus, "showNote", "", "note", lShowName, "");
			appendChild(runNoteBtn);
		}
//		runNoteBtn.setLabel(lShowName);
		runNoteBtn.registerObserver(getId());
		// update tasks button
//		String lPage = VView.v_run_tasks;
		lShowName = sSpring.getCaseComponentRoleName("tasks", "");
		boolean tasksUsed = (!lShowName.equals(""));
		runTasksBtn = (CRunTasksBtn)CDesktopComponents.vView().getComponent("runTasksBtn");
		if (runTasksBtn == null) {
			runTasksBtn = new CRunTasksBtn("runTasksBtn", lStatus, "showTasks", "", "tasks", lShowName, "");
			appendChild(runTasksBtn);
		}
//		runTasksBtn.setLabel(lShowName);
		runTasksBtn.registerObserver(getId());

		
		if (!(noteUsed || tasksUsed || (empackUsed && locationsUsed))) {
			runEmpackBtn.setVisible(false);
			runLocationsBtn.setVisible(false);
		} else {
			if (!empackUsed) {
				runEmpackBtn.setVisible(false);
			}
			if (!locationsUsed) {
				runLocationsBtn.setVisible(false);
			}
		}
			

		if (!noteUsed) {
			runNoteBtn.setVisible(false);
		}
		if (!tasksUsed) {
			runTasksBtn.setVisible(false);
		}

		setViewAreaStatus(getId(), "empty");
		setChoiceAreaStatus(getId(), "empty", null);
	}

	/**
	 * Shows components status.
	 * Is called when the player is started. Restores player status for current user
	 * and depending on runstatus. If preview, preselected casecomponent and tag are
	 * used to set the player in a certain state. Otherwise location is restored to
	 * last location visited during previous session.
	 */
	public void showComponentsStatus() {
		runEmpackBtn.setStatus(getCaseComponentRenderStatus("empack", ""));
		runLocationsBtn.setStatus(getCaseComponentRenderStatus("locations", ""));
		runNoteBtn.setStatus(getCaseComponentRenderStatus("note", ""));
		runTasksBtn.setStatus(getCaseComponentRenderStatus("tasks", ""));
		IXMLTag lSelectedLocationTag = null;
		IECaseComponent lLocationsComponent = null;
		IECaseComponent lEmpackComponent = null;
		IECaseComponent lTasksComponent = null;
		IECaseComponent lNoteComponent = null;
		IECaseComponent lEmpackActionComponent = null;
//		if (isRunStatus(AppConstants.runStatusPreview) || isRunStatus(AppConstants.runStatusPreviewReadOnly)) {
		if (isRunStatus(AppConstants.runStatusPreview) || isRunStatus(AppConstants.runStatusPreviewReadOnly) || isRunStatus(AppConstants.runStatusTutorRun)) {
			// get possibly selected location tag
			lSelectedLocationTag = getPreselectedLocationTag();
			// get possibly selected locations component
			lLocationsComponent = getPreselectedCaseComponent("locations");
			// get possibly selected tasks component
			lTasksComponent = getPreselectedCaseComponent("tasks");
			// get possibly selected note component
			lNoteComponent = getPreselectedCaseComponent("note");
			// get possibly selected empack action component
			lEmpackActionComponent = getPreselectedEmpackAction();
			if (lEmpackActionComponent == null)
				// get possibly selected empack component
				lEmpackComponent = getPreselectedCaseComponent("empack");
			else {
				runEmpackBtn.simulateOnClick();
				String lId = "empackaction" + lEmpackActionComponent.getCacId() + "Btn";
				CRunHoverBtn lBtn = (CRunHoverBtn) CDesktopComponents.vView().getComponent(lId);
				if (lBtn != null)
					lBtn.simulateOnClick();
				else
					onAction(getId(), "showEmpackAction", "" + lEmpackActionComponent.getCacId());
			}
			if ((lEmpackComponent == null) &&
					(lEmpackActionComponent == null) &&
					(lTasksComponent == null) &&
					(lNoteComponent == null)) {
				// get possibly selected location actions
				List<List<Object>> lLocationActions = getPreselectedLocationActions();
				// show only the one selected if so
				if (lLocationActions.size() > 0) {
					if (lLocationActions.size() == 1)
						onAction(getId(), "showLocationAction", lLocationActions.get(0));
					else
						onAction(getId(), "showLocationActions", null);
				}
				else if ((lLocationsComponent == null) && (lSelectedLocationTag == null)){
					// there is only one instance of the locations component
					lLocationsComponent = sSpring.getCaseComponent("locations", "");
				}
			}
		}
//		if (isRunStatus(AppConstants.runStatusRun) || isRunStatus(AppConstants.runStatusTutorRun)) {
		if (isRunStatus(AppConstants.runStatusRun)) {
			// there is only one instance of the locations component
			lLocationsComponent = sSpring.getCaseComponent("locations", "");
		}
		if (isRunStatus("demo")) {
		}
		if ((lLocationsComponent != null) || (lEmpackComponent != null) || (lTasksComponent != null) || (lNoteComponent != null)
				|| (lSelectedLocationTag != null)) {
			// show navigation bar
			if (lLocationsComponent != null)
				if (lSelectedLocationTag != null) {
					// prohibit adding location actions to prevent multiple occurences of the same action
					pAddActions = false;
				}
			runLocationsBtn.simulateOnClick();
			pAddActions = true;
			// show empack
			if (lEmpackComponent != null)
				runEmpackBtn.simulateOnClick();
			// show tasks
			if (lTasksComponent != null)
				runTasksBtn.simulateOnClick();
			// show location
			if (lSelectedLocationTag != null) {
				String lSelectedTagId = lSelectedLocationTag.getAttribute(AppConstants.keyId);
				List<IXMLTag> lLocations = getLocationTags();
				if (lLocations != null) {
					for (IXMLTag lLocation : lLocations) {
						String lId = "location" + lLocation.getAttribute(AppConstants.keyId) + "Btn";
						CRunHoverBtn lBtn2 = (CRunHoverBtn) CDesktopComponents.vView().getComponent(lId);
						if (lBtn2 != null) {
							String lTagId = (String) lBtn2.getActionStatus();
							if ((lTagId != null) && (lTagId.equals(lSelectedTagId)))
								onTimedAction(lId, "showLocation", lTagId);
						}
					}
				}
			}
			// show note
			if (lNoteComponent != null)
				runNoteBtn.simulateOnClick();
		}
	}

	/**
	 * Gets player status string.
	 *
	 * @param aKey the a key
	 * @param aStatus the a status
	 *
	 * @return status string
	 */
	protected String getPlayerStatusString(String aKey, Object aStatus) {
		if (aKey.equals("ChoiceAreaLayoutStatus")) {
			return getChoiceAreaLayoutStatus();
		}
		return "";
	}

	/**
	 * Gets player status xml tag.
	 *
	 * @param aKey the a key
	 * @param aStatus the a status
	 *
	 * @return status xml tag
	 */
	protected IXMLTag getPlayerStatusXmlTag(String aKey, Object aStatus) {
		if (aKey.equals("ActCaseComponentTag")) {
			return getActCaseComponentTag((String)aStatus);
		}
		return null;
	}

	/**
	 * Gets player status list.
	 *
	 * @param aKey the a key
	 * @param aStatus the a status
	 *
	 * @return status list
	 */
	public List getPlayerStatusList(String aKey, Object aStatus) {
		if (aKey.equals("EmpackActionComponents"))
			return getEmpackActionComponents();
		else if (aKey.equals("LocationTags"))
			return getLocationTags();
		return new ArrayList();
	}

	/**
	 * Sets player status.
	 *
	 * @param aKey the a key
	 * @param aStatus the a status
	 */
	protected void setPlayerStatus(String aKey, Object aStatus) {
		if (aKey.equals("TitleAreaMediaBtnsVisible")) {
			runTitleArea.setMediaBtnsVisible("true".equals((String)aStatus));
		}
		else if (aKey.equals("TitleAreaMediaBtnsAction")) {
			runTitleArea.setMediaBtnsAction((String)aStatus);
		}
		else if (aKey.equals("TitleAreaMediaBufferingVisible")) {
			runTitleArea.setMediaBufferingVisible("true".equals((String)aStatus));
		}
	}

	/**
	 * Sets the current case component for notes. Clears current tag for notes.
	 *
	 * @param aCurrentCaseComponentForNotes the current case component for notes
	 */
	public void setNoteCaseComponent(IECaseComponent aCurrentCaseComponentForNotes) {
		deselectNoteBtn(aCurrentCaseComponentForNotes);
		super.setNoteCaseComponent(aCurrentCaseComponentForNotes);
	}

	/**
	 * Sets the current tag for notes and current case component for notes.
	 *
	 * @param aCurrentCaseComponentForNotes the current case component for notes
	 * @param aCurrentTagForNotes the a current tag for notes
	 */
	public void setNoteTag(IECaseComponent aCurrentCaseComponentForNotes, IXMLTag aCurrentTagForNotes) {
		deselectNoteBtn(aCurrentCaseComponentForNotes);
		super.setNoteTag(aCurrentCaseComponentForNotes, aCurrentTagForNotes);
	}

	/**
	 * Deselects note btn.
	 *
	 * @param aCaseComponentForNotes the case component for notes
	 */
	protected void deselectNoteBtn(IECaseComponent aCaseComponentForNotes) {
		if (runNoteBtn != null) {
			if ((currentCaseComponentForNotes == null) ||
				(currentCaseComponentForNotes.getCacId() != aCaseComponentForNotes.getCacId()))
				runNoteBtn.deselect();
		}
	}

	/**
	 * Is called by several other components for notification.
	 * Calls method onAction, so see this method for possible aActions.
	 *
	 * @param aObserved the observed object, it should have interface CIObserved implemented
	 * @param aAction the action
	 * @param aStatus the status object, can be String or other class
	 */
	@Override
	public void observedNotify(CIObserved aObserved, String aAction, Object aStatus) {
		if (aAction.equals("endComponent"))
			endComponentViewComponent();
		super.observedNotify(aObserved, aAction, aStatus);
	}

	/**
	 * On action. Is called by several components change the state of the player.
	 * Possible actions are:
	 * - showLocations: shows location buttons within run choice area
	 * - showLocation: shows location image within run view area
	 * - beamLocation: simulates click on location button and shows location image
	 * - showEmpack: shows empack action buttons within run choice area
	 * - showEmpackAction: shows content of corresponding case component within run view area
	 * - showLocationActions: shows popup window with possible location actions
	 * - showLocationAction: shows location action within run view area
	 * - endComponent: restores empack action button state when a component shown within the run view area is closed
	 * - showTasks: shows tasks within popup window
	 * - endTasks: restores tasks button state
	 * - showNote: shows note window within run choice area
	 * - endNote: restores note button state
	 * - onExternalEvent: used to notify conversations component when user clicks pause/play
	 * - close: saves all pending status for current user
	 *
	 * @param sender the sender
	 * @param action the action
	 * @param status the status
	 */
	@Override
	public void onAction(String sender, String action, Object status) {
		if (action.equals("showLocations")) {
			// shows location selectors (=navigator bar) in choice area
			IECaseComponent lCaseComponent = sSpring.getCaseComponent("locations", "");
			sSpring.setRunComponentStatus(lCaseComponent,
					AppConstants.statusKeySelected, AppConstants.statusValueTrue,
					true, AppConstants.statusTypeRunGroup, true);
			if (runEmpackBtn != null)
				runEmpackBtn.deselect();
			if (runNoteBtn != null)
				runNoteBtn.deselect();
			if (runTasksBtn != null)
				runTasksBtn.deselect();
			setChoiceAreaStatus(getId(), "locations", null);
			sSpring.setRunComponentStatus(lCaseComponent,
					AppConstants.statusKeyOpened, AppConstants.statusValueTrue,
					true, AppConstants.statusTypeRunGroup, true);
			IXMLTag lLocationTag = getLocation(currentLocationTagId);
			if (lLocationTag == null)
				setNoteCaseComponent(lCaseComponent);
			else
				setNoteTag(lCaseComponent, lLocationTag);
		}
		else if (action.equals("beamLocation")) {
			runLocationsBtn.simulateOnClick();
			IXMLTag lLocation = getLocation((String) status);
			String lId = "location" + lLocation.getAttribute(AppConstants.keyId) + "Btn";
			CRunHoverBtn lBtn2 = (CRunHoverBtn) CDesktopComponents.vView().getComponent(lId);
			if (lBtn2 != null) {
				String lTagId = (String) lBtn2.getActionStatus();
				if ((lTagId != null) && (lTagId.equals(status)))
					onAction(lId, "showLocation", lTagId);
			}
		}
		else if (action.equals("showLocation")) {
			// kill pending video
			CRunConversations lComponent = (CRunConversations)CDesktopComponents.vView().getComponent("runConversations");
			if (lComponent != null)
				lComponent.removeFragmentView();
			// shows location in view area
			CRunLocationBtnsNeoclassic lLocBtns = (CRunLocationBtnsNeoclassic) CDesktopComponents.vView().getComponent("runLocationBtns");
			if (lLocBtns != null) {
				lLocBtns.deselectBtn(sender);
				lLocBtns.selectBtn(sender);
			}
			CRunEmpackActionBtnsNeoclassic lEmBtns = (CRunEmpackActionBtnsNeoclassic) CDesktopComponents.vView().getComponent("runEmpackActionBtns");
			if (lEmBtns != null)
				lEmBtns.deselectBtn("");

			currentLocationTagId = (String) status;
			IXMLTag lLocationTag = getLocation(currentLocationTagId);

			IECaseComponent lCaseComponent = sSpring.getCaseComponent("locations", "");
			String[] lKeys = new String[] { AppConstants.statusKeySelected, AppConstants.statusKeyOpened };
			String[] lValues = new String[] { AppConstants.statusValueTrue, AppConstants.statusValueTrue };
			sSpring.setRunTagStatus(lCaseComponent, lLocationTag.getAttribute(AppConstants.keyId), lLocationTag.getName(),
					lKeys[0], lValues[0], null, true, null, AppConstants.statusTypeRunGroup, true, false);
			sSpring.setRunTagStatus(lCaseComponent, lLocationTag.getAttribute(AppConstants.keyId), lLocationTag.getName(),
					lKeys[1], lValues[1], null, true, null, AppConstants.statusTypeRunGroup, true, false);

			// show location background
			setViewAreaStatus(sender, action, lLocationTag);

			// show location in title bar
			if (runTitleArea != null)
//				runTitleArea.setLocationName(sSpring.unescapeXML(lLocationTag.getChildValue("name")) +
//						" (" + sSpring.getRunGroup().getName() + ")");
				runTitleArea.setLocationName(sSpring.unescapeXML(lLocationTag.getChildValue("name")));

			setNoteTag(lCaseComponent, lLocationTag);

			// show location actions
			onAction(getId(), "showLocationActions", lLocationTag);

		}
		else if (action.equals("showEmpack")) {
			// shows empack component selectors (=empack bar) in choice area
			IECaseComponent lCaseComponent = sSpring.getCaseComponent("empack", "");
			sSpring.setRunComponentStatus(lCaseComponent,
					AppConstants.statusKeySelected, AppConstants.statusValueTrue,
					true, AppConstants.statusTypeRunGroup, true);
			CRunHoverBtn lBtn = (CRunHoverBtn) CDesktopComponents.vView().getComponent("runLocationsBtn");
			if (lBtn != null) {
				lBtn.deselect();
				setChoiceAreaStatus(getId(), "empack", null);
			}
			if (runNoteBtn != null)
				runNoteBtn.deselect();
			if (runTasksBtn != null)
				runTasksBtn.deselect();
			sSpring.setRunComponentStatus(lCaseComponent,
					AppConstants.statusKeyOpened, AppConstants.statusValueTrue,
					true, AppConstants.statusTypeRunGroup, true);
			setNoteCaseComponent(lCaseComponent);
		}
		else if (action.equals("showEmpackAction")) {
			// kill pending video
			CRunConversations lComponent = (CRunConversations)CDesktopComponents.vView().getComponent("runConversations");
			if (lComponent != null)
				lComponent.removeFragmentView();
			// shows empack component in view area
			CRunEmpackActionBtnsNeoclassic lEmBtns = (CRunEmpackActionBtnsNeoclassic) CDesktopComponents.vView().getComponent("runEmpackActionBtns");
			if (lEmBtns != null)
				lEmBtns.deselectBtn(sender);
			IECaseComponent lCaseComponent = sSpring.getCaseComponent(Integer.parseInt((String) status));
			sSpring.setRunComponentStatus(lCaseComponent,
					AppConstants.statusKeySelected, AppConstants.statusValueTrue,
					true, AppConstants.statusTypeRunGroup, true);
			setViewAreaStatus(sender, action, getEmpackAction((String) status));
			sSpring.setRunComponentStatus(lCaseComponent,
					AppConstants.statusKeyOpened, AppConstants.statusValueTrue,
					true, AppConstants.statusTypeRunGroup, true);
			setNoteCaseComponent(lCaseComponent);
		}
		else if (action.equals("showLocationActions")) {
			// shows location action selectors within popup window above view area
			// if only one action selector, the popup window isn't shown. the
			// location action is shown immediately

			// get location actions
			List<List<Object>> lLocationActions = getLocationActions((IXMLTag) status);
			if (lLocationActions.size() == 1) {
				List<Object> lLocationAction = lLocationActions.get(0);
				onAction(getId(), "showLocationAction", lLocationAction);
			}
			if (lLocationActions.size() > 1) {
				Map<String,Object> lParams = new HashMap<String,Object>();
				lParams.put("item", lLocationActions);
				CDesktopComponents.vView().modalPopupWithoutWaiting(VView.v_run_location_actions, null, lParams, "center");
			}
		}
		else if (action.equals("showLocationAction")) {
			// shows location action in view area
			CRunEmpackActionBtnsNeoclassic lEmBtns = (CRunEmpackActionBtnsNeoclassic) CDesktopComponents.vView().getComponent("runEmpackActionBtns");
			if (lEmBtns != null)
				lEmBtns.deselectBtn("");
			IECaseComponent lCaseComponent = (IECaseComponent) ((List) status).get(0);
			sSpring.setRunComponentStatus(lCaseComponent,
					AppConstants.statusKeySelected, AppConstants.statusValueTrue,
					true, AppConstants.statusTypeRunGroup, true);
			setNoteCaseComponent(lCaseComponent);
			setViewAreaStatus(getId(), "showLocationAction", status);
			sSpring.setRunComponentStatus(lCaseComponent,
					AppConstants.statusKeyOpened, AppConstants.statusValueTrue,
					true, AppConstants.statusTypeRunGroup, true);
		}
		else if (action.equals("endComponent")) {
			// shows location in view area after an empack component or location
			// action has been closed
			CRunEmpackActionBtnsNeoclassic lEmBtns = (CRunEmpackActionBtnsNeoclassic) CDesktopComponents.vView().getComponent("runEmpackActionBtns");
			if (lEmBtns != null)
				lEmBtns.deselectBtn("");
				setViewAreaStatus(getId(), "location");
			if (runLocationsBtn.getStatus().equals("selected")) {
				IECaseComponent lCaseComponent = sSpring.getCaseComponent("locations", "");
				IXMLTag lLocationTag = getLocation(currentLocationTagId);
				if (lLocationTag == null)
					setNoteCaseComponent(lCaseComponent);
				else
					setNoteTag(lCaseComponent, lLocationTag);
			}
			if (runEmpackBtn.getStatus().equals("selected")) {
				IECaseComponent lCaseComponent = sSpring.getCaseComponent("empack", "");
				setNoteCaseComponent(lCaseComponent);
			}
		}
		else if (action.equals("showTasks")) {
			// shows tasks as modal popup
			if (runEmpackBtn != null)
				runEmpackBtn.deselect();
			if (runLocationsBtn != null)
				runLocationsBtn.deselect();
			if (runNoteBtn != null)
				runNoteBtn.deselect();
			setChoiceAreaStatus(getId(), "tasks", null);
			IECaseComponent lCaseComponent = sSpring.getCaseComponent("tasks", "");
			if (lCaseComponent != null) {
				sSpring.setRunComponentStatus(lCaseComponent,
						AppConstants.statusKeySelected,
						AppConstants.statusValueTrue, true, AppConstants.statusTypeRunGroup, true);
				sSpring.setRunComponentStatus(lCaseComponent,
						AppConstants.statusKeyOpened,
						AppConstants.statusValueTrue, true, AppConstants.statusTypeRunGroup, true);
				Map<String,Object> lParams = new HashMap<String,Object>();
				lParams.put("item", lCaseComponent);
				CDesktopComponents.vView().modalPopup(VView.v_run_tasks, null, lParams, "center");
			}
		}
		else if (action.equals("endTasks")) {
			IECaseComponent lCaseComponent = sSpring.getCaseComponent("tasks", "");
			if (lCaseComponent != null)
				sSpring.setRunComponentStatus(lCaseComponent,
						AppConstants.statusKeyOpened,
						AppConstants.statusValueFalse, true, AppConstants.statusTypeRunGroup, true);
			// deselect tasks button
			if (runTasksBtn != null)
				runTasksBtn.deselect();
			restoreChoiceAreaStatus(sender);
		}
		else if (action.equals("showNote")) {
			// shows note as modal popup
			if (runEmpackBtn != null)
				runEmpackBtn.deselect();
			if (runLocationsBtn != null)
				runLocationsBtn.deselect();
			if (runTasksBtn != null)
				runTasksBtn.deselect();
			IECaseComponent lCaseComponent = sSpring.getCaseComponent("note", "");
			if (lCaseComponent != null) {
				sSpring.setRunComponentStatus(lCaseComponent,
						AppConstants.statusKeySelected,
						AppConstants.statusValueTrue, true, AppConstants.statusTypeRunGroup, true);
				sSpring.setRunComponentStatus(lCaseComponent,
						AppConstants.statusKeyOpened,
						AppConstants.statusValueTrue, true, AppConstants.statusTypeRunGroup, true);
				setChoiceAreaStatus(getId(), "note", null);
				setNoteContent();
			}
		}
		else if (action.equals("createNote")) {
			// create runnote by set status of choice area to note and restoring it
			// TODO in new situation note component already exists
			setChoiceAreaStatus(getId(), "note", null);
			restoreChoiceAreaStatus(getId());
		}
		else if (action.equals("endNote")) {
			IECaseComponent lCaseComponent = sSpring.getCaseComponent("note", "");
			if (lCaseComponent != null)
				sSpring.setRunComponentStatus(lCaseComponent,
						AppConstants.statusKeyOpened,
						AppConstants.statusValueFalse, true, AppConstants.statusTypeRunGroup, true);
			// deselect note button
			if (runNoteBtn != null)
				runNoteBtn.deselect();
			restoreChoiceAreaStatus(sender);
		}
		else if (action.equals("endAlert")) {
			restoreChoiceAreaStatus(sender);
		}
		else if (action.equals("startConversation")) {
			conversationStarted = true;
			if (getChoiceAreaStatus(sender).equals("conversation"))
				// conversation after conversation
				// so run choice area shows conversation interaction
				restoreChoiceAreaStatus(sender);
			setChoiceAreaStatus(sender, "conversation", null);
		}
		else if (action.equals("playFragment")) {
			String lFileExtension = (String)status;
			if ((lFileExtension.equals("flv") || lFileExtension.equals("mp4")) && conversationStarted) {
				// if flv is played by flash player and conversation is started
				doBeforePlayingFlv();
			}
			conversationStarted = false;
		}
		else if (action.equals("endConversation")) {
			restoreChoiceAreaStatus(sender);
		}
		else if (action.equals("onExternalEvent")) {
			String[] lStatus = (String[]) status;
			String lSender = lStatus[0];
			String lReceiver = lStatus[1];
			String lEvent = lStatus[2];
			String lEventData = lStatus[3];
			if (sender.equals("jwplayer")) {
				if (lEvent.equals("onTime")) {
					// if flv is played by flash player the player sends an onTime, in run_flash_fr.zul only one onTime is sent
					doAfterPlayingFlv();
				} 
			}
			else {
				super.onAction(sender, action, status);
			}
		}
		else if (action.equals("close")) {
			saveAllStatus();
		}
		else {
			super.onAction(sender, action, status);
		}
	}

	/**
	 * Hide conversation interaction till flash player starts playing.
	 */
	protected void doBeforePlayingFlv() {
		setStyle("cursor:wait;");
		if (conversationinteraction != null) {
			conversationinteraction.setVisible(false);
		}
	}

	/**
	 * Show conversation interaction if flash player starts playing.
	 */
	protected void doAfterPlayingFlv() {
		if (conversationinteraction != null && !conversationinteraction.isVisible()) {
			conversationinteraction.setVisible(true);
		}
		setStyle("cursor:default;");
	}

	/**
	 * Status change. Is called by SSpring class if script actions have changed status. This status change should
	 * be reflected within the player. Status changes are saved in a buffer and handled by method checkStatusChange.
	 * Only status changes of statusKeyPresent, statusKeyAccessible, statusKeyOpened, statusKeySent and
	 * statusKeyStarted are buffered.
	 *
	 * @param aCaseComponent the a case component
	 * @param aDataTag the a data tag
	 * @param aStatusTag the a status tag
	 * @param aStatusKey the a status key
	 * @param aStatusValue the a status value
	 * @param aFromScript the a from script, whether this method is called from within script, so is a result of a script action
	 */
	@Override
	public void statusChange(IECaseComponent aCaseComponent, IXMLTag aDataTag, IXMLTag aStatusTag, String aStatusKey, String aStatusValue, boolean aFromScript) {
		boolean lPresentChanged = aStatusKey.equals(AppConstants.statusKeyPresent);
		boolean lAccessibleChanged = aStatusKey.equals(AppConstants.statusKeyAccessible);
		boolean lOpenedChanged = aStatusKey.equals(AppConstants.statusKeyOpened);
		boolean lSent = aStatusKey.equals(AppConstants.statusKeySent);
		boolean lStarted = aStatusKey.equals(AppConstants.statusKeyStarted);
		if (!(lPresentChanged || lAccessibleChanged || lOpenedChanged || lSent || lStarted))
			return;
		CRunStatusChange lRunStatusChange = new CRunStatusChange();
		lRunStatusChange.setCaseComponent(aCaseComponent);
		lRunStatusChange.setDataTag(aDataTag);
		lRunStatusChange.setStatusTag(aStatusTag);
		lRunStatusChange.setStatusKey(aStatusKey);
		lRunStatusChange.setStatusValue(aStatusValue);
		pRunStatusChangeList.add(lRunStatusChange);
	}

	/**
	 * Saves all pending status.
	 */
	public void saveAllStatus() {
		sSpring.saveRunCaseComponentsStatus();
	}

	/**
	 * Checks status change. This method reads the status changes buffer and updates the player accordingly,
	 * by updating the different components of the player. Such as navigator, empack, tasks or note button, or
	 * location or empack action buttons, or the content of the run view area.
	 * This method should be made more general in such a way that new Emergo components could be more easyly added.
	 */
	public void checkStatusChange() {
		super.checkStatusChange();
		if ((!pIdle) || (pRunStatusChangeList.size() < 1))
			return;
		pIdle = false;
		while (pRunStatusChangeList.size() >= 1) {
			CRunStatusChange lRunStatusChange = (CRunStatusChange)pRunStatusChangeList.get(0);
			super.checkStatusChange(lRunStatusChange);
			IECaseComponent lCaseComponent = lRunStatusChange.getCaseComponent();
			IXMLTag lDataTag = lRunStatusChange.getDataTag();
			IXMLTag lStatusTag = lRunStatusChange.getStatusTag();
			String lStatusKey = lRunStatusChange.getStatusKey();
			String lStatusValue = lRunStatusChange.getStatusValue();
			pRunStatusChangeList.remove(0);

			boolean lUpdateOfViewedContent = false;
/*			if (runViewArea.getRunComponentView() != null) {
				if (runViewArea.getRunComponentView().getActRunComponent() != null) {
					if (runViewArea.getRunComponentView().getActRunComponent().getCaseComponent() != null) {
						if (runViewArea.getRunComponentView().getActRunComponent().getCaseComponent().getCacId() == lCaseComponent.getCacId()) {
							lUpdateOfViewedContent = runViewArea.getRunComponentView().getActRunComponent().updateContent(lDataTag, lStatusTag, lStatusKey, lStatusValue);
						}
					}
				}
			} */

			if (!lUpdateOfViewedContent) {
				boolean lComp = (lDataTag == null);
				boolean lTag = (lDataTag != null);
				String lTagId = "";
				String lTagName = "";
				if (lTag) {
					lTagId = lDataTag.getAttribute(AppConstants.keyId);
					lTagName = lDataTag.getName();
				}
				String lStatusTagName = "";
				if (lStatusTag != null)
					lStatusTagName = lStatusTag.getName();
				boolean lPresentChanged = lStatusKey.equals(AppConstants.statusKeyPresent);
				boolean lAccessibleChanged = lStatusKey.equals(AppConstants.statusKeyAccessible);
				boolean lOpenedChanged = lStatusKey.equals(AppConstants.statusKeyOpened);
				boolean lOpenedTrue = ((lOpenedChanged) && (lStatusValue.equals(AppConstants.statusValueTrue)));
				boolean lSent = lStatusKey.equals(AppConstants.statusKeySent);
				boolean lStarted = lStatusKey.equals(AppConstants.statusKeyStarted);
				boolean lChangeBtn = (lPresentChanged || lAccessibleChanged);
				boolean lSentTrue = ((lSent) && (lStatusValue.equals(AppConstants.statusValueTrue)));
				String lCode = lCaseComponent.getEComponent().getCode();
				boolean lAlert = ((lSentTrue) && (lCode.equals("alerts")));
				boolean lMail = ((lSentTrue) && (lCode.equals("mail")));
				boolean lAlertTag = (lAlert && (lTagName.equals("alert")));

				boolean lMailTag = (lMail && (!lTagName.equals("map")));
				// lDataTag = null for outmails becoming inmails for other rug's
				boolean lShowMailAlert = ((lMailTag) && ((lStatusTagName.equals("inmailpredef") || lStatusTagName.equals("inmailhelp"))));

				boolean lLocation = (lCode.equals("locations"));
				boolean lLocationTag = (lLocation && (lTagName.equals("location")));
				boolean lLocationBackgroundTag = (lLocation && (lTagName.equals("background")));
				boolean lEmpack = (lCode.equals("empack"));
				boolean lEmpackComp = (lEmpack && lComp);
				boolean lEmpackAction = (caseComponentIsActionOn(lCaseComponent, "empack"));
				boolean lEmpackActionComp = (lEmpackAction && lComp);
				boolean lNote = (lCode.equals("note"));
				boolean lNoteComp = (lNote && lComp);
				boolean lTasks = (lCode.equals("tasks"));
				boolean lTasksComp = (lTasks && lComp);
				boolean lConversations = (lCode.equals("conversations"));
				boolean lConversationTag = (lConversations && (lTagName.equals("conversation")));
				boolean lAssessmentTagPresentChanged = (lPresentChanged && (lCode.equals("assessments")) && lTag);
				String lTagPid = "";
				if (lTag) {
					lTagPid = lDataTag.getChildValue("name");
					if (lTagPid.equals(""))
						lTagPid = lDataTag.getChildValue("pid");
				}
				if (lAlertTag || lShowMailAlert) {
					String lValue = "";
					if (lAlertTag)
						lValue = lDataTag.getChildValue("richtext");
					else
						lValue = CDesktopComponents.vView().getLabel("run_alert.text.mail_alert");
					lValue = sSpring.unescapeXML(lValue);
					Map<String,Object> lParams = new HashMap<String,Object>();
					lParams.put("item", lValue);
					setChoiceAreaStatus(getId(), "alert", null);
					CRunAlert lRunAlert = (CRunAlert)CDesktopComponents.vView().getComponent("runAlert");
					if (lRunAlert != null)
						lRunAlert.setContent(lValue);
					if (lShowMailAlert) {
						CRunMailTree lRunMailTree = (CRunMailTree)CDesktopComponents.vView().getComponent("runMailTree");
						if (lRunMailTree != null)
							lRunMailTree.update();
					}
				}
				if (lLocationTag && lChangeBtn) {
					CRunLocationBtnsNeoclassic lLocBtns = (CRunLocationBtnsNeoclassic) CDesktopComponents.vView().getComponent("runLocationBtns");
					if (lLocBtns != null) {
						lLocBtns.updateBtn("location" + lTagId + "Btn", lStatusKey, lStatusValue);
					}
				}
				if (lLocationTag && lOpenedTrue) {
					if (!currentLocationTagId.equals(lDataTag.getAttribute(AppConstants.keyId)))
						onTimedAction(getId(), "beamLocation", lDataTag.getAttribute(AppConstants.keyId));
				}
				if (lEmpackActionComp && lChangeBtn) {
					CRunEmpackActionBtnsNeoclassic lEmBtns = (CRunEmpackActionBtnsNeoclassic) CDesktopComponents.vView().getComponent("runEmpackActionBtns");
					if (lEmBtns != null) {
						lEmBtns.updateBtn("empackaction" + lCaseComponent.getCacId() + "Btn", lStatusKey, lStatusValue);
					}
				}
				if (lEmpackComp && lChangeBtn) {
					updateBtn("runEmpackBtn", lStatusKey, lStatusValue);
				}
				if (lNoteComp && lChangeBtn) {
					updateBtn("runNoteBtn", lStatusKey, lStatusValue);
				}
				if (lTasksComp && lChangeBtn) {
					updateBtn("runTasksBtn", lStatusKey, lStatusValue);
				}
				if (lConversations) {
					if (lConversationTag && lStarted) {
						IXMLTag lLocationTag2 = getLocation(currentLocationTagId);
						// conversation can be triggered by another conversation in
						// which case the former is not yet finished, so use timed action
						onTimedAction(getId(), "showLocationActions", lLocationTag2);
					} else {
						CRunConversations lRunConversations = (CRunConversations)CDesktopComponents.vView().getComponent("runConversations");
						if (lRunConversations != null){
							STriggeredReference lTriggeredReference = new STriggeredReference(
									lRunStatusChange.getCaseComponent(),
									lRunStatusChange.getDataTag(),
									lRunStatusChange.getStatusTag(),
									lRunStatusChange.getStatusKey(),
									lRunStatusChange.getStatusValue(),
									lRunStatusChange.isFromScript());
							lRunConversations.update(lTriggeredReference);
						}
					}
				}
				if (lEmpackActionComp && lStarted) {
					onTimedAction(getId(), "showEmpackAction", ""+lCaseComponent.getCacId());
				}
				if (lLocationBackgroundTag && lOpenedChanged) {
					// show location background
					IXMLTag lLocationTag2 = getLocation(lDataTag.getParentTag().getAttribute(AppConstants.keyId));
					if (((String) lLocationTag2.getAttribute(AppConstants.keyId)).equals(currentLocationTagId))
						// only refresh background if background change of current location.
						setViewAreaLocationBackground(getId(), lLocationTag2);
				}
				if (lAssessmentTagPresentChanged) {
					CRunAssessments lRunAssessment = (CRunAssessments)CDesktopComponents.vView().getComponent("runAssessments");
					if (lRunAssessment != null) {
						lRunAssessment.setItemTagStatus(lDataTag.getParentTag().getAttribute(AppConstants.keyId), lDataTag.getAttribute(AppConstants.keyId), lStatusKey, lStatusValue);
						lRunAssessment.renderAssessment(lDataTag.getParentTag().getAttribute(AppConstants.keyId));
					}
				}
			}
		}
		pIdle = true;
	}

	/**
	 * Checks update external tags. Is called regularly by CRunMainTimer.
	 * Checks if there are updates of other users meant for the current user and if so updatesthe run view area accordingly.
	 * For instance if another user sends an email to the current user, this email should become visible within the
	 * emessages component of the current user. Or if users share a certain component, contributions of a user should become
	 * visible for the other users.
	 */
	public void checkUpdateExternalTags() {
		if (componentViewActRunComponent != null) {
			if (componentViewActRunComponent.getRunContentComponent() != null) {
				if (componentViewActRunComponent.getRunContentComponent() instanceof CRunTree)
					((CRunTree)componentViewActRunComponent.getRunContentComponent()).updateWithExternalTags();
			}
		}
	}


	/**
	 * Gets the location tags of the locations component. There is only one locations component within a case.
	 *
	 * @return the location tags
	 */
	public List<IXMLTag> getLocationTags() {
		// only one location case component per case per caserole
		IECaseComponent lCaseComponent = sSpring.getCaseComponent("locations", "");
		if (lCaseComponent == null)
			return null;
		return getCaseComponentRootTagChildTags(lCaseComponent);
	}

	/**
	 * Gets the preselected location tag. Is used for preview option. If preview should start with a certain case component tag.
	 *
	 * @return the preselected location tag
	 */
	protected IXMLTag getPreselectedLocationTag() {
		IXMLTag lTag = preselectedTag;
		if ((lTag != null) && (lTag.getName().equals("location")))
			return lTag;
		return null;
	}

	/**
	 * Gets the location given by aTagId.
	 *
	 * @param aTagId the a tag id
	 *
	 * @return the location
	 */
	protected IXMLTag getLocation(String aTagId) {
		if (aTagId == null)
			return null;
		List<IXMLTag> lLocations = getLocationTags();
		if (lLocations == null)
			return null;
		for (IXMLTag lLocation : lLocations) {
			if (aTagId.equals(lLocation.getAttribute(AppConstants.keyId)))
				return lLocation;
		}
		return null;
	}

	/**
	 * Has case component location actions. Checks if aCaseComponent has location actions.
	 *
	 * @param aCaseComponent the case component
	 *
	 * @return true, if succesfully
	 */
	protected boolean hasCaseComponentLocationActions(IECaseComponent aCaseComponent) {
		IECaseComponent lComponent = sSpring.getCaseComponent("locations", "");
		if (lComponent == null)
			// if no locations then no actions on location
			return false;
		List<List<Object>> lLocActions = new ArrayList<List<Object>>();
		List<IXMLTag> lLocationTags = getLocationTags();
		for (IXMLTag lLocationTag : lLocationTags) {
			getCaseComponentLocationActions(aCaseComponent, (lLocationTag != null), lLocationTag, lLocActions);
		}
		return (lLocActions.size() > 0);
	}

	/**
	 * Has case component location actions for current location. Checks if aCaseComponent has location actions
	 * for current location.
	 *
	 * @param aCaseComponent the case component
	 *
	 * @return true, if succesfully
	 */
	protected boolean hasCaseComponentLocationActionsForCurrentLocation(IECaseComponent aCaseComponent) {
		IECaseComponent lComponent = sSpring.getCaseComponent("locations", "");
		if (lComponent == null)
			// if no locations then no actions on location
			return false;
		IXMLTag lLocationTag = getLocation(currentLocationTagId);
		List<List<Object>> lLocActions = new ArrayList<List<Object>>();
		getCaseComponentLocationActions(aCaseComponent, (lLocationTag != null), lLocationTag, lLocActions);
		return (lLocActions.size() > 0);
	}

	/**
	 * Case component is action on case component given by aParentCode.
	 *
	 * @param eCaseComponent the e case component
	 * @param aParentCode the a parent code
	 *
	 * @return true, if successful
	 */
	protected boolean caseComponentIsActionOn(IECaseComponent eCaseComponent, String aParentCode) {
		IEComponent lComponent = eCaseComponent.getEComponent();
		IEComponent lParent = null;
		int parentComId = lComponent.getComComId();
		if (parentComId > 0)
			lParent = sSpring.getComponent(""+parentComId);
		if ((lParent != null) && (lParent.getCode().equals(aParentCode)))
			return true;
		if (aParentCode.equals(""))
			return true;
		return false;
	}

	/**
	 * Gets the preselected location actions. Is used for preview option. If preview should start with certain location actions.
	 *
	 * @return the preselected location actions
	 */
	protected List<List<Object>> getPreselectedLocationActions() {
		List<List<Object>> lActions = new ArrayList<List<Object>>();
		// is used for previewing. previewing is done on only 1 casecomponent,
		// saved in session
		IECaseComponent lCaseComponent = preselectedCaseComponent;
		if (lCaseComponent == null)
			return lActions;
		if (!caseComponentIsActionOn(lCaseComponent, "locations")) {
			if (!hasCaseComponentLocationActionsForCurrentLocation(lCaseComponent))
				return lActions;
		}
		// possibly to use casecomponent tag is saved in session
		IXMLTag lTag = preselectedTag;
		// if tag is not null is must be a previewed conversation tag
		if ((lTag != null) && (!lTag.getName().equals("conversation")) && (!lTag.getName().equals(AppConstants.contentElement)))
			return lActions;
		List<Object> lObjects = new ArrayList<Object>();
		lObjects.add(lCaseComponent);
		lObjects.add(lTag);
		lActions.add(lObjects);
		return lActions;
	}

	/**
	 * Gets the locations component. There is only only one within a case.
	 *
	 * @return the locations component
	 */
	protected IECaseComponent getLocationsComponent() {
		if (locationsComponent == null)
			locationsComponent = sSpring.getCaseComponent("locations", "");
		return locationsComponent;
	}

	/**
	 * Gets the empack action components, a list of case components which are in the empack.
	 *
	 * @return the empack action components
	 */
	public List<IECaseComponent> getEmpackActionComponents() {
		IECase lCase = sSpring.getCase();
		if (lCase == null)
			return null;
		List<IECaseComponent> lCaseComponents = sSpring.getCaseComponents(lCase);
		if ((lCaseComponents == null) || (lCaseComponents.size() == 0))
			return null;
		List<IECaseComponent> lActions = new ArrayList<IECaseComponent>();
		for (IECaseComponent lCaseComponent : lCaseComponents) {
			if (caseComponentIsActionOn(lCaseComponent, "empack")) {
				if (!hasCaseComponentLocationActions(lCaseComponent))
					lActions.add(lCaseComponent);
			}
		}
		return lActions;
	}

	/**
	 * Gets the preselected empack action. Is used for preview option. If preview should start with a certain case component.
	 *
	 * @return the preselected empack action or null if none
	 */
	protected IECaseComponent getPreselectedEmpackAction() {
		IECaseComponent lCaseComponent = preselectedCaseComponent;
		if (lCaseComponent != null) {
			if (caseComponentIsActionOn(lCaseComponent, "empack")) {
				if (!hasCaseComponentLocationActions(lCaseComponent))
					return lCaseComponent;
			}
		}
		return null;
	}

	/**
	 * Gets the empack action, a case component, given by aCacId.
	 *
	 * @param aCacId the a cac id
	 *
	 * @return the empack action or null if none
	 */
	protected IECaseComponent getEmpackAction(String aCacId) {
		if (aCacId == null)
			return null;
		List<IECaseComponent> lEmpackActions = getEmpackActionComponents();
		if (lEmpackActions == null)
			return null;
		for (IECaseComponent lCaseComponent : lEmpackActions) {
			if (aCacId.equals("" + lCaseComponent.getCacId())) {
				return lCaseComponent;
			}
		}
		return null;
	}

	/**
	 * Gets current location actions.
	 *
	 * @param aLocationTag the a location tag
	 *
	 * @return the location actions
	 */
	protected List<List<Object>> getLocationActions(IXMLTag aLocationTag) {
		List<List<Object>> lActions = new ArrayList<List<Object>>();
		IECase lCase = sSpring.getCase();
		if (lCase == null)
			return lActions;
		if (possibleCaseComponentsOnLocation == null) {
			possibleCaseComponentsOnLocation = sSpring.getCaseComponents(lCase, "conversations");
			possibleCaseComponentsOnLocation.addAll(sSpring.getCaseComponents(lCase, "references"));
			possibleCaseComponentsOnLocation.addAll(sSpring.getCaseComponents(lCase, "assessments"));
		}
		if (possibleCaseComponentsOnLocation == null)
			return lActions;
		boolean lFilterOnLocation = (aLocationTag != null);
		// if no filtering on location get all presentable location actions
		// in case one does not use a location component and for previewing
		// location actions
		for (IECaseComponent lCaseComponent : possibleCaseComponentsOnLocation) {
			getCaseComponentLocationActions(lCaseComponent, lFilterOnLocation, aLocationTag, lActions);
		}
		return lActions;
	}

	/**
	 * Gets the case component location actions.
	 *
	 * @param aCaseComponent the case component
	 * @param aFilterOnLocation filter on location, if not give all
	 * @param aLocationTag the location tag
	 * @param aActions used to return the actions
	 */
	protected void getCaseComponentLocationActions(IECaseComponent aCaseComponent, boolean aFilterOnLocation, IXMLTag aLocationTag, List<List<Object>> aActions) {
		boolean lConversationTags = (aCaseComponent.getEComponent().getCode().equals("conversations"));
		boolean lComponentRootTag = ((aCaseComponent.getEComponent().getCode().equals("references")) ||
				(aCaseComponent.getEComponent().getCode().equals("assessments")));
		if (!(lConversationTags || lComponentRootTag))
			return;
		// casecomponent has to be present
		boolean lCacPresent = (!(sSpring.getCurrentRunComponentStatus(
				aCaseComponent, AppConstants.statusKeyPresent, AppConstants.statusTypeRunGroup))
				.equals(AppConstants.statusValueFalse));
		if (!lCacPresent)
			return;
		String lCrossReferenceId = "";
		if (lConversationTags)
			lCrossReferenceId = "locationtags_conversationtags";
		if (lComponentRootTag)
			lCrossReferenceId = "locationtags_componentroot";
		List<String> lTagIds = null;
		if (aFilterOnLocation) {
			// get tag ids coupled to location
			lTagIds = cCaseHelper.getTagRefIds(lCrossReferenceId, "" + AppConstants.statusKeySelectedIndex,
					sSpring.getSCaseRoleHelper().getCaseRole(), getLocationsComponent(), aLocationTag, false);
		}
		List<IXMLTag> lTags = null;
		if (lConversationTags)
			lTags = getCaseComponentRootTagChildTags(aCaseComponent);
		if (lComponentRootTag) {
			IXMLTag lRootTag = getCaseComponentRootTag(aCaseComponent);
			lTags = new ArrayList<IXMLTag>();
			lTags.add(lRootTag);
		}
		if (lTags != null) {
			for (IXMLTag lTag : lTags) {
				String lTagId = lTag.getAttribute(AppConstants.keyId);
				String lStatus = sSpring.getCurrentTagStatus(lTag, AppConstants.statusKeyPresent);
				boolean lPresent = true;
				if (lConversationTags)
					// conversation tag has to be present
					lPresent = (lStatus.equals(AppConstants.statusValueTrue));
				if (lComponentRootTag)
					// root tag is always present
					;
				boolean lOnLocation = false;
				if (aFilterOnLocation) {
					if (lPresent) {
						for (String lIds : lTagIds) {
							String[] lIdArr = lIds.split(",");
							// TagId and CacId equal!
							if ((lIdArr[2].equals(lTagId)) && (lIdArr[1].equals("" + aCaseComponent.getCacId())))
								lOnLocation = true;
						}
					}
				} else
					lOnLocation = true;
				if (lPresent && lOnLocation) {
					List<Object> lObjects = new ArrayList<Object>();
					lObjects.add(aCaseComponent);
					if (lConversationTags) {
						lObjects.add(lTag);
						lObjects.add(lTag.getChildValue("name"));
					}
					if (lComponentRootTag) {
						lObjects.add(null);
						lObjects.add(sSpring.getCaseComponentRoleName(aCaseComponent));
					}
					aActions.add(lObjects);
				}
			}
		}
	}






	/** The choice area status. */
	protected String choiceAreaStatus = "";

	/** The empackactionbtns, used to open empack case components. */
	protected CRunEmpackActionBtnsNeoclassic empackactionbtns = null;

	/** The locationbtns, used to choose locations. */
	protected CRunLocationBtnsNeoclassic locationbtns = null;

	/** The conversationinteraction, used to ask questions during conversations. */
	protected CRunConversationInteraction conversationinteraction = null;

	/** The alert, used to show alerts. */
	protected CRunAlert alert = null;

	/** The note, used to make contextualised notes. */
	protected CRunNoteNeoclassic note = null;

	/** The tasks area, used to cover choice area. */
	protected CRunTasksAreaNeoclassic tasksArea = null;

	/** The previous choice area status, used to be able to present a previous state, for instance after closing an alert. */
	protected List<String> previousChoiceAreaStatus = new ArrayList<String>();

	public String getChoiceAreaStatus(String senderId) {
		return choiceAreaStatus;
	}

	public void setChoiceAreaStatus(String senderId, String status, CRunComponent referenceObject) {
		previousChoiceAreaStatus.add(status);
		pSetChoiceAreaStatus(status, referenceObject);
	}

	public void restoreChoiceAreaStatus(String senderId) {
		String lPreviousStatus = "";
		int lSize = previousChoiceAreaStatus.size();
		if (lSize > 1) {
			// get previous state
			lPreviousStatus = previousChoiceAreaStatus.get(lSize-2);
			// remove last added state
			previousChoiceAreaStatus.remove(lSize-1);
			// set previous state
			pSetChoiceAreaStatus(lPreviousStatus, null);
			if ((lPreviousStatus.equals("alert")) && (alert != null))
				alert.restoreContent();

			if (lPreviousStatus.equals("empack"))
				runEmpackBtn.setBtnSelected(true);
			if (lPreviousStatus.equals("locations"))
				runLocationsBtn.setBtnSelected(true);
			if (lPreviousStatus.equals("note"))
				runNoteBtn.setBtnSelected(true);
		
		}

	}

	/**
	 * Sets status of choice area. It is either 'locations', 'empack', 'conversation',
	 * 'alert' or 'note'.
	 * It creates the appropiate ZK components if they don't exist yet.
	 * And for 'locations' or 'empack' scroll arrows are shown if the choice area is
	 * too small to show all choices.
	 * aReferenceObject can be null but otherwise is another ZK component that is
	 * necessary to render the content of the choice area. For instance for 'conversation'
	 * it is equal to the CRunConversations component.
	 *
	 * @param aStatus the a status
	 * @param aReferenceObject the a reference object
	 */
	protected void pSetChoiceAreaStatus(String aStatus, CRunComponent aReferenceObject) {
		choiceAreaStatus = aStatus;
		boolean lEmpack = (aStatus.equals("empack"));
		boolean lLocations = (aStatus.equals("locations"));
		boolean lConversation = (aStatus.equals("conversation"));
		boolean lAlert = (aStatus.equals("alert"));
		boolean lNote = (aStatus.equals("note"));
		boolean lTasks = (aStatus.equals("tasks"));
		if ((lEmpack) && (empackactionbtns == null)) {
			empackactionbtns = (CRunEmpackActionBtnsNeoclassic)CDesktopComponents.vView().getComponent("runEmpackActionBtns");
			if (empackactionbtns == null) {
				empackactionbtns = new CRunEmpackActionBtnsNeoclassic(this, "runEmpackActionBtns");
				appendChild(empackactionbtns);
			}
			else {
				// TODO init
			}
		}
		if ((lLocations) && (locationbtns == null)) {
			locationbtns = (CRunLocationBtnsNeoclassic)CDesktopComponents.vView().getComponent("runLocationBtns");
			if (locationbtns == null) {
				locationbtns = new CRunLocationBtnsNeoclassic(this, "runLocationBtns");
				appendChild(locationbtns);
			}
			else {
				// TODO init
			}
		}
		if ((lConversation) && (conversationinteraction == null)) {
			conversationinteraction = (CRunConversationInteraction)CDesktopComponents.vView().getComponent("runConversationInteraction");
			if (conversationinteraction == null) {
				conversationinteraction = new CRunConversationInteraction("runConversationInteraction", aReferenceObject, "runConversations");
				appendChild(conversationinteraction);
			}
			else {
				// TODO init
			}
		}
		if ((lAlert) && (alert == null)) {
			alert = (CRunAlert)CDesktopComponents.vView().getComponent("runAlert");
			if (alert == null) {
				alert = new CRunAlert("runAlert", aReferenceObject);
				appendChild(alert);
			}
			else {
				// TODO init
			}
		}
		if ((lNote) && (note == null)) {
			note = (CRunNoteNeoclassic)CDesktopComponents.vView().getComponent("runNote");
			if (note == null) {
				note = new CRunNoteNeoclassic("runNote", aReferenceObject);
				appendChild(note);
			}
			else {
				// TODO init
			}
		}
		if ((lTasks) && (tasksArea == null)) {
			tasksArea = (CRunTasksAreaNeoclassic)CDesktopComponents.vView().getComponent("runTasksArea");
			if (tasksArea == null) {
				tasksArea = new CRunTasksAreaNeoclassic("runTasksArea", aReferenceObject);
				appendChild(tasksArea);
			}
			else {
				// TODO init
			}
		}
		if (empackactionbtns != null) {
			empackactionbtns.showArrows(lEmpack);
			empackactionbtns.setVisible(lEmpack);
		}
		if (locationbtns != null) {
			locationbtns.showArrows(lLocations);
			locationbtns.setVisible(lLocations);
		}
		if (conversationinteraction != null)
			conversationinteraction.setVisible(lConversation);
		if (alert != null)
			alert.setVisible(lAlert);
		if (note != null)
			note.setVisible(lNote);
		if (tasksArea != null)
			tasksArea.setVisible(lTasks);
	}




	/** The view area status. */
	protected String viewAreaStatus = "";

	/** The locationarea, used to show location image. */
	protected CRunLocationArea locationarea = null;

	/** The view area actcasecomponent, the current casecomponent for which content is shown. */
	protected IECaseComponent viewAreaActCaseComponent = null;

	/** The view area actcasecomponenttag, the current tag of the casecomponent for which content is shown. */
	protected IXMLTag viewAreaActCaseComponentTag = null;

	public IECaseComponent getActCaseComponent(String senderId) {
		return viewAreaActCaseComponent;
	}

	public IXMLTag getActCaseComponentTag(String senderId) {
		return viewAreaActCaseComponentTag;
	}

	public void setViewAreaStatus(String senderId, String status) {
		viewAreaStatus = status;
		if ((status.equals("location")) && (locationarea == null)) {
			locationarea = (CRunLocationArea)CDesktopComponents.vView().getComponent("runLocationArea");
			if (locationarea == null) {
				locationarea = new CRunLocationArea("runLocationArea");
				appendChild(locationarea);
			}
			else {
				// TODO init
			}
		}
		if (locationarea != null)
			locationarea.setVisible(status.equals("location"));
	}

	public void setViewAreaStatus(String senderId, String action, Object status) {
		viewAreaActCaseComponentTag = null;
		CRunTitleArea lTitleArea = (CRunTitleArea) CDesktopComponents.vView().getComponent("runTitleArea");
		if (lTitleArea != null) {
			lTitleArea.setMediaBufferingVisible(false);
			lTitleArea.setMediaBtnsVisible(false);
		}
		if (action.equals("showLocation")) {
			setViewAreaStatus(senderId, "location");
			viewAreaActCaseComponent = sSpring.getCaseComponent("locations", "");
			locationarea.showLocation(status);
		}
		if ((action.equals("showEmpackAction")) || (action.equals("showLocationAction"))) {
			setViewAreaStatus(senderId, "component");
			if (action.equals("showLocationAction")) {
				viewAreaActCaseComponent = (IECaseComponent) ((List) status).get(0);
				viewAreaActCaseComponentTag = (IXMLTag) ((List) status).get(1);
			} else
				viewAreaActCaseComponent = (IECaseComponent) status;
			onComponentViewAction(senderId, action, status);
		}
	}

	public void setViewAreaLocationBackground(String senderId, Object locationTag) {
		locationarea.showLocation(locationTag);
	}

	/** The component view act case component. */
	protected IECaseComponent componentViewActCaseComponent = null;

	/** The component view act component, the current run component shown within the area. */
	protected CRunComponent componentViewActRunComponent = null;

	/**
	 * On action. Show content within view depending on parameters. It is either an empack action or a location action.
	 * Either an empack component is shown or a component that is only shown on location.
	 *
	 * @param sender the sender
	 * @param action the action, a string identifying the type of action
	 * @param status the status, an object containing status info
	 */
	public void onComponentViewAction(String sender, String action, Object status) {
		if (action.equals("showEmpackAction"))
			showComponentViewEmpackAction((IECaseComponent) status);
		if (action.equals("showLocationAction")) {
			showComponentViewLocationAction((List) status);
		}
	}

	/**
	 * Shows empack action. Shows case component content within area for case component on empack.
	 * For instance for references.
	 *
	 * @param aCaseComponent the a case component
	 */
	public void showComponentViewEmpackAction(IECaseComponent aCaseComponent) {
		if (aCaseComponent == null)
			return;
		if (componentViewActRunComponent != null)
			componentViewActRunComponent.detach();
		componentViewActCaseComponent = aCaseComponent;
		componentViewActRunComponent = null;
		String lCode = componentViewActCaseComponent.getEComponent().getCode();
		if (lCode.equals("references"))
			componentViewActRunComponent = new CRunReferences("runReferences", componentViewActCaseComponent);
		if (lCode.equals("mail"))
			componentViewActRunComponent = new CRunMail("runMail", componentViewActCaseComponent);
		if (lCode.equals("assessments"))
			componentViewActRunComponent = new CRunAssessments("runAssessments", componentViewActCaseComponent);
		if (lCode.equals("logbook"))
			componentViewActRunComponent = new CRunLogbookNeoclassic("runLogbook", componentViewActCaseComponent);
		if (lCode.equals("googlemaps"))
			componentViewActRunComponent = new CRunGooglemaps("runGooglemaps", componentViewActCaseComponent);
		if (lCode.equals("canon"))
			componentViewActRunComponent = new CRunCanon("runCanon", componentViewActCaseComponent);
		if (lCode.equals("canonresult"))
			componentViewActRunComponent = new CRunCanonResult("runCanonResult", componentViewActCaseComponent);
		if (componentViewActRunComponent != null)
			appendChild(componentViewActRunComponent);
	}

	/**
	 * Shows location action. Shows case component content within area for case component on location.
	 * For instance for conversations.
	 * If more than one item within aCaseComponents the first item is used.
	 *
	 * @param aCaseComponents the a case components which should be shown
	 */
	public void showComponentViewLocationAction(List<IECaseComponent> aCaseComponents) {
		// check if casecomponent is instance of conversations component
		if (componentViewActRunComponent != null)
			componentViewActRunComponent.detach();
		componentViewActCaseComponent = (IECaseComponent) aCaseComponents.get(0);
		componentViewActRunComponent = null;
		String lCode = componentViewActCaseComponent.getEComponent().getCode();
		if (lCode.equals("conversations"))
			componentViewActRunComponent = new CRunConversations("runConversations", componentViewActCaseComponent);
		if (lCode.equals("references"))
			componentViewActRunComponent = new CRunReferences("runReferences", componentViewActCaseComponent);
		if (lCode.equals("assessments"))
			componentViewActRunComponent = new CRunAssessments("runAssessments", componentViewActCaseComponent);
		if (componentViewActRunComponent != null)
			appendChild(componentViewActRunComponent);
	}

	/**
	 * Ends component. Closes component and set opened of case component to false.
	 */
	public void endComponentViewComponent() {
		if (componentViewActCaseComponent != null) {
			sSpring.setRunComponentStatus(componentViewActCaseComponent, AppConstants.statusKeyOpened, AppConstants.statusValueFalse, true, AppConstants.statusTypeRunGroup, true);
			componentViewActRunComponent.detach();
		}
//		maybe add something for shared component
	}

	/**
	 * Gets the current run component.
	 *
	 * @return the act run component
	 */
	public CRunComponent getComponentViewActRunComponent() {
		return componentViewActRunComponent;
	}

}
