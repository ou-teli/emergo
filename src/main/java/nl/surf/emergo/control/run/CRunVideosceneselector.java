/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.utilities.PropsValues;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.STriggeredReference;

/**
 * The Class CRunVideosceneselector is used to show videosceneselector component
 * within the run view area of the emergo player..
 */
public class CRunVideosceneselector extends CRunForms {

	private static final long serialVersionUID = 7319956220123942837L;

	protected IXMLTag currentFragmentTag = null;

	public final static String runVideosceneselectorId = "runVideosceneselector";

	public final static String videosceneselectorPlayerName = "EM_fl_videosceneselector";
	public final static String defaultVideoWidthStr = "826px";
	public final static String defaultVideoHeightStr = "464px";

	public final static String[] textColors = new String[] { "#E9967A", "#2196F3", "#DA70D6", "#F0E68C", "#8FBC8F",
			"#FF69B4", "#F08080", "#00DD00", "#00DDDD", "#DD0000", "#DD00DD", "#DDDD00", "#00AA00", "#00AAAA",
			"#AA0000", "#AA00AA", "#AAAA00", "#007700" };

	public int maxNumberOfScenesToSelect = 0;
	public int minNumberOfCorrectScenesToSelect = 0;
	public boolean showControls = false;
	public int videoWidth = 0;
	public int videoHeight = 0;

	/**
	 * Instantiates a new c run videosceneselector.
	 */
	public CRunVideosceneselector() {
		super("runVideosceneselector", null);
		conversationsFeedbackIdSuffix = "Videosceneselector";
		init();
	}

	/**
	 * Instantiates a new c run videosceneselector.
	 *
	 * @param aId            the a id
	 * @param aCaseComponent the case component
	 */
	public CRunVideosceneselector(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
		conversationsFeedbackIdSuffix = "Videosceneselector";
		init();
	}

	/**
	 * Creates title area, content area to show tab structure, and buttons area with
	 * close button.
	 */
	@Override
	protected void createComponents() {
		CRunVbox lVbox = new CRunVbox();

		createTitleArea(lVbox);
		createContentArea(lVbox);
		createButtonsArea(lVbox);

		appendChild(lVbox);
	}

	/**
	 * Creates new content component, the tree.
	 *
	 * @return the component
	 */
	@Override
	protected Component newContentComponent() {
		return newContentComponent("runVideosceneselectorView");
	}

	/**
	 * Creates buttons area and adds close button.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run hbox
	 */
	@Override
	protected CRunHbox createButtonsArea(Component aParent) {
		CRunHbox lButtonsHbox = super.createButtonsArea(aParent);
		createCloseButton(lButtonsHbox);
		return lButtonsHbox;
	}

	/**
	 * Creates view.
	 */
	public void init() {
		Events.echoEvent("onInitZulfile", this, null);
	}

	/**
	 * Gets the run component helper.
	 *
	 * @return the run component helper
	 */
	@Override
	public CRunComponentHelper getRunComponentHelper() {
		return new CRunComponentHelper(null, "fragment", caseComponent, this);
	}

	/**
	 * Gets fragment tags.
	 *
	 * @return the fragment tags
	 */
	public List<IXMLTag> getFragmentTags() {
		return getPresentChildTags(getDataPlusStatusRootTag().getChild(AppConstants.contentElement), "fragment");
	}

	/**
	 * Gets category tags.
	 *
	 * @param aParentTag the a parent tag
	 * 
	 * @return the category tags
	 */
	public List<IXMLTag> getCategoryTags(IXMLTag aParentTag) {
		return getPresentChildTags(aParentTag, "category");
	}

	/**
	 * Gets scene tags.
	 *
	 * @param aParentTag the a parent tag
	 * 
	 * @return the scene tags
	 */
	public List<IXMLTag> getSceneTags(IXMLTag aParentTag) {
		return getPresentChildTags(aParentTag, "scene");

	}

	/**
	 * Gets all scene tags, that is also tags that are not present.
	 *
	 * @param aParentTag the a parent tag
	 * 
	 * @return the scene tags
	 */
	public List<IXMLTag> getAllSceneTags(IXMLTag aParentTag) {
		return aParentTag.getChilds("scene");
	}

	/**
	 * Set current fragment tag.
	 *
	 * @param aFragmentTag
	 */
	public void setCurrentFragmentTag(IXMLTag aFragmentTag) {
		currentFragmentTag = aFragmentTag;
	}

	/**
	 * Select scene.
	 *
	 * @param aCategoryTag the a scene tag
	 * @param aSceneTag    the a scene tag
	 * @param aSelected    the a selected
	 *
	 * @return if successful
	 */
	public boolean selectScene(IXMLTag aCategoryTag, IXMLTag aSceneTag, boolean aSelected) {
		if (aSceneTag == null) {
			return false;
		}
		setRunTagStatus(getCaseComponent(), aSceneTag, AppConstants.statusKeySelected, "" + aSelected, true);

		String lDataParentId = "";
		boolean lContainerok = false;
		if (aCategoryTag != null) {
			lDataParentId = aCategoryTag.getAttribute(AppConstants.keyId);
			boolean lCorrect = aSceneTag.getCurrentStatusAttribute(AppConstants.statusKeyCorrect)
					.equals(AppConstants.statusValueTrue);
			lContainerok = (lCorrect && aSelected) || (!lCorrect && !aSelected);

			lContainerok = lContainerok
					&& lDataParentId.equals(aSceneTag.getParentTag().getAttribute(AppConstants.keyId));
		}
		setRunTagStatus(getCaseComponent(), aSceneTag, AppConstants.statusKeyDataparentid, lDataParentId, true);
		setRunTagStatus(getCaseComponent(), aSceneTag, AppConstants.statusKeyContainerok, "" + lContainerok, true);
		return true;
	}

	/**
	 * Set number of rightly selected scenes.
	 *
	 * @param aNumber the a number
	 */
	public void setNumberOfRightlySelected(int aNumber) {
		if (currentFragmentTag != null) {
			int lNumber = 0;
			try {
				lNumber = Integer.parseInt(
						currentFragmentTag.getCurrentStatusAttribute(AppConstants.statusKeyNumberofcorrectitems));
			} catch (NumberFormatException e) {
				lNumber = -1;
			}
			if (lNumber != aNumber) {
				setRunTagStatus(getCaseComponent(), currentFragmentTag, AppConstants.statusKeyNumberofcorrectitems,
						"" + aNumber, true);
			}
		}
	}

	/**
	 * Get triggered feedback condition tag. aParentTags are possible parents of
	 * feedback condition tags.
	 *
	 * @return the feedback condition tag
	 */
	@Override
	public IXMLTag getTriggeredFeedbackConditionTag() {
		return super.getTriggeredFeedbackConditionTag(getFragmentTags());
	}

	/**
	 * Show feedback.
	 *
	 * @return the feedback condition tag
	 */
	@Override
	public IXMLTag showFeedback() {
		IXMLTag lFeedbackCondition = super.showFeedback(getFragmentTags());
		if (lFeedbackCondition != null) {
			// NOTE score -1 means 'no relevant score yet' because tool instruction is
			// shown, so don'nt increase number of attempts.
			if (!lFeedbackCondition.getChildValue("score").equals("-1")) {
				// increase numberofattempts for all form tags
				List<IXMLTag> lFragmentTags = getPresentChildTags(
						getDataPlusStatusRootTag().getChild(AppConstants.contentElement), "fragment");
				for (IXMLTag lFragmentTag : lFragmentTags) {
					int lNumberOfAttempts = 0;
					try {
						lNumberOfAttempts = Integer.parseInt(
								lFragmentTag.getCurrentStatusAttribute(AppConstants.statusKeyNumberofattempts));
					} catch (NumberFormatException e) {
						lNumberOfAttempts = 0;
					}
					lNumberOfAttempts++;
					setRunTagStatus(getCaseComponent(), lFragmentTag, AppConstants.statusKeyNumberofattempts,
							"" + lNumberOfAttempts, true);
				}
			}
		}
		return lFeedbackCondition;
	}

	@Override
	public String getFeedbackTitle() {
		return CDesktopComponents.vView().getLabel("run_videosceneselector.alert.feedback.title");
	}

	@Override
	public String getDefaultFeedbackText() {
		return CDesktopComponents.vView().getLabel("run_videosceneselector.alert.feedback.defaulttext");
	}

	@Override
	public void handleStatusChange(STriggeredReference aTriggeredReference) {
		Events.postEvent("onHandleStatusChange", this, aTriggeredReference);
	}

	@Override
	public void onAction(String sender, String action, Object status) {
		if (action.equals("onExternalEvent")) {
			String[] lStatus = (String[]) status;
			String lSender = lStatus[0];
			String lReceiver = lStatus[1];
			String lEvent = lStatus[2];
			String lEventData = lStatus[3];
			String[] lEventDataValues = lEventData.split(",");
			if (sender.matches("jwplayer")) {
				if (lEvent.equals("onStart")) {
					if (currentFragmentTag != null) {
						setRunTagStatus(getCaseComponent(), currentFragmentTag, AppConstants.statusKeyStarted,
								AppConstants.statusValueTrue, true);
					}
				} else if (lEvent.equals("onDuration")) {
					String lDurationStr = lEventDataValues[1];
					setVideoDuration(lDurationStr);
				} else if (lEvent.equals("onTime")) {
					String lDurationStr = lEventDataValues[1];
					String lOffsetStr = lEventDataValues[2];
					String lPositionStr = lEventDataValues[3];
					setVideoPosition(lPositionStr);
				} else if (lEvent.equals("onSeek")) {
					String lPositionStr = lEventDataValues[1];
					String lOffsetStr = lEventDataValues[2];
					setVideoPosition(lOffsetStr);
				} else if (lEvent.equals("onComplete")) {
					if (currentFragmentTag != null) {
						setRunTagStatus(getCaseComponent(), currentFragmentTag, AppConstants.statusKeyFinished,
								AppConstants.statusValueTrue, true);
					}
				} else if (lEvent.equals("onJavascriptNotify")) {
					String lZulEvent = lEventDataValues[1];
					String lZulEventData = lEventDataValues[2];
					Events.postEvent(lZulEvent, this, lZulEventData);
				} else if (lEvent.equals("onError")) {
					CDesktopComponents.sSpring().getSLogHelper().logAction(
							"error in " + sender + ": file " + lEventDataValues[1] + ": " + lEventDataValues[2]);
				}
			}
		} else {
			super.onAction(sender, action, status);
		}
	}

	protected long getVideoParameter(String aString) {
		try {
			return Math.round(Double.parseDouble(aString) * 1000);
		} catch (NumberFormatException e) {
		}
		return 0;
	}

	protected void setVideoDuration(String aDurationStr) {
		Events.postEvent("onSetDuration", this, getVideoParameter(aDurationStr));
	}

	protected void setVideoPosition(String aPositionStr) {
		Events.postEvent("onSetPosition", this, getVideoParameter(aPositionStr));
	}

	/**
	 * Methods originally used in ZUL file.
	 */

	public void onInitZulfile(Event aEvent) {
		String componentStyle = "";
		String url = getBackgroundUrl();
		if (!url.equals("")) {
			componentStyle = "background-image:url('" + url + "');";
		}

		IXMLTag triggeredFeedbackConditionTag = getTriggeredFeedbackConditionTag();
		if (triggeredFeedbackConditionTag != null) {
			// NOTE only show feedback if it is an instruction, which is indicated by a
			// score of -1
			if (sSpringHelper.getTagChildValue(triggeredFeedbackConditionTag, "score", "-1").equals("-1")) {
				Events.postEvent("onShowFeedback", this, null);
			}
		}

		// NOTE showFeedback and showScore are for all forms!
		showFeedback = false;
		showScore = false;

		dataElements = new ArrayList<Hashtable<String, Object>>();

		int textColorCounter = 0;
		List<IXMLTag> fragmentTags = getFragmentTags();
		for (IXMLTag fragmentTag : fragmentTags) {
			currentFragmentTag = fragmentTag;

			Hashtable<String, Object> hFragmentDataElements = new Hashtable<String, Object>();
			hFragmentDataElements.put("tag", fragmentTag);
			hFragmentDataElements.put("title", sSpringHelper.getTagChildValue(fragmentTag, "title", ""));
			hFragmentDataElements.put("hovertext", sSpringHelper.getTagChildValue(fragmentTag, "hovertext", ""));

			String lFrUrl = vView.getUrlEncodedFileName(sSpringHelper.getAbsoluteUrl(fragmentTag));
			List<List<String>> lStreamingUrls = vView.getHtml5StreamingUrls(lFrUrl);
			if (!lFrUrl.equals(""))
				lFrUrl = vView.uniqueView(lFrUrl);
			hFragmentDataElements.put("url", lFrUrl);

			int lUrlGrNr = lStreamingUrls.size();

			hFragmentDataElements.put("nrOfStreamingUrlGroups", lUrlGrNr);
			List<List<String>> lUrlGroups = new ArrayList<List<String>>();
			for (int i = 0; i < lUrlGrNr; i++) {
				List<String> lStrUrlsGr = new ArrayList<String>();
				List<String> lTemp = lStreamingUrls.get(i);
				int lUrlsNr = lTemp.size();
				for (int j = 0; j < lUrlsNr; j++) {
					lStrUrlsGr.add(lTemp.get(j));
				}
				lUrlGroups.add(lStrUrlsGr);
			}
			hFragmentDataElements.put("streamingUrls", lUrlGroups);

			hFragmentDataElements.put("videowidth", getVideoWidth(fragmentTag));
			hFragmentDataElements.put("videoheight", getVideoHeight(fragmentTag));

			// NOTE following list is needed so you can do nested foreach looping! see
			// below.
			List<Hashtable<String, Object>> pieceOrRefpieceDataElementsList = new ArrayList<Hashtable<String, Object>>();
			List<IXMLTag> pieceOrRefpieceTags = getPieceAndRefpieceTags(fragmentTag);
			handlepieceOrRefpieces(pieceOrRefpieceDataElementsList, pieceOrRefpieceTags);
			hFragmentDataElements.put("pieces", pieceOrRefpieceDataElementsList);

			maxNumberOfScenesToSelect = Integer
					.parseInt(sSpringHelper.getTagChildValue(fragmentTag, "maxnumberofscenes", "0"));
			boolean countMaxNumberOfScenesToSelect = true;
			if (maxNumberOfScenesToSelect > 0) {
				countMaxNumberOfScenesToSelect = false;
			}
			minNumberOfCorrectScenesToSelect = Integer
					.parseInt(sSpringHelper.getTagChildValue(fragmentTag, "minnumberofcorrectscenes", "0"));
			showControls = sSpringHelper
					.getCurrentStatusTagStatusChildAttribute(fragmentTag, AppConstants.statusKeyShowcontrols, "true")
					.equals("true");

			if (sSpringHelper.getTagStatusChildAttribute(fragmentTag, "showfeedback", "true").equals("true")) {
				showFeedback = true;
			}
			if (sSpringHelper.getTagStatusChildAttribute(fragmentTag, "showscore", "false").equals("true")) {
				showScore = true;
			}
			boolean isShowIfRightOrWrong = isShowIfRightOrWrong(fragmentTag);

			// NOTE following list is needed so you can do nested foreach looping! see
			// below.
			List<Hashtable<String, Object>> categoryDataElementsList = new ArrayList<Hashtable<String, Object>>();
			List<IXMLTag> categoryTags = getCategoryTags(fragmentTag);
			for (IXMLTag categoryTag : categoryTags) {
				Hashtable<String, Object> hCategoryDataElements = new Hashtable<String, Object>();
				hCategoryDataElements.put("tag", categoryTag);
				hCategoryDataElements.put("tagid", categoryTag.getAttribute(AppConstants.keyId));
				hCategoryDataElements.put("title", sSpringHelper.getTagChildValue(categoryTag, "title", ""));
				hCategoryDataElements.put("hovertext", sSpringHelper.getTagChildValue(categoryTag, "hovertext", ""));
				List<Hashtable<String, Object>> sceneList = getSceneList(categoryTag, isShowIfRightOrWrong);
				hCategoryDataElements.put("textcolor", textColors[textColorCounter]);
				textColorCounter++;
				textColorCounter = textColorCounter % textColors.length;
				hCategoryDataElements.put("scenes", sceneList);

				if (countMaxNumberOfScenesToSelect) {
					maxNumberOfScenesToSelect += getMaxNumberOfScenesToSelect(sceneList);
				}

				categoryDataElementsList.add(hCategoryDataElements);
			}
			hFragmentDataElements.put("categories", categoryDataElementsList);

			// NOTE following list is needed so you can do nested foreach looping! see
			// below.
			pieceOrRefpieceDataElementsList = new ArrayList<Hashtable<String, Object>>();
			IXMLTag feedbackConditionTag = getCurrentFeedbackConditionTag(fragmentTag);
			pieceOrRefpieceTags = getPieceAndRefpieceTags(feedbackConditionTag);
			handlepieceOrRefpieces(pieceOrRefpieceDataElementsList, pieceOrRefpieceTags);
			hFragmentDataElements.put("feedbackpieces", pieceOrRefpieceDataElementsList);

			dataElements.add(hFragmentDataElements);
		}
		setCurrentFragmentTag(currentFragmentTag);

		Map<String, Object> propertyMap = new HashMap<>();
		propertyMap.put("currentEmergoComponent", this);
		propertyMap.put("dataElements", dataElements);
		propertyMap.put("componentStyle", componentStyle);
		propertyMap.put("feedbackButtonUrl", sSpringHelper.stripEmergoPath(getFeedbackButtonUrl()));
		propertyMap.put("emergoRootUrl", CDesktopComponents.vView().getAbsoluteWebappsRoot());
		propertyMap.put("jwplayerKey", PropsValues.VIDEO_JWPLAYER_KEY);
		propertyMap.put("videosceneselectorPlayerName", videosceneselectorPlayerName);
		propertyMap.put("videoWidth", videoWidth);
		propertyMap.put("runVideosceneselectorId", runVideosceneselectorId);

		((HtmlMacroComponent) getRunContentComponent()).setDynamicProperty("a_propertyMap", propertyMap);

		((HtmlMacroComponent) getRunContentComponent()).setMacroURI(VView.v_run_videosceneselector_fr);
		getRunContentComponent().setVisible(true);
	}

	public void onShowFeedback(Event aEvent) {
		if (showFeedback) {
			IXMLTag triggeredFeedbackConditionTag = showFeedback();
			// NOTE score -1 means 'no relevant score yet or anymore' because tool
			// instruction or tool mc questions are shown, so feedback mechanism is misused
			// to show communication with npc's.
			if (triggeredFeedbackConditionTag != null) {
				if (!sSpringHelper.getTagChildValue(triggeredFeedbackConditionTag, "score", "-1").equals("-1")) {
					List<Component> videosceneselectorScenes = vView.getComponentsByPrefix("videosceneselectorScene_");
					for (Component videosceneselectorScene : videosceneselectorScenes) {
						boolean correct = isCorrect((IXMLTag) videosceneselectorScene.getAttribute("tag"));
						if (!correct && (boolean) videosceneselectorScene.getAttribute("selected")) {
							// deselect if not correct and selected
							selectScene(null, (IXMLTag) videosceneselectorScene.getAttribute("tag"), false);
						}
						Events.postEvent("onFeedbackUpdate", videosceneselectorScene, null);
					}
					Events.postEvent("onSetNumberOfSelected", this, null);
					Events.postEvent("onUpdateCategories", this, "feedbackGiven");
				}
				for (Hashtable<String, Object> dataElement : dataElements) {
					// for each form
					Events.postEvent("onUpdate",
							vView.getComponent(
									"feedbackPieces_" + ((IXMLTag) dataElement.get("tag")).getAttribute("id")),
							triggeredFeedbackConditionTag);
				}
			}
		} else {
			showDefaultFeedback();
		}
	}

	public void onCategorySelected(Event aEvent) {
		boolean isMaximumNumberSelected = Integer
				.parseInt((String) getAttribute("numberofselecteditems")) >= maxNumberOfScenesToSelect;
		Component category = vView.getComponent((String) aEvent.getData());
		if (category != null) {
			IXMLTag categoryTag = (IXMLTag) category.getAttribute("tag");
			String color = (String) category.getAttribute("color");
			Component videosceneselectorScene = getVideosceneselectorSceneByPos((long) getAttribute("position"));
			if (isVideosceneselectorSceneChangeable(videosceneselectorScene, isMaximumNumberSelected)) {
				String previousColor = (String) videosceneselectorScene.getAttribute("color");
				if ((boolean) videosceneselectorScene.getAttribute("selected") && !previousColor.equals(color)) {
					// deselect if scene previously is selected and new selected category is unequal
					// to previous one (given by color)
					selectScene(null, (IXMLTag) videosceneselectorScene.getAttribute("tag"), false);
				}
				// NOTE use sendEvent so value of selected is updated before selectScene is
				// called
				Events.sendEvent("onSetSelectedAndColor", videosceneselectorScene, color);
				selectScene(categoryTag, (IXMLTag) videosceneselectorScene.getAttribute("tag"),
						(boolean) videosceneselectorScene.getAttribute("selected"));
				Events.postEvent("onUpdate", videosceneselectorScene, null);
				Events.postEvent("onSetNumberOfSelected", this, null);
				Events.postEvent("onUpdateCategories", this, "categoryChanged");
			}
		}
	}

	public void onSetNumberOfSelected(Event aEvent) {
		List<Component> videosceneselectorScenes = vView.getComponentsByPrefix("videosceneselectorScene_");
		int numberOfSelected = getNumberOfSelectedScenes(videosceneselectorScenes);
		setAttribute("numberofselecteditems", "" + numberOfSelected);
		int numberOfRightlySelected = getNumberOfRightlySelectedScenes(videosceneselectorScenes);
		Component labelNumberOfSelected = vView.getComponent("videosceneselectorNumberOfSelected");
		if (labelNumberOfSelected != null) {
			Events.postEvent("onUpdate", labelNumberOfSelected, "" + numberOfSelected);
			boolean highlightright = numberOfRightlySelected >= minNumberOfCorrectScenesToSelect
					&& numberOfRightlySelected == numberOfSelected;
			boolean highlightwrong = numberOfSelected >= minNumberOfCorrectScenesToSelect
					&& numberOfRightlySelected < numberOfSelected;
			if (highlightright) {
				Events.postEvent("onSetSclassExtension", labelNumberOfSelected, "HighlightRight");
			} else if (highlightwrong) {
				Events.postEvent("onSetSclassExtension", labelNumberOfSelected, "HighlightWrong");
			} else {
				Events.postEvent("onSetSclassExtension", labelNumberOfSelected, "");
			}
		}
		setNumberOfRightlySelected(numberOfRightlySelected);
	}

	public void onSetDuration(Event aEvent) {
		// NOTE onSetDuration is sent from Java class CRunVideosceneselector when it
		// receives an onDuration message from the jw player, see below.
		// init scenes
		long duration = (long) aEvent.getData();
		setAttribute("duration", duration);
		List<Component> videosceneselectorCategories = vView.getComponentsByPrefix("videosceneselectorCategory_");
		List<Component> videosceneselectorScenes = vView.getComponentsByPrefix("videosceneselectorScene_");
		for (Component videosceneselectorScene : videosceneselectorScenes) {
			Object[] eventData = new Object[3];
			eventData[0] = duration;
			eventData[1] = videoWidth;
			// determine color
			String color = "";
			if ((boolean) videosceneselectorScene.getAttribute("selected")) {
				String dataparentid = sSpringHelper.getCurrentStatusTagStatusChildAttribute(
						(IXMLTag) videosceneselectorScene.getAttribute("tag"), AppConstants.statusKeyDataparentid, "");
				if (!dataparentid.equals("")) {
					for (Component videosceneselectorCategory : videosceneselectorCategories) {
						if (((IXMLTag) videosceneselectorCategory.getAttribute("tag")).getAttribute(AppConstants.keyId)
								.equals(dataparentid)) {
							color = (String) videosceneselectorCategory.getAttribute("color");
							break;
						}
					}
				}
			}
			eventData[2] = color;
			Events.postEvent("onInit", videosceneselectorScene, eventData);
			Events.postEvent("onUpdate", videosceneselectorScene, null);
		}
		Events.postEvent("onShowControls", this, null);
	}

	public void onShowControls(Event aEvent) {
		vView.getComponent("categoriesDiv").setVisible(showControls);
		vView.getComponent("scenesDiv").setVisible(showControls);
		vView.getComponent("videosceneselectorNumberOfSelectedDiv").setVisible(showControls);
	}

	public void onSetPosition(Event aEvent) {
		setAttribute("position", aEvent.getData());
		Events.postEvent("onUpdateCategories", this, "positionChanged");
	}

	public void onUpdateCategories(Event aEvent) {
		// NOTE update categories if current scene is unequal to previous scene
		// ("positionChanged") or if user (un)checks category ("categoryChanged") or if
		// feedback is given ("feedbackGiven").
		if (getAttribute("position") != null && aEvent.getData() != null) {
			Component currentVideosceneselectorScene = getVideosceneselectorSceneByPos((long) getAttribute("position"));
			Component previousVideosceneselectorScene = (Component) getAttribute("previousVideosceneselectorScene");
			if (currentVideosceneselectorScene != null) {
				String updateStatus = (String) aEvent.getData();
				boolean sceneChanged = updateStatus.equals("positionChanged")
						&& (previousVideosceneselectorScene == null
								|| currentVideosceneselectorScene != previousVideosceneselectorScene);
				boolean categoryChanged = updateStatus.equals("categoryChanged");
				boolean feedbackGiven = updateStatus.equals("feedbackGiven");
				if (sceneChanged || categoryChanged || feedbackGiven) {
					// get current category.
					Component currentVideosceneselectorCategory = getVideosceneselectorCategoryByColor(
							currentVideosceneselectorScene);
					List<Component> videosceneselectorCategories = vView
							.getComponentsByPrefix("videosceneselectorCategory_");
					// check/uncheck categories
					for (Component videosceneselectorCategory : videosceneselectorCategories) {
						// if category is equal to current category, check category
						boolean categoryChecked = videosceneselectorCategory == currentVideosceneselectorCategory;
						Events.postEvent("onCheck", videosceneselectorCategory, categoryChecked);
					}
					// enable/disable categories
					boolean isMaximumNumberOfScenesSelected = Integer
							.parseInt((String) getAttribute("numberofselecteditems")) >= maxNumberOfScenesToSelect;
					boolean isCurrentVideosceneDisabled = !isVideosceneselectorSceneChangeable(
							currentVideosceneselectorScene, isMaximumNumberOfScenesSelected);
					for (Component videosceneselectorCategory : videosceneselectorCategories) {
						boolean categoryDisabled = isCurrentVideosceneDisabled
								|| ((videosceneselectorCategory != currentVideosceneselectorCategory)
										&& (isMaximumNumberOfScenesSelected
												|| isMaxNumberOfScenesPerCategory(videosceneselectorCategory)));
						Events.postEvent("onDisable", videosceneselectorCategory, categoryDisabled);
					}
				}
			}
			setAttribute("previousVideosceneselectorScene", currentVideosceneselectorScene);
		}
	}

	public void onPlayScene(Event aEvent) {
		playScene((String[]) aEvent.getData());
	}

	public void onHandleStatusChange(Event aEvent) {
		// NOTE to handle pieces and refpieces call super
		super.onHandleStatusChange(aEvent, "videosceneselectorSceneDiv_");
		STriggeredReference triggeredReference = (STriggeredReference) aEvent.getData();
		if (triggeredReference != null) {
			if (triggeredReference.getDataTag() != null && triggeredReference.getDataTag().getName().equals("fragment")
					&& triggeredReference.getStatusKey().equals(AppConstants.statusKeyShowcontrols)) {
				showControls = triggeredReference.getStatusValue().equals(AppConstants.statusValueTrue);
				Events.postEvent("onShowControls", this, null);
				Events.postEvent("onShowFeedback", this, null);
			}
		}
	}

	public List<Hashtable<String, Object>> getSceneList(IXMLTag parentTag, boolean isShowIfRightOrWrong) {
		// NOTE following list is needed so you can do nested foreach looping! see
		// below.
		List<Hashtable<String, Object>> sceneDataElementsList = new ArrayList<Hashtable<String, Object>>();
		// NOTE get all scene tags, also non present ones
		List<IXMLTag> sceneTags = getAllSceneTags(parentTag);
		for (IXMLTag sceneTag : sceneTags) {
			Hashtable<String, Object> hSceneDataElements = new Hashtable<String, Object>();
			hSceneDataElements.put("tag", sceneTag);
			hSceneDataElements.put("hovertext", sSpringHelper.getTagChildValue(sceneTag, "hovertext", ""));
			// TODO range must be translated to a position and size, so style
			hSceneDataElements.put("range", sSpringHelper.getTagChildValue(sceneTag, "range", ""));
			hSceneDataElements.put("selected",
					sSpringHelper
							.getCurrentStatusTagStatusChildAttribute(sceneTag, AppConstants.statusKeySelected, "true")
							.equals("true"));
			if (isShowIfRightOrWrong) {
				hSceneDataElements.put("isrightlyselected", "" + isRightlySelected(sceneTag));
			} else {
				hSceneDataElements.put("isrightlyselected", "");
			}
			hSceneDataElements.put("present", sSpringHelper.getCurrentStatusTagStatusChildAttribute(sceneTag,
					AppConstants.statusKeyPresent, "true"));
			sceneDataElementsList.add(hSceneDataElements);
		}
		return sceneDataElementsList;
	}

	public int getMaxNumberOfScenesToSelect(List<Hashtable<String, Object>> sceneList) {
		int maxNumberOfScenesToSelect = 0;
		for (Hashtable<String, Object> scene : sceneList) {
			IXMLTag sceneTag = (IXMLTag) scene.get("tag");
			if (sSpringHelper.getCurrentStatusTagStatusChildAttribute(sceneTag, AppConstants.statusKeyCorrect, "true")
					.equals("true")) {
				maxNumberOfScenesToSelect++;
			}
		}
		return maxNumberOfScenesToSelect;
	}

	public String getVideoWidth(IXMLTag tag) {
		String videoWidthStr = defaultVideoWidthStr;
		String[] size = sSpringHelper.getTagChildValue(tag, "size", "").split(",");
		if (size.length == 2) {
			videoWidth = runWnd.getNumber(size[0], 0);
			String width = runWnd.getNumberStr(size[0], "");
			if (!width.equals("")) {
				videoWidthStr = "" + width + "px";
			}
		}
		return videoWidthStr;
	}

	public String getVideoHeight(IXMLTag tag) {
		String videoHeightStr = defaultVideoHeightStr;
		String[] size = sSpringHelper.getTagChildValue(tag, "size", "").split(",");
		if (size.length == 2) {
			videoHeight = runWnd.getNumber(size[1], 0);
			String height = runWnd.getNumberStr(size[1], "");
			if (!height.equals("")) {
				videoHeightStr = "" + height + "px";
			}
		}
		return videoHeightStr;
	}

	public void playScene(String[] range) {
		double position = 0 + Integer.parseInt(range[0]);
		position = position / 1000;
		Clients.evalJavaScript("seek(" + position + ");");
	}

	@Override
	public boolean isShowIfRightOrWrong(IXMLTag tag) {
		// get fragment tag
		while (tag != null && !tag.getName().equals("fragment")) {
			tag = tag.getParentTag();
		}
		if (tag == null) {
			return false;
		}
		int maxNumber = runWnd.getNumber(sSpringHelper.getTagChildValue(tag, "maxnumberofattempts", "-1"), -1);
		int number = runWnd.getNumber(
				sSpringHelper.getCurrentStatusTagStatusChildAttribute(tag, AppConstants.statusKeyNumberofattempts, "0"),
				0);
		return (maxNumber > -1) && (number >= maxNumber);
	}

	public boolean isCorrect(IXMLTag sceneTag) {
		boolean correct = sSpringHelper
				.getCurrentStatusTagStatusChildAttribute(sceneTag, AppConstants.statusKeyCorrect, "true")
				.equals("true");
		boolean correctCategory = sSpringHelper
				.getCurrentStatusTagStatusChildAttribute(sceneTag, AppConstants.statusKeyDataparentid, "")
				.equals(sceneTag.getParentTag().getAttribute(AppConstants.keyId));
		return correct && correctCategory;
	}

	public boolean isRightlySelected(IXMLTag sceneTag) {
		boolean correct = isCorrect(sceneTag);
		boolean selected = sSpringHelper
				.getCurrentStatusTagStatusChildAttribute(sceneTag, AppConstants.statusKeySelected, "true")
				.equals("true");
		return correct && selected;
	}

	public String getCategoryTagId(IXMLTag tag) {
		while (tag != null && !tag.getName().equals("category")) {
			tag = tag.getParentTag();
		}
		if (tag != null) {
			return tag.getAttribute("id");
		}
		return "";
	}

	public Component getVideosceneselectorSceneByPos(long position) {
		List<Component> videosceneselectorScenes = vView.getComponentsByPrefix("videosceneselectorScene_");
		for (Component videosceneselectorScene : videosceneselectorScenes) {
			String[] range = ((String) videosceneselectorScene.getAttribute("range")).split(",");
			long leftPos = Long.parseLong(range[0]);
			long rightPos = Long.parseLong(range[1]);
			if (position >= leftPos && position < rightPos) {
				return videosceneselectorScene;
			}
		}
		return null;
	}

	public boolean isVideosceneselectorSceneChangeable(Component videosceneselectorScene,
			boolean isMaximumNumberSelected) {
		if (videosceneselectorScene == null) {
			return false;
		}
		boolean isShowIfRightOrWrong = isShowIfRightOrWrong((IXMLTag) videosceneselectorScene.getAttribute("tag"));
		boolean correct = isCorrect((IXMLTag) videosceneselectorScene.getAttribute("tag"));
		boolean selected = (boolean) videosceneselectorScene.getAttribute("selected");
		return (!(correct && selected) || !isShowIfRightOrWrong) && (!isMaximumNumberSelected || selected);
	}

	public Component getVideosceneselectorCategoryByColor(Component videosceneselectorScene) {
		if (videosceneselectorScene == null) {
			return null;
		}
		String color = (String) videosceneselectorScene.getAttribute("color");
		if (color == null || color.equals("")) {
			return null;
		}
		List<Component> videosceneselectorCategories = vView.getComponentsByPrefix("videosceneselectorCategory_");
		for (Component videosceneselectorCategory : videosceneselectorCategories) {
			if (videosceneselectorCategory.getAttribute("color").equals(color)) {
				return videosceneselectorCategory;
			}
		}
		return null;
	}

	public int getNumberOfSelectedScenes(List<Component> videosceneselectorScenes) {
		int numberOfSelected = 0;
		for (Component videosceneselectorScene : videosceneselectorScenes) {
			if ((boolean) videosceneselectorScene.getAttribute("selected")) {
				numberOfSelected++;
			}
		}
		return numberOfSelected;
	}

	public int getNumberOfRightlySelectedScenes(List<Component> videosceneselectorScenes) {
		int numberOfRightlySelected = 0;
		for (Component videosceneselectorScene : videosceneselectorScenes) {
			if ((boolean) videosceneselectorScene.getAttribute("selected")) {
				if (isRightlySelected((IXMLTag) videosceneselectorScene.getAttribute("tag"))) {
					numberOfRightlySelected++;
				}
			}
		}
		return numberOfRightlySelected;
	}

	public Component getFirstVideosceneselectorSceneByStartPos(List<Component> videosceneselectorScenes) {
		Component firstVideosceneselectorScene = null;
		long previousStartPos = -1;
		for (Component videosceneselectorScene : videosceneselectorScenes) {
			String[] range = ((String) videosceneselectorScene.getAttribute("range")).split(",");
			long startPos = Long.parseLong(range[0]);
			if (previousStartPos == -1 || startPos < previousStartPos) {
				firstVideosceneselectorScene = videosceneselectorScene;
				previousStartPos = startPos;
			}
		}
		return firstVideosceneselectorScene;
	}

	public boolean isMaxNumberOfScenesPerCategory(Component videosceneselectorCategory) {
		IXMLTag categoryTag = (IXMLTag) videosceneselectorCategory.getAttribute("tag");
		int catMaxNumberOfScenesToSelect = Integer
				.parseInt(sSpringHelper.getTagChildValue(categoryTag, "maxnumberofscenes", "-1"));
		if (catMaxNumberOfScenesToSelect == -1) {
			return false;
		}
		String color = (String) videosceneselectorCategory.getAttribute("color");
		List<Component> videosceneselectorScenes = vView.getComponentsByPrefix("videosceneselectorScene_");
		int catNumberOfSelected = 0;
		for (Component videosceneselectorScene : videosceneselectorScenes) {
			if (color.equals(videosceneselectorScene.getAttribute("color"))) {
				catNumberOfSelected++;
			}
		}
		return (catNumberOfSelected >= catMaxNumberOfScenesToSelect);
	}

}
