/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.dashboard;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Progressmeter;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitInitBox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitPeerGroupMemberProgress;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;

public class CRunPVToolkitDashboardPeerGroupsDiv extends CDefDiv {

	private static final long serialVersionUID = -5619225763953266603L;

	public String zulfilepath = ((CRunPVToolkitInitBox)CDesktopComponents.vView().getComponent("PV-toolkit_initBox")).getZulfilepath();
	
	protected CRunPVToolkit pvToolkit;
	
	protected IERunGroup _actor;
	protected boolean _editable;
	protected List<IXMLTag> _commonPeerGroupTags;
		
	protected boolean excludeTeachersFeedback = false;
	protected boolean excludeStudentAssistantsFeedback = false;
	protected boolean showGroups = false;
	protected boolean showMeAndStudents = false;
	
	protected List<IERunGroupAccount> runGroupAccountsToInclude;
	protected List<String> peerRolesToInclude;
	
	protected String _idPrefix = "dashboard";
	protected String _classPrefix = "dashboardPeerGroups";
	
	public void init(IERunGroup actor, boolean editable, List<IXMLTag> commonPeerGroupTags) {
		_actor = actor;
		_editable = editable;
		_commonPeerGroupTags = commonPeerGroupTags;
		
		//NOTE always initialize, because recordings and feedback my be added during a session
		setClass(_classPrefix);

		pvToolkit = (CRunPVToolkit)CDesktopComponents.vView().getComponent("pvToolkit");
		
		runGroupAccountsToInclude = new ArrayList<IERunGroupAccount>();
		peerRolesToInclude = new ArrayList<String>();

		showGroups = showGroups();
		showMeAndStudents = showMeAndStudents();
		
		update();
	}
	
	protected boolean excludeTeachersFeedback() {
		if (!pvToolkit.hasStudentRole(_actor))
			return false;
		//TODO how to handle teacher progress when teacher only needs to give feedback if student asks for teacher feedback
		// teacher is only excluded when student belongs to experimental 'delayed teacher feedback' group and has not finished last assessment:
		return pvToolkit.excludeTeachersFeedback(_actor, null);
	}

	protected boolean excludeStudentAssistantsFeedback() {
		return pvToolkit.excludeStudentAssistantsFeedback(_actor);
	}

	protected boolean showGroups() {
		return false;
	}

	protected boolean showMeAndStudents() {
		return false;
	}

	public void update() {
		getChildren().clear();
		
		pvToolkit.setMemoryCaching(true);
		
		new CRunPVToolkitDefLabel(this, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font " + _classPrefix + "Title", getTitleLabelKey()}
		);
		
		new CRunPVToolkitDefImage(this, 
				new String[]{"class", "src"}, 
				new Object[]{_classPrefix + "Background", zulfilepath + "background_wit_smaller.svg"}
		);

		Div div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "Div"}
		);
		
		excludeTeachersFeedback = excludeTeachersFeedback();
		excludeStudentAssistantsFeedback = excludeStudentAssistantsFeedback();

		Rows rows = appendPeersGrid(div);
		int rowNumber = 1;
		rowNumber = appendPeersToGrid(rows, rowNumber);
		
		pvToolkit.setMemoryCaching(false);
		
	}
	
	protected String getTitleLabelKey() {
		String labelKey = "";
		if (_commonPeerGroupTags.size() <= 1) {
			labelKey = "PV-toolkit-dashboard.header.peergroup";
		}
		else {
			labelKey = "PV-toolkit-dashboard.header.peergroups";
		}
		return labelKey;
	}

    protected Rows appendPeersGrid(Component parent) {
    	return pvToolkit.appendGrid(
    			parent, 
    			getId() + "Grid",
    			"position:absolute;left:0px;top:0px;width:460px;height:180px;overflow:auto;", 
				new boolean[] {true, true, pvToolkit.hasCycleTags(), true}, 
    			new String[] {"10%", "40%", "10%", "40%"}, 
    			new boolean[] {false, true, true, true},
    			null,
    			"PV-toolkit-dashboard.column.label.", 
    			new String[] {"", "name", "cycle", "progress"});

    }

	protected int appendPeersToGrid(Rows rows, int rowNumber) {
		return appendPeersToGrid(
				rows, 
				rowNumber,
				new boolean[] {true, true, pvToolkit.hasCycleTags(), true} 
				);
	}

	protected void determineRugsAndPeerRolesToInclude(IERunGroupAccount me) {
		for (IXMLTag peerGroupTag : _commonPeerGroupTags) {
			List<IXMLTag> memberTags = pvToolkit.getStatusChildTag(peerGroupTag).getChilds("member");
			for (IXMLTag memberTag : memberTags) {
				IERunGroupAccount runGroupAccount = pvToolkit.getRunGroupAccount(pvToolkit.getStatusChildTagAttribute(memberTag, "rgaid"));
				String peerRole = pvToolkit.getStatusChildTagAttribute(memberTag, "role");
				boolean includeMe = showMeAndStudents && runGroupAccount != null && runGroupAccount.getRgaId() == me.getRgaId();
				boolean includeRunGroupAccount = runGroupAccount != null &&
						!peerRole.equals(CRunPVToolkit.peerGroupPassiveTeacherRole) && 
						(!excludeTeachersFeedback || !peerRole.equals(CRunPVToolkit.peerGroupTeacherRole)) &&
						(!excludeStudentAssistantsFeedback || !peerRole.equals(CRunPVToolkit.peerGroupStudentAssistantRole));

				if (includeMe || includeRunGroupAccount) {
					runGroupAccountsToInclude.add(runGroupAccount);
					peerRolesToInclude.add(peerRole);
				}
			}
		}
	}

	protected boolean includeRunGroupAccount(IERunGroupAccount runGroupAccount) {
		for (IERunGroupAccount tempRunGroupAccount : runGroupAccountsToInclude) {
			if (tempRunGroupAccount.getRgaId() == runGroupAccount.getRgaId()) {
				return true;
			}
		}
		return false;
	}

	protected CRunPVToolkitPeerGroupMemberProgress getPeerGroupMemberProgress(List<CRunPVToolkitPeerGroupMemberProgress> runPVToolkitPeerGroupMembersProgress, IERunGroupAccount runGroupAccount) {
		for (CRunPVToolkitPeerGroupMemberProgress peerGroupMemberProgress : runPVToolkitPeerGroupMembersProgress) {
			if (peerGroupMemberProgress.getRunGroupAccount().getRgaId() == runGroupAccount.getRgaId()) {
				return peerGroupMemberProgress;
			}
		}
		return null;
	}

	protected int appendPeersToGrid(
			Rows rows, 
			int rowNumber,
    		boolean[] showColumn 
			) {
		IERunGroupAccount me = pvToolkit.getCurrentRunGroupAccount();
		determineRugsAndPeerRolesToInclude(me);
		List<CRunPVToolkitPeerGroupMemberProgress> runPVToolkitPeerGroupMembersProgress = pvToolkit.getPeerGroupMembersProgress(runGroupAccountsToInclude, peerRolesToInclude);
		int meReachedCycleNumber = 0;
		List<Double> meProgressPerCycle = new ArrayList<Double>();
		for (int i=0;i<pvToolkit.getMaxCycleNumber();i++) {
			meProgressPerCycle.add(0.0);
		}
		int meCounter = 0;
		int studentsReachedCycleNumber = 0;
		List<Double> studentsProgressPerCycle = new ArrayList<Double>();
		for (int i=0;i<pvToolkit.getMaxCycleNumber();i++) {
			studentsProgressPerCycle.add(0.0);
		}
		int studentsCounter = 0;
		for (IXMLTag peerGroupTag : _commonPeerGroupTags) {
			List<IXMLTag> memberTags = pvToolkit.getStatusChildTag(peerGroupTag).getChilds("member");
			int groupReachedCycleNumber = 0;
			List<Double> groupProgressPerCycle = new ArrayList<Double>();
			for (int i=0;i<pvToolkit.getMaxCycleNumber();i++) {
				groupProgressPerCycle.add(0.0);
			}
			int groupCounter = 0;
			for (IXMLTag memberTag : memberTags) {
				IERunGroupAccount runGroupAccount = pvToolkit.getRunGroupAccount(pvToolkit.getStatusChildTagAttribute(memberTag, "rgaid"));
				boolean includeMe = showMeAndStudents && runGroupAccount != null && runGroupAccount.getRgaId() == me.getRgaId();
				boolean includeRunGroupAccount = runGroupAccount != null && includeRunGroupAccount(runGroupAccount);
				if (includeMe || includeRunGroupAccount) {
					CRunPVToolkitPeerGroupMemberProgress peerGroupMemberProgress = getPeerGroupMemberProgress(runPVToolkitPeerGroupMembersProgress, runGroupAccount);
					if (!showGroups && !showMeAndStudents) {
						boolean excludeFeedbackGiver = pvToolkit.excludeFeedbackGiver(runGroupAccount.getERunGroup(), me.getERunGroup());
						if (!excludeFeedbackGiver) {
							rowNumber = appendRowToGrid(
									rows, 
									rowNumber,
									showColumn,
									runGroupAccount,
									"",
									peerGroupMemberProgress
									);
						}
					}
					if (showGroups) {
						groupReachedCycleNumber += peerGroupMemberProgress.getReachedCycleNumber();
						for (int i=0;i<pvToolkit.getMaxCycleNumber();i++) {
							Double groupProgress = groupProgressPerCycle.get(i) + peerGroupMemberProgress.getProgressPerCycle().get(i);
							groupProgressPerCycle.set(i, groupProgress);
						}
						groupCounter++;
					}
					if (includeMe) {
						meReachedCycleNumber += peerGroupMemberProgress.getReachedCycleNumber();
						for (int i=0;i<pvToolkit.getMaxCycleNumber();i++) {
							Double meProgress = meProgressPerCycle.get(i) + peerGroupMemberProgress.getProgressPerCycle().get(i);
							meProgressPerCycle.set(i, meProgress);
						}
						meCounter++;
					}
					if (!includeMe && showMeAndStudents) {
						studentsReachedCycleNumber += peerGroupMemberProgress.getReachedCycleNumber();
						for (int i=0;i<pvToolkit.getMaxCycleNumber();i++) {
							Double studentsProgress = studentsProgressPerCycle.get(i) + peerGroupMemberProgress.getProgressPerCycle().get(i);
							studentsProgressPerCycle.set(i, studentsProgress);
						}
						studentsCounter++;
					}
				}
			}
			if (showGroups) {
				rowNumber = appendRowToGrid(
						rows, 
						rowNumber,
						showColumn,
						null,
						peerGroupTag.getChildAttribute(AppConstants.statusElement, "name"),
						getPeerGroupMemberProgress(groupCounter, groupReachedCycleNumber, groupProgressPerCycle)
						);
			}
		}
		if (showMeAndStudents) {
			if (meCounter > 0  ) {
				rowNumber = appendRowToGrid(
						rows, 
						rowNumber,
						showColumn,
						null,
						pvToolkit.getCurrentRunGroup().getName(),
						getPeerGroupMemberProgress(meCounter, meReachedCycleNumber, meProgressPerCycle)
						);
			}
			rowNumber = appendRowToGrid(
					rows, 
					rowNumber,
					showColumn,
					null,
					CDesktopComponents.vView().getLabel("PV-toolkit-dashboard.header.students"),
					getPeerGroupMemberProgress(studentsCounter, studentsReachedCycleNumber, studentsProgressPerCycle)
					);
		}

		return rowNumber;
	}

	protected CRunPVToolkitPeerGroupMemberProgress getPeerGroupMemberProgress(
			int counter,
			int reachedCycleNumber,
			List<Double> progressPerCycle
			) {
		if (counter == 0) {
			reachedCycleNumber = 0;
			progressPerCycle = new ArrayList<Double>();
			for (int i=0;i<pvToolkit.getMaxCycleNumber();i++) {
				progressPerCycle.add(new Double(0.0));
			}
		}
		else {
			double temp = reachedCycleNumber;
			temp = temp / counter;
			reachedCycleNumber = (int)Math.round(temp);
			for (int i=0;i<pvToolkit.getMaxCycleNumber();i++) {
				double progress = progressPerCycle.get(i);
				progressPerCycle.set(i, progress / counter);
			}
		}
		return new CRunPVToolkitPeerGroupMemberProgress(null, "", reachedCycleNumber, progressPerCycle);
	}

	protected int appendRowToGrid(
			Rows rows, 
			int rowNumber,
    		boolean[] showColumn,
    		IERunGroupAccount runGroupAccount,
    		String name,
    		CRunPVToolkitPeerGroupMemberProgress runPVToolkitPeerGroupMemberProgress
			) {
		Row row = new Row();
		rows.appendChild(row);
			
		int columnNumber = 0;
		if (showColumn.length > columnNumber && showColumn[columnNumber]) {
			String src = "";
			if (!showGroups) {
				src = zulfilepath + "user.svg";
			}
			else {
				src = zulfilepath + "user-friends.svg";
			}
			new CRunPVToolkitDefImage(row, 
					new String[]{"class", "src"}, 
					new Object[]{"gridImage", src}
					);
		}

		columnNumber++;
		if (showColumn.length > columnNumber && showColumn[columnNumber]) {
			if (!showGroups) {
				if (runGroupAccount != null) {
					name = runGroupAccount.getERunGroup().getName();
				}
			}
			Label label = new CRunPVToolkitDefLabel(row, 
					new String[]{"class", "value"}, 
					new Object[]{"font gridLabel", name}
					);
			if (!showGroups && runGroupAccount != null) {
				IEAccount account = runGroupAccount.getEAccount();
				label.setAttribute("orderValue", "" + account.getLastname() + "_" + account.getNameprefix() + "_" + account.getInitials());
			}
		}

		columnNumber++;
		if (showColumn.length > columnNumber && showColumn[columnNumber]) {
			int cycleNumber = runPVToolkitPeerGroupMemberProgress.getReachedCycleNumber();
			Label label = new CRunPVToolkitDefLabel(row, 
					new String[]{"class", "value"}, 
					new Object[]{"font gridLabel", "" + cycleNumber}
					);

			String orderValue = "" + cycleNumber;
			while (orderValue.length() < 4) {
				orderValue = "0" + orderValue;
			}
			label.setAttribute("orderValue", orderValue);
		}

		columnNumber++;
		if (showColumn.length > columnNumber && showColumn[columnNumber]) {
			//NOTE show progress in last reached cycle
			int reachedCycleNumber = runPVToolkitPeerGroupMemberProgress.getReachedCycleNumber();
			int progress = 0;
			if (reachedCycleNumber > 0) {
				progress = (new Double(100 * runPVToolkitPeerGroupMemberProgress.getProgressPerCycle().get(reachedCycleNumber - 1))).intValue();
			}
			//NOTE to be sure restrict progress to [0,100] because progress meter only accepts these values
			progress = Math.max(0, progress);
			progress = Math.min(progress, 100);
			Progressmeter progressMeter = new Progressmeter();
			row.appendChild(progressMeter);
			progressMeter.setValue(progress);
			progressMeter.setWidth("150px");

			String orderValue = "" + progress;
			while (orderValue.length() < 4) {
				orderValue = "0" + orderValue;
			}
			progressMeter.setAttribute("orderValue", orderValue);
		}

		rowNumber ++;

		return rowNumber;
	}

}
