/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.superTeacher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.util.Clients;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.ICaseComponentManager;
import nl.surf.emergo.business.IComponentManager;
import nl.surf.emergo.business.IRunManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitInitBox;
import nl.surf.emergo.control.run.cases.PVtoolkit.util.CRunPVToolkitCustomAttributeHelper;
import nl.surf.emergo.control.script.CScript;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IEComponent;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpring;

public class CRunPVToolkitSuperTeacherDiv extends CDefDiv {

	private static final long serialVersionUID = -2989150181145019005L;
	
	protected VView vView = CDesktopComponents.vView();
	protected CControl cControl = CDesktopComponents.cControl();
	protected SSpring sSpring = CDesktopComponents.sSpring();
	protected CScript cScript = CDesktopComponents.cScript();

	public String zulfilepath = ((CRunPVToolkitInitBox)CDesktopComponents.vView().getComponent("PV-toolkit_initBox")).getZulfilepath();
	
	protected CRunPVToolkit pvToolkit;
		
	protected IERunGroup _actor;
	protected boolean _editable;
	protected Map<String,String> _idMap;
	
	protected String _idPrefix = "superTeacher";
	protected String _classPrefix = "superTeacher";
	
	public void onCreate(CreateEvent aEvent) {
		_idPrefix = CRunPVToolkitCustomAttributeHelper.handleCustomAttribute(this, "_idPrefix", _idPrefix);
	}
	
	public void init(IERunGroup actor, boolean editable) {
		_actor = actor;
		_editable = editable;
		
		setClass(_classPrefix);

		pvToolkit = (CRunPVToolkit)CDesktopComponents.vView().getComponent("pvToolkit");
		
		update();
		
		setVisible(true);
	}
	
	public void init(IERunGroup actor, boolean editable, Map<String,String> idMap) {
		_idMap = idMap;
		init(actor, editable);
	}
	
	public void update() {
		//override
	}
		
	protected IEComponent getComponent(String pvComponentCode) {
		IComponentManager componentManager = (IComponentManager)sSpring.getBean("componentManager");
		IEComponent pvComponent = null;
		for (IEComponent component : componentManager.getAllComponents(true)) {
			if (component.getCode().equals(pvComponentCode)) {
				pvComponent = component;
				break;
			}
		}
		return pvComponent;
	}
	
	protected List<IECaseComponent> getCaseComponents(String pvComponentCode) {
		IEComponent pvComponent = getComponent(pvComponentCode);
		if (pvComponent == null) {
			return new ArrayList<IECaseComponent>();
		}
		else {
			ICaseComponentManager caseComponentManager = (ICaseComponentManager)sSpring.getBean("caseComponentManager");
			return caseComponentManager.getAllCaseComponentsByComId(pvComponent.getComId());
		}
	}
	
	protected void addElementPart(StringBuffer element, String key, String value, boolean separator) {
		element.append("\"");
		element.append(key);
		element.append("\":\"");
		element.append(value);
		element.append("\"");
		if (separator) {
			element.append(",");
		}
	}

	protected void addElementPart(StringBuffer element, String key, StringBuffer value, boolean separator) {
		element.append("\"");
		element.append(key);
		element.append("\":");
		element.append(value);
		if (separator) {
			element.append(",");
		}
	}

	protected void addElement(IECaseComponent caseComponent, String owner, String elementType, IXMLTag nodeTag, StringBuffer myElements, StringBuffer allElements) {
		StringBuffer element = new StringBuffer();
		addElementPart(element, "elementType", elementType, true);
		addElementPart(element, "cacId", "" + caseComponent.getCacId(), true);
		addElementPart(element, "tagId", nodeTag.getAttribute(AppConstants.keyId), true);
		addElementPart(element, "runId", "0", true);
		addElementPart(element, "name", sSpring.unescapeXML(nodeTag.getChildValue("name")), false);
		if (caseComponent.getEAccount().getAccId() == sSpring.getRunGroupAccount().getEAccount().getAccId()) {
			//author of case component is current user
			if (myElements.length() > 0) {
				myElements.append(",");
			}
			myElements.append("{");
			myElements.append(element);
			myElements.append("}");
		}
		if (allElements.length() > 0) {
			allElements.append(",");
		}
		element.append(",");
		addElementPart(element, "owner", owner, false);
		allElements.append("{");
		allElements.append(element);
		allElements.append("}");
	}
		
	protected void addElement(IERun run, String owner, String elementType, StringBuffer myElements, StringBuffer allElements) {
		StringBuffer element = new StringBuffer();
		addElementPart(element, "elementType", elementType, true);
		addElementPart(element, "cacId", "0", true);
		addElementPart(element, "tagId", "0", true);
		addElementPart(element, "runId", "" + run.getRunId(), true);
		addElementPart(element, "name", sSpring.isPreviewRun() ? run.getECase().getName() : run.getName(), false);
		if (run.getEAccount().getAccId() == sSpring.getRunGroupAccount().getEAccount().getAccId()) {
			//author of run is current user
			if (myElements.length() > 0) {
				myElements.append(",");
			}
			myElements.append("{");
			myElements.append(element);
			myElements.append("}");
		}
		if (allElements.length() > 0) {
			allElements.append(",");
		}
		element.append(",");
		addElementPart(element, "owner", owner, false);
		allElements.append("{");
		allElements.append(element);
		allElements.append("}");
	}
		
	protected StringBuffer[] getContent(String pvComponentCode, String pvContentName) {
		//get my pv elements: cacid, tagid, title
		//get all pv elements: cacid, tagid, title, owner
		StringBuffer myElements = new StringBuffer();
		StringBuffer allElements = new StringBuffer();

		IAccountManager accountManager = (IAccountManager)sSpring.getBean("accountManager");
		for (IECaseComponent caseComponent : getCaseComponents(pvComponentCode)) {
			String owner = accountManager.getAccountName(caseComponent.getEAccount());
			//NOTE get xml data plus status tags, so preview readonly mode will work as well
			//NOTE only tags of current case are returned, so no tags of other cases, because cScript only looks in current case
			//TODO fix this in future
			List<IXMLTag> nodeTags = cScript.getRunGroupNodeTags(caseComponent);
			if (nodeTags != null) {
				for (IXMLTag nodeTag : nodeTags) {
					if (nodeTag.getName().equals(pvContentName)) {
						addElement(caseComponent, owner, pvContentName, nodeTag, myElements, allElements);
					}
				}
			}
		}

		myElements.insert(0, "[");
		myElements.append("]");
		allElements.insert(0, "[");
		allElements.append("]");
		
		return new StringBuffer[]{myElements, allElements};
	}
		
	protected void updateContent(String pvComponentCode, String pvContentName) {
		StringBuffer[] content = getContent(pvComponentCode, pvContentName);
		
		Clients.evalJavaScript("init" + VView.getCapitalizeFirstChar(pvContentName) + "s('" + content[0].toString() + "','" + content[1].toString() + "');");
	}
		
	protected void initAllContent(String pvComponentCode, String pvContentName) {
		StringBuffer[] content = getContent(pvComponentCode, pvContentName);
		
		Clients.evalJavaScript("initAll" + VView.getCapitalizeFirstChar(pvContentName) + "s('" + content[1].toString() + "');");
	}
		
	protected int getCurrentRunStatus() {
		if (sSpring.isPreviewRun()) {
			return AppConstants.run_status_test;
		}
		else {
			return AppConstants.run_status_runnable;
		}
	}
		
	protected void updateRunContent(String pvComponentCode, String pvContentName) {
		List<Integer> casIds = new ArrayList<Integer>();
		for (IECaseComponent caseComponent : getCaseComponents(pvComponentCode)) {
			Integer casId = new Integer(caseComponent.getECase().getCasId());
			if (!casIds.contains(casId)) {
				casIds.add(casId);
			}
		}

		//get my pv elements: cacid, tagid, title
		//get all pv elements: cacid, tagid, title, owner
		StringBuffer myElements = new StringBuffer();
		StringBuffer allElements = new StringBuffer();

		IRunManager runManager = (IRunManager)sSpring.getBean("runManager");
		IAccountManager accountManager = (IAccountManager)sSpring.getBean("accountManager");
		int neededRunStatus = getCurrentRunStatus();
		for (Integer casId : casIds) {
			for (IERun run : runManager.getAllRunsByCasId(casId.intValue())) {
				if (run.getStatus() == neededRunStatus) {
					String owner = accountManager.getAccountName(run.getEAccount()); 
					addElement(run, owner, pvContentName, myElements, allElements);
				}
			}
		}

		myElements.insert(0, "[");
		myElements.append("]");
		allElements.insert(0, "[");
		allElements.append("]");
		
		Clients.evalJavaScript("init" + VView.getCapitalizeFirstChar(pvContentName) + "s('" + myElements.toString() + "','" + allElements.toString() + "');");
	}
		

	public void onNotify(Event event) {
		JSONObject jsonObject = (JSONObject)event.getData();
		String action = (String)jsonObject.get("action");
		if (action.equals("navigate_forward") || action.equals("navigate_backward")) {
			String toId = (String)jsonObject.get("toId");
			setVisible(false);
			CRunPVToolkitSuperTeacherDiv div = (CRunPVToolkitSuperTeacherDiv)CDesktopComponents.vView().getComponent(toId);
			if (div != null) {
				if (action.equals("navigate_forward")) {
					div.init(pvToolkit.getCurrentRunGroup(), true, _idMap);
				}
				else if (action.equals("navigate_backward")) {
					div.init(pvToolkit.getCurrentRunGroup(), true, _idMap);
				}
			}
		}
		else if (action.equals("edit_element")) {
			String elementType = (String)jsonObject.get("elementType");
			String cacId = (String)jsonObject.get("cacId");
			String tagId = (String)jsonObject.get("tagId");
			String runId = (String)jsonObject.get("runId");
			String elementName = (String)jsonObject.get("elementName");
			if (elementType.equals("rubric") || elementType.equals("method")) {
				IXMLTag elementTag = sSpring.getTag(sSpring.getCaseComponent(cacId), tagId);
				if (elementTag != null) {
					editElementTag(elementType, elementTag, elementName);
				}
			}
			initAllContent("PV_" + elementType + "s", elementType);
		}
		else if (action.equals("delete_element")) {
			String elementType = (String)jsonObject.get("elementType");
			String cacId = (String)jsonObject.get("cacId");
			String tagId = (String)jsonObject.get("tagId");
			String runId = (String)jsonObject.get("runId");
			if (elementType.equals("rubric") || elementType.equals("method")) {
				IXMLTag elementTag = sSpring.getTag(sSpring.getCaseComponent(cacId), tagId);
				if (elementTag != null) {
					deleteElementTag(elementType, elementTag);
				}
			}
			initAllContent("PV_" + elementType + "s", elementType);
		}
		else if (action.equals("show_element")) {
			String elementType = (String)jsonObject.get("elementType");
			String mode = (String)jsonObject.get("mode");
			String cacId = (String)jsonObject.get("cacId");
			String tagId = (String)jsonObject.get("tagId");
			String runId = (String)jsonObject.get("runId");
			String elementName = (String)jsonObject.get("elementName");
			if (mode.equals("new") && elementName.equals("")) {
				Clients.evalJavaScript("showDefaultAlertPopup('" + vView.getLabel("PV-toolkit-superTeacher." + elementType + "s.message.body.element.empty") + "');");
				return;
			}
			setVisible(false);
			if (mode.equals("new")) {
				if (elementType.equals("rubric") || elementType.equals("method")) {
					Map<String,String> childTagNameValueMap = new HashMap<String,String>();
					childTagNameValueMap.put("pid", elementName);
					childTagNameValueMap.put("name", elementName);
					if (elementType.equals("method")) {
						childTagNameValueMap.put("refrubric", "");
					}
					IXMLTag nodeTag = addElementTag("", elementType, elementType, childTagNameValueMap);

					StringBuffer myElements = new StringBuffer();
					StringBuffer allElements = new StringBuffer();

					IAccountManager accountManager = (IAccountManager)sSpring.getBean("accountManager");
					String owner = accountManager.getAccountName(sSpring.getRunGroupAccount().getEAccount()); 
					addElement(getCaseComponent(elementType), owner, elementType, nodeTag, myElements, allElements);

					myElements.insert(0, "[");
					myElements.append("]");
					allElements.insert(0, "[");
					allElements.append("]");
					
					cacId = "" + getCaseComponent(elementType).getCacId();
					tagId = nodeTag.getAttribute(AppConstants.keyId);

					Clients.evalJavaScript("add" + VView.getCapitalizeFirstChar(elementType) + "s('" + myElements.toString() + "');");
				}
				//TODO create new run element
			}
			if (mode.equals("update")) {
				if (elementType.equals("rubric")) {
					//check if for rubric tag if number of performance levels is set and if it has skill cluster or sub skill children
					//get node tag
					IXMLTag nodeTag = sSpring.getTag(sSpring.getCaseComponent(cacId), tagId);
					if (!isRubricInputOk(nodeTag)) {
						//if not ok, change mode to new, so dialog is shown to set number of performance levels and/or add skill clusters and/or sub skills
						mode = "new";
					}
				}
			}
			CRunPVToolkitSuperTeacherDiv div = (CRunPVToolkitSuperTeacherDiv)CDesktopComponents.vView().getComponent("superTeacher" + VView.getCapitalizeFirstChar(mode) + VView.getCapitalizeFirstChar(elementType) + "Div");
			if (div != null) {
				Map<String,String> idMap = new HashMap<String,String>();
				idMap.put("cacId", cacId);
				idMap.put("tagId", tagId);
				idMap.put("runId", runId);
				div.init(pvToolkit.getCurrentRunGroup(), true, idMap);
			}
		}
		else if (action.equals("copy_element")) {
			String elementType = (String)jsonObject.get("elementType");
			String cacId = (String)jsonObject.get("cacId");
			String tagId = (String)jsonObject.get("tagId");
			String runId = (String)jsonObject.get("runId");
			String elementName = (String)jsonObject.get("elementName");
			if (elementType.equals("rubric") || elementType.equals("method")) {
				IXMLTag originalNodeTag = sSpring.getTag(sSpring.getCaseComponent(cacId), tagId);
				Map<String,String> childTagNameValueMap = new HashMap<String,String>();
				childTagNameValueMap.put("pid", elementName);
				childTagNameValueMap.put("name", elementName);
				IXMLTag nodeTag = copyElementTag("", elementType, originalNodeTag, childTagNameValueMap);

				StringBuffer myElements = new StringBuffer();
				StringBuffer allElements = new StringBuffer();

				IAccountManager accountManager = (IAccountManager)sSpring.getBean("accountManager");
				String owner = accountManager.getAccountName(sSpring.getRunGroupAccount().getEAccount()); 
				addElement(getCaseComponent(elementType), owner, elementType, nodeTag, myElements, allElements);

				myElements.insert(0, "[");
				myElements.append("]");
				allElements.insert(0, "[");
				allElements.append("]");
				
				Clients.evalJavaScript("add" + VView.getCapitalizeFirstChar(elementType) + "s('" + myElements.toString() + "');");

				initAllContent("PV_" + elementType + "s", elementType);
			}
			//TODO copy run element
		}
	}
	
	protected boolean isRubricInputOk(IXMLTag tag) {
		if (tag == null || tag.getChildValue("name").equals("") || tag.getChildValue("numberofperformancelevels").equals("")) {
			//name is empty or number of performance levels is not set
			return false;
		}
		IXMLTag skillTag = tag.getChild("skill");
		if (skillTag == null || (skillTag.getChilds("skillcluster").size() == 0 && skillTag.getChilds("subskill").size() == 0)) {
			//it has no skill child or the skill child has no skill cluster or sub skill children
			return false;
		}
		return true;
	}
	
	protected boolean isMethodInputOk(IXMLTag tag) {
		IXMLTag rubricTag = pvToolkit.getReferencedNodeTag(sSpring.getCaseComponent(_idMap.get("cacId")), tag, "refrubric");
		if (tag == null || tag.getChildValue("name").equals("") || rubricTag == null || tag.getChilds("cycle").size() == 0) {
			//name is empty or no rubric is chosen or number of cycles is 0
			return false;
		}
		return true;
	}
	
	protected IECaseComponent getCaseComponent(String elementType) {
		IECaseComponent caseComponent = null;
		if (elementType.equals("rubric")) {
			//get case component of type 'PV_rubrics', it should exist when working with PV-tool. There will be a case with a number of empty case components
			caseComponent = sSpring.getCaseComponent(CRunPVToolkit.gCaseComponentNameRubrics, "");
		}
		else if (elementType.equals("method")) {
			caseComponent = sSpring.getCaseComponent(CRunPVToolkit.gCaseComponentNameProjects, "");
		}
		else if (elementType.equals("reference")) {
			caseComponent = sSpring.getCaseComponent("", "PV_references");
		}
		return caseComponent;
	}
	
	public IXMLTag addElementTag(String parentTagId, String elementType, String tagName, Map<String,String> childTagNameValueMap) {
		IECaseComponent caseComponent = getCaseComponent(elementType);
		IXMLTag elementTag = null;
		if (StringUtils.isEmpty(parentTagId)) {
			if (elementType.equals("rubric")) {
				if (!sSpring.isReadOnlyRun()) {
					//NOTE if not read only run update data tree. It is updated in application memory as well.
					IXMLTag dataRootTag = sSpring.getXmlDataTree(caseComponent);
					elementTag = addNodeTag(dataRootTag.getChild(AppConstants.contentElement), tagName, childTagNameValueMap);
					//NOTE already add skill tag
					IXMLTag skillTag = addNodeTag(elementTag, "skill", childTagNameValueMap);
					if (skillTag != null) {
						//NOTE already add performance level tags
						addPerformancelevelTags(skillTag);
					}
					sSpring.setCaseComponentData("" + caseComponent.getCacId(), sSpring.getXmlManager().xmlTreeToDoc(dataRootTag));
				}
				//NOTE always update data plus status tree cached as SSpring property. This will work for both preview and preview read-only mode. 
				IXMLTag dataPlusStatusRootTag = sSpring.getXmlDataPlusRunStatusTree(caseComponent, AppConstants.statusTypeRunGroup);
				elementTag = addNodeTag(dataPlusStatusRootTag.getChild(AppConstants.contentElement), tagName, childTagNameValueMap);
				//NOTE already add skill tag
				IXMLTag skillTag = addNodeTag(elementTag, "skill", childTagNameValueMap);
				if (skillTag != null) {
					addPerformancelevelTags(skillTag);
				}
			}
			else if (elementType.equals("method")) {
				Map<String,String> projectTagNameValueMap = new HashMap<String,String>();
				projectTagNameValueMap.put("pid", "PV_project");
				projectTagNameValueMap.put("name", "PV_project");
				if (!sSpring.isReadOnlyRun()) {
					//NOTE if not read only run update data tree. It is updated in application memory as well.
					IXMLTag dataRootTag = sSpring.getXmlDataTree(caseComponent);
					//method tag is child of project tag
					IXMLTag projectTag = getNodeTag(dataRootTag, "project");
					if (projectTag == null) {
						projectTag = addNodeTag(dataRootTag.getChild(AppConstants.contentElement), "project", projectTagNameValueMap);
					}
					addNodeTag(projectTag, tagName, childTagNameValueMap);
					sSpring.setCaseComponentData("" + caseComponent.getCacId(), sSpring.getXmlManager().xmlTreeToDoc(dataRootTag));
				}
				//NOTE always update data plus status tree cached as SSpring property. This will work for both preview and preview read-only mode. 
				IXMLTag dataPlusStatusRootTag = sSpring.getXmlDataPlusRunStatusTree(caseComponent, AppConstants.statusTypeRunGroup);
				//method tag is child of project tag
				IXMLTag projectTag = getNodeTag(dataPlusStatusRootTag, "project");
				if (projectTag == null) {
					projectTag = addNodeTag(dataPlusStatusRootTag.getChild(AppConstants.contentElement), "project", projectTagNameValueMap);
				}
				elementTag = addNodeTag(projectTag, tagName, childTagNameValueMap);
			}
		}
		else {
			if (!sSpring.isReadOnlyRun()) {
				//NOTE if not read only run update data tree. It is updated in application memory as well.
				IXMLTag dataRootTag = sSpring.getXmlDataTree(caseComponent);
				IXMLTag tempParentTag = sSpring.getXmlManager().getTagById(dataRootTag, parentTagId);
				if (tempParentTag != null) {
					elementTag = addNodeTag(tempParentTag, tagName, childTagNameValueMap);
					if (tagName.equals("cycle")) {
						//NOTE already add step tags
						addStepTags(elementTag);
					}
				}
				sSpring.setCaseComponentData("" + caseComponent.getCacId(), sSpring.getXmlManager().xmlTreeToDoc(dataRootTag));
			}
			//NOTE always update data plus status tree cached as SSpring property. This will work for both preview and preview read-only mode. 
			IXMLTag dataPlusStatusRootTag = sSpring.getXmlDataPlusRunStatusTree(caseComponent, AppConstants.statusTypeRunGroup);
			IXMLTag tempParentTag = sSpring.getXmlManager().getTagById(dataPlusStatusRootTag, parentTagId);
			if (tempParentTag != null) {
				elementTag = addNodeTag(tempParentTag, tagName, childTagNameValueMap);
				if (tagName.equals("cycle")) {
					//NOTE already add step tags
					addStepTags(elementTag);
				}
			}
		}
		//NOTE return data plus status element because it is always set
		return elementTag;
	}
	
	public IXMLTag copyElementTag(String parentTagId, String elementType, IXMLTag originalElementTag, Map<String,String> childTagNameValueMap) {
		IXMLTag elementTag = null;
		if (elementType.equals("rubric")) {
			IECaseComponent caseComponent = getCaseComponent(elementType);
			if (!sSpring.isReadOnlyRun()) {
				//NOTE if not read only run update data tree. It is updated in application memory as well.
				IXMLTag dataRootTag = sSpring.getXmlDataTree(caseComponent);
				if (StringUtils.isEmpty(parentTagId)) {
					copyNodeTag(dataRootTag.getChild(AppConstants.contentElement), originalElementTag, childTagNameValueMap);
				}
				else {
					IXMLTag tempParentTag = sSpring.getXmlManager().getTagById(dataRootTag, parentTagId);
					if (tempParentTag != null) {
						copyNodeTag(tempParentTag, originalElementTag, childTagNameValueMap);
					}
				}
				sSpring.setCaseComponentData("" + caseComponent.getCacId(), sSpring.getXmlManager().xmlTreeToDoc(dataRootTag));
			}
			//NOTE always update data plus status tree cached as SSpring property. This will work for both preview and preview read-only mode. 
			IXMLTag dataPlusStatusRootTag = sSpring.getXmlDataPlusRunStatusTree(caseComponent, AppConstants.statusTypeRunGroup);
			if (StringUtils.isEmpty(parentTagId)) {
				elementTag = copyNodeTag(dataPlusStatusRootTag.getChild(AppConstants.contentElement), originalElementTag, childTagNameValueMap);
			}
			else {
				IXMLTag tempParentTag = sSpring.getXmlManager().getTagById(dataPlusStatusRootTag, parentTagId);
				if (tempParentTag != null) {
					copyNodeTag(tempParentTag, originalElementTag, childTagNameValueMap);
				}
			}
		}
		else if (elementType.equals("method")) {
			//TODO copying tags belonging to a method using parentTag 
			IECaseComponent caseComponent = getCaseComponent(elementType);
			Map<String,String> projectTagNameValueMap = new HashMap<String,String>();
			projectTagNameValueMap.put("pid", "PV_project");
			projectTagNameValueMap.put("name", "PV_project");
			if (!sSpring.isReadOnlyRun()) {
				//NOTE if not read only run update data tree. It is updated in application memory as well.
				IXMLTag dataRootTag = sSpring.getXmlDataTree(caseComponent);
				if (StringUtils.isEmpty(parentTagId)) {
					//method tag is child of project tag
					IXMLTag projectTag = getNodeTag(dataRootTag, "project");
					if (projectTag == null) {
						projectTag = addNodeTag(dataRootTag.getChild(AppConstants.contentElement), "project", projectTagNameValueMap);
					}
					copyNodeTag(projectTag, originalElementTag, childTagNameValueMap);
				}
				else {
					IXMLTag tempParentTag = sSpring.getXmlManager().getTagById(dataRootTag, parentTagId);
					if (tempParentTag != null) {
						copyNodeTag(tempParentTag, originalElementTag, childTagNameValueMap);
					}
				}
				sSpring.setCaseComponentData("" + caseComponent.getCacId(), sSpring.getXmlManager().xmlTreeToDoc(dataRootTag));
			}
			//NOTE always update data plus status tree cached as SSpring property. This will work for both preview and preview read-only mode. 
			IXMLTag dataPlusStatusRootTag = sSpring.getXmlDataPlusRunStatusTree(caseComponent, AppConstants.statusTypeRunGroup);
			if (StringUtils.isEmpty(parentTagId)) {
				//method tag is child of project tag
				IXMLTag projectTag = getNodeTag(dataPlusStatusRootTag, "project");
				if (projectTag == null) {
					projectTag = addNodeTag(dataPlusStatusRootTag.getChild(AppConstants.contentElement), "project", projectTagNameValueMap);
				}
				elementTag = copyNodeTag(projectTag, originalElementTag, childTagNameValueMap);
			}
			else {
				IXMLTag tempParentTag = sSpring.getXmlManager().getTagById(dataPlusStatusRootTag, parentTagId);
				if (tempParentTag != null) {
					copyNodeTag(tempParentTag, originalElementTag, childTagNameValueMap);
				}
			}
		}
		//NOTE return data plus status element because it is always set
		return elementTag;
	}
	
	public IXMLTag editElementTag(String elementType, IXMLTag elementTag, String elementName) {
		IECaseComponent caseComponent = null;
		IXMLTag rootTag = null;
		if (elementType.equals("rubric") || elementType.equals("method")) {
			caseComponent = getCaseComponent(elementType);
			if (!sSpring.isReadOnlyRun()) {
				//NOTE if not read only run update data tree. It is updated in application memory as well.
				rootTag = sSpring.getXmlDataTree(caseComponent);
				editNodeTag(rootTag, elementTag, elementName);
				sSpring.setCaseComponentData("" + caseComponent.getCacId(), sSpring.getXmlManager().xmlTreeToDoc(rootTag));
			}
			//NOTE always update data plus status tree cached as SSpring property. This will work for both preview and preview read-only mode. 
			elementTag = editNodeTag(sSpring.getXmlDataPlusRunStatusTree(caseComponent, AppConstants.statusTypeRunGroup), elementTag, elementName);
		}
		//NOTE return data plus status element because it is always set
		return elementTag;
	}
	
	protected IXMLTag editElementTagChildValue(IXMLTag elementTag, String elementType, String childTagName, String childTagValue) {
		IECaseComponent caseComponent = getCaseComponent(elementType);
		if (!sSpring.isReadOnlyRun()) {
			//NOTE if not read only run update data tree. It is updated in application memory as well.
			IXMLTag rootTag = sSpring.getXmlDataTree(caseComponent);
			elementTag = sSpring.getXmlManager().getTagById(rootTag, elementTag.getAttribute(AppConstants.statusKeyId));
			setChildTagValue(elementTag, childTagName, childTagValue);
			sSpring.setCaseComponentData("" + caseComponent.getCacId(), sSpring.getXmlManager().xmlTreeToDoc(rootTag));
		}
		//NOTE always update data plus status tree cached as SSpring property. This will work for both preview and preview read-only mode.
		IXMLTag rootTag = sSpring.getXmlDataPlusRunStatusTree(caseComponent, AppConstants.statusTypeRunGroup);
		elementTag = sSpring.getXmlManager().getTagById(rootTag, elementTag.getAttribute(AppConstants.statusKeyId));
		setChildTagValue(elementTag, childTagName, childTagValue);
		//NOTE return data plus status element because it is always set
		return elementTag;
	}
	
	protected IXMLTag editElementTagStatusAttributeValue(IXMLTag elementTag, String elementType, String statusAttributeKey, String statusAttributeValue) {
		IECaseComponent caseComponent = getCaseComponent(elementType);
		if (!sSpring.isReadOnlyRun()) {
			//NOTE if not read only run update data tree. It is updated in application memory as well.
			IXMLTag rootTag = sSpring.getXmlDataTree(caseComponent);
			elementTag = sSpring.getXmlManager().getTagById(rootTag, elementTag.getAttribute(AppConstants.statusKeyId));
			setStatusAttributeValue(elementTag, statusAttributeKey, statusAttributeValue);
			sSpring.setCaseComponentData("" + caseComponent.getCacId(), sSpring.getXmlManager().xmlTreeToDoc(rootTag));
		}
		//NOTE always update data plus status tree cached as SSpring property. This will work for both preview and preview read-only mode.
		IXMLTag rootTag = sSpring.getXmlDataPlusRunStatusTree(caseComponent, AppConstants.statusTypeRunGroup);
		elementTag = sSpring.getXmlManager().getTagById(rootTag, elementTag.getAttribute(AppConstants.statusKeyId));
		setStatusAttributeValue(elementTag, statusAttributeKey, statusAttributeValue);
		//NOTE return data plus status element because it is always set
		return elementTag;
	}
	
	public IXMLTag deleteElementTag(String elementType, IXMLTag elementTag) {
		IECaseComponent caseComponent = null;
		IXMLTag rootTag = null;
		if (elementType.equals("rubric") || elementType.equals("method")) {
			caseComponent = getCaseComponent(elementType);
			if (!sSpring.isReadOnlyRun()) {
				//NOTE if not read only run update data tree. It is updated in application memory as well.
				rootTag = sSpring.getXmlDataTree(caseComponent);
				deleteNodeTag(rootTag, elementTag);
				sSpring.setCaseComponentData("" + caseComponent.getCacId(), sSpring.getXmlManager().xmlTreeToDoc(rootTag));
			}
			//NOTE always update data plus status tree cached as SSpring property. This will work for both preview and preview read-only mode. 
			elementTag = deleteNodeTag(sSpring.getXmlDataPlusRunStatusTree(caseComponent, AppConstants.statusTypeRunGroup), elementTag);
		}
		//NOTE return data plus status element because it is always set
		return elementTag;
	}
	
	protected IXMLTag getNodeTag(IXMLTag parentTag, String tagName) {
		IXMLTag tag = null;
		for (IXMLTag nodeTag : cScript.getNodeTags(parentTag)) {
			if (nodeTag.getName().equals(tagName)) {
				tag = nodeTag;
				break;
			}
		}
		return tag;
	}

	protected IXMLTag addNodeTag(IXMLTag parentTag, String tagName, Map<String,String> childTagNameValueMap) {
		IXMLTag nodeTag = getNewNodeTag(parentTag, tagName, childTagNameValueMap);
		parentTag.getChildTags().add(nodeTag);
		return nodeTag;
	}

	protected IXMLTag addNodeTagAfterTag(IXMLTag parentTag, IXMLTag tagBeforeNewTag, String tagName, Map<String,String> childTagNameValueMap) {
		IXMLTag nodeTag = getNewNodeTag(parentTag, tagName, childTagNameValueMap);
		int index = parentTag.getChildTags().indexOf(tagBeforeNewTag);
		parentTag.getChildTags().add(index + 1, nodeTag);
		return nodeTag;
	}

	protected IXMLTag addNodeTagBeforeTag(IXMLTag parentTag, IXMLTag tagAfterNewTag, String tagName, Map<String,String> childTagNameValueMap) {
		IXMLTag nodeTag = getNewNodeTag(parentTag, tagName, childTagNameValueMap);
		int index = parentTag.getChildTags().indexOf(tagAfterNewTag);
		parentTag.getChildTags().add(index, nodeTag);
		return nodeTag;
	}

	protected IXMLTag getNewNodeTag(IXMLTag parentTag, String tagName, Map<String,String> childTagNameValueMap) {
		IXMLTag nodeTag = sSpring.getXmlManager().newXMLTag(tagName, "");
		nodeTag.setParentTag(parentTag);
		nodeTag.setDefTag(pvToolkit.getDefNodeTag(parentTag, tagName));
		nodeTag.setAttribute(AppConstants.defKeyType, AppConstants.defValueNode);
		sSpring.getXmlManager().setTagId(nodeTag);
		for (Map.Entry<String, String> entry : childTagNameValueMap.entrySet()) {
			setChildTagValue(nodeTag, entry.getKey(), entry.getValue());
	    }
		return nodeTag;
	}

	protected void addPerformancelevelTags(IXMLTag parentTag) {
		IXMLTag rubricTag = parentTag.getParentTag();
		while (rubricTag != null && !rubricTag.getName().equals("rubric")) {
			rubricTag = rubricTag.getParentTag();
		}
		if (rubricTag != null) {
			int numberOfPerformanceLevels = 5;
			try {
				numberOfPerformanceLevels = Integer.parseInt(rubricTag.getChildValue("numberofperformancelevels"));
			} catch (NumberFormatException e) {
			}
			for (int i=numberOfPerformanceLevels;i>=1;i--) {
				Map<String,String> childTagNameValueMap = new HashMap<String,String>();
				childTagNameValueMap.put("pid", "" + i);
				childTagNameValueMap.put("level", "" + i);
				addNodeTag(parentTag, "performancelevel", childTagNameValueMap);
			}
		}
	}

	protected void addStepTags(IXMLTag cycleTag) {
		if (cycleTag == null) {
			return;
		}
		for (int i=0;i<5;i++) {
			Map<String,String> childTagNameValueMap = new HashMap<String,String>();
			childTagNameValueMap.put("pid", vView.getLabel("PV-toolkit.steptype." + CRunPVToolkit.stepTypes[i].getStepType()));
			childTagNameValueMap.put("name", vView.getLabel("PV-toolkit.steptype." + CRunPVToolkit.stepTypes[i].getStepType()));
			childTagNameValueMap.put("nameforteacher", vView.getLabel("PV-toolkit.steptype." + CRunPVToolkit.stepTypes[i].getStepType() + ".forteacher"));
			childTagNameValueMap.put("nameforpeerstudent", vView.getLabel("PV-toolkit.steptype." + CRunPVToolkit.stepTypes[i].getStepType() + ".forpeerstudent"));
			childTagNameValueMap.put("steptype", CRunPVToolkit.stepTypes[i].getStepType());
			IXMLTag stepTag = addNodeTag(cycleTag, "step", childTagNameValueMap);
			childTagNameValueMap = new HashMap<String,String>();
			for (int j=0;j<CRunPVToolkit.subStepTypes[i].length;j++) {
				childTagNameValueMap = new HashMap<String,String>();
				childTagNameValueMap.put("pid", vView.getLabel("PV-toolkit.substeptype." + CRunPVToolkit.subStepTypes[i][j].getSubStepType()));
				childTagNameValueMap.put("name", vView.getLabel("PV-toolkit.substeptype." + CRunPVToolkit.subStepTypes[i][j].getSubStepType()));
				childTagNameValueMap.put("substeptype", CRunPVToolkit.subStepTypes[i][j].getSubStepType());
				addNodeTag(stepTag, "substep", childTagNameValueMap);
			}
		}
	}

	protected IXMLTag copyNodeTag(IXMLTag parentTag, IXMLTag originalNodeTag) {
		IXMLTag nodeTag = sSpring.getXmlManager().copyTagUnique(originalNodeTag, parentTag, true);
		//NOTE copyTagUnique does not add tag to children of parent
		parentTag.getChildTags().add(nodeTag);
		return nodeTag;
	}

	protected IXMLTag copyNodeTag(IXMLTag parentTag, IXMLTag originalNodeTag, Map<String,String> childTagNameValueMap) {
		IXMLTag nodeTag = copyNodeTag(parentTag, originalNodeTag);
		//NOTE set new pid and name
		for (Map.Entry<String, String> entry : childTagNameValueMap.entrySet()) {
			setChildTagValue(nodeTag, entry.getKey(), entry.getValue());
	    }
		return nodeTag;
	}

	protected IXMLTag addChildTag(IXMLTag tag, String childTagName, String childTagValue) {
		IXMLTag childTag = sSpring.getXmlManager().newXMLTag(childTagName, "");
		tag.getChildTags().add(childTag);
		childTag.setParentTag(tag);
		childTag.setDefTag(tag.getDefTag().getChild(childTagName));
		childTag.setValue(sSpring.escapeXML(childTagValue));
		return childTag;
	}

	protected IXMLTag editNodeTag(IXMLTag rootTag, IXMLTag elementTag, String elementName) {
		elementTag = sSpring.getXmlManager().getTagById(rootTag, elementTag.getAttribute(AppConstants.statusKeyId));
		elementName = sSpring.escapeXML(elementName);
		elementTag.setChildValue("pid", elementName);
		elementTag.setChildValue("name", elementName);
		return elementTag;
	}

	protected IXMLTag deleteNodeTag(IXMLTag rootTag, IXMLTag elementTag) {
		elementTag = sSpring.getXmlManager().getTagById(rootTag, elementTag.getAttribute(AppConstants.statusKeyId));
		elementTag.getParentTag().getChildTags().remove(elementTag);
		elementTag.setParentTag(null);
		return elementTag;
	}

	protected String getChildTagValue(IXMLTag tag, String childTagName) {
		return sSpring.unescapeXML(tag.getChildValue(childTagName));
	}

	protected void setChildTagValue(IXMLTag tag, String childTagName, String childTagValue) {
		if (tag.getChild(childTagName) == null) {
			addChildTag(tag, childTagName, childTagValue);
		}
		else {
			tag.setChildValue(childTagName, sSpring.escapeXML(childTagValue));
		}
	}

	protected String getStatusAttributeValue(IXMLTag tag, String statusAttributeKey) {
		IXMLTag statusTag = tag.getChild(AppConstants.statusElement);
		return statusTag == null ? "" :statusTag.getAttribute(statusAttributeKey);
	}

	protected void setStatusAttributeValue(IXMLTag tag, String statusAttributeKey, String statusAttributeValue) {
		IXMLTag statusTag = tag.getChild(AppConstants.statusElement);
		if (statusTag == null) {
			statusTag = sSpring.getXmlManager().newXMLTag(AppConstants.statusElement, "");
			statusTag.setParentTag(tag);
			tag.getChildTags().add(statusTag);
		}
		statusTag.setAttribute(statusAttributeKey, statusAttributeValue);
	}

	protected IXMLTag moveNodeTag(IXMLTag parentTag, IXMLTag nodeTag) {
		nodeTag.getParentTag().getChildTags().remove(nodeTag);
		nodeTag.setParentTag(parentTag);
		parentTag.getChildTags().add(nodeTag);
		return nodeTag;
	}

	protected IXMLTag moveNodeTagAfterTag(IXMLTag parentTag, IXMLTag nodeTag, IXMLTag tagBeforeTag) {
		int index = parentTag.getChildTags().indexOf(tagBeforeTag);
		if (index >= 0) {
			if (tagBeforeTag.getParentTag() != nodeTag.getParentTag()) {
				index++;
			}
			nodeTag.getParentTag().getChildTags().remove(nodeTag);
			nodeTag.setParentTag(parentTag);
			parentTag.getChildTags().add(index, nodeTag);
		}
		return nodeTag;
	}

	protected IXMLTag moveNodeTagBeforeTag(IXMLTag parentTag, IXMLTag nodeTag, IXMLTag tagAfterTag) {
		int index = parentTag.getChildTags().indexOf(tagAfterTag);
		if (index >= 0) {
			nodeTag.getParentTag().getChildTags().remove(nodeTag);
			nodeTag.setParentTag(parentTag);
			parentTag.getChildTags().add(index, nodeTag);
		}
		return nodeTag;
	}

}
