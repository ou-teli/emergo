/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.Kastanje;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.util.StringUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.business.IXMLAttributeValueTime;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.control.run.CRunForms;
import nl.surf.emergo.control.run.CRunWnd;
import nl.surf.emergo.control.run.ounl2.CRunComponentOunl2;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpringHelper;

public class CRunKastanjehoeveEditFormInitBox extends CRunScoreInitBox {

	private static final long serialVersionUID = -8457322710625346255L;
	
	//NOTE: should be present in corresonding states component; 0 = not yet started, 1 = open, 2 = closed
	private static final String macroState = "macrostate";
	
	private static final String numberOfCorrectItems = "numberofcorrectitems";
	
	private static final String numberOfAnsweredItems = "numberofanswereditems";
	
	private static final String formFullyFilled = "formfullyfilled";
	
	protected VView vView = CDesktopComponents.vView();
	
	protected SSpringHelper sSpringHelper = new SSpringHelper();

	/** The run wnd. */
	protected CRunWnd runWnd = (CRunWnd) CDesktopComponents.vView().getComponent(CControl.runWnd);

	/** The reference case component. */
	protected IECaseComponent caseComponent;

	/** The states case component. */
	protected IECaseComponent statesComponent;

	protected List<Hashtable<String,Object>> dataElements;
	
	public CRunKastanjehoeveEditFormInitBox() {
		setId("editFormMacro");
	}

	public void onCreate() {
		//NOTE use echoEvent, otherwise macro parent does not yet exist, if code is used within zscript
	  	Events.echoEvent("onInit", this, null);
	}
	
	public void onInit() {
		
		IXMLTag lMacroTag = (IXMLTag)((HtmlMacroComponent)CRunComponent.getMacroParent(this)).getDynamicProperty("a_tag");
		statesComponent = CDesktopComponents.sSpring().getCaseComponent(CDesktopComponents.sSpring().getCase(), "", lMacroTag.getChildValue("pid") + " states");
		if (statesComponent == null)
			return;
		List<IXMLTag> lStateTags = CDesktopComponents.cScript().getRunGroupNodeTags(statesComponent, "state");
		String lBgImgName = "";
		String lPieceStyle = "";
		
		for (IXMLTag lStateTag : lStateTags) {
			if (lStateTag.getChildValue("key").equals("backgroundimage"))
				lBgImgName = lStateTag.getCurrentStatusAttribute("value");
			if (lStateTag.getChildValue("key").equals("referenceform"))
				caseComponentName = lStateTag.getCurrentStatusAttribute("value");
			if (lStateTag.getChildValue("key").equals(macroState))
				CDesktopComponents.sSpring().setRunTagStatus(statesComponent, lStateTag, AppConstants.statusKeyValue, "1", null,
						true, AppConstants.statusTypeRunGroup, true, false);
			if (lStateTag.getChildValue("key").equals("formpiecesstyle"))
				lPieceStyle = lStateTag.getCurrentStatusAttribute("value");
		}
		
		if (caseComponentName.equals(""))
			return;
		
		Map<String,Object> propertyMap = new HashMap<String,Object>();
		propertyMap.put("observer", getId());
		propertyMap.put("macroid", getId());
		propertyMap.put("casecomponent", ((HtmlMacroComponent)CRunComponent.getMacroParent(this)).getDynamicProperty("a_casecomponent"));
		propertyMap.put("tag", lMacroTag);
		propertyMap.put("backgroundimage", lBgImgName);
		propertyMap.put("piecesstyle", lPieceStyle);
		
		caseComponent = CDesktopComponents.sSpring().getCaseComponent(CDesktopComponents.sSpring().getCase(), "", caseComponentName);
		
		//Transfer of code from EditForms
		//NOTE showFeedback and showScore are for all forms!
		CRunForms Forms = new CRunForms();
		dataElements = new ArrayList<Hashtable<String,Object>>();
		SSpringHelper sSpringHelper = new SSpringHelper();
		List<IXMLTag> formTags = CDesktopComponents.cScript().getRunGroupNodeTags(caseComponent, "form");
		//List<IXMLTag> formTags = Forms.getFormTags();
		
		for (IXMLTag formTag : formTags) {
			Hashtable<String,Object> hFormDataElements = new Hashtable<String,Object>();
			hFormDataElements.put("tag", formTag);
			hFormDataElements.put("title", sSpringHelper.getTagChildValue(formTag, "title", ""));
			hFormDataElements.put("hovertext", sSpringHelper.getTagChildValue(formTag, "hovertext", ""));

			//NOTE following list is needed so you can do nested foreach looping! see below.
			List<Hashtable<String,Object>> pieceOrRefpieceDataElementsList = new ArrayList<Hashtable<String,Object>>();
			List<IXMLTag> pieceOrRefpieceTags = Forms.getPieceAndRefpieceTags(formTag);
			Forms.handlepieceOrRefpieces(pieceOrRefpieceDataElementsList, pieceOrRefpieceTags); 
			hFormDataElements.put("pieces", pieceOrRefpieceDataElementsList);

			//NOTE following list is needed so you can do nested foreach looping! see below.
			List<Hashtable<String,Object>> itemDataElementsList = new ArrayList<Hashtable<String,Object>>();
			//NOTE get all item tags, also non present ones
			List<IXMLTag> itemTags = formTag.getChilds("selector");
			for (IXMLTag itemTag : itemTags) {

				Hashtable<String,Object> hItemDataElements = new Hashtable<String,Object>();
				hItemDataElements.put("tag", itemTag);

				if (itemTag.getName().equals("selector")) {
					hItemDataElements.put("title", sSpringHelper.getTagChildValue(itemTag, "title", ""));
					hItemDataElements.put("answer", sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, "answer", ""));
					hItemDataElements.put("options", sSpringHelper.getTagChildValue(itemTag, "options", ""));
					hItemDataElements.put("multiselect", sSpringHelper.getTagChildValue(itemTag, "multiselect", "false"));
					hItemDataElements.put("numberofrows", sSpringHelper.getTagChildValue(itemTag, "numberofrows", "-1"));
				}

				hItemDataElements.put("styleposition", runWnd.getStylePosition(itemTag));
				hItemDataElements.put("stylewidth", runWnd.getStyleWidth(itemTag));
				hItemDataElements.put("stylesize", runWnd.getStyleSize(itemTag) + "text-align-last:center;");
				
				hItemDataElements.put("present", sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, AppConstants.statusKeyPresent, "true"));
				
				itemDataElementsList.add(hItemDataElements);
			}
			hFormDataElements.put("items", itemDataElementsList);

			//NOTE following list is needed so you can do nested foreach looping! see below.
			pieceOrRefpieceDataElementsList = new ArrayList<Hashtable<String,Object>>();
			IXMLTag feedbackConditionTag = Forms.getCurrentFeedbackConditionTag(formTag);
			pieceOrRefpieceTags = Forms.getPieceAndRefpieceTags(feedbackConditionTag);
			Forms.handlepieceOrRefpieces(pieceOrRefpieceDataElementsList, pieceOrRefpieceTags); 
			hFormDataElements.put("feedbackpieces", pieceOrRefpieceDataElementsList);

			dataElements.add(hFormDataElements);
		}

		propertyMap.put("currentEmergoComponent", this);
		propertyMap.put("dataElements", dataElements);
		List<IXMLTag> lFbTags = CDesktopComponents.cScript().getRunGroupNodeTags(caseComponent, AppConstants.contentElement);
		IXMLTag lRootTag = CDesktopComponents.sSpring().getXmlDataPlusRunStatusTree(caseComponent, AppConstants.statusTypeRunGroup);
		CRunComponent lHlpComp = new CRunComponent();	
		propertyMap.put("feedbackButtonUrl", sSpringHelper.stripEmergoPath(lHlpComp.getUrl(lRootTag.getChild(AppConstants.contentElement), "feedbackbutton")));

		//Calls the graphics .zul file
		String zulfilepath = (String)((HtmlMacroComponent)CRunComponent.getMacroParent(this)).getDynamicProperty("a_zulfilepath");
		propertyMap.put("zulfilepath", zulfilepath);
	
		HtmlMacroComponent macro = new HtmlMacroComponent();
		appendChild(macro);
		macro.setDynamicProperty("a_propertyMap", propertyMap);
		macro.setMacroURI(zulfilepath + "run_Kastanjehoeve_edit_form_view_macro.zul");
	}

	/**
	 * Saves selection.
	 *
	 * @param aSelectionTag the a selection tag
	 * @param aSelection the a selection, comma separated row numbers of chosen options
	 */
	public void saveSelection(IXMLTag aSelectionTag, String aSelection) {
		CDesktopComponents.sSpring().setRunTagStatus(caseComponent, aSelectionTag, AppConstants.statusKeyAnswer, aSelection, null,
				true, AppConstants.statusTypeRunGroup, true, false);
		CDesktopComponents.sSpring().setRunTagStatus(caseComponent, aSelectionTag, AppConstants.statusKeySelected, AppConstants.statusValueTrue, null,
				true, AppConstants.statusTypeRunGroup, true, false);
		//NOTE if aSelection contains same number of chosen options as the number of correct options, match aSelection with correct options and set statusKeyCorrect.
		//NOTE aSelection and correct options both contain numbers in ascending order.
		//NOTE so if selector input element is singleselect statusKeyCorrect is set at every choice,
		//but if multiselect it is only set if the number of chosen options equals the number of correct options.
		String lCorrectOptions = aSelectionTag.getChildValue("correctoptions");
		if (StringUtils.countOccurrencesOf(aSelection, ",") == StringUtils.countOccurrencesOf(lCorrectOptions, ",")) {
			Pattern p = Pattern.compile(lCorrectOptions, Pattern.DOTALL);
			Matcher m = p.matcher(aSelection);
			CDesktopComponents.sSpring().setRunTagStatus(caseComponent, aSelectionTag, AppConstants.statusKeyCorrect, "" + m.matches(), null,
					true, AppConstants.statusTypeRunGroup, true, false);
		}
	}

	/**
	 * Show feedback.
	 *
	 * @param aFormTag the a form tag
	 *
	 * @return the feedback condition tag
	 */
	public IXMLTag showFeedback(IXMLTag aFormTag) {
		List<IXMLTag> lFormTags = new ArrayList<IXMLTag>();
		lFormTags.add(aFormTag);
		return showFeedback(lFormTags);
	}

	/**
	 * Increase number of attempts.
	 *
	 * @param aTag the a tag
	 *
	 * @return number of attempts
	 */
	public int increaseNumberOfAttempts(IXMLTag aTag) {
		int lNumberOfAttempts = 0;
		try {
			lNumberOfAttempts = Integer.parseInt(aTag.getCurrentStatusAttribute(AppConstants.statusKeyNumberofattempts));
		} catch (NumberFormatException e) {
			lNumberOfAttempts = 0;
		}
		lNumberOfAttempts++;
		CDesktopComponents.sSpring().setRunTagStatus(caseComponent, aTag, AppConstants.statusKeyNumberofattempts, "" + lNumberOfAttempts, null,
				true, AppConstants.statusTypeRunGroup, true, false);
		return lNumberOfAttempts;
	}

	/**
	 * Show feedback.
	 *
	 * @param aFormTags the a form tags
	 *
	 * @return the feedback condition tag
	 */
	public IXMLTag showFeedback(List<IXMLTag> aFormTags) {
		CRunComponentOunl2 lRC = new CRunComponentOunl2();
		lRC.setCaseComponent(caseComponent);

		IXMLTag lFeedbackCondition = lRC.showFeedback(aFormTags);
	  	if (lFeedbackCondition != null) {
		  	//NOTE score -1 means 'no relevant score yet' because tool instruction is shown, so don't increase number of attempts.
		  	if (!lFeedbackCondition.getChildValue("score").equals("-1")) {
				//increase numberofattempts for all form tags
				List<IXMLTag> lFormTags = lRC.getPresentChildTags(CDesktopComponents.sSpring().getXmlDataPlusRunStatusTree(caseComponent, AppConstants.statusTypeRunGroup).getChild(AppConstants.contentElement), "form");
				for (IXMLTag lFormTag : lFormTags) {
					increaseNumberOfAttempts(lFormTag);
				}
		  	}
	  	}
		return lFeedbackCondition;
	}

	public Component getEditformItemDiv(Component zulItem) {
		Component editformItemDiv = zulItem;
		while (editformItemDiv != null && !editformItemDiv.getId().startsWith("editformItemDiv_", 0)) {
			editformItemDiv = editformItemDiv.getParent();
		}
		return editformItemDiv;
	}

	public void onReady(Event aEvent) {
		IXMLTag currentFormTag = (IXMLTag)dataElements.get(0).get("tag");
		IXMLTag triggeredFeedbackConditionTag = showFeedback(currentFormTag);
		//NOTE score -1 means 'no relevant score yet' because tool instruction is shown, so feedback mechanism is misused to show reactions of npc's.
		//TODO rename feedback in reaction in Java and zul
		if (triggeredFeedbackConditionTag != null) {
			if (!sSpringHelper.getTagChildValue(triggeredFeedbackConditionTag, "score", "-1").equals("-1")) {
				List<Component> editformItems = vView.getComponentsByPrefix("editformItem_");
				List<IXMLTag> handledTags = new ArrayList<IXMLTag>();
				for (Component editformItem : editformItems) {
					//NOTE editformItemOrContainer is used to keep the number of attempts
					Component editformItemOrContainer = getEditformItemOrContainer(editformItem);
					//NOTE editformItemDiv is set visible or not using script
					Component editformItemDiv = getEditformItemDiv(editformItem);
					if (editformItemOrContainer != null && editformItemDiv != null && editformItemDiv.isVisible()) {
						//NOTE only update number of attempts and send onUpdate if editformItemDiv tag is present
						if (!handledTags.contains(editformItemOrContainer.getAttribute("tag"))) {
							handledTags.add((IXMLTag)editformItemOrContainer.getAttribute("tag"));
							increaseNumberOfAttempts((IXMLTag)editformItemOrContainer.getAttribute("tag"));
						}
						Events.postEvent("onUpdate", editformItem, null);
					}
				}
			}
			for (Hashtable<String,Object> dataElement : dataElements) {
				//for each form
				//Events.postEvent("onUpdate", vView.getComponent("feedbackPieces_" + ((IXMLTag)dataElement.get("tag")).getAttribute("id")), triggeredFeedbackConditionTag);
			}
		}
	}
	
	public IXMLTag getFormTag(IXMLTag tag) {
		while (tag != null && !tag.getName().equals("form")) {
			tag = tag.getParentTag();
		}
		return tag;
	}

	public IXMLTag getContainerTag(IXMLTag tag) {
		while (tag != null && !tag.getName().equals("container")) {
			tag = tag.getParentTag();
		}
		return tag;
	}

	public boolean isShowIfRightOrWrong(IXMLTag tag) {
		// get form tag
		IXMLTag formTag = getFormTag(tag);
		if (formTag == null) {
			return false;
		}
		int maxNumber = runWnd.getNumber(sSpringHelper.getTagChildValue(formTag, "maxnumberofattempts", "-1"), -1);
		int number = runWnd.getNumber(sSpringHelper.getCurrentStatusTagStatusChildAttribute(formTag, AppConstants.statusKeyNumberofattempts, "0"), 0);
		return maxNumber > -1 && number >= maxNumber;
	}

	public boolean isHideRightWrong(IXMLTag tag) {
		if (containerDoNotShowRightWrong(tag)) {
			return true;
		}
		IXMLTag formTag = getFormTag(tag);
		return formTag == null || sSpringHelper.getTagChildValue(formTag, "showrightwrongonchange", "false").equals("false");
	}

	public boolean containerDoNotShowRightWrong(IXMLTag tag) {
		IXMLTag containerTag = getContainerTag(tag);
		if (containerTag == null) {
			return false;
		}
		return sSpringHelper.getTagChildValue(containerTag, "noshowrightwrong", "false").equals("true");
	}

	public String isInputCorrect(IXMLTag itemTag) {
		if (itemTag.getName().equals("section")) {
			Pattern p = Pattern.compile(sSpringHelper.getTagChildValue(itemTag, "correctanswer", ""), Pattern.DOTALL);
			Matcher m = p.matcher(sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, "string", ""));
			return "" + m.matches();
		}
		if (itemTag.getName().equals("selector")) {
			Pattern p = Pattern.compile(sSpringHelper.getTagChildValue(itemTag, "correctoptions", ""), Pattern.DOTALL);
			Matcher m = p.matcher(sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, "answer", ""));
			return "" + m.matches();
		}
		if (itemTag.getName().equals("alternative")) {
			boolean correct = sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, "correct", "false").equals("true");
			boolean selected = sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, "selected", "false").equals("true");
			if (selected) {
				return "" + correct;
			}
			else {
				return "";
			}
		}
		if (itemTag.getName().equals("text")) {
			boolean correct = sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, "correct", "false").equals("true");
			return "" + correct;
		}
		if (itemTag.getName().equals("plugin")) {
			boolean correct = sSpringHelper.getCurrentStatusTagStatusChildAttribute(getFormTag(itemTag), "correct", "false").equals("true");
			return "" + correct;
		}

		return "false";
	}

	public boolean isInputEverAnswered(IXMLTag itemTag) {
		List<IXMLAttributeValueTime> lValueTimes = null;
		if (itemTag.getName().equals("section")) {
			lValueTimes = itemTag.getStatusAttribute("string");
		}
		if (itemTag.getName().equals("selector")) {
			lValueTimes = itemTag.getStatusAttribute("answer");
		}
		if (itemTag.getName().equals("alternative")) {
			lValueTimes = itemTag.getStatusAttribute("selected");
		}
		if (itemTag.getName().equals("text")) {
			lValueTimes = itemTag.getStatusAttribute("correct");
		}
		if (itemTag.getName().equals("plugin")) {
			lValueTimes = itemTag.getStatusAttribute("correct");
		}
		if ((lValueTimes == null) || (lValueTimes.size() == 0))
			return false;
		return true;
	}

	public Component getEditformItemOrContainer(Component zulItem) {
		//NOTE returns editformItem or for alternatives and texts the parent container
		IXMLTag tag = (IXMLTag)zulItem.getAttribute("tag");
		if (tag == null) {
			return null;
		}
		Component editformItemOrContainer = zulItem;
		if (tag.getName().equals("alternative") || tag.getName().equals("text")) {
			while (editformItemOrContainer != null && (editformItemOrContainer.getAttribute("tag") == null || editformItemOrContainer.getAttribute("tag") == zulItem.getAttribute("tag"))) {
				editformItemOrContainer = editformItemOrContainer.getParent();
			}
			if (editformItemOrContainer == null || editformItemOrContainer.getAttribute("tag") == null) {
				return null;
			}
		}
		return editformItemOrContainer;
	}

	public void updateEditformItem(HtmlBasedComponent zulItem, String sclassPrefix, String hideRightWrongStr) {		
		//NOTE editformItemOrContainer is used to keep the number of attempts
		Component editformItemOrContainer = getEditformItemOrContainer(zulItem);
		if (editformItemOrContainer == null) {
			return;
		}
		boolean hideRightWrong = hideRightWrongStr != null && hideRightWrongStr.equals("hideRightWrong");
		String sclass = sclassPrefix;
		if (isShowIfRightOrWrong((IXMLTag)editformItemOrContainer.getAttribute("tag")) && !hideRightWrong) {
			String isInputCorrect = isInputCorrect((IXMLTag)zulItem.getAttribute("tag"));
			if (isInputCorrect.equals("true")) {
				sclass += "Right";
			}
			else if (isInputCorrect.equals("false")) {
				sclass += "Wrong";
			}
		}
		if (!zulItem.getSclass().equals(sclass)) {
			zulItem.setSclass(sclass);
		}
		//NOTE: calculate new score
		List<Component> editformItems = vView.getComponentsByPrefix("editformItem_");
		int lCorrectItemsTotal = 0;
		int lNumberOfAnsweredItems = 0;
		boolean lFormFullyAnswered = true;
		for (Component editformItem : editformItems) {
			String lPresent = (String)editformItem.getAttribute("present");
			if ((lPresent == null) || (lPresent.equalsIgnoreCase("true"))) {
				IXMLTag lTag = (IXMLTag)editformItem.getAttribute("tag");
				String isInputCorrect = isInputCorrect(lTag);
				if (isInputCorrect.equals("true")) {
					lCorrectItemsTotal += 1;
				}
				if (isInputEverAnswered(lTag))
					lNumberOfAnsweredItems += 1;
				else
					lFormFullyAnswered = false;
			}
		}
		List<IXMLTag> lStateTags = CDesktopComponents.cScript().getRunGroupNodeTags(statesComponent, "state");
		for (IXMLTag lStateTag : lStateTags) {
			if (lStateTag.getChildValue("key").equals(numberOfCorrectItems))
				CDesktopComponents.sSpring().setRunTagStatus(statesComponent, lStateTag, AppConstants.statusKeyValue, "" + lCorrectItemsTotal, null,
						true, AppConstants.statusTypeRunGroup, true, false);
			if (lStateTag.getChildValue("key").equals(numberOfAnsweredItems))
				CDesktopComponents.sSpring().setRunTagStatus(statesComponent, lStateTag, AppConstants.statusKeyValue, "" + lNumberOfAnsweredItems, null,
						true, AppConstants.statusTypeRunGroup, true, false);
			if (lStateTag.getChildValue("key").equals(formFullyFilled))
				CDesktopComponents.sSpring().setRunTagStatus(statesComponent, lStateTag, AppConstants.statusKeyValue, "" + lFormFullyAnswered, null,
						true, AppConstants.statusTypeRunGroup, true, false);
		}
	}

	public void onClose(Event aEvent) {
		List<IXMLTag> lStateTags = CDesktopComponents.cScript().getRunGroupNodeTags(statesComponent, "state");
		for (IXMLTag lStateTag : lStateTags) {
			if (lStateTag.getChildValue("key").equals(macroState))
				CDesktopComponents.sSpring().setRunTagStatus(statesComponent, lStateTag, AppConstants.statusKeyValue, "2", null,
						true, AppConstants.statusTypeRunGroup, true, false);
		}
	}
	
	public void onPieceClicked(Event aEvent) {
		IXMLTag lDataTag = (IXMLTag)aEvent.getData();
		if (lDataTag != null) {
			CDesktopComponents.sSpring().setRunTagStatus(caseComponent, lDataTag, AppConstants.statusKeySelected, AppConstants.statusValueTrue, null,
					true, AppConstants.statusTypeRunGroup, true, false);
			CDesktopComponents.sSpring().setRunTagStatus(caseComponent, lDataTag, AppConstants.statusKeyOpened, AppConstants.statusValueTrue, null,
					true, AppConstants.statusTypeRunGroup, true, false);
		}
	}
	
}
