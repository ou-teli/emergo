/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.control.def.CDefDiv;

public class CRunDragdropformContainerDiv extends CDefDiv {

	private static final long serialVersionUID = -2132088020602193049L;

	public void onMaximize(Event aEvent) {
		boolean maximized = getAttribute("maximized") != null && (Boolean)getAttribute("maximized");
		if (!maximized) {
			maximized = !maximized;
			setAttribute("maximized", maximized);
			Events.postEvent("onUpdateContainer", this, null);
		}
	}

	public void onMinimize(Event aEvent) {
		boolean maximized = getAttribute("maximized") != null && (Boolean)getAttribute("maximized");
		if (maximized) {
			maximized = !maximized;
			setAttribute("maximized", maximized);
			Events.postEvent("onUpdateContainer", this, null);
		}
	}

	public void onUpdateContainer(Event aEvent) {
		//TODO determine itemDiv more reliable
		Component itemDiv = getChildren().get(0).getChildren().get(1);
		boolean maximized = getAttribute("maximized") != null && (Boolean)getAttribute("maximized");
		if (maximized) {
			//TODO create style class?
			setStyle((String)getAttribute("originalstyle") + "z-index:2;top:0px;border: 1px solid #FFDDDD;background-color:#FFEEAA;");
			((HtmlBasedComponent)itemDiv).setStyle((String)itemDiv.getAttribute("originalstyle") + "height:422px;");
		}
		else {
			setStyle((String)getAttribute("originalstyle"));
			((HtmlBasedComponent)itemDiv).setStyle((String)itemDiv.getAttribute("originalstyle"));
		}
		//TODO determine maximize ('get(1)') and restore ('get(2)') buttons more reliable
		Component maximizeBtn = getChildren().get(0).getChildren().get(0).getChildren().get(1);
		Component restoreBtn = getChildren().get(0).getChildren().get(0).getChildren().get(2);
		if (maximized) {
			maximizeBtn.setVisible(false);
			restoreBtn.setVisible(true);
		}
		else {
			maximizeBtn.setVisible(true);
			restoreBtn.setVisible(false);
		}
	}

}
