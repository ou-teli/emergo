/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.view.VView;

public class CRunAssessmentsContentDiv extends CDefDiv {

	private static final long serialVersionUID = 2509480471306979433L;

	protected CRunAssessments currentEmergoComponent;
	
	protected boolean onTablet;

	protected String assessmentsTitle;

	protected String assessmentsInstruction;

	protected List<Object> assessmentsDatas;
	
	protected boolean multipleAssessments;

	protected static final String runAssessmentsZulId = "runAssessmentsZul";
	
	protected static final String runAssessmentsDivId = "runAssessmentsDiv";
	
	protected static final String macroAssessmentsId = "macroAssessments";

	protected int assessmentBtnsOffsetLeft = 0;
	
	protected int assessmentBtnsOffsetTop = 0;

	protected static final String runAssessmentDivId = "runAssessmentDiv";

	protected static final String macroAssessmentId = "macroAssessment";

	protected String assessmentTitle = "";
	
	protected String assessmentInstruction = "";
	
	protected String assessmentCanBeStarted = "true";
	
	protected String assessmentCanBeRestarted = "false";
	
	protected String assessmentCanBeResumed = "false";
	
	protected String assessmentCanBeInspected = "false";
	
	protected String assessmentCanBeFinished = "false";
	
	protected String assessmentShowScore = "true";
	
	protected String assessmentScore = "";
	
	protected String assessmentShowItemButtons = "true";
	
	protected String assessmentFeedbackText = "";
	
	protected List<Object> assessmentFeedbackPieces = new ArrayList<Object>();
	
	protected String assessmentShowFeedback = "true";
	
	protected List<Object> assessmentDatas;

	protected boolean multipleQuestions = true;

	protected static final String runAssessmentItemDivId = "runAssessmentItemDiv";

	protected static final String macroAssessmentItemId = "macroAssessmentItem";

	protected static final String assessmentItemTitle = "";
	
	protected static final String assessmentItemInstruction = "";
	
	protected List<Object> assessmentItemPieces = new ArrayList<Object>();
	
	protected String assessmentItemShowFeedback = "true";
	
	protected String assessmentItemDirectFeedback = "true";
	
	protected String assessmentItemType = "mkea";
	
	protected String assessmentItemAltOrientation = "vertical";

	protected String assessmentItemsEditable = "true";
	
	protected int currentAssessmentItemNumber = 3;

	protected int numberOfAssessmentItems = 5;
	
	protected List<Object> assessmentItemDatas = new ArrayList<Object>();

	protected String assessmentItemFeedbackInstruction = "";
	
	protected List<Object> assessmentItemFeedbackPieces = new ArrayList<Object>();

	protected static final String macroAssessmentFeedbackId = "macroAssessmentFeedback";
	
	protected String labelStyleShift;

	void setComponentVisible(Component component, boolean visible) {
		if (component != null) {
			component.setVisible(visible);
		}
	}

	public void onCreate(CreateEvent aEvent) {
		currentEmergoComponent = (CRunAssessments)VView.getParameter("emergoComponent", this, null);
		
		if (currentEmergoComponent == null || !currentEmergoComponent.getZulfile().equals("")) {
			detach();
			return;
		}

		onTablet = (Boolean)VView.getParameter("on_tablet", this, null);
		//NOTE no difference in tablet or location app anymore
		onTablet = true;
		
		//NOTE get data for assessments panel
		assessmentsTitle = (String)VView.getParameter("assessments_title", this, "");
		assessmentsInstruction = (String)VView.getParameter("assessments_instruction", this, "");
		assessmentsDatas = (List<Object>)VView.getParameter("assessments_datas", this, null);
		if (assessmentsDatas == null) {
			assessmentsTitle = "Assessment title";
		}
		if (assessmentsInstruction == null) {
			assessmentsInstruction = "Assessment instruction";
		}

		if (assessmentsDatas == null) {
			// fill with test data
			String title = "Onderdeel Onderdeel Onderdeel Onderdeel Onderdeel Onderdeel Onderdeel Onderdeel Onderdeel";
			String instruction = "Korte inhoud Korte inhoud Korte inhoud Korte inhoud Korte inhoud Korte inhoud Korte inhoud";
			String numberofitems = "11";
			String score = "9";

			assessmentsDatas = new ArrayList<Object>();
			for (int i=1;i<=20;i++) {
				List<Object> data = new ArrayList<Object>();
				data.add("");
				data.add("");
				data.add("true");
				data.add("false");
				data.add(title + " " + i);
				data.add(instruction);
				data.add(numberofitems);
				data.add("true");
				data.add(score);
				data.add("false");
				data.add("");
				data.add(new ArrayList<Object>());
				assessmentsDatas.add(data);
			}
		}

		//NOTE if assessments instruction also show assessments panel
		multipleAssessments = (assessmentsDatas.size() > 1 || !assessmentsInstruction.equals(""));

		//NOTE get data for assessment panel, it is needed to determine if there are multiple questions
		assessmentDatas = (List<Object>)VView.getParameter("assessment_datas", this, null);
		if (assessmentDatas == null) {
			assessmentTitle = "assessment title";
			assessmentInstruction = "assessment instruction";
		}

		//NOTE determine if multiple questions if only one assessment
		if (!multipleAssessments && assessmentsDatas.size() > 0) {
			//more than one question or no empty assessment instruction or no auto start assessment 
			multipleQuestions = assessmentDatas != null && (assessmentDatas.size() > 1 ||
					!((List<Object>)assessmentsDatas.get(0)).get(5).equals("") ||
					((List<Object>)assessmentsDatas.get(0)).get(9).equals("false"));
		}

		if (multipleAssessments) {
			labelStyleShift += "position:relative;left:12px;top:0px;";
		}
		else {
			labelStyleShift += "position:relative;left:0px;top:0px;";
		}

		//init panels
		Events.echoEvent("onInit", this, null);
	}
	
	public void onInit(Event aEvent) {
		//init panels
		Events.sendEvent("onInit", CDesktopComponents.vView().getComponent(runAssessmentsDivId), null);
		Events.sendEvent("onInit", CDesktopComponents.vView().getComponent(runAssessmentDivId), null);
		Events.sendEvent("onInit", CDesktopComponents.vView().getComponent(runAssessmentItemDivId), null);

		//NOTE show assessment panel or assessment item panel
		if (assessmentsDatas.size() > 0) {
			if (!multipleAssessments) {
				if (!multipleQuestions) {
					currentEmergoComponent.prepairAssessmentItem((String)((List<Object>)assessmentsDatas.get(0)).get(0));
					currentEmergoComponent.handleAssessmentItem((String)((List<Object>)assessmentDatas.get(0)).get(0));
				}
				else {
					currentEmergoComponent.handleAssessment((String)((List<Object>)assessmentsDatas.get(0)).get(0));
				}
			}
		}
	}
	
	public void onCloseAssessments() {
		if (currentEmergoComponent != null) {
			currentEmergoComponent.close();
		}
	}
	
}
