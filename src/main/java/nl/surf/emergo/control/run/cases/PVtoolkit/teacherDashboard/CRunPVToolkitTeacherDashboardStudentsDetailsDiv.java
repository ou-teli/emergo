/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.teacherDashboard;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Progressmeter;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitInitBox;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitNavigationButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitSkillWheelLevel1Div;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTag;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitPeerGroupMemberProgress;
import nl.surf.emergo.control.run.cases.PVtoolkit.util.CRunPVToolkitCustomAttributeHelper;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;

public class CRunPVToolkitTeacherDashboardStudentsDetailsDiv extends CDefDiv {

	private static final long serialVersionUID = -1908881424089127066L;

	public String zulfilepath = ((CRunPVToolkitInitBox)CDesktopComponents.vView().getComponent("PV-toolkit_initBox")).getZulfilepath();
	
	protected CRunPVToolkit pvToolkit;
		
	protected IECaseComponent caseComponent;
	protected IXMLTag skillTag;

	protected IERunGroup _actor;
	protected boolean _editable;
	protected List<IXMLTag> _commonPeerGroupTags;

	public boolean _initialized = false;
	
	protected List<String> otherRolesToFilterOn;
	
	protected List<IERunGroupAccount> runGroupAccountsToInclude;
	protected List<String> peerRolesToInclude;
	
	protected boolean showGroups = false;

	protected final static long improvePercentageThreshold = 101;
	protected final static long declinePercentageThreshold = 99;
	
	protected boolean _showBackButton = true;

	protected String _idPrefix = "teacherDashboard";
	protected String _classPrefix = "dashboardStudentsDetails";
	
	public void onCreate(CreateEvent aEvent) {
		_showBackButton = CRunPVToolkitCustomAttributeHelper.handleCustomAttribute(this, "_showBackButton", _showBackButton);

		_idPrefix = CRunPVToolkitCustomAttributeHelper.handleCustomAttribute(this, "_idPrefix", _idPrefix);
	}
	
	protected boolean showGroups() {
		return false;
	}

	public void init(IERunGroup actor, boolean editable, List<IXMLTag> commonPeerGroupTags) {
		_actor = actor;
		_editable = editable;
		_commonPeerGroupTags = commonPeerGroupTags;
		
		//NOTE always initialize, because recordings may be added during a session
		setClass(_classPrefix);

		pvToolkit = (CRunPVToolkit)CDesktopComponents.vView().getComponent("pvToolkit");
		
		runGroupAccountsToInclude = new ArrayList<IERunGroupAccount>();
		peerRolesToInclude = new ArrayList<String>();

		caseComponent = pvToolkit.getCurrentRubricsCaseComponent();
		skillTag = pvToolkit.getCurrentSkillTag();
		if (skillTag == null) {
			return;
		}
		otherRolesToFilterOn = null;
		if (pvToolkit.excludeStudentAssistantsFeedback(actor)) {
			otherRolesToFilterOn = new ArrayList<String>();
			otherRolesToFilterOn.add(CRunPVToolkit.peerGroupStudentRole);
			otherRolesToFilterOn.add(CRunPVToolkit.peerGroupTeacherRole);
			otherRolesToFilterOn.add(CRunPVToolkit.peerGroupPeerStudentRole);
		}
		
		showGroups = showGroups();

		update();
		
		_initialized = true;
	}
	
	public void update() {
		getChildren().clear();
		
		pvToolkit.setMemoryCaching(true);
		
		Div div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{"dashboardBackground"}
		);
		
		div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{"titleRight"}
		);
		
		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font titleRight", getTitleLabelKey()}
		);
		
		if (_showBackButton) {
			CRunPVToolkitNavigationButton btn = new CRunPVToolkitNavigationButton(this, 
					new String[]{"class", "cLabelKey"}, 
					new Object[]{"font pvtoolkitButton " + _classPrefix + "BackButton", "PV-toolkit.back"}
					);
			btn.init(getId(), _idPrefix + "Div");
		}

		div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "Div"}
		);
		
		Rows rows = appendGrid(div);
		int rowNumber = 1;
		rowNumber = appendDataToGrid(rows, rowNumber);
		
		pvToolkit.setMemoryCaching(false);
	}

	protected String getTitleLabelKey() {
		return "PV-toolkit-dashboard.header.students";
	}
	
    protected Rows appendGrid(Component parent) {
    	return pvToolkit.appendGrid(
    			parent, 
    			getId() + "Grid",
    			"position:absolute;left:0px;top:0px;width:1060px;height:480px;overflow:auto;", 
				new boolean[] {true, true, true, pvToolkit.hasCycleTags(), true, true, true}, 
    			new String[] {"3%", "25%", "15%", "7%", "10%", "25%", "15%"}, 
    			new boolean[] {false, true, true, true, true, true, false},
    			null,
    			"PV-toolkit-dashboard.column.label.", 
    			new String[] {"", "name", "group", "cycle", "development", "progress", "viewStudent"});

    }

	protected int appendDataToGrid(Rows rows, int rowNumber) {
		return appendDataToGrid(
				rows, 
				rowNumber,
				new boolean[] {true, true, true, pvToolkit.hasCycleTags(), true, true, true} 
				);
	}

	protected void determineRugsAndPeerRolesToInclude() {
		for (IXMLTag peerGroupTag : _commonPeerGroupTags) {
			List<IXMLTag> memberTags = pvToolkit.getStatusChildTag(peerGroupTag).getChilds("member");
			for (IXMLTag memberTag : memberTags) {
				IERunGroupAccount runGroupAccount = pvToolkit.getRunGroupAccount(pvToolkit.getStatusChildTagAttribute(memberTag, "rgaid"));
				String peerRole = pvToolkit.getStatusChildTagAttribute(memberTag, "role");
				boolean includeRunGroupAccount = runGroupAccount != null &&
						peerRole.equals(CRunPVToolkit.peerGroupStudentRole);
				if (includeRunGroupAccount) {
					runGroupAccountsToInclude.add(runGroupAccount);
					peerRolesToInclude.add(peerRole);
				}
			}
		}
	}

	protected boolean includeRunGroupAccount(IERunGroupAccount runGroupAccount) {
		if (runGroupAccount == null)
			return false;
		for (IERunGroupAccount tempRunGroupAccount : runGroupAccountsToInclude) {
			if (tempRunGroupAccount.getRgaId() == runGroupAccount.getRgaId()) {
				return true;
			}
		}
		return false;
	}

	protected CRunPVToolkitPeerGroupMemberProgress getPeerGroupMemberProgress(List<CRunPVToolkitPeerGroupMemberProgress> runPVToolkitPeerGroupMembersProgress, IERunGroupAccount runGroupAccount) {
		for (CRunPVToolkitPeerGroupMemberProgress peerGroupMemberProgress : runPVToolkitPeerGroupMembersProgress) {
			if (peerGroupMemberProgress.getRunGroupAccount().getRgaId() == runGroupAccount.getRgaId()) {
				return peerGroupMemberProgress;
			}
		}
		return null;
	}

	protected CRunPVToolkitPeerGroupMemberProgress getPeerGroupMemberProgress(
			int counter,
			int reachedCycleNumber,
			List<Double> progressPerCycle
			) {
		if (counter == 0) {
			reachedCycleNumber = 0;
			progressPerCycle = new ArrayList<Double>();
			for (int i=0;i<pvToolkit.getMaxCycleNumber();i++) {
				progressPerCycle.add(new Double(0.0));
			}
		}
		else {
			double temp = reachedCycleNumber;
			temp = temp / counter;
			reachedCycleNumber = (int)Math.round(temp);
			for (int i=0;i<pvToolkit.getMaxCycleNumber();i++) {
				double progress = progressPerCycle.get(i);
				progressPerCycle.set(i, progress / counter);
			}
		}
		return new CRunPVToolkitPeerGroupMemberProgress(null, "", reachedCycleNumber, progressPerCycle);
	}

	protected int appendDataToGrid(
			Rows rows, 
			int rowNumber,
    		boolean[] showColumn 
			) {
		determineRugsAndPeerRolesToInclude();
		List<CRunPVToolkitPeerGroupMemberProgress> runPVToolkitPeerGroupMembersProgress = pvToolkit.getPeerGroupMembersProgress(runGroupAccountsToInclude, peerRolesToInclude);
		for (IXMLTag peerGroupTag : _commonPeerGroupTags) {
			int groupReachedCycleNumber = 0;
			List<Double> groupProgressPerCycle = new ArrayList<Double>();
			for (int i=0;i<pvToolkit.getMaxCycleNumber();i++) {
				groupProgressPerCycle.add(0.0);
			}
			double groupDevelopment = 0.0;
			int groupCounter = 0;
			List<IXMLTag> memberTags = pvToolkit.getStatusChildTag(peerGroupTag).getChilds("member");
			for (IXMLTag memberTag : memberTags) {
				IERunGroupAccount runGroupAccount = pvToolkit.getRunGroupAccount(pvToolkit.getStatusChildTagAttribute(memberTag, "rgaid"));
				if (includeRunGroupAccount(runGroupAccount)) {
					CRunPVToolkitPeerGroupMemberProgress peerGroupMemberProgress = getPeerGroupMemberProgress(runPVToolkitPeerGroupMembersProgress, runGroupAccount);
					List<List<IXMLTag>> sharedFeedbackTagsPerFeedbackStep = pvToolkit.getSharedFeedbackTagsPerFeedbackStep(runGroupAccount.getERunGroup(), false, otherRolesToFilterOn, null);
					int maxCycleNumber = sharedFeedbackTagsPerFeedbackStep.size();
					double development = getDevelopment(runGroupAccount.getERunGroup(), maxCycleNumber, skillTag, sharedFeedbackTagsPerFeedbackStep);
					if (!showGroups) {
						rowNumber = appendRowToGrid(
								rows, 
								rowNumber,
								showColumn,
								peerGroupTag,
								runGroupAccount,
								peerGroupMemberProgress,
								development
								);
					}
					if (showGroups) {
						groupReachedCycleNumber += peerGroupMemberProgress.getReachedCycleNumber();
						for (int i=0;i<pvToolkit.getMaxCycleNumber();i++) {
							Double groupProgress = groupProgressPerCycle.get(i) + peerGroupMemberProgress.getProgressPerCycle().get(i);
							groupProgressPerCycle.set(i, groupProgress);
						}
						groupDevelopment += development;
						groupCounter++;
					}
				}
			}
			if (showGroups) {
				rowNumber = appendRowToGrid(
						rows, 
						rowNumber,
						showColumn,
						peerGroupTag,
						null,
						getPeerGroupMemberProgress(groupCounter, groupReachedCycleNumber, groupProgressPerCycle),
						groupDevelopment / groupCounter
						);
			}
		}

		return rowNumber;
	}

	double getDevelopment(IERunGroup actor, int cycleNumber, IXMLTag skillTag, List<List<IXMLTag>> sharedFeedbackTagsPerFeedbackStep) {
		double development = 1;
		if (cycleNumber <= 1) {
			//no development yet
			return development;
		}
		//get skill development
		development = pvToolkit.getDevelopment(actor, cycleNumber, skillTag, sharedFeedbackTagsPerFeedbackStep);
		//get sub skill development
		double subskillDevelopment = 0;
		int subskillCounter = 0;
		for (IXMLTag skillclusterTag : skillTag.getChilds("skillcluster")) {
			if (pvToolkit.isRunSpecificTagPresent(new CRunPVToolkitCacAndTag(caseComponent, skillclusterTag))) {
				//number of sub skills per skill cluster
				for (IXMLTag subskillTag : skillclusterTag.getChilds("subskill")) {
					if (pvToolkit.isRunSpecificTagPresent(new CRunPVToolkitCacAndTag(caseComponent, subskillTag))) {
						subskillDevelopment += pvToolkit.getDevelopment(actor, cycleNumber, subskillTag, sharedFeedbackTagsPerFeedbackStep);
						subskillCounter++;
					}
				}
			}
		}
		if (subskillCounter > 0) {
			//get mean of skill development and sub skill development, so weighting is 50%-50%
			development = (development + (subskillDevelopment / subskillCounter)) / 2;
		}
		return development;
	}
	
	protected int appendRowToGrid(
			Rows rows, 
			int rowNumber,
    		boolean[] showColumn,
    		IXMLTag peerGroupTag,
    		IERunGroupAccount runGroupAccount,
    		CRunPVToolkitPeerGroupMemberProgress runPVToolkitPeerGroupMemberProgress,
    		double development
			) {
		Row row = new Row();
		rows.appendChild(row);
			
		int columnNumber = 0;
		if (showColumn.length > columnNumber && showColumn[columnNumber]) {
			String src = "";
			if (!showGroups) {
				src = zulfilepath + "user.svg";
			}
			else {
				src = zulfilepath + "user-friends.svg";
			}
			new CRunPVToolkitDefImage(row, 
					new String[]{"class", "src"}, 
					new Object[]{"gridImage", src}
					);
		}

		columnNumber++;
		if (showColumn.length > columnNumber && showColumn[columnNumber]) {
			String name = "";
			if (!showGroups) {
				if (runGroupAccount != null) {
					name = runGroupAccount.getERunGroup().getName();
				}
			}
			else {
				name = peerGroupTag.getChildAttribute(AppConstants.statusElement, "name");
			}
			Label label = new CRunPVToolkitDefLabel(row, 
					new String[]{"class", "value"}, 
					new Object[]{"font gridLabel", name}
					);
			if (!showGroups && runGroupAccount != null) {
				IEAccount account = runGroupAccount.getEAccount();
				label.setAttribute("orderValue", "" + account.getLastname() + "_" + account.getNameprefix() + "_" + account.getInitials());
			}
		}

		if (!showGroups) {
			columnNumber++;
			if (showColumn.length > columnNumber && showColumn[columnNumber]) {
				new CRunPVToolkitDefLabel(row, 
						new String[]{"class", "value"}, 
						new Object[]{"font gridLabel", peerGroupTag.getChildAttribute(AppConstants.statusElement, "name")}
						);
			}
		}
		
		columnNumber++;
		if (showColumn.length > columnNumber && showColumn[columnNumber]) {
			int cycleNumber = runPVToolkitPeerGroupMemberProgress.getReachedCycleNumber();
			Label label = new CRunPVToolkitDefLabel(row, 
					new String[]{"class", "value"}, 
					new Object[]{"font gridLabel", "" + cycleNumber}
					);

			String orderValue = "" + cycleNumber;
			while (orderValue.length() < 4) {
				orderValue = "0" + orderValue;
			}
			label.setAttribute("orderValue", orderValue);
		}

		columnNumber++;
		if (showColumn.length > columnNumber && showColumn[columnNumber]) {
			String src = "";
			long percentage = (new Double(development*100)).longValue();
			String percentageStr = "";
			if (percentage <= declinePercentageThreshold) {
				src = "chart-line-down.svg";
				percentageStr = " (" + (percentage - 100) + "%)";
			}
			else if (percentage >= improvePercentageThreshold) {
				src = "chart-line-up.svg";
				percentageStr = " (" + (percentage - 100) + "%)";
			}
			else {
				src = "chart-bar.svg";
			}

			Div div = new CRunPVToolkitDefDiv(row, null, null); 
			String orderValue = "" + percentage;
			while (orderValue.length() < 6) {
				orderValue = "0" + orderValue;
			}
			div.setAttribute("orderValue", orderValue);
			new CRunPVToolkitDefImage(div, 
					new String[]{"class", "src"}, 
					new Object[]{"gridImage", zulfilepath + src}
					);
			if (!percentageStr.equals("")) {
				new CRunPVToolkitDefLabel(div, 
					new String[]{"class", "value"}, 
					new Object[]{"font gridLabel", percentageStr}
					);
			}
		}

		columnNumber++;
		if (showColumn.length > columnNumber && showColumn[columnNumber]) {
			//NOTE show progress in last reached cycle
			int reachedCycleNumber = runPVToolkitPeerGroupMemberProgress.getReachedCycleNumber();
			int progress = (new Double(100 * runPVToolkitPeerGroupMemberProgress.getProgressPerCycle().get(reachedCycleNumber - 1))).intValue();
			//NOTE to be sure restrict progress to [0,100] because progress meter only accepts these values
			progress = Math.max(0, progress);
			progress = Math.min(progress, 100);
			Progressmeter progressMeter = new Progressmeter();
			row.appendChild(progressMeter);
			progressMeter.setValue(progress);
			progressMeter.setWidth("225px");

			String orderValue = "" + progress;
			while (orderValue.length() < 4) {
				orderValue = "0" + orderValue;
			}
			progressMeter.setAttribute("orderValue", orderValue);
			progressMeter.setRenderdefer(100);
		}

		if (!showGroups) {
			columnNumber++;
			if (showColumn.length > columnNumber && showColumn[columnNumber]) {
				Label linkLabel = new CRunPVToolkitDefLabel(row, 
						new String[]{"class", "labelKey"}, 
						new Object[]{"font gridLink", "PV-toolkit-dashboard.column.label.viewStudent"}
						);
				toSkillWheelOnClickEventListener(this, linkLabel, runGroupAccount.getERunGroup());
			}
		}
		
		rowNumber ++;

		return rowNumber;
	}

	protected void toSkillWheelOnClickEventListener(Component fromComponent, Component component, IERunGroup runGroup) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				fromComponent.setVisible(false);
				
				CRunPVToolkitSkillWheelLevel1Div div = (CRunPVToolkitSkillWheelLevel1Div)CDesktopComponents.vView().getComponent(_idPrefix + "SkillWheelLevel2Div");
				if (div != null) {
					// NOTE we need to reset peergroup filter and cycle number
					div._cycleNumber = 0;
					div.otherRgaIdsToFilterOn = null;
					div.init(runGroup, false, 0, 0);
					div.setVisible(true);
				}
			}
		});
	}
	
}
