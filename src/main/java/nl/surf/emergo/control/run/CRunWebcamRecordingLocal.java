/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoField;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Progressmeter;
import org.zkoss.zul.Timer;

import com.github.kokorin.jaffree.StreamType;
import com.github.kokorin.jaffree.ffmpeg.FFmpeg;
import com.github.kokorin.jaffree.ffmpeg.FFmpegProgress;
import com.github.kokorin.jaffree.ffmpeg.FFmpegResultFuture;
import com.github.kokorin.jaffree.ffmpeg.NullOutput;
import com.github.kokorin.jaffree.ffmpeg.ProgressListener;
import com.github.kokorin.jaffree.ffmpeg.UrlInput;
import com.github.kokorin.jaffree.ffmpeg.UrlOutput;
import com.github.kokorin.jaffree.ffprobe.FFprobe;
import com.github.kokorin.jaffree.ffprobe.FFprobeResult;
import com.github.kokorin.jaffree.ffprobe.Stream;

import nl.surf.emergo.business.IBlobManager;
import nl.surf.emergo.business.IFileManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.domain.IEBlob;
import nl.surf.emergo.utilities.PropsValues;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpring;

public class CRunWebcamRecordingLocal extends CDefDiv {

	private static final long serialVersionUID = 2230955235386819296L;

	protected VView vView = CDesktopComponents.vView();
	protected SSpring sSpring = CDesktopComponents.sSpring();

	protected String _recordingBlobId = "";

	protected int _webcamRecordingLength = -1;

	protected String _classPrefix = "practiceNewRecording";
	protected String _idPrefix = "webcamRecording";
	protected String _logPrefix = "iSpot video recording: ";

	protected String locWebcamRecordVideoCodec;
	protected String locWebcamRecordAudioCodec;
	protected Boolean locConvertRecording;
	protected String locWebcamRecordVideoLibrary;
	protected String locWebcamRecordAudioLibrary;
	protected String locWebcamRecordVideoBitrate;
	protected String locWebcamRecordAudioBitrate;
	protected Integer locWebcamRecordMaxBitrate;
	protected Boolean locWebcamCheckSync;
	protected Integer locWebcamSyncDiff;
	protected Boolean locWebcamCheckRotation;

	protected int _recordTimeInSecs;

	protected ProgressListener _ProgListener;
	protected int _ProgressPercent;
	protected boolean _StopPWProgressTimer;
	protected boolean _PWProgressTimerFinished;

	protected String _FFOutUrl;
	protected FFprobe _FFProbe;
	protected FFmpegResultFuture _FFmpegResult;
	protected String _ConvertedRecordingName;
	protected boolean _IsWebcamRecording;
	protected String _ShowRecordingName;
	protected CRunWebcamDefLabel _PWProgLabel;
	protected CRunWebcamDefLabel _PWLoadLabel;

	protected String _progressState = "";
	protected String _progressStateUpload = "upload";
	protected String _progressStateConvert = "convert";
	protected int _progressTimeInSecs = 0;
	protected int _progressUploadLength = 0;

	protected static final String recordProgressTimerId = "RecordProgressTimer";
	protected static final String PWProgressTimerId = "PWProgressTimer";
	protected static final String webcamRecordFormat = "video/webm";
	protected static final String webcamRecordFileExtension = "webm";
	protected String locWebcamRecordFormat;
	protected String locWebcamRecordFileExtension;

	protected CRunWebcamRecordingCamera camera;

	public void init() {

		_recordingBlobId = "";
		_webcamRecordingLength = -1;

		update();
	}

	public void onCreate() {
		// NOTE use echoEvent, otherwise macro parent does not yet exist, if code is
		// used within zscript
		Events.echoEvent("onCreated", this, null);
	}

	public void onCreated() {
		init();
	}

	public void update() {
		getChildren().clear();

		Div div = new CRunWebcamDefDiv(this, new String[] { "class" }, new Object[] { _classPrefix + "Div" });

		addWebcamDiv(div);

		addPleaseWaitDiv(div);
	}

	public void addWebcamDiv(Component parent) {
		camera = new CRunWebcamRecordingCamera();
		parent.appendChild(camera);
		camera.setId(_idPrefix + "CameraCamera");
		camera.setClass(_classPrefix + "CameraCamera");
		camera.init(_idPrefix);
		_recordTimeInSecs = 0;
		Timer lRecordTimer = new Timer();
		parent.appendChild(lRecordTimer);
		lRecordTimer.setId(_idPrefix + recordProgressTimerId);
		lRecordTimer.setDelay(1000);
		lRecordTimer.setRepeats(true);
		lRecordTimer.setRunning(false);
		addRecordTimerOnTimerEventListener(lRecordTimer);
		Events.echoEvent("onCameraReady", this, null);
	}

	protected void addRecordTimerOnTimerEventListener(Component component) {
		component.addEventListener("onTimer", new EventListener<Event>() {
			@Override
			public void onEvent(Event event) {
				_recordTimeInSecs++;
			}
		});
	}

	public void onCameraReady() {
		if (camera != null) {
			Clients.evalJavaScript("recordingStarted();");
			camera.start();
		}
	}

	public String getWebcamRecordFormat() {
		if (locWebcamRecordFormat != null)
			return locWebcamRecordFormat;
		String lFormat = PropsValues.LOCAL_RECORDER_FORMAT;
		if (!lFormat.isEmpty())
			locWebcamRecordFormat = lFormat;
		else
			locWebcamRecordFormat = webcamRecordFormat;
		return locWebcamRecordFormat;
	}

	public String getWebcamRecordFileExtension() {
		if (locWebcamRecordFileExtension != null)
			return locWebcamRecordFileExtension;
		String lFExt = PropsValues.LOCAL_RECORDER_FILEEXTENSION;
		if (!lFExt.isEmpty())
			locWebcamRecordFileExtension = lFExt;
		else
			locWebcamRecordFileExtension = webcamRecordFileExtension;
		return locWebcamRecordFileExtension;
	}

	public void addPleaseWaitDiv(Component parent) {
		Div divPleaseWait = new CRunWebcamDefDiv(parent, new String[] { "id", "visible", "style" },
				new Object[] { _idPrefix + "PleaseWaitDiv", "false", "position:relative;top:200px;" });

		new CRunWebcamDefLabel(divPleaseWait, new String[] { "class", "labelKey" },
				new Object[] { "font " + _classPrefix + "PleaseWaitLabel", "run_ispot.webcam.label.pleaseWait" });

		_PWLoadLabel = new CRunWebcamDefLabel(divPleaseWait, new String[] { "class", "value" }, new Object[] {
				"font " + _classPrefix + "PleaseWaitProgressLabel",
				CDesktopComponents.vView().getLabel("run_ispot.webcam.label.pleaseWait.load").replace("%1", "0") });

		_PWProgLabel = new CRunWebcamDefLabel(divPleaseWait, new String[] { "class", "value" }, new Object[] {
				"font " + _classPrefix + "PleaseWaitProgressLabel",
				CDesktopComponents.vView().getLabel("run_ispot.webcam.label.pleaseWait.progress").replace("%1", "0") });
		_PWProgLabel.setVisible(false);

		Div lPWProgressDiv = new CRunWebcamDefDiv(divPleaseWait, new String[] { "id", "class", "visible" },
				new Object[] { _idPrefix + "PWProgressDiv", _classPrefix + "PWProgressDiv", "true" });

		Hbox lPWProgressHbox = new CRunWebcamDefHbox(lPWProgressDiv, null, null);

		Timer lPWTimer = new Timer();
		lPWProgressHbox.appendChild(lPWTimer);
		lPWTimer.setId(_idPrefix + PWProgressTimerId);
		lPWTimer.setDelay(1000);
		lPWTimer.setRepeats(true);
		lPWTimer.setRunning(false);
		addPWTimerOnTimerEventListener(lPWTimer);
		// lPWTimer.start();

		// Progressmeter lPWProgressMeter = new CRunPVToolkitDefProgressmeter();
		Progressmeter lPWProgressMeter = new Progressmeter();
		lPWProgressHbox.appendChild(lPWProgressMeter);
		lPWProgressMeter.setId(_idPrefix + "PWProgressMeter");
		lPWProgressMeter.setValue(0);
		lPWProgressMeter.setWidth("150px");

	}

	protected void addPWTimerOnTimerEventListener(Component component) {
		component.addEventListener("onTimer", new EventListener<Event>() {
			@Override
			public void onEvent(Event event) {
				Div lPD = (Div) CDesktopComponents.vView().getComponent(_idPrefix + "PWProgressDiv");
				if (lPD != null) {
					Progressmeter lPM = ((Progressmeter) CDesktopComponents.vView()
							.getComponent(_idPrefix + "PWProgressMeter"));
					if (lPM != null) {
						if (getProgressState().equals(_progressStateUpload)) {
							_progressTimeInSecs++;
							// NOTE: Rough indication of the required upload time, proportional to the
							// duration of the recording.
							// In practice, depending on the internet connection speed from client to
							// server.
							_ProgressPercent = Math.min(100,
									(int) Math.round(100. * _progressTimeInSecs / _progressUploadLength));
							_PWLoadLabel.setValue(
									CDesktopComponents.vView().getLabel("run_ispot.webcam.label.pleaseWait.load")
											.replace("%1", "" + _ProgressPercent));
						} else {
							if (getProgressState().equals(_progressStateConvert)) {
								_PWProgLabel.setValue(CDesktopComponents.vView()
										.getLabel("run_ispot.webcam.label.pleaseWait.progress")
										.replace("%1", "" + _ProgressPercent));
							}
						}
						lPM.setValue(_ProgressPercent);
					}
				}
				if (getProgressState().equals(_progressStateConvert)) {
					checkConvertRecordingProgress();
				}
			}
		});
	}

	protected void setProgressState(String aState) {
		_progressState = aState;
		_ProgressPercent = 0;
		if (aState.equals(_progressStateUpload)) {
			_PWProgLabel.setVisible(false);
			_PWLoadLabel.setValue(
					CDesktopComponents.vView().getLabel("run_ispot.webcam.label.pleaseWait.load").replace("%1", "0"));
			_PWLoadLabel.setVisible(true);
		}
		if (aState.equals(_progressStateConvert)) {
			_PWLoadLabel.setVisible(false);
			_PWProgLabel.setValue(CDesktopComponents.vView().getLabel("run_ispot.webcam.label.pleaseWait.progress")
					.replace("%1", "0"));
			_PWProgLabel.setVisible(true);
		}
		((Progressmeter) CDesktopComponents.vView().getComponent(_idPrefix + "PWProgressMeter")).setValue(0);
	}

	protected String getProgressState() {
		return _progressState;
	}

	public void cameraStarted() {
		_recordTimeInSecs = 0;
		((Timer) vView.getComponent(_idPrefix + recordProgressTimerId)).start();
	}

	public void cameraPaused() {
		((Timer) vView.getComponent(_idPrefix + recordProgressTimerId)).stop();
	}

	public void cameraResumed() {
		((Timer) vView.getComponent(_idPrefix + recordProgressTimerId)).start();
	}

	public void cameraStopped() {
		camera.setVisible(false);
		((Timer) vView.getComponent(_idPrefix + recordProgressTimerId)).stop();
		vView.getComponent(_idPrefix + "PleaseWaitDiv").setVisible(true);
		_progressUploadLength = _recordTimeInSecs;
		_progressTimeInSecs = 0;
		vView.getComponent(_idPrefix + "PWProgressDiv").setVisible(true);
		setProgressState(_progressStateUpload);
		((Timer) vView.getComponent(_idPrefix + PWProgressTimerId)).start();
	}

	public void again() {
		// NOTE remove possibly stored blob
		if (!StringUtils.isEmpty(_recordingBlobId)) {
			_recordingBlobId = "";
			_webcamRecordingLength = -1;
		}

		_ProgressPercent = 0;
		Div lPD = (Div) CDesktopComponents.vView().getComponent(_idPrefix + "PWProgressDiv");
		if (lPD != null) {
			((Progressmeter) CDesktopComponents.vView().getComponent(_idPrefix + "PWProgressMeter"))
					.setValue(_ProgressPercent);
			_PWProgLabel.setValue(CDesktopComponents.vView().getLabel("run_ispot.webcam.label.pleaseWait.progress")
					.replace("%1", "" + _ProgressPercent));
		}

		update();
	}

	public String storeVideoAsBlob(Media media) {

		vView.getComponent(_idPrefix + "PleaseWaitDiv").setVisible(false);
		((Timer) vView.getComponent(_idPrefix + PWProgressTimerId)).stop();

		IEBlob blob = setBlobMedia(media, AppConstants.blobtypeIntUrl);
		if (blob == null)
			return "";
		_recordingBlobId = "" + blob.getBloId();

		String url = sSpring.getSBlobHelper().getUrl("" + blob.getBloId(), AppConstants.blobtypeIntUrl);
		String convertedUrl = "";
		if (checkConvertRecording()) {
			sSpring.getSLogHelper().logAction(_logPrefix + "webcam/upload: start of conversion; file: " + url);
			_StopPWProgressTimer = false;
			_PWProgressTimerFinished = false;
			vView.getComponent(_idPrefix + "PleaseWaitDiv").setVisible(true);
			setProgressState(_progressStateConvert);
			((Timer) vView.getComponent(_idPrefix + PWProgressTimerId)).start();
			convertedUrl = convertRecordingIfNecessary(url);
			if (!convertedUrl.equals("")) {
				_StopPWProgressTimer = true;
			}
		} else
			sSpring.getSLogHelper().logAction(_logPrefix + "webcam/upload: no conversion configured; file: " + url);

		CDesktopComponents.cControl().setRunSessAttr("iSpotRecordingBlob", blob);

		if (!convertedUrl.equals("")) {
			Clients.evalJavaScript("eventToServer('onRecordComplete','webcam," + url + "');");
		}
		return blob.getUrl();
	}

	protected IEBlob setBlobMedia(Media aMedia, String aBlobtype) {
		boolean mediaOk = !(aMedia == null || aMedia.getName() == null || aMedia.getContentType() == null
				|| aMedia.getFormat() == null);
		if (!mediaOk)
			return null;
		IBlobManager lBean = (IBlobManager) sSpring.getBean("blobManager");
		IEBlob lBlob = lBean.getNewBlob();
		lBlob.setName("");
		lBlob.setFilename("");
		lBlob.setUrl("");
		lBlob.setContenttype("");
		lBlob.setFormat("");
		byte[] lContent = null;
		lContent = sSpring.getSBlobHelper().mediaToByteArray(aMedia);
		lBean.newFileBlob(lBlob, lContent);
		// NOTE put blob id in file name to make file unique. Files are cached, so if
		// same name is used, an old recording is shown instead of the new one
		String aName = (String) CDesktopComponents.cControl().getRunSessAttr("iSpotRecordingName") + "_bloid_"
				+ lBlob.getBloId() + "." + getWebcamRecordFileExtension();
		lBlob.setName(aName);
		lBlob.setFilename("");
		lBlob.setUrl("");
		if (aBlobtype.equals(AppConstants.blobtypeDatabase)) {
			lBlob.setFilename(aName);
		} else {
			lBlob.setUrl(aName);
		}
		lBlob.setContenttype(aMedia.getContentType());
		lBlob.setFormat(aMedia.getFormat());
		lBean.createFile(lBlob, lContent);
		lBean.updateBlob(lBlob);
		return lBlob;
	}

	protected String convertRecordingIfNecessary(String aRecordingName) {
		String lConvertVideoCodec = getWebcamRecordVideoCodec();
		String lConvertAudioCodec = getWebcamRecordAudioCodec();
		String lConvertVideoLibrary = getWebcamRecordVideoLibrary();
		String lConvertAudioLibrary = getWebcamRecordAudioLibrary();
		String lConvertVideoBitrate = getWebcamRecordVideoBitrate();
		String lConvertAudioBitrate = getWebcamRecordAudioBitrate();

		if (lConvertVideoCodec.isEmpty() || lConvertAudioCodec.isEmpty() || lConvertVideoLibrary.isEmpty()
				|| lConvertAudioLibrary.isEmpty() || lConvertVideoBitrate.isEmpty() || lConvertAudioBitrate.isEmpty()) {
			sSpring.getSLogHelper()
			.logAction(_logPrefix + "conversion of " + aRecordingName + " aborted: configuration missing: "
					+ lConvertVideoCodec + ":" + lConvertAudioCodec + ":" + lConvertVideoLibrary + ":"
					+ lConvertAudioLibrary + ":" + lConvertVideoBitrate + ":" + lConvertAudioBitrate);
			return aRecordingName;
		}

		if (StringUtils.isEmpty(aRecordingName)) {
			sSpring.getSLogHelper().logAction(_logPrefix + "conversion error: filename empty!");
			_ConvertedRecordingName = "";
			_StopPWProgressTimer = true;
			return "";
		}

		String lFFMpegDir = sSpring.getAppManager().getFFMPegPath();
		if (StringUtils.isEmpty(lFFMpegDir)) {
			sSpring.getSLogHelper().logAction(_logPrefix + "conversion error: ffmpeg path not valid!");
			_ConvertedRecordingName = "";
			_StopPWProgressTimer = true;
			return aRecordingName;
		}

		Path lFFMpegPath = Paths.get(lFFMpegDir);
		_FFProbe = FFprobe.atPath(lFFMpegPath);
		String lFFUrl = sSpring.getAppManager().getAbsoluteAppPath() + aRecordingName.substring(1);

		FFprobeResult lFFPResult = _FFProbe.setShowStreams(true).setInput(lFFUrl).execute();

		Float lAudioDuration = null;
		Float lVideoDuration = null;
		Integer lAudioBitrate = null;
		Integer lVideoBitrate = null;
		String lAudioCodec = "";
		String lVideoCodec = "";
		String lVideoRotation = "";
		boolean lCheckRotation = checkWebcamRotation();
		for (Stream stream : lFFPResult.getStreams()) {
			String lCodecName = stream.getCodecName();
			Float lStreamDuration = getStreamDuration(stream);
			Integer lStreamBitrate = stream.getBitRate();
			String lSpecInfo = "";
			if (stream.getCodecType().compareTo(StreamType.VIDEO) == 0) {
				if (lCodecName != null)
					lVideoCodec = lCodecName;
				lVideoDuration = lStreamDuration;
				lVideoBitrate = lStreamBitrate;
				lSpecInfo = " aspectratio: " + stream.getDisplayAspectRatio();
				if (lCheckRotation) {
					lVideoRotation = getVideoStreamRotation(stream);
					lSpecInfo = lSpecInfo + " rotation: " + lVideoRotation;
				}
			}
			if (stream.getCodecType().compareTo(StreamType.AUDIO) == 0) {
				if (lCodecName != null)
					lAudioCodec = lCodecName;
				lAudioDuration = lStreamDuration;
				lAudioBitrate = lStreamBitrate;
			}
			sSpring.getSLogHelper()
					.logAction("webcam recording original file: " + lFFUrl + ": Stream #" + stream.getIndex()
							+ " type: " + stream.getCodecType() + " codec: " + lCodecName + lSpecInfo + " duration: "
							+ lStreamDuration + " bitrate: " + lStreamBitrate);
		}

		final AtomicLong lAtomDuration = new AtomicLong();
		if (lVideoDuration == null) {
			// alternative way to calculate duration
			FFmpeg.atPath(lFFMpegPath).addInput(UrlInput.fromUrl(lFFUrl)).setOverwriteOutput(true)
					.addOutput(new NullOutput()).setProgressListener(new ProgressListener() {
						@Override
						public void onProgress(FFmpegProgress progress) {
							lAtomDuration.set(progress.getTimeMillis());
						}
					}).execute();
			// lVideoDuration = lAtomDuration.floatValue();
		} else
			lAtomDuration.set(Math.round(lVideoDuration));

		// if (lAudioDuration == null)
		// lAudioDuration = lVideoDuration;
		if ((lAtomDuration == null) || (lAtomDuration.floatValue() < 10))
			lAtomDuration.set(10);

		if (lVideoBitrate == null) {
			// try to estimate overall bit rate depending on file size and duration
			IFileManager lFM = (IFileManager) CDesktopComponents.sSpring().getBean("fileManager");
			lVideoBitrate = Math.round(lFM.getFile(lFFUrl).length() * 8000 / lAtomDuration.floatValue());
		}

		boolean lCheckSync = checkWebcamSync();
		int lSyncDiff = getWebcamSyncDiff();

		if ((lAudioDuration != null) && (lVideoDuration != null)
				&& (!lCheckSync || (Math.abs(lVideoDuration - lAudioDuration) < lSyncDiff))
				&& lAudioCodec.equalsIgnoreCase(lConvertAudioCodec) && lVideoCodec.equalsIgnoreCase(lConvertVideoCodec)
				&& (lVideoBitrate != null) && (lVideoBitrate < getWebcamMaxBitrate())) {
			if (_webcamRecordingLength == -1)
				// NOTE duration in milliseconds
				_webcamRecordingLength = Math.round(lVideoDuration / 1000);
			return aRecordingName;
		}

		_ConvertedRecordingName = aRecordingName.substring(0, aRecordingName.lastIndexOf(".") + 1)
				+ getWebcamRecordFileExtension();
		if (_ConvertedRecordingName.equals(aRecordingName))
			_ConvertedRecordingName = aRecordingName.substring(0, aRecordingName.lastIndexOf(".")) + "_conv."
					+ getWebcamRecordFileExtension();

		_FFOutUrl = sSpring.getAppManager().getAbsoluteAppPath() + _ConvertedRecordingName.substring(1);

		FFmpeg lFFMPeg = FFmpeg.atPath(lFFMpegPath);
		lFFMPeg.addInput(UrlInput.fromUrl(lFFUrl));

		if (lCheckSync) {
			if ((lAudioDuration != null) && (lVideoDuration != null)
					&& !(Math.abs(lVideoDuration - lAudioDuration) < lSyncDiff)) {
				int lOutOfSync = Math.round(lAudioDuration - lVideoDuration);
				// TODO account for first frame duration?
				// NOTE take audio from first input, and video from second input; delay second
				// input (video) by lOutOfSync ms.
				// If negative, first lOutOfSync ms wil be cut. Don't delay audio, because
				// 'source duration' will be stored in audio stream, and this value
				// seems to overrule the generated audio duration
				lFFMPeg.addInput(UrlInput.fromUrl(lFFUrl).addArguments("-itsoffset", lOutOfSync + "ms"))
						.addArguments("-map", "0:1").addArguments("-map", "1:0");
			}
		}

		// NOTE video area is 1080x440
		String lFilterString = "scale=w=1080:h=440:force_original_aspect_ratio=decrease";
		if (lCheckRotation && !lVideoRotation.isEmpty()) {
			String lRotFilter = getRotationFilter(lVideoRotation);
			if (!lRotFilter.isEmpty())
				lFilterString = lFilterString + "," + lRotFilter;
		}

		Div lProgDiv = (Div) CDesktopComponents.vView().getComponent(_idPrefix + "PWProgressDiv");
		Progressmeter lProgMeter = ((Progressmeter) CDesktopComponents.vView()
				.getComponent(_idPrefix + "PWProgressMeter"));
		if (lProgDiv != null) {

			lProgMeter.setValue(0);
		}

		addProgressEventListener(lAtomDuration);

		_FFmpegResult = lFFMPeg.addArguments("-b:v", lConvertVideoBitrate).addArguments("-b:a", lConvertAudioBitrate)
				.addArguments("-movflags", "faststart").addArguments("-map_metadata", "-1").setOverwriteOutput(true)
				.setFilter(StreamType.VIDEO, lFilterString)
				.addOutput(UrlOutput.toUrl(_FFOutUrl).setCodec(StreamType.VIDEO, lConvertVideoLibrary)
						.setCodec(StreamType.AUDIO, lConvertAudioLibrary).setFormat(getWebcamRecordFileExtension()))
				.setProgressListener(_ProgListener).executeAsync();

		return "";
	}

	protected void addProgressEventListener(AtomicLong pAtomDuration) {
		_ProgListener = new ProgressListener() {
			@Override
			public void onProgress(FFmpegProgress aProgress) {
				_ProgressPercent = Math.min(100,
						(int) Math.round(100. * aProgress.getTimeMillis() / pAtomDuration.get()));
			}
		};
	}

	protected void checkConvertRecordingProgress() {
		if (_StopPWProgressTimer) {
			if (!_PWProgressTimerFinished) {
				_PWProgressTimerFinished = true;
				((Timer) vView.getComponent(_idPrefix + PWProgressTimerId)).stop();
				recordingReadyToShow(_ConvertedRecordingName);
			}
		} else {
			if (_FFmpegResult.isDone()) {
				// NOTE determine duration of streams in converted file
				FFprobeResult lFFPResult = _FFProbe.setShowStreams(true).setInput(_FFOutUrl).execute();

				Float lAudioDuration = null;
				Float lVideoDuration = null;
				for (Stream stream : lFFPResult.getStreams()) {
					if (stream.getCodecType().compareTo(StreamType.VIDEO) == 0)
						lVideoDuration = getStreamDuration(stream);
					else
						lAudioDuration = getStreamDuration(stream);
				}
				if ((_webcamRecordingLength == -1) && (lVideoDuration != null))
					_webcamRecordingLength = Math.round(lVideoDuration / 1000);

				sSpring.getSLogHelper().logAction("webcam recording converted file: " + _FFOutUrl + ": video duration: "
						+ lVideoDuration + " ms" + "; audioduration: " + lAudioDuration + " ms");
				_StopPWProgressTimer = true;
			}
		}
	}

	protected void recordingReadyToShow(String pRecordingName) {
		String url = sSpring.getSBlobHelper().getUrl(_recordingBlobId, AppConstants.blobtypeIntUrl);
		if (!url.equals(pRecordingName)) {
			// NOTE adapt reference in blob

			url = pRecordingName.replace(VView.getInitParameter("emergo.streaming.path"), "");
			IBlobManager lBM = (IBlobManager) sSpring.getBean("blobManager");
			IEBlob lBlob = lBM.getBlob(Integer.parseInt(_recordingBlobId));
			lBlob.setUrl(url);
			lBM.saveBlob(lBlob);

			// NOTE add new file to content files filter access:
			url = sSpring.getSBlobHelper().getUrl(_recordingBlobId, AppConstants.blobtypeIntUrl);
		}

		if (!_IsWebcamRecording) {
			// TODO if webcam recording also upload migrate video to Wowza server
			// pvToolkit.copyVideoBlobToWowza(_recordingBlobId, lFileName);
		}

		Clients.evalJavaScript("eventToServer('onRecordComplete','webcam," + url + "');");
	}

	// NOTE return duration in ms
	protected Float getStreamDuration(Stream aStream) {
		Float lDur = aStream.getDuration();
		if (lDur == null) {
			String lDurStr = aStream.getTag("DURATION");
			if (!((lDurStr == null) || (lDurStr.isEmpty()))) {
				try {
					long lNanoDur = LocalTime.parse(lDurStr, DateTimeFormatter.ofPattern("HH:mm:ss.nnnnnnnnn"))
							.getLong(ChronoField.NANO_OF_DAY);
					lDur = (float) (lNanoDur / 1000000);
				} catch (DateTimeParseException e) {

				}
			}
		} else
			// NOTE stream duration in seconds!
			lDur = lDur * 1000;
		return lDur;
	}

	protected String getVideoStreamRotation(Stream aStream) {
		String lRot = aStream.getTag("rotate");
		if (lRot == null) {
			return "";
		}
		return lRot;
	}

	protected String getRotationFilter(String aRotation) {
		String lFilter = "";
		switch (aRotation) {
		case "90":
			lFilter = "transpose=1";
			break;
		case "180":
			lFilter = "transpose=2,transpose=2";
			break;
		case "270":
			lFilter = "transpose=2";
			break;
		default:
			break;
		}
		return lFilter;
	}

	protected String getWebcamRecordVideoCodec() {
		if (locWebcamRecordVideoCodec == null)
			locWebcamRecordVideoCodec = PropsValues.LOCAL_RECORDER_VIDEO_CODEC;
		return locWebcamRecordVideoCodec;
	}

	protected String getWebcamRecordAudioCodec() {
		if (locWebcamRecordAudioCodec == null)
			locWebcamRecordAudioCodec = PropsValues.LOCAL_RECORDER_AUDIO_CODEC;
		return locWebcamRecordAudioCodec;
	}

	protected boolean checkConvertRecording() {
		if (locConvertRecording == null)
			locConvertRecording = PropsValues.LOCAL_RECORDER_CONVERT;
		return locConvertRecording;
	}

	protected String getWebcamRecordVideoLibrary() {
		if (locWebcamRecordVideoLibrary == null)
			locWebcamRecordVideoLibrary = PropsValues.LOCAL_RECORDER_VIDEO_LIBRARY;
		return locWebcamRecordVideoLibrary;
	}

	protected String getWebcamRecordAudioLibrary() {
		if (locWebcamRecordAudioLibrary == null)
			locWebcamRecordAudioLibrary = PropsValues.LOCAL_RECORDER_AUDIO_LIBRARY;
		return locWebcamRecordAudioLibrary;
	}

	protected String getWebcamRecordVideoBitrate() {
		if (locWebcamRecordVideoBitrate == null)
			locWebcamRecordVideoBitrate = PropsValues.LOCAL_RECORDER_VIDEO_BITRATE;
		return locWebcamRecordVideoBitrate;
	}

	protected String getWebcamRecordAudioBitrate() {
		if (locWebcamRecordAudioBitrate == null)
			locWebcamRecordAudioBitrate = PropsValues.LOCAL_RECORDER_AUDIO_BITRATE;
		return locWebcamRecordAudioBitrate;
	}

	protected int getWebcamMaxBitrate() {
		if (locWebcamRecordMaxBitrate == null) {
			try {
				locWebcamRecordMaxBitrate = PropsValues.LOCAL_RECORDER_MAX_BITRATE;
			} catch (NumberFormatException e) {
				// NOTE default assume 3 Mb/s as maximum bitrate for streaming
				locWebcamRecordMaxBitrate = 3000000;
			}
		}
		return locWebcamRecordMaxBitrate;
	}

	protected boolean checkWebcamSync() {
		if (locWebcamCheckSync == null)
			locWebcamCheckSync = PropsValues.LOCAL_RECORDER_SYNCHRONIZATION_CHECK;
		return locWebcamCheckSync;
	}

	protected int getWebcamSyncDiff() {
		if (locWebcamSyncDiff == null) {
			locWebcamSyncDiff = PropsValues.LOCAL_RECORDER_SYNCHRONIZATION_DIFFERENCE;
			// NOTE assume 20 ms as minimum difference for noticeable out-of-sync issues
			if (locWebcamSyncDiff < 20)
				locWebcamSyncDiff = 200;
		}
		return locWebcamSyncDiff;
	}

	protected boolean checkWebcamRotation() {
		if (locWebcamCheckRotation == null)
			locWebcamCheckRotation = PropsValues.LOCAL_RECORDER_ROTATION_CHECK;
		return locWebcamCheckRotation;
	}

}
