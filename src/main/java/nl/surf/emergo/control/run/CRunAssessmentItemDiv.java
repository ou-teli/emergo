/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Event;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;

public class CRunAssessmentItemDiv extends CDefDiv {

	private static final long serialVersionUID = 5721175627738291812L;

	protected CRunAssessmentsContentDiv root;

	protected String buttonStyleShift;
	
	protected String labelStyleShift;

	public void onInit(Event aEvent) {
		root = (CRunAssessmentsContentDiv)CDesktopComponents.vView().getComponent(CRunAssessmentsContentDiv.runAssessmentsZulId);
		if (root.multipleAssessments) {
			buttonStyleShift = "position:relative;left:10px;";
		}
		if (root.multipleAssessments) {
			if (root.multipleQuestions) {
				labelStyleShift += "position:relative;left:24px;top:0px;";
			}
			else {
				labelStyleShift += "position:relative;left:12px;top:0px;";
			}
		}
		else {
			if (root.multipleQuestions) {
				labelStyleShift += "position:relative;left:12px;top:0px;";
			}
		}
		//NOTE To be sure hide runAssessmentDiv, otherwise when restarting an assessment it still is shown
		root.setComponentVisible(CDesktopComponents.vView().getComponent(CRunAssessmentsContentDiv.runAssessmentDivId), false);

		HtmlMacroComponent macro = new HtmlMacroComponent();
		appendChild(macro);
		macro.setId(CRunAssessmentsContentDiv.macroAssessmentItemId);
		macro.setDynamicProperty("a_super_observer", CRunAssessmentsContentDiv.runAssessmentDivId);
		macro.setDynamicProperty("a_assessments_title", root.assessmentsTitle);
		macro.setDynamicProperty("a_observer", CRunAssessmentsContentDiv.runAssessmentItemDivId);
		macro.setDynamicProperty("a_assessment_title", root.assessmentTitle);
		macro.setDynamicProperty("a_assessmentitem_title", CRunAssessmentsContentDiv.assessmentItemTitle);
		macro.setDynamicProperty("a_assessmentitem_instruction", CRunAssessmentsContentDiv.assessmentItemInstruction);
		macro.setDynamicProperty("a_assessmentitem_pieces", root.assessmentItemPieces);
		macro.setDynamicProperty("a_assessmentitem_showfeedback", root.assessmentItemShowFeedback);
		macro.setDynamicProperty("a_assessment_feedback_instruction", root.assessmentFeedbackText);
		macro.setDynamicProperty("a_assessment_feedback_pieces", root.assessmentFeedbackPieces);
		macro.setDynamicProperty("a_assessmentitem_directfeedback", root.assessmentItemDirectFeedback);
		macro.setDynamicProperty("a_assessmentitem_type", root.assessmentItemType);
		macro.setDynamicProperty("a_assessmentitem_alt_orientation", root.assessmentItemAltOrientation);
		macro.setDynamicProperty("a_assessmentitems_editable", root.assessmentItemsEditable);
		macro.setDynamicProperty("a_current_assessmentitem_number", root.currentAssessmentItemNumber);
		macro.setDynamicProperty("a_number_of_assessmentitems", root.numberOfAssessmentItems);
		macro.setDynamicProperty("a_assessmentitem_datas", root.assessmentItemDatas);
		macro.setDynamicProperty("a_assessment_multiple_assessments", root.multipleAssessments);
		macro.setDynamicProperty("a_assessment_multiple_questions", root.multipleQuestions);
		macro.setDynamicProperty("a_button_style_shift", buttonStyleShift);
		macro.setDynamicProperty("a_label_style_shift", labelStyleShift);
		//NOTE always show 'finish assessment' button at last question, when editable:
		macro.setDynamicProperty("a_assessment_can_be_finished", root.assessmentItemsEditable);
		macro.setMacroURI("../run_assessmentitempanel_macro.zul");
	}

	public void onHandleAssessmentItemAlt(Event aEvent) {
		if (root.currentEmergoComponent != null) {
			String assessmentItemAltTagId = (String)((Object[])aEvent.getData())[0];
			boolean checked = (Boolean)((Object[])aEvent.getData())[1];
			root.currentEmergoComponent.handleAssessmentItemAlt(assessmentItemAltTagId, checked);
		}
	}

	public void onHandleAssessmentItemField(Event aEvent) {
		if (root.currentEmergoComponent != null) {
			String assessmentItemAltTagId = (String)((Object[])aEvent.getData())[0];
			String value = (String)((Object[])aEvent.getData())[1];
			root.currentEmergoComponent.handleAssessmentItemField(assessmentItemAltTagId, value);
		}
	}

	public void onHandleFeedback(Event aEvent) {
		if (root.currentEmergoComponent != null) {
			root.currentEmergoComponent.handleAssessmentItemFeedback();
		}
	}

	public void onPreviousItem(Event aEvent) {
		if (root.currentEmergoComponent != null) {
			root.currentEmergoComponent.handlePreviousAssessmentItem();
		}
		CDesktopComponents.vView().getComponent(CRunAssessmentsContentDiv.runAssessmentDivId).setVisible(false);
	}

	public void onNextItem(Event aEvent) {
		if (root.currentEmergoComponent != null) {
			root.currentEmergoComponent.handleNextAssessmentItem();
		}
		CDesktopComponents.vView().getComponent(CRunAssessmentsContentDiv.runAssessmentDivId).setVisible(false);
	}

	public void onUpdateAssessment(Event aEvent) {
		// update assessment div
		if (root.currentEmergoComponent != null) {
			root.currentEmergoComponent.updateAssessment();
		}
		setVisible(false);
		CDesktopComponents.vView().getComponent(CRunAssessmentsContentDiv.runAssessmentDivId).setVisible(true);
	}

	public void onFinishAssessment(Event aEvent) {
		if (root.currentEmergoComponent != null) {
			root.currentEmergoComponent.finishAssessment();
		}
		setVisible(false);
		CDesktopComponents.vView().getComponent(CRunAssessmentsContentDiv.runAssessmentDivId).setVisible(true);
	}

}
