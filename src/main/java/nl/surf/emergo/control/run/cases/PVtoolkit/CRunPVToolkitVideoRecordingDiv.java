/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.cases.PVtoolkit.util.CRunPVToolkitCustomAttributeHelper;

public class CRunPVToolkitVideoRecordingDiv extends CDefDiv {

	private static final long serialVersionUID = -8424720811120668279L;

	public String zulfilepath = ((CRunPVToolkitInitBox)CDesktopComponents.vView().getComponent("PV-toolkit_initBox")).getZulfilepath();
	
	protected CRunPVToolkit pvToolkit;
	
	protected int _cycleNumber;
	
	protected IXMLTag _practiceTag;
	
	//NOTE id prefix should be overwritten by descending classes
	protected String _idPrefix = "";
	protected String _classPrefix = "video";
	
	public void onCreate(CreateEvent aEvent) {
		_idPrefix = CRunPVToolkitCustomAttributeHelper.handleCustomAttribute(this, "_idPrefix", _idPrefix);
	}
	
	public void init(int cycleNumber, IXMLTag practiceTag) {
		setClass(_classPrefix + "Recording");

		pvToolkit = (CRunPVToolkit)CDesktopComponents.vView().getComponent("pvToolkit");

		update(cycleNumber, practiceTag);
	}
	
	public void update(int cycleNumber, IXMLTag practiceTag) {
		_cycleNumber = cycleNumber;
		_practiceTag = practiceTag;

		//NOTE put all elements in a specific div and if it exists remove it.
		//Other elements are defined in the ZUL file and may not be removed 
		Div specificDiv = pvToolkit.getSpecificDiv(this);

		new CRunPVToolkitDefLabel(specificDiv, 
				new String[]{"class", "value"}, 
				new Object[]{"font " + _classPrefix + "RecordingTitle", (String)getAttribute("divTitle")}
		);
		
		//show link
    	IXMLTag pieceTag = pvToolkit.getStatusChildTag(_practiceTag).getChild("piece");
    	pvToolkit.showStatusLink(pieceTag, specificDiv, "", "font gridLink " + _classPrefix + "RecordingFileLink", pvToolkit.getRubricLabelText("PV-toolkit-myRecordings.label.file"));
		
		Button btn = new CRunPVToolkitNavigationButton(specificDiv, 
				new String[]{"class", "cLabelKey"}, 
				new Object[]{"font pvtoolkitButton " + _classPrefix + "RecordingBackButton", "PV-toolkit.back"}
		);
		addBackButtonOnClickEventListener(btn);

		new CRunPVToolkitDefLabel(specificDiv, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font " + _classPrefix + "RecordingNameHeader", "PV-toolkit-myRecordings.column.label.name"}
		);
			
		new CRunPVToolkitDefLabel(specificDiv, 
				new String[]{"class", "value"}, 
				new Object[]{"font " + _classPrefix + "RecordingName", pvToolkit.getStatusChildTagAttribute(_practiceTag, "name")}
		);
			
		new CRunPVToolkitDefLabel(specificDiv, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font " + _classPrefix + "RecordingDateHeader", "PV-toolkit-myRecordings.column.label.date"}
		);
			
		new CRunPVToolkitDefLabel(specificDiv, 
				new String[]{"class", "value"}, 
				new Object[]{"font " + _classPrefix + "RecordingDate", pvToolkit.getStatusChildTagAttribute(_practiceTag, "sharedate")}
		);

		if ((_cycleNumber > 0) && (pvToolkit.getMaxCycleNumber() > 1)) {
			new CRunPVToolkitDefLabel(specificDiv, 
					new String[]{"class", "labelKey"}, 
					new Object[]{"font " + _classPrefix + "RecordingCycleHeader", "PV-toolkit-myRecordings.column.label.cycle"}
			);
			
			new CRunPVToolkitDefLabel(specificDiv, 
					new String[]{"class", "value"}, 
					new Object[]{"font " + _classPrefix + "RecordingCycle", "" + _cycleNumber}
			);
		}

		pvToolkit.showStatusVideo(_practiceTag, _idPrefix + "Recording", _classPrefix + "Recording");
		
	}

	protected void addBackButtonOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				pvToolkit.clearVideo(_idPrefix + "Recording");
				Component componentFrom = CDesktopComponents.vView().getComponent(_idPrefix + "RecordingDiv");
				if (componentFrom != null) {
					componentFrom.setVisible(false);
				}
				Component componentTo = CDesktopComponents.vView().getComponent(_idPrefix + "RecordingsDiv");
				if (componentTo != null) {
					componentTo.setVisible(true);
				}
			}
		});
	}

}
