/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.List;

import org.zkoss.zul.Label;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listhead;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefListbox;
import nl.surf.emergo.control.def.CDefListcell;
import nl.surf.emergo.control.def.CDefListhead;
import nl.surf.emergo.control.def.CDefListheader;
import nl.surf.emergo.control.def.CDefListitem;

/**
 * The Class CRunMailSubjectContactsListbox. Used to show all possible subject receiver combinations of a mail
 * within the new mail dialogue.
 */
public class CRunMailSubjectContactsListbox extends CDefListbox {

	private static final long serialVersionUID = 8713592935786638208L;

	public String getClassName() {
		return CDesktopComponents.vView().getRunClassName(className);
	}

	/**
	 * Instantiates a new c run mail subjects combo.
	 *
	 * @param aId the a id
	 */
	public CRunMailSubjectContactsListbox(String aId) {
		setId(aId);
		setCheckmark(true);
		setRows(5);
	}

	/**
	 * Shows all subject contact combinations.
	 *
	 * @param aTags the a tags
	 * @param aTagId the a tag id
	 * @param aContactsStrings the a contacts strings
	 */
	public void showItems(List<IXMLTag> aTags, List<String> aContactsStrings, String aTagId) {
		getChildren().clear();
		if (aTags == null)
			return;
		Listhead lListhead = new CDefListhead();
		appendChild(lListhead);
		Listheader lListheader = new CDefListheader(CDesktopComponents.vView().getLabel("run_new_mail.prompt.subject"));
		lListhead.appendChild(lListheader);
		lListheader.setZclass(getClassName() + "_listheader");
		if (CDesktopComponents.sSpring().isTutorRun())
			lListheader = new CDefListheader(CDesktopComponents.vView().getLabel("run_new_mail.prompt.sender"));
		else
			lListheader = new CDefListheader(CDesktopComponents.vView().getLabel("run_new_mail.prompt.contacts"));
		lListhead.appendChild(lListheader);
		lListheader.setZclass(getClassName() + "_listheader");
		int i = 0;
		for (IXMLTag lTag : aTags) {
			String lTagId = lTag.getAttribute(AppConstants.keyId);
			String lSubject = CDesktopComponents.sSpring().unescapeXML(lTag.getChildValue("title"));
			String lSubjectToShow = lSubject;
			if (lSubject.equals(""))
				lSubjectToShow = "...";
			String lContacts = "";
			if (i < aContactsStrings.size())
				lContacts = (String)aContactsStrings.get(i);
			if (!lContacts.equals("")) {
				Listitem lListitem = new CDefListitem();
				// set value of listitem to item
				lListitem.setValue(lTagId);
				lListitem.setAttribute("subject", lSubject);
				lListitem.setAttribute("contacts", lContacts);
				Listcell lListcell = new CDefListcell();
				Label lLabel = new Label(lSubjectToShow);
				lLabel.setZclass(getClassName() + "_listcell_label");
				lListcell.appendChild(lLabel);
				lListitem.appendChild(lListcell);
				lListcell = new CDefListcell();
				lLabel = new Label(lContacts);
				lLabel.setZclass(getClassName() + "_listcell_label");
				lListcell.appendChild(lLabel);
				lListitem.appendChild(lListcell);
				appendChild(lListitem);
				if (lTagId.equals(aTagId))
					setSelectedItem(lListitem);
			}
			i++;
		}
	}

	/**
	 * On select notify new mail dialogue.
	 */
	@Override
	public void onSelect() {
		Listitem lListitem = getSelectedItem();
		String lTagId = (String) lListitem.getValue();
		String lSubject = (String) lListitem.getAttribute("subject");
		String lContacts = (String) lListitem.getAttribute("contacts");
		CRunNewMail runNewMail = (CRunNewMail) CDesktopComponents.vView().getComponent("runNewMail");
		if (runNewMail != null)
			runNewMail.subjectContactsSelected(lTagId, lSubject, lContacts);
	}
}
