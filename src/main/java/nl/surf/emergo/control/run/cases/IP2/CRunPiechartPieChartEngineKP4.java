package nl.surf.emergo.control.run.cases.IP2;

import java.awt.Color;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.Stroke;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.DefaultDrawingSupplier;
import org.jfree.chart.plot.PiePlot;
import org.zkoss.zkex.zul.impl.JFreeChartEngine;
import org.zkoss.zul.Chart;

/*
 * you are able to do many advanced chart customization by extending ChartEngine
 */
public class CRunPiechartPieChartEngineKP4 extends JFreeChartEngine {
	
	private static final long serialVersionUID = -5422881292887271154L;
	
	private boolean explode = false;
	
	public boolean prepareJFreeChart(JFreeChart jfchart, Chart chart) {
		jfchart.setBackgroundPaint(Color.white);

		PiePlot piePlot = (PiePlot) jfchart.getPlot();
		piePlot.setLabelBackgroundPaint(CRunPiechartChartColorsKP4.COLOR_7);

		//override some default colors
		Paint[] colors = new Paint[]
				{
				CRunPiechartChartColorsKP4.COLOR_1, 
				CRunPiechartChartColorsKP4.COLOR_2, 
				CRunPiechartChartColorsKP4.COLOR_3, 
				CRunPiechartChartColorsKP4.COLOR_4,
				CRunPiechartChartColorsKP4.COLOR_5,
				CRunPiechartChartColorsKP4.COLOR_6
				};
		
		DefaultDrawingSupplier defaults = new DefaultDrawingSupplier();
		piePlot.setDrawingSupplier(new DefaultDrawingSupplier(colors, new Paint[]{defaults.getNextFillPaint()}, new Paint[]{defaults.getNextOutlinePaint()}, 
				new Stroke[]{defaults.getNextStroke()}, new Stroke[] {defaults.getNextOutlineStroke()}, new Shape[] {defaults.getNextShape()}));
		
		piePlot.setShadowPaint(null);

		piePlot.setSectionOutlinesVisible(false);

		piePlot.setExplodePercent("Java", explode ? 0.2 : 0);

		return false;
	}

	public void setExplode(boolean explode) {
		this.explode = explode;
	}
}
