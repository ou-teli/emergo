/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Div;
import org.zkoss.zul.Html;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Treeitem;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CContentHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CIObserved;
import nl.surf.emergo.control.CSimpleCombo;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.def.CDefFCKeditor;
import nl.surf.emergo.control.def.CDefHtml;
import nl.surf.emergo.control.def.CDefLabel;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SStatustagEnrichment;

/**
 * The Class CRunCanon is used to show a canon within the run view area of the emergo player.
 */
public class CRunCanon extends CRunComponent {

	private static final long serialVersionUID = 2559316793237767891L;
	/** The selected piece. */

	protected IXMLTag piece = null;

	/**
	 * Instantiates a new c run canon.
	 */
	public CRunCanon() {
		super("runCanon", null, false);
		createComponents();
	}

	/**
	 * Instantiates a new c run canon.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the canon case component
	 */
	public CRunCanon(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent, false);
		createComponents();
	}

	/**
	 * Creates title area, content area to show canon tree, item area to show
	 * selected canon piece and buttons area with close button.
	 */
	@Override
	protected void createComponents() {
		CRunVbox lVbox = new CRunVbox();
		appendChild(lVbox);

		createTitleArea(lVbox);
		CRunHbox lHbox = new CRunHbox();
		lVbox.appendChild(lHbox);
		createContentArea(lHbox);
		CRunArea lItemArea = createItemArea(lHbox);
		lItemArea.setId(getId()+"ItemArea");
		createButtonsArea(lVbox);
	}

	/**
	 * Creates new content component, the canon tree.
	 *
	 * @return the component
	 */
	@Override
	protected Component newContentComponent() {
		return (new CRunCanonTree("runCanonTree", caseComponent, this));
	}

	/**
	 * Creates buttons area and adds close button.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run hbox
	 */
	@Override
	protected CRunHbox createButtonsArea(Component aParent) {
		CRunHbox lButtonsHbox = super.createButtonsArea(aParent);
		createCloseButton(lButtonsHbox);
		return lButtonsHbox;
	}

	/**
	 * Does contentitem action, clicking on a canon item.
	 * Shows item within item area.
	 *
	 * @param aContentItem the a contentitem, the canon item clicked
	 */
	@Override
	public void doContentItemAction(Component aContentItem) {
		Treeitem lTreeitem = (Treeitem)aContentItem;
		IXMLTag tag = (IXMLTag)lTreeitem.getAttribute("item");
		if ((tag == null) || (!tag.getName().equals("piece")))
			return;
		piece = tag;
		CRunArea lItemArea = (CRunArea)CDesktopComponents.vView().getComponent(getId()+"ItemArea");
		if (lItemArea != null)
			createItem(lItemArea,tag);
	}

	/**
	 * Shows item within item area. First removes previous item.
	 * If current run group account is not owner rating input elements are shown.
	 *
	 * @param aParent the ZK parent
	 * @param aItem the piece within the canon case component
	 *
	 * @return the c run vbox
	 */
	protected CRunVbox createItem(Component aParent,IXMLTag aItem) {
		removeItem(aParent);
		String lOwnerRugId = aItem.getAttribute(AppConstants.keyOwner_rgaid);
		boolean lOwner = (lOwnerRugId.equals(""+CDesktopComponents.sSpring().getRunGroupAccount().getRgaId()));
		CRunArea lArea = new CRunArea();
		aParent.appendChild(lArea);
		lArea.setZclass(getClassName()+"_item");
		Div lDiv = new CDefDiv();
		lDiv.setZclass(getClassName() + "_item_div");
		lArea.appendChild(lDiv);
		CRunVbox lVbox = new CRunVbox();
		lDiv.appendChild(lVbox);

//		append link
		createLink(lVbox,aItem);
//		append picture
		createPicture(lVbox,aItem);
//		append motivation
		createMotivation(lVbox,aItem);
//		if not owner append rating combobox and rating motivation field
		if (!lOwner) {
			createBreak(lVbox);
			createRating(lVbox,aItem);
			createRatingMotivation(lVbox,aItem);
			createBreak(lVbox);
			createRatingOkBtn(lVbox,aItem);
		}
		return lVbox;
	}

	/**
	 * Removes item from item area.
	 *
	 * @param aParent the a parent
	 */
	protected void removeItem(Component aParent) {
		if (aParent.getChildren() != null)
			aParent.getChildren().clear();
	}

	/**
	 * Shows link within item area.
	 *
	 * @param aParent the a parent
	 * @param aItem the a item
	 */
	protected void createLink(Component aParent,IXMLTag aItem) {
		Label lLabel = new CRunBlobLabel(aItem);
		aParent.appendChild(lLabel);
	}

	/**
	 * Shows picture within item area.
	 *
	 * @param aParent the a parent
	 * @param aItem the a item, the tag containing the picture tag as child
	 */
	protected void createPicture(Component aParent,IXMLTag aItem) {
		Image lImage = new CRunBlobImage(aItem);
		if (((lImage.getSrc() != null) && (!lImage.getSrc().equals(""))) || (lImage.getContent() != null)) {
			createBreak(aParent);
			aParent.appendChild(lImage);
			lImage.setWidth("150px");
			createBreak(aParent);
		}
	}

	/**
	 * Shows motivation within item area.
	 *
	 * @param aParent the a parent
	 * @param aItem the a item
	 */
	protected void createMotivation(Component aParent,IXMLTag aItem) {
		Html lHtml = new CDefHtml(CDesktopComponents.sSpring().unescapeXML(aItem.getChildValue("motivation")));
		aParent.appendChild(lHtml);
		lHtml.setZclass(getClassName() + "_item_html");
	}

	/**
	 * Creates rating combobox within item area and fills it with previous selection
	 * if applicable.
	 *
	 * @param aParent the a parent
	 * @param aItem the a item
	 */
	protected void createRating(Component aParent,IXMLTag aItem) {
		Label lLabel = new CDefLabel();
		aParent.appendChild(lLabel);
		lLabel.setValue(VView.getCapitalizeFirstChar(CContentHelper.getNodeTagLabel(caseComponent.getEComponent().getCode(), "rating")) + ":");
		String lRating = "";
		String lRgaId = ""+CDesktopComponents.sSpring().getRunGroupAccount().getRgaId();
		List<IXMLTag> lRgaChilds = aItem.getChild(AppConstants.statusElement).getChilds("rga");
		for (IXMLTag lRgaChild : lRgaChilds) {
			if (lRgaChild.getValue().equals(lRgaId)) {
				List<IXMLTag> lRatingChilds = lRgaChild.getChilds(AppConstants.statusKeyRating);
				if (lRatingChilds.size()>0)
					// get last rating
					lRating = ((IXMLTag)lRatingChilds.get(lRatingChilds.size()-1)).getValue();
			}
		}
		CSimpleCombo lCombo = new CSimpleCombo("runCanonRating");
		aParent.appendChild(lCombo);
		lCombo.setAttribute("changed", "false");
		String lRatings = "0,1,2,3,4,5,6,7,8,9,10";
		lCombo.showItems(lRatings, lRating);
	}

	/**
	 * Creates rating motivation edit field within item area and fills it with
	 * previous text if applicable.
	 *
	 * @param aParent the a parent
	 * @param aItem the a item
	 */
	protected void createRatingMotivation(Component aParent,IXMLTag aItem) {
		Label lLabel = new CDefLabel();
		aParent.appendChild(lLabel);
		lLabel.setValue(CDesktopComponents.vView().getCLabel(VView.childtagLabelKeyPrefix + "motivation") + ":");
		String lMotivation = "";
		String lRgaId = ""+CDesktopComponents.sSpring().getRunGroupAccount().getRgaId();
		List<IXMLTag> lRgaChilds = aItem.getChild(AppConstants.statusElement).getChilds("rga");
		for (IXMLTag lRgaChild : lRgaChilds) {
			if (lRgaChild.getValue().equals(lRgaId)) {
				List<IXMLTag> lRatingChilds = lRgaChild.getChilds("ratingmotivation");
				if (lRatingChilds.size()>0)
					// get last rating
					lMotivation = ((IXMLTag)lRatingChilds.get(lRatingChilds.size()-1)).getValue();
			}
		}
		CDefFCKeditor lTextbox = new CDefFCKeditor();
		aParent.appendChild(lTextbox);
		lTextbox.setId("runCanonRatingMotivation");
		lTextbox.setValue(CDesktopComponents.sSpring().unescapeXML(lMotivation));
		lTextbox.setAttribute("changed", "false");
		lTextbox.setHeight("100px");
		lTextbox.setWidth("400px");
		lTextbox.setToolbar("Basic");
	}

	/**
	 * Creates rating ok btn within item area to save rating and rating motivation.
	 *
	 * @param aParent the a parent
	 * @param aItem the a item
	 */
	protected void createRatingOkBtn(Component aParent,IXMLTag aItem) {
		CRunButton lButton = new CRunButton("", "ratingOk", aItem, CDesktopComponents.vView().getLabel("ok"), "_component_100", "");
		aParent.appendChild(lButton);
		lButton.registerObserver(getId());
	}

	/**
	 * Creates break within item area.
	 *
	 * @param aParent the a parent
	 */
	protected void createBreak(Component aParent) {
		Html lHtml = new CDefHtml("<br/>");
		aParent.appendChild(lHtml);
	}

	/**
	 * Is called by rating ok button and will save the rating for the current canon piece.
	 *
	 * @param aObserved the observed object, it should have interface CIObserved implemented
	 * @param aAction the action
	 * @param aStatus the status object, can be String or other class
	 */
	@Override
	public void observedNotify(CIObserved aObserved, String aAction, Object aStatus) {
		if (aAction.equals("ratingOk"))
			saveRating((IXMLTag)aStatus);
	}

	/**
	 * Saves rating and rating motivation for current run group account. And makes it
	 * available for other online run group accounts using the same canon component.
	 *
	 * @param aItem the piece within the canon case component
	 */
	protected void saveRating(IXMLTag aItem) {
		CSimpleCombo lCombo = (CSimpleCombo)CDesktopComponents.vView().getComponent("runCanonRating");
		CDefFCKeditor lTextbox = (CDefFCKeditor)CDesktopComponents.vView().getComponent("runCanonRatingMotivation");
		boolean lRatingChanged = false;
		String lRating = "";
		boolean lRatingMotivationChanged = false;
		String lRatingMotivation = "";
		if (lCombo != null) {
			lRatingChanged = (((String)lCombo.getAttribute("changed")).equals(AppConstants.statusValueTrue));
			if (lRatingChanged) {
				Listitem lListitem = ((Listbox) lCombo).getSelectedItem();
				if (lListitem != null) {
					lRating = (String) lListitem.getValue();
				}
			}
		}
		if (lTextbox != null) {
			lRatingMotivationChanged = (((String)lTextbox.getAttribute("changed")).equals(AppConstants.statusValueTrue));
			if (lRatingMotivationChanged) {
				lRatingMotivation = CDesktopComponents.sSpring().escapeXML(lTextbox.getValue());
			}
		}

		if (lRatingChanged || lRatingMotivationChanged) {
			IXMLTag lStatusStatusTag = CDesktopComponents.sSpring().newRunTag(AppConstants.statusElement);
			addRating(lStatusStatusTag, lRatingChanged, lRating, lRatingMotivationChanged, lRatingMotivation);
			// first change tag in memory
			CDesktopComponents.sSpring().addNewOrChangeExistingRgaTagToStatusTag(aItem, lStatusStatusTag.getChildTags());
			// then update statustag with status childs of memory tag
			// setRunTagStatus checks if childs already exist, if so they are skipped
			SStatustagEnrichment lStatustagEnrichment = new SStatustagEnrichment(); 
			lStatustagEnrichment.setStatusChildTags(aItem.getChild(AppConstants.statusElement).getChilds("rga"));
			CDesktopComponents.sSpring().setRunTagStatus(caseComponent,
					"", "", AppConstants.statusKeyRating,
					lRating, lStatustagEnrichment, true, aItem, getRunStatusType(), true, false);
			CRunTree lRunTree = (CRunTree)CDesktopComponents.vView().getComponent("runCanonTree");
			if (lRunTree != null) {
				CRunComponentHelper lTreeHelper = lRunTree.getRunComponentHelper();
				lTreeHelper.addExternalUpdateStatusTag(aItem, AppConstants.statusKeyRating, lRating);
			}
		}
	}


	/**
	 * Adds rating and/or rating motivation as xml child tags to aStatusStatusTag.
	 *
	 * @param aStatusStatusTag the a status status tag
	 * @param aRatingChanged the a rating changed state
	 * @param aRating the a rating
	 * @param aMotivationChanged the a motivation changed state
	 * @param aMotivation the a motivation
	 */
	private void addRating(IXMLTag aStatusStatusTag, boolean aRatingChanged, String aRating, boolean aMotivationChanged, String aMotivation) {
		if (aRatingChanged || aMotivationChanged) {
			// add rga child tag
			String lRgaId = "" + CDesktopComponents.sSpring().getRunGroupAccount().getRgaId();
			IXMLTag	lRgaChildTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag("rga", lRgaId);
			lRgaChildTag.setAttribute(AppConstants.defKeyType,"rgastatus");
			lRgaChildTag.setParentTag(aStatusStatusTag);
			aStatusStatusTag.getChildTags().add(lRgaChildTag);
			if (aRatingChanged) {
				// add rating childtag to rga child tag
				IXMLTag	lRgaChildChildTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag(AppConstants.statusKeyRating, aRating);
				lRgaChildChildTag.setAttribute(AppConstants.statusKeyTime, ""+CDesktopComponents.sSpring().getCaseTime());
				lRgaChildChildTag.setParentTag(lRgaChildTag);
				lRgaChildTag.getChildTags().add(lRgaChildChildTag);
			}
			if (aMotivationChanged) {
				// add motivation childtag to rga child tag
				IXMLTag	lRgaChildChildTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag("ratingmotivation", aMotivation);
				lRgaChildChildTag.setAttribute(AppConstants.statusKeyTime, ""+CDesktopComponents.sSpring().getCaseTime());
				lRgaChildChildTag.setParentTag(lRgaChildTag);
				lRgaChildTag.getChildTags().add(lRgaChildChildTag);
			}
		}
	}

}
