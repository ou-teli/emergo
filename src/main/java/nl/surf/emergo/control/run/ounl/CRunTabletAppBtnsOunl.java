/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.CRunChoiceHoverBtns;
import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.control.run.CRunHoverBtn;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunTabletAppBtns. Used to show tablet app buttons within tablet.
 * Clicking a tablet app button, opens the corresponding case component within the tablet.
 */
public class CRunTabletAppBtnsOunl extends CRunChoiceHoverBtns {

	private static final long serialVersionUID = -7597287841233457393L;
	
	/**
	 * Instantiates a new c run tablet app btns.
	 */
	public CRunTabletAppBtnsOunl() {
		super(null, "runTabletAppBtns", false);
	}

	/**
	 * Instantiates a new c run tablet app btns.
	 *
	 * @param aId the a id
	 * @param aParent the a parent
	 */
	public CRunTabletAppBtnsOunl(Component aParent, String aId) {
		super(aParent, aId, false);
	}
	
	protected int getMaxButtonsPerRow() {
		return 8;
	}

	protected class CaseComponentSortByName implements Comparator<IECaseComponent>{
		public int compare(IECaseComponent o1, IECaseComponent o2) {
			return o1.getName().compareTo(o2.getName());
		}
	}
	
	/**
	 * Updates hover buttons. Shows all tablet app buttons having the correct status.
	 */
	@Override
	public void update() {
		int counter = 0;
		for (Component lChild : buttonHboxList) {
			for (Component lComp : lChild.getChildren()) {
				if (lComp instanceof CRunHoverBtn) {
					CRunHoverBtn lBtn = (CRunHoverBtn)lComp;
					lBtn.detach();
				}
			}
			if (counter > 0) {
				lChild.detach();
			}
			counter ++;
		}
		if (runWnd == null)
			return;
		List<IECaseComponent> lTabletAppComponents = runWnd.getPlayerStatusList("TabletAppComponents", null);
		if (lTabletAppComponents == null)
			return;
		Collections.sort((List<IECaseComponent>)lTabletAppComponents, new CaseComponentSortByName());

		//NOTE get settings for all tablet app buttons
		String lAppButtonSize = "";
		String lAppButtonTextstyle = "";
		int lMaxNumberOfAppButtonsPerRow = getMaxButtonsPerRow();
		if (parent instanceof CRunComponent) {
			IXMLTag lContentTag = runWnd.getCaseComponentRootTag(((CRunComponent)parent).getCaseComponent());
			if (lContentTag != null) {
				lAppButtonSize = runWnd.getStyleSize(lContentTag, "appbuttonsize");
				lAppButtonTextstyle = lContentTag.getChildValue("appbuttontextstyle");
				String lMaxNumberOfAppButtonsPerRowStr = lContentTag.getChildValue("maxnumberofappbuttonsperrow");
				int lNumber = 0;
				try {
					lNumber = Integer.parseInt(lMaxNumberOfAppButtonsPerRowStr);
				} catch (NumberFormatException e) {
					lNumber = -1;
				}
				if (lNumber > 0) {
					lMaxNumberOfAppButtonsPerRow = lNumber;
				}
			}
		}
		
		int lVisibleCount = 0;
		counter = 0;
		for (IECaseComponent lCaseComponent : lTabletAppComponents) {
			String lId = "" + lCaseComponent.getCacId();
			String lName = lCaseComponent.getName();
			String lShowName = CDesktopComponents.sSpring().getCaseComponentRoleName("", lName);
			String lStatus = getBtnStatus(lCaseComponent);
			String lCode = lCaseComponent.getEComponent().getCode();
			List<String> lCacIdAndTagId = new ArrayList<String>();
			lCacIdAndTagId.add(lId);
			lCacIdAndTagId.add("0");
			
			Map<String,Object> lParams = new HashMap<String,Object>();
			lParams.put("id", runWnd.getTabletAppBtnId(lCaseComponent));
			lParams.put("status", lStatus);
			lParams.put("action", "showTabletApp");
			lParams.put("actionstatus", lCacIdAndTagId);
			lParams.put("imgprefix", lCode);
			lParams.put("label", lShowName);
			lParams.put("clientonclickaction", "");
			//NOTE get and set specific settings per case component, e.g., other background image
			lParams.putAll(runWnd.getCaseComponentBtnData(lCaseComponent));
			//NOTE set general settings per case component
			lParams.put("stylesize", lAppButtonSize);
			lParams.put("styletext", lAppButtonTextstyle);
			CRunHoverBtn lBtn = newHoverBtn(lParams);
			
			boolean lAccessible = CDesktopComponents.sSpring().getCurrentRunComponentStatus(
					lCaseComponent, AppConstants.statusKeyAccessible, AppConstants.statusTypeRunGroup).equals(AppConstants.statusValueTrue);
			lBtn.setAttribute("accessible", "" + lAccessible);
			if (!useArrows && counter == lMaxNumberOfAppButtonsPerRow) {
				buttonHbox = newButtonHbox();
				counter = 0;
			}
			buttonHbox.appendChild(lBtn);
			if (!lStatus.equals("empty")) {
				lVisibleCount ++;
				counter ++;
			}
		}
		setItemCount(lVisibleCount);
		showArrows(true);
		firstUpdate = false;
	}

	@Override
	protected CRunHoverBtn createHoverBtn(String aId, String aStatus, String aAction,
			Object aActionStatus, String aImgPrefix, String aLabel) {
		return new CRunTabletAppBtnOunl(aId, aStatus, aAction, aActionStatus, aImgPrefix, aLabel, "");
	}

	@Override
	protected CRunHoverBtn createHoverBtn(Map<String,Object> aParams) {
		return new CRunTabletAppBtnOunl(aParams);
	}

	/**
	 * Gets the btn status for the button identified by aCaseComponent.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return the btn status
	 */
	protected String getBtnStatus(IECaseComponent aCaseComponent) {
		boolean lPresent = ((CDesktopComponents.sSpring().getCurrentRunComponentStatus(
				aCaseComponent, AppConstants.statusKeyPresent, AppConstants.statusTypeRunGroup))
				.equals(AppConstants.statusValueTrue));
		boolean lAccessible = ((CDesktopComponents.sSpring().getCurrentRunComponentStatus(
				aCaseComponent, AppConstants.statusKeyAccessible, AppConstants.statusTypeRunGroup))
				.equals(AppConstants.statusValueTrue));
		boolean lOpened = ((CDesktopComponents.sSpring().getCurrentRunComponentStatus(
				aCaseComponent, AppConstants.statusKeyOpened, AppConstants.statusTypeRunGroup))
				.equals(AppConstants.statusValueTrue));
		// none of the empack action buttons not is pre-selected
		lOpened = false;
		return getBtnStatus(lPresent, lAccessible, lOpened);
	}

	/**
	 * Updates existing btn given by aBtnName with status given by aStatusKey and
	 * aStatusValue.
	 *
	 * @param aBtnName the a btn name
	 * @param aStatusKey the a status key
	 * @param aStatusValue the a status value
	 */
	public void updateBtn(String aBtnName, String aStatusKey, String aStatusValue) {
		super.updateBtn(aBtnName, aStatusKey, aStatusValue);
		CRunHoverBtn lBtn = (CRunHoverBtn) CDesktopComponents.vView().getComponent(aBtnName);
		if (lBtn == null)
			return;
		lBtn.setSclass(lBtn.getZclass() + "_" + lBtn.imgprefix);
	}

}
