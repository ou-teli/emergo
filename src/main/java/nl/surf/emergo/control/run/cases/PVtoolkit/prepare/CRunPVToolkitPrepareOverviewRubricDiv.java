/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.prepare;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Messagebox;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.feedback.CRunPVToolkitFeedbackOverviewRubricDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTag;
import nl.surf.emergo.view.VView;

public class CRunPVToolkitPrepareOverviewRubricDiv extends CRunPVToolkitFeedbackOverviewRubricDiv {

	private static final long serialVersionUID = -4190234208593126646L;

	public void update() {
		_currentTagIsStatusTag = false;
		
		_peerFeedback = false;
		_stepPrefix = "prepare";
		_idPrefix = "prepareOverview";
		_backToRecordingsId = "prepareRecordingsDiv"; 

		super.update();
	}
	
	protected void addSaveAndBackToRecordingsOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				VView vView = CDesktopComponents.vView();
				if (!feedbackIsReady()) {
					//show warning
					vView.showMessagebox(getRoot(), vView.getLabel("PV-toolkit-feedback.ready.confirmation"), vView.getLabel("PV-toolkit-feedback.ready"), Messagebox.OK, Messagebox.EXCLAMATION);
				}
				else {
					setFeedbackReady();
					toFeedbackRecordings(getId(), _stepPrefix, _currentTag);
				}
			}
		});
	}

	@Override
	protected String getTitle() {
		String labelKey = "PV-toolkit-prepare.header.assess";
		return CDesktopComponents.vView().getLabel(labelKey) + " " + pvToolkit.getTagChildValue(new CRunPVToolkitCacAndTag(caseComponent, _currentTag), "name");
	}
	
}
