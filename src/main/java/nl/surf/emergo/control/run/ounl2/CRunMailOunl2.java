/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl2;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Tree;

import nl.surf.emergo.control.run.CRunArea;
import nl.surf.emergo.control.run.ounl.CRunComponentDecoratorOunl;
import nl.surf.emergo.control.run.ounl.CRunMailOunl;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunMailOunl2 is used to show the mail component within the run view area of the
 * Emergo player.
 */
public class CRunMailOunl2 extends CRunMailOunl {

	private static final long serialVersionUID = -5471814890200829256L;

	/**
	 * Instantiates a new c run mail.
	 */
	public CRunMailOunl2() {
		super();
	}

	/**
	 * Instantiates a new c run mail.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the mail case component
	 */
	public CRunMailOunl2(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
	}

	@Override
	protected Component newContentComponent() {
		Tree tree = (Tree)super.newContentComponent();
		// change rows to infinite
		tree.setRows(0);
		return tree;
	}

	@Override
	protected CRunComponentDecoratorOunl createDecorator() {
		return new CRunComponentDecoratorOunl2();
	}

	@Override
	protected CRunArea createNewMailButton(String aId, String aEventAction, String aLabel) {
		return new CRunLabelButtonOunl2(aId, aEventAction, "", aLabel, "", "");
	}

}
