/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.superTeacher;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.CheckEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Div;
import org.zkoss.zul.Image;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Textbox;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitInitBox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefCheckbox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefCombobox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefComboitem;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefTextbox;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitPeerGroupMember;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.view.VView;

public class CRunPVToolkitSuperTeacherNewOrEditPeerGroupDiv extends CDefDiv {

	private static final long serialVersionUID = -2565214890460996470L;

	protected VView vView = CDesktopComponents.vView();
	
	protected String zulfilepath = ((CRunPVToolkitInitBox)vView.getComponent("PV-toolkit_initBox")).getZulfilepath();
	
	protected CRunPVToolkit pvToolkit;
	
	protected CRunPVToolkitSuperTeacherPeerGroupsDiv peerGroupsDiv;

	protected IXMLTag _peerGroupTag;
	
	protected List<IERunGroupAccount> runGroupAccounts;
	
	protected IAccountManager accountManager = (IAccountManager)CDesktopComponents.sSpring().getBean("accountManager");

	//NOTE isPreviewRun is true if preview option in authoring environment is used
	protected boolean isPreviewRun = false;
	
	protected String[] memberRoles = new String[] {
			CRunPVToolkit.peerGroupStudentRole, 
			CRunPVToolkit.peerGroupTeacherRole, 
			CRunPVToolkit.peerGroupStudentAssistantRole, 
			CRunPVToolkit.peerGroupPassiveTeacherRole, 
			CRunPVToolkit.peerGroupPeerStudentRole};
	protected String memberRoleLabelKeyPrefix = "PV-toolkit.peergroup.member.role.";
	protected String[] memberRoleLabels = new String[] {
			vView.getLabel(memberRoleLabelKeyPrefix + CRunPVToolkit.peerGroupStudentRole), 
			vView.getLabel(memberRoleLabelKeyPrefix + CRunPVToolkit.peerGroupTeacherRole), 
			vView.getLabel(memberRoleLabelKeyPrefix + CRunPVToolkit.peerGroupStudentAssistantRole), 
			vView.getLabel(memberRoleLabelKeyPrefix + CRunPVToolkit.peerGroupPassiveTeacherRole), 
			vView.getLabel(memberRoleLabelKeyPrefix + CRunPVToolkit.peerGroupPeerStudentRole)};

	protected String _idPrefix = "superTeacherNewOrEdit";
	protected String _classPrefix = "superTeacherNewOrEdit";
	
	public void init(IXMLTag peerGroupTag) {
		_peerGroupTag = peerGroupTag;
		
		pvToolkit = (CRunPVToolkit)vView.getComponent("pvToolkit");
		
		peerGroupsDiv = (CRunPVToolkitSuperTeacherPeerGroupsDiv)vView.getComponent("superTeacherPeerGroupsDiv");
		
		update();
		
		setVisible(true);
	}
	
	public void update() {
		
		getChildren().clear();
		
		pvToolkit.setMemoryCaching(true);
		
		runGroupAccounts = pvToolkit.getActiveRunGroupAccounts();
		isPreviewRun = CDesktopComponents.sSpring().getRun().getStatus() == AppConstants.run_status_test;

		new CRunPVToolkitDefImage(this, 
				new String[]{"class"}, 
				new Object[]{"popupBackground"}
		);

		new CRunPVToolkitDefImage(this, 
				new String[]{"class", "src"}, 
				new Object[]{_classPrefix + "PeerGroupBackground", zulfilepath + "popup-large-background.svg"}
		);

		Div div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "PeerGroupTitle"}
		);
		
		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font " + _classPrefix + "PeerGroupTitle", "PV-toolkit-superTeacher.header.peergroup"}
		);
		
		Image closeImage = new CRunPVToolkitDefImage(this, 
				new String[]{"class", "src"}, 
				new Object[]{_classPrefix + "PeerGroupCloseButton", zulfilepath + "close.svg"}
		);
		addCloseButtonOnClickEventListener(closeImage);

		div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "PeerGroupDiv"}
		);
		
		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font " + _classPrefix + "PeerGroupGiveNameTitle", "PV-toolkit-superTeacher.column.label.name"}
		);

		String name = "";
		if (_peerGroupTag != null) {
			name = pvToolkit.getStatusChildTagAttribute(_peerGroupTag, "name");
		}
		new CRunPVToolkitDefTextbox(div, 
				new String[]{"id", "class", "text"}, 
				new Object[]{_idPrefix + "PeerGroupGiveNameTextbox", "font " + _classPrefix + "PeerGroupGiveNameTextbox", name}
		);

		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font " + _classPrefix + "PeerGroupGiveScopeTitle", "PV-toolkit-superTeacher.column.label.scope"}
		);
		
		addPeerGroupScopeCombobox(div);

		Button btn = new CRunPVToolkitDefButton(div, 
				new String[]{"id", "class", "cLabelKey"}, 
				new Object[]{_idPrefix + "PeerGroupSaveButton", "font pvtoolkitButton " + _classPrefix + "PeerGroupSaveButton", "PV-toolkit.save"}
		);
		addSaveButtonOnClickEventListener(btn);

		div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "PeerGroupMembersDiv"}
		);
		
		Rows rows = appendPeersGrid(div);
		int rowNumber = 1;
		rowNumber = appendPeersToGrid(rows, rowNumber);

		pvToolkit.setMemoryCaching(false);
	}

	public Combobox addPeerGroupScopeCombobox(Component parent) {
		String peerGroupScope = "";
		if (_peerGroupTag != null) {
			peerGroupScope = pvToolkit.getStatusChildTagAttribute(_peerGroupTag, "scope");
		}
		Combobox combobox = new CRunPVToolkitDefCombobox(parent, 
				new String[]{"id", "width"}, 
				new Object[]{_idPrefix + "PeerGroupScopeCombobox", "250px"}
		);
		combobox.setClass("font " + _classPrefix + "PeerGroupScopeCombobox");

		String[] peerGroupScopes = null;
		if (pvToolkit.getCurrentCycleTag() == null) {
			//NOTE method has no cycles
			peerGroupScopes = new String[] {CRunPVToolkit.peerGroupMethodScope};
		}
		else {
			peerGroupScopes = new String[] {CRunPVToolkit.peerGroupMethodScope, CRunPVToolkit.peerGroupCycleScope};
		}
		String labelPrefix = "PV-toolkit.peergroup.scope.";
		for (int i=0;i<peerGroupScopes.length;i++) {
			Comboitem comboitem = new CRunPVToolkitDefComboitem(combobox, 
					new String[]{"value"}, 
					new Object[]{vView.getLabel(labelPrefix + peerGroupScopes[i])}
			);
			comboitem.setValue(peerGroupScopes[i]);
			comboitem.setClass("font " + _classPrefix + "PeerGroupScopeComboitem");
			if (peerGroupScopes[i].equals(peerGroupScope)) {
				combobox.setSelectedIndex(i);
			}
		}
		//TODO enable choosing other scopes. Now default is method scope
		if (peerGroupScope.equals("")) {
			combobox.setSelectedIndex(1);
		}
		combobox.setDisabled(true);
		
		return combobox;
	}

	protected void addCloseButtonOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				Component componentToHide = vView.getComponent(_idPrefix + "PeerGroupDiv");
				if (componentToHide != null) {
					componentToHide.setVisible(false);
				}
			}
		});
	}

	protected void addSaveButtonOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				String name = ((Textbox)vView.getComponent(_idPrefix + "PeerGroupGiveNameTextbox")).getValue().trim();
				if (name.equals("")) {
					//NOTE empty name, show warning
					vView.showMessagebox(getRoot(), vView.getLabel("PV-toolkit-superTeacher.peergroup.save.emptyname.warning"), vView.getLabel("PV-toolkit-superTeacher.peergroup.save"), Messagebox.OK, Messagebox.EXCLAMATION);
					return;
				}
				if (peerGroupsDiv.doesPeerGroupNameExist(_peerGroupTag, name)) {
					//NOTE name already exists, show warning
					vView.showMessagebox(getRoot(), vView.getLabel("PV-toolkit-superTeacher.peergroup.save.nameexists.warning"), vView.getLabel("PV-toolkit-superTeacher.peergroup.save"), Messagebox.OK, Messagebox.EXCLAMATION);
					return;
				}
				Combobox scopeCombobox = (Combobox)vView.getComponent(_idPrefix + "PeerGroupScopeCombobox");
				//NOTE selected item of combobox is preset or chosen by user
				peerGroupsDiv.savePeergroup(_peerGroupTag, name, scopeCombobox.getSelectedItem().getValue(), getPeerGroupMembers());
				
				setVisible(false);
			}
		});
	}

	protected List<CRunPVToolkitPeerGroupMember> getPeerGroupMembers() {
		List<CRunPVToolkitPeerGroupMember> peerGroupMembers = new ArrayList<CRunPVToolkitPeerGroupMember>();
		for (IERunGroupAccount runGroupAccount : runGroupAccounts) {
			Checkbox checkbox = (Checkbox)vView.getComponent(_idPrefix + "PeerGroupMemberCheckbox_" + runGroupAccount.getRgaId());
			Combobox combobox = (Combobox)vView.getComponent(_idPrefix + "PeerGroupMemberRoleCombobox_" + runGroupAccount.getRgaId());
			if (checkbox != null && combobox != null && checkbox.isChecked() && combobox.getSelectedItem() != null) {
				peerGroupMembers.add(new CRunPVToolkitPeerGroupMember(runGroupAccount, combobox.getSelectedItem().getValue()));
			}
		}
		return peerGroupMembers;
	}
	
    protected Rows appendPeersGrid(Component parent) {
    	return pvToolkit.appendGrid(
    			parent, 
    			getId() + "Grid",
    			"position:absolute;left:0px;top:0px;width:1060px;height:370px;overflow:auto;", 
    			null,
    			new String[] {"3%", "30%", "30%", "15%", "10%", "12%"}, 
    			new boolean[] {false, true, true, true, true, true},
    			null,
    			"PV-toolkit-superTeacher.peergroup.column.label.", 
    			new String[] {"", "name", "email", "userid", "member", "role"});
    }

	protected int appendPeersToGrid(Rows rows, int rowNumber) {
		return appendPeersToGrid(
				rows, 
				rowNumber,
				new boolean[] {true, true, true, true, true, true} 
				);
	}

	protected int appendPeersToGrid(
			Rows rows, 
			int rowNumber,
    		boolean[] showColumn 
			) {
		for (IERunGroupAccount runGroupAccount : runGroupAccounts) {
			rowNumber = appendPeerRowToGrid(
					rows, 
					rowNumber,
					showColumn,
					runGroupAccount
					);
		}

		return rowNumber;
	}
	
	protected int appendPeerRowToGrid(
			Rows rows, 
			int rowNumber,
    		boolean[] showColumn,
    		IERunGroupAccount runGroupAccount
			) {
		IXMLTag memberTag = pvToolkit.getPeerGroupMemberTag(_peerGroupTag, runGroupAccount);
		boolean memberChecked = memberTag != null;
		
		Row row = new Row();
		rows.appendChild(row);
			
		int columnNumber = 0;
		if (showColumn.length > columnNumber && showColumn[columnNumber]) {
			new CRunPVToolkitDefImage(row, 
					new String[]{"class", "src"}, 
					new Object[]{"gridImage", zulfilepath + "user.svg"}
					);
		}

		columnNumber++;
		if (showColumn.length > columnNumber && showColumn[columnNumber]) {
			new CRunPVToolkitDefLabel(row, 
					new String[]{"class", "value"}, 
					new Object[]{"font gridLabel", runGroupAccount.getERunGroup().getName()}
					);
		}

		columnNumber++;
		if (showColumn.length > columnNumber && showColumn[columnNumber]) {
			new CRunPVToolkitDefLabel(row, 
					new String[]{"class", "value"}, 
					new Object[]{"font gridLabel", runGroupAccount.getEAccount().getEmail()}
					);
		}

		columnNumber++;
		if (showColumn.length > columnNumber && showColumn[columnNumber]) {
			new CRunPVToolkitDefLabel(row, 
					new String[]{"class", "value"}, 
					new Object[]{"font gridLabel", runGroupAccount.getEAccount().getUserid()}
					);
		}

		columnNumber++;
		if (showColumn.length > columnNumber && showColumn[columnNumber]) {
			CRunPVToolkitDefCheckbox checkbox = new CRunPVToolkitDefCheckbox(row, 
					new String[]{"id", "checked"}, 
					new Object[]{_idPrefix + "PeerGroupMemberCheckbox_" + runGroupAccount.getRgaId(), memberChecked}
			);
			setCheckboxOrderValue(checkbox);
			addMemberOnCheckEventListener(checkbox);
		}

		columnNumber++;
		if (showColumn.length > columnNumber && showColumn[columnNumber]) {
			Combobox combobox = addPeerGroupMemberRoleCombobox(row, runGroupAccount, memberTag);
			setComboboxOrderValue(combobox);
			addRoleOnSelectEventListener(combobox);
		}
		
		rowNumber ++;

		return rowNumber;
	}

	protected void addMemberOnCheckEventListener(Component component) {
		component.addEventListener("onCheck", new EventListener<CheckEvent>() {
			public void onEvent(CheckEvent event) {
				setCheckboxOrderValue((Checkbox)event.getTarget());
			}
		});
	}
	
	protected void setCheckboxOrderValue(Checkbox checkbox) {
		if (checkbox.isChecked()) {
			checkbox.setAttribute("orderValue", "1");
		}
		else {
			checkbox.setAttribute("orderValue", "2");
		}
	}
	
	public Combobox addPeerGroupMemberRoleCombobox(Component parent, IERunGroupAccount runGroupAccount, IXMLTag memberTag) {
		Combobox combobox = new CRunPVToolkitDefCombobox(parent, 
				new String[]{"id", "width"}, 
				new Object[]{_idPrefix + "PeerGroupMemberRoleCombobox_" + runGroupAccount.getRgaId(), "100px"}
		);
		combobox.setClass("font " + _classPrefix + "PeerGroupMemberRoleCombobox");

		for (int i=0;i<memberRoles.length;i++) {
			Comboitem comboitem = new CRunPVToolkitDefComboitem(combobox, 
					new String[]{"value"}, 
					new Object[]{memberRoleLabels[i]}
			);
			comboitem.setValue(memberRoles[i]);
			comboitem.setClass("font " + _classPrefix + "PeerGroupMemberRoleComboitem");
		}
		setPeerGroupMemberRole(combobox, memberTag);
		
		return combobox;
	}
	
	public void setPeerGroupMemberRole(Combobox combobox, IXMLTag memberTag) {
		String memberRole = "";
		if (memberTag != null) {
			memberRole = pvToolkit.getStatusChildTagAttribute(memberTag, "role");
		}
		if (memberRole.equals("")) {
			//default role is student
			combobox.setSelectedIndex(0);
		}
		else {
			for (int i=0;i<memberRoles.length;i++) {
				if (memberRoles[i].equals(memberRole)) {
					combobox.setSelectedIndex(i);
				}
			}
		}
	}
	
	protected void setComboboxOrderValue(Combobox combobox) {
		//default role is student
		String memberRole = CRunPVToolkit.peerGroupStudentRole;
		Comboitem comboitem = combobox.getSelectedItem();
		if (comboitem != null) {
			memberRole = comboitem.getValue();
		}
		if (memberRole.equals(CRunPVToolkit.peerGroupTeacherRole)) {
			//show teachers on top
			combobox.setAttribute("orderValue", "1");
		}
		else if (memberRole.equals(CRunPVToolkit.peerGroupStudentAssistantRole)) {
			//show teachers on top
			combobox.setAttribute("orderValue", "2");
		}
		else if (memberRole.equals(CRunPVToolkit.peerGroupPassiveTeacherRole)) {
			//show teachers on top
			combobox.setAttribute("orderValue", "3");
		}
		else if (memberRole.equals(CRunPVToolkit.peerGroupStudentRole)) {
			//show teachers on top
			combobox.setAttribute("orderValue", "4");
		}
		else if (memberRole.equals(CRunPVToolkit.peerGroupPeerStudentRole)) {
			//show teachers on top
			combobox.setAttribute("orderValue", "5");
		}
	}
	
	protected void addRoleOnSelectEventListener(Component component) {
		component.addEventListener("onSelect", new EventListener<Event>() {
			public void onEvent(Event event) {
				setComboboxOrderValue((Combobox)event.getTarget());
			}
		});
	}
	
}
