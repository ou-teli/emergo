/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefImage;

/**
 * The Class CRunBlobImage. Used to show image.
 */
public class CRunBlobImageClassic extends CDefImage {

	private static final long serialVersionUID = -4070588904642326936L;

	/** The run wnd. */
	protected CRunWndClassic runWnd = (CRunWndClassic) CDesktopComponents.vView().getComponent(CControl.runWnd);

	/** The blobcontainer, the xml tag containing the blob tag as child. */
	protected IXMLTag blobcontainer = null;

	/**
	 * Instantiates a new c run blob label.
	 * 
	 * @param aBlobContainer the a blob container, the xml tag containing the blob tag as child
	 */
	public CRunBlobImageClassic(IXMLTag aBlobContainer) {
		setSrc("");
		setContent((org.zkoss.image.Image) null);
		blobcontainer = aBlobContainer;
		String lBloId = blobcontainer.getChildValue("picture");
		/** To do: canonpicture is called "illustration"; should be altered to "picture"? */
		if (lBloId.equals("")) {
			lBloId = blobcontainer.getChildValue("illustration");
		}
		if (!lBloId.equals("")) {
			setContent((org.zkoss.image.Image) null);
			setSrc(CDesktopComponents.sSpring().getSBlobHelper().getImageSrc(lBloId));
		}
	}
}
