/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.CreateEvent;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.utilities.PropsValues;
import nl.surf.emergo.view.VView;

public class CRunVideoMemosInitBox extends CRunStreamingMediaInitBox {

	private static final long serialVersionUID = -78029734843960819L;

	public void onCreate(CreateEvent aEvent) {
		init();

		CDesktopComponents.sSpring().initDesktopAttributes(getDesktop());

		String runComponentId = (String) VView.getParameter("runComponentId", this, "runMemos");

		// NOTE Video may have dimensions that differ from the case skin size.
		// In this case we need to correct video position and size so it will be shown
		// centered.
		// Notice that the memos component only plays conversations.
		String left = (String) getDesktop().getAttribute("flashMemosLeft");
		String top = (String) getDesktop().getAttribute("flashMemosTop");
		double maxWidth = Double.parseDouble((String) getDesktop().getAttribute("flashMemosMaxWidth"));
		double maxHeight = Double.parseDouble((String) getDesktop().getAttribute("flashMemosMaxHeight"));

		String width = (String) VView.getParameter("video_size_width", this,
				(String) getDesktop().getAttribute("runWndWidth"));
		String height = (String) VView.getParameter("video_size_height", this,
				(String) getDesktop().getAttribute("runWndHeight"));

		double leftDouble = Double.parseDouble(left);
		double topDouble = Double.parseDouble(top);
		// Remove 'px' extension
		width = width.substring(0, width.length() - 2);
		height = height.substring(0, height.length() - 2);
		double widthDouble = Double.parseDouble(width);
		double heightDouble = Double.parseDouble(height);
		double widthRatio = maxWidth / widthDouble;
		double heightRatio = maxHeight / heightDouble;
		if (widthRatio <= heightRatio) {
			heightDouble = widthRatio * heightDouble;
			width = "" + Math.round(maxWidth);
			height = "" + Math.round(heightDouble);
			top = "" + Math.round(topDouble + ((maxHeight - heightDouble) / 2));
		} else {
			widthDouble = heightRatio * widthDouble;
			width = "" + Math.round(widthDouble);
			height = "" + Math.round(maxHeight);
			left = "" + Math.round(leftDouble + ((maxWidth - widthDouble) / 2));
		}
		String divheight = "" + Math.round(Integer.parseInt(height) + Integer.parseInt(top));

		HtmlMacroComponent macro = new HtmlMacroComponent();
		appendChild(macro);
		macro.setDynamicProperty("a_emergoRootUrl", vView.getAbsoluteWebappsRoot());
		macro.setDynamicProperty("a_jwplayerKey", PropsValues.VIDEO_JWPLAYER_KEY);
		macro.setDynamicProperty("a_playerName", "EM_fl_memos");
		macro.setDynamicProperty("a_left", left);
		macro.setDynamicProperty("a_top", top);
		macro.setDynamicProperty("a_width", width);
		macro.setDynamicProperty("a_height", height);
		macro.setDynamicProperty("a_divheight", divheight);
		macro.setDynamicProperty("a_runComponentId", runComponentId);
		macro.setDynamicProperty("a_streamingUrls", streamingurls);
		// NOTE url may contain back slashes. Replace by slashes
		url = url.replaceAll(PropsValues.STREAMING_PLAYER_PATH_SEPARATOR, "/");
		macro.setDynamicProperty("a_url", url);
		macro.setMacroURI("../run_" + PropsValues.VIDEO_PLAYER_ID + "_memos_view_fr_macro.zul");
	}

}
