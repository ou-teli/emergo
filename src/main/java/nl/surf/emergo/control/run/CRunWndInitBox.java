/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefBox;
import nl.surf.emergo.view.VView;

/**
 * The Class CRunWndInitBox. To be able to initialize settings for the Emergo player window.
 */
public class CRunWndInitBox extends CDefBox
 {

	private static final long serialVersionUID = -7342452642919744132L;

	public CRunWndInitBox() {
		//NOTE enable setting user id in case read-only link is used to start Emergo player, for demo purposes.
		String lRunStatus = VView.getReqPar("runstatus");
		int lTestAccId = -1;
		int lAccId = CDesktopComponents.cControl().getAccId();
		if (lAccId == 0) {
			if (lRunStatus == null || !(lRunStatus.equals(AppConstants.runStatusRun) || lRunStatus.equals(AppConstants.runStatusPreview))) {
				CDesktopComponents.cControl().setAccId(lTestAccId);
			}
		} else {
			// if session started as read only player, but user changed url run status to 'run' and refreshed, then avoid saving player status change
			if (lAccId == lTestAccId) {
				if (lRunStatus != null && (lRunStatus.equals(AppConstants.runStatusRun) || lRunStatus.equals(AppConstants.runStatusPreview))) {
					CDesktopComponents.cControl().setAccId(0);
				}
			}
		}
	}

 }
