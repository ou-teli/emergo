package nl.surf.emergo.control.run.cases.IP2;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.PieModel;

public class CRunPiechartPieChartVMKP4 {

	CRunPiechartPieChartEngineKP4 engine;
	PieModel model;
	boolean threeD = false;
	String message;
	
	@Init
	public void init() {
		// prepare chart data
		engine = new CRunPiechartPieChartEngineKP4();

		model = CRunPiechartPieChartDataKP4.getModel();
	}

	public CRunPiechartPieChartEngineKP4 getEngine() {
		return engine;
	}

	public PieModel getModel() {
		return model;
	}

	public boolean isThreeD() {
		return threeD;
	}
	
	public String getMessage(){
		return message;
	}
	
	@Command("showMessage") 
	@NotifyChange("message")
	public void onShowMessage(
			@BindingParam("msg") String message){
		this.message = message;
	}
	
	@GlobalCommand("categoryChanged") 
	@NotifyChange("model")
	public void onCategoryChanged(
			@BindingParam("category")String category,
			@BindingParam("oldcategory") String oldcategory,
			@BindingParam("num") Number num){
		//NOTE categories cannot be simply overridden, like data so ...
		//... remove old category ...
		model.removeValue(oldcategory);
		//... and add category if num > 0.
		if (num.doubleValue() > 0) {
			model.setValue(category, num);
		}
	}
	
	@GlobalCommand("dataChanged") 
	@NotifyChange("model")
	public void onDataChanged(
			@BindingParam("category")String category,
			@BindingParam("num") Number num){
		if (num.doubleValue() > 0) {
			model.setValue(category, num);
		}
		else {
			model.removeValue(category);
		}
	}
	
	@GlobalCommand("configChanged") 
	@NotifyChange({"threeD","engine"})
	public void onConfigChanged(
			@BindingParam("threeD") boolean threeD,
			@BindingParam("exploded") boolean exploded){
		this.threeD = threeD;
		engine.setExplode(exploded);
	}
}
