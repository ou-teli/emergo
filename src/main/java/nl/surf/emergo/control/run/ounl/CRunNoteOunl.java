/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Label;

import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefLabel;
import nl.surf.emergo.control.run.CRunArea;
import nl.surf.emergo.control.run.CRunCloseBtn;
import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.control.run.CRunHbox;
import nl.surf.emergo.control.run.CRunNote;

/**
 * The Class CRunNoteOunl is used to show the note window within the run choice area of the
 * Emergo player. Notes can be edited within the logbook too, so this class has a public
 * method to save content.
 */
public class CRunNoteOunl extends CRunNote {

	private static final long serialVersionUID = -1534771257696653614L;

	/**
	 * Instantiates a new c run note.
	 * Creates title area with close box and edit field for note.
	 */
	public CRunNoteOunl() {
		super();
	}

	/**
	 * Instantiates a new c run note.
	 * Creates title area with close box and edit field for note.
	 *
	 * @param aId the a id
	 * @param aRunComponent the a run component
	 */
	public CRunNoteOunl(String aId, CRunComponent aRunComponent) {
		super(aId, aRunComponent);
	}

	/**
	 * Creates title area with close box and edit field for note.
	 */
	@Override
	public void init() {
		textboxRows = 5;
		maxStringLength = 70;
		super.init();
	}

	/**
	 * Creates title area, a title label and close box.
	 *
	 * @param aParent the a parent
	 */
	@Override
	protected void createTitleArea(Component aParent) {
		CRunHbox lSuperHbox = new CRunHbox();
		aParent.appendChild(lSuperHbox);

		String lType = "";
		CRunArea lArea = new CRunArea();
		lSuperHbox.appendChild(lArea);
		lArea.setZclass(getClassName()+lType+"_title_area_left");
		CRunHbox lHbox = new CRunHbox();
		lArea.appendChild(lHbox);
		Label lLabel = new CDefLabel();
		lLabel.setId(getId() + "Title");
		lLabel.setValue(CDesktopComponents.vView().getLabel("run_note.title"));
		lLabel.setZclass(getClassName() + "_title_area_label");
		lHbox.appendChild(lLabel);

		lArea = new CRunArea();
		lSuperHbox.appendChild(lArea);
		lArea.setZclass(getClassName()+"_title_area_right");
		CRunCloseBtn lButton = new CRunCloseBtn(getId() + "CloseBtn",
				"active","endNote", null, "", "");
		lButton.setCanHaveStatusSelected(false);
		lButton.registerObserver(getId());
		lButton.registerObserver(CControl.runWnd);
		lButton.setSclass(getClassName() + lType + "_close_btn_active");
		lArea.appendChild(lButton);
	}

	/**
	 * Get textbox width.
	 *
	 * @return the width
	 */
	@Override
	protected String getTextboxWidth() {
		return "551px";
	}

}
