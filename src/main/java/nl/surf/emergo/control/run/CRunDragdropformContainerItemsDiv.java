/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.DropEvent;
import org.zkoss.zk.ui.event.Event;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;

public class CRunDragdropformContainerItemsDiv extends CDefDiv {

	private static final long serialVersionUID = -7712736641095821949L;

	public void onClick(Event aEvent) {
		//alternative for drag and drop
		//NOTE click on text or document also generates on click to container, so ignore this on click.
		CRunDragdropforms runDragdropforms = (CRunDragdropforms)CDesktopComponents.vView().getComponent("runDragdropforms");
		if (runDragdropforms.ignoreOnClick) {
			runDragdropforms.ignoreOnClick = false;
		}
		else {
			if (runDragdropforms.draggedComponent != null) {
				((HtmlBasedComponent)runDragdropforms.draggedComponent).setStyle("");
				if (getDroppable().equals("true")) {
					runDragdropforms.dropItem(runDragdropforms.draggedComponent, this);
				}
				runDragdropforms.draggedComponent = null;
			}
		}
	}

	public void onDrop(DropEvent aEvent) {
		CRunDragdropforms runDragdropforms = (CRunDragdropforms)CDesktopComponents.vView().getComponent("runDragdropforms");
		//NOTE regular onDrop so clear possible selected component to drag by clicking on it
		runDragdropforms.draggedComponent = null;
		runDragdropforms.dropItem(aEvent.getDragged(), this);
	}
	
}
