/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.HashMap;
import java.util.Map;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zkex.zul.Jasperreport;
import org.zkoss.zul.Listbox;

import net.sf.jasperreports.engine.JRDataSource;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefButton;
import nl.surf.emergo.view.VView;

public class CRunJasperReportCreateBtn extends CDefButton {

	private static final long serialVersionUID = -7992489648584555243L;

	protected VView vView = CDesktopComponents.vView();

	public void onClick(Event aEvent) {
		//Preparing parameters
		vView.getComponent("control").setVisible(false);
		Map<String,Object> parameters = new HashMap<String,Object>();
		parameters.put("reportTitle", getDesktop().getAttribute("run_reporttitle"));
		
		Jasperreport report = (Jasperreport)vView.getComponent("report");
		report.setSrc((String)getDesktop().getAttribute("run_reportjasperfile"));
		report.setParameters(parameters);
		report.setDatasource((JRDataSource)getDesktop().getAttribute("run_reportdatasource"));
		Listbox format = (Listbox)vView.getComponent("format");
		report.setType((String)format.getSelectedItem().getValue());
		if (format.getSelectedItem().getValue().equals("rtf") || format.getSelectedItem().getValue().equals("odt")) {
			//NOTE rtf and odt are downloaded so hide popup
			getRoot().setVisible(false);
		} 
	}
	
}
