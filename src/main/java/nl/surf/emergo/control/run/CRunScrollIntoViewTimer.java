/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.util.Clients;

import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefTimer;

/**
 * The Class CRunScrollIntoViewTimer is used by the Emergo player (class CRunWnd).
 */
public class CRunScrollIntoViewTimer extends CDefTimer {

	private static final long serialVersionUID = 6156864806625150625L;

	/** The run wnd. */
	protected CRunWnd runWnd = (CRunWnd) CDesktopComponents.vView().getComponent(CControl.runWnd);

	/** The p idle, used to prevent parallel or overlapping timer handling. */
	protected boolean pIdle = true;

	/**
	 * Instantiates a new c run scroll into view timer.
	 *
	 * @param aId the a id
	 */
	public CRunScrollIntoViewTimer(String aId) {
		setId(aId);
		setRunning(false);
		setDelay(100);
		setRepeats(true);
	}

	/**
	 * On timer.
	 *
	 * @param aEvent the a event
	 */
	public void onTimer(Event aEvent) {
		if (!pIdle)
			return;
		pIdle = false;

		//Get the component
		Component lComponent = (Component)CDesktopComponents.vView().getComponent((String)runWnd.getAttribute("scrollIntoViewId"));
		if (lComponent != null && isRunning()) {
			//If component is found, meaning ZK is ready rendering, and if the timer is still running
			stop();					
			runWnd.setAttribute("scrollIntoViewId", "");
			Clients.scrollIntoView(lComponent);
			if (runWnd.getAttribute("isScrollIntoViewOriginalIdEmpty").equals("true")) {
				//Restore the originallly empty id of the component
				lComponent.setId("");
			}
		}
		
		pIdle = true;
	}

}
