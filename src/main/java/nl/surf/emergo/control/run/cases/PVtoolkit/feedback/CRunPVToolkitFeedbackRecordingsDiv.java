/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.feedback;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.CheckEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefHelper;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitSubStepsDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefCheckbox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.view.VView;

public class CRunPVToolkitFeedbackRecordingsDiv extends CRunPVToolkitFeedbackDiv {

	private static final long serialVersionUID = -7740166765787060198L;

	protected List<IXMLTag> practiceTagsForSending = new ArrayList<IXMLTag>();
	
	protected String _stepPrefix = "feedback";
	protected String _classPrefix = "feedback";
	
	protected Boolean _showAskFeedbackColumn = false;
	protected List<IXMLTag> _sharedPracticeTags;
	
	@Override
	public void init(IERunGroup actor, boolean reset, boolean editable, String subStepType) {
		_giveFeedback = subStepType.equals(CRunPVToolkit.feedbackGiveFeedbackSubStepType);
		_sendFeedback = subStepType.equals(CRunPVToolkit.feedbackSendFeedbackSubStepType);
		_analyzeVideo = subStepType.equals(CRunPVToolkit.prepareAnalyzeVideoSubStepType);
		_compareWithExperts = subStepType.equals(CRunPVToolkit.prepareCompareWithExpertsSubStepType);



		setClass(_classPrefix + "Recordings");
		super.init(actor, reset, editable, subStepType);
		if (!_initialized)
			setVisible(true);
		
		_initialized = true;
	}
	
	protected IXMLTag getStepTag() {
		return pvToolkit.getCurrentStepTag();
	}
	
	@Override
	public void reset() {
		_initialized = false;
	}
	
	@Override
	public void update() {
		getChildren().clear();
		
		pvToolkit.setMemoryCaching(true);
		
		caseComponent = pvToolkit.getCurrentRubricsCaseComponent();
		skillTag = pvToolkit.getCurrentSkillTag();
		
		_sharedPracticeTags = null;

		Div div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{"titleRight"}
		);
		
		CRunPVToolkitSubStepsDiv runPVToolkitSubStepsDiv = (CRunPVToolkitSubStepsDiv)CDesktopComponents.vView().getComponent("feedbackSubStepsDiv");
		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "value"}, 
				new Object[]{"font titleRight", (String)runPVToolkitSubStepsDiv.getAttribute("divTitle")}
		);
		
		div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "RecordingsDiv"}
		);
		
		Button sendBtn = new CRunPVToolkitDefButton(this, 
				new String[]{"id", "class", "cLabelKey"}, 
				new Object[]{getId() + "SendButton", "font pvtoolkitButton " + _classPrefix + "SendButtonInactive", "PV-toolkit-feedback.button.send"}
		);
		sendBtn.setDisabled(true);
		addSendOnClickEventListener(sendBtn);
		
		if (_giveFeedback || _sendFeedback) {
			if (pvToolkit.hasTeacherRole(_actor)) {
				//NOTE check if ask for feedback column has to be shown
				_showAskFeedbackColumn = (pvToolkit.mayAskTeacherFeedbackCount(_actor) >= 0);
				if (!_showAskFeedbackColumn) {
					//NOTE ask for feedback not configured by default; check if it is configured for individual student
					_sharedPracticeTags = pvToolkit.getFeedbackPracticeTags(_actor, pvToolkit.getPreviousStepTagInCurrentCycle(getStepTag(), CRunPVToolkit.practiceStepType), false, null, null);
					if (_sharedPracticeTags != null) {
						for (IXMLTag lPT : _sharedPracticeTags) {
							_showAskFeedbackColumn = (pvToolkit.mayAskTeacherFeedbackCount(pvToolkit.getRunGroupAccount(lPT.getAttribute(CRunPVToolkit.practiceRgaId)).getERunGroup()) >= 0);
							if (_showAskFeedbackColumn)
								break;
						}
					}
				}
			}
		}

		Rows rows = appendRecordingsGrid(div);
		int rowNumber = 1;
		rowNumber = appendRecordingsToGrid(rows, rowNumber);
		
		Button btn = new CRunPVToolkitDefButton(this, 
				new String[]{"class", "cLabelKey"}, 
				new Object[]{"font pvtoolkitButton subStepReadyButton", "PV-toolkit.ready"}
		);
		addBackToSubStepssOnClickEventListener(btn, _stepPrefix + "SubStepDiv");

		pvToolkit.setMemoryCaching(false);
		
	}

	protected void addBackToSubStepssOnClickEventListener(Component component, String fromComponentId) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				Component fromComponent = CDesktopComponents.vView().getComponent(fromComponentId);
				if (fromComponent != null) {
					fromComponent.setVisible(false);
				}
				CRunPVToolkitSubStepsDiv toComponent = (CRunPVToolkitSubStepsDiv)CDesktopComponents.vView().getComponent(_stepPrefix + "SubStepsDiv");
				if (toComponent != null) {
					toComponent.update();
					toComponent.setVisible(true);
				}
			}
		});
	}

	protected void addSendOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				VView vView = CDesktopComponents.vView();
				int choice = vView.showMessagebox(getRoot(), vView.getCLabel("PV-toolkit-feedback.send.confirmation"), vView.getCLabel("PV-toolkit-feedback.send"), Messagebox.CANCEL | Messagebox.OK, Messagebox.QUESTION);
				if (choice == Messagebox.OK) {
					for (IXMLTag practiceTagForSending : practiceTagsForSending) {
						setFeedbackShared(practiceTagForSending);
					}
					for (IXMLTag practiceTagForSending : practiceTagsForSending) {
						update(practiceTagForSending);
					}
					practiceTagsForSending.clear();
				}
			}
		});
	}

	public void update(IXMLTag practiceTag) {
		String practiceTagUuid = practiceTag.getAttribute(CRunPVToolkit.practiceUuId);
		VView vView = CDesktopComponents.vView();

		setLinkLabelVariableProperties((Label)vView.getComponent(getId() + "GridRowFeedbackLabel_" + practiceTagUuid), practiceTag);
		setStatusLabelVariableProperties((Label)vView.getComponent(getId() + "GridRowStatusLabel_" + practiceTagUuid), practiceTag);
		setSendCheckboxVariableProperties((Checkbox)vView.getComponent(getId() + "GridRowSendCheckbox_" + practiceTagUuid), practiceTag);
	}
	
    protected Rows appendRecordingsGrid(Component parent) {
    	String[] lColWidths= new String[8];
    	String[] lColComplete = {"3%", "21%", "11%", "12%", "14%", "13%", "10%", "16%"};
    	String[] lColMin = {"4%", "27%", "17%", "17%", "0%", "19%", "16%", "0%"};
    	String[] lColNoFB = {"3%", "23%", "14%", "15%", "0%", "15%", "12%", "18%"};
    	String[] lColNoAsk = {"4%", "24%", "14%", "14%", "17%", "16%", "11%", "0%"};
    	
    	if (!(_giveFeedback || _showAskFeedbackColumn)) {
    		lColWidths = lColMin;
    	} else {
    		if (!_giveFeedback) {
        		lColWidths = lColNoFB;
    		} else {
    			if (!_showAskFeedbackColumn) {
    	    		lColWidths = lColNoAsk;
    			} else
    				lColWidths = lColComplete;
    		}
    	}
    	
    	return pvToolkit.appendGrid(
    			parent, 
    			getId() + "Grid",
    			"position:absolute;left:0px;top:0px;width:850px;height:480px;overflow:auto;", 
				new boolean[] {true, true, true, true, _giveFeedback, true, true, _showAskFeedbackColumn}, 
    			lColWidths, 
    			new boolean[] {false, true, true, true, true, true, false, true},
    			null,
    			"PV-toolkit-feedback.column.label.",
    			new String[] {"", "activity", "peergroup", "deadline", "feedback", "status", "send", "askteacherfeedback"});
    }

	protected int appendRecordingsToGrid(Rows rows, int rowNumber) {
		return appendRecordingsToGrid(
				rows, 
				rowNumber,
				new boolean[] {true, true, true, true, _giveFeedback, true, true, _showAskFeedbackColumn} 
				);
	}

	protected int appendRecordingsToGrid(
			Rows rows, 
			int rowNumber,
    		boolean[] showColumn 
			) {
		if (_sharedPracticeTags == null) {
			_sharedPracticeTags = pvToolkit.getFeedbackPracticeTags(_actor, pvToolkit.getPreviousStepTagInCurrentCycle(getStepTag(), CRunPVToolkit.practiceStepType), false, null, null);
		}
		if (_sharedPracticeTags == null) {
			return rowNumber;
		}

		String deadline = "";
		String deadlineDate = pvToolkit.getDeadlineDateStr(_actor, getStepTag());
		String deadlineTime =  pvToolkit.getDeadlineTimeStr(_actor, getStepTag());
		if (!deadlineDate.equals("")) {
			deadline += deadlineDate;
			if (!deadlineTime.equals("")) {
				deadline += " " + deadlineTime;
			}
		}
		
		String deadlineYMD = "";
		if (!StringUtils.isEmpty(deadline)) {
			Date date = CDefHelper.getDateFromStrDMY(deadline);
			deadlineYMD = CDefHelper.getDateStrAsYMD(date);
		}
		for (IXMLTag practiceTag : _sharedPracticeTags) {
			IERunGroupAccount feedbackReceiver = pvToolkit.getRunGroupAccount(practiceTag.getAttribute(CRunPVToolkit.practiceRgaId));
			boolean feedbackIsReady = feedbackIsReady(practiceTag);
			if ((_giveFeedback || feedbackIsReady) && feedbackReceiver != null) {
				
				String practiceTagUuid = practiceTag.getAttribute(CRunPVToolkit.practiceUuId);
	
				Row row = new Row();
				rows.appendChild(row);
				
				int columnNumber = 0;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					new CRunPVToolkitDefImage(row, 
							new String[]{"class", "src"}, 
							new Object[]{"gridImage", zulfilepath + "ballot.svg"}
							);
				}
	
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					new CRunPVToolkitDefLabel(row, 
							new String[]{"class", "value"}, 
							new Object[]{"font gridLabel", CDesktopComponents.vView().getLabel("PV-toolkit-feedback.row.label.activity").replace("%1", feedbackReceiver.getERunGroup().getName())}
							);
				}
	
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					new CRunPVToolkitDefLabel(row, 
							new String[]{"class", "value"}, 
							new Object[]{"font gridLabel", pvToolkit.getPeerGroupName(feedbackReceiver.getERunGroup(), _actor)}
							);
				}
	
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					CRunPVToolkitDefLabel label = new CRunPVToolkitDefLabel(row, 
							new String[]{"class", "value"}, 
							new Object[]{"font gridLabel", deadline}
							);
					//NOTE for ordering use ymd format
					if (!StringUtils.isEmpty(deadlineYMD)) {
						label.setAttribute("orderValue", deadlineYMD);
					}
				}
				
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					Label linkLabel = new CRunPVToolkitDefLabel(row, 
							new String[]{"id", "class"}, 
							new Object[]{getId() + "GridRowFeedbackLabel_" + practiceTagUuid, "font gridLink"}
							);
					setLinkLabelVariableProperties(linkLabel, practiceTag);
					toFeedbackOnClickEventListener(linkLabel, _actor, getStepTag(), practiceTag);
				}
	
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					Label statusLabel = new CRunPVToolkitDefLabel(row, 
							new String[]{"id", "class"}, 
							new Object[]{getId() + "GridRowStatusLabel_" + practiceTagUuid, "font gridLabel"}
							);
					setStatusLabelVariableProperties(statusLabel, practiceTag);
				}
	
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					Checkbox sendCheckbox = new CRunPVToolkitDefCheckbox(row, 
							new String[]{"id", "class", "checked"}, 
							new Object[]{getId() + "GridRowSendCheckbox_" + practiceTagUuid, "gridCheckbox", false}
							);
					setSendCheckboxVariableProperties(sendCheckbox, practiceTag);
					sendCheckbox.setAttribute("practiceTag", practiceTag);
					addSendOnCheckEventListener(sendCheckbox);
				}
	
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					String lLabelStatus = "false";
					if (pvToolkit.practiceIsTeacherFeedbackAsked(feedbackReceiver.getERunGroup(), practiceTag)) {
						lLabelStatus = "true";
					}
					new CRunPVToolkitDefLabel(row, 
							new String[]{"class", "value"}, 
							new Object[]{"font gridLabel", CDesktopComponents.vView().getLabel("PV-toolkit-feedback.row.label.askteacherfeedback." + lLabelStatus)}
							);
				}
	
				rowNumber ++;
	
			}
		}

		return rowNumber;
	}

	public void setLinkLabelVariableProperties(Label linkLabel, IXMLTag practiceTag) {
		if (linkLabel == null || practiceTag == null) {
			return;
		}
		boolean feedbackIsShared = feedbackIsShared(practiceTag);
		//NOTE editable if _editable and not in consultation or shared
		boolean editable = _editable && !feedbackIsShared;

		String labelKey = "PV-toolkit-feedback.row.button.feedback.";
		if (editable) {
			labelKey += "give";
		}
		else {
			labelKey += "view";
		}
		linkLabel.setAttribute("editable", editable);
		linkLabel.setValue(CDesktopComponents.vView().getLabel(labelKey));
	}
	
	public void setStatusLabelVariableProperties(Label statusLabel, IXMLTag practiceTag) {
		if (statusLabel == null || practiceTag == null) {
			return;
		}
		statusLabel.setValue(CDesktopComponents.vView().getLabel("PV-toolkit-feedback.status." + getFeedbackStatus(practiceTag)));
	}
	
	public void setSendCheckboxVariableProperties(Checkbox sendCheckbox, IXMLTag practiceTag) {
		if (sendCheckbox == null || practiceTag == null) {
			return;
		}
		boolean feedbackIsReady = feedbackIsReady(practiceTag);
		boolean feedbackIsShared = feedbackIsShared(practiceTag);
		//NOTE editable if _editable and not in consultation or shared
		boolean editable = _editable && !feedbackIsShared;
		//NOTE possible to send if editable and ready
		boolean possibleToSend = editable && feedbackIsReady;

		//NOTE possible to send if editable and ready
		sendCheckbox.setVisible(possibleToSend);
	}
	
	protected void toFeedbackOnClickEventListener(Component component, IERunGroup actor, IXMLTag feedbackStepTag, IXMLTag practiceTag) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				IXMLTag practiceStepTag = pvToolkit.getPreviousStepTagInCurrentCycle(feedbackStepTag, CRunPVToolkit.practiceStepType);
				if (practiceStepTag == null) {
					return;
				}
				CDesktopComponents.vView().getComponent(_stepPrefix + "RecordingsDiv").setVisible(false);
				
				boolean showPreviousFeedback = false;
				if (!_showMyFeedback) {
					List<IXMLTag> previousCurrentStepTags = pvToolkit.getStepTagsInPreviousCycles(practiceStepTag, CRunPVToolkit.practiceStepType);
					showPreviousFeedback = previousCurrentStepTags.size() > 0;
				}
				
				if (showPreviousFeedback) {
					CRunPVToolkitFeedbackPreviousOverviewRubricDiv previousOverviewDiv = (CRunPVToolkitFeedbackPreviousOverviewRubricDiv)CDesktopComponents.vView().getComponent(_stepPrefix + "PreviousOverviewRubricDiv");
					if (previousOverviewDiv != null) {
						previousOverviewDiv.init(actor, practiceStepTag, practiceTag, false, true);
						previousOverviewDiv.setVisible(true);
					}
				}
				else {
					CRunPVToolkitFeedbackFeedbackDiv feedbackDiv = (CRunPVToolkitFeedbackFeedbackDiv)CDesktopComponents.vView().getComponent(_stepPrefix + "FeedbackDiv");
					if (feedbackDiv != null) {
						boolean editable = false;
						if (component.getAttribute("editable") != null) {
							editable = (boolean)component.getAttribute("editable");
						}
						feedbackDiv.init(actor, practiceStepTag, practiceTag, editable);
						feedbackDiv.setVisible(true);
					}
				}
			}
		});
	}
	
	protected void addSendOnCheckEventListener(Component component) {
		component.addEventListener("onCheck", new EventListener<CheckEvent>() {
			public void onEvent(CheckEvent event) {
				if (event.isChecked()) {
					addPracticeTagForSending((IXMLTag)event.getTarget().getAttribute("practiceTag"));
				}
				else {
					removePracticeTagForSending((IXMLTag)event.getTarget().getAttribute("practiceTag"));
				}
			}
		});
	}
	
	protected void addPracticeTagForSending(IXMLTag practiceTag) {
		if (!practiceTagsForSending.contains(practiceTag)) {
			practiceTagsForSending.add(practiceTag);
		}
		updateSendButton();
	}

	protected void removePracticeTagForSending(IXMLTag practiceTag) {
		if (practiceTagsForSending.contains(practiceTag)) {
			practiceTagsForSending.remove(practiceTag);
		}
		updateSendButton();
	}


	protected void updateSendButton() {
		Button sendBtn = (Button)CDesktopComponents.vView().getComponent(getId() + "SendButton");
		if (sendBtn == null) {
			return;
		}
		if (practiceTagsForSending.size() > 0) {
			sendBtn.setZclass("font pvtoolkitButton " + _classPrefix + "SendButtonActive");
			sendBtn.setDisabled(false);
		}
		else {
			sendBtn.setZclass("font pvtoolkitButton " + _classPrefix + "SendButtonInactive");
			sendBtn.setDisabled(true);
		}
	}
	
	@Override
	protected IXMLTag getFeedbackStepTag() {
		//NOTE for feedback recordings the feedback step is equal to the current step tag.
		return getStepTag();
	}
	
}
