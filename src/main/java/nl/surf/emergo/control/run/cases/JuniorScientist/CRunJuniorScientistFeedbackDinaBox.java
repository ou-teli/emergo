/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.JuniorScientist;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.event.Event;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;

/**
 * This class extends from CRunJuniorScientistFeedbackSvenBox2 and partly operates equally. Now there are 25 questions.
 * The differences are that the questions are given to the macro ZUL file as a JSON string. The reason for this is that the HTML file used
 * for this macro is that complex and contains images. Therefore the rendering in the ZUL file is not done by ZK script using a foreach but
 * is done using Javascript which is able to parse the JSON string. If an answer is given the rerendering is also handled by Javascript.
 * Therefore method onGiveAnswer is overwritten.
 */
public class CRunJuniorScientistFeedbackDinaBox extends CRunJuniorScientistFeedbackSvenBox2 {

	private static final long serialVersionUID = -8084676822334151309L;

	protected static final String hellaCommentPidPrefix = "hella_comment_";

	protected String showAnimationPid = ""; 
	protected String feedbackOkStateKeyPid = ""; 

	@Override
	public void onInit() {
		super.onInitMacro();
		
		feedbackCaseComponentName = "phase_4_feedback_dina";
		feedbackOkStateKeyPid = "game_4_feedback_dina_ok";
		showAnimationPid = "game_4_show_animation";
		feedbackReadyStateKeyPid = "game_4_feedback_dina_ready";

		feedbackAppCaseComponent = getFeedbackAppCaseComponent();

		piecePopupMacroId = getUuid() + "_piece_popup_macro";
		piecePopupId = getUuid() + "_piece_popup";
		
		referencesCaseComponent = getReferencesComponent();
		//NOTE super ancestor CRunComponent needs case component to be set
		setCaseComponent(referencesCaseComponent);

		//determine references properties to be used within the child macro
		setCommissionProperties();
		
		//add the child macro
		addChildMacro("JuniorScientist_feedback_dina_view_macro.zul");
	}
	
	@Override
	protected int getNumberOfSelectors() {
		return 25;
	}
	
	@Override
	protected boolean useScrollIntoView() {
		return false;
	}
	
	@Override
	protected void setCommissionProperties() {
		// General
		
		propertyMap.put("sender", getStateTagValue(employeeCardNameStateKey));

		propertyMap.put("showAnimation", getStateTagValue(showAnimationPid));
		String feedbackReady = getStateTagValue(feedbackReadyStateKeyPid);
		propertyMap.put("feedbackReady", feedbackReady);
		
		// Selectors
		
		numberOfGivenAnswers = 0;
		numberOfCorrectAnswers = 0;

		selectorTags = new ArrayList<IXMLTag>();
		
		StringBuffer selectorsBuffer = new StringBuffer();
		
		//put all node tags in a map per pid to be able to get them by number later on
		Map<String,IXMLTag> nodeTagsByPid = getNodeTagsByPid();
		int counter = 1;
		boolean render = true;
		while (nodeTagsByPid.containsKey(selectorPidPrefix + counter)) {
			//content to be rendered is entered in two content elements per selector
			IXMLTag selectorTag = nodeTagsByPid.get(selectorPidPrefix + counter);
			
			selectorTags.add(selectorTag);
			
			StringBuffer jsonBuffer = new StringBuffer();
			addJsonKeyValue(jsonBuffer, "tagId", selectorTag.getAttribute(AppConstants.keyId), true);
			addJsonKeyValue(jsonBuffer, "counter", "" + counter, true);
			addJsonKeyValue(jsonBuffer, "render", "" + render, true);
			
			addJsonKeyValue(jsonBuffer, "correctAnswer", selectorTag.getChildValue("correctoptions"), true);

			//get default or given answer
			String answer = getAnswer(selectorTag);
			addJsonKeyValue(jsonBuffer, "answer", answer, true);

			boolean isAnswerEmpty = answer.equals("");
			if (!isAnswerEmpty) {
				numberOfGivenAnswers++;
			}
			else {
				render = false;
			}
			//is answer given correct
			boolean isAnswerCorrect = isAnswerCorrect(selectorTag, answer);
			if (isAnswerCorrect) {
				numberOfCorrectAnswers++;
			}

			addJsonKeyValue(jsonBuffer, "answerIsCorrect", "" + isAnswerCorrect, true);
			
			IXMLTag hellaCommentTag = nodeTagsByPid.get(hellaCommentPidPrefix + counter);
			String hellaComment = "";
			if (hellaCommentTag != null) {
				hellaComment = sSpring.unescapeXML(hellaCommentTag.getChildValue("defaulttext")).replace("'", "\\'");
			}
			addJsonKeyValue(jsonBuffer, "hellaComment", hellaComment, true);
			
			StringBuffer selectorOptionsBuffer = new StringBuffer();

			//get options within selector
			List<String> options = Arrays.asList(selectorTag.getChildValue("options").split("\n"));
			int optionCounter = 1;
			int selectedIndex = -1;
			for (String option : options) {
				StringBuffer jsonBuffer2 = new StringBuffer();
				addJsonKeyValue(jsonBuffer2, "option", option, true);
				addJsonKeyValue(jsonBuffer2, "optionCounter", "" + optionCounter, false);
				//preselect given answer
				if (("" + optionCounter).equals(answer)) {
					selectedIndex = optionCounter - 1;
				}

				if (selectorOptionsBuffer.length() > 0) {
					selectorOptionsBuffer.append(",");
				}
				selectorOptionsBuffer.append("{");
				selectorOptionsBuffer.append(jsonBuffer2);
				selectorOptionsBuffer.append("}");

				optionCounter++;
			}
			selectorOptionsBuffer.insert(0, "[");
			selectorOptionsBuffer.append("]");

			addJsonKeyValue(jsonBuffer, "options", selectorOptionsBuffer, true);
			addJsonKeyValue(jsonBuffer, "selectedIndex", "" + selectedIndex, false);

			if (selectorsBuffer.length() > 0) {
				selectorsBuffer.append(",");
			}
			selectorsBuffer.append("{");
			selectorsBuffer.append(jsonBuffer);
			selectorsBuffer.append("}");
			
			
			counter++;
		}

		selectorsBuffer.insert(0, "[");
		selectorsBuffer.append("]");

		propertyMap.put("selectors", selectorsBuffer.toString());
		
		propertyMap.put("numberOfSelectors", getNumberOfSelectors());
		propertyMap.put("numberOfGivenAnswers", numberOfGivenAnswers);
		propertyMap.put("numberOfCorrectAnswers", numberOfCorrectAnswers);

		propertyMap.put("showComments", "" + (feedbackReady.equals(AppConstants.statusValueTrue) && numberOfCorrectAnswers < numberOfGivenAnswers));
		

		// Pieces
		
		propertyMap.put("referencesTitle", sSpring.unescapeXML(sSpring.getCaseComponentRoleName("", referencesCaseComponent.getName())));
		
		//NOTE get pieces within folder 'Fase 3 - Data-analyse'
		List<Map<String,Object>> pieces = new ArrayList<Map<String,Object>>();
		propertyMap.put("pieces", pieces);
		IXMLTag rootTag = sSpring.getXmlDataPlusRunStatusTree(referencesCaseComponent, AppConstants.statusTypeRunGroup);
		if (rootTag == null) {
			return;
		}
		IXMLTag contentTag = rootTag.getChild(AppConstants.contentElement);
		if (contentTag == null) {
			return;
		}
		
		IXMLTag correctFolderTag = null;
		//loop through folders
		for (IXMLTag folderTag : contentTag.getChilds("map")) {
			if (sSpring.unescapeXML(folderTag.getChildValue("name")).equals("Fase 4 - Rapportage")) {
				correctFolderTag = folderTag;
				break;
			}
		}
		if (correctFolderTag != null) {
			for (IXMLTag pieceTag : correctFolderTag.getChilds("piece")) {
				if (!pieceTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse) &&
						!sSpring.unescapeXML(pieceTag.getChildValue("name")).equals("Feedback aan Dina")) {
					//NOTE don't add 'feedback aan Dina' macro as piece within 'feedback aan Dina' macro! 
					Map<String,Object> hPieceData = new HashMap<String,Object>();
					pieces.add(hPieceData);
					hPieceData.put("tag", pieceTag);
					hPieceData.put("pieceName", sSpring.unescapeXML(pieceTag.getChildValue("name")));
				}
			}
		}

		propertyMap.put("pieceData", getPieceData(null));
		
	}
	
	protected void addJsonKeyValue(StringBuffer element, String key, String value, boolean separator) {
		element.append("\"");
		element.append(key);
		element.append("\":\"");
		element.append(value);
		element.append("\"");
		if (separator) {
			element.append(",");
		}
	}

	protected void addJsonKeyValue(StringBuffer element, String key, StringBuffer value, boolean separator) {
		element.append("\"");
		element.append(key);
		element.append("\":");
		element.append(value);
		if (separator) {
			element.append(",");
		}
	}

	@Override
	public void onGiveAnswer(Event event) {
		Object[] objects = ((Object[])event.getData());
		String selectorTagId = (String)objects[0];
		String answer = (String)objects[1];
		
		IXMLTag selectorTag = null;
		for (IXMLTag tempSelectorTag : selectorTags) {
			if (tempSelectorTag.getAttribute(AppConstants.keyId).equals(selectorTagId)) {
				selectorTag = tempSelectorTag;
				break;
			}
		}

		if (selectorTag != null) {
			boolean isAnswerCorrect = isAnswerCorrect(selectorTag, "" + answer);
			//just like for the original editforms component set following states
			setRunTagStatusJS(getFeedbackAppCaseComponent(), selectorTag, AppConstants.statusKeyAnswer, answer, false);
			setRunTagStatusJS(getFeedbackAppCaseComponent(), selectorTag, AppConstants.statusKeySelected, AppConstants.statusValueTrue, false);
			setRunTagStatusJS(getFeedbackAppCaseComponent(), selectorTag, AppConstants.statusKeyCorrect, "" + isAnswerCorrect, false);
		}

		if (getStateTagValue(showAnimationPid).equals(AppConstants.statusValueTrue)) {
			//don't show animation anymore if an answer is given
			setStateTagValue(showAnimationPid, AppConstants.statusValueFalse);
		}
	}

	public void onReady(Event event) {
		hideMacro(currentTag);
		if (areAllAnswersCorrect()) {
			setStateTagValue(feedbackOkStateKeyPid, AppConstants.statusValueTrue);
		}
		setStateTagValue(feedbackReadyStateKeyPid, AppConstants.statusValueTrue);
	}
	
}
