/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit;

import org.zkoss.zk.ui.Component;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;

public class CRunPVToolkitVideoRecordingLink extends CRunPVToolkitDefLabel {

	private static final long serialVersionUID = -8059257407531016987L;

	protected int _cycleNumber;
	protected IXMLTag _practiceTag;
	protected String _componentToHideId = "";
	protected String _componentToShowId = "";
	protected String _componentToInitIdPrefix = "";
	
	public CRunPVToolkitVideoRecordingLink(Component parent, String[] keys, Object[] values) {
		super(parent, keys, values);
	}

	public void init(int cycleNumber, IXMLTag practiceTag, String componentToHideId, String componentToShowId, String componentToInitIdPrefix) {
		_cycleNumber = cycleNumber;
		_practiceTag = practiceTag;
		_componentToHideId = componentToHideId;
		_componentToShowId = componentToShowId;
		_componentToInitIdPrefix = componentToInitIdPrefix;
	}

	public void onClick() {
		Component levelDiv = CDesktopComponents.vView().getComponent(_componentToHideId);
		if (levelDiv != null) {
			levelDiv.setVisible(false);
		}
		Component recordingDiv = CDesktopComponents.vView().getComponent(_componentToShowId);
		if (recordingDiv != null) {
			CRunPVToolkitVideoRecordingDiv videoRecordingDiv = (CRunPVToolkitVideoRecordingDiv)CDesktopComponents.vView().getComponent(_componentToInitIdPrefix + "RecordingDiv");
			videoRecordingDiv.setAttribute("divTitle", getValue());
			videoRecordingDiv.init(_cycleNumber, _practiceTag);
			recordingDiv.setVisible(true);
		}
	}

}
