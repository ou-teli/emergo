/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.JuniorScientist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.event.Event;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.zkspring.STriggeredReference;

/**
 * This class is used to show both a phase ready pop-up. It uses content entered within a tasks case component. There are main tasks (phases) with one level of sub tasks.
 * The used tasks case component should have present=false and present should stay false, to prevent rendering of the original tasks component on the tablet.
 * Accessible and finished of tasks determine the layout of the pop-up.
 * 
 * A state with pid 'phase_ready_text' is used to influence the behavior of the pop-up and is set by script.
 * If its value is not empty the value is shown within the pop-up and an animation is played.
 * 
 * The pop-up offers options to open the tasks macro or not.
 */
public class CRunJuniorScientistPhaseReadyBox extends CRunJuniorScientistBox {

	private static final long serialVersionUID = 6264194472103642914L;

	protected final static String tasksCaseComponentName = "game_tasks";
	protected IECaseComponent tasksCaseComponent;
	
	protected final static String phaseReadyTextStateKeyPid = "phase_ready_text"; 

	@Override
	public void onInit() {
		super.onInitMacro();
		
		tasksCaseComponent = getTasksComponent();
		//NOTE super ancestor CRunComponent needs case component to be set
		setCaseComponent(tasksCaseComponent);

		//determine task properties to be used within the child macro
		setTasksProperties();
		
		//add the child macro
		addChildMacro("JuniorScientist_phase_ready_view_macro.zul");
	}
	
	@Override
	public void onUpdate() {
		//rerender child macro with adjusted properties
		childMacro.recreate();
	}

	protected void setTasksProperties() {
		List<Map<String,Object>> tasks = new ArrayList<Map<String,Object>>();
		propertyMap.put("tasks", tasks);
		IXMLTag rootTag = sSpring.getXmlDataPlusRunStatusTree(tasksCaseComponent, AppConstants.statusTypeRunGroup);
		if (rootTag == null) {
			return;
		}
		IXMLTag contentTag = rootTag.getChild(AppConstants.contentElement);
		if (contentTag == null) {
			return;
		}
		
		//NOTE current main task (= phase) name is shown within tasks bar
		String currentMainTaskName = "";
		int taskCounter = 0;
		//loop through main tasks
		for (IXMLTag taskTag : contentTag.getChilds("task")) {
			Map<String,Object> hTaskData = new HashMap<String,Object>();
			tasks.add(hTaskData);
			hTaskData.put("tag", taskTag);
			hTaskData.put("taskNumber", "" + (taskCounter + 1));
			boolean taskFinished = areSubTasksFinished(taskTag);
			hTaskData.put("taskDone", taskFinished);
			String taskName = sSpring.unescapeXML(taskTag.getChildValue("name"));
			if (taskFinished) {
				currentMainTaskName = taskName;
			}
			taskCounter++;
		}
		
		propertyMap.put("currentMainTaskName", currentMainTaskName);
		propertyMap.put("currentMainTaskPresentText", getStateTagValue(phaseReadyTextStateKeyPid));
	}
	
	protected boolean areSubTasksFinished(IXMLTag taskTag) {
		boolean areSubTasksFinished = true;
		for (IXMLTag subTaskTag : taskTag.getChilds("task")) {
			boolean subTaskFinished = subTaskTag.getCurrentStatusAttribute(AppConstants.statusKeyFinished).equals(AppConstants.statusValueTrue);
			areSubTasksFinished = areSubTasksFinished && subTaskFinished; 
		}
		return areSubTasksFinished;
	}
	
	protected IECaseComponent getTasksComponent() {
		return sSpring.getCaseComponent("", tasksCaseComponentName);
	}
	
	/** If a button is clicked within the child macro file, following event is triggered. */
	public void onButtonClick(Event event) {
		if (event.getData().equals("close_popup")) {
			//hide macro component
			setRunTagStatusJS(getNavigationCaseComponent(), currentTag, AppConstants.statusKeyPresent, AppConstants.statusValueFalse, false);
		}
		else if (event.getData().equals("show_phases_popup")) {
			//NOTE get tasks macro tag
			IXMLTag tasksMacroTag = getTagByPid(getNavigationCaseComponent(), "tasks_macro");
			if (tasksMacroTag != null) {
				//set opened of tasks component to true
				setRunComponentStatusJS(tasksCaseComponent, AppConstants.statusKeyOpened, AppConstants.statusValueTrue);
			}

			//hide phase readey macro component
			setRunTagStatusJS(getNavigationCaseComponent(), currentTag, AppConstants.statusKeyPresent, AppConstants.statusValueFalse, false);

			if (tasksMacroTag != null) {
				//update it
				setRunTagStatusJS(getNavigationCaseComponent(), tasksMacroTag, AppConstants.statusKeyUpdate, AppConstants.statusValueTrue, true);
			}
		}
	}
	
	@Override
	public void handleStatusChange(STriggeredReference aTriggeredReference) {
	}

}
