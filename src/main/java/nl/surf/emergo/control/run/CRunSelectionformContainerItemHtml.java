/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefHtml;
import nl.surf.emergo.zkspring.SSpringHelper;

public class CRunSelectionformContainerItemHtml extends CDefHtml {

	private static final long serialVersionUID = 7969486224363610564L;

	public void onCreate(CreateEvent aEvent) {
		String content = (String)getAttribute("content");
		String lParStart = "<p>";
		String lParEnd = "</p>";
		int lStartIndex = content.indexOf(lParStart);
		int lEndIndex = content.lastIndexOf(lParEnd);
		if (lStartIndex >= 0 && lEndIndex > lStartIndex) {
			content = content.substring(lStartIndex + lParStart.length(), lEndIndex);
			setContent(content);
		}
		String br = "<br/>";
		int pos = getContent().lastIndexOf("<br/>");
		//only add space if last part of content is not equal to break
		if (pos == -1 || (pos + br.length() < getContent().length())) {
			setContent(getContent() + "&nbsp;");
		}
	}

	public void onClick(Event aEvent) {
		CRunSelectionforms runSelectionforms = (CRunSelectionforms)CDesktopComponents.vView().getComponent(CRunSelectionforms.runSelectionformsId);
		if (runSelectionforms == null) {
			return;
		}
		boolean accessible = (boolean)getAttribute("accessible");
		if (accessible) {
			boolean right = runSelectionforms.isShowIfRightOrWrong((IXMLTag)getAttribute("tag")) && runSelectionforms.isRightlySelected((IXMLTag)getAttribute("tag"));
			//NOTE rightly selected are not clickable anymore
			if (!right) {
				boolean selected = (boolean)getAttribute("selected");
				boolean allowClick = false;
				Component container = CDesktopComponents.vView().getComponent("selectionformItemBox_" + runSelectionforms.getContainerTagId((IXMLTag)getAttribute("tag")));
				if (container != null) {
					int numberofselecteditems = Integer.parseInt((String)container.getAttribute("numberofselecteditems"));
					int maxitemstoselect = Integer.parseInt((String)container.getAttribute("maxitemstoselect"));
					allowClick = (!selected && numberofselecteditems < maxitemstoselect) || (selected && numberofselecteditems <= maxitemstoselect);
				}
				if (allowClick) {
					selected = !selected;
					setAttribute("selected", selected);
					Events.postEvent("onUpdate", this, null);
					runSelectionforms.selectItem((IXMLTag)getAttribute("tag"), selected);
				}
			}
		}
	}

	public void onFeedbackUpdate(Event aEvent) {
		CRunSelectionforms runSelectionforms = (CRunSelectionforms)CDesktopComponents.vView().getComponent(CRunSelectionforms.runSelectionformsId);
		if (runSelectionforms == null) {
			return;
		}
		SSpringHelper sSpringHelper = new SSpringHelper();
		boolean accessible = (boolean)getAttribute("accessible");
		if (accessible) {
			boolean correct = sSpringHelper.getCurrentStatusTagStatusChildAttribute((IXMLTag)getAttribute("tag"), AppConstants.statusKeyCorrect, "true").equals("true");
			if (!correct) {
				//deselect not correct ones
				boolean selected = (boolean)getAttribute("selected");
				if (selected) {
					selected = !selected;
					setAttribute("selected", selected);
					Events.postEvent("onUpdate", this, null);
					runSelectionforms.selectItem((IXMLTag)getAttribute("tag"), selected);
				}
			}
			else {
				Events.postEvent("onUpdate", this, null);
			}
		}
	}

	public void onUpdate(Event aEvent) {
		CRunSelectionforms runSelectionforms = (CRunSelectionforms)CDesktopComponents.vView().getComponent(CRunSelectionforms.runSelectionformsId);
		if (runSelectionforms == null) {
			return;
		}
		String sclass = "CRunSelectionformContainerItem";
		boolean accessible = (boolean)getAttribute("accessible");
		if (accessible) {
			sclass += "Selectable";
			if (runSelectionforms.isShowIfRightOrWrong((IXMLTag)getAttribute("tag"))) {
				if (runSelectionforms.isRightlySelected((IXMLTag)getAttribute("tag"))) {
					sclass += "Right";
				}
				else {
					sclass += "Wrong";
				}
			}
			boolean selected = (boolean)getAttribute("selected");
			if (selected) {
				sclass += " CRunSelectionformContainerItemBoxSelected";
			}
		}
		if (getSclass() == null || !getSclass().equals(sclass)) {
			setSclass(sclass);
		}
		Component container = CDesktopComponents.vView().getComponent("selectionformItemBox_" + runSelectionforms.getContainerTagId((IXMLTag)getAttribute("tag")));
		if (container != null) {
			Events.postEvent("onUpdate", container, null);
		}
	}

}
