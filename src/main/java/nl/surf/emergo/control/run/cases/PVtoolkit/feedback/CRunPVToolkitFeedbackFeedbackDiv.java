/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.feedback;

import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Div;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.domain.IERunGroup;

public class CRunPVToolkitFeedbackFeedbackDiv extends CRunPVToolkitFeedbackDiv {

	private static final long serialVersionUID = 5921544493734670095L;

	public void init(IERunGroup actor, IXMLTag currentStepTag, IXMLTag currentTag, boolean editable) {
		_actor = actor;
		_editable = editable;

		pvToolkit = (CRunPVToolkit)CDesktopComponents.vView().getComponent("pvToolkit");
		
		setClass(_classPrefix + "Feedback");

		update(currentStepTag, currentTag);
		_initialized = true;
	}

	public void update(IXMLTag currentStepTag, IXMLTag currentTag) {
		_currentStepTag = currentStepTag;
		_currentTag = currentTag;

		//NOTE this class determines _editable itself so parameter editable in init method above is neglected!
		//NOTE _currentStepTag and _currentTag have to be set before, because this methods feedbackIsShared and practiceIsShared uses these properties!
		if (_peerFeedback) {
			if (!_showMyFeedback) {
				//NOTE Peer feedback: while feedback is not shared peer feedback is editable
				_editable = !feedbackIsShared();
			}
			else {
				//NOTE Show my feedback: feedback is not editable
				_editable = false;
			}
		}
		else {
			//NOTE Self feedback: while practice is not shared self feedback is editable
			_editable = !practiceIsShared();
		}

		//NOTE put all elements in a specific div and if it exists remove it.
		//Other elements are defined in the ZUL file and may not be removed 
		Div specificDiv = pvToolkit.getSpecificDiv(this);

		new CRunPVToolkitDefImage(specificDiv, 
				new String[]{"class", "visible"}, 
				new Object[]{"feedbackInfoButton", "false"}
		);
		
		Div div = new CRunPVToolkitDefDiv(specificDiv, 
				new String[]{"class"}, 
				new Object[]{"titleRight"}
		);
		
		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "value"}, 
				new Object[]{"font titleRight", getTitle()}
		);

		//show link
		addFileLink(_currentTag, specificDiv, "", "font gridLink " + _classPrefix + "FileLink", pvToolkit.getRubricLabelText("PV-toolkit-file"));
		
		boolean showPreviousFeedback = false;
		if (!_showMyFeedback) {
			List<IXMLTag> previousCurrentStepTags = pvToolkit.getStepTagsInPreviousCycles(currentStepTag, CRunPVToolkit.practiceStepType);
			showPreviousFeedback = previousCurrentStepTags.size() > 0;
		}
		if (showPreviousFeedback) {
			Component navBtn = new CRunPVToolkitDefButton(specificDiv, 
					new String[]{"class", "cLabelKey"}, 
					new Object[]{"font pvtoolkitButton " + _classPrefix + "BackButton", "PV-toolkit.back"}
			);
			addBackToPreviousButtonOnClickEventListener(navBtn, getId(), _idPrefix + "PreviousOverviewRubricDiv");
		}
		else {
			Component navBtn = new CRunPVToolkitDefButton(specificDiv, 
					new String[]{"class", "cLabelKey"}, 
					new Object[]{"font pvtoolkitButton " + _classPrefix + "BackButton", "PV-toolkit.back"}
					);
			addBackButtonOnClickEventListener(navBtn, _currentTag);
		}
		
		Component btn = new CRunPVToolkitDefButton(specificDiv, 
				new String[]{"class", "cLabelKey"}, 
				new Object[]{"font pvtoolkitButton " + _classPrefix + "ForwardButton", "PV-toolkit.forward"}
		);
		addForwardButtonOnClickEventListener(btn);

		showVideo();
		
		CRunPVToolkitFeedbackRubricDiv rubric = (CRunPVToolkitFeedbackRubricDiv)CDesktopComponents.vView().getComponent(_idPrefix + "RubricDiv");
		if (rubric != null) {
			if (currentStepTag != null) {
				rubric.init(_actor, currentStepTag, _currentTag, _editable, false);
				rubric.setVisible(true);
			}
		}
		
	}
	
	protected String getTitle() {
		String labelKey = "PV-toolkit-feedback.header.giveFeedback";
		return CDesktopComponents.vView().getLabel(labelKey) + " " + pvToolkit.getRgaName(_currentTag.getAttribute(CRunPVToolkit.practiceRgaId));
	}
	
	protected void showVideo() {
		pvToolkit.showStatusVideo(_currentTag, _idPrefix, _classPrefix);
	}

	protected void clearVideo() {
		pvToolkit.clearVideo(_idPrefix);
	}

	protected void addBackToPreviousButtonOnClickEventListener(Component component, String fromComponentId, String toComponentId) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				clearVideo();
				Component fromComponent = CDesktopComponents.vView().getComponent(fromComponentId);
				if (fromComponent != null) {
					fromComponent.setVisible(false);
				}
				Component toComponent = CDesktopComponents.vView().getComponent(toComponentId);
				if (toComponent != null) {
					toComponent.setVisible(true);
				}
			}
		});
	}

	protected void addBackButtonOnClickEventListener(Component component, IXMLTag tag) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				clearVideo();
				toFeedbackRecordings(_idPrefix + "FeedbackDiv", _idPrefix, tag);
			}
		});
	}

	protected void addForwardButtonOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				clearVideo();
				toFeedbackOverviewRubric(_idPrefix + "FeedbackDiv", _idPrefix, _editable);
			}
		});
	}

}
