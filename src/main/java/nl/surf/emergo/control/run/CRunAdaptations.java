/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.DOMException;
import org.xml.sax.SAXException;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Include;

import eu.rageproject.asset.manager.AssetManager;
import eu.rageproject.assets.hatasset.ArgumentException;
import eu.rageproject.assets.hatasset.ArgumentOutOfRangeException;
import eu.rageproject.assets.hatasset.HATAsset;
import eu.rageproject.assets.hatasset.HATAsset.HATMode;
import eu.rageproject.assets.hatasset.HATPlayer;
import eu.rageproject.assets.hatasset.HATScenario;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefInclude;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.utilities.RageAssetManagerBridge;
import nl.surf.emergo.zkspring.STriggeredReference;

/**
 * The Class CRunAdaptations is used to recalculate player and scenario
 * adaptations when a player has finished a scenario.
 */
public class CRunAdaptations extends CRunComponent {
	private static final Logger _log = LogManager.getLogger(CRunAdaptations.class);
	private static final long serialVersionUID = 342489917416470517L;

	private CRunAdaptationsHelper cRunHelper = new CRunAdaptationsHelper();
	private RageAssetManagerBridge managerBridge = new RageAssetManagerBridge();
	private HATAsset hatAsset;

	/**
	 * Instantiates a new c run adaptations.
	 */
	public CRunAdaptations() {
		super("runAdaptations", null);
		init();
	}

	/**
	 * Instantiates a new c run adaptations.
	 *
	 * @param aId            the a id
	 * @param aCaseComponent the adaptations case component
	 */
	public CRunAdaptations(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
		init();
	}

	/**
	 * Creates title area, content area to show apps, and buttons area with close
	 * button.
	 */
	@Override
	protected void createComponents() {
		/*
		 * CRunVbox lVbox = new CRunVbox();
		 * 
		 * createTitleArea(lVbox); createContentArea(lVbox); createButtonsArea(lVbox);
		 * 
		 * appendChild(lVbox);
		 */
	}

	/**
	 * Creates new content component.
	 *
	 * @return the component
	 */
	@Override
	protected Component newContentComponent() {
		Include lView = new CDefInclude();
		lView.setId("runAdaptationsView");
		lView.setVisible(false);
		return lView;
	}

	/**
	 * Creates buttons area and adds close button.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run hbox
	 */
	@Override
	protected CRunHbox createButtonsArea(Component aParent) {
		CRunHbox lButtonsHbox = super.createButtonsArea(aParent);
		createCloseButton(lButtonsHbox);
		return lButtonsHbox;
	}

	/**
	 * Initializes RAGE HAT asset.
	 */
	protected void init() {
		cRunHelper.setCaseComponent(getCaseComponent());
		cRunHelper.setRunAdaptation(this);
		AssetManager.getInstance().setBridge(managerBridge);
		hatAsset = new HATAsset(cRunHelper);
		// if (!cRunHelper.settingsExist()) {
		addScenariosAndPlayers();
		// }
	}

	/**
	 * Gets the asset mode
	 *
	 * @return the asset mode string
	 */
	private String getAssetMode() {
		IXMLTag lContentTag = getDataPlusStatusRootTag().getChild(AppConstants.contentElement);
		String lMode = "";
		lMode = lContentTag.getChildValue("mode");
		if (lContentTag != null) {
			lMode = lContentTag.getChild("mode").getValue();
		}
		return lMode;
	}

	/**
	 * Gets the adaptation tags.
	 *
	 * @return the adaptation tags
	 */
	protected List<IXMLTag> getAdaptationTags() {
		List<IXMLTag> lAdaptationTags = new ArrayList<IXMLTag>();
		IXMLTag lContentTag = getDataPlusStatusRootTag().getChild(AppConstants.contentElement);
		if (lContentTag != null) {
			for (IXMLTag lAdaptationTag : lContentTag.getChildTags(AppConstants.defValueNode)) {
				if (lAdaptationTag.getName().equals("adaptation")) {
					lAdaptationTags.add(lAdaptationTag);
				}
			}
		}
		return lAdaptationTags;
	}

	/**
	 * Gets the child scenario tag of game tag with name scenario Id.
	 *
	 * @param aGameTag    the XML parent game tag
	 * @param aScenarioId the name of the scenario
	 * 
	 * @return the scenario tag
	 */
	protected IXMLTag getScenarioTag(IXMLTag aGameTag, String aScenarioId) {
		if (aGameTag != null) {
			for (IXMLTag lScenarioTag : aGameTag.getChildTags(AppConstants.defValueNode)) {
				if (lScenarioTag.getChildValue("name").equals(aScenarioId)) {
					return lScenarioTag;
				}
			}
		}
		return null;
	}

	/**
	 * Adds default player settings for game aGameTag.
	 * 
	 * @param aGameTag      the XML game tag
	 * @param aAdaptationId the name of the adaptation
	 * 
	 * @return true if new player, false otherwise
	 */
	protected boolean addPlayer(IXMLTag aGameTag, String aAdaptationId) {
		String lGameId = aGameTag.getChildValue("name");
		String lPlayerId = Integer.toString(CDesktopComponents.sSpring().getRunGroupAccount().getRgaId());
		try {
			hatAsset.FindPlayerSetting(aAdaptationId, lGameId, lPlayerId);
			return false;
		} catch (ArgumentException e) {
			HATPlayer lHatPlayer = new HATPlayer(aAdaptationId, lGameId, lPlayerId,
					cRunHelper
							.doubleFromString(aGameTag.getCurrentStatusAttribute(AppConstants.statusKeyPRating), 0.75),
					0,
					cRunHelper.doubleFromString(aGameTag.getCurrentStatusAttribute(AppConstants.statusKeyPKFactor),
							0.0075),
					cRunHelper.doubleFromString(aGameTag.getCurrentStatusAttribute(AppConstants.statusKeyPUncertainty),
							1.0),
					LocalDateTime.now());

			hatAsset.getSettings().addPlayer(lHatPlayer);
			return true;
		}
	}

	/**
	 * Adds default scenario settings for scenario aScenarioTag.
	 * 
	 * @param aScenarioTag  the XML scenario tag
	 * @param aAdaptationId the name of the adaptation
	 * @param aGameId       the name of the game
	 * 
	 * @return true if new scenario, false otherwise
	 */
	protected boolean addScenario(IXMLTag aScenarioTag, String aAdaptationId, String aGameId) {
		String lScenarioId = aScenarioTag.getChildValue("name");
		try {
			hatAsset.FindScenarioSetting(aAdaptationId, aGameId, lScenarioId);
			return false;
		} catch (ArgumentException e) {
			HATScenario lHatScenario = new HATScenario(aAdaptationId, aGameId, lScenarioId,
					cRunHelper.doubleFromString(aScenarioTag.getCurrentStatusAttribute(AppConstants.statusKeySRating),
							0.75),
					0,
					cRunHelper.doubleFromString(aScenarioTag.getCurrentStatusAttribute(AppConstants.statusKeySKFactor),
							0.0075),
					cRunHelper.doubleFromString(
							aScenarioTag.getCurrentStatusAttribute(AppConstants.statusKeySUncertainty), 1.0),
					LocalDateTime.now(), cRunHelper.intFromString(
							aScenarioTag.getCurrentStatusAttribute(AppConstants.statusKeyTimeLimit), 900000));

			hatAsset.getSettings().addScenario(lHatScenario);
			return true;
		}
	}

	/**
	 * Adds all game scenarios to the HAT asset.
	 */
	protected void addScenariosAndPlayers() {
		boolean lChanged = false;
		String lMode = getAssetMode();
		if (!hatAsset.getSettings().getMode().toString().equals(lMode)) {
			hatAsset.getSettings().setMode(HATMode.valueOf(lMode));
			lChanged = true;
		}
		for (IXMLTag lAdaptationTag : getAdaptationTags()) {
			String lAdaptationId = lAdaptationTag.getChildValue("name");
			for (IXMLTag lGameTag : lAdaptationTag.getChilds("game")) {
				if (addPlayer(lGameTag, lAdaptationId))
					lChanged = true;
				String lGameId = lGameTag.getChildValue("name");
				for (IXMLTag lScenarioTag : lGameTag.getChilds("scenario")) {
					if (addScenario(lScenarioTag, lAdaptationId, lGameId))
						lChanged = true;
				}
			}
		}
		if (lChanged) {
			hatAsset.SaveSettings(cRunHelper.getRageSettingsId());
		}
	}

	/**
	 * Handles status change due to firing of script actions.
	 *
	 * @param aTriggeredReference the a triggered reference
	 */
	@Override
	public void handleStatusChange(STriggeredReference aTriggeredReference) {
	}

	@Override
	public void handleBaseStatusChange(IECaseComponent aCaseComponent, IXMLTag aDataTag, IXMLTag aStatusTag,
			String aStatusKey, String aStatusValue) {
		if (aDataTag.getName().equals("game")) {
			if (aStatusKey.equals(AppConstants.statusKeyOpened) && aStatusValue.equals(AppConstants.statusValueTrue)) {
				// new game; determine best fitting scenario for current player
				// it's up to the Emergo game script if this scenario is actually used
				try {
					String lCurrentScenario = hatAsset.TargetScenarioID(aDataTag.getParentTag().getChildValue("name"),
							aDataTag.getChildValue("name"),
							Integer.toString(CDesktopComponents.sSpring().getRunGroupAccount().getRgaId()));
					IXMLTag lScenarioTag = getScenarioTag(aDataTag, lCurrentScenario);
					if (lScenarioTag != null) {
						setRunTagStatus(getCaseComponent(), lScenarioTag, AppConstants.statusKeyOpened,
								AppConstants.statusValueTrue, true);
					}
				} catch (ArgumentException | ArgumentOutOfRangeException lEx) {
					CDesktopComponents.sSpring().getSLogHelper().logAction(
							"could not determine current scenario for game " + aDataTag.getChildValue("name"));
				}
			}

		} else if (aDataTag.getName().equals("scenario")) {
			if (aStatusKey.equals(AppConstants.statusKeyStarted) && aStatusValue.equals(AppConstants.statusValueTrue)) {
				setRunTagStatus(getCaseComponent(), aDataTag, AppConstants.statusKeyStartTime,
						Long.toString(System.currentTimeMillis()), true);
			} else if (aStatusKey.equals(AppConstants.statusKeyFinished)
					&& aStatusValue.equals(AppConstants.statusValueTrue)) {
				double lStartTime = cRunHelper
						.doubleFromString(aDataTag.getCurrentStatusAttribute(AppConstants.statusKeyStartTime), 0);
				double lCurrentTime = System.currentTimeMillis();
				String lAdaptationId = aDataTag.getParentTag().getParentTag().getChildValue("name");
				String lGameId = aDataTag.getParentTag().getChildValue("name");
				String lScenarioId = aDataTag.getChildValue("name");
				String lPlayerId = Integer.toString(CDesktopComponents.sSpring().getRunGroupAccount().getRgaId());
				double lAnswer = cRunHelper
						.doubleFromString(aDataTag.getCurrentStatusAttribute(AppConstants.statusKeyScore), 0);
				try {
					hatAsset.UpdateRatings(lAdaptationId, lGameId, lPlayerId, lScenarioId, lCurrentTime - lStartTime,
							lAnswer);
					String lNewScenario = hatAsset.TargetScenarioID(lAdaptationId, lGameId, lPlayerId);
					if (!lNewScenario.equals(lScenarioId)) {
						IXMLTag lScenarioTag = getScenarioTag(aDataTag.getParentTag(), lNewScenario);
						setRunTagStatus(getCaseComponent(), lScenarioTag, AppConstants.statusKeyOpened,
								AppConstants.statusValueTrue, true);
					}
				} catch (DOMException | XPathExpressionException | ArgumentException | TransformerException
						| ParserConfigurationException | SAXException | IOException | ArgumentOutOfRangeException e) {
					CDesktopComponents.sSpring().getSLogHelper()
							.logAction("could not update player and scenario ratings: " + e.toString());
					_log.error(e);
				}
			}
		}
		// String lScenarioSettings =
		// CDesktopComponents.sSpring().getCurrentTagStatus(aDataTag,AppConstants.statusKeyGameSettings);
	}

	/**
	 * Cleanup when session ends. Asset should be removed from AssetManager
	 * (currently not yet implemented).
	 */
	@Override
	public void cleanup() {
		hatAsset = null;
		super.cleanup();
	}

}
