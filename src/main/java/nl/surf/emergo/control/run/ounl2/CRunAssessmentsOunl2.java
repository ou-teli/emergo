/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl2;

import nl.surf.emergo.control.run.CRunArea;
import nl.surf.emergo.control.run.ounl.CRunAssessmentsOunl;
import nl.surf.emergo.control.run.ounl.CRunComponentDecoratorOunl;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunAssessmentsOunl2 is used to show assessments within the run view area of the
 * Emergo player.
 */
public class CRunAssessmentsOunl2 extends CRunAssessmentsOunl {

	private static final long serialVersionUID = 5082570900028115623L;

	/**
	 * Instantiates a new c run assessments.
	 */
	public CRunAssessmentsOunl2() {
		super();
	}

	/**
	 * Instantiates a new c run assessments.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the assessments case component
	 * @param aOnTablet the a on tablet
	 */
	public CRunAssessmentsOunl2(String aId, IECaseComponent aCaseComponent, boolean aOnTablet) {
		super(aId, aCaseComponent, aOnTablet);
	}

	@Override
	protected CRunComponentDecoratorOunl createDecorator() {
		return new CRunComponentDecoratorOunl2();
	}

	@Override
	protected CRunArea createButton (String aId, String aEventAction,
			Object aEventActionStatus, String aLabel, String aZclassExtension,
			String aClientOnClickAction) {
		return new CRunLabelButtonOunl2(aId, aEventAction, aEventActionStatus, aLabel, aZclassExtension, aClientOnClickAction);
	}

}
