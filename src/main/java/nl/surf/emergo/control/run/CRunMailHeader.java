/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zul.Label;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefLabel;

/**
 * The Class CRunMailHeader. Used to receiver/sender and subject of a received or
 * sent mail.
 */
public class CRunMailHeader extends CRunVbox {

	private static final long serialVersionUID = -5369392890762223307L;

	/** The mailtag. */
	protected IXMLTag mailtag = null;

	/**
	 * Instantiates a new c run mail header.
	 */
	public CRunMailHeader() {
		super();
		setZclass(getClassName());
	}

	/**
	 * Gets the mail tag.
	 *
	 * @return the mail tag
	 */
	public IXMLTag getMailTag() {
		return mailtag;
	}

	/**
	 * Sets the mail tag.
	 *
	 * @param aMailTag the new mail tag
	 */
	public void setMailTag(IXMLTag aMailTag) {
		mailtag = aMailTag;
	}

	/**
	 * On create fill mailtag and show mail header.
	 *
	 * @param aEvent the a event
	 */
	public void onCreate(CreateEvent aEvent) {
		setMailTag((IXMLTag) (aEvent.getArg()).get("item"));
		// create all necessary components within window
		createComponents();
	}

	/**
	 * Shows receiver/sender and subject.
	 */
	public void createComponents() {
		if (mailtag != null) {
			String lReceiverName = CDesktopComponents.sSpring().unescapeXML(mailtag.getStatusChildValue("receivername"));
			String lSenderName = CDesktopComponents.sSpring().unescapeXML(mailtag.getStatusChildValue("sendername"));
			Label lLabel = null;
			if (!lReceiverName.equals("")) {
				lLabel = new CDefLabel(CDesktopComponents.vView().getLabel("run_mail.receiver")
						+ " " + lReceiverName);
				appendChild(lLabel);
			}
			if (!lSenderName.equals("")) {
				lLabel = new CDefLabel(CDesktopComponents.vView().getLabel("run_mail.sender") + " "
						+ lSenderName);
				appendChild(lLabel);
			}
			String lSubject = CDesktopComponents.sSpring().unescapeXML(mailtag.getStatusChildValue("title"));
			lLabel = new CDefLabel(lSubject);
			appendChild(lLabel);
		}
	}
}