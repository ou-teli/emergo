/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Html;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Progressmeter;
import org.zkoss.zul.Timer;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.utilities.PropsValues;
import nl.surf.emergo.view.VView;

public class CRunIspotViewersDiv extends CDefDiv {

	private static final long serialVersionUID = 4418926675538914718L;

	protected VView vView = CDesktopComponents.vView();

	protected CRunIspot runIspot = null;

	public void onCreate(CreateEvent aEvent) {
		runIspot = (CRunIspot) vView.getComponent("runIspot");
	}

	public void onIspotShowText(Event aEvent) {
		Clients.evalJavaScript("showWebcamMessage('');");
		Events.sendEvent("onIspotHideViewers", this, null);
		((Html) vView.getComponent("ispotTextViewer")).setContent((String) aEvent.getData());
		runIspot.setComponentVisible(vView.getComponent("ispotTextViewerDiv"), true);
	}

	public void onIspotShowPlayer(Event aEvent) {
		Clients.evalJavaScript("showWebcamMessage('');");
		Events.sendEvent("onIspotHideViewers", this, null);
		String url = vView.getAbsoluteUrl((String) aEvent.getData());
		runIspot.setCurrentPlayedUrl(url);
		Include ispotPlayerViewer = (Include) vView.getComponent("ispotPlayerViewer");
		ispotPlayerViewer.setAttribute("url", url);
		ispotPlayerViewer.setAttribute("playerName", "EM_fl_ispot");
		ispotPlayerViewer.setAttribute("showControls", "true");
		ispotPlayerViewer.setAttribute("video_size_width", "830px");
		ispotPlayerViewer.setAttribute("video_size_height", "467px");
		ispotPlayerViewer.setAttribute("runComponentId", runIspot.getId());
		ispotPlayerViewer.setSrc("");
		String fileExtension = vView.getFileExtension(url);
		if (fileExtension.matches("flv|mp4|webm|ogv|fla|mp3")) {
			ispotPlayerViewer.setSrc(VView.v_run_flash_fr);
		}
		runIspot.setComponentVisible(vView.getComponent("ispotPlayerViewerDiv"), true);
	}

	public void onIspotShowWebcam(Event aEvent) {
		boolean lStoreLocal = !PropsValues.STREAMING_SERVER_STORE;
		if (runIspot.registerNewRecording(lStoreLocal)) {
			Clients.evalJavaScript(
					"showWebcamMessage('" + vView.getLabel("run_ispot.message.waitforinitialization") + "');");
			CRunIspotIntervention dataElement = (CRunIspotIntervention) aEvent.getData();
			setAttribute("dataElement", dataElement);
			Include ispotWebcamViewer = (Include) vView.getComponent("ispotWebcamViewer");
			ispotWebcamViewer.setAttribute("runComponentId", runIspot.getId());
			ispotWebcamViewer.setAttribute("webcamContainerId", runIspot.webcamContainerId);
			ispotWebcamViewer.setAttribute("seeSelf", "" + dataElement.isSeeSelf());
			ispotWebcamViewer.setAttribute("recordingName", dataElement.getRecordingName());
			ispotWebcamViewer.setAttribute("boxId", runIspot.webcamBoxId);
			ispotWebcamViewer.setSrc(VView.v_run_webcam_fr);
			runIspot.setComponentVisible(vView.getComponent("ispotWebcamViewerDiv"), true);
		} else
			Clients.evalJavaScript(
					"showWebcamMessage('" + vView.getLabel("run_ispot.error.webcam.recordLimitExceeded") + "');");
	}

	public void onIspotWebcamError(Event aEvent) {
		String lMessage = vView.getLabel("run_ispot.error.webcam." + aEvent.getData());
		if ((lMessage == null) || lMessage.isEmpty())
			lMessage = "error message: " + aEvent.getData();
		Clients.evalJavaScript("showWebcamMessage('" + lMessage + "');");
	}

	public void onIspotRecordingStarted(Event aEvent) {
		CRunIspotIntervention dataElement = (CRunIspotIntervention) aEvent.getData();
		if (!dataElement.isSeeSelf())
			Clients.evalJavaScript(
					"showWebcamMessage('" + vView.getLabel("run_ispot.message.recordingstarted") + "');");
		else
			Clients.evalJavaScript("showWebcamMessage('');");
		Events.sendEvent("onIspotShowWebcamProgress", this, null);
		Events.sendEvent("onIspotShowNumberOfRemainingResits", this, null);
	}

	public void onIspotRecordingPaused(Event aEvent) {
		CRunIspotIntervention dataElement = (CRunIspotIntervention) aEvent.getData();
		if (!dataElement.isSeeSelf())
			Clients.evalJavaScript("showWebcamMessage('" + vView.getLabel("run_ispot.message.recordingpaused") + "');");
		else
			Clients.evalJavaScript("showWebcamMessage('');");
		Events.sendEvent("onIspotShowWebcamPaused", this, null);
	}

	public void onIspotRecordingResumed(Event aEvent) {
		CRunIspotIntervention dataElement = (CRunIspotIntervention) aEvent.getData();
		if (!dataElement.isSeeSelf())
			Clients.evalJavaScript(
					"showWebcamMessage('" + vView.getLabel("run_ispot.message.recordingresumed") + "');");
		else
			Clients.evalJavaScript("showWebcamMessage('');");
		Events.sendEvent("onIspotShowWebcamResumed", this, null);
	}

	public void onIspotShowWebcamProgress(Event aEvent) {
		if (runIspot.recordingShowProgress) {
			runIspot.recordingTimerCount = 0;
			((Timer) vView.getComponent("ispotWebcamTimer")).start();
			((Progressmeter) vView.getComponent("ispotWebcamProgressmeter")).setValue(0);
			runIspot.setComponentVisible(vView.getComponent("ispotWebcamProgressmeter"), true);
			runIspot.setComponentVisible(vView.getComponent("ispotWebcamProgressDiv"), true);
		}
	}

	public void onIspotShowWebcamPaused(Event aEvent) {
		if (runIspot.recordingShowProgress) {
			((Timer) vView.getComponent("ispotWebcamTimer")).stop();
		}
	}

	public void onIspotShowWebcamResumed(Event aEvent) {
		if (runIspot.recordingShowProgress) {
			((Timer) vView.getComponent("ispotWebcamTimer")).start();
		}
	}

	public void onIspotHideWebcamProgress(Event aEvent) {
		if (runIspot.recordingShowProgress) {
			((Timer) vView.getComponent("ispotWebcamTimer")).stop();
			runIspot.setComponentVisible(vView.getComponent("ispotWebcamProgressmeter"), false);
		}
		runIspot.setComponentVisible(vView.getComponent("ispotWebcamProgressDiv"), false);
		Clients.evalJavaScript("showWebcamMessage('" + vView.getLabel("run_ispot.message.waitforrecording") + "');");
	}

	public void onIspotRecordingTimeOut(Event aEvent) {
		Events.sendEvent("onIspotHideWebcamProgress", this, null);
		// NOTE only vignettes can have max duration
		Events.sendEvent("onIspotStopRecording", vView.getComponent("ispotVignette"), null);
	}

	public void onIspotShowWebcamInstruction(Event aEvent) {
		runIspot.setComponentVisible(vView.getComponent("ispotWebcamInstructionDiv"), true);
	}

	public void onIspotHideWebcamInstruction(Event aEvent) {
		runIspot.setComponentVisible(vView.getComponent("ispotWebcamInstructionDiv"), false);
	}

	public void onIspotShowNumberOfRemainingResits(Event aEvent) {
		runIspot.setComponentVisible(vView.getComponent("ispotWebcamInstructionDiv"), false);
		if (runIspot.getCurrentRecordingId().equals(runIspot.getFeedbackInterventionId()))
			return;
		CRunIspotIntervention dataElement = (CRunIspotIntervention) getAttribute("dataElement");
		int numberOfResits = dataElement.getNumberOfResits();
		int numberOfReactions = dataElement.getNumberOfReactions();
		if (numberOfResits > 0 && numberOfReactions <= numberOfResits) {
			((Label) vView.getComponent("ispotNumberOfRemainingResits"))
					.setValue(vView.getLabel("run_ispot.header.number_of_remaining_resits") + ": "
							+ (numberOfResits - numberOfReactions));
			runIspot.setComponentVisible(vView.getComponent("ispotNumberOfRemainingResitsDiv"), true);
		}
	}

	public void onIspotShowFeedbackButtons(Event aEvent) {
		Clients.evalJavaScript("showWebcamMessage('');");
		Events.sendEvent("onIspotHideViewers", this, null);
		Events.sendEvent("onInit", vView.getComponent("ispotFeedbackBtnsViewer"), null);
	}

	public void onIspotHideViewers(Event aEvent) {
		runIspot.setComponentVisible(vView.getComponent("ispotTextViewerDiv"), false);
		((Include) vView.getComponent("ispotPlayerViewer")).setSrc("");
		runIspot.setComponentVisible(vView.getComponent("ispotPlayerViewerDiv"), false);
		runIspot.setComponentVisible(vView.getComponent("ispotWebcamInstructionDiv"), false);
		((Include) vView.getComponent("ispotWebcamViewer")).setSrc("");
		runIspot.setComponentVisible(vView.getComponent("ispotWebcamViewerDiv"), false);
		runIspot.setComponentVisible(vView.getComponent("ispotWebcamProgressDiv"), false);
		runIspot.setComponentVisible(vView.getComponent("ispotNumberOfRemainingResitsDiv"), false);
		runIspot.setComponentVisible(vView.getComponent("ispotFeedbackBtnsViewerDiv"), false);
	}

}
