/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.spl;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.run.CRunComponentHelper;
import nl.surf.emergo.control.run.CRunConversationInteraction;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunTutorialConversationsSpl is used to show conversation videos and images within the tutorial
 * component. Conversation interaction is shown within the run choice area of the player.
 */
public class CRunTutorialConversationsSpl extends CRunConversationsSpl {

	private static final long serialVersionUID = -3992171365897636879L;

	public boolean isChangeNoteContext() {
		return false;
	}

	/**
	 * Instantiates a new c run conversations.
	 * Creates views for image and for video.
	 * Gets current conversation, sets selected and opened of it to true and starts it.
	 */
	public CRunTutorialConversationsSpl() {
		super();
	}

	/**
	 * Instantiates a new c run conversations.
	 * Creates views for image and for video.
	 * Gets current conversation, sets selected and opened of it to true and starts it.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the a case component
	 */
	public CRunTutorialConversationsSpl(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
	}

	/**
	 * Instantiates a new c run conversations.
	 * Creates views for image and for video.
	 * Gets current conversation, sets selected and opened of it to true and starts it.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the a case component
	 * @param aConversationTag the a conversation tag
	 * @param aRunConversationInteraction the a run conversation interaction
	 */
	public CRunTutorialConversationsSpl(String aId, IECaseComponent aCaseComponent, IXMLTag aConversationTag, CRunConversationInteraction aRunConversationInteraction) {
		super(aId, aCaseComponent, aConversationTag, aRunConversationInteraction);
	}

	@Override
	public CRunComponentHelper getRunComponentHelper() {
		return new CRunTutorialConversationsHelperSpl(null, "question,map,alternative,field", caseComponent, this);
	}

}
