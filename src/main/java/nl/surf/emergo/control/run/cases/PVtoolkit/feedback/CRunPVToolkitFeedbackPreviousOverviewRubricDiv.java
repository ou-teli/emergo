/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.feedback;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Div;
import org.zkoss.zul.Messagebox;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitSkillWheelOverviewTipsTopsDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitSkillWheelRecordingDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTag;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.view.VView;

public class CRunPVToolkitFeedbackPreviousOverviewRubricDiv extends CRunPVToolkitFeedbackRubricDiv {

	private static final long serialVersionUID = 9100911172063527385L;

	protected IXMLTag _originalCurrentStepTag;
	
	protected IXMLTag _originalCurrentTag;

	public void init(IERunGroup peer, IXMLTag currentStepTag, IXMLTag currentTag, boolean editable, boolean unfolded) {
		_showPreviousOverview = true;

		_classPrefix = "feedbackOverview";
		
		super.init(peer, currentStepTag, currentTag, editable, unfolded);
	}

	public void update() {
		_originalCurrentStepTag = _currentStepTag;
		_originalCurrentTag = _currentTag;
		//NOTE get previous practice tags to render previous overview
		List<IXMLTag> previousCurrentStepTags = pvToolkit.getStepTagsInPreviousCycles(_originalCurrentStepTag, _currentStepTag.getChildValue("steptype"));
		_currentStepTag = previousCurrentStepTags.get(previousCurrentStepTags.size() - 1);
		//NOTE overview is only used if feedback is given to another user
		//if feedback is given on an existing video example of the whole skill, this code is not used
		_currentTag = pvToolkit.getFeedbackPracticeTag(_actor, _currentStepTag, !_peerFeedback, _originalCurrentTag, null, null);

		super.update();
		
		Div div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{"titleRight"}
		);
		
		String labelKey = "";
		String value = "";
		String feedbackReceiverName = "";
		if (!_peerFeedback) {
			labelKey = "PV-toolkit-feedback.header.selffeedbackPreviousOverview";
			value = CDesktopComponents.vView().getLabel(labelKey);
		}
		else {
			labelKey = "PV-toolkit-feedback.header.feedbackPreviousOverview";
			if (_currentTag != null) {
				feedbackReceiverName = pvToolkit.getRgaName(_currentTag.getAttribute(CRunPVToolkit.practiceRgaId));
				value = CDesktopComponents.vView().getLabel(labelKey).replace("%1", feedbackReceiverName);
			}
		}
		CDesktopComponents.cControl().setRunSessAttr("feedbackReceiverName", feedbackReceiverName);
		
		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "value"}, 
				new Object[]{"font titleRight", value}
		);
		
		Component btn = new CRunPVToolkitDefButton(this, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font pvtoolkitButton " + _classPrefix + "RecordingButton", "PV-toolkit-skillwheel.button.recording"}
				);
		addToRecordingOnClickEventListener(btn, this);
	
		btn = new CRunPVToolkitDefButton(this, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font pvtoolkitButton " + _classPrefix + "OverviewTipsTopsButton", "PV-toolkit-skillwheel.button.overviewtipstops"}
				);
		addToOverViewTipsTopsOnClickEventListener(btn, this);
			
		//back button
		Component navBtn = new CRunPVToolkitDefButton(this, 
				new String[]{"class", "cLabelKey"}, 
				new Object[]{"font pvtoolkitButton " + _classPrefix + "BackButton", "PV-toolkit.back"}
				);
		addBackButtonOnClickEventListener(navBtn, _originalCurrentTag);

		//forward button
		Component forwardBtn = new CRunPVToolkitDefButton(this, 
				new String[]{"class", "cLabelKey"}, 
				new Object[]{"font pvtoolkitButton " + _classPrefix + "ForwardButton", "PV-toolkit.forward"}
				);
		addForwardButtonOnClickEventListener(forwardBtn);
		
	}
	
	protected void addToRecordingOnClickEventListener(Component component, Component feedbackPreviousOverviewLevelDiv) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				IERunGroupAccount runGroupAccount = pvToolkit.getRunGroupAccount(_currentTag.getAttribute(CRunPVToolkit.practiceRgaId));
				if (runGroupAccount != null) {
					feedbackPreviousOverviewLevelDiv.setVisible(false);
					CRunPVToolkitSkillWheelRecordingDiv recordingDiv = (CRunPVToolkitSkillWheelRecordingDiv)CDesktopComponents.vView().getComponent(_idPrefix + "RecordingDiv");
					recordingDiv.init(runGroupAccount.getERunGroup(), false, pvToolkit.getCurrentCycleNumber() - 1, 0, feedbackPreviousOverviewLevelDiv.getId());
					recordingDiv.setVisible(true);
				}
			}
		});
	}

	protected void addToOverViewTipsTopsOnClickEventListener(Component component, Component feedbackPreviousOverviewLevelDiv) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				IERunGroupAccount runGroupAccount = pvToolkit.getRunGroupAccount(_currentTag.getAttribute(CRunPVToolkit.practiceRgaId));
				if (runGroupAccount != null) {
					feedbackPreviousOverviewLevelDiv.setVisible(false);
					CRunPVToolkitSkillWheelOverviewTipsTopsDiv overviewTipsTopsDiv = (CRunPVToolkitSkillWheelOverviewTipsTopsDiv)CDesktopComponents.vView().getComponent(_idPrefix + "OverviewTipsTopsDiv");
					initOverviewTipsTopsDiv(overviewTipsTopsDiv);
					overviewTipsTopsDiv.init(runGroupAccount.getERunGroup(), false, pvToolkit.getCurrentCycleNumber() - 1, 0, feedbackPreviousOverviewLevelDiv.getId());
					overviewTipsTopsDiv.setVisible(true);
				}
			}
		});
	}

	protected void initOverviewTipsTopsDiv(CRunPVToolkitSkillWheelOverviewTipsTopsDiv overviewTipsTopsDiv) {
		//NOTE set rga id to filter on to current user _actor, so only tips/tops of _actor are shown. Otherwise tips/tops of other peers are shown as well.
		List<Integer> rgaIdsToFilterOn = new ArrayList<Integer>();
		rgaIdsToFilterOn.add(pvToolkit.getActiveRunGroupAccount(_actor.getRugId()).getRgaId());
		overviewTipsTopsDiv.otherRgaIdsToFilterOn = rgaIdsToFilterOn;
	}

	protected void addBackButtonOnClickEventListener(Component component, IXMLTag currentTag) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				toFeedbackRecordings(_idPrefix + "RubricDiv", _stepPrefix, currentTag);
			}
		});
	}

	protected void addForwardButtonOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				possiblyCopyScoresAndTipsTops();
				
				toFeedback(_idPrefix + "RubricDiv", _stepPrefix, _originalCurrentStepTag, _originalCurrentTag);
			}
		});
	}

	protected void possiblyCopyScoresAndTipsTops() {
		if (!((pvToolkit.hasTeacherRole || pvToolkit.hasStudentAssistantRole) && pvToolkit.getTeacherMayUseScoresOfPreviousCycle())) {
			//NOTE only teacher may be allowed to copy scores and tips/tops from previous cycle
			return;
		}
		//NOTE get previous scores and tips/tops and possibly copy to previous step, and save in database.
		//Only copy if no score and tips/tops are given yet for current step.

		IXMLTag previousStepTag = _currentStepTag;
		IXMLTag previousTag = _currentTag;

		//restore original tags to check if current scores and tips/tops are empty
		_currentStepTag = _originalCurrentStepTag;
		_currentTag = _originalCurrentTag;
		
		//check if current scores and tips/tops are empty
		if (scoresOrTipsTopsAreGiven()) {
			return;
		}
			
		//set previous tags to get previous scores and tips/tips
		_currentStepTag = previousStepTag;
		_currentTag = previousTag;
		//get previous skill data
		Integer previousSkillLevel = getSkillLevel(skillTag);
		String previousSkillTips = getTips(skillTag);
		String previousSkillTops = getTops(skillTag);
		//get previous skill cluster and sub skill data
		List<List<Integer>> previousLevelsPerSkillCluster = new ArrayList<List<Integer>>();
		List<String> previousSkillClusterTips = new ArrayList<String>();
		List<String> previousSkillClusterTops = new ArrayList<String>();
		for (IXMLTag skillclusterTag : skillTag.getChilds("skillcluster")) {
			if (pvToolkit.isRunSpecificTagPresent(new CRunPVToolkitCacAndTag(caseComponent, skillclusterTag))) {
				previousSkillClusterTips.add(getTips(skillclusterTag));
				previousSkillClusterTops.add(getTops(skillclusterTag));
				List<Integer> oldLevels = new ArrayList<Integer>();
				previousLevelsPerSkillCluster.add(oldLevels);
				for (IXMLTag subskillTag : skillclusterTag.getChilds("subskill")) {
					if (pvToolkit.isRunSpecificTagPresent(new CRunPVToolkitCacAndTag(caseComponent, subskillTag))) {
						oldLevels.add(getSubSkillLevel(subskillTag));
					}
				}
			}
		}
			
		VView vView = CDesktopComponents.vView();
		if (vView.showMessagebox(getRoot(), vView.getLabel("PV-toolkit.feedback.copy.previous.cycle.confirmation"), 
			vView.getLabel("PV-toolkit.feedback.copy.previous.cycle"), 
			Messagebox.YES + Messagebox.NO, Messagebox.QUESTION) != Messagebox.YES) {
			//teacher don't want to copy
			return;
		}

		//NOTE to copy scores and tips/tops restore original tags
		_currentStepTag = _originalCurrentStepTag;
		_currentTag = _originalCurrentTag;
		
		//copy skill score and tips/tops
		saveLevel(skillTag, previousSkillLevel);
		saveTips(skillTag, previousSkillTips);
		saveTops(skillTag, previousSkillTops);
		//copy skill cluster and sub skill data
		int skillclusterCounter = 0;
		for (IXMLTag skillclusterTag : skillTag.getChilds("skillcluster")) {
			if (pvToolkit.isRunSpecificTagPresent(new CRunPVToolkitCacAndTag(caseComponent, skillclusterTag))) {
				int subskillCounter = 0;
				//copy skill cluster tips/tops
				saveTips(skillclusterTag, previousSkillClusterTips.get(skillclusterCounter));
				saveTops(skillclusterTag, previousSkillClusterTops.get(skillclusterCounter));
				for (IXMLTag subskillTag : skillclusterTag.getChilds("subskill")) {
					if (pvToolkit.isRunSpecificTagPresent(new CRunPVToolkitCacAndTag(caseComponent, subskillTag))) {
						//copy sub skill score
						saveLevel(subskillTag, previousLevelsPerSkillCluster.get(skillclusterCounter).get(subskillCounter));
						subskillCounter++;
					}
				}
				skillclusterCounter++;
			}
		}
	}

	protected boolean scoresOrTipsTopsAreGiven() {
		//check if current scores and tips/tops are empty
		if (getSkillLevel(skillTag) > -1 ||
				!getTips(skillTag).equals("") || 
				!getTops(skillTag).equals("")) {
			//Only copy if no score and tips/tops are given yet for current step.
			return true;
		}
		for (IXMLTag skillclusterTag : skillTag.getChilds("skillcluster")) {
			if (pvToolkit.isRunSpecificTagPresent(new CRunPVToolkitCacAndTag(caseComponent, skillclusterTag))) {
				if (!getTips(skillclusterTag).equals("") || !getTops(skillclusterTag).equals("")) {
					//Only copy if no tips/tops are given yet for current step.
					return true;
				}
				for (IXMLTag subskillTag : skillclusterTag.getChilds("subskill")) {
					if (pvToolkit.isRunSpecificTagPresent(new CRunPVToolkitCacAndTag(caseComponent, subskillTag))) {
						if (getSubSkillLevel(subskillTag) > -1) {
							//Only copy if no scores are given yet for current step.
							return true;
						}
					}
				}
			}
		}
		return false;
	}

}
