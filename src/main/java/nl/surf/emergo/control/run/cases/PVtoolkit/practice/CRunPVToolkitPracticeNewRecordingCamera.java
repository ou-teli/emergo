/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.practice;

import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zkmax.zul.Camera;
import org.zkoss.zkmax.zul.event.StateChangeEvent;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Timer;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.view.VView;

public class CRunPVToolkitPracticeNewRecordingCamera extends Camera {

	private static final long serialVersionUID = 8739586542561449400L;

	protected String _idPrefix = "";
	protected String _classPrefix = "";
	
	protected int maxWebcamRecordingLength = -1;

	public void init(String idPrefix, String classPrefix) {
		_idPrefix = idPrefix;
		_classPrefix = classPrefix;
		
		CRunPVToolkit pvToolkit = (CRunPVToolkit)CDesktopComponents.vView().getComponent("pvToolkit");
		CRunPVToolkitPracticeNewRecording practiceNewRecording = (CRunPVToolkitPracticeNewRecording)CDesktopComponents.vView().getComponent(_idPrefix);
		maxWebcamRecordingLength = practiceNewRecording.getMaxWebcamRecordingLength();
		if (maxWebcamRecordingLength > 0) {
			setLengthLimit(practiceNewRecording.getMaxWebcamRecordingLength());
		}
		setRecordFormat(pvToolkit.getWebcamRecordFormat());
		setVisible(pvToolkit.getSeeSelf());
	}

	public void onStateChange(StateChangeEvent event) {
		if (event.getState() == Camera.START) {
			CRunPVToolkitPracticeNewRecording practiceNewRecording = (CRunPVToolkitPracticeNewRecording)CDesktopComponents.vView().getComponent(_idPrefix);
			if (maxWebcamRecordingLength > 0) {
				CDesktopComponents.vView().getComponent(_idPrefix + "ProgressDiv").setVisible(true);
				((CRunPVToolkitPracticeNewRecordingProgressTimer)CDesktopComponents.vView().getComponent(_idPrefix + "ProgressTimer")).init(_idPrefix);
				((Timer)CDesktopComponents.vView().getComponent(_idPrefix + "ProgressTimer")).start();
			}
			practiceNewRecording.setWebcamRecordingStarted(true);
		}
		else if (event.getState() == Camera.PAUSE) {
			if (maxWebcamRecordingLength > 0) {
				((Timer)CDesktopComponents.vView().getComponent(_idPrefix + "ProgressTimer")).stop();
			}
		}
		else if (event.getState() == Camera.RESUME) {
			if (maxWebcamRecordingLength > 0) {
				((Timer)CDesktopComponents.vView().getComponent(_idPrefix + "ProgressTimer")).start();
			}
		}
		else if (event.getState() == Camera.STOP) {
			//NOTE sometimes Null pointer exception, so check of components exist
			Component progressDiv = CDesktopComponents.vView().getComponent(_idPrefix + "ProgressDiv");
			if (progressDiv != null) {
				CDesktopComponents.vView().getComponent(_idPrefix + "ProgressDiv").setVisible(false);
			}
			if (maxWebcamRecordingLength > 0) {
				Timer progressTimer = (Timer)CDesktopComponents.vView().getComponent(_idPrefix + "ProgressTimer");
				if (progressTimer != null) {
					progressTimer.stop();
				}
			}
			Component stopRecordingButton = CDesktopComponents.vView().getComponent(_idPrefix + "StopRecordingButton");
			if (stopRecordingButton != null && stopRecordingButton.isVisible()) {
				//NOTE if user has not stopped recording himself but it is stopped due to reaching length limit (see init method above), simulate click on stop button
				Events.echoEvent("onClick", stopRecordingButton, null);
			}
		}
	}

	public void onVideoUpload(UploadEvent event) {
		Media media = event.getMedia();
		CRunPVToolkitPracticeNewRecording practiceNewRecording = (CRunPVToolkitPracticeNewRecording)CDesktopComponents.vView().getComponent(_idPrefix);
		//if (media == null || !(media instanceof Media)) {
		boolean mediaOk = !(media == null || !(media instanceof Media) || media.getName() == null || media.getContentType() == null || media.getFormat() == null);
		if (!mediaOk) {
			VView vView = CDesktopComponents.vView();
			vView.showMessagebox(getRoot(), vView.getCLabel("PV-toolkit-practice.no_upload.confirmation"), vView.getCLabel("PV-toolkit-practice.no_upload"), Messagebox.OK, Messagebox.EXCLAMATION);
			CDesktopComponents.sSpring().getSLogHelper().logAction(practiceNewRecording._logPrefix + "webcam: error in file upload");
			practiceNewRecording.again();
			return;
		}
		if (practiceNewRecording != null) {
			int webcamRecordingLength = -1;
			if (maxWebcamRecordingLength > 0) {
				webcamRecordingLength = ((CRunPVToolkitPracticeNewRecordingProgressTimer)CDesktopComponents.vView().getComponent(_idPrefix + "ProgressTimer")).getTimeInSecs();
			}
			practiceNewRecording.storeVideoAsBlobAndShow(media, true, webcamRecordingLength);
			//practiceNewRecording.showVideoDiv("");
		}
	}

}
