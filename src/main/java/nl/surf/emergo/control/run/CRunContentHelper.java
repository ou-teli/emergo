/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Treeitem;

import nl.surf.emergo.business.IXMLAttributeValueTime;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.business.impl.XMLAttributeValueTime;
import nl.surf.emergo.control.CChooseFileButton;
import nl.surf.emergo.control.CContentHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CGmaps;
import nl.surf.emergo.control.CTree;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpring;

/**
 * The Class CRunContentHelper.
 *
 * Used to overwrite some author methods by player methods, because for instance other layout.
 * But also adds specific player methods.
 */
public class CRunContentHelper extends CContentHelper {

	/** The showtagnames, names of tags that should be shown as content. */
	protected String showtagnames = "";

	/** The case component. */
	protected IECaseComponent caseComponent = null;

	/** The run component, the ZK component showing content title, content and buttons. */
	protected CRunComponent runComponent = null;

	/** The run status type, see IAppManager, default is statusTypeRunGroup. */
	protected String runStatusType = AppConstants.statusTypeRunGroup;

	/** The extendable, if content can be added by users. */
	protected boolean extendable = false;

	/**
	 * Instantiates a new c run component helper.
	 *
	 * @param aContentComponent the a contentcomponent, the ZK component actually showing the content, tree or gmaps
	 * @param aShowTagNames the a show tag names, names of tags that should be shown as content
	 * @param aCaseComponent the a case component
	 * @param aRunComponent the a run component, the ZK component showing content title, content and buttons
	 */
	public CRunContentHelper(Component aContentComponent, String aShowTagNames, IECaseComponent aCaseComponent, CRunComponent aRunComponent) {
		super(aContentComponent);
		showtagnames = aShowTagNames;
		caseComponent = aCaseComponent;
		runComponent = aRunComponent;
		if (runComponent != null) {
			extendable = runComponent.getContentExtendable();
		}
	}

	/**
	 * Instantiates a new c run component helper.
	 *
	 * @param aContentComponent the a contentcomponent, the ZK component actually showing the content, tree or gmaps
	 * @param aShowTagNames the a show tag names, names of tags that should be shown as content
	 * @param aCaseComponent the a case component
	 * @param aRunComponent the a run component, the ZK component showing content title, content and buttons
	 * @param aSSpring the a s spring
	 */
	public CRunContentHelper(Component aContentComponent, String aShowTagNames, IECaseComponent aCaseComponent, CRunComponent aRunComponent, SSpring aSSpring) {
		super(aContentComponent, aSSpring);
		showtagnames = aShowTagNames;
		caseComponent = aCaseComponent;
		runComponent = aRunComponent;
		if (runComponent != null) {
			extendable = runComponent.getContentExtendable();
		}
	}

	/**
	 * Gets the edit view, the page for editing a component item and its associated xml tag.
	 *
	 * @return the edit view
	 */
	@Override
	public String getEditView() {
		return VView.v_run_edit_component;
	}

	/**
	 * Gets new choose file button.
	 *
	 * @return new choose file button
	 */
	@Override
	public CChooseFileButton newChooseFileButton() {
		return new CRunChooseFileButton();
	}

	/**
	 * Gets the run component.
	 *
	 * @return the run component
	 */
	public CRunComponent getRunComponent() {
		return runComponent;
	}

	/**
	 * Sets the run component.
	 *
	 * @param aRunComponent the new run component
	 */
	public void setRunComponent(CRunComponent aRunComponent) {
		runComponent = aRunComponent;
	}

	/**
	 * Gets the run status type.
	 *
	 * @return the run status type
	 */
	public String getRunStatusType() {
		if (runComponent != null)
			return runComponent.getRunStatusType();
		return runStatusType;
	}

	/**
	 * Sets the run status type.
	 *
	 * @param aRunStatusType the new run status type
	 */
	public void setRunStatusType(String aRunStatusType) {
		runStatusType = aRunStatusType;
	}

	/**
	 * Gets the case component.
	 *
	 * @return the case component
	 */
	@Override
	public IECaseComponent getCaseComponent() {
		return caseComponent;
	}

	/**
	 * Gets the current cas id.
	 *
	 * @return the cas id
	 */
	@Override
	protected String getCasId() {
		return ("" + CDesktopComponents.sSpring().getCase().getCasId());
	}

	/**
	 * Gets the component id of the case component.
	 *
	 * @return the cac com id
	 */
	@Override
	protected String getCacComId() {
		if (caseComponent == null)
			return "-1";
		return "" + caseComponent.getEComponent().getComId();
	}

	/**
	 * Gets the cac id.
	 *
	 * @return the cac id
	 */
	@Override
	protected String getCacId() {
		if (caseComponent == null)
			return "-1";
		return "" + caseComponent.getCacId();
	}

	/**
	 * Gets the current car id.
	 *
	 * @return the car id
	 */
	protected String getCarId() {
		if (CDesktopComponents.sSpring().getSCaseRoleHelper().getCaseRole() == null)
			return "-1";
		return "" + CDesktopComponents.sSpring().getSCaseRoleHelper().getCaseRole().getCarId();
	}

	/**
	 * Gets the current rug id.
	 *
	 * @return the rug id
	 */
	protected String getRugId() {
		if (CDesktopComponents.sSpring().getRunGroupAccount() == null)
			return "-1";
		return ("" + CDesktopComponents.sSpring().getRunGroupAccount().getERunGroup().getRugId());
	}

	/**
	 * Gets the current run id.
	 *
	 * @return the run id
	 */
	protected String getRunId() {
		if (CDesktopComponents.sSpring().getRun() == null)
			return "-1";
		return ("" + CDesktopComponents.sSpring().getRun().getRunId());
	}

	/**
	 * Gets the current rut id.
	 *
	 * @return the rut id
	 */
	protected String getRutId() {
		if (CDesktopComponents.sSpring().getRunTeam() == null)
			return "-1";
		return ("" + CDesktopComponents.sSpring().getRunTeam().getRutId());
	}

	/**
	 * Gets the current xml definition tree.
	 * 
	 * @return the xml def tree
	 */
	@Override
	public IXMLTag getXmlDefTree() {
		return CDesktopComponents.sSpring().getXmlDefTree(getCacComId());
	}

	/**
	 * Gets the current xml data plus status tree depending on run status type.
	 *
	 * @return the xml data tree
	 */
	@Override
	public IXMLTag getXmlDataTree() {
		return CDesktopComponents.sSpring().getXmlDataPlusRunStatusTree(getCacId(), getRunStatusType());
	}

	/**
	 * Gets the current xml data plus status string depending on run status type.
	 *
	 * @return the xml data
	 */
	@Override
	public String getXmlData() {
		if (getRunStatusType().equals(AppConstants.statusTypeRunGroup))
			return CDesktopComponents.sSpring().getRunGroupCaseComponentDataPlusStatus(getRugId(), getCacId());
		if (getRunStatusType().equals(AppConstants.statusTypeRun))
			return CDesktopComponents.sSpring().getRunCaseComponentDataPlusStatus(getRunId(), getCacId());
		if (getRunStatusType().equals(AppConstants.statusTypeRunTeam))
			return CDesktopComponents.sSpring().getRunTeamCaseComponentDataPlusStatus(getRutId(), getCacId());
		return "";
	}

	/**
	 * Sets the current xml status depending on run status type.
	 *
	 * @param aRootTag the new xml data
	 */
	@Override
	public void setXmlData(IXMLTag aRootTag) {
		if (getRunStatusType().equals(AppConstants.statusTypeRunGroup)) {
			CDesktopComponents.sSpring().setSaveRunGroupCaseComponentStatusRootTag(getRugId(), getCacId(), aRootTag);
		}
		if (getRunStatusType().equals(AppConstants.statusTypeRun))
			CDesktopComponents.sSpring().setRunCaseComponentStatusRootTag(getRunId(), getCacId(), aRootTag);
		if (getRunStatusType().equals(AppConstants.statusTypeRunTeam))
			CDesktopComponents.sSpring().setRunTeamCaseComponentStatusRootTag(getRutId(), getCacId(), aRootTag);
	}

	/**
	 * Gets the xml status tree for the current case component.
	 *
	 * @return the xml status tree
	 */
	public IXMLTag getXmlStatusTree() {
		return CDesktopComponents.sSpring().getXmlRunStatusTree(caseComponent, getRunStatusType());
	}

	/**
	 * Xml node to rows. Converts xml node tag childs to rows.
	 *
	 * @param aXmlTree the a xml tree
	 * @param aNew the a new
	 * @param aClickedNodeTag the a clicked node tag
	 * @param aNodename the a nodename
	 * @param aNewnodetype the a new node type
	 * @param aRows the a rows
	 */
	@Override
	public void xmlNodeToRows(IXMLTag aXmlTree, boolean aNew,
			IXMLTag aClickedNodeTag, String aNodename, String aNewnodetype, Component aRows) {
		updateWithExternalTags();
		boolean lDeleted = ((CDesktopComponents.sSpring().getCurrentTagStatus(aClickedNodeTag,
				AppConstants.statusKeyDeleted))
				.equals(AppConstants.statusValueTrue));
		if (!lDeleted) {
			super.xmlNodeToRows(aXmlTree, aNew, aClickedNodeTag, aNodename, aNewnodetype, aRows);
		}
		else {
			//TODO give appropriate alert why action is canceled.
		}
	}

	/**
	 * Gets the node child. Looks first in associated status tag. If not found returns data tag child.
	 * 
	 * @param aDataTag the data node tag
	 * @param aChildName the child name
	 * 
	 * @return the child
	 */
	@Override
	public IXMLTag getNodeChild(IXMLTag aDataTag, String aChildName) {
		IXMLTag lChildTag = null;
		//NOTE in case of user generated tag or user adjusted tag (in player) search value in status tag
		IXMLTag lStatusTagStatusChildTag = CDesktopComponents.sSpring().getXmlManager().getStatusTagStatusChildTag(aDataTag);
		if (lStatusTagStatusChildTag != null) {
			lChildTag = lStatusTagStatusChildTag.getChild(aChildName);
		}
		//NOTE if not found in status tag use child in data tag
		if (lChildTag == null) {
			lChildTag = super.getNodeChild(aDataTag, aChildName);
		}
		return lChildTag;
	}
	
	/**
	 * Gets the node child value. Looks first in associated status tag. If not found returns data tag value.
	 * 
	 * @param aDataTag the data node tag
	 * @param aChildName the child name
	 * 
	 * @return the child value
	 */
	@Override
	public String getNodeChildValue(IXMLTag aDataTag, String aChildName) {
		String lValue = "";
		//NOTE in case of user generated tag or user adjusted tag (in player) search value in status tag
		IXMLTag lStatusTagStatusChildTag = CDesktopComponents.sSpring().getXmlManager().getStatusTagStatusChildTag(aDataTag);
		if (lStatusTagStatusChildTag != null) {
			lValue = lStatusTagStatusChildTag.getChildValue(aChildName);
		}
		//NOTE if not found in status tag use value in data tag
		if (lValue.equals("")) {
			lValue = super.getNodeChildValue(aDataTag, aChildName);
		}
		return lValue;
	}
	
	/**
	 * Gets the node child value. Looks first in associated status tag. If not found returns data tag value.
	 * 
	 * @param aChildTag the child tag
	 * 
	 * @return the child value
	 */
	@Override
	public String getNodeChildValue(IXMLTag aChildTag) {
		if (aChildTag.getParentTag() != null &&
				aChildTag.getParentTag().getAttribute(AppConstants.defKeyType).equals(AppConstants.defValueNode)) {
			return getNodeChildValue(aChildTag.getParentTag(), aChildTag.getName());
		}
		else {
			return super.getNodeChildValue(aChildTag);
		}
	}
	
	/**
	 * Gets the node child attribute value. Looks first in associated status tag. If not found returns data tag value.
	 * 
	 * @param aDataTag the data node tag
	 * @param aChildName the child name
	 * @param aKey the attribute key
	 * 
	 * @return the child attribute value
	 */
	@Override
	public String getNodeChildAttribute(IXMLTag aDataTag, String aChildName, String aKey) {
		String lValue = "";
		//NOTE in case of user generated tag or user adjusted tag (in player) search value in status tag
		IXMLTag lStatusTagStatusChildTag = CDesktopComponents.sSpring().getXmlManager().getStatusTagStatusChildTag(aDataTag);
		if (lStatusTagStatusChildTag != null) {
			lValue = lStatusTagStatusChildTag.getChildAttribute(aChildName, aKey);
		}
		//NOTE if not found in status tag use value in data tag
		if (lValue.equals("")) {
			lValue = super.getNodeChildAttribute(aDataTag, aChildName, aKey);
		}
		return lValue;
	}
	
	/**
	 * Gets the node child attribute value. Looks first in associated status tag. If not found returns data tag value.
	 * 
	 * @param aChildTag the child tag
	 * @param aKey the attribute key
	 * 
	 * @return the child attribute value
	 */
	@Override
	public String getNodeChildAttribute(IXMLTag aChildTag, String aKey) {
		if (aChildTag.getParentTag() != null &&
				aChildTag.getParentTag().getAttribute(AppConstants.defKeyType).equals(AppConstants.defValueNode)) {
			return getNodeChildAttribute(aChildTag.getParentTag(), aChildTag.getName(), aKey);
		}
		else {
			return super.getNodeChildAttribute(aChildTag, aKey);
		}
	}
	
	/**
	 * Sets the child attribute value.
	 * 
	 * @param aChildTag the child tag
	 * @param aKey the attribute key
	 * @param aValue the attribute value
	 */
	@Override
	public void setChildAttribute(IXMLTag aChildTag, String aKey, String aValue) {
		aChildTag.addAttributeValueTimeToList(aKey, aValue, CDesktopComponents.sSpring().getCaseTime());
	}
	
	/**
	 * Gets the xml node child. Looks in associated status tag. If not found it is added.
	 *
	 * @param xmlNode the xml node
	 * @param aChildName the a child name
	 *
	 * @return the xml node child
	 */
	@Override
	public IXMLTag getXmlNodeChild(IXMLTag xmlNode, String aChildName) {
		//NOTE use status child tag of status tag instead of xmlNode
		if (xmlNode.getStatusTag() == null) {
			xmlNode.setStatusTag(CDesktopComponents.sSpring().newRunNodeTag(xmlNode.getName(), xmlNode.getAttribute(AppConstants.keyId), null, null));
		}
		IXMLTag lStatusTagStatusChildTag = CDesktopComponents.sSpring().getXmlManager().getStatusTagStatusChildTag(xmlNode);
		//NOTE if status child tag, return it.
		if (aChildName.equals(AppConstants.statusElement)) {
			return lStatusTagStatusChildTag;
		}
		//NOTE if not status child tag return child of status child tag.
//		if child does not exist add it.
		IXMLTag lChild = lStatusTagStatusChildTag.getChild(aChildName);
		if (lChild == null) {
			//NOTE xmlNode is still needed to get definition
			IXMLTag lDefChild = xmlNode.getDefTag().getChild(aChildName);
			if (lDefChild == null)
				return null;
			lChild = lDefChild.cloneWithoutParentAndChildren();
			lChild.setDefTag(lDefChild);
			lChild.setParentTag(lStatusTagStatusChildTag);
			lStatusTagStatusChildTag.getChildTags().add(lChild);
		}
		return lChild;
		
	}

	/**
	 * Converts xml data content part to content items. If the content is extendable the
	 * method of super is called, the root element should be rendered to be able to add content
	 * items. Otherwise the root element is not shown.
	 *
	 * @param aXmlTree the xml data tree
	 * @param aComponent the a component
	 */
	@Override
	public void xmlContentToContentItems(IXMLTag aXmlTree, Component aComponent) {
		if (extendable) {
//			show tree with root element because it can be extended by a run member
			super.xmlContentToContentItems(aXmlTree, aComponent);
			return;
		}
//		show tree without root element because it can not be extended by a run member
		if (aXmlTree == null)
			return;
		IXMLTag lContentTag = aXmlTree.getChild(AppConstants.contentElement);
		if (lContentTag == null)
			return;
		List<IXMLTag> lChildTags = lContentTag.getChildTags(AppConstants.defValueNode);
		if (lChildTags == null)
			return;
		currentCaseComponentCode = getCaseComponent().getEComponent().getCode();
		xmlChildsToContentItems(lChildTags, aComponent, null);
	}

	/**
	 * Creates new sibling xml node tag aItem within xml status of current status tree.
	 * This tag can be added as child of a data tag or as child of another status tag.
	 * And it can be added before a data tag or before another status tag.
	 * Ids of parent and before node are saved as status attributes. Also the owner rga id
	 * is saved as status attribute.
	 *
	 * @param aItem the a item
	 * @param aBeforenode the a beforenode
	 * @param aSaveInDb the a save in db
	 *
	 * @return the error list, empty if ok
	 */
	@Override
	public List<String[]> newSiblingNode(IXMLTag aItem, IXMLTag aBeforenode, boolean aSaveInDb) {
		List<String[]> lErrors = new ArrayList<String[]>();
		IXMLTag lRootTag = getXmlStatusTree();
		IXMLTag lContentTag = lRootTag.getChild(AppConstants.contentElement);
		//NOTE for original data tags (so not user generated) set status tag if not yet available
		if (aItem.getStatusTag() == null && !aItem.getAttribute(AppConstants.keyExtended).equals(AppConstants.statusValueTrue)) {
			aItem.setStatusTag(CDesktopComponents.sSpring().newRunNodeTag(aItem.getName(), aItem.getAttribute(AppConstants.keyId), null, null));
		}
		IXMLTag lStatusTag = aItem.getStatusTag();
		lStatusTag.setDefTag(aItem.getDefTag());
//		first set status tag context before attributes
		addBeforeId(aItem,aBeforenode);
		addBeforeId(lStatusTag,aBeforenode);
		addParentId(aItem,aBeforenode.getParentTag());
		addParentId(lStatusTag,aBeforenode.getParentTag());
		aItem.setAttribute(AppConstants.keyExtended, AppConstants.statusValueTrue);
		lStatusTag.setAttribute(AppConstants.keyExtended, AppConstants.statusValueTrue);
		if (aSaveInDb) {
			addOwnerId(aItem,"" + CDesktopComponents.sSpring().getRunGroupAccount().getRgaId());
			addOwnerId(lStatusTag,"" + CDesktopComponents.sSpring().getRunGroupAccount().getRgaId());
		}
		else
			addOwnerId(lStatusTag,getOwnerId(aItem));
//		create child of content root
		CDesktopComponents.sSpring().getXmlManager().newChildNode(lRootTag, lStatusTag, lContentTag, lErrors);
		aItem.setAttribute(AppConstants.keyRefstatusid, lStatusTag.getAttribute(AppConstants.keyId));
		CDesktopComponents.sSpring().setXmlStatusTagById(getCaseComponent(), lStatusTag, getRunStatusType());
		if (lErrors.size() == 0) {
			if (aSaveInDb)
				setXmlData(lRootTag);
		}
		if (aSaveInDb) {
			while (runComponent.incMaxId > 0) {
				CDesktopComponents.sSpring().getXmlManager().setTagId(aItem);
				runComponent.incMaxId = runComponent.incMaxId - 1;
			}
			CDesktopComponents.sSpring().getXmlManager().setTagId(aItem);
		}
		else
			runComponent.incMaxId = runComponent.incMaxId + 1;
//		set attribute so datatag in memory has reference to status
		aItem.setAttribute(AppConstants.keyRefstatusid, lStatusTag.getAttribute(AppConstants.keyId));
		return lErrors;
	}

	/**
	 * Creates new child xml node tag aItem within xml status of current status tree.
	 * This tag can be added as child of a data tag or as child of another status tag.
	 * Id of parent node is saved as status attributes. Also the owner rga id is saved
	 * as status attribute.
	 *
	 * @param aItem the a item
	 * @param aParentnode the a parentnode
	 * @param aSaveInDb the a save in db
	 *
	 * @return the error list, empty if ok
	 */
	@Override
	public List<String[]> newChildNode(IXMLTag aItem, IXMLTag aParentnode, boolean aSaveInDb) {
		List<String[]> lErrors = new ArrayList<String[]>();
		IXMLTag lRootTag = getXmlStatusTree();
		//NOTE for original data tags (so not user generated) set status tag if not yet available
		if (aItem.getStatusTag() == null && !aItem.getAttribute(AppConstants.keyExtended).equals(AppConstants.statusValueTrue)) {
			aItem.setStatusTag(CDesktopComponents.sSpring().newRunNodeTag(aItem.getName(), aItem.getAttribute(AppConstants.keyId), null, null));
		}
		IXMLTag lStatusTag = aItem.getStatusTag();
		lStatusTag.setDefTag(aItem.getDefTag());
//		first set status tag context parent attributes
		addParentId(aItem,aParentnode);
		addParentId(lStatusTag,aParentnode);
		aItem.setAttribute(AppConstants.keyExtended, AppConstants.statusValueTrue);
		lStatusTag.setAttribute(AppConstants.keyExtended, AppConstants.statusValueTrue);
		if (aSaveInDb) {
			addOwnerId(aItem,"" + CDesktopComponents.sSpring().getRunGroupAccount().getRgaId());
			addOwnerId(lStatusTag,"" + CDesktopComponents.sSpring().getRunGroupAccount().getRgaId());
		}
		else {
			addOwnerId(lStatusTag,getOwnerId(aItem));
		}
		IXMLTag lContentTag = lRootTag.getChild(AppConstants.contentElement);
//		create child of content root
		CDesktopComponents.sSpring().getXmlManager().newChildNode(lRootTag, lStatusTag, lContentTag, lErrors);
		aItem.setAttribute(AppConstants.keyRefstatusid, lStatusTag.getAttribute(AppConstants.keyId));
		CDesktopComponents.sSpring().setXmlStatusTagById(getCaseComponent(), lStatusTag, getRunStatusType());
		if (lErrors.size() == 0) {
			if (aSaveInDb)
				setXmlData(lRootTag);
		}
		if (aSaveInDb) {
			while (runComponent.incMaxId > 0) {
				CDesktopComponents.sSpring().getXmlManager().setTagId(aItem);
				runComponent.incMaxId = runComponent.incMaxId - 1;
			}
			CDesktopComponents.sSpring().getXmlManager().setTagId(aItem);
		}
		else
			runComponent.incMaxId = runComponent.incMaxId + 1;
//		set attribute so datatag in memory has reference to status
		aItem.setAttribute(AppConstants.keyRefstatusid, lStatusTag.getAttribute(AppConstants.keyId));
		return lErrors;
	}

	/**
	 * Adds before id. The id is added as status attribute of aItem. Also if the before node is a data tag
	 * or a status tag is saved.
	 *
	 * @param aItem the a item
	 * @param aBeforenode the a beforenode
	 */
	protected void addBeforeId(IXMLTag aItem, IXMLTag aBeforenode) {
		if (aBeforenode != null) {
			String lRefdataid = aBeforenode.getAttribute(AppConstants.keyRefdataid);
			if (lRefdataid.equals("") && !aBeforenode.getAttribute(AppConstants.keyExtended).equals("true")) {
				lRefdataid = aBeforenode.getAttribute(AppConstants.keyId);
			}
			if (!lRefdataid.equals("")) {
				aItem.setAttribute(AppConstants.keyDatabeforeid, lRefdataid);
			}
			else {
				String lRefstatusid = aBeforenode.getAttribute(AppConstants.keyRefstatusid);
				aItem.setAttribute(AppConstants.keyStatusbeforeid, lRefstatusid);
			}
		}
	}

	/**
	 * Adds parent id. The id is added as status attribute of aItem. Also if the parent node is a data tag
	 * or a status tag is saved.
	 *
	 * @param aItem the a item
	 * @param aParentnode the a parentnode
	 */
	protected void addParentId(IXMLTag aItem, IXMLTag aParentnode) {
		if (aParentnode != null) {
			String lRefdataid = aParentnode.getAttribute(AppConstants.keyRefdataid);
			if (lRefdataid.equals("") && !aParentnode.getAttribute(AppConstants.keyExtended).equals("true")) {
				lRefdataid = aParentnode.getAttribute(AppConstants.keyId);
			}
			if (!lRefdataid.equals("")) {
				aItem.setAttribute(AppConstants.keyDataparentid, lRefdataid);
			}
			else {
				String lRefstatusid = aParentnode.getAttribute(AppConstants.keyRefstatusid);
				aItem.setAttribute(AppConstants.keyStatusparentid, lRefstatusid);
			}
		}
	}

	/**
	 * Gets the rga owner id.
	 *
	 * @param aItem the a item
	 *
	 * @return the owner id
	 */
	protected String getOwnerId(IXMLTag aItem) {
		return aItem.getAttribute(AppConstants.keyOwner_rgaid);
	}

	/**
	 * Adds owner rga id as attribute of aItem.
	 *
	 * @param aItem the a item
	 * @param aId the a id
	 */
	protected void addOwnerId(IXMLTag aItem, String aId) {
		aItem.setAttribute(AppConstants.keyOwner_rgaid,aId);
	}

	/**
	 * Updates existing xml node tag aItem within xml status of current status tree.
	 *
	 * @param aItem the a item
	 * @param aSaveInDb the a save in db
	 *
	 * @return the error list, empty if ok
	 */
	@Override
	public List<String[]> updateNode(IXMLTag aItem, boolean aSaveInDb) {
		List<String[]> lErrors = new ArrayList<String[]>();
		IXMLTag lRootTag = getXmlStatusTree();
		IXMLTag lContentTag = lRootTag.getChild(AppConstants.contentElement);
		//NOTE for original data tags (so not user generated) set status tag if not yet available
		if (aItem.getStatusTag() == null && !aItem.getAttribute(AppConstants.keyExtended).equals(AppConstants.statusValueTrue)) {
			aItem.setStatusTag(CDesktopComponents.sSpring().newRunNodeTag(aItem.getName(), aItem.getAttribute(AppConstants.keyId), null, null));
		}
		IXMLTag lStatusTag = aItem.getStatusTag();
		lStatusTag.setDefTag(aItem.getDefTag());
		if (aItem.getAttribute(AppConstants.keyRefstatusid).equals("")) {
			CDesktopComponents.sSpring().getXmlManager().newChildNode(lRootTag, lStatusTag, lContentTag, lErrors);
			aItem.setAttribute(AppConstants.keyRefstatusid, lStatusTag.getAttribute(AppConstants.keyId));
		}
		else {
			CDesktopComponents.sSpring().getXmlManager().replaceNode(lRootTag, lStatusTag, lErrors);
		}
		CDesktopComponents.sSpring().setXmlStatusTagById(getCaseComponent(), lStatusTag, getRunStatusType());
		if (lErrors.size() == 0) {
			if (aSaveInDb)
				setXmlData(lRootTag);
		}
		return lErrors;
	}

	/**
	 * Removes references to xml tag aItem within xml data of system casecomponent 'case'.
	 * But within player only user generated content and this is not yet coupled to user
	 * generated content of other case components, so there will be no references within
	 * the 'case' case component status.
	 *
	 * @param aItem the xml tag
	 */
	@Override
	public void removeReferences(IXMLTag aItem) {
		// in future has to be filled in for status of casecasecomponent
	}

	/**
	 * Deletes xml status node tag not by deleting it, but by setting status deleted to true.
	 * In this way everything users do within the player is saved.
	 * If content is shared among users, these users, if they are online, are notified about this status change.
	 *
	 * @param aItem the xml tag
	 * @param aSaveInDb the save in db state
	 */
	@Override
	public void deleteNode(IXMLTag aItem, boolean aSaveInDb) {
		if (aSaveInDb) {
			updateWithExternalTags();
		}
		// remove references to tag in system casecasecomponent
		removeReferences(aItem);
		// child will not be removed but gets status deleted
		// set rungrouptagstatus of tag in db
		CDesktopComponents.sSpring().setRunTagStatus(caseComponent, aItem, AppConstants.statusKeyDeleted, AppConstants.statusValueTrue, true, getRunStatusType(), aSaveInDb, false);
		if (aSaveInDb) {
			addExternalUpdateTag("delete", null, aItem);
		}
	}

	/**
	 * Adds external update tag. A tag that should be added in status of other users.
	 *
	 * @param aUpdateType the update type
	 * @param aSelectedTag the selected tag
	 * @param aTag the tag
	 */
	@Override
	public void addExternalUpdateTag(String aUpdateType, IXMLTag aSelectedTag, IXMLTag aTag) {
		boolean compositeRunGroup = CDesktopComponents.sSpring().getRunGroup().getComposite();
		if (!(extendable || compositeRunGroup)) return;
		Hashtable<String,Object> lDataUpdate = new Hashtable<String,Object>();
		lDataUpdate.put("cacid", ""+caseComponent.getCacId());
		lDataUpdate.put("updatetype", aUpdateType);
		if (aSelectedTag != null)
			lDataUpdate.put("selectedtag", aSelectedTag);
		lDataUpdate.put("tag", aTag);
		CDesktopComponents.sSpring().getSUpdateHelper().putExternalDataUpdate(caseComponent, getRunStatusType(),lDataUpdate);
	}

	/**
	 * Adds external update status tag. A tag that should change the status attributes of other users.
	 *
	 * @param aTag the tag
	 * @param aStatusKey the status key
	 * @param aStatusValue the status value
	 */
	@Override
	public void addExternalUpdateStatusTag(IXMLTag aTag, String aStatusKey, String aStatusValue) {
		boolean compositeRunGroup = CDesktopComponents.sSpring().getRunGroup().getComposite();
		if (!(extendable || compositeRunGroup)) return;
		Hashtable<String,Object> lDataUpdate = new Hashtable<String,Object>();
		lDataUpdate.put("cacid", ""+caseComponent.getCacId());
		lDataUpdate.put("updatetype", "updatestatus");
		lDataUpdate.put("tag", aTag);
		lDataUpdate.put("key", aStatusKey);
		lDataUpdate.put("value", aStatusValue);
		CDesktopComponents.sSpring().getSUpdateHelper().putExternalDataUpdate(caseComponent, getRunStatusType(),lDataUpdate);
	}

	/**
	 * Updates existing status with external tags. Gets all update tags for current user and
	 * processes them.
	 */
	@Override
	public void updateWithExternalTags() {
		if (CDesktopComponents.sSpring().getRunGroup() == null) {
			return;
		}
		boolean compositeRunGroup = CDesktopComponents.sSpring().getRunGroup().getComposite();
		if (!(extendable || compositeRunGroup)) return;
//		get external datatags of other rungroupaccounts
		List<Hashtable<String,Object>> lDataUpdates = CDesktopComponents.sSpring().getSUpdateHelper().getExternalDataUpdates(getRunStatusType());
		List<Hashtable<String,Object>> lDataUpdatesToRemove = new ArrayList<Hashtable<String,Object>>();
		currentCaseComponentCode = caseComponent.getEComponent().getCode();
		for (Hashtable<String,Object> lDataUpdate : lDataUpdates) {
			String lCacId = "";
			String lUpdateType = "";
			IXMLTag lSelectedTag = null;
			IXMLTag lTag = null;
			IXMLTag lCurrentSelectedTag = null;
			IXMLTag lCurrentTag = null;
			String lStatusKey = "";
			String lStatusValue = "";
			if (lDataUpdate.containsKey("cacid")) {
				lCacId = (String)lDataUpdate.get("cacid");
			}
			if (lCacId.equals("" + caseComponent.getCacId())) {
				if (lDataUpdate.containsKey("updatetype")) {
					lUpdateType = (String)lDataUpdate.get("updatetype");
				}
				if (lDataUpdate.containsKey("selectedtag")) {
					lSelectedTag = (IXMLTag)lDataUpdate.get("selectedtag");
				}
				if (lDataUpdate.containsKey("tag")) {
					lTag = (IXMLTag)lDataUpdate.get("tag");
				}
				if (lDataUpdate.containsKey("key")) {
					lStatusKey = (String)lDataUpdate.get("key");
				}
				if (lDataUpdate.containsKey("value")) {
					lStatusValue = (String)lDataUpdate.get("value");
				}
				//NOTE if update from other rga then lSelectedTag is the data plus status tag of the other rga.
				//So set lCurrentSelectedTag by the tag for this rga
				if (lSelectedTag != null) {
					lCurrentSelectedTag = CDesktopComponents.sSpring().getXmlManager().getTagById(getXmlDataTree(), lSelectedTag.getAttribute(AppConstants.keyId));
				}
				//NOTE if update from other rga then lTag is the data plus status tag of the other rga.
				//So set lCurrentTag of this rga has to be updated with data of lTag of the other rga
				if (lTag != null) {
					lCurrentTag = CDesktopComponents.sSpring().getXmlManager().getTagById(getXmlDataTree(), lTag.getAttribute(AppConstants.keyId));
				}
				if (lUpdateType.equals("newsibling")) {
					//NOTE only add tag if it does not exist yet
					if (lCurrentTag == null) {
						//NOTE copy lTag in lCurrentTag, also copy status tag
						lCurrentTag = copyTag(lTag, lCurrentSelectedTag.getParentTag());
						if (lCurrentTag != null) {
							newSiblingNode(lCurrentTag, lCurrentSelectedTag, false);
							Component lContentItem = null;
							Component lContentParentItem = null;
							if (currentCaseComponentCode.equals("googlemaps")) {
								lContentItem = ((CGmaps)component).getContentItem(lCurrentSelectedTag.getAttribute(AppConstants.keyId));
								lContentParentItem = lContentItem.getParent();
							}
							else {
								lContentItem = ((CTree)component).getContentItem(lCurrentSelectedTag.getAttribute(AppConstants.keyId));
								lContentParentItem = ((Treeitem)lContentItem).getParentItem();
							}
							if (lContentItem != null) {
								newContentItem(lContentParentItem, lContentItem, lCurrentSelectedTag, lCurrentTag, "sibling");
							}
						}
					}
					lDataUpdatesToRemove.add(lDataUpdate);
				}
				else if (lUpdateType.equals("newchild")) {
					//NOTE only add tag if it does not exist yet
					if (lCurrentTag == null) {
						//NOTE copy lTag in lCurrentTag, also copy status tag
						lCurrentTag = copyTag(lTag, lCurrentSelectedTag);
						if (lCurrentTag != null) {
							newChildNode(lCurrentTag, lCurrentSelectedTag, false);
							Component lContentItem = null;
							Component lContentParentItem = null;
							if (currentCaseComponentCode.equals("googlemaps")) {
								lContentParentItem = ((CGmaps)component).getContentItem(lCurrentSelectedTag.getAttribute(AppConstants.keyId));
							}
							else {
								lContentParentItem = ((CTree)component).getContentItem(lCurrentSelectedTag.getAttribute(AppConstants.keyId));
							}
							if (lContentParentItem != null) {
								newContentItem(lContentParentItem, lContentItem, lCurrentSelectedTag, lCurrentTag, "child");
							}
						}
					}
					lDataUpdatesToRemove.add(lDataUpdate);
				}
				else if (lUpdateType.equals("update")) {
					//NOTE replace child tags of lCurrentTag with lTag
					if (lCurrentTag != null) {
						updateTag(lTag, lCurrentTag);
						updateNode(lCurrentTag, false);
						Component lContentItem = null;
						if (currentCaseComponentCode.equals("googlemaps")) {
							lContentItem = ((CGmaps)component).getContentItem(lCurrentTag.getAttribute(AppConstants.keyId));
						}
						else {
							lContentItem = ((CTree)component).getContentItem(lCurrentTag.getAttribute(AppConstants.keyId));
						}
						if (lContentItem != null) {
							editContentItem(lContentItem.getParent(), lContentItem, lCurrentTag);
						}
					}
					lDataUpdatesToRemove.add(lDataUpdate);
				}
				else if (lUpdateType.equals("delete")) {
					if (lCurrentTag != null) {
						updateTagStatus(lTag, lCurrentTag, AppConstants.statusKeyDeleted);
						deleteNode(lCurrentTag, false);
						Component lContentItem = null;
						if (currentCaseComponentCode.equals("googlemaps")) {
							lContentItem = ((CGmaps)component).getContentItem(lCurrentTag.getAttribute(AppConstants.keyId));
						}
						else {
							lContentItem = ((CTree)component).getContentItem(lCurrentTag.getAttribute(AppConstants.keyId));
						}
						if (lContentItem != null) {
							deleteContentItem(lContentItem);
						}
					}
					lDataUpdatesToRemove.add(lDataUpdate);
				}
				else if (lUpdateType.equals("updatestatus")) {
					//NOTE enrich status attribute of lCurrentTag with lTag
					if (lCurrentTag != null) {
						updateTagStatus(lTag, lCurrentTag, lStatusKey);
						updateNode(lCurrentTag, false);
						Component lContentItem = ((CTree)component).getContentItem(lCurrentTag.getAttribute(AppConstants.keyId));
						if (lContentItem != null) {
							editContentItem(lContentItem.getParent(), lContentItem, lCurrentTag);
						}
					}
					lDataUpdatesToRemove.add(lDataUpdate);
				}
			}
		}
		for (Hashtable<String,Object> lDataUpdateToRemove : lDataUpdatesToRemove) {
			lDataUpdates.remove(lDataUpdateToRemove);
		}
	}

	/**
	 * Copies tag, also copies status tag
	 *
	 * @param aTag the a tag
	 * @param aParentTag the a parent tag
	 *
	 * @return the copied tag
	 */
	protected IXMLTag copyTag(IXMLTag aTag, IXMLTag aParentTag) {
		IXMLTag lCopiedTag = CDesktopComponents.sSpring().getXmlManager().copyTagUnique(aTag, aParentTag, false);
		//TODO status tags of node child tags have to be copied too. And tree items for childs too.
		//NOTE copyTagUnique does not add tag to children of parent
		aParentTag.getChildTags().add(lCopiedTag);
		if (aTag.getStatusTag() != null) {
			IXMLTag lRootTag = getXmlStatusTree();
			IXMLTag lContentTag = lRootTag.getChild(AppConstants.contentElement);
			IXMLTag lStatusTag = CDesktopComponents.sSpring().getXmlManager().copyTagUnique(aTag.getStatusTag(), lContentTag, false);
			//NOTE copyTagUnique does not add tag to children of parent
			lContentTag.getChildTags().add(lStatusTag);
			lCopiedTag.setStatusTag(lStatusTag);
			CDesktopComponents.sSpring().getXmlManager().setTagId(lCopiedTag.getStatusTag());
		}
		return lCopiedTag;
	}

	/**
	 * Updates status attributes and childs
	 *
	 * @param aFromTag the a from tag
	 * @param aToTag the a to tag
	 */
	protected void updateTag(IXMLTag aFromTag, IXMLTag aToTag) {
		IXMLTag lFromNodeChild = getXmlNodeChild(aFromTag, AppConstants.statusElement);
		IXMLTag lToNodeChild = getXmlNodeChild(aToTag, AppConstants.statusElement);
		lToNodeChild.getAttributes().clear();
		if (lFromNodeChild.getAttributes() != null) {
			Hashtable<String,List<IXMLAttributeValueTime>> lAttributes = new Hashtable<String,List<IXMLAttributeValueTime>>(); 
			for (Enumeration<String> keys = lFromNodeChild.getAttributes().keys(); keys.hasMoreElements();) {
				String lKey = new String(keys.nextElement());
				List<IXMLAttributeValueTime> lValueTimes = new ArrayList<IXMLAttributeValueTime>();
				for (IXMLAttributeValueTime lValueTime : lFromNodeChild.getAttributes().get(lKey)) {
					lValueTimes.add(new XMLAttributeValueTime(new String(lValueTime.getValue()), lValueTime.getTime()));
				}
				lAttributes.put(lKey, lValueTimes);
			}
			lToNodeChild.setAttributes(lAttributes);
		}
		lToNodeChild.getChildTags().clear();
		if (lFromNodeChild.getChildTags() != null) {
			List<IXMLTag> lChildTags = new ArrayList<IXMLTag>();
			for (IXMLTag lChildTag : lFromNodeChild.getChildTags()) {
				lChildTags.add(CDesktopComponents.sSpring().getXmlManager().copyTagUnique(lChildTag, lToNodeChild, false));
			}
			lToNodeChild.setChildTags(lChildTags);
		}
	}

	/**
	 * Updates status attribute
	 *
	 * @param aFromTag the a from tag
	 * @param aToTag the a to tag
	 * @param aKey the a key
	 */
	protected void updateTagStatus(IXMLTag aFromTag, IXMLTag aToTag, String aKey) {
		getXmlNodeChild(aToTag, AppConstants.statusElement).addAttributeValueTimeToList(aKey,
				aFromTag.getCurrentStatusAttribute(aKey), aFromTag.getCurrentStatusAttributeTime(aKey));
	}

}