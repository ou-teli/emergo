/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.event.DOMExceptionEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkmax.zul.Camera;
import org.zkoss.zkmax.zul.event.StateChangeEvent;
import org.zkoss.zul.Messagebox;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.view.VView;

public class CRunWebcamRecordingCamera extends Camera {

	private static final long serialVersionUID = -3842426173908413084L;

	protected VView vView = CDesktopComponents.vView();
	protected String _parentId = "";
	
	protected CRunWebcamRecordingLocal webcamRecordingDiv;

	public void init(String parentId) {
		_parentId = parentId;
		
		CRunIspot runIspot = (CRunIspot)vView.getComponent("runIspot");

		setLengthLimit((new Long(runIspot.recordingMaxDuration)).intValue());
		setRecordFormat(((CRunWebcamRecordingLocal)CDesktopComponents.vView().getComponent(_parentId)).getWebcamRecordFormat());
		
	}
	
	protected CRunWebcamRecordingLocal getWebcamRecordingDiv() {
		if (webcamRecordingDiv == null)
			webcamRecordingDiv = (CRunWebcamRecordingLocal)CDesktopComponents.vView().getComponent(_parentId);
		return webcamRecordingDiv;
	}

	public void onStateChange(StateChangeEvent event) {
		if (event.getState() == Camera.START) {
			Clients.evalJavaScript("eventToServer('onRecordingStarted','webcam');");
			getWebcamRecordingDiv().cameraStarted();
		}
		else if (event.getState() == Camera.PAUSE) {
			Clients.evalJavaScript("eventToServer('onRecordingPaused','webcam');");
			getWebcamRecordingDiv().cameraPaused();
		}
		else if (event.getState() == Camera.RESUME) {
			Clients.evalJavaScript("eventToServer('onRecordingResumed','webcam');");
			getWebcamRecordingDiv().cameraResumed();
		}
		else if (event.getState() == Camera.STOP) {
			Clients.evalJavaScript("eventToServer('onRecordingStopped','webcam');");
			getWebcamRecordingDiv().cameraStopped();
		}
	}

	public void onVideoUpload(UploadEvent event) {
		Media media = event.getMedia();
		if (getWebcamRecordingDiv() == null) {
			return;
		}
		boolean error = false;
		if (media == null || !(media instanceof Media)) {
			error = true;
		}
		else {
			String recordingName = getWebcamRecordingDiv().storeVideoAsBlob(media);
			if (recordingName.equals("")) {
				error = true;
			}
		}
		if (error) {
			VView vView = CDesktopComponents.vView();
			vView.showMessagebox(getRoot(), vView.getCLabel("run_ispot.error.webcam.no_upload.confirmation"), vView.getCLabel("run_ispot.error.webcam.no_upload"), Messagebox.OK, Messagebox.EXCLAMATION);
			getWebcamRecordingDiv().again();
		}
	}

	public void onCameraUnavailable(DOMExceptionEvent event) {
		//TODO
		vView.showMessagebox(getRoot(), vView.getCLabel("run_ispot.error.webcam.no_camera.confirmation"), vView.getCLabel("run_ispot.error.webcam.no_camera"), Messagebox.OK, Messagebox.EXCLAMATION);
	}
	
	public void onLengthLimitExceeded(Event event) {
		//TODO
		vView.showMessagebox(getRoot(), "length limit exceeded", "recording too long", Messagebox.OK, Messagebox.INFORMATION);
	}

}
