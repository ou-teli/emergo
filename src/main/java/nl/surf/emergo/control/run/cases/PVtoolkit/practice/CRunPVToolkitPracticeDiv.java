/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.practice;

import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitSubSubStepDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTag;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitNotification;

public class CRunPVToolkitPracticeDiv extends CRunPVToolkitSubSubStepDiv {

	private static final long serialVersionUID = -231978550093844751L;

	protected IXMLTag _practiceStepTag;
	
	protected IXMLTag _practiceTag;
	
	public String getPracticeStatus(IXMLTag practiceTag) {
		if (practiceIsShared(practiceTag)) {
			return "shared";
		}
		else if (practiceHasSelfFeedback(practiceTag)) {
			return "selffeedback";
		}
		return "";
	}
	
	public boolean practiceIsShared(IXMLTag practiceTag) {
		return pvToolkit.getPracticeStatus(_actor, practiceTag, "share").equals(AppConstants.statusValueTrue);
	}
	
	public boolean practiceIsShared() {
		return practiceIsShared(_practiceTag);
	}
	
	public boolean atLeastOnepracticeIsShared(List<IXMLTag> practiceTags) {
		for (IXMLTag practiceTag : practiceTags) {
			if (practiceIsShared(practiceTag)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean practiceHasSelfFeedback(IXMLTag practiceTag) {
		return pvToolkit.getPracticeStatus(_actor, practiceTag, "selffeedback").equals(AppConstants.statusValueTrue);
	}
	
	public boolean practiceHasSelfFeedback() {
		return practiceIsShared(_practiceTag);
	}
	
	public void setPracticeShared(IXMLTag practiceTag, boolean pTeacherFbAsked) {
		pvToolkit.setPracticeShared(_actor, practiceTag, pTeacherFbAsked);

		pvToolkit.mailNotification(CRunPVToolkit.onPracticeShared, pvToolkit.getCurrentStepTag(), practiceTag);

		pvToolkit.publishNotification(new Event(CRunPVToolkit.onPracticeShared, null, new CRunPVToolkitNotification(_actor, pvToolkit.getCurrentStepTag(), null)));
	}
	
	public void setPracticeSelfFeedback(IXMLTag practiceTag) {
		pvToolkit.setPracticeSelfFeedback(_actor, practiceTag);
	}
	
	public void setPracticeSelfFeedback() {
		setPracticeSelfFeedback(_practiceTag);
	}

	public boolean hasSelfFeedbackSubStep() {
		if (pvToolkit == null) {
			pvToolkit = (CRunPVToolkit)CDesktopComponents.vView().getComponent("pvToolkit");
		}
		IXMLTag practiceStepTag = pvToolkit.getCurrentStepTag();
		if (practiceStepTag != null) {
			CRunPVToolkitCacAndTag runPVToolkitCacAndTag = new CRunPVToolkitCacAndTag(pvToolkit.getCurrentProjectsCaseComponent(), practiceStepTag);
			for (IXMLTag subStepTag : pvToolkit.getSubStepTags(runPVToolkitCacAndTag)) {
				if (subStepTag.getChildValue("substeptype").equals(CRunPVToolkit.practiceSelfFeedbackSubStepType)) {
					return true;
				}
			}
		}
		return false;
	}
	
	
	public void toPracticeRecordings(String fromComponentId) {
		Component fromComponent = CDesktopComponents.vView().getComponent(fromComponentId);
		if (fromComponent != null) {
			fromComponent.setVisible(false);
		}
		CRunPVToolkitPracticeRecordingsDiv toComponent = (CRunPVToolkitPracticeRecordingsDiv)CDesktopComponents.vView().getComponent("practiceRecordingsDiv");
		if (toComponent != null) {
			toComponent.update();
			toComponent.setVisible(true);
		}
	}

}
