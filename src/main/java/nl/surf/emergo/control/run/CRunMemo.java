/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.ArrayList;
import java.util.List;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunMemo is used to handle saving a memo. It is not visible for the user.
 */
public class CRunMemo extends CRunArea {

	private static final long serialVersionUID = 18595564930347154L;

	/** The case component, the memo casecomponent. */
	protected IECaseComponent caseComponent = null;

	/** The max string length. */
	static final String memoTitleSeparator = ": ";

	/**
	 * Instantiates a new c run memo.
	 */
	public CRunMemo() {
		super();
		setId("runMemo");
		init();
	}

	/**
	 * Instantiates a new c run memo.
	 *
	 * @param aId the a id
	 */
	public CRunMemo(String aId) {
		super();
		setId(aId);
		init();
	}

	/**
	 *
	 */
	public void init() {
		// there is only one memo component allowed
		caseComponent = CDesktopComponents.sSpring().getCaseComponent("memo", "");
	}

	/**
	 * Save memo if it doesn't exist yet.
	 *
	 * @param aCac the a cac
	 * @param aConversationTag the a conversation tag
	 * @param aFragmentTag the a fragment tag
	 *
	 */
	public void saveMemo(IECaseComponent aCac, IXMLTag aConversationTag, IXMLTag aFragmentTag) {
		if (caseComponent == null || aCac == null || aConversationTag == null || aFragmentTag == null) {
			return;
		}
		// memos are private so statusTypeRunGroup
		IXMLTag lStatusRootTag = CDesktopComponents.sSpring().getXmlRunStatusTree(caseComponent, AppConstants.statusTypeRunGroup);
		if (lStatusRootTag == null) {
			return;
		}
		IXMLTag lStatusContentTag = lStatusRootTag.getChild(AppConstants.contentElement);
		if (lStatusContentTag == null) {
			return;
		}
		String lCacId = "" + aCac.getCacId();
		String lConversationTagId = aConversationTag.getAttribute(AppConstants.keyId);
		String lFragmentTagId = aFragmentTag.getAttribute(AppConstants.keyId);
		// determine if status tag already exists
		List<IXMLTag> lStatusTags = lStatusContentTag.getChildTags();
		IXMLTag lStatusTag = null;
		for (IXMLTag lTempStatusTag : lStatusTags) {
			if (lTempStatusTag.getAttribute(AppConstants.keyRefcacid).equals(lCacId) &&
					lTempStatusTag.getAttribute(AppConstants.keyRefdataid).equals(lFragmentTagId)) {
				lStatusTag = lTempStatusTag;
			}
		}
		if (lStatusTag == null) {
			// if not exists add
			lStatusTag = CDesktopComponents.sSpring().getXmlManager().newNodeTag(caseComponent.getEComponent().getXmldefinition(), "memo");
			lStatusTag.setAttribute(AppConstants.keyRefcacid, lCacId);
			lStatusTag.setAttribute(AppConstants.keyRefdataid, lFragmentTagId);
			lStatusTag.setAttribute("refcontainerid", lConversationTagId);
			String lPid = CDesktopComponents.sSpring().unescapeXML(aConversationTag.getChildValue("name"));
			if (lPid.equals("")) {
				lPid = CDesktopComponents.sSpring().unescapeXML(CDesktopComponents.sSpring().getXmlManager().getTagKeyValues(aConversationTag, aConversationTag.getDefAttribute(AppConstants.defKeyKey)));
			}
			if (aFragmentTag.getParentTag().getName().equals("question")) {
				lPid += memoTitleSeparator + CDesktopComponents.sSpring().unescapeXML(aFragmentTag.getParentTag().getChildValue("text"));
			}
			lStatusTag.getChild("pid").setValue(lPid);
			List<String[]> lErrors = new ArrayList<String[]>();
			CDesktopComponents.sSpring().getXmlManager().newChildNode(lStatusRootTag, lStatusTag, lStatusContentTag, lErrors);
			// save status
			CDesktopComponents.sSpring().setSaveRunGroupCaseComponentStatusRootTag(CDesktopComponents.sSpring().getRunGroup(), caseComponent, lStatusRootTag);
		}
	}

}
