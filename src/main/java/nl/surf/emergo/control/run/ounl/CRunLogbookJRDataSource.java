package nl.surf.emergo.control.run.ounl;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

public class CRunLogbookJRDataSource implements JRDataSource {

	private Object[][] data;

	private int index = -1;

	public CRunLogbookJRDataSource() {
	}

	public boolean next() throws JRException {
		if (data == null) {
			return false;
		}
		index++;
		return (index < data.length);
	}

	public Object getFieldValue(JRField field) throws JRException {
		if (data == null) {
			return null;
		}
		Object value = null;
		String fieldName = field.getName();
		
		if ("context".equals(fieldName)) {
			value = data[index][0];
		} else if ("title".equals(fieldName)) {
			value = data[index][1];
		} else if ("note".equals(fieldName)) {
			value = data[index][2];
		}
		
		return value;
	}

	public void setData(Object[][] data) {
		this.data = data;
	}
}
