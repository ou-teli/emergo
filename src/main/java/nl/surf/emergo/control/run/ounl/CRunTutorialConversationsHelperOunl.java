/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl;

import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.Treerow;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CTree;
import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunConversationsHelperOunl. Helps rendering conversation interaction.
 */
public class CRunTutorialConversationsHelperOunl extends CRunConversationsHelperOunl {

	/**
	 * Instantiates a new c run conversations helper ounl.
	 *
	 * @param aTree the ZK conversations tree
	 * @param aShowTagNames the a show tag names, comma separated, only tags with these names are shown
	 * @param aCaseComponent the conversations case component
	 * @param aRunComponent the a run component, the ZK conversations component
	 */
	public CRunTutorialConversationsHelperOunl(CTree aTree, String aShowTagNames, IECaseComponent aCaseComponent, CRunComponent aRunComponent) {
		super(aTree, aShowTagNames, aCaseComponent, aRunComponent);
	}


	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunConversationsHelper#renderTreecell(org.zkoss.zul.Treeitem, org.zkoss.zul.Treerow, nl.surf.emergo.business.IXMLTag, java.lang.String, boolean)
	 */
	protected Treecell renderTreecell(Treeitem aTreeitem, Treerow aTreerow,
			IXMLTag aItem, String aKeyValues, boolean aAccIsAuthor) {
		Treecell lTreecell = super.renderTreecell(aTreeitem, aTreerow, aItem, aKeyValues, aAccIsAuthor);
		String lStatus = "active";
		if (isOpened(aItem))
			lStatus = "opened";
		if (!isAccessible(aItem))
			lStatus = "inactive";
		// NOTE class name must be in source. No neet solution yet
		String lTreecellZclass = "CRunTutorialConversationInteraction" + "_treecell_"+lStatus;
		if (treeitemCount % 2 == 0)
			lTreecell.setZclass(lTreecellZclass + "_even");
		else
			lTreecell.setZclass(lTreecellZclass + "_odd");
		return lTreecell;
	}

}
