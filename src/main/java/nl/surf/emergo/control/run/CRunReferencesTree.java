/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zul.Treeitem;

import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunReferencesTree.
 */
public class CRunReferencesTree extends CRunTree {

	private static final long serialVersionUID = 146061564195936758L;

	/**
	 * Instantiates a new c run references tree.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the references case component
	 * @param aRunComponent the a run component, the ZK references component
	 */
	public CRunReferencesTree(String aId, IECaseComponent aCaseComponent, CRunComponent aRunComponent) {
		super(aId, aCaseComponent, aRunComponent);
		tagopenednames = "piece,refpiece";
		setRows(23);
		update();
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTree#getRunComponentHelper()
	 */
	@Override
	public CRunComponentHelper getRunComponentHelper() {
		return new CRunReferencesHelper(this, tagopenednames, caseComponent, runComponent);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTree#doTreeitemAction(org.zkoss.zul.Treeitem)
	 */
	@Override
	public void doTreeitemAction(Treeitem aTreeitem) {
/*		IXMLTag tag = getContentItemTag(aTreeitem);
		if (tag == null || sSpring == null)
			return;
//		check if Accessible
		if (!getRunComponentHelper().isAccessible(tag))
			return;
		String lHref = sSpring.getUrl(tag);
		if (lHref.equals(""))
			return;
		sSpring.showHref(lHref); */
	}
}
