/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.superTeacher;

import org.zkforge.ckez.CKeditor;
import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.util.Clients;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitVideoExamplesDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTag;
import nl.surf.emergo.domain.IECaseComponent;

public class CRunPVToolkitSuperTeacherUpdateRubricDiv extends CRunPVToolkitSuperTeacherUpdateRubricSkillDiv {

	private static final long serialVersionUID = -4061130195403736736L;

	public void update() {
		//NOTE new rubric is always stored in XML data and status that is cached within the SSpring class, regardless if preview is read only or not.
		IXMLTag nodeTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get("cacId")), _idMap.get("tagId"));
		if (nodeTag != null) {
			initSkill(nodeTag);
			initSkillElements(nodeTag, false, true, false);
			
			Clients.evalJavaScript("initUpdateRubric('" + skill.toString() + "','" + skillElements.toString() + "','" +
					getColorsAsJsonStr(pvToolkit.getSkillClusterColors()) + "','" +
					getColorsAsJsonStr(pvToolkit.getPerformanceLevelSkillClusterColors()) + "');");
		}
	}
		
	public void onNotify (Event event) {
		JSONObject jsonObject = (JSONObject)event.getData();
		String action = (String)jsonObject.get("action");
		if (action.equals("update_rubric")) {
			setVisible(false);
			CRunPVToolkitSuperTeacherDiv div = (CRunPVToolkitSuperTeacherDiv)CDesktopComponents.vView().getComponent("superTeacherNewRubricDiv");
			if (div != null) {
				div.init(pvToolkit.getCurrentRunGroup(), true, _idMap);
			}
		}
		else if (action.equals("show_richtext")) {
			String richtextFieldZkId = (String)jsonObject.get("richtextFieldZkId");
			String inputText = (String)jsonObject.get("inputText");
			CKeditor fCKeditor = (CKeditor)vView.getComponent(richtextFieldZkId);
			if (fCKeditor != null) {
				//NOTE create clone of fck editor, otherwise if inputText is empty, value shown might be value entered for other element.
				CKeditor newFCKeditor = (CKeditor)fCKeditor.clone();
				Component parent = fCKeditor.getParent();
				fCKeditor.detach();
				parent.appendChild(newFCKeditor);

				newFCKeditor.setValue(inputText);
				
				//NOTE show div after value is set, otherwise previous value may be shown briefly
				Clients.evalJavaScript("showPromptRichtextPopupDiv();");
			}
		}
		else if (action.equals("save_richtext")) {
			String tagId = (String)jsonObject.get("tagId");
			String tagName = (String)jsonObject.get("tagName");
			String richtextFieldZkId = (String)jsonObject.get("richtextFieldZkId");
			String descriptionFieldId = (String)jsonObject.get("descriptionFieldId");
			String description = ((CKeditor)vView.getComponent(richtextFieldZkId)).getValue();
			IXMLTag elementTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get("cacId")), tagId);
			if (elementTag != null) {
				editElementTagChildValue(elementTag, "rubric", "description", description);
			}
		
			Clients.evalJavaScript("updateRichtext('" + tagName + "','" + descriptionFieldId + "','" + description + "');");
		}
		else if (action.equals("edit_performance_level_examples") || action.equals("edit_skill_examples")) {
			IECaseComponent caseComponent = sSpring.getCaseComponent(_idMap.get("cacId"));
			IXMLTag parentTag = null;
			String childTagName = "";
			boolean performanceExamples = false;
			if (action.equals("edit_performance_level_examples")) {
				String tagId = (String)jsonObject.get("tagId");
				parentTag = sSpring.getTag(caseComponent, tagId);
				childTagName = "videoperformancelevelexample";
				performanceExamples = true;
			}
			else {
				IXMLTag rubricTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get("cacId")), _idMap.get("tagId"));
				parentTag = rubricTag.getChild(skillTagName);
				childTagName = "videoskillexample";
				performanceExamples = false;
			}
			if (parentTag != null) {
				CRunPVToolkitVideoExamplesDiv feedbackScoreLevelVideoExamples = (CRunPVToolkitVideoExamplesDiv)CDesktopComponents.vView().getComponent(_idPrefix + "VideoExamplesDiv");
				if (feedbackScoreLevelVideoExamples != null) {
					CRunPVToolkitCacAndTag runPVToolkitCacAndPerformancelevelTag = new CRunPVToolkitCacAndTag(caseComponent, parentTag);
					feedbackScoreLevelVideoExamples.init(runPVToolkitCacAndPerformancelevelTag, childTagName, performanceExamples);
					feedbackScoreLevelVideoExamples.setVisible(true);
				}
			}
		}
		else {
			super.onNotify(event);
		}
	}
	
}
