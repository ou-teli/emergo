/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import org.zkoss.gmaps.Gmarker;
import org.zkoss.zk.ui.Component;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CGmaps;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.zkspring.SSpring;

/**
 * The Class CRunGooglemapsHelper. Helps rendering google maps markers.
 */
public class CRunGooglemapsHelperClassic extends CRunComponentHelperClassic {

	/**
	 * Instantiates a new c run googlemaps helper.
	 * 
	 * @param aGmaps the ZK gmaps
	 * @param aShowTagNames the a show tag names, comma separated, only tags with these names are shown
	 * @param aCaseComponent the conversations case component
	 * @param aRunComponent the a run component, the ZK conversations component
	 */
	public CRunGooglemapsHelperClassic(CGmaps aGmaps, String aShowTagNames, IECaseComponent aCaseComponent, CRunComponentClassic aRunComponent) {
		super(aGmaps, aShowTagNames, aCaseComponent, aRunComponent);
	}

	/**
	 * Instantiates a new c run googlemaps helper.
	 * 
	 * @param aGmaps the ZK gmaps
	 * @param aShowTagNames the a show tag names, comma separated, only tags with these names are shown
	 * @param aCaseComponent the conversations case component
	 * @param aRunComponent the a run component, the ZK conversations component
	 * @param sSpring the a sspring
	 */
	public CRunGooglemapsHelperClassic(CGmaps aGmaps, String aShowTagNames, IECaseComponent aCaseComponent, CRunComponentClassic aRunComponent, SSpring sSpring) {
		super(aGmaps, aShowTagNames, aCaseComponent, aRunComponent, sSpring);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.CRunComponentHelper#accIsAuthor(org.zkoss.zk.ui.Component)
	 */
	@Override
	public boolean accIsAuthor(Component aObject) {
		if (extendable)
			return true;
		return true;
	}
	
	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.CRunContentHelper#renderGmapsItem(org.zkoss.zk.ui.Component, org.zkoss.zk.ui.Component, nl.surf.emergo.business.IXMLTag, java.lang.String)
	 */
	public Component renderGmapsItem(Component aParent, Component aInsertBefore, IXMLTag aItem) {
		Component lGmarker = super.renderGmapsItem(aParent, aInsertBefore, aItem);
		return decorateGmapsItem(lGmarker, aItem);
	}
	
	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.CRunContentHelper#renderGmapsItem(org.zkoss.zk.ui.Component, org.zkoss.zk.ui.Component, nl.surf.emergo.business.IXMLTag, java.lang.String)
	 */
	public Component reRenderGmapsItem(Component aParent, Component aComponent, IXMLTag aItem) {
		Component lGmarker = super.reRenderGmapsItem(aParent, aComponent, aItem);
		return decorateGmapsItem(lGmarker, aItem);
	}
	
	protected Component decorateGmapsItem(Component aComponent, IXMLTag aItem) {
		Component lGmarker = aComponent;
		if (lGmarker instanceof Gmarker) {
			boolean lAccIsAuthor = false;
			boolean lExtendable = runComponent.getContentExtendable();
			if (lExtendable) {
				boolean lContentOwnership = runComponent.getContentOwnership();
				String lOwnerRugId = "";
				IXMLTag lTag = (IXMLTag)((Gmarker)lGmarker).getAttribute("item");
				if (lTag != null)
					lOwnerRugId = lTag.getAttribute(AppConstants.keyOwner_rgaid);
				boolean lOwner = (lOwnerRugId.equals(""+CDesktopComponents.sSpring().getRunGroupAccount().getRgaId()));
				lAccIsAuthor = ((!lContentOwnership) || (lOwner));
			}
			if (lAccIsAuthor)
				((Gmarker)lGmarker).setDraggable("true");
			else
				((Gmarker)lGmarker).setDraggable("false");
		}
		if ((lGmarker instanceof Gmarker) && (aItem.getAttribute(AppConstants.keyExtended).equals(AppConstants.statusValueTrue))) {
			((Gmarker)lGmarker).setIconImage("gmaps/marker_hex.png");
			String lOwnerRgaId = aItem.getAttribute(AppConstants.keyOwner_rgaid);
			if (!lOwnerRgaId.equals(""))
				((Gmarker)lGmarker).setTooltiptext(((Gmarker)lGmarker).getTooltiptext() + 
						" (" +CDesktopComponents.sSpring().getRunGroupAccount(Integer.parseInt(lOwnerRgaId)).getERunGroup().getName() + ")");
		}
		return lGmarker;
	}
	
}