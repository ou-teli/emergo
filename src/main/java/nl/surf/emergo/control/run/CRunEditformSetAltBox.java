/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Div;
import org.zkoss.zul.Html;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefBox;

public class CRunEditformSetAltBox extends CDefBox {

	private static final long serialVersionUID = -9179350990142659306L;

	public void onCreate(CreateEvent aEvent) {
		CRunWnd runWnd = (CRunWnd) CDesktopComponents.vView().getComponent(CControl.runWnd);
		CRunEditforms runEditforms = (CRunEditforms)CDesktopComponents.vView().getComponent("runEditforms");
		if (runEditforms == null) {
			return;
		}
		int width = Integer.parseInt(runWnd.getNumberStr(runEditforms.sSpringHelper.getTagChildValue(((IXMLTag)getAttribute("tag")).getParentTag(), "width", ""), "-1"));
		if (width >= 0) {
			setStyle("width:" + width + "px;");
		}
		//insert content on right position
		Div div = new Div();
		if (((String)getAttribute("childcheckposition")).equals("left") || ((String)getAttribute("childcheckposition")).equals("top")) {
			appendChild(div);
		}
		else {
			insertBefore(div, getChildren().get(0));
		}
		div.setSclass("CRunEditformAltHtmlDiv" + getAttribute("childcheckposition"));
		if (((String)getAttribute("childcheckposition")).equals("left") || ((String)getAttribute("childcheckposition")).equals("right")) {
			if (width >= 0) {
				div.setStyle("width:" + (width - 27) + "px;");
			}
		}
		Html html = new Html();
		div.appendChild(html);
		html.setContent((String)getAttribute("content"));
		html.setSclass("CRunEditformAltHtml");

	}

	public void onClick(Event aEvent) {
		Events.postEvent("onClick", getFellow("editformItemCheckbox_" + ((IXMLTag)getAttribute("tag")).getAttribute("id")), null);
	}

}
