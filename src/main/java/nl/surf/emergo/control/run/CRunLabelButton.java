/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zhtml.Table;
import org.zkoss.zhtml.Td;
import org.zkoss.zhtml.Tr;
import org.zkoss.zul.Label;

import nl.surf.emergo.control.def.CDefLabel;
import nl.surf.emergo.control.def.CDefTable;
import nl.surf.emergo.control.def.CDefTd;
import nl.surf.emergo.control.def.CDefTr;

/**
 * The Class CRunLabelButton, is ancestor of all label buttons within the Emergo player.
 */
public class CRunLabelButton extends CRunArea {

	private static final long serialVersionUID = -2965948004103602620L;

	/** The event action. */
	protected String eventAction = "";

	/** The event action status. */
	protected Object eventActionStatus = null;

	/** The label. */
	protected Label labelComp = null;

	/** The label str. */
	protected String label = "";

	/** The disabled. */
	protected boolean disabled = false;

	/** The label. */
	protected String classNamePlusExtension = "";

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
		createLabelButton();
	}

	/**
	 * Instantiates a new c run label button.
	 */
	public CRunLabelButton() {
		// NOTE for usage in zul file
		classNamePlusExtension = getClassName();
		setZclass(classNamePlusExtension);
	}

	/**
	 * Instantiates a new c run label button.
	 *
	 * @param aId the a id
	 * @param aEventAction the a event action, to be send when button is clicked
	 * @param aEventActionStatus the a event action status, when button is clicked
	 * @param aLabel the a label of the button
	 * @param aZclassExtension the a s class extension for the style class
	 * @param aClientOnClickAction the a client action to be executed when clicked
	 */
	public CRunLabelButton(String aId, String aEventAction,
			Object aEventActionStatus, String aLabel, String aZclassExtension,
			String aClientOnClickAction) {
		if (!aId.equals(""))
			setId(aId);
		setEventAction(aEventAction);
		setEventActionStatus(aEventActionStatus);
		label = aLabel;
		classNamePlusExtension = getClassName() + aZclassExtension;
		setZclass(classNamePlusExtension);
		createLabelButton();
		if ((aClientOnClickAction != null) && (!aClientOnClickAction.equals("")))
			setWidgetListener("onClick", aClientOnClickAction);
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
		createLabelButton();
	}

	/**
	 * Shows bread crumbs.
	 *
	 */
	protected void createLabelButton() {
		getChildren().clear();
		String lClassName = classNamePlusExtension;
		if (disabled)
			lClassName += "Disabled";
		this.setSclass(lClassName);
		Table table = new CDefTable();
		table.setDynamicProperty("border", "0");
		table.setDynamicProperty("cellspacing", "0");
		table.setDynamicProperty("cellpadding", "0");
		table.setDynamicProperty("height", "29");
		appendChild(table);

		// Add top row for possibly positioning text vertically
		Tr tr = new CDefTr();
		table.appendChild(tr);
		Td td = new CDefTd();
		td.setSclass(classNamePlusExtension + "Top");
		tr.appendChild(td);

		tr = new CDefTr();
		table.appendChild(tr);
		td = new CDefTd();
		lClassName = classNamePlusExtension + "Left";
		if (disabled)
			lClassName += "Disabled";
		td.setSclass(lClassName);
		tr.appendChild(td);
		td = new CDefTd();
		td.setSclass(classNamePlusExtension + "Label");
		tr.appendChild(td);
		labelComp = new CDefLabel();
		lClassName = classNamePlusExtension + "Label";
		if (disabled)
			lClassName += "Disabled";
		labelComp.setSclass(lClassName);
		labelComp.setValue(this.label);
		td.appendChild(labelComp);
		td = new CDefTd();
		lClassName = classNamePlusExtension + "Right";
		if (disabled)
			lClassName += "Disabled";
		td.setSclass(lClassName);
		tr.appendChild(td);
	}

	/**
	 * Sets the event action.
	 *
	 * @param aEventAction the new event action
	 */
	public void setEventAction(String aEventAction) {
		eventAction = aEventAction;
	}

	/**
	 * Sets the event action status.
	 *
	 * @param aEventActionStatus the new event action status
	 */
	public void setEventActionStatus(Object aEventActionStatus) {
		eventActionStatus = aEventActionStatus;
	}

	/**
	 * On click notify observers with eventAction and eventActionStatus.
	 */
	public void onClick() {
		if (!isDoubleClick()) {
			if (!disabled)
				notifyObservers(eventAction, eventActionStatus);
		}
	}
}
