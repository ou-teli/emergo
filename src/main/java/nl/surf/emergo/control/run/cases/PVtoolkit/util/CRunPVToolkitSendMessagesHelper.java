/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.doomdark.uuid.UUIDGenerator;

import nl.surf.emergo.business.IRunGroupAccountManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefHelper;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTag;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTags;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.zkspring.SSpring;

public class CRunPVToolkitSendMessagesHelper {

	protected SSpring sSpring;

	protected IRunGroupAccountManager runGroupAccountManager;
	protected IECaseComponent projectsCaseComponent;
	protected IXMLTag methodTag;
	
	public CRunPVToolkitSendMessagesHelper(SSpring sSpring, IECaseComponent projectsCaseComponent, IXMLTag methodTag) {
		this.sSpring = sSpring;
		runGroupAccountManager = (IRunGroupAccountManager)sSpring.getBean("runGroupAccountManager");

		setProjectsCaseComponent(projectsCaseComponent);
		setMethodTag(methodTag);
	}
	
	public void setRunStatus(String runStatus) {
		sSpring.setRunStatus(AppConstants.runStatusRun);
	}
	
	public void setRun(IERun run) {
		sSpring.setRun(run);
	}
	
	public void setRunGroupAccount(IERunGroupAccount runGroupAccount) {
		sSpring.setRunGroupAccount(runGroupAccount);
	}
	public IECaseComponent getProjectsCaseComponent() {
		return projectsCaseComponent;
	}

	public void setProjectsCaseComponent(IECaseComponent projectsCaseComponent) {
		this.projectsCaseComponent = projectsCaseComponent;
	}

	public IXMLTag getMethodTag() {
		return methodTag;
	}

	public void setMethodTag(IXMLTag methodTag) {
		this.methodTag = methodTag;
	}

	protected IXMLTag getRunStatusTag(IXMLTag statusRootTag, IXMLTag dataNodeTag) {
		if (statusRootTag == null || dataNodeTag == null) {
			return null;
		}
		IXMLTag statusContentTag = statusRootTag.getChild(AppConstants.contentElement);
		if (statusContentTag == null) {
			return null;
		}
		String tagId = dataNodeTag.getAttribute(AppConstants.keyId);
		String tagName = dataNodeTag.getName();
		for (IXMLTag tempRunStatusTag : statusContentTag.getChilds(tagName)) {
			if (tempRunStatusTag.getAttribute(AppConstants.keyRefdataid).equals(tagId)) {
				return tempRunStatusTag;
			}
		}
		return null;
	}

	protected CRunPVToolkitCacAndTags getPeerGroupTags(IERun run) {
		List<IXMLTag> xmlTags = new ArrayList<IXMLTag>();
		CRunPVToolkitCacAndTag cacAndTag = new CRunPVToolkitCacAndTag(getProjectsCaseComponent(), getRunProjectsStatusChildTag(run, getMethodTag()));
		if (cacAndTag != null || cacAndTag.getXmlTag() != null) {
			xmlTags = cacAndTag.getXmlTag().getChilds("peergroup");
		}
		return new CRunPVToolkitCacAndTags(getProjectsCaseComponent(), xmlTags);
	}
	
	protected IXMLTag getPeerGroupMemberTag(IXMLTag peerGroupTag, IERunGroupAccount runGroupAccount) {
		if (peerGroupTag == null) {
			return null;
		}
		String rgaId = "" + runGroupAccount.getRgaId();
		IXMLTag statusChildTag = peerGroupTag.getChild(AppConstants.statusElement);
		if (statusChildTag == null) {
			return null;
		}
		
		for (IXMLTag peerGroupMemberTag : statusChildTag.getChilds("member")) {
			if (getStatusChildTagAttribute(peerGroupMemberTag, "rgaid").equals(rgaId)) {
				return peerGroupMemberTag;
			}
		}
	
		return null;
	}
	
	protected List<IXMLTag> getPeerGroupMemberTags(IERunGroup runGroup) {
		List<IXMLTag> peerGroupMemberTags = new ArrayList<IXMLTag>();
		List<IERunGroupAccount> tempRunGroupAccounts = runGroupAccountManager.getAllRunGroupAccountsByRugId(runGroup.getRugId());
		if (tempRunGroupAccounts.size() == 0) {
			return peerGroupMemberTags;
		}
		CRunPVToolkitCacAndTags cacAndTags = getPeerGroupTags(runGroup.getERun());
		for (IXMLTag peerGroupTag : cacAndTags.getXmlTags()) {
			IXMLTag peerGroupMemberTag = null;
			//NOTE assume only one run group account exists
			peerGroupMemberTag = getPeerGroupMemberTag(peerGroupTag, tempRunGroupAccounts.get(0));
			if (peerGroupMemberTag != null) {
				peerGroupMemberTags.add(peerGroupMemberTag);
			}
		}
		return peerGroupMemberTags;
	}

	protected List<String> getPeerGroupRoles(IERunGroup runGroup) {
		List<String> peerGroupRoles = new ArrayList<String>();
		if (runGroup == null) {
			return peerGroupRoles;
		}
		List<IXMLTag> peerGroupMemberTags = getPeerGroupMemberTags(runGroup);
		for (IXMLTag peerGroupMemberTag : peerGroupMemberTags) {
			peerGroupRoles.add(getStatusChildTagAttribute(peerGroupMemberTag, "role"));
		}
		return peerGroupRoles;
	}
	
	protected int[] getNumbersOfPeerGroupRoles(IERunGroup runGroup, String[] roles) {
		int[] numbersOfPeerGroupRoles = new int[roles.length];
		if (runGroup == null) {
			return numbersOfPeerGroupRoles;
		}
		List<String> peerGroupRoles = getPeerGroupRoles(runGroup);
		for (String peerGroupRole : peerGroupRoles) {
			for (int i=0;i<roles.length;i++) {
				if (peerGroupRole.equals(roles[i])) {
					numbersOfPeerGroupRoles[i]++;
				}
			}
		}
		return numbersOfPeerGroupRoles;
	}
	
	protected boolean roleMayReceiveMessage(IERunGroupAccount runGroupAccount, IXMLTag stepTag) {
		if (stepTag == null) {
			return true;
		}
		//NOTE check role
		int[] numbersOfPeerGroupRoles = getNumbersOfPeerGroupRoles(runGroupAccount.getERunGroup(), new String[]{
				CRunPVToolkit.peerGroupStudentRole,
				CRunPVToolkit.peerGroupTeacherRole,
				CRunPVToolkit.peerGroupStudentAssistantRole,
				CRunPVToolkit.peerGroupPassiveTeacherRole,
				CRunPVToolkit.peerGroupPeerStudentRole
				});
		return (numbersOfPeerGroupRoles[0] > 0) ||
				((stepTag.getChildValue("steptype").equals(CRunPVToolkit.feedbackStepType) && (numbersOfPeerGroupRoles[1] > 0) || (numbersOfPeerGroupRoles[4] > 0)));
	}

	public boolean mayReceiveMessage(IERunGroupAccount runGroupAccount, IXMLTag stepTag, boolean defaultReceiveValue) {
		if (!runGroupAccount.getERunGroup().getActive()) {
			return false;
		}

		//NOTE check role
		if (!roleMayReceiveMessage(runGroupAccount, stepTag)) {
			return false;
		}
		//NOTE check if all sub steps are finished
		IXMLTag rootTag = sSpring.getXmlDataPlusRunGroupStatusTree(runGroupAccount.getERunGroup().getRugId(), getProjectsCaseComponent());
		if (rootTag == null) {
			return false;
		}
		if (stepTag == null) {
			//no sub steps so may receive mail
			return defaultReceiveValue;
		}
		String lUserId = runGroupAccount.getEAccount().getUserid();
		String stepTagId = stepTag.getAttribute(AppConstants.keyId);
		for (IXMLTag nodeTag : CDesktopComponents.cScript().getNodeTags(rootTag)) {
			if (nodeTag.getName().equals("step") && nodeTag.getAttribute(AppConstants.keyId).equals(stepTagId) &&
					!nodeTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse)) {
				boolean subStepsFinished = true;
				for (IXMLTag subStepTag : nodeTag.getChilds("substep")) {
					sSpring.getSLogHelper().logDebug("Checking finished of substep '" + subStepTag.getChildValue("pid") + "' of account " + lUserId);
					if (!nodeTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse) &&
							!subStepTag.getCurrentStatusAttribute(AppConstants.statusKeyFinished).equals(AppConstants.statusValueTrue)) {
						subStepsFinished = false;
						sSpring.getSLogHelper().logDebug("Substep not finished.");
						break;
					}
				}
				if (!subStepsFinished) {
					return true;
				}
			}
		}
		return false;
	}

	public void sendMessage(List<IERunGroupAccount> runGroupAccounts, IECaseComponent messagesCaseComponent, IXMLTag messageTag, IXMLTag stepTag, boolean isPreviewRun) {
		if (messagesCaseComponent == null || messageTag == null) {
			return;
		}
		String mailAddresses = "";
		for (IERunGroupAccount runGroupAccount : runGroupAccounts) {
			mailAddresses = handleMessage(runGroupAccount, messagesCaseComponent, messageTag, stepTag, mailAddresses);
		}
		if (!mailAddresses.equals("")) {
			mailNotification(mailAddresses, sSpring.unescapeXML(messageTag.getChildValue("title")), sSpring.unescapeXML(messageTag.getChildValue("text")), isPreviewRun);
		}
	}

	public void sendMessage(IERunGroupAccount runGroupAccount, IECaseComponent messagesCaseComponent, IXMLTag messageTag, IXMLTag stepTag, boolean isPreviewRun) {
		List<IERunGroupAccount> runGroupAccounts = new ArrayList<IERunGroupAccount>();
		runGroupAccounts.add(runGroupAccount);
		sendMessage(runGroupAccounts, messagesCaseComponent, messageTag, stepTag, isPreviewRun);
	}

	public IXMLTag getRunProjectsStatusChildTag(IERun run, IXMLTag dataNodeTag) {
		//NOTE get 'status' child tag of the run status tag, i.e. the tag that has to be stored in run progress
		IXMLTag runStatusTag = getRunStatusTag(sSpring.getXmlRunStatusTree(run.getRunId(), getProjectsCaseComponent()), dataNodeTag);
		if (runStatusTag != null) {
			return runStatusTag.getChild(AppConstants.statusElement);
		}
		return null;
	}
	
	public String getStatusChildTagAttribute(IXMLTag tag, String attributeKey) {
		return sSpring.unescapeXML(tag.getChildAttribute(AppConstants.statusElement, attributeKey)).replace(AppConstants.statusCommaReplace, ",");
	}
	
	protected String handleMessage(IERunGroupAccount runGroupAccount, IECaseComponent messagesCaseComponent, IXMLTag messageTag, IXMLTag stepTag, String mailAddresses) {
		if (!mailAddresses.equals("")) {
			mailAddresses += ",";
		}
		mailAddresses += runGroupAccount.getEAccount().getEmail();

		//NOTE set status child tags for message tag
		String tagName = "messagedetails";
		IXMLTag tag = sSpring.getXmlManager().newXMLTag(tagName, "");
		tag.setAttribute(tagName + "uuid", UUIDGenerator.getInstance().generateTimeBasedUUID().toString());
		//NOTE always set rga id to current rga id, because status will always be saved for the current rga
		tag.setAttribute(tagName + "rgaid", "" + runGroupAccount.getRgaId());
		tag.setAttribute("messagedetailsreceiverrgaid",  "" + runGroupAccount.getRgaId());

		IXMLTag statusChildTag = sSpring.getXmlManager().newXMLTag(AppConstants.statusElement, "");
		tag.getChildTags().add(statusChildTag);
		statusChildTag.setParentTag(tag);

		statusChildTag.setAttribute("steptagid", stepTag != null ? stepTag.getAttribute(AppConstants.keyId) : "");
		statusChildTag.setAttribute("senddate", CDefHelper.getDateStrAsDMY(new Date()));
		statusChildTag.setAttribute("sendtime", CDefHelper.getDateStrAsHMS(new Date()));
		statusChildTag.setAttribute("send", "true");

		List<IXMLTag> statusChildTags = new ArrayList<IXMLTag>();
		statusChildTags.add(tag);
		//NOTE set sent of message tag to true for rungroup
		String xmlData = sSpring.getSUpdateHelper().getXmlDataTagForTagStatusUpdate(messageTag.getName(), messageTag.getAttribute(AppConstants.keyId), AppConstants.statusKeySent, AppConstants.statusValueTrue, statusChildTags, false);
		sSpring.getSUpdateHelper().saveXmlDataComponentOrTagStatusUpdate(runGroupAccount.getERunGroup(), messagesCaseComponent.getCacId(), xmlData);
		sSpring.getSLogHelper().logDebug("Mail sent stored in update table for account " + runGroupAccount.getEAccount().getUserid());

		return mailAddresses;
	}

	protected void mailNotification(String mailAddresses, String messageTitle, String messageText, boolean isPreviewRun) {
		if (!StringUtils.isEmpty(mailAddresses)) {
			//NOTE only add bccs if not preview run
			if (!isPreviewRun) {
				String notificationMailAdminBccs = sSpring.unescapeXML(getStateValueAsString(CRunPVToolkit.messageStateAdminBccs)).replace(AppConstants.statusCommaReplace, ",");
				if (!notificationMailAdminBccs.equals(""))
					mailAddresses += "," + notificationMailAdminBccs;
			}
			sSpring.getSLogHelper().logDebug("Mail sent to bcc: " + mailAddresses);
			sSpring.getAppManager().sendMail(sSpring.getAppManager().getSysvalue(AppConstants.syskey_smtpnoreplysender), null, null, mailAddresses, messageTitle, messageText);
		}
	}

	public String getStateValueAsString(String stateTagIdKey) {
		IECaseComponent lStatesCaseComponent = (sSpring.getCaseComponent(sSpring.getCase(), "", CRunPVToolkit.gCaseComponentNameStates));
		List<String> errorMessages = new ArrayList<String>();
		return sSpring.getCurrentRunTagStatus(lStatesCaseComponent, "state", stateTagIdKey, AppConstants.statusKeyValue, AppConstants.statusTypeRunGroup, errorMessages);
	}

}
