/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.spl;

import nl.surf.emergo.control.run.CRunArea;
import nl.surf.emergo.control.run.ounl.CRunComponentDecoratorOunl;
import nl.surf.emergo.control.run.ounl.CRunMailOunl;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunMailSpl is used to show the mail component within the run view area of the
 * Emergo player.
 */
public class CRunMailSpl extends CRunMailOunl {

	private static final long serialVersionUID = 7755759927587678299L;

	/**
	 * Instantiates a new c run mail.
	 */
	public CRunMailSpl() {
		super();
	}

	/**
	 * Instantiates a new c run mail.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the mail case component
	 */
	public CRunMailSpl(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
	}

	@Override
	protected CRunComponentDecoratorOunl createDecorator() {
		return new CRunComponentDecoratorSpl();
	}

	@Override
	protected CRunArea createNewMailButton(String aId, String aEventAction, String aLabel) {
		return new CRunLabelButtonSpl(aId, aEventAction, "", aLabel, "", "");
	}

}
