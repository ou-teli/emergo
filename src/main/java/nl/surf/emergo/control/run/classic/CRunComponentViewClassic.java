/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import java.util.List;

import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CIObserved;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunComponentView, a view within the view area in which run components are shown.
 */
public class CRunComponentViewClassic extends CRunVboxClassic {

	private static final long serialVersionUID = 2720445537863619201L;

	/** The act case component. */
	protected IECaseComponent actCaseComponent = null;

	/** The act component, the current run component shown within the area. */
	protected CRunComponentClassic actRunComponent = null;

	/**
	 * Instantiates a new c run component view.
	 * 
	 * @param aId the a id
	 */
	public CRunComponentViewClassic(String aId) {
		super();
		setId(aId);
	}

	/**
	 * On action. Show content within view depending on parameters. It is either an empack action or a location action.
	 * Either an empack component is shown or a component that is only shown on location.
	 * 
	 * @param sender the sender
	 * @param action the action, a string identifying the type of action
	 * @param status the status, an object containing status info
	 */
	public void onAction(String sender, String action, Object status) {
		if (action.equals("showEmpackAction"))
			showEmpackAction((IECaseComponent) status);
		if (action.equals("showLocationAction")) {
			showLocationAction((List<IECaseComponent>) status);
		}
	}

	/**
	 * Shows empack action. Shows case component content within area for case component on empack.
	 * For instance for references.
	 * 
	 * @param aCaseComponent the a case component
	 */
	public void showEmpackAction(IECaseComponent aCaseComponent) {
		if (aCaseComponent == null)
			return;
		getChildren().clear();
		actCaseComponent = aCaseComponent;
		actRunComponent = null;
		String lCode = actCaseComponent.getEComponent().getCode();
		if (lCode.equals("references"))
			actRunComponent = new CRunReferencesClassic("runReferences", actCaseComponent);
		if (lCode.equals("mail"))
			actRunComponent = new CRunMailClassic("runMail", actCaseComponent);
		if (lCode.equals("assessments"))
			actRunComponent = new CRunAssessmentsClassic("runAssessments", actCaseComponent);
		if (lCode.equals("logbook"))
			actRunComponent = new CRunLogbookClassic("runLogbook", actCaseComponent);
		if (lCode.equals("googlemaps"))
			actRunComponent = new CRunGooglemapsClassic("runGooglemaps", actCaseComponent);
		if (lCode.equals("canon"))
			actRunComponent = new CRunCanonClassic("runCanon", actCaseComponent);
		if (lCode.equals("canonresult"))
			actRunComponent = new CRunCanonResultClassic("runCanonResult", actCaseComponent);
		if (actRunComponent != null)
			appendChild(actRunComponent);
	}

	/**
	 * Shows location action. Shows case component content within area for case component on location.
	 * For instance for conversations.
	 * If more than one item within aCaseComponents the first item is used.
	 * 
	 * @param aCaseComponents the a case components which should be shown
	 */
	public void showLocationAction(List<IECaseComponent> aCaseComponents) {
		// check if casecomponent is instance of conversations component
		getChildren().clear();
		actCaseComponent = (IECaseComponent) aCaseComponents.get(0);
		actRunComponent = null;
		String lCode = actCaseComponent.getEComponent().getCode();
		if (lCode.equals("conversations"))
			actRunComponent = new CRunConversationsClassic("runConversations", actCaseComponent);
		if (lCode.equals("references"))
			actRunComponent = new CRunReferencesClassic("runReferences", actCaseComponent);
		if (lCode.equals("assessments"))
			actRunComponent = new CRunAssessmentsClassic("runAssessments", actCaseComponent);
		if (actRunComponent != null)
			appendChild(actRunComponent);
	}

	/**
	 * Is called when a run component is closed by a user.
	 * 
	 * @param aObserved the observed object, it should have interface CIObserved implemented
	 * @param aAction the action
	 * @param aStatus the status object, can be String or other class
	 */
	@Override
	public void observedNotify(CIObserved aObserved, String aAction, Object aStatus) {
		if (aAction.equals("endComponent"))
			endComponent();
	}

	/**
	 * Ends component. Closes component and set opened of case component to false.
	 */
	public void endComponent() {
		getChildren().clear();
		if (actCaseComponent != null)
			CDesktopComponents.sSpring().setRunComponentStatus(actCaseComponent, AppConstants.statusKeyOpened, AppConstants.statusValueFalse, true, AppConstants.statusTypeRunGroup, true);
//		maybe add something for shared component
	}

	/**
	 * Gets the current run component.
	 * 
	 * @return the act run component
	 */
	public CRunComponentClassic getActRunComponent() {
		return actRunComponent;
	}

}
