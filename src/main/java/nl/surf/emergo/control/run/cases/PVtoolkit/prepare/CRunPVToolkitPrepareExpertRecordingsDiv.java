/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.prepare;

import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Label;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitSkillWheelLevel2Div;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTag;
import nl.surf.emergo.domain.IERunGroup;

public class CRunPVToolkitPrepareExpertRecordingsDiv extends CRunPVToolkitPrepareRecordingsDiv {

	private static final long serialVersionUID = -7886544929602606865L;

	@Override
	public void init(IERunGroup actor, boolean reset, boolean editable, String subStepType) {
		_subStepsIdPostfix = "SubStepDiv3";		
		
		super.init(actor, reset, editable, subStepType);
	}
	
	@Override
    protected Rows appendRecordingsGrid(Component parent) {
    	return pvToolkit.appendGrid(
    			parent, 
    			getId() + "Grid",
    			"position:absolute;left:0px;top:0px;width:1060px;height:480px;overflow:auto;", 
    			null,
    			new String[] {"3%", "80%", "17%"}, 
    			new boolean[] {false, true, true},
    			null,
    			"PV-toolkit-prepare.column.label.",
    			new String[] {"", "videoexample", "compare"});
    }

	@Override
	protected int appendRecordingsToGrid(Rows rows, int rowNumber) {
		return appendRecordingsToGrid(
				rows, 
				rowNumber,
				new boolean[] {true, true, true} 
				);
	}

	@Override
	protected int appendRecordingsToGrid(
			Rows rows, 
			int rowNumber,
    		boolean[] showColumn 
			) {
		List<IXMLTag> skillExampleTags = pvToolkit.getPresentChildTags(new CRunPVToolkitCacAndTag(caseComponent, skillTag), "videoskillexample");
		if (skillExampleTags == null || skillExampleTags.size() == 0) {
			return rowNumber;
		}

		for (IXMLTag skillExampleTag : skillExampleTags) {
			if (_compareWithExperts && feedbackIsReady(skillExampleTag)) {
				Row row = new Row();
				rows.appendChild(row);
				
				int columnNumber = 0;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					new CRunPVToolkitDefImage(row, 
							new String[]{"class", "src"}, 
							new Object[]{"gridImage", zulfilepath + "video.svg"}
							);
				}
	
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					String name = pvToolkit.getTagChildValue(new CRunPVToolkitCacAndTag(caseComponent, skillExampleTag), "name");
					new CRunPVToolkitDefLabel(row, 
							new String[]{"class", "value"}, 
							new Object[]{"font gridLabel", name}
							);
				}
	
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					Label link = new CRunPVToolkitDefLabel(row, 
							new String[]{"class", "labelKey"}, 
							new Object[]{"font gridLink", "PV-toolkit-prepare.row.button.compare"}
							);
					toCompareOnClickEventListener(link, getId(), skillExampleTag);
				}
	
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					new CRunPVToolkitDefLabel(row, 
							new String[]{"class", "labelKey"}, 
							new Object[]{"font gridLabel", "PV-toolkit-prepare.status." + getFeedbackStatus(skillExampleTag)}
							);
				}
	
				rowNumber ++;
	
			}
		}

		return rowNumber;
	}

	protected void toCompareOnClickEventListener(Component component, String fromComponentId, IXMLTag skillExampleTag) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				Component fromComponent = CDesktopComponents.vView().getComponent(fromComponentId);
				if (fromComponent != null) {
					fromComponent.setVisible(false);
				}
				CRunPVToolkitSkillWheelLevel2Div skillWheel = (CRunPVToolkitSkillWheelLevel2Div)CDesktopComponents.vView().getComponent(_stepPrefix + "SkillWheelLevel2Div");
				if (skillWheel != null) {
					CDesktopComponents.cControl().setRunSessAttr("skillExampleTag", skillExampleTag);
					skillWheel.init(_actor, false, 0, 0);
				}
			}
		});
	}
	
}
