/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.zkoss.lang.Strings;
import org.zkoss.util.resource.Labels;
import org.zkoss.web.Attributes;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.IRunAccountManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefMacro;
import nl.surf.emergo.domain.IEBlob;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERunAccount;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.utilities.PropsValues;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpring;

/**
 * The Class CRunIspot is used to show ispot component within the run view area
 * of the emergo player.
 */
public class CRunIspot extends CRunComponent {

	private static final long serialVersionUID = 463897345751652149L;

	/** The id to store xml data plus status root tags in application memory. */
	private static final String iSpotXmlDataPlusStatusRootTags = "iSpotXmlDataPlusStatusRootTags";

	/** Are webcam recordings assessed. */
	boolean areWebcamRecordingsAssessed = true;

	/** The current recording type. */
	protected String currentRecordingId = "";

	/** The current played url. */
	protected String currentPlayedUrl = "";

	/** Used to store intervention tags temporarily within one method. */
	private Hashtable<Integer, List<IXMLTag>> tempInterventionTags = new Hashtable<Integer, List<IXMLTag>>();

	protected VView vView = CDesktopComponents.vView();

	protected SSpring sSpring = CDesktopComponents.sSpring();

	protected static int nrOfConcurrentRecordings = 0;
	protected Integer maxNrOfConcurrentRecordings;
	protected boolean busyRecording = false;

	public boolean canCurrentRgaSeeFeedbackOnOthers = false;
	public boolean recordingCanBeStarted = false;
	public long recordingMaxDuration = -1;
	public boolean recordingShowProgress = false;
	public long recordingTimerCount = 0;
	public String stylePath = "../style/";
	protected String listboxStyle = "padding-left:5px;text-align:left;";

	public Boolean is_Menu_SingleItem = true;
	public String instructionVisible = "";
	public String colophonVisible = "";
	public String vignetteTypeStart = "start";
	public String vignetteTypePractice = "practice";
	public String vignetteTypePeerfeedback = "peerfeedback";
	public String vignetteTypeTest = "test";
	public String[] vignetteTypes = new String[] { vignetteTypeStart, vignetteTypePractice, vignetteTypeTest, vignetteTypePeerfeedback };
	public String[] vignetteTypesVisible = new String[] { "true", "true", "true", "true" };
	public String is_Menu_AutoStartType = "";
	public boolean feedbackVisible = false;
	public String giveFeedbackVisible = "";
	public String inspectFeedbackVisible = "";

	public String interventionTypeVignette = "Vignette";
	public String interventionTypeGivenFeedback = "GivenFeedback";
	public String interventionTypeInspectedFeedback = "InspectedFeedback";

	public String webcamContainerId = "webcamContainerDiv";
	public String webcamBoxId = "runWebcamInitBox";

	public String getListboxStyle() {
		return listboxStyle;
	}

	public void setListboxStyle(String listboxStyle) {
		this.listboxStyle = listboxStyle;
	}

	/**
	 * Instantiates a new c run ispot.
	 */
	public CRunIspot() {
		super("runIspot", null);
		init();
	}

	/**
	 * Instantiates a new c run ispot.
	 *
	 * @param aId            the a id
	 * @param aCaseComponent the case component
	 */
	public CRunIspot(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
		init();
	}

	/**
	 * Creates title area, content area to show tab structure, and buttons area with
	 * close button.
	 */
	@Override
	protected void createComponents() {
		CRunVbox lVbox = new CRunVbox();

		createTitleArea(lVbox);
		createContentArea(lVbox);
		createButtonsArea(lVbox);

		appendChild(lVbox);
	}

	/**
	 * Creates new content component, the tree.
	 *
	 * @return the component
	 */
	@Override
	protected Component newContentComponent() {
		HtmlMacroComponent ispotView = new CDefMacro();
		ispotView.setId("runIspotView");
		ispotView.setZclass("");
		ispotView.setVisible(false);
		return ispotView;
	}

	/**
	 * Creates buttons area and adds close button.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run hbox
	 */
	@Override
	protected CRunHbox createButtonsArea(Component aParent) {
		CRunHbox lButtonsHbox = super.createButtonsArea(aParent);
		createCloseButton(lButtonsHbox);
		return lButtonsHbox;
	}

	/**
	 * Creates view.
	 */
	public void init() {
		// Only if webcam recordings are assessed student status has to be stored in
		// application memory (to improve performance)
		areWebcamRecordingsAssessed = areWebcamRecordingsAssessed();
		// NOTE load case specific labels
		Labels.register(
				new CRunComponentLabelLocator(Executions.getCurrent().getDesktop().getWebApp().getServletContext(),
						getId(), (Locale) CDesktopComponents.cControl().getSessAttr(Attributes.PREFERRED_LOCALE)));

		sSpring.getAppManager().addDataTreeReferenceToRemoveFromAppMemory(iSpotXmlDataPlusStatusRootTags);
		// save ispot first time started. Is necessary to ask user for some
		// configuration settings
		boolean lStarted = sSpring.getCurrentRunComponentStatus(caseComponent, AppConstants.statusKeyStarted,
				AppConstants.statusTypeRunGroup).equals(AppConstants.statusValueTrue);
		if (!lStarted) {
			setRunComponentStatus(caseComponent, AppConstants.statusKeyStarted, AppConstants.statusValueTrue, true);
		}

		Events.postEvent("onInitZulfile", this, null);
	}

	/**
	 * Gets the run component helper.
	 *
	 * @return the run component helper
	 */
	@Override
	public CRunComponentHelper getRunComponentHelper() {
		return new CRunComponentHelper(null, "subject", caseComponent, this);
	}

	/**
	 * Gets the iSpot xml data plus status root tags.
	 *
	 * @return hashtable
	 */
	protected synchronized Hashtable<String, Hashtable<String, IXMLTag>> getISpotXmlDataPlusRootTags() {
		if (sSpring.getAppContainer().get(iSpotXmlDataPlusStatusRootTags) == null)
			sSpring.getAppContainer().put(iSpotXmlDataPlusStatusRootTags,
					new Hashtable<String, Hashtable<String, IXMLTag>>(0));
		return (Hashtable) sSpring.getAppContainer().get(iSpotXmlDataPlusStatusRootTags);
	}

	/**
	 * Gets the iSpot xml data plus status root tags by cac.
	 *
	 * @param aCac the case component
	 *
	 * @return hashtable
	 */
	protected synchronized Hashtable<String, IXMLTag> getISpotXmlDataPlusRootTagsByCac(IECaseComponent aCac) {
		String lCacId = "" + aCac.getCacId();
		if (!getISpotXmlDataPlusRootTags().containsKey(lCacId)) {
			getISpotXmlDataPlusRootTags().put(lCacId, new Hashtable<String, IXMLTag>(0));
		}
		return getISpotXmlDataPlusRootTags().get(lCacId);
	}

	/**
	 * Gets the iSpot xml data plus status root tag by cac and rga.
	 *
	 * @param aCac the case component
	 * @param aRga the run group account
	 *
	 * @return root tag
	 */
	protected synchronized IXMLTag getISpotXmlDataPlusRootTagsByCacByRga(IECaseComponent aCac, IERunGroupAccount aRga) {
		String lRgaId = "" + aRga.getRgaId();
		if (!getISpotXmlDataPlusRootTagsByCac(aCac).containsKey(lRgaId)) {
			return null;
		}
		return getISpotXmlDataPlusRootTagsByCac(aCac).get(lRgaId);
	}

	/**
	 * Gets xml data plus status root tag of aRga from application memory.
	 *
	 * @param aRga the run group account
	 *
	 * @return the root tag
	 */
	protected IXMLTag getXmlDataPlusStatusRootTagFromAppMemory(IERunGroupAccount aRga) {
		return getISpotXmlDataPlusRootTagsByCacByRga(caseComponent, aRga);
	}

	/**
	 * Sets xml data plus status root tag of aRga in application memory.
	 *
	 * @param aRga        the run group account
	 * @param aXmlRootTag the xml root tag
	 */
	protected synchronized void setXmlDataPlusStatusRootTagFromAppMemory(IERunGroupAccount aRga, IXMLTag aXmlRootTag) {
		if (areWebcamRecordingsAssessed) {
			// Only store student status in application memory if webcam recordings are
			// assessed
			String lRgaId = "" + aRga.getRgaId();
			getISpotXmlDataPlusRootTagsByCac(caseComponent).put(lRgaId, aXmlRootTag);
		}
	}

	/**
	 * Updates application memory for current rga.
	 */
	protected void updateAppMemoryForCurrentRga() {
		// NOTE call this method for current rga so application memory will be updated
		getXmlDataPlusStatusRootTag(sSpring.getRunGroupAccount());
	}

	/**
	 * Gets xml data plus status root tag.
	 *
	 * @param aRga the run group account
	 *
	 * @return the root tag
	 */
	protected IXMLTag getXmlDataPlusStatusRootTag(IERunGroupAccount aRga) {
		IERunGroupAccount lCurrentRga = sSpring.getRunGroupAccount();
		IXMLTag lXmlRootTag = null;
		if (aRga.getERunGroup().getRugId() == lCurrentRga.getERunGroup().getRugId()) {
			lXmlRootTag = sSpring.getXmlDataPlusRunStatusTree(caseComponent, AppConstants.statusTypeRunGroup);
			if (areWebcamRecordingsAssessed) {
				// set in application memory so other rgas can use it
				setXmlDataPlusStatusRootTagFromAppMemory(aRga, lXmlRootTag);
			}
		} else {
			if (areWebcamRecordingsAssessed) {
				// Only get student status from application memory if webcam recordings are
				// assessed
				lXmlRootTag = getXmlDataPlusStatusRootTagFromAppMemory(aRga);
			}
			if (lXmlRootTag == null) {
				lXmlRootTag = sSpring.getXmlDataPlusRunGroupStatusTree(aRga.getERunGroup().getRugId(), caseComponent);
				if (areWebcamRecordingsAssessed) {
					// set in application memory so other rgas can use it
					setXmlDataPlusStatusRootTagFromAppMemory(aRga, lXmlRootTag);
				}
			}
		}
		return lXmlRootTag;
	}

	/**
	 * Gets instruction tag.
	 *
	 * @param aRga the run group account
	 *
	 * @return the instruction tag
	 */
	protected IXMLTag getInstructionTag(IERunGroupAccount aRga) {
		IXMLTag lXmlRootTag = getXmlDataPlusStatusRootTag(aRga);
		if (lXmlRootTag != null) {
			for (IXMLTag lInstructionTag : lXmlRootTag.getChild(AppConstants.contentElement)
					.getChildTags(AppConstants.defValueNode)) {
				if (lInstructionTag.getName().equals("instruction")
						&& lInstructionTag.getCurrentStatusAttribute(AppConstants.statusKeyOpened)
								.equals(AppConstants.statusValueTrue)
						&& !lInstructionTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent)
								.equals(AppConstants.statusValueFalse)) {
					return lInstructionTag;
				}
			}
		}
		return null;
	}

	/**
	 * Gets subject tags.
	 *
	 * @param aRga the run group account
	 *
	 * @return the subject tags
	 */
	protected List<IXMLTag> getSubjectTags(IERunGroupAccount aRga) {
		List<IXMLTag> lSubjectTags = new ArrayList<IXMLTag>();
		IXMLTag lXmlRootTag = getXmlDataPlusStatusRootTag(aRga);
		if (lXmlRootTag != null) {
			for (IXMLTag lSubjectTag : lXmlRootTag.getChild(AppConstants.contentElement)
					.getChildTags(AppConstants.defValueNode)) {
				if (lSubjectTag.getName().equals("subject")
						&& !lSubjectTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent)
								.equals(AppConstants.statusValueFalse)) {
					lSubjectTags.add(lSubjectTag);
				}
			}
		}
		return lSubjectTags;
	}

	/**
	 * Gets intervention tags.
	 *
	 * @param aRga the run group account
	 * @param pAllVignettes loop through all vignettes instead of only present vignettes
	 *
	 * @return the intervention tags
	 */
	public List<IXMLTag> getInterventionTags(IERunGroupAccount aRga, boolean pAllVignettes) {
		List<IXMLTag> lInterventionTags = new ArrayList<IXMLTag>();
		if (pAllVignettes) {
			for (IXMLTag lSubjectTag : getSubjectTags(aRga)) {
				for (IXMLTag lVignetteTag : lSubjectTag.getChilds("vignette")) {
					for (IXMLTag lInterventionTag : lVignetteTag.getChilds("intervention")) {
						if (!lInterventionTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent)
								.equals(AppConstants.statusValueFalse)) {
							lInterventionTags.add(lInterventionTag);
						}
					}
				}
			}
		} else {
			for (IXMLTag lSubjectTag : getSubjectTags(aRga)) {
				for (IXMLTag lVignetteTag : lSubjectTag.getChilds("vignette")) {
					if (!lVignetteTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent)
							.equals(AppConstants.statusValueFalse)) {
						for (IXMLTag lInterventionTag : lVignetteTag.getChilds("intervention")) {
							if (!lInterventionTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent)
									.equals(AppConstants.statusValueFalse)) {
								lInterventionTags.add(lInterventionTag);
							}
						}
					}
				}
			}
		}
		return lInterventionTags;
	}

	/**
	 * Gets intervention tag by id.
	 *
	 * @param aRga the run group account
	 * @param aId  the id
	 *
	 * @return the intervention tag
	 */
	public IXMLTag getInterventionTagById(IERunGroupAccount aRga, String aId, boolean pAllVignettes) {
		for (IXMLTag lInterventionTag : getInterventionTags(aRga, pAllVignettes)) {
			if (lInterventionTag.getAttribute(AppConstants.keyId).equals(aId)) {
				return lInterventionTag;
			}
		}
		return null;
	}

	/**
	 * Clears temp intervention tags.
	 */
	private void clearTempInterventionTags() {
		tempInterventionTags.clear();
	}

	/**
	 * Gets temp intervention tags.
	 *
	 * @param aRga the run group account
	 *
	 * @return the given feedbacks
	 */
	private List<IXMLTag> getTempInterventionTags(IERunGroupAccount aRga, boolean pAll) {
		if (!tempInterventionTags.containsKey(aRga.getRgaId())) {
			tempInterventionTags.put(aRga.getRgaId(), getInterventionTags(aRga, pAll));
		}
		return tempInterventionTags.get(aRga.getRgaId());
	}

	/**
	 * Gets iSpot instruction.
	 *
	 * @return the instruction
	 */
	public String getInstruction() {
		// current rga
		IERunGroupAccount lCurrentRga = sSpring.getRunGroupAccount();
		IXMLTag lInstructionTag = getInstructionTag(lCurrentRga);
		if (lInstructionTag != null) {
			return sSpring.unescapeXML(lInstructionTag.getChildValue("richtext").trim());
		}
		return "";
	}

	/**
	 * Gets iSpot colophon.
	 *
	 * @return the colophon text
	 */
	public String getColophon() {
		IXMLTag lComponentTag = getDataPlusStatusRootTag().getChild(AppConstants.contentElement);
		if (lComponentTag != null) {
			return sSpring.unescapeXML(lComponentTag.getChildValue("colophon").trim());
		}
		return "";
	}

	/**
	 * Gets iSpot 'overview' property for the given vignette type. Determines if
	 * vignette overview list is shown.
	 *
	 * @return the overview property.
	 */
	public boolean getShowOverview(String pVignetteType) {
		IXMLTag lComponentTag = getDataPlusStatusRootTag().getChild(AppConstants.componentElement);
		String lShowOverviewStr = lComponentTag.getCurrentStatusAttribute(pVignetteType + "overview");
		return ((lShowOverviewStr == null) || !lShowOverviewStr.equals("false"));
	}

	/**
	 * Gets interventions tag vignette type.
	 *
	 * @param aInterventionTag the intervention tag
	 *
	 * @return the type
	 */
	private String getInterventionTagVignetteType(IXMLTag aInterventionTag) {
		if (aInterventionTag == null || aInterventionTag.getParentTag() == null) {
			return "";
		}
		return aInterventionTag.getParentTag().getChildValue("type");
	}

	/**
	 * Gets interventions.
	 *
	 * @param aVignetteType the vignette type
	 *
	 * @return the interventions
	 */
	public List<CRunIspotIntervention> getInterventions(String aVignetteType) {
		List<CRunIspotIntervention> lResultList = new ArrayList<CRunIspotIntervention>();

		// current rga = reaction giver
		IERunGroupAccount lCurrentRga = sSpring.getRunGroupAccount();

		// variable is needed to be able to walk through different reactions
		CRunIspotIntervention lPreviousIntervention = null;
		// loop through intervention tags of current rga = reaction giver
		for (IXMLTag lCurrentInterventionTag : getInterventionTags(lCurrentRga, false)) {
			if (getInterventionTagVignetteType(lCurrentInterventionTag).equals(aVignetteType)) {
				CRunIspotIntervention lIntervention = new CRunIspotIntervention();
				addInterventionData(lCurrentRga, lIntervention, aVignetteType, lCurrentInterventionTag,
						lPreviousIntervention);
				lResultList.add(lIntervention);
				lPreviousIntervention = lIntervention;
			}
		}
		return lResultList;
	}

	/**
	 * Is giving feedback allowed.
	 *
	 * @param aVignetteType the vignette type
	 *
	 * @return boolean
	 */
	public boolean isGivingFeedbackAllowed(String aVignetteType) {
		IERunGroupAccount lCurrentRga = sSpring.getRunGroupAccount();
		// get vignette types that current rga is allowed to give feedback for
		List<String> lAllowedVignetteTypes = getAssesVignetteTypes(lCurrentRga);
		return lAllowedVignetteTypes.indexOf(aVignetteType) >= 0;
	}

	/**
	 * Get unique recording name.
	 *
	 * @param aIntervention    the intervention
	 * @param aInterventionTag the intervention tag
	 *
	 * @return the recording name
	 */
	public String getUniqueRecordingName(CRunIspotIntervention aIntervention, IXMLTag aInterventionTag) {
		String lType = "reaction";
		String lReactionRgaIdStr = "";
		if (aIntervention instanceof CRunIspotGivenFeedback) {
			lType = "feedback";
			lReactionRgaIdStr = "reactionrgaid_" + aIntervention.getReactionRga().getRgaId() + "_";
		}
		String lRecordingName = sSpring.getPropEmergoServerId() + "/" + String.valueOf(sSpring.getCase().getCasId())
				+ "/" + "cacid_" + caseComponent.getCacId() + "/" + "runid_"
				+ String.valueOf(sSpring.getRun().getRunId()) + "/" + "rgaid_"
				+ String.valueOf(sSpring.getRunGroupAccount().getRgaId()) + "/" + lType + "_" + lReactionRgaIdStr
				+ aIntervention.getSubjectName() + "_" + aIntervention.getVignetteName() + "_" + "tagid_"
				+ aInterventionTag.getAttribute(AppConstants.keyId);
		return lRecordingName;
	}

	/**
	 * Get absolute recording url.
	 *
	 * @param aBaseUrl the base url
	 *
	 * @return the absolute url
	 */
	public String getAbsoluteRecordingUrl(String aBaseUrl) {
		return sSpring.getSBlobHelper().getIntUrl(sSpring.getCase(), aBaseUrl, false);
	}

	/**
	 * Get state.
	 *
	 * @param aTag the tag
	 *
	 * @return the state
	 */
	public String getState(IXMLTag aTag) {
		String lState = "empty";
		if (aTag.getCurrentStatusAttribute(AppConstants.statusKeyFinished).equals(AppConstants.statusValueTrue)) {
			lState = "done";
		} else if (aTag.getCurrentStatusAttribute(AppConstants.statusKeyOpened).equals(AppConstants.statusValueTrue)) {
			lState = "busy";
		}
		return lState;
	}

	/**
	 * Adds intervention data.
	 *
	 * @param aRga                  the run group account
	 * @param aIntervention         the intervention
	 * @param aVignetteType         the vignette type
	 * @param aInterventionTag      the intervention tag
	 * @param aPreviousIntervention the previous intervention
	 */
	public void addInterventionData(IERunGroupAccount aRga, CRunIspotIntervention aIntervention, String aVignetteType,
			IXMLTag aInterventionTag, CRunIspotIntervention aPreviousIntervention) {
		aIntervention.setPrevious(aPreviousIntervention);
		if (aPreviousIntervention != null) {
			aPreviousIntervention.setNext(aIntervention);
		}

		aIntervention.setCaseComponent(caseComponent);
		aIntervention.setTag(aInterventionTag);
		aIntervention.setTagId(Integer.parseInt(aInterventionTag.getAttribute(AppConstants.keyId)));

		aIntervention.setType(aVignetteType);
		String lValue = aInterventionTag.getDefChildValue("maxduration");
		aIntervention.setMaxDuration(Integer.parseInt(!lValue.equals("") ? lValue : "-1"));
		// initial intervention value of number of resits can be overruled by status
		// value set by administrator (for student) or case run manager (for run):
		lValue = aInterventionTag.getCurrentStatusAttribute(AppConstants.statusKeyNumberofresits);
		if (lValue.equals("-2")) {
			// if status not set, use initial value
			lValue = aInterventionTag.getDefChildValue("numberofresits");
		}
		aIntervention.setNumberOfResits(Integer.parseInt(!lValue.equals("") ? lValue : "-1"));
		aIntervention.setSeeSelf(aInterventionTag.getDefChildValue("seeself").equals(AppConstants.statusValueTrue));
		aIntervention.setAutoStart(aInterventionTag.getCurrentStatusAttribute(AppConstants.statusKeyAutostart)
				.equals(AppConstants.statusValueTrue));
		aIntervention.setInterruptable(!aInterventionTag.getCurrentStatusAttribute(AppConstants.statusKeyInterruptable)
				.equals(AppConstants.statusValueFalse));
		aIntervention.setPublishable(!aInterventionTag.getCurrentStatusAttribute(AppConstants.statusKeyPublishable)
				.equals(AppConstants.statusValueFalse));
		aIntervention.setReviewable(!aInterventionTag.getCurrentStatusAttribute(AppConstants.statusKeyReviewable)
				.equals(AppConstants.statusValueFalse));

		// add subject title
		String lSubjectName = sSpring
				.unescapeXML(aInterventionTag.getParentTag().getParentTag().getChildValue("title"));
		aIntervention.setSubjectName(lSubjectName);

		// add vignette title, instruction and url
		IXMLTag lVignetteTag = aInterventionTag.getParentTag();
		String lVignetteName = sSpring.unescapeXML(lVignetteTag.getChildValue("title"));
		aIntervention.setVignetteName(lVignetteName);
		aIntervention.setVignetteInstruction(sSpring.unescapeXML(lVignetteTag.getChildValue("instruction")));
		aIntervention.setVignetteUrl(vView.getAbsoluteUrl(sSpring.getSBlobHelper().getUrl(lVignetteTag)));
		aIntervention.setVignetteReviewable(!lVignetteTag.getCurrentStatusAttribute(AppConstants.statusKeyReviewable)
				.equals(AppConstants.statusValueFalse));
		aIntervention.setVignetteNextIfPublished(
				!lVignetteTag.getCurrentStatusAttribute(AppConstants.statusKeyNextifpublished)
						.equals(AppConstants.statusValueFalse));
		aIntervention.setVignetteAllowPrevious(!lVignetteTag
				.getCurrentStatusAttribute(AppConstants.statusKeyAllowprevious).equals(AppConstants.statusValueFalse));
		aIntervention.setVignetteRanking(lVignetteTag.getCurrentStatusAttribute(AppConstants.statusKeyRanking));

		aIntervention.setFeedbackTexts(getFeedbackTexts(lVignetteTag));
		aIntervention.setFeedbackFragments(getFeedbackFragments(lVignetteTag));

		aIntervention.setReactionRga(aRga);
		aIntervention.setRecordingName(getUniqueRecordingName(aIntervention, aInterventionTag));
		aIntervention.setReactionState(getState(aInterventionTag));
		aIntervention.setReactionDoneDate(aInterventionTag.getCurrentStatusAttribute("finisheddate"));
		// NOTE only the last reaction is saved
		IEBlob lBlob = sSpring.getSBlobHelper().getBlob(aInterventionTag.getStatusChildValue("reaction"));
		if (lBlob != null) {
			aIntervention.setReactionUrl(getAbsoluteRecordingUrl(lBlob.getUrl()));
		}
		lValue = aInterventionTag.getCurrentStatusAttribute(AppConstants.statusKeyNumberofreactions);
		aIntervention.setNumberOfReactions(Integer.parseInt(!lValue.equals("") ? lValue : "0"));
	}

	/**
	 * Gets feedback texts.
	 *
	 * @param aVignetteTag the vignette tag
	 *
	 * @return the feedback texts
	 */
	protected List<CRunIspotFeedbackText> getFeedbackTexts(IXMLTag aVignetteTag) {
		List<CRunIspotFeedbackText> lFeedbackTexts = new ArrayList<CRunIspotFeedbackText>();
		List<IXMLTag> lXmlTags = aVignetteTag.getChilds("feedbacktext");
		for (IXMLTag lXmlTag : lXmlTags) {
			if (!lXmlTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent)
					.equals(AppConstants.statusValueFalse)) {
				CRunIspotFeedbackText lFeedbackText = new CRunIspotFeedbackText();
				lFeedbackText.setTagId(Integer.parseInt(lXmlTag.getAttribute(AppConstants.keyId)));
				lFeedbackText.setTitle(sSpring.unescapeXML(lXmlTag.getChildValue("title")));
				lFeedbackText.setText(sSpring.unescapeXML(lXmlTag.getChildValue("description")));
				lFeedbackTexts.add(lFeedbackText);
			}
		}
		return lFeedbackTexts;
	}

	/**
	 * Gets feedback fragments.
	 *
	 * @param aVignetteTag the vignette tag
	 *
	 * @return the feedback fragments
	 */
	protected List<CRunIspotFeedbackFragment> getFeedbackFragments(IXMLTag aVignetteTag) {
		List<CRunIspotFeedbackFragment> lFeedbackFragments = new ArrayList<CRunIspotFeedbackFragment>();
		List<IXMLTag> lXmlTags = aVignetteTag.getChilds("feedbackfragment");
		for (IXMLTag lXmlTag : lXmlTags) {
			if (!lXmlTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent)
					.equals(AppConstants.statusValueFalse)) {
				CRunIspotFeedbackFragment lFeedbackFragment = new CRunIspotFeedbackFragment();
				lFeedbackFragment.setTagId(Integer.parseInt(lXmlTag.getAttribute(AppConstants.keyId)));
				lFeedbackFragment.setTitle(sSpring.unescapeXML(lXmlTag.getChildValue("title")));
				lFeedbackFragment.setUrl(vView.getAbsoluteUrl(sSpring.getSBlobHelper().getUrl(lXmlTag)));
				lFeedbackFragments.add(lFeedbackFragment);
			}
		}
		return lFeedbackFragments;
	}

	/**
	 * Is tutor in run.
	 *
	 * @param aRga the run group account
	 *
	 * @return boolean
	 */
	public boolean isTutorInRun(IERunGroupAccount aRga) {
		// if preview as tutor
		if (sSpring.isTutorRun()) {
			return true;
		}
		// determine of rga has role tutor and is tutor in run
		IAccountManager accountManager = (IAccountManager) sSpring.getBean("accountManager");
		boolean lIsTutorInRun = false;
		if (accountManager.hasRole(aRga.getEAccount(), AppConstants.c_role_tut)) {
			IRunAccountManager runAccountManager = (IRunAccountManager) sSpring.getBean("runAccountManager");
			IERunAccount lRunAccount = runAccountManager
					.getRunAccountByRunIdAccId(aRga.getERunGroup().getERun().getRunId(), aRga.getEAccount().getAccId());
			if (lRunAccount != null) {
				lIsTutorInRun = lRunAccount.getTutactive();
			}
		}
		return lIsTutorInRun;
	}

	/**
	 * Gets vignette types that can be assessed by rga.
	 *
	 * @param aRga the run group account
	 *
	 * @return the vignette types
	 */
	public List<String> getAssesVignetteTypes(IERunGroupAccount aRga) {
		// determine if practice and/or test vignettes can be assessed
		boolean lAssessPracticeVignettes = false;
		boolean lAssessTestVignettes = false;
		IXMLTag lComponentTag = getDataPlusStatusRootTag().getChild(AppConstants.componentElement);
		if (isTutorInRun(aRga)) {
			// if tutor get component settings for tutorassespractice and tutorassestest
			lAssessPracticeVignettes = lComponentTag.getCurrentStatusAttribute("tutorasses" + vignetteTypePractice)
					.equals(AppConstants.statusValueTrue);
			lAssessTestVignettes = lComponentTag.getCurrentStatusAttribute("tutorasses" + vignetteTypeTest)
					.equals(AppConstants.statusValueTrue);
		} else {
			// if no tutor get component settings for studentassespractice and
			// studentassestest.
			lAssessPracticeVignettes = lComponentTag.getCurrentStatusAttribute("studentasses" + vignetteTypePractice)
					.equals(AppConstants.statusValueTrue);
			lAssessTestVignettes = lComponentTag.getCurrentStatusAttribute("studentasses" + vignetteTypeTest)
					.equals(AppConstants.statusValueTrue);
		}
		List<String> lVignetteTypes = new ArrayList<String>();
		if (lAssessPracticeVignettes) {
			lVignetteTypes.add(vignetteTypePractice);
			//NOTE for the moment, treat peer feedback vignettes like practice vignettes
			lVignetteTypes.add(vignetteTypePeerfeedback);
		}
		if (lAssessTestVignettes) {
			lVignetteTypes.add(vignetteTypeTest);
		}
		return lVignetteTypes;
	}

	/**
	 * Are webcam recordings assessed.
	 *
	 * @return boolean
	 */
	protected boolean areWebcamRecordingsAssessed() {
		IXMLTag lComponentTag = getDataPlusStatusRootTag().getChild(AppConstants.componentElement);
		return lComponentTag.getCurrentStatusAttribute("tutorasses" + vignetteTypePractice)
				.equals(AppConstants.statusValueTrue)
				|| lComponentTag.getCurrentStatusAttribute("tutorasses" + vignetteTypeTest)
						.equals(AppConstants.statusValueTrue)
				|| lComponentTag.getCurrentStatusAttribute("studentasses" + vignetteTypePractice)
						.equals(AppConstants.statusValueTrue)
				|| lComponentTag.getCurrentStatusAttribute("studentasses" + vignetteTypeTest)
						.equals(AppConstants.statusValueTrue);
	}

	/**
	 * Gets feedback given tag for rga. It is a child tag with name feedbackgiven.
	 *
	 * @param aRga             the run group account
	 * @param aInterventionTag the intervention tag
	 */
	public IXMLTag getFeedbackGivenTagForRga(IERunGroupAccount aRga, IXMLTag aInterventionTag) {
		List<IXMLTag> lFeedbackGivenTags = aInterventionTag.getStatusChilds("feedbackgiven");
		for (IXMLTag lFeedbackGivenTag : lFeedbackGivenTags) {
			if (lFeedbackGivenTag.getAttribute("reactionrgaid").equals("" + aRga.getRgaId())) {
				return lFeedbackGivenTag;
			}
		}
		return null;
	}

	/**
	 * Gets given feedback tag for rga.
	 *
	 * @param aRga                           the run group account
	 * @param aRgaInterventionTag            the rga intervention tag
	 * @param aFeedbackGiverInterventionTags the feedback giver intervention tags
	 *
	 * @return the given feedback tag
	 */
	public IXMLTag getFeedbackGivenTagForRga(IERunGroupAccount aRga, IXMLTag aRgaInterventionTag,
			List<IXMLTag> aFeedbackGiverInterventionTags) {
		IXMLTag lFoundTag = null;
		for (IXMLTag lFeedbackGiverInterventionTag : aFeedbackGiverInterventionTags) {
			// key ids must be equal to get the same intervention
			if (lFeedbackGiverInterventionTag.getAttribute(AppConstants.keyId)
					.equals(aRgaInterventionTag.getAttribute(AppConstants.keyId))) {
				IXMLTag lTag = getFeedbackGivenTagForRga(aRga, lFeedbackGiverInterventionTag);
				if (lTag != null) {
					// NOTE only one feedback per intervention and rga
					lFoundTag = lTag;
					break;
				}
			}
		}
		return lFoundTag;
	}

	/**
	 * Gets given feedbacks.
	 *
	 * @return the given feedbacks
	 */
	public List<CRunIspotIntervention> getGivenFeedbacks() {
		List<CRunIspotIntervention> lResultList = new ArrayList<CRunIspotIntervention>();

		// current rga = feedback giver
		IERunGroupAccount lCurrentRga = sSpring.getRunGroupAccount();
		// get vignette types that current rga is allowed to give feedback for
		List<String> lAllowedVignetteTypes = getAssesVignetteTypes(lCurrentRga);
		// get all possible reaction giver rgas
		List<IERunGroupAccount> lReactionGiverRgas = sSpring.getRunGroupAccountsWithinRun();

		clearTempInterventionTags();
		// variable is needed to be able to walk through different feedbacks
		CRunIspotIntervention lPreviousIntervention = null;
		// loop through all allowed vignette types
		for (String lVignetteType : lAllowedVignetteTypes) {
			// get intervention tags for current rga = feedback giver
			List<IXMLTag> lCurrentInterventionTags = getTempInterventionTags(lCurrentRga, true);
			// loop through all possible reaction giver rgas
			for (IERunGroupAccount lReactionGiverRga : lReactionGiverRgas) {
				// get reaction for active rga if unequal to current rga
				if (lReactionGiverRga.getERunGroup().getActive()
						&& lReactionGiverRga.getERunGroup().getRugId() != lCurrentRga.getERunGroup().getRugId()) {
					// loop through intervention tags of current rga = reaction giver
					for (IXMLTag lReactionGiverInterventionTag : getTempInterventionTags(lReactionGiverRga, false)) {
						// only show reaction to give feedback to, if correct vignette type and if
						// reaction is done
						if (getInterventionTagVignetteType(lReactionGiverInterventionTag).equals(lVignetteType)
								&& getState(lReactionGiverInterventionTag).equals("done")) {
							CRunIspotIntervention lIntervention = new CRunIspotGivenFeedback();
							addInterventionData(lReactionGiverRga, lIntervention, lVignetteType,
									lReactionGiverInterventionTag, lPreviousIntervention);
							// the feedbackgiven tag is a child tag of the status tag of one of the current
							// intervention tags
							IXMLTag lFeedbackGivenTag = getFeedbackGivenTagForRga(lReactionGiverRga,
									lReactionGiverInterventionTag, lCurrentInterventionTags);
							addGivenFeedbackData(lCurrentRga, (CRunIspotGivenFeedback) lIntervention,
									lFeedbackGivenTag);
							lResultList.add(lIntervention);
							lPreviousIntervention = lIntervention;
						}
					}
				}
			}
		}
		clearTempInterventionTags();
		return lResultList;
	}

	/**
	 * Adds given feedback data.
	 *
	 * @param aRga              the run group account
	 * @param aGivenFeedback    the given feedback
	 * @param aFeedbackGivenTag the feedback given tag
	 */
	public void addGivenFeedbackData(IERunGroupAccount aRga, CRunIspotGivenFeedback aGivenFeedback,
			IXMLTag aFeedbackGivenTag) {
		aGivenFeedback.setGivenFeedbackRga(aRga);
		aGivenFeedback.setRecordingName(getUniqueRecordingName(aGivenFeedback, aGivenFeedback.getTag()));
		String lState = "empty";
		String lDoneDate = "";
		// NOTE the feedback given tag is null if no feedback given yet
		if (aFeedbackGivenTag != null) {
			lState = getState(aFeedbackGivenTag);
			lDoneDate = aFeedbackGivenTag.getCurrentStatusAttribute("finisheddate");
			IEBlob lBlob = sSpring.getSBlobHelper().getBlob(aFeedbackGivenTag.getValue());
			if (lBlob != null) {
				aGivenFeedback.setGivenFeedbackUrl(getAbsoluteRecordingUrl(lBlob.getUrl()));
			}
		}
		aGivenFeedback.setGivenFeedbackState(lState);
		aGivenFeedback.setGivenFeedbackDoneDate(lDoneDate);
	}

	/**
	 * Can current rga see feedbacks on others.
	 *
	 * @return boolean
	 */
	public boolean canCurrentRgaSeeFeedbackOnOthers() {
		if (isTutorInRun(sSpring.getRunGroupAccount())) {
			return true;
		}
		IXMLTag lComponentTag = getDataPlusStatusRootTag().getChild(AppConstants.componentElement);
		return lComponentTag.getCurrentStatusAttribute("studentseefeedbackonothers")
				.equals(AppConstants.statusValueTrue);
	}

	/**
	 * Gets feedback received tag for rga. It is a child tag with name
	 * feedbackreceived.
	 *
	 * @param aRga             the run group account
	 * @param aInterventionTag the intervention tag
	 */
	public IXMLTag getFeedbackReceivedTagForRga(IERunGroupAccount aRga, IXMLTag aInterventionTag) {
		List<IXMLTag> lFeedbackReceivedTags = aInterventionTag.getStatusChilds("feedbackreceived");
		for (IXMLTag lFeedbackReceivedTag : lFeedbackReceivedTags) {
			if (lFeedbackReceivedTag.getAttribute("feedbackgivenrgaid").equals("" + aRga.getRgaId())) {
				return lFeedbackReceivedTag;
			}
		}
		return null;
	}

	/**
	 * Gets inspected feedback tag for rga.
	 *
	 * @param aRga                               the run group account
	 * @param aRgaInterventionTag                the rga intervention tag
	 * @param aFeedbackInspectorInterventionTags the feedback inspector intervention
	 *                                           tags
	 *
	 * @return the inspected feedback tag
	 */
	public IXMLTag getFeedbackInspectedTagForRga(IERunGroupAccount aRga, IXMLTag aRgaInterventionTag,
			List<IXMLTag> aFeedbackInspectorInterventionTags) {
		IXMLTag lFoundTag = null;
		for (IXMLTag lFeedbackInspectorInterventionTag : aFeedbackInspectorInterventionTags) {
			// key ids must be equal to get the same intervention
			if (lFeedbackInspectorInterventionTag.getAttribute(AppConstants.keyId)
					.equals(aRgaInterventionTag.getAttribute(AppConstants.keyId))) {
				IXMLTag lTag = getFeedbackReceivedTagForRga(aRga, lFeedbackInspectorInterventionTag);
				if (lTag != null) {
					// NOTE only one feedback inspected per intervention and rga
					lFoundTag = lTag;
					break;
				}
			}
		}
		return lFoundTag;
	}

	/**
	 * Gets inspected feedbacks.
	 *
	 * @return the inspected feedbacks
	 */
	public List<CRunIspotIntervention> getInspectedFeedbacks() {
		List<CRunIspotIntervention> lResultList = new ArrayList<CRunIspotIntervention>();

		// this is a configuration option for the iSpot component
		boolean lCanCurrentRgaSeeFeedbackOnOthers = canCurrentRgaSeeFeedbackOnOthers();
		// current rga = feedback inspector
		IERunGroupAccount lCurrentRga = sSpring.getRunGroupAccount();
		// get vignette types that current rga is allowed to inspect feedback for
		List<String> lAllowedVignetteTypes = new ArrayList<String>();
		lAllowedVignetteTypes.add(vignetteTypePractice);
		lAllowedVignetteTypes.add(vignetteTypePeerfeedback);
		lAllowedVignetteTypes.add(vignetteTypeTest);
		// get all possible reaction giver rgas
		List<IERunGroupAccount> lReactionGiverRgas = sSpring.getRunGroupAccountsWithinRun();
		// get all possible feedback giver rgas
		List<IERunGroupAccount> lFeedbackGiverRgas = sSpring.getRunGroupAccountsWithinRun();

		// get intervention tags for current rga = feedback inspector
		List<IXMLTag> lCurrentInterventionTags = getTempInterventionTags(lCurrentRga, true);
		clearTempInterventionTags();
		// variable is needed to be able to walk through different feedbacks
		CRunIspotIntervention lPreviousIntervention = null;
		// loop through allowed vignette types
		for (String lVignetteType : lAllowedVignetteTypes) {
			// loop through all possible feedback giver rgas
			for (IERunGroupAccount lFeedbackGiverRga : lFeedbackGiverRgas) {
				// get feedback for active rga
				if (lFeedbackGiverRga.getERunGroup().getActive()) {
					// get intervention tags for feedback giver
					List<IXMLTag> lFeedbackGiverInterventionTags = getTempInterventionTags(lFeedbackGiverRga, true);
					// loop through all possible reaction giver rgas
					for (IERunGroupAccount lReactionGiverRga : lReactionGiverRgas) {
						// get reaction for active rga and if current rga can see feedback on others or
						// if reaction giver rga is equal to current rga
						if (lReactionGiverRga.getERunGroup().getActive()
								&& (lCanCurrentRgaSeeFeedbackOnOthers || lReactionGiverRga.getERunGroup()
										.getRugId() == lCurrentRga.getERunGroup().getRugId())) {
							// get intervention tags for reaction giver
							List<IXMLTag> lReactionGiverInterventionTags = getTempInterventionTags(lReactionGiverRga, false);
							// loop through intervention tags of reaction giver
							for (IXMLTag lReactionGiverInterventionTag : lReactionGiverInterventionTags) {
								// only show feedback to inspect, if correct vignette type and reaction is done
								if (getInterventionTagVignetteType(lReactionGiverInterventionTag).equals(lVignetteType)
										&& getState(lReactionGiverInterventionTag).equals("done")) {
									// the feedbackgiven tag is a child tag of the status tag of one of the feedback
									// giver intervention tags
									IXMLTag lFeedbackGivenTag = getFeedbackGivenTagForRga(lReactionGiverRga,
											lReactionGiverInterventionTag, lFeedbackGiverInterventionTags);
									// only show feedback to inspect, if feedback is done
									if (lFeedbackGivenTag != null && getState(lFeedbackGivenTag).equals("done")) {
										CRunIspotIntervention lIntervention = new CRunIspotInspectedFeedback();
										addInterventionData(lReactionGiverRga, lIntervention, lVignetteType,
												lReactionGiverInterventionTag, lPreviousIntervention);
										addGivenFeedbackData(lFeedbackGiverRga, (CRunIspotGivenFeedback) lIntervention,
												lFeedbackGivenTag);
										IXMLTag lFoundFeedbackInspectedTag = null;
										// loop through intervention tags of feedback giver
										for (IXMLTag lFeedbackGiverInterventionTag : lFeedbackGiverInterventionTags) {
											// key ids must be equal to get the same intervention
											if (lFeedbackGiverInterventionTag.getAttribute(AppConstants.keyId).equals(
													lReactionGiverInterventionTag.getAttribute(AppConstants.keyId))) {
												// the feedbackinspected tag is a child tag of the status tag of one of
												// the current intervention tags
												IXMLTag lFeedbackInspectedTag = getFeedbackInspectedTagForRga(
														lFeedbackGiverRga, lFeedbackGiverInterventionTag,
														lCurrentInterventionTags);
												if (lFeedbackInspectedTag != null) {
													lFoundFeedbackInspectedTag = lFeedbackInspectedTag;
												}
											}
										}
										addInspectedFeedbackData(lCurrentRga,
												(CRunIspotInspectedFeedback) lIntervention, lFoundFeedbackInspectedTag);
										lResultList.add(lIntervention);
										lPreviousIntervention = lIntervention;
									}
								}
							}
						}
					}
				}
			}
		}
		clearTempInterventionTags();
		return lResultList;
	}

	/**
	 * Adds inspected feedback data.
	 *
	 * @param aRga                 the run group account
	 * @param aInspectedFeedback   the inspected feedback
	 * @param aFeedbackReceivedTag the feedback received tag
	 */
	public void addInspectedFeedbackData(IERunGroupAccount aRga, CRunIspotInspectedFeedback aInspectedFeedback,
			IXMLTag aFeedbackReceivedTag) {
		aInspectedFeedback.setInspectedFeedbackRga(aRga);
		String lState = "empty";
		String lDoneDate = "";
		// NOTE the feedback inspected tag is null if no feedback inspected yet
		if (aFeedbackReceivedTag != null) {
			lState = getState(aFeedbackReceivedTag);
			lDoneDate = aFeedbackReceivedTag.getCurrentStatusAttribute("finisheddate");
		}
		aInspectedFeedback.setInspectedFeedbackState(lState);
		aInspectedFeedback.setInspectedFeedbackDoneDate(lDoneDate);
	}

	/**
	 * Get vignette intervention object id.
	 *
	 * @return the vignette intervention object id
	 */
	public String getVignetteInterventionId() {
		return "ispotVignette";
	}

	/**
	 * Get feedback intervention object id.
	 *
	 * @return the feedback intervention object id
	 */
	public String getFeedbackInterventionId() {
		return "ispotGivenFeedback";
	}

	/**
	 * Set current intervention recording object id.
	 *
	 * @param aRecordingId the recording type
	 */
	public void setCurrentRecordingId(String aRecordingId) {
		currentRecordingId = aRecordingId;
	}

	/**
	 * Get current intervention recording object id.
	 *
	 * @return the recording id
	 */
	public String getCurrentRecordingId() {
		return currentRecordingId;
	}

	/**
	 * Set current played url.
	 *
	 * @param aPlayedUrl the played url
	 */
	public void setCurrentPlayedUrl(String aPlayedUrl) {
		currentPlayedUrl = aPlayedUrl;
	}

	/**
	 * Set intervention status. Is used to set intervention status changes within
	 * Ispot zul files.
	 *
	 * @param aIntervention the intervention
	 * @param aStatusKey    the status key
	 * @param aStatusValue  the status value
	 */
	public void setInterventionStatus(CRunIspotIntervention aIntervention, String aStatusKey, String aStatusValue) {
		setRunTagStatus(aIntervention.getCaseComponent(), aIntervention.getTag(), aStatusKey, aStatusValue, true);
		// NOTE call this method so application memory will be updated
		updateAppMemoryForCurrentRga();
	}

	/**
	 * Get intervention status. Is used to get intervention status within Ispot zul
	 * files.
	 *
	 * @param aIntervention the intervention
	 * @param aStatusKey    the status key
	 *
	 * @return the intervention status
	 */
	public String getInterventionStatus(CRunIspotIntervention aIntervention, String aStatusKey) {
		return aIntervention.getTag().getCurrentStatusAttribute(aStatusKey);
	}

	/**
	 * Save intervention reaction. Is used in Ispot zul files.
	 *
	 * @param aIntervention    the intervention
	 * @param aFileNameToStore the 'internal url' name of the file, to store in a
	 *                         blob
	 */
	public void saveInterventionReaction(CRunIspotIntervention aIntervention, String aFileNameToStore) {
		saveStreamingServerInterventionReaction(aIntervention, aFileNameToStore);
	}

	/**
	 * Save intervention feedback. Is used in Ispot zul files.
	 *
	 * @param aGivenFeedback   the given feedback
	 * @param aFileNameToStore the 'internal url' name of the file, to store in a
	 *                         blob
	 */
	public void saveInterventionFeedback(CRunIspotGivenFeedback aGivenFeedback, String aFileNameToStore) {
		saveStreamingServerInterventionGivenFeedback(aGivenFeedback, aFileNameToStore);
	}

	/**
	 * Gets status status tag.
	 *
	 * @param aParentTag the parent tag
	 *
	 * @return the status status tag
	 */
	protected IXMLTag getStatusStatusTag(IXMLTag aParentTag) {
		if (aParentTag == null) {
			return null;
		}
		IXMLTag lChildTag = aParentTag.getChild(AppConstants.statusElement);
		if (lChildTag == null) {
			lChildTag = sSpring.getXmlManager().newXMLTag(AppConstants.statusElement, "");
			aParentTag.getChildTags().add(lChildTag);
		}
		return lChildTag;
	}

	/**
	 * Gets status status child tag.
	 *
	 * @param aParentTag the parent tag
	 * @param aTagName   the tag name
	 * @param aTagType   the tag type
	 * @param aAttKey    the attribute key to check
	 * @param aAttValue  the attribute value to check
	 *
	 * @return the child tag
	 */
	protected IXMLTag getStatusStatusChildTag(IXMLTag aParentTag, String aTagName, String aTagType, String aAttKey,
			String aAttValue) {
		if (aParentTag == null) {
			return null;
		}
		List<IXMLTag> lChildTags = aParentTag.getChilds(aTagName);
		IXMLTag lFoundChildTag = null;
		for (IXMLTag lChildTag : lChildTags) {
			if (Strings.isEmpty(aAttKey) || lChildTag.getAttribute(aAttKey).equals(aAttValue)) {
				lFoundChildTag = lChildTag;
				break;
			}
		}
		if (lFoundChildTag == null) {
			// no child yet
			lFoundChildTag = sSpring.getXmlManager().newXMLTag(aTagName, "");
			if (!aTagType.equals("")) {
				lFoundChildTag.setAttribute(AppConstants.defKeyType, aTagType);
			}
			if (!Strings.isEmpty(aAttKey)) {
				lFoundChildTag.setAttribute(aAttKey, aAttValue);
			}
			aParentTag.getChildTags().add(lFoundChildTag);
		}
		return lFoundChildTag;
	}

	/**
	 * Saves status change.
	 */
	protected void saveStatusChange() {
		// save status
		IXMLTag lStatusRootTag = sSpring.getXmlRunStatusTree(caseComponent, AppConstants.statusTypeRunGroup);
		sSpring.setSaveRunGroupCaseComponentStatusRootTag(sSpring.getRunGroup(), caseComponent, lStatusRootTag);
		// NOTE call this method so application memory will be updated
		updateAppMemoryForCurrentRga();
	}

	/**
	 * Stores name of intervention recording saved on streaming server into blob.
	 *
	 * @param aIntervention       the intervention
	 * @param aBlobTagName        the blob tag name
	 * @param aReactionRgaIdKey   the reaction rga id key
	 * @param aReactionRgaIdValue the reaction rga id value
	 * @param aUrl                the url
	 * @param aTagName            the tag name
	 * @param aTagId              the tag id
	 *
	 * @return blo id, if successful, else empty string
	 */
	protected String saveStreamingServerInterventionRecording(CRunIspotIntervention aIntervention, String aBlobTagName,
			String aReactionRgaIdKey, String aReactionRgaIdValue, String aUrl, String aTagName, String aTagId, boolean pAllVignettes) {
		IXMLTag lInterventionStatusTag = getInterventionStatusTag(aIntervention, pAllVignettes);
		if (lInterventionStatusTag == null) {
			return "";
		}
		IXMLTag lStatusStatusTag = getStatusStatusTag(lInterventionStatusTag);
		IXMLTag lStatusStatusChildTag = getStatusStatusChildTag(lStatusStatusTag, aBlobTagName, "blob",
				aReactionRgaIdKey, aReactionRgaIdValue);
		lStatusStatusChildTag.setAttribute(AppConstants.keyBlobtype, AppConstants.blobtypeIntUrl);
		String lBlobId = lStatusStatusChildTag.getValue();
		if (PropsValues.STREAMING_SERVER_STORE) {
			// NOTE don't remove old blob if storage is on streaming server, because blob id
			// is not used in path to webcam recording
			lBlobId = sSpring.getSBlobHelper().replaceUrlBlob(lBlobId, aUrl);
		} else {
			IEBlob lBlob = (IEBlob) CDesktopComponents.cControl().getRunSessAttr("iSpotRecordingBlob");
			if (lBlob != null) {
				IEBlob lOldBlob = sSpring.getSBlobHelper().getBlob(lBlobId);
				if (lOldBlob != null) {
					String lOldUrl = lOldBlob.getUrl();
					if (lOldUrl.contains("_conv.")) {
						String lOldUrlNotconverted = lOldUrl.replace("_conv.", ".");
						lOldBlob.setUrl(lOldUrlNotconverted);
						sSpring.getBlobManager().deleteFile(lOldBlob);
						lOldBlob.setUrl(lOldUrl);
					}
					sSpring.getBlobManager().deleteBlob(lOldBlob, false);
				}
				lBlobId = "" + lBlob.getBloId();
			}
		}
		lStatusStatusChildTag.setValue(lBlobId);

		sSpring.setRunTagStatusValue(lInterventionStatusTag, AppConstants.statusKeySaved, AppConstants.statusValueTrue);
		sSpring.getSLogHelper().logSetRunTagAction("setruntagstatus", AppConstants.statusTypeRunGroup,
				sSpring.getXmlManager().unescapeXML(caseComponent.getName()), aTagName, aTagId,
				AppConstants.statusKeySaved, AppConstants.statusValueTrue, false);

		// save status
		saveStatusChange();
		return lBlobId;
	}

	/**
	 * Stores name of intervention reaction saved on streaming server into blob.
	 *
	 * @param aIntervention the intervention
	 *
	 * @return blo id, if successful, else empty string
	 */
	protected String saveStreamingServerInterventionReaction(CRunIspotIntervention aIntervention,
			String aFileNameToStore) {
		return saveStreamingServerInterventionRecording(aIntervention, "reaction", null, null, aFileNameToStore,
				aIntervention.getTag().getName(), aIntervention.getTag().getChildValue("pid"), false);
	}

	/**
	 * Stores name of intervention given feedback saved on streaming server into
	 * blob.
	 *
	 * @param aGivenFeedback the given feedback
	 *
	 * @return blo id, if successful, else empty string
	 */
	protected String saveStreamingServerInterventionGivenFeedback(CRunIspotGivenFeedback aGivenFeedback,
			String aFileNameToStore) {
		return saveStreamingServerInterventionRecording(aGivenFeedback, "feedbackgiven", "reactionrgaid",
				"" + aGivenFeedback.getReactionRga().getRgaId(), aFileNameToStore, aGivenFeedback.getTag().getName(),
				aGivenFeedback.getTag().getChildValue("pid") + "_feedback", true);
	}

	/**
	 * Gets intervention status tag.
	 *
	 * @param aIntervention the intervention
	 *
	 * @return intervention status tag
	 */
	protected IXMLTag getInterventionStatusTag(CRunIspotIntervention aIntervention, boolean pAllVignettes) {
		IXMLTag lStatusRootTag = sSpring.getXmlRunStatusTree(aIntervention.getCaseComponent(),
				AppConstants.statusTypeRunGroup);
		if (lStatusRootTag == null) {
			return null;
		}
		IXMLTag lStatusContentTag = lStatusRootTag.getChild(AppConstants.contentElement);
		if (lStatusContentTag == null) {
			return null;
		}
		String lInterventionTagId = aIntervention.getTag().getAttribute(AppConstants.keyId);
		IXMLTag lInterventionStatusTag = null;
		for (IXMLTag lStatusTag : lStatusContentTag.getChilds("intervention")) {
			if (lStatusTag.getAttribute(AppConstants.keyRefdataid).equals(lInterventionTagId)) {
				lInterventionStatusTag = lStatusTag;
			}
		}
		if (lInterventionStatusTag == null) {
			// create new status tag
			lInterventionStatusTag = sSpring.newRunNodeTag(aIntervention.getTag().getName(),
					aIntervention.getTag().getAttribute(AppConstants.keyId), null, null);
			lInterventionStatusTag.setAttribute(AppConstants.defKeyType, AppConstants.defValueNode);
			sSpring.getXmlManager().newChildNodeSimple(lStatusRootTag, lInterventionStatusTag, null);

		}
		// NOTE set status tag of current (feedback giver or feedback inspector) intervention tag
		IXMLTag lCurrentInterventionTag = getInterventionTagById(sSpring.getRunGroupAccount(), lInterventionTagId, pAllVignettes);
		if (lCurrentInterventionTag.getStatusTag() == null) {
			lCurrentInterventionTag.setStatusTag(lInterventionStatusTag);
		}
		return lInterventionStatusTag;
	}

	/**
	 * Set given feedback status. Is used to set given feedback status changes
	 * within Ispot zul files.
	 *
	 * @param aGivenFeedback the given feedback
	 * @param aStatusKey     the status key
	 * @param aStatusValue   the status value
	 */
	public void setGivenFeedbackStatus(CRunIspotGivenFeedback aGivenFeedback, String aStatusKey, String aStatusValue) {
		IXMLTag lInterventionStatusTag = getInterventionStatusTag(aGivenFeedback, true);
		if (lInterventionStatusTag == null) {
			return;
		}
		IXMLTag lStatusStatusTag = getStatusStatusTag(lInterventionStatusTag);
		IXMLTag lStatusStatusChildTag = getStatusStatusChildTag(lStatusStatusTag, "feedbackgiven", "blob",
				"reactionrgaid", "" + aGivenFeedback.getReactionRga().getRgaId());
		lStatusStatusChildTag.setAttribute(AppConstants.keyBlobtype, AppConstants.blobtypeIntUrl);
		sSpring.setRunTagStatusValue(lStatusStatusChildTag, aStatusKey, aStatusValue);
		// save status
		saveStatusChange();
	}

	/**
	 * Set inspected feedback status. Is used to set inspected feedback status
	 * changes within Ispot zul files.
	 *
	 * @param aInspectedFeedback the inspected feedback
	 * @param aStatusKey         the status key
	 * @param aStatusValue       the status value
	 */
	public void setInspectedFeedbackStatus(CRunIspotInspectedFeedback aInspectedFeedback, String aStatusKey,
			String aStatusValue) {
		IXMLTag lInterventionStatusTag = getInterventionStatusTag(aInspectedFeedback, true);
		if (lInterventionStatusTag == null) {
			return;
		}
		IXMLTag lStatusStatusTag = getStatusStatusTag(lInterventionStatusTag);
		IXMLTag lStatusStatusChildTag = getStatusStatusChildTag(lStatusStatusTag, "feedbackreceived", "",
				"feedbackgivenrgaid", "" + aInspectedFeedback.getGivenFeedbackRga().getRgaId());
		sSpring.setRunTagStatusValue(lStatusStatusChildTag, aStatusKey, aStatusValue);
		// save status
		saveStatusChange();
	}

	@Override
	public void onAction(String sender, String action, Object status) {
		if (action.equals("onExternalEvent")) {
			String[] lStatus = (String[]) status;
			String lSender = lStatus[0];
			String lReceiver = lStatus[1];
			String lEvent = lStatus[2];
			String lEventData = lStatus[3];
			String[] lEventDataValues = lEventData.split(",");
			if (sender.matches("jwplayer|videojsplayer")) {
				// iSpot plays vignette fragment, expert feedback fragment, reaction fragment or
				// feedback fragment
				if (lEvent.equals("onComplete") || lEvent.equals("onError")) {
					// if flv/mp4 has reached the end, the player sends an onComplete, in
					// run_[flash/mp4]_macro_fr.zul
					// notify ispotVignette and let it decide what to do
					Events.sendEvent("onIspotStreamCompleted", vView.getComponent("ispotVignette"), currentPlayedUrl);
					// notify iSpotInspectedFeedback and let it decide what to do
					Events.sendEvent("onIspotStreamCompleted", vView.getComponent("ispotInspectedFeedback"),
							currentPlayedUrl);
				}
				if (lEvent.equals("onError")) {
					logMessage("error in " + sender + ": file " + currentPlayedUrl + ": " + lEventDataValues[1]);
					Events.sendEvent("onIspotWebcamError", vView.getComponent("ispotViewers"), lEventDataValues[1]);
				}
			}
			if (sender.matches("webcam")) {
				Component lRecComponent = vView.getComponent(currentRecordingId);
				if (lEvent.equals("onRecordingStarted")) {
					logMessage("iSpot - webcam.onRecordingStarted - " + currentRecordingId);
					Events.sendEvent("onIspotRecordingStarted", lRecComponent, null);
				} else if (lEvent.equals("onRecordingPaused")) {
					Events.sendEvent("onIspotRecordingPaused", lRecComponent, null);
					logMessage("iSpot - webcam.onRecordingPaused - " + currentRecordingId);
				} else if (lEvent.equals("onRecordingResumed")) {
					Events.sendEvent("onIspotRecordingResumed", lRecComponent, null);
					logMessage("iSpot - webcam.onRecordingResumed - " + currentRecordingId);
				} else if (lEvent.equals("onRecordingStopped")) {
					logMessage("iSpot - webcam.onRecordingStopped - " + currentRecordingId);
				} else if (lEvent.equals("onRecordComplete")) {
					String lFileName = lEventDataValues[1];
					// distinguish between recording of reaction or feedback
					Events.sendEvent("onIspotSaveRecording", lRecComponent, lFileName);
					logMessage("iSpot - end of webcam recording - " + currentRecordingId + ": " + lFileName);
					releaseRecording();
				} else if (lEvent.equals("onShowWebcamSettings")) {
					Events.sendEvent("onIspotShowWebcamSettings", lRecComponent, null);
				} else if (lEvent.equals("onError")) {
					logMessage("error in recording from webcam: " + lEventDataValues[1]);
					Events.sendEvent("onIspotWebcamError", vView.getComponent("ispotViewers"), lEventDataValues[1]);
					releaseRecording();
				} else if (lEvent.equals("onLog")) {
					logMessage("browser webcam status: " + lEventDataValues[1]);
				}
			}
		}
	}

	/**
	 * Logs a message. Handles messages generated in Ispot zul files.
	 *
	 * @param aMessage the message
	 */
	public void logMessage(String aMessage) {
		sSpring.getSLogHelper().logAction(aMessage);
	}

	@Override
	public void cleanup() {
		releaseRecording();
		super.cleanup();
	}

	/**
	 * Methods originally used in ZUL file.
	 */

	public void onInitZulfile(Event aEvent) {
		canCurrentRgaSeeFeedbackOnOthers = canCurrentRgaSeeFeedbackOnOthers();

		Map<String, Object> propertyMap = new HashMap<String, Object>();
		propertyMap.put("currentEmergoComponent", this);

		is_Menu_SingleItem = true;
		instructionVisible = "";
		if (!getInstruction().equals("")) {
			is_Menu_SingleItem = false;
			instructionVisible = "true";
		} else {
			instructionVisible = "false";
		}
		colophonVisible = "";
		if (!getColophon().equals("")) {
			is_Menu_SingleItem = false;
			colophonVisible = "true";
		} else {
			colophonVisible = "false";
		}
		is_Menu_AutoStartType = "";
		feedbackVisible = false;
		for (int i = 0; i < vignetteTypes.length; i++) {
			List<CRunIspotIntervention> dataElements = getInterventions(vignetteTypes[i]);
			if (dataElements == null || dataElements.size() == 0) {
				vignetteTypesVisible[i] = "false";
			} else {
				if (is_Menu_SingleItem) {
					if (is_Menu_AutoStartType.equals("")) {
						is_Menu_AutoStartType = vignetteTypes[i];
					} else {
						is_Menu_SingleItem = false;
					}
				}
				if (isGivingFeedbackAllowed(vignetteTypes[i])) {
					feedbackVisible = true;
					is_Menu_SingleItem = false;
				}
			}
		}
		giveFeedbackVisible = "" + feedbackVisible;
		inspectFeedbackVisible = giveFeedbackVisible;

		String[] events = new String[] { "onIspotExplanation", "onShowVignettes", "onShowVignettes", "onShowVignettes", "onShowVignettes",
				"onGiveFeedbacks", "onInspectFeedbacks", "onIspotColophon" };
		String[] eventData = new String[] { "", vignetteTypes[0], vignetteTypes[1], vignetteTypes[2], vignetteTypes[3], "", "", "" };
		String[] menuLabels = new String[] {
				sSpring.replaceVariablesWithinString(vView.getLabel("run_ispot.menuoption.ispot_explanation")),
				sSpring.replaceVariablesWithinString(vView.getLabel("run_ispot.menuoption." + vignetteTypes[0])),
				sSpring.replaceVariablesWithinString(vView.getLabel("run_ispot.menuoption." + vignetteTypes[1])),
				sSpring.replaceVariablesWithinString(vView.getLabel("run_ispot.menuoption." + vignetteTypes[2])),
				sSpring.replaceVariablesWithinString(vView.getLabel("run_ispot.menuoption." + vignetteTypes[3])),
				sSpring.replaceVariablesWithinString(vView.getLabel("run_ispot.menuoption.give_feedback")),
				sSpring.replaceVariablesWithinString(vView.getLabel("run_ispot.menuoption.inspect_feedback")),
				sSpring.replaceVariablesWithinString(vView.getLabel("run_ispot.menuoption.ispot_colophon")) };
		String[] optionVisible = new String[] { instructionVisible, vignetteTypesVisible[0], vignetteTypesVisible[1],
				vignetteTypesVisible[2], vignetteTypesVisible[3], giveFeedbackVisible, inspectFeedbackVisible, colophonVisible };
		List<String[]> buttonsData = new ArrayList<String[]>();
		String type = "Menu";
		for (int i = 0; i < events.length; i++) {
			buttonsData.add(new String[] { events[i], eventData[i], menuLabels[i],
					"" + (optionVisible[i].equals("true") && !menuLabels[i].equals("")) });
		}
		propertyMap.put("iSpotButtonsData" + type, buttonsData);

		propertyMap.put("iSpotListboxStyle", listboxStyle);

		buttonsData = new ArrayList<String[]>();
		type = interventionTypeVignette;
		buttonsData.add(new String[] { "ispotPreviousButton" + type, "Previous" + type,
				vView.getLabel("run_ispot.button.previous_vignette"), "DarkGreen" });
		buttonsData.add(new String[] { "ispotNextButton" + type, "Next" + type,
				vView.getLabel("run_ispot.button.next_vignette"), "DarkGreen" });
		buttonsData.add(new String[] { "ispotShowInstructionButton" + type, "ShowInstruction",
				vView.getLabel("run_ispot.button.instruction"), "DarkBlue" });
		buttonsData.add(new String[] { "ispotShowVignetteButton" + type, "ShowVignette",
				vView.getLabel("run_ispot.button.vignette"), "DarkBlue" });
		buttonsData.add(new String[] { "ispotShowFeedbackButton" + type, "ShowFeedback",
				vView.getLabel("run_ispot.button.example_feedback"), "DarkBlue" });
		buttonsData.add(new String[] { "ispotShowRecordingButton" + type, "ShowRecording",
				vView.getLabel("run_ispot.button.recording"), "DarkRed" });
		buttonsData.add(new String[] { "ispotStartRecordingButton" + type, "StartRecording",
				vView.getLabel("run_ispot.button.start_recording"), "DarkRed" });
		buttonsData.add(new String[] { "ispotPauseRecordingButton" + type, "PauseRecording",
				vView.getLabel("run_ispot.button.pause_recording"), "DarkRed" });
		buttonsData.add(new String[] { "ispotResumeRecordingButton" + type, "ResumeRecording",
				vView.getLabel("run_ispot.button.resume_recording"), "DarkRed" });
		buttonsData.add(new String[] { "ispotStopRecordingButton" + type, "StopRecording",
				vView.getLabel("run_ispot.button.stop_recording"), "DarkRed" });
		buttonsData.add(new String[] { "ispotRedoRecordingButton" + type, "RedoRecording",
				vView.getLabel("run_ispot.button.redo_recording"), "DarkRed" });
		buttonsData.add(new String[] { "ispotSendRecordingButton" + type, "SendRecording",
				vView.getLabel("run_ispot.button.send_recording"), "DarkRed" });
		propertyMap.put("iSpotButtonsData" + type, buttonsData);

		buttonsData = new ArrayList<String[]>();
		type = interventionTypeGivenFeedback;
		buttonsData.add(new String[] { "ispotPreviousButton" + type, "Previous" + type,
				vView.getLabel("run_ispot.button.previous_given_feedback"), "DarkGreen" });
		buttonsData.add(new String[] { "ispotNextButton" + type, "Next" + type,
				vView.getLabel("run_ispot.button.next_given_feedback"), "DarkGreen" });
		buttonsData.add(new String[] { "ispotShowInstructionButton" + type, "ShowInstruction",
				vView.getLabel("run_ispot.button.instruction"), "DarkBlue" });
		buttonsData.add(new String[] { "ispotShowVignetteButton" + type, "ShowVignette",
				vView.getLabel("run_ispot.button.vignette"), "DarkBlue" });
		buttonsData.add(new String[] { "ispotShowFeedbackButton" + type, "ShowFeedback",
				vView.getLabel("run_ispot.button.example_feedback"), "DarkBlue" });
		buttonsData.add(new String[] { "ispotShowReactionButton" + type, "ShowReaction",
				vView.getLabel("run_ispot.button.reaction"), "DarkBlue" });
		buttonsData.add(new String[] { "ispotShowRecordingButton" + type, "ShowRecording",
				vView.getLabel("run_ispot.button.recording"), "DarkRed" });
		buttonsData.add(new String[] { "ispotStartRecordingButton" + type, "StartRecording",
				vView.getLabel("run_ispot.button.start_recording"), "DarkRed" });
		buttonsData.add(new String[] { "ispotPauseRecordingButton" + type, "PauseRecording",
				vView.getLabel("run_ispot.button.pause_recording"), "DarkRed" });
		buttonsData.add(new String[] { "ispotResumeRecordingButton" + type, "ResumeRecording",
				vView.getLabel("run_ispot.button.resume_recording"), "DarkRed" });
		buttonsData.add(new String[] { "ispotStopRecordingButton" + type, "StopRecording",
				vView.getLabel("run_ispot.button.stop_recording"), "DarkRed" });
		buttonsData.add(new String[] { "ispotRedoRecordingButton" + type, "RedoRecording",
				vView.getLabel("run_ispot.button.redo_recording"), "DarkRed" });
		buttonsData.add(new String[] { "ispotSendRecordingButton" + type, "SendRecording",
				vView.getLabel("run_ispot.button.send_recording"), "DarkRed" });
		propertyMap.put("iSpotButtonsData" + type, buttonsData);

		buttonsData = new ArrayList<String[]>();
		type = interventionTypeInspectedFeedback;
		buttonsData.add(new String[] { "ispotPreviousButton" + type, "Previous" + type,
				vView.getLabel("run_ispot.button.previous_inspected_feedback"), "DarkGreen" });
		buttonsData.add(new String[] { "ispotNextButton" + type, "Next" + type,
				vView.getLabel("run_ispot.button.next_inspected_feedback"), "DarkGreen" });
		buttonsData.add(new String[] { "ispotShowInstructionButton" + type, "ShowInstruction",
				vView.getLabel("run_ispot.button.instruction"), "DarkBlue" });
		buttonsData.add(new String[] { "ispotShowVignetteButton" + type, "ShowVignette",
				vView.getLabel("run_ispot.button.vignette"), "DarkBlue" });
		buttonsData.add(new String[] { "ispotShowFeedbackButton" + type, "ShowFeedback",
				vView.getLabel("run_ispot.button.example_feedback"), "DarkBlue" });
		buttonsData.add(new String[] { "ispotShowReactionButton" + type, "ShowReaction",
				vView.getLabel("run_ispot.button.reaction"), "DarkBlue" });
		buttonsData.add(new String[] { "ispotShowCommentButton" + type, "ShowComment",
				vView.getLabel("run_ispot.button.peer_feedback"), "DarkBlue" });
		propertyMap.put("iSpotButtonsData" + type, buttonsData);

		String reactionGiverVisible = canCurrentRgaSeeFeedbackOnOthers ? "true" : "false";
		String titleColWidth = canCurrentRgaSeeFeedbackOnOthers ? "187px" : "250px";
		propertyMap.put("iSpotInspectedFeedbackReactionGiverVisible", reactionGiverVisible);
		propertyMap.put("iSpotInspectedFeedbackTitleColWidth", titleColWidth);
		propertyMap.put("iSpotVignetteInterventionId", getVignetteInterventionId());
		propertyMap.put("iSpotFeedbackInterventionId", getFeedbackInterventionId());

		((HtmlMacroComponent) getRunContentComponent()).setDynamicProperty("a_propertyMap", propertyMap);
		((HtmlMacroComponent) getRunContentComponent()).setMacroURI(VView.v_run_ispot_fr);
		getRunContentComponent().setVisible(true);
	}

	public void setComponentVisible(Component component, boolean visible) {
		if (component != null) {
			component.setVisible(visible);
		}
	}

	protected int getMaxNumberOfConcurrentRecordings() {
		if (maxNrOfConcurrentRecordings == null) {
			try {
				maxNrOfConcurrentRecordings = PropsValues.STREAMING_RECORDER_CONCURRENT_NUMBER;
			} catch (NumberFormatException e) {
				maxNrOfConcurrentRecordings = 1;
			}
		}
		return maxNrOfConcurrentRecordings;
	}

	public boolean registerNewRecording(boolean pStoreLocal) {
		// NOTE For now, number of concurrent recordings streaming to Wowza server is
		// limited to 1. When using EMERGO server, we do not (yet) limit the number.
		if (pStoreLocal || (nrOfConcurrentRecordings < getMaxNumberOfConcurrentRecordings())) {
			nrOfConcurrentRecordings += 1;
			busyRecording = true;
			logMessage("Recording registered; number of active recordings: " + nrOfConcurrentRecordings);
			return true;
		}
		logMessage(
				"Request for recording refused; limit of " + nrOfConcurrentRecordings + " active recordings reached.");
		return false;
	}

	public void releaseRecording() {
		if (busyRecording) {
			nrOfConcurrentRecordings -= 1;
			busyRecording = false;
			logMessage("Recording cleared from memory; remaining number of active recordings: "
					+ nrOfConcurrentRecordings);
		}
	}

}
