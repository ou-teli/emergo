/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.IP2;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Div;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefVbox;

public class CRunDashboardSatisfactionVboxTemplate extends CDefVbox {

	private static final long serialVersionUID = 1036158652643169751L;

	public void onInit(Event aEvent) {
		CRunDashboardDiv runDashboardDiv = (CRunDashboardDiv)CDesktopComponents.vView().getComponent("dashboardDiv");
		if (runDashboardDiv == null) {
			return;
		}
		int number = ((int[])aEvent.getData())[0];
		int offsetx = ((int[])aEvent.getData())[1];
		int offsety = ((int[])aEvent.getData())[2];
		//
		setStyle("position:absolute;left:" + offsetx + "px;top:" + offsety + "px;");
		//
		Label label = (Label)getChildren().get(0).getChildren().get(0);
		label.setValue((String)runDashboardDiv.getMinigamesatisfactionData(number, "specialist"));
		//
		Div div = (Div)getChildren().get(1).getChildren().get(0).getChildren().get(1);
		Component component = (Component)CDesktopComponents.vView().getComponent("templateMinigameSatisfactionItem_").clone();
		component.setId("");
		div.insertBefore(component, null);
		Events.postEvent("onInit", component, new Object[]{"nice", ((double[])runDashboardDiv.getMinigamesatisfactionData(number, "satisfaction"))[0]});
		//
		div = (Div)getChildren().get(1).getChildren().get(0).getChildren().get(3);
		component = (Component)CDesktopComponents.vView().getComponent("templateMinigameSatisfactionItem_").clone();
		component.setId("");
		div.insertBefore(component, null);
		Events.postEvent("onInit", component, new Object[]{"difficult", ((double[])runDashboardDiv.getMinigamesatisfactionData(number, "satisfaction"))[1]});
		//
		Image image = (Image)getChildren().get(1).getChildren().get(1);
		image.setSrc((String)runDashboardDiv.getMinigamesatisfactionData(number, "imagesrc"));
		//
		setVisible(true);
	}

}
