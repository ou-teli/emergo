/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

import javax.servlet.ServletContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import nl.surf.emergo.control.CDesktopComponents;

public class CRunComponentLabelLocator implements org.zkoss.util.resource.LabelLocator {
	private static final Logger _log = LogManager.getLogger(CRunComponentLabelLocator.class);
	private ServletContext _svlctx;
	private String _name;
	private Locale _locale;

	public CRunComponentLabelLocator(ServletContext svlctx, String name, Locale locale) {
		_svlctx = svlctx;
		_name = name;
		_locale = locale;
	}

	@Override
	public URL locate(Locale locale) {
		try {
			if (CDesktopComponents.sSpring().getRun() != null) {
				URL lResource = _svlctx.getResource(CDesktopComponents.sSpring().getSBlobHelper().getIntUrl(
						CDesktopComponents.sSpring().getRun().getECase(), _name + "_" + locale + ".properties", false));
				if (lResource == null)
					lResource = _svlctx.getResource(CDesktopComponents.sSpring().getSBlobHelper()
							.getIntUrl(CDesktopComponents.sSpring().getRun().getECase(), _name + ".properties", false));
				return lResource;
			}
		} catch (MalformedURLException e) {
			_log.error(e);
		}
		return null;
	}

}
