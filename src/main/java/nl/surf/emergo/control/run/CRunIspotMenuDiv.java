/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.view.VView;

public class CRunIspotMenuDiv extends CDefDiv {

	private static final long serialVersionUID = -1095749658196651533L;

	protected VView vView = CDesktopComponents.vView();

	protected CRunIspot runIspot = null;

	public void onCreate(CreateEvent aEvent) {
		runIspot = (CRunIspot)vView.getComponent("runIspot");
		if (runIspot.is_Menu_SingleItem && !runIspot.is_Menu_AutoStartType.equals("")) {
			Events.postEvent("onShowVignettes", this, runIspot.is_Menu_AutoStartType);
		}
	}

	public void onIspotExplanation(Event aEvent) {
		handleIspotSubComponent("ispotInstruction", true, true, null);
	}

	public void onShowVignettes(Event aEvent) {
		handleIspotSubComponent("ispotVignettes", false, !runIspot.is_Menu_SingleItem || runIspot.is_Menu_AutoStartType.equals(""), aEvent.getData());
	}

	public void onGiveFeedbacks(Event aEvent) {
		handleIspotSubComponent("ispotGivenFeedbacks", true, true, aEvent.getData());
	}

	public void onInspectFeedbacks(Event aEvent) {
		handleIspotSubComponent("ispotInspectedFeedbacks", true, true, aEvent.getData());
	}

	public void onIspotColophon(Event aEvent) {
		handleIspotSubComponent("ispotColophon", true, true, null);
	}

	protected void handleIspotSubComponent(String componentId, boolean showComponent, boolean showMenuButton, Object data) {
		runIspot.setSclass("CRunIspotOpaque");
		Component ispotSubComponent = vView.getComponent(componentId);
		if (ispotSubComponent != null) {
			setVisible(false);
			if (showMenuButton) {
				runIspot.setComponentVisible(vView.getComponent("ispotMenuButton"), true);
			}
			Events.postEvent("onUpdate", ispotSubComponent, data);
			if (showComponent) {
				ispotSubComponent.setVisible(true);
			}
		}
	}

	public void onMenu(Event aEvent) {
		runIspot.setSclass("CRunIspotTransparent");
		Events.postEvent("onIspotCancelRecording", vView.getComponent("ispotVignette"), null);
		Events.postEvent("onIspotCancelRecording", vView.getComponent("ispotGivenFeedback"), null);
		runIspot.setComponentVisible(vView.getComponent("ispotMenuButton"), false);
		runIspot.setComponentVisible(vView.getComponent("ispotViewers"), false);
		runIspot.setComponentVisible(vView.getComponent("ispotInstruction"), false);
		runIspot.setComponentVisible(vView.getComponent("ispotVignettes"), false);
		runIspot.setComponentVisible(vView.getComponent("ispotVignettesButton"), false);
		runIspot.setComponentVisible(vView.getComponent("ispotVignette"), false);
		runIspot.setComponentVisible(vView.getComponent("ispotGivenFeedbacks"), false);
		runIspot.setComponentVisible(vView.getComponent("ispotGivenFeedbacksButton"), false);
		runIspot.setComponentVisible(vView.getComponent("ispotGivenFeedback"), false);
		runIspot.setComponentVisible(vView.getComponent("ispotInspectedFeedbacks"), false);
		runIspot.setComponentVisible(vView.getComponent("ispotInspectedFeedbacksButton"), false);
		runIspot.setComponentVisible(vView.getComponent("ispotInspectedFeedback"), false);
		runIspot.setComponentVisible(vView.getComponent("ispotColophon"), false);
		setVisible(true);
		Clients.evalJavaScript("showWebcamMessage('');");
	}

	public void onEnableMenu(Event aEvent) {
		runIspot.setComponentVisible(vView.getComponent("ispotMenuButton"), (Boolean)aEvent.getData() && (!runIspot.is_Menu_SingleItem || runIspot.is_Menu_AutoStartType.equals("")));
	}

}
