/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.feedback;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Vbox;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitInitBox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefHbox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefHtml;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefVbox;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTag;

public class CRunPVToolkitFeedbackSubSkillButton extends CRunPVToolkitDefDiv {
	
	private static final long serialVersionUID = 808157820001018208L;

	public String zulfilepath = ((CRunPVToolkitInitBox)CDesktopComponents.vView().getComponent("PV-toolkit_initBox")).getZulfilepath();
	
	protected int _numberOfPerformanceLevels = 0;
	protected int _numberOfPerformanceLevelsToShow  = 0;
	protected int[] _performanceLevels;
	
	protected int _maxLabelLength = 33;
	protected int _starWidth = 18;

	protected CRunPVToolkitCacAndTag _runPVToolkitCacAndTag;
	protected String[] _childNames;
	protected int _level;
	protected boolean _hasScore;
	protected boolean _hasTipsTops;
	protected String _feedbackRubricId;

	protected CRunPVToolkitFeedbackRubricDiv _feedbackRubric;

	protected boolean _accessible = true;

	protected String _stepPrefix;
	protected String _classPrefix;
	
	protected CRunPVToolkit pvToolkit;
	
	protected final static int tipTopsButtonWidth = 20;
	protected final static int performanceLevelVboxWidth = 160;
	
	public CRunPVToolkitFeedbackSubSkillButton(Component parent, String[] keys, Object[] values) {
		super(parent, keys, values);
	}
	
	public void init(CRunPVToolkitCacAndTag runPVToolkitCacAndTag, String[] childNames, int level, String feedbackRubricId, String stepPrefix, String classStr) {
		_runPVToolkitCacAndTag = runPVToolkitCacAndTag;
		_childNames = childNames;
		_feedbackRubricId = feedbackRubricId;
		_stepPrefix = stepPrefix;
		_classPrefix = classStr;

		_feedbackRubric = (CRunPVToolkitFeedbackRubricDiv)CDesktopComponents.vView().getComponent(_feedbackRubricId);
		_hasScore = hasScore();
		_hasTipsTops = hasTipsTops();

		IXMLTag subSkillTag = _runPVToolkitCacAndTag.getXmlTag();
		//NOTE sub skill and possibly skill cluster are accessible. Skill tag always is accessible.
		_accessible = !subSkillTag.getCurrentStatusAttribute(AppConstants.statusKeyAccessible).equals(AppConstants.statusValueFalse);
		if (subSkillTag.getParentTag().getName().equals("skillcluster")) {
			_accessible	= _accessible && !subSkillTag.getParentTag().getCurrentStatusAttribute(AppConstants.statusKeyAccessible).equals(AppConstants.statusValueFalse);
		}

		if (!_hasScore || _feedbackRubric.inOverview() || _feedbackRubric.inRubric()) {
			setStyle("cursor:default;");
		}
		if (_feedbackRubric.inOverview()) {
			_maxLabelLength = 47;
		}
		if (_feedbackRubric.inRubric()) {
			_maxLabelLength = Integer.MAX_VALUE;
		}

		pvToolkit = (CRunPVToolkit)CDesktopComponents.vView().getComponent("pvToolkit");
		
		_numberOfPerformanceLevels = pvToolkit.getNumberOfPerformanceLevels();
		_numberOfPerformanceLevelsToShow = pvToolkit.getNumberOfPerformanceLevelsToShow();
		_performanceLevels = pvToolkit.getPerformanceLevels();
		
		update(level);
	}
	
	protected boolean hasScore() {
		return _feedbackRubric.subskillHasScore();
	}
	
	protected boolean hasTipsTops() {
		return _feedbackRubric.subskillHasTipsTops();
	}
	
	public void update() {
		update(_level);
	}
	
	protected String imagePrefix() {
		if (_runPVToolkitCacAndTag.getXmlTag().getName().equals("skillcluster")) {
			return "subskill";
		}
		else {
			//NOTE if no skill clusters use image of skill component so position and width of sub skill components is correct
			return "skill";
		}
	}
	
	public void update(int level) {
		if (!_feedbackRubric.inRubric()) {
			updateButton(level);
		}
		else {
			updateRubric(level);
		}
	}

	public void updateButton(int level) {
		_level = level;
		getChildren().clear();

		String src = "";
		if (_level > 0) {
			src = zulfilepath + imagePrefix() + "-blue.svg";
		}
		else {
			src = zulfilepath + imagePrefix() + ".svg";
		}
		if (!_feedbackRubric.inOverview()) {
			new CRunPVToolkitDefImage(this, 
					new String[]{"class", "src"}, 
					new Object[]{_classPrefix + "Background", src}
			);
		}

		Div div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "Label"}
		);

		String style = "";
		if (_level == CRunPVToolkit.noLevelPossible) {
			style = "color:#999999;";
		}
		else {
			if (!_feedbackRubric.inOverview() && _level > 0) {
				style = "color:#ffffff;";
			}
		}
		if (!StringUtils.isEmpty(getLabelKey())) {
			new CRunPVToolkitDefLabel(div, 
					new String[]{"class", "style", "labelKey", "maxLabelLength"}, 
					new Object[]{"font " + _classPrefix + "Label", style, getLabelKey(), _maxLabelLength}
					);
		}
		else {
			new CRunPVToolkitDefLabel(div, 
					new String[]{"class", "style", "cacAndTag", "tagChildName", "maxLabelLength"}, 
					new Object[]{"font " + _classPrefix + "Label", style, _runPVToolkitCacAndTag, _childNames[0], _maxLabelLength}
					);
		}
		
		style = "";
		int right = 10;
		if (_hasTipsTops) {
			right += tipTopsButtonWidth;
		}
		style = "width:" + (_starWidth * _numberOfPerformanceLevelsToShow) + "px;right:" + right + "px;";
		div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class", "style"}, 
				new Object[]{_classPrefix + "Level", style}
		);

		if (_hasScore && _accessible) {
			CRunPVToolkitFeedbackScoreHbox hbox = new CRunPVToolkitFeedbackScoreHbox(div, null, null);
			hbox.init(_runPVToolkitCacAndTag, _level, _feedbackRubricId);
		}

		if (_hasTipsTops && _accessible) {
			CRunPVToolkitFeedbackTipsTopsDiv tipsTopsDiv = new CRunPVToolkitFeedbackTipsTopsDiv(this, 
					new String[]{"class"}, 
					new Object[]{_classPrefix + "TipsTops"}
			);
			tipsTopsDiv.init(_runPVToolkitCacAndTag, _level, _feedbackRubricId, _stepPrefix);
		}
	}

	public void updateRubric(int level) {
		_level = level;
		getChildren().clear();
		
		Hbox hbox = new CRunPVToolkitDefHbox(this, null, null);

		Div div = new CRunPVToolkitDefDiv(hbox, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "Label"}
		);

		String style = "";
		if (!_feedbackRubric.inOverview()) {
			if (_level > 0) {
				style = "color:#ffffff;";
			}
		}
		if (!StringUtils.isEmpty(getLabelKey())) {
			new CRunPVToolkitDefLabel(div, 
					new String[]{"class", "style", "labelKey", "maxLabelLength"}, 
					new Object[]{"font " + _classPrefix + "Label", style, getLabelKey(), _maxLabelLength}
					);
		}
		else {
			Vbox vbox = new CRunPVToolkitDefVbox(div, null, null);
			new CRunPVToolkitDefLabel(vbox, 
					new String[]{"class", "style", "cacAndTag", "tagChildName", "maxLabelLength"}, 
					new Object[]{"font " + _classPrefix + "Label", style, _runPVToolkitCacAndTag, _childNames[0], _maxLabelLength}
					);
			new CRunPVToolkitDefLabel(vbox, 
					new String[]{"class", "style", "cacAndTag", "tagChildName", "maxLabelLength"}, 
					new Object[]{"font " + _classPrefix + "Description", style, _runPVToolkitCacAndTag, _childNames[1], _maxLabelLength}
					);
		}
		
		List<IXMLTag> performancelevelTags = _runPVToolkitCacAndTag.getXmlTag().getChilds("performancelevel");
		//NOTE width of vbox depend on number of performance levels
		double width = 5 * performanceLevelVboxWidth;
		width = width / _numberOfPerformanceLevelsToShow;
		String widthStyle = "width:" + width + "px;";
		if (!pvToolkit.getCurrentRubricCode().equals("PP")) {
			for (int i=0;i<performancelevelTags.size();i++) {
				showRubricPerformanceLevel(performancelevelTags.get(i), hbox, widthStyle);
			}
		}
		else {
			for (int i=(performancelevelTags.size()-1);i>=0;i--) {
				showRubricPerformanceLevel(performancelevelTags.get(i), hbox, widthStyle);
			}
		}
	}

	public void showRubricPerformanceLevel(IXMLTag performancelevelTag, Hbox hbox, String widthStyle) {
		int performanceLevel = Integer.parseInt(performancelevelTag.getChildValue("level"));
		if (_performanceLevels[performanceLevel-1] <= 0) {
			return;
		}
		Vbox vbox = new CRunPVToolkitDefVbox(hbox,
				new String[]{"class", "style"}, 
				new Object[]{_classPrefix + "ButtonLevelVbox", widthStyle}
				);

		//rubricSubSkillButtonLevelVbox
	
		/*div = new CRunPVToolkitDefDiv(vbox, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "ButtonLevel" + performanceLevel}
				);
		div = new CRunPVToolkitDefDiv(div, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "ButtonLevelDescription"}
				); */
		CRunPVToolkitCacAndTag runPVToolkitCacAndPerformancelevelTag = new CRunPVToolkitCacAndTag(_runPVToolkitCacAndTag.getCaseComponent(), performancelevelTag);
	
		List<IXMLTag> performanceLevelExampleTags = pvToolkit.getPresentChildTags(runPVToolkitCacAndPerformancelevelTag, "videoperformancelevelexample");
		if (performanceLevelExampleTags.size() > 0) {
			String labelKey = "PV-toolkit-feedback.button.videoexample";
			if (performanceLevelExampleTags.size() > 1) {
				labelKey += "s";
			}
			Label link = new CRunPVToolkitDefLabel(vbox, 
					new String[]{"class", "labelKey"}, 
					new Object[]{"font gridLink " + _classPrefix + "ScoreLevelVideoExamples", labelKey}
					);
			_feedbackRubric.addRubricVideoExamplesLinkOnClickEventListener(link, runPVToolkitCacAndPerformancelevelTag, "videoperformancelevelexample", true);
		}

		new CRunPVToolkitDefHtml(vbox,
				new String[]{"class", "cacAndTag", "tagChildName"}, 
				new Object[]{"font " + _classPrefix + "ScoreLevelDescription", runPVToolkitCacAndPerformancelevelTag, "description"}
		);
	}

	protected String getLabelKey() {
		return "";
	}
	
	public void onClick() {
		if (!_hasScore || _feedbackRubric.inOverview() || _feedbackRubric.inRubric()) {
			return;
		}
		CRunPVToolkitFeedbackScoreLevelsDiv feedbackScoreLevels = (CRunPVToolkitFeedbackScoreLevelsDiv)CDesktopComponents.vView().getComponent(_stepPrefix + "ScoreLevelsDiv");
		if (feedbackScoreLevels != null) {
			feedbackScoreLevels.setAttribute("skillclusterCounter", getAttribute("skillclusterCounter"));
			feedbackScoreLevels.setAttribute("subskillCounter", getAttribute("subskillCounter"));
			feedbackScoreLevels.init(_runPVToolkitCacAndTag, _level, _hasTipsTops, _feedbackRubricId);
			feedbackScoreLevels.setVisible(true);
		}
	}

}
