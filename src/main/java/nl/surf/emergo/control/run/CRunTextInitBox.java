/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefBox;
import nl.surf.emergo.view.VView;

public class CRunTextInitBox extends CDefBox {

	private static final long serialVersionUID = 8172780662602210130L;

	// NOTE maxBalloonChars depends on size of balloon, so maybe should be entered with balloon text or somehow must be determined by trying to put text
	// and skip words until no overflow. 
	public static final int maxBalloonChars = 300;
	public static final int minDelay = 3000;
	public static final int maxDelay = 15000;
	// NOTE spacing better should be css value
	public static final int spacing = 10;

	public String runComponentId = "";
	public String innerWidth = "";
	public String innerHeight = "";

	public void onCreate(CreateEvent aEvent) {
		Events.echoEvent("onInit", this, null);
	}

	public void onInit(Event aEvent) {
		CDesktopComponents.sSpring().initDesktopAttributes(getDesktop());

		runComponentId = (String)VView.getParameter("runComponentId", this, "runConversations");

		//NOTE fragment size may overwrite width of parent CRunConversations
		String txt_width = (String)VView.getParameter("fragment_size_width", this, (String)VView.getParameter("video_size_width", this, (String)getDesktop().getAttribute("runWndWidth")));
		String txt_height = (String)VView.getParameter("fragment_size_height", this, (String)VView.getParameter("video_size_height", this, (String)getDesktop().getAttribute("runWndHeight")));

		innerWidth = "" + (Integer.parseInt(txt_width.substring(0, txt_width.length() - 2)) - 2 * (spacing + 1)) + "px";
		innerHeight = "" + (Integer.parseInt(txt_height.substring(0, txt_height.length() - 2)) - 2 * (spacing + 1)) + "px";
		
		HtmlMacroComponent macro = new HtmlMacroComponent();
		appendChild(macro);
		macro.setDynamicProperty("a_runComponentId", runComponentId);
		macro.setDynamicProperty("a_maxDelay", maxDelay);
		macro.setDynamicProperty("a_spacing", spacing);
		macro.setDynamicProperty("a_innerWidth", innerWidth);
		macro.setDynamicProperty("a_innerHeight", innerHeight);
		macro.setMacroURI("../run_text_view_fr_macro.zul");
	}

}
