/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.webservices.client.rest.TestsClient;
import nl.surf.emergo.zkspring.STriggeredReference;

/**
 * The Class CRunTests is used to show tests component within the run view area of the emergo player..
 */
public class CRunTests extends CRunForms {

	private static final long serialVersionUID = -1800168412596837585L;

	protected String _statusKeyTestsGroupId = "_keyTestsGroupId";
	
	public final static String runTestsId = "runTests";

	/**
	 * Instantiates a new c run tests.
	 */
	public CRunTests() {
		super("runTests", null);
		conversationsFeedbackIdSuffix = "Tests";
		init();
	}

	/**
	 * Instantiates a new c run tests.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the case component
	 */
	public CRunTests(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
		conversationsFeedbackIdSuffix = "Tests";
		init();
	}

	/**
	 * Creates title area, content area to show tab structure,
	 * and buttons area with close button.
	 */
	@Override
	protected void createComponents() {
		CRunVbox lVbox = new CRunVbox();

		createTitleArea(lVbox);
		createContentArea(lVbox);
		createButtonsArea(lVbox);

		appendChild(lVbox);
	}

	/**
	 * Creates new content component, the tree.
	 *
	 * @return the component
	 */
	@Override
	protected Component newContentComponent() {
		return newContentComponent("runTestsView");
	}

	/**
	 * Creates buttons area and adds close button.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run hbox
	 */
	@Override
	protected CRunHbox createButtonsArea(Component aParent) {
		CRunHbox lButtonsHbox = super.createButtonsArea(aParent);
		createCloseButton(lButtonsHbox);
		return lButtonsHbox;
	}

	/**
	 * Creates view.
	 */
	public void init() {
		formChildTagNames = "section,test";
		
		Events.postEvent("onInitZulfile", this, null);
	}

	/**
	 * Gets the run component helper.
	 *
	 * @return the run component helper
	 */
	@Override
	public CRunComponentHelper getRunComponentHelper() {
		return new CRunComponentHelper(null, "form", caseComponent, this);
	}

	/**
	 * Get tests url. If aFormTag has url. It is added if it is not set yet.
	 *
	 * @param aFormTag the a form tag
	 *
	 * @return the tests url
	 */
	public String getTestsUrl(IXMLTag aFormTag) {
		String lUrl = "";
		if (aFormTag != null) {
			lUrl = CDesktopComponents.sSpring().getSBlobHelper().getUrl(aFormTag);
		}
		if (!lUrl.equals("")) {
			//if has url, return it
			return lUrl;
		}
		lUrl = aFormTag.getCurrentStatusAttribute(AppConstants.statusKeyUrl);
		if (!lUrl.equals("")) {
			//if url is set, return it
			return lUrl;
		}
		//otherwise get url and save it
		TestsClient testsClient = new TestsClient();
		Hashtable<String, String> testsData = testsClient.getTestsData();
		if (testsData.containsKey("testsUrl")) {
			lUrl = testsData.get("testsUrl");
		}
		if (!lUrl.equals("")) {
			setRunTagStatus(getCaseComponent(), aFormTag, AppConstants.statusKeyUrl, lUrl, true);
		}
		if (testsData.containsKey("testsGroupId")) {
			// save group id for later use
			setRunTagStatus(getCaseComponent(), aFormTag, _statusKeyTestsGroupId, testsData.get("testsGroupId"), true);
		}
		return lUrl;
	}

	/**
	 * Get tests user id. If aFormTag has user id. It is added if it is not set yet.
	 *
	 * @param aFormTag the a form tag
	 *
	 * @return the tests user id
	 */
	public String getTestsUserId(IXMLTag aFormTag) {
		String lUserId = aFormTag.getCurrentStatusAttribute(AppConstants.statusKeyId);
		if (!lUserId.equals("")) {
			//if user id is set, return it
			return lUserId;
		}
		//otherwise get user id and save it
		TestsClient testsClient = new TestsClient();
		Hashtable<String, String> testsData = testsClient.getTestsUserData(getTestsUrl(aFormTag));
		if (testsData.containsKey("userId")) {
			lUserId = testsData.get("userId");
		}
		if (!lUserId.equals("")) {
			setRunTagStatus(getCaseComponent(), aFormTag, AppConstants.statusKeyId, lUserId, true);
		}
		return lUserId;
	}

	/**
	 * Get test node.
	 *
	 * @param aTestTag the a test tag
	 *
	 * @return the test node
	 */
	protected String getTestNodeId(IXMLTag aTestTag) {
		IXMLTag lFormTag = aTestTag.getParentTag();
		String lKeyTestNode = "_" + CDesktopComponents.sSpring().unescapeXML(aTestTag.getChildValue("type"));
		String lNodeId = lFormTag.getCurrentStatusAttribute(lKeyTestNode);
		if (lNodeId.equals("")) {
			//get test nodes and save them
			TestsClient testsClient = new TestsClient();
			Hashtable<String, String> testsData = testsClient.getTests(lFormTag.getCurrentStatusAttribute(_statusKeyTestsGroupId));
			for (Enumeration<String> keys = testsData.keys(); keys.hasMoreElements();) {
				String lTestType = keys.nextElement();
				String lTestNode = testsData.get(lTestType);
				setRunTagStatus(getCaseComponent(), lFormTag, "_" + lTestType, lTestNode, true);
			}
			lNodeId = lFormTag.getCurrentStatusAttribute(lKeyTestNode);
		}
		return lNodeId;
	}

	/**
	 * Get test result.
	 *
	 * @param aTestTag the a test tag
	 *
	 * @return the test result
	 */
	public String getTestResult(IXMLTag aTestTag) {
		String lResult = aTestTag.getCurrentStatusAttribute(AppConstants.statusKeyAnswer);
		if (!lResult.equals("")) {
			lResult = lResult.replaceAll("'", "\"");
			//if result is set, return it
			return lResult;
		}
		//otherwise get result and save it
		TestsClient testsClient = new TestsClient();
		lResult = testsClient.getTestsResult(aTestTag.getParentTag().getCurrentStatusAttribute(_statusKeyTestsGroupId), getTestsUserId(aTestTag.getParentTag()));
		if (!lResult.equals("")) {
			lResult = lResult.replaceAll("\"", "'");
			setRunTagStatus(getCaseComponent(), aTestTag, AppConstants.statusKeyAnswer, lResult, true);
			setRunTagStatus(getCaseComponent(), aTestTag, AppConstants.statusKeyFinished, AppConstants.statusValueTrue, true);
		}
		return lResult;
	}

	/**
	 * Saves section text.
	 *
	 * @param aSectionTag the a section tag
	 * @param aText the a text
	 */
	public void saveSectionText(IXMLTag aSectionTag, String aText) {
		//NOTE because the text is saved as an attribute value \n has to be replaced by a string constant.
		//Otherwise the text is not reproduced correctly, crlf's are then replaced by spaces.
		aText = aText.replace("\n", AppConstants.statusCrLfReplace);
		setRunTagStatus(getCaseComponent(), aSectionTag, AppConstants.statusKeyString, CDesktopComponents.sSpring().escapeXML(aText), true);
	}

	/**
	 * Increase number of attempts.
	 *
	 * @param aTag the a tag
	 *
	 * @return number of attempts
	 */
	public int increaseNumberOfAttempts(IXMLTag aTag) {
		int lNumberOfAttempts = 0;
		try {
			lNumberOfAttempts = Integer.parseInt(aTag.getCurrentStatusAttribute(AppConstants.statusKeyNumberofattempts));
		} catch (NumberFormatException e) {
			lNumberOfAttempts = 0;
		}
		lNumberOfAttempts++;
		setRunTagStatus(getCaseComponent(), aTag, AppConstants.statusKeyNumberofattempts, "" + lNumberOfAttempts, true);
		return lNumberOfAttempts;
	}

	
	/**
	 * Show feedback.
	 *
	 * @return the feedback condition tag
	 */
	public IXMLTag showFeedback() {
		IXMLTag lFeedbackCondition = super.showFeedback(getFormTags());
	  	if (lFeedbackCondition != null) {
		  	//NOTE score -1 means 'no relevant score yet' because tool instruction is shown, so don'nt increase number of attempts.
		  	if (!lFeedbackCondition.getChildValue("score").equals("-1")) {
				//increase numberofattempts for all form tags
				List<IXMLTag> lFormTags = getPresentChildTags(getDataPlusStatusRootTag().getChild(AppConstants.contentElement), "form");
				for (IXMLTag lFormTag : lFormTags) {
					int lNumberOfAttempts = 0;
					try {
						lNumberOfAttempts = Integer.parseInt(lFormTag.getCurrentStatusAttribute(AppConstants.statusKeyNumberofattempts));
					} catch (NumberFormatException e) {
						lNumberOfAttempts = 0;
					}
					lNumberOfAttempts++;
					setRunTagStatus(getCaseComponent(), lFormTag, AppConstants.statusKeyNumberofattempts, "" + lNumberOfAttempts, true);
				}
		  	}
	  	}
		return lFeedbackCondition;
	}

	@Override
	public String getFeedbackTitle() {
		return CDesktopComponents.vView().getLabel("run_tests.alert.feedback.title");
	}

	@Override
	public String getDefaultFeedbackText() {
		return CDesktopComponents.vView().getLabel("run_tests.alert.feedback.defaulttext");
	}

	@Override
	public void handleStatusChange(STriggeredReference aTriggeredReference) {
		Events.postEvent("onHandleStatusChange", this, aTriggeredReference);
	}


	/**
	 * Methods originally used in ZUL file.
	 */

	public void onInitZulfile(Event aEvent) {
		String componentStyle = "";
		String url = getBackgroundUrl();
		if (!url.equals("")) {
			componentStyle = "background-image:url('" + url + "');";
		}
		
		IXMLTag triggeredFeedbackConditionTag = getTriggeredFeedbackConditionTag();
		if (triggeredFeedbackConditionTag != null) {
			//NOTE only show feedback if it is an instruction, which is indicated by a score of -1
			if (sSpringHelper.getTagChildValue(triggeredFeedbackConditionTag, "score", "-1").equals("-1")) {
				Events.postEvent("onShowFeedback", this, null);
			}
		}

		//NOTE showFeedback and showScore are for all forms!
		showFeedback = false;
		showScore = false;
		dataElements = new ArrayList<Hashtable<String,Object>>();
		List<IXMLTag> formTags = getFormTags();
		for (int i=0;i<formTags.size();i++) {
			IXMLTag formTag = formTags.get(i);
			Hashtable<String,Object> hFormDataElements = new Hashtable<String,Object>();
			hFormDataElements.put("tag", formTag);
			hFormDataElements.put("title", sSpringHelper.getTagChildValue(formTag, "title", ""));
			hFormDataElements.put("hovertext", sSpringHelper.getTagChildValue(formTag, "hovertext", ""));
			hFormDataElements.put("showfeedback", sSpringHelper.getTagStatusChildAttribute(formTag, "showfeedback", "true"));
			if (hFormDataElements.get("showfeedback").equals("true")) {
				showFeedback = true;
			}
			hFormDataElements.put("showscore", sSpringHelper.getTagStatusChildAttribute(formTag, "showscore", "false"));
			if (hFormDataElements.get("showscore").equals("true")) {
				showScore = true;
			}

			//NOTE following list is needed so you can do nested foreach looping! see below.
			List<Hashtable<String,Object>> pieceOrRefpieceDataElementsList = new ArrayList<Hashtable<String,Object>>();
			List<IXMLTag> pieceOrRefpieceTags = getPieceAndRefpieceTags(formTag);
			handlepieceOrRefpieces(pieceOrRefpieceDataElementsList, pieceOrRefpieceTags); 
			hFormDataElements.put("pieces", pieceOrRefpieceDataElementsList);

			//NOTE following list is needed so you can do nested foreach looping! see below.
			List<Hashtable<String,Object>> itemDataElementsList = new ArrayList<Hashtable<String,Object>>();
			//NOTE get all item tags, also non present ones
			List<IXMLTag> itemTags = getAllItemTags(formTag);
			for (int j=0;j<itemTags.size();j++) {
				IXMLTag itemTag = itemTags.get(j);

				boolean isShowIfRightOrWrong = isShowIfRightOrWrong(itemTag);

				Hashtable<String,Object> hItemDataElements = new Hashtable<String,Object>();
				hItemDataElements.put("tag", itemTag);
				hItemDataElements.put("title", sSpringHelper.getTagChildValue(itemTag, "title", ""));
				boolean overwrite = true;
				if (itemTag.getName().equals("section")) {
					overwrite = sSpringHelper.getTagStatusChildAttribute(itemTag, "overwrite", "true").equals("true");
					String value = "";
					if (overwrite) {
						value = sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, "string", "");
					}
					if (value.equals("")) {
						value = sSpringHelper.getTagChildValue(itemTag, "defaulttext", "");
					}
					//NOTE for flat text \n has to be replaced.
					hItemDataElements.put("flatvalue", value.replace("\n", "<br/>"));
					hItemDataElements.put("value", value);
					hItemDataElements.put("overwrite", "" + overwrite);
					hItemDataElements.put("numberoflines", sSpringHelper.getTagChildValue(itemTag, "numberoflines", "5"));
					if (overwrite && isShowIfRightOrWrong) {
						hItemDataElements.put("isinputcorrect", isInputCorrect(itemTag));
					}
					else {
						hItemDataElements.put("isinputcorrect", "");
					}
				}
				if (itemTag.getName().equals("test")) {
					//TODO uncomment next line and delete the following line
					//					hItemDataElements.put("value", getDefaulttext(itemTag));
					hItemDataElements.put("value", "");
				}
				hItemDataElements.put("styleposition", runWnd.getStylePosition(itemTag));
				hItemDataElements.put("stylewidth", runWnd.getStyleWidth(itemTag));
				hItemDataElements.put("stylesize", runWnd.getStyleSize(itemTag));
				hItemDataElements.put("present", sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, AppConstants.statusKeyPresent, "true"));

				itemDataElementsList.add(hItemDataElements);
			}
			hFormDataElements.put("items", itemDataElementsList);

			//NOTE following list is needed so you can do nested foreach looping! see below.
			pieceOrRefpieceDataElementsList = new ArrayList<Hashtable<String,Object>>();
			IXMLTag feedbackConditionTag = getCurrentFeedbackConditionTag(formTag);
			pieceOrRefpieceTags = getPieceAndRefpieceTags(feedbackConditionTag);
			handlepieceOrRefpieces(pieceOrRefpieceDataElementsList, pieceOrRefpieceTags); 
			hFormDataElements.put("feedbackpieces", pieceOrRefpieceDataElementsList);

			dataElements.add(hFormDataElements);
		}

		Map<String,Object> propertyMap = new HashMap<String,Object>();
		propertyMap.put("currentEmergoComponent", this);
		propertyMap.put("dataElements", dataElements);
		propertyMap.put("componentStyle", componentStyle);
		propertyMap.put("feedbackButtonUrl", sSpringHelper.stripEmergoPath(getFeedbackButtonUrl()));
		((HtmlMacroComponent)getRunContentComponent()).setDynamicProperty("a_propertyMap", propertyMap);
		((HtmlMacroComponent)getRunContentComponent()).setMacroURI(VView.v_run_tests_fr);
		getRunContentComponent().setVisible(true);
	}

	public void onShowFeedback(Event aEvent) {
		if (showFeedback) {
			IXMLTag currentFormTag = (IXMLTag)dataElements.get(currentFormIndex).get("tag");
			IXMLTag triggeredFeedbackConditionTag = showFeedback(currentFormTag);
			//NOTE score -1 means 'no relevant score yet' because tool instruction is shown, so feedback mechanism is misused to show reactions of npc's.
			//TODO rename feedback in reaction in Java and zul
			if (triggeredFeedbackConditionTag != null) {
				if (!sSpringHelper.getTagChildValue(triggeredFeedbackConditionTag, "score", "-1").equals("-1")) {
					List<Component> testItems = vView.getComponentsByPrefix("testItem_");
					List<IXMLTag> handledTags = new ArrayList<IXMLTag>();
					for (Component testItem : testItems) {
						//NOTE testItemOrContainer is used to keep the number of attempts
						Component testItemOrContainer = getTestItemOrContainer(testItem);
						//NOTE testItemDiv is set visible or not using script
						Component testItemDiv = getTestItemDiv(testItem);
						if (testItemOrContainer != null && testItemDiv != null && testItemDiv.isVisible()) {
							//NOTE only update number of attempts and send onUpdate if testItemDiv tag is present
							if (!handledTags.contains(testItemOrContainer.getAttribute("tag"))) {
								handledTags.add((IXMLTag)testItemOrContainer.getAttribute("tag"));
								increaseNumberOfAttempts((IXMLTag)testItemOrContainer.getAttribute("tag"));
							}
							Events.postEvent("onUpdate", testItem, null);
						}
					}
				}
				for (Hashtable<String,Object> dataElement : dataElements) {
					//for each form
					Events.postEvent("onUpdate", vView.getComponent("feedbackPieces_" + ((IXMLTag)dataElement.get("tag")).getAttribute("id")), triggeredFeedbackConditionTag);
				}
			}
		}
		else {
			showDefaultFeedback();
		}
	}

	public void onHandleStatusChange(Event aEvent) {
		//NOTE to handle pieces and refpieces call super
		super.onHandleStatusChange(aEvent, "testItemDiv_");
		STriggeredReference triggeredReference = (STriggeredReference)aEvent.getData();
		if (triggeredReference != null) {
			if (triggeredReference.getDataTag() != null &&
				(triggeredReference.getDataTag().getName().equals("section") ||
				triggeredReference.getDataTag().getName().equals("test")) &&
				triggeredReference.getStatusKey().equals(AppConstants.statusKeyPresent)) {
				Component lComponent = CDesktopComponents.vView().getComponent("testItemDiv_" + triggeredReference.getDataTag().getAttribute(AppConstants.keyId));
				if (lComponent != null) {
					lComponent.setVisible(triggeredReference.getStatusValue().equals(AppConstants.statusValueTrue));
				}
			}
		}
	}
	
	@Override
	public boolean isShowIfRightOrWrong(IXMLTag tag) {
		if (tag.getName().equals("test")) {
			return false;
		}
		//get form tag
		IXMLTag formTag = getFormTag(tag);
		if (formTag == null) {
			return false;
		}
		int maxNumber = runWnd.getNumber(sSpringHelper.getTagChildValue(formTag, "maxnumberofattempts", "-1"), -1);
		int number = runWnd.getNumber(sSpringHelper.getCurrentStatusTagStatusChildAttribute(tag, AppConstants.statusKeyNumberofattempts, "0"), 0);
		return maxNumber > -1 && number >= maxNumber; 
	}
	
	public String isInputCorrect(IXMLTag itemTag) {
		if (itemTag.getName().equals("section")) {
			Pattern p = Pattern.compile(sSpringHelper.getTagChildValue(itemTag, "correctanswer", ""), Pattern.DOTALL);
			Matcher m = p.matcher(sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, "string", ""));
			return "" + m.matches();
		}
		if (itemTag.getName().equals("selector")) {
			Pattern p = Pattern.compile(sSpringHelper.getTagChildValue(itemTag, "correctoptions", ""), Pattern.DOTALL);
			Matcher m = p.matcher(sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, "answer", ""));
			return "" + m.matches();
		}
		if (itemTag.getName().equals("alternative")) {
			boolean correct = sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, "correct", "false").equals("true");
			boolean selected = sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, "selected", "false").equals("true");
			if (selected) {
				return "" + correct;
			}
			else {
				return "";
			}
		}
		if (itemTag.getName().equals("text")) {
			boolean correct = sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, "correct", "false").equals("true");
			return "" + correct;
		}

		return "false";
	}

	public Component getTestItemDiv(Component zulItem) {
		Component testItemDiv = zulItem;
		while (testItemDiv != null && !testItemDiv.getId().startsWith("testItemDiv_", 0)) {
			testItemDiv = testItemDiv.getParent();
		}
		return testItemDiv;
	}

	public Component getTestItemOrContainer(Component zulItem) {
		//NOTE returns testItem or for alternatives and texts the parent container
		if (zulItem.getAttribute("tag") == null) {
			return null;
		}
		Component testItemOrContainer = zulItem;
		if (((IXMLTag)zulItem.getAttribute("tag")).getName().equals("alternative") || ((IXMLTag)zulItem.getAttribute("tag")).getName().equals("text")) {
			while (testItemOrContainer != null && (testItemOrContainer.getAttribute("tag") == null || testItemOrContainer.getAttribute("tag") == zulItem.getAttribute("tag"))) {
				testItemOrContainer = testItemOrContainer.getParent();
			}
			if (testItemOrContainer == null || testItemOrContainer.getAttribute("tag") == null) {
				return null;
			}
		}
		return testItemOrContainer;
	}

	public void updateTestItem(HtmlBasedComponent zulItem, String sclassPrefix, String hideRightWrongStr) {		
		//NOTE testItemOrContainer is used to keep the number of attempts
		Component testItemOrContainer = getTestItemOrContainer(zulItem);
		if (testItemOrContainer == null) {
			return;
		}
		boolean hideRightWrong = hideRightWrongStr != null && hideRightWrongStr.equals("hideRightWrong");
		String sclass = sclassPrefix;
		if (isShowIfRightOrWrong((IXMLTag)testItemOrContainer.getAttribute("tag")) && !hideRightWrong) {
			String isInputCorrect = isInputCorrect((IXMLTag)zulItem.getAttribute("tag"));
			if (isInputCorrect.equals("true")) {
				sclass += "Right";
			}
			else if (isInputCorrect.equals("false")) {
				sclass += "Wrong";
			}
		}
		if (!zulItem.getSclass().equals(sclass)) {
			zulItem.setSclass(sclass);
		}
	}

	public String getDefaulttext(IXMLTag testTag) {
		String value = sSpringHelper.getTagChildValue(testTag, "defaulttext", "");
		String result = "";
		//TODO uncomment next line
		//result = getTestResult(testTag);
		JSONObject jsonObject = null;
		//TODO Parsing json may be not necessary anymore!
		if (!result.equals("")) {
			//JSONParser jsonParser = new JSONParser();
			//jsonObject = (JSONObject)jsonParser.parse(result);
		}
		String unknown = vView.getLabel("run_tests.unknown");
		int startPos = value.indexOf("[%");
		int endPos = value.indexOf("%]");
		while (startPos >= 0 && endPos >= 0 && endPos > startPos) {
			String parameter = value.substring(startPos + 2, endPos);
			if (jsonObject == null) {
				parameter = unknown; 
			}
			else {
				parameter = (String)jsonObject.get(parameter);
			}
			value = value.substring(0, startPos) + parameter + value.substring(endPos + 2);
			startPos = value.indexOf("[%");
			endPos = value.indexOf("%]");
		}
		//NOTE for flat text \n has to be replaced.
		return value.replace("\n", "<br/>"); 
	}

}
