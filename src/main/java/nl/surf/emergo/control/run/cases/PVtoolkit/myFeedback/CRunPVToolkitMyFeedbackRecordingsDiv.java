/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.myFeedback;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefHelper;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.cases.PVtoolkit.feedback.CRunPVToolkitFeedbackRecordingsDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitFeedbackPractice;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;

public class CRunPVToolkitMyFeedbackRecordingsDiv extends CRunPVToolkitFeedbackRecordingsDiv {

	private static final long serialVersionUID = -8589581515041437552L;

	@Override
	public void init(IERunGroup actor, boolean reset, boolean editable, String subStepType) {
		_showMyFeedback = true;

		_stepPrefix = "myFeedback";
		
		setClass(_classPrefix + "Recordings");
		super.init(actor, reset, editable, subStepType);
		if (!_initialized)
			setVisible(true);
		
		_initialized = true;
	}
	
	@Override
	public void update() {
		getChildren().clear();
		
		pvToolkit.setMemoryCaching(true);
		
		caseComponent = pvToolkit.getCurrentRubricsCaseComponent();
		skillTag = pvToolkit.getCurrentSkillTag();

		Div div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{"titleRight"}
		);
		
		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font titleRight", "PV-toolkit-myFeedback.header.myFeedback"}
		);
		
		div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "RecordingsDiv"}
		);
		
		Rows rows = appendRecordingsGrid(div);
		int rowNumber = 1;
		rowNumber = appendRecordingsToGrid(rows, rowNumber);
		
		pvToolkit.setMemoryCaching(false);
		
	}

    protected Rows appendRecordingsGrid(Component parent) {
    	return pvToolkit.appendGrid(
    			parent, 
    			getId() + "Grid",
    			"position:absolute;left:0px;top:0px;width:1060px;height:480px;overflow:auto;", 
				new boolean[] {true, true, true, pvToolkit.hasCycleTags(), true, true}, 
    			new String[] {"3%", "42%", "25%", "5%", "20%", "15%"}, 
    			new boolean[] {false, true, true, true, true, false},
    			null,
    			"PV-toolkit-myFeedback.column.label.",
    			new String[] {"", "activity", "peergroup", "cycle", "deadline", "feedback"});
    }

	protected int appendRecordingsToGrid(Rows rows, int rowNumber) {
		return appendRecordingsToGrid(
				rows, 
				rowNumber,
				new boolean[] {true, true, true, pvToolkit.hasCycleTags(), true, true} 
				);
	}

	protected int appendRecordingsToGrid(
			Rows rows, 
			int rowNumber,
    		boolean[] showColumn 
			) {
		// NOTE include also former peer group members
		List<IXMLTag> lAllPracticeStepTags = pvToolkit.getStepTags(CRunPVToolkit.practiceStepType);
		List<IXMLTag> lCyclePracticeStepTags = new ArrayList<>();
		// NOTE assume step tags are sorted in order of cycles
		for (int lInd = 0; lInd < pvToolkit.getCurrentCycleNumber(); lInd++) {
			lCyclePracticeStepTags.add(lAllPracticeStepTags.get(lInd));
		}

		List<CRunPVToolkitFeedbackPractice> feedbackPractices = pvToolkit.getCycleFeedbackPractices(_actor, lCyclePracticeStepTags, false, null, null, true);
		if (feedbackPractices == null) {
			return rowNumber;
		}
		
		// NOTE because of peculiarities in Rob's demonstrator run, where peers in each cycle are copies of peers in the previous cycle (but in different peer groups)
		// different peers are connected to the same practice, so in this case multiple practices with the same practiceUuId are present. To avoid 'not unique'
		// errors, remove duplicate practice tags, only keeping the last one, unless we find one connected to a peer in the current peer group
		// NOTE: find members of _actor's current peer group
		List<IERunGroup> lPeerRGs = pvToolkit.getUsersThatPractice(_actor);
		List<IERunGroupAccount> lPeerRGAs = pvToolkit.getRunGroupAccounts(lPeerRGs);
		ListIterator<IERunGroupAccount> lPRGAIterator = lPeerRGAs.listIterator();
		List<String> lRGAids = new ArrayList<>();
		while (lPRGAIterator.hasNext()) {
			IERunGroupAccount lRGA = lPRGAIterator.next();
			lRGAids.add(lRGA.getRgaId() + "");
		}
		Map<String, CRunPVToolkitFeedbackPractice> lRelevantFBPracticesMap = new LinkedHashMap<>();
		List<String> lDealtPrUuids = new ArrayList<>();
		for (CRunPVToolkitFeedbackPractice lFBPractice : feedbackPractices) {
			String lPrTagUuid = lFBPractice.getPracticeTag().getAttribute(CRunPVToolkit.practiceUuId);
			if (!lDealtPrUuids.contains(lPrTagUuid)) {
				String lPrTagRgaid = lFBPractice.getPracticeTag().getAttribute(CRunPVToolkit.practiceRgaId);
				if (lRGAids.contains(lPrTagRgaid)) {
					lDealtPrUuids.add(lPrTagUuid);
				}
				lRelevantFBPracticesMap.put(lPrTagUuid, lFBPractice);
			}
		}

		//NOTE: now list 'in order of appearance'
		for (CRunPVToolkitFeedbackPractice feedbackPractice : lRelevantFBPracticesMap.values()) {
			setFeedbackStepTag(feedbackPractice.getFeedbackStepTag());
			IXMLTag practiceTag = feedbackPractice.getPracticeTag();
			IERunGroupAccount feedbackReceiver = pvToolkit.getRunGroupAccount(practiceTag.getAttribute(CRunPVToolkit.practiceRgaId));
			boolean feedbackIsReady = feedbackIsReady(practiceTag);
			if (feedbackIsReady && feedbackReceiver != null) {
				
				String practiceTagUuid = practiceTag.getAttribute(CRunPVToolkit.practiceUuId);
	
				Row row = new Row();
				rows.appendChild(row);
				
    			int columnNumber = 0;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					new CRunPVToolkitDefImage(row, 
							new String[]{"class", "src"}, 
							new Object[]{"gridImage", zulfilepath + "ballot.svg"}
							);
				}
	
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					new CRunPVToolkitDefLabel(row, 
							new String[]{"class", "value"}, 
							new Object[]{"font gridLabel", CDesktopComponents.vView().getLabel("PV-toolkit-feedback.row.label.activity").replace("%1", feedbackReceiver.getERunGroup().getName())}
							);
				}
	
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					new CRunPVToolkitDefLabel(row, 
							new String[]{"class", "value"}, 
							new Object[]{"font gridLabel", pvToolkit.getPeerGroupName(feedbackReceiver.getERunGroup(), _actor)}
							);
				}
	
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					String cycleNumber = "";
					if (getFeedbackStepTag().getParentTag().getName().equals("cycle")) {
						cycleNumber = "" + pvToolkit.getCycleNumber(getFeedbackStepTag().getParentTag());
					}
					Label label = new CRunPVToolkitDefLabel(row, 
							new String[]{"class", "value"}, 
							new Object[]{"font gridLabel", cycleNumber}
							);
					while (cycleNumber.length() < 4) {
						cycleNumber = "0" + cycleNumber;
					}
					label.setAttribute("orderValue", cycleNumber);
				}
	
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					String deadline = "";
					String deadlineDate = pvToolkit.getDeadlineDateStr(_actor, getFeedbackStepTag());
					String deadlineTime =  pvToolkit.getDeadlineTimeStr(_actor, getFeedbackStepTag());
					if (!deadlineDate.equals("")) {
						deadline += deadlineDate;
						if (!deadlineTime.equals("")) {
							deadline += " " + deadlineTime;
						}
					}
					
					String deadlineYMD = "";
					if (!StringUtils.isEmpty(deadline)) {
						Date date = CDefHelper.getDateFromStrDMY(deadline);
						deadlineYMD = CDefHelper.getDateStrAsYMD(date);
					}

					CRunPVToolkitDefLabel label = new CRunPVToolkitDefLabel(row, 
							new String[]{"class", "value"}, 
							new Object[]{"font gridLabel", deadline}
							);
					//NOTE for ordering use ymd format
					if (!StringUtils.isEmpty(deadlineYMD)) {
						label.setAttribute("orderValue", deadlineYMD);
					}
				}
				
				columnNumber++;
				if (showColumn.length > columnNumber && showColumn[columnNumber]) {
					Label linkLabel = new CRunPVToolkitDefLabel(row, 
							new String[]{"id", "class"}, 
							new Object[]{getId() + "GridRowFeedbackLabel_" + practiceTagUuid, "font gridLink"}
							);
					setLinkLabelVariableProperties(linkLabel, practiceTag);
					toFeedbackOnClickEventListener(linkLabel, _actor, getFeedbackStepTag(), practiceTag);
				}
	
				rowNumber ++;
	
			}
		}

		return rowNumber;
	}

	@Override
	protected IXMLTag getFeedbackStepTag() {
		return feedbackStepTag;
	}

	@Override
	protected IXMLTag getStepTag() {
		return getFeedbackStepTag();
	}

}
