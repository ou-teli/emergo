/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.feedback;

import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Label;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitSubSubStepDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitNotification;
import nl.surf.emergo.control.run.cases.PVtoolkit.practice.CRunPVToolkitPracticeRecordingsDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.prepare.CRunPVToolkitPrepareRecordingsDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.util.CRunPVToolkitCustomAttributeHelper;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;

public class CRunPVToolkitFeedbackDiv extends CRunPVToolkitSubSubStepDiv {

	private static final long serialVersionUID = 5603590650189130172L;

	protected IXMLTag _currentStepTag;
	protected IXMLTag feedbackStepTag;
	
	protected IXMLTag _currentTag;
	protected boolean _currentTagIsStatusTag = true;
	protected boolean _unfolded;
	
	protected IECaseComponent caseComponent;
	protected IXMLTag skillTag;
	
	protected boolean _skillclusterHasScore;
	protected boolean _skillclusterHasTipsTops;
	protected boolean _subskillHasScore;
	protected boolean _subskillHasTipsTops;
	protected boolean _skillHasScore;
	protected boolean _skillHasTipsTops;

	protected boolean _peerFeedback = true;
	protected boolean _showOverview = false;
	protected boolean _showPreviousOverview = false;
	protected boolean _showRubric = false;
	
	protected boolean _giveFeedback = true;
	protected boolean _sendFeedback = false;
	protected boolean _analyzeVideo = false;
	protected boolean _compareWithExperts = false;
	protected boolean _showMyFeedback = false;
	
	public String _stepPrefix = "feedback";
	public String _idPrefix = "feedback";
	public String _backToRecordingsId = "";

	public String _classPrefix = "feedback";
	
	public void onCreate(CreateEvent aEvent) {
		_currentTagIsStatusTag = CRunPVToolkitCustomAttributeHelper.handleCustomAttribute(this, "_currentTagIsStatusTag", _currentTagIsStatusTag);
	
		_stepPrefix = CRunPVToolkitCustomAttributeHelper.handleCustomAttribute(this, "_stepPrefix", _stepPrefix);
		_idPrefix = CRunPVToolkitCustomAttributeHelper.handleCustomAttribute(this, "_idPrefix", _idPrefix);
		_backToRecordingsId = CRunPVToolkitCustomAttributeHelper.handleCustomAttribute(this, "_backToRecordingsId", _backToRecordingsId);
	}
	
	protected void setFeedbackStepTag(IXMLTag feedbackStepTag) {
		this.feedbackStepTag = feedbackStepTag;
	}

	public boolean skillclusterHasScore() {
		return _skillclusterHasScore;
	}
	
	public boolean skillclusterHasTipsTops() {
		return _skillclusterHasTipsTops;
	}
	
	public boolean subskillHasScore() {
		return _subskillHasScore;
	}
	
	public boolean subskillHasTipsTops() {
		return _subskillHasTipsTops;
	}
	
	public boolean skillHasScore() {
		return _skillHasScore;
	}
	
	public boolean skillHasTipsTops() {
		return _skillHasTipsTops;
	}
	
	public boolean inOverview() {
		return _showOverview || _showPreviousOverview;
	}
	
	public boolean inRubric() {
		return _showRubric;
	}
	
	protected IXMLTag getFeedbackStepTag() {
		//NOTE when giving feedback determine feedback step from current step
		if (_currentStepTag != null) {
			String currentStepType = _currentStepTag.getChildValue("steptype");
			if (currentStepType.equals(CRunPVToolkit.prepareStepType)) {
				//NOTE always use progress of first feedback step, because progress of prepare steps is shared over cycles
				return pvToolkit.getFirstStepTag(CRunPVToolkit.feedbackStepType);
			}
			else if (currentStepType.equals(CRunPVToolkit.practiceStepType)) {
				//NOTE if current step is practice step look forward
				return pvToolkit.getNextStepTagInCurrentCycle(_currentStepTag, CRunPVToolkit.feedbackStepType);
			}
			else if (currentStepType.equals(CRunPVToolkit.feedbackStepType)) {
				//NOTE if current step is prepare or practice step look forward
				return _currentStepTag;
			}
			else {
				//NOTE if current step is define goals step look backward
				return pvToolkit.getPreviousStepTagInCurrentCycle(_currentStepTag, CRunPVToolkit.feedbackStepType);
			}
		}
		return null;
	}
	
	public int getNumberOfSubSkills() {
		return pvToolkit.getNumberOfSubSkills(caseComponent, skillTag);
	}

	public int getNumberOfAccessibleSubSkills() {
		return pvToolkit.getNumberOfAccessibleSubSkills(caseComponent, skillTag);
	}

	public int getNumberOfGivenSubSkillScores(IXMLTag currentTag) {
		return pvToolkit.getNumberOfGivenSubSkillScores(_actor, caseComponent, getFeedbackStepTag(), currentTag, _currentTagIsStatusTag, skillTag);
	}

	public int getNumberOfGivenSubSkillScores() {
		return getNumberOfGivenSubSkillScores(_currentTag);
	}

	public Integer getSkillLevel(IXMLTag skillTag, IXMLTag currentTag) {
		return pvToolkit.getSkillLevel(_actor, caseComponent, getFeedbackStepTag(), currentTag, _currentTagIsStatusTag, skillTag);
	}

	public Integer getSkillLevel(IXMLTag skillTag) {
		return getSkillLevel(skillTag, _currentTag);
	}

	public Integer getSkillClusterLevel(IXMLTag skillclusterTag, IXMLTag currentTag) {
		return pvToolkit.getSkillClusterLevel(_actor, caseComponent, getFeedbackStepTag(), currentTag, _currentTagIsStatusTag, skillclusterTag);
	}

	public Integer getSkillClusterLevel(IXMLTag skillclusterTag) {
		return getSkillClusterLevel(skillclusterTag, _currentTag);
	}

	public Integer getSubSkillLevel(IXMLTag subskillTag, IXMLTag currentTag) {
		return pvToolkit.getSubSkillLevel(_actor, caseComponent, getFeedbackStepTag(), currentTag, _currentTagIsStatusTag, subskillTag);
	}

	public Integer getSubSkillLevel(IXMLTag subskillTag) {
		return getSubSkillLevel(subskillTag, _currentTag);
	}

	public List<Integer> getSubSkillLevelsPerSkillCluster(IXMLTag skillclusterTag, IXMLTag currentTag) {
		return pvToolkit.getSubSkillLevelsPerSkillCluster(_actor, caseComponent, getFeedbackStepTag(), currentTag, _currentTagIsStatusTag, skillclusterTag);
	}

	public List<Integer> getSubSkillLevelsPerSkillCluster(IXMLTag skillclusterTag) {
		return getSubSkillLevelsPerSkillCluster(skillclusterTag, _currentTag);
	}

	public int getLevel(IXMLTag skillTag, IXMLTag currentTag) {
		return pvToolkit.getFeedbackLevel(_actor, getFeedbackStepTag(), currentTag, _currentTagIsStatusTag, skillTag);
	}
	
	public int getLevel(IXMLTag skillTag) {
		return getLevel(skillTag, _currentTag);
	}
	
	public void saveLevel(IXMLTag skillTag, int level, IXMLTag currentTag) {
		pvToolkit.saveFeedbackLevel(_actor, getFeedbackStepTag(), currentTag, _currentTagIsStatusTag, skillTag, level);
	}
	
	public void saveLevel(IXMLTag skillTag, int level) {
		saveLevel(skillTag, level, _currentTag);
	}
	
	public String getTips(IXMLTag skillTag, IXMLTag currentTag) {
		return pvToolkit.getFeedbackTips(_actor, getFeedbackStepTag(), currentTag, _currentTagIsStatusTag, skillTag);
	}

	public String getTips(IXMLTag skillTag) {
		return getTips(skillTag, _currentTag);
	}

	public String getTops(IXMLTag skillTag, IXMLTag currentTag) {
		return pvToolkit.getFeedbackTops(_actor, getFeedbackStepTag(), currentTag, _currentTagIsStatusTag, skillTag);
	}

	public String getTops(IXMLTag skillTag) {
		return getTops(skillTag, _currentTag);
	}

	public void saveTips(IXMLTag skillTag, String tips, IXMLTag currentTag) {
		pvToolkit.saveFeedbackTips(_actor, getFeedbackStepTag(), currentTag, _currentTagIsStatusTag, skillTag, tips);
	}

	public void saveTips(IXMLTag skillTag, String tips) {
		saveTips(skillTag, tips, _currentTag);
	}

	public void saveTops(IXMLTag skillTag, String tops, IXMLTag currentTag) {
		pvToolkit.saveFeedbackTops(_actor, getFeedbackStepTag(), currentTag, _currentTagIsStatusTag, skillTag, tops);
	}

	public void saveTops(IXMLTag skillTag, String tops) {
		saveTops(skillTag, tops, _currentTag);
	}

	public boolean feedbackIsEditable(IXMLTag currentTag) {
		return _editable && !feedbackIsShared(currentTag);
	}
	
	public boolean feedbackIsEditable() {
		return feedbackIsEditable(_currentTag);
	}
	
	public String getFeedbackStatus(IXMLTag currentTag) {
		if (feedbackIsShared(currentTag)) {
			return "shared";
		}
		else if (feedbackIsInConsultation(currentTag)) {
			return "inconsultation";
		}
		else if (feedbackIsReady(currentTag)) {
			return "ready";
		}
		else if (feedbackIsBusy(currentTag)) {
			return "busy";
		}
		else if (feedbackIsNew(currentTag)) {
			return "new";
		}
		return "";
	}
	
	public boolean feedbackIsNew(IXMLTag currentTag) {
		return getNumberOfGivenSubSkillScores(currentTag) == 0;
	}
	
	public boolean feedbackIsNew() {
		return getNumberOfGivenSubSkillScores() == 0;
	}
	
	public boolean feedbackIsBusy(IXMLTag currentTag) {
		int number = getNumberOfGivenSubSkillScores(currentTag);
		//NOTE increase max number by 1 because skill itself is scored as well
		int maxNumber = getNumberOfAccessibleSubSkills() + 1;
		return number > 0 && number <= maxNumber;
	}
	
	public boolean feedbackIsBusy() {
		return getNumberOfGivenSubSkillScores() < getNumberOfAccessibleSubSkills();
	}
	
	public boolean feedbackIsReady(IXMLTag currentTag) {
		return pvToolkit.getFeedbackStatus(_actor, getFeedbackStepTag(), currentTag, _currentTagIsStatusTag, "ready").equals(AppConstants.statusValueTrue);
	}
	
	public boolean feedbackIsReady() {
		return feedbackIsReady(_currentTag);
	}
	
	public boolean feedbackIsInConsultation(IXMLTag currentTag) {
		return !feedbackIsShared(currentTag) && pvToolkit.getFeedbackStatus(_actor, getFeedbackStepTag(), currentTag, _currentTagIsStatusTag, "inconsultation").equals(AppConstants.statusValueTrue);
	}
	
	public boolean feedbackIsInConsultation() {
		return !feedbackIsShared(_currentTag) && pvToolkit.getFeedbackStatus(_actor, getFeedbackStepTag(), _currentTag, _currentTagIsStatusTag, "inconsultation").equals(AppConstants.statusValueTrue);
	}
	
	public boolean feedbackIsShared(IXMLTag currentTag) {
		return pvToolkit.getFeedbackStatus(_actor, getFeedbackStepTag(), currentTag, _currentTagIsStatusTag, "share").equals(AppConstants.statusValueTrue);
	}
	
	public boolean feedbackIsShared() {
		return feedbackIsShared(_currentTag);
	}
	
	public boolean allOtherFeedbacksAreShared(IXMLTag currentTag) {
		//NOTE this method is called if a feedback giver (= _actor) has given feedback, so only check if other feedback givers of feedback receiver have shared their feedback 
		//get rga id of feedback giver. It should not be checked.
		List<IERunGroupAccount> runGroupAccounts = pvToolkit.getRunGroupAccounts(_actor);
		if (runGroupAccounts.isEmpty()) {
			return false;
		}
		String feedbackGiverRgaId = "" + runGroupAccounts.get(0).getRgaId();
		
		//get feedback receiver. Peer group members of receiver should be checked.
		IERunGroupAccount feedbackReceiver = pvToolkit.getRunGroupAccount(currentTag.getAttribute(CRunPVToolkit.practiceRgaId));
		if (feedbackReceiver == null) {
			return false;
		}
		IERunGroup feedbackReceiverRG = feedbackReceiver.getERunGroup();
		
		//get all other peer group members of feedback receiver
		List<IXMLTag> peerGroupMemberTags = pvToolkit.getOtherPeerGroupMemberTags(feedbackReceiverRG);
		for (IXMLTag peerGroupMemberTag : peerGroupMemberTags) {
			String peerGroupMemberRgaId = pvToolkit.getStatusChildTagAttribute(peerGroupMemberTag, "rgaid");
			if (!peerGroupMemberRgaId.equals(feedbackGiverRgaId)) {
				//don't include feedback giver
				String role = pvToolkit.getStatusChildTagAttribute(peerGroupMemberTag, "role");
				if ((!role.equals(CRunPVToolkit.peerGroupStudentAssistantRole) || !pvToolkit.excludeStudentAssistantsFeedback(feedbackReceiverRG)) &&
						(!role.equals(CRunPVToolkit.peerGroupTeacherRole) || !pvToolkit.excludeTeachersFeedback(feedbackReceiverRG, getFeedbackStepTag()))) {
					//only include student assistant and teacher if needed
					IERunGroupAccount feedbackGiver = pvToolkit.getRunGroupAccount(peerGroupMemberRgaId);
					if (feedbackGiver != null) {
						boolean excludeFeedbackGiver = pvToolkit.excludeFeedbackGiver(feedbackGiver.getERunGroup(), feedbackReceiverRG);
						if (!excludeFeedbackGiver && !pvToolkit.getFeedbackStatus(feedbackGiver.getERunGroup(), getFeedbackStepTag(), currentTag, _currentTagIsStatusTag, "share").equals(AppConstants.statusValueTrue)) {
							return false;
						}
					}
				}
			}
		}
		return true;
	}
	
	public boolean allOtherFeedbacksAreShared() {
		return allOtherFeedbacksAreShared(_currentTag);
	}
	
	public void setFeedbackReady(IXMLTag currentTag) {
		pvToolkit.setFeedbackReady(_actor, getFeedbackStepTag(), currentTag, _currentTagIsStatusTag);
	}
	
	public void setFeedbackReady() {
		setFeedbackReady(_currentTag);
	}
	
	public void setFeedbackInConsultation(IXMLTag currentTag, boolean value) {
		pvToolkit.setFeedbackInConsultation(_actor, getFeedbackStepTag(), currentTag, value);
	}
	
	public void setFeedbackInConsultation(boolean value) {
		setFeedbackInConsultation(_currentTag, value);
	}
	
	public void setFeedbackShared(IXMLTag currentTag) {
		pvToolkit.setFeedbackShared(_actor, getFeedbackStepTag(), currentTag);

		/* START EXPERIMENTAL CODE */
		//NOTE send notification to feedback receiver except for student assistant role
		if (pvToolkit.hasStudentAssistantRole) {
			//do nothing
			return;
		}
		//get feedback receiver from practice tag
		IERunGroupAccount feedbackReceiver = pvToolkit.getRunGroupAccount(currentTag.getAttribute(CRunPVToolkit.practiceRgaId));
		if (feedbackReceiver == null) {
			//do nothing
			return;
		}
		boolean excludeFeedbackGiver = pvToolkit.excludeFeedbackGiver(pvToolkit.getCurrentRunGroup(), feedbackReceiver.getERunGroup());
		if (excludeFeedbackGiver) {
			//do nothing
			return;
		}
		/* END EXPERIMENTAL CODE */

		pvToolkit.mailNotification(CRunPVToolkit.onFeedbackShared, pvToolkit.getCurrentStepTag(), currentTag);
		pvToolkit.publishNotification(new Event(CRunPVToolkit.onFeedbackShared, null, new CRunPVToolkitNotification(_actor, _currentStepTag, null)));
		
		if (allOtherFeedbacksAreShared(currentTag)) {
			pvToolkit.mailNotification(CRunPVToolkit.onAllFeedbackShared, pvToolkit.getCurrentStepTag(), currentTag);
			pvToolkit.publishNotification(new Event(CRunPVToolkit.onAllFeedbackShared, null, new CRunPVToolkitNotification(_actor, _currentStepTag, null)));
		}
	}
	
	public void setFeedbackShared() {
		setFeedbackShared(_currentTag);
	}

	
	public void setPracticeSelfFeedback() {
		pvToolkit.setPracticeSelfFeedback(_actor, _currentTag);
	}
	
	public boolean practiceIsShared(IXMLTag currentTag) {
		return pvToolkit.getPracticeStatus(_actor, currentTag, "share").equals(AppConstants.statusValueTrue);
	}
	
	public boolean practiceIsShared() {
		return practiceIsShared(_currentTag);
	}
	

	public void toFeedbackRecordings(String fromComponentId, String toComponentIdPrefix, IXMLTag tag) {
		Component fromComponent = CDesktopComponents.vView().getComponent(fromComponentId);
		if (fromComponent != null) {
			fromComponent.setVisible(false);
		}
		Component toComponent = CDesktopComponents.vView().getComponent(toComponentIdPrefix + "RecordingsDiv");
		if (toComponent != null) {
			if (!_showMyFeedback) {
				if (toComponent instanceof CRunPVToolkitPrepareRecordingsDiv) {
					((CRunPVToolkitPrepareRecordingsDiv)toComponent).update();
				}
				if (toComponent instanceof CRunPVToolkitFeedbackRecordingsDiv) {
					((CRunPVToolkitFeedbackRecordingsDiv)toComponent).update(tag);
				}
				else if (toComponent instanceof CRunPVToolkitPracticeRecordingsDiv) {
					((CRunPVToolkitPracticeRecordingsDiv)toComponent).update();
				}
			}
			toComponent.setVisible(true);
		}
	}

	public void toFeedback(String fromComponentId, String toComponentIdPrefix, IXMLTag originalPracticeStepTag, IXMLTag originalCurrentTag) {
		Component fromComponent = CDesktopComponents.vView().getComponent(fromComponentId);
		if (fromComponent != null) {
			fromComponent.setVisible(false);
		}
		CRunPVToolkitFeedbackFeedbackDiv toComponent = (CRunPVToolkitFeedbackFeedbackDiv)CDesktopComponents.vView().getComponent(toComponentIdPrefix + "FeedbackDiv");
		if (toComponent != null) {
			//NOTE used original practice tags for giving feedback
					
			toComponent.init(_actor, originalPracticeStepTag, originalCurrentTag, true);
			toComponent.setVisible(true);
		}
	}
	
	public void toFeedbackOverviewRubric(String fromComponentId, String toComponentIdPrefix, boolean editable) {
		Component fromComponent = CDesktopComponents.vView().getComponent(fromComponentId);
		if (fromComponent != null) {
			fromComponent.setVisible(false);
		}
		CRunPVToolkitFeedbackOverviewRubricDiv toComponent = (CRunPVToolkitFeedbackOverviewRubricDiv)CDesktopComponents.vView().getComponent(toComponentIdPrefix + "OverviewRubricDiv");
		if (toComponent != null) {
			toComponent.init(_actor, _currentStepTag, _currentTag, editable, true);
			toComponent.setVisible(true);
		}
	}

	public Label addFileLink(IXMLTag currentTag, Component parent, String id, String classStr, String value) {
		//TODO there may be multiple files
		IXMLTag pieceTag = pvToolkit.getStatusChildTag(currentTag).getChild("piece");
		return pvToolkit.showStatusLink(pieceTag, parent, id, classStr, value);
	}
				
}
