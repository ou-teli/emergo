/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.ForwardEvent;

import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefGenericForwardComposer;
import nl.surf.emergo.view.VView;

public class CRunForwardComposer extends CDefGenericForwardComposer{

	private static final long serialVersionUID = 4338821107294855240L;

	protected VView vView = new VView();

	public void onExternalEvent$runWnd(Event event){
		onExternalEvent(event);
	}

	protected void onExternalEvent(Event event){
        ForwardEvent forwardEvent = (ForwardEvent)event;
        Object[] data = (Object[])forwardEvent.getOrigin().getData();
        String sender = (String)data[0];
        String action = "onExternalEvent";
        String[] value = new String[4];
        value[0] = (String)data[0];
        value[1] = (String)data[1];
        value[2] = (String)data[2];
        value[3] = (String)data[3];
		CRunWnd runWnd = (CRunWnd) vView.getComponent(CControl.runWnd);
		CDesktopComponents.sSpring().getSLogHelper().logAction("external event from " + sender + ": " + value[0] + ", " + value[1] + ", " +
				value[2] + ", " + value[3]);
		if (runWnd != null) {
			runWnd.onAction(sender, action, value);
		}
	}

}