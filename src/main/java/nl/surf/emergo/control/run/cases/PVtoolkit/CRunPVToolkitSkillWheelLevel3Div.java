/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Div;
import org.zkoss.zul.Rows;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.domain.IERunGroup;

public class CRunPVToolkitSkillWheelLevel3Div extends CRunPVToolkitSkillWheelLevel1Div {

	private static final long serialVersionUID = -6212904047229194463L;
	
	protected int initialCycleNumber;
	protected int initialSkillClusterNumber;
	
	@Override
	public void init(IERunGroup actor, boolean editable, int cycleNumber, int skillClusterNumber) {
		_skillWheelZoomFactor = 1.50;
		_presentationLevel = 3;
		_classPrefix = "skillWheelLevel3";
		initialSkillClusterNumber = skillClusterNumber;
		initialCycleNumber = cycleNumber;

		super.init(actor, editable, cycleNumber, skillClusterNumber);
	}
	
	public void init(IERunGroup actor, boolean editable, int cycleNumber, int skillClusterNumber, List<Integer> pOtherRgaIdsToFilterOn) {
		_skillWheelZoomFactor = 1.50;
		_presentationLevel = 3;
		_classPrefix = "skillWheelLevel3";
		otherRgaIdsToFilterOn = pOtherRgaIdsToFilterOn;
		initialOtherRgaIdsToFilterOn = new ArrayList<>(pOtherRgaIdsToFilterOn);
		initialSkillClusterNumber = skillClusterNumber;
		initialCycleNumber = cycleNumber;
		
		super.init(actor, editable, cycleNumber, skillClusterNumber);
	}
	
	@Override
	public void update(int cycleNumber) {
		super.update(cycleNumber);

		pvToolkit.setMemoryCaching(true);
		
		Component btn1 = new CRunPVToolkitDefButton(this, 
				new String[]{"class", "cLabelKey"}, 
				new Object[]{"font pvtoolkitButton " + _classPrefix + "BackButton", "PV-toolkit.back"}
				);
		addBackOnClickEventListener(btn1, _idPrefix + "Level3Div", _idPrefix + "Level2Div");

		if (_compareWithExperts || (_cycleNumber > 0 && _cycleNumber <= _sharedPracticeTagsPerFeedbackStep.size())) {
			if (!_compareWithExperts) {
				if (_mayFilter) { 
					String labelKey = "";
					if (!_compareFeedbackGivers) {
						labelKey = "PV-toolkit-skillwheel.button.filter";
					}
					else {
						labelKey = "PV-toolkit-skillwheel.button.groups";
					}
					Component btn = new CRunPVToolkitDefButton(this, 
							new String[]{"class", "labelKey"}, 
							new Object[]{"font pvtoolkitButton " + _classPrefix + "FilterButton", labelKey}
							);
					if (!_compareFeedbackGivers) {
						addShowFilterOnClickEventListener(btn, this);
					}
					else {
						addShowGroupsOnClickEventListener(btn, this);
					}
				}
			}

			if (!_compareFeedbackGivers) {
				Component btn = new CRunPVToolkitDefButton(this, 
						new String[]{"class", "labelKey"}, 
						new Object[]{"font pvtoolkitButton " + _classPrefix + "RecordingButton", "PV-toolkit-skillwheel.button.recording"}
						);
				addToRecordingOnClickEventListener(btn, this);
			
				btn = new CRunPVToolkitDefButton(this, 
						new String[]{"class", "labelKey"}, 
						new Object[]{"font pvtoolkitButton " + _classPrefix + "OverviewTipsTopsButton", "PV-toolkit-skillwheel.button.overviewtipstops"}
						);
				addToOverViewTipsTopsOnClickEventListener(btn, this);
			}
		}
		
		Div tipsTopsDiv = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "TipsTopsDiv"}
		);
		
		Rows rows = appendTipsTopsGrid(tipsTopsDiv);
		int rowNumber = 1;
		rowNumber = appendTipsOrTopsToGrid(rows, "tops", rowNumber);
		appendTipsOrTopsToGrid(rows, "tips", rowNumber);
		
		pvToolkit.setMemoryCaching(false);
		
	}

    protected Rows appendTipsTopsGrid(Component parent) {
    	String markLabelKey = "mark";
    	if (!_editable) {
    		markLabelKey = "marked";
    	}
    	return pvToolkit.appendGrid(
    			parent,
    			getId() + "Grid",
    			"position:absolute;left:0px;top:0px;width:530px;height:410px;overflow:auto;", 
    			new boolean[] {true, true, true, _showMarks},
    			new String[] {"6%", "54%", "20%", "20%"}, 
    			new boolean[] {false, true, true, true},
    			null,
    			"PV-toolkit-skillwheel.column.label.", 
    			new String[] {"", "tiptop", "peer", markLabelKey});
	}

	protected int appendTipsOrTopsToGrid(Rows rows, String tipsOrTops, int rowNumber) {
		return appendTipsOrTopsToGrid(
				rows, 
				tipsOrTops, 
				rowNumber,
				new boolean[] {true, true, true, false, false, false, false, false, _showMarks} 
				);
	}

	protected void addBackOnClickEventListener(Component component, String componentFromId, String componentToId) {
		// NOTE only used to go back to level 2
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				CRunPVToolkitSkillWheelLevel3Div componentFrom = (CRunPVToolkitSkillWheelLevel3Div)CDesktopComponents.vView().getComponent(componentFromId);
				IERunGroup lActor = null;
				boolean lEditable = false;
				int lCycleNumber = initialCycleNumber;
				int lSkillClusterNumberToFilter = 0;
				List<Integer> lOtherRgaIdsToFilterOn = initialOtherRgaIdsToFilterOn;
				if (componentFrom != null) {
					lActor = componentFrom._actor;
					lEditable = componentFrom._editable;
					lCycleNumber = componentFrom._cycleNumber;
					// NOTE: show all clusters in level 2:
					lSkillClusterNumberToFilter = 0;
					lOtherRgaIdsToFilterOn = componentFrom.otherRgaIdsToFilterOn;
					componentFrom.setVisible(false);
				}
				toComponent(componentToId, lActor, lEditable, lCycleNumber, lSkillClusterNumberToFilter, lOtherRgaIdsToFilterOn);
			}
		});
	}

	protected void toComponent(String componentToId, IERunGroup pActor, boolean pEditable, int pCycleNumber, int pSkillClusterNumberToFilter, List<Integer> pOtherRgaIdsToFilterOn) {
		Component componentTo = CDesktopComponents.vView().getComponent(componentToId);
		if (componentTo != null) {
			if (componentTo instanceof CRunPVToolkitSkillWheelLevel1Div) {
				boolean lRgaIdsChanged = (((CRunPVToolkitSkillWheelLevel2Div)componentTo).initialOtherRgaIdsToFilterOn != null) && !((CRunPVToolkitSkillWheelLevel2Div)componentTo).initialOtherRgaIdsToFilterOn.equals(pOtherRgaIdsToFilterOn);
				// NOTE: check for componentTo._cycleNumber, because initialCycleNumber can be reset by OverviewTipsTopsDiv:
				if (lRgaIdsChanged || (((CRunPVToolkitSkillWheelLevel2Div)componentTo)._cycleNumber != pCycleNumber)) {
					// NOTE: componentToInit.otherRgaIdsToFilterOn is already equal to componentFrom.otherRgaIdsToFilterOn if set in componentFrom, because both point to same ArrayList
					// NOTE: set componentTo._cycleNumber, because parameter in init method will be ignored for level 1 and level 2:
					((CRunPVToolkitSkillWheelLevel2Div)componentTo)._cycleNumber = pCycleNumber;
					if (lRgaIdsChanged) {
						((CRunPVToolkitSkillWheelLevel2Div)componentTo).init(pActor, pEditable, pCycleNumber, pSkillClusterNumberToFilter, pOtherRgaIdsToFilterOn);
					} else {
						((CRunPVToolkitSkillWheelLevel2Div)componentTo).init(pActor, pEditable, pCycleNumber, pSkillClusterNumberToFilter);
					}
				}
			}
			componentTo.setVisible(true);
		}
	}

}
