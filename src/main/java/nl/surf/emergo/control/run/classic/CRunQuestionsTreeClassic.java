/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import org.zkoss.zul.Treeitem;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunQuestionsTree.
 */
public class CRunQuestionsTreeClassic extends CRunTreeClassic {

	private static final long serialVersionUID = -2486752811566075124L;

	/** The conversation the questions belong to. */
	protected IXMLTag conversation = null;

	/**
	 * Instantiates a new c run questions tree.
	 * 
	 * @param aId the a id
	 * @param aRunComponent the a run component, the ZK mail component
	 */
	public CRunQuestionsTreeClassic(String aId, CRunComponentClassic aRunComponent) {
		super(aId, null, aRunComponent);
		tagopenednames = "question";
//		setRows(6);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTree#getRunComponentHelper()
	 */
	@Override
	public CRunComponentHelperClassic getRunComponentHelper() {
		return new CRunConversationsHelperClassic(this, tagopenednames, caseComponent, runComponent);
	}

	/**
	 * Shows questions.
	 * 
	 * @param aCaseComponent the a case component
	 * @param aConversation the a conversation
	 */
	public void showQuestions(IECaseComponent aCaseComponent,
			IXMLTag aConversation) {
		caseComponent = aCaseComponent;
		conversation = aConversation;
		update(conversation);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTree#setTreeitemStatus(org.zkoss.zul.Treeitem)
	 */
	@Override
	public Treeitem setTreeitemStatus(Treeitem aTreeitem) {
		Treeitem lTreeitem = aTreeitem;
		IXMLTag tag = getContentItemTag(lTreeitem);
		if (tag != null) {
			lTreeitem = setRunTagStatus(lTreeitem, tag,
					AppConstants.statusKeyOutfoldable,
					AppConstants.statusValueTrue, tagopenednames, true);
//			maybe add something for shared component
		}
		lTreeitem = super.setTreeitemStatus(lTreeitem);
		return lTreeitem;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTree#doTreeitemAction(org.zkoss.zul.Treeitem)
	 */
	@Override
	public void doTreeitemAction(Treeitem aTreeitem) {
		IXMLTag tag = getContentItemTag(aTreeitem);
		if (tag == null)
			return;
		String lName = tag.getName();
		if (!lName.equals("question"))
			return;
		CRunConversationsClassic lComp = (CRunConversationsClassic) CDesktopComponents.vView().getComponent("runConversations");
		if (lComp == null)
			return;
		for (IXMLTag childtag : tag.getChilds("fragment")) {
			lName = childtag.getName();
			if (lName.equals("fragment")) {
				boolean lPresent = (CDesktopComponents.sSpring().getCurrentTagStatus(childtag, AppConstants.statusKeyPresent).equals(AppConstants.statusValueTrue));
				boolean lOpened = (CDesktopComponents.sSpring().getCurrentTagStatus(childtag, AppConstants.statusKeyOpened).equals(AppConstants.statusValueTrue));
				if (lPresent && lOpened) {
					lComp.playFragment(conversation, childtag);
					return;
				}
			}
		}
		lComp.playFragment(conversation, null);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTree#reRenderTreeitem(org.zkoss.zul.Treeitem, nl.surf.emergo.business.IXMLTag, boolean)
	 */
	public Treeitem reRenderTreeitem(Treeitem aTreeitem, IXMLTag aTag, boolean aReRenderChilds) {
//		always rerender with childs
		return super.reRenderTreeitem(aTreeitem, aTag, true);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTree#update(nl.surf.emergo.business.IXMLTag)
	 */
	@Override
	public void update(IXMLTag aTag) {
		IXMLTag lTag = aTag;
		if (lTag == null)
			lTag = conversation;
		if (lTag.getAttribute(AppConstants.keyId) == conversation.getAttribute(AppConstants.keyId)) {
			super.update(lTag);
		} else {
//			super.update(lTag);
			CRunComponentHelperClassic cComponent = getRunComponentHelper();
			IXMLTag lRoot = cComponent.getXmlDataPlusStatusTree();
			cComponent.setItemAttributes(this, lRoot.getChild(AppConstants.contentElement));
			String lTagName = lTag.getName();
			if (lTagName.equals("fragment"))
				// rerender question (or complete conversation)
				lTag = lTag.getParentTag();
			lTag = CDesktopComponents.sSpring().xmlManager.getTagById(lRoot, lTag.getAttribute(AppConstants.keyId));
			if (lTag == null)
				// tag not found - possibly removed -> rerender complete conversation
				lTag = conversation;
			Treeitem lTreeItem = getContentItem (lTag.getAttribute(AppConstants.keyId));
			if (lTreeItem != null)
				lTreeItem = reRenderTreeitem (lTreeItem, lTag, true);
			else {
				// treeitem not found - possibly removed -> rerender complete conversation
				lTag = conversation;
				super.update(lTag);
			}
		}
	}

}
