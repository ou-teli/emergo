/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.List;

import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefLabel;

/**
 * The Class CRunLocationActionLabel. Used to show a clickable location action.
 */
public class CRunLocationActionLabel extends CDefLabel {

	private static final long serialVersionUID = 3281955125294549133L;

	/** The run wnd. */
	protected CRunWnd runWnd = (CRunWnd) CDesktopComponents.vView().getComponent(CControl.runWnd);

	/** The location action. */
	protected List<Object> locationAction = null;

	public String getClassName() {
		return CDesktopComponents.vView().getRunClassName(className);
	}

	/**
	 * Instantiates a new c run location action label.
	 *
	 * @param aLocationAction the a location action
	 */
	public CRunLocationActionLabel(List<Object> aLocationAction) {
		locationAction = aLocationAction;
		String lName = (String)locationAction.get(2);
		setValue(lName);
		setZclass(getClassName());
	}

	/**
	 * On click notify Emergo player of chosen location action.
	 */
	public void onClick() {
		if (!isDoubleClick()) {
			if (locationAction == null)
				return;
			if (runWnd != null) {
				getRoot().detach();
				runWnd.onAction(getId(), "showLocationAction", locationAction);
			}
		}
	}
}
