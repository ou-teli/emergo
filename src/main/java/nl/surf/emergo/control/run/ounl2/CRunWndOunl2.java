/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl2;

import java.util.Map;

import nl.surf.emergo.control.run.CRunAlert;
import nl.surf.emergo.control.run.CRunArea;
import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.control.run.CRunConversationInteraction;
import nl.surf.emergo.control.run.CRunHoverBtn;
import nl.surf.emergo.control.run.CRunMemo;
import nl.surf.emergo.control.run.CRunNotifications;
import nl.surf.emergo.control.run.CRunScores;
import nl.surf.emergo.control.run.ounl.CRunBreadcrumbsAreaOunl;
import nl.surf.emergo.control.run.ounl.CRunLocationAreaOunl;
import nl.surf.emergo.control.run.ounl.CRunWndOunl;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunWndOunl2. The Ounl Emergo player window.
 */
public class CRunWndOunl2 extends CRunWndOunl {

	private static final long serialVersionUID = 3207316446425103490L;

	/**
	 * Instantiates a new c run wnd.
	 * Reads google maps key out of .properties file and set it as session var,
	 * so google maps will work within the player.
	 */
	public CRunWndOunl2() {
		super();
	}

	@Override
	protected CRunHoverBtn createTabletBtn(Map<String,Object> aParams) {
		return new CRunTabletBtnOunl2(aParams);
	}

	@Override
	protected CRunHoverBtn createNoteBtn(Map<String,Object> aParams) {
		return new CRunNoteBtnOunl2(aParams);
	}

	@Override
	protected CRunHoverBtn createMemoBtn(Map<String,Object> aParams) {
		return new CRunMemoBtnOunl2(aParams);
	}

	@Override
	protected CRunHoverBtn createDashboardBtn(Map<String,Object> aParams) {
		return new CRunDashboardBtnOunl2(aParams);
	}

	@Override
	protected CRunComponent createConversationsComponent(IECaseComponent aCaseComponent) {
		return new CRunConversationsOunl2("runConversations", aCaseComponent);
	}
	
	@Override
	protected CRunConversationInteraction createConversationInteraction(CRunComponent aRunComponent) {
		return new CRunConversationInteractionOunl2("runConversationInteraction", aRunComponent, "runConversations");
	}
	
	@Override
	protected CRunComponent createTabletComponent(IECaseComponent aCaseComponent) {
		return new CRunTabletOunl2("runTablet", aCaseComponent);
	}
	
	@Override
	protected CRunComponent createDashboardComponent(IECaseComponent aCaseComponent) {
		return new CRunDashboardOunl2("runDashboard", aCaseComponent);
	}
	
	@Override
	protected CRunComponent createReferencesComponent(IECaseComponent aCaseComponent) {
		return new CRunReferencesOunl2("runReferences", aCaseComponent);
	}
	
	@Override
	protected CRunComponent createMailComponent(IECaseComponent aCaseComponent) {
		return new CRunMailOunl2("runMail", aCaseComponent);
	}
	
	@Override
	protected CRunComponent createAssessmentsComponent(IECaseComponent aCaseComponent, boolean aOnTablet) {
		return new CRunAssessmentsOunl2("runAssessments", aCaseComponent, aOnTablet);
	}
	
	@Override
	protected CRunComponent createLogbookComponent(IECaseComponent aCaseComponent) {
		return new CRunLogbookOunl2("runLogbook", aCaseComponent);
	}
	
	@Override
	protected CRunComponent createGooglemapsComponent(IECaseComponent aCaseComponent) {
		return new CRunGooglemapsOunl2("runGooglemaps", aCaseComponent);
	}
	
	@Override
	protected CRunComponent createTasksComponent(IECaseComponent aCaseComponent) {
		return new CRunTasksOunl2("runTasks", aCaseComponent);
	}
	
	@Override
	protected CRunComponent createDirectingComponent(IECaseComponent aCaseComponent) {
		return new CRunDirectingOunl2("runDirecting", aCaseComponent);
	}
	
	@Override
	protected CRunComponent createMemosComponent(IECaseComponent aCaseComponent) {
		return new CRunMemosOunl2("runMemos", aCaseComponent);
	}
	
	@Override
	protected CRunComponent createVideomanualComponent(IECaseComponent aCaseComponent) {
		return new CRunVideomanualOunl2("runVideomanual", aCaseComponent);
	}
	
	@Override
	protected CRunComponent createTutorialComponent(IECaseComponent aCaseComponent, String aChapterId) {
		return new CRunTutorialOunl2("runTutorial", aCaseComponent, aChapterId);
	}
	
	@Override
	protected CRunComponent createProfileComponent(IECaseComponent aCaseComponent) {
		return new CRunProfileOunl2("runProfile", aCaseComponent);
	}
	
	@Override
	protected CRunScores createScoresComponent(IECaseComponent aCaseComponent) {
		return new CRunScoresOunl2("runScores", aCaseComponent);
	}
	
	@Override
	protected CRunNotifications createNotificationsComponent(IECaseComponent aCaseComponent) {
		//NOTE There can be multiple notifications components so add case component id within notifications id.
		return new CRunNotificationsOunl2("runNotifications_" + aCaseComponent.getCacId(), aCaseComponent);
	}
	
	@Override
	protected CRunArea createNoteComponent(CRunComponent aRunComponent) {
		return new CRunNoteOunl2("runNote", aRunComponent);
	}
	
	@Override
	protected CRunMemo createMemoComponent() {
		return new CRunMemoOunl2("runMemo");
	}
	
	@Override
	protected CRunAlert createAlertComponent(CRunComponent aRunComponent) {
		return new CRunAlertOunl2("runAlert", aRunComponent);
	}
	
	@Override
	protected CRunComponent createIspotComponent(IECaseComponent aCaseComponent) {
		return new CRunIspotOunl2("runIspot", aCaseComponent);
	}
	
	@Override
	protected CRunComponent createEditformsComponent(IECaseComponent aCaseComponent) {
		return new CRunEditformsOunl2("runEditforms", aCaseComponent);
	}
	
	@Override
	protected CRunComponent createDragdropformsComponent(IECaseComponent aCaseComponent) {
		return new CRunDragdropformsOunl2("runDragdropforms", aCaseComponent);
	}
	
	@Override
	protected CRunComponent createSelectionformsComponent(IECaseComponent aCaseComponent) {
		return new CRunSelectionformsOunl2("runSelectionforms", aCaseComponent);
	}
	
	@Override
	protected CRunComponent createVideosceneselectorComponent(IECaseComponent aCaseComponent) {
		return new CRunVideosceneselectorOunl2("runVideosceneselector", aCaseComponent);
	}
	
	@Override
	protected CRunComponent createTestsComponent(IECaseComponent aCaseComponent) {
		return new CRunTestsOunl2("runTests", aCaseComponent);
	}
	
	@Override
	protected CRunComponent createGraphicalformsComponent(IECaseComponent aCaseComponent) {
		return new CRunGraphicalformsOunl2("runGraphicalforms", aCaseComponent);
	}
	
	@Override
	protected CRunLocationAreaOunl createLocationArea() {
		return new CRunLocationAreaOunl2("runLocationArea");
	}
	
	@Override
	protected CRunBreadcrumbsAreaOunl createBreadcrumbsArea() {
		return new CRunBreadcrumbsAreaOunl2("runBreadcrumbsArea");
	}
	
	@Override
	protected CRunArea createCloseArea() {
		return new CRunCloseAreaOunl2("runCloseArea");
	}
	
}
