/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.JuniorScientist;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.zkoss.zhtml.Span;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Div;
import org.zkoss.zul.Html;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.zkspring.STriggeredReference;

/**
 * This class is used to show questions with two options during phase 3, first feedback to Sven. There are 6 questions shown within an HTML text.
 * An editforms component is used for storage of questions, possible answers and answer given. And to store prefix and postfix texts for questions as well.
 * It also stores tweets to be shown if answers are incorrect. There is a tweet for each question.
 * 
 * Short cuts to accompanying references may be shown in the bottom left corner. The references may contain hints if a student has asked for specific hints.
 * The references are HTML files containing all hints. Hints not asked are filtered out within this class. See method getPieceData.
 * If hints are asked is stored in states.
 * 
 * Note that this macro component uses ZK elements to present questions and answers. During phase 4 HTML and javascript is used to present questions and answers.
 */
public class CRunJuniorScientistFeedbackSvenBox1 extends CRunJuniorScientistBox {

	private static final long serialVersionUID = -7177851459436058115L;

	protected final static String referencesCaseComponentName = "game_references";
	protected IECaseComponent referencesCaseComponent;
	
	protected String feedbackCaseComponentName = "";
	protected IECaseComponent feedbackAppCaseComponent;
	
	protected List<IXMLTag> selectorTags;
	/** Prefixes of pids entered by a case developer. */
	protected static final String prefixTextPidPrefix = "prefix_text_";
	protected static final String selectorPidPrefix = "selector_";
	protected static final String postfixTextPidPrefix = "postfix_text_";
	protected static final String tweetPidPrefix = "tweet_";

	/** Prefix of pids of states entered by a case developer. */
	protected String feedbackPieceHintsPrefix = "";
	protected String feedbackHintsAsked = "";
	protected String feedbackHintsToAsk = "";
	protected String feedbackCurrentQuestion = "";
	
	protected String feedbackTweetsAreShownStateKeyPid = ""; 
	protected String feedbackReadyStateKeyPid = ""; 
	protected boolean feedbackReadyState = false; 
	
	protected String piecePopupMacroId;
	protected String piecePopupId;

	protected String tweetsPopupId;
	
	/** To keep the number of answered selectors. */
	int numberOfGivenAnswers = 0;
	/** To keep the number of correctly answered selectors. */
	int numberOfCorrectAnswers = 0;
	
	@Override
	public void onInit() {
		super.onInitMacro();
		
		int version = getVersion();
		feedbackCaseComponentName = "phase_3_feedback_sven_" + version;
		feedbackPieceHintsPrefix = "game_3_feedback_sven_" + version + "_piece_hints_";
		feedbackHintsAsked = "game_3_feedback_sven_" + version + "_hints_asked";
		feedbackHintsToAsk = "game_3_feedback_sven_" + version + "_hints_to_ask";
		feedbackCurrentQuestion = "game_3_feedback_sven_" + version + "_current_questionnr";
		feedbackTweetsAreShownStateKeyPid = "game_3_feedback_sven_" + version + "_tweets_are_shown";
		feedbackReadyStateKeyPid = "game_3_feedback_sven_" + version + "_ready";

		feedbackReadyState = getStateTagValue(feedbackReadyStateKeyPid).equals(AppConstants.statusValueTrue);

		feedbackAppCaseComponent = getFeedbackAppCaseComponent();

		piecePopupMacroId = getUuid() + "_piece_popup_macro";
		piecePopupId = getUuid() + "_piece_popup";

		tweetsPopupId = getUuid() + "_tweets_shown";
		
		referencesCaseComponent = getReferencesComponent();
		//NOTE super ancestor CRunComponent needs case component to be set
		setCaseComponent(referencesCaseComponent);

		//determine references properties to be used within the child macro
		setCommissionProperties();
		
		//add the child macro
		addChildMacro("JuniorScientist_feedback_sven_view_macro.zul");
		
		boolean showTweets = !feedbackReadyState && getStateTagValue(feedbackTweetsAreShownStateKeyPid).equals(AppConstants.statusValueTrue);
		if (showTweets) {
			//NOTE if there are tweets show tweets advice notification
			//showNotification(getNotificationsComponentName(), "tweets_advice");
		}
	}
	
	protected int getVersion() {
		return 1;
	}
	
	protected int getNumberOfSelectors() {
		return getHintChars().length;
	}
	
	protected String[] getHintChars() {
		//if a hint is asked the corresponding character is shown in the interface
		return getStateTagValue(feedbackHintsToAsk).split(AppConstants.statusCommaReplace);
	}
	
	protected boolean useScrollIntoView() {
		//if an answer is given scroll down to see the next question
		return true;
	}
	
	@Override
	public void onUpdate() {
		//rerender child macro with adjusted properties
		childMacro.recreate();
	}

	protected void setCommissionProperties() {
		// General
		
		propertyMap.put("version", getVersion());

		propertyMap.put("sender", getStateTagValue(employeeCardNameStateKey));

		boolean[] hintsAreAsked = new boolean[getNumberOfSelectors()];
		for (int i=0;i<getNumberOfSelectors();i++) {
			hintsAreAsked[i] = getHintIsAsked(i + 1);
		}

		// Selectors
		
		numberOfGivenAnswers = 0;
		numberOfCorrectAnswers = 0;

		selectorTags = new ArrayList<IXMLTag>();
		
		List<Map<String,Object>> selectors = new ArrayList<Map<String,Object>>();
		propertyMap.put("selectors", selectors);
		List<String> tweets = new ArrayList<String>();
		propertyMap.put("tweets", tweets);
		propertyMap.put("tweetsPopupId", tweetsPopupId);
		
		boolean showTweets = !feedbackReadyState && getStateTagValue(feedbackTweetsAreShownStateKeyPid).equals(AppConstants.statusValueTrue);
		propertyMap.put("showTweets", showTweets);
		propertyMap.put("readOnly", feedbackReadyState);

		//put all node tags in a map per pid to be able to get them by number later on
		Map<String,IXMLTag> nodeTagsByPid = getNodeTagsByPid();
		int counter = 1;
		boolean render = true;
		while (nodeTagsByPid.containsKey(selectorPidPrefix + counter)) {
			//content to be rendered is entered in two content elements per selector
			IXMLTag selectorTag = nodeTagsByPid.get(selectorPidPrefix + counter);
			IXMLTag prefixTextTag = null;
			if (nodeTagsByPid.containsKey(prefixTextPidPrefix + counter)) {
				prefixTextTag = nodeTagsByPid.get(prefixTextPidPrefix + counter);
			}
			IXMLTag postfixTextTag = null;
			if (nodeTagsByPid.containsKey(postfixTextPidPrefix + counter)) {
				postfixTextTag = nodeTagsByPid.get(postfixTextPidPrefix + counter);
			}
			IXMLTag tweetTag = nodeTagsByPid.get(tweetPidPrefix + counter);
			
			selectorTags.add(selectorTag);
			
			Map<String,Object> hData = new HashMap<String,Object>();
			hData.put("selectorTag", selectorTag);
			hData.put("counter", counter);
			hData.put("render", render);
			String prefixText = "";
			if (prefixTextTag != null) {
				prefixText = sSpring.unescapeXML(prefixTextTag.getChildValue("defaulttext"));
			}
			hData.put("prefixText", prefixText);
			String postfixText = "";
			if (postfixTextTag != null) {
				postfixText = sSpring.unescapeXML(postfixTextTag.getChildValue("defaulttext"));
			}
			hData.put("postfixText", postfixText);
			String tweet = "";
			if (tweetTag != null) {
				tweet = sSpring.unescapeXML(tweetTag.getChildValue("defaulttext"));
			}
			hData.put("tweet", tweet);

			hData.put("correctAnswer", selectorTag.getChildValue("correctoptions"));

			//get default or given answer
			String answer = getAnswer(selectorTag);
			hData.put("answer", answer);

			boolean isAnswerEmpty = answer.equals("");
			if (!isAnswerEmpty) {
				numberOfGivenAnswers++;
			}
			else {
				render = false;
			}
			//is answer given correct
			boolean isAnswerCorrect = isAnswerCorrect(selectorTag, answer);
			if (isAnswerCorrect) {
				numberOfCorrectAnswers++;
			}
			hData.put("showTweetIcon", showTweets && !isAnswerCorrect);
			if (showTweets && !isAnswerCorrect) {
				tweets.add(tweet);
			}

			hData.put("disabled", isAnswerCorrect);
			hData.put("hintIsAsked", hintsAreAsked[counter - 1]);
			hData.put("hintChar", getHintChars()[counter - 1]);
			
			List<Map<String,Object>> selectorOptions = new ArrayList<Map<String,Object>>();
			hData.put("options", selectorOptions);

			//get options within selector
			List<String> options = Arrays.asList(sSpring.unescapeXML(selectorTag.getChildValue("options")).split("\n"));
			int optionCounter = 1;
			int selectedIndex = -1;
			for (String option : options) {
				Map<String,Object> hOptions = new HashMap<String,Object>();
				hOptions.put("option", option);
				hOptions.put("optionCounter", optionCounter);
				//preselect given answer
				if (("" + optionCounter).equals(answer)) {
					selectedIndex = optionCounter - 1;
				}
				selectorOptions.add(hOptions);
				optionCounter++;
			}
			hData.put("selectedIndex", selectedIndex);

			selectors.add(hData);
			
			counter++;
		}

		propertyMap.put("numberOfSelectors", getNumberOfSelectors());
		propertyMap.put("numberOfGivenAnswers", numberOfGivenAnswers);
		propertyMap.put("numberOfCorrectAnswers", numberOfCorrectAnswers);

		// Pieces
		
		propertyMap.put("referencesTitle", sSpring.unescapeXML(sSpring.getCaseComponentRoleName("", referencesCaseComponent.getName())));
		
		//NOTE get pieces within folder 'Fase 3 - Data-analyse'
		List<Map<String,Object>> pieces = new ArrayList<Map<String,Object>>();
		propertyMap.put("pieces", pieces);
		if (!feedbackReadyState) {
			IXMLTag rootTag = sSpring.getXmlDataPlusRunStatusTree(referencesCaseComponent, AppConstants.statusTypeRunGroup);
			if (rootTag == null) {
				return;
			}
			IXMLTag contentTag = rootTag.getChild(AppConstants.contentElement);
			if (contentTag == null) {
				return;
			}
			
			Object[] piecesHints = new Object[4];
			for (int i=0;i<=3;i++) {
				piecesHints[i] = getStateTagValue(feedbackPieceHintsPrefix + (i + 1)).split(AppConstants.statusCommaReplace);
			}
			
			IXMLTag correctFolderTag = null;
			//loop through folders
			for (IXMLTag folderTag : contentTag.getChilds("map")) {
				if (sSpring.unescapeXML(folderTag.getChildValue("name")).equals("Fase 3 - Data-analyse")) {
					correctFolderTag = folderTag;
					break;
				}
			}
			if (correctFolderTag != null) {
				counter = 0;
				for (IXMLTag pieceTag : correctFolderTag.getChilds("piece")) {
					if (sSpring.unescapeXML(pieceTag.getChildValue("name")).endsWith(" " + getVersion())) {
						Map<String,Object> hPieceData = new HashMap<String,Object>();
						pieces.add(hPieceData);
						hPieceData.put("tag", pieceTag);
						hPieceData.put("pieceName", sSpring.unescapeXML(pieceTag.getChildValue("name")));
						List<Map<String,Object>> hintList = new ArrayList<Map<String,Object>>();
						hPieceData.put("hintChars", hintList);
						//NOTE there are six types of hints related to the six selectors being asked
						for (int i=0;i<selectorTags.size();i++) {
							if (hintsAreAsked[i]) {
								for (int j=0;j<((String[])piecesHints[counter]).length;j++) {
									if (((String[])piecesHints[counter])[j].equals(getHintChars()[i])) {
										Map<String,Object> hHintData = new HashMap<String,Object>();
										hintList.add(hHintData);
										hHintData.put("hintChar", getHintChars()[i]);
									}
								}
							}
						}
						hPieceData.put("pieceHighlight", hintList.size() > 0);
						counter++;
					}
				}
			}
		}

		propertyMap.put("pieceData", getPieceData(null));
		
	}
	
	protected Map<String,IXMLTag> getNodeTagsByPid() {
		//put all node tags in a map per pid to be able to get them by number later on
		Map<String,IXMLTag> nodeTagsByPid = new HashMap<String,IXMLTag>();
		IXMLTag rootTag = sSpring.getXmlDataPlusRunStatusTree(feedbackAppCaseComponent, AppConstants.statusTypeRunGroup);
		if (rootTag == null) {
			return nodeTagsByPid;
		}
		IXMLTag contentTag = rootTag.getChild(AppConstants.contentElement);
		if (contentTag == null) {
			return nodeTagsByPid;
		}
		IXMLTag formTag = contentTag.getChild("form");
		if (formTag == null) {
			return nodeTagsByPid;
		}
		for (IXMLTag nodeTag : cScript.getChildNodeTags(formTag)) {
			nodeTagsByPid.put(nodeTag.getChildValue("pid"), nodeTag);
		}
		
		return nodeTagsByPid;
	}
	
	protected IECaseComponent getReferencesComponent() {
		return sSpring.getCaseComponent("", referencesCaseComponentName);
	}
	
	protected String getAnswer(IXMLTag selectorTag) {
		return selectorTag.getCurrentStatusAttribute(AppConstants.statusKeyAnswer);
	}	

	protected boolean isAnswerCorrect(IXMLTag selectorTag, String answer) {
		//correct options are entered in selector tag
		Pattern p = Pattern.compile(selectorTag.getChildValue("correctoptions"), Pattern.DOTALL);
		Matcher m = p.matcher(answer);
		return m.matches();
	}

	protected boolean getHintIsAsked(int number) {
		return ("," + getStateTagValue(feedbackHintsAsked) + ",").contains("," + number + ",");
	}
	
	protected void setHintIsAsked(int number) {
		String hintsAsked = getStateTagValue(feedbackHintsAsked);
		if (hintsAsked.contains("," + number + ",")) {
			return;
		}
		if (!hintsAsked.equals("")) {
			hintsAsked += ",";
		}
		hintsAsked += "" + number;
		setStateTagValue(feedbackHintsAsked, hintsAsked);
	}
	
	protected IECaseComponent getFeedbackAppCaseComponent() {
		return sSpring.getCaseComponent("", feedbackCaseComponentName);
	}
	

	public void onShowPopup(Event event) {
		Object[] data = (Object[])event.getData();
		Component sender = vView.getComponent((String)data[0]);
		HtmlBasedComponent popup = (HtmlBasedComponent)vView.getComponent(getUuid() + "_popup");
		if (sender == null || popup == null) {
			return;
		}

		Events.echoEvent("onUpdate", popup, sender);

		popup.setSclass(popup.getSclass().replace(" disappear", "") + " appear");
		setStateTagValue(feedbackCurrentQuestion, (String)(data[1] + ""));
	}

	public void onAskHint(Event event) {
		int number = (int)event.getData();
		setHintIsAsked(number);
		
		setCommissionProperties();

		Component div = vView.getComponent(getUuid() + "_div_item_" + number);
		if (div != null) {
			div.setAttribute("hintIsAsked", true);
		}

		Span component = (Span)vView.getComponent(getUuid() + "_div_hint_" + number);
		if (component != null) {
			component.setVisible(true);
		}
		
		HtmlMacroComponent macro = (HtmlMacroComponent)vView.getComponent(getUuid() + "_pieces_macro");
		if (macro != null) {
			macro.setDynamicProperty("a_pieces", propertyMap.get("pieces"));
			macro.recreate();
		}
	}
	
	protected String getAnswerJavaFunction() {
		return "feedbackSvenShowAnswer";
	}
	
	public void onGiveAnswer(Event event) {
		IXMLTag selectorTag = (IXMLTag)((Object[])event.getData())[0];
		int i = (int)((Object[])event.getData())[1];
		int answer = (int)((Object[])event.getData())[2];
		boolean scrollIntoView = useScrollIntoView() && (boolean)((Object[])event.getData())[3];

		boolean isAnswerCorrect = isAnswerCorrect(selectorTag, "" + answer);
		//just like for the original editforms component set following states
		setRunTagStatusJS(getFeedbackAppCaseComponent(), selectorTag, AppConstants.statusKeyAnswer, "" + answer, false);
		setRunTagStatusJS(getFeedbackAppCaseComponent(), selectorTag, AppConstants.statusKeySelected, AppConstants.statusValueTrue, false);
		setRunTagStatusJS(getFeedbackAppCaseComponent(), selectorTag, AppConstants.statusKeyCorrect, "" + isAnswerCorrect, false);
		
		Component component1 = vView.getComponent(getUuid() + "_div_button_" + i + "_new");
		if (component1 != null) {
			component1.setVisible(false);
		}
		
		Component component2 = vView.getComponent(getUuid() + "_div_button_" + i + "_edit");
		if (component2 != null) {
			component2.setVisible(true);
		}
		
		Span component3 = (Span)vView.getComponent(getUuid() + "_div_answer_" + i);
		if (component3 != null) {
			List<String> options = Arrays.asList(sSpring.unescapeXML(selectorTag.getChildValue("options")).split("\n"));
			Clients.evalJavaScript(getAnswerJavaFunction() + "('" + (getUuid() + "_div_answer_" + i) + "', '" + "<" + options.get(answer - 1) + ">" + "');");
			component3.setVisible(true);
		}
		
		if (i < selectorTags.size()) {
			Component component4 = vView.getComponent(getUuid() + "_div_item_" + (i + 1));
			if (component4 != null) {
				component4.setVisible(true);
			}
		}

		if (areAllAnswersGiven()) {
			Html component4 = (Html)vView.getComponent(getUuid() + "_div_end_of_mail");
			if (component4 != null) {
				component4.setVisible(true);
			}
			
			Div component5 = (Div)vView.getComponent(getUuid() + "_div_send_button");
			if (component5 != null) {
				component5.setVisible(true);
			}
		}
		
		if (scrollIntoView) {
			Clients.evalJavaScript("scrollIntoView('" + getUuid() + "_div_mail_read_panel');");
		}
	}
	
	public void onPieceClick(Event event) {
		//NOTE a piece is clicked so show piece pop-up
		IXMLTag tag = (IXMLTag)event.getData();

		vView.getComponent(piecePopupId).setVisible(true);
			
		//NOTE rerender macro to show updated piece
		HtmlMacroComponent piecePopupMacro = (HtmlMacroComponent)vView.getComponent(piecePopupMacroId);
		piecePopupMacro.setDynamicProperty("a_tag", tag);
		piecePopupMacro.setDynamicProperty("a_pieceData", getPieceData(tag));
		piecePopupMacro.recreate();

		//just like for the original references component set selected and opened for the piece
		setRunTagStatusJS(getReferencesComponent(), tag, AppConstants.statusKeySelected, AppConstants.statusValueTrue, false);
		setRunTagStatusJS(getReferencesComponent(), tag, AppConstants.statusKeyOpened, AppConstants.statusValueTrue, false);

	}
	
	public void onPiecePopupClose(Event event) {
		//NOTE close piece pop-up
		vView.getComponent(piecePopupId).setVisible(false);
	}
	
	public void onShowTweets(Event event) {
		//NOTE show tweets macro
		vView.getComponent(tweetsPopupId).setVisible(true);
	}
	
	public void onTweetsShownClose(Event event) {
		//NOTE show tweets macro
		vView.getComponent(tweetsPopupId).setVisible(false);
	}
	
	protected Map<String,Object> getPieceData(IXMLTag pieceTag) {
		Map<String,Object> hPieceData = new HashMap<String,Object>();
		hPieceData.put("zulfilepath", getZulfilepath());
		hPieceData.put("macroBoxUuid", getUuid());
		hPieceData.put("piecePopupVisible", pieceTag != null);
		hPieceData.put("referencesTitle", sSpring.unescapeXML(sSpring.getCaseComponentRoleName("", referencesCaseComponent.getName())));
		String pieceName = "";
		String pieceContent = "";
		if (pieceTag != null) {
			pieceName = sSpring.unescapeXML(pieceTag.getChildValue("name"));
			String pieceUrl = sSpring.getSBlobHelper().getUrl(pieceTag, "blob");
			//NOTE html is shown in a zk html tag so get content of file 
			String pieceFilePath = sSpring.getAppManager().getAbsoluteAppPath() + pieceUrl.replaceFirst("/", ""); 
			if (sSpring.getFileManager().fileExists(pieceFilePath)) {
				pieceContent = new String(sSpring.getFileManager().readFile(pieceFilePath));
				//NOTE there are six types of hints related to the six selectors being asked
				/*
				for (int i=1;i<=getHintChars().length;i++) {
					//answers are stored as state tag value
					if (!getHintIsAsked(i)) {
						//strip content of hints for selector i by replacing with empty string
						pieceContent = pieceContent.replace(
								"<tr><td class=\"js-hint\"><span class=\"js-indicator yellow\">" + getHintChars()[i - 1] + "</span>Ontbrekende analyse</td></tr>", 
								"");
						pieceContent = pieceContent.replace(
								"<tr><td class=\"js-hint\"><span class=\"js-indicator yellow\">" + getHintChars()[i - 1] + "</span>Ontbrekende analyse</td></tr>", 
								"");
						pieceContent = pieceContent.replace(
								"<td class=\"js-hint\"><span class=\"js-indicator yellow\">" + getHintChars()[i - 1] + "</span>",
								"<td>");
					}
				}
				*/
			}
		}
		hPieceData.put("pieceName", pieceName);
		hPieceData.put("pieceContent", pieceContent);
		return hPieceData;
	}
	
	public void sendFeedback() {
		hideMacro(currentTag);
		if (!areAllAnswersCorrect()) {
			showMacro("feedback_sven_tweets_macro_" + getVersion());
			setStateTagValue(feedbackTweetsAreShownStateKeyPid, AppConstants.statusValueTrue);
		}
		else {
			setStateTagValue(feedbackReadyStateKeyPid, AppConstants.statusValueTrue);
		}
	}
	
	protected boolean areAllAnswersGiven() {
		//put all node tags in a map per pid to be able to get them by number later on
		Map<String,IXMLTag> nodeTagsByPid = getNodeTagsByPid();
		int counter = 1;
		while (nodeTagsByPid.containsKey(selectorPidPrefix + counter)) {
			//content to be rendered is entered in two content elements per selector
			IXMLTag selectorTag = nodeTagsByPid.get(selectorPidPrefix + counter);
			String answer = getAnswer(selectorTag);
			if (answer.equals("")) {
				return false;
			}
			counter++;
		}
		return true;
	}
	
	protected boolean areAllAnswersCorrect() {
		//put all node tags in a map per pid to be able to get them by number later on
		Map<String,IXMLTag> nodeTagsByPid = getNodeTagsByPid();
		int counter = 1;
		while (nodeTagsByPid.containsKey(selectorPidPrefix + counter)) {
			//content to be rendered is entered in two content elements per selector
			IXMLTag selectorTag = nodeTagsByPid.get(selectorPidPrefix + counter);
			if (!isAnswerCorrect(selectorTag, getAnswer(selectorTag))) {
				return false;
			}
			counter++;
		}
		return true;
	}
	
	public void onAnimationEnd(Event event) {
		sendFeedback();
	}
	
	@Override
	public void handleStatusChange(STriggeredReference aTriggeredReference) {
	}

}
