/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.Gamebrics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Div;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CCaseHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefBox;
import nl.surf.emergo.control.def.CDefHelper;
import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.script.CScript;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpring;

public class CRunDashboardDivInitBox extends CDefBox {

	private static final long serialVersionUID = -86776646161307291L;

	protected VView vView = CDesktopComponents.vView();
	protected SSpring sSpring = CDesktopComponents.sSpring();
	protected CScript cScript = CDesktopComponents.cScript();
	protected CCaseHelper cCaseHelper = new CCaseHelper();
	
	protected String zulfilepath = "";
	protected IECaseComponent currentCaseComponent;
	protected IXMLTag currentTag;
	
	protected IECaseComponent rubricsCaseComponent;
	protected List<IXMLTag> rubricTags;
	protected List<IXMLTag> subskillTags;
	protected IXMLTag rubricTag;
	protected IXMLTag skillTag;
	protected int numberOfSkillClusters = 0;

	protected IECaseComponent challengesCaseComponent;
	protected List<IXMLTag> microchallengeTags;
	protected List<IXMLTag> challengecontainerTags;
	
	protected IECaseComponent feedbacksCaseComponent;
	protected List<IXMLTag> roottextTags;
	protected List<IXMLTag> feedbacktextTags;

	private static final int defaultRubricNumberOfPerformanceLevels = 5;

	private static final double scorePerPerformanceLevel = 50;

	private static final double scoreLevelWeak = 0.25;
	private static final double scoreLevelReasonable = 0.5;
	
	protected int rubricNumberOfPerformanceLevels = defaultRubricNumberOfPerformanceLevels;
	
	protected Map<String,Double> maxPerformanceLevelPerSubSkillId = new HashMap<String,Double>();
	protected Map<String,Double> maxPerformanceLevelsUntilNowPerSubSkillId = new HashMap<String,Double>();
	protected Map<String,Double> maxScorePerSubSkillId = new HashMap<String,Double>();
	protected Map<String,Double> scorePerSubSkillId = new HashMap<String,Double>();
	protected Double totalMaxScore = new Double(0);
	protected Double totalScore = new Double(0);
	protected Map<String,Double> relativeScorePerSubSkillId = new HashMap<String,Double>();
	protected Map<String,Double> maxScoreNotPlayedPerSubSkillId = new HashMap<String,Double>();
	protected Map<String,Double> performanceLevelScorePerSubSkillId = new HashMap<String,Double>();

	protected Map<String,List<IXMLTag>> microchallengeTagsPerSubskillId = new HashMap<String,List<IXMLTag>>();
	protected Map<String,List<Integer>> microchallengeNumbersPerSubskillId = new HashMap<String,List<Integer>>();

	protected Map<String,Map<String,Double>> maxLevelPerMicrochallengeIdPerSubskillId = new HashMap<String,Map<String,Double>>();
	protected Map<String,Map<String,Double>> levelPerChallengecontainerIdPerSubskillId = new HashMap<String,Map<String,Double>>();
	protected Map<String,Map<String,Double>> numberOfMicrochallengesPerChallengecontainterIdPerSubskillId = new HashMap<String,Map<String,Double>>();
	protected Map<String,Map<String,Double>> maxLevelPerChallengecontainerIdPerSubskillId = new HashMap<String,Map<String,Double>>();
	protected Map<Integer,String> challengeContainerNumberToId = new HashMap<Integer,String>();
	
	protected Map<String,Double> numberOfMicrochallengesPerSubskillId = new HashMap<String,Double>();
	protected Map<String,Double> numberOfMaxScoredMicrochallengesPerSubskillId = new HashMap<String,Double>();
	
	protected int momentDuoDiff = 1;
	protected int momentIndex = -1;

	public static final String roottext_percentage_0 = "Je hebt geen van de challenges goed volbracht. Als je nog wilt verbeteren, ";
	public static final String roottext_percentage_0_25 = "Je hebt een aantal van de challenges goed volbracht. Als je nog wilt verbeteren, ";
	public static final String roottext_percentage_25_50 = "Je hebt bijna de helft van de challenges goed volbracht. Als je nog wilt verbeteren, ";
	public static final String roottext_percentage_50_75 = "Je hebt het merendeel van de challenges goed volbracht. Als je nog wilt verbeteren, ";
	public static final String roottext_percentage_75_100 = "Je hebt bijna alle challenges goed volbracht. Als je nog wilt verbeteren, ";
	public static final String roottext_percentage_100 = "Je hebt alle challenges goed volbracht. Je kunt je niet verder verbeteren.";

	public static final String[][] feedbackPerPeformancelevelPerSubskill =  {
			{
				"",
				"probeer bij het uitvoeren van de challenges na te gaan waarom bepaalde items met informatie nodig zijn.", 
				"probeer bij het uitvoeren van de challenges te bepalen wat de verschillen / overeenkomsten tussen de verschillende items met informatie zijn.", 
				"probeer bij het uitvoeren van de challenges te herkennen welke items met informatie nodig zijn, waarom deze nodig zijn, en de onderliggende verschillen / overeenkomsten te bepalen. Neem deze overwegingen mee in je keuzes." 
			},
			{
				"",
				"probeer bij het uitvoeren van de challenges na te gaan waarom items fout kunnen zijn.", 
				"probeer bij het uitvoeren van de challenges te bepalen welke soorten fouten er kunnen voorkomen en hoe deze verbeterd kunnen worden.", 
				"probeer bij het uitvoeren van de challenges te herkennen welke items fout zijn, waarom deze fout zijn, welke soort fouten er zijn en hoe deze verbeterd kunnen worden. Neem deze overwegingen mee in je keuzes."
			},
			{
				"",
				"probeer bij het uitvoeren van de challenges na te gaan waarom je bepaalde algemene principes kunt afleiden en waarom uit de betreffende informatie.", 
				"probeer bij het uitvoeren van de challenges te bepalen welke onderliggende kenmerken verschillende principes hebben.", 
				"probeer bij het uitvoeren van de challenges te herkennen welke algemene principes je kunt afleiden en vanuit welke informatie, waarom je deze principes kunt afleiden, en welke onderliggende kenmerken verschillende principes hebben. Neem deze overwegingen mee in je keuzes."
			},
			{
				"",
				"probeer bij het uitvoeren van de challenges na te gaan waarom er bepaalde gevolgtrekkingen gemaakt kunnen worden uit de algemene principes en waarom deze in de betreffende specifieke contexten toegepast kunnen worden.", 
				"probeer bij het uitvoeren van de challenges te bepalen welke onderliggende kenmerken verschillende soorten gevolgtrekkingen kunnen hebben.", 
				"probeer tijdens het uitvoeren van de challenges te herkennen welke gevolgtrekkingen er gemaakt kunnen worden uit de algemene principes, in welke specifieke contexten, waarom, en welke onderliggende eigenschappen ze hebben. Neem deze overwegingen mee in je keuzes."
			},
			{
				"",
				"probeer bij het uitvoeren van de challenges na te gaan waarom de challenges op te delen zijn in de betreffende (deel)aspecten.", 
				"probeer bij het uitvoeren van de challenges te bepalen welke onderliggende kenmerken de verschillende (deel)aspecten hebben.", 
				"probeer bij het uitvoeren van de challenges te herkennen te welke (deel)aspecten er zijn, waarom en probeer te bepalen welke onderliggende kenmerken de verschillende (deel)aspecten hebben. Neem deze overwegingen mee tijdens het uitvoeren van de challenges."
			},
			{
				"",
				"probeer bij het uitvoeren van de challenges na te gaan waarom de informatie op deze verschillende manieren te structureren is en op deze volgordes.", 
				"probeer bij het uitvoeren van de challenges te bepalen welke onderliggende kenmerken de verschillende manieren van structureren van de informatie hebben.", 
				"probeer bij het uitvoeren van de challenges te herkennen welke gevolgen er voortkomen uit de verschillende manieren van informatie structureren. Houd bij het doen van de challenges rekening met de verschillende mogelijke manieren van structureren."
			},
			{
				"",
				"probeer bij het uitvoeren van de challenges na te gaan waarom de verschillende beslissingen mogelijk zijn op basis van de beschikbare informatie.", 
				"probeer bij het uitvoeren van de challenges te bepalen welke gevolgen de beslissingen hebben en aan welke criteria de mogelijke beslissingen voldoen.", 
				"probeer bij het uitvoeren van de challenges te herkennen welke beslissingen er mogelijk zijn, waarom, welke gevolgen deze hebben en aan welke criteria de beslissingen voldoen. Neem deze overwegingen mee bij het uitvoeren van de challenges."
			},
			{
				"",
				"probeer bij het uitvoeren van de challenges na te gaan waarom de verschillende perspectieven mogelijk zijn.", 
				"probeer te bepalen welke onderliggende kenmerken de verschillende mogelijke perspectieven hebben en wat de implicaties van elk perspectief zijn.", 
				"probeer bij het uitvoeren van de challenges te herkennen welke perspectieven er zijn en waarom, de onderliggende eigenschappen ervan en wat de implicaties van elk perspectief zijn. Neem deze overwegingen mee bij het uitvoeren van de challenges."
			}
	};
	
	public static final String performanceLevelAchievedState = "achieved";
	public static final String performanceLevelImprovedState = "improved";
	public static final String performanceLevelDeterioratedState = "deteriorated";
	public static final String performanceLevelNoScoreState = "noScore";
	public static final String performanceLevelNotAbleToScoreState = "notAbleToScore";
	public static final String performanceLevelNotYetAddressedState = "notYetAddressed";
	public static final String performanceLevelImprovementPossibleState = "improvementPossible";
	public static final String performanceLevelNoImprovementPossibleState = "noImprovementPossible";
	public static final String performanceLevelDeterioratedWithoutBlameState = "deterioratedWithoudBlame";

	protected static final String performanceLevelAchievedColor = "#C9EB88";
	protected static final String performanceLevelImprovedColor = "#7CAD48";
	protected static final String performanceLevelDeterioratedColor = "#D47F6B";
	public static final String performanceLevelNotAbleToScoreColor = "transparent";
	public static final String performanceLevelNotYetAddressedColor = "#CECECE";
	public static final String performanceLevelImprovementPossibleColor = "#FFFFFF";
	public static final String performanceLevelNoImprovementPossibleColor = "#8A8A8A";
	public static final String performanceLevelDeterioratedWithoutBlameColor = "#DD9A20";

	public static final String performanceLevelSubSkillColor = "#FFFFFF";

	public void onCreate(CreateEvent aEvent) {
		HtmlMacroComponent mainMacro = (HtmlMacroComponent)(HtmlMacroComponent)CRunComponent.getMacroParent(this);

		zulfilepath = (String)mainMacro.getDynamicProperty("a_zulfilepath");
		currentCaseComponent = (IECaseComponent)mainMacro.getDynamicProperty("a_casecomponent");
		currentTag = (IXMLTag)mainMacro.getDynamicProperty("a_tag");

		int numberOfTimesDashboardIsShown = currentTag.getStatusAttributeCount(AppConstants.statusKeyPresent, AppConstants.statusValueTrue);
		
		Map<String,Object> propertyMap = new HashMap<String,Object>();

		rubricsCaseComponent = sSpring.getCaseComponent(sSpring.getCase(), "", "GB_rubrics");
		rubricTags = cScript.getNodeTags(rubricsCaseComponent, "rubric");
		subskillTags = cScript.getNodeTags(rubricsCaseComponent, "subskill");

		double totalScorePerPerformanceLevel = 0;
		double totalSubSkillsWeight = 0;
		//TODO account for skillclusters as well?
		for (IXMLTag subskillTag : subskillTags) {
			maxPerformanceLevelPerSubSkillId.put(subskillTag.getAttribute(AppConstants.keyId), new Double(0));
			maxPerformanceLevelsUntilNowPerSubSkillId.put(subskillTag.getAttribute(AppConstants.keyId), new Double(0));
			maxScorePerSubSkillId.put(subskillTag.getAttribute(AppConstants.keyId), new Double(0));
			scorePerSubSkillId.put(subskillTag.getAttribute(AppConstants.keyId), new Double(0));
			relativeScorePerSubSkillId.put(subskillTag.getAttribute(AppConstants.keyId), new Double(0));
			maxScoreNotPlayedPerSubSkillId.put(subskillTag.getAttribute(AppConstants.keyId), new Double(0));
			microchallengeTagsPerSubskillId.put(subskillTag.getAttribute(AppConstants.keyId), new ArrayList<IXMLTag>());
			microchallengeNumbersPerSubskillId.put(subskillTag.getAttribute(AppConstants.keyId), new ArrayList<Integer>());
			numberOfMicrochallengesPerSubskillId.put(subskillTag.getAttribute(AppConstants.keyId), new Double(0));
			numberOfMaxScoredMicrochallengesPerSubskillId.put(subskillTag.getAttribute(AppConstants.keyId), new Double(0));
			totalScorePerPerformanceLevel += scorePerPerformanceLevel;
			double subskillWeighting = 0;
			try {
				subskillWeighting = Double.parseDouble(subskillTag.getChildValue("weighting"));
			} catch (NumberFormatException e) {
				subskillWeighting = 1;
			}
			performanceLevelScorePerSubSkillId.put(subskillTag.getAttribute(AppConstants.keyId), subskillWeighting);
			totalSubSkillsWeight += subskillWeighting;
		}

		for (IXMLTag subskillTag : subskillTags) {
			double subskillWeighting = performanceLevelScorePerSubSkillId.get(subskillTag.getAttribute(AppConstants.keyId));
			double subSkillScorePerPerformanceLevel = 0;
			if (totalSubSkillsWeight == 0) {
				subSkillScorePerPerformanceLevel = scorePerPerformanceLevel;
			} else {
				subSkillScorePerPerformanceLevel = subskillWeighting / totalSubSkillsWeight * totalScorePerPerformanceLevel;
			}
			performanceLevelScorePerSubSkillId.put(subskillTag.getAttribute(AppConstants.keyId), subSkillScorePerPerformanceLevel);
		}
		
		challengesCaseComponent = sSpring.getCaseComponent(sSpring.getCase(), "", "GB_challenges");		
		microchallengeTags = cScript.getRunGroupNodeTags(challengesCaseComponent, "microchallenge");
		//NOTE only use micro challenges that are present and parents are present as well.
		for (int i=microchallengeTags.size()-1;i>=0;i--) {
			if (!microchallengeTags.get(i).getCurrentStatusAttribute(AppConstants.statusKeyPresent).equals(AppConstants.statusValueTrue) ||
					getChallengecontainerParentTag(microchallengeTags.get(i)) == null) {
				microchallengeTags.remove(i);
			}
		}
		//sort micro challenge tags by finished time, meaning the time when the value of the micro challenge is by script
		Collections.sort(microchallengeTags, new MicrochallengeSortByFinishedTime());

		challengecontainerTags = cScript.getRunGroupNodeTags(challengesCaseComponent, "challengecontainer");
		//NOTE only use challenge containers on top level because they represent mini games and they should be present
		for (int i=challengecontainerTags.size()-1;i>=0;i--) {
			if (!challengecontainerTags.get(i).getParentTag().getName().equals(AppConstants.contentElement) ||
					!challengecontainerTags.get(i).getCurrentStatusAttribute(AppConstants.statusKeyPresent).equals(AppConstants.statusValueTrue)) {
				challengecontainerTags.remove(i);
			}
		}
		//sort challenge container tags by finished time, meaning the time when finished of the challenge container is set by script
		Collections.sort(challengecontainerTags, new ChallengeContainerSortByFinishedTime());

		for (IXMLTag challengecontainerTag : challengecontainerTags) {
			Map<String,Double> levelsPerSubskillId = new HashMap<String,Double>();
			Map<String,Double> numberOfMicrochallengesPerSubskillId = new HashMap<String,Double>();
			Map<String,Double> maxLevelsPerSubskillId = new HashMap<String,Double>();
			//TODO account for skillclusters as well?
			for (IXMLTag subskillTag : subskillTags) {
				levelsPerSubskillId.put(subskillTag.getAttribute(AppConstants.keyId), new Double(0));
				numberOfMicrochallengesPerSubskillId.put(subskillTag.getAttribute(AppConstants.keyId), new Double(0));
				maxLevelsPerSubskillId.put(subskillTag.getAttribute(AppConstants.keyId), new Double(0));
			}			
			levelPerChallengecontainerIdPerSubskillId.put(challengecontainerTag.getAttribute(AppConstants.keyId), levelsPerSubskillId);
			numberOfMicrochallengesPerChallengecontainterIdPerSubskillId.put(challengecontainerTag.getAttribute(AppConstants.keyId), numberOfMicrochallengesPerSubskillId);
			maxLevelPerChallengecontainerIdPerSubskillId.put(challengecontainerTag.getAttribute(AppConstants.keyId), maxLevelsPerSubskillId);
		}

		String rubricNumberOfPerformanceLevelsStr = "";
		if (rubricTags.size() > 0) {
			//TODO account for multiple rubrics
			rubricTag = rubricTags.get(0);
			rubricNumberOfPerformanceLevelsStr = rubricTag.getChildValue("numberofperformancelevels");
			skillTag = rubricTag.getChild("skill");
			numberOfSkillClusters = skillTag.getChilds("skillcluster").size();			
		}
		try {
			rubricNumberOfPerformanceLevels = Integer.parseInt(rubricNumberOfPerformanceLevelsStr);
		} catch (NumberFormatException e) {
		}
		propertyMap.put("rubricNumberOfPerformanceLevels", "" + rubricNumberOfPerformanceLevels);
		
		feedbacksCaseComponent = sSpring.getCaseComponent(sSpring.getCase(), "", "GB_feedbacks");	
		roottextTags = cScript.getRunGroupNodeTags(feedbacksCaseComponent, "roottext");
		feedbacktextTags = cScript.getRunGroupNodeTags(feedbacksCaseComponent, "feedbacktext");
		
		StringBuffer microchallengesBuffer = new StringBuffer();
		int microchallengeCounter = 1;
		for (IXMLTag xmlTag : microchallengeTags) {
			if (!maxLevelPerMicrochallengeIdPerSubskillId.containsKey(xmlTag.getAttribute(AppConstants.keyId))) {
				Map<String,Double> maxLevelsPerSubskillId = new HashMap<String,Double>();
				for (IXMLTag subskillTag : subskillTags) {
					maxLevelsPerSubskillId.put(subskillTag.getAttribute(AppConstants.keyId), new Double(0));
				}
				maxLevelPerMicrochallengeIdPerSubskillId.put(xmlTag.getAttribute(AppConstants.keyId), maxLevelsPerSubskillId);
			}
			IXMLTag challengecontainerParentTag = getChallengecontainerParentTag(xmlTag);
			StringBuffer microchallengesubskillsBuffer = new StringBuffer();
			int[] scoreData = calculateMaxScoreAndScorePerMicroChallenge(
					challengecontainerParentTag, 
					xmlTag, 
					rubricNumberOfPerformanceLevels, 
					subskillTags.size(), 
					microchallengesubskillsBuffer, 
					microchallengeCounter);
			microchallengesubskillsBuffer.insert(0, "[");
			microchallengesubskillsBuffer.append("]");
			boolean microChallengeIsFinished = scoreData[1] > 0; 
			if (microChallengeIsFinished) {
				//NOTE only show micro challenges that are finished so have a score > 0
				StringBuffer jsonBuffer = new StringBuffer();
				addJsonKeyValue(jsonBuffer, "tagId", xmlTag.getAttribute(AppConstants.keyId), true);
				addJsonKeyValue(jsonBuffer, "name", sSpring.unescapeXML(xmlTag.getChildValue("name")), true);
				addJsonKeyValue(jsonBuffer, "description", sSpring.unescapeXML(challengecontainerParentTag.getChildValue("name")) + " " + sSpring.unescapeXML(xmlTag.getChildValue("pid")), true);
				addJsonKeyValue(jsonBuffer, "maxScore", "" + scoreData[0], true);
				addJsonKeyValue(jsonBuffer, "score", "" + scoreData[1], true);
				addJsonKeyValue(jsonBuffer, "microchallengesubskills", microchallengesubskillsBuffer, false);
				if (microchallengesBuffer.length() > 0) {
					microchallengesBuffer.append(",");
				}
				microchallengesBuffer.append("{");
				microchallengesBuffer.append(jsonBuffer);
				microchallengesBuffer.append("}");
			}
		
			microchallengeCounter++;
		}
		microchallengesBuffer.insert(0, "[");
		microchallengesBuffer.append("]");
	
		StringBuffer microchallengesPerSubskillBuffer = new StringBuffer();
		for (IXMLTag subskillTag : subskillTags) {
			String subskillTagId = subskillTag.getAttribute(AppConstants.keyId);
			if (maxScorePerSubSkillId.get(subskillTagId) > 0) {
				Double tempScore = new Double(scorePerSubSkillId.get(subskillTagId)) / maxScorePerSubSkillId.get(subskillTagId);
				relativeScorePerSubSkillId.put(subskillTagId, tempScore);
			}
			else {
				relativeScorePerSubSkillId.put(subskillTagId, new Double(0));
			}
			StringBuffer jsonBuffer = new StringBuffer();
			StringBuffer microchallengesplayedPerSubskillBuffer = new StringBuffer();
			StringBuffer microchallengesnotplayedPerSubskillBuffer = new StringBuffer();
			List<IXMLTag> microchallengeTags = microchallengeTagsPerSubskillId.get(subskillTagId);
			for (IXMLTag microchallengeTag : microchallengeTags) {
				int number = microchallengeNumbersPerSubskillId.get(subskillTagId).get(microchallengeTags.indexOf(microchallengeTag));
				String description = sSpring.unescapeXML(getChallengecontainerParentTag(microchallengeTag).getChildValue("name")) + " " + sSpring.unescapeXML(microchallengeTag.getChildValue("pid"));
				boolean microChallengeIsFinished = !microchallengeTag.getCurrentStatusAttribute(AppConstants.statusKeyValue).equals("0");

				StringBuffer tempMicrochallengesBuffer = microChallengeIsFinished ? microchallengesplayedPerSubskillBuffer : microchallengesnotplayedPerSubskillBuffer;
				StringBuffer jsonBuffer2 = new StringBuffer();
				addJsonKeyValue(jsonBuffer2, "number", "" + number, true);
				addJsonKeyValue(jsonBuffer2, "description", description, false);
				if (tempMicrochallengesBuffer.length() > 0) {
					tempMicrochallengesBuffer.append(",");
				}
				tempMicrochallengesBuffer.append("{");
				tempMicrochallengesBuffer.append(jsonBuffer2);
				tempMicrochallengesBuffer.append("}");
			}
			microchallengesplayedPerSubskillBuffer.insert(0, "[");
			microchallengesplayedPerSubskillBuffer.append("]");
			microchallengesnotplayedPerSubskillBuffer.insert(0, "[");
			microchallengesnotplayedPerSubskillBuffer.append("]");
			addJsonKeyValue(jsonBuffer, "microchallengesplayed", microchallengesplayedPerSubskillBuffer, true);
			addJsonKeyValue(jsonBuffer, "microchallengesnotplayed", microchallengesnotplayedPerSubskillBuffer, true);
			addJsonKeyValue(jsonBuffer, "maxscorenotplayed", "" + Math.round(maxScoreNotPlayedPerSubSkillId.get(subskillTagId)), false);

			if (microchallengesPerSubskillBuffer.length() > 0) {
				microchallengesPerSubskillBuffer.append(",");
			}
			microchallengesPerSubskillBuffer.append("{");
			microchallengesPerSubskillBuffer.append(jsonBuffer);
			microchallengesPerSubskillBuffer.append("}");
		}
		microchallengesPerSubskillBuffer.insert(0, "[");
		microchallengesPerSubskillBuffer.append("]");

		StringBuffer subskillsBuffer = new StringBuffer();
		for (IXMLTag subskillTag : subskillTags) {
			StringBuffer jsonBuffer = new StringBuffer();
			addJsonKeyValue(jsonBuffer, "tagId", subskillTag.getAttribute(AppConstants.keyId), true);
			addJsonKeyValue(jsonBuffer, "name", sSpring.unescapeXML(subskillTag.getChildValue("name")), true);
			addJsonKeyValue(jsonBuffer, "maxPerformanceLevel", "" + maxPerformanceLevelPerSubSkillId.get(subskillTag.getAttribute(AppConstants.keyId)).intValue(), true);
			addJsonKeyValue(jsonBuffer, "maxPerformanceLevelUntilNow", "" + maxPerformanceLevelsUntilNowPerSubSkillId.get(subskillTag.getAttribute(AppConstants.keyId)).intValue(), true);
			Double maxScore = maxScorePerSubSkillId.get(subskillTag.getAttribute(AppConstants.keyId));
			Double score = scorePerSubSkillId.get(subskillTag.getAttribute(AppConstants.keyId));
			addJsonKeyValue(jsonBuffer, "maxScore", "" + Math.round(maxScore), true);
			addJsonKeyValue(jsonBuffer, "score", "" + Math.round(score), true);
			if (score > 0) {
				totalMaxScore += maxScore;
				totalScore += score;
			}
			String performance = "";
			if (relativeScorePerSubSkillId.get(subskillTag.getAttribute(AppConstants.keyId)) <= scoreLevelWeak) {
				performance = "weak";
			}
			else if (relativeScorePerSubSkillId.get(subskillTag.getAttribute(AppConstants.keyId)) <= scoreLevelReasonable) {
				performance = "reasonable";
			}
			else {
				performance = "good";
			}
			addJsonKeyValue(jsonBuffer, "performance", performance, false);
			if (subskillsBuffer.length() > 0) {
				subskillsBuffer.append(",");
			}
			subskillsBuffer.append("{");
			subskillsBuffer.append(jsonBuffer);
			subskillsBuffer.append("}");
		}
		subskillsBuffer.insert(0, "[");
		subskillsBuffer.append("]");

		StringBuffer momentsBuffer = new StringBuffer();
		for (IXMLTag challengecontainerTag : challengecontainerTags) {
			if (challengecontainerTag.getCurrentStatusAttribute(AppConstants.statusKeyFinished).equals(AppConstants.statusValueTrue)) {
				//NOTE only show challenge containers that are finished
				StringBuffer jsonBuffer = new StringBuffer();
				addJsonKeyValue(jsonBuffer, "tagId", challengecontainerTag.getAttribute(AppConstants.keyId), true);
				addJsonKeyValue(jsonBuffer, "name", sSpring.unescapeXML(challengecontainerTag.getChildValue("name")), true);
				StringBuffer challengecontainerSubskillsBuffer = new StringBuffer();
				for (IXMLTag subskillTag : subskillTags) {
					double level = levelPerChallengecontainerIdPerSubskillId.get(challengecontainerTag.getAttribute(AppConstants.keyId)).get(subskillTag.getAttribute(AppConstants.keyId));
					double number = numberOfMicrochallengesPerChallengecontainterIdPerSubskillId.get(challengecontainerTag.getAttribute(AppConstants.keyId)).get(subskillTag.getAttribute(AppConstants.keyId));
					double maxLevel = maxLevelPerChallengecontainerIdPerSubskillId.get(challengecontainerTag.getAttribute(AppConstants.keyId)).get(subskillTag.getAttribute(AppConstants.keyId));
					if (level > 0 && number > 1) {
						level = level / number;
						levelPerChallengecontainerIdPerSubskillId.get(challengecontainerTag.getAttribute(AppConstants.keyId)).put(subskillTag.getAttribute(AppConstants.keyId), level);
					}
					if (maxLevel > 0 && number > 1) {
						maxLevel = maxLevel / number;
						maxLevelPerChallengecontainerIdPerSubskillId.get(challengecontainerTag.getAttribute(AppConstants.keyId)).put(subskillTag.getAttribute(AppConstants.keyId), maxLevel);
					}
					if (level < 1) {
						//NOTE level may be less than 1 if maximum sub skill level for a certain micro challenge is less than 4.
						//if for instance maximum level is 1 and student has a score of 2 (in range of 1 till 4) for the micro challenge,
						//then level will be 2/4 = 0.5
						level = 1;
					}
					int intLevel = new Long(Math.round(level)).intValue();
	
					StringBuffer jsonBuffer2 = new StringBuffer();
					addJsonKeyValue(jsonBuffer2, "tagId", subskillTag.getAttribute(AppConstants.keyId), true);
					addJsonKeyValue(jsonBuffer2, "name", sSpring.unescapeXML(subskillTag.getChildValue("name")), true);
					addJsonKeyValue(jsonBuffer2, "level", "" + intLevel, false);
					if (challengecontainerSubskillsBuffer.length() > 0) {
						challengecontainerSubskillsBuffer.append(",");
					}
					challengecontainerSubskillsBuffer.append("{");
					challengecontainerSubskillsBuffer.append(jsonBuffer2);
					challengecontainerSubskillsBuffer.append("}");
				}			
				challengecontainerSubskillsBuffer.insert(0, "[");
				challengecontainerSubskillsBuffer.append("]");
				addJsonKeyValue(jsonBuffer, "challengecontainersubskills", challengecontainerSubskillsBuffer, false);
				if (momentsBuffer.length() > 0) {
					momentsBuffer.append(",");
				}
				momentsBuffer.append("{");
				momentsBuffer.append(jsonBuffer);
				momentsBuffer.append("}");
			
				momentIndex++;

				challengeContainerNumberToId.put(momentIndex, challengecontainerTag.getAttribute(AppConstants.keyId));
			}
		}
		momentsBuffer.insert(0, "[");
		momentsBuffer.append("]");

		StringBuffer advicesBuffer = new StringBuffer();
		int counter = 0;
		for (IXMLTag subskillTag : subskillTags) {
			StringBuffer jsonBuffer = new StringBuffer();
			String feedback = "";
			double relativeScore = relativeScorePerSubSkillId.get(subskillTag.getAttribute(AppConstants.keyId));
			if (relativeScore == 0) {
				feedback = vView.getLabel("Gamebrics.dashboard.advices.advice_not_yet_addressed");
			}
			else {
				double number = numberOfMicrochallengesPerSubskillId.get(subskillTag.getAttribute(AppConstants.keyId));
				double maxNumber = numberOfMaxScoredMicrochallengesPerSubskillId.get(subskillTag.getAttribute(AppConstants.keyId));
				double percentage = maxNumber / number;
				feedback = getRootText(percentage) + " " + getFeedbackText(percentage, subskillTag, counter);
			}
			addJsonKeyValue(jsonBuffer, "advice", feedback, false);
			
			if (advicesBuffer.length() > 0) {
				advicesBuffer.append(",");
			}
			advicesBuffer.append("{");
			advicesBuffer.append(jsonBuffer);
			advicesBuffer.append("}");
			
			counter++;
		}
		advicesBuffer.insert(0, "[");
		advicesBuffer.append("]");

		
		HtmlMacroComponent macro = new HtmlMacroComponent();
		appendChild(macro);

		propertyMap.put("relativePath", zulfilepath);
		
		propertyMap.put("numberOfTimesDashboardIsShown", "" + numberOfTimesDashboardIsShown);
		
		long caseTime = Math.round(sSpring.getCaseTime() * 1000);
		long numberOfDays = caseTime / (24 * 60 * 60 * 1000);
		propertyMap.put("total_days", "" + numberOfDays);
		propertyMap.put("total_time", CDefHelper.getDateStrAsHMS(new Date(caseTime)));
		
		//TODO
		propertyMap.put("mean_time", "00:25:20");
		
		propertyMap.put("total_score", "" + Math.round(totalScore) + " / " + Math.round(totalMaxScore));

		//TODO
		propertyMap.put("challenge_time", "00:03:00");
		propertyMap.put("challenge_mean_time", "00:01:20");
		
		propertyMap.put("subskills", subskillsBuffer.toString());
		propertyMap.put("microchallenges", microchallengesBuffer.toString());
		propertyMap.put("moments", momentsBuffer.toString());
		propertyMap.put("momentDuoDiff", "" + momentDuoDiff);
		propertyMap.put("microchallengespersubskill", microchallengesPerSubskillBuffer.toString());
		propertyMap.put("advices", advicesBuffer.toString());

		macro.setDynamicProperty("a_propertyMap", propertyMap);
		macro.setMacroURI(zulfilepath + "run_dashboard_view_div.zul");

		Events.echoEvent("onShowSkillWheel", this, null);
	}

	protected class MicrochallengeSortByFinishedTime implements Comparator<IXMLTag>{
		public int compare(IXMLTag tag1, IXMLTag tag2) {
			return compareTime(tag1.getCurrentStatusAttributeTime(AppConstants.statusKeyValue), tag2.getCurrentStatusAttributeTime(AppConstants.statusKeyValue));
		}
	}
	
	protected class ChallengeContainerSortByFinishedTime implements Comparator<IXMLTag>{
		public int compare(IXMLTag tag1, IXMLTag tag2) {
			return compareTime(tag1.getCurrentStatusAttributeTime(AppConstants.statusKeyFinished), tag2.getCurrentStatusAttributeTime(AppConstants.statusKeyFinished));
		}
	}
	
	protected int compareTime(double time1, double time2) {
		if (time1 == time2) {
			return 0;
		}
		else if (time1 == 0 && time2 > 0) {
			return 1;
		}
		else if (time1 > 0 && time2 == 0) {
			return -1;
		}
		else if (time1 > time2) {
			return 1;
		}
		else if (time2 > time1) {
			return -1;
		}
		return 0;
	}
	
	protected IXMLTag getChallengecontainerParentTag(IXMLTag microchallengeTag) {
		//NOTE only use challenge containers on top level because they represent mini games
		IXMLTag parentTag = microchallengeTag.getParentTag();
		while (parentTag != null) {
			if (!parentTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent).equals(AppConstants.statusValueTrue)) {
				return null;
			}
			if (parentTag.getParentTag().getName().equals(AppConstants.contentElement)) {
				return parentTag;
			}
			parentTag = parentTag.getParentTag();
		}
		return null;
	}
	
	protected IXMLTag getReferencedNodeTag(IECaseComponent caseComponent, IXMLTag nodeTag, String refChildTagName) {
		//get node tag referenced to by another node tag, e.g., method tag has a reference to a rubric tag
		if (nodeTag == null) {
			return null;
		}
		String referencetype = nodeTag.getChild(refChildTagName).getDefTag().getAttribute(AppConstants.defKeyReftype);
		List<IXMLTag> referenceTags = cCaseHelper.getRefTags(referencetype, "" + AppConstants.statusKeySelectedIndex, CDesktopComponents.sSpring().getSCaseRoleHelper().getCaseRole(), caseComponent, nodeTag);
		//NOTE ref should not be multiple
		if (referenceTags.size() == 1) {
			return referenceTags.get(0);
		}
		return null;
	}

	protected int[] calculateMaxScoreAndScorePerMicroChallenge(
			IXMLTag challengecontainerTag, 
			IXMLTag microchallengeTag, 
			double rubricNumberOfPerformanceLevels, 
			double rubricNumberOfSubskills, 
			StringBuffer microchallengesubskillsBuffer, 
			int microchallengeCounter) {
		int[] scoreData = new int[2];
		scoreData[0] = 0;
		scoreData[1] = 0;
		
		if (rubricNumberOfSubskills == 0) {
			return scoreData;
		}

		//NOTE microchallengeMaxScore is the max score for a micro challenge filled in using Gamebrics-author. It default is empty and calculated.
		int microchallengeMaxScore = 0;
		try {
			microchallengeMaxScore = Integer.parseInt(microchallengeTag.getChildValue("maxscore"));
		} catch (NumberFormatException e) {
		}
		boolean maxScoreIsFilledIn = microchallengeMaxScore > 0;
		double tmpMicrochallengeMaxScore = 0;

		//NOTE microchallengeGameScore is the score given to the micro challenge and it set by game script.
		//value 0 indicates that micro challenge is not completed yet
		//otherwise value is in interval 1..rubricNumberOfPerformanceLevels
		int microchallengeGameScore = 0; 
		try {
			microchallengeGameScore = Integer.parseInt(microchallengeTag.getCurrentStatusAttribute(AppConstants.statusKeyValue));
		} catch (NumberFormatException e) {
		}
		
		boolean microchallengeIsFinished = microchallengeGameScore > 0; 
		
		double totalScore = 0;
		
		//NOTE calculate max score depending on sub skills involved, their mutual weighting, and their weighting and maximum performance level within the micro challenge
		List<IXMLTag> microchallengesubskillTags = microchallengeTag.getChilds("microchallengesubskill");

		//NOTE calculate total micro challenge sub skill weighting
		Map<String,Double> microChallengeWeightingPerSubSkillId = new HashMap<String,Double>();
		double totalMicroChallengeWeighting = 0;
		for (IXMLTag xmlTag : microchallengesubskillTags) {
			double microchallengesubskillWeighting = 0;
			try {
				microchallengesubskillWeighting = Double.parseDouble(xmlTag.getChildValue("weighting"));
			} catch (NumberFormatException e) {
			}
			if (microchallengesubskillWeighting == 0 || microchallengesubskillWeighting == 1) {
				//NOTE String value in XML tag is either empty or 1 meaning no value is filled in by case developer
				//so calculate weighting depending on number of subskills (will be stored in totalMicroChallengeWeighting)
				microchallengesubskillWeighting = 1;
			}
			totalMicroChallengeWeighting += microchallengesubskillWeighting;
			microChallengeWeightingPerSubSkillId.put(xmlTag.getAttribute(AppConstants.keyId), microchallengesubskillWeighting);
		}
		
		for (IXMLTag xmlTag : microchallengesubskillTags) {
			//determine subskill weighting
			double performanceLevelScorePerSubSkill = 0;
			IXMLTag referencedSubskillTag = getReferencedNodeTag(challengesCaseComponent, xmlTag, "refsubskill");
			if (referencedSubskillTag != null) {
				try {
					performanceLevelScorePerSubSkill = performanceLevelScorePerSubSkillId.get(referencedSubskillTag.getAttribute(AppConstants.keyId));
				} catch (NumberFormatException e) {
				}
				microchallengeTagsPerSubskillId.get(referencedSubskillTag.getAttribute(AppConstants.keyId)).add(microchallengeTag);
				microchallengeNumbersPerSubskillId.get(referencedSubskillTag.getAttribute(AppConstants.keyId)).add(microchallengeCounter);
				//determine subskill maximum performance level
				double subskillMaxPerformanceLevel = rubricNumberOfPerformanceLevels;
				try {
					subskillMaxPerformanceLevel = Double.parseDouble(xmlTag.getChildValue("maxperformancelevel"));
					maxLevelPerMicrochallengeIdPerSubskillId.get(microchallengeTag.getAttribute(AppConstants.keyId)).put(referencedSubskillTag.getAttribute(AppConstants.keyId), subskillMaxPerformanceLevel);
				} catch (NumberFormatException e1) {
				}
				double tempMaxPerformanceLevelPerSubSkill = maxPerformanceLevelPerSubSkillId.get(referencedSubskillTag.getAttribute(AppConstants.keyId)).doubleValue();
				if (subskillMaxPerformanceLevel > tempMaxPerformanceLevelPerSubSkill) {
					maxPerformanceLevelPerSubSkillId.put(referencedSubskillTag.getAttribute(AppConstants.keyId), new Double(subskillMaxPerformanceLevel));
					if (microchallengeGameScore > 0) {
						maxPerformanceLevelsUntilNowPerSubSkillId.put(referencedSubskillTag.getAttribute(AppConstants.keyId), new Double(subskillMaxPerformanceLevel));
					}
				}
				//determine subskill weighting within micro challenge
				double microchallengesubskillWeighting = microChallengeWeightingPerSubSkillId.get(xmlTag.getAttribute(AppConstants.keyId)) / totalMicroChallengeWeighting;
				
				double maxScorePerSubSkill = 0;
				if (!maxScoreIsFilledIn) {
					maxScorePerSubSkill = subskillMaxPerformanceLevel * microchallengesubskillWeighting * performanceLevelScorePerSubSkill;
				}

				if (microchallengeIsFinished) {
					//TODO what if max score is filled in?
					double tempMaxScorePerSubSkill = maxScorePerSubSkillId.get(referencedSubskillTag.getAttribute(AppConstants.keyId)).doubleValue();
					tempMaxScorePerSubSkill += maxScorePerSubSkill; 
					maxScorePerSubSkillId.put(referencedSubskillTag.getAttribute(AppConstants.keyId), new Double(tempMaxScorePerSubSkill));
				}
				else {
					double tempMaxScoreNotPlayedPerSubSkill = maxScoreNotPlayedPerSubSkillId.get(referencedSubskillTag.getAttribute(AppConstants.keyId)).doubleValue();
					tempMaxScoreNotPlayedPerSubSkill += maxScorePerSubSkill; 
					maxScoreNotPlayedPerSubSkillId.put(referencedSubskillTag.getAttribute(AppConstants.keyId), new Double(tempMaxScoreNotPlayedPerSubSkill));
				}

				double scorePerSubSkill = (microchallengeGameScore / rubricNumberOfPerformanceLevels) * maxScorePerSubSkill;
				double tempScorePerSubSkill = scorePerSubSkillId.get(referencedSubskillTag.getAttribute(AppConstants.keyId)).doubleValue();
				tempScorePerSubSkill += scorePerSubSkill; 
				scorePerSubSkillId.put(referencedSubskillTag.getAttribute(AppConstants.keyId), new Double(tempScorePerSubSkill));
				totalScore += scorePerSubSkill;

				if (!maxScoreIsFilledIn) {
					tmpMicrochallengeMaxScore += maxScorePerSubSkill;
				}

				StringBuffer jsonBuffer = new StringBuffer();
				addJsonKeyValue(jsonBuffer, "tagId", referencedSubskillTag == null ? "0" : referencedSubskillTag.getAttribute(AppConstants.keyId), true);
				addJsonKeyValue(jsonBuffer, "maxScore", "" + (new Double(maxScorePerSubSkill)).intValue(), true);
				addJsonKeyValue(jsonBuffer, "score", "" + (new Double(scorePerSubSkill)).intValue(), true);
				String performance = "";
				if (maxScorePerSubSkill == 0 || (scorePerSubSkill / maxScorePerSubSkill) <= scoreLevelWeak) {
					performance = "weak";
				}
				else if ((scorePerSubSkill / maxScorePerSubSkill) <= scoreLevelReasonable) {
					performance = "reasonable";
				}
				else {
					performance = "good";
				}
				addJsonKeyValue(jsonBuffer, "performance", performance, false);
				if (microchallengesubskillsBuffer.length() > 0) {
					microchallengesubskillsBuffer.append(",");
				}
				microchallengesubskillsBuffer.append("{");
				microchallengesubskillsBuffer.append(jsonBuffer);
				microchallengesubskillsBuffer.append("}");
				
				double levelPerChallengeContainerIdPerSubskillId = levelPerChallengecontainerIdPerSubskillId.get(challengecontainerTag.getAttribute(AppConstants.keyId)).get(referencedSubskillTag.getAttribute(AppConstants.keyId));
				levelPerChallengeContainerIdPerSubskillId += new Double(microchallengeGameScore).doubleValue() * (subskillMaxPerformanceLevel / rubricNumberOfPerformanceLevels);
				levelPerChallengecontainerIdPerSubskillId.get(challengecontainerTag.getAttribute(AppConstants.keyId)).put(referencedSubskillTag.getAttribute(AppConstants.keyId), levelPerChallengeContainerIdPerSubskillId);
				
				double maxLevelPerChallengeContainerIdPerSubskillId = maxLevelPerChallengecontainerIdPerSubskillId.get(challengecontainerTag.getAttribute(AppConstants.keyId)).get(referencedSubskillTag.getAttribute(AppConstants.keyId));
				maxLevelPerChallengeContainerIdPerSubskillId += subskillMaxPerformanceLevel;
				maxLevelPerChallengecontainerIdPerSubskillId.get(challengecontainerTag.getAttribute(AppConstants.keyId)).put(referencedSubskillTag.getAttribute(AppConstants.keyId), maxLevelPerChallengeContainerIdPerSubskillId);
				
				double numberOfMicrochallengesPerChallengeContainerIdPerSubskillId = numberOfMicrochallengesPerChallengecontainterIdPerSubskillId.get(challengecontainerTag.getAttribute(AppConstants.keyId)).get(referencedSubskillTag.getAttribute(AppConstants.keyId));
				numberOfMicrochallengesPerChallengeContainerIdPerSubskillId++;
				numberOfMicrochallengesPerChallengecontainterIdPerSubskillId.get(challengecontainerTag.getAttribute(AppConstants.keyId)).put(referencedSubskillTag.getAttribute(AppConstants.keyId), numberOfMicrochallengesPerChallengeContainerIdPerSubskillId);
				
				if (referencedSubskillTag != null && scorePerSubSkill > 0) {
					double number = numberOfMicrochallengesPerSubskillId.get(referencedSubskillTag.getAttribute(AppConstants.keyId));
					number++;
					numberOfMicrochallengesPerSubskillId.put(referencedSubskillTag.getAttribute(AppConstants.keyId), number);
					if ((scorePerSubSkill / maxScorePerSubSkill) == 1) {
						double maxNumber = numberOfMaxScoredMicrochallengesPerSubskillId.get(referencedSubskillTag.getAttribute(AppConstants.keyId));
						maxNumber++;
						numberOfMaxScoredMicrochallengesPerSubskillId.put(referencedSubskillTag.getAttribute(AppConstants.keyId), maxNumber);
					}
				}
			}

		}

		if (!maxScoreIsFilledIn) {
			microchallengeMaxScore = (int)Math.round(tmpMicrochallengeMaxScore);
		}
		scoreData[0] = microchallengeMaxScore;
		scoreData[1] = (int)Math.round(totalScore);
		return scoreData;
	}
	
	protected void addJsonKeyValue(StringBuffer element, String key, String value, boolean separator) {
		element.append("\"");
		element.append(key);
		element.append("\":\"");
		element.append(value);
		element.append("\"");
		if (separator) {
			element.append(",");
		}
	}

	protected void addJsonKeyValue(StringBuffer element, String key, StringBuffer value, boolean separator) {
		element.append("\"");
		element.append(key);
		element.append("\":");
		element.append(value);
		if (separator) {
			element.append(",");
		}
	}

	protected StringBuffer getColorsAsString(String[] colorArr, int skillClusterNumberToFilter) {
		StringBuffer colors = new StringBuffer();
		colors.append("[");
		for (int i=0;i<colorArr.length;i++) {
			if (skillClusterNumberToFilter ==  0 || i == (skillClusterNumberToFilter - 1)) {
				colors.append("\""+ colorArr[i] + "\"");
			}
			else {
				colors.append("\""+ "transparent" + "\"");
			}
			if (i < (colorArr.length - 1)) {
				colors.append(",");
			}
		}
		colors.append("]");
		return colors;
	}
	
	protected StringBuffer getStringListAsString(List<String> stringList) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("[");
		for (int i=0;i<stringList.size();i++) {
			buffer.append("\""+ stringList.get(i) + "\"");
			if (i < (stringList.size() - 1)) {
				buffer.append(",");
			}
		}
		buffer.append("]");
		return buffer;
	}
	
	protected List<String> getPerformanceLevelColors(int momentIndex, int skillclusterCounter, IXMLTag subskillTag, boolean compareMoments, String skillWheelDivId) {
		String currentChallengecontainerId = challengeContainerNumberToId.get(momentIndex);
		double currentLevel = levelPerChallengecontainerIdPerSubskillId.get(currentChallengecontainerId).get(subskillTag.getAttribute(AppConstants.keyId));
		if (currentLevel > 0 && currentLevel < 1) {
			currentLevel = 1;
		}
		int intCurrentLevel = new Long(Math.round(currentLevel)).intValue();

		double currentMaxLevel = maxLevelPerChallengecontainerIdPerSubskillId.get(currentChallengecontainerId).get(subskillTag.getAttribute(AppConstants.keyId));
		if (currentMaxLevel > 0 && currentMaxLevel < 1) {
			currentMaxLevel = 1;
		}
		int intCurrentMaxLevel = new Long(Math.round(currentMaxLevel)).intValue();

		double maxLevelOverall = maxPerformanceLevelPerSubSkillId.get(subskillTag.getAttribute(AppConstants.keyId));
		int intMaxLevelOverall = new Long(Math.round(maxLevelOverall)).intValue();

		int intReachableMaxLevelInPreviousMoments = 0;
		int intMaxLevelInPreviousMoments = 0;
		int intLastLevelInPreviousMoments = 0;
		for (int i=0;i<momentIndex;i++) {
			String previousChallengecontainerId = challengeContainerNumberToId.get(i);
			double previousMaxLevel = maxLevelPerChallengecontainerIdPerSubskillId.get(previousChallengecontainerId).get(subskillTag.getAttribute(AppConstants.keyId));
			if (previousMaxLevel > 0 && previousMaxLevel < 1) {
				previousMaxLevel = 1;
			}
			int intPreviousMaxLevel = new Long(Math.round(previousMaxLevel)).intValue();
			if (intPreviousMaxLevel > intReachableMaxLevelInPreviousMoments) {
				intReachableMaxLevelInPreviousMoments = intPreviousMaxLevel; 
			}
			double previousLevel = levelPerChallengecontainerIdPerSubskillId.get(previousChallengecontainerId).get(subskillTag.getAttribute(AppConstants.keyId));
			if (previousLevel > 0 && previousLevel < 1) {
				previousLevel = 1;
			}
			int intPreviousLevel = new Long(Math.round(previousLevel)).intValue();
			if (intPreviousLevel > intMaxLevelInPreviousMoments) {
				intMaxLevelInPreviousMoments = intPreviousLevel; 
			}
			if (intPreviousLevel > 0) {
				intLastLevelInPreviousMoments = intPreviousLevel; 
			}
		}

		int intReachableMaxLevelInNextMoments = 0;
		for (int i=momentIndex+1;i<maxLevelPerChallengecontainerIdPerSubskillId.size();i++) {
			String nextChallengecontainerId = challengeContainerNumberToId.get(i);
			double nextMaxLevel = 0;
			if (maxLevelPerChallengecontainerIdPerSubskillId.containsKey(nextChallengecontainerId) &&
					maxLevelPerChallengecontainerIdPerSubskillId.get(nextChallengecontainerId).containsKey(subskillTag.getAttribute(AppConstants.keyId))) {
				nextMaxLevel = maxLevelPerChallengecontainerIdPerSubskillId.get(nextChallengecontainerId).get(subskillTag.getAttribute(AppConstants.keyId));
			}
			if (nextMaxLevel > 0 && nextMaxLevel < 1) {
				nextMaxLevel = 1;
			}
			int intNextMaxLevel = new Long(Math.round(nextMaxLevel)).intValue();
			if (intNextMaxLevel > intReachableMaxLevelInNextMoments) {
				intReachableMaxLevelInNextMoments = intNextMaxLevel; 
			}
		}

		int intPreviousLevel = 0;
		int intPreviousMaxLevel = 0;
		if (compareMoments) {
			if (momentIndex - momentDuoDiff >= 0) {
				String previousChallengecontainerId = challengeContainerNumberToId.get(momentIndex - momentDuoDiff);
				double previousLevel = levelPerChallengecontainerIdPerSubskillId.get(previousChallengecontainerId).get(subskillTag.getAttribute(AppConstants.keyId));
				if (previousLevel > 0 && previousLevel < 1) {
					previousLevel = 1;
				}
				intPreviousLevel = new Long(Math.round(previousLevel)).intValue();

				double previousMaxLevel = maxLevelPerChallengecontainerIdPerSubskillId.get(previousChallengecontainerId).get(subskillTag.getAttribute(AppConstants.keyId));
				if (previousMaxLevel > 0 && previousMaxLevel < 1) {
					previousMaxLevel = 1;
				}
				intPreviousMaxLevel = new Long(Math.round(previousMaxLevel)).intValue();
			}
		}
		
		//NOTE if level is not scored in current moment use last level in previous moments
		//Although the level is not scored in the current moment the student may have accomplished a certain level before
		if (intCurrentLevel == 0) {
			intCurrentLevel = intLastLevelInPreviousMoments;
		}
		//NOTE if level is not scored in previous moment use last level in previous moments
		//Although the level is not scored in the previous moment the student may have accomplished a certain level before
		if (intPreviousLevel == 0) {
			intPreviousLevel = intLastLevelInPreviousMoments;
		}
		
		List<String> performanceLevelStates = new ArrayList<String>();
		for (int i=1;i<=rubricNumberOfPerformanceLevels;i++) {
			if (i > intMaxLevelOverall) {
				performanceLevelStates.add(performanceLevelNotAbleToScoreState);
			}
			else if (intCurrentLevel == 0) {
				if (i > intReachableMaxLevelInPreviousMoments) {
					performanceLevelStates.add(performanceLevelNotYetAddressedState);
				}
				else {
					performanceLevelStates.add(performanceLevelNoScoreState);
				}
			}
			else if (!compareMoments) {
				if (i <= intCurrentLevel) {
					if (skillWheelDivId.equals("GB_skill_wheel_figure_small_right")) {
						if (intMaxLevelInPreviousMoments == 0 || (intPreviousLevel > 0 && i > intPreviousLevel)) {
							performanceLevelStates.add(performanceLevelImprovedState);
						}
						else {
							performanceLevelStates.add(performanceLevelAchievedState);
						}
					}
					else {
						performanceLevelStates.add(performanceLevelAchievedState);
					}
				}
				else if (i <= intCurrentMaxLevel || (intReachableMaxLevelInNextMoments > 0 && i <= intReachableMaxLevelInNextMoments)) {
					performanceLevelStates.add(performanceLevelNoScoreState);
				}
				else {
					performanceLevelStates.add(performanceLevelNoImprovementPossibleState);
				}
			}
			else {
				if (i <= intCurrentLevel) {
					if (intMaxLevelInPreviousMoments == 0 || (intPreviousLevel > 0 && i > intPreviousLevel)) {
						performanceLevelStates.add(performanceLevelImprovedState);
					}
					else {
						performanceLevelStates.add(performanceLevelAchievedState);
					}
				}
				else {
					if (intPreviousLevel > 0 && i <= intPreviousLevel) {
						if (i <= intCurrentMaxLevel || (intReachableMaxLevelInNextMoments > 0 && i <= intReachableMaxLevelInNextMoments)) {
							performanceLevelStates.add(performanceLevelDeterioratedState);
						}
						else {
							performanceLevelStates.add(performanceLevelDeterioratedWithoutBlameState);
						}
					}
					else if (i <= intCurrentMaxLevel || (intReachableMaxLevelInNextMoments > 0 && i <= intReachableMaxLevelInNextMoments)) {
						performanceLevelStates.add(performanceLevelNoScoreState);
					}
					else {
						performanceLevelStates.add(performanceLevelNoImprovementPossibleState);
					}
				}
			}
		}
		return getPerformanceLevelColors(performanceLevelStates, skillclusterCounter);
	}

	protected List<String> getPerformanceLevelColors(List<String> performanceLevelStates, int skillclusterCounter) {
		List<String> performanceLevelColors = new ArrayList<String>();
		for (String performanceLevelState : performanceLevelStates) {
			if (performanceLevelState.equals(performanceLevelNotAbleToScoreState)) {
				performanceLevelColors.add(performanceLevelNotAbleToScoreColor);
			}
			else if (performanceLevelState.equals(performanceLevelNotYetAddressedState)) {
				performanceLevelColors.add(performanceLevelNotYetAddressedColor);
			}
			else if (performanceLevelState.equals(performanceLevelAchievedState)) {
				performanceLevelColors.add(performanceLevelAchievedColor);
			}
			else if (performanceLevelState.equals(performanceLevelImprovedState)) {
				performanceLevelColors.add(performanceLevelImprovedColor);
			}
			else if (performanceLevelState.equals(performanceLevelDeterioratedState)) {
				performanceLevelColors.add(performanceLevelDeterioratedColor);
			}
			else if (performanceLevelState.equals(performanceLevelDeterioratedWithoutBlameState)) {
				//NOTE simplification of wheel
//				performanceLevelColors.add(performanceLevelDeterioratedWithoutBlameColor);
				performanceLevelColors.add(performanceLevelNotYetAddressedColor);
			}
			else if (performanceLevelState.equals(performanceLevelImprovementPossibleState)) {
				performanceLevelColors.add(performanceLevelImprovementPossibleColor);
			}
			else if (performanceLevelState.equals(performanceLevelNoImprovementPossibleState)) {
				//NOTE simplification of wheel
//				performanceLevelColors.add(performanceLevelNoImprovementPossibleColor);
				performanceLevelColors.add(performanceLevelNotYetAddressedColor);
			}
			else if (performanceLevelState.equals(performanceLevelNoScoreState)) {
				if (numberOfSkillClusters > 0) {
					performanceLevelColors.add(CRunPVToolkit.getPerformanceLevelSkillClusterColorsProp()[skillclusterCounter]);
				}
				else {
					performanceLevelColors.add(performanceLevelSubSkillColor);
				}
			}
		}
		return performanceLevelColors;
	}
	
	protected String getRootText(double percentage) {
		for (IXMLTag roottextTag : roottextTags) {
			double minpercentage = 0;
			double maxpercentage = 0;
			try {
				minpercentage = Double.parseDouble(roottextTag.getChildValue("minpercentage"));
			} catch (NumberFormatException e) {
			}
			try {
				maxpercentage = Double.parseDouble(roottextTag.getChildValue("maxpercentage"));
			} catch (NumberFormatException e) {
			}
			minpercentage = minpercentage / 100;
			maxpercentage = maxpercentage / 100;
			boolean found = false;
			if (percentage == 0 && minpercentage == 0 && maxpercentage == 0) {
				found = true;
			}
			else if (percentage >= minpercentage && percentage < maxpercentage) {
				found = true;
			}
			else if (percentage == 1 && minpercentage == 1 && maxpercentage == 1) {
				found = true;
			}
			if (found) {
				return sSpring.unescapeXML(roottextTag.getChildValue("text"));
			}
		}
		return "";
	}

	protected String getFeedbackText(double percentage, IXMLTag subskillTag, int counter) {
		if (percentage < 1) {
			int maxPerformancelevel = maxPerformanceLevelPerSubSkillId.get(subskillTag.getAttribute(AppConstants.keyId)).intValue();
			int level = maxPerformancelevel;
			if (level >= rubricNumberOfPerformanceLevels) {
				level = rubricNumberOfPerformanceLevels - 1;
			}
			for (IXMLTag feedbacktextTag : feedbacktextTags) {
				//TODO better solution to get correct feedback text
				String[] pidArr = sSpring.unescapeXML(feedbacktextTag.getChildValue("pid")).split("_");
				if (pidArr[1].equals("" + (counter + 1)) && pidArr[2].equals("" + (level + 1))) {
					return sSpring.unescapeXML(feedbacktextTag.getChildValue("text"));
				}
			}
		}
		return "";
	}

	public void onShowSkillWheel(Event event) {
		if (momentIndex < 0) {
			//No moment yet
			return;
		}
		if (momentIndex - momentDuoDiff >= 0) {
			showSkillWheel(momentIndex - momentDuoDiff, false, "GB_skill_wheel_figure_small_left", getUuid(), getUuid() + "SmallLeft", 0.41);
			showSkillWheel(momentIndex, false, "GB_skill_wheel_figure_small_right", getUuid(), getUuid() + "SmallRight", 0.41);
		}
		vView.getComponent("GB_skill_wheel_figure_small_left").setVisible(momentIndex - momentDuoDiff >= 0);
		vView.getComponent("GB_skill_wheel_figure_small_right").setVisible(momentIndex - momentDuoDiff >= 0);
		showSkillWheel(momentIndex, true, "GB_skill_wheel_figure", getUuid(), getUuid(), 1.85);

	}

	public void showSkillWheel(int momentIndex, boolean compareMoments, String skillWheelDivId, String skillWheelParentDivUuid, String skillWheelUniquePostfix, double zoomFactor) {
		Map<String,Object> propertyMap = new HashMap<String,Object>();
		propertyMap.put("zulfilepath", zulfilepath);

		//number of skill clusters
		int skillclusterCounter = 0;
		int skillClusterNumberToFilter = 0;
		//number of sub skills per skill cluster
		List<Integer> subskillCounters = new ArrayList<Integer>();
		//segments = sub skills
		List<Integer> subskillSegments = new ArrayList<Integer>();
		
		List<String> skillclusterNames = new ArrayList<String>();
		List<String> skillclusterAccessibles = new ArrayList<String>();

		List<String> subskillNames = new ArrayList<String>();;
		List<String> subskillAccessibles = new ArrayList<String>();;
		List<List<List<String>>> performanceColorsPerSkillClusterPerSubSkill = new ArrayList<List<List<String>>>();

		List<IXMLTag> parentTags = new ArrayList<IXMLTag>();
		//NOTE a rubric contains either skill clusters, directly only the skill tag, and sub skills within them or only sub skills directly under the skill tag. A mixture is not allowed.
		if (numberOfSkillClusters > 0) {
			parentTags = skillTag.getChilds("skillcluster");
		}
		else {
			parentTags.add(skillTag);
		}
			
		for (IXMLTag parentTag : parentTags) {
			if (!parentTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse)) {
				if (parentTag.getName().equals("skillcluster")) {
					skillclusterNames.add(sSpring.unescapeXML(parentTag.getChildValue("name")));
					skillclusterAccessibles.add("" + !parentTag.getCurrentStatusAttribute(AppConstants.statusKeyAccessible).equals(AppConstants.statusValueFalse));
				}
				List<List<String>> subSkillPerformanceColorsPerSkillCluster = new ArrayList<List<String>>();
				performanceColorsPerSkillClusterPerSubSkill.add(subSkillPerformanceColorsPerSkillCluster);
				//number of sub skills per skill cluster
				int subskillCounter = 0;
				for (IXMLTag subskillTag : parentTag.getChilds("subskill")) {
					if (!subskillTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse)) {
						subskillNames.add(sSpring.unescapeXML(subskillTag.getChildValue("name")));
						subskillAccessibles.add("" + !subskillTag.getCurrentStatusAttribute(AppConstants.statusKeyAccessible).equals(AppConstants.statusValueFalse));
						subskillSegments.add(1);
						List<String> subSkillPerformanceColorsPerSubSkill = new ArrayList<String>();
						subSkillPerformanceColorsPerSkillCluster.add(subSkillPerformanceColorsPerSubSkill);
						List<String> performanceLevelColors;
						performanceLevelColors = getPerformanceLevelColors(momentIndex, skillclusterCounter, subskillTag, compareMoments, skillWheelDivId);
						subSkillPerformanceColorsPerSubSkill.addAll(performanceLevelColors);
						subskillCounter++;
					}
				}
				subskillCounters.add(subskillCounter);
				if (parentTag.getName().equals("skillcluster")) {
					skillclusterCounter++;
				}
			}
		}

		StringBuffer PL_colors = new StringBuffer();
		PL_colors.append("[");
		for (int i=0;i<rubricNumberOfPerformanceLevels;i++) {
			PL_colors.append("[");
			int number = 0;
			for (int j=0;j<performanceColorsPerSkillClusterPerSubSkill.size();j++) {
				for (int k=0;k<subskillCounters.get(j);k++) {
					if (skillClusterNumberToFilter ==  0 || j == (skillClusterNumberToFilter - 1)) {
						PL_colors.append("\"" + performanceColorsPerSkillClusterPerSubSkill.get(j).get(k).get(i) + "\"");
					}
					else {
						PL_colors.append("\"" + "transparent" + "\"");
					}
					if (number < (subskillSegments.size() - 1)) {
						PL_colors.append(",");
					}
					number ++;
				}
			}
			PL_colors.append("]");
			if (i < (rubricNumberOfPerformanceLevels - 1)) {
				PL_colors.append(",");
			}
		}
		PL_colors.append("]");

		propertyMap.put("SC_names", getStringListAsString(skillclusterNames).toString());
		propertyMap.put("SC_accessibles", getStringListAsString(skillclusterAccessibles).toString());
		propertyMap.put("SS_names", getStringListAsString(subskillNames).toString());
		propertyMap.put("SS_accessibles", getStringListAsString(subskillAccessibles).toString());
		List<String> temp = new ArrayList<String>();
		temp.add(CDesktopComponents.vView().getLabel("PV-toolkit-skillwheel.label.subskill.inaccessible"));
		propertyMap.put("SS_inaccessibleText", getStringListAsString(temp));
		temp = new ArrayList<String>();
		temp.add(CDesktopComponents.vView().getLabel("PV-toolkit-skillwheel.label.subskill.accessible"));
		propertyMap.put("SS_accessibleText", getStringListAsString(temp));
		
		propertyMap.put("SC_colors", getColorsAsString(CRunPVToolkit.getSkillClusterColorsProp(), skillClusterNumberToFilter).toString());
		propertyMap.put("SC_colors_pastel", getColorsAsString(CRunPVToolkit.getPerformanceLevelSkillClusterColorsProp(), skillClusterNumberToFilter).toString());
		
		propertyMap.put("SC_number", skillclusterCounter);
		propertyMap.put("SS_numbers", subskillCounters);
		//number of performance levels
		propertyMap.put("PL_number", rubricNumberOfPerformanceLevels);

		propertyMap.put("PL_segments", subskillSegments);
		
		propertyMap.put("PL_colors", PL_colors.toString());

		propertyMap.put("skill_wheel_div_uuid", skillWheelParentDivUuid);
		propertyMap.put("skill_wheel_unique_postfix", skillWheelUniquePostfix);
		propertyMap.put("number_of_levels", rubricNumberOfPerformanceLevels);
		propertyMap.put("presentation_level", 2);
		propertyMap.put("zoom_factor", zoomFactor);
		propertyMap.put("rotation_degree", 0);

		propertyMap.put("segmentOffset", 15);
		propertyMap.put("segmentPadding", 0);
		propertyMap.put("padAngle", 0.0);
		propertyMap.put("frameColor", "#707070");
		propertyMap.put("SC_hide", true);
		propertyMap.put("SS_PLs", new ArrayList<Integer>());

		Div skillWheelDiv = (Div)vView.getComponent(skillWheelDivId);
		skillWheelDiv.getChildren().clear();

		HtmlMacroComponent macro = new HtmlMacroComponent();
		skillWheelDiv.appendChild(macro);
		macro.setDynamicProperty("a_propertyMap", propertyMap);
		macro.setMacroURI(zulfilepath + "run_dashboard-skill-wheel.zul");

		Clients.evalJavaScript("renderSkillWheel" + skillWheelUniquePostfix + "();");
	}

	public void onNotify(Event event) {
		JSONObject jsonObject = (JSONObject)event.getData();
		String action = (String)jsonObject.get("action");
		if (action.equals("set_moment_index_and_duo_diff")) {
			momentIndex = Integer.parseInt((String)jsonObject.get("momentIndex"));
			momentDuoDiff = Integer.parseInt((String)jsonObject.get("momentDuoDiff"));

			Events.echoEvent("onShowSkillWheel", this, null);
		}
		if (action.equals("close_dashboard")) {
			sSpring.setRunTagStatus(currentCaseComponent, currentTag, AppConstants.statusKeyPresent, AppConstants.statusValueFalse, null, true, AppConstants.statusTypeRunGroup, true, false, true);
		}
	}
	
}
