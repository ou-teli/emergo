/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Event;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;

public class CRunAssessmentsPanelDiv extends CDefDiv {

	private static final long serialVersionUID = 3654358501211162825L;
	
	protected CRunAssessmentsContentDiv root;

	public void onInit(Event aEvent) {
		root = (CRunAssessmentsContentDiv)CDesktopComponents.vView().getComponent(CRunAssessmentsContentDiv.runAssessmentsZulId);
		HtmlMacroComponent macro = new HtmlMacroComponent();
		appendChild(macro);
		macro.setId(CRunAssessmentsContentDiv.macroAssessmentsId);
		macro.setDynamicProperty("a_on_tablet", root.onTablet);
		macro.setDynamicProperty("a_observer", CRunAssessmentsContentDiv.runAssessmentsDivId);
		macro.setDynamicProperty("a_event", "onChooseAssessment");
		macro.setDynamicProperty("a_assessments_title", root.assessmentsTitle);
		macro.setDynamicProperty("a_assessments_instruction", root.assessmentsInstruction);
		macro.setDynamicProperty("a_assessments_datas", root.assessmentsDatas);
		macro.setMacroURI("../run_assessmentspanel_macro.zul");

		setVisible(root.multipleAssessments);
	}
	
	public void onChooseAssessment(Event aEvent) {
		if (root.currentEmergoComponent != null) {
			String assessmentTagId = (String)((Object[])aEvent.getData())[0];
			String assessmentItemTagId = (String)((Object[])aEvent.getData())[1];
			if (assessmentItemTagId != null && !assessmentItemTagId.equals("")) {
				root.multipleQuestions = false;
				root.currentEmergoComponent.prepairAssessmentItem(assessmentTagId);
				root.currentEmergoComponent.handleAssessmentItem(assessmentItemTagId);
			}
			else {
				root.multipleQuestions = true;
				root.currentEmergoComponent.handleAssessment(assessmentTagId);
			}
		}
		setVisible(false);
	}
	
}
