/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import org.zkoss.zk.ui.Component;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.CRunCloseBtn;

/**
 * The Class CRunConversationInteraction is used to ask questions during conversations
 * and is shown within the run choice area.
 */
public class CRunConversationInteractionClassic extends CRunVboxClassic {

	private static final long serialVersionUID = -8421718350456679237L;

	/** The run component, the conversations component. */
	protected CRunComponentClassic runComponent = null;

	/** The are questions shown. */
	public boolean areQuestionsShown = false;

	/**
	 * Instantiates a new c run conversation interaction.
	 * Creates button area with close and ask question button.
	 * And creates content area.
	 * 
	 * @param aId the a id
	 * @param aRunComponent the a run component
	 */
	public CRunConversationInteractionClassic(String aId, CRunComponentClassic aRunComponent) {
		super();
		setId(aId);
		runComponent = aRunComponent;
		String lType = "Min";
		if (runWnd.runChoiceAreaMed)
			lType = "Med";
		if (runWnd.runChoiceAreaMax)
			lType = "Max";
		setZclass(className + lType);

		createButtonsArea(this);
		createContentArea(this);
	}

	/**
	 * Creates button area with close and ask question button.
	 * 
	 * @param aParent the a parent
	 */
	protected void createButtonsArea(Component aParent) {
		CRunAreaClassic lButtonsArea = new CRunAreaClassic();
		CRunHboxClassic lHbox = new CRunHboxClassic();
		createCloseButton(lHbox);
		createAskQuestionButton(lHbox);
		lButtonsArea.appendChild(lHbox);
		aParent.appendChild(lButtonsArea);
	}

	/**
	 * Creates close button.
	 *
	 * @param aParent the a parent
	 */
//	protected CRunConversationBtn createCloseButton(Component aParent) {
//		CRunConversationBtn lButton = new CRunConversationBtn("runConversationEndBtn",
//				"active","endComponent", runComponent,
//				CDesktopComponents.vView().getLabel("run_conversations.button.endconversation.prefix"), "");
	protected CRunCloseBtn createCloseButton(Component aParent) {
		CRunCloseBtn lButton = new CRunCloseBtn("runConversationEndBtn",
				"active","endComponent", runComponent,
				CDesktopComponents.vView().getLabel("run_conversations.button.endconversation.prefix"), "");
		lButton.setTooltiptext(CDesktopComponents.vView().getLabel("run_conversations.button.endconversation.tooltip"));
//		set to false, so button can be clicked multiple times
		lButton.setCanHaveStatusSelected(false);
		lButton.registerObserver("runConversations");
		lButton.registerObserver(CControl.runWnd);
		aParent.appendChild(lButton);
		return lButton;
	}

	/**
	 * Creates ask question button.
	 *
	 * @param aParent the a parent
	 */
//	protected CRunConversationBtn createAskQuestionButton(Component aParent) {
//		CRunConversationBtn lButton = new CRunConversationBtn("runConversationAskQuestionBtn",
//				"active","askQuestion", "",
//				CDesktopComponents.vView().getLabel("run_conversations.button.askquestion.prefix"), "");
	protected CRunCloseBtn createAskQuestionButton(Component aParent) {
		CRunCloseBtn lButton = new CRunCloseBtn ("runConversationAskQuestionBtn",
				"active","askQuestion", "",
				CDesktopComponents.vView().getLabel("run_conversations.button.askquestion.prefix"), "");
//		set to false, so button can be clicked multiple times
		lButton.setCanHaveStatusSelected(false);
		lButton.registerObserver("runConversations");
		aParent.appendChild(lButton);
		return lButton;
	}

	/**
	 * Creates content area and questions tree within it.
	 * 
	 * @param aParent the a parent
	 * 
	 * @return the c run area
	 */
	protected CRunAreaClassic createContentArea(Component aParent) {
		String lType = "Min";
		if (runWnd.runChoiceAreaMed)
			lType = "Med";
		if (runWnd.runChoiceAreaMax)
			lType = "Max";
		CRunAreaClassic lContentArea = new CRunAreaClassic();
		lContentArea.setZclass(className + lType + "_content_area");
		CRunQuestionsTreeClassic lQuestions = new CRunQuestionsTreeClassic("runQuestionsTree", runComponent);
		lQuestions.setSclass(className + lType + "_content");
		lContentArea.appendChild(lQuestions);
		aParent.appendChild(lContentArea);
		return lContentArea;
	}

	/**
	 * Sets the status. Either the close button and the questions tree or
	 * the close button and the ask questions button.
	 * 
	 * @param aStatus the new status
	 */
	public void setStatus(String aStatus) {
		CRunQuestionsTreeClassic lQuestions = (CRunQuestionsTreeClassic) CDesktopComponents.vView().getComponent("runQuestionsTree");
		boolean lStatusIsConversation = aStatus.equals("conversation");
		// if questions are shown, don't hide them anymore and don't show ask questions button anymore
		if (lStatusIsConversation)
			areQuestionsShown = lStatusIsConversation;
		if (lQuestions != null) {
			if (areQuestionsShown) {
				if (!lQuestions.isVisible())
					lQuestions.setVisible(true);
			}
			else {
				if (lQuestions.isVisible())
					lQuestions.setVisible(false);
			}
		}
		if (areQuestionsShown) {
//			CRunConversationBtn lButton = (CRunConversationBtn) CDesktopComponents.vView().getComponent("runConversationAskQuestionBtn");
			CRunCloseBtn lButton = (CRunCloseBtn) CDesktopComponents.vView().getComponent("runConversationAskQuestionBtn");
			if (lButton != null)
				lButton.setVisible(false);
		}
	}

	/**
	 * Sets the buttons status depending on if there are questions and if so, if they are shown.
	 * 
	 * @param aAreQuestions the a are questions status
	 */
	public void setButtonsStatus(boolean aAreQuestions) {
//		CRunConversationBtn lButton = (CRunConversationBtn) CDesktopComponents.vView().getComponent("runConversationAskQuestionBtn");
		CRunCloseBtn lButton = (CRunCloseBtn) CDesktopComponents.vView().getComponent("runConversationAskQuestionBtn");
		// if questions are shown, don't show ask questions button anymore
		if (lButton != null)
			lButton.setVisible((aAreQuestions) && (!areQuestionsShown));
	}

	/**
	 * Sets the buttons action depending on aFileExt. If media player is used client action is set to
	 * stop media player playing if one of the buttons is clicked.
	 * 
	 * @param aFileExt the new btns action
	 */
	public void setButtonsAction(String aFileExt) {
		String lAction = "";
		if ((aFileExt.equals("wmv")) || (aFileExt.equals("mpg")))
			lAction = runWnd.getMediaplayerStopAction();
//		CRunConversationBtn lButton = (CRunConversationBtn) CDesktopComponents.vView().getComponent("runConversationEndBtn");
		CRunCloseBtn lButton = (CRunCloseBtn) CDesktopComponents.vView().getComponent("runConversationEndBtn");
		if (lButton != null)
//			lButton.setAction(lAction);
			lButton.setWidgetListener("onClick", lAction);
//		lButton = (CRunConversationBtn) CDesktopComponents.vView().getComponent("runConversationAskQuestionBtn");
		lButton = (CRunCloseBtn) CDesktopComponents.vView().getComponent("runConversationAskQuestionBtn");
		if (lButton != null)
//			lButton.setAction(lAction);
			lButton.setWidgetListener("onClick", lAction);
	}

	/**
	 * Re-renders tree items if necessary.
	 * 
	 * @param aTag the changed element
	 */
	public void update(IXMLTag aTag) {
		CRunQuestionsTreeClassic lQuestions = (CRunQuestionsTreeClassic) CDesktopComponents.vView().getComponent("runQuestionsTree");
		if (areQuestionsShown)
			lQuestions.update(aTag);
	}

}
