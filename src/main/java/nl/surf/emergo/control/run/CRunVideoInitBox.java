/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.CreateEvent;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.utilities.PropsValues;
import nl.surf.emergo.view.VView;

public class CRunVideoInitBox extends CRunStreamingMediaInitBox {

	private static final long serialVersionUID = -1255294986231102261L;

	protected Desktop getMDesktop() {
		Desktop desktop = null;
		if (Executions.getCurrent() != null)
			desktop = Executions.getCurrent().getDesktop();
		return desktop;
	}

	public void onCreate(CreateEvent aEvent) {
		/*
		 * if (getDesktop() != null) //if (Events.isListened(this, "onInit", false))
		 * Events.postEvent("onInit", this, null); else Events.postEvent("onCreate",
		 * this, null); }
		 * 
		 * public void onInit(Event aEvent) {
		 */
		init();

		CDesktopComponents.sSpring().initDesktopAttributes(getMDesktop());

		String runComponentId = (String) VView.getParameter("runComponentId", this, "runConversations");

		// NOTE get position relative to position of parent CRunConversations
		// RunConversations (absolute) position is set in CRunConversations class, so we
		// don't need to use
		// video_position_left and video_position_top here
		String left = (String) VView.getParameter("fragment_position_left", this, "0px");
		String top = (String) VView.getParameter("fragment_position_top", this, "0px");
		int lL = Integer.parseInt(left.substring(0, left.length() - 2));
		int lT = Integer.parseInt(top.substring(0, top.length() - 2));

		// NOTE fragment size may overwrite size of parent CRunConversations
		// NOTE if size neither set in conversation nor in fragment, make size equal to
		// runWindow size

		String width = (String) VView.getParameter("fragment_size_width", this, (String) VView
				.getParameter("video_size_width", this, (String) getMDesktop().getAttribute("runWndWidth")));
		String height = (String) VView.getParameter("fragment_size_height", this, (String) VView
				.getParameter("video_size_height", this, (String) getMDesktop().getAttribute("runWndHeight")));
		// NOTE fragment_size_width and fragment_size_height should end with 'px'
		int lH = Integer.parseInt(height.substring(0, height.length() - 2));
		int lW = Integer.parseInt(width.substring(0, width.length() - 2));

		// NOTE cover and video player are placed in 'absolute' position, on same level
		String coverTop = top;

		// NOTE position of start video button; relative to position beneath container
		// cover area; button image is 50x35 px
		String startBtnLeft = "" + (lL + lW / 2 - 25) + "px";
		// String startBtnTop = "" + (lT - 3 * lH / 2 - 18) + "px";

		// NOTE start button is placed in 'absolute' position, on same level as video
		// player and cover
		String startBtnTop = "" + (lH / 2 - 18) + "px";

		// NOTE for jw player image may not be null or empty string, so only add image
		// line if not empty.
		// If image is null or empty string jw player reloads run.zul!!!!!
		String image = (String) VView.getParameter("image", this, "");
		String imageLine = "";
		if (!image.equals("")) {
			imageLine = "image: \"" + image + "\",";
		}

		String videooverlay = (String) VView.getParameter("video_overlay", this, null);
		String videooverlaystyle = "";
		if (videooverlay != null && !videooverlay.equals("")) {
			videooverlaystyle = "background-image:url('" + videooverlay + "');background-repeat:no-repeat;";
		}

		HtmlMacroComponent macro = new HtmlMacroComponent();
		appendChild(macro);
		macro.setId("playerMacro_" + runComponentId);
		macro.setDynamicProperty("a_emergoRootUrl", vView.getAbsoluteWebappsRoot());
		macro.setDynamicProperty("a_jwplayerKey", PropsValues.VIDEO_JWPLAYER_KEY);
		macro.setDynamicProperty("a_playerName", VView.getParameter("playerName", this, "runFlash_" + runComponentId));
		macro.setDynamicProperty("a_left", left);
		macro.setDynamicProperty("a_top", top);
		macro.setDynamicProperty("a_width", width);
		macro.setDynamicProperty("a_height", height);
		macro.setDynamicProperty("a_coverTop", coverTop);
		macro.setDynamicProperty("a_videooverlaystyle", videooverlaystyle);
		macro.setDynamicProperty("a_startBtnLeft", startBtnLeft);
		macro.setDynamicProperty("a_startBtnTop", startBtnTop);
		macro.setDynamicProperty("a_playerControls", VView.getParameter("showControls", this, "false"));
		macro.setDynamicProperty("a_runComponentId", runComponentId);
		macro.setDynamicProperty("a_playerAutostart", VView.getParameter("playerAutostart", this, "true"));
		macro.setDynamicProperty("a_playerStretching", VView.getParameter("playerStretching", this, "uniform"));
		macro.setDynamicProperty("a_streamingUrls", streamingurls);
		// NOTE url may contain back slashes. Replace by slashes
		url = url.replaceAll(PropsValues.STREAMING_PLAYER_PATH_SEPARATOR, "/");
		macro.setDynamicProperty("a_url", url);
		macro.setDynamicProperty("a_imageLine", imageLine);
		macro.setMacroURI("../run_" + PropsValues.VIDEO_PLAYER_ID + "_view_fr_macro.zul");
	}

}
