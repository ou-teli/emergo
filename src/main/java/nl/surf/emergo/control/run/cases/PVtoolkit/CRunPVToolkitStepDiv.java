/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit;

import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Button;
import org.zkoss.zul.Caption;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Html;
import org.zkoss.zul.Label;
import org.zkoss.zul.Popup;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefHbox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTag;

public class CRunPVToolkitStepDiv extends CDefDiv {

	private static final long serialVersionUID = 3228925858970198586L;

	private CRunPVToolkit pvToolkit;

	public void onCreate() {
		pvToolkit = (CRunPVToolkit)CDesktopComponents.vView().getComponent("pvToolkit");

		setClass("stepDiv");
	}

	public void onUpdate(Event event) {
		getChildren().clear();
		
		Map<String,Object> keyValueMap = (Map<String,Object>)event.getData();
		String zulfilepath = ((CRunPVToolkitInitBox)CDesktopComponents.vView().getComponent("PV-toolkit_initBox")).getZulfilepath();
		
		Hbox hbox = new CRunPVToolkitDefHbox(this, null, null);
		
		String stepLabelClass = "stepLabel1";
		if (pvToolkit.getMaxCycleNumber() > 1) {
			new CRunPVToolkitDefLabel(hbox, 
					new String[]{"class", "cLabelKey"}, 
					new Object[]{"font stepLabel1", "PV-toolkit.cycle"}
					);
			new CRunPVToolkitDefImage(hbox, 
					new String[]{"class", "src"}, 
					new Object[]{"stepIcon", zulfilepath + "number_" + (int)keyValueMap.get("cycleNumber") + "_gray.svg"}
					);
			stepLabelClass = "stepLabel2";
		}
				
		new CRunPVToolkitDefLabel(hbox, 
				new String[]{"class", "cLabelKey"}, 
				new Object[]{"font " + stepLabelClass, "PV-toolkit.step"}
		);
		int stepNumber = (int)keyValueMap.get("stepNumber");
		int cycleNumber = (int)keyValueMap.get("cycleNumber");
		List<List<IXMLTag>> currentStepTagsPerCycle = pvToolkit.getCurrentStepTagsPerCycle();
		stepNumber = ((stepNumber - 1) % currentStepTagsPerCycle.get(cycleNumber - 1).size()) + 1;
		new CRunPVToolkitDefImage(hbox, 
				new String[]{"class", "src"}, 
				new Object[]{"stepIcon", zulfilepath + "number_" + stepNumber + "_gray.svg"}
		);

		IXMLTag learningActivityTag = pvToolkit.getLearningActivityChildTag((IXMLTag)keyValueMap.get("stepTag"));
		if (learningActivityTag != null) {
			Label label = new CRunPVToolkitDefLabel(hbox, 
				new String[]{"class", "cacAndTag", "tagChildName"}, 
				new Object[]{"font stepLabel2", new CRunPVToolkitCacAndTag(pvToolkit.getCurrentProjectsCaseComponent(), learningActivityTag), "name"}
					);
			if (!pvToolkit.hasStudentRole) {
				//NOTE if role is teacher, passive teacher or peerstudent remove 'peer'
				label.setValue(pvToolkit.removePeerInString(label.getValue()));
			}
		}

		Button btn = new CRunPVToolkitDefButton(hbox, 
				new String[]{"class", "cLabelKey"}, 
				new Object[]{"font stepInstuctionButton", "PV-toolkit.instruction"}
		);
		addBackButtonOnClickEventListener(btn, (IXMLTag)keyValueMap.get("stepTag"));

		setVisible(pvToolkit.has_step_title);	

	}

	protected void addBackButtonOnClickEventListener(Component component, IXMLTag stepTag) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				((Caption)CDesktopComponents.vView().getComponent("instructionPopupCaption")).setLabel(CDesktopComponents.vView().getLabel("PV-toolkit.instruction"));
				String html = "";
				IXMLTag learningActivityTag = pvToolkit.getLearningActivityChildTag(stepTag);
				if (learningActivityTag != null) {
					html = pvToolkit.getTagChildValue(new CRunPVToolkitCacAndTag(pvToolkit.getCurrentProjectsCaseComponent(), learningActivityTag), "richtext");
				}
				((Html)CDesktopComponents.vView().getComponent("instructionPopupHtml")).setContent(html);
				((Popup)CDesktopComponents.vView().getComponent("instructionPopup")).open(component);
			}
		});
	}

}
