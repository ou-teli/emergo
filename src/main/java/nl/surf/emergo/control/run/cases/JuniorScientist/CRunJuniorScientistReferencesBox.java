/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.JuniorScientist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Event;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.zkspring.STriggeredReference;

/**
 * This class is used to show both a references icon and a references pop-up. It uses content entered within a references case component. There are folders with one level of pieces.
 * The used references case component should have present=false and present should stay false, to prevent rendering of the original references component on the tablet.
 * Present of folders and pieces determine the layout of references pop-up and is set by game script.
 * 
 * Note that because the references macro is defined under the root element of the navigation component, it will be rerendered for each location, i.e., if present of the macro is true.
 * This way the macro is updated on each location. If you want to update the macro on the same location, use game script to set update of the macro to true.
 */
public class CRunJuniorScientistReferencesBox extends CRunJuniorScientistBox {

	private static final long serialVersionUID = -8142455635086956762L;

	protected final static String referencesCaseComponentName = "game_references";
	protected IECaseComponent referencesCaseComponent;
	
	protected final static String paramKeyMacro = "macro";

	protected String referencesIconId;
	protected String referencesPopupId;
	protected String piecePopupMacroId;
	protected String piecePopupId;
	
	protected boolean referencesIconVisible = true;
	protected boolean referencesPopupVisible = false;
	protected boolean piecePopupVisible = false;

	@Override
	public void onInit() {
		super.onInitMacro();
		
		referencesIconId = getUuid() + "_icon";
		referencesPopupId = getUuid() + "_popup";
		piecePopupMacroId = getUuid() + "_piece_popup_macro";
		piecePopupId = getUuid() + "_piece_popup";
		
		referencesCaseComponent = getReferencesComponent();
		//NOTE super ancestor CRunComponent needs case component to be set
		setCaseComponent(referencesCaseComponent);

		//NOTE if opened of the references case component is true, the pop-up should be shown and the icon should be hidden
		String opened = sSpring.getCurrentRunComponentStatus(referencesCaseComponent, AppConstants.statusKeyOpened, AppConstants.statusTypeRunGroup);
		referencesIconVisible = !opened.equals(AppConstants.statusValueTrue);
		referencesPopupVisible = !referencesIconVisible;
		piecePopupVisible = false;
		
		propertyMap.put("referencesIconVisible", referencesIconVisible);
		propertyMap.put("referencesPopupVisible", referencesPopupVisible);
		propertyMap.put("piecePopupVisible", piecePopupVisible);

		//determine references properties to be used within the child macro
		setReferencesProperties();
		
		//add the child macro
		addChildMacro("JuniorScientist_references_view_macro.zul");
	}
	
	@Override
	public void onUpdate() {
		
		//rerender child macro with adjusted properties
		childMacro.recreate();
	}

	protected void setReferencesProperties() {
		propertyMap.put("referencesTitle", sSpring.unescapeXML(sSpring.getCaseComponentRoleName("", referencesCaseComponent.getName())));
		
		List<Map<String,Object>> folders = new ArrayList<Map<String,Object>>();
		propertyMap.put("folders", folders);
		IXMLTag rootTag = sSpring.getXmlDataPlusRunStatusTree(referencesCaseComponent, AppConstants.statusTypeRunGroup);
		if (rootTag == null) {
			return;
		}
		IXMLTag contentTag = rootTag.getChild(AppConstants.contentElement);
		if (contentTag == null) {
			return;
		}
		
		//loop through folders
		for (IXMLTag folderTag : contentTag.getChilds("map")) {
			boolean folderPresent = !folderTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse);
			if (folderPresent) {
				Map<String,Object> hFolderData = new HashMap<String,Object>();
				folders.add(hFolderData);
				hFolderData.put("tag", folderTag);
				hFolderData.put("folderName", sSpring.unescapeXML(folderTag.getChildValue("name")));
				List<Map<String,Object>> pieces = new ArrayList<Map<String,Object>>();
				hFolderData.put("pieces", pieces);
				//loop through pieces
				for (IXMLTag pieceTag : folderTag.getChilds("piece")) {
					boolean piecePresent = !pieceTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse);
					if (piecePresent) {
						Map<String,Object> hPieceData = new HashMap<String,Object>();
						pieces.add(hPieceData);
						hPieceData.put("tag", pieceTag);
						hPieceData.put("pieceName", sSpring.unescapeXML(pieceTag.getChildValue("name")));
						String pieceType = "";
						IXMLTag blobTag = pieceTag.getChild("blob");
						if (blobTag != null) {
							pieceType = blobTag.getAttribute("mediatype");
						}
						hPieceData.put("pieceType", pieceType);
					}
				}
			}
		}

		propertyMap.put("pieceData", getPieceData(null));
	}
	
	protected IECaseComponent getReferencesComponent() {
		return sSpring.getCaseComponent("", referencesCaseComponentName);
	}
	
	/** If a button is clicked within the child macro file, following event is triggered. */
	public void onButtonClick(Event event) {
		if (event.getData().equals("show_popup")) {
			//NOTE the references icon is clicked, so hide it and show the references pop-up

			//just like for the original references component set selected and opened for the case component
			setRunComponentStatusJS(referencesCaseComponent, AppConstants.statusKeySelected, AppConstants.statusValueTrue);
			setRunComponentStatusJS(referencesCaseComponent, AppConstants.statusKeyOpened, AppConstants.statusValueTrue);

			referencesIconVisible = false;
			referencesPopupVisible = true;
			vView.getComponent(referencesIconId).setVisible(referencesIconVisible);
			vView.getComponent(referencesPopupId).setVisible(referencesPopupVisible);
			propertyMap.put("referencesIconVisible", referencesIconVisible);
			propertyMap.put("referencesPopupVisible", referencesPopupVisible);
		}
		else if (event.getData().equals("show_icon")) {
			//NOTE the references pop-up is closed, so hide it and show the references icon

			//just like for the original references component set opened for the case component
			setRunComponentStatusJS(referencesCaseComponent, AppConstants.statusKeyOpened, AppConstants.statusValueFalse);

			referencesIconVisible = true;
			referencesPopupVisible = false;
			vView.getComponent(referencesIconId).setVisible(referencesIconVisible);
			vView.getComponent(referencesPopupId).setVisible(referencesPopupVisible);
			propertyMap.put("referencesIconVisible", referencesIconVisible);
			propertyMap.put("referencesPopupVisible", referencesPopupVisible);
		}
		else if (event.getData() instanceof IXMLTag) {
			//NOTE a piece is clicked so hide references pop-up and show piece pop-up
			IXMLTag tag = (IXMLTag)event.getData();

			//just like for the original references component set selected and opened for the piece
			setRunTagStatusJS(getReferencesComponent(), tag, AppConstants.statusKeySelected, AppConstants.statusValueTrue, false);
			setRunTagStatusJS(getReferencesComponent(), tag, AppConstants.statusKeyOpened, AppConstants.statusValueTrue, false);

			referencesPopupVisible = false;
			piecePopupVisible = true;
			vView.getComponent(referencesPopupId).setVisible(referencesPopupVisible);
			vView.getComponent(piecePopupId).setVisible(piecePopupVisible);
			propertyMap.put("referencesPopupVisible", referencesPopupVisible);
			propertyMap.put("piecePopupVisible", piecePopupVisible);
			
			//NOTE rerender macro to show updated piece
			HtmlMacroComponent piecePopupMacro = (HtmlMacroComponent)vView.getComponent(piecePopupMacroId);
			piecePopupMacro.setDynamicProperty("a_tag", tag);
			piecePopupMacro.setDynamicProperty("a_pieceData", getPieceData(tag));
			piecePopupMacro.recreate();
		}
		else if (event.getData().equals("hide_piece_popup")) {
			//NOTE close piece pop-up and show references pop-up

			referencesPopupVisible = true;
			piecePopupVisible = false;
			vView.getComponent(referencesPopupId).setVisible(referencesPopupVisible);
			vView.getComponent(piecePopupId).setVisible(piecePopupVisible);
			propertyMap.put("referencesPopupVisible", referencesPopupVisible);
			propertyMap.put("piecePopupVisible", piecePopupVisible);
		}
	}
	
	protected Map<String,Object> getPieceData(IXMLTag pieceTag) {
		Map<String,Object> hPieceData = new HashMap<String,Object>();
		hPieceData.put("zulfilepath", getZulfilepath());
		hPieceData.put("macroBoxUuid", getUuid());
		hPieceData.put("piecePopupVisible", piecePopupVisible);
		hPieceData.put("referencesTitle", sSpring.unescapeXML(sSpring.getCaseComponentRoleName("", referencesCaseComponent.getName())));
		String pieceName = "";
		String pieceUrl = "";
		String pieceExtension = "";
		String pieceContent = "";
		String pieceMacroURI = "";
		if (pieceTag != null) {
			pieceName = sSpring.unescapeXML(pieceTag.getChildValue("name"));
			pieceUrl = sSpring.getSBlobHelper().getUrl(pieceTag, "blob");
			pieceExtension = vView.getFileExtension(pieceUrl);
			if (!vView.isAbsoluteUrl(pieceUrl) && !pieceExtension.equals("html")) {
				pieceUrl = vView.getAbsoluteUrl(pieceUrl);
			}
			if (pieceExtension.equals("html")) {
				//NOTE html is shown in a zk html tag so get content of file 
				String pieceFilePath = sSpring.getAppManager().getAbsoluteAppPath() + pieceUrl.replaceFirst("/", ""); 
				if (sSpring.getFileManager().fileExists(pieceFilePath)) {
					pieceContent = new String(sSpring.getFileManager().readFile(pieceFilePath));
				}
			}
			pieceMacroURI = getParamValue(pieceTag, paramKeyMacro);
			if (!pieceMacroURI.equals("")) {
				pieceMacroURI = zulfilepath + "JuniorScientist_" + pieceMacroURI + ".zul";
			}
		}
		hPieceData.put("pieceName", pieceName);
		hPieceData.put("pieceUrl", pieceUrl);
		hPieceData.put("pieceExtension", pieceExtension);
		hPieceData.put("pieceContent", pieceContent);
		hPieceData.put("pieceMacroURI", pieceMacroURI);
		if (!pieceMacroURI.equals("")) {
			//NOTE if macro (defined under the root element of the navigation component) has to be shown, set case component and tag, because they are necessary for rendering the macro
			hPieceData.put("casecomponent", propertyMap.get("casecomponent"));
			hPieceData.put("tag", propertyMap.get("tag"));
			//NOTE macro needs to know that it has to render itself statically because it has to behave as a piece.
			hPieceData.put("showstatic", true);
		}
		return hPieceData;
	}
	
	@Override
	public void handleStatusChange(STriggeredReference aTriggeredReference) {
	}

}
