/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.IP2;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Image;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefImage;

public class CRunSatisfactionBoxOptionsImage extends CDefImage {

	private static final long serialVersionUID = 2601626826018669421L;

	protected CRunSatisfactionDiv satisfactionDiv = null;

	public void onClick(Event aEvent) {
		satisfactionDiv = (CRunSatisfactionDiv)CDesktopComponents.vView().getComponent("satisfactionDiv");
		if (satisfactionDiv == null) {
			return;
		}

		for (int i=1;i<7;i++) {
			Image image = (Image)getParent().getParent().getChildren().get(i).getChildren().get(0);
			if (i > (((int)getAttribute("number")) + 1)) {
				image.setSrc(satisfactionDiv.relativePath + "bolletje-leeg.png");
			}
			else {
				image.setSrc(satisfactionDiv.relativePath + "bolletje-" + getAttribute("typeOfSatisfaction") + ".png");
			}
		}
		satisfactionDiv.saveSatisfaction((String)getAttribute("typeOfSatisfaction"), (((int)getAttribute("number")) + 1));

	}

}
