/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.AbstractComponent;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

import nl.surf.emergo.business.IAppManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CIObserved;
import nl.surf.emergo.control.def.CDefFCKeditor;
import nl.surf.emergo.control.def.CDefHbox;
import nl.surf.emergo.control.def.CDefLabel;
import nl.surf.emergo.control.def.CDefTextbox;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.utilities.FileHelper;

/**
 * The Class CRunNewMail. Is used to show input elements for new mail.
 */
public class CRunNewMailClassic extends CRunComponentClassic {
	private static final Logger _log = LogManager.getLogger(CRunComponentClassic.class);
	private static final long serialVersionUID = 6829002663834086078L;

	/**
	 * Instantiates a new c run new mail.
	 * 
	 * @param aId            the a id
	 * @param aCaseComponent the a case component
	 */
	public CRunNewMailClassic(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
	}

	/**
	 * Creates title area, mail area and buttons area.
	 */
	@Override
	public void createComponents() {
		CRunVboxClassic lVbox = new CRunVboxClassic();

		createTitleArea(lVbox);
		createMailArea(lVbox);
		createButtonsArea(lVbox);

		appendChild(lVbox);
	}

	/**
	 * Creates mail area. A subject contact combinations listbox, a subject field
	 * for help mails, a rich text field for the mail body and an attachments area
	 * to add attachments.
	 * 
	 * @param aParent the a parent
	 * 
	 * @return the c run area
	 */
	protected CRunAreaClassic createMailArea(Component aParent) {
		CRunAreaClassic lMailArea = new CRunAreaClassic();
		lMailArea.setSclass(className + "_mail_area");
		aParent.appendChild(lMailArea);
		createSubjectContacts(lMailArea);
		createSubjects(lMailArea);
		createRichtext(lMailArea);
		createAttachments(lMailArea);
		return lMailArea;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.surf.emergo.control.run.CRunComponent#newTitleLabel(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	protected CDefLabel newTitleLabel(String aLabel, String aSclass) {
		CDefLabel lLabel = new CDefLabel();
		lLabel.setValue(CDesktopComponents.vView().getLabel("run_new_mail.title"));
		lLabel.setSclass(aSclass);
		return lLabel;
	}

	/**
	 * Creates subject contacts listbox.
	 * 
	 * @param aParent the a parent
	 */
	public void createSubjectContacts(Component aParent) {
		// show listbox with subject contacts, active mails.
		CRunMailClassic lMail = (CRunMailClassic) CDesktopComponents.vView().getComponent("runMail");
		if (lMail == null)
			return;

		CRunHboxClassic lHbox = new CRunHboxClassic();
		lHbox.setId("runMailSubjectContacts");
		aParent.appendChild(lHbox);

		CRunAreaClassic lArea = new CRunAreaClassic();
		lHbox.appendChild(lArea);
		lArea.setSclass(className + "_mail");
		lArea.setWidth("100px");
		lArea.setHeight("100px");
		Label lLabel = new CDefLabel();
		if (CDesktopComponents.sSpring().isTutorRun())
			lLabel.setValue(CDesktopComponents.vView().getLabel("run_new_mail.prompt.from"));
		else
			lLabel.setValue(CDesktopComponents.vView().getLabel("run_new_mail.prompt.to"));
		lArea.appendChild(lLabel);
		lLabel.setSclass("CRunNewMail_mail_text");

		lArea = new CRunAreaClassic();
		lHbox.appendChild(lArea);
		List<IXMLTag> lMailTags = lMail.getMailTemplateTags();
		CRunMailSubjectContactsListboxClassic lListbox = new CRunMailSubjectContactsListboxClassic(
				"runMailSubjectContactsListbox");
		lArea.appendChild(lListbox);
		lListbox.showItems(lMailTags, lMail.getContactStrings(lMailTags), "");
	}

	/**
	 * Creates subject edit field.
	 * 
	 * @param aParent the a parent
	 */
	public void createSubjects(Component aParent) {
		// show combobox with subjects, content depends on chosen person.
		// editable combobox, so subject can be free.
		// if subject is not predefined then help mail, otherwise predefined
		// mail
		CRunHboxClassic lHbox = new CRunHboxClassic();
		lHbox.setId("runMailSubjects");
		lHbox.setVisible(false);
		aParent.appendChild(lHbox);

		CRunAreaClassic lArea = new CRunAreaClassic();
		lHbox.appendChild(lArea);
		lArea.setWidth("100px");
		lArea.setHeight("20px");
		lArea.setSclass(className + "_mail");
		Label lLabel = new CDefLabel();
		lLabel.setValue(CDesktopComponents.vView().getLabel("run_new_mail.prompt.subject"));
		lArea.appendChild(lLabel);
		lLabel.setSclass("CRunNewMail_mail_text");

		lArea = new CRunAreaClassic();
		lHbox.appendChild(lArea);
		CRunVboxClassic lVbox = new CRunVboxClassic();
		lArea.appendChild(lVbox);

		// and show editfield for own subject if there is a possibility for help
		// mails
		Textbox lTextbox = new CDefTextbox();
		lVbox.appendChild(lTextbox);
		lTextbox.setId("runMailSubjectField");
		lTextbox.setCols(47);
		lTextbox.setSclass(className + "_mail");
	}

	/**
	 * Creates richtext area for body text.
	 * 
	 * @param aParent the a parent
	 */
	public void createRichtext(Component aParent) {
		// show simple richtext field
		CRunHboxClassic lHbox = new CRunHboxClassic();
		lHbox.setId("runMailBody");
		lHbox.setVisible(false);
		aParent.appendChild(lHbox);

		CRunAreaClassic lArea = new CRunAreaClassic();
		lHbox.appendChild(lArea);
		lArea.setWidth("100px");
		lArea.setHeight("20px");
		lArea.setSclass(className + "_mail");
		Label lLabel = new CDefLabel();
		lLabel.setValue(CDesktopComponents.vView().getLabel("run_new_mail.prompt.body"));
		lArea.appendChild(lLabel);
		lLabel.setSclass("CRunNewMail_mail_text");

		lArea = new CRunAreaClassic();
		lHbox.appendChild(lArea);
//		lArea.setSclass(className + "_mail");
		CDefFCKeditor lTextbox = new CDefFCKeditor();
		lArea.appendChild(lTextbox);
		lTextbox.setId("runMailBodyField");
		lTextbox.setHeight("200px");
		lTextbox.setWidth("654px");
		lTextbox.setToolbar("Basic");
	}

	/**
	 * Creates attachments area.
	 * 
	 * @param aParent the a parent
	 */
	public void createAttachments(Component aParent) {
		// show new attachtment button
		// creating new attachtment will add an attachment child with a show and
		// a delete button
		CRunHboxClassic lHbox = new CRunHboxClassic();
		lHbox.setId("runMailAttachmentsBox");
		lHbox.setVisible(false);
		aParent.appendChild(lHbox);

		CRunAreaClassic lArea = new CRunAreaClassic();
		lHbox.appendChild(lArea);
		lArea.setWidth("100px");
		lArea.setHeight("100px");
		lArea.setSclass(className + "_mail");
		Label lLabel = new CDefLabel();
		lLabel.setValue(CDesktopComponents.vView().getLabel("run_new_mail.prompt.attachments"));
		lArea.appendChild(lLabel);
		lLabel.setSclass("CRunNewMail_mail_text");

		lArea = new CRunAreaClassic();
		lHbox.appendChild(lArea);
		CRunVboxClassic lVbox = new CRunVboxClassic();
		lArea.appendChild(lVbox);
		lVbox.setId("runMailAttachments");
		CRunButtonClassic lButton = new CRunButtonClassic("", "newMailAttachment", null,
				CDesktopComponents.vView().getLabel("run_new_mail.button.new_attachment"), "_component_200", "");
		// NOTE set upload so one file with unlimited size can be uploaded
		lButton.setUpload("true,maxsize=-1,native,multiple=false");
		lButton.registerObserver(this.getId());
		lVbox.appendChild(lButton);
		lVbox.setSclass(className + "_mail");
	}

	/**
	 * Creates new mail attachment.
	 * 
	 * @return true, if successful
	 */
	public boolean newMailAttachment(Object aMedia) {
		List<String> errors = new ArrayList<>();
		FileHelper.isMediaValid((Media) aMedia, errors);

		if (errors.isEmpty()) {
			CRunVboxClassic lVbox = (CRunVboxClassic) CDesktopComponents.vView().getComponent("runMailAttachments");
			if (lVbox != null) {
				// create new attachtment (hbox) with label, show and a delete button
				// add media as attribute to hbox
				// and add as child to runMailAttachments
				CRunHboxClassic lAttachment = new CRunNewMailAttachmentClassic((Media) aMedia);
				lAttachment.setAttribute("attachment", "true");
				lVbox.appendChild(lAttachment);
			}
			return true;
		}

		StringBuilder sb = new StringBuilder();
		sb.append("Geen geldige upload: ");
		sb.append(aMedia);
		sb.append(" ");
		sb.append(errors.get(0));
		_log.warn(sb.toString());
		Messagebox.show(sb.toString() + aMedia, "Error", Messagebox.OK, Messagebox.ERROR);

		return false;
	}

	/**
	 * Creates button area with send mail button and cancel button.
	 * 
	 * @param aParent the a parent
	 * 
	 * @return the c run hbox
	 */
	@Override
	protected CRunHboxClassic createButtonsArea(Component aParent) {
		CRunHboxClassic lButtonsHbox = super.createButtonsArea(aParent);
		// show send mail button and cancel button
		CRunButtonClassic lButton = new CRunButtonClassic("runSendMailBtn", "sendMail", null,
				CDesktopComponents.vView().getLabel("send"), "_component_100", "");
		lButton.registerObserver(this.getId());
//		lButtonsHbox.appendChild(lButton);
		// because error messages are displayed as parent of the ok button, add a hbox
		// as parent,
		// otherwise the error message is not displayed on the right position.
		CDefHbox lHbox = new CDefHbox();
		lHbox.appendChild(lButton);
		lButtonsHbox.appendChild(lHbox);
		lButton.setDisabled(true);

		lButton = new CRunButtonClassic("", "cancelMail", null, CDesktopComponents.vView().getLabel("cancel"),
				"_component_100", "");
		lButton.registerObserver(this.getId());
		lButtonsHbox.appendChild(lButton);
		return lButtonsHbox;
	}

	/**
	 * Is called by cancel, send mail or new attachment button.
	 * 
	 * @param aObserved the observed object, it should have interface CIObserved
	 *                  implemented
	 * @param aAction   the action
	 * @param aStatus   the status object, can be String or other class
	 */
	@Override
	public void observedNotify(CIObserved aObserved, String aAction, Object aStatus) {
		if (aAction.equals("cancelMail")) {
			Events.echoEvent("onCancelMail", this, null);
		}
		if (aAction.equals("sendMail")) {
			Events.echoEvent("onSendMail", this, null);
		}
		if (aAction.equals("newMailAttachment")) {
			newMailAttachment(aStatus);
		}
	}

	protected void cleanupNewMailWnd() {
		// NOTE remove temporary files possibly created during uploading attachments.
		runWnd.removeTempFiles();
		CRunNewMailWndClassic lWnd = (CRunNewMailWndClassic) CDesktopComponents.vView().getComponent("runNewMailWnd");
		if (lWnd != null) {
			lWnd.detach();
		}
	}

	public void onCancelMail() {
		// NOTE if mail is canceled cleanup new mail wnd.
		cleanupNewMailWnd();
	}

	public void onSendMail() {
		if (sendMail()) {
			// NOTE if mail is sent cleanup new mail wnd.
			cleanupNewMailWnd();
		}
	}

	/**
	 * Called if subject contact selected.
	 * 
	 * @param aMailTagId the a mail tag id
	 * @param aSubject   the a subject
	 * @param aContacts  the a contacts
	 */
	public void subjectContactsSelected(String aMailTagId, String aSubject, String aContacts) {
		CRunHboxClassic lSubjects = (CRunHboxClassic) CDesktopComponents.vView().getComponent("runMailSubjects");
		lSubjects.setVisible(true);
		CDefTextbox lSubjectField = (CDefTextbox) CDesktopComponents.vView().getComponent("runMailSubjectField");
		lSubjectField.setValue(aSubject);
		lSubjectField.setDisabled(!aSubject.equals(""));
		lSubjectField.setVisible(true);
		CRunHboxClassic lBody = (CRunHboxClassic) CDesktopComponents.vView().getComponent("runMailBody");
		lBody.setVisible(true);
		CRunHboxClassic lMailAttachmentBox = (CRunHboxClassic) CDesktopComponents.vView()
				.getComponent("runMailAttachmentsBox");
		lMailAttachmentBox.setVisible(true);
		CRunButtonClassic lButton = (CRunButtonClassic) CDesktopComponents.vView().getComponent("runSendMailBtn");
		lButton.setDisabled(false);
		lButton.setVisible(true);

		String lBodyStr = "";
		IXMLTag lMailTag = CDesktopComponents.sSpring().getTag(caseComponent, aMailTagId);
		if (lMailTag != null) {
			lBodyStr = CDesktopComponents.sSpring().unescapeXML(lMailTag.getChildValue("defaulttext"));
		}
		CDefFCKeditor lBodyField = (CDefFCKeditor) CDesktopComponents.vView().getComponent("runMailBodyField");
		lBodyField.setValue(lBodyStr);

		if (!CDesktopComponents.sSpring().isTutorRun())
			return;
		if (lMailTag != null) {
			lBodyStr = CDesktopComponents.sSpring().unescapeXML(lMailTag.getChildValue("richtext"));
			if (lBodyStr.equals("")) {
				lBodyStr = CDesktopComponents.sSpring().unescapeXML(lMailTag.getChildValue("defaulttext"));
			}
		}
		lBodyField.setValue(lBodyStr);
	}

	/**
	 * Sends mail. First checks input and shows error message if applicable.
	 * 
	 * @return true, if successful
	 */
	public boolean sendMail() {
		CRunMailSubjectContactsListboxClassic lListbox = (CRunMailSubjectContactsListboxClassic) CDesktopComponents
				.vView().getComponent("runMailSubjectContactsListbox");
		CDefTextbox lSubjectField = (CDefTextbox) CDesktopComponents.vView().getComponent("runMailSubjectField");
		CDefFCKeditor lBodyField = (CDefFCKeditor) CDesktopComponents.vView().getComponent("runMailBodyField");

		// check input
		IAppManager lAppManager = CDesktopComponents.sSpring().getAppManager();
		List<String[]> lErrors = new ArrayList<>();
		Listitem lSelectedItem = lListbox.getSelectedItem();
		String lMailTagId = (String) lSelectedItem.getValue();
		String lSubject = CDesktopComponents.sSpring().escapeXML(lSubjectField.getValue().trim());
		String lContactName = (String) lSelectedItem.getAttribute("contacts");

		String lBody = "";
		boolean lSubjectEmpty = false;
		if (lSubject.equals(""))
			lSubjectEmpty = true;
		if (lSubjectEmpty)
			lAppManager.addError(lErrors, "mail.subject", "error_empty");
		boolean lBodyEmpty = false;
		if (lBodyField.isVisible()) {
			lBody = CDesktopComponents.sSpring().escapeXML(lBodyField.getValue().trim());
			if (lBody.equals(""))
				lBodyEmpty = true;
		}
		if (lBodyEmpty)
			lAppManager.addError(lErrors, "mail.body", "error_empty");

		if (!lErrors.isEmpty())
			CDesktopComponents.cControl()
					.showErrors((AbstractComponent) CDesktopComponents.vView().getComponent("runSendMailBtn"), lErrors);
		else {
			// get attachments, are components of type Media
			List<Media> lAttachments = new ArrayList<>();
			CRunVboxClassic lMailAttachmentBox = (CRunVboxClassic) CDesktopComponents.vView()
					.getComponent("runMailAttachments");
			List<Component> lChildComps = lMailAttachmentBox.getChildren();
			if (lChildComps != null) {
				for (Component lComponent : lChildComps) {
					String lAttachmentStr = (String) lComponent.getAttribute("attachment");
					if ((lAttachmentStr != null) && (lAttachmentStr.equals("true"))) {
						Media lMedia = (Media) lComponent.getAttribute("content");
						lAttachments.add(lMedia);
					}
				}
			}

			CRunMailTreeClassic lRunMailTree = (CRunMailTreeClassic) CDesktopComponents.vView()
					.getComponent("runMailTree");
			if (lRunMailTree != null) {
				// send mail
				lRunMailTree.mailIsSent(lMailTagId, lContactName, lSubject, lBody, lAttachments);
			}
		}
		return (lErrors.size() == 0);
	}
}