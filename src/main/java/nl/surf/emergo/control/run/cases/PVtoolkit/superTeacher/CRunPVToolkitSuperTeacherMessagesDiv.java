/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.superTeacher;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitInitBox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTag;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTags;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpring;

public class CRunPVToolkitSuperTeacherMessagesDiv extends CDefDiv {

	private static final long serialVersionUID = 815715022013132928L;

	public String zulfilepath = ((CRunPVToolkitInitBox)CDesktopComponents.vView().getComponent("PV-toolkit_initBox")).getZulfilepath();
	
	protected CRunPVToolkit pvToolkit;
		
	protected IERunGroup _actor;
	protected boolean _editable;
	
	protected IAccountManager accountManager = (IAccountManager)CDesktopComponents.sSpring().getBean("accountManager");

	protected boolean isPreviewRun = false;

	protected IECaseComponent messagesCaseComponent;
	protected List<IXMLTag> messageTags;
	
	protected String _idPrefix = "superTeacher";
	protected String _classPrefix = "superTeacher";
	
	public void init(IERunGroup actor, boolean editable) {
		_actor = actor;
		_editable = editable;
		
		setClass(_classPrefix);

		pvToolkit = (CRunPVToolkit)CDesktopComponents.vView().getComponent("pvToolkit");
		
		update();
		
		setVisible(true);
	}
	
	public void update() {
		getChildren().clear();
		
		pvToolkit.setMemoryCaching(true);
		
		isPreviewRun = CDesktopComponents.sSpring().getRun().getStatus() == AppConstants.run_status_test;

		Div div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{"titleRight"}
		);
		
		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font titleRight", "PV-toolkit-superTeacher.header.messages"}
		);
		
		if (_editable) {
			Button btn = new CRunPVToolkitDefButton(this, 
					new String[]{"class", "labelKey"}, 
					new Object[]{"font pvtoolkitButton " + _classPrefix + "NewMessageButton", "PV-toolkit-superTeacher.button.newMessage"}
			);
			addNewMessageButtonOnClickEventListener(btn);
		}

		div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "MessagesDiv"}
		);
		
		Rows rows = appendMessagesGrid(div);
		int rowNumber = 1;
		rowNumber = appendMessagesToGrid(rows, rowNumber);
		
		Button btn = new CRunPVToolkitDefButton(this, 
				new String[]{"class", "cLabelKey"}, 
				new Object[]{"font pvtoolkitButton " + _classPrefix + "BackButton", "PV-toolkit.back"}
		);
		addBackToRunsOnClickEventListener(btn, this, CDesktopComponents.vView().getComponent("superTeacherRunsDiv"));

		pvToolkit.setMemoryCaching(false);
	}

	protected void addNewMessageButtonOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				CRunPVToolkitSuperTeacherNewOrEditMessageDiv popup = (CRunPVToolkitSuperTeacherNewOrEditMessageDiv)CDesktopComponents.vView().getComponent(_idPrefix + "NewOrEditMessageDiv");
				if (popup != null) {
					popup.init(null);
				}
			}
		});
	}

	protected void addBackToRunsOnClickEventListener(Component component, Component fromComponent, Component toComponent) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				if (fromComponent != null) {
					fromComponent.setVisible(false);
				}
				if (toComponent != null) {
					toComponent.setVisible(true);
				}
			}
		});
	}

	public void update(IXMLTag stepMessageTag) {
		String stepMessageTagUuid = stepMessageTag.getAttribute("stepmessageuuid");
		VView vView = CDesktopComponents.vView();

		setMessageLabelVariableProperties((Label)vView.getComponent(getId() + "GridRowMessageLabel_" + stepMessageTagUuid), stepMessageTag);
		setMessageDateLabelVariableProperties((Label)vView.getComponent(getId() + "GridRowMessageDateLabel_" + stepMessageTagUuid), stepMessageTag);
		setStepLabelVariableProperties((Label)vView.getComponent(getId() + "GridRowStepLabel_" + stepMessageTagUuid), stepMessageTag);
		setDeadlineLabelVariableProperties((Label)vView.getComponent(getId() + "GridRowDeadlineLabel_" + stepMessageTagUuid), stepMessageTag);
	}
	
	protected Rows appendMessagesGrid(Component parent) {
    	return pvToolkit.appendGrid(
    			parent, 
    			getId() + "Grid",
    			"position:absolute;left:0px;top:0px;width:1060px;height:430px;overflow:auto;", 
				new boolean[] {true, true, true, true, _editable, _editable, true}, 
    			new String[] {"20%", "18%", "30%", "18%", "7%", "7%", "14%"}, 
    			new boolean[] {true, true, true, true, false, false,false},
    			null,
    			"PV-toolkit-superTeacher.column.label.", 
    			new String[] {"message", "messageDateTime", "step", "deadline", "edit", "delete", "send"});
    }

	protected int appendMessagesToGrid(Rows rows, int rowNumber) {
		return appendMessagesToGrid(
				rows, 
				rowNumber,
				new boolean[] {true, true, true, true, _editable, _editable, true} 
				);
	}

	protected int appendMessagesToGrid(
			Rows rows, 
			int rowNumber,
    		boolean[] showColumn 
			) {
		CRunPVToolkitCacAndTags steps = pvToolkit.getStepMessageTags(_actor);
		CRunPVToolkitCacAndTags messages = pvToolkit.getMessages();
		if (messages == null) {
			return rowNumber;
		}
		messagesCaseComponent = messages.getCaseComponent();
		messageTags = messages.getXmlTags();
		for (IXMLTag stepMessageTag : steps.getXmlTags()) {
			
			String stepMessageTagUuid = stepMessageTag.getAttribute(stepMessageTag.getName() + "uuid");
			
			Row row = new Row();
			rows.appendChild(row);
			row.setAttribute("tag", stepMessageTag);
			
			String stepTagId = pvToolkit.getStatusChildTagAttribute(stepMessageTag, "steptagid");
			IXMLTag stepTag = null;
			for (IXMLTag tempStepTag : pvToolkit.getCurrentStepTags()) {
				if (tempStepTag.getAttribute(AppConstants.keyId).equals(stepTagId)) {
					stepTag = tempStepTag;
					break;
				}
			}
				
			String chosenMessageCacId = pvToolkit.getStatusChildTagAttribute(stepMessageTag, "messagecacid");
			String chosenMessageTagId = pvToolkit.getStatusChildTagAttribute(stepMessageTag, "messagetagid");
			
			String messageDate = "";

			int columnNumber = 0;
			if (showColumn.length > columnNumber && showColumn[columnNumber]) {
				Label label = new CRunPVToolkitDefLabel(row, 
						new String[]{"id", "class"}, 
						new Object[]{getId() + "GridRowMessageLabel_" + stepMessageTagUuid, "font gridLabel"}
						);
				setMessageLabelVariableProperties(label, stepMessageTag);
			}
			
			columnNumber++;
			if (showColumn.length > columnNumber && showColumn[columnNumber]) {
				Label label = new CRunPVToolkitDefLabel(row, 
						new String[]{"id", "class"}, 
						new Object[]{getId() + "GridRowMessageDateLabel_" + stepMessageTagUuid, "font gridLabel"}
						);
				messageDate = setMessageDateLabelVariableProperties(label, stepMessageTag);
			}
			
			columnNumber++;
			if (showColumn.length > columnNumber && showColumn[columnNumber]) {
				Label label = new CRunPVToolkitDefLabel(row, 
						new String[]{"id", "class"}, 
						new Object[]{getId() + "GridRowStepLabel_" + stepMessageTagUuid, "font gridLabel"}
						);
				setStepLabelVariableProperties(label, stepMessageTag);
			}
			
			columnNumber++;
			if (showColumn.length > columnNumber && showColumn[columnNumber]) {
				Label label = new CRunPVToolkitDefLabel(row, 
						new String[]{"id", "class"}, 
						new Object[]{getId() + "GridRowDeadlineLabel_" + stepMessageTagUuid, "font gridLabel"}
						);
				setDeadlineLabelVariableProperties(label, stepMessageTag);
			}
			
			columnNumber++;
			if (showColumn.length > columnNumber && showColumn[columnNumber]) {
				Image image = new CRunPVToolkitDefImage(row,
						new String[]{"class", "src"}, 
						new Object[]{"gridImageClickable", zulfilepath + "edit.svg"}
				);
				addEditOnClickEventListener(image, stepMessageTag);
				image.setVisible(_editable);
			}

			columnNumber++;
			if (showColumn.length > columnNumber && showColumn[columnNumber]) {
				Image image = new CRunPVToolkitDefImage(row,
						new String[]{"class", "src"}, 
						new Object[]{"gridImageClickable", zulfilepath + "trash.svg"}
				);
				addDeleteOnClickEventListener(image, stepMessageTag);
				image.setVisible(_editable);
			}

			columnNumber++;
			if (showColumn.length > columnNumber && showColumn[columnNumber]) {
				if (messageDate.equals("")) {
					Button btn = new CRunPVToolkitDefButton(row, 
							new String[]{"class", "cLabelKey"}, 
							new Object[]{"font " + _classPrefix + "SendMessageButton", "PV-toolkit.send"}
							);
					btn.setAttribute("messageCacId", chosenMessageCacId);
					btn.setAttribute("messageTagId", chosenMessageTagId);
					addSendMessageButtonOnClickEventListener(btn, stepTag);
				}
				else {
					new CRunPVToolkitDefLabel(row, 
							new String[]{"class", "labelKey"}, 
							new Object[]{"font gridLabel", "PV-toolkit-superTeacher.row.label.automatic"}
							);
				}
			}
			
			rowNumber++;
		}
		
		return rowNumber;
	}
	
	public void setMessageLabelVariableProperties(Label label, IXMLTag stepMessageTag) {
		if (label == null || stepMessageTag == null) {
			return;
		}
		String chosenMessageCacId = pvToolkit.getStatusChildTagAttribute(stepMessageTag, "messagecacid");
		String chosenMessageTagId = pvToolkit.getStatusChildTagAttribute(stepMessageTag, "messagetagid");
		String messageName = "";
		if (!chosenMessageCacId.equals("") && !chosenMessageTagId.equals("")) {
			for (IXMLTag messageTag : messageTags) {
				String messageCacId = "" + messagesCaseComponent.getCacId();
				String messageTagId = messageTag.getAttribute(AppConstants.keyId);
				if (messageCacId.equals(chosenMessageCacId) && messageTagId.equals(chosenMessageTagId)) {
					messageName = CDesktopComponents.sSpring().unescapeXML(messageTag.getChildValue("pid"));
				}
			}
		}
		label.setValue(messageName);
	}
	
	public void setStepLabelVariableProperties(Label label, IXMLTag stepMessageTag) {
		if (label == null || stepMessageTag == null) {
			return;
		}
		String stepName = "";
		String stepTagId = pvToolkit.getStatusChildTagAttribute(stepMessageTag, "steptagid");
		if (!stepTagId.equals("")) {
			IXMLTag stepTag = null;
			for (IXMLTag tempStepTag : pvToolkit.getCurrentStepTags()) {
				if (tempStepTag.getAttribute(AppConstants.keyId).equals(stepTagId)) {
					stepTag = tempStepTag;
					break;
				}
			}
			IXMLTag learningActivityTag = pvToolkit.getLearningActivityChildTag(stepTag);
			if (learningActivityTag != null) {
				stepName = pvToolkit.getTagChildValue(new CRunPVToolkitCacAndTag(pvToolkit.getCurrentProjectsCaseComponent(), learningActivityTag), "name");
			}
		}
		label.setValue(stepName);
	}
	
	public void setDeadlineLabelVariableProperties(Label label, IXMLTag stepMessageTag) {
		if (label == null || stepMessageTag == null) {
			return;
		}
		String dateTimeStr = "";
		if (stepMessageTag != null) {
			String dateStr = pvToolkit.getStatusChildTagAttribute(stepMessageTag, AppConstants.statusKeyDeadlineDate);
			String timeStr = pvToolkit.getStatusChildTagAttribute(stepMessageTag, AppConstants.statusKeyDeadlineTime);
			if (!dateStr.equals("")) {
				dateTimeStr += dateStr;
				if (!timeStr.equals("")) {
					dateTimeStr += " " + timeStr;
				}
			}
		}
		label.setValue(dateTimeStr);
	}
	
	public String setMessageDateLabelVariableProperties(Label label, IXMLTag stepMessageTag) {
		if (label == null || stepMessageTag == null) {
			return "";
		}
		String dateTimeStr = "";
		if (stepMessageTag != null) {
			String dateStr = pvToolkit.getStatusChildTagAttribute(stepMessageTag, AppConstants.statusKeyReminderDate);
			String timeStr = pvToolkit.getStatusChildTagAttribute(stepMessageTag, AppConstants.statusKeyReminderTime);
			if (!dateStr.equals("")) {
				dateTimeStr += dateStr;
				if (!timeStr.equals("")) {
					dateTimeStr += " " + timeStr;
				}
			}
		}
		label.setValue(dateTimeStr);
		return dateTimeStr;
	}
	
	protected void addEditOnClickEventListener(Component component, IXMLTag stepMessageTag) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				CRunPVToolkitSuperTeacherNewOrEditMessageDiv popup = (CRunPVToolkitSuperTeacherNewOrEditMessageDiv)CDesktopComponents.vView().getComponent(_idPrefix + "NewOrEditMessageDiv");
				if (popup != null) {
					popup.init(stepMessageTag);
				}
			}
		});
	}
	
	protected void addDeleteOnClickEventListener(Component component, IXMLTag stepMessageTag) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				VView vView = CDesktopComponents.vView();
				int choice = vView.showMessagebox(getRoot(), vView.getCLabel("PV-toolkit.delete.confirmation"), vView.getCLabel("PV-toolkit.delete"), Messagebox.CANCEL | Messagebox.OK, Messagebox.QUESTION);
				if (choice == Messagebox.OK) {
					deleteMessage(stepMessageTag);
				}
			}
		});
	}
	
	protected void addSendMessageButtonOnClickEventListener(Component component, IXMLTag stepTag) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				IXMLTag messageTag = getMessageTag(component);
				if (messageTag != null) {
					CRunPVToolkitSuperTeacherSendMessageDiv popup = (CRunPVToolkitSuperTeacherSendMessageDiv)CDesktopComponents.vView().getComponent(_idPrefix + "SendMessageDiv");
					if (popup != null) {
						popup.init(messagesCaseComponent, messageTag, stepTag);
					}
				}
			}
		});
	}

	protected IXMLTag getMessageTag(Component component) {
		String messageCacId = (String)component.getAttribute("messageCacId");
		String messageTagId = (String)component.getAttribute("messageTagId");
		if (!StringUtils.isEmpty(messageCacId) && !StringUtils.isEmpty(messageTagId)) {
			SSpring sSpring = CDesktopComponents.sSpring();
			IECaseComponent messagesCaseComponent = sSpring.getCaseComponent(messageCacId);
			if (messagesCaseComponent != null) {
				return sSpring.getXmlDataPlusRunStatusTag(messagesCaseComponent, AppConstants.statusTypeRunGroup, messageTagId);
			}
		}
		return null;
	}

	public void saveMessage(IXMLTag stepMessageTag, String messageCacId, String messageTagId, String messageDate, String messageTime, IXMLTag stepTag, String deadlineDate, String deadlineTime) {
		boolean newTag = stepMessageTag == null;
		stepMessageTag = pvToolkit.saveStepMessage(_actor, stepMessageTag, messageCacId, messageTagId, messageDate, messageTime, stepTag, deadlineDate, deadlineTime);
		if (stepMessageTag != null) {
			if (newTag) {
				//TODO add row
				update();
			}
			else {
				update(stepMessageTag);
			}
		}
	}
	
	public void deleteMessage(IXMLTag stepMessageTag) {
		if (pvToolkit.deleteStepMessage(stepMessageTag)) {
			pvToolkit.deleteGridRow(getId() + "Grid", stepMessageTag);
		}
	}
	
}
