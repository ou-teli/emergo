/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CCaseHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunCaseHelper.
 *
 * Instead of using XML data tree of case case component (in class CCaseHelper), XML data plus status tree is used.
 */
public class CRunCaseHelper extends CCaseHelper {

	@Override
	public IXMLTag getCaseCaseComponentContentTag() {
		if (caseCaseComponentRootTag == null) {
			IECaseComponent lCaseCaseComponent = CDesktopComponents.sSpring().getCaseComponent(CDesktopComponents.sSpring().getCase(), "case", "");
			caseCaseComponentRootTag = CDesktopComponents.sSpring().getXmlDataPlusRunStatusTree(lCaseCaseComponent, AppConstants.statusTypeRunGroup);
		}
		if (caseCaseComponentRootTag == null)
			return null;
		return caseCaseComponentRootTag.getChild(AppConstants.contentElement);
	}

	@Override
	public void setCaseCaseComponentContentTag(IXMLTag aContentTag) {
		// there is only one instance of the 'case' case component
		// every time read the content before it is changed, because other authors can change the case case component content too, simultanously
		IECaseComponent lCaseCaseComponent = CDesktopComponents.sSpring().getCaseComponent(CDesktopComponents.sSpring().getCase(), "case", "");
		IXMLTag lContentTag = getCaseComponentContentTag(lCaseCaseComponent);
		if (lContentTag == null)
			return;
		lContentTag.setChildTags(aContentTag.getChildTags());
		CDesktopComponents.sSpring().setSaveRunGroupCaseComponentStatusRootTag(CDesktopComponents.sSpring().getRunGroup(), lCaseCaseComponent, lContentTag.getParentTag());
	}

	/**
	 * Gets the case component content tag of its xml data.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return the case component content tag
	 */
	protected IXMLTag getCaseComponentContentTag(IECaseComponent aCaseComponent) {
		if (aCaseComponent == null)
			return null;
		IXMLTag lRootTag = CDesktopComponents.sSpring().getXmlDataPlusRunStatusTree(aCaseComponent, AppConstants.statusTypeRunGroup);
		if (lRootTag == null)
			return null;
		return lRootTag.getChild(AppConstants.contentElement);
	}

}