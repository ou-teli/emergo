/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.IP2;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CIObserved;
import nl.surf.emergo.control.run.CRunArea;
import nl.surf.emergo.control.run.CRunAssessments;
import nl.surf.emergo.domain.IECaseComponent;

public class CRunSatisfactionDiv extends CRunArea {

	private static final long serialVersionUID = 6230717604905096737L;

	protected CRunAssessments runAssessments = null;
	
	protected String balId = "";
	
	protected String opdrachtNummer = "";
	
	/* Assignment */
	protected String assignment = "";
	
	public String getAssignment() {
		return assignment;
	}

	public void setAssignment(String assignment) {
		this.assignment = assignment;
	}

	/* Specialist */
	protected String specialist = "";

	public String getSpecialist() {
		return specialist;
	}

	public void setSpecialist(String specialist) {
		this.specialist = specialist;
	}

	/* Satisfaction */
	protected int[] satisfaction = new int[2];

	public String relativePath = "";

	public int[] getSatisfaction() {
		return satisfaction;
	}

	public void setSatisfaction(int[] satisfaction) {
		this.satisfaction = satisfaction;
	}

	public CRunSatisfactionDiv() {
		super();
		init();
	}

	public void init() {
		//get bal id and opdrachtnummer
		String[] stateKeys = new String[]{"AfgerondBalId", "AfgerondOpdrachtNummer"}; 
		List<String[]> stateDataList = new ArrayList<String[]>();
		String tagName = "state";
		String statusKey = "value";
		for (int i=0;i<stateKeys.length;i++) {
			String[] stateData = new String[4];
			//casecomponent name
			stateData[0] = "Game states experimenten";
			//tagname
			stateData[1] = tagName;
			//tagkey
			stateData[2] = stateKeys[i];
			//statuskey
			stateData[3] = statusKey;
			stateDataList.add(stateData);
		}
		List<String> errorMessages = new ArrayList<String>();
		String[] statusValues = CDesktopComponents.sSpring().getMultipleCurrentRunTagStatus(stateDataList, AppConstants.statusTypeRunGroup, errorMessages);
		if (errorMessages.size() == 0) {
			balId = statusValues[0];
			opdrachtNummer = statusValues[1];
			//get and set assignment and specialist
			IECaseComponent caseComponent = CDesktopComponents.sSpring().getCaseComponent(CDesktopComponents.sSpring().getCase(), "navigation", "");
			if (caseComponent != null) {
				List<IXMLTag> nodeTags = CDesktopComponents.cScript().getNodeTags(caseComponent, "passage");
				String assignmentId = balId + "_" + opdrachtNummer;
				String nodeTagName = "O_" + assignmentId;
				for (IXMLTag nodeTag : nodeTags) {
					if (nodeTag.getChildValue("pid").equals(nodeTagName)) {
						setAssignment(CDesktopComponents.sSpring().unescapeXML(nodeTag.getChildValue("name")));
						IXMLTag parentTag = nodeTag.getParentTag();
						while (parentTag != null && !parentTag.getName().equals("passage")) {
							parentTag = parentTag.getParentTag();
						}
						if (parentTag != null) {
							setSpecialist(CDesktopComponents.sSpring().unescapeXML(parentTag.getChildValue("hovertext")));
						}
						break;
					}
				}
			}
		}
		if (errorMessages.size() > 0) {
			return;
		}
		
		//get given satisfaction
		stateKeys = new String[]{"OSAT_" + opdrachtNummer + "_1", "OSAT_" + opdrachtNummer + "_2"}; 
		stateDataList = new ArrayList<String[]>();
		tagName = "state";
		statusKey = "value";
		for (int i=0;i<stateKeys.length;i++) {
			String[] stateData = new String[4];
			//casecomponent name
			stateData[0] = "B_" + balId + " states";
			//tagname
			stateData[1] = tagName;
			//tagkey
			stateData[2] = stateKeys[i];
			//statuskey
			stateData[3] = statusKey;
			stateDataList.add(stateData);
		}
		errorMessages = new ArrayList<String>();
		statusValues = CDesktopComponents.sSpring().getMultipleCurrentRunTagStatus(stateDataList, AppConstants.statusTypeRunGroup, errorMessages);
		if (errorMessages.size() == 0) {
			int[] satisfaction = new int[2];
			satisfaction[0] = Integer.parseInt(statusValues[0]);
			satisfaction[1] = Integer.parseInt(statusValues[1]);
			setSatisfaction(satisfaction);
		}

		Events.postEvent("onInitZulfile", this, null);
	}
	
	public void saveSatisfaction(String typeOfSatisfaction, int answer) {
		//casecomponent name
		String caseComponentName = "B_" + balId + " states";
		//tagname
		String tagName = "state";
		//tagkey
		String stateKey = "";
		if (typeOfSatisfaction.equals("nice")) {
			stateKey = "OSAT_" + opdrachtNummer + "_1";
		}
		else {
			stateKey = "OSAT_" + opdrachtNummer + "_2";
		}
		//statuskey
		String statusKey = "value";
		List<String> errorMessages = new ArrayList<String>();
		CDesktopComponents.sSpring().setCurrentRunTagStatus(caseComponentName, tagName, stateKey, statusKey, "" + answer, AppConstants.statusTypeRunGroup, true, errorMessages);

		//casecomponent name
		caseComponentName = "Game scripts experimenten";
		//tagname
		tagName = "counter";
		//tagkey
		stateKey = "";
		if (typeOfSatisfaction.equals("nice")) {
			stateKey = "COU satisfactie1";
		}
		else {
			stateKey = "COU satisfactie2";
		}
		//statuskey
		statusKey = "value";
		errorMessages = new ArrayList<String>();
		CDesktopComponents.sSpring().setCurrentRunTagStatus(caseComponentName, tagName, stateKey, statusKey, "" + answer, AppConstants.statusTypeRunGroup, true, errorMessages);
}

	public CRunAssessments getRunAssessments() {
		if (runAssessments == null) {
			runAssessments = (CRunAssessments)CDesktopComponents.vView().getComponent("runAssessments");
		}
		return runAssessments;
	}

	@Override
	public void observedNotify(CIObserved aObserved, String aAction, Object aStatus) {
	}

	
	/**
	 * Methods originally used in ZUL file.
	 */

	public void onInitZulfile(Event aEvent) {
		relativePath = getRunAssessments().getZulfilePath();
	}

	public void onCreate(CreateEvent aEvent) {
		//set background color of content area
		((HtmlBasedComponent)getParent().getParent().getParent().getParent().getParent()).setStyle("background-color:#55A2FB;");
	}

}
