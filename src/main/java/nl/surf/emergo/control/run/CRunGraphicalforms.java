/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.STriggeredReference;

/**
 * The Class CRunGraphicalforms is used to show graphicalforms component within the run view area of the emergo player..
 */
public class CRunGraphicalforms extends CRunForms {

	private static final long serialVersionUID = -2204010484617448775L;

	public int dropZindex = 1;

	/**
	 * Instantiates a new c run graphicalforms.
	 */
	public CRunGraphicalforms() {
		super("runGraphicalforms", null);
		conversationsFeedbackIdSuffix = "Graphicalforms";
		init();
	}

	/**
	 * Instantiates a new c run graphicalforms.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the case component
	 */
	public CRunGraphicalforms(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
		conversationsFeedbackIdSuffix = "Graphicalforms";
		init();
	}

	/**
	 * Creates new content component, the tree.
	 *
	 * @return the component
	 */
	@Override
	protected Component newContentComponent() {
		return newContentComponent("runGraphicalformsView");
	}

	/**
	 * Creates view.
	 */
	public void init() {
		formChildTagNames = "point";

		Events.postEvent("onInitZulfile", this, null);
	}

	/**
	 * Saves position.
	 *
	 * @param aPointTag the a point tag
	 * @param aPosition the a position
	 */
	public void savePosition(IXMLTag aPointTag, String aPosition) {
		String[] lPositionArr = aPosition.split(",");
		setRunTagStatus(getCaseComponent(), aPointTag, AppConstants.statusKeyXposition, CDesktopComponents.sSpring().escapeXML(lPositionArr[0]), true);
		setRunTagStatus(getCaseComponent(), aPointTag, AppConstants.statusKeyYposition, CDesktopComponents.sSpring().escapeXML(lPositionArr[1]), true);
	}

	@Override
	public String getFeedbackTitle() {
		return CDesktopComponents.vView().getLabel("run_graphicalforms.alert.feedback.title");
	}

	@Override
	public String getDefaultFeedbackText() {
		return CDesktopComponents.vView().getLabel("run_graphicalforms.alert.feedback.defaulttext");
	}

	/**
	 * Show alert.

	 * @param aTitle the a title
	 * @param aText the a text
	 */
	public void showAlert(String aTitle, String aText) {
		if (runWnd != null) {
			runWnd.showAlert(aTitle, aText);
		}
	}

	@Override
	public void handleStatusChange(STriggeredReference aTriggeredReference) {
		Events.postEvent("onHandleStatusChange", this, aTriggeredReference);
	}

	
	/**
	 * Methods originally used in ZUL file.
	 */

	public void onInitZulfile(Event aEvent) {
		String componentStyle = "";
		String url = getBackgroundUrl();
		if (!url.equals("")) {
			componentStyle = "background-image:url('" + url + "');";
		}

		IXMLTag triggeredFeedbackConditionTag = getTriggeredFeedbackConditionTag();
		if (triggeredFeedbackConditionTag != null) {
			//NOTE only show feedback if it is an instruction, which is indicated by a score of -1
			if (sSpringHelper.getTagChildValue(triggeredFeedbackConditionTag, "score", "-1").equals("-1")) {
				Events.postEvent("onShowFeedback", this, null);
			}
		}

		//NOTE showFeedback and showScore are for all forms!
		showFeedback = false;
		showScore = false;
		dataElements = new ArrayList<Hashtable<String,Object>>();
		List<IXMLTag> formTags = getFormTags();
		for (IXMLTag formTag : formTags) {
			Hashtable<String,Object> hFormDataElements = new Hashtable<String,Object>();
			hFormDataElements.put("tag", formTag);
			hFormDataElements.put("title", sSpringHelper.getTagChildValue(formTag, "title", ""));
			hFormDataElements.put("hovertext", sSpringHelper.getTagChildValue(formTag, "hovertext", ""));
			hFormDataElements.put("numberofresits", sSpringHelper.getTagChildValue(formTag, "numberofresits", "-1"));
			hFormDataElements.put("showfeedback", sSpringHelper.getTagStatusChildAttribute(formTag, "showfeedback", "true"));
			if (hFormDataElements.get("showfeedback").equals("true")) {
				showFeedback = true;
			}
			hFormDataElements.put("showscore", sSpringHelper.getTagStatusChildAttribute(formTag, "showscore", "false"));
			if (hFormDataElements.get("showscore").equals("true")) {
				showScore = true;
			}

			boolean isShowIfRightOrWrong = isShowIfRightOrWrong(formTag);

			//NOTE following list is needed so you can do nested foreach looping! see below.
			List<Hashtable<String,Object>> pieceOrRefpieceDataElementsList = new ArrayList<Hashtable<String,Object>>();
			List<IXMLTag> pieceOrRefpieceTags = getPieceAndRefpieceTags(formTag);
			handlepieceOrRefpieces(pieceOrRefpieceDataElementsList, pieceOrRefpieceTags); 
			hFormDataElements.put("pieces", pieceOrRefpieceDataElementsList);

			//NOTE following list is needed so you can do nested foreach looping! see below.
			List<Hashtable<String,Object>> itemDataElementsList = new ArrayList<Hashtable<String,Object>>();
			//NOTE get all item tags, also non present ones
			List<IXMLTag> itemTags = getAllItemTags(formTag);
			for (IXMLTag itemTag : itemTags) {
				Hashtable<String,Object> hItemDataElements = new Hashtable<String,Object>();
				hItemDataElements.put("tag", itemTag);
				hItemDataElements.put("title", sSpringHelper.getTagChildValue(itemTag, "title", ""));
				hItemDataElements.put("description", sSpringHelper.getTagChildValue(itemTag, "description", ""));
				hItemDataElements.put("src", sSpringHelper.getAbsoluteUrl(itemTag));
				hItemDataElements.put("position", getPosition(itemTag));
				hItemDataElements.put("size", sSpringHelper.getTagChildValue(itemTag, "size", ""));
				hItemDataElements.put("placeable", sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, AppConstants.statusKeyDraggable, "false"));
				hItemDataElements.put("draggable", sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, AppConstants.statusKeyDraggable, "false"));
				hItemDataElements.put("droppable", sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, AppConstants.statusKeyDroppable, "true"));
				if (isShowIfRightOrWrong) {
					hItemDataElements.put("isrightlypositioned", "" + isRightlyPositioned(itemTag));
				}
				else {
					hItemDataElements.put("isrightlypositioned", "");
				}
				hItemDataElements.put("present", sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, AppConstants.statusKeyPresent, "true"));

				itemDataElementsList.add(hItemDataElements);
			}
			hFormDataElements.put("items", itemDataElementsList);

			//NOTE following list is needed so you can do nested foreach looping! see below.
			pieceOrRefpieceDataElementsList = new ArrayList<Hashtable<String,Object>>();
			IXMLTag feedbackConditionTag = getCurrentFeedbackConditionTag(formTag);
			pieceOrRefpieceTags = getPieceAndRefpieceTags(feedbackConditionTag);
			handlepieceOrRefpieces(pieceOrRefpieceDataElementsList, pieceOrRefpieceTags); 
			hFormDataElements.put("feedbackpieces", pieceOrRefpieceDataElementsList);

			dataElements.add(hFormDataElements);
		}
		
		Map<String,Object> propertyMap = new HashMap<String,Object>();
		propertyMap.put("currentEmergoComponent", this);
		propertyMap.put("dataElements", dataElements);
		propertyMap.put("componentStyle", componentStyle);
		propertyMap.put("feedbackButtonUrl", sSpringHelper.stripEmergoPath(getFeedbackButtonUrl()));
		((HtmlMacroComponent)getRunContentComponent()).setDynamicProperty("a_propertyMap", propertyMap);
		((HtmlMacroComponent)getRunContentComponent()).setMacroURI(VView.v_run_graphicalforms_fr);
		getRunContentComponent().setVisible(true);
	}

	public void onShowFeedback(Event aEvent) {
		onShowFeedback(aEvent, "graphicalformItem_", "onUpdate");
	}

	public void onHandleStatusChange(Event aEvent) {
		//NOTE to handle pieces and refpieces call super
		super.onHandleStatusChange(aEvent, "graphicalformItemDiv_");
		STriggeredReference triggeredReference = (STriggeredReference)aEvent.getData();
		if (triggeredReference != null) {
			if (triggeredReference.getDataTag() != null &&
				triggeredReference.getDataTag().getName().equals("point") && 
				(triggeredReference.getStatusKey().equals(AppConstants.statusKeyPresent) ||
				 triggeredReference.getStatusKey().equals(AppConstants.statusKeyDraggable) ||
				 triggeredReference.getStatusKey().equals(AppConstants.statusKeyXposition) ||
				 triggeredReference.getStatusKey().equals(AppConstants.statusKeyYposition))) {
				Component lComponent = CDesktopComponents.vView().getComponent("graphicalformItem_" + triggeredReference.getDataTag().getAttribute(AppConstants.keyId));
				if (lComponent != null) {
					if (triggeredReference.getStatusKey().equals(AppConstants.statusKeyPresent)) {
						lComponent.setVisible(triggeredReference.getStatusValue().equals(AppConstants.statusValueTrue));
					}
					else if (triggeredReference.getStatusKey().equals(AppConstants.statusKeyDraggable)) {
						lComponent.setAttribute("placeable", "" + triggeredReference.getStatusValue().equals(AppConstants.statusValueTrue));
					}
					else if (triggeredReference.getStatusKey().equals(AppConstants.statusKeyXposition)) {
						if (triggeredReference.getSetFromScript()) {
							//NOTE only set position if set from script to prevent an endless loop
							setXPosition(lComponent, triggeredReference.getStatusValue());
						}
					}
					else if (triggeredReference.getStatusKey().equals(AppConstants.statusKeyYposition)) {
						if (triggeredReference.getSetFromScript()) {
							//NOTE only set position if set from script to prevent an endless loop
							setYPosition(lComponent, triggeredReference.getStatusValue());
						}
					}
				}
			}
		}
	}
	
	public void dropItem(Component dragged, Component dropped, int x, int y) {
		String position = (String)dragged.getAttribute("position");
		if (dropped != null && !dropped.getId().equals("")) {
			Integer left = getLeft((String)dropped.getAttribute("position"), (String)dropped.getAttribute("size"));
			Integer top = getTop((String)dropped.getAttribute("position"), (String)dropped.getAttribute("size"));
			if (left != null) {
				x += left.intValue();
			}
			if (top != null) {
				y += top.intValue();
			}
			dropZindex++;
			((HtmlBasedComponent)dragged).setZindex(dropZindex);
		}
		IXMLTag pointTag = (IXMLTag)dragged.getAttribute("tag");
		if (sSpringHelper.getTagChildValue(pointTag, "centerifcorrect", "").equals(AppConstants.statusValueTrue)) {
			//NOTE if point should be centered if correct adjust x and y
			position = centerPositionIfCorrect(pointTag, x, y);
		}
		else {
			position = "" + x + "," + y;
		}

		savePosition(pointTag, position);

		dragged.setAttribute("position", position);
		Events.postEvent("onUpdatePosition", dragged, null);
	}

	public void setXPosition(Component component, String xposition) {
		IXMLTag pointTag = (IXMLTag)component.getAttribute("tag");
		String yposition = sSpringHelper.getCurrentStatusTagStatusChildAttribute(pointTag, AppConstants.statusKeyYposition, "");
		if (xposition.equals("") || yposition.equals("")) {
			return;
		}
		setPosition(component, Integer.parseInt(xposition), Integer.parseInt(yposition));
	}

	public void setYPosition(Component component, String yposition) {
		IXMLTag pointTag = (IXMLTag)component.getAttribute("tag");
		String xposition = sSpringHelper.getCurrentStatusTagStatusChildAttribute(pointTag, AppConstants.statusKeyXposition, "");
		if (xposition.equals("") || yposition.equals("")) {
			return;
		}
		setPosition(component, Integer.parseInt(xposition), Integer.parseInt(yposition));
	}

	public void setPosition(Component component, int x, int y) {
		IXMLTag pointTag = (IXMLTag)component.getAttribute("tag");
		String position = "";
		if (sSpringHelper.getTagChildValue(pointTag, "centerifcorrect", "").equals(AppConstants.statusValueTrue)) {
			//NOTE if point should be centered if correct adjust x and y
			position = centerPositionIfCorrect(pointTag, x, y);
		}
		else {
			position = "" + x + "," + y;
		}

		savePosition(pointTag, position);

		component.setAttribute("position", position);
		Events.postEvent("onUpdatePosition", component, null);
	}

	public void showItem(int x, int y) {
		Component itemToShow = null;
		List<Component> items = vView.getComponentsByPrefix("graphicalformItem_");
		for (Component item : items) {
			if (!item.isVisible() && item.getAttribute("placeable").equals("true")) {
				itemToShow = item;
				break;
			}
		}
		if (itemToShow == null) {
			for (Component item : items) {
				if (item.getAttribute("placeable").equals("true")) {
					itemToShow = item;
					break;
				}
			}
		} 
		if (itemToShow == null) {
			return;
		} 
		dropItem(itemToShow, null, x, y);
		sSpringHelper.setRunTagStatus(this, (IXMLTag)itemToShow.getAttribute("tag"), AppConstants.statusKeyPresent, "true", true);
	}

	public Integer getLeft(String position, String size) {
		return getPositionElement(position, size, 0);
	}

	public Integer getTop(String position, String size) {
		return getPositionElement(position, size, 1);
	}

	public Integer getPositionElement(String position, String size, int index) {
		if (position == null || size == null) {
			return null;
		}
		String[] positionStrArr = position.split(",");
		if (positionStrArr.length == 2 && index >= 0 && index < positionStrArr.length) {
			Long positionElementLong = new Long(runWnd.getNumber(positionStrArr[index], 0));
			String[] sizeArr = size.split(",");
			if (sizeArr.length == 2) {
				Long sizeElementLong = new Long(runWnd.getNumber(sizeArr[index], 0));
				positionElementLong -= Math.round(sizeElementLong / 2);
			}
			return new Integer(positionElementLong.intValue());
		}
		return null;
	}

	public String getStylePosition(String position, String size) {
		if (position == null || size == null) {
			return "";
		}
		String stylePosition = "";
		Integer left = getLeft(position, size);
		Integer top = getTop(position, size);
		if (left != null && top != null) {
			stylePosition += "left:" + left.intValue() + "px;top:" + top.intValue() + "px;";
		}
		if (!stylePosition.equals("")) {
			stylePosition = "position:absolute;" + stylePosition;
		}
		return stylePosition;
	}

	public String getPosition(IXMLTag itemTag) {
		String xposition = sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, AppConstants.statusKeyXposition, "");
		String yposition = sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, AppConstants.statusKeyYposition, "");
		String position = "";
		if (!xposition.equals("") && !yposition.equals("")) {
			position = xposition + "," + yposition;
		}
		return position;
	}

	protected boolean isRightlyPositioned(IXMLTag pointTag) {
		String xposition = sSpringHelper.getCurrentStatusTagStatusChildAttribute(pointTag, AppConstants.statusKeyXposition, "");
		String yposition = sSpringHelper.getCurrentStatusTagStatusChildAttribute(pointTag, AppConstants.statusKeyYposition, "");
		if (xposition.equals("") || yposition.equals("")) {
			return false;
		}
		String correctxrange = sSpringHelper.getTagChildValue(pointTag, "correctxrange", "");
		String correctyrange = sSpringHelper.getTagChildValue(pointTag, "correctyrange", "");
		String left = runWnd.getNumberStrInStr(correctxrange, 0);
		String right = runWnd.getNumberStrInStr(correctxrange, 1);
		String top = runWnd.getNumberStrInStr(correctyrange, 0);
		String bottom = runWnd.getNumberStrInStr(correctyrange, 1);
		boolean correct = true;
		if (!left.equals("") && !right.equals("")) {
			int xpos = Integer.parseInt(xposition);
			correct = correct && (xpos >= Integer.parseInt(left) && xpos <= Integer.parseInt(right));
		}
		if (!top.equals("") && !bottom.equals("")) {
			int ypos = Integer.parseInt(yposition);
			correct = correct && (ypos >= Integer.parseInt(top) && ypos <= Integer.parseInt(bottom));
		}
		return correct;
	}

	protected String centerPositionIfCorrect(IXMLTag pointTag, int x, int y) {
		String correctxrange = sSpringHelper.getTagChildValue(pointTag, "correctxrange", "");
		String correctyrange = sSpringHelper.getTagChildValue(pointTag, "correctyrange", "");
		String left = runWnd.getNumberStrInStr(correctxrange, 0);
		String right = runWnd.getNumberStrInStr(correctxrange, 1);
		String top = runWnd.getNumberStrInStr(correctyrange, 0);
		String bottom = runWnd.getNumberStrInStr(correctyrange, 1);
		boolean correct = true;
		if (!left.equals("") && !right.equals("")) {
			correct = correct && (x >= Integer.parseInt(left) && x <= Integer.parseInt(right));
		}
		if (!top.equals("") && !bottom.equals("")) {
			correct = correct && (y >= Integer.parseInt(top) && y <= Integer.parseInt(bottom));
		}
		if (correct && !left.equals("") && !right.equals("")) {
			x = Integer.parseInt(left) + ((Integer.parseInt(right) - Integer.parseInt(left)) / 2);
		}
		if (correct && !top.equals("") && !bottom.equals("")) {
			y = Integer.parseInt(top) + ((Integer.parseInt(bottom) - Integer.parseInt(top)) / 2);
		}
		return "" + x + "," + y;
	}

}
