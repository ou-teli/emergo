/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.AbstractComponent;
import org.zkoss.zk.ui.event.Event;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CIObserved;
import nl.surf.emergo.control.run.CRunEmpackBtn;
import nl.surf.emergo.control.run.CRunHoverBtn;
import nl.surf.emergo.control.run.CRunLocationsBtn;
import nl.surf.emergo.control.run.CRunNoteBtn;
import nl.surf.emergo.control.run.CRunStatusChange;
import nl.surf.emergo.control.run.CRunTasksBtn;
import nl.surf.emergo.control.run.CRunWnd;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IEComponent;
import nl.surf.emergo.view.VView;

/**
 * The Class CRunWnd. The Emergo player window.
 */
public class CRunWndClassic extends CRunWnd
 {

	private static final long serialVersionUID = 3355532304709749519L;

	/** The run title area to show location, user name and close box. */
	public CRunTitleAreaClassic runTitleArea = null;

	/** The run view area to show location images or content of case components. */
	public CRunViewAreaClassic runViewArea = null;

	/** The run empack btn to show empack action buttons corresponding to casecomponents.
	 *  When clicked it shows the content of a case component within the run view area. */
	public CRunEmpackBtn runEmpackBtn = null;

	/** The run locations btn to show location buttons corresponding to locations within the locations component.
	 *  When clicked it shows the current location image within the run view area. */
	public CRunLocationsBtn runLocationsBtn = null;

	/** The run note btn to show the note window within the run choice area. */
	public CRunNoteBtn runNoteBtn = null;

	/** The run tasks btn to show the content of the tasks component within a popup window. */
	public CRunTasksBtn runTasksBtn = null;

	/** The run choice area to show options which change the content of the run view area. */
	public CRunChoiceAreaClassic runChoiceArea = null;

	/** The p case component tag list. Used to buffer status changes due to script actions. */
	protected List<CRunStatusChange> pCaseComponentTagList = new ArrayList<CRunStatusChange>();

	/** The current location tag id. The tagid of the current location. */
	protected String currentLocationTagId = "";

	/** The run choice area medium.  */
	public boolean runChoiceAreaMed = false;

	/** The run choice area max.  */
	public boolean runChoiceAreaMax = false;

	/** Register the onEmNotify event. Is used to send commands from the client to the server ic CRunWnd, the player. */
	/*
	static {
		// TODO replace by mechanism in Ounl skin
		new CRunEmNotifyCommand("onEmNotify", Command.IGNORE_OLD_EQUIV);
	}
	*/
	
	/**
	 * Instantiates a new c run wnd.
	 * Reads google maps key out of .properties file and set it as session var,
	 * so google maps will work within the player.
	 */
	public CRunWndClassic() {
		super();
	}

	/**
	 * On create:
	 * - save ZK desktop id to use it when ZK desktop is closed
	 * - get request parameter runstatus, see IAppManager
	 * - get request parameter rgaId, the id of the user opening the player
	 * - get request parameter rutId, the possible runteam the user belongs to
	 * - get request parameter cacId, the possible casecomponent to preselect
	 * - get request parameter tagId, the possible tag to preselect
	 * - get request parameter mediacontrol, if windows media player will show controls
	 * - get request parameters for language to use
	 * - start CRunMainTimer
	 * - add rungroupaccount to rungroupaccounts who have an open player, so who are
	 *   playing right now
	 * 
	 * @param aEvent the a event
	 */
	public void onCreate(Event aEvent) {
		super.onCreate(aEvent);
		determineChoiceAreaSize();
	}

	/**
	 * Determines choice area size depending on existance of locations, empack, task and note component
	 * within current case.
	 */
	protected void determineChoiceAreaSize() {
		boolean empackUsed = (sSpring.getCaseComponent("empack", "") != null);
		boolean locationsUsed = (sSpring.getCaseComponent("locations", "") != null);
		boolean noteUsed = (sSpring.getCaseComponent("note", "") != null);
		boolean tasksUsed = (sSpring.getCaseComponent("tasks", "") != null);

		boolean noLeft = false;
		boolean noRight = false;
//		if ((!empackUsed) || (!locationsUsed))
		if ((!empackUsed) && (!locationsUsed))
			noLeft = true;
		if ((!noteUsed) && (!tasksUsed))
			noRight = true;
		if (noLeft && noRight)
			runChoiceAreaMax = true;
		else {
			if (noLeft || noRight)
				runChoiceAreaMed = true;
		}
	}

	/**
	 * Gets choice area layout status.
	 */
	protected String getChoiceAreaLayoutStatus() {
		String lStatus = "Min";
		if (runChoiceAreaMed)
			lStatus = "Med";
		else if (runChoiceAreaMax)
			lStatus = "Max";
		return lStatus;
	}

	/**
	 * Gets player status string.
	 *
	 * @param aKey the a key
	 * @param aStatus the a status
	 *
	 * @return status string
	 */
	protected String getPlayerStatusString(String aKey, Object aStatus) {
		if (aKey.equals("ChoiceAreaLayoutStatus")) {
			return getChoiceAreaLayoutStatus();
		}
		return "";
	}

	/**
	 * Cleanup. Is called when the ZK desktop (the current browser instance)
	 * is closed.
	 * - saves note if note window is opened
	 * - saves all pending status for the user
	 * - removes current rungroupaccount from list of rungroupaccounts who are
	 *   playing right now
	 */
	public synchronized void cleanup(){
		if (saveAtCleanup()) {
			CRunNoteClassic lRunNote = (CRunNoteClassic)CDesktopComponents.vView().getComponent("runNote");
			if (lRunNote != null)
				lRunNote.saveContent();
		}
		super.cleanup();
	}
	
	/**
	 * Creates child components of player.
	 */
	public void createComponents() {
		CRunVboxClassic lVbox = new CRunVboxClassic();
		createViews(lVbox);
		appendChild(lVbox);
	}

	/**
	 * Creates views, the title area, the run view area and the areas beneath it.
	 * 
	 * @param aVbox the a vbox
	 */
	protected void createViews(CRunVboxClassic aVbox) {
		// window is separated in areas: title (for titlebar), view (to show
		// locations and casecomponents) and choice (buttons to show locations
		// or casecomponents)
		// create title area
		runTitleArea = new CRunTitleAreaClassic("runTitleArea");
		aVbox.appendChild(runTitleArea);
		// create view area
		runViewArea = new CRunViewAreaClassic("runViewArea", "empty");
		aVbox.appendChild(runViewArea);
		// create choice area
		CRunHboxClassic lHbox = new CRunHboxClassic();
		createViewAreas(lHbox);
		aVbox.appendChild(lHbox);
	}

	/**
	 * Creates view areas. The areas beneath the run view area.
	 * On the left the empack and navigator button.
	 * On the right the note and tasks button.
	 * And in between the run choice area.
	 * 
	 * @param aHbox the a hbox
	 */
	protected void createViewAreas(CRunHboxClassic aHbox) {
		// choice area is separated in: empack button, navigation button, choice
		// area, note button and tasks button

		String lStatus = "empty";

		CRunVboxClassic lVbox = new CRunVboxClassic();
		// create empack button
		String lShowName = sSpring.getCaseComponentRoleName("empack", "");
		boolean empackUsed = (!lShowName.equals(""));
//		runEmpackBtn = new CRunHoverBtnClassic("runEmpackBtn", lStatus, "showEmpack", "", "side", "empack", lShowName, "");
		runEmpackBtn = new CRunEmpackBtn("runEmpackBtn", lStatus, "showEmpack", "", "empack", lShowName, "");
		runEmpackBtn.registerObserver(this.getId());
		lVbox.appendChild(runEmpackBtn);
		// create navigation button
		lShowName = sSpring.getCaseComponentRoleName("locations", "");
		boolean locationsUsed = (!lShowName.equals(""));
//		runLocationsBtn = new CRunHoverBtnClassic("runLocationsBtn", lStatus, "showLocations", "", "side", "locations", lShowName, "");
		runLocationsBtn = new CRunLocationsBtn("runLocationsBtn", lStatus, "showLocations", "", "locations", lShowName, "");
		runLocationsBtn.registerObserver(this.getId());
		lVbox.appendChild(runLocationsBtn);
		aHbox.appendChild(lVbox);

		lVbox = new CRunVboxClassic();
		// create choice area
		runChoiceArea = new CRunChoiceAreaClassic("runChoiceArea", "empty");
		lVbox.appendChild(runChoiceArea);
		aHbox.appendChild(lVbox);

		lVbox = new CRunVboxClassic();
		// create note button
		lShowName = sSpring.getCaseComponentRoleName("note", "");
		boolean noteUsed = (!lShowName.equals(""));
//		runNoteBtn = new CRunHoverBtnClassic("runNoteBtn", lStatus, "showNote", "", "side", "note", lShowName, "");
		runNoteBtn = new CRunNoteBtn("runNoteBtn", lStatus, "showNote", "", "note", lShowName, "");
		runNoteBtn.registerObserver(this.getId());
		lVbox.appendChild(runNoteBtn);
		// create tasks button
//		String lPage = VView.v_run_tasks;
//		String lStyle = "height=300,width=500";
		String lAction = "";
		lShowName = sSpring.getCaseComponentRoleName("tasks", "");
		boolean tasksUsed = (!lShowName.equals(""));
//		runTasksBtn = new CRunHoverBtnClassic("runTasksBtn", lStatus, "showTasks", "", "side", "tasks", lShowName, lAction);
		runTasksBtn = new CRunTasksBtn("runTasksBtn", lStatus, "showTasks", "", "tasks", lShowName, lAction);
		runTasksBtn.registerObserver(this.getId());
		lVbox.appendChild(runTasksBtn);
		aHbox.appendChild(lVbox);

		if ((!empackUsed) || (!locationsUsed)) {
/*			if (empackUsed) {
				runEmpackBtn.setBtnVisible(true);
				runEmpackBtn.simulateOnClick();
			}
			if (locationsUsed) {
				runLocationsBtn.setBtnVisible(true);
		//		runLocationsBtn.simulateOnClick();
				onTimedAction(getId(), "showLocations", null);
			}
			runEmpackBtn.setVisible(false);
			runLocationsBtn.setVisible(false);
	*/	}
		if (!noteUsed) {
			runNoteBtn.setVisible(false);
		}
		if (!tasksUsed) {
			runTasksBtn.setVisible(false);
		}
	}

	/**
	 * Shows components status.
	 * Is called when the player is started. Restores player status for current user
	 * and depending on runstatus. If preview, preselected casecomponent and tag are
	 * used to set the player in a certain state. Otherwise location is restored to
	 * last location visited during previous session.
	 */
	public void showComponentsStatus() {
		runEmpackBtn.setStatus(getCaseComponentStatus("empack", ""));
		runLocationsBtn.setStatus(getCaseComponentStatus("locations", ""));
		runNoteBtn.setStatus(getCaseComponentStatus("note", ""));
		runTasksBtn.setStatus(getCaseComponentStatus("tasks", ""));
		IXMLTag lSelectedLocationTag = null;
		IECaseComponent lLocationsComponent = null;
		IECaseComponent lEmpackComponent = null;
		IECaseComponent lTasksComponent = null;
		IECaseComponent lNoteComponent = null;
		IECaseComponent lEmpackActionComponent = null;
		if (isRunStatus(AppConstants.runStatusPreview) || isRunStatus(AppConstants.runStatusPreviewReadOnly) || isRunStatus(AppConstants.runStatusTutorRun)) {
			// get possibly selected location tag
			lSelectedLocationTag = getPreselectedLocationTag();
			// get possibly selected locations component
			lLocationsComponent = getPreselectedCaseComponent("locations");
			// get possibly selected tasks component
			lTasksComponent = getPreselectedCaseComponent("tasks");
			// get possibly selected note component
			lNoteComponent = getPreselectedCaseComponent("note");
			// get possibly selected empack action component
			lEmpackActionComponent = getPreselectedEmpackAction();
			if (lEmpackActionComponent == null)
				// get possibly selected empack component
				lEmpackComponent = getPreselectedCaseComponent("empack");
			else {
				runEmpackBtn.simulateOnClick();
				String lId = "empackaction" + lEmpackActionComponent.getCacId() + "Btn";
				CRunHoverBtnClassic lBtn = (CRunHoverBtnClassic) CDesktopComponents.vView().getComponent(lId);
				if (lBtn != null)
					lBtn.simulateOnClick();
				else
					onAction(getId(), "showEmpackAction", "" + lEmpackActionComponent.getCacId());
			}
			if ((lEmpackComponent == null) &&
					(lEmpackActionComponent == null) &&
					(lTasksComponent == null) &&
					(lNoteComponent == null)) {
				// get possibly selected location actions
				List<List<Object>> lLocationActions = getPreselectedLocationActions();
				// show only the one selected if so
				if (lLocationActions.size() > 0) {
					if (lLocationActions.size() == 1)
						onAction(getId(), "showLocationAction", lLocationActions.get(0));
					else
						onAction(getId(), "showLocationActions", null);
				}
				else if ((lLocationsComponent == null) && (lSelectedLocationTag == null)){
					// there is only one instance of the locations component
					lLocationsComponent = sSpring.getCaseComponent("locations", "");
				}
			} 
		}
//		if (isRunStatus(AppConstants.runStatusRun) || isRunStatus(AppConstants.runStatusTutorRun)) {
		if (isRunStatus(AppConstants.runStatusRun)) {
			// there is only one instance of the locations component
			lLocationsComponent = sSpring.getCaseComponent("locations", "");
		}
		if (isRunStatus("demo")) {
		}
		if ((lLocationsComponent != null) || (lEmpackComponent != null) || (lTasksComponent != null) || (lNoteComponent != null) 
				|| (lSelectedLocationTag != null)) {
			// show navigation bar
			if (lLocationsComponent != null)
				if (lSelectedLocationTag != null) {
					// prohibit adding location actions to prevent multiple occurences of the same action 
					pAddActions = false;
				}
			runLocationsBtn.simulateOnClick();
			pAddActions = true;
			// show empack
			if (lEmpackComponent != null)
				runEmpackBtn.simulateOnClick();
			// show tasks
			if (lTasksComponent != null)
				runTasksBtn.simulateOnClick();
			// show location
			if (lSelectedLocationTag != null) {
				String lSelectedTagId = lSelectedLocationTag.getAttribute(AppConstants.keyId);
				List<IXMLTag> lLocations = getLocationTags();
				if (lLocations != null) {
					for (IXMLTag lLocation : lLocations) {
						String lId = "location" + lLocation.getAttribute(AppConstants.keyId) + "Btn";
						CRunHoverBtnClassic lBtn2 = (CRunHoverBtnClassic) CDesktopComponents.vView().getComponent(lId);
						if (lBtn2 != null) {
							String lTagId = (String) lBtn2.getActionStatus();
							if ((lTagId != null) && (lTagId.equals(lSelectedTagId)))
								onTimedAction(lId, "showLocation", lTagId);
						}
					}
				}
			}
			// show note
			if (lNoteComponent != null)
				runNoteBtn.simulateOnClick();
			/*
		*/
		}
	}

	/**
	 * Checks if run status is equal to aStatus.
	 * 
	 * @param aStatus the a status
	 * 
	 * @return true, if is run status
	 */
	protected boolean isRunStatus(String aStatus) {
		String lStatus = sSpring.getRunStatus();
		if (lStatus == null)
			return false;
		return lStatus.equals(aStatus);
	}

	/**
	 * Gets the current case component for notes.
	 * 
	 * @return the current case component for notes
	 */
	public IECaseComponent getCurrentCaseComponentForNotes() {
		return currentCaseComponentForNotes;
	}

	/**
	 * Sets the current case component for notes. Clears current tag for notes.
	 * 
	 * @param aCurrentCaseComponentForNotes the current case component for notes
	 */
	public void setNoteCaseComponent(IECaseComponent aCurrentCaseComponentForNotes) {
		deselectNoteBtn(aCurrentCaseComponentForNotes);
		currentCaseComponentForNotes = aCurrentCaseComponentForNotes;
		currentTagForNotes = null;
		setNoteContent();
	}

	/**
	 * Gets the current tag for notes.
	 * 
	 * @return the current tag for notes
	 */
	public IXMLTag getCurrentTagForNotes() {
		return currentTagForNotes;
	}

	/**
	 * Sets the current tag for notes and current case component for notes.
	 * 
	 * @param aCurrentCaseComponentForNotes the current case component for notes
	 * @param aCurrentTagForNotes the a current tag for notes
	 */
	public void setNoteTag(IECaseComponent aCurrentCaseComponentForNotes, IXMLTag aCurrentTagForNotes) {
		deselectNoteBtn(aCurrentCaseComponentForNotes);
		currentCaseComponentForNotes = aCurrentCaseComponentForNotes;
		currentTagForNotes = aCurrentTagForNotes;
		setNoteContent();
	}

	/**
	 * Deselects note btn.
	 * 
	 * @param aCaseComponentForNotes the case component for notes
	 */
	protected void deselectNoteBtn(IECaseComponent aCaseComponentForNotes) {
		if (runNoteBtn != null) {
			if ((currentCaseComponentForNotes == null) ||
				(currentCaseComponentForNotes.getCacId() != aCaseComponentForNotes.getCacId()))
				runNoteBtn.deselect();
		}
	}

	/**
	 * Sets note content.
	 */
	protected void setNoteContent() {
		CRunNoteClassic lRunNote = (CRunNoteClassic)CDesktopComponents.vView().getComponent("runNote");
		if (lRunNote != null)
			lRunNote.setContent(getCurrentCaseComponentForNotes(), getCurrentTagForNotes());
	}


	/**
	 * Is called by several other components for notification.
	 * Calls method onAction, so see this method for possible aActions.
	 * 
	 * @param aObserved the observed object, it should have interface CIObserved implemented
	 * @param aAction the action
	 * @param aStatus the status object, can be String or other class
	 */
	@Override
	public void observedNotify(CIObserved aObserved, String aAction, Object aStatus) {
		if (aObserved != null)
			onAction(((AbstractComponent) aObserved).getId(), aAction, aStatus);
	}

	/**
	 * On action. Is called by several components change the state of the player.
	 * Possible actions are:
	 * - showLocations: shows location buttons within run choice area
	 * - showLocation: shows location image within run view area 
	 * - beamLocation: simulates click on location button and shows location image
	 * - showEmpack: shows empack action buttons within run choice area
	 * - showEmpackAction: shows content of corresponding case component within run view area
	 * - showLocationActions: shows popup window with possible location actions
	 * - showLocationAction: shows location action within run view area
	 * - endComponent: restores empack action button state when a component shown within the run view area is closed
	 * - showTasks: shows tasks within popup window
	 * - endTasks: restores tasks button state
	 * - showNote: shows note window within run choice area
	 * - endNote: restores note button state
	 * - onEmNotify: used to notify conversations component when user clicks pause/play
	 * - close: saves all pending status for current user
	 * 
	 * @param sender the sender
	 * @param action the action
	 * @param status the status
	 */
	@Override
	public void onAction(String sender, String action, Object status) {
		if (action.equals("showLocations")) {
			// shows location selectors (=navigator bar) in choice area
			IECaseComponent lCaseComponent = sSpring.getCaseComponent("locations", "");
			sSpring.setRunComponentStatus(lCaseComponent,
					AppConstants.statusKeySelected, AppConstants.statusValueTrue,
					true, AppConstants.statusTypeRunGroup, true);
			if (runEmpackBtn != null)
				runEmpackBtn.deselect();
			if (runChoiceArea != null)
				runChoiceArea.setStatus("locations");
			sSpring.setRunComponentStatus(lCaseComponent,
					AppConstants.statusKeyOpened, AppConstants.statusValueTrue,
					true, AppConstants.statusTypeRunGroup, true);
			IXMLTag lLocationTag = getLocation(currentLocationTagId);
			if (lLocationTag == null)
				setNoteCaseComponent(lCaseComponent);
			else
				setNoteTag(lCaseComponent, lLocationTag);
			/*
			*/
		}
		else if (action.equals("beamLocation")) {
			runLocationsBtn.simulateOnClick();
			IXMLTag lLocation = getLocation((String) status);
			String lId = "location" + lLocation.getAttribute(AppConstants.keyId) + "Btn";
			CRunHoverBtnClassic lBtn2 = (CRunHoverBtnClassic) CDesktopComponents.vView().getComponent(lId);
			if (lBtn2 != null) {
				String lTagId = (String) lBtn2.getActionStatus();
				if ((lTagId != null) && (lTagId.equals(status)))
					onAction(lId, "showLocation", lTagId);
			}
		}
		else if (action.equals("showLocation")) {
			// kill pending video
			CRunConversationsClassic lComponent = (CRunConversationsClassic)CDesktopComponents.vView().getComponent("runConversations");
			if (lComponent != null)
				lComponent.removeVideoView();
			// shows location in view area
			CRunLocationBtnsClassic lLocBtns = (CRunLocationBtnsClassic) CDesktopComponents.vView().getComponent("runLocationBtns");
			if (lLocBtns != null) {
				lLocBtns.deselectBtn(sender);
				lLocBtns.selectBtn(sender);
			}
			CRunEmpackActionBtnsClassic lEmBtns = (CRunEmpackActionBtnsClassic) CDesktopComponents.vView().getComponent("runEmpackActionBtns");
			if (lEmBtns != null)
				lEmBtns.deselectBtn("");

			currentLocationTagId = (String) status;
			IXMLTag lLocationTag = getLocation(currentLocationTagId);

			IECaseComponent lCaseComponent = sSpring.getCaseComponent("locations", "");
			String[] lKeys = new String[] { AppConstants.statusKeySelected, AppConstants.statusKeyOpened };
			String[] lValues = new String[] { AppConstants.statusValueTrue, AppConstants.statusValueTrue };
			sSpring.setRunTagStatus(lCaseComponent, lLocationTag.getAttribute(AppConstants.keyId), lLocationTag.getName(),
					lKeys[0], lValues[0], null, true, null, AppConstants.statusTypeRunGroup, true, false);
			sSpring.setRunTagStatus(lCaseComponent, lLocationTag.getAttribute(AppConstants.keyId), lLocationTag.getName(),
					lKeys[1], lValues[1], null, true, null, AppConstants.statusTypeRunGroup, true, false);

			// show location background
			if (runViewArea != null)
				runViewArea.setStatus(sender, action, lLocationTag);

			// show location in title bar
			if (runTitleArea != null)
				runTitleArea.setLocationName(sSpring.unescapeXML(lLocationTag.getChildValue("name")));
//				runTitleArea.setLocationName(sSpring.unescapeXML(lLocationTag.getChildValue("name")) +
//						" (" + sSpring.getRunGroup().getName() + ")");

			setNoteTag(lCaseComponent, lLocationTag);
			
			// show location actions
			onAction(getId(), "showLocationActions", lLocationTag);

		}
		else if (action.equals("showEmpack")) {
			// shows empack component selectors (=empack bar) in choice area
			IECaseComponent lCaseComponent = sSpring.getCaseComponent("empack", "");
			sSpring.setRunComponentStatus(lCaseComponent,
					AppConstants.statusKeySelected, AppConstants.statusValueTrue,
					true, AppConstants.statusTypeRunGroup, true);
			CRunLocationsBtn lBtn = (CRunLocationsBtn) CDesktopComponents.vView().getComponent("runLocationsBtn");
			if (lBtn != null) {
				lBtn.deselect();
				if (runChoiceArea != null)
					runChoiceArea.setStatus("empack");
			}
			sSpring.setRunComponentStatus(lCaseComponent,
					AppConstants.statusKeyOpened, AppConstants.statusValueTrue,
					true, AppConstants.statusTypeRunGroup, true);
			setNoteCaseComponent(lCaseComponent);
		}
		else if (action.equals("showEmpackAction")) {
			// kill pending video
			CRunConversationsClassic lComponent = (CRunConversationsClassic)CDesktopComponents.vView().getComponent("runConversations");
			if (lComponent != null)
				lComponent.removeVideoView();
			// shows empack component in view area
			CRunEmpackActionBtnsClassic lEmBtns = (CRunEmpackActionBtnsClassic) CDesktopComponents.vView().getComponent("runEmpackActionBtns");
			if (lEmBtns != null)
				lEmBtns.deselectBtn(sender);
			IECaseComponent lCaseComponent = sSpring.getCaseComponent(Integer.parseInt((String) status));
			sSpring.setRunComponentStatus(lCaseComponent,
					AppConstants.statusKeySelected, AppConstants.statusValueTrue,
					true, AppConstants.statusTypeRunGroup, true);
			if (runViewArea != null)
				runViewArea.setStatus(sender, action, getEmpackAction((String) status));
			sSpring.setRunComponentStatus(lCaseComponent,
					AppConstants.statusKeyOpened, AppConstants.statusValueTrue,
					true, AppConstants.statusTypeRunGroup, true);
			setNoteCaseComponent(lCaseComponent);
		}
		else if (action.equals("showLocationActions")) {
			// shows location action selectors within popup window above view area
			// if only one action selector, the popup window isn't shown. the
			// location action is shown immediately

			// get location actions
			List<List<Object>> lLocationActions = getLocationActions((IXMLTag) status);
			if (lLocationActions.size() == 1) {
				List<Object> lLocationAction = (List<Object>) lLocationActions.get(0);
				onAction(getId(), "showLocationAction", lLocationAction);
			}
			if (lLocationActions.size() > 1) {
				Map<String,Object> lParams = new HashMap<String,Object>();
				lParams.put("item", lLocationActions);
				CDesktopComponents.vView().modalPopupWithoutWaiting(VView.v_run_location_actions, null, lParams, "center");
			}
		}
		else if (action.equals("showLocationAction")) {
			// shows location action in view area
			CRunEmpackActionBtnsClassic lEmBtns = (CRunEmpackActionBtnsClassic) CDesktopComponents.vView().getComponent("runEmpackActionBtns");
			if (lEmBtns != null)
				lEmBtns.deselectBtn("");
			IECaseComponent lCaseComponent = (IECaseComponent) ((List) status).get(0);
			sSpring.setRunComponentStatus(lCaseComponent,
					AppConstants.statusKeySelected, AppConstants.statusValueTrue,
					true, AppConstants.statusTypeRunGroup, true);
			setNoteCaseComponent(lCaseComponent);
			if (runViewArea != null)
				runViewArea.setStatus(getId(), "showLocationAction", status);
			sSpring.setRunComponentStatus(lCaseComponent,
					AppConstants.statusKeyOpened, AppConstants.statusValueTrue,
					true, AppConstants.statusTypeRunGroup, true);
		}
		else if (action.equals("endComponent")) {
			// shows location in view area after an empack component or location
			// action has been closed
			CRunEmpackActionBtnsClassic lEmBtns = (CRunEmpackActionBtnsClassic) CDesktopComponents.vView().getComponent("runEmpackActionBtns");
			if (lEmBtns != null)
				lEmBtns.deselectBtn("");
			if (runViewArea != null)
				runViewArea.setStatus("location");
			if (runLocationsBtn.getStatus().equals("selected")) {
				IECaseComponent lCaseComponent = sSpring.getCaseComponent("locations", "");
				IXMLTag lLocationTag = getLocation(currentLocationTagId);
				if (lLocationTag == null)
					setNoteCaseComponent(lCaseComponent);
				else
					setNoteTag(lCaseComponent, lLocationTag);
			}
			if (runEmpackBtn.getStatus().equals("selected")) {
				IECaseComponent lCaseComponent = sSpring.getCaseComponent("empack", "");
				setNoteCaseComponent(lCaseComponent);
			}
		}
		else if (action.equals("showTasks")) {
			// shows tasks as modal popup
			IECaseComponent lCaseComponent = sSpring.getCaseComponent("tasks", "");
			if (lCaseComponent != null) {
				sSpring.setRunComponentStatus(lCaseComponent,
						AppConstants.statusKeySelected,
						AppConstants.statusValueTrue, true, AppConstants.statusTypeRunGroup, true);
				sSpring.setRunComponentStatus(lCaseComponent,
						AppConstants.statusKeyOpened,
						AppConstants.statusValueTrue, true, AppConstants.statusTypeRunGroup, true);
				Map<String,Object> lParams = new HashMap<String,Object>();
				lParams.put("item", lCaseComponent);
				CDesktopComponents.vView().modalPopup(VView.v_run_tasks, null, lParams, "center");
			}
		}
		else if (action.equals("endTasks")) {
			IECaseComponent lCaseComponent = sSpring.getCaseComponent("tasks", "");
			if (lCaseComponent != null)
				sSpring.setRunComponentStatus(lCaseComponent,
						AppConstants.statusKeyOpened,
						AppConstants.statusValueFalse, true, AppConstants.statusTypeRunGroup, true);
			// deselect tasks button
			if (runTasksBtn != null)
				runTasksBtn.deselect();
		}
		else if (action.equals("showNote")) {
			// shows note as modal popup
			IECaseComponent lCaseComponent = sSpring.getCaseComponent("note", "");
			if (lCaseComponent != null) {
				sSpring.setRunComponentStatus(lCaseComponent,
						AppConstants.statusKeySelected,
						AppConstants.statusValueTrue, true, AppConstants.statusTypeRunGroup, true);
				sSpring.setRunComponentStatus(lCaseComponent,
						AppConstants.statusKeyOpened,
						AppConstants.statusValueTrue, true, AppConstants.statusTypeRunGroup, true);
				runChoiceArea.setStatus("note");
				setNoteContent();
			}
		}
		else if (action.equals("endNote")) {
			IECaseComponent lCaseComponent = sSpring.getCaseComponent("note", "");
			if (lCaseComponent != null)
				sSpring.setRunComponentStatus(lCaseComponent,
						AppConstants.statusKeyOpened,
						AppConstants.statusValueFalse, true, AppConstants.statusTypeRunGroup, true);
			// deselect note button
			if (runNoteBtn != null)
				runNoteBtn.deselect();
		}
		else if (action.equals("onEmNotify")) {
			if (sender.equals("mediaplayer")) {
				CRunConversationsClassic lComponent = (CRunConversationsClassic) CDesktopComponents.vView().getComponent("runConversations");
				if (lComponent != null)
					lComponent.onAction(sender, action, status);
			}
		}
		else if (action.equals("close")) {
			saveAllStatus();
		}
		else {
			super.onAction(sender, action, status);
		}
	}

	/**
	 * Status change. Is called by SSpring class if script actions have changed status. This status change should
	 * be reflected within the player. Status changes are saved in a buffer and handled by method checkStatusChange.
	 * Only status changes of statusKeyPresent, statusKeyAccessible, statusKeyOpened, statusKeySent and
	 * statusKeyStarted are buffered.
	 * 
	 * @param aCaseComponent the a case component
	 * @param aDataTag the a data tag
	 * @param aStatusTag the a status tag
	 * @param aStatusKey the a status key
	 * @param aStatusValue the a status value
	 * @param aFromScript the a from script, whether this method is called from within script, so is a result of a script action
	 */
	@Override
	public void statusChange(IECaseComponent aCaseComponent, IXMLTag aDataTag, IXMLTag aStatusTag, String aStatusKey, String aStatusValue, boolean aFromScript) {
		if (aStatusKey.equals(AppConstants.statusKeyPresent) ||
				aStatusKey.equals(AppConstants.statusKeyAccessible) ||
				aStatusKey.equals(AppConstants.statusKeyOpened) ||
				aStatusKey.equals(AppConstants.statusKeySent) ||
				aStatusKey.equals(AppConstants.statusKeyStarted)) {
			CRunStatusChange lRunStatusChange = new CRunStatusChange();
			lRunStatusChange.setCaseComponent(aCaseComponent);
			lRunStatusChange.setDataTag(aDataTag);
			lRunStatusChange.setStatusTag(aStatusTag);
			lRunStatusChange.setStatusKey(aStatusKey);
			lRunStatusChange.setStatusValue(aStatusValue);
			lRunStatusChange.setFromScript(aFromScript);
			pCaseComponentTagList.add(lRunStatusChange);
		}
		
	}

	/**
	 * Checks status change. This method reads the status changes buffer and updates the player accordingly,
	 * by updating the different components of the player. Such as navigator, empack, tasks or note button, or
	 * location or empack action buttons, or the content of the run view area.
	 * This method should be made more general in such a way that new Emergo components could be more easyly added.
	 */
	public void checkStatusChange() {
		List<CRunStatusChange> lCaseComponentTagList = sSpring.getExternalStatusChangesPerRgaIdAndDesktopId(this.rgaId, this.desktopId);
		while (lCaseComponentTagList.size() > 0) {
			pCaseComponentTagList.add(lCaseComponentTagList.get(0));
			lCaseComponentTagList.remove(0);
		}
		if ((!pIdle) || (pCaseComponentTagList.size() < 1))
			return;
		pIdle = false;
		while (pCaseComponentTagList.size() >= 1) {
			CRunStatusChange lRunStatusChange = (CRunStatusChange)pCaseComponentTagList.get(0);
			super.checkStatusChange(lRunStatusChange);
			IECaseComponent lCaseComponent = lRunStatusChange.getCaseComponent();
			IXMLTag lDataTag = lRunStatusChange.getDataTag();
			IXMLTag lStatusTag = lRunStatusChange.getStatusTag();
			String lStatusKey = lRunStatusChange.getStatusKey();
			String lStatusValue = lRunStatusChange.getStatusValue();
			boolean lOriginatesFromScript = lRunStatusChange.isFromScript();
			pCaseComponentTagList.remove(0);

			boolean lUpdateOfViewedContent = false;
/*			if (runViewArea.getRunComponentView() != null) {
				if (runViewArea.getRunComponentView().getActRunComponent() != null) {
					if (runViewArea.getRunComponentView().getActRunComponent().getCaseComponent() != null) {
						if (runViewArea.getRunComponentView().getActRunComponent().getCaseComponent().getCacId() == lCaseComponent.getCacId()) {
							lUpdateOfViewedContent = runViewArea.getRunComponentView().getActRunComponent().updateContent(lDataTag, lStatusTag, lStatusKey, lStatusValue);
						}
					}
				}
			} */
			
			if (!lUpdateOfViewedContent) {
				boolean lComp = (lDataTag == null);
				boolean lTag = (lDataTag != null);
				String lTagId = "";
				String lTagName = "";
				if (lTag) {
					lTagId = lDataTag.getAttribute(AppConstants.keyId);
					lTagName = lDataTag.getName();
				}
				String lStatusTagName = "";
				if (lStatusTag != null)
					lStatusTagName = lStatusTag.getName();
				boolean lPresentChanged = lStatusKey.equals(AppConstants.statusKeyPresent);
				boolean lAccessibleChanged = lStatusKey.equals(AppConstants.statusKeyAccessible);
				boolean lOpenedChanged = lStatusKey.equals(AppConstants.statusKeyOpened);
				boolean lOpenedTrue = ((lOpenedChanged) && (lStatusValue.equals(AppConstants.statusValueTrue)));
				boolean lSent = lStatusKey.equals(AppConstants.statusKeySent);
				boolean lStarted = lStatusKey.equals(AppConstants.statusKeyStarted);
				boolean lChangeBtn = (lPresentChanged || lAccessibleChanged);
				boolean lSentTrue = ((lSent) && (lStatusValue.equals(AppConstants.statusValueTrue)));
				String lCode = lCaseComponent.getEComponent().getCode();
				boolean lAlert = ((lSentTrue) && (lCode.equals("alerts")));
				boolean lMail = ((lSentTrue) && (lCode.equals("mail")));
				boolean lAlertTag = (lAlert && (lTagName.equals("alert")));

				boolean lMailTag = (lMail && (!lTagName.equals("map")));
				// lDataTag = null for outmails becoming inmails for other rug's
				boolean lShowMailAlert = ((lMailTag) && ((lStatusTagName.equals("inmailpredef") || lStatusTagName.equals("inmailhelp"))));

				boolean lLocation = (lCode.equals("locations"));
				boolean lLocationTag = (lLocation && (lTagName.equals("location")));
				boolean lLocationBackgroundTag = (lLocation && (lTagName.equals("background")));
				boolean lEmpack = (lCode.equals("empack"));
				boolean lEmpackComp = (lEmpack && lComp);
				boolean lEmpackAction = (caseComponentIsActionOn(lCaseComponent, "empack"));
				boolean lEmpackActionComp = (lEmpackAction && lComp);
				boolean lNote = (lCode.equals("note"));
				boolean lNoteComp = (lNote && lComp);
				boolean lTasks = (lCode.equals("tasks"));
				boolean lTasksComp = (lTasks && lComp);
				boolean lConversations = (lCode.equals("conversations"));
				boolean lConversationTag = (lConversations && (lTagName.equals("conversation")));
				boolean lAssessmentTagPresentChanged = (lPresentChanged && (lCode.equals("assessments")) && lTag);
				String lTagPid = "";
				if (lTag) {
					lTagPid = lDataTag.getChildValue("name");
					if (lTagPid.equals(""))
						lTagPid = lDataTag.getChildValue("pid");
				}
				if (lAlertTag || lShowMailAlert) {
					String lValue = "";
					if (lAlertTag)
						lValue = lDataTag.getChildValue("richtext");
					else
						lValue = CDesktopComponents.vView().getLabel("run_alert.text.mail_alert");
					lValue = sSpring.unescapeXML(lValue);
					Map<String,Object> lParams = new HashMap<String,Object>();
					lParams.put("item", lValue);
					runChoiceArea.setStatus("alert");
					CRunAlertClassic lRunAlert = (CRunAlertClassic)CDesktopComponents.vView().getComponent("runAlert");
					if (lRunAlert != null)
						lRunAlert.setContent(lValue);
					if (lShowMailAlert) {
						CRunMailTreeClassic lRunMailTree = (CRunMailTreeClassic)CDesktopComponents.vView().getComponent("runMailTree");
						if (lRunMailTree != null)
							lRunMailTree.update();
					}
				}
				if (lLocationTag && lChangeBtn) {
					CRunLocationBtnsClassic lLocBtns = (CRunLocationBtnsClassic) CDesktopComponents.vView().getComponent("runLocationBtns");
					if (lLocBtns != null) {
						lLocBtns.updateBtn("location" + lTagId + "Btn", lStatusKey, lStatusValue);
					}
				}
				if (lLocationTag && lOpenedTrue) {
					if (!currentLocationTagId.equals(lDataTag.getAttribute(AppConstants.keyId)))
						onTimedAction(getId(), "beamLocation", lDataTag.getAttribute(AppConstants.keyId));
				}
				if (lEmpackActionComp && lChangeBtn) {
					if (!hasCaseComponentLocationActions(lCaseComponent)) {
						CRunEmpackActionBtnsClassic lEmBtns = (CRunEmpackActionBtnsClassic) CDesktopComponents.vView().getComponent("runEmpackActionBtns");
						if (lEmBtns != null) {
							lEmBtns.updateBtn("empackaction" + lCaseComponent.getCacId() + "Btn", lStatusKey, lStatusValue);
						}
					}
				}
				if (lEmpackComp && lChangeBtn) {
					updateBtn("runEmpackBtn", lStatusKey, lStatusValue);
				}
				if (lNoteComp && lChangeBtn) {
					updateBtn("runNoteBtn", lStatusKey, lStatusValue);
				}
				if (lTasksComp && lChangeBtn) {
					updateBtn("runTasksBtn", lStatusKey, lStatusValue);
				}
				if (lConversations) {
					if (lConversationTag && lStarted) {
						IXMLTag lLocationTag2 = getLocation(currentLocationTagId);
						// conversation can be triggered by another conversation in
						// which case
						// the former is not yet finished, so use timed action
						onTimedAction(getId(), "showLocationActions", lLocationTag2);
					} else {
						CRunConversationsClassic lRunConversations = (CRunConversationsClassic)CDesktopComponents.vView().getComponent("runConversations");
						if (lRunConversations != null){
							lRunConversations.update(lCaseComponent,lDataTag,lStatusKey,lStatusValue);
						}
					}
				}
				if (lEmpackActionComp && lStarted) {
					onTimedAction(getId(), "showEmpackAction", ""+lCaseComponent.getCacId());
				}
				if (lLocationBackgroundTag && lOpenedChanged) {
					// show location background
					if (runViewArea != null) {
						IXMLTag lLocationTag2 = getLocation(lDataTag.getParentTag().getAttribute(AppConstants.keyId));
						if (((String) lLocationTag2.getAttribute(AppConstants.keyId)).equals(currentLocationTagId))
							// only refresh background if background change of current location.
							runViewArea.setLocationBackground(getId(), lLocationTag2);
					}
				}
				if (lAssessmentTagPresentChanged) {
					CRunAssessmentsClassic lRunAssessment = (CRunAssessmentsClassic)CDesktopComponents.vView().getComponent("runAssessments");
					if (lRunAssessment != null) {
						lRunAssessment.setItemTagStatus(lDataTag.getParentTag().getAttribute(AppConstants.keyId), lDataTag.getAttribute(AppConstants.keyId), lStatusKey, lStatusValue);
						lRunAssessment.renderAssessment(lDataTag.getParentTag().getAttribute(AppConstants.keyId));
					}
				} 
			}			
		}
		pIdle = true;
	}

	/**
	 * Checks update external tags. Is called regularly by CRunMainTimer.
	 * Checks if there are updates of other users meant for the current user and if so updatesthe run view area accordingly.
	 * For instance if another user sends an email to the current user, this email should become visible within the
	 * emessages component of the current user. Or if users share a certain component, contributions of a user should become
	 * visible for the other users.  
	 */
	public void checkUpdateExternalTags() {
		if (runViewArea.getRunComponentView() != null) {
			if (runViewArea.getRunComponentView().getActRunComponent() != null) {
				if (runViewArea.getRunComponentView().getActRunComponent().getRunContentComponent() != null) {
					if (runViewArea.getRunComponentView().getActRunComponent().getRunContentComponent() instanceof CRunTreeClassic)
						((CRunTreeClassic)runViewArea.getRunComponentView().getActRunComponent().getRunContentComponent()).updateWithExternalTags();
				}
			}
		}
	}

	/**
	 * Updates button according to status key and value.
	 * 
	 * @param aBtnName the a btn name
	 * @param aStatusKey the a status key
	 * @param aStatusValue the a status value
	 */
	public void updateBtn(String aBtnName, String aStatusKey, String aStatusValue) {
		CRunHoverBtn lBtn = (CRunHoverBtn) CDesktopComponents.vView().getComponent(aBtnName);
		if (lBtn == null)
			return;
		boolean lBool = (aStatusValue.equals(AppConstants.statusValueTrue));
		if (aStatusKey.equals(AppConstants.statusKeyPresent))
			lBtn.setBtnVisible(lBool);
		if (aStatusKey.equals(AppConstants.statusKeyAccessible))
			lBtn.setBtnEnabled(lBool);
		if (aStatusKey.equals(AppConstants.statusKeySelected))
			lBtn.setBtnSelected(lBool);
	}

	/**
	 * Gets the case component status, either by component code or by case component name.
	 * If the case component is visible and/or disabled.
	 * 
	 * @param aComponentCode the a component code of a case component
	 * @param aCaseComponentName the a case component name
	 * 
	 * @return the case component status
	 */
	protected String getCaseComponentStatus(String aComponentCode, String aCaseComponentName) {
		String lStatus = "empty";
		IECaseComponent lCaseComponent = sSpring.getCaseComponent(aComponentCode, aCaseComponentName);
		if (lCaseComponent == null)
			return lStatus;
		IXMLTag lRootTag = sSpring.getXmlDataPlusRunStatusTree(lCaseComponent, AppConstants.statusTypeRunGroup);
		if (lRootTag == null)
			return lStatus;
		IXMLTag lComponentTag = lRootTag.getChild(AppConstants.componentElement);
		if (lComponentTag == null)
			return lStatus;
		boolean lPresent = !lComponentTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse);
		boolean lAccessible = !lComponentTag.getCurrentStatusAttribute(AppConstants.statusKeyAccessible).equals(AppConstants.statusValueFalse);
		boolean lOpened = lComponentTag.getCurrentStatusAttribute(AppConstants.statusKeyOpened).equals(AppConstants.statusValueTrue);
		if ((lPresent) && (lAccessible)) {
			lStatus = "active";
		}
		if ((lPresent) && (!lAccessible))
			lStatus = "inactive";
		return lStatus;
	}

	/**
	 * Gets the selected case component. Is used for preview option. If preview should start with a certain case component.
	 * 
	 * @param aComponentCode the a component code
	 * 
	 * @return the preselected case component or null of case component is not preselected
	 */
	protected IECaseComponent getPreselectedCaseComponent(String aComponentCode) {
		IECaseComponent lCaseComponent = preselectedCaseComponent;
		if (lCaseComponent != null) {
			if (lCaseComponent.getEComponent().getCode().equals(aComponentCode))
				return lCaseComponent;
		}
		return null;
	}

	/**
	 * Gets the case component tags. The child tags of the content tag.
	 * 
	 * @param aCaseComponent the a case component
	 * 
	 * @return the case component tags
	 */
	protected List<IXMLTag> getCaseComponentTags(IECaseComponent aCaseComponent) {
		if (aCaseComponent == null)
			return null;
		IXMLTag lRoot = sSpring.getXmlDataPlusRunStatusTree(aCaseComponent, AppConstants.statusTypeRunGroup);
		if (lRoot == null)
			return null;
		IXMLTag lChild = lRoot.getChild(AppConstants.contentElement);
		if (lChild == null)
			return null;
		return lChild.getChildTags(AppConstants.defValueNode);
	}

	/**
	 * Gets the case component root tag.
	 * 
	 * @param aCaseComponent the a case component
	 * 
	 * @return the case component root tag
	 */
	public IXMLTag getCaseComponentRootTag(IECaseComponent aCaseComponent) {
		if (aCaseComponent == null)
			return null;
		IXMLTag lRoot = sSpring.getXmlDataPlusRunStatusTree(aCaseComponent, AppConstants.statusTypeRunGroup);
		if (lRoot == null)
			return null;
		IXMLTag lChild = lRoot.getChild(AppConstants.contentElement);
		return lChild;
	}

	/**
	 * Gets the location tags of the locations component. There is only one locations component within a case.
	 * 
	 * @return the location tags
	 */
	public List<IXMLTag> getLocationTags() {
		// only one location case component per case per caserole
		IECaseComponent lCaseComponent = sSpring.getCaseComponent("locations", "");
		if (lCaseComponent == null)
			return null;
		return getCaseComponentTags(lCaseComponent);
	}

	/**
	 * Gets the preselected location tag. Is used for preview option. If preview should start with a certain case component tag.
	 * 
	 * @return the preselected location tag
	 */
	protected IXMLTag getPreselectedLocationTag() {
		IXMLTag lTag = preselectedTag;
		if ((lTag != null) && (lTag.getName().equals("location")))
			return lTag;
		return null;
	}

	/**
	 * Gets the location given by aTagId.
	 * 
	 * @param aTagId the a tag id
	 * 
	 * @return the location
	 */
	protected IXMLTag getLocation(String aTagId) {
		if (aTagId == null)
			return null;
		List<IXMLTag> lLocations = getLocationTags();
		if (lLocations == null)
			return null;
		for (IXMLTag lLocation : lLocations) {
			if (aTagId.equals(lLocation.getAttribute(AppConstants.keyId)))
				return lLocation;
		}
		return null;
	}

	/**
	 * Gets the empack action components, a list of case components which are in the empack.
	 * 
	 * @return the empack action components
	 */
	public List<IECaseComponent> getEmpackActionComponents() {
		IECase lCase = sSpring.getCase();
		if (lCase == null)
			return null;
		List<IECaseComponent> lCaseComponents = sSpring.getCaseComponents(lCase);
		if ((lCaseComponents == null) || (lCaseComponents.size() == 0))
			return null;
		List<IECaseComponent> lActions = new ArrayList<IECaseComponent>();
		for (IECaseComponent lCaseComponent : lCaseComponents) {
			if (caseComponentIsActionOn(lCaseComponent, "empack")) {
				if (!hasCaseComponentLocationActions(lCaseComponent))
					lActions.add(lCaseComponent);
			}
		}
		return lActions;
	}

	/**
	 * Has case component location actions. Checks if aCaseComponent has location actions.
	 * 
	 * @param aCaseComponent the case component
	 * 
	 * @return true, if succesfully
	 */
	private boolean hasCaseComponentLocationActions(IECaseComponent aCaseComponent) {
		IECaseComponent lComponent = sSpring.getCaseComponent("locations", "");
		if (lComponent == null)
			// if no locations then no actions on location
			return false;
		List<List<Object>> lLocActions = new ArrayList<List<Object>>();
		List<IXMLTag> lLocationTags = getLocationTags();
		for (IXMLTag lLocationTag : lLocationTags) {
			getCaseComponentLocationActions(aCaseComponent, (lLocationTag != null), lLocationTag, lLocActions);
		}
		return (lLocActions.size() > 0);
	}
	

	/**
	 * Has case component location actions for current location. Checks if aCaseComponent has location actions
	 * for current location.
	 * 
	 * @param aCaseComponent the case component
	 * 
	 * @return true, if succesfully
	 */
	private boolean hasCaseComponentLocationActionsForCurrentLocation(IECaseComponent aCaseComponent) {
		IECaseComponent lComponent = sSpring.getCaseComponent("locations", "");
		if (lComponent == null)
			// if no locations then no actions on location
			return false;
		IXMLTag lLocationTag = getLocation(currentLocationTagId);
		List<List<Object>> lLocActions = new ArrayList<List<Object>>();
		getCaseComponentLocationActions(aCaseComponent, (lLocationTag != null), lLocationTag, lLocActions);
		return (lLocActions.size() > 0);
	}
	

	/**
	 * Gets the preselected empack action. Is used for preview option. If preview should start with a certain case component.
	 * 
	 * @return the preselected empack action or null if none
	 */
	protected IECaseComponent getPreselectedEmpackAction() {
		IECaseComponent lCaseComponent = preselectedCaseComponent;
		if (lCaseComponent != null) {
			if (caseComponentIsActionOn(lCaseComponent, "empack")) {
				if (!hasCaseComponentLocationActions(lCaseComponent))
					return lCaseComponent;
			}
		}
		return null;
	}

	/**
	 * Case component is action on case component given by aParentCode.
	 * 
	 * @param eCaseComponent the e case component
	 * @param aParentCode the a parent code
	 * 
	 * @return true, if successful
	 */
	protected boolean caseComponentIsActionOn(IECaseComponent eCaseComponent, String aParentCode) {
		IEComponent lComponent = eCaseComponent.getEComponent();
		IEComponent lParent = null;
		int parentComId = lComponent.getComComId();
		if (parentComId > 0)
			lParent = sSpring.getComponent(""+parentComId);
		if ((lParent != null) && (lParent.getCode().equals(aParentCode)))
			return true;
		if (aParentCode.equals(""))
			return true;
		return false;
	}

	/**
	 * Gets the empack action, a case component, given by aCacId.
	 * 
	 * @param aCacId the a cac id
	 * 
	 * @return the empack action or null if none
	 */
	protected IECaseComponent getEmpackAction(String aCacId) {
		if (aCacId == null)
			return null;
		List<IECaseComponent> lEmpackActions = getEmpackActionComponents();
		if (lEmpackActions == null)
			return null;
		for (IECaseComponent lCaseComponent : lEmpackActions) {
			if (aCacId.equals("" + lCaseComponent.getCacId())) {
				return lCaseComponent;
			}
		}
		return null;
	}

	/**
	 * Gets the preselected location actions. Is used for preview option. If preview should start with certain location actions.
	 * 
	 * @return the preselected location actions
	 */
	protected List<List<Object>> getPreselectedLocationActions() {
		List<List<Object>> lActions = new ArrayList<List<Object>>();
		// is used for previewing. previewing is done on only 1 casecomponent,
		// saved in session
		IECaseComponent lCaseComponent = preselectedCaseComponent;
		if (lCaseComponent == null)
			return lActions;
		if (!caseComponentIsActionOn(lCaseComponent, "locations")) {
			if (!hasCaseComponentLocationActionsForCurrentLocation(lCaseComponent))
				return lActions;
		}
		// possibly to use casecomponent tag is saved in session
		IXMLTag lTag = preselectedTag;
		// if tag is not null is must be a previewed conversation tag
		if ((lTag != null) && (!lTag.getName().equals("conversation")) && (!lTag.getName().equals(AppConstants.contentElement)))
			return lActions;
		List<Object> lObjects = new ArrayList<Object>();
		lObjects.add(lCaseComponent);
		lObjects.add(lTag);
		lActions.add(lObjects);
		return lActions;
	}

	/** The possible case components on location. Used for buffering in memory. */
	protected List<IECaseComponent> possibleCaseComponentsOnLocation = null;

	/** The locations component. Used for buffering in memory. */
	protected IECaseComponent locationsComponent = null;
	
	/**
	 * Gets the locations component. There is only only one within a case.
	 * 
	 * @return the locations component
	 */
	private IECaseComponent getLocationsComponent() {
		if (locationsComponent == null)
			locationsComponent = sSpring.getCaseComponent("locations", "");
		return locationsComponent;
	}

	/**
	 * Gets current location actions.
	 * 
	 * @param aLocationTag the a location tag
	 * 
	 * @return the location actions
	 */
	protected List<List<Object>> getLocationActions(IXMLTag aLocationTag) {
		List<List<Object>> lActions = new ArrayList<List<Object>>();
		IECase lCase = sSpring.getCase();
		if (lCase == null)
			return lActions;
		if (possibleCaseComponentsOnLocation == null) {
			possibleCaseComponentsOnLocation = sSpring.getCaseComponents(lCase, "conversations");
			possibleCaseComponentsOnLocation.addAll(sSpring.getCaseComponents(lCase, "references"));
			possibleCaseComponentsOnLocation.addAll(sSpring.getCaseComponents(lCase, "assessments"));
		}
		if (possibleCaseComponentsOnLocation == null)
			return lActions;
		boolean lFilterOnLocation = (aLocationTag != null);
		// if no filtering on location get all presentable location actions
		// in case one does not use a location component and for previewing
		// location actions
		for (IECaseComponent lCaseComponent : possibleCaseComponentsOnLocation) {
			getCaseComponentLocationActions(lCaseComponent, lFilterOnLocation, aLocationTag, lActions);
		}
		return lActions;
	}

	/**
	 * Gets the case component location actions.
	 * 
	 * @param aCaseComponent the case component
	 * @param aFilterOnLocation filter on location, if not give all
	 * @param aLocationTag the location tag
	 * @param aActions used to return the actions
	 */
	protected void getCaseComponentLocationActions(IECaseComponent aCaseComponent, boolean aFilterOnLocation, IXMLTag aLocationTag, List<List<Object>> aActions) {
		boolean lConversationTags = (aCaseComponent.getEComponent().getCode().equals("conversations"));
		boolean lComponentRootTag = ((aCaseComponent.getEComponent().getCode().equals("references")) ||
				(aCaseComponent.getEComponent().getCode().equals("assessments")));
		if (!(lConversationTags || lComponentRootTag))
			return;
		// casecomponent has to be present
		boolean lCacPresent = (!(sSpring.getCurrentRunComponentStatus(
				aCaseComponent, AppConstants.statusKeyPresent, AppConstants.statusTypeRunGroup))
				.equals(AppConstants.statusValueFalse));
		if (!lCacPresent)
			return;
		String lCrossReferenceId = "";
		if (lConversationTags)
			lCrossReferenceId = "locationtags_conversationtags";
		if (lComponentRootTag)
			lCrossReferenceId = "locationtags_componentroot";
		List<String> lTagIds = null;
		if (aFilterOnLocation) {
			// get tag ids coupled to location
			lTagIds = cCaseHelper.getTagRefIds(lCrossReferenceId, "" + AppConstants.statusKeySelectedIndex,
					sSpring.getSCaseRoleHelper().getCaseRole(), getLocationsComponent(), aLocationTag, false);
		}
		List<IXMLTag> lTags = null;
		if (lConversationTags)
			lTags = getCaseComponentTags(aCaseComponent);
		if (lComponentRootTag) {
			IXMLTag lRootTag = getCaseComponentRootTag(aCaseComponent);
			lTags = new ArrayList<IXMLTag>();
			lTags.add(lRootTag);
		}
		if (lTags != null) {
			for (IXMLTag lTag : lTags) {
				String lTagId = lTag.getAttribute(AppConstants.keyId);
				String lStatus = sSpring.getCurrentTagStatus(lTag, AppConstants.statusKeyPresent);
				boolean lPresent = true;
				if (lConversationTags)
					// conversation tag has to be present
					lPresent = (lStatus.equals(AppConstants.statusValueTrue));
				if (lComponentRootTag)
					// root tag is always present
					;
				boolean lOnLocation = false;
				if (aFilterOnLocation) {
					if (lPresent) {
						for (String lIds : lTagIds) {
							String[] lIdArr = lIds.split(",");
							// TagId and CacId equal!
							if ((lIdArr[2].equals(lTagId)) && (lIdArr[1].equals("" + aCaseComponent.getCacId())))
								lOnLocation = true;
						}
					}
				} else
					lOnLocation = true;
				if (lPresent && lOnLocation) {
					List<Object> lObjects = new ArrayList<Object>();
					lObjects.add(aCaseComponent);
					if (lConversationTags) {
						lObjects.add(lTag);
						String lName = sSpring.unescapeXML(lTag.getChildValue("name"));
						if (lName.equals("")) {
							lName = sSpring.getXmlManager().getTagKeyValues(lTag, lTag.getDefAttribute(AppConstants.defKeyKey));
						}
						lObjects.add(lName);
					}
					if (lComponentRootTag) {
						lObjects.add(null);
						lObjects.add(sSpring.getCaseComponentRoleName(aCaseComponent));
					}
					aActions.add(lObjects);
				}
			}
		}
	}

	/**
	 * Gets the mediaplayer stop action. The javascript to stop mediaplayer from playing.
	 * 
	 * @return the mediaplayer stop action
	 */
	public String getMediaplayerStopAction() {
		return "if (document.mediaplayer != null) document.mediaplayer.controls.pause();";
	}

}
