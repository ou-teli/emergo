/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.spl;

import nl.surf.emergo.control.run.ounl.CRunComponentDecoratorOunl;
import nl.surf.emergo.control.run.ounl.CRunVideomanualOunl;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunVideomanualSpl is used to show the videomanual within the run view area of the
 * Emergo player.
 */
public class CRunVideomanualSpl extends CRunVideomanualOunl {

	private static final long serialVersionUID = -1775672740989277974L;

	/**
	 * Instantiates a new c run videomanual.
	 */
	public CRunVideomanualSpl() {
		super();
	}

	/**
	 * Instantiates a new c run videomanual.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the videomanual case component
	 */
	public CRunVideomanualSpl(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
	}

	@Override
	protected CRunComponentDecoratorOunl createDecorator() {
		return new CRunComponentDecoratorSpl();
	}

}
