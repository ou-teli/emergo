/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.IP2;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Image;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CIObserved;
import nl.surf.emergo.control.run.CRunArea;
import nl.surf.emergo.control.run.CRunDashboard;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.zkspring.STriggeredReference;

public class CRunDashboardDiv extends CRunArea {

	private static final long serialVersionUID = -2179323727857046878L;

	protected CRunDashboard runDashboard = null;
	
	protected double maxSatisfactionScore = 5.0;

	protected String valueNotStarted = "0";

	protected String valueBusy = "1";
	
	protected String valueReady = "2";
	
	protected String assignmentNotFinishedTime = "-1";
	
	protected double performanceFactorGood = 2.0;
	
	protected double performanceFactorMedium = 1.5;
	
	protected double performanceFactorBad = 1.0;
	
	protected double errorIndicatorGoodNormal = 0.16666667;
	
	protected double errorIndicatorMediumNormal = 0.33333333;
	
	protected double errorIndicatorGoodTextfragmentselector = 0.33333333;
	
	protected double errorIndicatorMediumTextfragmentselector = 0.66666667;
	
	
	protected String relativePath = "";

	
	/*
	 * Optie om efficienter te maken, maar lijkt niet nodig want snel genoeg:
	 * Als dashboard wordt gesloten, hide dan alleen en laat case component bestaan.
	 * Dan alleen initieel de te tonen waarden bepalen en daarna dmv notificaties waarden iedere keer updaten.
	 * Als de dashboard dan wordt geopend worden de waarden opnieuw gerenderd. De waarden zelf zijn van tevoren al bepaald.
	 * Als de dashboard open is, kunnen de waarden niet wijzigen, zoals bij de scores component, alleen de tijd moet meelopen.
	 * Daartoe ofwel een template conditie toevoegen die bij updates van states met key "OPER_*" een status attribute van de dashboard zet op de state key
	 * waarop de dashboard dan weer kan reageren om een of meerdere waarden te updaten.
	 * Het zou ook kunnen door SSpring klasse uit te breiden met Observer/Observed pattern waarop andere componenten zich kunnen abonneren.
	 * Dit zodat de template conditie niet te veel overhead geeft.
	 * Er zouden dan verschillende typen events kunnen zijn waarop je je kunt abonneren, bijv. state veranderingen. Dan worden wel te veel notificaties gestuurd,
	 * omdat alleen "OPER_*" gewenst is. Hoewel ook het afronden van een opdracht dus "O_?" moet een notificatie van komen. Of je abonneert je op componenten van een bepaald type.
	 * De notificaties naar de componenten zouden ook via het event mechanisme van ZK kunnen lopen.
	 */

	/* Skills */
	/* Fields are skill id, type of skill (Content related or General), skill label, maxNumberOfPoints, numberOfPoints. */
	protected String[][] skills = new String[][]
			{
				{"1", "C", "Diagnostiek & testen", "0", "0"},
				{"2", "C", "Coaching", "0", "0"},
				{"3", "C", "Therapie", "0", "0"},
				{"4", "C", "Interventie ontwikkeling", "0", "0"},
				{"5", "G", "Conceptueel leren", "0", "0"},
				{"6", "G", "Informatie vaardigheden", "0", "0"},
				{"7", "G", "Zelfreflectie", "0", "0"},
				{"8", "G", "Kennismaking", "0", "0"}
			};
	
	public String[][] getSkills() {
		return skills;
	}

	protected List<String> getSkillDataList(String typeOfSkill, String typeOfData) {
		List<String> data = new ArrayList<String>();
		for (int i=0;i<skills.length;i++) {
			if (StringUtils.isEmpty(typeOfSkill) || skills[i][1].equals(typeOfSkill)) {
				data.add(getSkillData(i, typeOfData));
			}
		}
		return data;
	}

	public String getSkillDataById(String id, String typeOfData) {
		for (int i=0;i<skills.length;i++) {
			if (!StringUtils.isEmpty(id) && skills[i][0].equals(id)) {
				return getSkillData(i, typeOfData);
			}
		}
		return "";
	}

	public String getSkillData(int index, String typeOfData) {
		if (index < 0 || index >= skills.length) {
			return "";
		}
		if (typeOfData.equals("id")) {
			return skills[index][0];
		}
		else if (typeOfData.equals("type")) {
			return skills[index][1];
		}
		else if (typeOfData.equals("label")) {
			return skills[index][2];
		}
		else if (typeOfData.equals("maxNumberOfPoints")) {
			return skills[index][3];
		}
		else if (typeOfData.equals("numberOfPoints")) {
			return skills[index][4];
		}
		return "";
	}

	protected int getSkillIndex(String id) {
		for (int i=0;i<skills.length;i++) {
			if (!StringUtils.isEmpty(id) && skills[i][0].equals(id)) {
				return i;
			}
		}
		return -1;
	}

	public void setSkillDataById(String id, String typeOfData, String data) {
		for (int i=0;i<skills.length;i++) {
			if (!StringUtils.isEmpty(id) && skills[i][0].equals(id)) {
				setSkillData(i, typeOfData, data);
				return;
			}
		}
	}

	public void setSkillData(int index, String typeOfData, String data) {
		if (index < 0 || index >= skills.length) {
			return;
		}
		if (typeOfData.equals("id")) {
			skills[index][0] = data;
		}
		else if (typeOfData.equals("type")) {
			skills[index][1] = data;
		}
		else if (typeOfData.equals("label")) {
			skills[index][2] = data;
		}
		else if (typeOfData.equals("maxNumberOfPoints")) {
			skills[index][3] = data;
		}
		else if (typeOfData.equals("numberOfPoints")) {
			skills[index][4] = data;
		}
	}

	  /*
	  Per opdracht moet het volgende bekend zijn:
	  - id: komt overeen met bal en opdrachtnummer
	  - afgerond: state O_[$opdrachtnummer$] van case component B_[$balid$] states
	  - omschrijving: dit is gelijk aan de titel van de doorgang in het opdrachtoverzicht
	  - vaardigheden: 1 of meer vaardigheden die tijdens de opdracht aan bod komen. Plus een weegfactor per vaardigheid? Als het er meer dan 1 is?
	  - statusvariabelen: 1 of meer statusvariabelen die de performance aangeven. Per statusvariabele een weegfactor, een type, het aantal alternatieven of open vragen, het goede alternatief
	  	of het aantal goede alternatieven bij meer uit meer
	  
	  */
	
	/* Assignments */
	/* Fields are assignment id, finishedtime (case time when assignment was finished), title, specialist, performanceFactor, niceScore. */
	protected String[][] assignments = new String[][]
			{
				{"AO1_1", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"AO1_2", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"AO1_3", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"AO1_4", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"AO2_1", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"AO2_2", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"AO2_3", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"AO2_4", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"AO3_1", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"AO3_2", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"AO3_3", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"AO3_4", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"AO4_1", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"AO4_2", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"AO4_3", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"AO4_4", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"GP1_1", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"GP1_2", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"GP1_3", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"GP1_4", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"GP2_1", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"GP2_2", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"GP2_3", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"GP2_4", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"GP3_1", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"GP3_2", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"GP3_3", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"GP3_4", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"GP4_1", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"GP4_2", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"GP4_3", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"GP4_4", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"KP1_1", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"KP1_2", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"KP1_3", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"KP1_4", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"KP2_1", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"KP2_2", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"KP2_3", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"KP2_4", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"KP3_1", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"KP3_2", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"KP3_3", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"KP3_4", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"KP4_1", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"KP4_2", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"KP4_3", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"KP4_4", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"LL1_1", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"LL1_2", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"LL1_3", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"LL1_4", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"LL2_1", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"LL2_2", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"LL2_3", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"LL2_4", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"LL3_1", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"LL3_2", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"LL3_3", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"LL3_4", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"LL4_1", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"LL4_2", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"LL4_3", assignmentNotFinishedTime, "", "", "0.0", "0.0"},
				{"LL4_4", assignmentNotFinishedTime, "", "", "0.0", "0.0"}
			};
	
	protected List<String> getAssignmentDataList(String typeOfData) {
		List<String> data = new ArrayList<String>();
		for (int i=0;i<assignments.length;i++) {
			data.add(getAssignmentData(i, typeOfData));
		}
		return data;
	}

	public String getAssignmentDataById(String id, String typeOfData) {
		for (int i=0;i<assignments.length;i++) {
			if (!StringUtils.isEmpty(id) && assignments[i][0].equals(id)) {
				return getAssignmentData(i, typeOfData);
			}
		}
		return "";
	}

	public String getAssignmentData(int index, String typeOfData) {
		if (index < 0 || index >= assignments.length) {
			return "";
		}
		if (typeOfData.equals("id")) {
			return assignments[index][0];
		}
		else if (typeOfData.equals("finishedtime")) {
			return assignments[index][1];
		}
		else if (typeOfData.equals("title")) {
			return assignments[index][2];
		}
		else if (typeOfData.equals("specialist")) {
			return assignments[index][3];
		}
		else if (typeOfData.equals("performanceFactor")) {
			return assignments[index][4];
		}
		else if (typeOfData.equals("niceScore")) {
			return assignments[index][5];
		}
		return "";
	}

	public void setAssignmentDataById(String id, String typeOfData, String data) {
		for (int i=0;i<assignments.length;i++) {
			if (!StringUtils.isEmpty(id) && assignments[i][0].equals(id)) {
				setAssignmentData(i, typeOfData, data);
				return;
			}
		}
	}

	public void setAssignmentData(int index, String typeOfData, String data) {
		if (index < 0 || index >= assignments.length) {
			return;
		}
		if (typeOfData.equals("id")) {
			assignments[index][0] = data;
		}
		else if (typeOfData.equals("finishedtime")) {
			assignments[index][1] = data;
		}
		else if (typeOfData.equals("title")) {
			assignments[index][2] = data;
		}
		else if (typeOfData.equals("specialist")) {
			assignments[index][3] = data;
		}
		else if (typeOfData.equals("performanceFactor")) {
			assignments[index][4] = data;
		}
		else if (typeOfData.equals("niceScore")) {
			assignments[index][5] = data;
		}
	}

	/* Assignment skills */
	/* Fields are per assignment arrays of skill id and number of points. */
	/* NOTE: assignmentskills array has same length as assignments */
	protected String[][][] assignmentskills = new String[][][]
			{
				/* AO1_1 */	{{"8", "2"}},
				/* AO1_2 */	{{"5", "2"}},
				/* AO1_3 */	{{"1", "1"},{"2", "1"}},
				/* AO1_4 */	{{"1", "1"},{"5", "1"}},
				/* AO2_1 */	{{"8", "2"}},
				/* AO2_2 */	{{"1", "1"},{"6", "1"}},
				/* AO2_3 */	{{"7", "2"}},
				/* AO2_4 */	{{"2", "2"}},
				/* AO3_1 */	{{"8", "2"}},
				/* AO3_2 */	{{"6", "2"}},
				/* AO3_3 */	{{"1", "1"},{"5", "1"}},
				/* AO3_4 */	{{"1", "1"},{"5", "1"}},
				/* AO4_1 */	{{"1", "0"}},
				/* AO4_2 */	{{"1", "0"}},
				/* AO4_3 */	{{"1", "0"}},
				/* AO4_4 */	{{"1", "0"}},
				/* GP1_1 */	{{"8", "2"}},
				/* GP1_2 */	{{"5", "1"},{"6", "1"}},
				/* GP1_3 */	{{"7", "2"}},
				/* GP1_4 */	{{"3", "2"}},
				/* GP2_1 */	{{"8", "2"}},
				/* GP2_2 */	{{"4", "1"},{"5", "1"}},
				/* GP2_3 */	{{"4", "1"},{"7", "1"}},
				/* GP2_4 */	{{"4", "2"}},
				/* GP3_1 */	{{"8", "2"}},
				/* GP3_2 */	{{"4", "1"},{"5", "1"}},
				/* GP3_3 */	{{"4", "1"},{"7", "1"}},
				/* GP3_4 */	{{"4", "2"}},
				/* GP4_1 */	{{"1", "0"}},
				/* GP4_2 */	{{"1", "0"}},
				/* GP4_3 */	{{"1", "0"}},
				/* GP4_4 */	{{"1", "0"}},
				/* KP1_1 */	{{"8", "2"}},
				/* KP1_2 */	{{"1", "1"},{"6", "1"}},
				/* KP1_3 */	{{"1", "2"}},
				/* KP1_4 */	{{"1", "1"},{"6", "1"}},
				/* KP2_1 */	{{"8", "2"}},
				/* KP2_2 */	{{"1", "2"}},
				/* KP2_3 */	{{"1", "1"},{"7", "1"}},
				/* KP2_4 */	{{"1", "2"}},
				/* KP3_1 */	{{"3", "1"},{"5", "1"}},
				/* KP3_2 */	{{"3", "1"},{"5", "1"}},
				/* KP3_3 */	{{"7", "2"}},
				/* KP3_4 */	{{"1", "1"},{"3", "1"}},
				/* KP4_1 */	{{"1", "0"}},
				/* KP4_2 */	{{"1", "0"}},
				/* KP4_3 */	{{"1", "0"}},
				/* KP4_4 */	{{"1", "0"}},
				/* LL1_1 */	{{"8", "2"}},
				/* LL1_2 */	{{"1", "1"},{"5", "1"}},
				/* LL1_3 */	{{"7", "2"}},
				/* LL1_4 */	{{"1", "2"}},
				/* LL2_1 */	{{"1", "1"},{"5", "1"}},
				/* LL2_2 */	{{"5", "1"},{"6", "1"}},
				/* LL2_3 */	{{"7", "2"}},
				/* LL2_4 */	{{"7", "2"}},
				/* LL3_1 */	{{"2", "1"},{"5", "1"}},
				/* LL3_2 */	{{"2", "2"}},
				/* LL3_3 */	{{"7", "2"}},
				/* LL3_4 */	{{"2", "2"}},
				/* LL4_1 */	{{"1", "0"}},
				/* LL4_2 */	{{"1", "0"}},
				/* LL4_3 */	{{"1", "0"}},
				/* LL4_4 */	{{"1", "0"}}
			};
	
	protected String getAssignmentSkillData(int index1, int index2, String typeOfData) {
		if (index1 < 0 || index1 >= assignmentskills.length) {
			return "";
		}
		if (index2 < 0 || index2 >= assignmentskills[index1].length) {
			return "";
		}
		if (typeOfData.equals("skillId")) {
			return assignmentskills[index1][index2][0];
		}
		else if (typeOfData.equals("numberOfPoints")) {
			return assignmentskills[index1][index2][1];
		}
		return "";
	}

	protected double[] getSkillMaxNumberOfPointsArr() {
		double[] data = new double[skills.length];
		for (int i=0;i<assignmentskills.length;i++) {
			for (int j=0;j<assignmentskills[i].length;j++) {
				int index = getSkillIndex(getAssignmentSkillData(i, j, "skillId"));
				if (index >= 0 && index < skills.length) {
					//Multiply by factor 2 because performance will multiply number of points by maximal a factor 2
					data[index] += 2.0 * Double.parseDouble(getAssignmentSkillData(i, j, "numberOfPoints"));
				}
			}
		}
		return data;
	}

	/* Assignment states */
	/* Fields are per assignment arrays of state tag key, performance type, number of alternatives or number of open questions, number of correct alternatives or correct alternative, performance weight. */
	/* NOTE: assignmentstates array has same length as assignments */
	protected String[][][] assignmentstates = new String[][][]
			{
				{	/* AO1_1 */
					{"OPER_1_1_V", "Profielvragen", "28", "14", "0.5"},
					{"OPER_1_1", "Profiel", "28", "14", "0.5"}
				},
				{	/* AO1_2 */
					{"OPER_2_1_F_2", "MCvraag", "3", "1", "1"}
				},
				{	/* AO1_3 */
					{"OPER_3_1", "Videosceneselector", "14", "5", "1"}
				},
				{	/* AO1_4 */
					{"OPER_4_1_F_2", "MCvraag", "2", "1", "0.25"},
					{"OPER_4_1_F_3", "MCvraag", "2", "2", "0.25"},
					{"OPER_4_1_F_4", "MCvraag", "2", "1", "0.25"},
					{"OPER_4_1_F_5", "MCvraag", "2", "2", "0.25"}
				},
				{	/* AO2_1 */
					{"OPER_1_1_V", "Profielvragen", "28", "14", "0.5"},
					{"OPER_1_1", "Profiel", "28", "14", "0.5"}
				},
				{	/* AO2_2 */
					{"OPER_2_1_1", "EditformsMultiselect", "10", "5", "0.6667"},
					{"OPER_2_1_F_1", "MC vraag", "5", "5", "0.3333"}
				},
				{	/* AO2_3 */
					{"OPER_3_1", "Reflectie", "", "", "1"},
				},
				{	/* AO2_4 */
					{"OPER_4_1_F_1", "MCvraag", "3", "1", "0.1667"},
					{"OPER_4_1_F_2", "MCvraag", "3", "3", "0.1667"},
					{"OPER_4_1_F_3", "MCvraag", "3", "1", "0.1666"},
					{"OPER_4_1_F_4", "MCvraag", "3", "3", "0.1667"},
					{"OPER_4_1_F_5", "MCvraag", "3", "2", "0.1667"},
					{"OPER_4_1_F_6", "MCvraag", "3", "1|2", "0.1666"}
				},
				{	/* AO3_1 */
					{"OPER_1_1_V", "Profielvragen", "28", "14", "0.5"},
					{"OPER_1_1", "Profiel", "28", "14", "0.5"}
				},
				{	/* AO3_2 */
					{"OPER_2_1_A", "EditformsOpenvragen", "Geen controle", "-", "-"},
					{"OPER_2_1_F_1", "MCvraag", "2", "2", "0.3333"},
					{"OPER_2_1_F_2", "MCvraag", "2", "1", "0.3333"},
					{"OPER_2_1_F_3", "MCvraag", "2", "2", "0.3334"}
				},
				{	/* AO3_3 */
				},
				{	/* AO3_4 */
					{"OPER_4_1_F_1", "MCvraag", "3", "2", "0.5"},
					{"OPER_4_1_F_2", "MCvraag", "3", "3", "0.5"}
				},
				{	/* AO4_1 */
				},
				{	/* AO4_2 */
				},
				{	/* AO4_3 */
				},
				{	/* AO4_4 */
				},
				{	/* GP1_1 */
					{"OPER_1_1_V", "Profielvragen", "28", "14", "0.5"},
					{"OPER_1_1", "Profiel", "28", "14", "0.5"}
				},
				{	/* GP1_2 */
					{"OPER_2_1_F_1", "MCvraag", "5", "3", "0.1"},
					{"OPER_2_1_F_2", "MCvraag", "4", "1", "0.1"},
					{"OPER_2_1_F_3", "MCvraag", "4", "4", "0.1"},
					{"OPER_2_1_F_4", "MCvraag", "4", "2", "0.1"},
					{"OPER_2_1_G_1", "MCvraag", "6", "1", "0.1"},
					{"OPER_2_1_G_2", "MCvraag", "5", "5", "0.1"},
					{"OPER_2_1_G_3", "MCvraag", "4", "3", "0.1"},
					{"OPER_2_1_G_4", "MCvraag", "3", "1", "0.1"},
					{"OPER_2_1_G_5", "MCvraag", "2", "2", "0.1"},
					{"OPER_2_1_G_6", "MCvraag", "1", "1", "0.1"}
				},
				{	/* GP1_3 */
					{"OPER_3_1", "Reflectie", "", "", "1"},
				},
				{	/* GP1_4 */
					{"OPER_4_1_G_2", "MCvraag", "2", "1", "0.1111"},
					{"OPER_4_1_G_3", "MCvraag", "2", "2", "0.1111"},
					{"OPER_4_1_G_4", "MCvraag", "2", "1", "0.1111"},
					{"OPER_4_1", "Dragdropforms", "6", "6", "0.3334"},
					{"OPER_4_2_F_1", "MCvraag", "3", "1", "0.1111"},
					{"OPER_4_2_F_2", "MCvraag", "2", "2", "0.1111"},
					{"OPER_4_2_F_3", "MCvraag", "3", "3", "0.1111"}
				},
				{	/* GP2_1 */
					{"OPER_1_1_V", "Profielvragen", "28", "14", "0.5"},
					{"OPER_1_1", "Profiel", "28", "14", "0.5"}
				},
				{	/* GP2_2 */
					{"OPER_2_1_F_1", "MCvraag", "4", "1", "0.2"},
					{"OPER_2_1", "Dragdropforms", "10", "10", "0.8"}
				},
				{	/* GP2_3 */
					{"OPER_3_1", "Reflectie", "", "", "1"},
				},
				{	/* GP2_4 */
					{"OPER_4_1_F_1", "MCvraag", "3", "2", "0.3333"},
					{"OPER_4_1_1", "EditformsMultiselect", "5", "2", "0.6667"}
				},
				{	/* GP3_1 */
					{"OPER_1_1_V", "Profielvragen", "28", "14", "0.5"},
					{"OPER_1_1", "Profiel", "28", "14", "0.5"}
				},
				{	/* GP3_2 */
					{"OPER_2_1_F_1", "MCvraag", "2", "1", "0.0167"},
					{"OPER_2_1_F_2", "MCvraag", "2", "2", "0.0167"},
					{"OPER_2_1_F_3", "MCvraag", "2", "1", "0.0167"},
					{"OPER_2_1_F_4", "MCvraag", "2", "1", "0.0167"},
					{"OPER_2_1_F_5", "MCvraag", "2", "2", "0.0167"},
					{"OPER_2_1_F_6", "MCvraag", "2", "1", "0.0167"},
					{"OPER_2_1_F_7", "MCvraag", "2", "2", "0.0167"},
					{"OPER_2_1_F_8", "MCvraag", "2", "2", "0.0167"},
					{"OPER_2_1_F_9", "MCvraag", "2", "1", "0.0167"},
					{"OPER_2_1_F_10", "MCvraag", "2", "1", "0.0167"},
					{"OPER_2_2_1", "EditformsSingleselect", "8", "7", "0.046"},
					{"OPER_2_2_2", "EditformsSingleselect", "9", "7", "0.046"},
					{"OPER_2_2_3", "EditformsSingleselect", "12", "2", "0.047"},
					{"OPER_2_2_4", "EditformsSingleselect", "8", "4", "0.046"},
					{"OPER_2_2_5", "EditformsSingleselect", "9", "8", "0.046"},
					{"OPER_2_2_6", "EditformsSingleselect", "12", "2", "0.047"},
					{"OPER_2_2_7", "EditformsSingleselect", "8", "8", "0.046"},
					{"OPER_2_2_8", "EditformsSingleselect", "9", "2", "0.046"},
					{"OPER_2_2_9", "EditformsSingleselect", "12", "12", "0.047"},
					{"OPER_2_2_10", "EditformsSingleselect", "8", "2", "0.046"},
					{"OPER_2_2_11", "EditformsSingleselect", "9", "7", "0.046"},
					{"OPER_2_2_12", "EditformsSingleselect", "12", "11", "0.047"},
					{"OPER_2_2_13", "EditformsSingleselect", "8", "3", "0.046"},
					{"OPER_2_2_14", "EditformsSingleselect", "9", "7", "0.046"},
					{"OPER_2_2_15", "EditformsSingleselect", "12", "2", "0.047"},
					{"OPER_2_2_16", "EditformsSingleselect", "8", "7", "0.046"},
					{"OPER_2_2_17", "EditformsSingleselect", "9", "7", "0.046"},
					{"OPER_2_2_18", "EditformsSingleselect", "12", "3", "0.046"}
				},
				{	/* GP3_3 */
					{"OPER_3_1", "Reflectie", "", "", "1"},
				},
				{	/* GP3_4 */
				},
				{	/* GP4_1 */
				},
				{	/* GP4_2 */
				},
				{	/* GP4_3 */
				},
				{	/* GP4_4 */
				},
				{	/* KP1_1 */
					{"OPER_1_1_V", "Profielvragen", "28", "14", "0.5"},
					{"OPER_1_1", "Profiel", "28", "14", "0.5"}
				},
				{	/* KP1_2 */
					{"OPER_2_1", "Dragdropforms", "11", "8", "1"}
				},
				{	/* KP1_3 */
					{"OPER_3_1_A", "EditformsOpenvragen", "6", "-", "0.5"},
					{"OPER_3_1_B", "EditformsOpenvragen", "2", "-", "0.15"},
					{"OPER_3_1_C", "EditformsOpenvragen", "2", "-", "0.15"},
					{"OPER_3_1_F_1", "MCvraag", "2", "2", "0.1"},
					{"OPER_3_1_F_2", "MCvraag", "2", "1", "0.1"}
				},
				{	/* KP1_4 */
					{"OPER_4_1", "Textfragmentselector", "53", "19", "0.5"},
					{"OPER_4_1_F_1", "MCvraag", "2", "1", "0.0625"},
					{"OPER_4_1_F_2", "MCvraag", "3", "2", "0.0625"},
					{"OPER_4_1_F_3", "MCvraag", "3", "3", "0.0625"},
					{"OPER_4_1_F_4", "MCvraag", "3", "2", "0.0625"},
					{"OPER_4_1_F_5", "MCvraag", "4", "2", "0.0625"},
					{"OPER_4_1_F_6", "MCvraag", "4", "1", "0.0625"},
					{"OPER_4_1_F_7", "MCvraag", "4", "4", "0.0625"},
					{"OPER_4_1_G_1", "MCvraag", "3", "3", "0.0625"}
				},
				{	/* KP2_1 */
					{"OPER_1_1_V", "Profielvragen", "28", "14", "0.5"},
					{"OPER_1_1", "Profiel", "28", "14", "0.5"}
				},
				{	/* KP2_2 */
					{"OPER_2_2", "Dragdropforms", "10", "10", "1"}
				},
				{	/* KP2_3 */
					{"OPER_3_1", "Reflectie", "", "", "1"},
				},
				{	/* KP2_4 */
					{"OPER_4_1_A", "EditformsOpenvragen", "4", "-", "0.6"},
					{"OPER_4_1_B", "EditformsOpenvragen", "2", "-", "0.3"},
					{"OPER_4_1_F_1", "MCvraag", "2", "2", "0.1"}
				},
				{	/* KP3_1 */
					{"OPER_1_1_G_1", "MCvraag", "7", "3", "1"}
				},
				{	/* KP3_2 */
					{"OPER_2_1_F_1", "MCvraag", "2", "2", "0.1"},
					{"OPER_2_1_F_2", "MCvraag", "2", "2", "0.1"},
					{"OPER_2_1_F_3", "MCvraag", "2", "1", "0.1"},
					{"OPER_2_1_F_4", "MCvraag", "2", "2", "0.1"},
					{"OPER_2_1_F_5", "MCvraag", "2", "1", "0.1"},
					{"OPER_2_1_F_6", "MCvraag", "2", "2", "0.1"},
					{"OPER_2_1_F_7", "MCvraag", "2", "2", "0.1"},
					{"OPER_2_1_F_8", "MCvraag", "2", "1", "0.1"},
					{"OPER_2_1_F_9", "MCvraag", "2", "1", "0.1"},
					{"OPER_2_1_F_10", "MCvraag", "2", "2", "0.1"}
				},
				{	/* KP3_3 */
					{"OPER_3_1", "Reflectie", "", "", "1"},
				},
				{	/* KP3_4 */
					{"OPER_4_1_F_1", "MCvraag", "2", "1", "0.125"},
					{"OPER_4_1_F_2", "MCvraag", "2", "2", "0.125"},
					{"OPER_4_1_F_3", "MCvraag", "2", "2", "0.125"},
					{"OPER_4_1_F_4", "MCvraag", "2", "1", "0.125"},
					{"OPER_4_1_F_5", "MCvraag", "2", "1", "0.125"},
					{"OPER_4_1_F_6", "MCvraag", "2", "2", "0.125"},
					{"OPER_4_1_F_7", "MCvraag", "2", "2", "0.125"},
					{"OPER_4_1_F_8", "MCvraag", "2", "1", "0.125"}
				},
				{	/* KP4_1 */
				},
				{	/* KP4_2 */
					{"OPER_2_1_1", "EditformsSingleselect", "9", "5", "0.0556"},
					{"OPER_2_1_2", "EditformsSingleselect", "9", "1|7|8", "0.0556"},
					{"OPER_2_1_3", "EditformsSingleselect", "9", "2|4|6", "0.0555"},
					{"OPER_2_1_4", "EditformsSingleselect", "9", "5", "0.0556"},
					{"OPER_2_1_5", "EditformsSingleselect", "9", "7", "0.0555"},
					{"OPER_2_1_6", "EditformsSingleselect", "9", "3|4|6", "0.0556"},
					{"OPER_2_1_7", "EditformsSingleselect", "9", "1|3|7", "0.0555"},
					{"OPER_2_1_8", "EditformsSingleselect", "9", "4|9", "0.0556"},
					{"OPER_2_1_9", "EditformsSingleselect", "9", "1|3|4|9", "0.0555"},
					{"OPER_2_1_10", "EditformsSingleselect", "9", "1|7", "0.0556"},
					{"OPER_2_1_11", "EditformsSingleselect", "9", "1|3|7", "0.0555"},
					{"OPER_2_1_12", "EditformsSingleselect", "9", "4|6|9", "0.0556"},
					{"OPER_2_1_13", "EditformsSingleselect", "9", "5", "0.0555"},
					{"OPER_2_1_14", "EditformsSingleselect", "9", "3|5", "0.0556"},
					{"OPER_2_1_15", "EditformsSingleselect", "9", "4|6|9", "0.0555"},
					{"OPER_2_1_16", "EditformsSingleselect", "9", "4|6|9", "0.0556"},
					{"OPER_2_1_17", "EditformsSingleselect", "9", "7", "0.0555"},
					{"OPER_2_1_18", "EditformsSingleselect", "9", "3|5", "0.0556"}
				},
				{	/* KP4_3 */
					{"OPER_3_1_1", "EditformsMultiselect", "9", "2", "0.5"},
					{"OPER_3_4_1", "EditformsTaartdiagram", "3", "-", "0.5"}
				},
				{	/* KP4_4 */
					{"OPER_4_1_F_1", "MCvraag", "5", "1|2", "0.1"},
					{"OPER_4_1_F_2", "MCvraag", "5", "1|2|3", "0.1"},
					{"OPER_4_1_F_3", "MCvraag", "5", "5", "0.1"},
					{"OPER_4_1_F_4", "MCvraag", "5", "2", "0.1"},
					{"OPER_4_1_F_5", "MCvraag", "5", "3|4", "0.1"},
					{"OPER_4_1_F_6", "MCvraag", "5", "3|4", "0.1"},
					{"OPER_4_1_F_7", "MCvraag", "5", "4", "0.1"},
					{"OPER_4_1_F_8", "MCvraag", "5", "4", "0.1"},
					{"OPER_4_1_F_9", "MCvraag", "5", "5", "0.1"},
					{"OPER_4_1_F_10", "MCvraag", "5", "3", "0.1"}
				},
				{	/* LL1_1 */
					{"OPER_1_1_V", "Profielvragen", "28", "14", "0.5"},
					{"OPER_1_1", "Profiel", "28", "14", "0.5"}
				},
				{	/* LL1_2 */
					{"OPER_2_1_F_1", "MCvraag", "3", "2", "0.1"},
					{"OPER_2_1_F_2", "MCvraag", "3", "3", "0.1"},
					{"OPER_2_2", "Videosceneselector", "18", "7", "0.5"},
					{"OPER_2_3", "Dragdropforms", "35", "11", "0.3"}
				},
				{	/* LL1_3 */
					{"OPER_3_1", "Reflectie", "", "", "1"},
				},
				{	/* LL1_4 */
					{"OPER_4_1_A", "EditformsOpenvragen", "14", "-", "0.9"},
					{"OPER_4_1_F_1", "MCvraag", "2", "2", "0.1"}
				},
				{	/* LL2_1 */
					{"OPER_1_1_G_1", "MCvraag", "3", "2", "0.1667"},
					{"OPER_1_1_G_2", "MCvraag", "3", "1", "0.1666"},
					{"OPER_1_1", "Graphicalforms", "9", "1", "0.6667"}
				},
				{	/* LL2_2 */
					{"OPER_2_1_G_1", "MCvraag", "3", "1", "0.14"},
					{"OPER_2_1_G_2", "MCvraag", "3", "2", "0.14"},
					{"OPER_2_1_F_1", "MCvraag", "4", "2", "0.14"},
					{"OPER_2_1_F_2", "MCvraag", "4", "1", "0.14"},
					{"OPER_2_1_F_3", "MCvraag", "4", "3", "0.14"},
					{"OPER_2_1_F_4", "MCvraag", "4", "3", "0.15"},
					{"OPER_2_1_F_5", "MCvraag", "4", "3", "0.15"}
				},
				{	/* LL2_3 */
					{"OPER_3_1", "Reflectie", "", "", "1"},
				},
				{	/* LL2_4 */
					{"OPER_4_1", "Reflectie", "", "", "1"},
				},
				{	/* LL3_1 */
					{"OPER_1_1", "Dragdropforms", "8", "8", "1"}
				},
				{	/* LL3_2 */
					{"OPER_2_1", "Dragdropforms", "24", "24", "0.7"},
					{"OPER_2_1_F_1", "MCvraag", "4", "1", "0.1"},
					{"OPER_2_1_F_2", "MCvraag", "4", "3", "0.1"},
					{"OPER_2_1_F_3", "MCvraag", "4", "4", "0.1"}
				},
				{	/* LL3_3 */
					{"OPER_3_1", "Reflectie", "", "", "1"},
				},
				{	/* LL3_4 */
					{"OPER_4_1_F_1", "MCvraag", "7", "4", "0.14"},
					{"OPER_4_1_F_2", "MCvraag", "6", "6", "0.14"},
					{"OPER_4_1_F_3", "MCvraag", "5", "3", "0.14"},
					{"OPER_4_1_F_4", "MCvraag", "4", "3", "0.14"},
					{"OPER_4_1_F_5", "MCvraag", "3", "1", "0.14"},
					{"OPER_4_1_F_6", "MCvraag", "2", "2", "0.15"},
					{"OPER_4_1_F_7", "MCvraag", "1", "1", "0.15"}
				},
				{	/* LL4_1 */
				},
				{	/* LL4_2 */
				},
				{	/* LL4_3 */
				},
				{	/* LL4_4 */
				}
			};
	
	protected String getAssignmentStateData(int index1, int index2, String typeOfData) {
		if (index1 < 0 || index1 >= assignmentstates.length) {
			return "";
		}
		if (index2 < 0 || index2 >= assignmentstates[index1].length) {
			return "";
		}
		if (typeOfData.equals("tagKey")) {
			return assignmentstates[index1][index2][0];
		}
		else if (typeOfData.equals("performanceType")) {
			return assignmentstates[index1][index2][1];
		}
		else if (typeOfData.equals("numberOfAlternatives") ||
				typeOfData.equals("numberOfOpenQuestions")) {
			return assignmentstates[index1][index2][2];
		}
		else if (typeOfData.equals("numberOfCorrectAlternatives") ||
				typeOfData.equals("correctAlternative")) {
			return assignmentstates[index1][index2][3];
		}
		else if (typeOfData.equals("performanceWeight")) {
			return assignmentstates[index1][index2][4];
		}
		return "";
	}

	protected double[] getSkillNumberOfPointsArr() {
		double[] data = new double[skills.length];
		for (int i=0;i<assignmentskills.length;i++) {
			//Only determine points for assignments that are finished
			if (!getAssignmentData(i, "finishedtime").equals(assignmentNotFinishedTime)) {
				//Determine performance multiply factor for number of points
				double performanceFactor = getAssignmentPerformanceFactor(i);
				setAssignmentData(i, "performanceFactor", "" + performanceFactor);
				for (int j=0;j<assignmentskills[i].length;j++) {
					int index = Integer.parseInt(assignmentskills[i][j][0])-1;
					if (index >= 0 && index < skills.length) {
						//Multiply by performanceMultiplyFactor
						data[index] += performanceFactor * Double.parseDouble(assignmentskills[i][j][1]);
					}
				}
			}
		}
		return data;
	}

	protected double getAssignmentPerformanceFactor(int index) {
		if (index < 0 || index >= assignmentstates.length) {
			return performanceFactorBad;
		}
		String assignmentId = getAssignmentData(index, "id");
		if (assignmentId.equals("")) {
			return performanceFactorBad;
		}
		List<String[]> stateDataList = new ArrayList<String[]>();
		String caseComponentName = "B_" + assignmentId.substring(0, 3) + " states";
		String tagName = "state";
		String statusKey = "value";
		for (int i=0;i<assignmentstates[index].length;i++) {
			String[] stateData = new String[4];
			//casecomponent name
			stateData[0] = caseComponentName;
			//tagname
			stateData[1] = tagName;
			//tagkey
			stateData[2] = getAssignmentStateData(index, i, "tagKey"); 
			//statuskey
			stateData[3] = statusKey;
			stateDataList.add(stateData);
		}
		List<String> errorMessages = new ArrayList<String>();
		String[] numberOfErrorsArr = CDesktopComponents.sSpring().getMultipleCurrentRunTagStatus(stateDataList, AppConstants.statusTypeRunGroup, errorMessages);
		double performanceFactor = 0;
		if (errorMessages.size() == 0) {
			for (int i=0;i<numberOfErrorsArr.length;i++) {
				double numberOfErrors = Double.parseDouble(numberOfErrorsArr[i]);
				String performanceType = getAssignmentStateData(index, i, "performanceType");
				double performanceWeight = Double.parseDouble(getAssignmentStateData(index, i, "performanceWeight"));
				double errorIndicatorGood = errorIndicatorGoodNormal;
				double errorIndicatorMedium = errorIndicatorMediumNormal;
				if (performanceType.equals("Textfragmentselector")) {
					errorIndicatorGood = errorIndicatorGoodTextfragmentselector;
					errorIndicatorMedium = errorIndicatorMediumTextfragmentselector;
				}
				double numberOfAlternatives = 0.0;
				if (performanceType.equals("EditformsOpenvragen")) {
					//There are 3 alternatives: good feedback appears right away, after first error feedback or after second error feedback.
					numberOfAlternatives = 3.0;
				}
				else if (performanceType.equals("Reflectie")) {
					//For reflection the number of errors is always 0 because performance is not checked
					//So set the number of alternatives to 1. There is no choice possible. Below dividing 0 by 1 results in errorIndicator being 0, so a good performance.
					numberOfAlternatives = 1.0;
				}
				else {
					numberOfAlternatives = Double.parseDouble(getAssignmentStateData(index, i, "numberOfAlternatives"));
				}
				if (numberOfAlternatives > 0) {
					double errorIndicator = numberOfErrors / numberOfAlternatives;
					if (errorIndicator <= errorIndicatorGood) {
						performanceFactor += performanceWeight * performanceFactorGood;
					}
					else if (errorIndicator <= errorIndicatorMedium) {
						performanceFactor += performanceWeight * performanceFactorMedium;
					}
					else {
						performanceFactor += performanceWeight * performanceFactorBad;
					}
				}
			}
		}
		return performanceFactor;
	}

	/* Satisfaction */
	protected double[] satisfaction = new double[2];
	
	public boolean showSatisfaction() {
		List<String> errorMessages = new ArrayList<String>();
		String value = CDesktopComponents.sSpring().getCurrentRunTagStatus("Game states experimenten", "state", "VraagSatisfactie", "value", AppConstants.statusTypeRunGroup, errorMessages); 
		if (errorMessages.size() == 0) {
			return value.equals(AppConstants.statusValueTrue);
		}
		return false;
	}

	public double[] getSatisfaction() {
		return satisfaction;
	}

	public void setSatisfaction(double[] satisfaction) {
		this.satisfaction = satisfaction;
	}

	protected double[] calculateSatisfaction() {
		return calculateSatisfaction("");
	}

	protected double[] calculateSatisfaction(String minigame) {
		double[] data = new double[satisfaction.length];
		double numberOfFinishedAssignments = 0.0;
		for (int i=0;i<assignments.length;i++) {
			//If minigame is empty or assignment is part of minigame
			if (StringUtils.isEmpty(minigame) || getAssignmentData(i, "id").indexOf(minigame) == 0) {
				//Only determine satisfaction for assignments that are finished
				if (!getAssignmentData(i, "finishedtime").equals(assignmentNotFinishedTime)) {
					//Determine performance multiply factor for number of points
					double[] assignmentSatisfaction = getAssignmentSatisfaction(i);
					setAssignmentData(i, "niceScore", "" + assignmentSatisfaction[0]);
					for (int j=0;j<assignmentSatisfaction.length;j++) {
						//Add satisfaction
						data[j] += assignmentSatisfaction[j];
					}
					numberOfFinishedAssignments++;
				}
			}
		}
		if (numberOfFinishedAssignments > 0) {
			for (int j=0;j<data.length;j++) {
				//Divide by number of assignments
				data[j] = data[j] / numberOfFinishedAssignments;
			}
		}
		else {
			for (int j=0;j<data.length;j++) {
				//No value yet
				data[j] = -1;
			}
		}
		return data;
	}

	protected double[] getAssignmentSatisfaction(int index) {
		double[] data = new double[satisfaction.length];
		if (index < 0 || index >= assignments.length) {
			return data;
		}
		String assignmentId = getAssignmentData(index, "id");
		if (assignmentId.equals("")) {
			return data;
		}
		List<String[]> stateDataList = new ArrayList<String[]>();
		String caseComponentName = "B_" + assignmentId.substring(0, 3) + " states";
		String tagName = "state";
		String statusKey = "value";
		for (int i=1;i<=2;i++) {
			String[] stateData = new String[4];
			//casecomponent name
			stateData[0] = caseComponentName;
			//tagname
			stateData[1] = tagName;
			//tagkey
			stateData[2] = "OSAT_" + assignmentId.substring(4, 5) + "_" + i; 
			//statuskey
			stateData[3] = statusKey;
			stateDataList.add(stateData);
		}
		List<String> errorMessages = new ArrayList<String>();
		String[] satisfaction = CDesktopComponents.sSpring().getMultipleCurrentRunTagStatus(stateDataList, AppConstants.statusTypeRunGroup, errorMessages);
		if (errorMessages.size() == 0) {
			for (int i=0;i<satisfaction.length;i++) {
				data[i] = Double.parseDouble(satisfaction[i]);
			}
		}
		return data;
	}

	/* Minigamesatisfactions */
	/* Fields are mini game id, specialist, image, satisfaction. */
	//NOTE order of minigames must be same as on main screen
	protected Object[][] minigamesatisfactions = new Object[][]
			{
				{"KP1", "", "", new double[]{0, 0}},
				{"KP2", "", "", new double[]{0, 0}},
				{"KP3", "", "", new double[]{0, 0}},
				{"KP4", "", "", new double[]{0, 0}},
				{"LL1", "", "", new double[]{0, 0}},
				{"LL2", "", "", new double[]{0, 0}},
				{"LL3", "", "", new double[]{0, 0}},
				{"LL4", "", "", new double[]{0, 0}},
				{"GP1", "", "", new double[]{0, 0}},
				{"GP2", "", "", new double[]{0, 0}},
				{"GP3", "", "", new double[]{0, 0}},
				{"GP4", "", "", new double[]{0, 0}},
				{"AO1", "", "", new double[]{0, 0}},
				{"AO2", "", "", new double[]{0, 0}},
				{"AO3", "", "", new double[]{0, 0}},
				{"AO4", "", "", new double[]{0, 0}}
			};
	
	protected List<Object> getMinigamesatisfactionDataList(String typeOfData) {
		List<Object> data = new ArrayList<Object>();
		for (int i=0;i<minigamesatisfactions.length;i++) {
			data.add(getMinigamesatisfactionData(i, typeOfData));
		}
		return data;
	}

	public Object getMinigamesatisfactionDataById(String id, String typeOfData) {
		for (int i=0;i<minigamesatisfactions.length;i++) {
			if (!StringUtils.isEmpty(id) && ((String)minigamesatisfactions[i][0]).equals(id)) {
				return getMinigamesatisfactionData(i, typeOfData);
			}
		}
		return "";
	}

	public Object getMinigamesatisfactionData(int index, String typeOfData) {
		if (index < 0 || index >= minigamesatisfactions.length) {
			return "";
		}
		if (typeOfData.equals("id")) {
			return minigamesatisfactions[index][0];
		}
		else if (typeOfData.equals("specialist")) {
			return minigamesatisfactions[index][1];
		}
		else if (typeOfData.equals("imagesrc")) {
			return minigamesatisfactions[index][2];
		}
		else if (typeOfData.equals("satisfaction")) {
			return minigamesatisfactions[index][3];
		}
		return "";
	}

	public void setMinigamesatisfactionDataById(String id, String typeOfData, Object data) {
		for (int i=0;i<minigamesatisfactions.length;i++) {
			if (!StringUtils.isEmpty(id) && ((String)minigamesatisfactions[i][0]).equals(id)) {
				setMinigamesatisfactionData(i, typeOfData, data);
				return;
			}
		}
	}

	public void setMinigamesatisfactionData(int index, String typeOfData, Object data) {
		if (index < 0 || index >= minigamesatisfactions.length) {
			return;
		}
		if (typeOfData.equals("id")) {
			minigamesatisfactions[index][0] = data;
		}
		else if (typeOfData.equals("specialist")) {
			minigamesatisfactions[index][1] = data;
		}
		else if (typeOfData.equals("imagesrc")) {
			minigamesatisfactions[index][2] = data;
		}
		else if (typeOfData.equals("satisfaction")) {
			minigamesatisfactions[index][3] = data;
		}
	}

	/* Time */
	protected long[] caseTimeAsHoursAndMinutes = new long[2];
	
	public long[] getCaseTimeAsHoursAndMinutes() {
		return caseTimeAsHoursAndMinutes;
	}

	public void setCaseTimeAsHoursAndMinutes(long[] caseTimeAsHoursAndMinutes) {
		this.caseTimeAsHoursAndMinutes = caseTimeAsHoursAndMinutes;
	}

	protected long[] calculateCaseTimeAsHoursAndMinutes() {
		long[] time = new long[2];
		//case time in secs
		long caseTime = Math.round(CDesktopComponents.sSpring().getCaseTime());
		time[0] = caseTime / 3600;
		time[1] = caseTime / 60 - time[0] * 60;
		return time;
	}
	
	public List<String> getSkillAssignmentTitles(String skilId) {
		List<String> data = new ArrayList<String>();
		//use times to sort assignment titles on finished time
		List<Double> times = new ArrayList<Double>();
		int indexToSearch = getSkillIndex(skilId);
		if (indexToSearch < 0 || indexToSearch >= skills.length) {
			return data;
		}
		//get assignments for this skill
		for (int i=0;i<assignmentskills.length;i++) {
			for (int j=0;j<assignmentskills[i].length;j++) {
				int index = getSkillIndex(getAssignmentSkillData(i, j, "skillId"));
				if (index == indexToSearch) {
					//is assignment finished?
					String finishedtime = getAssignmentData(i, "finishedtime");
					if (!finishedtime.equals(assignmentNotFinishedTime)) {
						String title = getAssignmentData(i, "title");
						if (!data.contains(title)) {
							double finishedtimeD = Double.parseDouble(finishedtime);
							int counter = 0;
							for (double time : times) {
								if (time <= finishedtimeD) {
									counter++;
								}
							}
							times.add(counter, finishedtimeD);
							data.add(counter, getAssignmentData(i, "specialist") + ": " + title);
						}
					}
				}
			}
		}
		return data;
	}

	/* Professions */
	protected String[][] professions = new String[][]
			{
				{"1", "Arbeid en organisatie psychologie", "0", "0"},
				{"2", "Gezondheids psychologie", "0", "0"},
				{"3", "Klinische psychologie", "0", "0"},
				{"4", "Levensloop psychologie", "0", "0"}
			};
	
	public String[][] getProfessions() {
		return professions;
	}

	protected List<String> getProfessionDataList(String typeOfData) {
		List<String> data = new ArrayList<String>();
		for (int i=0;i<professions.length;i++) {
			data.add(getProfessionData(i, typeOfData));
		}
		return data;
	}

	public String getProfessionDataById(String id, String typeOfData) {
		for (int i=0;i<professions.length;i++) {
			if (!StringUtils.isEmpty(id) && professions[i][0].equals(id)) {
				return getProfessionData(i, typeOfData);
			}
		}
		return "";
	}

	public String getProfessionData(int index, String typeOfData) {
		if (index < 0 || index >= professions.length) {
			return "";
		}
		if (typeOfData.equals("id")) {
			return professions[index][0];
		}
		else if (typeOfData.equals("label")) {
			return professions[index][1];
		}
		else if (typeOfData.equals("maxNumberOfPoints")) {
			return professions[index][2];
		}
		else if (typeOfData.equals("numberOfPoints")) {
			return professions[index][3];
		}
		return "";
	}

	protected int getProfessionIndex(String id) {
		for (int i=0;i<professions.length;i++) {
			if (!StringUtils.isEmpty(id) && professions[i][0].equals(id)) {
				return i;
			}
		}
		return -1;
	}

	public void setProfessionDataById(String id, String typeOfData, String data) {
		for (int i=0;i<professions.length;i++) {
			if (!StringUtils.isEmpty(id) && professions[i][0].equals(id)) {
				setProfessionData(i, typeOfData, data);
				return;
			}
		}
	}

	public void setProfessionData(int index, String typeOfData, String data) {
		if (index < 0 || index >= professions.length) {
			return;
		}
		if (typeOfData.equals("id")) {
			professions[index][0] = data;
		}
		else if (typeOfData.equals("label")) {
			professions[index][1] = data;
		}
		else if (typeOfData.equals("maxNumberOfPoints")) {
			professions[index][2] = data;
		}
		else if (typeOfData.equals("numberOfPoints")) {
			professions[index][3] = data;
		}
	}

	protected double[] getProfessionMaxNumberOfPointsArr(boolean mustBeFinished) {
		double[] data = new double[professions.length];
		//max number of points = max performance score * max satisfaction score
		double maxNumberOfPoints = performanceFactorGood * maxSatisfactionScore;
		for (int i=0;i<assignments.length;i++) {
			//Only determine points for assignments that are finished
			if (!mustBeFinished || !getAssignmentData(i, "finishedtime").equals(assignmentNotFinishedTime)) {
				String assignmentId = getAssignmentData(i, "id");
				if (assignmentId.indexOf("AO") == 0) {
					data[0] += maxNumberOfPoints;
				}
				else if (assignmentId.indexOf("GP") == 0) {
					data[1] += maxNumberOfPoints;
				}
				else if (assignmentId.indexOf("KP") == 0) {
					data[2] += maxNumberOfPoints;
				}
				else if (assignmentId.indexOf("LL") == 0) {
					data[3] += maxNumberOfPoints;
				}
			}
		}
		return data;
	}

	protected double[] getProfessionNumberOfPointsArr(boolean mustBeFinished) {
		double[] data = new double[professions.length];
		for (int i=0;i<assignments.length;i++) {
			//Only determine points for assignments that are finished
			if (!mustBeFinished || !getAssignmentData(i, "finishedtime").equals(assignmentNotFinishedTime)) {
				String assignmentId = getAssignmentData(i, "id");
				double assignmentPerformanceFactor = Double.parseDouble(getAssignmentData(i, "performanceFactor"));
				//NOTE nice scores are saved as 1 till 6, to distinguish them from no score given that corresponds to 0.
				//So to get the real score substract 1.
				double niceScore = Double.parseDouble(getAssignmentData(i, "niceScore")) - 1;
				double numberOfPoints = assignmentPerformanceFactor * niceScore; 
				if (assignmentId.indexOf("AO") == 0) {
					data[0] += numberOfPoints;
				}
				else if (assignmentId.indexOf("GP") == 0) {
					data[1] += numberOfPoints;
				}
				else if (assignmentId.indexOf("KP") == 0) {
					data[2] += numberOfPoints;
				}
				else if (assignmentId.indexOf("LL") == 0) {
					data[3] += numberOfPoints;
				}
			}
		}
		return data;
	}

	
	public CRunDashboardDiv() {
		super();
		init();
	}

	public void init() {
		List<String> assignmentIds = getAssignmentDataList("id");
		//Set assignment data
		//Set assignment finishedtime, >= 0 if state '"O_" + assignmentnumber' of component '"B_" + balid + " states" is equal to "2"'. -1 if not finished
		List<String[]> stateDataList = new ArrayList<String[]>();
		String tagName = "state";
		String statusKey = "value";
		for (String assignmentId : assignmentIds) {
			String[] stateData = new String[4];
			//casecomponent name
			stateData[0] = "B_" + assignmentId.substring(0, 3) + " states";
			//tagname
			stateData[1] = tagName;
			//tagkey
			stateData[2] = "O_" + assignmentId.substring(4, 5);
			//statuskey
			stateData[3] = statusKey;
			stateDataList.add(stateData);
		}
		List<String> errorMessages = new ArrayList<String>();
		double[] statusTimes = CDesktopComponents.sSpring().getMultipleCurrentRunTagStatusTime(stateDataList, AppConstants.statusTypeRunGroup, errorMessages);
		if (errorMessages.size() == 0) {
			for (int i=0;i<statusTimes.length;i++) {
				if (statusTimes[i] <= 0) {
					//NOTE finishedtime is 0 if not yet finished
					setAssignmentData(i, "finishedtime", assignmentNotFinishedTime);
				}
				else {
					setAssignmentData(i, "finishedtime", "" + statusTimes[i]);
				}
			}
		}
		//Set assignment titles and specialists, get them out of passage content and parent content in navigation component
		IECaseComponent navigationComponent = CDesktopComponents.sSpring().getCaseComponent(CDesktopComponents.sSpring().getCase(), "navigation", "");
		if (navigationComponent != null) {
			List<IXMLTag> nodeTags = CDesktopComponents.cScript().getNodeTags(navigationComponent, "passage");
			for (String assignmentId : assignmentIds) {
				String nodeTagName = "O_" + assignmentId;
				for (IXMLTag nodeTag : nodeTags) {
					if (nodeTag.getChildValue("pid").equals(nodeTagName)) {
						setAssignmentDataById(assignmentId, "title", CDesktopComponents.sSpring().unescapeXML(nodeTag.getChildValue("name")));
						IXMLTag parentTag = nodeTag.getParentTag();
						while (parentTag != null && !parentTag.getName().equals("passage")) {
							parentTag = parentTag.getParentTag();
						}
						if (parentTag != null) {
							setAssignmentDataById(assignmentId, "specialist", CDesktopComponents.sSpring().unescapeXML(parentTag.getChildValue("hovertext")));
						}
						break;
					}
				}
			}
		}
		//Set skill data
		//Set skill maxNumberOfPoints
		double[] maxNumberOfPointsArr = getSkillMaxNumberOfPointsArr();
		for (int i=0;i<maxNumberOfPointsArr.length;i++) {
			setSkillData(i, "maxNumberOfPoints", "" + maxNumberOfPointsArr[i]);
		}
		//Set skill numberOfPoints
		double[] numberOfPointsArr = getSkillNumberOfPointsArr();
		for (int i=0;i<numberOfPointsArr.length;i++) {
			setSkillData(i, "numberOfPoints", "" + numberOfPointsArr[i]);
		}
		//Set satisfaction data
		setSatisfaction(calculateSatisfaction());
		//Set minigame satisfaction data
		List<Object> minigamegameIds = getMinigamesatisfactionDataList("id");
		//Set minigame specialists and images, get them out of passage content in navigation component
		if (navigationComponent != null) {
			List<IXMLTag> nodeTags = CDesktopComponents.cScript().getNodeTags(navigationComponent, "passage");
			for (Object minigamegameId : minigamegameIds) {
				String nodeTagName = "B_" + ((String)minigamegameId);
				for (IXMLTag nodeTag : nodeTags) {
					if (nodeTag.getChildValue("pid").equals(nodeTagName)) {
						setMinigamesatisfactionDataById((String)minigamegameId, "specialist", CDesktopComponents.sSpring().unescapeXML(nodeTag.getChildValue("hovertext")));
						setMinigamesatisfactionDataById((String)minigamegameId, "imagesrc", CDesktopComponents.sSpring().getSBlobHelper().getUrl(nodeTag));
						break;
					}
				}
			}
		}
		for (Object minigamegameId : minigamegameIds) {
			setMinigamesatisfactionDataById((String)minigamegameId, "satisfaction", calculateSatisfaction((String)minigamegameId));
		}
		//Set time data
		setCaseTimeAsHoursAndMinutes(calculateCaseTimeAsHoursAndMinutes()); 
		//Set profession data
		//Set profession maxNumberOfPoints
		maxNumberOfPointsArr = getProfessionMaxNumberOfPointsArr(true);
		for (int i=0;i<maxNumberOfPointsArr.length;i++) {
			setProfessionData(i, "maxNumberOfPoints", "" + maxNumberOfPointsArr[i]);
		}
		//Set profession numberOfPoints
		numberOfPointsArr = getProfessionNumberOfPointsArr(true);
		for (int i=0;i<numberOfPointsArr.length;i++) {
			setProfessionData(i, "numberOfPoints", "" + numberOfPointsArr[i]);
		}
		
		storeStates();
		
		Events.postEvent("onInitZulfile", this, null);
	}
	
	public void storeStates() {

		//get old state values
		List<String[]> stateDataList = new ArrayList<String[]>();
		String caseComponentName = "Game states experimenten";
		String tagName = "state";
		String[] stateTagKeys = new String[] {
				"DashboardSkillPercentages",
				"DashboardNiceSatisfactions",
				"DashboardDifficultSatisfactions",
				"DashboardProfessionPercentages",
				"DashboardAssignmentPerformanceFactors"
				};
		String statusKey = "value";
		for (int i=0;i<stateTagKeys.length;i++) {
			String[] stateData = new String[4];
			//casecomponent name
			stateData[0] = caseComponentName;
			//tagname
			stateData[1] = tagName;
			//tagkey
			stateData[2] = stateTagKeys[i];
			//statuskey
			stateData[3] = statusKey;
			stateDataList.add(stateData);
		}
		List<String> errorMessages = new ArrayList<String>();
		String[] oldStatusValues = CDesktopComponents.sSpring().getMultipleCurrentRunTagStatus(stateDataList, AppConstants.statusTypeRunGroup, errorMessages);
		if (errorMessages.size() > 0) {
			return;
		}

		StringBuffer[] statusValues = new StringBuffer[] {
				new StringBuffer(),
				new StringBuffer(),
				new StringBuffer(),
				new StringBuffer(),
				new StringBuffer()
				};

		//get DashboardSkillPercentages
		for (int i=0;i<skills.length;i++) {
			double maxNumberOfPoints = Double.parseDouble(getSkillData(i, "maxNumberOfPoints"));
			double numberOfPoints = Double.parseDouble(getSkillData(i, "numberOfPoints"));
			long skillPercentage = Math.round((100 * numberOfPoints) / maxNumberOfPoints);
			if (i > 0) {
				statusValues[0].append(AppConstants.statusValueSeparator);
			}
			statusValues[0].append(skillPercentage);
		}
		
		//get DashboardNiceSatisfactions
		//get DashboardDifficultSatisfactions
		for (int i=0;i<minigamesatisfactions.length;i++) {
			double[] satisfaction = (double[])getMinigamesatisfactionData(i, "satisfaction"); 
			double niceSatisfaction = satisfaction[0];
			if (i > 0) {
				statusValues[1].append(AppConstants.statusValueSeparator);
			}
			statusValues[1].append(niceSatisfaction);
			double difficultSatisfaction = satisfaction[1];
			if (i > 0) {
				statusValues[2].append(AppConstants.statusValueSeparator);
			}
			statusValues[2].append(difficultSatisfaction);
		}

		//get DashboardProfessionPercentages
		for (int i=0;i<professions.length;i++) {
			double maxNumberOfPoints = Double.parseDouble(getProfessionData(i, "maxNumberOfPoints"));
			double numberOfPoints = Double.parseDouble(getProfessionData(i, "numberOfPoints"));
			long professionPercentage = Math.round((100 * numberOfPoints) / maxNumberOfPoints);
			if (i > 0) {
				statusValues[3].append(AppConstants.statusValueSeparator);
			}
			statusValues[3].append(professionPercentage);
		}

		//get DashboardAssignmentPerformanceFactors
		for (int i=0;i<assignments.length;i++) {
			double assignmentPerformanceFactor = Double.parseDouble(getAssignmentData(i, "performanceFactor"));
			if (i > 0) {
				statusValues[4].append(AppConstants.statusValueSeparator);
			}
			statusValues[4].append(assignmentPerformanceFactor);
		}

		//store new state values
		for (int i=0;i<stateTagKeys.length;i++) {
			String statusValue = statusValues[i].toString();
			if (!statusValue.equals(oldStatusValues[i])) {
				CDesktopComponents.sSpring().setCurrentRunTagStatus(caseComponentName, tagName, stateTagKeys[i], statusKey, statusValue, AppConstants.statusTypeRunGroup, false, errorMessages);
			}
		}
		
	}
	
	public CRunDashboard getRunDashboard() {
		if (runDashboard == null) {
			runDashboard = (CRunDashboard)CDesktopComponents.vView().getComponent("runDashboard");
		}
		return runDashboard;
	}

	@Override
	public void observedNotify(CIObserved aObserved, String aAction, Object aStatus) {
	}


	/**
	 * Methods originally used in ZUL file.
	 */

	public void onInitZulfile(Event aEvent) {
		relativePath = getRunDashboard().getZulfilePath();
	}

	public void initSatisfactionRating(HtmlBasedComponent div, String typeOfSatisfaction, double satisfactionValue) {
		//
		div.setClass(typeOfSatisfaction + "valuediv");
		//
		HtmlBasedComponent box = (HtmlBasedComponent)div.getChildren().get(0);
		box.setClass(typeOfSatisfaction + "value");
		//
		for (int i=0;i<6;i++) {
			Image image = new Image();
			box.appendChild(image);
			if (i >= Math.round(satisfactionValue)) {
				image.setSrc(relativePath + "bolletje-leeg.png");
			}
			else {
				image.setSrc(relativePath + "bolletje-" + typeOfSatisfaction + ".png");
			}
		}
		//
		div.setVisible(true);
	}

	public void onHandleStatusChange(Event aEvent) {
		STriggeredReference triggeredReference = (STriggeredReference)aEvent.getData();
		if (triggeredReference != null) {
			if (triggeredReference.getDataTag() == null &&
				triggeredReference.getStatusKey().equals(AppConstants.statusKeyUpdate)) {
				init();
			}
		}
	}

}
