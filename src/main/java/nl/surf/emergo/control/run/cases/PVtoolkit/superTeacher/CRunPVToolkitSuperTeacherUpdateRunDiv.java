/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.superTeacher;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;

public class CRunPVToolkitSuperTeacherUpdateRunDiv extends CRunPVToolkitSuperTeacherDiv {

	private static final long serialVersionUID = -7964990524258517981L;

	public void update() {
		getChildren().clear();
		
		pvToolkit.setMemoryCaching(true);
		
		Div div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{"titleCenter"}
		);
		
		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font titleCenter", "PV-toolkit.mainmenuoption.superTeacher"}
		);
		
		Button btn = new CRunPVToolkitDefButton(this, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font pvtoolkitButton " + _classPrefix + "PeerGroupsButton", "PV-toolkit-superTeacher.header.peerGroups"}
		);
		addPeerGroupsButtonOnClickEventListener(btn, this);

		btn = new CRunPVToolkitDefButton(this, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font pvtoolkitButton " + _classPrefix + "MessagesButton", "PV-toolkit-superTeacher.header.messages"}
		);
		addMessagesButtonOnClickEventListener(btn, this);

		btn = new CRunPVToolkitDefButton(this, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font pvtoolkitButton " + _classPrefix + "ResearchButton", "PV-toolkit-superTeacher.header.research"}
		);
		addResearchButtonOnClickEventListener(btn, this);

		btn = new CRunPVToolkitDefButton(this, 
				new String[]{"class", "cLabelKey"}, 
				new Object[]{"font pvtoolkitButton " + _classPrefix + "BackButton", "PV-toolkit.back"}
		);
		addBackToAuthoringEnvironmentOnClickEventListener(btn, this, CDesktopComponents.vView().getComponent("superTeacherAuthoringEnvironmentDiv"));

		pvToolkit.setMemoryCaching(false);
	}

	protected void addPeerGroupsButtonOnClickEventListener(Component component, Component componentToHide) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				componentToHide.setVisible(false);
				CRunPVToolkitSuperTeacherPeerGroupsDiv div = (CRunPVToolkitSuperTeacherPeerGroupsDiv)CDesktopComponents.vView().getComponent("superTeacherPeerGroupsDiv");
				if (div != null) {
					div.init(pvToolkit.getCurrentRunGroup(), true);
				}
			}
		});
	}

	protected void addMessagesButtonOnClickEventListener(Component component, Component componentToHide) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				componentToHide.setVisible(false);
				CRunPVToolkitSuperTeacherMessagesDiv div = (CRunPVToolkitSuperTeacherMessagesDiv)CDesktopComponents.vView().getComponent("superTeacherMessagesDiv");
				if (div != null) {
					div.init(pvToolkit.getCurrentRunGroup(), true);
				}
			}
		});
	}

	protected void addResearchButtonOnClickEventListener(Component component, Component componentToHide) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				componentToHide.setVisible(false);
				CRunPVToolkitSuperTeacherResearchDiv div = (CRunPVToolkitSuperTeacherResearchDiv)CDesktopComponents.vView().getComponent("superTeacherResearchDiv");
				if (div != null) {
					div.init(pvToolkit.getCurrentRunGroup(), true);
				}
			}
		});
	}

	protected void addBackToAuthoringEnvironmentOnClickEventListener(Component component, Component fromComponent, Component toComponent) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				if (fromComponent != null) {
					fromComponent.setVisible(false);
				}
				if (toComponent != null) {
					toComponent.setVisible(true);
				}
			}
		});
	}

}
