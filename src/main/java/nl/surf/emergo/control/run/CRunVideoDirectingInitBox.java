/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.CreateEvent;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefBox;
import nl.surf.emergo.utilities.PropsValues;
import nl.surf.emergo.view.VView;

public class CRunVideoDirectingInitBox extends CDefBox {

	private static final long serialVersionUID = 5029514167540305009L;

	public List<String> settinglabels;
	public Integer settingnumber = 0;
	public List<String> viewlabels;
	public List<Boolean> settingspresent;
	public List<Boolean> settingsaccessible;
	public int numberOfViews = 0;
	public List<String> viewNumbers;
	public int buttoncounter = 1;
	public int listitemcounter = 1;

	public void onCreate(CreateEvent aEvent) {
		VView vView = CDesktopComponents.vView();

		CDesktopComponents.sSpring().initDesktopAttributes(getDesktop());

		String CRunDirectingLabelButton = "";
		CRunComponent runComponent = (CRunComponent) vView.getComponent("runDirecting");
		if (runComponent != null) {
			HtmlMacroComponent parentMacro = (HtmlMacroComponent) runComponent.getMacroParent(this);
			if (parentMacro != null) {
				CRunDirectingLabelButton = (String) parentMacro.getDynamicProperty("a_CRunDirectingLabelButton");
				if (CRunDirectingLabelButton == null) {
					CRunDirectingLabelButton = "";
				}
			}
		}

		settinglabels = (List<String>) VView.getParameter("directing_settinglabels", this, null);
		settingnumber = (Integer) VView.getParameter("directing_settingnumber", this, null);
		viewlabels = (List<String>) VView.getParameter("directing_viewlabels", this, null);
		settingspresent = (List<Boolean>) VView.getParameter("directing_settingspresent", this, null);
		settingsaccessible = (List<Boolean>) VView.getParameter("directing_settingsaccessible", this, null);
		numberOfViews = viewlabels.size();
		viewNumbers = new ArrayList<String>();
		for (int i = 1; i <= numberOfViews; i++) {
			viewNumbers.add("" + i);
		}

		String runComponentId = (String) VView.getParameter("runComponentId", this, "runDirecting");

		String width = (String) VView.getParameter("video_size_width", this, "");
		String height = (String) VView.getParameter("video_size_height", this, "");
		if (width.equals("")) {
			// default width, originally for Sexology game, video size 2800x430
			width = "" + (352 * 2800 / 430); // "2344";
		}
		if (height.equals("")) {
			// default height, originally for Sexology game, video size 2800x430
			height = "352";
		}

		String url = vView.getUrlEncodedFileName((String) VView.getParameter("directing_url", this, ""));
		String html5url = vView.getHtml5StreamingUrl(url);
		List<List<String>> streamingurls = vView.getHtml5StreamingUrls(url);
		if (!url.equals(""))
			url = vView.uniqueView(url);
		if (!html5url.equals(""))
			html5url = vView.uniqueView(html5url);

		boolean visible = numberOfViews != 0;
		int totalWidth = Integer.parseInt(width);
		String viewWidth = "0";
		if (numberOfViews != 0) {
			viewWidth = "" + (totalWidth / numberOfViews);
		}

		// 640 pixels is maximum view width in directing component
		// 360 pixels is maximum height in directing component
		String left = "" + (16 + (640 - Integer.parseInt(viewWidth)) / 2);
		String top = "" + (19 + (360 - Integer.parseInt(height)) / 2);

		String cont_height = "" + (Integer.parseInt(height) + Integer.parseInt(top));

		// style of controlbar
		String controlbarStyle = "top:" + (Integer.parseInt(height) - 24) + "px;" + "width:"
				+ Integer.parseInt(viewWidth) + "px;";

		// String startBtnLeft = "" + ((640 - Integer.parseInt(left)) / 2 - 25) + "px";
		// String startBtnTop = "" + ((360 - Integer.parseInt(top)) / 2 - 18) + "px";
		String startBtnLeft = "" + (Integer.parseInt(viewWidth) / 2 - 25) + "px";
		String startBtnTop = "" + (Integer.parseInt(height) / 2 - 18) + "px";

		String bufferAreaLeft = "" + (Integer.parseInt(viewWidth) / 2 - 58) + "px";
		String bufferAreaTop = "" + (Integer.parseInt(height) / 2 - 38) + "px";

		HtmlMacroComponent macro = new HtmlMacroComponent();
		appendChild(macro);
		macro.setDynamicProperty("a_CRunDirectingLabelButton", CRunDirectingLabelButton);
		macro.setDynamicProperty("a_emergoRootUrl", vView.getAbsoluteWebappsRoot());
		macro.setDynamicProperty("a_viewNumbers", viewNumbers);
		macro.setDynamicProperty("a_settingnumber", settingnumber);
		macro.setDynamicProperty("a_settinglabels", settinglabels);
		macro.setDynamicProperty("a_visible", visible);
		macro.setDynamicProperty("a_jwplayerKey", PropsValues.VIDEO_JWPLAYER_KEY);
		macro.setDynamicProperty("a_playerName", "EM_fl_directing");
		macro.setDynamicProperty("a_left", left);
		macro.setDynamicProperty("a_top", top);
		macro.setDynamicProperty("a_viewWidth", viewWidth);
		macro.setDynamicProperty("a_cont_height", cont_height);
		macro.setDynamicProperty("a_bufferAreaLeft", bufferAreaLeft);
		macro.setDynamicProperty("a_bufferAreaTop", bufferAreaTop);
		macro.setDynamicProperty("a_width", width);
		macro.setDynamicProperty("a_height", height);
		macro.setDynamicProperty("a_controlbarStyle", controlbarStyle);
		macro.setDynamicProperty("a_startBtnLeft", startBtnLeft);
		macro.setDynamicProperty("a_startBtnTop", startBtnTop);
		macro.setDynamicProperty("a_runComponentId", runComponentId);
		macro.setDynamicProperty("a_numberOfViews", numberOfViews);
		// NOTE url may contain back slashes. Replace by slashes
		url = url.replaceAll(PropsValues.STREAMING_PLAYER_PATH_SEPARATOR, "/");
		macro.setDynamicProperty("a_url", url);
		html5url = html5url.replaceAll(PropsValues.STREAMING_PLAYER_PATH_SEPARATOR, "/");
		macro.setDynamicProperty("a_html5url", html5url);
		macro.setDynamicProperty("a_streamingUrls", streamingurls);
		macro.setMacroURI("../run_" + PropsValues.VIDEO_PLAYER_ID + "_directing_view_fr_macro.zul");
	}

}
