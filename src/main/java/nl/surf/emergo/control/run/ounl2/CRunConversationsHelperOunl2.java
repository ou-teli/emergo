/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl2;

import nl.surf.emergo.control.CTree;
import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.control.run.ounl.CRunConversationsHelperOunl;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunConversationsHelperOunl2. Helps rendering conversation interaction.
 */
public class CRunConversationsHelperOunl2 extends CRunConversationsHelperOunl {

	/**
	 * Instantiates a new c run conversations helper ounl.
	 *
	 * @param aTree the ZK conversations tree
	 * @param aShowTagNames the a show tag names, comma separated, only tags with these names are shown
	 * @param aCaseComponent the conversations case component
	 * @param aRunComponent the a run component, the ZK conversations component
	 */
	public CRunConversationsHelperOunl2(CTree aTree, String aShowTagNames, IECaseComponent aCaseComponent, CRunComponent aRunComponent) {
		super(aTree, aShowTagNames, aCaseComponent, aRunComponent);
		maxStringLength = 75;
	}

}