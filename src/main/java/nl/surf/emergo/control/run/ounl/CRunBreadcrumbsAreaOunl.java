/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zhtml.Table;
import org.zkoss.zhtml.Td;
import org.zkoss.zhtml.Tr;
import org.zkoss.zul.Label;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefTable;
import nl.surf.emergo.control.def.CDefTd;
import nl.surf.emergo.control.def.CDefTr;
import nl.surf.emergo.control.run.CRunArea;

/**
 * The Class CRunBreadcrumbsAreaOunl. Used to show bread crumbs.
 * Notifies Emergo player when crumb is clicked.
 */
public class CRunBreadcrumbsAreaOunl extends CRunArea {

	private static final long serialVersionUID = -7792982728009584205L;

	/** The breadcrumbs. */
	protected List<IXMLTag> breadcrumbs = new ArrayList<IXMLTag>();

	/** The breadcrumb names. */
	protected List<String> breadcrumbnames = new ArrayList<String>();

	/**
	 * Instantiates a new c run bread crumbs area.
	 */
	public CRunBreadcrumbsAreaOunl() {
		super("runBreadcrumbsArea");
		init();
	}

	/**
	 * Instantiates a new c run bread crumbs area.
	 *
	 * @param aId the a id
	 */
	public CRunBreadcrumbsAreaOunl(String aId) {
		super(aId);
		init();
	}

	/**
	 * Init
	 */
	public void init() {
		setZclass(getClassName());
	}

	/**
	 * Clear
	 */
	public void clear() {
		breadcrumbs.clear();
		breadcrumbnames.clear();
	}

	/**
	 * Shows bread crumb.
	 *
	 * @param status the status
	 */
	public void showBreadcrumb(Object status) {
		IXMLTag locationTag = (IXMLTag)status;
		if (locationTag == null)
			return;
		String name = CDesktopComponents.sSpring().unescapeXML(locationTag.getChildValue("name"));
		if (name.equals("")) {
			name = locationTag.getChildValue("pid");
		}
		// determine rootTag
		IXMLTag rootTag = null;
		IXMLTag parentTag = locationTag.getParentTag();
		while (parentTag != null) {
			if (parentTag.getName().equals(AppConstants.rootElement)) {
				rootTag = parentTag;
				break;
			}
			else {
				parentTag = parentTag.getParentTag();
			}
		}
		// determine componentTag
		IXMLTag componentTag = null;
		if (rootTag != null) {
			componentTag = rootTag.getChild(AppConstants.componentElement);
		}
		boolean nobreadcrumb = (componentTag != null && componentTag.getChildAttribute(AppConstants.statusElement, AppConstants.statusKeyNobreadcrumb).equals(AppConstants.statusValueTrue)) ||
				locationTag.getChildValue("nobreadcrumb").equals(AppConstants.statusValueTrue);
		int index = breadcrumbnames.indexOf(name);
		if (index < 0) {
			// check if parent locations already in breadcrumbs
			// if not add it
			List<IXMLTag> parents = new ArrayList<IXMLTag>();
			List<String> parentNames = new ArrayList<String>();
			IXMLTag parent = locationTag.getParentTag();
			while (parent != null) {
				if (parent.getName().equals("location")) {
					String parentName = CDesktopComponents.sSpring().unescapeXML(parent.getChildValue("name"));
					if (parentName.equals(""))
						parentName = parent.getChildValue("pid");
					boolean present = false;
					for (String breadcrumbname : breadcrumbnames) {
						if (breadcrumbname.equals(parentName))
							present = true;
					}
					if (!present) {
						parents.add(parent);
						parentNames.add(parentName);
					}
				}
				parent = parent.getParentTag();
			}
			for (int i = (parents.size() - 1);i >= 0;i--) {
				breadcrumbs.add(parents.get(i));
				breadcrumbnames.add(parentNames.get(i));
			}
			// if tag in breadcrumbs, add tag
			breadcrumbs.add(locationTag);
			breadcrumbnames.add(name);
		}
		else if (index < (breadcrumbs.size() - 1)) {
			// if tag in breadcrumbs, remove items if tag is not last one
			for (int i = (breadcrumbs.size() - 1);i > index;i--) {
				breadcrumbs.remove(i);
				breadcrumbnames.remove(i);
			}
		}
		showBreadcrumbs(nobreadcrumb);
	}

	/**
	 * Has parent bread crumb.
	 *
	 */
	public boolean hasParentBreadcrumb() {
		return breadcrumbs.size() > 1;
	}

	/**
	 * Show parent bread crumb if it has one.
	 *
	 */
	public void showParentBreadcrumb() {
		if (breadcrumbs.size() <= 1) {
			return;
		}
		if (runWnd != null) {
			runWnd.onAction(getId(), "showLocation", breadcrumbs.get(breadcrumbs.size() - 2).getAttribute(AppConstants.keyId));
		}
	}

	/**
	 * Shows bread crumbs.
	 *
	 */
	protected void showBreadcrumbs(boolean nobreadcrumb) {
		getChildren().clear();
		if (nobreadcrumb || breadcrumbs.size() == 0)
			return;
		Table table = new CDefTable();
		table.setDynamicProperty("border", "0");
		table.setDynamicProperty("cellspacing", "0");
		table.setDynamicProperty("cellpadding", "0");
		table.setDynamicProperty("height", "32");
		appendChild(table);
		Tr tr = new CDefTr();
		table.appendChild(tr);
		Td td = new CDefTd();
		td.setSclass(getClassName() + "Left");
		tr.appendChild(td);
		int counter = 0;
/*		if (nobreadcrumb) {
			counter = breadcrumbs.size() - 1;
		} */
		for (int i = counter;i < breadcrumbs.size();i++) {
			IXMLTag locationTag = breadcrumbs.get(i);
			if (i > counter) {
				td = new CDefTd();
				td.setSclass(getClassName() + "Next");
				tr.appendChild(td);
			}
			td = getNewBreadcrumbTd(locationTag);
			td.setSclass(getClassName() + "Spacer");
			tr.appendChild(td);
			td = getNewBreadcrumbTd(locationTag);
			td.setSclass(getClassName() + "Label");
			tr.appendChild(td);
			Label label = getNewBreadcrumbLabel(locationTag);
			td.appendChild(label);
			td = getNewBreadcrumbTd(locationTag);
			td.setSclass(getClassName() + "Spacer");
			tr.appendChild(td);
		}
		td = new CDefTd();
		td.setSclass(getClassName() + "Right");
		tr.appendChild(td);
	}

	/**
	 * Gets new bread crumb label.
	 */
	protected Label getNewBreadcrumbLabel(IXMLTag aLocationTag) {
		return new CRunBreadcrumbLabelOunl(aLocationTag);
	}

	/**
	 * Gets new bread crumb td.
	 */
	protected Td getNewBreadcrumbTd(IXMLTag aLocationTag) {
		return new CRunBreadcrumbTdOunl(aLocationTag);
	}

}
