/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.List;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERunGroupAccount;

/**
 * The Class CRunIspotIntervention is to contain intervention data.
 */
public class CRunIspotIntervention {
	
	protected CRunIspotIntervention previous;

	protected CRunIspotIntervention next;

	protected IECaseComponent caseComponent;
	
	protected IXMLTag tag;
	
	protected int tagId;

	protected String type;

	protected int maxDuration;

	protected int numberOfResits;

	protected boolean seeSelf;

	protected boolean autoStart;

	protected boolean interruptable;

	protected boolean publishable;

	protected boolean reviewable;

	protected String subjectName;

	protected String vignetteName;

	protected String vignetteInstruction;
	
	protected String vignetteUrl;
	
	protected boolean vignetteReviewable;

	protected boolean vignetteNextIfPublished;

	protected boolean vignetteAllowPrevious;

	protected String vignetteRanking;

	protected List<CRunIspotFeedbackText> feedbackTexts;
	
	protected List<CRunIspotFeedbackFragment> feedbackFragments;
	
	protected String recordingName;

	protected IERunGroupAccount reactionRga;
	
	protected String reactionState;

	protected String reactionDoneDate;

	protected String reactionUrl;
	
	protected int numberOfReactions;

	public CRunIspotIntervention getPrevious() {
		return previous;
	}

	public void setPrevious(CRunIspotIntervention previous) {
		this.previous = previous;
	}

	public CRunIspotIntervention getNext() {
		return next;
	}

	public void setNext(CRunIspotIntervention next) {
		this.next = next;
	}

	public IECaseComponent getCaseComponent() {
		return caseComponent;
	}

	public void setCaseComponent(IECaseComponent caseComponent) {
		this.caseComponent = caseComponent;
	}

	public IXMLTag getTag() {
		return tag;
	}

	public void setTag(IXMLTag tag) {
		this.tag = tag;
	}

	public int getTagId() {
		return tagId;
	}

	public void setTagId(int tagId) {
		this.tagId = tagId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getMaxDuration() {
		return maxDuration;
	}

	public void setMaxDuration(int maxDuration) {
		this.maxDuration = maxDuration;
	}

	public int getNumberOfResits() {
		return numberOfResits;
	}

	public void setNumberOfResits(int numberOfResits) {
		this.numberOfResits = numberOfResits;
	}

	public boolean isSeeSelf() {
		return seeSelf;
	}

	public void setSeeSelf(boolean seeSelf) {
		this.seeSelf = seeSelf;
	}

	public boolean isAutoStart() {
		return autoStart;
	}

	public void setAutoStart(boolean autoStart) {
		this.autoStart = autoStart;
	}

	public boolean isInterruptable() {
		return interruptable;
	}

	public void setInterruptable(boolean interruptable) {
		this.interruptable = interruptable;
	}

	public boolean isPublishable() {
		return publishable;
	}

	public void setPublishable(boolean publishable) {
		this.publishable = publishable;
	}

	public boolean isReviewable() {
		return reviewable;
	}

	public void setReviewable(boolean reviewable) {
		this.reviewable = reviewable;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public String getVignetteName() {
		return vignetteName;
	}

	public void setVignetteName(String vignetteName) {
		this.vignetteName = vignetteName;
	}

	public String getVignetteInstruction() {
		return vignetteInstruction;
	}

	public void setVignetteInstruction(String vignetteInstruction) {
		this.vignetteInstruction = vignetteInstruction;
	}

	public String getVignetteUrl() {
		return vignetteUrl;
	}

	public void setVignetteUrl(String vignetteUrl) {
		this.vignetteUrl = vignetteUrl;
	}

	public boolean isVignetteReviewable() {
		return vignetteReviewable;
	}

	public void setVignetteReviewable(boolean reviewable) {
		this.vignetteReviewable = reviewable;
	}

	public boolean isVignetteNextIfPublished() {
		return vignetteNextIfPublished;
	}

	public void setVignetteNextIfPublished(boolean nextIfPublished) {
		this.vignetteNextIfPublished = nextIfPublished;
	}

	public boolean isVignetteAllowPrevious() {
		return vignetteAllowPrevious;
	}

	public void setVignetteAllowPrevious(boolean allowPrevious) {
		this.vignetteAllowPrevious = allowPrevious;
	}

	public String getVignetteRanking() {
		return vignetteRanking;
	}

	public void setVignetteRanking(String vignetteRanking) {
		this.vignetteRanking = vignetteRanking;
	}

	public List<CRunIspotFeedbackText> getFeedbackTexts() {
		return feedbackTexts;
	}

	public void setFeedbackTexts(List<CRunIspotFeedbackText> feedbackTexts) {
		this.feedbackTexts = feedbackTexts;
	}

	public List<CRunIspotFeedbackFragment> getFeedbackFragments() {
		return feedbackFragments;
	}

	public void setFeedbackFragments(
			List<CRunIspotFeedbackFragment> feedbackFragments) {
		this.feedbackFragments = feedbackFragments;
	}

	public String getRecordingName() {
		return recordingName;
	}

	public void setRecordingName(String recordingName) {
		this.recordingName = recordingName;
	}

	public IERunGroupAccount getReactionRga() {
		return reactionRga;
	}

	public void setReactionRga(IERunGroupAccount reactionRga) {
		this.reactionRga = reactionRga;
	}

	public String getReactionState() {
		return reactionState;
	}

	public void setReactionState(String reactionState) {
		this.reactionState = reactionState;
	}

	public String getReactionDoneDate() {
		return reactionDoneDate;
	}

	public void setReactionDoneDate(String reactionDoneDate) {
		this.reactionDoneDate = reactionDoneDate;
	}

	public String getReactionUrl() {
		return reactionUrl;
	}

	public void setReactionUrl(String reactionUrl) {
		this.reactionUrl = reactionUrl;
	}

	public int getNumberOfReactions() {
		return numberOfReactions;
	}

	public void setNumberOfReactions(int numberOfReactions) {
		this.numberOfReactions = numberOfReactions;
	}

}
