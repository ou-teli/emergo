/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.practice;

import java.util.ArrayList;
import java.util.List;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitSubStepsDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTag;

public class CRunPVToolkitPracticeSubStepsDiv extends CRunPVToolkitSubStepsDiv {

	private static final long serialVersionUID = 4210708169170851816L;

	@Override
	protected List<String> getComponentToNavigateToIds(List<IXMLTag> subStepTags) {
		List<String> componentIds = new ArrayList<String>();
		for (IXMLTag subStepTag : subStepTags) {
			String subSteptype = subStepTag.getChildValue("substeptype");
			if (subSteptype.equals(CRunPVToolkit.practicePracticeSubStepType)) {
				componentIds.add("practiceSubStepDiv");
			}
			else if (subSteptype.equals(CRunPVToolkit.practiceSelfFeedbackSubStepType)) {
				componentIds.add("practiceSubStepDiv");
			}
			else if (subSteptype.equals(CRunPVToolkit.practiceAskFeedbackSubStepType)) {
				componentIds.add("practiceSubStepDiv");
			}
		}
		return componentIds;
	}
	
	@Override
	protected List<String> getComponentToInitIds(List<IXMLTag> subStepTags) {
		List<String> componentIds = new ArrayList<String>();
		for (IXMLTag subStepTag : subStepTags) {
			String subSteptype = subStepTag.getChildValue("substeptype");
			if (subSteptype.equals(CRunPVToolkit.practicePracticeSubStepType)) {
				componentIds.add("practiceSubStepDiv");
			}
			else if (subSteptype.equals(CRunPVToolkit.practiceSelfFeedbackSubStepType)) {
				componentIds.add("practiceSubStepDiv");
			}
			else if (subSteptype.equals(CRunPVToolkit.practiceAskFeedbackSubStepType)) {
				componentIds.add("practiceSubStepDiv");
			}
		}
		return componentIds;
	}
	
	@Override
	protected boolean updateSubSteps(List<IXMLTag> subStepTags) {
		CRunPVToolkitCacAndTag practice = pvToolkit.getPractice(_actor, getStepTagForSubSteps());
		//NOTE no change if no practice tags
		if (practice == null || practice.getXmlTag() == null) {
			return false;
		}
		List<IXMLTag> practiceTags = practice.getXmlTag().getChilds("practice");
		List<Boolean> subStepsDone = getSubStepsDone(subStepTags, practiceTags);
		boolean lChanged = false;
		for (IXMLTag subStepTag : subStepTags) {
			int lInd = subStepTags.indexOf(subStepTag);
			if (updateSubStepAccessible(subStepTags, subStepsDone, lInd)) {
				lChanged = true;
			}
			if (updateSubStepFinished(subStepTags, subStepsDone, lInd)) {
				lChanged = true;
			}
		}
		return lChanged;
	}

	@Override
	protected boolean updateSubStepAccessible(List<IXMLTag> subStepTags, List<Boolean> subStepsDone, int index) {
		//NOTE if last practice sub step is done disable all buttons
		int lastIndex = subStepsDone.size() - 1;
		if (subStepsDone.get(lastIndex)) {
			//NOTE super teacher or admin may inspect sub steps
			boolean userIsAdmin = false;
			String currentUserRole = (String)CDesktopComponents.cControl().getAccSessAttr("role");
			userIsAdmin = currentUserRole != null && currentUserRole.equals(AppConstants.c_role_adm);
			if (isSubStepAccessible(subStepTags.get(index)) != (pvToolkit.isSuperTeacher() || userIsAdmin)) {
				setSubStepAccessible(subStepTags.get(index), pvToolkit.isSuperTeacher() || userIsAdmin);
				return true;
			}
			return false;
		}
		else {
			return super.updateSubStepAccessible(subStepTags, subStepsDone, index);
		}
	}

	protected List<Boolean> getSubStepsDone(List<IXMLTag> subStepTags, List<IXMLTag> practiceTags) {
		return pvToolkit.getPracticeSubStepsDone(_actor, subStepTags, practiceTags);
	}

}
