/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import java.util.List;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zul.Label;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefLabel;

/**
 * The Class CRunLocationActions. Used to show a list of location actions to be
 * chosen from on location by the Emergo user.
 */
public class CRunLocationActionsClassic extends CRunVboxClassic {

	private static final long serialVersionUID = 6391935849902622645L;

	/** The location actions. */
	protected List<List<Object>> locationActions = null;

	/**
	 * Gets the location actions.
	 * 
	 * @return the location actions
	 */
	public List<List<Object>> getLocationActions() {
		return locationActions;
	}

	/**
	 * Sets the location actions.
	 * 
	 * @param aLocationActions the new location actions
	 */
	public void setLocationActions(List<List<Object>> aLocationActions) {
		locationActions = aLocationActions;
	}

	/**
	 * On create fill locationActions and show location actions.
	 * 
	 * @param aEvent the a event
	 */
	public void onCreate(CreateEvent aEvent) {
		setLocationActions((List<List<Object>>) (aEvent.getArg()).get("item"));
		// create all necessary components within window
		createComponents();
	}

	/**
	 * Creates components. Shows location actions as clickable labels.
	 */
	public void createComponents() {
		if (locationActions != null) {
			if (locationActions.size() > 0) {
				Label lLabel = null;
				lLabel = new CDefLabel(CDesktopComponents.vView().getLabel("run_location_actions.locationactions"));
				appendChild(lLabel);
				for (List<Object> lLocationAction : locationActions) {
					lLabel = new CRunLocationActionLabelClassic(lLocationAction);
					appendChild(lLabel);
				}
			}
		}
	}
}