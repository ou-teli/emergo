/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.superTeacher;

import org.zkforge.ckez.CKeditor;
import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.util.Clients;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IECaseComponent;

public class CRunPVToolkitSuperTeacherUpdateMethodDiv extends CRunPVToolkitSuperTeacherUpdateMethodCyclesDiv {

	private static final long serialVersionUID = -3016405572110458713L;

	protected IECaseComponent referencesCaseComponent;
	protected StringBuffer referencesElements;
	protected static final String pieceTagName = "piece";

	public void update() {
		//NOTE new method is always stored in XML data and status that is cached within the SSpring class, regardless if preview is read only or not.
		IXMLTag nodeTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get("cacId")), _idMap.get("tagId"));
		if (nodeTag != null) {
			initMethod(nodeTag);
			initMethodElements(nodeTag);
		}
		
		referencesCaseComponent = sSpring.getCaseComponent("", "PV_references");
		initReferencesElements();
		cScript.getNodeTags(referencesCaseComponent, "piece");

		Clients.evalJavaScript("initUpdateMethod('" + method.toString() + "','" + methodElements.toString() + "','" + referencesElements.toString() + "');");
	}

	protected void initReferencesElements() {
		referencesElements = new StringBuffer();
		
		if (referencesCaseComponent != null) {
			int counter = 1;
			for (IXMLTag pieceTag : cScript.getNodeTags(referencesCaseComponent, pieceTagName)) {
				addReferencesElement(pieceTag.getParentTag(), pieceTag, counter);
				counter++;
			}
			
		}
		
		referencesElements.insert(0, "[");
		referencesElements.append("]");
	}
		
	protected void addReferencesElement(IXMLTag parentTag, IXMLTag nodeTag, int counter) {
		StringBuffer element = new StringBuffer();
		addElementPart(element, "parentTagId", parentTag.getAttribute(AppConstants.keyId), true);
		addElementPart(element, "parentTagName", parentTag.getName(), true);
		addElementPart(element, "tagId", nodeTag.getAttribute(AppConstants.keyId), true);
		addElementPart(element, "tagName", nodeTag.getName(), true);
		addElementPart(element, "name", sSpring.unescapeXML(nodeTag.getChildValue("name")), false);
		if (referencesElements.length() > 0) {
			referencesElements.append(",");
		}
		referencesElements.append("{");
		referencesElements.append(element);
		referencesElements.append("}");
	}

	public void onNotify(Event event) {
		JSONObject jsonObject = (JSONObject)event.getData();
		String action = (String)jsonObject.get("action");
		if (action.equals("update_method")) {
			setVisible(false);
			CRunPVToolkitSuperTeacherDiv div = (CRunPVToolkitSuperTeacherDiv)CDesktopComponents.vView().getComponent("superTeacherNewMethodDiv");
			if (div != null) {
				div.init(pvToolkit.getCurrentRunGroup(), true, _idMap);
			}
		}
		else if (action.equals("show_learningactivity")) {
			String tagId = (String)jsonObject.get("tagId");
	    	String role = (String)jsonObject.get("role");
			IXMLTag elementTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get("cacId")), tagId);
			if (elementTag != null) {
				String learningactivity = getLearningActivity(elementTag, role);
				if (learningactivity.equals("") && !role.equals("student")) {
					//NOTE if learning activity is empty and role is not student take student learning activity as default text
					learningactivity = getLearningActivity(elementTag, "student");
				}
				CKeditor fCKeditor = (CKeditor)vView.getComponent("updateMethodRichtextInput");
				if (fCKeditor != null) {
					//NOTE create clone of fck editor, otherwise if inputText is empty, value shown might be value entered for other element.
					CKeditor newFCKeditor = (CKeditor)fCKeditor.clone();
					Component parent = fCKeditor.getParent();
					fCKeditor.detach();
					parent.appendChild(newFCKeditor);

					newFCKeditor.setAttribute("tagId", tagId);
					newFCKeditor.setAttribute("role", role);
					newFCKeditor.setValue(learningactivity);
				}
			}
		}
		else if (action.equals("select_reference")) {
			String tagId = (String)jsonObject.get("tagId");
	    	String role = (String)jsonObject.get("role");
			String referenceTagId = (String)jsonObject.get("referenceTagId");
	    	String selected = (String)jsonObject.get("selected");
			IXMLTag elementTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get("cacId")), tagId);
			if (elementTag != null) {
				String referenceids = getStatusAttributeValue(elementTag, "referenceidsfor" + role).replace(AppConstants.statusCommaReplace, ",");
				boolean referenceidsContainsReferenceTagId = ("," + referenceids + ",").contains("," + referenceTagId + ",");
				if (selected.equals(AppConstants.statusValueTrue)) {
					if (!referenceidsContainsReferenceTagId) {
						referenceids += referenceids.equals("") ? referenceTagId : "," + referenceTagId;
					}
				}
				else {
					if (referenceidsContainsReferenceTagId) {
						String[] referenceidsArr = referenceids.split(",");
						referenceids = "";
						for (int i=0;i<referenceidsArr.length;i++) {
							if (!referenceidsArr[i].equals(referenceTagId)) {
								if (!referenceids.equals("")) {
									referenceids += ",";
								}
								referenceids += referenceidsArr[i];
							}
						}
					}					
				}
				editElementTagStatusAttributeValue(elementTag, "method", "referenceidsfor" + role, referenceids.replace(",", AppConstants.statusCommaReplace));
			}
		}
		else {
			super.onNotify(event);
		}
	}

	public void onSaveLearningactivity(Event event) {
		CKeditor fCKeditor = (CKeditor)vView.getComponent("updateMethodRichtextInput");
		String tagId = (String)fCKeditor.getAttribute("tagId");
    	String role = (String)fCKeditor.getAttribute("role");
    	String learningactivity = fCKeditor.getValue();
		IXMLTag elementTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get("cacId")), tagId);
		if (elementTag != null) {
			editElementTagChildValue(elementTag, "method", role.equals("student") ? "learningactivity" : "learningactivityfor" + role, learningactivity);
		}
	}

	protected String getLearningActivity(IXMLTag elementTag, String role) {
		String tagName = elementTag.getName();
		String learningactivity = sSpring.unescapeXML(elementTag.getChildValue(role.equals("student") ? "learningactivity" : "learningactivityfor" + role));
		//NOTE for legacy reasons step tags may have learning activities as child tags
		if (learningactivity.equals("") && tagName.equals(stepTagName)) {
			for (IXMLTag learningactivityTag : elementTag.getChilds("learningactivity")) {
				//NOTE student learning activity is preselected and teacher learning activity pid ends with '_teacher'
				if ((role.equals("student") && learningactivityTag.getCurrentStatusAttribute(AppConstants.statusKeyOpened).equals(AppConstants.statusValueTrue)) ||
						(role.equals("teacher") && learningactivityTag.getChildValue("pid").endsWith("_teacher"))) {
					learningactivity = sSpring.unescapeXML(learningactivityTag.getChildValue("richtext"));
					break;
				}
			}
		}
		return learningactivity;
	}
	
}
