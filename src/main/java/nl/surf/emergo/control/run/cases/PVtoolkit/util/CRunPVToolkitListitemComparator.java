/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.util;

import java.util.Comparator;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listitem;

public class CRunPVToolkitListitemComparator implements Comparator<Listitem> {

	//column number to sort on
	protected int columnNumber;
	protected boolean ascending;
	
    public CRunPVToolkitListitemComparator(int columnNumber, boolean ascending) {
    	this.columnNumber = columnNumber;
    	this.ascending = ascending;
    }
    
    public int compare(Listitem listitem1, Listitem listitem2) {
    	int compare = 0;
    	if (columnNumber < 0 || columnNumber >= listitem1.getChildren().size()) {
    		return compare;
    	}
    	//get children
    	//NOTE children of a listitem are listcells and each listcell may only contain one child 
    	Component child1 = listitem1.getChildren().get(columnNumber).getChildren().get(0);
    	Component child2 = listitem2.getChildren().get(columnNumber).getChildren().get(0);
    	if (child1.getAttribute("orderValue") instanceof String && child2.getAttribute("orderValue") instanceof String) {
        	//if orderValue is set use it. It then overrules a possible value of the component, e.g., a label value
        	compare = ((String)child1.getAttribute("orderValue")).compareTo((String)child2.getAttribute("orderValue"));
    	}
    	else if (child1 instanceof Label && child2 instanceof Label) {
        	//if labels compare label values
        	compare = ((Label)child1).getValue().compareTo(((Label)child2).getValue());
    	}
    	if (!ascending) {
    		compare = -compare;
    	}
        return compare;
    }

}
