/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import org.zkoss.util.media.Media;

import nl.surf.emergo.business.IFileManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CContentFilesFilter;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CIObserved;
import nl.surf.emergo.control.CIObserver;
import nl.surf.emergo.control.CObserverManager;
import nl.surf.emergo.control.def.CDefLabel;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpring;

/**
 * The Class CRunBlobLabel. Used to show a clickable label to show a blob.
 * 
 * Implements CIObserved en CIObserver interfaces and uses observerManager
 * to implement Observer design pattern.
 */
public class CRunBlobLabelClassic extends CDefLabel implements CIObserved, CIObserver {

	private static final long serialVersionUID = 2096189940845777827L;

	/** The observer manager. */
	protected CObserverManager observerManager = new CObserverManager(this);

	/** The blobcontainer, the xml tag containing the blob tag as child. */
	protected IXMLTag blobcontainer = null;

	/** The media. */
	protected Media media = null;

	/** The diskpath. */
	protected String diskpath = "";

	/** The filename. */
	protected String filename = "";
	
	/** The event action. */
	protected String eventAction = "";

	/** The event action status. */
	protected Object eventActionStatus = null;

	/** Is blob allowed to be opened in browser window? If false, only allow download. */
	protected boolean allowInline = true;

	public String getClassName() {
		return CDesktopComponents.vView().getRunClassName(className);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.def.CIObserved#registerObserver(java.lang.String)
	 */
	public void registerObserver(String aObserverId) {
		observerManager.registerObserver(aObserverId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.def.CIObserved#removeObserver(java.lang.String)
	 */
	public void removeObserver(String aObserverId) {
		observerManager.removeObserver(aObserverId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.def.CIObserved#notifyObservers(java.lang.String, java.lang.Object)
	 */
	public void notifyObservers(String aAction, Object aStatus) {
		observerManager.notifyObservers(aAction, aStatus);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.def.CIObserver#observedNotify(nl.surf.emergo.control.def.CIObserved, java.lang.String, java.lang.Object)
	 */
	public void observedNotify(CIObserved aObserved, String aAction,
			Object aStatus) {
		if (aObserved != null)
			this.notifyObservers(aAction, aStatus);
	}

	/**
	 * Sets the event action.
	 * 
	 * @param aEventAction the new event action
	 */
	public void setEventAction(String aEventAction) {
		eventAction = aEventAction;
	}

	/**
	 * Sets the event action status.
	 * 
	 * @param aEventActionStatus the new event action status
	 */
	public void setEventActionStatus(Object aEventActionStatus) {
		eventActionStatus = aEventActionStatus;
	}

	/**
	 * Instantiates a new empty c run blob label.
	 */
	public CRunBlobLabelClassic() {
	}

	/**
	 * Instantiates a new filled c run blob label, using aBlobContainer.
	 *
	 * @param aBlobContainer the a blob container, the xml tag containing the blob tag as child
	 */
	public CRunBlobLabelClassic(IXMLTag aBlobContainer) {
		blobcontainer = aBlobContainer;
		init();
		setOnClickWidgetListener();
	}

	/**
	 * Instantiates a new filled c run blob label, using aBlobContainer and aLabel.
	 *
	 * @param aBlobContainer the a blob container
	 * @param aLabel the a label
	 */
	public CRunBlobLabelClassic(IXMLTag aBlobContainer,String aLabel) {
		blobcontainer = aBlobContainer;
		init(aLabel);
		setOnClickWidgetListener();
	}

	/**
	 * Instantiates a new filled c run blob label, using aBlobContainer, with boolean to determine label type.
	 *
	 * @param aBlobContainer the a blob container
	 * @param aAllowInline false if only download is allowed
	 */
	public CRunBlobLabelClassic(IXMLTag aBlobContainer, boolean aAllowInline) {
		blobcontainer = aBlobContainer;
		allowInline = aAllowInline;
		init();
		setOnClickWidgetListener();
	}

	/**
	 * Instantiates a new filled c run blob label, using aMedia.
	 *
	 * @param aMedia the a media
	 */
	public CRunBlobLabelClassic(Media aMedia) {
		media = aMedia;
		init(media.getName());
		setOnClickWidgetListener();
	}

	/**
	 * Sets the spring.
	 * 
	 * @param aSpring the new spring
	 */
	public void setSpring(SSpring aSpring) {
	}

	/**
	 * Sets the blob container, the xml tag containing the blob tag as child.
	 * 
	 * @param aBlobContainer the new blob container
	 */
	public void setBlobContainer(IXMLTag aBlobContainer) {
		blobcontainer = aBlobContainer;
		setOnClickWidgetListener();
	}

	/**
	 * Inits by setting value to blob file name and setting style to hot word style.
	 */
	public void init() {
		String lName = blobcontainer.getChildValue("name");
		if (lName.equals("")) {
			lName = blobcontainer.getChildValue("pid");
		}
		init(CDesktopComponents.sSpring().unescapeXML(lName));
	}

	/**
	 * Inits by setting value to aLabel and setting style to hot word style.
	 * 
	 * @param aLabel the a label
	 */
	public void init(String aLabel) {
		setValue(aLabel);
		if (media != null || CDesktopComponents.sSpring().getSBlobHelper().hasUrl(blobcontainer))
			setZclass(getClassName() + "_active");
		else
			setZclass(getClassName() + "_inactive");
	}

	/**
	 * set on click widget listener.
	 */
	public void setOnClickWidgetListener() {
		String lHrefBase = getHref();
		if (!lHrefBase.equals("") && CDesktopComponents.sSpring() != null) {
			String lHref = CDesktopComponents.sSpring().getSBlobHelper().convertHrefForCertainMediaTypes(lHrefBase);
			if (!CDesktopComponents.vView().isAbsoluteUrl(lHref)) {
				if (!lHref.equals("") && lHref.indexOf("/") != 0)
					lHref = "/" + lHref;
				lHref = CDesktopComponents.vView().getEmergoWebappsRoot() + lHref;
			}
			if (!lHref.equals("")) {
				// avoid javascript unterminated string error
				lHref = lHref.replace("'", "\\'");
				setWidgetListener("onClick", CDesktopComponents.vView().getJavascriptWindowOpenFullscreen(lHref));
				if (!allowInline)
					CContentFilesFilter.addOnlyDownloadResource(lHrefBase);
			}
		}
	}

	/**
	 * Gets href
	 *
	 * @return the href
	 */
	protected String getHref() {
		String lHref = "";
		if (blobcontainer != null)
			lHref = CDesktopComponents.sSpring().getSBlobHelper().getUrl(blobcontainer);
		if (media != null)
			lHref = getMediaHref();
		return lHref;
	}

	/**
	 * Gets the href using media and creates temp file out of media.
	 * Deletes previous temp file.
	 *
	 * @return the href
	 */
	public String getMediaHref() {
		String lHref = "";
		deleteTempFile();
		// lText is blob in database, so create blob in temp map using session
		// id. And create url.
		String lSubPath = CDesktopComponents.vView().getUniqueTempSubPath();
		// path within browser
		String lPath = VView.getInitParameter("emergo.temp.path") + lSubPath;
		// path on disk
		String lDiskPath = CDesktopComponents.sSpring().getAppManager().getAbsoluteTempPath() + lSubPath;

		IFileManager fileManager = (IFileManager) CDesktopComponents.sSpring().getBean("fileManager");
		if (media != null) {
			String lFilename = fileManager.createFile(lDiskPath, media
					.getName(), CDesktopComponents.sSpring().getSBlobHelper().mediaToByteArray(media));
			if (!lFilename.equals("")) {
				diskpath = lDiskPath;
				filename = lDiskPath + media.getName();
				lHref = lPath + media.getName();
			}
		}
		return lHref;
	}

	/**
	 * Deletes temp file.
	 */
	public void deleteTempFile() {
		IFileManager fileManager = (IFileManager) CDesktopComponents.sSpring().getBean("fileManager");
		if (!filename.equals(""))
			fileManager.deleteFile(filename);
		filename = "";
		if (!diskpath.equals(""))
			fileManager.deleteDir(diskpath);
		diskpath = "";
	}

	/**
	 * On click save selected and opened status.
	 */
	public void onClick() {
		notifyObservers(eventAction, eventActionStatus);
	}

}
