/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Event;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;

public class CRunFormsFeedbackPiecesDiv extends CDefDiv {

	private static final long serialVersionUID = 6430824748884855051L;
	
	protected String formId = "";

	public void onUpdate(Event aEvent) {
		IXMLTag feedbackConditionTag = (IXMLTag)aEvent.getData();
		CRunForms runForms = (CRunForms)CDesktopComponents.vView().getComponent(formId);
		if (runForms != null) {
			List<Hashtable<String,Object>> pieceOrRefpieceDataElementsList = new ArrayList<Hashtable<String,Object>>();
			if (feedbackConditionTag == null) {
				feedbackConditionTag = runForms.getCurrentFeedbackConditionTag((IXMLTag)getAttribute("formTag"));
			}
			List<IXMLTag> pieceOrRefpieceTags = runForms.getPieceAndRefpieceTags(feedbackConditionTag);
			runForms.handlepieceOrRefpieces(pieceOrRefpieceDataElementsList, pieceOrRefpieceTags); 
			//set macro parameter and rerender
			HtmlMacroComponent macro = (HtmlMacroComponent)runForms.getMacroChild(this);
			if (macro != null) {
				macro.setDynamicProperty("a_pieces", pieceOrRefpieceDataElementsList);
				macro.recreate();
			}
			setVisible(pieceOrRefpieceDataElementsList.size() > 0);
		}
	}

}
