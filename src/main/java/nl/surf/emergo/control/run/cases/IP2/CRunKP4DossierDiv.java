/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.IP2;

import java.util.Map;

import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.run.CRunArea;
import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.control.run.CRunEditforms;

public class CRunKP4DossierDiv extends CRunArea {

	private static final long serialVersionUID = -5352102946475477695L;

	public String relativePath = "";
	public String[] folders = null;
	public String[][] resources = null;

	public CRunKP4DossierDiv() {
		super();
		init();
	}

	public void init() {
		Events.postEvent("onInitZulfile", this, null);
	}


	/**
	 * Methods originally used in ZUL file.
	 */

	public void onInitZulfile(Event aEvent) {
		HtmlMacroComponent macro = (HtmlMacroComponent)CRunComponent.getMacroParent(this);
		Map<String,Object> propertyMap = (Map<String,Object>)macro.getDynamicProperty("a_propertyMap");
		CRunEditforms currentEmergoComponent = (CRunEditforms)propertyMap.get("currentEmergoComponent");
		if (currentEmergoComponent == null) {
			return;
		}
		relativePath = currentEmergoComponent.getZulfilePath((IXMLTag)getParent().getAttribute("tag"));

		folders = new String[]
				{
				"Personalia",
				"Correspondentie",
				"Intake en diagnostiek",
				"Behandelplan",
				"Decursus"
				};
		resources = new String[][]
				{
					{
						"Personalia",
						"Toestemmingsverklaring inzage dossier",
						"Toestemmingsverklaring opvragen NPO"
					},
					{
						"Opvragen neuropsychologisch rapport",
						"Verwijsbrief"
					},
					{
						"Neuropsychologisch rapport",
						"Intakeverslag",
						"Werkaantekeningen Philip",
						"Outcome Questionnaire 1",
						"Outcome Questionnaire 2",
						"Outcome Questionnaire 3 (sessie 12)", 
						"Uitwerking 3e Outcome Questionnaire"
					},
					{
						"Behandelplan"
					},
					{
						"Decursus"
					}
				};
	}

}
