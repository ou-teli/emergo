/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.IP2;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Label;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefVbox;

public class CRunKP4DossierVbox extends CDefVbox {

	private static final long serialVersionUID = -4897827202445696461L;

	protected CRunKP4DossierDiv dossierDiv = null;

	public void onCreate(CreateEvent aEvent) {
		dossierDiv = (CRunKP4DossierDiv)getParent();
		if (dossierDiv == null) {
			return;
		}
		for (int i=0;i<dossierDiv.folders.length;i++) {
			Events.sendEvent("onCreateFolder", this, i);
			for (int j=0;j<dossierDiv.resources[i].length;j++) {
				Events.sendEvent("onCreateResource", this, new Integer[]{i, j});
			}
		}
	}

	public void onCreateFolder(Event aEvent) {
		Label label = new Label(dossierDiv.folders[(int)aEvent.getData()]);
		appendChild(label);
	}

	public void onCreateResource(Event aEvent) {
		Label label = new Label(dossierDiv.resources[((Integer[])aEvent.getData())[0]][((Integer[])aEvent.getData())[1]]);
		label.setSclass("CRunEditformLink");
		label.setStyle("position:relative;left:10px;top:-4px;");
		String href = dossierDiv.relativePath + "KP4_dossier/" + dossierDiv.resources[((Integer[])aEvent.getData())[0]][((Integer[])aEvent.getData())[1]] + ".pdf";
		label.setWidgetListener("onClick", CDesktopComponents.vView().getJavascriptWindowOpenFullscreen(href));
		appendChild(label);
	}

}
