/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import java.util.List;
import java.util.Locale;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Include;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CIObserved;
import nl.surf.emergo.control.def.CDefInclude;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SDatatagReference;

/**
 * The Class CRunConversations is used to show conversation videos and images within the run view area of the
 * Emergo player. Conversation questions are shown within the run choice area of the player.
 */
public class CRunConversationsClassic extends CRunComponentClassic {

	protected static final long serialVersionUID = -4693369493699716037L;

	/** The run conversation image view to show still images during conversation. */
	protected CRunConversationImageViewClassic imageView = null;

	/** The video view to show the video being played. It's an include so src can be changed. */
	public Include videoView = null;

	/** The saverunchoiceareastatus, to reset status of run choice area after closing conversation. */
	protected String saverunchoiceareastatus = "";

	/** The current conversation tag. */
	protected IXMLTag currentconversationtag = null;

	/** The current startfragment tag. */
	protected IXMLTag currentstartfragmenttag = null;

	/** The current fragment tag. */
	protected IXMLTag currentfragmenttag = null;

	/** The x-value of the conversation video area position. */
	protected String conversationVideoLeftPosition = "";

	/** The y-value of the conversation video area position. */
	protected String conversationVideoTopPosition = "";

	/** The original style. */
	protected String originalStyle = "";

	/**
	 * Instantiates a new c run conversations.
	 * Creates views for image and for video.
	 * Gets current conversation, sets selected and opened of it to true and starts it.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the a case component
	 */
	public CRunConversationsClassic(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);

		createImageView(this);
		createVideoView(this);

		// runchoicearea shows conversationinteraction
		if (runWnd.runChoiceArea.getStatus().equals("conversation"))
			// conversation after conversation
			runWnd.runChoiceArea.restoreStatus();
		// save runchoiceareastatus and set new status
		saverunchoiceareastatus = runWnd.runChoiceArea.getStatus();
		runWnd.runChoiceArea.setStatus("conversation", this);

		originalStyle = getStyle();
		if (originalStyle == null)
			originalStyle = "";

		// get conversation to play
		CRunConversationsHelperClassic cComponent = new CRunConversationsHelperClassic(null, "question", caseComponent, this);
		IXMLTag lXmlTree = cComponent.getXmlDataPlusStatusTree();
		currentconversationtag = getCurrentConversation(lXmlTree.getChild(AppConstants.contentElement).getChildTags(AppConstants.defValueNode));
		if (currentconversationtag == null)
			return;

		initVideoPositionAndSize();

		// set conversation status
		setRunTagStatus(caseComponent, currentconversationtag, AppConstants.statusKeySelected, AppConstants.statusValueTrue, true);
		setRunTagStatus(caseComponent, currentconversationtag, AppConstants.statusKeyOpened, AppConstants.statusValueTrue, true);
//		maybe add something for shared component

		CRunConversationInteractionClassic lInteraction = (CRunConversationInteractionClassic) CDesktopComponents.vView().getComponent("runConversationInteraction");
		if (lInteraction != null)
			lInteraction.areQuestionsShown = false;

		// check if conversation must start with fragment or not
		currentstartfragmenttag = getCurrentStartFragment(currentconversationtag);
		if (currentstartfragmenttag != null)
			playFragment(currentconversationtag, currentstartfragmenttag);
		else
			showQuestions(currentconversationtag);

		runWnd.setNoteTag(caseComponent, currentconversationtag);
	}

	public Include getVideoView() {
		return videoView;
	}

	protected IXMLTag getTagChildTag(IXMLTag aTag, String aTagChildName) {
		if (aTag == null) {
			return null;
		}
		return aTag.getChild(aTagChildName);
	}

	protected String getTagPixelChildValue(IXMLTag aTag, String aTagChildName, int index) {
		IXMLTag childTag = getTagChildTag(aTag, aTagChildName);
		if (childTag == null) {
			return "";
		}
		String values = childTag.getValue();
		if (values.equals("")) {
			return "";
		}
		int value = 0;
		String[] arr = values.split(",");
		if (index >= arr.length) {
			return "";
		}
		try {
			value = Integer.parseInt(arr[index]);
		} catch (NumberFormatException e) {
			return "";
		}
		return "" + value + "px";
	}

	protected String getConversationTagChildValue(String aConversationTagChildName, int index) {
		return getTagPixelChildValue(currentconversationtag, aConversationTagChildName, index);
	}

	protected String getConversationVideoOverlay() {
		String overlaySrc = CDesktopComponents.sSpring().getSBlobHelper().getUrl(currentconversationtag);
		overlaySrc = overlaySrc.replace("\\", "/");
		if (overlaySrc.indexOf("/") == 0)
			overlaySrc = overlaySrc.substring(1);
		if (!overlaySrc.equals("") && !CDesktopComponents.vView().isAbsoluteUrl(overlaySrc))
			overlaySrc = CDesktopComponents.vView().getEmergoWebappsRoot() + "/" + overlaySrc;
		return overlaySrc;
	}

	/**
	 * Creates image view.
	 *
	 * @param aParent the a parent
	 */
	protected void createImageView(Component aParent) {
		CRunHboxClassic lHbox = new CRunHboxClassic();
		imageView = new CRunConversationImageViewClassic("runConversationsImageView");
		imageView.setVisible(false);
		lHbox.appendChild(imageView);
		aParent.appendChild(lHbox);
	}

	/**
	 * Creates video view.
	 *
	 * @param aParent the a parent
	 */
	protected void createVideoView(Component aParent) {
		videoView = new CDefInclude();
		videoView.setId("runConversationsVideoView");
		// set class or is sizing player enough?
		videoView.setSclass("");
		videoView.setVisible(false);
		videoView.setAttribute("runComponentId", getId());
		aParent.appendChild(videoView);
	}

	/**
	 * Gets video position and size for current conversation and, if set, sets position and size of conversation and video frame.
	 */
	protected void initVideoPositionAndSize() {
		// NOTE Get position and size of conversation. If not null, it overrules default values in style class.
		setStyle(originalStyle + "position:absolute;");
		originalStyle = getStyle();
		String lVideoPositionLeft = getConversationTagChildValue("position", 0);
		if (!(lVideoPositionLeft.equals(""))) {
			// NOTE Set position and size of conversation
			setLeft(lVideoPositionLeft);
			//videoView.setLeft(lVideoPositionLeft);
			conversationVideoLeftPosition = lVideoPositionLeft;
		} else
			conversationVideoLeftPosition = getLeft();
		String lVideoPositionTop = getConversationTagChildValue("position", 1);
		if (!(lVideoPositionTop.equals(""))) {
			// NOTE Set position and size of conversation
			setTop(lVideoPositionTop);
			//videoView.setTop(lVideoPositionTop);
			conversationVideoTopPosition = lVideoPositionTop;
		} else
			conversationVideoTopPosition = getTop();
		String lVideoSizeWidth = getConversationTagChildValue("size", 0);
		if (!(lVideoSizeWidth.equals(""))) {
			// NOTE Set position and size of conversation
			setWidth(lVideoSizeWidth);
			//videoView.setWidth(lVideoSizeWidth);
		}
		String lVideoSizeHeight = getConversationTagChildValue("size", 1);
		if (!(lVideoSizeHeight.equals(""))) {
			// NOTE Set position and size of conversation
			setHeight(lVideoSizeHeight);
			//videoView.setHeight(lVideoSizeHeight);
		}

		// NOTE Set session vars to be used by video player
		/*
		CDesktopComponents.vView().getSession().setAttribute("video_position_top", lVideoPositionTop);
		CDesktopComponents.vView().getSession().setAttribute("video_position_left", lVideoPositionLeft);
		CDesktopComponents.vView().getSession().setAttribute("video_size_width", lVideoSizeWidth);
		CDesktopComponents.vView().getSession().setAttribute("video_size_height", lVideoSizeHeight);
		CDesktopComponents.vView().getSession().setAttribute("video_overlay", getConversationVideoOverlay());
		*/
		// NOTE Set attributes to be used by video player
		videoView.setAttribute("video_position_left", lVideoPositionLeft);
		videoView.setAttribute("video_position_top", lVideoPositionTop);
		videoView.setAttribute("video_size_width", lVideoSizeWidth);
		videoView.setAttribute("video_size_height", lVideoSizeHeight);
		videoView.setAttribute("video_overlay", getConversationVideoOverlay());
	}

	/**
	 * Is called when run conversations component is closed or user wants to ask a question.
	 *
	 * @param aObserved the observed object, it should have interface CIObserved implemented
	 * @param aAction the action
	 * @param aStatus the status object, can be String or other class
	 */
	@Override
	public void observedNotify(CIObserved aObserved, String aAction, Object aStatus) {
		if (aAction.equals("endComponent")) {
			endCurrentConversation();
			// necessary?
			if (runComponentView != null)
				runComponentView.endComponent();
		}
		if (aAction.equals("askQuestion")) {
			showQuestions(currentconversationtag);
		}
	}

	/**
	 * Gets the current conversation.
	 *
	 * @param aConversationTags the a conversation tags
	 *
	 * @return the conversation tag
	 */
	public IXMLTag getCurrentConversation(List<IXMLTag> aConversationTags) {
		IXMLTag lActTag = runWnd.runViewArea.getActCaseComponentTag();
		if (lActTag == null)
			return null;
		return CDesktopComponents.sSpring().getXmlManager().getTagById(aConversationTags, lActTag.getAttribute(AppConstants.keyId));
	}

	/**
	 * Gets the current start fragment of the conversation.
	 *
	 * @param aConversation the a conversation
	 *
	 * @return the current start fragment tag or null if none
	 */
	public IXMLTag getCurrentStartFragment(IXMLTag aConversation) {
		if (aConversation == null)
			return null;
		List<IXMLTag> lChildTags = aConversation.getChildTags(AppConstants.defValueNode);
		for (IXMLTag lChildTag : lChildTags) {
			String lName = lChildTag.getName();
			if (lName.equals("fragment")) {
				boolean lPresent = (CDesktopComponents.sSpring().getCurrentTagStatus(lChildTag, AppConstants.statusKeyPresent).equals(AppConstants.statusValueTrue));
				boolean lOpened = (CDesktopComponents.sSpring().getCurrentTagStatus(lChildTag, AppConstants.statusKeyOpened).equals(AppConstants.statusValueTrue));
				if (lPresent && lOpened)
					// get first opened
					return lChildTag;
			}
		}
		return null;
	}

	/**
	 * Gets video position for current fragment and, if set, overrules position of conversation and video frame.
	 */
	protected void adjustVideoPosition(IXMLTag aFragment) {
		// NOTE Get position of fragment. If not null, it overrules conversation position.
		String lVideoPositionLeft = getTagPixelChildValue(aFragment, "position", 0);
		String lVideoPositionTop = getTagPixelChildValue(aFragment, "position", 1);
		// NOTE Set position of conversation
		if (!lVideoPositionLeft.equals(""))
			setLeft(lVideoPositionLeft);
		else
			setLeft(conversationVideoLeftPosition);
		if (!lVideoPositionTop.equals(""))
			setTop(lVideoPositionTop);
		else
			setTop(conversationVideoTopPosition);
	}

	/**
	 * Sets Include attributes to be used by fragment player. In classic skin only used for text fragments
	 *
	 * @param aFragment the a fragment
	 */
	protected void initFragmentPositionAndSize(IXMLTag aFragment) {
		videoView.setAttribute("fragment_position_left", getTagPixelChildValue(aFragment, "position", 0));
		videoView.setAttribute("fragment_position_top", getTagPixelChildValue(aFragment, "position", 1));
		videoView.setAttribute("fragment_size_width", getTagPixelChildValue(aFragment, "size", 0));
		videoView.setAttribute("fragment_size_height", getTagPixelChildValue(aFragment, "size", 1));
	}

	/**
	 * Gets referenced fragment.
	 *
	 * @param aCaseComponent the a case component
	 * @param aTag the a tag
	 *
	 * @return boolean
	 */
	public SDatatagReference getReferencedFragment(IECaseComponent aCaseComponent, IXMLTag aTag) {
		SDatatagReference lReference = CDesktopComponents.sSpring().getSReferencedDataTagHelper().getDatatagReference(aCaseComponent, aTag, "reffragment");
		if (lReference == null || lReference.getCaseComponent() == null || lReference.getDataTag() == null) {
			return null;
		}
		return lReference;
	}

	/**
	 * Plays text.
	 *
	 * @param aFragmentView the a fragment view
	 * @param aFragmentTag the a fragment tag
	 * @param aText the a text
	 *
	 * @return if fragment view should be shown
	 */
	public boolean playText(Include aFragmentView, IXMLTag aFragmentTag, String aText) {
		aFragmentView.setVisible(false);
		if (!aText.equals("")) {
			// show text if not empty
			aFragmentView.setAttribute("tag", aFragmentTag);
			aFragmentView.setAttribute("text", aText);
			SDatatagReference lNextFragment = getReferencedFragment(getCaseComponent(), aFragmentTag);
			//NOTE try to get referenced fragment of fragment tag. If no referenced fragment, try to get referenced fragment of the parent tag that might be an alternative
			if (lNextFragment == null) {
				lNextFragment = getReferencedFragment(getCaseComponent(), aFragmentTag.getParentTag());
			}
			aFragmentView.setAttribute("hasNextFragment", "" + (lNextFragment != null));
			Component runTextDiv = CDesktopComponents.vView().getComponent("runTextDiv_" + aFragmentView.getAttribute("runComponentId"));
			if (runTextDiv == null) {
				aFragmentView.setSrc(VView.v_run_empty_fr);
				aFragmentView.setSrc(VView.v_run_text_fr);
			}
			else {
				Events.postEvent("onPlayText", runTextDiv, null);
			}
			aFragmentView.setVisible(true);
			// set started status of fragment
			setRunTagStatus(getCaseComponent(), aFragmentTag, AppConstants.statusKeyStarted, AppConstants.statusValueTrue, true);
		}
		else {
			aFragmentView.setSrc(VView.v_run_empty_fr);
		}
		return !aText.equals("");
	}

	/**
	 * Plays fragment and sets status of runchoicearea according to conversation type (with or without questions).
	 * Fragment formats can be flv, swf, wmv, mpg, wma, wav.
	 * If the format isn't equal to one of these the following applies.
	 * If the fragment is an url and it contains youtube.com, the youtube video is played embedded within an iframe.
	 * All other urls are shown within an embedded iframe.
	 *
	 * @param aConversation the a conversation
	 * @param aFragment the a fragment
	 */
	public void playFragment(IXMLTag aConversation, IXMLTag aFragment) {
		videoView.setSrc("run_component_empty_fr.zul");
		if (runWnd == null || aFragment == null)
			return;
		if (aConversation != null) {
			videoView.setAttribute("showCloseButton", "" + aConversation.getDefChildValue("showclosebutton").equals(AppConstants.statusValueTrue));
			videoView.setAttribute("showControls", "" + aConversation.getDefChildValue("showcontrols").equals(AppConstants.statusValueTrue));
			String zindex = aConversation.getDefChildValue("z-index");
			if (!zindex.equals("")) {
				setStyle(originalStyle + "z-index:" + zindex + ";");
			}
		}
		boolean lVisible = false;
		// set status of conversation interaction to video
		CRunConversationInteractionClassic lInteraction = (CRunConversationInteractionClassic) CDesktopComponents.vView().getComponent("runConversationInteraction");
		lInteraction.setStatus("video");
		// set status of buttons
		lInteraction.setButtonsStatus(areQuestions(aConversation));
		// hide image
		boolean lImageVisible = imageView.isVisible();
		imageView.setVisible(false);
		// get url of fragment
		String lUrl = CDesktopComponents.sSpring().getSBlobHelper().getUrl(aFragment);
		if (lUrl.equals("")) {

			String lText = CDesktopComponents.sSpring().unescapeXML(aFragment.getChildValue("text"));
			if (!lText.equals("")) {
				showCurrentBackgroundImage(aConversation);
				initFragmentPositionAndSize(aFragment);
				playText(videoView, aFragment, lText);
			} else {
				// show description if not empty and no streaming media found
				String lDescription = CDesktopComponents.sSpring().unescapeXML(aFragment.getChildValue("description"));
				if (!lDescription.equals("")) {
					videoView.setAttribute("description_title", CDesktopComponents.vView().getCLabel(VView.childtagLabelKeyPrefix + "description"));
					videoView.setAttribute("description", lDescription);
					videoView.setSrc("run_component_description_fr.zul");
					videoView.setVisible(true);
				}
			}

			setRunTagStatus(caseComponent, aFragment, AppConstants.statusKeyOpened, AppConstants.statusValueTrue, true);
//			maybe add something for shared component
			// save current fragment
			currentfragmenttag = aFragment;
			return;
		}
		IXMLTag lBlobChild = aFragment.getChild("blob");
		String lBlobtype = lBlobChild.getAttribute(AppConstants.keyBlobtype);
		if (!(lBlobtype.equals(AppConstants.blobtypeExtUrl))) {
			// put emergo path before url for player
			if (!CDesktopComponents.vView().isAbsoluteUrl(lUrl))
				lUrl = CDesktopComponents.vView().getEmergoRootUrl() + CDesktopComponents.vView().getEmergoWebappsRoot() + lUrl;
		}
		// determine if wmv, mpg, flv or swf
		String lFileExtension = "";
		int lPos = lUrl.lastIndexOf(".");
		if (lPos > 0) {
			lFileExtension = (lUrl.substring(lPos + 1, lUrl.length())).toLowerCase(Locale.ENGLISH);
		}
		// urls can have parameters
		lPos = lFileExtension.indexOf("?");
		if (lPos > 0) {
			lFileExtension = lFileExtension.substring(0, lPos);
		}
		// set opened status of fragment
		setRunTagStatus(caseComponent, aFragment, AppConstants.statusKeyOpened, AppConstants.statusValueTrue, true);
//		maybe add something for shared component
		// save current fragment
		currentfragmenttag = aFragment;
		// show media control buttons
		// for flash control will be within flash player
		runWnd.runTitleArea.setMediaBtnsVisible(false);
		if ((lFileExtension.equals("wmv")) || (lFileExtension.equals("mpg")))
			runWnd.runTitleArea.setMediaBufferingVisible(false);
		runWnd.runTitleArea.setMediaBtnsAction("");
		lInteraction.setButtonsAction("");
		if (lFileExtension.matches("wmv|mpg|wma|wav")) {
			runWnd.runTitleArea.setMediaBtnsAction(lFileExtension);
			lInteraction.setButtonsAction(lFileExtension);
			// show player frame with correct url, will autoplay
			CDesktopComponents.cControl().setAccSessAttr("media_url", lUrl);
			if ((lFileExtension.equals("wmv")) || (lFileExtension.equals("wma"))) {
				if (runWnd.showMediaControl)
					videoView.setSrc("run_windowsmedia_with_control_fr.zul");
				else
					videoView.setSrc("run_windowsmedia_fr.zul");
			}
			else
				videoView.setSrc("run_windowsmedia_mpg_fr.zul");
			if ((lFileExtension.equals("wmv")) || (lFileExtension.equals("mpg")))
				lVisible = true;
		}
		else {
			if (lFileExtension.matches("flv|mp4|swf|unity3d|webm|ogv|fla|mp3")) {
				runWnd.runTitleArea.setMediaBtnsAction(lFileExtension);
				lInteraction.setButtonsAction(lFileExtension);
				if (lFileExtension.matches("flv|mp4|webm|ogv|fla|mp3")) {
					// show player frame with correct url, will autoplay, if background it is shown
					String lBackName = "";
					IXMLTag lBackTag = CDesktopComponents.sSpring().getSBlobHelper().getBackgroundTag(aConversation);
					if (lBackTag != null) {
						lBackName = CDesktopComponents.sSpring().getSBlobHelper().getUrl(lBackTag);
						lBackName = lBackName.replace("\\", "/");
						if (lBackName.indexOf("/") == 0)
							lBackName = lBackName.substring(1);
					}
					videoView.setAttribute("url", lUrl);
					videoView.setAttribute("image", lBackName);
					videoView.setSrc("run_flash_fr.zul");
				}
				if (lFileExtension.equals("swf")) {
					videoView.setAttribute("url", lUrl);
					videoView.setSrc("run_flash_swf_fr.zul");
				}
				if (lFileExtension.equals("unity3d")) {
					CDesktopComponents.cControl().setAccSessAttr("unity_url", lUrl);
					videoView.setSrc("run_unity_fr.zul");
				}
			}
			else {
				lPos = lUrl.indexOf("youtube.com");
				if (lPos > 0) {
					CDesktopComponents.cControl().setAccSessAttr("youtube_url", lUrl);
					videoView.setSrc("run_youtube_fr.zul");
				}
				else {
					if (!CDesktopComponents.vView().isAbsoluteUrl(lUrl))
						// add root otherwise iframe will add root and '/emergo' so
						// lUrl will contain '/emergo/emergo' and resource will not be found
						lUrl = CDesktopComponents.vView().getEmergoRootUrl() + lUrl;
					videoView.setAttribute("tag", aFragment);
					videoView.setAttribute("url", lUrl);
					videoView.setSrc("run_website_fr.zul");
				}
			}
			lVisible = true;
		}

		videoView.setVisible(lVisible);
		if (!lVisible)
			imageView.setVisible(lImageVisible);
		adjustVideoPosition(aFragment);
	}

	/**
	 * Shows questions for conversation within runchoicearea and shows current background image of conversation, can be non existing.
	 *
	 * @param aConversation the a conversation
	 */
	public void showQuestions(IXMLTag aConversation) {
		// hide media control buttons
		runWnd.runTitleArea.setMediaBtnsVisible(false);
		// set status of conversation interaction to questions
		CRunConversationInteractionClassic lInteraction = (CRunConversationInteractionClassic) CDesktopComponents.vView().getComponent("runConversationInteraction");
		lInteraction.setStatus("conversation");
		// show questions
		CRunQuestionsTreeClassic lTree = (CRunQuestionsTreeClassic) CDesktopComponents.vView().getComponent("runQuestionsTree");
		lTree.showQuestions(caseComponent, aConversation);
		// hide video and set src empty, so player will stop
		videoView.setVisible(false);
		videoView.setSrc("run_component_empty_fr.zul");
		// show image
		showCurrentBackgroundImage(aConversation);
	}

	/**
	 * Shows current background image for conversation.
	 *
	 * @param aConversation the a conversation
	 */
	public void showCurrentBackgroundImage(IXMLTag aConversation) {
		// show image
		imageView.setVisible(false);
		CRunBackgroundImageClassic lImage = imageView.getRunBackgroundImage();
		lImage.showImage(aConversation);
		imageView.setVisible(true);
	}

	/**
	 * Ends current conversation and sets opened to false and finished to true.
	 */
	public void endCurrentConversation() {
		closeConversation();
		// save status of conversation
		setRunTagStatus(caseComponent, currentconversationtag, AppConstants.statusKeyOpened, AppConstants.statusValueFalse, true);
		setRunTagStatus(caseComponent, currentconversationtag, AppConstants.statusKeyFinished, AppConstants.statusValueTrue, true);
//		maybe add something for shared component
	}

	/**
	 * Close conversation and restores runchoicearea status.
	 */
	public void closeConversation() {
		// hide media control buttons
		runWnd.runTitleArea.setMediaBtnsVisible(false);
		// restore runchoicearea status
		runWnd.runChoiceArea.setStatus(saverunchoiceareastatus);
		removeVideoView();
		// hide image
		imageView.setVisible(false);
	}

	/**
	 * Remove video view. To stop video from playing.
	 */
	public void removeVideoView() {
		if (videoView != null) {
			// hide video and set src empty, so player will stop
			videoView.setVisible(false);
			videoView.setSrc("run_component_empty_fr.zul");
			videoView.detach();
			videoView = null;
		}
	}

	/**
	 * Checks if there are questions for the fragment.
	 *
	 * @param aFragment the a fragment
	 *
	 * @return true, if successful
	 */
	public boolean areQuestions(IXMLTag aFragment) {
		if (aFragment == null)
			return false;
		// if maps and/or questions
		List<IXMLTag> lMapChilds = aFragment.getChilds("map");
		List<IXMLTag> lQuestionChilds = aFragment.getChilds("question");
		return ((lMapChilds.size() > 0) || (lQuestionChilds.size() > 0));
	}

	/**
	 * On action. Is used to show a buffering alert and to show play/pause buttons for the windows media player.
	 *
	 * @param sender the sender
	 * @param action the action
	 * @param status the status
	 */
	public void onAction(String sender, String action, Object status) {
		if (action.equals("onEmNotify")) {
			String[] lStatus = (String[]) status;
			if (lStatus[0].equals("media_ended")) {
				// set status finished of fragment to true
				// notify media_ended is not reliable!
				setRunTagStatus(caseComponent, currentfragmenttag, AppConstants.statusKeyFinished, AppConstants.statusValueTrue, true);
//				maybe add something for shared component
			}
			if (lStatus[0].equals("buffering")) {
				runWnd.runTitleArea.setMediaBufferingVisible(true);
				runWnd.runTitleArea.setMediaBtnsVisible(false);
			}
			if (lStatus[0].equals("playing")) {
				runWnd.runTitleArea.setMediaBufferingVisible(false);
				runWnd.runTitleArea.setMediaBtnsVisible(true);
			}
		}
	}

	/**
	 * Get conversation. loops through the predecessors of a tag to find the conversation tag.
	 *
	 * @param aChildTag tag that is a (sub) child of a converation tag
	 */
	private IXMLTag getConversation (IXMLTag aChildTag) {
		IXMLTag lConvTag = null;
		if (aChildTag != null) {
			lConvTag = aChildTag.getParentTag();
			while (!((lConvTag == null) || (lConvTag.getName().equals("conversation")))) {
				lConvTag = lConvTag.getParentTag();
			}
		}
		return lConvTag;
	}

	/**
	 * Update. If status of an element of the shown conversation has changed, re-render it.
	 *
	 * @param aComp the conversations component
	 * @param aTag the element that has changed
	 * @param aKey the status parameter
	 * @param aValue the status new value
	 */
	public void update(IECaseComponent aComp, IXMLTag aTag, String aKey, String aValue) {
		if (aTag == null)
			return;
		String lTagName = aTag.getName();
		boolean lConversationBackgroundTag = (lTagName.equals("background"));
		boolean lConversationFragmentTag = (lTagName.equals("fragment"));
		boolean lConversationQuestionTag = (lTagName.equals("question"));
		boolean lConversationMapTag = (lTagName.equals("map"));
		boolean lPresentChanged = aKey.equals(AppConstants.statusKeyPresent);
		boolean lAccessibleChanged = aKey.equals(AppConstants.statusKeyAccessible);
		boolean lOpenedChanged = aKey.equals(AppConstants.statusKeyOpened);
		if (currentconversationtag == null)
			return;
		if (caseComponent.getCacId() != aComp.getCacId())
			return;
		IXMLTag lChangedConversation = getConversation (aTag);
		if (lChangedConversation == null || !lChangedConversation.getAttribute(AppConstants.keyId).equals(currentconversationtag.getAttribute(AppConstants.keyId)))
			return;
		// update the status:
		CRunConversationsHelperClassic cComponent = new CRunConversationsHelperClassic(null, "question", caseComponent, this);
		IXMLTag lXmlTree = cComponent.getXmlDataPlusStatusTree();
		cComponent.setItemAttributes(this, lXmlTree.getChild(AppConstants.contentElement));
		currentconversationtag = getCurrentConversation(lXmlTree.getChild(AppConstants.contentElement).getChildTags(AppConstants.defValueNode));
		if (lConversationBackgroundTag && lOpenedChanged) {
				// then show the background
				showCurrentBackgroundImage(currentconversationtag);
		}
		if ((lConversationFragmentTag || lConversationQuestionTag || lConversationMapTag) && (lOpenedChanged || lPresentChanged || lAccessibleChanged)) {
			CRunConversationInteractionClassic lInteraction = (CRunConversationInteractionClassic) CDesktopComponents.vView().getComponent("runConversationInteraction");
			if (lInteraction != null) {
				lInteraction.update(aTag);
			}
		}
	}

}
