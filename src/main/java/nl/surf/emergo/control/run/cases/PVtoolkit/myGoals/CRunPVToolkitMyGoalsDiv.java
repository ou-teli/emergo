/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.myGoals;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Textbox;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitInitBox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefTextbox;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTags;
import nl.surf.emergo.domain.IERunGroup;

public class CRunPVToolkitMyGoalsDiv extends CDefDiv {

	private static final long serialVersionUID = -894684677709941800L;

	public String zulfilepath = ((CRunPVToolkitInitBox)CDesktopComponents.vView().getComponent("PV-toolkit_initBox")).getZulfilepath();
	
	protected CRunPVToolkit pvToolkit;
		
	protected IERunGroup _actor;
	protected boolean _editable;
	
	protected CRunPVToolkitCacAndTags myRecordings;

	protected String _classPrefix = "myGoals";
	
	public void init(IERunGroup actor, boolean editable) {
		_actor = actor;
		_editable = editable;
		
		//NOTE always initialize, because recordings my be added during a session
		setClass(_classPrefix);

		pvToolkit = (CRunPVToolkit)CDesktopComponents.vView().getComponent("pvToolkit");
		
		update();
	}
	
	public void update() {
		getChildren().clear();
		
		pvToolkit.setMemoryCaching(true);
		
		Div div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{"titleRight"}
		);
		
		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font titleRight", "PV-toolkit-myGoals.header.myGoals"}
		);
		
		div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "Div"}
		);
		
		Rows rows = appendRecordingsGrid(div);
		int rowNumber = 1;
		rowNumber = appendRecordingsToGrid(rows, rowNumber);
		
		pvToolkit.setMemoryCaching(false);
	}

    protected Rows appendRecordingsGrid(Component parent) {
    	return pvToolkit.appendGrid(
    			parent, 
    			getId() + "Grid",
    			"position:absolute;left:0px;top:0px;width:1060px;height:540px;overflow:auto;", 
    			null,
    			new String[] {"3%", "7%", "90%"}, 
    			new boolean[] {false, true, false},
    			null,
    			"PV-toolkit-myGoals.column.label.", 
    			new String[] {"", "cycle", "goals"});
    }

	protected int appendRecordingsToGrid(Rows rows, int rowNumber) {
		return appendRecordingsToGrid(
				rows, 
				rowNumber,
				new boolean[] {true, true, true} 
				);
	}

	protected int appendRecordingsToGrid(
			Rows rows, 
			int rowNumber,
    		boolean[] showColumn 
			) {
		for (IXMLTag defineGoalsStepTag : pvToolkit.getStepTags(CRunPVToolkit.defineGoalsStepType)) {
			Row row = new Row();
			rows.appendChild(row);
				
			int columnNumber = 0;
			if (showColumn.length > columnNumber && showColumn[columnNumber]) {
				new CRunPVToolkitDefImage(row, 
						new String[]{"class", "src"}, 
						new Object[]{"gridImage", zulfilepath + "bullseye-arrow.svg"}
						);
			}
	
			columnNumber++;
			if (showColumn.length > columnNumber && showColumn[columnNumber]) {
				String cycleNumber = "";
				if (defineGoalsStepTag.getParentTag().getName().equals("cycle")) {
					cycleNumber = "" + pvToolkit.getCycleNumber(defineGoalsStepTag.getParentTag());
				}
				Label label = new CRunPVToolkitDefLabel(row, 
						new String[]{"class", "value"}, 
						new Object[]{"font gridLabel", cycleNumber}
						);
				while (cycleNumber.length() < 4) {
					cycleNumber = "0" + cycleNumber;
				}
				label.setAttribute("orderValue", cycleNumber);
			}
	
			columnNumber++;
			if (showColumn.length > columnNumber && showColumn[columnNumber]) {
				Textbox textbox = new CRunPVToolkitDefTextbox(row, 
						new String[]{"class", "rows", "width", "text"},
						new Object[]{"font gridTextbox", "5", "900px", pvToolkit.getGoals(_actor, defineGoalsStepTag)}
						);
				textbox.setDisabled(true);
			}

			rowNumber ++;
	
		}

		return rowNumber;
	}

}
