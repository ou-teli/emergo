/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.practice;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;

import nl.surf.emergo.business.IFileManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CContentFilesFilter;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitCloseImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitInitBox;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitSubStepsDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefCheckbox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefHbox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.view.VView;

public class CRunPVToolkitPracticeReadyDiv extends CDefDiv {
	private static final Logger _log = LogManager.getLogger(CRunPVToolkitPracticeReadyDiv.class);
	private static final long serialVersionUID = -5147894270835488370L;

	public String zulfilepath = ((CRunPVToolkitInitBox) CDesktopComponents.vView().getComponent("PV-toolkit_initBox"))
			.getZulfilepath();

	protected IERunGroup _actor;
	protected IXMLTag _practiceTag;

	protected String _idPrefix = "practice";
	protected String _classPrefix = "practiceReady";

	protected Media media;

	protected int mayAskFbCount;

	public void init(IERunGroup actor, IXMLTag practiceTag, String recordingName) {
		_actor = actor;
		_practiceTag = practiceTag;

		setClass("popupDiv");

		getChildren().clear();

		CRunPVToolkit pvToolkit = (CRunPVToolkit) CDesktopComponents.vView().getComponent("pvToolkit");

		new CRunPVToolkitDefImage(this, new String[] { "class" }, new Object[] { "popupBackground" });

		Div popupDiv = new CRunPVToolkitDefDiv(this, new String[] { "class" }, new Object[] { _classPrefix });

		new CRunPVToolkitDefImage(popupDiv, new String[] { "class", "src" },
				new Object[] { _classPrefix + "Background", zulfilepath + "practice-ready-background.svg" });

		CRunPVToolkitCloseImage closeImage = new CRunPVToolkitCloseImage(popupDiv, new String[] { "class", "src" },
				new Object[] { _classPrefix + "CloseImage", zulfilepath + "close.svg" });
		closeImage.init(_idPrefix + "ReadyDiv");

		CRunPVToolkitSubStepsDiv runPVToolkitSubStepsDiv = (CRunPVToolkitSubStepsDiv) CDesktopComponents.vView()
				.getComponent("practiceSubStepsDiv");
		new CRunPVToolkitDefLabel(popupDiv, new String[] { "class", "value" }, new Object[] {
				"font " + _classPrefix + "Title", (String) runPVToolkitSubStepsDiv.getAttribute("divTitle") });

		new CRunPVToolkitDefLabel(popupDiv, new String[] { "class", "value" },
				new Object[] { "font " + _classPrefix + "SubTitle", recordingName });

		CRunPVToolkitDefCheckbox checkbox = new CRunPVToolkitDefCheckbox(popupDiv,
				new String[] { "class", "value", "checked" },
				new Object[] { "font " + _classPrefix + "PeerGroupCheckboxInactive",
						pvToolkit.getStudentPeerGroupName(_actor), true });
		checkbox.setDisabled(true);
		
		mayAskFbCount = pvToolkit.mayAskTeacherFeedbackCount(_actor);
		if (mayAskFbCount > 0) {
			boolean lChecked = false;
			boolean lDisabled = false;
			int lHasAskedFbCount = pvToolkit.hasAskedTeacherFeedbackCount(_actor);
			if (lHasAskedFbCount >= mayAskFbCount) {
				lDisabled = true;
			} else {
				int lCyclesToGo = pvToolkit.getMaxCycleNumber() - pvToolkit.getCurrentCycleNumber() + 1;
				if (lCyclesToGo <= (mayAskFbCount - lHasAskedFbCount)) {
					lChecked = true;
					lDisabled = true;
				}
			}
			String labelKey = "PV-toolkit-practice.button.askteacherfeedback";
			CRunPVToolkitDefCheckbox lAskFbCheckbox = new CRunPVToolkitDefCheckbox(popupDiv,
					new String[] { "id", "class", "value", "labelKey" },
					new Object[] { _idPrefix + "AskTeacherFbButton", "font " + _classPrefix + "AskTeacherFbCheckbox", "Docentfeedback", lChecked });
			lAskFbCheckbox.setDisabled(lDisabled);
			if (lDisabled)
				lAskFbCheckbox.setChecked(lChecked);
			labelKey = "PV-toolkit-practice.label.askteacherfeedback.xtra." + lChecked + "." + lDisabled;
			new CRunPVToolkitDefLabel(popupDiv, new String[] { "class", "labelKey" },
					new Object[] { "font " + _classPrefix + "AskTeacherFbLabel", labelKey });
		}

		String labelKey = "PV-toolkit-practice.label.uploadFiles";
		new CRunPVToolkitDefLabel(popupDiv, new String[] { "class", "labelKey" },
				new Object[] { "font " + _classPrefix + "UploadFilesLabel", labelKey });

		labelKey = "PV-toolkit-practice.button.uploadFiles";
		Button btn = new CRunPVToolkitDefButton(popupDiv, new String[] { "id", "class", "labelKey" },
				new Object[] { _idPrefix + "UploadFilesButton",
						"font pvtoolkitButton " + _classPrefix + "UploadFilesButton", labelKey });
		// TODO implement possibility to upload multiple files if needed
		btn.setUpload("true,maxsize=-1,accept=native,multiple=false");
		addUploadFilesOnUploadEventListener(btn);

		Hbox hbox = new CRunPVToolkitDefHbox(popupDiv, new String[] { "class" },
				new String[] { _classPrefix + "FileHbox" });

		labelKey = "PV-toolkit-file";
		Label link = new CRunPVToolkitDefLabel(hbox, new String[] { "id", "class", "labelKey" },
				new Object[] { _idPrefix + "FileLink", "font gridLink " + _classPrefix + "FileLink", labelKey });
		link.setVisible(false);
		addFileLinkOnClickEventListener(link);

		Image image = new CRunPVToolkitDefImage(hbox, new String[] { "id", "class", "src" },
				new Object[] { _idPrefix + "DeleteFileImage", "gridImageClickable " + _classPrefix + "DeleteFileImage",
						zulfilepath + "trash.svg" });
		image.setVisible(false);
		addDeleteFileImageOnClickEventListener(image);

		Label fileName = new CRunPVToolkitDefLabel(popupDiv, new String[] { "id", "class" },
				new Object[] { _idPrefix + "FileName", "font " + _classPrefix + "FileName" });
		fileName.setVisible(false);

		Button lAFBBtn = new CRunPVToolkitDefButton(popupDiv, new String[] { "id", "class", "cLabelKey" },
				new Object[] { _idPrefix + "SaveButton", "font pvtoolkitButton " + _classPrefix + "SaveButtonInactive",
						"PV-toolkit.save" });
		lAFBBtn.setDisabled(true);
		addAskFeedbackOnClickEventListener(lAFBBtn);

		setVisible(true);

	}

	protected void addUploadFilesOnUploadEventListener(Component component) {
		component.addEventListener("onUpload", new EventListener<UploadEvent>() {
			@Override
			public void onEvent(UploadEvent event) {
				// TODO in principle multiple files could be uploaded
				media = event.getMedia();
				if (media == null || !(media instanceof Media)) {
					return;
				}
				VView vView = CDesktopComponents.vView();
				CRunPVToolkit pvToolkit = (CRunPVToolkit) CDesktopComponents.vView().getComponent("pvToolkit");
				if (pvToolkit.getCurrentRubricCode().equals("JP")) {
					if (media instanceof org.zkoss.video.Video) {
						vView.showMessagebox(getRoot(),
								pvToolkit.getRubricCLabelText("PV-toolkit-practice.wrongFile.confirmation"),
								pvToolkit.getRubricCLabelText("PV-toolkit-practice.wrongFile"), Messagebox.OK,
								Messagebox.EXCLAMATION);
						return;
					}
				} else if (pvToolkit.getCurrentRubricCode().equals("PP")) {
					String allowedFileTypes = pvToolkit.getStateValueAsString("allowed_file_types");
					if (!allowedFileTypes.equals("")) {
						String fileType = "";
						try {
							fileType = media.isBinary() ? pvToolkit.tika.detect(media.getByteData())
									: pvToolkit.tika
											.detect(IOUtils.toByteArray(media.getReaderData(), StandardCharsets.UTF_8));
						} catch (IOException e) {
							_log.error(e);
						}

						// state tag pid "allowed_file_types"
						String[] allowedFileTypesArr = allowedFileTypes.split(",");
						boolean fileTypeOk = false;
						for (int i = 0; i < allowedFileTypesArr.length; i++) {
							if (allowedFileTypesArr[i].equals(fileType)) {
								fileTypeOk = true;
								break;
							}
						}
						if (!fileTypeOk) {
							vView.showMessagebox(getRoot(),
									pvToolkit.getRubricCLabelText("PV-toolkit-practice.wrongFile.confirmation"),
									pvToolkit.getRubricCLabelText("PV-toolkit-practice.wrongFile"), Messagebox.OK,
									Messagebox.EXCLAMATION);
							return;
						}
					}
				}

				component.setVisible(false);
				CDesktopComponents.vView().getComponent(_idPrefix + "FileLink").setVisible(true);
				((Label) CDesktopComponents.vView().getComponent(_idPrefix + "FileName"))
						.setValue("(" + media.getName() + ")");
				CDesktopComponents.vView().getComponent(_idPrefix + "FileName").setVisible(true);
				CDesktopComponents.vView().getComponent(_idPrefix + "DeleteFileImage").setVisible(true);
				setSaveButtonDisabled(false);
			}
		});
	}

	protected void addFileLinkOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			@Override
			public void onEvent(Event event) {
				if (media == null || !(media instanceof Media)) {
					return;
				}
				String javascriptOnClickAction = getJavascriptOnClickAction();
				if (!StringUtils.isEmpty(javascriptOnClickAction)) {
					Clients.evalJavaScript(javascriptOnClickAction);
				}
			}
		});
	}

	protected String getJavascriptOnClickAction() {
		String fileName = media.getName();
		// add name to session variable, so file can be previewed
		CDesktopComponents.sSpring().getSBlobHelper().setBlobInSessionVar(fileName, "0");

		String url = "";
		// create file in temp folder and path to file
		String subPath = CDesktopComponents.vView().getUniqueTempSubPath();
		// path within browser
		String path = VView.getInitParameter("emergo.temp.path") + subPath;
		// path on disk
		String diskPath = CDesktopComponents.sSpring().getAppManager().getAbsoluteTempPath() + subPath;
		IFileManager fileManager = (IFileManager) CDesktopComponents.sSpring().getBean("fileManager");
		// if content within input element, it is just uploaded so create temp file to
		// show
		fileName = fileManager.createFile(diskPath, fileName,
				CDesktopComponents.sSpring().getSBlobHelper().mediaToByteArray(media));
		if (!fileName.equals("")) {
			url = path + fileName;
		}
		CContentFilesFilter.addOnlyDownloadResource(url);
		url = CDesktopComponents.sSpring().getSBlobHelper().convertHrefForCertainMediaTypes(url);
		if (!CDesktopComponents.vView().isAbsoluteUrl(url)) {
			if (!url.equals("") && url.indexOf("/") != 0) {
				url = "/" + url;
			}
			url = CDesktopComponents.vView().getEmergoWebappsRoot() + url;
		}
		// avoid javascript unterminated string error
		url = url.replace("'", "\\'");
		return CDesktopComponents.vView().getJavascriptWindowOpenFullscreen(url);
	}

	protected void addDeleteFileImageOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			@Override
			public void onEvent(Event event) {
				media = null;

				component.setVisible(false);
				CDesktopComponents.vView().getComponent(_idPrefix + "UploadFilesButton").setVisible(true);
				CDesktopComponents.vView().getComponent(_idPrefix + "FileLink").setVisible(false);
				CDesktopComponents.vView().getComponent(_idPrefix + "FileName").setVisible(false);
				setSaveButtonDisabled(true);
			}
		});
	}

	protected void setSaveButtonDisabled(boolean disabled) {
		Button saveButton = (Button) CDesktopComponents.vView().getComponent(_idPrefix + "SaveButton");
		saveButton.setDisabled(disabled);
		String classStr = "font pvtoolkitButton " + _classPrefix + "SaveButton";
		if (disabled) {
			classStr += "Inactive";
		} else {
			classStr += "Active";
		}
		saveButton.setZclass(classStr);
	}

	protected void addAskFeedbackOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			@Override
			public void onEvent(Event event) {
				// TODO in principle multiple medias could be saved
				if (media == null) {
					return;
				}
				VView vView = CDesktopComponents.vView();
				int choice = vView.showMessagebox(getRoot(), vView.getCLabel("PV-toolkit-practice.ask.confirmation"),
						vView.getCLabel("PV-toolkit-practice.ask"), Messagebox.CANCEL | Messagebox.OK,
						Messagebox.QUESTION);
				if (choice == Messagebox.OK) {
					CRunPVToolkitPracticeRecordingsDiv practiceRecordingsDiv = (CRunPVToolkitPracticeRecordingsDiv) vView
							.getComponent(_idPrefix + "RecordingsDiv");
					if (practiceRecordingsDiv != null) {
						boolean lTeacherFbAsked = false;
						if (mayAskFbCount > 0) {
							lTeacherFbAsked = ((CRunPVToolkitDefCheckbox)CDesktopComponents.vView().getComponent(_idPrefix + "AskTeacherFbButton")).isChecked();
						}
						String blobId = CDesktopComponents.sSpring().getSBlobHelper().setBlobMedia("", media);
						practiceRecordingsDiv.addPracticePiece(_practiceTag, media.getName(), blobId);
						practiceRecordingsDiv.sharePractice(_practiceTag, lTeacherFbAsked);
						// hide pop-up
						setVisible(false);
						practiceRecordingsDiv.update();
					}
				}
			}
		});
	}

}
