/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.Hashtable;

import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.business.IXMLTag;

public class CRunDragdropformItemHelper {

	public static void onInit(CRunDragdropforms aRunDragdropforms, HtmlBasedComponent aTarget, Hashtable<String,Object> item) {
		aTarget.setAttribute("item", item);
		aTarget.setAttribute("tag", item.get("tag"));
		aTarget.setAttribute("content", item.get("content"));
		//originally draggable
		aTarget.setAttribute("draggable", item.get("originallydraggable")); 
		aTarget.setDraggable((String)item.get("draggable"));
		aTarget.setDroppable((String)item.get("droppable"));
		aTarget.setVisible(item.get("present").equals("true"));
		aTarget.setAttribute("orient", aTarget.getParent().getAttribute("orient"));
		boolean isDocument = ((IXMLTag)aTarget.getAttribute("tag")).getName().equals("document");
		aTarget.setAttribute("isDocument", isDocument);
		if (isDocument && aRunDragdropforms != null) {
			aRunDragdropforms.setOnClickWidgetListenerForBlobChild(aTarget, (IXMLTag)aTarget.getAttribute("tag"));
		}
	}

	public static void onClick(CRunDragdropforms aRunDragdropforms, HtmlBasedComponent aTarget) {
		if ((Boolean)aTarget.getAttribute("isDocument") && aRunDragdropforms != null) {
			aRunDragdropforms.observedNotify(null, "pieceClicked", (IXMLTag)aTarget.getAttribute("tag"));
		}
		//alternative for drag and drop
		aRunDragdropforms.ignoreOnClick = true;
		if (aRunDragdropforms.draggedComponent != null) {
			((HtmlBasedComponent)aRunDragdropforms.draggedComponent).setStyle("");
			if (aTarget.getDroppable().equals("true")) {
				aRunDragdropforms.dropItem(aRunDragdropforms.draggedComponent, aTarget);
			}
			aRunDragdropforms.draggedComponent = null;
		}
		else if (aTarget.getDraggable().equals("true")) {
			aTarget.setStyle("border: 3px solid #0000CC;");
			aRunDragdropforms.draggedComponent = aTarget;
		}
	}

	public static void onDisable(HtmlBasedComponent aTarget) {
		if (aTarget.getId() != null && !aTarget.getId().equals("")) {
			aTarget.setAttribute("originalId", aTarget.getId());
			aTarget.setId("");
		}
		aTarget.setDraggable("false");
		Events.postEvent("onUpdate", aTarget, null);
	}

	public static void onEnable(HtmlBasedComponent aTarget) {
		if (aTarget.getAttribute("originalId") != null && !aTarget.getAttribute("originalId").equals("")) {
			aTarget.setId((String)aTarget.getAttribute("originalId"));
			aTarget.setAttribute("originalId", "");
		}
		aTarget.setDraggable((String)aTarget.getAttribute("draggable"));
		Events.postEvent("onUpdate", aTarget, null);
	}

	public static void onDrop(CRunDragdropforms aRunDragdropforms, HtmlBasedComponent aDragged, HtmlBasedComponent aDropped) {
		//NOTE regular onDrop so clear possible selected component to drag by clicking on it
		aRunDragdropforms.draggedComponent = null;
		aRunDragdropforms.dropItem(aDragged, aDropped);
	}


}
