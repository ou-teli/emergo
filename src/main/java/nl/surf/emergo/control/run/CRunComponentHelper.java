/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.gmaps.Gmarker;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.Treerow;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.zkspring.SSpring;

/**
 * The Class CRunComponentHelper.
 *
 * Used to overwrite some author methods by player methods, because for instance other layout.
 * But also adds specific player methods.
 */
public class CRunComponentHelper extends CRunContentHelper {

	/**
	 * Instantiates a new c run component helper.
	 *
	 * @param aContentComponent the a contentcomponent, the ZK component actually showing the content, tree or gmaps
	 * @param aShowTagNames the a show tag names, names of tags that should be shown as content
	 * @param aCaseComponent the a case component
	 * @param aRunComponent the a run component, the ZK component showing content title, content and buttons
	 */
	public CRunComponentHelper(Component aContentComponent, String aShowTagNames, IECaseComponent aCaseComponent, CRunComponent aRunComponent) {
		super(aContentComponent, aShowTagNames, aCaseComponent, aRunComponent);
	}

	/**
	 * Instantiates a new c run component helper.
	 *
	 * @param aContentComponent the a contentcomponent, the ZK component actually showing the content, tree or gmaps
	 * @param aShowTagNames the a show tag names, names of tags that should be shown as content
	 * @param aCaseComponent the a case component
	 * @param aRunComponent the a run component, the ZK component showing content title, content and buttons
	 * @param sSpring the a sspring
	 */
	public CRunComponentHelper(Component aContentComponent, String aShowTagNames, IECaseComponent aCaseComponent, CRunComponent aRunComponent, SSpring sSpring) {
		super(aContentComponent, aShowTagNames, aCaseComponent, aRunComponent, sSpring);
	}

	/**
	 * Normally within the player there is no author, so false is returned.
	 *
	 * @param aObject a ZK component on the case component player page
	 *
	 * @return true, if successful
	 */
	@Override
	public boolean accIsAuthor(Component aObject) {
		return false;
	}

	/**
	 * Normally within the player there is no context author menu, so it is not set.
	 *
	 * @param aObject a ZK component on the case component player page
	 */
	@Override
	public void setContextMenu(Component aObject) {
	}

	/**
	 * Calls same method of super.
	 * And if outfolded status is false, sets open of the tree item to false.
	 *
	 * @param aParent the parent
	 * @param aInsertBefore the content item to insert before, if null it will be appended
	 * @param aItem the xml tag
	 *
	 * @return the new treeitem
	 */
	@Override
	public Treeitem renderTreeitem(Component aParent, Component aInsertBefore, IXMLTag aItem) {
		Treeitem lTreeitem = super.renderTreeitem(aParent, aInsertBefore, aItem);
		lTreeitem = outfoldTreeitem(lTreeitem, aItem);
		return lTreeitem;
	}

	/**
	 * Calls same method of super.
	 * And if outfolded status is false, sets open of the tree item to false.
	 *
	 * @param aComponent the component
	 * @param aItem the xml tag
	 *
	 * @return the treeitem
	 */
	@Override
	public Treeitem reRenderTreeitem(Component aComponent, IXMLTag aItem) {
		//NOTE if present is set to false in player, hide tree item
		//Present cannot be set true again
		Treeitem lTreeitem = (Treeitem)aComponent;
		if (!isPresent(aItem) || isDeleted(aItem)) {
			lTreeitem.setVisible(false);
		}
		else {
			lTreeitem = super.reRenderTreeitem(aComponent, aItem);
			lTreeitem = outfoldTreeitem(lTreeitem, aItem);
		}
		return lTreeitem;
	}

	/**
	 * Rerenders gmap item using xml tag aItem.
	 *
	 * @param aParent the parent
	 * @param aComponent the component
	 * @param aItem the xml tag
	 *
	 * @return the new gmarker
	 */
	@Override
	public Component reRenderGmapsItem(Component aParent, Component aComponent, IXMLTag aItem) {
		//NOTE if present is set to false in player, hide tree item
		//Present cannot be set true again
		Gmarker lMarker = (Gmarker)aComponent;
		if (aItem.getCurrentStatusAttribute(AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse)) {
			lMarker.setVisible(false);
		}
		else {
			lMarker = (Gmarker)super.reRenderGmapsItem(aParent, aComponent, aItem);
		}
		return lMarker;
	}

	protected Treeitem outfoldTreeitem(Treeitem aTreeitem, IXMLTag aItem) {
		boolean lOutfolded = ((CDesktopComponents.sSpring().getCurrentTagStatus(aItem, AppConstants.statusKeyOutfolded)).equals(AppConstants.statusValueTrue));
		lOutfolded = (lOutfolded) || (aItem.getName().equals(AppConstants.contentElement));
		if (!lOutfolded)
			aTreeitem.setOpen(false);
		return aTreeitem;
	}

	/**
	 * Renders treecell using xml tag aItem.
	 *
	 * @param aTreeitem the treeitem
	 * @param aTreerow the treerow parent
	 * @param aItem the xml tag
	 * @param aKeyValues the key values
	 * @param aAccIsAuthor the account is author state
	 *
	 * @return the new treecell
	 */
	@Override
	protected Treecell renderTreecell(Treeitem aTreeitem, Treerow aTreerow, IXMLTag aItem, String aKeyValues, boolean aAccIsAuthor) {
		Treecell lTreecell = getTreerowTreecell(aTreerow, 0);
		lTreecell.setLabel(aKeyValues);
		showStatus(lTreecell, aItem);
		return lTreecell;
	}

	/**
	 * Renders info treecell, but within the player this cell is not used, so it renders nothing.
	 *
	 * @param aTreeitem the treeitem
	 * @param aTreerow the treerow parent
	 * @param aItem the xml tag
	 * @param aKeyValues the key values
	 * @param aAccIsAuthor the account is author state
	 *
	 * @return the new info treecell
	 */
	@Override
	protected Treecell renderInfoTreecell(Treeitem aTreeitem, Treerow aTreerow, IXMLTag aItem, String aKeyValues, boolean aAccIsAuthor) {
		return null;
	}

	/**
	 * Gets the current status value for a xml tag.
	 *
	 * @param aItem the a item
	 * @param aStatusKey the a status key
	 *
	 * @return the status value
	 */
	public String getStatusValue(IXMLTag aItem, String aStatusKey) {
		return CDesktopComponents.sSpring().getCurrentTagStatus(aItem, aStatusKey);
	}

	/**
	 * Gets the current status time for a xml tag.
	 *
	 * @param aItem the a item
	 * @param aStatusKey the a status key
	 *
	 * @return the status time
	 */
	public String getStatusTime(IXMLTag aItem, String aStatusKey) {
		if (aItem == null) {
			return null;
		}
		return "" + aItem.getCurrentStatusAttributeTime(aStatusKey);
	}

	/**
	 * Checks if current status value is equal to aStatusValue.
	 *
	 * @param aItem the a item
	 * @param aStatusKey the a status key
	 * @param aStatusValue the a status value
	 * @param aIsShowTagName if true then tagname of aItem must be one of the showtagnames for this component
	 *
	 * @return true, if is status value equal to
	 */
	private boolean isStatusValueEqualTo(IXMLTag aItem, String aStatusKey, String aStatusValue, Boolean aIsShowTagName) {
		boolean lEqual = (CDesktopComponents.sSpring().getCurrentTagStatus(aItem, aStatusKey)
				.equals(aStatusValue));
		boolean lNameInTagNames = true;
		if (aIsShowTagName) {
			String lTagNames = "," + showtagnames + ",";
			lNameInTagNames = (lTagNames.indexOf("," + aItem.getName() + ",") >= 0);
		}
		return ((lNameInTagNames) && (lEqual));
	}

	/**
	 * Checks if current status value is equal to aStatusValue.
	 *
	 * @param aItem the a item
	 * @param aStatusKey the a status key
	 * @param aStatusValue the a status value
	 *
	 * @return true, if is status value equal to
	 */
	public boolean isStatusValueEqualTo(IXMLTag aItem, String aStatusKey, String aStatusValue) {
		return isStatusValueEqualTo(aItem, aStatusKey, aStatusValue, true);
	}

	/**
	 * Checks if opened of current status of xmltag aItem is true.
	 *
	 * @param aItem the a item
	 *
	 * @return true, if is opened
	 */
	public boolean isOpened(IXMLTag aItem) {
		return isStatusValueEqualTo(aItem, AppConstants.statusKeyOpened,
				AppConstants.statusValueTrue);
	}

	/**
	 * Checks if finished of current status of xmltag aItem is true.
	 *
	 * @param aItem the a item
	 *
	 * @return true, if is finished
	 */
	public boolean isFinished(IXMLTag aItem) {
		return isStatusValueEqualTo(aItem, AppConstants.statusKeyFinished,
				AppConstants.statusValueTrue);
	}

	/**
	 * Checks if required of current status of xmltag aItem is true.
	 *
	 * @param aItem the a item
	 *
	 * @return true, if is required
	 */
	public boolean isRequired(IXMLTag aItem) {
		return isStatusValueEqualTo(aItem, AppConstants.statusKeyRequired,
				AppConstants.statusValueTrue);
	}

	/**
	 * Checks if accessible of current status of xmltag aItem is true.
	 *
	 * @param aItem the a item
	 *
	 * @return true, if is accessible
	 */
	public boolean isAccessible(IXMLTag aItem) {
		return (!isStatusValueEqualTo(aItem, AppConstants.statusKeyAccessible,
				AppConstants.statusValueFalse));
	}

	/**
	 * Checks if canceled of current status of xmltag aItem is true.
	 *
	 * @param aItem the a item
	 *
	 * @return true, if is canceled
	 */
	public boolean isCanceled(IXMLTag aItem) {
		return isStatusValueEqualTo(aItem, AppConstants.statusKeyCanceled,
				AppConstants.statusValueTrue);
	}

	/**
	 * Checks if present of current status of xmltag aItem is true.
	 *
	 * @param aItem the a item
	 *
	 * @return true, if is present
	 */
	public boolean isPresent(IXMLTag aItem) {
		return isStatusValueEqualTo(aItem, AppConstants.statusKeyPresent, AppConstants.statusValueTrue, false);
	}

	/**
	 * Checks if deleted of current status of xmltag aItem is true.
	 *
	 * @param aItem the a item
	 *
	 * @return true, if is deleted
	 */
	public boolean isDeleted(IXMLTag aItem) {
		return isStatusValueEqualTo(aItem, AppConstants.statusKeyDeleted, AppConstants.statusValueTrue, false);
	}

	/**
	 * Gets the xml data plus status tree, so data enriched with status.
	 *
	 * @return the xml tree plus status
	 */
	public IXMLTag getXmlDataPlusStatusTree() {
		return CDesktopComponents.sSpring().getXmlDataPlusRunStatusTree(getCacId(), getRunStatusType());
	}

	/**
	 * Converts xml child tags to content items. Nested.
	 * Content items are shown depending on their current status.
	 *
	 * @param aChildTags the xml child tags
	 * @param aParent the ZK parent component
	 * @param aInsertBefore the content item to insert before, if null it will be appended
	 *
	 * @return the new content items
	 */
	@Override
	public List<Component> xmlChildsToContentItems(List<IXMLTag> aChildTags, Component aParent, Component aInsertBefore) {
		if ((aChildTags == null) || (aChildTags.size() == 0))
			return null;
		List<Component> lContentItems = new ArrayList<Component>();
		for (IXMLTag xmltag : aChildTags) {
			boolean lHidden = ((CDesktopComponents.sSpring().getCurrentTagStatus(xmltag,
						AppConstants.statusKeyPresent))
						.equals(AppConstants.statusValueFalse));
			boolean lDeleted = ((CDesktopComponents.sSpring().getCurrentTagStatus(xmltag,
						AppConstants.statusKeyDeleted))
						.equals(AppConstants.statusValueTrue));
			if (!(lHidden || lDeleted)) {
				Component lContentItem = null;
				if (currentCaseComponentCode.equals("googlemaps")) {
					lContentItem = renderGmapsItem(aParent, aInsertBefore, xmltag);
				}
				else {
					lContentItem = renderTreeitem(aParent, null, xmltag);
				}
				if (lContentItem != null) {
					lContentItems.add(lContentItem);
					boolean lOpened = ((CDesktopComponents.sSpring().getCurrentTagStatus(xmltag,
							AppConstants.statusKeyOutfolded))
							.equals(AppConstants.statusValueTrue));
					lOpened = (lOpened) || (xmltag.getName().equals(AppConstants.contentElement));
					if (!lOpened) {
						if (lContentItem instanceof Treeitem)
							((Treeitem)lContentItem).setOpen(false);
					}
					boolean lChildrenHidden = ((CDesktopComponents.sSpring().getCurrentTagStatus(
							xmltag, AppConstants.statusKeyOutfoldable))
							.equals(AppConstants.statusValueFalse));
					if (!lChildrenHidden)
						xmlChildsToContentItems(xmltag.getChildTags(AppConstants.defValueNode),
								lContentItem, null);
				}
			}
		}
		return lContentItems;
	}
	
	/**
	 * Returns visible xml node tags. Nested.
	 *
	 * @param aTag the xml node tag
	 *
	 * @return the visible xml node tags
	 */
	public List<IXMLTag> getVisibleXmlNodeTags(IXMLTag aNodeTag) {
		if (aNodeTag == null) {
			return new ArrayList<IXMLTag>();
		}
		List<IXMLTag> lTags = new ArrayList<IXMLTag>();
		for (IXMLTag lTag : aNodeTag.getChildTags()) {
			if (lTag.getAttribute(AppConstants.defKeyType).equals(AppConstants.defValueNode)) {
				boolean lHidden = CDesktopComponents.sSpring().getCurrentTagStatus(lTag,
						AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse);
				boolean lDeleted = CDesktopComponents.sSpring().getCurrentTagStatus(lTag,
						AppConstants.statusKeyDeleted).equals(AppConstants.statusValueTrue);
				if (!(lHidden || lDeleted)) {
					IXMLTag lClonedNodeTag = lTag.cloneWithoutParentAndChildren();
					lClonedNodeTag.setParentTag(aNodeTag);
					lTags.add(lClonedNodeTag);
					boolean lChildrenHidden = ((CDesktopComponents.sSpring().getCurrentTagStatus(
							lTag, AppConstants.statusKeyOutfoldable)).equals(AppConstants.statusValueFalse));
					if (!lChildrenHidden) {
						lClonedNodeTag.setChildTags(getVisibleXmlNodeTags(lTag));
					}
				}
			}
			else {
				IXMLTag lClonedTag = lTag.cloneWithoutParentAndChildren();
				lClonedTag.setParentTag(aNodeTag);
				lTags.add(lClonedTag);
			}
		}
		return lTags;
	}
	
}
