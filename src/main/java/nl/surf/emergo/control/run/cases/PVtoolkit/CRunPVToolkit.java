/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.tika.Tika;
import org.doomdark.uuid.UUIDGenerator;
import org.zkoss.lang.Strings;
import org.zkoss.util.media.Media;
import org.zkoss.video.Video;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.CheckEvent;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkmax.zul.Camera;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Column;
import org.zkoss.zul.Columns;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listhead;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;

import nl.surf.emergo.business.IBlobManager;
import nl.surf.emergo.business.IRunGroupAccountManager;
import nl.surf.emergo.business.IRunGroupCaseComponentManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CCaseHelper;
import nl.surf.emergo.control.CContentFilesFilter;
import nl.surf.emergo.control.CContentHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefColumn;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.def.CDefGrid;
import nl.surf.emergo.control.def.CDefHelper;
import nl.surf.emergo.control.def.CDefListbox;
import nl.surf.emergo.control.def.CDefListhead;
import nl.surf.emergo.control.def.CDefListheader;
import nl.surf.emergo.control.def.CDefTimer;
import nl.surf.emergo.control.run.CRunCaseHelper;
import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.control.run.cases.PVtoolkit.dashboard.CRunPVToolkitDashboardMessagesDetailsDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.dashboard.CRunPVToolkitDashboardMessagesDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.dashboard.CRunPVToolkitDashboardPeerGroupsDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefCheckbox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefHbox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.cases.PVtoolkit.definegoals.CRunPVToolkitDefineGoalsDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.feedback.CRunPVToolkitFeedbackRecordingsDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.feedback.CRunPVToolkitFeedbackSubStepsDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.method.CRunPVToolkitMethodMethodDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTag;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTags;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitFeedbackPractice;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitFeedbackTipOrTop;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitMessage;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitNotification;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitPeerGroup;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitPeerGroupMember;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitPeerGroupMemberProgress;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitStepType;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitSubStepType;
import nl.surf.emergo.control.run.cases.PVtoolkit.myFeedback.CRunPVToolkitMyFeedbackRecordingsDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.myGoals.CRunPVToolkitMyGoalsDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.myRecordings.CRunPVToolkitMyRecordingsRecordingsDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.rubric.CRunPVToolkitRubricRubricDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.superTeacher.CRunPVToolkitSuperTeacherAuthoringEnvironmentDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.teacherDashboard.CRunPVToolkitTeacherDashboardPeerGroupsDetailsDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.teacherDashboard.CRunPVToolkitTeacherDashboardStudentsDetailsDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.util.CRunPVToolkitListitemComparator;
import nl.surf.emergo.control.run.cases.PVtoolkit.util.CRunPVToolkitRowComparator;
import nl.surf.emergo.control.run.cases.PVtoolkit.util.CRunPVToolkitJsonHelper;
import nl.surf.emergo.domain.IEBlob;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.domain.IERunGroupCaseComponent;
import nl.surf.emergo.utilities.AntiSamyHelper;
import nl.surf.emergo.utilities.FileHelper;
import nl.surf.emergo.utilities.PropsValues;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpring;
import nl.surf.emergo.zkspring.STriggeredReference;

/**
 * The PV-tool uses a number of panels that represent different functionalities.
 * One of these panels is a main menu that enables a user to switch between
 * panels. However, a user can not switch between the panels that represent the
 * different step types. These panels are set by the PV-tool according to the
 * progress of a user. If present of case component 'PV_demoscript' is set to
 * true, menu options are added to allow a user to switch between different
 * steps (e.g., for demonstrations).
 */
public class CRunPVToolkit extends CRunComponent {

	private static final long serialVersionUID = -3708858366630712147L;

	protected VView vView = CDesktopComponents.vView();
	protected SSpring sSpring = CDesktopComponents.sSpring();
	protected CCaseHelper cCaseHelper = new CRunCaseHelper();
	protected IRunGroupAccountManager runGroupAccountManager = (IRunGroupAccountManager) sSpring.getBean("runGroupAccountManager");
	protected IRunGroupCaseComponentManager runGroupCaseComponentManager = (IRunGroupCaseComponentManager) sSpring.getBean("runGroupCaseComponentManager");

	/**
	 * States case component that contains states to be able to set the initial
	 * layout of the PV-tool, e.g., presence of main menu or step title, or start
	 * panel
	 */
	protected IECaseComponent currentStatesCaseComponent;
	/**
	 * States case component that contains states to keep track of the current state
	 * of the PV-tool, e.g., current rubric, method, cycle or step, or panel
	 */
	protected IECaseComponent currentStatesCurrentCaseComponent;

	/**
	 * Current rubrics case component properties. There may be multiple rubrics case
	 * components with each having multiple rubric tags.
	 */
	protected IECaseComponent currentRubricsCaseComponent;
	protected IXMLTag currentRubricTag;
	protected String currentRubricCode = "";
	protected IXMLTag currentSkillTag;

	/**
	 * Current projects case component properties. There may be multiple projects
	 * case components with each having multiple project tags.
	 */
	protected IECaseComponent currentProjectsCaseComponent;
	protected IXMLTag currentProjectTag;
	/** A project tag may have multiple method tags */
	protected IXMLTag currentMethodTag;
	/** A method tag may have multiple cycle tags or none */
	protected IXMLTag currentCycleTag;
	/**
	 * Step tags within current method (if no cycle tags) or within current cycle
	 */
	protected List<IXMLTag> currentStepTags = new ArrayList<IXMLTag>();
	/**
	 * Current step tag, number and step title for user. Number and title are used
	 * in the interface. Number indicates a number within the method, so if the
	 * method has cycle tags, it may become larger than 5.
	 */
	protected IXMLTag currentStepTag;
	protected int currentStepNumber;
	protected String currentStep;

	/**
	 * Current messages case component. There may be multiple messages case
	 * components with each having multiple message tags. Messages are used to
	 * notify users if certain events happen, e.g., a peer feedback is given.
	 * Messages are sent both by e-mail and within the PV-tool.
	 */
	protected IECaseComponent currentMessagesCaseComponent;

	/** The current panel being shown to the user */
	protected String currentPanel;

	/**
	 * If the user chooses the menu option 'current activity' for the first time,
	 * the current step panel has to be rendered. Later on, the panel will be kept
	 * in memory.
	 */
	protected boolean currentActivityIsRendered = false;

	/**
	 * The path to the ZUL file of the current macro. It is used to be able to
	 * reference to other files like assets or style sheets with regard to the path
	 * to the ZUL file.
	 */
	public String zulfilepath = ((CRunPVToolkitInitBox) vView.getComponent("PV-toolkit_initBox")).getZulfilepath();

	/* START RW GAME SPECIFIC CODE */
	/**
	 * Get presence of main menu or step title. E.g., within game both are not
	 * present
	 */
	public boolean has_main_menu = ((CRunPVToolkitInitBox) vView.getComponent("PV-toolkit_initBox")).getHasMainMenu();
	public boolean has_step_title = ((CRunPVToolkitInitBox) vView.getComponent("PV-toolkit_initBox")).getHasStepTitle();
	/* END RW GAME SPECIFIC CODE */

	/**
	 * The PV tool case component names.
	 */
	public static final String gCaseComponentNameRubrics = "PV_rubrics";
	public static final String gCaseComponentNameProjects = "PV_projects";
	public static final String gCaseComponentNameMessages = "PV_messages";
	public static final String gCaseComponentNameStates = "PV_states";
	public static final String gCaseComponentNameStatesInitial = "PV_states_initial";
	public static final String gCaseComponentNameStatesCurrent = "PV_states_current";

	/* START EXPERIMENTAL CODE */
	public static final String gCaseComponentNameStatesExperiment = "PV_states_experiment";
	public static final String gExpIdStateKey = "str_expid";
	public static final String gExpIdSetStateKey = "str_expid_set";
	public static final String gExpAssBaseStateKey = "str_experiment_assessments_";
	public static final String gExpAssBaseCompNameKey = "PV_assessment_cycle";
	public static final String gExpAssNamesNotInStates = "@@@NOT_PRESENT_IN_STATES@@@";

	public static final String gExperimentVariantR1R3 = "A";
	public static final String gExperimentVariantR2R4 = "B";
	public static final String gExperimentVariantTPS = "T";
	public static final String gExperimentVariantPS = "P";
	public static final String gExperimentVariantTFBU = "U";
	public static final String gExperimentVariantTFBD = "D";
	public static final String gExperimentVariantTFBR1 = "F";
	public static final String gExperimentVariantTFBR2 = "S";
	/* END EXPERIMENTAL CODE */

	/**
	 * The practice feedback rating sub elements ids.
	 */
	public static final String _feedbackRatingKeyStr = "feedbackrating";
	public static final String _feedbackRatingLevelStr = "level";
	public static final String _feedbackRatingRgaIdStr = "feedbackrgaid";

	/**
	 * The five step types. Each step type has its own interface and possible sub
	 * steps.
	 */
	public static final String prepareStepType = "preparestep";
	public static final String practiceStepType = "practicestep";
	public static final String feedbackStepType = "feedbackstep";
	public static final String viewFeedbackStepType = "viewfeedbackstep";
	public static final String defineGoalsStepType = "definegoalsstep";

	/** The sub step types. */
	public static final String prepareStudyRubricSubStepType = "studyrubricsubstep";
	public static final String prepareAnalyzeVideoSubStepType = "analyzevideosubstep";
	public static final String prepareCompareWithExpertsSubStepType = "comparewithexpertssubstep";
	public static final String practicePracticeSubStepType = "practicesubstep";
	public static final String practiceSelfFeedbackSubStepType = "selffeedbacksubstep";
	public static final String practiceAskFeedbackSubStepType = "askfeedbacksubstep";
	public static final String feedbackGiveFeedbackSubStepType = "givefeedbacksubstep";
	public static final String feedbackSendFeedbackSubStepType = "sendfeedbacksubstep";
	public static final String viewFeedbackViewFeedbackSubStepType = "viewfeedbacksubstep";
	public static final String defineGoalsSetGoalsSubStepType = "setgoalssubstep";

	/**
	 * CRunPVToolkitStepType properties: String stepType; String
	 * mandatoryPreviousStepType; boolean mandatoryForStudent; boolean
	 * mandatoryForTeacher; boolean mandatoryForPeerStudent; boolean
	 * defaultPresentForStudent; boolean defaultPresentForTeacher; boolean
	 * defaultPresentForPeerStudent;
	 * 
	 * 'Mandatory previous step type' means that if the previous step type is turned
	 * off the step type must also be turned off. This has nothing to do with
	 * conditions yet. 'Mandatory previous step type' is role independent. NOTE that
	 * feedback step type is not mandatory for teachers and peer students because
	 * they may not give feedback in every cycle.
	 */
	public static final CRunPVToolkitStepType[] stepTypes = new CRunPVToolkitStepType[] {
			new CRunPVToolkitStepType(CRunPVToolkit.prepareStepType, "", false, false, false, true, false, false),
			new CRunPVToolkitStepType(CRunPVToolkit.practiceStepType, "", true, false, false, true, false, false),
			new CRunPVToolkitStepType(CRunPVToolkit.feedbackStepType, "", true, false, false, true, true, true),
			new CRunPVToolkitStepType(CRunPVToolkit.viewFeedbackStepType, CRunPVToolkit.practiceStepType, true, false,
					false, true, false, false),
			new CRunPVToolkitStepType(CRunPVToolkit.defineGoalsStepType, CRunPVToolkit.viewFeedbackStepType, false,
					false, false, true, false, false) };

	/**
	 * CRunPVToolkitSubStepType properties: String subStepType; String
	 * mandatoryPreviousSubStepType; boolean mandatoryForStudent; boolean
	 * mandatoryForTeacher; boolean mandatoryForPeerStudent; boolean
	 * defaultPresentForStudent; boolean defaultPresentForTeacher; boolean
	 * defaultPresentForPeerStudent;
	 * 
	 * 'Mandatory previous sub step type' means that if the previous sub step type
	 * is turned off the sub step type must also be turned off. This has nothing to
	 * do with conditions yet. 'Mandatory previous sub step type' is role
	 * independent. NOTE that mandatory for sub step types only applies if the
	 * parent step type is present. For instance, if the practice step type is
	 * present, the practice and ask feedback sub step types are mandatory, but the
	 * self feedback sub step type is optional.
	 */
	public static final CRunPVToolkitSubStepType[][] subStepTypes = new CRunPVToolkitSubStepType[][] {
			{ new CRunPVToolkitSubStepType(CRunPVToolkit.prepareStudyRubricSubStepType, "", false, false, false, true,
					false, false),
					new CRunPVToolkitSubStepType(CRunPVToolkit.prepareAnalyzeVideoSubStepType, "", false, false, false,
							true, false, false),
					new CRunPVToolkitSubStepType(CRunPVToolkit.prepareCompareWithExpertsSubStepType,
							CRunPVToolkit.prepareAnalyzeVideoSubStepType, false, false, false, true, false, false) },
			{ new CRunPVToolkitSubStepType(CRunPVToolkit.practicePracticeSubStepType, "", true, true, true, true, false,
					false),
					new CRunPVToolkitSubStepType(CRunPVToolkit.practiceSelfFeedbackSubStepType,
							CRunPVToolkit.practicePracticeSubStepType, false, false, false, true, false, false),
					new CRunPVToolkitSubStepType(CRunPVToolkit.practiceAskFeedbackSubStepType,
							CRunPVToolkit.practicePracticeSubStepType, true, true, true, true, false, false) },
			{ new CRunPVToolkitSubStepType(CRunPVToolkit.feedbackGiveFeedbackSubStepType, "", true, true, true, true,
					true, true),
					new CRunPVToolkitSubStepType(CRunPVToolkit.feedbackSendFeedbackSubStepType,
							CRunPVToolkit.feedbackGiveFeedbackSubStepType, true, true, true, true, true, true) },
			{ new CRunPVToolkitSubStepType(CRunPVToolkit.viewFeedbackViewFeedbackSubStepType, "", true, true, true,
					true, false, false) },
			{ new CRunPVToolkitSubStepType(CRunPVToolkit.defineGoalsSetGoalsSubStepType, "", true, true, true, true,
					false, false) } };

	/**
	 * The two peer group scope types, meaning the level on which the peer group is
	 * defined, either method or cycle.
	 */
	public static final boolean defaultMayscoreskill = true;
	public static final boolean defaultMaytipsandtopsskill = false;
	public static final String defaultFeedbacklevelofscores = "subskill";
	public static final String defaultFeedbackleveloftipsandtops = "skillcluster";

	/**
	 * The two peer group scope types, meaning the level on which the peer group is
	 * defined, either method or cycle.
	 */
	public static final String peerGroupMethodScope = "method";
	public static final String peerGroupCycleScope = "cycle";

	/** For storing all active rungroups. */
	protected List<IERunGroup> _activeRunGroups;

	/** For storing all active rungroupaccounts. */
	protected List<IERunGroupAccount> _activeRunGroupAccounts;

	/** For storing all current rungroupaccounts. */
	protected Map<Integer, IERunGroupAccount> _runGroupAccounts = new HashMap<Integer, IERunGroupAccount>();

	/* START EXPERIMENTAL CODE */
	/** For storing rungroupaccount experiment ids. */
	protected Map<Integer, String> _runGroupAccountsExpIds = new HashMap<Integer, String>();
	/* END EXPERIMENTAL CODE */

	/** For storing all peer group tags. */
	protected CRunPVToolkitCacAndTags _peerGroupTags;

	/** For storing all peer groups. */
	protected List<CRunPVToolkitPeerGroup> _peerGroups;

	/**
	 * The five user roles within a peer group defined so far. Student will walk
	 * through all steps within a method. Teacher will only walk through the
	 * feedback steps in the method, giving feedback to students. Teachers walk
	 * together with students, so are obliged to give feedback right away. Student
	 * assistant, will just like a teacher walk through the feedback steps. They,
	 * however, don't have to give feedback right away, and students see their
	 * feedback later on. This role was added for research purposes. Probably it
	 * should be better called 'teacher delayed feedback'. Passive teacher is meant
	 * for teachers that won't give feedback, but must be able to see feedback given
	 * by others. Peer student is meant for students that will only give feedback,
	 * so will not practice a skill, e.g., for senior students.
	 */
	public static final String peerGroupStudentRole = "student";
	public static final String peerGroupTeacherRole = "teacher";
	/* START EXPERIMENTAL CODE */
	public static final String peerGroupStudentAssistantRole = "studentassistant";
	/* END EXPERIMENTAL CODE */
	public static final String peerGroupPassiveTeacherRole = "passiveteacher";
	public static final String peerGroupPeerStudentRole = "peerstudent";

	/**
	 * Are set for a user when PV-tool is opened. Are needed to personalize the
	 * PV-tool.
	 */
	public boolean hasStudentRole;
	public boolean hasTeacherRole;
	/* START EXPERIMENTAL CODE */
	public boolean hasStudentAssistantRole;
	/* END EXPERIMENTAL CODE */
	public boolean hasPassiveTeacherRole;
	public boolean hasPeerStudentRole;

	/**
	 * Are set for a user when PV-tool is opened. Are needed to personalize the
	 * PV-tool.
	 */
	public boolean hasPeersWithStudentRole;
	public boolean hasPeersWithTeacherRole;
	/* START EXPERIMENTAL CODE */
	public boolean hasPeersWithStudentAssistantRole;
	/* END EXPERIMENTAL CODE */
	public boolean hasPeersWithPassiveTeacherRole;
	public boolean hasPeersWithPeerStudentRole;

	/* START EXPERIMENTAL CODE */
	public boolean participatesInVariantR1R3;
	public boolean participatesInVariantR2R4;
	public String hasExperimentId;
	/* END EXPERIMENTAL CODE */

	/**
	 * Are set for a user when PV-tool is opened. Are needed to personalize the
	 * PV-tool.
	 */
	public boolean isStudent;
	public boolean isTeacher;
	public boolean isFeedbackGiver;

	protected List<String> gInitializedPanels = new ArrayList<String>();

	/**
	 * The ZUL files for the different panels used within the PV-tool. A number of
	 * panels is equal for students and teachers, e.g., notifications, rubric,
	 * method, my-feedback and feedback. Other panels differ or are specifically
	 * meant for teachers. The super-teacher panel only is available when a user has
	 * been given a super teacher role as well, by setting a corresponding state in
	 * PV_states_initial to true.
	 */
	protected static final String[] macroZulFiles = new String[] { "PV-toolkit-main-menu.zul",
			"PV-toolkit-dashboard.zul", "PV-toolkit-teacher-dashboard.zul", "PV-toolkit-progress.zul",
			"PV-toolkit-teacher-groups-progress.zul", "PV-toolkit-teacher-students-progress.zul",
			"PV-toolkit-notifications.zul", "PV-toolkit-rubric.zul", "PV-toolkit-method.zul", "PV-toolkit-my-goals.zul",
			"PV-toolkit-my-feedback.zul", "PV-toolkit-my-recordings.zul", "PV-toolkit-super-teacher.zul",
			"PV-toolkit-prepare.zul", "PV-toolkit-practice.zul", "PV-toolkit-feedback.zul",
			"PV-toolkit-view-feedback.zul", "PV-toolkit-define-goals.zul" };

	/**
	 * Every panel contains a div that has an id that starts with this prefix. The
	 * rest of the id indicates the function of the panel, e.g., 'feedback' for the
	 * feedback step panel or 'my-recordings' for the my-recordings panel.
	 */
	protected static final String panelIdPrefix = "panel_";

	/**
	 * All PV-tool labels start with this prefix. The labels are located in the
	 * 'PV-toolkit.properties' file within folder 'WEB-INF'.
	 */
	public static final String generalLabelKeyPrefix = "PV-toolkit.";

	/**
	 * Constants used for the skill wheel. Achievement, improvement and
	 * deterioration are used to show differences in scores between different
	 * cycles. Both, first and second are used when comparing scores of different
	 * user groups (first and second group).
	 */
	public static final String performanceLevelAchievedState = "achieved";
	public static final String performanceLevelImprovedState = "improved";
	public static final String performanceLevelDeterioratedState = "deteriorated";
	public static final String performanceLevelCompareBothState = "equal";
	public static final String performanceLevelCompareFirstState = "first";
	public static final String performanceLevelCompareSecondState = "second";
	public static final String performanceLevelNoScoreState = "noScore";

	/** Skill wheel colors used for achievement, improvement and deterioration. */
	protected static final String performanceLevelAchievedColor = "#CBE5A3";
	protected static final String performanceLevelImprovedColor = "#74BA0C";
	protected static final String performanceLevelDeterioratedColor = "#BA0C48";
	/** Skill wheel colors used for both, first and second. */
	protected static final String performanceLevelCompareBothColor = "#CBE5A3";
	protected static final String performanceLevelCompareFirstColor = "#1082DF";
	protected static final String performanceLevelCompareSecondColor = "#DFD210";
	/**
	 * Colors used for different skill clusters in inner segments of the skill
	 * wheel. These colors are used when giving feedback as well. When more than ten
	 * skill clusters, colors are repeated.
	 */
	protected String[] skillWheelPLClusterColors;
	protected static final String skillWheelPLClusterColorsState = "str_SWPLClusterColors";
	protected static final String defaultPerformanceLevelSkillClusterColors = "#FFD6C6,#AAE5DF,#F2E09A,#A7C9EB,#E7C6FF,#CFE888,#E4C4AF,#A8EBF5,#FFB8B8,#97F3BC";
	/**
	 * Colors used for different skill clusters in outer segments of the skill
	 * wheel. When more than ten skill clusters, colors are repeated.
	 */
	protected String[] skillWheelClusterColors;
	public static final String skillWheelClusterColorsState = "str_SWClusterColors";
	public static final String defaultSkillClusterColors = "#FFA888,#47CCBE,#E2CC5C,#699FD5,#B37ADE,#A5CF2D,#CE9C7B,#66D7E9,#FF7878,#62E295";

	/**
	 * Message texts may contain references to specific PV-tool content, e.g., the
	 * cycle number, or the message sender or receiver name.
	 */
	public static final String messageVarPrefix = "[#";
	public static final String messageVarPostfix = "#]";
	public static final String messageVarCycleNumber = "cycle_number";
	public static final String messageVarSenderName = "sender_name";
	public static final String messageVarReceiverName = "receiver_name";
	public static final String messageVarNewLine = "CRLF";
	public static final String messageVarFeedbackRating = "feedback_rating";

	/**
	 * Used to store extra message text replace content.
	 */
	protected Map<String, String> messageExtraVarKeyValues = new HashMap<String, String>();

	/**
	 * All notifications will be sent to these e-mail addresses as well, without
	 * other users knowing. TODO Get bccs from elsewhere, probably a setting by a
	 * super teacher.
	 */
	protected String notificationMailAdminBccs;
	public static final String messageStateAdminBccs = "str_MailAdminBccs";

	/**
	 * Message ids of messages in message component PV_messages that will be automatically sent when certain events take place.
	 * TODO should be part of authoring environment.
	 */
	public static final String messageIdAskForFeedback = "notification_ask_for_feedback";
	public static final String messageIdAskTeacherForFeedback = "notification_ask_teacher_for_feedback";
	public static final String messageIdDoNotAskTeacherForFeedback = "notification_do_not_ask_teacher_for_feedback";
	public static final String messageIdGiveFeedback = "notification_give_feedback";
	public static final String messageIdAllFeedbacksGiven = "notification_all_feedbacks_given";
	public static final String messageIdCertificateOfParticipation = "certificate_of_participation";
	public static final String messageIdFeedbackRated = "feedback_rated";

	public static final String stepMessageTagName = "stepmessage";

	/**
	 * To improve performance, progress of different rungroups may be temporarily
	 * cached in memory.
	 */
	protected boolean _tempMemoryCache = false;
	protected Map<String, IXMLTag> _tempXmlStatusRootTagCache = new HashMap<String, IXMLTag>();

	/**
	 * An event queue to notify other users that are working with the PV-tool at the
	 * moment the event is occurring. The interface might be updated as well, e.g.,
	 * if a user has asked for feedback or has given feedback, this should be
	 * visible for other users in the same peer group. There are different type of
	 * events.
	 */
	public EventQueue<Event> eventQueuePVtoolkitNotifications = EventQueues.lookup("PVtoolkitNotifications",
			EventQueues.APPLICATION, true);
	public static final String onStepFinished = "onStepFinished";
	public static final String onSubStepFinished = "onSubStepFinished";
	public static final String onSubStepsFinished = "onSubStepsFinished";
	public static final String onPracticeShared = "onPracticeShared";
	public static final String onFeedbackShared = "onFeedbackShared";
	public static final String onAllFeedbackShared = "onAllFeedbackShared";
	public static final String onCertificateOfParticipation = "onCertificateOfParticipation";
	public static final String onFeedbackRated = "onFeedbackRated";
	/** A notification timer is used to spread the event handling in time. */
	protected CDefTimer notificationTimer = null;
	/**
	 * Notification events are stored in this array and handled by the notification
	 * timer.
	 */
	protected List<Event> notificationEvents = new ArrayList<Event>();

	/**
	 * For both getting peer groups and getting run groups in a peer group, the peer
	 * groups defined by a super teacher have to be walked through. By using
	 * following constants the same loop may be used for returning different types,
	 * this way preventing redundant code.
	 */
	public static final String resultTypePeerGroupTag = "peerGroupTag";
	public static final String resultTypeRunGroup = "runGroup";
	
	/**
	 * Tag status attribute ids.
	 */
	public static final String practiceRgaId = "practicergaid";
	public static final String practiceUuId = "practiceuuid";

	/**
	 * Webcam settings. Third and fourth one are read from emergo.properties TODO
	 * Probably first and second one should be read from this file as well.
	 */
	public static final String webcamRecordFormat = "video/webm";
	public static final String webcamRecordFileExtension = "webm";
	protected String locWebcamRecordFormat;
	protected String locWebcamRecordFileExtension;
	protected int maxWebcamRecordingLength = 420;
	protected int warningWebcamRecordingLength = 336;
	protected int dangerWebcamRecordingLength = 378;

	/**
	 * Default value for skill, skill cluster or sub skill level if no score is
	 * given yet.
	 */
	public static final int noLevelYet = -1;
	/**
	 * Default value for skill, skill cluster or sub skill level if not accessible
	 * and no score can be given.
	 */
	public static final int noLevelPossible = -2;

	/** Library used for detecting format of uploaded file, e.g., pdf or mp4. */
	public Tika tika = new Tika();

	/* START TESTING CODE */
	/**
	 * One test state defines if feedback is stored in states as well. Default it is
	 * false. It is meant to be able to extract teacher scores and tips/tops more
	 * easily. Note that the storage is not cycle or student specific. Scores thus
	 * are overwritten, if a second student is scored, or the same student in
	 * another cycle.
	 */
	protected boolean _storeFeedbackInStatesAsWell;
	/* END TESTING CODE */

	public IECaseComponent getCurrentStatesCaseComponent() {
		return currentStatesCaseComponent;
	}

	public void setCurrentStatesCaseComponent(IECaseComponent currentStatesCaseComponent) {
		this.currentStatesCaseComponent = currentStatesCaseComponent;
	}

	public IECaseComponent getCurrentStatesCurrentCaseComponent() {
		return currentStatesCurrentCaseComponent;
	}

	public void setCurrentStatesCurrentCaseComponent(IECaseComponent currentStatesCurrentCaseComponent) {
		this.currentStatesCurrentCaseComponent = currentStatesCurrentCaseComponent;
	}

	public IECaseComponent getCurrentRubricsCaseComponent() {
		return currentRubricsCaseComponent;
	}

	public void setCurrentRubricsCaseComponent(IECaseComponent currentRubricsCaseComponent) {
		this.currentRubricsCaseComponent = currentRubricsCaseComponent;
	}

	public IXMLTag getCurrentRubricTag() {
		return currentRubricTag;
	}

	public void setCurrentRubricTag(IXMLTag currentRubricTag) {
		this.currentRubricTag = currentRubricTag;
	}

	public String getCurrentRubricCode() {
		return currentRubricCode;
	}

	public void setCurrentRubricCode(String code) {
		currentRubricCode = code;
	}

	public int getNumberOfPerformanceLevels() {
		// number of performance levels is a child tag value of a rubric
		int numberOfPerformanceLevels = 5;
		if (getCurrentRubricTag() != null) {
			String numberOfLevels = getCurrentRubricTag().getChildValue("numberofperformancelevels");
			if (!numberOfLevels.equals("")) {
				try {
					numberOfPerformanceLevels = Integer.parseInt(numberOfLevels);
				} catch (NumberFormatException e) {
				}
			}
		}
		return numberOfPerformanceLevels;
	}

	/**
	 * levels to show is a status attribute value of a rubric if empty all levels
	 * should be shown. If not empty it must contain a string with levels separated
	 * by commas, e.g., '1,3,5' to indicate that levels 2 and 4 should not be shown
	 * this method has two functions. To indicate if a performance level should be
	 * shown and to indicate the level value to show if a rubric has 5 levels and
	 * the levels to show are '1,3,5', this method will return [1,0,2,0,3], where 0
	 * indicates the level should not be shown and the other numbers the value to be
	 * shown, in this case the number of stars to show and to choose. Saved scores
	 * then will be in the range [1..5], either 1, 3, or 5.
	 */
	public int[] getPerformanceLevels() {
		int[] levels = new int[getNumberOfPerformanceLevels()];
		String strLevelsToFilterOn = "";
		if (getCurrentRubricTag() != null) {
			strLevelsToFilterOn = getCurrentRubricTag().getCurrentStatusAttribute("levelstoshow")
					.replace(AppConstants.statusCommaReplace, ",");
		}
		if (!strLevelsToFilterOn.equals("")) {
			strLevelsToFilterOn = "," + strLevelsToFilterOn + ",";
		}
		int level = 1;
		for (int i = 0; i < levels.length; i++) {
			if (strLevelsToFilterOn.equals("") || strLevelsToFilterOn.contains("," + (i + 1) + ",")) {
				levels[i] = level;
				level++;
			} else {
				levels[i] = 0;
			}
		}
		return levels;
	}

	public int getNumberOfPerformanceLevelsToShow() {
		// if should be filtered on certain levels, this number is less than the number
		// of performance levels of the rubric
		int numberOfLevelsToShow = 0;
		int[] levels = getPerformanceLevels();
		for (int i = 0; i < levels.length; i++) {
			if (levels[i] > 0) {
				numberOfLevelsToShow++;
			}
		}
		return numberOfLevelsToShow;
	}

	/**
	 * level values to use instead of values already given is a status attribute
	 * value of a rubric if empty the default values are used (given per performance
	 * level). If not empty it must contain a string with level values separated by
	 * commas. E.g., if there are five levels within the rubric the values to use
	 * could be '1,2,2,2,3' (there should be 5 numbers!) to indicate that level
	 * values 3 and 4 should become 2 and level value 5 should become 3.
	 */
	public int[] getPerformanceLevelValues() {
		int[] levelValues = new int[getNumberOfPerformanceLevels()];
		String[] levelValuesArr = null;
		if (getCurrentRubricTag() != null) {
			String strLevelValues = getCurrentRubricTag().getCurrentStatusAttribute("levelvalues")
					.replace(AppConstants.statusCommaReplace, ",");
			if (!strLevelValues.equals("")) {
				levelValuesArr = strLevelValues.split(",");
			}
		}
		for (int i = 0; i < levelValues.length; i++) {
			levelValues[i] = i + 1;
			if (levelValuesArr != null) {
				try {
					levelValues[i] = Integer.parseInt(levelValuesArr[i]);
				} catch (NumberFormatException e) {
				}
			}
		}
		return levelValues;
	}

	public IXMLTag getContentTag(IXMLTag tag) {
		while (tag != null) {
			if (tag.getName().equals(AppConstants.contentElement)) {
				return tag;
			}
			tag = tag.getParentTag();
		}
		return null;
	}

	public IXMLTag getDefNodeTag(IXMLTag tag, String tagName) {
		if (tag == null) {
			return null;
		}
		IXMLTag contentTag = getContentTag(tag);
		if (contentTag != null && contentTag.getDefTag() != null) {
			return contentTag.getDefTag().getChild(tagName);
		}
		return null;
	}

	public IXMLTag getDefChildTag(IXMLTag tag, String tagName) {
		if (tag == null) {
			return null;
		}
		if (tag.getDefTag() != null) {
			return tag.getDefTag().getChild(tagName);
		}
		return null;
	}

	public IXMLTag getCurrentSkillTag() {
		return currentSkillTag;
	}

	public void setCurrentSkillTag(IXMLTag skillTag) {
		this.currentSkillTag = skillTag;
	}

	public IECaseComponent getCurrentProjectsCaseComponent() {
		return currentProjectsCaseComponent;
	}

	public void setCurrentProjectsCaseComponent(IECaseComponent currentProjectsCaseComponent) {
		this.currentProjectsCaseComponent = currentProjectsCaseComponent;
	}

	public IXMLTag getCurrentProjectTag() {
		return currentProjectTag;
	}

	public void setCurrentProjectTag(IXMLTag currentProjectTag) {
		this.currentProjectTag = currentProjectTag;
	}

	public IXMLTag getCurrentMethodTag() {
		return currentMethodTag;
	}

	public void setCurrentMethodTag(IXMLTag currentMethodTag) {
		this.currentMethodTag = currentMethodTag;
	}

	public boolean hasCycleTags() {
		return getPresentChildTags(
				new CRunPVToolkitCacAndTag(getCurrentProjectsCaseComponent(), currentCycleTag.getParentTag()), "cycle")
				.size() > 0;
	}

	public IXMLTag getCurrentCycleTag() {
		return currentCycleTag;
	}

	public void setCurrentCycleTag(IXMLTag currentCycleTag) {
		this.currentCycleTag = currentCycleTag;
		if (currentCycleTag == null) {
			return;
		}
		// NOTE store cycle tag id as state because in demo mode choosing a cycle (by
		// choosing a step) is a user action
		List<String> errorMessages = new ArrayList<String>();
		sSpring.setCurrentRunTagStatus(getCurrentStatesCurrentCaseComponent(), "state", "currentCycleTagId",
				AppConstants.statusKeyValue, "" + currentCycleTag.getAttribute(AppConstants.keyId),
				AppConstants.statusTypeRunGroup, true, errorMessages);
	}

	public int getCycleNumber(IXMLTag cycleTag) {
		// NOTE cycle number is shown in skill wheel
		if (cycleTag != null) {
			List<IXMLTag> cycleTags = getPresentChildTags(
					new CRunPVToolkitCacAndTag(getCurrentProjectsCaseComponent(), cycleTag.getParentTag()), "cycle");
			return cycleTags.indexOf(cycleTag) + 1;
		}
		return 1;
	}

	public int getCurrentCycleNumber() {
		return getCycleNumber(getCurrentCycleTag());
	}

	public int getMaxCycleNumber() {
		int numberOfCycleTags = getPresentChildTags(
				new CRunPVToolkitCacAndTag(getCurrentProjectsCaseComponent(), getCurrentMethodTag()), "cycle").size();
		// NOTE if no cycle tags max cycle is 1
		return numberOfCycleTags == 0 ? 1 : numberOfCycleTags;
	}

	public List<IXMLTag> getCurrentStepTags() {
		return currentStepTags;
	}

	public void setCurrentStepTags(List<IXMLTag> stepTags) {
		this.currentStepTags = stepTags;
	}

	public IXMLTag getCurrentStepTag() {
		return currentStepTag;
	}

	public List<List<IXMLTag>> getCurrentStepTagsPerCycle() {
		List<List<IXMLTag>> stepTagsPerCycle = new ArrayList<List<IXMLTag>>();
		for (int i = 0; i < getMaxCycleNumber(); i++) {
			stepTagsPerCycle.add(new ArrayList<IXMLTag>());
		}
		if (!hasCycleTags()) {
			stepTagsPerCycle.get(0).addAll(getCurrentStepTags());
		} else {
			for (IXMLTag stepTag : getCurrentStepTags()) {
				int cycleNumber = getCycleNumber(stepTag.getParentTag());
				stepTagsPerCycle.get(cycleNumber - 1).add(stepTag);
			}
		}
		return stepTagsPerCycle;
	}

	public IECaseComponent getCurrentMessagesCaseComponent() {
		return currentMessagesCaseComponent;
	}

	public void setCurrentMessagesCaseComponent(IECaseComponent currentMessagesCaseComponent) {
		this.currentMessagesCaseComponent = currentMessagesCaseComponent;
	}

	public void setCurrentStepTag(IXMLTag currentStepTag) {
		this.currentStepTag = currentStepTag;
		// NOTE store step tag id as state because in demo mode choosing a step is a
		// user action
		List<String> errorMessages = new ArrayList<String>();
		sSpring.setCurrentRunTagStatus(getCurrentStatesCurrentCaseComponent(), "state", "currentStepTagId",
				AppConstants.statusKeyValue, "" + currentStepTag.getAttribute(AppConstants.keyId),
				AppConstants.statusTypeRunGroup, true, errorMessages);
	}

	public IXMLTag getFirstStepTag(String stepType) {
		List<IXMLTag> stepTags = getStepTags(stepType);
		if (stepTags == null || stepTags.size() == 0) {
			return null;
		}
		return stepTags.get(0);
	}

	public IXMLTag getNextStepTag(IXMLTag stepTag) {
		if (stepTag == null) {
			return null;
		}
		// get step tags for current role
		List<IXMLTag> stepTags = null;
		if (hasStudentRole) {
			// NOTE if student role get all steps
			stepTags = getStepTags("");
		} else if (stepTag.getChildValue("steptype").equals(feedbackStepType)) {
			// NOTE other roles only get feedback steps
			stepTags = getStepTags(feedbackStepType);
		}
		if (stepTags == null) {
			return null;
		}
		// return next step tag
		int index = stepTags.indexOf(stepTag);
		if (index >= 0 && index < (stepTags.size() - 1)) {
			return stepTags.get(index + 1);
		}
		return null;
	}

	public IXMLTag getPreviousOrNextStepTagInCurrentCycle(IXMLTag stepTag, String stepType, boolean next) {
		if (stepTag == null) {
			return null;
		}
		// NOTE get previous or next step tag within current cycle or method
		List<IXMLTag> stepTags = getCurrentStepTags();
		int divIndex = 0;
		if (next) {
			divIndex = 1;
		} else {
			divIndex = -1;
		}
		int index = stepTags.indexOf(stepTag) + divIndex;
		while (index >= 0) {
			stepTag = stepTags.get(index);
			if (stepType.equals("") || stepTag.getChildValue("steptype").equals(stepType)) {
				return stepTag;
			}
			index = stepTags.indexOf(stepTag) + divIndex;
		}
		return null;
	}

	public IXMLTag getPreviousStepTagInCurrentCycle(IXMLTag stepTag, String wantedStepType) {
		return getPreviousOrNextStepTagInCurrentCycle(stepTag, wantedStepType, false);
	}

	public IXMLTag getNextStepTagInCurrentCycle(IXMLTag stepTag, String wantedStepType) {
		return getPreviousOrNextStepTagInCurrentCycle(stepTag, wantedStepType, true);
	}

	public List<IXMLTag> getStepTagsInPreviousCycles(IXMLTag stepTag, String wantedStepType) {
		// NOTE get previous step tags within other cycles of the method
		IECaseComponent projectsCaseComponent = getCurrentProjectsCaseComponent();
		List<IXMLTag> previousStepTags = new ArrayList<IXMLTag>();
		// NOTE parent tag might be cycle or method tag (if no cycles)
		IXMLTag cycleTag = stepTag.getParentTag();
		if (!cycleTag.getName().equals("cycle")) {
			// NOTE no cycles, return empty list
			return previousStepTags;
		}
		IXMLTag methodTag = cycleTag.getParentTag();
		if (!methodTag.getName().equals("method")) {
			// NOTE no method tag, return empty list
			return previousStepTags;
		}
		int cycleIndex = methodTag.getChildTags().indexOf(cycleTag);
		List<IXMLTag> cycleTags = methodTag.getChilds("cycle");
		for (IXMLTag tempCycleTag : cycleTags) {
			if (isRunSpecificTagPresent(new CRunPVToolkitCacAndTag(projectsCaseComponent, tempCycleTag))) {
				int tempCycleIndex = methodTag.getChildTags().indexOf(tempCycleTag);
				if (tempCycleIndex < cycleIndex) {
					// NOTE cycle before current cycle
					List<IXMLTag> stepTags = tempCycleTag.getChilds("step");
					for (IXMLTag tempStepTag : stepTags) {
						if (isRunSpecificTagPresent(new CRunPVToolkitCacAndTag(projectsCaseComponent, tempStepTag))) {
							if (wantedStepType.equals("")
									|| tempStepTag.getChildValue("steptype").equals(wantedStepType)) {
								previousStepTags.add(tempStepTag);
							}
						}
					}
				}
			}
		}
		return previousStepTags;
	}

	public List<IXMLTag> getStepTags(String stepType) {
		List<IXMLTag> stepTags = new ArrayList<IXMLTag>();
		// NOTE get node tags within current method tag
		List<IXMLTag> childNodeTags = new ArrayList<IXMLTag>();
		CDesktopComponents.cScript().getNewNodeTagList(getCurrentMethodTag().getChildTags(AppConstants.defValueNode),
				childNodeTags);
		for (IXMLTag childNodeTag : childNodeTags) {
			if (childNodeTag.getName().equals("step")) {
				if (stepType.equals("") || childNodeTag.getChildValue("steptype").equals(stepType)) {
					if (isRunSpecificTagPresent(
							new CRunPVToolkitCacAndTag(getCurrentProjectsCaseComponent(), childNodeTag))) {
						stepTags.add(childNodeTag);
					}
				}
			}
		}
		return stepTags;
	}

	protected List<IXMLTag> getCurrentStepTags(IXMLTag parentTag) {
		List<IXMLTag> currentStepTags = new ArrayList<IXMLTag>();
		for (IXMLTag stepTag : parentTag.getChilds("step")) {
			if (isRunSpecificTagPresent(new CRunPVToolkitCacAndTag(getCurrentProjectsCaseComponent(), stepTag))) {
				currentStepTags.add(stepTag);
			}
		}
		return currentStepTags;
	}

	public int getCurrentStepNumber() {
		return currentStepNumber;
	}

	public void setCurrentStepNumber(int currentStepNumber) {
		this.currentStepNumber = currentStepNumber;
		// NOTE store step number as state because in demo mode choosing a step is a
		// user action
		List<String> errorMessages = new ArrayList<String>();
		sSpring.setCurrentRunTagStatus(getCurrentStatesCurrentCaseComponent(), "state", "currentStepNumber",
				AppConstants.statusKeyValue, "" + currentStepNumber, AppConstants.statusTypeRunGroup, true,
				errorMessages);
	}

	public String getCurrentStep() {
		return currentStep;
	}

	public void setCurrentStep(String currentStep) {
		this.currentStep = currentStep;
		// NOTE store step as state because in demo mode choosing a step is a user
		// action
		List<String> errorMessages = new ArrayList<String>();
		sSpring.setCurrentRunTagStatus(getCurrentStatesCurrentCaseComponent(), "state", "currentStep",
				AppConstants.statusKeyValue, "" + currentStep, AppConstants.statusTypeRunGroup, true, errorMessages);
	}

	public String getCurrentStepType() {
		String stepType = "";
		if (currentStepTag != null) {
			stepType = currentStepTag.getChildValue("steptype");
		}
		return stepType;
	}

	public List<IXMLTag> getSubStepTags(CRunPVToolkitCacAndTag runPVToolkitCacAndTag) {
		return getPresentChildTags(runPVToolkitCacAndTag, "substep");
	}
	
	protected IXMLTag getPracticeTag(IXMLTag pSiblingStepTag) {
		if (pSiblingStepTag == null)
			pSiblingStepTag = getCurrentStepTag();
		if (pSiblingStepTag == null)
			return null;
		List<IXMLTag> lStepTags = getStepTags(practiceStepType);
		for (IXMLTag lStepTag : lStepTags) {
			if (lStepTag.getParentTag() == pSiblingStepTag.getParentTag())
				return lStepTag;
		}
		return null;
	}

	public String getCurrentPanel() {
		return currentPanel;
	}

	public void setCurrentPanel(String currentPanel) {
		this.currentPanel = currentPanel;
		// NOTE store step as state because in demo mode choosing a step is a user
		// action
		List<String> errorMessages = new ArrayList<String>();
		sSpring.setCurrentRunTagStatus(getCurrentStatesCurrentCaseComponent(), "state", "currentPanel",
				AppConstants.statusKeyValue, "" + currentPanel, AppConstants.statusTypeRunGroup, true, errorMessages);
	}

	public int getMaxWebcamRecordingLength() {
		return maxWebcamRecordingLength;
	}

	public void setMaxWebcamRecordingLength(int maxWebcamRecordingLength) {
		this.maxWebcamRecordingLength = maxWebcamRecordingLength;
	}

	public int getWarningWebcamRecordingLength() {
		return warningWebcamRecordingLength;
	}

	public void setWarningWebcamRecordingLength(int warningWebcamRecordingLength) {
		this.warningWebcamRecordingLength = warningWebcamRecordingLength;
	}

	public int getDangerWebcamRecordingLength() {
		return dangerWebcamRecordingLength;
	}

	public void setDangerWebcamRecordingLength(int dangerWebcamRecordingLength) {
		this.dangerWebcamRecordingLength = dangerWebcamRecordingLength;
	}

	public IERunGroup getCurrentRunGroup() {
		return sSpring.getRunGroup();
	}

	public IERunGroupAccount getCurrentRunGroupAccount() {
		return sSpring.getRunGroupAccount();
	}

	public IERunGroupAccount getRunGroupAccount(String rgaIdStr) {
		// NOTE rgaIdStr is stored in user progress
		int rgaId = 0;
		try {
			rgaId = Integer.parseInt(rgaIdStr);
		} catch (NumberFormatException e) {
		}
		if (_runGroupAccounts.containsKey(rgaId)) {
			return _runGroupAccounts.get(rgaId);
		}
		IERunGroupAccount runGroupAccount = sSpring.getRunGroupAccount(rgaId);
		if (runGroupAccount == null
				|| runGroupAccount.getERunGroup().getERun().getRunId() != sSpring.getRun().getRunId()) {
			// NOTE check if run group account is part of run because run status may be
			// copied if run is copied
			return null;
		}
		if (!runGroupAccount.getERunGroup().getActive()) {
			return null;
		}
		// store in property for better performance
		_runGroupAccounts.put(rgaId, runGroupAccount);
		return runGroupAccount;
	}

	public String getRgaName(String rgaIdStr) {
		IERunGroupAccount rga = getRunGroupAccount(rgaIdStr);
		if (rga != null) {
			return rga.getERunGroup().getName();
		}
		return "";
	}

	public List<IERunGroup> getActiveRunGroups() {
		if (_activeRunGroups == null) {
			_activeRunGroups = new ArrayList<IERunGroup>();
			for (IERunGroup runGroup : sSpring.getRunGroups()) {
				if (runGroup.getActive()) {
					_activeRunGroups.add(runGroup);
				}
			}
		}
		return _activeRunGroups;
	}

	public List<Integer> getRugIds(List<IERunGroup> runGroups) {
		List<Integer> ids = new ArrayList<Integer>();
		if (runGroups == null) {
			return ids;
		}
		for (IERunGroup runGroup : runGroups) {
			if (!ids.contains(runGroup.getRugId())) {
				ids.add(runGroup.getRugId());
			}
		}
		return ids;
	}

	public List<IERunGroupAccount> getActiveRunGroupAccounts() {
		if (_activeRunGroupAccounts == null) {
			_activeRunGroupAccounts = runGroupAccountManager
					.getAllRunGroupAccountsByRugIds(getRugIds(getActiveRunGroups()));
		}
		return _activeRunGroupAccounts;
	}

	public List<IERunGroupAccount> getRunGroupAccounts(List<IERunGroup> runGroups) {
		List<IERunGroupAccount> runGroupAccounts = new ArrayList<IERunGroupAccount>();
		for (IERunGroupAccount runGroupAccount : getActiveRunGroupAccounts()) {
			for (IERunGroup runGroup : runGroups) {
				if (runGroupAccount.getERunGroup().getRugId() == runGroup.getRugId()) {
					runGroupAccounts.add(runGroupAccount);
				}
			}
		}
		return runGroupAccounts;
	}

	public List<IERunGroupAccount> getRunGroupAccounts(IERunGroup runGroup) {
		List<IERunGroup> runGroups = new ArrayList<IERunGroup>();
		runGroups.add(runGroup);
		return getRunGroupAccounts(runGroups);
	}

	public List<Integer> getRgaIds(List<IERunGroupAccount> runGroupAccounts) {
		List<Integer> ids = new ArrayList<Integer>();
		if (runGroupAccounts == null) {
			return ids;
		}
		for (IERunGroupAccount runGroupAccount : runGroupAccounts) {
			if (!ids.contains(runGroupAccount.getRgaId())) {
				ids.add(runGroupAccount.getRgaId());
			}
		}
		return ids;
	}

	public IERunGroupAccount getActiveRunGroupAccount(int rugId) {
		for (IERunGroupAccount runGroupAccount : getActiveRunGroupAccounts()) {
			if (runGroupAccount.getERunGroup().getRugId() == rugId) {
				return runGroupAccount;
			}
		}
		return null;
	}

	public void setMemoryCaching(boolean cache) {
		_tempMemoryCache = cache;
		if (!cache) {
			_tempXmlStatusRootTagCache.clear();
		}
	}

	public String getZulfilepath() {
		return zulfilepath;
	}

	public boolean isAdmin() {
		String currentUserRole = (String) CDesktopComponents.cControl().getAccSessAttr("role");
		return currentUserRole != null && currentUserRole.equals(AppConstants.c_role_adm);
	}

	public boolean isSuperTeacher() {
		// is user super teacher as well
		return isSuperTeacher(getCurrentRunGroup());
	}
	
	public boolean isSuperTeacher(IERunGroup pRunGroup) {
		IECaseComponent caseComponent = sSpring.getCaseComponent(sSpring.getCase(), "", gCaseComponentNameStatesInitial);
		if (caseComponent == null) {
			return false;
		}
		// NOTE only show super teacher options if super_teacher is true
		return getRunGroupStatusValue(pRunGroup, caseComponent, "state", "super_teacher", AppConstants.statusKeyValue)
				.equals(AppConstants.statusValueTrue);
	}
	
	/* START EXPERIMENTAL CODE */
	protected String getExperimentId(IERunGroupAccount pRunGroupAccount) {
		int lRgaId = pRunGroupAccount.getRgaId();
		if (_runGroupAccountsExpIds.containsKey(lRgaId)) {
			return _runGroupAccountsExpIds.get(lRgaId);
		}
		IERunGroup lRunGroup = pRunGroupAccount.getERunGroup();
		
		IECaseComponent lExpCComponent = sSpring.getCaseComponent(sSpring.getCase(), "", gCaseComponentNameStatesExperiment);
		String lExpId = getRunGroupStatusValue(lRunGroup, lExpCComponent, "state", gExpIdStateKey, AppConstants.statusKeyValue);
		if (AppConstants.statusValueTrue.equals(getRunGroupStatusValue(lRunGroup, lExpCComponent, "state", gExpIdSetStateKey, AppConstants.statusKeyValue))) {
			if (StringUtils.isEmpty(lExpId)) {
				_runGroupAccountsExpIds.put(lRgaId, "");
				return "";
			}
			_runGroupAccountsExpIds.put(lRgaId, lExpId);
			return lExpId;
		}

		IERunGroupAccount lCurrRGAccount = getCurrentRunGroupAccount();
		boolean lResetAccount = false;
		if (getCurrentRunGroupAccount().getRgaId() != lRgaId) {
			lCurrRGAccount = getCurrentRunGroupAccount();
			sSpring.saveRunCaseComponentsStatus();
			sSpring.setRunGroupAccount(pRunGroupAccount);
			lResetAccount = true;
		}
		
		List<IXMLTag> lExpStateTags = CDesktopComponents.cScript().getNodeTags(lExpCComponent);
		if (StringUtils.isEmpty(lExpId)) {
			//if states variable not present, check if extra data content of account contains experiment setting information
			CRunPVToolkitJsonHelper lJSonHelper = new CRunPVToolkitJsonHelper(sSpring);
			lExpId = lJSonHelper.getStringFromExtraData(pRunGroupAccount, "EXP_id");
			if (!StringUtils.isEmpty(lExpId)) {
				IXMLTag lExpIdTag = getTagByNameAndKey(lExpStateTags, "state", gExpIdStateKey);
				if (lExpIdTag != null) {
					sSpring.setRunTagStatus(lExpCComponent, lExpIdTag, AppConstants.statusKeyValue, lExpId, true, AppConstants.statusTypeRunGroup, true, false);
				}
			}
		}
		if (!StringUtils.isEmpty(lExpId)) {
			IXMLTag lExpIdSetTag = getTagByNameAndKey(lExpStateTags, "state", gExpIdSetStateKey);
			if (lExpIdSetTag != null) {
				sSpring.setRunTagStatus(lExpCComponent, lExpIdSetTag, AppConstants.statusKeyValue, AppConstants.statusValueTrue, true, AppConstants.statusTypeRunGroup, true, false);
			}
			if (lResetAccount) {
				sSpring.saveRunCaseComponentsStatus();
				sSpring.setRunGroupAccount(lCurrRGAccount);
			}
			_runGroupAccountsExpIds.put(lRgaId, lExpId);
			return lExpId;
		}
		if (lResetAccount) {
			sSpring.saveRunCaseComponentsStatus();
			sSpring.setRunGroupAccount(lCurrRGAccount);
		}

		//NOTE next code is legacy; do not remove because otherwise wrong status data could be shown when reviewing older runs  
		
		String userid = "";
		boolean isPreviewRun = sSpring.getRun().getStatus() == AppConstants.run_status_test;
		if (isPreviewRun) {
			userid = pRunGroupAccount.getERunGroup().getName();
		} else {
			userid = pRunGroupAccount.getEAccount().getUserid();
		}
		if (userid.length() == "um_pv_001-01B-eo-ce-sa".length()) {
			_runGroupAccountsExpIds.put(lRgaId, userid.substring(12, 13));
			return userid.substring(12, 13);
		}
		_runGroupAccountsExpIds.put(lRgaId, "");
		return "";
	}

	protected boolean participatesInExperimentVariant(IERunGroupAccount pRunGroupAccount, String pVariantChar) {
		return (getExperimentId(pRunGroupAccount).equalsIgnoreCase(pVariantChar));
	}

	protected boolean participatesInExperimentVariant(IERunGroup pRunGroup, String pVariantChar) {
		List<IERunGroupAccount> lRunGroupAccounts = getRunGroupAccounts(pRunGroup);
		if (lRunGroupAccounts.isEmpty()) {
			return false;
		}
		return participatesInExperimentVariant(lRunGroupAccounts.get(0), pVariantChar);
	}

	protected boolean participatesInExperimentVariant(String pVariantChar) {
		return participatesInExperimentVariant(getCurrentRunGroupAccount(), pVariantChar);
	}

	public boolean participatesInVariantR1R3() {
		return participatesInExperimentVariant(gExperimentVariantR1R3);
	}

	public boolean participatesInVariantR2R4() {
		return participatesInExperimentVariant(gExperimentVariantR2R4);
	}

	public boolean participatesInVariantTPS() {
		return participatesInExperimentVariant(gExperimentVariantTPS);
	}

	public boolean participatesInVariantPS() {
		return participatesInExperimentVariant(gExperimentVariantPS);
	}

	public boolean participatesInVariantTFBU() {
		return participatesInExperimentVariant(gExperimentVariantTFBU);
	}

	public boolean participatesInVariantTFBD() {
		return participatesInExperimentVariant(gExperimentVariantTFBD);
	}

	public boolean participatesInVariantTFBR1() {
		return participatesInExperimentVariant(gExperimentVariantTFBR1);
	}

	public boolean participatesInVariantTFBR2() {
		return participatesInExperimentVariant(gExperimentVariantTFBR2);
	}

	public boolean participatesInVariantR1R3(IERunGroupAccount runGroupAccount) {
		return participatesInExperimentVariant(runGroupAccount, gExperimentVariantR1R3);
	}

	public boolean participatesInVariantR2R4(IERunGroupAccount runGroupAccount) {
		return participatesInExperimentVariant(runGroupAccount, gExperimentVariantR2R4);
	}

	public boolean participatesInVariantTPS(IERunGroupAccount runGroupAccount) {
		return participatesInExperimentVariant(runGroupAccount, gExperimentVariantTPS);
	}

	public boolean participatesInVariantPS(IERunGroupAccount runGroupAccount) {
		return participatesInExperimentVariant(runGroupAccount, gExperimentVariantPS);
	}

	public boolean participatesInVariantTFBU(IERunGroupAccount runGroupAccount) {
		return participatesInExperimentVariant(runGroupAccount, gExperimentVariantTFBU);
	}

	public boolean participatesInVariantTFBD(IERunGroupAccount runGroupAccount) {
		return participatesInExperimentVariant(runGroupAccount, gExperimentVariantTFBD);
	}

	public boolean participatesInVariantTFBR1(IERunGroupAccount runGroupAccount) {
		return participatesInExperimentVariant(runGroupAccount, gExperimentVariantTFBR1);
	}

	public boolean participatesInVariantTFBR2(IERunGroupAccount runGroupAccount) {
		return participatesInExperimentVariant(runGroupAccount, gExperimentVariantTFBR2);
	}

	public boolean participatesInVariantR1R3(IERunGroup runGroup) {
		return participatesInExperimentVariant(runGroup, gExperimentVariantR1R3);
	}

	public boolean participatesInVariantR2R4(IERunGroup runGroup) {
		return participatesInExperimentVariant(runGroup, gExperimentVariantR2R4);
	}

	public boolean participatesInVariantTPS(IERunGroup runGroup) {
		return participatesInExperimentVariant(runGroup, gExperimentVariantTPS);
	}

	public boolean participatesInVariantPS(IERunGroup runGroup) {
		return participatesInExperimentVariant(runGroup, gExperimentVariantPS);
	}

	public boolean participatesInVariantTFBU(IERunGroup runGroup) {
		return participatesInExperimentVariant(runGroup, gExperimentVariantTFBU);
	}

	public boolean participatesInVariantTFBD(IERunGroup runGroup) {
		return participatesInExperimentVariant(runGroup, gExperimentVariantTFBD);
	}

	public boolean participatesInVariantTFBR1(IERunGroup runGroup) {
		return participatesInExperimentVariant(runGroup, gExperimentVariantTFBR1);
	}

	public boolean participatesInVariantTFBR2(IERunGroup runGroup) {
		return participatesInExperimentVariant(runGroup, gExperimentVariantTFBR2);
	}
	
	public String getExperimentAssessments(IERunGroup pRunGroup) {
		IECaseComponent lExpCComponent = sSpring.getCaseComponent(sSpring.getCase(), "", gCaseComponentNameStatesExperiment);
		List <IERunGroupAccount> lRGAccounts = getRunGroupAccounts(pRunGroup);
		if (!lRGAccounts.isEmpty()) {
			String lExpId = getExperimentId(lRGAccounts.get(0));
			if (!Strings.isEmpty(lExpId))
				return getRunGroupStatusValue(pRunGroup, lExpCComponent, "state", gExpAssBaseStateKey + lExpId, AppConstants.statusKeyValue).replace(AppConstants.statusCommaReplace, ",");
		}
		return "";
	}
	
	public String getExperimentAssessment(int pCycle, int pStep, IERunGroup pRunGroup) {
		String lExpAssessmentNamesStr = getExperimentAssessments(pRunGroup);
		if (!Strings.isEmpty(lExpAssessmentNamesStr)) {
			List<String> lExpAssessmentNames = Arrays.asList(lExpAssessmentNamesStr.split(","));
			if (!lExpAssessmentNames.isEmpty()) {
				String lAssNameWC = gExpAssBaseCompNameKey + pCycle + "_step" + pStep + "_.*";
		        Pattern pattern = Pattern.compile(lAssNameWC);
		        // Use Stream API to filter based on the regex
		        List<String> lFoundAssessments = lExpAssessmentNames.stream().filter(lName -> pattern.matcher(lName).matches()).collect(Collectors.toList());
		        if (!lFoundAssessments.isEmpty()) {
		        	//NOTE assume only 1 assessment for cycle-step combination
		        	return lFoundAssessments.get(0);
		        }
				return "";
			}
		}
		return gExpAssNamesNotInStates;
	}

	public String getExperimentAssessmentType(int pCycle, int pStep, IERunGroup pRunGroup) {
		String lAssName = getExperimentAssessment(pCycle, pStep, pRunGroup);
		if (Strings.isEmpty(lAssName)) {
			return "";
		}
		if (gExpAssNamesNotInStates.equals(lAssName)) {
			return gExpAssNamesNotInStates;
		}
		return lAssName.substring((gExpAssBaseCompNameKey + pCycle + "_step" + pStep + "_").length());
	}

	public List<Boolean> getHideTeacherFeedbackPerCycle(IERunGroup actor) {
		List<Boolean> hideTeacherFeedbackPerCycle = new ArrayList<Boolean>();
		for (int i = 1; i <= getMaxCycleNumber(); i++) {
			boolean hide = (participatesInVariantR1R3(actor) && (i == 2 || i == 4))
					|| (participatesInVariantR2R4(actor) && (i == 1 || i == 3));
			if (!hide) {
				IXMLTag lStepTag = getCurrentStepTagsPerCycle().get(i - 1).get(0);
				if (lStepTag != null) {
					hide = excludeTeachersFeedback(actor, lStepTag);
				}
			}
			hideTeacherFeedbackPerCycle.add(hide);
		}
		return hideTeacherFeedbackPerCycle;
	}

	public boolean excludeFeedbackGiver(IERunGroup feedbackGiver, IERunGroup feedbackReceiver) {
		if (feedbackGiver == null || feedbackReceiver == null) {
			return false;
		}
		return hasTeacherRole(feedbackGiver) && getCurrentCycleNumber() > 0
				&& getHideTeacherFeedbackPerCycle(feedbackReceiver).get(getCurrentCycleNumber() - 1);
	}

	/* END EXPERIMENTAL CODE */

	public boolean isPreviewRun() {
		return sSpring.isPreviewRun();
	}

	/* START TESTING CODE */
	public boolean storeFeedbackInStatesAsWell() {
		IECaseComponent caseComponent = sSpring.getCaseComponent(sSpring.getCase(), "", gCaseComponentNameStatesInitial);
		if (caseComponent == null) {
			return false;
		}
		List<String> errorMessages = new ArrayList<String>();
		return sSpring.getCurrentRunTagStatus(caseComponent, "state", "store_feedback_in_states_as_well",
						AppConstants.statusKeyValue, AppConstants.statusTypeRunGroup, errorMessages)
				.equals(AppConstants.statusValueTrue);
	}
	/* END TESTING CODE */

	public String getTagChildValue(CRunPVToolkitCacAndTag runPVToolkitCacAndTag, String childName) {
		// NOTE child values may be overwritten by teachers. Then the data tag will be
		// adjusted.
		return sSpring.unescapeXML(runPVToolkitCacAndTag.getXmlTag().getChildValue(childName));
	}

	public void setTagChildValue(CRunPVToolkitCacAndTag runPVToolkitCacAndTag, String childName, String childValue) {
		// NOTE child values may be overwritten by teachers. Then the data tag will be
		// adjusted.
		runPVToolkitCacAndTag.getXmlTag().setChildValue(childName, sSpring.escapeXML(childValue));
		// NOTE get content helper to adjust content
		CContentHelper contentHelper = new CContentHelper();
		// NOTE save current cacId saved for authoring environment
		String saveCacId = (String) CDesktopComponents.cControl().getAccSessAttr("cacId");
		// NOTE set cacId for content helper to cac id of correct case component
		CDesktopComponents.cControl().setAccSessAttr("cacId", "" + runPVToolkitCacAndTag.getCaseComponent().getCacId());
		contentHelper.updateNode(runPVToolkitCacAndTag.getXmlTag(), !sSpring.isReadOnlyRun());
		// NOTE restore cacId for authoring environment
		CDesktopComponents.cControl().setAccSessAttr("cacId", saveCacId);
	}

	public boolean isRunSpecificTagPresent(CRunPVToolkitCacAndTag runPVToolkitCacAndTag) {
		// TODO presence of tags may be overwritten by teachers which will be stored in
		// their progress or run progress. filtering has to be done by super teacher
		// Then first try to get the presence by using key (cacId, tagId). If it is
		// empty return true.
		// NOTE temporarily filter skill clusters and sub skills
		if (runPVToolkitCacAndTag.getCaseComponent().getEComponent().getCode().equals(gCaseComponentNameRubrics)) {
			if (runPVToolkitCacAndTag.getXmlTag().getName().equals("skillcluster")) {
				// TODO filtering has to be done by super teacher
				// NOTE temporarily hide sixth skill cluster
				IXMLTag skillClusterTag = runPVToolkitCacAndTag.getXmlTag();
				if (skillClusterTag.getParentTag().getChilds("skillcluster").indexOf(skillClusterTag) == 5) {
					return false;
				}
			} else if (runPVToolkitCacAndTag.getXmlTag().getName().equals("subskill")) {
				// TODO filtering has to be done by super teacher
				// NOTE temporarily hide second sub skill of seventh skill cluster
				IXMLTag subSkillTag = runPVToolkitCacAndTag.getXmlTag();
				IXMLTag skillClusterTag = subSkillTag.getParentTag();
				if (skillClusterTag.getParentTag().getChilds("skillcluster").indexOf(skillClusterTag) == 6) {
					if (subSkillTag.getParentTag().getChilds("subskill").indexOf(subSkillTag) == 1) {
						return false;
					}
				}
			}
			// TODO filtering on performance level does not work yet
			/*
			 * else if
			 * (runPVToolkitCacAndTag.getXmlTag().getName().equals("performancelevel")) {
			 * //NOTE temporarily hide sixth skill cluster IXMLTag performanceLevelTag =
			 * runPVToolkitCacAndTag.getXmlTag(); if
			 * (performanceLevelTag.getParentTag().getChilds("performancelevel").indexOf(
			 * performanceLevelTag) == 2) { return false; } }
			 */
		}
		return !runPVToolkitCacAndTag.getXmlTag().getCurrentStatusAttribute(AppConstants.statusKeyPresent)
				.equals(AppConstants.statusValueFalse);
	}

	public List<IXMLTag> getPresentChildTags(CRunPVToolkitCacAndTag runPVToolkitCacAndTag, String childTagName) {
		List<IXMLTag> childTags = new ArrayList<IXMLTag>();
		if (isRunSpecificTagPresent(runPVToolkitCacAndTag)) {
			for (IXMLTag childTag : runPVToolkitCacAndTag.getXmlTag().getChilds(childTagName)) {
				if (isRunSpecificTagPresent(
						new CRunPVToolkitCacAndTag(runPVToolkitCacAndTag.getCaseComponent(), childTag))) {
					childTags.add(childTag);
				}
			}
		}
		return childTags;
	}

	protected IECaseComponent getCurrentCaseComponent(String stateCacIdKey, String componentCode) {
		IECaseComponent caseComponent = null;
		List<String> errorMessages = new ArrayList<String>();
		String currentCacId = sSpring.getCurrentRunTagStatus(
				getCurrentStatesCurrentCaseComponent(), "state", stateCacIdKey, AppConstants.statusKeyValue,
				AppConstants.statusTypeRunGroup, errorMessages);
		if (currentCacId.equals("")) {
			// TODO if multiple case components the name of the one you want should be an
			// initial run parameter. has to be done by super teacher
			caseComponent = sSpring.getCaseComponent(sSpring.getCase(), componentCode, "");
			if (caseComponent != null) {
				// store current cac id in state stateCacIdKey
				sSpring.setCurrentRunTagStatus(getCurrentStatesCurrentCaseComponent(), "state", stateCacIdKey,
						AppConstants.statusKeyValue, "" + caseComponent.getCacId(), AppConstants.statusTypeRunGroup,
						true, errorMessages);
			}
		} else {
			caseComponent = sSpring.getCaseComponent(currentCacId);
		}
		return caseComponent;
	}

	protected IXMLTag getCurrentXmlTag(IECaseComponent caseComponent, IXMLTag parentTag, String childName,
			String stateTagIdKey) {
		IXMLTag xmlTag = getCurrentXmlTag(caseComponent, stateTagIdKey);
		if (xmlTag == null) {
			// NOTE get tag within parent tag. Assume only one tag exists
			xmlTag = parentTag.getChild(childName);
			if (xmlTag != null) {
				// store current tag id in state stateTagIdKey
				sSpring.setCurrentRunTagStatus(getCurrentStatesCurrentCaseComponent(), "state", stateTagIdKey,
						AppConstants.statusKeyValue, "" + xmlTag.getAttribute(AppConstants.keyId),
						AppConstants.statusTypeRunGroup, true, new ArrayList<String>());
			}
		}
		return xmlTag;
	}

	protected IXMLTag getCurrentXmlTag(IECaseComponent caseComponent, String stateTagIdKey) {
		IXMLTag xmlTag = null;
		List<String> errorMessages = new ArrayList<String>();
		String currentTagId = sSpring.getCurrentRunTagStatus(
				getCurrentStatesCurrentCaseComponent(), "state", stateTagIdKey, AppConstants.statusKeyValue,
				AppConstants.statusTypeRunGroup, errorMessages);
		if (!currentTagId.equals("")) {
			xmlTag = sSpring.getCaseComponentXmlDataTagById(caseComponent, currentTagId);
		}
		return xmlTag;
	}

	protected Integer getStateValueAsInteger(IECaseComponent caseComponent, String stateTagIdKey) {
		Integer integer = null;
		List<String> errorMessages = new ArrayList<String>();
		String integerStr = sSpring.getCurrentRunTagStatus(caseComponent, "state", stateTagIdKey,
				AppConstants.statusKeyValue, AppConstants.statusTypeRunGroup, errorMessages);
		int number = -1;
		try {
			number = Integer.parseInt(integerStr);
			integer = new Integer(number);
		} catch (NumberFormatException e) {
		}
		return integer;
	}

	public Integer getStateValueAsInteger(String stateTagIdKey) {
		return getStateValueAsInteger(getCurrentStatesCaseComponent(), stateTagIdKey);
	}

	public Integer getCurrentStateValueAsInteger(String stateTagIdKey) {
		return getStateValueAsInteger(getCurrentStatesCurrentCaseComponent(), stateTagIdKey);
	}

	public String getStateValueAsString(String stateTagIdKey) {
		List<String> errorMessages = new ArrayList<String>();
		return sSpring.getCurrentRunTagStatus(getCurrentStatesCaseComponent(), "state",
				stateTagIdKey, AppConstants.statusKeyValue, AppConstants.statusTypeRunGroup, errorMessages);
	}

	public String getCurrentStateValueAsString(String stateTagIdKey) {
		List<String> errorMessages = new ArrayList<String>();
		return sSpring.getCurrentRunTagStatus(getCurrentStatesCurrentCaseComponent(), "state",
				stateTagIdKey, AppConstants.statusKeyValue, AppConstants.statusTypeRunGroup, errorMessages);
	}
	
	public String getRunGroupStatusValue(IERunGroup pRunGroup, IECaseComponent pCaseComponent, String pName, String pKey, String pValue) {
		if (pRunGroup.getRugId() == getCurrentRunGroup().getRugId()) {
			List<String> errorMessages = new ArrayList<String>();
			return sSpring.getCurrentRunTagStatus(pCaseComponent, pName, pKey, pValue, AppConstants.statusTypeRunGroup, errorMessages);
		}
		IXMLTag rootTag = getXmlDataPlusStatusRootTag(pRunGroup, pCaseComponent, AppConstants.statusTypeRunGroup);
		if (rootTag != null) {
			List<IXMLTag> nodeTags = CDesktopComponents.cScript().getNodeTags(rootTag);
			for (IXMLTag lNodeTag : nodeTags) {
				if (lNodeTag.getName().equals(pName) && lNodeTag.getChildValue(lNodeTag.getDefAttribute(AppConstants.defKeyKey)).equals(pKey)) {
					return sSpring.getCurrentTagStatus(lNodeTag, pValue);
				}
			}
		}
		return "";
	}

	public IXMLTag getReferencedNodeTag(IECaseComponent caseComponent, IXMLTag nodeTag, String refChildTagName) {
		// get node tag referenced to by another node tag, e.g., method tag has a
		// reference to a rubric tag
		if (nodeTag == null) {
			return null;
		}
		String referencetype = nodeTag.getDefTag().getChild(refChildTagName).getAttribute(AppConstants.defKeyReftype);
		if (referencetype.equals("")) {
			return null;
		}
		List<IXMLTag> referenceTags = cCaseHelper.getRefTags(referencetype, "" + AppConstants.statusKeySelectedIndex,
				sSpring.getSCaseRoleHelper().getCaseRole(), caseComponent, nodeTag);
		// NOTE ref should not multiple
		if (referenceTags.size() == 1) {
			return referenceTags.get(0);
		}
		return null;
	}

	public void setReferencedNodeTag(IECaseComponent caseComponent, IXMLTag nodeTag, String refChildTagName,
			IECaseComponent referencedCaseComponent, IXMLTag referencedNodeTag) {
		// set node tag referenced to by another node tag, e.g., method tag has a
		// reference to a rubric tag
		if (nodeTag == null || referencedCaseComponent == null || referencedNodeTag == null) {
			return;
		}
		List<List<String>> refTagIdsList = new ArrayList<List<String>>();
		List<String> refTagIds = new ArrayList<String>();
		String triggerId = "" + AppConstants.statusKeySelectedIndex;
		String carId = "" + sSpring.getSCaseRoleHelper().getCaseRole().getCarId();
		String cacId = "" + referencedCaseComponent.getCacId();
		String tagId = referencedNodeTag.getAttribute(AppConstants.keyId);
		refTagIds.add(triggerId + "," + carId + "," + cacId + "," + tagId);
		refTagIdsList.add(refTagIds);
		setReferenceIds(caseComponent, nodeTag, refChildTagName, refTagIdsList);
	}

	protected void setReferenceIds(IECaseComponent caseComponent, IXMLTag tag, String refChildTagName,
			List<List<String>> refTagIdsList) {
		IECaseComponent caseCaseComponent = sSpring.getCaseComponent("case", "");
		if (!sSpring.isReadOnlyRun()) {
			// NOTE if not read only run update data tree. It is updated in application
			// memory as well.
			IXMLTag caseComponentRootTag = sSpring.getXmlDataTree(caseComponent);
			IXMLTag caseCaseComponentRootTag = sSpring.getXmlDataTree(caseCaseComponent);
			setReferenceIds(caseCaseComponentRootTag, caseComponent, caseComponentRootTag, tag, refChildTagName,
					refTagIdsList);
			sSpring.setCaseComponentData("" + caseCaseComponent.getCacId(),
					sSpring.getXmlManager().xmlTreeToDoc(caseCaseComponentRootTag));
		}
		// NOTE always update data plus status tree cached as SSpring property. This
		// will work for both preview and preview read-only mode.
		IXMLTag caseComponentRootTag = sSpring.getXmlDataPlusRunStatusTree(caseComponent,
				AppConstants.statusTypeRunGroup);
		IXMLTag caseCaseComponentRootTag = sSpring.getXmlDataPlusRunStatusTree(caseCaseComponent,
				AppConstants.statusTypeRunGroup);
		setReferenceIds(caseCaseComponentRootTag, caseComponent, caseComponentRootTag, tag, refChildTagName,
				refTagIdsList);
	}

	protected void setReferenceIds(IXMLTag caseCaseComponentRootTag, IECaseComponent caseComponent,
			IXMLTag caseComponentRootTag, IXMLTag tag, String refChildTagName, List<List<String>> refTagIdsList) {
		tag = sSpring.getXmlManager().getTagById(caseComponentRootTag, tag.getAttribute(AppConstants.statusKeyId));
		IXMLTag refTag = tag.getChild(refChildTagName);
		if (refTag == null) {
			refTag = sSpring.getXmlManager().newXMLTag(refChildTagName, "");
			tag.getChildTags().add(refTag);
			refTag.setParentTag(tag);
			refTag.setDefTag(getDefChildTag(tag, refChildTagName));
		}
		String referenceType = tag.getDefTag().getChild(refChildTagName).getAttribute(AppConstants.defKeyReftype);
		List<String> referenceTypeList = new ArrayList<String>();
		referenceTypeList.add(referenceType);
		List<IXMLTag> tagList = new ArrayList<IXMLTag>();
		tagList.add(tag);
		cCaseHelper.setReferenceIds(caseCaseComponentRootTag.getChild(AppConstants.contentElement), caseComponent,
				referenceTypeList, tagList, refTagIdsList);
	}

	protected IXMLTag asetReferenceIds(IXMLTag caseCaseComponentRootTag, IECaseComponent caseComponent,
			IXMLTag caseComponentRootTag, IXMLTag tag, String refChildTagName, List<List<String>> refTagIdsList) {
		tag = sSpring.getXmlManager().getTagById(caseComponentRootTag, tag.getAttribute(AppConstants.statusKeyId));
		IXMLTag refTag = tag.getChild(refChildTagName);
		if (refTag == null) {
			refTag = sSpring.getXmlManager().newXMLTag(refChildTagName, "");
			tag.getChildTags().add(refTag);
			refTag.setParentTag(tag);
			refTag.setDefTag(getDefChildTag(tag, refChildTagName));
		}
		return tag;
	}

	public IXMLTag getTagByNameAndKey(List<IXMLTag> nodeTags, String tagName, String tagKey) {
		for (IXMLTag nodeTag : nodeTags) {
			if (nodeTag.getName().equals(tagName)
					&& nodeTag.getChildValue(nodeTag.getDefAttribute(AppConstants.defKeyKey)).equals(tagKey)) {
				return nodeTag;
			}
		}
		return null;
	}

	protected void getOrRestoreCaseComponents() {
		// NOTE loads all PV-tool case components
		setCurrentStatesCaseComponent(sSpring.getCaseComponent(sSpring.getCase(), "", gCaseComponentNameStates));
		setCurrentStatesCurrentCaseComponent(sSpring.getCaseComponent(sSpring.getCase(), "", CRunPVToolkit.gCaseComponentNameStatesCurrent));
		// TODO initially following case components should be chosen by super teacher.
		// And later on they should be determined by state values containing their
		// cacids
		setCurrentRubricsCaseComponent(sSpring.getCaseComponent(sSpring.getCase(), "", gCaseComponentNameRubrics));
		setCurrentProjectsCaseComponent(sSpring.getCaseComponent(sSpring.getCase(), "", gCaseComponentNameProjects));
		setCurrentMessagesCaseComponent(sSpring.getCaseComponent(sSpring.getCase(), "", gCaseComponentNameMessages));
	}

	public void getOrRestoreProjectTags() {
		// TODO select rubric tag depending on chosen project and method by user, and on
		// current cycle (if method has cycles)
		// NOTE get root tag of projects component. Assume only one project tag exists
		IXMLTag rootTag = sSpring.getXmlDataPlusRunStatusTree(getCurrentProjectsCaseComponent(),
				AppConstants.statusTypeRunGroup);
		if (rootTag == null) {
			return;
		}
		IXMLTag contentRootTag = rootTag.getChild(AppConstants.contentElement);
		if (contentRootTag == null || contentRootTag.getChildTags().size() == 0) {
			return;
		}

		// TODO project tag and method tag should be initially chosen by super teacher
		setCurrentProjectTag(
				getCurrentXmlTag(getCurrentProjectsCaseComponent(), contentRootTag, "project", "currentProjectTagId"));
		setCurrentMethodTag(getCurrentXmlTag(getCurrentProjectsCaseComponent(), getCurrentProjectTag(), "method",
				"currentMethodTagId"));
		setCurrentCycleTag(getCurrentXmlTag(getCurrentProjectsCaseComponent(), getCurrentMethodTag(), "cycle",
				"currentCycleTagId"));

		// determine current step tags
		List<IXMLTag> currentStepTags = new ArrayList<IXMLTag>();
		if (getCurrentCycleTag() == null) {
			currentStepTags.addAll(getCurrentStepTags(getCurrentMethodTag()));
		} else {
			// NOTE loop over all cycle tags
			for (IXMLTag cycleTag : getCurrentCycleTag().getParentTag().getChilds("cycle")) {
				if (isRunSpecificTagPresent(new CRunPVToolkitCacAndTag(getCurrentProjectsCaseComponent(), cycleTag))) {
					currentStepTags.addAll(getCurrentStepTags(cycleTag));
				}
			}
		}

		setCurrentStepTags(currentStepTags);
	}

	public void getOrRestoreRubricTags() {
		// NOTE get root tag of rubrics component. Assume only one rubrics tag exists
		IXMLTag rootTag = sSpring.getXmlDataPlusRunStatusTree(getCurrentRubricsCaseComponent(),
				AppConstants.statusTypeRunGroup);
		if (rootTag == null) {
			return;
		}
		IXMLTag contentRootTag = rootTag.getChild(AppConstants.contentElement);
		if (contentRootTag == null || contentRootTag.getChildTags().size() == 0) {
			return;
		}
		IXMLTag rubricTag = getReferencedNodeTag(getCurrentProjectsCaseComponent(), getCurrentMethodTag(), "refrubric");
		if (rubricTag != null) {

			setCurrentRubricTag(rubricTag);
			setCurrentRubricCode(sSpring.unescapeXML(rubricTag.getChildValue("pid")));

		}

		setCurrentSkillTag(getCurrentXmlTag(getCurrentRubricsCaseComponent(), getCurrentRubricTag(), "skill",
				"currentSkillTagId"));
	}

	public void updateStatusProperties() {
		getOrRestoreProjectTags();
		getOrRestoreRubricTags();
		restoreStatus();
	}

	public void restoreStatus() {
		// NOTE restores step and panel
		IXMLTag stepTag = getCurrentXmlTag(getCurrentProjectsCaseComponent(), "currentStepTagId");
		if (stepTag != null) {
			setCurrentStepTag(stepTag);
		}
		Integer stepNumber = getCurrentStateValueAsInteger("currentStepNumber");
		if (stepNumber != null) {
			setCurrentStepNumber(stepNumber.intValue());
		}
		String step = getCurrentStateValueAsString("currentStep");
		if (!StringUtils.isEmpty(step)) {
			setCurrentStep(step);
		}
		String panel = getCurrentStateValueAsString("currentPanel");
		if (!StringUtils.isEmpty(panel)) {
			setCurrentPanel(panel);
		}

		Integer max_length = getStateValueAsInteger("max_webcam_recording_length");
		if (max_length != null && max_length > 0) {
			setMaxWebcamRecordingLength(max_length.intValue());
		}
		Integer warning_length = getStateValueAsInteger("warning_webcam_recording_length");
		if (warning_length != null && warning_length > 0) {
			setWarningWebcamRecordingLength(warning_length.intValue());
		}
		Integer danger_length = getStateValueAsInteger("danger_webcam_recording_length");
		if (danger_length != null && danger_length > 0) {
			setDangerWebcamRecordingLength(danger_length.intValue());
		}

		// max_webcam_recording_length
		// warning_webcam_recording_length
		// danger_webcam_recording_length

		// TODO restoreStatus should also restore tutor specific content, which is case
		// content overruled by a tutor
	}

	protected void getRoles() {
		hasStudentRole = hasStudentRole(sSpring.getRunGroup());
		hasTeacherRole = hasTeacherRole(sSpring.getRunGroup());
		hasStudentAssistantRole = hasStudentAssistantRole(sSpring.getRunGroup());
		hasPassiveTeacherRole = hasPassiveTeacherRole(sSpring.getRunGroup());
		hasPeerStudentRole = hasPeerStudentRole(sSpring.getRunGroup());

		hasPeersWithStudentRole = hasPeersWithStudentRole(sSpring.getRunGroup());
		hasPeersWithTeacherRole = hasPeersWithTeacherRole(sSpring.getRunGroup());
		hasPeersWithStudentAssistantRole = hasPeersWithStudentAssistantRole(sSpring.getRunGroup());
		hasPeersWithPassiveTeacherRole = hasPeersWithPassiveTeacherRole(sSpring.getRunGroup());
		hasPeersWithPeerStudentRole = hasPeersWithPeerStudentRole(sSpring.getRunGroup());

		isStudent = hasStudentRole || hasPeerStudentRole;
		isTeacher = hasTeacherRole || hasStudentAssistantRole || hasPassiveTeacherRole;
		isFeedbackGiver = isStudent || hasTeacherRole || hasStudentAssistantRole;
	}
	
	public static String[] getPerformanceLevelSkillClusterColorsProp() {
		String lSWPLClusterColorsStr = VView.getStaticLabel("PV-toolkit-skillwheel.colors." + skillWheelPLClusterColorsState);
		if (StringUtils.isEmpty(lSWPLClusterColorsStr))
			lSWPLClusterColorsStr = defaultPerformanceLevelSkillClusterColors;
		return lSWPLClusterColorsStr.split(" *, *");
	}

	protected void setPerformanceLevelSkillClusterColors() {
		String lSWPLClusterColorsStr = sSpring.unescapeXML(getStateValueAsString(skillWheelPLClusterColorsState).replace(AppConstants.statusCommaReplace, ","));
		if (StringUtils.isEmpty(lSWPLClusterColorsStr))
			skillWheelPLClusterColors = getPerformanceLevelSkillClusterColorsProp();
		else
			skillWheelPLClusterColors = lSWPLClusterColorsStr.split(" *, *");
	}

	public String[] getPerformanceLevelSkillClusterColors() {
		return skillWheelPLClusterColors;
	}

	public static String[] getSkillClusterColorsProp() {
		String lSWClusterColorsStr = VView.getStaticLabel("PV-toolkit-skillwheel.colors." + skillWheelClusterColorsState);
		if (StringUtils.isEmpty(lSWClusterColorsStr))
			lSWClusterColorsStr = defaultSkillClusterColors;
		return lSWClusterColorsStr.split(" *, *");
	}

	protected void setSkillClusterColors() {
		String lSWClusterColorsStr = sSpring.unescapeXML(getStateValueAsString(skillWheelClusterColorsState).replace(AppConstants.statusCommaReplace, ","));
		if (StringUtils.isEmpty(lSWClusterColorsStr))
			skillWheelClusterColors = getSkillClusterColorsProp();
		else
			skillWheelClusterColors = lSWClusterColorsStr.split(" *, *");
	}

	public String[] getSkillClusterColors() {
		return skillWheelClusterColors;
	}

	protected void setMailAdminBccs() {
		notificationMailAdminBccs = sSpring.unescapeXML(getStateValueAsString(messageStateAdminBccs).replace(AppConstants.statusCommaReplace, ","));
	}
	
	public String getMailAdminBccs() {
		return notificationMailAdminBccs;
	}
	
	protected void addPeerGroupMember(IERunGroupAccount pRunGroupAccount, IXMLTag pPeerGroupTag, String pPeerGroupName, String pRole) {
		boolean lChanged = false;
		boolean lFound = false;
		IXMLTag lStatusChildTag = getStatusChildTag(pPeerGroupTag);
		List<CRunPVToolkitPeerGroupMember> lPeerGroupMembers = new ArrayList<>();
		for (IXMLTag lMemberTag : lStatusChildTag.getChilds("member")) {
			String lRgaId = getStatusChildTagAttribute(lMemberTag, "rgaid");
			String lRole = getStatusChildTagAttribute(lMemberTag, "role");
			if (lRgaId.equals("" + pRunGroupAccount.getRgaId())) {
				lFound = true;
				if (!lRole.equals(pRole)) {
					lRole = pRole;
					lChanged = true;
				}
			}
			IERunGroupAccount lRunGroupAccount = getRunGroupAccount(lRgaId);
			if (lRunGroupAccount != null) {
				lPeerGroupMembers.add(new CRunPVToolkitPeerGroupMember(lRunGroupAccount, lRole));
			}
		}
		if (!lFound) {
			lPeerGroupMembers.add(new CRunPVToolkitPeerGroupMember(pRunGroupAccount, pRole));
			lChanged = true;
		}
		if (lChanged) {
			savePeerGroup(pRunGroupAccount.getERunGroup(), pPeerGroupTag, pPeerGroupName, peerGroupCycleScope, lPeerGroupMembers);
		}
	}
	
	protected void removePeerGroupMember(IERunGroupAccount pRunGroupAccount, IXMLTag pPeerGroupTag, String pPeerGroupName, String pRole) {
		boolean lChanged = false;
		IXMLTag lStatusChildTag = getStatusChildTag(pPeerGroupTag);
		List<CRunPVToolkitPeerGroupMember> lPeerGroupMembers = new ArrayList<>();
		for (IXMLTag lMemberTag : lStatusChildTag.getChilds("member")) {
			String lRgaId = getStatusChildTagAttribute(lMemberTag, "rgaid");
			String lRole = getStatusChildTagAttribute(lMemberTag, "role");
			boolean lAddToMembers = true;
			if (lRgaId.equals("" + pRunGroupAccount.getRgaId())) {
				if (lRole.equals(pRole)) {
					lAddToMembers = false;
					lChanged = true;
				}
			}
			if (lAddToMembers) {
				IERunGroupAccount lRunGroupAccount = getRunGroupAccount(lRgaId);
				if (lRunGroupAccount != null) {
					lPeerGroupMembers.add(new CRunPVToolkitPeerGroupMember(lRunGroupAccount, lRole));
				}
			}
		}
		if (lChanged) {
			savePeerGroup(pRunGroupAccount.getERunGroup(), pPeerGroupTag, pPeerGroupName, peerGroupCycleScope, lPeerGroupMembers);
		}
	}
	
	protected void addNewPeerGroupMember(IERunGroupAccount pRunGroupAccount, Set<String> pPGNamesSet, String pRole) {
		List<CRunPVToolkitPeerGroupMember> lPeerGroupMembers = new ArrayList<>();
		lPeerGroupMembers.add(new CRunPVToolkitPeerGroupMember(pRunGroupAccount, pRole));
		for (String lPGName : pPGNamesSet) {
			savePeerGroup(pRunGroupAccount.getERunGroup(), null, lPGName, peerGroupCycleScope, lPeerGroupMembers);
		}
	}
	
	protected boolean checkForPeergroupMembership(IERunGroup pRunGroup) {
		IERunGroupAccount lSaveRGAccount = getCurrentRunGroupAccount();
		boolean lResetAccount = false;
		List<IERunGroupAccount> lRunGroupAccounts = getRunGroupAccounts(pRunGroup);
		if (lRunGroupAccounts.isEmpty()) {
			return false;
		}
		IERunGroupAccount lCurrRGAccount = lRunGroupAccounts.get(0);
		String lPGIsSetStateKey = "boolPeerGroupSetFromED";
		if (lCurrRGAccount.getRgaId() != lSaveRGAccount.getRgaId()) {
			sSpring.saveRunCaseComponentsStatus();
			sSpring.setRunGroupAccount(lCurrRGAccount);
			lResetAccount = true;
		}
		
		if (!AppConstants.statusValueTrue.equals(getRunGroupStatusValue(pRunGroup, getCurrentStatesCaseComponent(), "state", lPGIsSetStateKey, AppConstants.statusKeyValue))) {

			CRunPVToolkitJsonHelper lJSonHelper = new CRunPVToolkitJsonHelper(sSpring);
			
			String[] lPGStudArray = lJSonHelper.getStringArrayFromExtraData(lCurrRGAccount, "Peergroups_student");
			String[] lPGTeachArray = lJSonHelper.getStringArrayFromExtraData(lCurrRGAccount, "Peergroups_teacher");
			
			Set<String> lPGStudSet = null;
			Set<String> lPGTeachSet = null;

			if ((lPGStudArray != null) && (lPGStudArray.length > 0)) {
				lPGStudSet = new HashSet<>(Arrays.asList(lPGStudArray));
			}
			if ((lPGTeachArray != null) && (lPGTeachArray.length > 0)) {
				lPGTeachSet = new HashSet<>(Arrays.asList(lPGTeachArray));
			}

			//NOTE if peer group member setting not present in account extradata field, do nothing (peer group removal must be done by superteacher)
			if ((lPGStudSet != null) || (lPGTeachSet != null)) {
				
				CRunPVToolkitCacAndTags lPeerGroupTags = getPeerGroupTags(pRunGroup);
				List<IXMLTag> lPGXMLTags = lPeerGroupTags.getXmlTags();
				
				if (lPGXMLTags != null) {
					for (IXMLTag lPGTag : lPGXMLTags) {
						IXMLTag lStatusChildTag = getStatusChildTag(lPGTag);
						String lPGName = sSpring.unescapeXML(lStatusChildTag.getAttribute("name").replace(AppConstants.statusCommaReplace, ","));
						if ((lPGStudSet != null) && !lPGStudSet.isEmpty() && lPGStudSet.contains(lPGName)) {
							addPeerGroupMember(lCurrRGAccount, lPGTag, lPGName, peerGroupStudentRole);
							lPGStudSet.remove(lPGName);
						} else {
							removePeerGroupMember(lCurrRGAccount, lPGTag, lPGName, peerGroupStudentRole);
						}
						if ((lPGTeachSet != null) && !lPGTeachSet.isEmpty() && lPGTeachSet.contains(lPGName)) {
							addPeerGroupMember(lCurrRGAccount, lPGTag, lPGName, peerGroupTeacherRole);
							lPGTeachSet.remove(lPGName);
						} else {
							removePeerGroupMember(lCurrRGAccount, lPGTag, lPGName, peerGroupTeacherRole);
						}
					}
				}
				
				//NOTE remaining peer group member settings must be added to new peer group
				if ((lPGStudSet != null) && !lPGStudSet.isEmpty()) {
					addNewPeerGroupMember(lCurrRGAccount, lPGStudSet, peerGroupStudentRole);
				}
				if ((lPGTeachSet != null) && !lPGTeachSet.isEmpty()) {
					addNewPeerGroupMember(lCurrRGAccount, lPGTeachSet, peerGroupTeacherRole);
				}
				
				List<IXMLTag> lStateTags = CDesktopComponents.cScript().getNodeTags(getCurrentStatesCaseComponent());
				IXMLTag lIsSetStateTag = getTagByNameAndKey(lStateTags, "state", lPGIsSetStateKey);
				if (lIsSetStateTag != null) {
					// if not found, do nothing
					sSpring.setRunTagStatus(getCurrentStatesCaseComponent(), lIsSetStateTag, AppConstants.statusKeyValue, AppConstants.statusValueTrue, true, AppConstants.statusTypeRunGroup, true, false);
				}
			}
		}

		if (lResetAccount) {
			sSpring.saveRunCaseComponentsStatus();
			sSpring.setRunGroupAccount(lSaveRGAccount);
		}
		return lResetAccount;
	}
		
	protected boolean checkForPeergroupMemberships() {
		String lShouldWeCheckForPeerGroups = "boolCheckEDForPeerGroups";
		if (!AppConstants.statusValueTrue.equals(getRunGroupStatusValue(getCurrentRunGroup(), getCurrentStatesCaseComponent(), "state", lShouldWeCheckForPeerGroups, AppConstants.statusKeyValue))) {
			return false;
		}
		boolean lRGSwitched = false;
		for (IERunGroup lRunGroup : getActiveRunGroups()) {
			if (checkForPeergroupMembership(lRunGroup)) {
				lRGSwitched = true;
			}
		}
		//NOTE no need to check again for this account
		List<IXMLTag> lStateTags = CDesktopComponents.cScript().getNodeTags(getCurrentStatesCaseComponent());
		IXMLTag lStateTag = getTagByNameAndKey(lStateTags, "state", lShouldWeCheckForPeerGroups);
		if (lStateTag != null) {
			sSpring.setRunTagStatus(getCurrentStatesCaseComponent(), lStateTag, AppConstants.statusKeyValue, AppConstants.statusValueFalse, true, AppConstants.statusTypeRunGroup, true, false);
		}
		return lRGSwitched;
	}
	
	@Override
	public void onCreate(CreateEvent event) {
		super.onCreate(event);

		// subscribe to event queue to be able to get notifications by other users
		eventQueuePVtoolkitNotifications.subscribe(new EventListener<Event>() {
			@Override
			public void onEvent(Event event) {
				handleNotificationEvent(event);
			}
		});
		
		getOrRestoreCaseComponents();
		// NOTE first determine project tags, because rubric tag is set in method tag
		getOrRestoreProjectTags();
		getOrRestoreRubricTags();

		if (checkForPeergroupMemberships()) {
			// if current rungroupaccount was changed we need to restore full status
			updateStatusProperties();
		} else {
			restoreStatus();
		}

		getRoles();

		setSkillClusterColors();
		setPerformanceLevelSkillClusterColors();
		setMailAdminBccs();

		// TODO restoreStatus should also restore tutor specific content, which is case
		// content overruled by a tutor
		// TODO Add user specific labels using vView.setUserSpecificLabels()
		// These are values entered in tutor landing pages that overrule general values
		// that should be shown to tutor and student
		// In principle it may also be values entered by a student that may overrule
		// tutor values

		/* START EXPERIMENTAL CODE */
		participatesInVariantR1R3 = participatesInVariantR1R3();
		participatesInVariantR2R4 = participatesInVariantR2R4();
		/* END EXPERIMENTAL CODE */

		// load all macros
		Events.sendEvent("onUpdate", this, null);
		// restore content of all macros
		// NOTE use echoEvent so all ZK components are already rendered
		Events.echoEvent("onRenderRestoredStatus", this, null);

		// timer for handling notifications
		notificationTimer = new CDefTimer();
		// if multiple notifications show one every 10 seconds
		notificationTimer.setDelay(10000);
		notificationTimer.setRepeats(true);
		notificationTimer.stop();
		appendChild(notificationTimer);
		addOnTimerEventListener(notificationTimer, this);

		/* START TESTING CODE */
		// get experimental state value
		_storeFeedbackInStatesAsWell = storeFeedbackInStatesAsWell();
		/* END TESTING CODE */

	}

	public void publishNotification(Event event) {
		// current user notifies other users (listeners)
		// first save progress to disk, so listeners will have updated progress
		sSpring.saveRunCaseComponentsStatus();

		eventQueuePVtoolkitNotifications.publish(event);
	}

	protected void handleNotificationEvent(Event event) {
		// handle notification to current user, user timer to handle it
		IERunGroup sender = ((CRunPVToolkitNotification) event.getData()).getRunGroup();
		// check if sender is in same peer group as current rungroup
		if (sender.getRugId() != getCurrentRunGroup().getRugId()) {
			// NOTE only notify other users
			notificationTimer.stop();
			notificationEvents.add(event);
			sSpring.getSLogHelper().logAction("Notification event added: " + event.getName() + ": " + event.toString());
			notificationTimer.start();
		}
	}

	protected void addOnTimerEventListener(Component component, Component notifyComponent) {
		// timer event to handle notification to current user
		component.addEventListener("onTimer", new EventListener<Event>() {
			@Override
			public void onEvent(Event event) {
				if (notificationEvents.size() > 0) {
					// show one notification
					// NOTE only show notification if it does not interfere with webcam recording or
					// other user activities
					Camera camera = (Camera) vView.getComponent("practiceNewRecordingCameraCamera");
					boolean showNotification = (camera == null || !camera.isRecording());
					if (showNotification) {
						// handle notification and remove it from list
						Event notificationEvent = notificationEvents.get(0);
						Events.echoEvent(notificationEvent.getName(), notifyComponent, notificationEvent.getData());
						notificationEvents.remove(0);
					}
				}
				if (notificationEvents.size() == 0) {
					// no more notifications, stop timer
					notificationTimer.stop();
				}
			}
		});
	}

	/* The different kind of notifications */
	public void onStepFinished(Event event) {
		// NOTE only give notification for teacher role
		IERunGroup sender = ((CRunPVToolkitNotification) event.getData()).getRunGroup();
		IXMLTag stepTag = ((CRunPVToolkitNotification) event.getData()).getXmlTag();
		Object data = ((CRunPVToolkitNotification) event.getData()).getData();
		// TODO has to be implemented in teacher version
	}

	public void onSubStepsFinished(Event event) {
		IERunGroup sender = ((CRunPVToolkitNotification) event.getData()).getRunGroup();
		IXMLTag stepTag = ((CRunPVToolkitNotification) event.getData()).getXmlTag();
		Object data = ((CRunPVToolkitNotification) event.getData()).getData();

		// NOTE if the user has loaded the feedback sub steps overview, update it, so
		// the user might be able to finish the feedback step
		CRunPVToolkitFeedbackSubStepsDiv feedbackSubStepsDiv = (CRunPVToolkitFeedbackSubStepsDiv) vView
				.getComponent("feedbackSubStepsDiv");
		if (feedbackSubStepsDiv != null && feedbackSubStepsDiv._initialized) {
			feedbackSubStepsDiv.update();
		}

		// TODO show notification. Users get a little pop-up like in Teams and should
		// see the new notification in the notifications overview
//		vView.showMessagebox(getRoot(), vView.getCLabel("PV-toolkit-feedback.notification.shared.all.confirmation"), vView.getCLabel("PV-toolkit-feedback.notification.shared.all"), Messagebox.OK, Messagebox.INFORMATION);
	}

	public void onSubStepFinished(Event event) {
		// NOTE probably only give notification for teacher role, but maybe this
		// notification is not needed at all
		IERunGroup sender = ((CRunPVToolkitNotification) event.getData()).getRunGroup();
		IXMLTag subStepTag = ((CRunPVToolkitNotification) event.getData()).getXmlTag();
		Object data = ((CRunPVToolkitNotification) event.getData()).getData();

		// TODO show notification. Users get a little pop-up like in Teams and should
		// see the new notification in the notifications overview
	}

	protected void handleMessage() {
		// if the user has loaded messages overviews, update them, so the user sees the
		// message
		CRunPVToolkitDashboardMessagesDiv div1 = (CRunPVToolkitDashboardMessagesDiv) vView
				.getComponent("dashboardMessagesDiv");
		if (div1 == null) {
			div1 = (CRunPVToolkitDashboardMessagesDiv) vView.getComponent("teacherDashboardMessagesDiv");
		}
		if (div1 != null && div1._initialized) {
			div1.update();
		}
		CRunPVToolkitDashboardMessagesDetailsDiv div2 = (CRunPVToolkitDashboardMessagesDetailsDiv) vView
				.getComponent("dashboardMessagesDetailsDiv");
		if (div2 != null && div2._initialized) {
			div2.update();
		}
		if (div2 == null) {
			div2 = (CRunPVToolkitDashboardMessagesDetailsDiv) vView.getComponent("teacherDashboardMessagesDetailsDiv");
		}
		CRunPVToolkitDashboardMessagesDetailsDiv div3 = (CRunPVToolkitDashboardMessagesDetailsDiv) vView
				.getComponent("notificationsMessagesDetailsDiv");
		if (div3 != null && div3._initialized) {
			div3.update();
		}
		// highlight message icon
		runWnd.highlightComponentInverse("notificationsIcon");
	}

	public void onPracticeShared(Event event) {
		// NOTE give notification for both teacher and student role
		IERunGroup sender = ((CRunPVToolkitNotification) event.getData()).getRunGroup();
		IXMLTag practiceStepTag = ((CRunPVToolkitNotification) event.getData()).getXmlTag();
		Object data = ((CRunPVToolkitNotification) event.getData()).getData();

		// if the user has loaded the feedback recordings overview, update it, so the
		// user sees a new student to give feedback to
		CRunPVToolkitFeedbackRecordingsDiv div = (CRunPVToolkitFeedbackRecordingsDiv) vView
				.getComponent("feedbackRecordingsDiv");
		if (div != null && div._initialized) {
			div.update();
		}

		// message is sent by practice sharer
		handleMessage();
	}

	public void onFeedbackShared(Event event) {
		// NOTE give notification for both teacher and student role
		IERunGroup sender = ((CRunPVToolkitNotification) event.getData()).getRunGroup();
		IXMLTag feedbackStepTag = ((CRunPVToolkitNotification) event.getData()).getXmlTag();
		Object data = ((CRunPVToolkitNotification) event.getData()).getData();

		// NOTE if feedback is shared update the dashboard, so the user sees the new
		// feedback given in the skill wheel and tips/tops overviews
		resetDashboardPanel();

		// message is sent by feedback sharer so highlight message icon
		handleMessage();
	}

	public static String replaceMessageVars(String messageStr, Map<String, String> messageVarKeyValues) {
		// replace all references to PV-tool content in a message
		for (Map.Entry<String, String> messageVarKeyValue : messageVarKeyValues.entrySet()) {
			messageStr = messageStr.replace(messageVarPrefix + messageVarKeyValue.getKey() + messageVarPostfix,
					messageVarKeyValue.getValue());
		}
		return messageStr;
	}

	public IXMLTag getMessageTag(String messagePid) {
		IXMLTag messageTag = null;
		IXMLTag rootTag = getXmlDataPlusStatusRootTag(getCurrentRunGroup(), getCurrentMessagesCaseComponent(),
				AppConstants.statusTypeRunGroup);
		if (rootTag != null) {
			List<IXMLTag> nodeTags = CDesktopComponents.cScript().getNodeTags(rootTag);
			for (IXMLTag nodeTag : nodeTags) {
				if (nodeTag.getChildValue("pid").equals(messagePid)) {
					messageTag = nodeTag;
					break;
				}
			}
		}
		if (messageTag == null)
			sSpring.getSLogHelper().logAction("    message with id " + messagePid + " not found in message component " + getCurrentMessagesCaseComponent().getName());

		return messageTag;
	}

	public String mailNotification(IERunGroupAccount runGroupAccountFrom, IERunGroupAccount runGroupAccountTo,
			IXMLTag messageTag) {
		// mail notification from one user to another
		String mailAddress = runGroupAccountTo.getEAccount().getEmail();
		if (StringUtils.isEmpty(mailAddress)) {
			return "mail address of rgaid " + runGroupAccountTo.getRgaId() + "' is empty";
		}
		if (messageTag == null) {
			return "message tag is null";
		}

		// replace all references to PV-tool content in a message
		Map<String, String> messageVarKeyValues = new HashMap<String, String>();
		messageVarKeyValues.put(messageVarCycleNumber, "" + getCurrentCycleNumber());
		if (runGroupAccountFrom != null) {
			messageVarKeyValues.put(messageVarSenderName, runGroupAccountFrom.getERunGroup().getName());
		}
		if (runGroupAccountTo != null) {
			messageVarKeyValues.put(messageVarReceiverName, runGroupAccountTo.getERunGroup().getName());
		}
		messageVarKeyValues.put(messageVarNewLine, System.lineSeparator());

		// extra content
		messageExtraVarKeyValues.forEach((lKey, lValue) -> {
			messageVarKeyValues.put(lKey, lValue);
	    });

		String messageTitle = replaceMessageVars(sSpring.unescapeXML(messageTag.getChildValue("title")),
				messageVarKeyValues);
		String messageText = replaceMessageVars(sSpring.unescapeXML(messageTag.getChildValue("text")),
				messageVarKeyValues);

		return mailNotification(mailAddress, messageTitle, messageText);
	}

	public String mailNotification(String mailAddresses, String messageTitle, String messageText) {
		if (!StringUtils.isEmpty(mailAddresses)) {
			if (!getMailAdminBccs().equals("")) {
				// add admins who should be notified as well
				mailAddresses += "," + getMailAdminBccs();
			}
			return sSpring.getAppManager().sendMail(
					sSpring.getAppManager().getSysvalue(AppConstants.syskey_smtpnoreplysender), null, null,
					mailAddresses, messageTitle, messageText);
		}
		return "tos is empty";
	}

	public boolean mailNotification(String eventName, IXMLTag stepTag, IXMLTag practiceTag) {
		// send mail notification depending on a certain event

		// get current run group account (sender)
		IERunGroup currentRunGroup = getCurrentRunGroup();
		List<IERunGroupAccount> runGroupAccounts = runGroupAccountManager
				.getAllRunGroupAccountsByRugId(currentRunGroup.getRugId());
		if (runGroupAccounts.size() != 1) {
			return false;
		}
		IERunGroupAccount currentRunGroupAccount = runGroupAccounts.get(0);

		// a student has asked for feedback
		if (eventName.equals(onPracticeShared)) {
			// NOTE send mail to all peer group members
			IXMLTag lStuMessageTag = getMessageTag(messageIdAskForFeedback);
			if (lStuMessageTag == null) {
				return false;
			}
			sSpring.getSLogHelper().logAction("*** MailNotification practice shared ***");
			IXMLTag lTeacherMessageTag = lStuMessageTag;

			int lStuMayAskFbCount = mayAskTeacherFeedbackCount(currentRunGroup);
			if (lStuMayAskFbCount >= 0) {
				if (practiceIsTeacherFeedbackAsked(currentRunGroup, practiceTag))
					lTeacherMessageTag = getMessageTag(messageIdAskTeacherForFeedback);
				else
					lTeacherMessageTag = getMessageTag(messageIdDoNotAskTeacherForFeedback);
			}
			for (IERunGroup runGroup : getUsersThatGiveFeedback(currentRunGroup, stepTag, true, false)) {
				if (runGroup.getRugId() != currentRunGroup.getRugId()) {
					IXMLTag messageTag = lStuMessageTag;
					if (hasTeacherRole(runGroup))
						messageTag = lTeacherMessageTag;
					if (messageTag != null) {
						// get run group account (feedback giver)
						runGroupAccounts = runGroupAccountManager.getAllRunGroupAccountsByRugId(runGroup.getRugId());
						if (runGroupAccounts.size() == 1) {
							IERunGroupAccount feedbackGiver = runGroupAccounts.get(0);

							sendMessage(messageTag, feedbackGiver, stepTag);

							String result = mailNotification(currentRunGroupAccount, feedbackGiver, messageTag);
							boolean mailIsSent = result.equals("");
							sSpring.getSLogHelper()
									.logAction("    MailNotification from: rgaid=" + currentRunGroupAccount.getRgaId()
											+ " to: rgaid=" + feedbackGiver.getRgaId() + ", "
											+ feedbackGiver.getEAccount().getLastname() + ", "
											+ feedbackGiver.getEAccount().getEmail() + ", "
											+ (mailIsSent ? "succeeded" : "failed: " + result));
						}
					}
				}
			}
		}

		// a user has given feedback
		else if (eventName.equals(onFeedbackShared) && practiceTag != null) {
			IXMLTag messageTag = getMessageTag(messageIdGiveFeedback);
			if (messageTag == null) {
				return false;
			}
			// get feedback receiver from practice tag
			IERunGroupAccount feedbackReceiver = getRunGroupAccount(practiceTag.getAttribute(practiceRgaId));
			if (feedbackReceiver == null) {
				return false;
			}

			sendMessage(messageTag, feedbackReceiver, stepTag);

			sSpring.getSLogHelper().logAction("*** MailNotification feedback shared ***");
			String result = mailNotification(currentRunGroupAccount, feedbackReceiver, messageTag);
			boolean mailIsSent = result.equals("");
			sSpring.getSLogHelper()
					.logAction("    MailNotification from: rgaid=" + currentRunGroupAccount.getRgaId() + " to: rgaid="
							+ feedbackReceiver.getRgaId() + ", " + feedbackReceiver.getEAccount().getLastname() + ", "
							+ feedbackReceiver.getEAccount().getEmail() + ", "
							+ (mailIsSent ? "succeeded" : "failed: " + result));
		}

		// all feedbackgivers have given feedback
		else if (eventName.equals(onAllFeedbackShared) && practiceTag != null) {
			IXMLTag messageTag = getMessageTag(messageIdAllFeedbacksGiven);
			if (messageTag == null) {
				return false;
			}
			// get feedback receiver from practice tag
			IERunGroupAccount feedbackReceiver = getRunGroupAccount(practiceTag.getAttribute(practiceRgaId));
			if (feedbackReceiver == null) {
				return false;
			}

			sendMessage(messageTag, feedbackReceiver, stepTag);

			sSpring.getSLogHelper().logAction("*** MailNotification all feedbacks are given ***");
			String result = mailNotification(currentRunGroupAccount, feedbackReceiver, messageTag);
			boolean mailIsSent = result.equals("");
			sSpring.getSLogHelper()
					.logAction("    MailNotification from: SYSTEM to: rgaid=" + feedbackReceiver.getRgaId() + ", "
							+ feedbackReceiver.getEAccount().getLastname() + ", "
							+ feedbackReceiver.getEAccount().getEmail() + ", "
							+ (mailIsSent ? "succeeded" : "failed: " + result));
		}

		// a student has finished the method
		else if (eventName.equals(onCertificateOfParticipation)) {
			// NOTE send mail with certificate of participation to student
			if (!hasStudentRole) {
				// do nothing
				return false;
			}
			IXMLTag messageTag = getMessageTag(messageIdCertificateOfParticipation);
			if (messageTag == null) {
				return false;
			}

			sendMessage(messageTag, currentRunGroupAccount, stepTag);

			sSpring.getSLogHelper().logAction("*** MailNotification certificate of participation ***");
			// sender is null (system message)
			String result = mailNotification(null, currentRunGroupAccount, messageTag);
			boolean mailIsSent = result.equals("");
			sSpring.getSLogHelper()
					.logAction("    MailNotification from SYSTEM to: rgaid=" + currentRunGroupAccount.getRgaId() + ", "
							+ currentRunGroupAccount.getEAccount().getLastname() + ", "
							+ currentRunGroupAccount.getEAccount().getEmail() + ", "
							+ (mailIsSent ? "succeeded" : "failed: " + result));
		}

		// a student has finished view feedback step and must give feedback rating to student peers
		else if (eventName.equals(onFeedbackRated)) {
			// NOTE send mail with rating level to student peers
			if (!hasStudentRole) {
				// do nothing
				return false;
			}
			IXMLTag messageTag = getMessageTag(messageIdFeedbackRated);
			if (messageTag == null) {
				return false;
			}

			
			
			sSpring.getSLogHelper().logAction("*** MailNotification feedback rated ***");
			
			List<IXMLTag> lRatingTags = getStatusChildTag(practiceTag).getChilds(_feedbackRatingKeyStr);
			if ((lRatingTags == null) || (lRatingTags.size() == 0))
				return false;
			for (IXMLTag lRatingTag : lRatingTags) {
				String lLevel = getStatusChildTagAttribute(lRatingTag, _feedbackRatingLevelStr);
				String lRgaId = getStatusChildTagAttribute(lRatingTag, _feedbackRatingRgaIdStr);
				if (!StringUtils.isEmpty(lRgaId)) {
					messageExtraVarKeyValues.put(messageVarFeedbackRating, lLevel);
					IERunGroupAccount lFeedbackGiver = runGroupAccountManager.getRunGroupAccount(Integer.parseInt(lRgaId));
					sendMessage(messageTag, lFeedbackGiver, stepTag);

					String result = mailNotification(currentRunGroupAccount, lFeedbackGiver, messageTag);
					boolean mailIsSent = result.equals("");
					sSpring.getSLogHelper()
							.logAction("    MailNotification from: rgaid=" + currentRunGroupAccount.getRgaId()
									+ " to: rgaid=" + lFeedbackGiver.getRgaId() + ", "
									+ lFeedbackGiver.getEAccount().getLastname() + ", "
									+ lFeedbackGiver.getEAccount().getEmail() + ", "
									+ (mailIsSent ? "succeeded" : "failed: " + result));
				}
			}
			
			messageExtraVarKeyValues.clear();

		}
		return true;
	}

	public void onUpdate(Event event) {
		getChildren().clear();

		// set PV-tool background
		new CRunPVToolkitDefImage(this, new String[] { "class", "src" },
				new Object[] { "background", zulfilepath + "blauw-achtergrond.svg" });

		// show PV-tool menu button
		if (has_main_menu) {
			CRunPVToolkitNavigationImage navigateImage = new CRunPVToolkitMainMenuImage(this,
					new String[] { "id", "class", "src" },
					new Object[] { "mainMenuButton", "mainMenuButton", zulfilepath + "hoofdmenu.svg" });
			navigateImage.init("mainMenuButton", "mainMenu");
		}

		Map<String, Object> propertyMap = new HashMap<String, Object>();
		propertyMap.put("zulfilepath", zulfilepath);

		propertyMap.put("isStudent", isStudent);
		propertyMap.put("isTeacher", isTeacher);
		propertyMap.put("isPractitioner", hasStudentRole);
		propertyMap.put("isFeedbackGiver", isFeedbackGiver);
		propertyMap.put("isSuperTeacher", isSuperTeacher());

		// load macro files
		for (int i = 0; i < macroZulFiles.length; i++) {
			// TODO remove if statement.
			// Temporarily do not include super teacher macro if no super teacher, because
			// macro causes CSS conflicts.
			if (!macroZulFiles[i].equals("PV-toolkit-super-teacher.zul") || isSuperTeacher()) {
				HtmlMacroComponent macro = new HtmlMacroComponent();
				appendChild(macro);
				macro.setDynamicProperty("a_propertyMap", propertyMap);
				macro.setMacroURI(zulfilepath + macroZulFiles[i]);
			}
		}

		// show info button
		// TODO remove? because it is not used
		new CRunPVToolkitDevImage(this, new String[] { "id", "class", "visible" },
				new Object[] { "infoButton", "infoButton", "false" });
	}

	public void onRenderRestoredStatus(Event event) {
		/* START USER SPECIFIC CODE */
		// learning activity descriptions may depend on role and step
		// TODO should be set by super teacher
		if (setUserSpecificLearningActivities())
			updateStatusProperties();

		// learning materials may depend on cycle and peer group
		// TODO should be set by super teacher
		setUserSpecificLearningMaterials();
		/* END USER SPECIFIC CODE */

		// determine or restore panel
		String panel = getCurrentPanel();
		if (panel == null) {
			if (hasStudentRole) {
				// NOTE set panel to first prepare step
				panel = setPanel(getFirstStepTag(prepareStepType));
			} else if (hasTeacherRole || hasStudentAssistantRole || hasPeerStudentRole) {
				// NOTE show panel for third step, because user will give feedback only
				panel = setPanel(getFirstStepTag(feedbackStepType));
			} else if (hasPassiveTeacherRole) {
				// NOTE show dashboard, passive teacher is not involved in method steps
				panel = "teacherDashboard";
			}
		}
		if (StringUtils.isEmpty(panel)) {
			// if no panel can be determined, the user is no member of a peer group yet, so
			// has no access
			vView.showMessagebox(getRoot(), vView.getCLabel("PV-toolkit.noaccess.confirmation"),
					vView.getCLabel("PV-toolkit.noaccess"), Messagebox.OK, Messagebox.EXCLAMATION);
		} else {
			showPanel(panel);
		}
	}

	/* START USER SPECIFIC CODE */
	public boolean setUserSpecificLearningActivities() {
		boolean lChanged = false;
		// TODO better general solution. super teacher should set this
		if (hasTeacherRole || hasStudentAssistantRole) {
			// NOTE student role is default
			// set teacher learning activities. They only participate in step 3 and may have
			// different descriptions for these steps.
			String[] stepTagPids = new String[getMaxCycleNumber()];
			for (int i = 0; i < getMaxCycleNumber(); i++) {
				stepTagPids[i] = "stap_" + (i + 1) + "_3";
			}
			if (setUserSpecificLearningActivities(stepTagPids, "_teacher"))
				lChanged = true;
		}
		/* START EXPERIMENTAL CODE */
		if (hasStudentRole && hasPeersWithStudentAssistantRole) {
			// NOTE variant TPS is default
			// set student learning activities. they differ in last step, for research
			// purposes
			if (setUserSpecificLearningActivities(new String[] { "stap_" + getMaxCycleNumber() + "_5" }, "_PS"))
				lChanged = true;
		}
		if (hasStudentRole && participatesInVariantR2R4) {
			// NOTE variant R1R3 is default
			// set student learning activities. They differ for all 'view feedback' steps
			String[] stepTagPids = new String[getMaxCycleNumber()];
			for (int i = 0; i < getMaxCycleNumber(); i++) {
				stepTagPids[i] = "stap_" + (i + 1) + "_4";
			}
			if (setUserSpecificLearningActivities(stepTagPids, "_R2R4"))
				lChanged = true;
		}
		/* END EXPERIMENTAL CODE */
		return lChanged;
	}

	/* START EXPERIMENTAL CODE */
	protected boolean setUserSpecificLearningActivities(String[] stepTagPids, String stepTagPidPostfix) {
		boolean lChanged = false;
		for (IXMLTag stepTag : getCurrentStepTags()) {
			for (int i = 0; i < stepTagPids.length; i++) {
				for (IXMLTag learningActivityTag : stepTag.getChilds("learningactivity")) {
					if (learningActivityTag.getChildValue("pid").equals(stepTagPids[i] + stepTagPidPostfix)) {
						sSpring.setRunTagStatus(getCurrentProjectsCaseComponent(),
								learningActivityTag, AppConstants.statusKeyOpened, AppConstants.statusValueTrue, true,
								AppConstants.statusTypeRunGroup, true, false);
						lChanged = true;
					}
				}
			}
		}
		return lChanged;
	}
	/* END EXPERIMENTAL CODE */

	public void setUserSpecificLearningMaterials() {
		if (hasStudentRole) {
			/* START RUBRIC SPECIFIC CODE */
			String lLMIDsSetStateKey = "str_LM_ids_set";
			String lLMIDsSetString = getStateValueAsString(lLMIDsSetStateKey);
			if (!AppConstants.statusValueTrue.equals(lLMIDsSetString)) {
				//NOTE if "true", learning materials are initialized

				List<IXMLTag> stateTags = CDesktopComponents.cScript().getNodeTags(getCurrentStatesCaseComponent());
				//first check if states variable is present:
				String lLMIDsStateKey = "str_LM_ids";
				String lLMIDsString = getStateValueAsString(lLMIDsStateKey);
				String[] lLMArray;
				boolean lStoreLMIdsInState = false;
				
				if (!StringUtils.isEmpty(lLMIDsString)) {
					lLMArray = lLMIDsString.replace(AppConstants.statusCommaReplace, ",").split(",");
				} else {
					//if states variable not present, check 'extradata' JSON content of current account
					CRunPVToolkitJsonHelper lJSonHelper = new CRunPVToolkitJsonHelper(sSpring);
					lLMArray = lJSonHelper.getStringArrayFromExtraData(getCurrentRunGroupAccount(), "LM_ids");
					lStoreLMIdsInState = true;
				}
				if ((lLMArray != null) && (lLMArray.length > 0)) {
					String lLMIds = "";
					int lNr = 0;
					for (String lLMId : lLMArray) {
						// get state tag using state tag key
						lNr++;
						IXMLTag lLMIdTag = getTagByNameAndKey(stateTags, "state", "str_LM_id_" + lNr);
						if (lLMIdTag != null) {
							sSpring.setRunTagStatus(getCurrentStatesCaseComponent(), lLMIdTag, AppConstants.statusKeyValue, lLMId,
									true, AppConstants.statusTypeRunGroup, true, false);
							//NOTE script condition should fire, to set learning material present if corresponding state variable is set
						}
						if (lStoreLMIdsInState) {
							if (!StringUtils.isEmpty(lLMIds)) {
								lLMIds += AppConstants.statusCommaReplace;
							}
							lLMIds += lLMId;
						}
					}
					if (lStoreLMIdsInState) {
						IXMLTag lLMIdsStateTag = getTagByNameAndKey(stateTags, "state", lLMIDsStateKey);
						if (lLMIdsStateTag != null) {
							// if not found, do nothing
							sSpring.setRunTagStatus(getCurrentStatesCaseComponent(), lLMIdsStateTag, AppConstants.statusKeyValue, lLMIds,
									true, AppConstants.statusTypeRunGroup, true, false);
						}
					}
					//NOTE learning materials only need to be initialized once, so set state variable
					//NOTE do not do this for legacy situation, so setting can be overruled
					IXMLTag lLMSetStateTag = getTagByNameAndKey(stateTags, "state", lLMIDsSetStateKey);
					if (lLMSetStateTag != null) {
						// if not found, do nothing
						sSpring.setRunTagStatus(getCurrentStatesCaseComponent(), lLMSetStateTag, AppConstants.statusKeyValue, AppConstants.statusValueTrue,
								true, AppConstants.statusTypeRunGroup, true, false);
					}
				} else {
					//NOTE legacy, learning material in references component, set present depending on user id of student; if above code tested, remove this code, because status of learning materials in older runs is already set
					/* START TO BE REMOVED */
					if (getCurrentRubricCode().equals("JP")) {
						String userid = "";
						boolean isPreviewRun = sSpring.getRun().getStatus() == AppConstants.run_status_test;
						if (isPreviewRun) {
							userid = getCurrentRunGroupAccount().getERunGroup().getName();
						} else {
							userid = getCurrentRunGroupAccount().getEAccount().getUserid();
						}
						// following case component contains the different learning materials with
						// present false.
						IECaseComponent pvReferences = sSpring.getCaseComponent("", "PV_references");
						if (userid.length() == "um_pv_001-01T-eo-ce-sa".length() && pvReferences != null) {
							List<IXMLTag> pieceTags = CDesktopComponents.cScript().getNodeTags(pvReferences, "piece");

							// NOTE pieces have no pid, only a title that should be the same even if learning
							// materials differ.
							// so use the index of the learning material to set the correct one

							// first learning material
							String learningMaterialCode1 = userid.substring(17, 19);
							int learningMaterialIndex1 = -1;
							if (learningMaterialCode1.equals("ce")) {
								learningMaterialIndex1 = 0;
							} else if (learningMaterialCode1.equals("cg")) {
								learningMaterialIndex1 = 1;
							} else if (learningMaterialCode1.equals("sa")) {
								learningMaterialIndex1 = 2;
							}
							// make the first correct learning material present
							for (int i = 0; i < 3; i++) {
								IXMLTag pieceTag = pieceTags.get(i);
								sSpring.setRunTagStatus(pvReferences, pieceTag,
										AppConstants.statusKeyPresent, "" + (i == learningMaterialIndex1), true,
										AppConstants.statusTypeRunGroup, true, false);
							}

							// second learning material
							String learningMaterialCode2 = userid.substring(20, 22);
							int learningMaterialIndex2 = -1;
							if (learningMaterialCode2.equals("ce")) {
								learningMaterialIndex2 = 3;
							} else if (learningMaterialCode2.equals("cg")) {
								learningMaterialIndex2 = 4;
							} else if (learningMaterialCode2.equals("sa")) {
								learningMaterialIndex2 = 5;
							}
							// make the second correct learning material present
							for (int i = 3; i < 6; i++) {
								IXMLTag pieceTag = pieceTags.get(i);
								sSpring.setRunTagStatus(pvReferences, pieceTag,
										AppConstants.statusKeyPresent, "" + (i == learningMaterialIndex2), true,
										AppConstants.statusTypeRunGroup, true, false);
							}

						}
					}
					/* END TO BE REMOVED */
				}
			}
			
			/* END RUBRIC SPECIFIC CODE */
		}
	}
	/* END USER SPECIFIC CODE */

	public String getPanelId(String steptype) {
		// every step macro ZUL file contains a div whose id contains the panel string
		String panel = "";
		if (steptype.equals(prepareStepType)) {
			panel = "prepare";
		} else if (steptype.equals(practiceStepType)) {
			panel = "practice";
		} else if (steptype.equals(feedbackStepType)) {
			panel = "feedback";
		} else if (steptype.equals(viewFeedbackStepType)) {
			panel = "viewFeedback";
		} else if (steptype.equals(defineGoalsStepType)) {
			panel = "defineGoals";
		}
		return panel;
	}

	public String setPanel(IXMLTag stepTag) {
		// set panel depending on step
		if (stepTag == null) {
			return "";
		}
		String panel = getPanelId(stepTag.getChildValue("steptype"));
		if (!panel.equals("")) {
			IXMLTag currentCycleTag = null;
			if (stepTag != null && stepTag.getParentTag().getName().equals("cycle")) {
				currentCycleTag = stepTag.getParentTag();
			}
			// set data related to the step
			setCurrentCycleTag(currentCycleTag);
			setCurrentStepTag(stepTag);
			setCurrentStepNumber(getCurrentStepTags().indexOf(stepTag) + 1);
			setCurrentStep(panel);
			// set panel according to step
			setCurrentPanel(panel);
		}
		return panel;
	}

	public void showPanel(String panel) {
		if (!StringUtils.isEmpty(panel)) {
			Map<String, Object> keyValueMap = null;
			// NOTE also non step panels like, e.g., the dashboard need to know current step
			// data to present it in the interface
			if (getCurrentStepTag() != null) {
				keyValueMap = new HashMap<String, Object>();
				keyValueMap.put("stepTag", getCurrentStepTag());
				keyValueMap.put("stepNumber", getCurrentStepNumber());
				keyValueMap.put("cycleNumber", getCurrentCycleNumber());
				keyValueMap.put("step", getCurrentStep());
				keyValueMap.put("resetPanel", true);
			}
			showPanel(panel, keyValueMap);
		} else {
			if (!isSuperTeacher()) {
				// NOTE hide menu button, user has no PV-toolkit role
				Component mainMenuButton = vView.getComponent("mainMenuButton");
				if (mainMenuButton != null) {
					mainMenuButton.setVisible(false);
				}
			}
			// if super teacher the main menu button is present, so the user is able to
			// choose the super teacher menu option
		}
	}

	public boolean showNextStep(IXMLTag stepTag) {
		// if a step is finished by the user the next step is shown
		IXMLTag nextStepTag = getNextStepTag(stepTag);
		if (nextStepTag == null) {
			return false;
		}
		String panel = setPanel(nextStepTag);
		showPanel(panel);
		return true;
	}

	public void resetDashboardPanel() {
		// NOTE shows first component within panel
		// NOTE if components are added in the ZUL file, their ids should be added in
		// the string array, except for pop-ups
		String[] ids = new String[] { "dashboardDiv", "skillWheelLevel2Div", "skillWheelLevel3Div",
				"compareSkillWheelLevel2Div", "compareSkillWheelLevel3Div", "skillWheelRecordingDiv",
				"skillWheelOverviewTipsTopsDiv", "dashboardMessagesDetailsDiv" };
		for (int i = 0; i < ids.length; i++) {
			vView.getComponent(ids[i]).setVisible(i == 0);
		}
	}

	public void resetProgressPanel() {
		// NOTE shows first component within panel
		// NOTE if components are added in the ZUL file, their ids should be added in
		// the string array, except for pop-ups
		String[] ids = new String[] { "progressSkillWheelLevel2Div", "progressSkillWheelLevel3Div",
				"progressCompareSkillWheelLevel2Div", "progressCompareSkillWheelLevel3Div",
				"progressSkillWheelRecordingDiv", "progressSkillWheelOverviewTipsTopsDiv" };
		for (int i = 0; i < ids.length; i++) {
			vView.getComponent(ids[i]).setVisible(i == 0);
		}
	}

	public void resetMyRecordingsPanel() {
		// NOTE show first component within panel
		// NOTE if components are added in the ZUL file, their ids should be added in
		// the string array, except for pop-ups
		String[] ids = new String[] { "myRecordingsRecordingsDiv", "myRecordingsRecordingDiv",
				"myRecordingsPreviousOverviewRubricDiv", "myRecordingsPreviousOverviewRecordingDiv",
				"myRecordingsFeedbackDiv", "myRecordingsOverviewRubricDiv" };
		for (int i = 0; i < ids.length; i++) {
			vView.getComponent(ids[i]).setVisible(i == 0);
		}
	}

	public void resetMyFeedbackPanel() {
		// NOTE show first component within panel
		// NOTE if components are added in the ZUL file, their ids should be added in
		// the string array, except for pop-ups
		String[] ids = new String[] { "myFeedbackRecordingsDiv", "myFeedbackFeedbackDiv", "myFeedbackScoreLevelsDiv",
				"myFeedbackOverviewRubricDiv", "myFeedbackScoreLevelTipsTopsDiv",
				"myFeedbackScoreLevelVideoExamplesDiv" };
		for (int i = 0; i < ids.length; i++) {
			vView.getComponent(ids[i]).setVisible(i == 0);
		}
		CRunPVToolkitMyFeedbackRecordingsDiv div = (CRunPVToolkitMyFeedbackRecordingsDiv) vView
				.getComponent("myFeedbackRecordingsDiv");
		if (div != null) {
			div._initialized = false;
		}
	}

	public void resetFeedbackPanel() {
		CRunPVToolkitFeedbackRecordingsDiv div = (CRunPVToolkitFeedbackRecordingsDiv) vView
				.getComponent("feedbackRecordingsDiv");
		if (div != null) {
			div._initialized = false;
		}
	}

	public void resetTeacherDashboardPanel() {
		// NOTE shows first component within panel
		// NOTE if components are added in the ZUL file, their ids should be added in
		// the string array, except for pop-ups
		String[] ids = new String[] { "teacherDashboardDiv", "teacherDashboardMessagesDetailsDiv",
				"teacherDashboardPeerGroupsDetailsDiv", "teacherDashboardStudentsDetailsDiv",
				"teacherDashboardSkillWheelLevel2Div", "teacherDashboardSkillWheelLevel3Div",
				"teacherDashboardCompareSkillWheelLevel2Div", "teacherDashboardCompareSkillWheelLevel3Div",
				"teacherDashboardSkillWheelRecordingDiv", "teacherDashboardSkillWheelOverviewTipsTopsDiv" };
		for (int i = 0; i < ids.length; i++) {
			vView.getComponent(ids[i]).setVisible(i == 0);
		}
	}

	public void resetTeacherStudentsProgressPanel() {
		// NOTE shows first component within panel
		// NOTE if components are added in the ZUL file, their ids should be added in
		// the string array, except for pop-ups
		String[] ids = new String[] { "teacherStudentsProgressStudentsDetailsDiv",
				"teacherStudentsProgressSkillWheelLevel2Div", "teacherStudentsProgressSkillWheelLevel3Div",
				"teacherStudentsProgressCompareSkillWheelLevel2Div",
				"teacherStudentsProgressCompareSkillWheelLevel3Div", "teacherStudentsProgressSkillWheelRecordingDiv",
				"teacherStudentsProgressSkillWheelOverviewTipsTopsDiv" };
		for (int i = 0; i < ids.length; i++) {
			vView.getComponent(ids[i]).setVisible(i == 0);
		}
	}

	public void resetTeacherPeerGroupsProgressPanel() {
		// NOTE shows first component within panel
		// NOTE if components are added in the ZUL file, their ids should be added in
		// the string array, except for pop-ups
		String[] ids = new String[] { "teacherPeerGroupsProgressPeerGroupsDetailsDiv" };
		for (int i = 0; i < ids.length; i++) {
			vView.getComponent(ids[i]).setVisible(i == 0);
		}
	}

	public void showPanel(String panel, Map<String, Object> keyValueMap) {
		// hide all panels
		List<Component> components = vView.getComponentsByPrefix(getPanelidprefix());
		for (Component component : components) {
			component.setVisible(false);
		}
		if (keyValueMap != null && keyValueMap.containsKey("stepTag")) {
			// if step panel is shown update step label as well
			Div stepDiv = (CRunPVToolkitStepDiv) vView.getComponent(panel + "StepDiv");
			if (stepDiv != null) {
				Events.sendEvent("onUpdate", stepDiv, keyValueMap);
			}
		}

		// NOTE resetPanel=true means that the panel will show its first page, as if you
		// visit the panel for the first time or if another step is chosen or set
		// NOTE resetPanel=false means that the panel will show the page on which the
		// user was working the last time he visited the panel during the current
		// session
		boolean resetPanel = false;
		if (keyValueMap != null && keyValueMap.containsKey("resetPanel")) {
			resetPanel = (boolean) keyValueMap.get("resetPanel");
		}
		if (!gInitializedPanels.contains(panel)) {
			if (!resetPanel)
				resetPanel = true;
			gInitializedPanels.add(panel);
		}

		// NOTE updatePanel=true means that the panel has to be updated
		boolean updatePanel = false;
		if (keyValueMap != null && keyValueMap.containsKey("updatePanel")) {
			updatePanel = (boolean) keyValueMap.get("updatePanel");
		}

		// NOTE if current activity menu option is clicked for the first time
		// (resetPanel is false and updatePanel is true), reset panel, otherwise its
		// content is not rendered for the first time
		if (!resetPanel && updatePanel && !currentActivityIsRendered) {
			resetPanel = true;
			updatePanel = false;
			currentActivityIsRendered = true;
		}

		// NOTE if shown from the main menu the panel is always editable.
		// TODO it could be that a teacher opens a student panel and then it should not
		// be editable.
		boolean isPanelEditable = true;

		if (resetPanel) {
			// NOTE for student
			// NOTE if a (step) panel is reset also reset dashboard meaning the first
			// component of the dashboard is shown
			resetDashboardPanel();
			// NOTE if a (step) panel is reset also reset progress meaning the first
			// component of progress level 2 is shown
			resetProgressPanel();
			// NOTE if a (step) panel is reset also reset my feedback meaning the first
			// component of feedback overview is shown
			resetMyFeedbackPanel();
			resetFeedbackPanel();
			// NOTE if a (step) panel is reset also reset my recordings meaning the first
			// component of recordings overview is shown
			resetMyRecordingsPanel();

			// NOTE for teacher
			// NOTE if a (step) panel is reset also reset dashboard meaning the first
			// component of the dashboard is shown
			resetTeacherDashboardPanel();
			// NOTE if a (step) panel is reset also reset students progress meaning the
			// overview of students is shown
			resetTeacherStudentsProgressPanel();
			// NOTE if a (step) panel is reset also reset peer groups progress meaning the
			// overview of peer groups is shown
			resetTeacherPeerGroupsProgressPanel();
		}
		if (updatePanel) {
			// update define goals components as well, so they always show the last state
			if (panel.equals("defineGoals")) {
				String[] defineGoalsIds = new String[] { "defineGoalsDiv", "defineGoalsOverviewTipsTopsDiv" };
				for (int i = 0; i < defineGoalsIds.length; i++) {
					CRunPVToolkitDefineGoalsDiv div = (CRunPVToolkitDefineGoalsDiv) vView
							.getComponent(defineGoalsIds[i]);
					if (div._initialized) {
						div.update();
					}
				}
			}
		}

		if (panel.equals("dashboard")) {
			// initialize dashboard compartments
			CRunPVToolkitDashboardMessagesDiv divLeft1 = (CRunPVToolkitDashboardMessagesDiv) vView
					.getComponent("dashboardMessagesDiv");
			if (divLeft1 != null) {
				// NOTE always initialize messages so recently added messages are shown
				divLeft1.init(getCurrentRunGroup(), false);
			}
			CRunPVToolkitDashboardPeerGroupsDiv divLeft2 = (CRunPVToolkitDashboardPeerGroupsDiv) vView
					.getComponent("dashboardPeerGroupsDiv");
			if (divLeft2 != null) {
				// NOTE always initialize peergroup so recently changed status is shown
				divLeft2.init(getCurrentRunGroup(), false,
						getCommonPeerGroupTags(getCurrentRunGroup(), null, null, null, true));
			}
			CRunPVToolkitSkillWheelLevel1Div divRight = (CRunPVToolkitSkillWheelLevel1Div) vView
					.getComponent("skillWheelLevel1Div");
			if (divRight != null && divRight.isVisible()) {
				// NOTE only initialize first level of score wheel if it is visible
				divRight.init(getCurrentRunGroup(), false, 0, 0);
			}
		} else if (panel.equals("progress")) {
			// initialize skill wheel, progress shows skill wheel with zoom level 2
			CRunPVToolkitSkillWheelLevel2Div div = (CRunPVToolkitSkillWheelLevel2Div) vView
					.getComponent("progressSkillWheelLevel2Div");
			if (div != null) {
				div.init(getCurrentRunGroup(), false, 0, 0);
			}
		}
		if (panel.equals("teacherDashboard")) {
			// initialize dashboard compartments
			CRunPVToolkitDashboardMessagesDiv divLeft1 = (CRunPVToolkitDashboardMessagesDiv) vView
					.getComponent("teacherDashboardMessagesDiv");
			if (divLeft1 != null) {
				// NOTE always initialize messages so recently added messages are shown
				divLeft1.init(getCurrentRunGroup(), false);
			}
			// initialize common peer group tags. Same data is shown within different
			// dashboard compartments
			List<IXMLTag> commonPeerGroupTags = getCommonPeerGroupTags(getCurrentRunGroup(), null, null, null, true);
			CRunPVToolkitDashboardPeerGroupsDiv divLeft2 = (CRunPVToolkitDashboardPeerGroupsDiv) vView
					.getComponent("teacherDashboardPeerGroupsDiv");
			if (divLeft2 != null) {
				// NOTE always initialize messages so recently added messages are shown
				divLeft2.init(getCurrentRunGroup(), false, commonPeerGroupTags);
			}
			CRunPVToolkitDashboardPeerGroupsDiv divRight1 = (CRunPVToolkitDashboardPeerGroupsDiv) vView
					.getComponent("teacherDashboardMeDiv");
			if (divRight1 != null) {
				divRight1.init(getCurrentRunGroup(), false, commonPeerGroupTags);
			}
			CRunPVToolkitDashboardPeerGroupsDiv divRight2 = (CRunPVToolkitDashboardPeerGroupsDiv) vView
					.getComponent("teacherDashboardStudentsDiv");
			if (divRight2 != null) {
				divRight2.init(getCurrentRunGroup(), false, commonPeerGroupTags);
			}
		} else if (panel.equals("teacherGroupsProgress")) {
			CRunPVToolkitTeacherDashboardPeerGroupsDetailsDiv div = (CRunPVToolkitTeacherDashboardPeerGroupsDetailsDiv) vView
					.getComponent("teacherPeerGroupsProgressPeerGroupsDetailsDiv");
			if (div != null) {
				div.init(getCurrentRunGroup(), false, getCommonPeerGroupTags(getCurrentRunGroup(), null, null, null, true));
			}
		} else if (panel.equals("teacherStudentsProgress")) {
			CRunPVToolkitTeacherDashboardStudentsDetailsDiv div = (CRunPVToolkitTeacherDashboardStudentsDetailsDiv) vView
					.getComponent("teacherStudentsProgressStudentsDetailsDiv");
			if (div != null) {
				div.init(getCurrentRunGroup(), false, getCommonPeerGroupTags(getCurrentRunGroup(), null, null, null, true));
			}
		} else if (panel.equals("notifications")) {
			// initialize my recordings
			CRunPVToolkitDashboardMessagesDetailsDiv div = (CRunPVToolkitDashboardMessagesDetailsDiv) vView
					.getComponent("notificationsMessagesDetailsDiv");
			if (div != null) {
				div.init(getCurrentRunGroup(), isPanelEditable);
			}
		} else if (panel.equals("rubric")) {
			// initialize rubric
			CRunPVToolkitRubricRubricDiv div = (CRunPVToolkitRubricRubricDiv) vView.getComponent("rubricRubricDiv");
			if (div != null) {
				div.init(getCurrentRunGroup(), null, null, false, true);
			}
		} else if (panel.equals("method")) {
			// initialize method
			CRunPVToolkitMethodMethodDiv div = (CRunPVToolkitMethodMethodDiv) vView.getComponent("methodMethodDiv");
			if (div != null) {
				div.init();
			}
		} else if (panel.equals("myRecordings")) {
			// initialize my recordings
			CRunPVToolkitMyRecordingsRecordingsDiv div = (CRunPVToolkitMyRecordingsRecordingsDiv) vView
					.getComponent("myRecordingsRecordingsDiv");
			if (div != null) {
				div.init(getCurrentRunGroup(), isPanelEditable);
			}
		} else if (panel.equals("myFeedback")) {
			// initialize my feedback
			CRunPVToolkitMyFeedbackRecordingsDiv div = (CRunPVToolkitMyFeedbackRecordingsDiv) vView
					.getComponent("myFeedbackRecordingsDiv");
			if (div != null) {
				div.init(getCurrentRunGroup(), resetPanel, false, "showFeedback");
			}
		} else if (panel.equals("myGoals")) {
			// initialize my goals
			CRunPVToolkitMyGoalsDiv div = (CRunPVToolkitMyGoalsDiv) vView.getComponent("myGoalsDiv");
			if (div != null) {
				div.init(getCurrentRunGroup(), isPanelEditable);
			}
		} else if (panel.equals("superTeacher")) {
			// initialize super teacher landing page
			CRunPVToolkitSuperTeacherAuthoringEnvironmentDiv div = (CRunPVToolkitSuperTeacherAuthoringEnvironmentDiv) vView
					.getComponent("superTeacherAuthoringEnvironmentDiv");
			if (div != null) {
				div.init(getCurrentRunGroup(), isPanelEditable);
			}
		}

		else {
			// initialize step panel
			CRunPVToolkitSubStepsDiv div = (CRunPVToolkitSubStepsDiv) vView.getComponent(panel + "SubStepsDiv");
			if (div != null) {
				div.init(getCurrentRunGroup(), resetPanel, isPanelEditable);
			}
		}

		// show panel
		Component lComp = vView.getComponent(getPanelidprefix() + panel);
		if (lComp != null)
			lComp.setVisible(true);
	}

	public IXMLTag getXmlDataPlusStatusRootTag(IERunGroup runGroup, IECaseComponent caseComponent, String statusType) {
		// get data plus status root tag for a specific run group, which may be the
		// current user's run group or another user's run group
		if (!runGroup.getActive()) {
			return null;
		}
		IXMLTag xmlRootTag = null;
		if (runGroup.getRugId() == getCurrentRunGroup().getRugId()) {
			// current user's run group
			if (statusType.equals(AppConstants.statusTypeRunGroup) || statusType.equals(AppConstants.statusTypeRun)) {
				xmlRootTag = sSpring.getXmlDataPlusRunStatusTree(caseComponent, statusType);
			}
		} else {
			// other user's run group
			if (statusType.equals(AppConstants.statusTypeRunGroup)) {
				xmlRootTag = sSpring.getXmlDataPlusRunGroupStatusTree(runGroup.getRugId(), caseComponent);
			} else if (statusType.equals(AppConstants.statusTypeRun)) {
				xmlRootTag = sSpring.getXmlDataPlusRunStatusTree(caseComponent, statusType);
			}
		}
		// NOTE AppConstants.statusTypeRunTeam is not used, because run teams are not
		// used for peer groups. Peer groups are stored as run status and can be
		// authored by a super teacher.
		return xmlRootTag;
	}

	public List<IXMLTag> getPeerRunGroupXmlDataPlusStatusRootTags(List<IERunGroup> runGroups,
			IECaseComponent caseComponent) {
		List<IXMLTag> xmlRootTags = new ArrayList<IXMLTag>();
		if (runGroups.size() == 0) {
			return xmlRootTags;
		}
		// get data plus status root tag for a specific run groups, which may be the
		// current user's run group or another user's run group
		List<Integer> rugIds = new ArrayList<Integer>();
		for (IERunGroup runGroup : runGroups) {
			if (runGroup.getActive()) {
				rugIds.add(runGroup.getRugId());
			}
		}
		if (rugIds.size() == 0) {
			return xmlRootTags;
		}

		IXMLTag defRootTag = sSpring.getXmlDefTree(caseComponent.getEComponent());
		IXMLTag dataRootTag = sSpring.getXmlDataTree(caseComponent);

		List<IERunGroupCaseComponent> runGroupCaseComponents = runGroupCaseComponentManager
				.getAllRunGroupCaseComponentsByRugIdsCacId(rugIds, caseComponent.getCacId());
		for (IERunGroup runGroup : runGroups) {
			//NOTE: could be that student has enrolled, but not yet has been active in PVTool, in that case rungroup exists, but rungroup casecomponent does not yet exist
			boolean lFound = false;
			String xmlStatus = sSpring.getEmptyXml();
			for (IERunGroupCaseComponent runGroupCaseComponent : runGroupCaseComponents) {
				if (runGroupCaseComponent.getRugRugId() == runGroup.getRugId()) {
					lFound = true;
					xmlStatus = runGroupCaseComponent.getXmldata();
					if (StringUtils.isEmpty(xmlStatus)) {
						xmlStatus = sSpring.getEmptyXml();
					}
				}
			}
			IXMLTag statusRootTag = sSpring.getXmlManager().getXmlTree(defRootTag, xmlStatus,
					AppConstants.status_tag);
			// make a copy of data so data is not polluted with status
			dataRootTag = sSpring.getXmlManager().copyTag(dataRootTag, null);
			if (dataRootTag != null) {
				// merge data and status
				Hashtable<String, IXMLTag> dataTagsById = new Hashtable<String, IXMLTag>();
				Hashtable<String, IXMLTag> dataTagsByRefstatusid = new Hashtable<String, IXMLTag>();
				String lLocaleString = sSpring.inRun() && caseComponent.getECase().getMultilingual() ? sSpring.getPropLocaleString() : "";
				sSpring.populateDataPlusStatusTagsContainersAndHandleMultiLanguage(dataRootTag, dataTagsById,
						dataTagsByRefstatusid, lLocaleString);
				IXMLTag dataPlusStatusRootTag = sSpring.getXmlManager().getXmlTree(dataRootTag, statusRootTag,
						dataTagsById, dataTagsByRefstatusid);
				xmlRootTags.add(dataPlusStatusRootTag);
			}
		}

		return xmlRootTags;
	}

	protected IXMLTag getXmlStatusRootTag(IERunGroup runGroup, IECaseComponent caseComponent, String statusType) {
		// get status root tag for a specific run group, which may be the current user's
		// run group or another user's run group
		if (!runGroup.getActive()) {
			return null;
		}
		String key = "" + runGroup.getRugId() + "_" + caseComponent.getCacId() + "_" + statusType;
		// use temporary memory cache for better performance
		if (_tempMemoryCache) {
			if (_tempXmlStatusRootTagCache.containsKey(key)) {
				return _tempXmlStatusRootTagCache.get(key);
			}
		}
		IXMLTag xmlRootTag = null;
		if (runGroup.getRugId() == getCurrentRunGroup().getRugId()) {
			// current user's run group
			xmlRootTag = sSpring.getXmlRunStatusTree(caseComponent, statusType);
		} else {
			// other user's run group
			if (statusType.equals(AppConstants.statusTypeRunGroup)) {
				xmlRootTag = sSpring.getXmlRunGroupStatusTree(runGroup.getRugId(), caseComponent);
			} else if (statusType.equals(AppConstants.statusTypeRun)) {
				xmlRootTag = sSpring.getXmlRunStatusTree(runGroup.getERun().getRunId(), caseComponent);
			}
		}
		if (_tempMemoryCache) {
			_tempXmlStatusRootTagCache.put(key, xmlRootTag);
		}
		// NOTE AppConstants.statusTypeRunTeam is not used, because run teams are not
		// used for peer groups. Peer groups are stored as run status and can be
		// authored by a super teacher.
		return xmlRootTag;
	}

	protected void saveStatusChange(IECaseComponent caseComponent, String statusType) {
		// save status for current user.
		// NOTE status is never saved for another user. However, the other user may be
		// notified and then access the status of this user using the method above
		IXMLTag statusRootTag = sSpring.getXmlRunStatusTree(caseComponent, statusType);
		if (statusType.equals(AppConstants.statusTypeRunGroup)) {
			sSpring.setSaveRunGroupCaseComponentStatusRootTag(getCurrentRunGroup(), caseComponent, statusRootTag);
		} else if (statusType.equals(AppConstants.statusTypeRun)) {
			sSpring.setRunCaseComponentStatusRootTag(sSpring.getRun(), caseComponent, statusRootTag);
		}
		// NOTE AppConstants.statusTypeRunTeam is not used, because run teams are not
		// used for peer groups. Peer groups are stored as run status and can be
		// authored by a super teacher.
	}

	public IXMLTag getStatusChildTag(IXMLTag tag) {
		// NOTE get the status child tag with name "status" and if it not exists add it
		if (tag == null) {
			return null;
		}
		IXMLTag childTag = tag.getChild(AppConstants.statusElement);
		if (childTag == null) {
			childTag = sSpring.getXmlManager().newXMLTag(AppConstants.statusElement, "");
			tag.getChildTags().add(childTag);
		}
		return childTag;
	}

	public String getStatusChildTagAttribute(IXMLTag tag, String attributeKey) {
		return sSpring.unescapeXML(tag.getChildAttribute(AppConstants.statusElement, attributeKey))
				.replace(AppConstants.statusCommaReplace, ",");
	}

	public void setStatusChildTagAttribute(IXMLTag tag, String attributeKey, String attributeValue) {
		tag.setChildAttribute(AppConstants.statusElement, attributeKey,
				sSpring.escapeXML(attributeValue).replace(",", AppConstants.statusCommaReplace));
	}

	protected List<IXMLTag> getRunStatusTags(IXMLTag statusRootTag, IXMLTag dataNodeTag) {
		// looks for a status tag with name given by dataNodeTag and refdataid given by
		// id of dataNodeTag
		// if it does not exist it is added
		if (statusRootTag == null || dataNodeTag == null) {
			return null;
		}
		IXMLTag statusContentTag = statusRootTag.getChild(AppConstants.contentElement);
		if (statusContentTag == null) {
			return null;
		}
		String tagId = dataNodeTag.getAttribute(AppConstants.keyId);
		String tagName = dataNodeTag.getName();
		List<IXMLTag> runStatusTags = new ArrayList<IXMLTag>();
		for (IXMLTag tempRunStatusTag : statusContentTag.getChilds(tagName)) {
			if (tempRunStatusTag.getAttribute(AppConstants.keyRefdataid).equals(tagId)) {
				runStatusTags.add(tempRunStatusTag);
			}
		}
		if (runStatusTags.size() == 0) {
			// create new run status tag
			IXMLTag runStatusTag = sSpring.newRunNodeTag(tagName, tagId, null, null);
			runStatusTag.setAttribute(AppConstants.defKeyType, AppConstants.defValueNode);
			sSpring.getXmlManager().newChildNodeSimple(statusRootTag, runStatusTag, null);
			runStatusTags.add(runStatusTag);
		}
		// NOTE set status tag of data node tag if not set already
		if (dataNodeTag.getStatusTag() == null) {
			// if multiple status tags, set to first one
			dataNodeTag.setStatusTag(runStatusTags.get(0));
		}
		return runStatusTags;
	}

	protected IXMLTag getRunStatusTag(IXMLTag statusRootTag, IXMLTag dataNodeTag) {
		return getRunStatusTags(statusRootTag, dataNodeTag).get(0);
	}

	protected List<IXMLTag> getRunStatusTags(IERunGroup runGroup, IECaseComponent caseComponent, IXMLTag dataNodeTag,
			String statusType) {
		return getRunStatusTags(getXmlStatusRootTag(runGroup, caseComponent, statusType), dataNodeTag);
	}

	protected IXMLTag getRunStatusTag(IERunGroup runGroup, IECaseComponent caseComponent, IXMLTag dataNodeTag,
			String statusType) {
		List<IXMLTag> getRunStatusTags = getRunStatusTags(getXmlStatusRootTag(runGroup, caseComponent, statusType), dataNodeTag);
		return (getRunStatusTags == null ? null : getRunStatusTags.get(0));
	}

	public IXMLTag getRunProjectsStatusChildTag(IERunGroup runGroup, IXMLTag dataNodeTag, String statusType) {
		// NOTE get 'status' child tag of the run status tag, i.e. the tag that is used
		// to store PV-tool data
		IXMLTag runStatusTag = getRunStatusTag(runGroup, getCurrentProjectsCaseComponent(), dataNodeTag, statusType);
		return getStatusChildTag(runStatusTag);
	}

	public IXMLTag getRunMessagesStatusChildTag(IERunGroup runGroup, IXMLTag dataNodeTag, String statusType) {
		// NOTE get 'status' child tag of the run status tag, i.e. the tag that is used
		// to store PV-tool data
		IXMLTag runStatusTag = getRunStatusTag(runGroup, getCurrentMessagesCaseComponent(), dataNodeTag, statusType);
		return getStatusChildTag(runStatusTag);
	}

	public List<IXMLTag> getRunStepTagStatusChildTags(IERunGroup runGroup, String stepType, String statusType) {
		// NOTE get step tags within the 'status child of the run status tag
		List<IXMLTag> runStepTagStatusChildTags = new ArrayList<IXMLTag>();
		// NOTE get step tags within current method tag
		List<IXMLTag> childNodeTags = new ArrayList<IXMLTag>();
		CDesktopComponents.cScript().getNewNodeTagList(getCurrentMethodTag().getChildTags(AppConstants.defValueNode),
				childNodeTags);
		for (IXMLTag childNodeTag : childNodeTags) {
			if (childNodeTag.getName().equals("step")) {
				if (stepType.equals("") || childNodeTag.getChildValue("steptype").equals(stepType)) {
					runStepTagStatusChildTags.add(getRunProjectsStatusChildTag(runGroup, childNodeTag, statusType));
				}
			}
		}
		return runStepTagStatusChildTags;
	}

	protected IXMLTag getNewRunStatusChildTag(IXMLTag runTagStatusChildTag, String tagName, IXMLTag refTag,
			boolean refTagIsStatusTag) {
		// NOTE create status child tag. It is a child tag of runStepTagStatusChildTag
		// (which has name 'status')
		// NOTE See XML definition of step tag for a description of the
		// attributes/values to be set
		IXMLTag tag = sSpring.getXmlManager().newXMLTag(tagName, "");
		tag.setParentTag(runTagStatusChildTag);
		runTagStatusChildTag.getChildTags().add(tag);
		// create an unique identifier for the tag
		tag.setAttribute(tagName + "uuid", UUIDGenerator.getInstance().generateTimeBasedUUID().toString());
		// NOTE always set rga id to current rga id, because status will always be saved
		// for the current rga
		tag.setAttribute(tagName + "rgaid", "" + getCurrentRunGroupAccount().getRgaId());
		// NOTE possibly set reference to other tag, e.g., if feedback is given it is
		// given on another status child tag created during practicing
		if (refTag != null) {
			if (refTagIsStatusTag) {
				// a reference to another status child tag
				String refTagName = refTag.getName();
				String refTagUuidKey = refTagName + "uuid";
				String refTagRgaidKey = refTagName + "rgaid";
				tag.setAttribute(refTagUuidKey, refTag.getAttribute(refTagUuidKey));
				tag.setAttribute(refTagRgaidKey, refTag.getAttribute(refTagRgaidKey));
			} else {
				// a reference to a data tag
				String refTagIdKey = refTag.getName() + "id";
				tag.setAttribute(refTagIdKey, refTag.getAttribute(AppConstants.keyId));
			}
		}

		return tag;
	}

	protected IXMLTag getNewOrExistingRunStatusChildTag(IXMLTag runTagStatusChildTag, String tagName, IXMLTag refTag,
			Map<String, String> refStatusTagAttributes, boolean refTagIsStatusTag, boolean createNew,
			boolean multiple) {
		// NOTE get new or existing status child tag. It is a child tag of
		// runStepTagStatusChildTag (which has name 'status')
		if (runTagStatusChildTag == null) {
			return null;
		}
		if (multiple) {
			if (refTag != null) {
				// NOTE multiple child tags are allowed.
				// there may for instance be multiple feedback tags for a certain step tag, for
				// each student given feedback
				if (refTagIsStatusTag) {
					// a reference to another status child tag, get it by uuid or possibly other
					// status tag attributes
					String refTagUuidKey = refTag.getName() + "uuid";
					String refTagUuid = refTag.getAttribute(refTagUuidKey);
					boolean checkRefStatusTagAttributes = (refStatusTagAttributes != null
							&& refStatusTagAttributes.size() > 0);
					for (IXMLTag tempRefTag : runTagStatusChildTag.getChilds(tagName)) {
						boolean found = tempRefTag.getAttribute(refTagUuidKey).equals(refTagUuid);
						if (checkRefStatusTagAttributes) {
							// not only check for uuid but for other status tag attributes as well.
							IXMLTag tempRefStatusTag = getStatusChildTag(tempRefTag);
							for (Map.Entry<String, String> entry : refStatusTagAttributes.entrySet()) {
								found = found && tempRefStatusTag.getAttribute(entry.getKey()).equals(entry.getValue());
							}
						}
						if (found) {
							return tempRefTag;
						}
					}
				} else {
					// a reference to a data tag, get it by its id
					String refTagIdKey = refTag.getName() + "id";
					String refTagId = refTag.getAttribute(AppConstants.keyId);
					for (IXMLTag tempRefTag : runTagStatusChildTag.getChilds(tagName)) {
						if (tempRefTag.getAttribute(refTagIdKey).equals(refTagId)) {
							return tempRefTag;
						}
					}
				}
			}
		} else {
			// NOTE only one child allowed, get it by tagName, e.g., defining goals is done
			// once
			IXMLTag childTag = runTagStatusChildTag.getChild(tagName);
			if (childTag != null) {
				return childTag;
			}
		}
		if (createNew) {
			// it does not exist, create a new one.
			return getNewRunStatusChildTag(runTagStatusChildTag, tagName, refTag, refTagIsStatusTag);
		} else {
			return null;
		}
	}

	protected IXMLTag getNewOrExistingRunStatusChildTag(IXMLTag runTagStatusChildTag, String tagName,
			IXMLTag refRunStatusTag, boolean refTagIsStatusTag, boolean createNew, boolean multiple) {
		return getNewOrExistingRunStatusChildTag(runTagStatusChildTag, tagName, refRunStatusTag, null,
				refTagIsStatusTag, createNew, multiple);
	}

	/* START DEMO CODE */
	public boolean isDemoMode() {
		// in demo mode it for instance is possible to switch between steps using the
		// main menu
		return !sSpring
				.getCurrentRunComponentStatus(sSpring.getCaseComponent(sSpring.getCase(), "", "PV_demoscript"),
						AppConstants.statusKeyPresent, AppConstants.statusTypeRunGroup)
				.equals(AppConstants.statusValueFalse);
	}
	/* END DEMO CODE */

	public String removePeerInString(String value) {
		// for teacher labels peer has to be removed from strings
		if (value.contains("Peerfeedback")) {
			value = value.replaceAll("Peerfeedback", "Feedback");
		}
		if (value.contains("peerfeedback")) {
			value = value.replaceAll("peerfeedback", "feedback");
		}
		return value;
	}

	public int getNumberOfSkillClusters(IECaseComponent caseComponent, IXMLTag skillTag) {
		// get number of skill cluster tags for skill tag
		int numberOfSkillClusters = 0;
		for (IXMLTag skillclusterTag : skillTag.getChilds("skillcluster")) {
			if (isRunSpecificTagPresent(new CRunPVToolkitCacAndTag(caseComponent, skillclusterTag))) {
				numberOfSkillClusters++;
			}
		}
		return numberOfSkillClusters;
	}

	protected int getNumberOfAccessibleSubSkills(IECaseComponent caseComponent, IXMLTag skillTag,
			boolean checkOnAccessible) {
		// get number of sub skills tags for skill tag
		int numberOfSubSkills = 0;
		for (IXMLTag skillclusterTag : skillTag.getChilds("skillcluster")) {
			if (isRunSpecificTagPresent(new CRunPVToolkitCacAndTag(caseComponent, skillclusterTag))
					&& (!checkOnAccessible
							|| !skillclusterTag.getCurrentStatusAttribute(AppConstants.statusKeyAccessible)
									.equals(AppConstants.statusValueFalse))) {
				for (IXMLTag subskillTag : skillclusterTag.getChilds("subskill")) {
					if (isRunSpecificTagPresent(new CRunPVToolkitCacAndTag(caseComponent, subskillTag))
							&& (!checkOnAccessible
									|| !subskillTag.getCurrentStatusAttribute(AppConstants.statusKeyAccessible)
											.equals(AppConstants.statusValueFalse))) {
						numberOfSubSkills++;
					}
				}
			}
		}
		return numberOfSubSkills;
	}

	public int getNumberOfSubSkills(IECaseComponent caseComponent, IXMLTag skillTag) {
		return getNumberOfAccessibleSubSkills(caseComponent, skillTag, false);
	}

	public int getNumberOfAccessibleSubSkills(IECaseComponent caseComponent, IXMLTag skillTag) {
		return getNumberOfAccessibleSubSkills(caseComponent, skillTag, true);
	}

	public IXMLTag getStepTagByChildTagOfStatusTag(IERunGroup rungroup, IXMLTag childTagOfStatusTag) {
		// first get parent tag with name step within childTagOfStatusTag within status
		IXMLTag stepStatusTag = childTagOfStatusTag.getParentTag();
		while (stepStatusTag != null && !stepStatusTag.getName().equals("step")) {
			stepStatusTag = stepStatusTag.getParentTag();
		}
		if (stepStatusTag == null) {
			return null;
		}
		// then get step tag by using refdataid attribute within statusStepTag
		return sSpring.getCaseComponentXmlDataTagById(getCurrentProjectsCaseComponent(),
				stepStatusTag.getAttribute(AppConstants.keyRefdataid));
	}

	public IXMLTag getLearningActivityChildTag(IXMLTag nodeTag) {
		// learning activities can be defined as node child tags of several tags defined
		// in a projects case component
		if (nodeTag == null) {
			return null;
		}
		// there may be multiple learning activities. The active one is the one with
		// opened is true
		for (IXMLTag learningActivityChildTag : nodeTag.getChilds("learningactivity")) {
			if (learningActivityChildTag.getAttribute(AppConstants.defKeyType).equals(AppConstants.defValueNode)
					&& learningActivityChildTag.getCurrentStatusAttribute(AppConstants.statusKeyOpened)
							.equals(AppConstants.statusValueTrue)) {
				return learningActivityChildTag;
			}
		}
		return null;
	}

	public boolean areAllSubStepsDoneForAllPeers(IERunGroup runGroup, IXMLTag stepTag, List<IXMLTag> subStepTags) {
		//can be used if all peers have to go to a next step at the same time
		String stepType = stepTag.getChildValue("steptype"); 
		if (stepType.equals(practiceStepType)) {
			//NOTE not used yet
			List<IERunGroup> peers = getUsersThatPractice(runGroup);
			if (peers == null || peers.size() == 0)
				return true;
			//loop through all peers to determine if they have done all sub steps
			for (IERunGroup peer : peers) {
				List<Boolean> subStepsDone = null;
				//NOTE not used yet
				CRunPVToolkitCacAndTag practice = getPractice(peer, stepTag);
				if (practice == null || practice.getXmlTag() == null)
					return false;
				List<IXMLTag> practiceTags = practice.getXmlTag().getChilds("practice");
				subStepsDone = getPracticeSubStepsDone(peer, subStepTags, practiceTags);
				//if no substeps done (subStepsDone == null) return true
				if (subStepsDone != null)
					for (Boolean subStepDone : subStepsDone)
						if (!subStepDone)
							return false;
			}
		}
		else if (stepType.equals(feedbackStepType)) {
			//NOTE is used
			//NOTE some peers may be part of other peer groups (for instance teachers); only check player window actor peer group(s) 
			Map<CRunPVToolkitPeerGroup, List<IERunGroup>> lPgsPeers = getPeergroupsUsersThatGiveFeedback(runGroup, stepTag, false, true);
			if (lPgsPeers == null || lPgsPeers.isEmpty()) {
				return true;
			}
			for (Map.Entry<CRunPVToolkitPeerGroup, List<IERunGroup>> lPgPeers : lPgsPeers.entrySet()) {
				CRunPVToolkitPeerGroup lPeerGroup = lPgPeers.getKey();
				List<IERunGroup> peers = lPgPeers.getValue();
				//loop through all peers to determine if they have done all sub steps
				for (IERunGroup peer : peers) {
					List<Boolean> subStepsDone = null;
					//NOTE is used
					List<IXMLTag> sharedPracticeTags = getFeedbackPracticeTags(peer, getPreviousStepTagInCurrentCycle(stepTag, CRunPVToolkit.practiceStepType), false, null, null);
					if (sharedPracticeTags == null)
						return false;
					subStepsDone = getPeergroupFeedbackSubStepsDone(peer, stepTag, subStepTags, sharedPracticeTags, lPeerGroup);
					//if no substeps done (subStepsDone == null) return true
					if (subStepsDone != null)
						for (Boolean subStepDone : subStepsDone)
							if (!subStepDone)
								return false;
				}
	        }
		}
		
		return true;
	}

	/* PREPARE SUBSTEPS DONE */
	public List<Boolean> getPrepareSubStepsDone(IERunGroup actor, IXMLTag feedbackStepTag, List<IXMLTag> subStepTags,
			List<IXMLTag> skillExampleTags) {
		List<Boolean> subStepsDone = new ArrayList<Boolean>();
		for (IXMLTag subStepTag : subStepTags) {
			String subSteptype = subStepTag.getChildValue("substeptype");
			boolean subStepDone = false;
			if (subSteptype.equals(CRunPVToolkit.prepareStudyRubricSubStepType)) {
				// NOTE not possible to check if this sub step is done
				subStepDone = true;
			}
			if (subSteptype.equals(CRunPVToolkit.prepareAnalyzeVideoSubStepType)) {
				// NOTE at least one video has to be analyzed
				for (IXMLTag skillExampleTag : skillExampleTags) {
					subStepDone = subStepDone || videoExampleIsReady(actor, feedbackStepTag, skillExampleTag);
					if (subStepDone) {
						break;
					}
				}
			} else if (subSteptype.equals(CRunPVToolkit.prepareCompareWithExpertsSubStepType)) {
				// NOTE not possible to check if this sub step is done
				subStepDone = true;
			}
			subStepsDone.add(new Boolean(subStepDone));
		}
		return subStepsDone;
	}

	public boolean videoExampleIsReady(IERunGroup actor, IXMLTag feedbackStepTag, IXMLTag skillExampleTag) {
		return getFeedbackStatus(actor, feedbackStepTag, skillExampleTag, false, "ready")
				.equals(AppConstants.statusValueTrue);
	}

	/* PRACTICE SUBSTEPS DONE */
	public List<Boolean> getPracticeSubStepsDone(IERunGroup actor, List<IXMLTag> subStepTags,
			List<IXMLTag> practiceTags) {
		List<Boolean> subStepsDone = new ArrayList<Boolean>();
		for (IXMLTag subStepTag : subStepTags) {
			String subSteptype = subStepTag.getChildValue("substeptype");
			boolean subStepDone = false;
			if (subSteptype.equals(CRunPVToolkit.practicePracticeSubStepType)) {
				// NOTE at least one recording
				subStepDone = practiceTags.size() > 0;
			}
			if (subSteptype.equals(CRunPVToolkit.practiceSelfFeedbackSubStepType)) {
				// NOTE at least one self feedback
				for (IXMLTag practiceTag : practiceTags) {
					subStepDone = subStepDone || practiceHasSelfFeedback(actor, practiceTag);
					if (subStepDone) {
						break;
					}
				}
			} else if (subSteptype.equals(CRunPVToolkit.practiceAskFeedbackSubStepType)) {
				// NOTE at least one recording is shared
				for (IXMLTag practiceTag : practiceTags) {
					subStepDone = subStepDone || practiceIsShared(actor, practiceTag);
					if (subStepDone) {
						break;
					}
				}
			}
			subStepsDone.add(new Boolean(subStepDone));
		}
		return subStepsDone;
	}

	public boolean practiceHasSelfFeedback(IERunGroup actor, IXMLTag practiceTag) {
		return getPracticeStatus(actor, practiceTag, "selffeedback").equals(AppConstants.statusValueTrue);
	}

	public boolean practiceIsShared(IERunGroup actor, IXMLTag practiceTag) {
		return getPracticeStatus(actor, practiceTag, "share").equals(AppConstants.statusValueTrue);
	}

	public boolean practiceIsTeacherFeedbackAsked(IERunGroup pActor, IXMLTag pPracticeTag) {
		return getPracticeStatus(pActor, pPracticeTag, AppConstants.statusKeyAskteacherfeedback).equals(AppConstants.statusValueTrue);
	}

	/* FEEDBACK SUBSTEPS DONE */
	public List<Boolean> getFeedbackSubStepsDone(IERunGroup actor, IXMLTag feedbackStepTag, List<IXMLTag> subStepTags,
			List<IXMLTag> sharedPracticeTags) {
		List<IERunGroup> usersThatPractice = getUsersThatPractice(actor);
		List<Boolean> subStepsDone = new ArrayList<Boolean>();
		for (IXMLTag subStepTag : subStepTags) {
			String subSteptype = subStepTag.getChildValue("substeptype");
			boolean subStepDone = usersThatPractice.size() == sharedPracticeTags.size();
			if (subStepDone) {
				if (subSteptype.equals(CRunPVToolkit.feedbackGiveFeedbackSubStepType)) {
					// NOTE all feedbacks should be ready or shared.
					// NOTE because one feedback state is used to store either busy, ready or
					// shared, shared should be checked as well to determine if sub step is ready
					// if a user chooses to send the feedback right away after feedback is given,
					// the state is set to ready and immediately afterwards to shared. And this
					// before sub steps page is shown.
					for (IXMLTag practiceTag : sharedPracticeTags) {
						subStepDone = subStepDone && (feedbackIsReady(actor, feedbackStepTag, practiceTag)
								|| feedbackIsShared(actor, feedbackStepTag, practiceTag));
					}
				} else if (subSteptype.equals(CRunPVToolkit.feedbackSendFeedbackSubStepType)) {
					// NOTE all feedbacks should be shared
					for (IXMLTag practiceTag : sharedPracticeTags) {
						subStepDone = subStepDone && feedbackIsShared(actor, feedbackStepTag, practiceTag);
					}
				}
			}
			subStepsDone.add(new Boolean(subStepDone));
		}
		return subStepsDone;
	}

	/* FEEDBACK SUBSTEPS DONE */
	public List<Boolean> getPeergroupFeedbackSubStepsDone(IERunGroup actor, IXMLTag feedbackStepTag, List<IXMLTag> subStepTags, List<IXMLTag> sharedPracticeTags, CRunPVToolkitPeerGroup aPeerGroup) {
		//NOTE only check given feedback to practicing users of aPeerGroup
		List<IERunGroup> usersThatPractice = getUsersThatPractice(actor);
		//NOTE items of usersThatPractice can be part of different peer groups, for example if actor has teacher role
		List<CRunPVToolkitPeerGroupMember> lPgMembers = aPeerGroup.getPeerGroupMembers();
		List<IERunGroup> lCheckUsTP = new ArrayList<IERunGroup>();
		List<IXMLTag> lCheckSPTs = new ArrayList<IXMLTag>();
		boolean lIsTeacher = getPeerGroupMemberRole(aPeerGroup.getPeerGroupTag(), actor).equals(peerGroupTeacherRole);
		for (CRunPVToolkitPeerGroupMember lPgMember : lPgMembers) {
			if (lPgMember.getRole().equals(peerGroupStudentRole)) {
				int lRgaId = lPgMember.getRunGroupAccount().getRgaId();
				IERunGroup lRug = lPgMember.getRunGroupAccount().getERunGroup();
				int lRugId = lRug.getRugId();
				if (lRugId != actor.getRugId()) {
					boolean lTestIfAskedForTeacherFeedback = false;
					boolean lExcludeTeacherFB = false;
					if (lIsTeacher) {
						lExcludeTeacherFB = excludeTeachersFeedback(lRug, feedbackStepTag);
						if (!lExcludeTeacherFB && (mayAskTeacherFeedbackCount(lRug) > 0))
							lTestIfAskedForTeacherFeedback = true;
					}
					if (!lExcludeTeacherFB) {
						boolean lExcludeRunGroup = false;
						for (IXMLTag lSPT : sharedPracticeTags)
							if (lSPT.getAttribute(practiceRgaId).equals("" + lRgaId)) {
								if (lTestIfAskedForTeacherFeedback) {
									//NOTE if student must ask for teacher feedback but hasn't done this in this cycle, teacher does not need to give feedback
									if (!practiceIsTeacherFeedbackAsked(lRug, lSPT))
										lExcludeRunGroup = true;
								}
								if (!lExcludeRunGroup)
									lCheckSPTs.add(lSPT);
							}
						if (!lExcludeRunGroup)
							for (IERunGroup lUTP : usersThatPractice)
								if (lUTP.getRugId() == lRugId)
									lCheckUsTP.add(lUTP);
					}
				}
			}
		}
		List<Boolean> subStepsDone = new ArrayList<Boolean>();
		boolean subStepDone = lCheckUsTP.size() == lCheckSPTs.size();
		if (subStepDone) {
			//NOTE all feedbacks should be ready or shared.
			//NOTE because one feedback state is used to store either busy, ready or shared, shared should be checked as well to determine if sub step is ready
			//if a user chooses to send the feedback right away after feedback is given, the state is set to ready and immediately afterwards to shared. And this before sub steps page is shown.
			for (IXMLTag practiceTag : lCheckSPTs)
				subStepDone = subStepDone && feedbackIsShared(actor, feedbackStepTag, practiceTag);
		}
		subStepsDone.add(new Boolean(subStepDone));
		return subStepsDone;
	}

	public boolean feedbackIsReady(IERunGroup actor, IXMLTag feedbackStepTag, IXMLTag practiceTag) {
		return getFeedbackStatus(actor, feedbackStepTag, practiceTag, true, "ready")
				.equals(AppConstants.statusValueTrue);
	}

	public boolean feedbackIsShared(IERunGroup actor, IXMLTag feedbackStepTag, IXMLTag practiceTag) {
		return getFeedbackStatus(actor, feedbackStepTag, practiceTag, true, "share")
				.equals(AppConstants.statusValueTrue);
	}

	/* VIEW FEEDBACK SUBSTEPS DONE */
	public List<Boolean> getViewFeedbackSubStepsDone(List<IXMLTag> subStepTags) {
		List<Boolean> subStepsDone = new ArrayList<Boolean>();
		for (IXMLTag subStepTag : subStepTags) {
			String subSteptype = subStepTag.getChildValue("substeptype");
			boolean subStepDone = false;
			if (subSteptype.equals(CRunPVToolkit.viewFeedbackViewFeedbackSubStepType)) {
				// NOTE cannot check if sub step is done
				subStepDone = true;
			}
			subStepsDone.add(new Boolean(subStepDone));
		}
		return subStepsDone;
	}

	/* DEFINE GOALS SUBSTEPS DONE */
	public List<Boolean> getDefineGoalsSubStepsDone(List<IXMLTag> subStepTags) {
		List<Boolean> subStepsDone = new ArrayList<Boolean>();
		for (IXMLTag subStepTag : subStepTags) {
			String subSteptype = subStepTag.getChildValue("substeptype");
			boolean subStepDone = false;
			if (subSteptype.equals(CRunPVToolkit.defineGoalsSetGoalsSubStepType)) {
				// NOTE cannot check if sub step is done
				subStepDone = true;
			}
			subStepsDone.add(new Boolean(subStepDone));
		}
		return subStepsDone;
	}

	@Override
	public void handleStatusChange(STriggeredReference aTriggeredReference) {
		// NOTE just like other Emergo components, the PV Toolkit is notified in case of
		// status change due to case script.
		Events.echoEvent("onHandleStatusChange", this, aTriggeredReference);
	}

	public void onHandleStatusChange(Event aEvent) {
		STriggeredReference triggeredReference = (STriggeredReference) aEvent.getData();
		if (triggeredReference == null || triggeredReference.getDataTag() == null) {
			return;
		}
		if (triggeredReference.getDataTag().getName().equals("message")
				&& triggeredReference.getStatusKey().equals(AppConstants.statusKeySent)
				&& triggeredReference.getStatusValue().equals(AppConstants.statusValueTrue)
				&& triggeredReference.getSetFromScript()) {
			// NOTE if PV message is sent by SYSTEM (by a scheduler class active in the
			// background, so not within the Emergo player), getSetFromScript() returns true
			handleMessage();
		}
	}

	public void showStatusVideo(IXMLTag runStatusTag, String idPrefix, String classPrefix) {
		// a status video is a video uploaded or recorded by a user
		String url = getStatusUrl(runStatusTag);
		int webcamRecordingLength = getWebcamRecordingLength(runStatusTag);
		showVideoUrl(url, webcamRecordingLength, idPrefix, classPrefix);
	}

	public void showDataVideo(IXMLTag dataTag, String aChildTagName, String idPrefix, String classPrefix) {
		// a data video is a video entered by a case developer
		String url = getDataUrl(dataTag, aChildTagName);
		int webcamRecordingLength = -1;
		showVideoUrl(url, webcamRecordingLength, idPrefix, classPrefix);
	}

	public void clearVideo(String idPrefix) {
		Component videoDiv = vView.getComponent(idPrefix + "VideoDiv");
		videoDiv.getChildren().clear();
	}

	public int getWebcamRecordingLength(IXMLTag runStatusTag) {
		// the web cam recording length is determined by a timer during recording, and
		// stored in progress
		int webcamRecordingLength = -1;
		String webcamRecordingLengthStr = getStatusChildTagAttribute(runStatusTag, "webcamRecordingLength");
		if (webcamRecordingLengthStr != null && !webcamRecordingLengthStr.equals("")) {
			webcamRecordingLength = Integer.parseInt(webcamRecordingLengthStr);
		}
		return webcamRecordingLength;
	}

	public String getStatusUrl(IXMLTag runStatusTag) {
		// a status url is determined by a blob id stored in progress with blobtype
		// 'database'
		return sSpring.getSBlobHelper().getUrl(runStatusTag.getValue(), AppConstants.blobtypeDatabase);
	}

	public String getDataUrl(IXMLTag dataTag, String aChildTagName) {
		// a data url is determined by a child tag of type blob of a data tag
		return sSpring.getSBlobHelper().getUrl(dataTag, aChildTagName);
	}

	public void showVideoUrl(String url, int webcamRecordingLength, String idPrefix, String classPrefix) {
		// NOTE setting src of video element resulted in Javascript that notifies video
		// scroll bar not working properly.
		// It only worked for the first video, but not if another video was chosen.
		// Therefore now the video element is removed and added every time and this
		// works fine.

		// remove possible previous video
		clearVideo(idPrefix);

		// Within PV-tool videos may be played on different panels in a video div that
		// is identified by idPrefix.
		Component videoDiv = vView.getComponent(idPrefix + "VideoDiv");

		// add video element
		// an include is used to be able to set it src to v_run_flash_fr
		Include videoVideoViewer = new Include();
		videoDiv.appendChild(videoVideoViewer);
		// include attributes are set to be used within v_run_flash_fr
		if (!vView.isAbsoluteUrl(url)) {
			url = vView.getAbsoluteUrl(url);
		}
		videoVideoViewer.setAttribute("url", url);
		videoVideoViewer.setAttribute("playerName", idPrefix + "VideoVideo");
		videoVideoViewer.setAttribute("showControls", "true");
		videoVideoViewer.setAttribute("playerAutostart", "true");
		videoVideoViewer.setAttribute("playerStretching", "fit");
		videoVideoViewer.setAttribute("playerStretching", "fit");
		if (classPrefix != null) {
			// TODO use static final String constants. Or determine from css file. Or read
			// from properties file.
			// classprefix gives context and determines size of video to be played
			if (classPrefix.equals("videoRecording") || classPrefix.equals("skillWheelRecording")) {
				videoVideoViewer.setAttribute("video_size_width", "840px");
				videoVideoViewer.setAttribute("video_size_height", "470px");
			} else if (classPrefix.equals("videoExamples")) {
				videoVideoViewer.setAttribute("video_size_width", "690px");
				videoVideoViewer.setAttribute("video_size_height", "385px");
			} else if (classPrefix.equals("practiceNewRecording")) {
				videoVideoViewer.setAttribute("video_size_width", "1080px");
				videoVideoViewer.setAttribute("video_size_height", "440px");
			} else if (classPrefix.equals("feedback")) {
				videoVideoViewer.setAttribute("video_size_width", "565px");
				videoVideoViewer.setAttribute("video_size_height", "520px");
			} else if (classPrefix.equals("superTeacher")) {
				videoVideoViewer.setAttribute("video_size_width", "1080px");
				videoVideoViewer.setAttribute("video_size_height", "440px");
			}
		}
		videoVideoViewer.setAttribute("runComponentId", getId());
		// play video
		videoVideoViewer.setSrc(VView.v_run_flash_fr);

		// add zoom video control
		addZoomVideoControl(idPrefix, classPrefix);
	}

	public void showVideoMedia(Video media, int webcamRecordingLength, String idPrefix, String classPrefix) {
		// NOTE setting src of video element resulted in Javascript that notifies video
		// scroll bar not working properly.
		// It only worked for the first video, but not if another video was chosen.
		// Therefore now the video element is removed and added every time and this
		// works fine.

		// remove possible previous video
		clearVideo(idPrefix);

		// Within PV-tool videos may be played on different panels in a video div that
		// is identified by idPrefix.
		Component videoDiv = vView.getComponent(idPrefix + "VideoDiv");

		// add video element, a zK video component is used to play the media
		CRunPVToolkitVideoVideo videoVideo = new CRunPVToolkitVideoVideo();
		videoDiv.appendChild(videoVideo);
		videoVideo.setId(idPrefix + "VideoVideo");
		videoVideo.init(idPrefix, webcamRecordingLength);
		if (media != null) {
			// play video
			videoVideo.setContent(media);
		}
		if (!StringUtils.isEmpty(classPrefix)) {
			videoVideo.setClass(classPrefix + "VideoVideo");
		}

		// add zoom video control
		addZoomVideoControl(idPrefix, classPrefix);
	}

	protected void addZoomVideoControl(String idPrefix, String classPrefix) {
		// adds zoom video control if it does not exist yet
		// Within PV-tool videos may be controlled on different panels using a zoom
		// video div that is identified by idPrefix.
		CRunPVToolkitZoomVideoDiv zoomVideoDiv = (CRunPVToolkitZoomVideoDiv) vView
				.getComponent(idPrefix + "ZoomVideoDiv");
		if (zoomVideoDiv == null) {
			// if it does not exist, add it to a video interaction div defined in a ZUL file
			// and initialize it
			Component videoInteractionDiv = vView.getComponent(idPrefix + "VideoInteractionDiv");
			if (videoInteractionDiv != null) {
				zoomVideoDiv = new CRunPVToolkitZoomVideoDiv(videoInteractionDiv, new String[] { "id", "class" },
						new Object[] { idPrefix + "ZoomVideoDiv", classPrefix + "ZoomVideoDiv" });
				String divClassPrefix = "videoRecordingZoomVideo";
				zoomVideoDiv.init(idPrefix, divClassPrefix);
			}
		}
	}

	protected Label showFileLink(Component parent, String url, Media media, String id, String classStr, String value) {
		// shows link and handles click on it
		Label link = new CRunPVToolkitDefLabel(parent, new String[] { "id", "class", "value" },
				new Object[] { id, classStr, value });
		if (!StringUtils.isEmpty(url)) {
			link.addEventListener("onClick", new EventListener<Event>() {
				@Override
				public void onEvent(Event event) {
					Clients.evalJavaScript(vView.getJavascriptWindowOpenFullscreen(url));
				}
			});
			return link;
		}
		return null;
	}

	protected Label showDataOrStatusLink(IXMLTag tag, String childTagName, Component parent, String id, String classStr,
			String value, boolean dataLink) {
		// shows link to content entered by case developer or content entered by student
		if (tag == null) {
			return null;
		}
		String url = "";
		if (dataLink) {
			// get url from content entered by case developer
			url = getDataUrl(tag, childTagName);
		} else {
			// get url from content entered by student
			url = getStatusUrl(tag);
			CContentFilesFilter.addOnlyDownloadResource(url);
		}
		if (StringUtils.isEmpty(url)) {
			return null;
		}
		if (!vView.isAbsoluteUrl(url)) {
			url = vView.getAbsoluteUrl(url);
		}
		return showFileLink(parent, url, null, id, classStr, value);
	}

	public Label showDataLink(IXMLTag dataTag, String childTagName, Component parent, String id, String classStr,
			String value) {
		// shows a link to a blob entered by a case developer. Links are used on several
		// panels.
		return showDataOrStatusLink(dataTag, childTagName, parent, id, classStr, value, true);
	}

	public Label showStatusLink(IXMLTag runStatusTag, Component parent, String id, String classStr, String value) {
		// shows a link to a blob entered by a student. Links are used on several
		// panels.
		return showDataOrStatusLink(runStatusTag, "", parent, id, classStr, value, false);
	}

	public Rows appendGrid(Component parent, String gridId,
			// style is used to set position and size of the grid
			String gridStyle,
			// if columns have to be shown
			boolean[] showColumn,
			// columns widths are set as percentages, e.g., '60%', counting up to 100%
			String[] columnWidths,
			// if columns may be sorted
			boolean[] sortColumn,
			// if columns may be checked
			boolean[] checkColumn,
			// prefix for the grid's column header labels
			String labelKeyPrefix,
			// postfix for the header labels per column
			String[] labelKeyPostfixes) {
		// grids are used on multiple panels to show overviews of data. it is
		// configurable which columns may be sorted or checked. The latter meaning all
		// checkboxes in the column can be checked/unchecked with one click.
		// this method creates the grid and its column headers, but adds no data,
		// because this can be very specific depending on the kind of data to be shown.

		Grid grid = new CDefGrid();
		parent.appendChild(grid);
		if (!StringUtils.isEmpty(gridId)) {
			grid.setId(gridId);
		}
		grid.setStyle(gridStyle);

		Columns columns = new Columns();
		grid.appendChild(columns);

		// append grid column headers
		for (int i = 0; i < labelKeyPostfixes.length; i++) {
			if (showColumn == null || showColumn[i]) {
				Column column = new CDefColumn();
				columns.appendChild(column);
				column.setWidth(columnWidths[i]);
				if (sortColumn[i]) {
					// if column may be sorted add sorting mechanism
					column.setSortAscending(new CRunPVToolkitRowComparator(i, true));
					column.setSortDescending(new CRunPVToolkitRowComparator(i, false));
				}
				if (!labelKeyPostfixes[i].equals("")) {
					String label = vView.getLabel(labelKeyPrefix + labelKeyPostfixes[i]);
					if (label.equals("")) {
						// NOTE if not found try general label
						label = vView.getCLabel(generalLabelKeyPrefix + labelKeyPostfixes[i]);
					}
					if (checkColumn != null && checkColumn[i]) {
						// if column may be checked add checkbox
						CRunPVToolkitDefCheckbox checkbox = new CRunPVToolkitDefCheckbox(column, null, null);
						addGridColumnOnCheckEventListener(checkbox, gridId, i);
					}
					new CRunPVToolkitDefLabel(column, new String[] { "value" }, new Object[] { label });
				}
			}
		}

		Rows rows = new Rows();
		grid.appendChild(rows);

		return rows;
	}

	protected void addGridColumnOnCheckEventListener(Component component, String gridId, int columnNumber) {
		// checks or unchecks all checkboxes in a column
		component.addEventListener("onCheck", new EventListener<CheckEvent>() {
			@Override
			public void onEvent(CheckEvent event) {
				Grid grid = (Grid) vView.getComponent(gridId);
				if (grid != null) {
					for (Object object : grid.getRows().getChildren()) {
						if (object instanceof Row) {
							Row row = (Row) object;
							// NOTE the grid cell may only contain a checkbox, so no multiple ZK components
							if (columnNumber < row.getChildren().size()
									&& row.getChildren().get(columnNumber) instanceof Checkbox) {
								((Checkbox) row.getChildren().get(columnNumber)).setChecked(event.isChecked());
							}
						}
					}
				}
			}
		});
	}

	public Row getGridRow(String gridId, IXMLTag tag) {
		// get the row by grid id and tag. One grid row normally shows content of one
		// tag. When rendering rows, this tag is set as attribute of a row.
		Grid grid = (Grid) vView.getComponent(gridId);
		if (grid == null) {
			return null;
		}
		for (Object object : grid.getRows().getChildren()) {
			if (object instanceof Row) {
				Row row = (Row) object;
				if (row.getAttribute("tag") == tag) {
					return row;
				}
			}
		}
		return null;
	}

	public void deleteGridRow(String gridId, IXMLTag tag) {
		// delete row by grid id and tag. When rendering rows, tag is set as attribute
		// of a row.
		Row row = getGridRow(gridId, tag);
		if (row != null) {
			row.detach();
		}
	}

	public Listbox appendListbox(Component parent, String listboxId,
			// style is used to set position and size of the listbox
			String listboxStyle,
			// if columns have to be shown
			boolean[] showColumn,
			// columns widths are set as percentages, e.g., '60%', counting up to 100%
			String[] columnWidths,
			// if columns may be sorted
			boolean[] sortColumn,
			// if columns may be checked
			boolean[] checkColumn,
			// prefix for the listbox's column header labels
			String labelKeyPrefix,
			// postfix for the header labels per column
			String[] labelKeyPostfixes) {
		// listboxes are used on multiple panels to show overviews of data. it is
		// configurable which columns may be sorted or checked. The latter meaning all
		// checkboxes in the column can be checked/unchecked with one click.
		// this method creates the listbox and its column headers, but adds no data,
		// because this can be very specific depending on the kind of data to be shown.

		Listbox listbox = new CDefListbox();
		parent.appendChild(listbox);
		if (!StringUtils.isEmpty(listboxId)) {
			listbox.setId(listboxId);
		}
		listbox.setStyle(listboxStyle);

		Listhead listhead = new CDefListhead();
		listbox.appendChild(listhead);

		// append listbox column headers
		for (int i = 0; i < labelKeyPostfixes.length; i++) {
			if (showColumn == null || showColumn[i]) {
				Listheader listheader = new CDefListheader();
				listhead.appendChild(listheader);
				listheader.setWidth(columnWidths[i]);
				if (sortColumn[i]) {
					// if column may be sorted add sorting mechanism
					listheader.setSortAscending(new CRunPVToolkitListitemComparator(i, true));
					listheader.setSortDescending(new CRunPVToolkitListitemComparator(i, false));
				}
				if (!labelKeyPostfixes[i].equals("")) {
					String label = vView.getLabel(labelKeyPrefix + labelKeyPostfixes[i]);
					if (label.equals("")) {
						// NOTE if not found try general label
						label = vView.getCLabel(generalLabelKeyPrefix + labelKeyPostfixes[i]);
					}
					if (checkColumn != null && checkColumn[i]) {
						// if column may be checked add checkbox
						CRunPVToolkitDefCheckbox checkbox = new CRunPVToolkitDefCheckbox(listheader, null, null);
						addListboxColumnOnCheckEventListener(checkbox, listboxId, i);
					}
					new CRunPVToolkitDefLabel(listheader, new String[] { "value" }, new Object[] { label });
				}
			}
		}

		return listbox;
	}

	protected void addListboxColumnOnCheckEventListener(Component component, String listboxId, int columnNumber) {
		// checks or unchecks all checkboxes in a column
		component.addEventListener("onCheck", new EventListener<CheckEvent>() {
			@Override
			public void onEvent(CheckEvent event) {
				Listbox listbox = (Listbox) vView.getComponent(listboxId);
				if (listbox != null) {
					for (Listitem listitem : listbox.getItems()) {
						// NOTE the list cell may only contain a checkbox, so no multiple ZK components
						if (columnNumber < listitem.getChildren().size()
								&& listitem.getChildren().get(columnNumber) instanceof Checkbox) {
							((Checkbox) listitem.getChildren().get(columnNumber)).setChecked(event.isChecked());
						}
					}
				}
			}
		});
	}

	public Listitem getListboxListitem(String listboxId, IXMLTag tag) {
		// get the listitem by listbox id and tag. One listitem normally shows content
		// of one tag. When rendering listitems, this tag is set as attribute of a
		// listitem.
		Listbox listbox = (Listbox) vView.getComponent(listboxId);
		if (listbox == null) {
			return null;
		}
		for (Listitem listitem : listbox.getItems()) {
			if (listitem.getAttribute("tag") == tag) {
				return listitem;
			}
		}
		return null;
	}

	public void deleteListboxListitem(String listboxId, IXMLTag tag) {
		// delete listitem by listbox id and tag. When rendering listitems, tag is set
		// as attribute of a listitem.
		Listitem listitem = getListboxListitem(listboxId, tag);
		if (listitem != null) {
			listitem.detach();
		}
	}

	public Div getSpecificDiv(Component parent) {
		// if you have a screen that partly is static and partly is dynamic, meaning it
		// may be partly updated, then you can use a specific div as the parent of the
		// dynamic part.
		// so if you want to update the screen, you just update the dynamic part by
		// calling this method (that will remove the div and its children and create a
		// new one) and add the children again (with new data).
		String specificDivId = parent.getId() + "SpecificDiv";
		Div specificDiv = (Div) vView.getComponent(specificDivId);
		if (specificDiv != null) {
			specificDiv.detach();
		}
		specificDiv = new CDefDiv();
		specificDiv.setId(specificDivId);
		if (parent.getChildren().size() == 0) {
			parent.appendChild(specificDiv);

		} else {
			parent.insertBefore(specificDiv, parent.getChildren().get(0));
		}
		return specificDiv;
	}

	/* START EXPERIMENTAL CODE */
	public boolean isUsedInExperiment() {
		return isUsedInExperiment(getCurrentRunGroup());
	}
	
	public boolean isUsedInExperiment(IERunGroup pRunGroup) {
		// the behavior of the PV-tool may be different in an experimental setting, like
		// for research purposes. It is set using a state.
		
		IECaseComponent lCaseComponent = sSpring.getCaseComponent(sSpring.getCase(), "", gCaseComponentNameStatesExperiment);
		if (lCaseComponent == null) {
			return false;
		}
		return !getRunGroupStatusValue(pRunGroup, lCaseComponent, "state", "experiment", AppConstants.statusKeyValue).equals(AppConstants.statusValueFalse);
	}
	
	protected boolean isLastAssessmentFinished(IERunGroup pRunGroup) {
		//NOTE if last assessment finished, show feedback to student if student belongs to experimental delayed teacher feedback group
		IECaseComponent lExpCComponent = sSpring.getCaseComponent(sSpring.getCase(), "", gCaseComponentNameStatesExperiment);
		String[] lLastAssCCNames = getRunGroupStatusValue(pRunGroup, lExpCComponent, "state", "str_final_assessment_compname", AppConstants.statusKeyValue).replace(AppConstants.statusCommaReplace, ",").split(",");
		String[] lLastAssNames = getRunGroupStatusValue(pRunGroup, lExpCComponent, "state", "str_final_assessment_assname", AppConstants.statusKeyValue).replace(AppConstants.statusCommaReplace, ",").split(",");
		boolean lNotPresent = true;
		for (String lLastAssCCName : lLastAssCCNames) {
			IECaseComponent lAssCaseComponent = sSpring.getCaseComponent(sSpring.getCase(), "", lLastAssCCName);
			if (lAssCaseComponent != null) {
				lNotPresent = false;
				for (String lLastAssName : lLastAssNames) {
					// if assessment is finished, include feedback
					if (getRunGroupStatusValue(pRunGroup, lAssCaseComponent, "assessment", lLastAssName, AppConstants.statusKeyFinished).equals(AppConstants.statusValueTrue))
						return true;
				}
			}
		}
		// if assessment case component not present, always include feedback
		return lNotPresent;
	}

	public boolean excludeStudentAssistantsFeedback(IERunGroup pRunGroup) {
		// this method is added for experimental reasons. Student assistants feedback
		// (may be regular teacher as well) is not shown to a student until a certain
		// step in the method is reached
		// NOTE if no experiment include student assistant feedback
		if (!isUsedInExperiment(pRunGroup)) {
			return false;
		}
		// no student assistant in peer group of user
		if (!hasPeersWithStudentAssistantRole(pRunGroup)) {
			return false;
		}
		// check if state variable is set (initial run state or set rungroup state):
		IECaseComponent lExpCComponent = sSpring.getCaseComponent(sSpring.getCase(), "", gCaseComponentNameStatesExperiment);
		if (lExpCComponent != null) {
			String lExcludeSAFb = getRunGroupStatusValue(pRunGroup, lExpCComponent, "state", "str_exclude_studentassistantfb", AppConstants.statusKeyValue);
			if ("false".equalsIgnoreCase(lExcludeSAFb))
				return false;
		}
		return !isLastAssessmentFinished(pRunGroup);
	}

	protected boolean isTeacherFbDelayed(IERunGroup pRunGroup) {
		if (!isUsedInExperiment(pRunGroup)) {
			return false;
		}
/*
		if (pStepTag != null && pStepTag.getParentTag().getName().equals("cycle")) {
			int lCycleNr = getCycleNumber(pStepTag.getParentTag());
			if (participatesInVariantTFBR1(pRunGroup) || participatesInVariantTFBR2(pRunGroup)) {
				if ((lCycleNr == 4) && isLastAssessmentFinished(pRunGroup))
					return false;
				return true;
			}
		}
	*/	
		IECaseComponent lExpCComponent = sSpring.getCaseComponent(sSpring.getCase(), "", gCaseComponentNameStatesExperiment);
		if (lExpCComponent == null) {
			return false;
		}
		String lUserIdTeacherFbDelayedMask = getRunGroupStatusValue(pRunGroup, lExpCComponent, "state", "str_teacherfb_delayed_mask", AppConstants.statusKeyValue);
		if (StringUtils.isEmpty(lUserIdTeacherFbDelayedMask))
			return false;
		String lUserid = "";
		boolean lIsPreviewRun = sSpring.getRun().getStatus() == AppConstants.run_status_test;
		if (lIsPreviewRun) {
			lUserid = pRunGroup.getName();
		} else {
			List<IERunGroupAccount> lRunGroupAccounts = getRunGroupAccounts(pRunGroup);
			if (lRunGroupAccounts.isEmpty()) {
				return false;
			}
			lUserid = lRunGroupAccounts.get(0).getEAccount().getUserid();
		}
		return lUserid.matches(lUserIdTeacherFbDelayedMask);
	}
	/* END EXPERIMENTAL CODE */

	public boolean excludeTeachersFeedback(IERunGroup pRunGroup, IXMLTag pStepTag) {
		// Teachers feedback is not shown to a student if student can ask for feedback but hasn't done that in the current cycle.
		// NOTE if experiment, check if feedback should be delayed until student has finished final assessment.
		// Depends on experimental condition, coded in student's user id or in account's extra data JSON array.
		// if peer group of user does not contain a teacher, no need to exclude.
		if (!hasPeersWithTeacherRole(pRunGroup)) {
			return false;
		}
		int lStuMayAskFbCount = mayAskTeacherFeedbackCount(pRunGroup);
		if (lStuMayAskFbCount >= 0) {

			IXMLTag lPracticeTag = getPracticeTag(pStepTag);
			if (lPracticeTag != null) {
				CRunPVToolkitCacAndTag lRGPractice = getPractice(pRunGroup, lPracticeTag);
				if (!(lRGPractice == null || lRGPractice.getXmlTag() == null)) {
					List<IXMLTag> lRGPracticeTags = lRGPractice.getXmlTag().getChilds("practice");
					for (IXMLTag lRGPracticeTag : lRGPracticeTags) {
						if (getPracticeStatus(pRunGroup, lRGPracticeTag, "share").equals(AppConstants.statusValueTrue)) {
							if (!practiceIsTeacherFeedbackAsked(pRunGroup, lRGPracticeTag))
								return true;
						}
					}
				}
			}
		}
		if (!isUsedInExperiment(pRunGroup)) {
			return false;
		}
		/* START EXPERIMENTAL CODE */
		if (participatesInVariantTFBR1(pRunGroup) || participatesInVariantTFBR2(pRunGroup)) {
			if (pStepTag != null && pStepTag.getParentTag().getName().equals("cycle")) {
				int lCycleNr = getCycleNumber(pStepTag.getParentTag());
				if ((participatesInVariantTFBR1(pRunGroup) && (lCycleNr == 1)) ||
					(participatesInVariantTFBR2(pRunGroup) && (lCycleNr == 2)) ||
					((lCycleNr == 4) && isLastAssessmentFinished(pRunGroup)))
					return false;
				return true;
			}
		}
		
		if (!isTeacherFbDelayed(pRunGroup))
			return false;
		
		return !isLastAssessmentFinished(pRunGroup);
		/* END EXPERIMENTAL CODE */
	}
	
	public int getStepOrderNumber(String pStepType) {
		List<String> lStepTypes = Arrays.asList(prepareStepType, practiceStepType, feedbackStepType, viewFeedbackStepType, defineGoalsStepType);
		return lStepTypes.indexOf(pStepType) + 1;
	}

	/* START EXPERIMENTAL CODE */
	public void onShowAssessment(Event event) {
		// this method is added for experimental reasons. Students have to do an
		// assessment if they have finished certain steps.
		// NOTE only show assessment if state experiment is true
		if (!isUsedInExperiment()) {
			return;
		}
		// NOTE starting assessments is handled by setting state values to true in this
		// case component
		IECaseComponent caseComponent = sSpring.getCaseComponent(sSpring.getCase(), "", gCaseComponentNameStatesExperiment);
		if (caseComponent == null) {
			return;
		}
		// event data contains context of assessment
		String[] data = (String[]) event.getData();
		String cycleNumber = data[0];
		String stepNumber = data[1];
		String type = data[2];
		// state tag key related to starting the assessment
		String stateTagKey = "start_" + gExpAssBaseCompNameKey + cycleNumber + "_step" + stepNumber + "_" + type;
		// get state tag using state tag key
		List<IXMLTag> stateTags = CDesktopComponents.cScript().getNodeTags(caseComponent);
		IXMLTag stateTag = getTagByNameAndKey(stateTags, "state", stateTagKey);
		// NOTE if stateTag not found or not present no assessment is started. So
		// existence of stateTag triggers assessment.
		if (stateTag == null) {
			// if not found, do nothing
			return;
		}
		// set value of state tag to true. It will trigger script to start the
		// assessment and to present it to the student until all questions are answered.
		sSpring.setRunTagStatus(caseComponent, stateTag, AppConstants.statusKeyValue, AppConstants.statusValueTrue,
				true, AppConstants.statusTypeRunGroup, true, false);
	}
	/* END EXPERIMENTAL CODE */

	/* PREPARE */

//NOTE prepare is always related to the first prepare step, so only status of first prepare step is used. In following prepare steps (in next cycles) a student sees what he has done in the first step and may adjust it. 
//NOTE data stored for the prepare step are scores and tips/tops given for a number of skill examples, so a student gives 'feedback' on a skill example therefore the method name below contains 'PrepareFeedback'.
//data is stored as progress of the prepare step tag and refers to a skill example tag. For scoring and storing the same methods are used as for giving feedback, see further.

	public IXMLTag getPrepareFeedbackTag(IERunGroup runGroup, IXMLTag skillExampleTag) {
		// NOTE always use progress of first feedback step, because progress of prepare
		// steps is shared over cycles
		// returns a tag in the student's progress that contains the scores and
		// tips/tops given
		return getFeedbackNewOrExistingFeedbackTag(getRunProjectsStatusChildTag(runGroup,
				getFirstStepTag(CRunPVToolkit.feedbackStepType), AppConstants.statusTypeRunGroup), skillExampleTag,
				false, false);
	}

	/* PRACTICE */

//NOTE practice is always related to the current step tag, which has step type 'practicestep', and related to and stored for the current run group given by getCurrentRunGroup()
//Every practice gets an attribute 'practiceuuid' containing a UUID to uniquely identify the practice.
//NOTE Practices are shown on the 'My recordings' panel as well. Not shared practices may be edited and deleted on the 'My recordings' panel.

	public CRunPVToolkitCacAndTag getPractice(IERunGroup runGroup, IXMLTag practiceStepTag) {
		// returns a tag in the student's progress that contains the practices done
		// during the practiceStepTag
		return new CRunPVToolkitCacAndTag(getCurrentProjectsCaseComponent(),
				getRunProjectsStatusChildTag(runGroup, practiceStepTag, AppConstants.statusTypeRunGroup));
	}

	public void savePractice() {
		// stores changes in practices in database for current step, steps are part of
		// projects case component
		saveStatusChange(getCurrentProjectsCaseComponent(), AppConstants.statusTypeRunGroup);
	}

	protected IXMLTag getPracticeNewPracticeTag(IXMLTag runStepTagStatusChildTag) {
		// because a practice is a video, it is stored as blob
		IXMLTag tag = getNewOrExistingRunStatusChildTag(runStepTagStatusChildTag, "practice", null, true, true, true);
		tag.setAttribute(AppConstants.defKeyType, "blob");
		tag.setAttribute("blobtype", AppConstants.blobtypeIntUrl);
		return tag;
	}

	protected IXMLTag getPracticeNewPracticePieceTag(IXMLTag practiceTagStatusChildTag) {
		// a practice may have additional pieces, like, e.g., a 'Pleitnotitie', stored
		// as blob tags as well
		IXMLTag tag = sSpring.getXmlManager().newXMLTag("piece", "");
		tag.setParentTag(practiceTagStatusChildTag);
		practiceTagStatusChildTag.getChildTags().add(tag);
		tag.setAttribute(AppConstants.defKeyType, "blob");
		tag.setAttribute("blobtype", AppConstants.blobtypeIntUrl);
		return tag;
	}

	protected IXMLTag getPracticeNewFeedbackRatingTag(IXMLTag practiceTagStatusChildTag) {
		// a practice may have additional feedback ratings
		IXMLTag tag = sSpring.getXmlManager().newXMLTag(CRunPVToolkit._feedbackRatingKeyStr, "");
		tag.setParentTag(practiceTagStatusChildTag);
		practiceTagStatusChildTag.getChildTags().add(tag);
		return tag;
	}

	public boolean addPractice(IERunGroup runGroup, IXMLTag practiceStepTag, String name, String blobId,
			int webcamRecordingLength) {
		// creates new practice tag and sets attributes to store practice data
		IXMLTag tag = getPracticeNewPracticeTag(
				getRunProjectsStatusChildTag(runGroup, practiceStepTag, AppConstants.statusTypeRunGroup));
		if (tag == null) {
			return false;
		}

		// NOTE see XML definition of step tag for a description of the
		// attributes/values to be set
		tag.setValue(blobId);
		IXMLTag statusChildTag = getStatusChildTag(tag);
		name = AntiSamyHelper.cleanup(name);
		statusChildTag.setAttribute("name", sSpring.escapeXML(name).replace(",", AppConstants.statusCommaReplace));
		statusChildTag.setAttribute("recordingdate", CDefHelper.getDateStrAsDMY(new Date()));
		statusChildTag.setAttribute("webcamRecordingLength", "" + webcamRecordingLength);
		// NOTE initially the practice is not shared, so this attribute is not set

		savePractice();

		return true;
	}

	public boolean updatePractice(IXMLTag practiceTag, String name) {
		// update name of practice
		// NOTE see XML definition of step tag for a description of the
		// attributes/values to be set
		IXMLTag statusChildTag = getStatusChildTag(practiceTag);
		name = AntiSamyHelper.cleanup(name);
		statusChildTag.setAttribute("name", sSpring.escapeXML(name).replace(",", AppConstants.statusCommaReplace));

		savePractice();

		return true;
	}

	public void copyVideoBlobToWowza(String recordingBlobId, String fileName) {
		// The purpose of this method is to copy an uploaded or recorded file to the
		// Wowza server.
		// TODO temporarily deactivate until video can be converted to right format for
		// Wowza server
		/*
		 * SSpring sSpring = sSpring; FtpHelper ftpHelper = new
		 * FtpHelper(); String relativePath = sSpring.getPropEmergoServerId() + "/" +
		 * sSpring.getCase().getCasId() + "/" +
		 * VView.getInitParameter("emergo.blob.path") + recordingBlobId + "/"; String
		 * subPath = PropsValues.STREAMING_SERVER_FTP_RUNVIDEOS_SUBPATH + relativePath;
		 * File blobFile = sSpring.getBlobFile(recordingBlobId); if
		 * (ftpHelper.ftpTransFileCopyBlob(blobFile.getAbsolutePath(), subPath,
		 * fileName)) { FileHelper fileHelper = new FileHelper(); String newBlobFileName
		 * = relativePath + fileName; IBlobManager bean =
		 * (IBlobManager)sSpring.getBean("blobManager"); IEBlob blob =
		 * bean.getBlob(Integer.parseInt(recordingBlobId));
		 * blob.setName(newBlobFileName); blob.setFilename(newBlobFileName);
		 * bean.saveBlob(blob); fileHelper.deleteFile(blobFile.getAbsolutePath()); }
		 */
	}

	public void deleteVideoBlobFromWowza(String blobFileName) {
		// The purpose of this method is to delete a video file on the Wowza server.
		// TODO temporarily deactivate until video can be converted to right format for
		// Wowza server
		/*
		 * String relativePath = blobFileName.substring(0, blobFileName.lastIndexOf("/")
		 * + 1); String fileName = blobFileName.substring(blobFileName.lastIndexOf("/")
		 * + 1); FtpHelper ftpHelper = new FtpHelper(); String subPath =
		 * PropsValues.STREAMING_SERVER_FTP_RUNVIDEOS_SUBPATH + relativePath;
		 * ftpHelper.ftpTransFileDeleteBlob(subPath, fileName);
		 */
	}

	public void deleteVideoBlob(String blobId) {
		// Because blobs created within the PV-tool are not stored in a regular way (as
		// part of status of node tags), they have to be specifically deleted using this
		// method.
		// Otherwise the blob files will remain on the (Wowza) server.
		IEBlob blob = sSpring.getSBlobHelper().getBlob(blobId);
		if (blob == null) {
			return;
		}
		String blobFileName = blob.getFilename();
		if (blobFileName.contains("/")) {
			// if stored on Wowza file name contains path
			deleteVideoBlobFromWowza(blobFileName);
		}
		sSpring.getSBlobHelper().deleteBlob(blobId);
	}

	public boolean deletePractice(IXMLTag practiceTag) {
		// NOTE delete associated blob
		deleteVideoBlob(practiceTag.getValue());
		// NOTE delete associated pieces
		for (IXMLTag pieceTag : getStatusChildTag(practiceTag).getChilds("piece")) {
			// NOTE pieces are not uploaded to the Wowza server so don't have to removed
			// over there
			sSpring.getSBlobHelper().deleteBlob(pieceTag.getValue());
		}
		practiceTag.getParentTag().getChildTags().remove(practiceTag);

		savePractice();

		return true;
	}

	public boolean getSeeSelf() {
		// see self if making webcam recording. Default true.
		boolean value = getCurrentMethodTag().getCurrentStatusAttribute("seeself").equals(AppConstants.statusValueTrue);
		if (getCurrentCycleTag() != null) {
			// NOTE value may be overwritten in cycle
			value = value
					|| getCurrentCycleTag().getCurrentStatusAttribute("seeself").equals(AppConstants.statusValueTrue);
		}
		return value;
	}

	public boolean getMayChangeSeeSelf() {
		// may user change see self. Default false.
		boolean value = getCurrentMethodTag().getCurrentStatusAttribute("maychangeseeself")
				.equals(AppConstants.statusValueTrue);
		if (getCurrentCycleTag() != null) {
			// NOTE value may be overwritten in cycle
			value = value || getCurrentCycleTag().getCurrentStatusAttribute("maychangeseeself")
					.equals(AppConstants.statusValueTrue);
		}
		return value;
	}

	public boolean addPracticePiece(IERunGroup runGroup, IXMLTag practiceStepTag, IXMLTag practiceTag, String name,
			String blobId) {
		// adds a piece tag as child to a practice tag. Practices may have related
		// pieces that are uploaded by a user
		if (practiceTag == null) {
			return false;
		}

		IXMLTag statusChildTag = getStatusChildTag(practiceTag);
		IXMLTag pieceTag = getPracticeNewPracticePieceTag(statusChildTag);
		// NOTE see XML definition of step tag for a description of the
		// attributes/values to be set
		pieceTag.setValue(blobId);
		statusChildTag = getStatusChildTag(pieceTag);
		statusChildTag.setAttribute("name", sSpring.escapeXML(name).replace(",", AppConstants.statusCommaReplace));

		savePractice();

		return true;
	}

	public IXMLTag addPracticeFeedbackRating(IXMLTag practiceTag, String pRgaId, String pLevel) {
		// adds a feedback rating tag as child to a practice tag.
		if (practiceTag == null) {
			return null;
		}

		IXMLTag statusChildTag = getStatusChildTag(practiceTag);
		IXMLTag feedbackRatingTag = getPracticeNewFeedbackRatingTag(statusChildTag);
		// NOTE see XML definition of step tag for a description of the
		// attributes/values to be set
		statusChildTag = getStatusChildTag(feedbackRatingTag);
		statusChildTag.setAttribute(_feedbackRatingRgaIdStr, pRgaId);
		statusChildTag.setAttribute(_feedbackRatingLevelStr, pLevel);

		savePractice();

		return feedbackRatingTag;
	}

	public String getPracticeStatus(IERunGroup runGroup, IXMLTag practiceTag, String key) {
		// practice tag may have different states
		if (practiceTag == null) {
			return "";
		}
		return getStatusChildTagAttribute(practiceTag, key);
	}

	public void setPracticeStatus(IERunGroup runGroup, IXMLTag practiceTag, String key, String value) {
		// practice tag may have different states
		if (practiceTag == null) {
			return;
		}
		setStatusChildTagAttribute(practiceTag, key, value);
	}

	public void setPracticeSelfFeedback(IERunGroup runGroup, IXMLTag practiceTag) {
		// set if student has given self feedback on practice
		if (practiceTag == null) {
			return;
		}
		setPracticeStatus(runGroup, practiceTag, "selffeedback", AppConstants.statusValueTrue);
		savePractice();
	}

	public int mayAskTeacherFeedbackCount(IERunGroup pRunGroup) {
		// get number of times student may ask teacher for feedback
		// if not set, return -1: student can never ask for feedback
		//		- if progress to 'view feedback' is only permitted if all peers have given feedback: teacher must give feedback to every student in peer group
		// if 0, student can never ask for feedback, teacher doesn't need to give feedback for students to be able to view feedback
		// if > 0: student can ask for feedback
		//		- if progress to 'view feedback' is only permitted if all peers have given feedback: teacher must give feedback to student(s) that asked for feedback

		int lCount = -1;

		IECaseComponent lProjComponent = getCurrentProjectsCaseComponent();
		String lAskCountStr = getRunGroupStatusValue(pRunGroup, lProjComponent, "method", getCurrentMethodTag().getChildValue(getCurrentMethodTag().getDefAttribute(AppConstants.defKeyKey)), AppConstants.statusKeyMayaskteacherfeedbackcount);
		if (!lAskCountStr.equals("")) {
			try {
				lCount = Integer.parseInt(lAskCountStr);
			} catch (NumberFormatException e) {
			}
		}
		if ((lCount < 0) || (lCount >= getMaxCycleNumber())) {
			// if student must always ask for feedback, don't show check box
			lCount = -1;
		}
		
		return lCount;
	}

	public int hasAskedTeacherFeedbackCount(IERunGroup pRunGroup) {
		// get number of times student has asked teacher for feedback
		// if not set, return 0
		IECaseComponent lProjComponent = getCurrentProjectsCaseComponent();
		String lAskedCountStr = getRunGroupStatusValue(pRunGroup, lProjComponent, "method", getCurrentMethodTag().getChildValue(getCurrentMethodTag().getDefAttribute(AppConstants.defKeyKey)), AppConstants.statusKeyTeacherfeedbackaskedcount);
		int lCount = 0;
		if (!lAskedCountStr.equals("")) {
			try {
				lCount = Integer.parseInt(lAskedCountStr);
			} catch (NumberFormatException e) {
			}
		}
		if (lCount < 0) {
			lCount = 0;
		}
		return lCount;
	}

	public void incAskedTeacherFeedbackCount(IERunGroup pRunGroup) {
		IECaseComponent lProjComponent = getCurrentProjectsCaseComponent();
		String lAskedCountStr = getRunGroupStatusValue(pRunGroup, lProjComponent, "method", getCurrentMethodTag().getChildValue(getCurrentMethodTag().getDefAttribute(AppConstants.defKeyKey)), AppConstants.statusKeyTeacherfeedbackaskedcount);
		int lCount = 0;
		if (!lAskedCountStr.equals("")) {
			try {
				lCount = Integer.parseInt(lAskedCountStr);
			} catch (NumberFormatException e) {
			}
		}
		if (lCount < 0) {
			lCount = 0;
		}
		lCount++;
		List<String> errorMessages = new ArrayList<String>();
		sSpring.setCurrentRunTagStatus(lProjComponent, "method", getCurrentMethodTag().getChildValue(getCurrentMethodTag().getDefAttribute(AppConstants.defKeyKey)), AppConstants.statusKeyTeacherfeedbackaskedcount, "" + lCount, AppConstants.statusTypeRunGroup, true, errorMessages);
		//setStatusChildTagAttribute(getCurrentMethodTag(), AppConstants.statusKeyTeacherfeedbackaskedcount, "" + lCount);
	}

	public void setPracticeShared(IERunGroup runGroup, IXMLTag practiceTag, boolean pTeacherFbAsked) {
		// set if student asks feedback on practice
		if (practiceTag == null) {
			return;
		}
		setPracticeStatus(runGroup, practiceTag, "share", AppConstants.statusValueTrue);
		setPracticeStatus(runGroup, practiceTag, "sharedate", CDefHelper.getDateStrAsDMY(new Date()));
		setPracticeStatus(runGroup, practiceTag, "sharetime", CDefHelper.getDateStrAsHMS(new Date()));
		setPracticeStatus(runGroup, practiceTag, AppConstants.statusKeyAskteacherfeedback, pTeacherFbAsked ? AppConstants.statusValueTrue : AppConstants.statusValueFalse);
		if (pTeacherFbAsked)
			incAskedTeacherFeedbackCount(runGroup);
		savePractice();

		// TODO if files are streamed from Wowza server code has to be adjusted
		// NOTE add _shared to file name to be able to identify which recording is
		// shared on file level, in case of multiple recordings
		IBlobManager bean = (IBlobManager) sSpring.getBean("blobManager");
		IEBlob blob = bean.getBlob(Integer.parseInt(practiceTag.getValue()));
		File blobFile = sSpring.getSBlobHelper().getBlobFile(practiceTag.getValue());
		if (blob != null && blobFile != null) {
			String fileName = blob.getFilename();
			if (fileName.contains(".") && !fileName.contains("_shared")) {
				// update file
				FileHelper fileHelper = new FileHelper();
				String blobFileName = blobFile.getAbsolutePath();
				String newBlobFileName = blobFileName.substring(0, blobFileName.lastIndexOf(".")) + "_shared"
						+ blobFileName.substring(blobFileName.lastIndexOf("."));
				fileHelper.renameFile(blobFileName, newBlobFileName);
				// update blob
				String newFileName = fileName.substring(0, fileName.lastIndexOf(".")) + "_shared"
						+ fileName.substring(fileName.lastIndexOf("."));
				blob.setFilename(newFileName);
				blob.setName(newFileName);
				bean.updateBlob(blob);
			}
		}

		if (isUsedInExperiment(runGroup)) {
			// NOTE experimental condition: show possible assessment for step type practice
			String currentStepType = getCurrentStepType();
			int currentCycleNumber = getCurrentCycleNumber();
			int lCurrentStepNumber = getStepOrderNumber(currentStepType);
			String lAssType = getExperimentAssessmentType(currentCycleNumber, lCurrentStepNumber, runGroup);
			if (gExpAssNamesNotInStates.equals(lAssType)) {
				//NOTE legacy; if assessment component names are stored in states the provision of assessments is handled for all steps in CRunPVToolkitSubStepsDiv.setStepFinished 
				if (currentStepType.equals(CRunPVToolkit.practiceStepType)) {
					if (currentCycleNumber == 1 || currentCycleNumber == 3) {
						// only assessments in cycle 1 and 3
						Events.echoEvent("onShowAssessment", this,
								new String[] { "" + currentCycleNumber, "" + lCurrentStepNumber, "N" });
					}
				}
			}
		}
	}

	/* FEEDBACK AND SELF FEEDBACK */

//NOTE (self) feedback is always related to the current step tag. For feedback it has step type 'feedbackstep' and for self feedback it has steptype 'practicestep'.
//Feedback is also related to and stored for the current run group given by getCurrentRunGroup()
//Every feedback gets an attribute 'feedbackuuid' containing a UUID.
//Feedback is given on a shared practice of another student within the current peer group. This relation is stored as attribute 'practiceuuid' within the feedback
//Self feedback is given on a shared or not shared practice of the current student. However, data storage is the same.

	public void saveFeedback() {
		// stores changes in feedbacks in database for current step, steps are part of
		// projects case component
		saveStatusChange(getCurrentProjectsCaseComponent(), AppConstants.statusTypeRunGroup);
	}

	public List<IERunGroup> getPeerRungroups(IERunGroup runGroup, boolean selfFeedback,
			List<String> otherRolesToFilterOn, List<Integer> otherRgaIdsToFilterOn, boolean pIncludeAllPeerGroups) {
		// get peer run groups for run group, possibly filtered on roles and/or peers
		// (if list parameters are not null)
		if (selfFeedback) {
			List<IERunGroup> peerRunGroups = new ArrayList<IERunGroup>();
			// NOTE if self feedback then peer is self
			peerRunGroups.add(runGroup);
			return peerRunGroups;
		}

		// TODO a teacher setting may be that less than (n - 1) students will give feedback.
		// Then feedback givers have to be randomly assigned
		if (pIncludeAllPeerGroups) {
			return getAllPeerGroupsRunGroups(runGroup, null, otherRolesToFilterOn, otherRgaIdsToFilterOn);
		}
		return getPeerRunGroups(runGroup, null, otherRolesToFilterOn, otherRgaIdsToFilterOn);
	}

	public List<IERunGroupAccount> getPeerRungroupAccounts(IERunGroup runGroup, boolean selfFeedback,
			List<String> otherRolesToFilterOn, List<Integer> otherRgaIdsToFilterOn) {
		// get peer run group accounts for run group, possibly filtered on roles and/or
		// peers (if list parameters are not null)
		List<IERunGroup> peerRunGroups = getPeerRungroups(runGroup, selfFeedback, otherRolesToFilterOn,
				otherRgaIdsToFilterOn, true);
		return getRunGroupAccounts(peerRunGroups);
	}

	public List<IERunGroup> getUsersThatPractice(IERunGroup runGroup) {
		// get users that practice for run group (so are his peers)
		List<String> rolesToFilterOn = new ArrayList<String>();
		// only students practice, so filter on student role
		rolesToFilterOn.add(peerGroupStudentRole);
		return getPeerRungroups(runGroup, false, rolesToFilterOn, null, false);
	}
	
	protected List<String> getRolesThatGiveFeedback(IERunGroup runGroup, IXMLTag pStepTag, boolean includeStudentAssistants, boolean pExcludeHiddenTeacherFeedback) {
		// get user roles that give feedback for run group (so are his peers)
		List<String> rolesToFilterOn = new ArrayList<String>();
		// add roles to filter on, i.e., all roles that give feedback
		rolesToFilterOn.add(peerGroupStudentRole);
		/* START EXPERIMENTAL CODE */
		if (!isUsedInExperiment(runGroup)) {
			//NOTE only add teacher if student is not allowed to ask for feedback or has asked for feedback
			if (!(pExcludeHiddenTeacherFeedback && excludeTeachersFeedback(runGroup, pStepTag)))
				rolesToFilterOn.add(peerGroupTeacherRole);
			rolesToFilterOn.add(peerGroupStudentAssistantRole);
		} else {
			boolean lAddTeacher = true;
			int lCurrentCycleNr = getCurrentCycleNumber();
			if (pExcludeHiddenTeacherFeedback) {
				if ((participatesInVariantR1R3 && (lCurrentCycleNr == 2 || lCurrentCycleNr == 4)) ||
					(participatesInVariantR2R4 && (lCurrentCycleNr == 1 || lCurrentCycleNr == 3))) {
					lAddTeacher = false;
				} else {
					if (!(participatesInVariantR1R3 || participatesInVariantR2R4) && excludeTeachersFeedback(runGroup, pStepTag)) {
						lAddTeacher = false;
					}
				}
			}
			if (lAddTeacher)
				rolesToFilterOn.add(peerGroupTeacherRole);
			
			if (includeStudentAssistants || !excludeStudentAssistantsFeedback(runGroup)) {
				// excludeStudentAssistantsFeedback is an experimental condition
				rolesToFilterOn.add(peerGroupStudentAssistantRole);
			}
		}
		/* END EXPERIMENTAL CODE */
		rolesToFilterOn.add(peerGroupPeerStudentRole);
		return rolesToFilterOn;
	}

	protected List<String> getRolesThatReceiveFeedbackRating() {
		//NOTE teachers and student assistants excluded
		List<String> rolesToFilterOn = new ArrayList<String>();
		rolesToFilterOn.add(peerGroupStudentRole);
		rolesToFilterOn.add(peerGroupPeerStudentRole);
		return rolesToFilterOn;
	}
	
	public List<IERunGroup> getUsersThatReceiveFeedbackRating(IERunGroup runGroup) {
		// get users that give feedback on run group practice (so are his peers); teachers excluded
		return getPeerRungroups(runGroup, false, getRolesThatReceiveFeedbackRating(), null, false);
	}
	
	public List<IERunGroup> getUsersThatGiveFeedback(IERunGroup runGroup, IXMLTag pStepTag, boolean includeStudentAssistants, boolean pCheckForHiddenFeedback) {
		return getPeerRungroups(runGroup, false, getRolesThatGiveFeedback(runGroup, pStepTag, includeStudentAssistants, pCheckForHiddenFeedback), null, false);
	}

	public Map<CRunPVToolkitPeerGroup, List<IERunGroup>> getPeergroupsUsersThatGiveFeedback(IERunGroup runGroup, IXMLTag pStepTag, boolean includeStudentAssistants, boolean pCheckForHiddenFeedback) {
		return getPeergroupsPeerRunGroups(runGroup, getRolesThatGiveFeedback(runGroup, pStepTag, includeStudentAssistants, pCheckForHiddenFeedback));
	}
	
	public boolean getTeacherMayUseScoresOfPreviousCycle() {
		// may teacher scores and tips/tops for current cycle be filled with values
		// given in previous cycle for the same student.
		return getCurrentMethodTag().getCurrentStatusAttribute("teachermayusescoresofpreviouscycle")
				.equals(AppConstants.statusValueTrue);
	}

	public String getFeedbackLevelOfScores(IXMLTag methodTag, String childName) {
		// apart from scoring on skill cluster or sub skill level, skill as a whole may
		// be scored. Default is true.
		String childValue = methodTag.getChildValue(childName);
		return childValue.equals("") ? defaultFeedbacklevelofscores : childValue;
	}

	public String getFeedbackLevelOfScores(IXMLTag methodTag, IXMLTag cycleTag) {
		// feedback scores may be given on skill, skill cluster or sub skill level. The
		// latter is default.
		// NOTE interface is not prepared for scoring on skill cluster level. And skill
		// level is always scored, even if configuration is on sub skill level
		String childName = "feedbacklevelofscores";
		String level = getFeedbackLevelOfScores(methodTag, childName);
		if (cycleTag != null) {
			String cycleLevel = cycleTag.getChildValue(childName);
			level = cycleLevel.equals("") ? level : cycleLevel;
		}
		return level;
	}

	public String getFeedbackLevelOfTipsAndTops(IXMLTag methodTag, String childName) {
		// apart from scoring on skill cluster or sub skill level, skill as a whole may
		// be scored. Default is true.
		String childValue = methodTag.getChildValue(childName);
		return childValue.equals("") ? defaultFeedbackleveloftipsandtops : childValue;
	}

	public String getFeedbackLevelOfTipsAndTops(IXMLTag methodTag, IXMLTag cycleTag) {
		// feedback tips and tops may be given on skill, skill cluster or sub skill
		// level. Skill cluster is default.
		// NOTE interface is not prepared for giving tips or tops on skill or sub skill level.
		String childName = "feedbackleveloftipsandtops";
		String level = getFeedbackLevelOfTipsAndTops(methodTag, childName);
		if (cycleTag != null) {
			String cycleLevel = cycleTag.getChildValue(childName);
			level = cycleLevel.equals("") ? level : cycleLevel;
		}
		return level;
	}

	public boolean getMayScoreSkill(IXMLTag methodTag, String attKey) {
		// apart from scoring on skill cluster or sub skill level, skill as a whole may
		// be scored. Default is true.
		String attValue = methodTag.getCurrentStatusAttribute(attKey);
		return attValue.equals("") ? defaultMayscoreskill : Boolean.parseBoolean(attValue);
	}

	public boolean getMayScoreSkill(IXMLTag methodTag, IXMLTag cycleTag) {
		// apart from scoring on skill cluster or sub skill level, skill as a whole may
		// be scored. Default is true.
		String attKey = "mayscoreskill";
		boolean value = getMayScoreSkill(methodTag, attKey);
		if (cycleTag != null) {
			// NOTE value may be overwritten in cycle
			String attValue = cycleTag.getCurrentStatusAttribute(attKey);
			value = value || (attValue.equals("") ? value : Boolean.parseBoolean(attValue));
		}
		return value;
	}

	public boolean getMayTipsAndTopsSkill(IXMLTag methodTag, String attKey) {
		// apart from giving tips/tops on skill cluster or sub skill level, skill as a
		// whole may be given tips/tops. Default is false.
		String attValue = methodTag.getCurrentStatusAttribute(attKey);
		return attValue.equals("") ? defaultMaytipsandtopsskill : Boolean.parseBoolean(attValue);
	}

	public boolean getMayTipsAndTopsSkill(IXMLTag methodTag, IXMLTag cycleTag) {
		// apart from giving tips/tops on skill cluster or sub skill level, skill as a
		// whole may be given tips/tops. Default is false.
		String attKey = "maytipsandtopsskill";
		boolean value = getMayTipsAndTopsSkill(methodTag, attKey);
		if (cycleTag != null) {
			// NOTE value may be overwritten in cycle
			String attValue = cycleTag.getCurrentStatusAttribute(attKey);
			value = value || (attValue.equals("") ? value : Boolean.parseBoolean(attValue));
		}
		return value;
	}

	public List<CRunPVToolkitFeedbackPractice> getFeedbackPractices(IERunGroup runGroup, IXMLTag practiceStepTag,
			boolean selfFeedback, List<String> otherRolesToFilterOn, List<Integer> otherRgaIdsToFilterOn, boolean pIncludeAllPeergroups) {
		// get feedback and practice data to be used in feedback step for run group (a
		// feedback giver). Possibly filter on step tag, roles or peers (if parameters are not null)
		List<IXMLTag> practiceStepTags = null;
		if (practiceStepTag == null) {
			// include all practice step tags
			practiceStepTags = this.getStepTags(practiceStepType);
		} else {
			practiceStepTags = new ArrayList<IXMLTag>();
			practiceStepTags.add(practiceStepTag);
		}
		return getCycleFeedbackPractices(runGroup, practiceStepTags, selfFeedback, otherRolesToFilterOn, otherRgaIdsToFilterOn, pIncludeAllPeergroups);
	}
	
	public List<CRunPVToolkitFeedbackPractice> getCycleFeedbackPractices(IERunGroup runGroup, List<IXMLTag> practiceStepTags,
			boolean selfFeedback, List<String> otherRolesToFilterOn, List<Integer> otherRgaIdsToFilterOn, boolean pIncludeAllPeergroups) {
		// get feedback and practice data to be used in feedback step for run group (a
		// feedback giver). Possibly filter on step tag, roles or peers (if parameters are not null)
		List<CRunPVToolkitFeedbackPractice> feedbackPractices = new ArrayList<CRunPVToolkitFeedbackPractice>();
		IECaseComponent caseComponent = getCurrentProjectsCaseComponent();
		// get peer run groups of run group that might have asked for feedback
		List<IERunGroup> peerRunGroups = getPeerRungroups(runGroup, selfFeedback, otherRolesToFilterOn, otherRgaIdsToFilterOn, pIncludeAllPeergroups);
		for (IERunGroup peerRunGroup : peerRunGroups) {
			// loop through practice steps
			for (IXMLTag tempPracticeStepTag : practiceStepTags) {
				// get status of peer run group
				IXMLTag runStatusTag = getRunStatusTag(peerRunGroup, caseComponent, tempPracticeStepTag,
						AppConstants.statusTypeRunGroup);
				if (runStatusTag != null) {
					// get next feedback step from practice step. it is the step in which feedback
					// should be given on the practice
					IXMLTag feedbackStepTag = getNextStepTagInCurrentCycle(tempPracticeStepTag, feedbackStepType);
					IXMLTag runStatusStatusTag = getStatusChildTag(runStatusTag);
					// get all practice tags for peer run group
					List<IXMLTag> practiceTags = runStatusStatusTag.getChilds("practice");
					for (IXMLTag practiceTag : practiceTags) {
						// NOTE for self feedback return all practice tags, for peer feedback only the
						// ones that have been shared
						if (selfFeedback || getStatusChildTagAttribute(practiceTag, "share")
								.equals(AppConstants.statusValueTrue)) {
							// NOTE because of peculiarities in Rob's demonstrator run, where peers in each cycle are copies of peers in the previous cycle (but in different peer groups)
							// we need to check if stored practicergaid in practice tag is correct
							List<IERunGroupAccount> lPRGAccs = getRunGroupAccounts(peerRunGroup);
							if (lPRGAccs.size() > 0) {
								if (!(lPRGAccs.get(0).getRgaId() + "").equals(practiceTag.getAttribute(CRunPVToolkit.practiceRgaId))) {
									practiceTag.setAttribute(CRunPVToolkit.practiceRgaId, lPRGAccs.get(0).getRgaId() + "");
								}
								feedbackPractices.add(new CRunPVToolkitFeedbackPractice(feedbackStepTag, practiceTag));
							}
						}
					}
				}
			}
		}
		return feedbackPractices;
	}

	public List<IXMLTag> getFeedbackPracticeTags(IERunGroup runGroup, IXMLTag practiceStepTag, boolean selfFeedback,
			List<String> otherRolesToFilterOn, List<Integer> otherRgaIdsToFilterOn) {
		// get practice tags to be used in feedback step for run group (a feedback
		// giver). Possibly filter on step tag, roles or peers
		// NOTE only include peer group members
		List<CRunPVToolkitFeedbackPractice> feedbackPractices = getFeedbackPractices(runGroup, practiceStepTag,
				selfFeedback, otherRolesToFilterOn, otherRgaIdsToFilterOn, false);
		List<IXMLTag> feedbackPracticeTags = new ArrayList<IXMLTag>();
		for (CRunPVToolkitFeedbackPractice feedbackPractice : feedbackPractices) {
			feedbackPracticeTags.add(feedbackPractice.getPracticeTag());
		}
		return feedbackPracticeTags;
	}

	public IXMLTag getFeedbackPracticeTag(IERunGroup runGroup, IXMLTag practiceStepTag, boolean selfFeedback,
			IXMLTag practiceTag, List<String> otherRolesToFilterOn, List<Integer> otherRgaIdsToFilterOn) {
		// get shared practice tag within practice step tag for run group where
		// practitioner is determined from practice tag. Possibly filter on roles or
		// peers. practiceStepTag is not null.
		if (practiceTag == null) {
			return null;
		}
		String id = practiceTag.getAttribute(practiceRgaId);
		List<IXMLTag> practiceTags = getFeedbackPracticeTags(runGroup, practiceStepTag, selfFeedback,
				otherRolesToFilterOn, otherRgaIdsToFilterOn);
		for (IXMLTag tempPracticeTag : practiceTags) {
			// NOTE practice tag must be shared. Only one practice tag may be shared.
			if (tempPracticeTag.getAttribute(practiceRgaId).equals(id)
					&& getStatusChildTagAttribute(tempPracticeTag, "share").equals(AppConstants.statusValueTrue)) {
				return tempPracticeTag;
			}
		}
		return null;
	}

	protected IXMLTag getFeedbackNewOrExistingFeedbackTag(IXMLTag runStepTagStatusChildTag, IXMLTag refTag,
			boolean refTagIsStatusTag, boolean createNew) {
		// if refTag is not null it is used to get an existing feedback tag
		if (refTag == null) {
			return null;
		}
		String tagName = "feedback";
		IXMLTag tag = getNewOrExistingRunStatusChildTag(runStepTagStatusChildTag, tagName, refTag, refTagIsStatusTag,
				createNew, true);
		if (tag == null) {
			return null;
		}
		if (tag.getAttribute("rubricscacid").equals("")) {
			// store cacid of rubrics case component used as attribute
			tag.setAttribute("rubricscacid", "" + this.getCurrentRubricsCaseComponent().getCacId());
		}
		return tag;
	}

	public IXMLTag getFeedbackTag(IERunGroup runGroup, IXMLTag feedbackStepTag, IXMLTag refTag,
			boolean refTagIsStatusTag, boolean createNew) {
		return getFeedbackNewOrExistingFeedbackTag(
				getRunProjectsStatusChildTag(runGroup, feedbackStepTag, AppConstants.statusTypeRunGroup), refTag,
				refTagIsStatusTag, createNew);
	}

	public String getFeedbackStatus(IERunGroup runGroup, IXMLTag feedbackStepTag, IXMLTag refTag,
			boolean refTagIsStatusTag, String key) {
		// get feedback status, e.g., if it is ready or shared or not
		if (refTag == null) {
			return "";
		}
		IXMLTag tag = getFeedbackTag(runGroup, feedbackStepTag, refTag, refTagIsStatusTag, false);
		if (tag == null) {
			return "";
		}
		return getStatusChildTagAttribute(tag, key);
	}

	public void setFeedbackStatus(IERunGroup runGroup, IXMLTag feedbackStepTag, IXMLTag refTag,
			boolean refTagIsStatusTag, String key, String value) {
		// set feedback status, e.g., if it is ready or shared or not
		if (refTag == null) {
			return;
		}
		IXMLTag tag = getFeedbackTag(runGroup, feedbackStepTag, refTag, refTagIsStatusTag, true);
		if (tag == null) {
			return;
		}
		setStatusChildTagAttribute(tag, key, value);
	}

	public void setFeedbackReady(IERunGroup runGroup, IXMLTag feedbackStepTag, IXMLTag refTag,
			boolean refTagIsStatusTag) {
		// set if all sub skills and skill are scored
		if (refTag == null) {
			return;
		}
		setFeedbackStatus(runGroup, feedbackStepTag, refTag, refTagIsStatusTag, "ready", AppConstants.statusValueTrue);
		saveFeedback();
	}

	public void setFeedbackInConsultation(IERunGroup runGroup, IXMLTag feedbackStepTag, IXMLTag practiceTag,
			boolean value) {
		// not used yet. Is meant for teachers to be able to consult each other on
		// feedbacks given, before feedbacks are shared.
		if (practiceTag == null) {
			return;
		}
		setFeedbackStatus(runGroup, feedbackStepTag, practiceTag, true, "inconsultation", "" + value);
		saveFeedback();
	}

	public void setFeedbackShared(IERunGroup runGroup, IXMLTag feedbackStepTag, IXMLTag practiceTag) {
		// set if feedback receiver may see feedback
		if (practiceTag == null) {
			return;
		}
		setFeedbackStatus(runGroup, feedbackStepTag, practiceTag, true, "share", AppConstants.statusValueTrue);
		// if shared store date/time as well
		setFeedbackStatus(runGroup, feedbackStepTag, practiceTag, true, "sharedate",
				CDefHelper.getDateStrAsDMY(new Date()));
		setFeedbackStatus(runGroup, feedbackStepTag, practiceTag, true, "sharetime",
				CDefHelper.getDateStrAsHMS(new Date()));
		saveFeedback();
	}

	protected IXMLTag getFeedbackNewOrExistingPerformanceTag(IXMLTag runStepTagStatusChildTag, IXMLTag skillTag,
			String childTagName, boolean createNew) {
		// NOTE get performance tag given by skill id and child tag name. It is a child
		// tag of runStepTagStatusChildTag (with name 'status'). If it does not exist
		// add it
		// performance tags are stored as child tags of the status child tag of a
		// feedback tag. It are either level, tips or tops tags.
		String skillTagId = skillTag.getAttribute(AppConstants.keyId);
		List<IXMLTag> performanceTags = runStepTagStatusChildTag.getChilds(childTagName);
		IXMLTag tag = null;
		for (IXMLTag performanceTag : performanceTags) {
			if (getStatusChildTagAttribute(performanceTag, "skilltagid").equals(skillTagId)) {
				// tag already exists
				tag = performanceTag;
				break;
			}
		}
		if (tag == null && createNew) {
			// tag does not exist yet
			// NOTE add performance tag with name given by childTagName. See XML definition
			// of step tag for a description of the attributes/values to be set
			tag = sSpring.getXmlManager().newXMLTag(childTagName, "");
			runStepTagStatusChildTag.getChildTags().add(tag);
			IXMLTag statusChildTag = getStatusChildTag(tag);
			statusChildTag.setAttribute("skilltagid", skillTagId);
		}
		return tag;
	}

	protected IXMLTag getFeedbackPerformanceTag(IERunGroup runGroup, IXMLTag feedbackStepTag, IXMLTag refTag,
			boolean refTagIsStatusTag, IXMLTag skillTag, String childTagName, boolean createNew) {
		// performance tags are stored as child tags of the status child tag of a
		// feedback tag. It are either level, tips or tops tags.
		IXMLTag feedbackTag = getFeedbackNewOrExistingFeedbackTag(
				getRunProjectsStatusChildTag(runGroup, feedbackStepTag, AppConstants.statusTypeRunGroup), refTag,
				refTagIsStatusTag, createNew);
		if (feedbackTag == null) {
			return null;
		} else {
			return getFeedbackNewOrExistingPerformanceTag(getStatusChildTag(feedbackTag), skillTag, childTagName,
					createNew);
		}
	}

	protected IXMLTag getFeedbackPerformanceLevelTag(IERunGroup runGroup, IXMLTag feedbackStepTag, IXMLTag refTag,
			boolean refTagIsStatusTag, IXMLTag skillTag, boolean createNew) {
		// returns performance level tag
		return getFeedbackPerformanceTag(runGroup, feedbackStepTag, refTag, refTagIsStatusTag, skillTag,
				"performancelevel", createNew);
	}

	public int getFeedbackLevel(IERunGroup runGroup, IXMLTag feedbackStepTag, IXMLTag refTag, boolean refTagIsStatusTag,
			IXMLTag skillTag) {
		boolean accessible = !skillTag.getCurrentStatusAttribute(AppConstants.statusKeyAccessible)
				.equals(AppConstants.statusValueFalse);
		// skillTag is either a skill, a skill cluster or a sub skill tag
		if (refTag == null) {
			// no feedback tag that is used to store level yet
			return (accessible ? noLevelYet : noLevelPossible);
		}
		IXMLTag tag = getFeedbackPerformanceLevelTag(runGroup, feedbackStepTag, refTag, refTagIsStatusTag, skillTag,
				false);
		if (tag == null) {
			// no performance level tag yet
			return (accessible ? noLevelYet : noLevelPossible);
		}
		int level = 0;
		try {
			String levelStr = getStatusChildTagAttribute(tag, "level");
			if (levelStr.length() > 0) {
				level = Integer.parseInt(getStatusChildTagAttribute(tag, "level"));
			}
		} catch (NumberFormatException e) {
		}
		return level;
	}

	public double getMeanPerformanceLevel(IERunGroup actor, int cycleNumber, IXMLTag skillTag,
			List<List<IXMLTag>> sharedFeedbackTagsPerFeedbackStep, List<IERunGroupAccount> filterOnRunGroupAccounts) {
		// get mean performance level given by peers in one cycle, possibly filtered on
		// certain peers (if filterOnRunGroupAccounts is not null)
		// NOTE return mean level as double to be prepared for showing non integer
		// values within skill wheel
		// NOTE mean level may depend on setting for weighting between teachers and students.
		double meanLevel = 0;
		if (cycleNumber < 1 || cycleNumber > sharedFeedbackTagsPerFeedbackStep.size()) {
			return meanLevel;
		}
		double numberOfPeers = 0;
		double sumOfLevels = 0;
		double weightingTeacherStudent = getWeightingTeacherStudent();
		double numberOfTeachers = 0;
		double numberOfStudents = 0;
		double sumOfLevelsForTeachers = 0;
		double sumOfLevelsForStudents = 0;
		// loop through all given feedback for a certain cycle;
		for (IXMLTag feedbackTag : sharedFeedbackTagsPerFeedbackStep.get(cycleNumber - 1)) {
			IERunGroupAccount runGroupAccount = getRunGroupAccount(feedbackTag.getAttribute("feedbackrgaid"));
			if (runGroupAccount != null) {
				if (includeRunGroupAccount(filterOnRunGroupAccounts, runGroupAccount)) {
					IXMLTag feedbackStepTag = getStepTagByChildTagOfStatusTag(runGroupAccount.getERunGroup(),
							feedbackTag);
					double level = getFeedbackLevel(runGroupAccount.getERunGroup(), feedbackStepTag, feedbackTag, true,
							skillTag);
					String peerRole = "";
					if (weightingTeacherStudent != -1) {
						// if weighting is set peer role matters
						peerRole = getPeerGroupFeedbackGiverRole(actor, runGroupAccount.getERunGroup());
					}
					// level -1 means no level given yet
					if (level >= 0) {
						if (weightingTeacherStudent == -1) {
							// if weighting is not set increase general sum of levels
							sumOfLevels += level;
						} else {
							// if weighting is set increase role specific sum of levels
							if (peerRole.equals(peerGroupTeacherRole)
									|| peerRole.equals(peerGroupStudentAssistantRole)) {
								sumOfLevelsForTeachers += level;
							} else {
								sumOfLevelsForStudents += level;
							}
						}
					}

					if (weightingTeacherStudent == -1) {
						// if weighting is not set increase general number of peers
						numberOfPeers++;
					} else {
						// if weighting is set increase role specific number of peers
						if (peerRole.equals(peerGroupTeacherRole) || peerRole.equals(peerGroupStudentAssistantRole)) {
							numberOfTeachers++;
						} else {
							numberOfStudents++;
						}
					}
				}
			}
		}
		if (weightingTeacherStudent == -1) {
			if (numberOfPeers > 0) {
				// if weighting is not set calculate general mean level
				meanLevel += sumOfLevels / numberOfPeers;
			}
		} else {
			// if weighting is set calculate mean level accounting for peer roles
			if (numberOfTeachers == 0) {
				// if no teachers calculate mean students level
				meanLevel += sumOfLevelsForStudents / numberOfStudents;
			} else if (numberOfStudents == 0) {
				// if no students calculate mean teachers level
				meanLevel += sumOfLevelsForTeachers / numberOfTeachers;
			} else {
				// if teachers and students calculate role weighted mean level
				meanLevel += weightingTeacherStudent * (sumOfLevelsForTeachers / numberOfTeachers);
				meanLevel += (1 - weightingTeacherStudent) * (sumOfLevelsForStudents / numberOfStudents);
			}
		}
		return meanLevel;
	}

	protected boolean includeRunGroupAccount(List<IERunGroupAccount> filterOnRunGroupAccounts,
			IERunGroupAccount runGroupAccount) {
		// checks if run group account has to be filtered out
		if (filterOnRunGroupAccounts == null) {
			return true;
		}
		for (IERunGroupAccount tempRunGroupAccount : filterOnRunGroupAccounts) {
			if (tempRunGroupAccount.getRgaId() == runGroupAccount.getRgaId()) {
				return true;
			}
		}
		return false;
	}

	public double getWeightingTeacherStudent() {
		// get weighting of teacher feedback scores with respect to student feedback scores
		// if not set it returns -1, meaning teacher(s) and students are equally
		// weighted. If one teacher and three students all contribute 25%.
		// if set it is a value between 0 and 1. E.g., if it is 0.5 and there is one
		// teacher and three students, the teacher score contributes 50%.
		String weightingTeacherStudent = getCurrentMethodTag()
				.getCurrentStatusAttribute(AppConstants.statusKeyWeightingteacherstudent);
		double weighting = -1;
		if (!weightingTeacherStudent.equals("")) {
			try {
				weighting = Double.parseDouble(weightingTeacherStudent);
			} catch (NumberFormatException e) {
			}
		}
		if (weighting < 0 || weighting > 1) {
			weighting = -1;
		}
		return weighting;
	}

	public double getDevelopment(IERunGroup actor, int cycleNumber, IXMLTag skillTag,
			List<List<IXMLTag>> sharedFeedbackTagsPerFeedbackStep) {
		// gets development between cycle and previous cycle
		// NOTE development = 1 means no improvement or decline. > 1 means improvement.
		// < 1 means decline.
		// E.g., if development = 2, it means an improvement of 100% compared to previous cycle
		double development = 1;
		if (cycleNumber <= 1) {
			// no development yet
			return development;
		}
		double firstLevel = getMeanPerformanceLevel(actor, cycleNumber - 1, skillTag, sharedFeedbackTagsPerFeedbackStep,
				null);
		if (firstLevel == 0) {
			// normally first level may not be 0 if cycleNumber > 1, but during testing it might be
			return development;
		}
		double secondLevel = getMeanPerformanceLevel(actor, cycleNumber, skillTag, sharedFeedbackTagsPerFeedbackStep,
				null);
		return secondLevel / firstLevel;
	}

	public void saveFeedbackLevel(IERunGroup runGroup, IXMLTag feedbackStepTag, IXMLTag refTag,
			boolean refTagIsStatusTag, IXMLTag skillTag, int level) {
		// saves score given by feedback giver runGroup to practitioner or expert
		// (determined by refTag), for certain skill tag,
		// where skillTag is either a skill, a skill cluster or a sub skill tag
		if (refTag == null) {
			// no feedback tag that is used to store level yet
			return;
		}
		IXMLTag tag = getFeedbackPerformanceLevelTag(runGroup, feedbackStepTag, refTag, refTagIsStatusTag, skillTag,
				true);
		if (tag == null) {
			// if tag not found or created
			return;
		}
		getStatusChildTag(tag).setAttribute("level", "" + level);
		saveFeedback();

		/* START TESTING CODE */
		if (_storeFeedbackInStatesAsWell && getCurrentCycleNumber() == 1 && getCurrentStepNumber() == 3) {
			// NOTE temporarily save levels as state as well. To be able to easily distract teacher scores
			String caseComponentName = "PV_states_teachers_cycle" + getCurrentCycleNumber() + "_step"
					+ getCurrentStepNumber();
			String stateTagKey = "";
			if (skillTag.getName().equals("subskill")) {
				String skillTagName = skillTag.getChildValue("pid");
				String[] skillTagNameArr = skillTagName.split("_");
				stateTagKey = "subskill_" + getCurrentStepNumber() + "_" + skillTagNameArr[1] + "_" + skillTagNameArr[2]
						+ "_level";
			} else {
				stateTagKey = "skill_" + getCurrentStepNumber() + "_level";
			}
			List<String> errors = new ArrayList<String>();
			sSpring.setCurrentRunTagStatus(caseComponentName, "state", stateTagKey, AppConstants.statusKeyValue,
					"" + level, AppConstants.statusTypeRunGroup, true, errors);
		}
		/* END TESTING CODE */
	}

	protected IXMLTag getSkillExampleExpertPerformanceLevelTag(IXMLTag skillExampleExpertTag, IXMLTag skillTag) {
		// expert levels are entered in authoring environment as child tags of skillExampleExpertTag
		// skillTag is either a skill, a skill cluster or a sub skill tag
		if (skillExampleExpertTag == null || skillTag == null) {
			return null;
		}
		List<IXMLTag> skillExampleLevelTags = skillExampleExpertTag.getChilds("videoskillexamplelevel");
		List<IXMLTag> performanceLevelTags = skillTag.getChilds("performancelevel");
		for (IXMLTag skillExampleLevelTag : skillExampleLevelTags) {
			IXMLTag referencedPerformanceLevelTag = getReferencedNodeTag(getCurrentRubricsCaseComponent(),
					skillExampleLevelTag, "refperformancelevel");
			if (referencedPerformanceLevelTag != null) {
				String refPerformanceLevelTagId = referencedPerformanceLevelTag.getAttribute(AppConstants.keyId);
				for (IXMLTag performanceLevelTag : performanceLevelTags) {
					if (performanceLevelTag.getAttribute(AppConstants.keyId).equals(refPerformanceLevelTagId)) {
						return performanceLevelTag;
					}
				}
			}
		}
		return null;
	}

	public int getSkillExampleExpertLevel(IXMLTag videoSkillExampleExpertTag, IXMLTag skillTag) {
		// expert levels are entered in authoring environment as child tags of
		// skillExampleExpertTag
		// skillTag is either a skill, a skill cluster or a sub skill tag
		IXMLTag performanceLevelTag = getSkillExampleExpertPerformanceLevelTag(videoSkillExampleExpertTag, skillTag);
		if (performanceLevelTag == null) {
			// no performance level tag yet
			return noLevelYet;
		}
		int level = 0;
		try {
			String levelStr = performanceLevelTag.getChildValue("level");
			if (levelStr.length() > 0) {
				level = Integer.parseInt(levelStr);
			}
		} catch (NumberFormatException e) {
		}
		return level;
	}

	public Integer getSkillLevel(IERunGroup runGroup, IECaseComponent caseComponent, IXMLTag feedbackStepTag,
			IXMLTag refTag, boolean refTagIsStatusTag, IXMLTag skillTag) {
		if (refTag == null) {
			// no feedback tag that is used to store level yet
			return new Integer(noLevelYet);
		}
		if (skillTag.getName().equals("skill")
				&& isRunSpecificTagPresent(new CRunPVToolkitCacAndTag(caseComponent, skillTag))) {
			return new Integer(getFeedbackLevel(runGroup, feedbackStepTag, refTag, refTagIsStatusTag, skillTag));
		}
		return new Integer(noLevelYet);
	}

	public Integer getSkillClusterLevel(IERunGroup runGroup, IECaseComponent caseComponent, IXMLTag feedbackStepTag,
			IXMLTag refTag, boolean refTagIsStatusTag, IXMLTag skillclusterTag) {
		if (refTag == null) {
			// no feedback tag that is used to store level yet
			return new Integer(noLevelYet);
		}
		if (skillclusterTag.getName().equals("skillcluster")
				&& isRunSpecificTagPresent(new CRunPVToolkitCacAndTag(caseComponent, skillclusterTag))) {
			return new Integer(getFeedbackLevel(runGroup, feedbackStepTag, refTag, refTagIsStatusTag, skillclusterTag));
		}
		return new Integer(noLevelYet);
	}

	public Integer getSubSkillLevel(IERunGroup runGroup, IECaseComponent caseComponent, IXMLTag feedbackStepTag,
			IXMLTag refTag, boolean refTagIsStatusTag, IXMLTag skillclusterTag) {
		if (refTag == null) {
			// no feedback tag that is used to store level yet
			return new Integer(noLevelYet);
		}
		if (skillclusterTag.getName().equals("subskill")
				&& isRunSpecificTagPresent(new CRunPVToolkitCacAndTag(caseComponent, skillclusterTag))) {
			return new Integer(getFeedbackLevel(runGroup, feedbackStepTag, refTag, refTagIsStatusTag, skillclusterTag));
		}
		return new Integer(noLevelYet);
	}

	public List<Integer> getSubSkillLevelsPerSkill(IERunGroup runGroup, IECaseComponent caseComponent,
			IXMLTag feedbackStepTag, IXMLTag refTag, boolean refTagIsStatusTag, IXMLTag skillTag) {
		List<Integer> subskillLevels = new ArrayList<Integer>();
		if (refTag == null) {
			// no feedback tag that is used to store level yet
			return subskillLevels;
		}
		for (IXMLTag skillclusterTag : skillTag.getChildTags(AppConstants.defValueNode)) {
			if (skillclusterTag.getName().equals("skillcluster")
					&& isRunSpecificTagPresent(new CRunPVToolkitCacAndTag(caseComponent, skillclusterTag))) {
				subskillLevels.addAll(getSubSkillLevelsPerSkillCluster(runGroup, caseComponent, feedbackStepTag, refTag,
						refTagIsStatusTag, skillclusterTag));
			}
		}
		return subskillLevels;
	}

	public List<Integer> getSubSkillLevelsPerSkillCluster(IERunGroup runGroup, IECaseComponent caseComponent,
			IXMLTag feedbackStepTag, IXMLTag refTag, boolean refTagIsStatusTag, IXMLTag skillclusterTag) {
		List<Integer> subskillLevels = new ArrayList<Integer>();
		for (IXMLTag subskillTag : skillclusterTag.getChildTags(AppConstants.defValueNode)) {
			if (subskillTag.getName().equals("subskill")
					&& isRunSpecificTagPresent(new CRunPVToolkitCacAndTag(caseComponent, subskillTag))) {
				subskillLevels.add(new Integer(
						getFeedbackLevel(runGroup, feedbackStepTag, refTag, refTagIsStatusTag, subskillTag)));
			}
		}
		return subskillLevels;
	}

	public int getNumberOfGivenSubSkillScores(IERunGroup runGroup, IECaseComponent caseComponent,
			IXMLTag feedbackStepTag, IXMLTag refTag, boolean refTagIsStatusTag, IXMLTag skillTag) {
		int number = 0;
		if (refTag == null) {
			// no feedback tag that is used to store level yet
			return number;
		}
		List<Integer> subSkillLevelsPerSkill = getSubSkillLevelsPerSkill(runGroup, caseComponent, feedbackStepTag,
				refTag, refTagIsStatusTag, skillTag);
		for (Integer subSkillLevelPerSkill : subSkillLevelsPerSkill) {
			if (subSkillLevelPerSkill.intValue() > 0) {
				number++;
			}
		}
		return number;
	}

	protected IXMLTag getFeedbackPerformanceTipsOrTopsTag(IERunGroup runGroup, IXMLTag feedbackStepTag, IXMLTag refTag,
			boolean refTagIsStatusTag, IXMLTag skillTag, String tipsOrTops, boolean createNew) {
		// returns performance tips or tops tag
		if (refTag == null) {
			// no feedback tag that is used to store tips or tops yet
			return null;
		}
		return getFeedbackPerformanceTag(runGroup, feedbackStepTag, refTag, refTagIsStatusTag, skillTag,
				"performance" + tipsOrTops, createNew);
	}

	public String getFeedbackTipsOrTops(IERunGroup runGroup, IXMLTag feedbackStepTag, IXMLTag refTag,
			boolean refTagIsStatusTag, IXMLTag skillTag, String tipsOrTops) {
		if (refTag == null) {
			// no feedback tag that is used to store tips or tops yet
			return "";
		}
		IXMLTag performanceTipsOrTopsTag = getFeedbackPerformanceTipsOrTopsTag(runGroup, feedbackStepTag, refTag,
				refTagIsStatusTag, skillTag, tipsOrTops, false);
		if (performanceTipsOrTopsTag == null) {
			// no performance tips or tops tag yet
			return "";
		}
		return getStatusChildTagAttribute(performanceTipsOrTopsTag, tipsOrTops)
				.replace(AppConstants.statusCrLfReplace, "\n").replace(AppConstants.statusCommaReplace, ",");
	}

	public String getFeedbackTips(IERunGroup runGroup, IXMLTag feedbackStepTag, IXMLTag refTag,
			boolean refTagIsStatusTag, IXMLTag skillTag) {
		return getFeedbackTipsOrTops(runGroup, feedbackStepTag, refTag, refTagIsStatusTag, skillTag, "tips");
	}

	public String getFeedbackTops(IERunGroup runGroup, IXMLTag feedbackStepTag, IXMLTag refTag,
			boolean refTagIsStatusTag, IXMLTag skillTag) {
		return getFeedbackTipsOrTops(runGroup, feedbackStepTag, refTag, refTagIsStatusTag, skillTag, "tops");
	}

	protected void saveFeedbackTipsOrTops(IERunGroup runGroup, IXMLTag feedbackStepTag, IXMLTag refTag,
			boolean refTagIsStatusTag, IXMLTag skillTag, String tipsOrTops, String value) {
		if (refTag == null) {
			// no feedback tag that is used to store tips or tops yet
			return;
		}
		IXMLTag performanceTipsOrTopsTag = getFeedbackPerformanceTipsOrTopsTag(runGroup, feedbackStepTag, refTag,
				refTagIsStatusTag, skillTag, tipsOrTops, true);
		if (performanceTipsOrTopsTag == null) {
			// no performance tips or tops tag yet
			return;
		}

		//TODO set proper antisamy configuration
		//value = AntiSamyHelper.cleanup(value);

		getStatusChildTag(performanceTipsOrTopsTag).setAttribute(tipsOrTops, sSpring.escapeXML(
				value.replace("\n", AppConstants.statusCrLfReplace).replace(",", AppConstants.statusCommaReplace)));
		saveFeedback();

		/* START TESTING CODE */
		if (_storeFeedbackInStatesAsWell && getCurrentCycleNumber() == 1 && getCurrentStepNumber() == 3) {
			// NOTE temporarily save levels as state as well. To be able to easily distract teacher scores
			String caseComponentName = "PV_states_teachers_cycle" + getCurrentCycleNumber() + "_step"
					+ getCurrentStepNumber();
			String skillTagName = skillTag.getChildValue("pid");
			// JP_1_Inhoud-structuur
			String[] skillTagNameArr = skillTagName.split("_");
			String stateTagKey = "skillcluster_" + getCurrentStepNumber() + "_" + skillTagNameArr[1] + "_" + tipsOrTops;
			List<String> errors = new ArrayList<String>();
			sSpring.setCurrentRunTagStatus(caseComponentName, "state", stateTagKey, AppConstants.statusKeyValue,
					sSpring.escapeXML(value.replace("\n", AppConstants.statusCrLfReplace).replace(",",
							AppConstants.statusCommaReplace)),
					AppConstants.statusTypeRunGroup, true, errors);
		}
		/* END TESTING CODE */
	}

	public void saveFeedbackTips(IERunGroup runGroup, IXMLTag feedbackStepTag, IXMLTag refTag,
			boolean refTagIsStatusTag, IXMLTag skillTag, String tips) {
		saveFeedbackTipsOrTops(runGroup, feedbackStepTag, refTag, refTagIsStatusTag, skillTag, "tips", tips);
	}

	public void saveFeedbackTops(IERunGroup runGroup, IXMLTag feedbackStepTag, IXMLTag refTag,
			boolean refTagIsStatusTag, IXMLTag skillTag, String tops) {
		saveFeedbackTipsOrTops(runGroup, feedbackStepTag, refTag, refTagIsStatusTag, skillTag, "tops", tops);
	}

	protected IXMLTag getSkillExampleTipsTopsTag(IXMLTag skillExampleExpertTag, IXMLTag skillTag) {
		// expert tips and tops are entered in authoring environment as child tags of skillExampleExpertTag
		if (skillExampleExpertTag == null || skillTag == null) {
			return null;
		}
		String skillTagId = skillTag.getAttribute(AppConstants.keyId);
		List<IXMLTag> skillExampleTipsTopsTags = skillExampleExpertTag.getChilds("videoskillexampletipstops");
		for (IXMLTag skillExampleTipsTopsTag : skillExampleTipsTopsTags) {
			IXMLTag referencedSkillTag = getReferencedNodeTag(getCurrentRubricsCaseComponent(), skillExampleTipsTopsTag,
					"ref" + skillTag.getName());
			if (referencedSkillTag != null && referencedSkillTag.getAttribute(AppConstants.keyId).equals(skillTagId)) {
				return skillExampleTipsTopsTag;
			}
		}
		return null;
	}

	public String getSkillExampleExpertTipsOrTops(IXMLTag skillExampleExpertTag, IXMLTag skillTag, String tipsOrTops) {
		IXMLTag skillExampleTipsTopsTag = getSkillExampleTipsTopsTag(skillExampleExpertTag, skillTag);
		if (skillExampleTipsTopsTag == null) {
			return "";
		}
		return sSpring.unescapeXML(skillExampleTipsTopsTag.getChildValue(tipsOrTops)
				.replace(AppConstants.statusCrLfReplace, "\n").replace(AppConstants.statusCommaReplace, ","));
	}

	public String getSkillExampleExpertTips(IXMLTag skillExampleExpertTag, IXMLTag skillTag) {
		return getSkillExampleExpertTipsOrTops(skillExampleExpertTag, skillTag, "tips");
	}

	public String getSkillExampleExpertTops(IXMLTag skillExampleExpertTag, IXMLTag skillTag) {
		return getSkillExampleExpertTipsOrTops(skillExampleExpertTag, skillTag, "tops");
	}

	public void renderStars(Component parent, int numberOfStars, boolean selected, String classPrefix) {
		// renders rating stars
		parent.getChildren().clear();

		if (StringUtils.isEmpty(classPrefix)) {
			classPrefix = "score";
		}

		Div div = new CRunPVToolkitDefDiv(parent, new String[] { "class" },
				new Object[] { classPrefix + "LevelStars" });

		Hbox hbox = new CRunPVToolkitDefHbox(div, new String[] { "class" },
				new Object[] { classPrefix + "LevelStars" });

		for (int i = 0; i < numberOfStars; i++) {
			appendStar(hbox, selected, classPrefix);
		}
	}

	protected void appendStar(Component parent, boolean selected, String classPrefix) {
		// renders rating star
		String src = "";
		if (selected) {
			src = zulfilepath + "star-filled-blue.svg";
		} else {
			src = zulfilepath + "star-filled-black.svg";
		}
		new CRunPVToolkitDefImage(parent, new String[] { "class", "src" },
				new Object[] { classPrefix + "LevelStar", src });
	}

	public boolean mayRateGivenFeedback(IERunGroup pRunGroup) {
		// can student grade received feedback
		IECaseComponent lProjComponent = getCurrentProjectsCaseComponent();
		String lMayRateFBStr = getRunGroupStatusValue(pRunGroup, lProjComponent, "method", getCurrentMethodTag().getChildValue(getCurrentMethodTag().getDefAttribute(AppConstants.defKeyKey)), AppConstants.statusKeyMayrategivenfeedback);
		if (StringUtils.isEmpty(lMayRateFBStr))
			return false;
		if (!lMayRateFBStr.equalsIgnoreCase(AppConstants.statusValueTrue))
			return false;
		return true;
	}
		
	public boolean mustRateGivenFeedback(IERunGroup pRunGroup) {
		// must student grade received feedback
		IECaseComponent lProjComponent = getCurrentProjectsCaseComponent();
		String lMustRateFBStr = getRunGroupStatusValue(pRunGroup, lProjComponent, "method", getCurrentMethodTag().getChildValue(getCurrentMethodTag().getDefAttribute(AppConstants.defKeyKey)), AppConstants.statusKeyMustrategivenfeedback);
		if (StringUtils.isEmpty(lMustRateFBStr))
			return false;
		if (!lMustRateFBStr.equalsIgnoreCase(AppConstants.statusValueTrue))
			return false;
		return true;
	}
		
	/* DEFINE GOALS */

	public void saveGoals() {
		// stores changes in goals in database for current step, steps are part of
		// projects case component
		saveStatusChange(getCurrentProjectsCaseComponent(), AppConstants.statusTypeRunGroup);
	}

	protected IXMLTag getDefineGoalsNewDefineGoalsTag(IXMLTag runStepTagStatusChildTag) {
		return getNewOrExistingRunStatusChildTag(runStepTagStatusChildTag, "definegoals", null, true, true, false);
	}

	public String getGoals(IERunGroup runGroup, IXMLTag defineGoalsStepTag) {
		IXMLTag tag = getDefineGoalsNewDefineGoalsTag(
				getRunProjectsStatusChildTag(runGroup, defineGoalsStepTag, AppConstants.statusTypeRunGroup));
		if (tag == null) {
			// if no define goals tag
			return "";
		}
		IXMLTag statusChildTag = getStatusChildTag(tag);
		return statusChildTag.getAttribute("goals").replace(AppConstants.statusCrLfReplace, "\n")
				.replace(AppConstants.statusCommaReplace, ",");
	}

	public boolean saveGoals(IERunGroup runGroup, IXMLTag defineGoalsStepTag, String goals) {
		IXMLTag tag = getDefineGoalsNewDefineGoalsTag(
				getRunProjectsStatusChildTag(runGroup, defineGoalsStepTag, AppConstants.statusTypeRunGroup));
		if (tag == null) {
			return false;
		}

		IXMLTag statusChildTag = getStatusChildTag(tag);
		statusChildTag.setAttribute("goals", sSpring.escapeXML(
				goals.replace("\n", AppConstants.statusCrLfReplace).replace(",", AppConstants.statusCommaReplace)));
		// set share to be consistent with practicing and giving feedback
		statusChildTag.setAttribute("share", AppConstants.statusValueTrue);
		// if shared store date/time as well
		statusChildTag.setAttribute("sharedate", CDefHelper.getDateStrAsDMY(new Date()));
		statusChildTag.setAttribute("sharetime", CDefHelper.getDateStrAsHMS(new Date()));

		saveGoals();

		return true;
	}

	/* DASHBOARD */

//NOTE the dashboard will be available to the student to see his activities or progress. But teachers must also be able to see the progress for a specific student, the skill wheel.
//Therefore the rungroup is a parameter

	public List<IXMLTag> getPeerXmlStatusRootTags(IERunGroup runGroup, boolean selfFeedback,
			IECaseComponent caseComponent, String statusType, List<String> otherRolesToFilterOn,
			List<Integer> otherRgaIdsToFilterOn) {
		// get status root tags for all peers, possibly filtered on certain roles or peers
		List<IXMLTag> peerXmlStatusRootTags = new ArrayList<IXMLTag>();
		for (IERunGroup peerRunGroup : getPeerRungroups(runGroup, selfFeedback, otherRolesToFilterOn,
				otherRgaIdsToFilterOn, true)) {
			peerXmlStatusRootTags.add(getXmlStatusRootTag(peerRunGroup, caseComponent, statusType));
		}
		return peerXmlStatusRootTags;
	}

	public List<IXMLTag> getSharedPracticeTagsPerFeedbackStep(IERunGroup runGroup) {
		// NOTE get shared practice tags to be able to show practice info like practice
		// product (e.g., video) or practice name in dashboard, for every cycle
		List<IXMLTag> sharedPracticeTagsPerFeedbackStep = new ArrayList<IXMLTag>();
		for (IXMLTag runStepTagStatusChildTag : getRunStepTagStatusChildTags(runGroup, practiceStepType,
				AppConstants.statusTypeRunGroup)) {
			for (IXMLTag practiceTag : runStepTagStatusChildTag.getChilds("practice")) {
				if (getStatusChildTagAttribute(practiceTag, "share").equals(AppConstants.statusValueTrue)) {
					sharedPracticeTagsPerFeedbackStep.add(practiceTag);
				}
			}
		}
		return sharedPracticeTagsPerFeedbackStep;
	}

	public List<List<IXMLTag>> getSharedFeedbackTagsPerFeedbackStep(IERunGroup runGroup, boolean includeSelfFeedback,
			List<String> otherRolesToFilterOn, List<Integer> otherRgaIdsToFilterOn) {
		// NOTE get shared feedback tags per feedback step (so per cycle), to be able to
		// fill the skill wheel and show tips/tops
		// NOTE get shared practice uuids, because feedbacks will refer to practices by
		// these practice uuids
		List<String> sharedPracticeUuids = new ArrayList<String>();
		List<IXMLTag> sharedPracticeTagsPerFeedbackStep = getSharedPracticeTagsPerFeedbackStep(runGroup);
		String lPracticeRgaId = "";
		for (IXMLTag sharedPracticeTagPerFeedbackStep : sharedPracticeTagsPerFeedbackStep) {
			sharedPracticeUuids.add(sharedPracticeTagPerFeedbackStep.getAttribute(practiceUuId));
			if (StringUtils.isEmpty(lPracticeRgaId)) {
				lPracticeRgaId = sharedPracticeTagPerFeedbackStep.getAttribute(practiceRgaId);
			}
		}

		// NOTE get shared feedback tags per feedback step
		List<List<IXMLTag>> sharedFeedbackTagsPerFeedbackStep = new ArrayList<List<IXMLTag>>();
		IECaseComponent caseComponent = getCurrentProjectsCaseComponent();
		// get status root tags for all peers, possibly filtered on certain roles or peers
		List<IXMLTag> peerXmlStatusRootTags = getPeerXmlStatusRootTags(runGroup, false, caseComponent,
				AppConstants.statusTypeRunGroup, otherRolesToFilterOn, otherRgaIdsToFilterOn);
		IXMLTag selfXmlStatusRootTag = null;
		if (includeSelfFeedback) {
			List<IXMLTag> selfXmlStatusRootTags = getPeerXmlStatusRootTags(runGroup, true, caseComponent,
					AppConstants.statusTypeRunGroup, otherRolesToFilterOn, otherRgaIdsToFilterOn);
			if (selfXmlStatusRootTags.size() > 0) {
				selfXmlStatusRootTag = selfXmlStatusRootTags.get(0);
			}
			peerXmlStatusRootTags.add(selfXmlStatusRootTag);
		}
		// NOTE if there are multiple feedback step tags these correspond to different cycles
		// TODO what if there are multiple feedback step tags within one cycle?
		// NOTE feedback is stored in feedback steps
		List<IXMLTag> feedbackStepTags = getStepTags(feedbackStepType);
		for (IXMLTag feedbackStepTag : feedbackStepTags) {
			List<IXMLTag> sharedFeedbackTags = new ArrayList<IXMLTag>();
			for (IXMLTag peerXmlStatusRootTag : peerXmlStatusRootTags) {
				boolean isSelfFeedback = peerXmlStatusRootTag == selfXmlStatusRootTag;
				IXMLTag runStatusTag = getRunStatusTag(peerXmlStatusRootTag, feedbackStepTag);
				IXMLTag runStatusStatusTag = getStatusChildTag(runStatusTag);
				List<IXMLTag> feedbackTags = runStatusStatusTag.getChilds("feedback");
				for (IXMLTag feedbackTag : feedbackTags) {
					// NOTE added test on practiceRgaId because of Rob's demonstrator run which contains status data that is directly manipulated in the database 
					if (feedbackTag.getAttribute(practiceRgaId).equals(lPracticeRgaId)) {
						// NOTE ready is set in case of self feedback or feedback is shared by peers
						boolean ok = (isSelfFeedback && getStatusChildTagAttribute(feedbackTag, "ready").equals(AppConstants.statusValueTrue))
								|| getStatusChildTagAttribute(feedbackTag, "share").equals(AppConstants.statusValueTrue);
						if (ok) {
							if (sharedPracticeUuids.contains(feedbackTag.getAttribute(practiceUuId)))
								sharedFeedbackTags.add(feedbackTag);
						}
					}
				}
			}
			sharedFeedbackTagsPerFeedbackStep.add(sharedFeedbackTags);
		}
		return sharedFeedbackTagsPerFeedbackStep;
	}

	protected void saveMarkedFeedbackTipsOrTops() {
		// stores changes in marked tips/tops in database
		saveStatusChange(getCurrentProjectsCaseComponent(), AppConstants.statusTypeRunGroup);
	}

	/* DOCUMENTED TILL THIS POINT */

	protected IXMLTag getMarkedFeedbackTipsOrTopsTag(IERunGroup runGroup, CRunPVToolkitFeedbackTipOrTop feedback,
			boolean createNew) {
		IXMLTag skillTag = null;
		if (feedback.isSkillHasTipsTops()) {
			skillTag = feedback.getSkillTag();
		} else if (feedback.isSkillclusterHasTipsTops()) {
			skillTag = feedback.getSkillClusterTag();
		} else if (feedback.isSubskillHasTipsTops()) {
			skillTag = feedback.getSubSkillTag();
		}
		if (skillTag == null) {
			return null;
		}
		String tagName = "";
		if (feedback.isTip()) {
			tagName = "marktips";
		} else if (feedback.isTop()) {
			tagName = "marktops";
		}
		if (tagName.equals("")) {
			return null;
		}
		Map<String, String> refStatusTagAttributes = new HashMap<String, String>();
		refStatusTagAttributes.put("skilltagid", skillTag.getAttribute(AppConstants.keyId));
		return getNewOrExistingRunStatusChildTag(
				getRunProjectsStatusChildTag(runGroup, feedback.getStepTag(), AppConstants.statusTypeRunGroup), tagName,
				feedback.getFeedbackTag(), refStatusTagAttributes, feedback.isFeedbackTagIsStatusTag(), createNew,
				true);
	}

	public boolean getMarkedFeedbackTipsOrTops(IERunGroup runGroup, CRunPVToolkitFeedbackTipOrTop feedback) {
		IXMLTag feedbackTipsOrTopsMarkTag = getMarkedFeedbackTipsOrTopsTag(runGroup, feedback, false);
		if (feedbackTipsOrTopsMarkTag == null) {
			return false;
		}
		IXMLTag statusChildTag = getStatusChildTag(feedbackTipsOrTopsMarkTag);
		return statusChildTag.getAttribute("mark").equals(AppConstants.statusValueTrue);
	}

	public IXMLTag getFeedbackTipsOrTopsSkillTag(CRunPVToolkitFeedbackTipOrTop feedback) {
		IXMLTag skillTag = feedback.getSubSkillTag();
		if (skillTag == null) {
			skillTag = feedback.getSkillClusterTag();
		}
		if (skillTag == null) {
			skillTag = feedback.getSkillTag();
		}
		return skillTag;
	}

	public void saveMarkedFeedbackTipsOrTops(IERunGroup runGroup, CRunPVToolkitFeedbackTipOrTop feedback) {
		IXMLTag feedbackTipsOrTopsMarkTag = getMarkedFeedbackTipsOrTopsTag(runGroup, feedback, true);
		IXMLTag statusChildTag = getStatusChildTag(feedbackTipsOrTopsMarkTag);
		statusChildTag.setAttribute("mark", "" + feedback.isMark());
		if (feedback.isMark()) {
			IXMLTag skillTag = getFeedbackTipsOrTopsSkillTag(feedback);
			if (skillTag != null) {
				statusChildTag.setAttribute("skilltagid", skillTag.getAttribute(AppConstants.keyId));
				statusChildTag.setAttribute("markdate", CDefHelper.getDateStrAsDMY(new Date()));
				statusChildTag.setAttribute("marktime", CDefHelper.getDateStrAsHMS(new Date()));
			}
		}

		saveMarkedFeedbackTipsOrTops();
	}

	public void renderStars(Component parent, List<String> performanceLevelStates) {
		parent.getChildren().clear();

		Div div = new CRunPVToolkitDefDiv(parent, new String[] { "class" }, new Object[] { "scoreLevelStars" });

		Hbox hbox = new CRunPVToolkitDefHbox(div, new String[] { "class" }, new Object[] { "scoreLevelStars" });

		for (String performanceLevelState : performanceLevelStates) {
			appendStar(hbox, performanceLevelState);
		}
	}

	protected void appendStar(Component parent, String performanceLevelState) {
		String src = "";
		if (performanceLevelState.equals(CRunPVToolkit.performanceLevelAchievedState)) {
			src = zulfilepath + "star-filled-lightgreen.svg";
		} else if (performanceLevelState.equals(CRunPVToolkit.performanceLevelImprovedState)) {
			src = zulfilepath + "star-filled-green.svg";
		} else if (performanceLevelState.equals(CRunPVToolkit.performanceLevelDeterioratedState)) {
			src = zulfilepath + "star-filled-red.svg";
		} else if (performanceLevelState.equals(CRunPVToolkit.performanceLevelCompareBothState)) {
			src = zulfilepath + "star-filled-lightgreen.svg";
		} else if (performanceLevelState.equals(CRunPVToolkit.performanceLevelCompareFirstState)) {
			src = zulfilepath + "star-filled-blue.svg";
		} else if (performanceLevelState.equals(CRunPVToolkit.performanceLevelCompareSecondState)) {
			src = zulfilepath + "star-filled-yellow.svg";
		} else if (performanceLevelState.equals(CRunPVToolkit.performanceLevelNoScoreState)) {
			src = zulfilepath + "star-gray.svg";
		}
		new CRunPVToolkitDefImage(parent, new String[] { "class", "src" }, new Object[] { "scoreLevelStar", src });
	}

	public List<String> getPerformanceLevelColors(List<String> performanceLevelStates, int skillclusterCounter) {
		List<String> performanceLevelColors = new ArrayList<String>();
		for (String performanceLevelState : performanceLevelStates) {
			if (performanceLevelState.equals(CRunPVToolkit.performanceLevelAchievedState)) {
				performanceLevelColors.add(CRunPVToolkit.performanceLevelAchievedColor);
			} else if (performanceLevelState.equals(CRunPVToolkit.performanceLevelImprovedState)) {
				performanceLevelColors.add(CRunPVToolkit.performanceLevelImprovedColor);
			} else if (performanceLevelState.equals(CRunPVToolkit.performanceLevelDeterioratedState)) {
				performanceLevelColors.add(CRunPVToolkit.performanceLevelDeterioratedColor);
			} else if (performanceLevelState.equals(CRunPVToolkit.performanceLevelCompareBothState)) {
				performanceLevelColors.add(CRunPVToolkit.performanceLevelCompareBothColor);
			} else if (performanceLevelState.equals(CRunPVToolkit.performanceLevelCompareFirstState)) {
				performanceLevelColors.add(CRunPVToolkit.performanceLevelCompareFirstColor);
			} else if (performanceLevelState.equals(CRunPVToolkit.performanceLevelCompareSecondState)) {
				performanceLevelColors.add(CRunPVToolkit.performanceLevelCompareSecondColor);
			} else if (performanceLevelState.equals(CRunPVToolkit.performanceLevelNoScoreState)) {
				performanceLevelColors.add(getPerformanceLevelSkillClusterColors()[skillclusterCounter]);
			}
		}
		return performanceLevelColors;
	}

	public List<CRunPVToolkitFeedbackTipOrTop> getPerformanceTipsOrTops(IERunGroup actor, IXMLTag skillTag,
			List<List<IXMLTag>> sharedFeedbackTagsPerFeedbackStep, List<IXMLTag> skillExampleExpertTags,
			IXMLTag skillExampleFeedbackTag, boolean feedbackTagsAreStatusTags, int cycleNumber, String giveTipsOrTops,
			boolean compareWithExperts) {

		if (!compareWithExperts) {
			return getPerformanceTipsOrTops(actor, skillTag, sharedFeedbackTagsPerFeedbackStep,
					feedbackTagsAreStatusTags, cycleNumber, giveTipsOrTops);
		} else {
			return getPerformanceTipsOrTops(actor, skillTag, skillExampleExpertTags, skillExampleFeedbackTag,
					feedbackTagsAreStatusTags, cycleNumber, giveTipsOrTops);
		}
	}

	public List<CRunPVToolkitFeedbackTipOrTop> getPerformanceTipsOrTops(IERunGroup actor, IXMLTag skillTag,
			List<List<IXMLTag>> sharedFeedbackTagsPerFeedbackStep, boolean feedbackTagsAreStatusTags, int cycleNumber,
			String giveTipsOrTops) {
		List<CRunPVToolkitFeedbackTipOrTop> performanceTipsOrTops = new ArrayList<CRunPVToolkitFeedbackTipOrTop>();

		String feedbackLevelOfTipsAndTops = getFeedbackLevelOfTipsAndTops(getCurrentMethodTag(), getCurrentCycleTag());
		boolean mayTipsAndTopsSkill = getMayTipsAndTopsSkill(getCurrentMethodTag(), getCurrentCycleTag());
		boolean skillclusterHasTipsTops = feedbackLevelOfTipsAndTops.equals("skillcluster");
		boolean subskillHasTipsTops = feedbackLevelOfTipsAndTops.equals("subskill");
		boolean skillHasTipsTops = feedbackLevelOfTipsAndTops.equals("skill") || mayTipsAndTopsSkill;

		// NOTE tips and tops may be both on skill level and/or skill cluster/sub skill
		// level
		if (skillHasTipsTops) {
			performanceTipsOrTops = getPerformanceTipsOrTops(actor, performanceTipsOrTops, skillTag, null, 0, null,
					sharedFeedbackTagsPerFeedbackStep, feedbackTagsAreStatusTags, cycleNumber, giveTipsOrTops);
		}

		int numberOfSkillClusters = getNumberOfSkillClusters(getCurrentRubricsCaseComponent(), skillTag);
		List<IXMLTag> parentTags = new ArrayList<IXMLTag>();
		// NOTE a rubric contains either skill clusters, directly under the skill tag,
		// and sub skills within them or only sub skills directly under the skill tag. A
		// mixture is not allowed.
		if (numberOfSkillClusters > 0) {
			parentTags = skillTag.getChilds("skillcluster");
		} else {
			parentTags.add(skillTag);
		}

		int skillClusterNumber = 1;
		for (IXMLTag parentTag : parentTags) {
			if (isRunSpecificTagPresent(new CRunPVToolkitCacAndTag(getCurrentRubricsCaseComponent(), parentTag))) {
				if (skillclusterHasTipsTops) {
					if (parentTag.getName().equals("skillcluster")) {
						performanceTipsOrTops = getPerformanceTipsOrTops(actor, performanceTipsOrTops, skillTag,
								parentTag, skillClusterNumber, null, sharedFeedbackTagsPerFeedbackStep,
								feedbackTagsAreStatusTags, cycleNumber, giveTipsOrTops);
					}
				} else if (subskillHasTipsTops) {
					for (IXMLTag subSkillTag : parentTag.getChilds("subskill")) {
						if (isRunSpecificTagPresent(
								new CRunPVToolkitCacAndTag(getCurrentRubricsCaseComponent(), subSkillTag))) {
							performanceTipsOrTops = getPerformanceTipsOrTops(actor, performanceTipsOrTops, skillTag,
									parentTag, skillClusterNumber, subSkillTag, sharedFeedbackTagsPerFeedbackStep,
									feedbackTagsAreStatusTags, cycleNumber, giveTipsOrTops);
						}
					}
				}
				if (parentTag.getName().equals("skillcluster")) {
					skillClusterNumber++;
				}
			}
		}
		return performanceTipsOrTops;
	}

	public List<CRunPVToolkitFeedbackTipOrTop> getPerformanceTipsOrTops(IERunGroup actor, IXMLTag skillTag,
			List<IXMLTag> skillExampleExpertTags, IXMLTag skillExampleFeedbackTag, boolean feedbackTagsAreStatusTags,
			int cycleNumber, String giveTipsOrTops) {
		List<CRunPVToolkitFeedbackTipOrTop> performanceTipsOrTops = new ArrayList<CRunPVToolkitFeedbackTipOrTop>();

		String feedbackLevelOfTipsAndTops = getFeedbackLevelOfTipsAndTops(getCurrentMethodTag(), getCurrentCycleTag());
		boolean mayTipsAndTopsSkill = getMayTipsAndTopsSkill(getCurrentMethodTag(), getCurrentCycleTag());
		boolean skillclusterHasTipsTops = feedbackLevelOfTipsAndTops.equals("skillcluster");
		boolean subskillHasTipsTops = feedbackLevelOfTipsAndTops.equals("subskill");
		boolean skillHasTipsTops = feedbackLevelOfTipsAndTops.equals("skill") || mayTipsAndTopsSkill;

		// NOTE tips and tops may be both on skill level and/or skill cluster/sub skill
		// level
		if (skillHasTipsTops) {
			performanceTipsOrTops = getPerformanceTipsOrTops(actor, performanceTipsOrTops, skillTag, null, 0, null,
					skillExampleExpertTags, skillExampleFeedbackTag, giveTipsOrTops);
		}

		int numberOfSkillClusters = getNumberOfSkillClusters(getCurrentRubricsCaseComponent(), skillTag);
		List<IXMLTag> parentTags = new ArrayList<IXMLTag>();
		// NOTE a rubric contains either skill clusters, directly under the skill tag,
		// and sub skills within them or only sub skills directly under the skill tag. A
		// mixture is not allowed.
		if (numberOfSkillClusters > 0) {
			parentTags = skillTag.getChilds("skillcluster");
		} else {
			parentTags.add(skillTag);
		}

		int skillClusterNumber = 1;
		for (IXMLTag parentTag : parentTags) {
			if (isRunSpecificTagPresent(new CRunPVToolkitCacAndTag(getCurrentRubricsCaseComponent(), parentTag))) {
				if (skillclusterHasTipsTops) {
					if (parentTag.getName().equals("skillcluster")) {
						performanceTipsOrTops = getPerformanceTipsOrTops(actor, performanceTipsOrTops, skillTag,
								parentTag, skillClusterNumber, null, skillExampleExpertTags, skillExampleFeedbackTag,
								giveTipsOrTops);
					}
				} else if (subskillHasTipsTops) {
					for (IXMLTag subSkillTag : parentTag.getChilds("subskill")) {
						if (isRunSpecificTagPresent(
								new CRunPVToolkitCacAndTag(getCurrentRubricsCaseComponent(), subSkillTag))) {
							performanceTipsOrTops = getPerformanceTipsOrTops(actor, performanceTipsOrTops, skillTag,
									parentTag, skillClusterNumber, subSkillTag, skillExampleExpertTags,
									skillExampleFeedbackTag, giveTipsOrTops);
						}
					}
				}
				skillClusterNumber++;
			}
		}
		return performanceTipsOrTops;
	}

	protected List<CRunPVToolkitFeedbackTipOrTop> getPerformanceTipsOrTops(IERunGroup actor,
			List<CRunPVToolkitFeedbackTipOrTop> performanceTipsOrTops, IXMLTag skillTag, IXMLTag skillClusterTag,
			int skillClusterNumber, IXMLTag subSkillTag, List<List<IXMLTag>> sharedFeedbackTagsPerFeedbackStep,
			boolean feedbackTagsAreStatusTags, int cycleNumber, String giveTipsOrTops) {

		if (cycleNumber < 1 || cycleNumber > sharedFeedbackTagsPerFeedbackStep.size()) {
			return performanceTipsOrTops;
		}

		IXMLTag tempSkillTag = null;
		if (subSkillTag != null) {
			tempSkillTag = subSkillTag;
		} else if (skillClusterTag != null) {
			tempSkillTag = skillClusterTag;
		} else if (skillTag != null) {
			tempSkillTag = skillTag;
		}
		if (tempSkillTag == null) {
			return performanceTipsOrTops;
		}

		for (IXMLTag feedbackTag : sharedFeedbackTagsPerFeedbackStep.get(cycleNumber - 1)) {
			IERunGroupAccount runGroupAccount = getRunGroupAccount(feedbackTag.getAttribute("feedbackrgaid"));
			if (runGroupAccount != null) {
				IXMLTag feedbackStepTag = getStepTagByChildTagOfStatusTag(runGroupAccount.getERunGroup(), feedbackTag);
				String value = "";
				if (tempSkillTag != null && giveTipsOrTops.equals("tips")) {
					value = getFeedbackTips(runGroupAccount.getERunGroup(), feedbackStepTag, feedbackTag,
							feedbackTagsAreStatusTags, tempSkillTag);
				}
				if (tempSkillTag != null && giveTipsOrTops.equals("tops")) {
					value = getFeedbackTops(runGroupAccount.getERunGroup(), feedbackStepTag, feedbackTag,
							feedbackTagsAreStatusTags, tempSkillTag);
				}
				if (!value.equals("")) {
					CRunPVToolkitFeedbackTipOrTop tipOrTop = new CRunPVToolkitFeedbackTipOrTop();
					performanceTipsOrTops.add(tipOrTop);
					tipOrTop.setSkillTag(skillTag);
					tipOrTop.setSkillClusterTag(skillClusterTag);
					tipOrTop.setSkillClusterNumber(skillClusterNumber);
					tipOrTop.setSubSkillTag(subSkillTag);
					tipOrTop.setStepTag(feedbackStepTag);
					tipOrTop.setFeedbackTag(feedbackTag);
					tipOrTop.setFeedbackTagIsStatusTag(feedbackTagsAreStatusTags);
					tipOrTop.setPeer(runGroupAccount);
					tipOrTop.setFeedbackName(runGroupAccount.getERunGroup().getName());
					String peerRole = getPeerGroupFeedbackGiverRole(actor, runGroupAccount.getERunGroup());
					// NOTE student does not see student-assistant in tips/tops but teacher
					if (hasStudentRole && peerRole.equals(peerGroupStudentAssistantRole)) {
						peerRole = peerGroupTeacherRole;
					}
					tipOrTop.setFeedbackRole(vView.getLabel("PV-toolkit.peergroup.member.role." + peerRole));
					tipOrTop.setShareDate(getStatusChildTagAttribute(feedbackTag, "sharedate"));
					tipOrTop.setShareTime(getStatusChildTagAttribute(feedbackTag, "sharetime"));
					tipOrTop.setSkillHasTipsTops(tempSkillTag.getName().equals("skill"));
					tipOrTop.setSkillclusterHasTipsTops(tempSkillTag.getName().equals("skillcluster"));
					tipOrTop.setSubskillHasTipsTops(tempSkillTag.getName().equals("subskill"));
					tipOrTop.setTip(giveTipsOrTops.equals("tips"));
					tipOrTop.setTop(giveTipsOrTops.equals("tops"));
					tipOrTop.setValue(value);
					tipOrTop.setMark(getMarkedFeedbackTipsOrTops(actor, tipOrTop));

				}
			}
		}
		return performanceTipsOrTops;
	}

	protected List<CRunPVToolkitFeedbackTipOrTop> getPerformanceTipsOrTops(IERunGroup actor,
			List<CRunPVToolkitFeedbackTipOrTop> performanceTipsOrTops, IXMLTag skillTag, IXMLTag skillClusterTag,
			int skillClusterNumber, IXMLTag subSkillTag, List<IXMLTag> skillExampleExpertTags,
			IXMLTag skillExampleFeedbackTag, String giveTipsOrTops) {

		IXMLTag tempSkillTag = null;
		if (subSkillTag != null) {
			tempSkillTag = subSkillTag;
		} else if (skillClusterTag != null) {
			tempSkillTag = skillClusterTag;
		} else if (skillTag != null) {
			tempSkillTag = skillTag;
		}
		if (tempSkillTag == null) {
			return performanceTipsOrTops;
		}

		// NOTE feedback step is feedback step in first cycle because comparing rating
		// video example of whole skill is done within prepare step and progress is
		// shared between prepare steps.
		IXMLTag feedbackStepTag = getFirstStepTag(CRunPVToolkit.feedbackStepType);

		for (IXMLTag skillExampleExpertTag : skillExampleExpertTags) {
			String value = "";
			if (tempSkillTag != null && giveTipsOrTops.equals("tips")) {
				value = getSkillExampleExpertTips(skillExampleExpertTag, tempSkillTag);
			}
			if (tempSkillTag != null && giveTipsOrTops.equals("tops")) {
				value = getSkillExampleExpertTops(skillExampleExpertTag, tempSkillTag);
			}
			if (!value.equals("")) {
				CRunPVToolkitFeedbackTipOrTop tipOrTop = new CRunPVToolkitFeedbackTipOrTop();
				performanceTipsOrTops.add(tipOrTop);
				tipOrTop.setSkillTag(skillTag);
				tipOrTop.setSkillClusterTag(skillClusterTag);
				tipOrTop.setSkillClusterNumber(skillClusterNumber);
				tipOrTop.setSubSkillTag(subSkillTag);
				tipOrTop.setStepTag(feedbackStepTag);
				tipOrTop.setFeedbackTag(skillExampleExpertTag);
				tipOrTop.setFeedbackTagIsStatusTag(false);
				tipOrTop.setPeer(null);
				tipOrTop.setFeedbackName(sSpring.unescapeXML(skillExampleExpertTag.getChildValue("name"))
						.replace(AppConstants.statusCommaReplace, ","));
				tipOrTop.setFeedbackRole(vView.getLabel("PV-toolkit.peergroup.member.role.expert"));
				tipOrTop.setShareDate("");
				tipOrTop.setShareTime("");
				tipOrTop.setSkillHasTipsTops(tempSkillTag.getName().equals("skill"));
				tipOrTop.setSkillclusterHasTipsTops(tempSkillTag.getName().equals("skillcluster"));
				tipOrTop.setSubskillHasTipsTops(tempSkillTag.getName().equals("subskill"));
				tipOrTop.setTip(giveTipsOrTops.equals("tips"));
				tipOrTop.setTop(giveTipsOrTops.equals("tops"));
				tipOrTop.setValue(value);
				tipOrTop.setMark(getMarkedFeedbackTipsOrTops(actor, tipOrTop));

			}
		}

		IERunGroupAccount runGroupAccount = getRunGroupAccount(skillExampleFeedbackTag.getAttribute("feedbackrgaid"));
		if (runGroupAccount != null) {
			String value = "";
			if (tempSkillTag != null && giveTipsOrTops.equals("tips")) {
				value = getFeedbackTips(runGroupAccount.getERunGroup(), feedbackStepTag, skillExampleFeedbackTag, true,
						tempSkillTag);
			}
			if (tempSkillTag != null && giveTipsOrTops.equals("tops")) {
				value = getFeedbackTops(runGroupAccount.getERunGroup(), feedbackStepTag, skillExampleFeedbackTag, true,
						tempSkillTag);
			}
			if (!value.equals("")) {
				CRunPVToolkitFeedbackTipOrTop tipOrTop = new CRunPVToolkitFeedbackTipOrTop();
				performanceTipsOrTops.add(tipOrTop);
				tipOrTop.setSkillTag(skillTag);
				tipOrTop.setSkillClusterTag(skillClusterTag);
				tipOrTop.setSkillClusterNumber(skillClusterNumber);
				tipOrTop.setSubSkillTag(subSkillTag);
				tipOrTop.setStepTag(feedbackStepTag);
				tipOrTop.setFeedbackTag(skillExampleFeedbackTag);
				tipOrTop.setFeedbackTagIsStatusTag(true);
				tipOrTop.setPeer(runGroupAccount);
				tipOrTop.setFeedbackName(runGroupAccount.getERunGroup().getName());
				String peerRole = getPeerGroupFeedbackGiverRole(actor, runGroupAccount.getERunGroup());
				tipOrTop.setFeedbackRole(vView.getLabel("PV-toolkit.peergroup.member.role." + peerRole));
				tipOrTop.setShareDate(getStatusChildTagAttribute(skillExampleFeedbackTag, "sharedate"));
				tipOrTop.setShareTime(getStatusChildTagAttribute(skillExampleFeedbackTag, "sharetime"));
				tipOrTop.setSkillHasTipsTops(tempSkillTag.getName().equals("skill"));
				tipOrTop.setSkillclusterHasTipsTops(tempSkillTag.getName().equals("skillcluster"));
				tipOrTop.setSubskillHasTipsTops(tempSkillTag.getName().equals("subskill"));
				tipOrTop.setTip(giveTipsOrTops.equals("tips"));
				tipOrTop.setTop(giveTipsOrTops.equals("tops"));
				tipOrTop.setValue(value);
				tipOrTop.setMark(getMarkedFeedbackTipsOrTops(actor, tipOrTop));
			}
		}
		return performanceTipsOrTops;
	}

	/* MY RECORDINGS */

//NOTE my recordings are related to all practice steps within the current method tag, and related to and stored for the current run group given by getCurrentRunGroup()
//Every practice has an attribute 'practiceuuid' containing a UUID. New recordings cannot be made. Only for not shared recordings edit (of name) and delete are available.

	public CRunPVToolkitCacAndTags getMyRecordings(IERunGroup runGroup) {
		// NOTE get all recordings within all practice steps within the current method
		return new CRunPVToolkitCacAndTags(getCurrentProjectsCaseComponent(),
				getRunStepTagStatusChildTags(runGroup, practiceStepType, AppConstants.statusTypeRunGroup));
	}

	/* SUPER TEACHER */

//NOTE there will be only one super teacher per run. For non stand-alone runs an admin can upgrade one teacher to super teacher.
//For stand-alone runs a new user will be automatically super teacher, because he must be able to run a method himself.
//settings for the super teacher are saved in run status, not in run group status, so settings will be available to all users.
//these settings are saved for the current method (tag).
//NOTE there are three role types within the run: student, teacher and peerstudent:	
//student: is member of peer group and walks through all step types.
//teacher: may be member of peer group and only walks through feedback steps. May inspect peer groups as well. 
//peerstudent: may be member of peer group and only walks through feedback steps. 
//If stand alone use of PV-tool all three types may be super teacher as well.			

	public CRunPVToolkitCacAndTags getPeerGroupTags(IERunGroup runGroup) {		
		if (_peerGroupTags == null) {
			CRunPVToolkitCacAndTag cacAndTag = new CRunPVToolkitCacAndTag(getCurrentProjectsCaseComponent(),
					getRunProjectsStatusChildTag(runGroup, getCurrentMethodTag(), AppConstants.statusTypeRun));
			List<IXMLTag> xmlTags;
			if (cacAndTag.getXmlTag() == null) {
				xmlTags = new ArrayList<IXMLTag>();
			} else {
				xmlTags = cacAndTag.getXmlTag().getChilds("peergroup");
			}
			_peerGroupTags = new CRunPVToolkitCacAndTags(getCurrentProjectsCaseComponent(), xmlTags);

		}
		return _peerGroupTags;
	}

	public List<CRunPVToolkitPeerGroup> getPeerGroups(List<IXMLTag> peerGroupTags) {
		if (_peerGroups == null) {
			_peerGroups = new ArrayList<CRunPVToolkitPeerGroup>();
			for (IXMLTag peerGroupTag : peerGroupTags) {
				IXMLTag statusChildTag = getStatusChildTag(peerGroupTag);
				String name = sSpring
						.unescapeXML(statusChildTag.getAttribute("name").replace(AppConstants.statusCommaReplace, ","));
				String scope = sSpring.unescapeXML(
						statusChildTag.getAttribute("scope").replace(AppConstants.statusCommaReplace, ","));
				List<CRunPVToolkitPeerGroupMember> peerGroupMembers = new ArrayList<CRunPVToolkitPeerGroupMember>();
				for (IXMLTag memberTag : statusChildTag.getChilds("member")) {
					String rgaId = getStatusChildTagAttribute(memberTag, "rgaid");
					String role = getStatusChildTagAttribute(memberTag, "role");
					IERunGroupAccount runGroupAccount = getRunGroupAccount(rgaId);
					if (runGroupAccount != null) {
						peerGroupMembers.add(new CRunPVToolkitPeerGroupMember(runGroupAccount, role));
					}
				}
				_peerGroups.add(new CRunPVToolkitPeerGroup(peerGroupTag, name, scope, peerGroupMembers));
			}
		}
		return _peerGroups;
	}

	public boolean isMemberOfPeerGroup(CRunPVToolkitPeerGroup peerGroup, IERunGroupAccount runGroupAccount) {
		for (CRunPVToolkitPeerGroupMember peerGroupMember : peerGroup.getPeerGroupMembers()) {
			if (peerGroupMember.getRunGroupAccount().getRgaId() == runGroupAccount.getRgaId()) {
				return true;
			}
		}
		return false;
	}

	public boolean savePeerGroup(IERunGroup runGroup, IXMLTag peerGroupTag, String name, String scope,
			List<CRunPVToolkitPeerGroupMember> peerGroupMembers) {
		CRunPVToolkitCacAndTag cacAndTag = new CRunPVToolkitCacAndTag(getCurrentProjectsCaseComponent(),
				getRunProjectsStatusChildTag(runGroup, getCurrentMethodTag(), AppConstants.statusTypeRun));
		if (peerGroupTag == null) {
			peerGroupTag = getNewOrExistingRunStatusChildTag(cacAndTag.getXmlTag(), "peergroup", null, true, true,
					true);
		}
		IXMLTag statusChildTag = getStatusChildTag(peerGroupTag);
		statusChildTag.setAttribute("name", sSpring.escapeXML(name).replace(",", AppConstants.statusCommaReplace));
		statusChildTag.setAttribute("scope", sSpring.escapeXML(scope).replace(",", AppConstants.statusCommaReplace));

		// remove existing peer group members
		statusChildTag.getChildTags().clear();
		// add tags
		for (CRunPVToolkitPeerGroupMember peerGroupMember : peerGroupMembers) {
			IXMLTag peerGroupMemberTag = getNewOrExistingRunStatusChildTag(statusChildTag, "member", null, true, true,
					true);
			IXMLTag memberStatusChildTag = getStatusChildTag(peerGroupMemberTag);
			memberStatusChildTag.setAttribute("rgaid", "" + peerGroupMember.getRunGroupAccount().getRgaId());
			memberStatusChildTag.setAttribute("role", peerGroupMember.getRole());
		}

		savePeerGroups();

		// NOTE clear cached peer group tags
		_peerGroupTags = null;

		return true;
	}

	public List<IXMLTag> getPeerGroupMemberTags(IXMLTag peerGroupTag) {
		if (peerGroupTag == null) {
			return null;
		}
		return getStatusChildTag(peerGroupTag).getChilds("member");
	}

	public IXMLTag getPeerGroupMemberTag(IXMLTag peerGroupTag, IERunGroupAccount runGroupAccount) {
		if (peerGroupTag == null) {
			return null;
		}
		String rgaId = "" + runGroupAccount.getRgaId();
		IXMLTag statusChildTag = getStatusChildTag(peerGroupTag);

		for (IXMLTag peerGroupMemberTag : statusChildTag.getChilds("member")) {
			if (getStatusChildTagAttribute(peerGroupMemberTag, "rgaid").equals(rgaId)) {
				return peerGroupMemberTag;
			}
		}

		return null;
	}

	public boolean deletePeerGroup(IXMLTag peerGroupTag) {
		peerGroupTag.getParentTag().getChildTags().remove(peerGroupTag);

		savePeerGroups();

		// NOTE clear cached peer group tags
		_peerGroupTags = null;

		return true;
	}

	public void savePeerGroups() {
		// stores changes in peer groups in database for current method, methods are
		// part of projects case component
		// note that changes are stored in run status, not in run group status
		saveStatusChange(getCurrentProjectsCaseComponent(), AppConstants.statusTypeRun);
	}

	private List<Object> getCommonPeerGroupTagsOrRunGroups(IERunGroup runGroup, IERunGroup otherRunGroup,
			List<String> otherRolesToFilterOn, List<Integer> otherRgaIdsToFilterOn, String resultType, Boolean pIncludeSuperTeacher, boolean pIncludeSelf) {
		return getPeerGroupTagsOrRunGroups(runGroup, otherRunGroup, otherRolesToFilterOn, otherRgaIdsToFilterOn, resultType, pIncludeSuperTeacher, pIncludeSelf, false);
	}
	
	private List<Object> getAllPeerGroupTagsOrRunGroups(IERunGroup runGroup, IERunGroup otherRunGroup,
			List<String> otherRolesToFilterOn, List<Integer> otherRgaIdsToFilterOn, String resultType, Boolean pIncludeSuperTeacher, boolean pIncludeSelf) {
		return getPeerGroupTagsOrRunGroups(runGroup, otherRunGroup, otherRolesToFilterOn, otherRgaIdsToFilterOn, resultType, pIncludeSuperTeacher, pIncludeSelf, true);
	}
	
	private List<Object> getPeerGroupTagsOrRunGroups(IERunGroup runGroup, IERunGroup otherRunGroup,
			List<String> otherRolesToFilterOn, List<Integer> otherRgaIdsToFilterOn, String resultType, Boolean pIncludeSuperTeacher, boolean pIncludeSelf, boolean pAllPGTagsOrRGroups) {
		List<Object> results = new ArrayList<Object>();
		CRunPVToolkitCacAndTags cacAndTags = getPeerGroupTags(runGroup);
		List<IERunGroupAccount> runGroupAccounts = getRunGroupAccounts(runGroup);
		if (runGroupAccounts.size() == 0) {
			return null;
		}

		boolean filterOnOtherRole = otherRolesToFilterOn != null;
		boolean filterOnOtherRgaIds = otherRgaIdsToFilterOn != null;
		// NOTE assume only one run group account exists
		IERunGroupAccount runGroupAccount = runGroupAccounts.get(0);

		List<CRunPVToolkitPeerGroup> peerGroups = getPeerGroups(cacAndTags.getXmlTags());

		// store rga id of run group account
		int rgaId = runGroupAccount.getRgaId();

		if (pIncludeSuperTeacher)
			pIncludeSuperTeacher = isSuperTeacher();

		// loop through peer group tags
		for (CRunPVToolkitPeerGroup peerGroup : peerGroups) {
			// NOTE if pAllPGTagsOrRGroups, then don't check if peer is member of same peer group as runGroup, so just return all peer group tags or members of all peer groups
			if (pIncludeSuperTeacher || pAllPGTagsOrRGroups || isMemberOfPeerGroup(peerGroup, runGroupAccount)) {
				// if run group account is part of peer group
				if ((resultType.equals(resultTypePeerGroupTag) && !results.contains(peerGroup.getPeerGroupTag()))
						|| resultType.equals(resultTypeRunGroup)) {
					// NOTE if peerGroupTagType only one peer group member has to be checked. If at
					// least one found the peer group is added
					// NOTE if runGroupType all peer group member have to be added
					// get all peer group members
					for (CRunPVToolkitPeerGroupMember peerGroupMember : peerGroup.getPeerGroupMembers()) {
						// get member rga
						IERunGroupAccount member = peerGroupMember.getRunGroupAccount();
						boolean ok = pIncludeSelf || (member.getRgaId() != rgaId);
						if (ok && filterOnOtherRole) {
							// get role of peer group member
							String memberRole = peerGroupMember.getRole();
							boolean memberRoleOk = false;
							for (String otherRoleToFilterOn : otherRolesToFilterOn) {
								if (otherRoleToFilterOn.equals(memberRole)) {
									memberRoleOk = true;
									break;
								}
							}
							ok = ok && memberRoleOk;
						}
						if (ok) {
							// if member is other rga
							if (member != null && (otherRunGroup == null
									|| member.getERunGroup().getRugId() == otherRunGroup.getRugId())) {
								// if not filter on rgaId add right away
								boolean add = true;
								if (filterOnOtherRgaIds) {
									// otherwise check if member has correct role
									add = otherRgaIdsToFilterOn.contains(member.getRgaId());
								}
								if (add) {
									// either return peer group tags or run groups
									if (resultType.equals(resultTypePeerGroupTag)) {
										if (!results.contains(peerGroup.getPeerGroupTag())) {
											results.add(peerGroup.getPeerGroupTag());
										}
										break;
									} else if (resultType.equals(resultTypeRunGroup)) {
										if (!results.contains(member.getERunGroup())) {
											results.add(member.getERunGroup());
										}
									}
								}
							}
						}
					}
				}
			}
		}

		return results;
	}

	public List<IXMLTag> getCommonPeerGroupTags(IERunGroup runGroup, IERunGroup otherrunGroup,
			List<String> otherRolesToFilterOn, List<Integer> otherRgaIdsToFilterOn, Boolean pIncludeSuperTeacher) {
		return getCommonPeerGroupTags(runGroup, otherrunGroup,
				otherRolesToFilterOn, otherRgaIdsToFilterOn, pIncludeSuperTeacher, false);
	}

	public List<IXMLTag> getCommonPeerGroupTags(IERunGroup runGroup, IERunGroup otherrunGroup,
			List<String> otherRolesToFilterOn, List<Integer> otherRgaIdsToFilterOn, Boolean pIncludeSuperTeacher, boolean pIncludeSelf) {
		List<Object> objects = getCommonPeerGroupTagsOrRunGroups(runGroup, otherrunGroup, otherRolesToFilterOn,
				otherRgaIdsToFilterOn, resultTypePeerGroupTag, pIncludeSuperTeacher, pIncludeSelf);
		List<IXMLTag> peerGroupTags = new ArrayList<IXMLTag>();
		for (Object object : objects) {
			if (object instanceof IXMLTag) {
				peerGroupTags.add((IXMLTag) object);
			}
		}
		return peerGroupTags;
	}

	public IXMLTag getCommonPeerGroupTag(IERunGroup runGroup, IERunGroup otherrunGroup) {
		List<IXMLTag> peerGroupTags = getCommonPeerGroupTags(runGroup, otherrunGroup, null, null, false);
		if (peerGroupTags.size() == 0) {
			return null;
		} else {
			return peerGroupTags.get(0);
		}
	}

	public List<IERunGroup> getPeerRunGroups(IERunGroup runGroup, IERunGroup otherrunGroup,
			List<String> otherRolesToFilterOn, List<Integer> otherRgaIdsToFilterOn) {
		List<Object> objects = getCommonPeerGroupTagsOrRunGroups(runGroup, otherrunGroup, otherRolesToFilterOn,
				otherRgaIdsToFilterOn, resultTypeRunGroup, true, false);
		List<IERunGroup> runGroups = new ArrayList<IERunGroup>();
		for (Object object : objects) {
			if (object instanceof IERunGroup) {
				runGroups.add((IERunGroup) object);
			}
		}
		return runGroups;
	}

	public List<IERunGroup> getAllPeerGroupsRunGroups(IERunGroup runGroup, IERunGroup otherrunGroup,
			List<String> otherRolesToFilterOn, List<Integer> otherRgaIdsToFilterOn) {
		List<Object> objects = getAllPeerGroupTagsOrRunGroups(runGroup, otherrunGroup, otherRolesToFilterOn,
				otherRgaIdsToFilterOn, resultTypeRunGroup, true, false);
		List<IERunGroup> runGroups = new ArrayList<IERunGroup>();
		for (Object object : objects) {
			if (object instanceof IERunGroup) {
				runGroups.add((IERunGroup) object);
			}
		}
		return runGroups;
	}

	public Map<CRunPVToolkitPeerGroup, List<IERunGroup>> getPeergroupsPeerRunGroups(IERunGroup runGroup, List<String> otherRolesToFilterOn) {
		Map<CRunPVToolkitPeerGroup, List<IERunGroup>> lResultMap = new HashMap<CRunPVToolkitPeerGroup, List<IERunGroup>>();
		CRunPVToolkitCacAndTags cacAndTags = getPeerGroupTags(runGroup);
		List<IERunGroupAccount> runGroupAccounts = getRunGroupAccounts(runGroup);
		if (runGroupAccounts.size() == 0) {
			return null;
		}

		boolean filterOnOtherRole = otherRolesToFilterOn != null;
		//NOTE assume only one run group account exists
		IERunGroupAccount runGroupAccount = runGroupAccounts.get(0);
		
		List<CRunPVToolkitPeerGroup> peerGroups = getPeerGroups(cacAndTags.getXmlTags());
		
		//store rga id of run group account
		int rgaId = runGroupAccount.getRgaId();

		boolean isSuperTeacher = isSuperTeacher();

		//loop through peer group tags
		for (CRunPVToolkitPeerGroup peerGroup : peerGroups) {
			if (isSuperTeacher || isMemberOfPeerGroup(peerGroup, runGroupAccount)) {
				//if run group account is part of peer group
				List<IERunGroup> results = new ArrayList<IERunGroup>();
				for (CRunPVToolkitPeerGroupMember peerGroupMember : peerGroup.getPeerGroupMembers()) {
					//get member rga
					IERunGroupAccount member = peerGroupMember.getRunGroupAccount();
					boolean ok = member.getRgaId() != rgaId;
					if (ok && filterOnOtherRole) {
						//get role of peer group member
						String memberRole = peerGroupMember.getRole();
						boolean memberRoleOk = false;
						for (String otherRoleToFilterOn : otherRolesToFilterOn) {
							if (otherRoleToFilterOn.equals(memberRole)) {
								memberRoleOk = true;
								break;
							}
						}
						ok = ok && memberRoleOk;
					}
					if (ok) {
						results.add(member.getERunGroup());
					}
				}
				lResultMap.put(peerGroup, results);
			}
		}
		return lResultMap;
	}

	public List<IXMLTag> getPeerGroupMemberTags(IERunGroup runGroup) {
		List<IXMLTag> peerGroupMemberTags = new ArrayList<IXMLTag>();
		List<IERunGroupAccount> tempRunGroupAccounts = runGroupAccountManager
				.getAllRunGroupAccountsByRugId(runGroup.getRugId());
		if (tempRunGroupAccounts.size() == 0) {
			return peerGroupMemberTags;
		}
		CRunPVToolkitCacAndTags cacAndTags = getPeerGroupTags(runGroup);
		for (IXMLTag peerGroupTag : cacAndTags.getXmlTags()) {
			IXMLTag peerGroupMemberTag = null;
			// NOTE assume only one run group account exists
			peerGroupMemberTag = getPeerGroupMemberTag(peerGroupTag, tempRunGroupAccounts.get(0));
			if (peerGroupMemberTag != null) {
				peerGroupMemberTags.add(peerGroupMemberTag);
			}
		}
		return peerGroupMemberTags;
	}

	public String getPeerGroupMemberRole(IXMLTag peerGroupTag, IERunGroup runGroup) {
		List<IERunGroupAccount> tempRunGroupAccounts = runGroupAccountManager
				.getAllRunGroupAccountsByRugId(runGroup.getRugId());
		if (tempRunGroupAccounts.size() == 0) {
			return "";
		}
		IXMLTag peerGroupMemberTag = null;
		// NOTE assume only one run group account exists
		peerGroupMemberTag = getPeerGroupMemberTag(peerGroupTag, tempRunGroupAccounts.get(0));
		if (peerGroupMemberTag != null) {
			return getStatusChildTagAttribute(peerGroupMemberTag, "role");
		}
		return "";
	}

	public List<IXMLTag> getOtherPeerGroupMemberTags(IERunGroup runGroup) {
		List<IXMLTag> peerGroupMemberTags = new ArrayList<IXMLTag>();
		List<IERunGroupAccount> tempRunGroupAccounts = runGroupAccountManager
				.getAllRunGroupAccountsByRugId(runGroup.getRugId());
		if (tempRunGroupAccounts.size() == 0) {
			return peerGroupMemberTags;
		}
		String rgaId = "" + tempRunGroupAccounts.get(0).getRgaId();
		// get all peer groups
		CRunPVToolkitCacAndTags cacAndAllPeerGroupTags = getPeerGroupTags(runGroup);
		// get peer groups runGroup is member of
		List<IXMLTag> runGroupPeerGroupTags = new ArrayList<IXMLTag>();
		for (IXMLTag peerGroupTag : cacAndAllPeerGroupTags.getXmlTags()) {
			if (!runGroupPeerGroupTags.contains(peerGroupTag)) {
				IXMLTag statusChildTag = getStatusChildTag(peerGroupTag);
				for (IXMLTag peerGroupMemberTag : statusChildTag.getChilds("member")) {
					if (getStatusChildTagAttribute(peerGroupMemberTag, "rgaid").equals(rgaId)) {
						runGroupPeerGroupTags.add(peerGroupTag);
					}
				}
			}
		}
		// loop through peer groups run group is member of to get other members
		for (IXMLTag peerGroupTag : runGroupPeerGroupTags) {
			IXMLTag statusChildTag = getStatusChildTag(peerGroupTag);
			for (IXMLTag peerGroupMemberTag : statusChildTag.getChilds("member")) {
				if (!getStatusChildTagAttribute(peerGroupMemberTag, "rgaid").equals(rgaId)) {
					peerGroupMemberTags.add(peerGroupMemberTag);
				}
			}
		}
		return peerGroupMemberTags;
	}

	public IXMLTag getPeerGroupMemberTag(IXMLTag peerGroupTag, IERunGroup runGroup) {
		IXMLTag peerGroupMemberTag = null;
		List<IERunGroupAccount> tempRunGroupAccounts = runGroupAccountManager
				.getAllRunGroupAccountsByRugId(runGroup.getRugId());
		// NOTE assume only one run group account exists
		if (tempRunGroupAccounts.size() > 0) {
			peerGroupMemberTag = getPeerGroupMemberTag(peerGroupTag, tempRunGroupAccounts.get(0));
		}
		return peerGroupMemberTag;
	}

	public boolean isStudentWithPeerGroupRole(IERunGroup runGroup, String role) {
		List<IXMLTag> peerGroupMemberTags = getPeerGroupMemberTags(runGroup);
		// NOTE assume only one peer group per student!
		if (peerGroupMemberTags == null || peerGroupMemberTags.size() == 0) {
			return false;
		}
		IXMLTag peerGroupMemberTag = peerGroupMemberTags.get(0);
		return getStatusChildTagAttribute(peerGroupMemberTag, "role").equals(role);
	}
	
	public String getPeerGroupName(IERunGroup runGroup1, IERunGroup runGroup2) {
		IXMLTag peerGroupTag = getCommonPeerGroupTag(runGroup1, runGroup2);
		if (peerGroupTag != null) {
			return sSpring.unescapeXML(getStatusChildTagAttribute(peerGroupTag, "name"))
					.replace(AppConstants.statusCommaReplace, ",");
		}
		return "";
	}

	public String getStudentPeerGroupName(IERunGroup runGroup) {
		List<IXMLTag> peerGroupMemberTags = getPeerGroupMemberTags(runGroup);
		// NOTE assume only one peer group per student!
		if (peerGroupMemberTags == null || peerGroupMemberTags.size() == 0) {
			return "";
		}
		IXMLTag peerGroupMemberTag = peerGroupMemberTags.get(0);
		// NOTE parent tag of member tag is status tag and parent of status tag is peer
		// group tag, see XML definition
		return sSpring.unescapeXML(getStatusChildTagAttribute(peerGroupMemberTag.getParentTag().getParentTag(), "name"))
				.replace(AppConstants.statusCommaReplace, ",");
	}

	public String getPeerGroupFeedbackGiverRole(IERunGroup feedbackReceiver, IERunGroup feedbackGiver) {
		if (feedbackReceiver == null || feedbackGiver == null) {
			return "";
		}
		if (feedbackReceiver.getRugId() == feedbackGiver.getRugId()) {
			return "self";
		} else {
			IXMLTag peerGroupTag = getCommonPeerGroupTag(feedbackReceiver, feedbackGiver);
			if (peerGroupTag != null) {
				IXMLTag peerGroupMemberTag = getPeerGroupMemberTag(peerGroupTag, feedbackGiver);
				if (peerGroupMemberTag != null) {
					return getStatusChildTagAttribute(peerGroupMemberTag, "role");
				}
			}
			return "";
		}
	}

	public List<String> getPeerGroupRoles(IERunGroup runGroup) {
		List<String> peerGroupRoles = new ArrayList<String>();
		if (runGroup == null) {
			return peerGroupRoles;
		}
		List<IXMLTag> peerGroupMemberTags = getPeerGroupMemberTags(runGroup);
		for (IXMLTag peerGroupMemberTag : peerGroupMemberTags) {
			peerGroupRoles.add(getStatusChildTagAttribute(peerGroupMemberTag, "role"));
		}
		return peerGroupRoles;
	}

	public int getNumberOfPeerGroupRoles(IERunGroup runGroup, String role) {
		int numberOfPeerGroupRoles = 0;
		if (runGroup == null) {
			return numberOfPeerGroupRoles;
		}
		List<String> peerGroupRoles = getPeerGroupRoles(runGroup);
		for (String peerGroupRole : peerGroupRoles) {
			if (peerGroupRole.equals(role)) {
				numberOfPeerGroupRoles++;
			}
		}
		return numberOfPeerGroupRoles;
	}

	public boolean hasStudentRole(IERunGroup runGroup) {
		return getNumberOfPeerGroupRoles(runGroup, peerGroupStudentRole) > 0;
	}

	public boolean hasTeacherRole(IERunGroup runGroup) {
		return getNumberOfPeerGroupRoles(runGroup, peerGroupTeacherRole) > 0;
	}

	public boolean hasStudentAssistantRole(IERunGroup runGroup) {
		return getNumberOfPeerGroupRoles(runGroup, peerGroupStudentAssistantRole) > 0;
	}

	public boolean hasPassiveTeacherRole(IERunGroup runGroup) {
		return getNumberOfPeerGroupRoles(runGroup, peerGroupPassiveTeacherRole) > 0;
	}

	public boolean hasPeerStudentRole(IERunGroup runGroup) {
		return getNumberOfPeerGroupRoles(runGroup, peerGroupPeerStudentRole) > 0;
	}
	
	public boolean isActiveInPeerGroup (IERunGroup pRunGroup) {
		return (hasStudentRole(pRunGroup) || hasTeacherRole(pRunGroup) || hasStudentAssistantRole(pRunGroup) || hasPassiveTeacherRole(pRunGroup) || hasPeerStudentRole(pRunGroup));
	}

	public List<String> getOtherPeerGroupRoles(IERunGroup runGroup) {
		List<String> peerGroupRoles = new ArrayList<String>();
		if (runGroup == null) {
			return peerGroupRoles;
		}
		List<IXMLTag> peerGroupMemberTags = getOtherPeerGroupMemberTags(runGroup);
		for (IXMLTag peerGroupMemberTag : peerGroupMemberTags) {
			peerGroupRoles.add(getStatusChildTagAttribute(peerGroupMemberTag, "role"));
		}
		return peerGroupRoles;
	}

	public int getNumberOfOtherPeerGroupRoles(IERunGroup runGroup, String role) {
		int numberOfPeerGroupRoles = 0;
		if (runGroup == null) {
			return numberOfPeerGroupRoles;
		}
		List<String> peerGroupRoles = getOtherPeerGroupRoles(runGroup);
		for (String peerGroupRole : peerGroupRoles) {
			if (peerGroupRole.equals(role)) {
				numberOfPeerGroupRoles++;
			}
		}
		return numberOfPeerGroupRoles;
	}

	public boolean hasPeersWithStudentRole(IERunGroup runGroup) {
		return getNumberOfOtherPeerGroupRoles(runGroup, peerGroupStudentRole) > 0;
	}

	public boolean hasPeersWithTeacherRole(IERunGroup runGroup) {
		return getNumberOfOtherPeerGroupRoles(runGroup, peerGroupTeacherRole) > 0;
	}

	public boolean hasPeersWithStudentAssistantRole(IERunGroup runGroup) {
		return getNumberOfOtherPeerGroupRoles(runGroup, peerGroupStudentAssistantRole) > 0;
	}

	public boolean hasPeersWithPassiveTeacherRole(IERunGroup runGroup) {
		return getNumberOfOtherPeerGroupRoles(runGroup, peerGroupPassiveTeacherRole) > 0;
	}

	public boolean hasPeersWithPeerStudentRole(IERunGroup runGroup) {
		return getNumberOfOtherPeerGroupRoles(runGroup, peerGroupPeerStudentRole) > 0;
	}

	public CRunPVToolkitCacAndTags getMessages() {
		IECaseComponent caseComponent = getCurrentMessagesCaseComponent();
		if (caseComponent == null) {
			return null;
		}
		List<IXMLTag> messageTags = CDesktopComponents.cScript().getNodeTags(caseComponent, "message");
		return new CRunPVToolkitCacAndTags(caseComponent, messageTags);
	}

	protected class HashtableMessageDetailsTagSortByDateTime implements Comparator<CRunPVToolkitMessage> {
		@Override
		public int compare(CRunPVToolkitMessage pvMessage1, CRunPVToolkitMessage pvMessage2) {
			Date date1 = CDefHelper.getDateFromStrDMYAndHMS(pvMessage1.getSendDate(), pvMessage1.getSendTime());
			Date date2 = CDefHelper.getDateFromStrDMYAndHMS(pvMessage2.getSendDate(), pvMessage2.getSendTime());
			if (date1 == null || date2 == null) {
				return 0;
			}
			if (date1.getTime() > date2.getTime()) {
				return -1;
			} else if (date1.getTime() < date2.getTime()) {
				return 1;
			} else {
				return 0;
			}
		}
	}

	public List<CRunPVToolkitMessage> getMessages(IERunGroup runGroup, boolean received) {
		// NOTE default order messages from new to old
		List<CRunPVToolkitMessage> pvMessages = new ArrayList<CRunPVToolkitMessage>();
		CRunPVToolkitCacAndTags messages = getMessages();
		if (messages == null) {
			return pvMessages;
		}
		// get all run groups in peer group including runGroup
		List<IERunGroup> runGroups = getPeerRungroups(runGroup, false, null, null, true);
		// include runGroup because messages sent by SYSTEM within PVToolkitSchedulers
		// class are stored in runGroup status
		runGroups.add(0, runGroup);
		IERunGroupAccount currentRunGroupAccount = getActiveRunGroupAccount(runGroup.getRugId());
		for (IXMLTag messageTag : messages.getXmlTags()) {
			// get all messageread tags for run group
			// NOTE there may be multiple message status tags for one message tag
			List<IXMLTag> runStatusTags = getRunStatusTags(runGroup, messages.getCaseComponent(), messageTag,
					AppConstants.statusTypeRunGroup);
			List<IXMLTag> messagereadTags = new ArrayList<IXMLTag>();
			for (IXMLTag runStatusTag : runStatusTags) {
				IXMLTag runStatusStatusTag = getStatusChildTag(runStatusTag);
				// get all messageread tags for run group
				messagereadTags.addAll(runStatusStatusTag.getChilds("messageread"));
			}
			for (IERunGroup tempRunGroup : runGroups) {
				// NOTE there may be multiple message status tags for one message tag
				List<IXMLTag> tempRunStatusTags = getRunStatusTags(tempRunGroup, messages.getCaseComponent(),
						messageTag, AppConstants.statusTypeRunGroup);
				for (IXMLTag tempRunStatusTag : tempRunStatusTags) {
					IXMLTag tempRunStatusStatusTag = getStatusChildTag(tempRunStatusTag);
					// get all messagedetails tags for peer run group
					List<IXMLTag> messagedetailsTags = tempRunStatusStatusTag.getChilds("messagedetails");
					for (IXMLTag messagedetailsTag : messagedetailsTags) {
						IERunGroupAccount sender = getRunGroupAccount(
								messagedetailsTag.getAttribute("messagedetailsrgaid"));
						IERunGroupAccount receiver = getRunGroupAccount(
								messagedetailsTag.getAttribute("messagedetailsreceiverrgaid"));
						if (sender != null && receiver != null && sender.getRgaId() == receiver.getRgaId()) {
							// NOTE if sender is equal to receiver, sender was SYSTEM!
							sender = null;
						}
						boolean receiverOk = received && receiver != null
								&& receiver.getRgaId() == currentRunGroupAccount.getRgaId();
						boolean senderOk = !received && sender != null
								&& sender.getRgaId() == currentRunGroupAccount.getRgaId();
						if (receiverOk || senderOk) {
							CRunPVToolkitMessage pvMessage = new CRunPVToolkitMessage();
							pvMessage.setMessagesCaseComponent(messages.getCaseComponent());
							pvMessage.setMessageTag(messageTag);
							pvMessage.setMessagedetailsTag(messagedetailsTag);
							pvMessage.setSender(sender);
							pvMessage.setReceiver(receiver);
							pvMessage.setStepTag(
									sSpring.getCaseComponentXmlDataTagById(getCurrentProjectsCaseComponent(),
											getStatusChildTagAttribute(messagedetailsTag, "steptagid")));
							pvMessage.setSendDate(getStatusChildTagAttribute(messagedetailsTag, "senddate"));
							pvMessage.setSendTime(getStatusChildTagAttribute(messagedetailsTag, "sendtime"));
							boolean read = false;
							for (IXMLTag messagereadTag : messagereadTags) {
								if (messagereadTag.getAttribute("messagedetailsuuid")
										.equals(messagedetailsTag.getAttribute("messagedetailsuuid"))) {
									IXMLTag statusChildTag = getStatusChildTag(messagereadTag);
									read = statusChildTag.getAttribute("read").equals(AppConstants.statusValueTrue);
									break;
								}
							}
							pvMessage.setRead(read);
							pvMessages.add(pvMessage);
						}
					}
				}
			}
		}

		Collections.sort(pvMessages, new HashtableMessageDetailsTagSortByDateTime());
		return pvMessages;
	}

	protected IXMLTag getNewMessageDetailsTag(IXMLTag runStepTagStatusChildTag) {
		IXMLTag tag = getNewOrExistingRunStatusChildTag(runStepTagStatusChildTag, "messagedetails", null, true, true,
				true);
		return tag;
	}

	protected boolean sendMessage(IXMLTag messageTag, IERunGroupAccount receiver, IXMLTag stepTag) {
		if (messageTag == null) {
			return false;
		}

		sSpring.setRunTagStatus(getCurrentMessagesCaseComponent(), messageTag, AppConstants.statusKeySent,
				AppConstants.statusValueTrue, true, AppConstants.statusTypeRunGroup, true, false);
		IXMLTag tag = getNewMessageDetailsTag(
				getRunMessagesStatusChildTag(getCurrentRunGroup(), messageTag, AppConstants.statusTypeRunGroup));
		if (tag == null) {
			return false;
		}

		// NOTE see XML definition
		// NOTE sender is always current run group account and is already handled in
		// method getNewMessageDetailsTag
		tag.setAttribute("messagedetailsreceiverrgaid",
				receiver != null ? ("" + receiver.getRgaId()) : ("" + getCurrentRunGroupAccount().getRgaId()));

		IXMLTag statusChildTag = getStatusChildTag(tag);
		statusChildTag.setAttribute("steptagid", stepTag != null ? stepTag.getAttribute(AppConstants.keyId) : "");
		statusChildTag.setAttribute("senddate", CDefHelper.getDateStrAsDMY(new Date()));
		statusChildTag.setAttribute("sendtime", CDefHelper.getDateStrAsHMS(new Date()));
		statusChildTag.setAttribute("send", "true");
		
		// extra content
		String lExtraVarKeys = "";
		int lNr = 1;
		
		for (Map.Entry<String, String> lExtras : messageExtraVarKeyValues.entrySet()) {
			if (!lExtraVarKeys.isEmpty()) {
				lExtraVarKeys += ",";
			}
			lExtraVarKeys += lExtras.getKey();
			statusChildTag.setAttribute("extravar_" + lNr, lExtras.getValue());
			lNr++;
		}
		if (!lExtraVarKeys.isEmpty()) {
			statusChildTag.setAttribute("extrakeys", lExtraVarKeys);
		}
		
		saveMessageDetails();

		return true;
	}

	public void saveMessageDetails() {
		// stores changes in message details in database, message details are part of
		// messages case component
		saveStatusChange(getCurrentMessagesCaseComponent(), AppConstants.statusTypeRunGroup);
	}

	protected IXMLTag getNewMessageReadTag(IXMLTag runStepTagStatusChildTag) {
		IXMLTag tag = getNewOrExistingRunStatusChildTag(runStepTagStatusChildTag, "messageread", null, true, true,
				true);
		return tag;
	}

	public boolean setMessageRead(CRunPVToolkitMessage message) {
		if (message == null) {
			return false;
		}

		IXMLTag tag = getNewMessageReadTag(getRunMessagesStatusChildTag(getCurrentRunGroup(), message.getMessageTag(),
				AppConstants.statusTypeRunGroup));
		if (tag == null) {
			return false;
		}

		// NOTE see XML definition
		// NOTE sender is always current run group account and is already handled in
		// method getNewMessageDetailsTag
		tag.setAttribute("messagedetailsuuid", message.getMessagedetailsTag().getAttribute("messagedetailsuuid"));
		tag.setAttribute("messagedetailsrgaid", message.getMessagedetailsTag().getAttribute("messagedetailsrgaid"));

		IXMLTag statusChildTag = getStatusChildTag(tag);
		statusChildTag.setAttribute("readdate", CDefHelper.getDateStrAsDMY(new Date()));
		statusChildTag.setAttribute("readtime", CDefHelper.getDateStrAsHMS(new Date()));
		statusChildTag.setAttribute("read", "" + message.isRead());

		saveMessageDetails();

		return true;
	}

	public void saveMessageRead() {
		// stores changes in message read in database, message reads are part of
		// messages case component
		saveStatusChange(getCurrentMessagesCaseComponent(), AppConstants.statusTypeRunGroup);
	}

	public CRunPVToolkitCacAndTags getStepMessageTags(IERunGroup runGroup) {
		CRunPVToolkitCacAndTag cacAndTag = new CRunPVToolkitCacAndTag(getCurrentProjectsCaseComponent(),
				getRunProjectsStatusChildTag(runGroup, getCurrentMethodTag(), AppConstants.statusTypeRun));
		List<IXMLTag> xmlTags;
		if (cacAndTag == null || cacAndTag.getXmlTag() == null) {
			xmlTags = new ArrayList<IXMLTag>();
		} else {
			xmlTags = cacAndTag.getXmlTag().getChilds(stepMessageTagName);
		}
		return new CRunPVToolkitCacAndTags(getCurrentProjectsCaseComponent(), xmlTags);
	}

	public IXMLTag getStepMessageTag(List<IXMLTag> stepMessageTags, IXMLTag stepTag) {
		String stepTagId = stepTag.getAttribute(AppConstants.keyId);
		for (IXMLTag stepMessageTag : stepMessageTags) {
			if (getStatusChildTagAttribute(stepMessageTag, "steptagid").equals(stepTagId)) {
				return stepMessageTag;
			}
		}
		return null;
	}

	public IXMLTag saveStepMessage(IERunGroup runGroup, IXMLTag stepMessageTag, String messageCacId,
			String messageTagId, String reminderDate, String reminderTime, IXMLTag stepTag, String deadlineDate,
			String deadlineTime) {
		CRunPVToolkitCacAndTag cacAndTag = new CRunPVToolkitCacAndTag(getCurrentProjectsCaseComponent(),
				getRunProjectsStatusChildTag(runGroup, getCurrentMethodTag(), AppConstants.statusTypeRun));
		if (stepMessageTag == null) {
			stepMessageTag = getNewOrExistingRunStatusChildTag(cacAndTag.getXmlTag(), stepMessageTagName, null, true, true,
					true);
		}
		IXMLTag statusChildTag = getStatusChildTag(stepMessageTag);
		statusChildTag.setAttribute("messagecacid", messageCacId);
		statusChildTag.setAttribute("messagetagid", messageTagId);
		statusChildTag.setAttribute("reminderdate", reminderDate);
		statusChildTag.setAttribute("remindertime", reminderTime);
		String stepTagId = "";
		if (stepTag != null) {
			stepTagId = stepTag.getAttribute(AppConstants.keyId);
		}
		statusChildTag.setAttribute("steptagid", stepTagId);
		statusChildTag.setAttribute("deadlinedate", deadlineDate);
		statusChildTag.setAttribute("deadlinetime", deadlineTime);

		saveStepMessages();

		return stepMessageTag;
	}

	public boolean deleteStepMessage(IXMLTag stepMessageTag) {
		stepMessageTag.getParentTag().getChildTags().remove(stepMessageTag);

		saveStepMessages();

		return true;
	}

	public void saveStepMessages() {
		// stores changes in step messages in database, steps are part of projects case
		// component
		// note that changes are stored in run status, not in run group status
		saveStatusChange(getCurrentProjectsCaseComponent(), AppConstants.statusTypeRun);
	}

	public String getDeadlineDateStr(IERunGroup runGroup, IXMLTag stepTag) {
		CRunPVToolkitCacAndTags steps = getStepMessageTags(runGroup);
		IXMLTag stepMessageTag = getStepMessageTag(steps.getXmlTags(), stepTag);
		if (stepMessageTag != null) {
			String dateStr = getStatusChildTagAttribute(stepMessageTag, AppConstants.statusKeyDeadlineDate);
			if (!dateStr.equals("")) {
				Date date = CDefHelper.getDateFromStrYMD(dateStr);
				return CDefHelper.getDateStrAsDMY(date);
			}
		}
		return "";
	}

	public String getDeadlineTimeStr(IERunGroup runGroup, IXMLTag stepTag) {
		CRunPVToolkitCacAndTags steps = getStepMessageTags(runGroup);
		IXMLTag stepMessageTag = getStepMessageTag(steps.getXmlTags(), stepTag);
		if (stepMessageTag != null) {
			return getStatusChildTagAttribute(stepMessageTag, AppConstants.statusKeyDeadlineTime);
		}
		return "";
	}

	public List<CRunPVToolkitPeerGroupMemberProgress> getPeerGroupMembersProgress(
			List<IERunGroupAccount> runGroupAccounts, List<String> peerRoles) {
		List<IERunGroup> runGroups = new ArrayList<IERunGroup>();
		for (IERunGroupAccount runGroupAccount : runGroupAccounts) {
			runGroups.add(runGroupAccount.getERunGroup());
		}
		// NOTE get root tags for peers
		List<IXMLTag> peerRunGroupXmlDataPlusStatusRootTags = getPeerRunGroupXmlDataPlusStatusRootTags(runGroups,
				getCurrentProjectsCaseComponent());

		List<CRunPVToolkitPeerGroupMemberProgress> peerGroupMembersProgress = new ArrayList<CRunPVToolkitPeerGroupMemberProgress>();
		int maxNumberOfSteps = getCurrentStepTags().size();
		// NOTE get current step tags per cycle, i.e., step tags of current user
		List<List<IXMLTag>> currentStepTagsPerCycle = getCurrentStepTagsPerCycle();
		int maxCycleNumber = getMaxCycleNumber();
		for (IERunGroupAccount runGroupAccount : runGroupAccounts) {
			List<Integer> maxNumberOfStepsPerCycle = new ArrayList<Integer>();
			for (List<IXMLTag> stepTags : currentStepTagsPerCycle) {
				maxNumberOfStepsPerCycle.add(new Integer(stepTags.size()));
			}

			String peerRole = peerRoles.get(runGroupAccounts.indexOf(runGroupAccount));
			int reachedCycleNumber = 1;
			List<Double> progressPerCycle = new ArrayList<Double>();
			for (int i = 0; i < maxCycleNumber; i++) {
				progressPerCycle.add(new Double(0));
			}
			if (!peerRole.equals(peerGroupStudentRole)) {
				// NOTE if no student role last step is last feedback step, so correct max
				// number of steps
				for (int i = (maxCycleNumber - 1); i >= 0; i--) {
					for (int j = (currentStepTagsPerCycle.get(i).size() - 1); j >= 0; j--) {
						if (!currentStepTagsPerCycle.get(i).get(j).getChildValue("steptype").equals(feedbackStepType)) {
							Integer maxNumberOfCycleSteps = maxNumberOfStepsPerCycle.get(i) - 1;
							maxNumberOfStepsPerCycle.set(i, maxNumberOfCycleSteps);
						} else {
							break;
						}
					}
				}
			}
			if (maxNumberOfSteps <= 0) {
				peerGroupMembersProgress.add(new CRunPVToolkitPeerGroupMemberProgress(runGroupAccount, peerRole,
						reachedCycleNumber, progressPerCycle));
			} else {
				List<Integer> reachedNumberOfStepsPerCycle = new ArrayList<Integer>();
				for (int i = 0; i < maxCycleNumber; i++) {
					reachedNumberOfStepsPerCycle.add(new Integer(0));
				}
				int index = runGroupAccounts.indexOf(runGroupAccount);
				int size = peerRunGroupXmlDataPlusStatusRootTags.size();
				if (index >= 0 && index < size) {
					IXMLTag runGroupAccountRootTag = peerRunGroupXmlDataPlusStatusRootTags.get(index);
					List<IXMLTag> stepTagsFollowingLastFinishedStepTag = new ArrayList<IXMLTag>();
					for (int i = 0; i < maxCycleNumber; i++) {
						stepTagsFollowingLastFinishedStepTag.add(null);
					}
					if (runGroupAccountRootTag != null) {
						// NOTE loop through current step tags per cycle, i.e., step tags of current
						// user
						for (int i = 0; i < currentStepTagsPerCycle.size(); i++) {
							List<IXMLTag> currentStepTags = currentStepTagsPerCycle.get(i);
							for (IXMLTag currentStepTag : currentStepTags) {
								// get step tag for peer using id of current step tag
								IXMLTag stepTag = sSpring.getXmlManager().getTagById(
										runGroupAccountRootTag, currentStepTag.getAttribute(AppConstants.keyId));
								if (stepTag != null && stepTag.getCurrentStatusAttribute(AppConstants.statusKeyFinished)
										.equals(AppConstants.statusValueTrue)) {
									// NOTE get cycle number from current step tag
									if (currentStepTag.getParentTag().getName().equals("cycle")) {
										reachedCycleNumber = getCycleNumber(currentStepTag.getParentTag());
									}
									// NOTE get index of current step tag
									int indexOfStepTag = currentStepTags.indexOf(currentStepTag);
									if (indexOfStepTag < (maxNumberOfStepsPerCycle.get(i) - 1)) {
										// NOTE get step tag following last finished step tag for peer
										stepTagsFollowingLastFinishedStepTag.set(i,
												sSpring.getXmlManager().getTagById(
														runGroupAccountRootTag, currentStepTags.get(indexOfStepTag + 1)
																.getAttribute(AppConstants.keyId)));
									} else {
										// NOTE if last step tag there is no following step tag
										stepTagsFollowingLastFinishedStepTag.set(i, null);
									}
									// NOTE reached number of steps is index + 1. Use this instead of a counter
									// because teachers only do feedback steps or finished of a step might not be
									// set due to bugs or overload.
									// Also when testing the PV-tool previous steps might not be finished
									reachedNumberOfStepsPerCycle.set(i, indexOfStepTag + 1);
								}
							}
						}
					}
					for (int i = 0; i < currentStepTagsPerCycle.size(); i++) {
						// NOTE it might be that some sub steps are already finished
						double subStepsProgress = 0.0;
						if (stepTagsFollowingLastFinishedStepTag.get(i) != null) {
							// NOTE get current sub step tags, i.e., sub step tags of current user
							List<IXMLTag> currentSubStepTags = getSubStepTags(new CRunPVToolkitCacAndTag(
									getCurrentProjectsCaseComponent(), stepTagsFollowingLastFinishedStepTag.get(i)));
							int maxNumberOfSubSteps = currentSubStepTags.size();
							if (maxNumberOfSubSteps > 0) {
								int reachedNumberOfSubSteps = 0;
								for (IXMLTag currentSubStepTag : currentSubStepTags) {
									// get sub step tag for peer using id of current sub step tag
									IXMLTag subStepTag = sSpring.getXmlManager().getTagById(
											runGroupAccountRootTag, currentSubStepTag.getAttribute(AppConstants.keyId));
									if (subStepTag != null
											&& subStepTag.getCurrentStatusAttribute(AppConstants.statusKeyFinished)
													.equals(AppConstants.statusValueTrue)) {
										// NOTE get index of current sub step tag
										int indexOfSubStepTag = currentSubStepTags.indexOf(currentSubStepTag);
										// NOTE reached number of sub steps is index + 1. Use this instead of a counter
										// because teachers only do feedback steps or finished of a sub step might not
										// be set due to bugs or overload.
										// Also when testing the PV-tool previous sub steps might not be finished
										reachedNumberOfSubSteps = indexOfSubStepTag + 1;
									}
								}
								subStepsProgress = new Double(reachedNumberOfSubSteps)
										/ new Double(maxNumberOfSubSteps);
							}
						}
						progressPerCycle.set(i, (new Double(reachedNumberOfStepsPerCycle.get(i)) + subStepsProgress)
								/ new Double(maxNumberOfStepsPerCycle.get(i)));
					}
				}
				peerGroupMembersProgress.add(new CRunPVToolkitPeerGroupMemberProgress(runGroupAccount, peerRole,
						reachedCycleNumber, progressPerCycle));
			}
		}

		return peerGroupMembersProgress;
	}

	public String getWebcamRecordFormat() {
		if (locWebcamRecordFormat != null)
			return locWebcamRecordFormat;
		String lFormat = PropsValues.LOCAL_RECORDER_FORMAT;
		if (!lFormat.isEmpty())
			locWebcamRecordFormat = lFormat;
		else
			locWebcamRecordFormat = webcamRecordFormat;
		return locWebcamRecordFormat;
	}

	public String getWebcamRecordFileExtension() {
		if (locWebcamRecordFileExtension != null)
			return locWebcamRecordFileExtension;
		String lExt = PropsValues.LOCAL_RECORDER_FILEEXTENSION;
		if (!lExt.isEmpty())
			locWebcamRecordFileExtension = lExt;
		else
			locWebcamRecordFileExtension = webcamRecordFileExtension;
		return locWebcamRecordFileExtension;
	}

	public static String getPanelidprefix() {
		return panelIdPrefix;
	}

	protected String pGetRubricLabelText(String pLabelId, boolean pCapitalize) {
		//search for label key value in states component
		String lLabelString = getStateValueAsString(pLabelId);
		if (StringUtils.isEmpty(lLabelString)) {
			//first check for Rubric-specific label
			String lLabelId = pLabelId + "." + getCurrentRubricCode();
			lLabelString = vView.getLabel(lLabelId);
			if (StringUtils.isEmpty(lLabelString)) {
				//if not present, try global key
				lLabelString = vView.getLabel(pLabelId);
			}
		}
		if (pCapitalize)
			VView.getCapitalizeFirstChar(lLabelString);
		return lLabelString;
	}

	public String getRubricLabelText(String pLabelId) {
		return pGetRubricLabelText(pLabelId, false);
	}

	public String getRubricCLabelText(String pLabelId) {
		return pGetRubricLabelText(pLabelId, true);
	}

}
