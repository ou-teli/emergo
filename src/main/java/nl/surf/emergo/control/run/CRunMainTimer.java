/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.Date;

import org.zkoss.zk.ui.event.Event;

import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefTimer;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunMainTimer is used by the Emergo player (class CRunWnd).
 */
public class CRunMainTimer extends CDefTimer {

	private static final long serialVersionUID = -734326229621625097L;

	/** The run wnd. */
	protected CRunWnd runWnd = (CRunWnd) CDesktopComponents.vView().getComponent(CControl.runWnd);
	
	/** is player opened first time. */
	protected boolean isPlayerIsOpenedFirstTime = true;

	/**
	 * The starttime is the time the player is started the first time by
	 * a certain rungroup. It is the time in milliseconds since
	 * januari 1, 1970, which is given by System.currentTimeMillis().
	 *
	 * When the player is started a next time by the same rungroup starttime
	 * is increase by the amount of milliseconds elapsed since the player was
	 * closed the last time, given by currenttime.
	 * In this way currenttime minus starttime equals the total time the player
	 * was open summarised over all sessions, which is called case time.
	 */
	protected long starttime = 0;

	/**
	 * The currenttime is the current time. It is the time in milliseconds since
	 * januari 1, 1970, which is given by System.currentTimeMillis().
	 * It is increased while the player is open.
	 */
	public long currenttime = 0;

	/**
	 * The currentstarttime is the time the current player instance is opened.
	 * It is the time in milliseconds since januari 1, 1970,
	 * which is given by System.currentTimeMillis().
	 * So currenttime minus currentstarttime is equal to the time the current
	 * player instance is open, the session time.
	 */
	protected long currentstarttime = 0;

	// there is only one instance of the case component
	/** The casecasecomponent is used to save start time and current time. */
	protected IECaseComponent casecasecomponent = null;

	/** The p start up, used to detect the first timer tick. */
	protected boolean pStartUp = true;

	/** The p idle, used to prevent parallel or overlapping timer handling. */
	protected boolean pIdle = true;

	/** The timerTicks1, used to count timer ticks. */
	protected long timerTicks1 = 0;

	/** The delay in milliseconds. */
	protected static final int delay = 3000;

	/** The maxTimerTicks1_1, used to define a period of 3 seconds. */
	protected static final long maxTimerTicks1_1 = 1;

	/** The maxTimerTicks1_2, used to define a period of 30 seconds. */
	protected static final long maxTimerTicks1_2 = 10;

	/** The timerTicks2, used to count timer ticks. */
	protected long timerTicks2 = 0;

	/** The maxTimerTicks2_1, used to define a period of 600 seconds. */
	protected static final long maxTimerTicks2_1 = 200;

	/** The detach components. */
	protected boolean detachComponents = false;

	/** The detach components. */
	protected CRunTree runTreeForScrollIntoView = null;

	public boolean isDetachComponents() {
		return detachComponents;
	}

	public void setDetachComponents(boolean detachComponents) {
		this.detachComponents = detachComponents;
	}

	public CRunTree getRunTreeForScrollIntoView() {
		return runTreeForScrollIntoView;
	}

	public void setRunTreeForScrollIntoView(CRunTree runTreeForScrollIntoView) {
		this.runTreeForScrollIntoView = runTreeForScrollIntoView;
	}

	/**
	 * Instantiates a new c run main timer.
	 * Gets saved starttime and currenttime, adjusts them to current time value and
	 * saves new values.
	 *
	 * @param aId the a id
	 */
	public CRunMainTimer(String aId) {
		casecasecomponent = CDesktopComponents.sSpring().getCaseComponent("case", "");
		setId(aId);
		setRunning(true);
		setDelay(1);
		setRepeats(true);
	}

	/**
	 * Inits main timer. Gets saved starttime and currenttime, adjusts them to current time value and saves new values. 
	 */
	/* TEST CODE	
	protected void init() {
		if (casecasecomponent != null) {
			// get current time
			long lCurrenttime = System.currentTimeMillis();
			// get saved start time and current time
			long lSavedstarttime = 0;
			long lSavedcurrenttime = 0;
			String lStartTime = CDesktopComponents.sSpring().getCurrentRunComponentStatus(
					casecasecomponent, AppConstants.statusKeyStartTime, AppConstants.statusTypeRunGroup);
			isPlayerIsOpenedFirstTime = lStartTime.equals("0");
			if (!lStartTime.equals(""))
				lSavedstarttime = Long.parseLong(lStartTime);
			String lCurrentTime = CDesktopComponents.sSpring().getCurrentRunComponentStatus(
					casecasecomponent, AppConstants.statusKeyCurrentTime, AppConstants.statusTypeRunGroup);
			if (!lCurrentTime.equals(""))
				lSavedcurrenttime = Long.parseLong(lCurrentTime);
			// if saved start time and current time are empty, set start time to
			// current time
			if ((lSavedstarttime == 0) && (lSavedcurrenttime == 0)) {
				lSavedstarttime = lCurrenttime;
			}
			// else increase saved start time by (current start time minus saved current time), which is time elapsed between end of previous session and start of this session
			else {
				lSavedstarttime = lSavedstarttime + (lCurrenttime - lSavedcurrenttime);
			}
			lSavedcurrenttime = lCurrenttime;
			// and save
			starttime = lSavedstarttime;
			currentstarttime = lSavedcurrenttime;
			currenttime = lSavedcurrenttime;
			CDesktopComponents.sSpring().setRunComponentStatus(casecasecomponent,
					AppConstants.statusKeyStartTime, "" + starttime, false, AppConstants.statusTypeRunGroup, true);
			CDesktopComponents.sSpring().setRunComponentStatus(casecasecomponent,
					AppConstants.statusKeyCurrentTime, "" + currenttime, false, AppConstants.statusTypeRunGroup, true);
		}
	}
	*/
	
	/**
	 * Inits case. Executes possible run actions (set initial properties) and sets case status opened to true. 
	 */
	/* TEST CODE	
	protected void initCase() {
		if (casecasecomponent != null) {
			// if player is opened first time check if there are run actions that should be executed
			if (isPlayerIsOpenedFirstTime) {
				runWnd.executeRunActions();
			}
			
			CDesktopComponents.sSpring().setRunComponentStatus(casecasecomponent,
					AppConstants.statusKeyOpened, AppConstants.statusValueTrue, true, AppConstants.statusTypeRunGroup, true);
			runWnd.checkForNotCleanupedRunWndsOfOtherRgas();
		}
	}
	*/
	
	/**
	 * Inits case. Gets saved starttime and currenttime, adjusts them to current time value and
	 * saves new values. Executes possible run actions (set initial properties) and sets case status opened to true. 
	 */
	protected void initCase() {
		if (casecasecomponent != null) {
			// get current time
			long lCurrenttime = System.currentTimeMillis();
			// get saved start time and current time
			long lSavedstarttime = 0;
			long lSavedcurrenttime = 0;
			String lStartTime = CDesktopComponents.sSpring().getCurrentRunComponentStatus(
					casecasecomponent, AppConstants.statusKeyStartTime, AppConstants.statusTypeRunGroup);
			boolean lIsPlayerIsOpenedFirstTime = lStartTime.equals("0");
			if (!lStartTime.equals(""))
				lSavedstarttime = Long.parseLong(lStartTime);
			String lCurrentTime = CDesktopComponents.sSpring().getCurrentRunComponentStatus(
					casecasecomponent, AppConstants.statusKeyCurrentTime, AppConstants.statusTypeRunGroup);
			if (!lCurrentTime.equals(""))
				lSavedcurrenttime = Long.parseLong(lCurrentTime);
			// if saved start time and current time are empty, set start time to
			// current time
			if ((lSavedstarttime == 0) && (lSavedcurrenttime == 0)) {
				lSavedstarttime = lCurrenttime;
			}
			// else increase saved start time by (current start time minus saved
			// current time)
			else {
				lSavedstarttime = lSavedstarttime
						+ (lCurrenttime - lSavedcurrenttime);
			}
			lSavedcurrenttime = lCurrenttime;
			// and save
			starttime = lSavedstarttime;
			currentstarttime = lSavedcurrenttime;
			currenttime = lSavedcurrenttime;
			CDesktopComponents.sSpring().setRunComponentStatus(casecasecomponent,
					AppConstants.statusKeyStartTime, "" + starttime, false, AppConstants.statusTypeRunGroup, true);
			CDesktopComponents.sSpring().setRunComponentStatus(casecasecomponent,
					AppConstants.statusKeyCurrentTime, "" + currenttime, false, AppConstants.statusTypeRunGroup, true);
			
			// if player is opened first time check if there are run actions that should be executed
			if (lIsPlayerIsOpenedFirstTime) {
				runWnd.executeRunActions();
			}
			
			CDesktopComponents.sSpring().setRunComponentStatus(casecasecomponent,
					AppConstants.statusKeyOpened, AppConstants.statusValueTrue, true, AppConstants.statusTypeRunGroup, true);
			//runWnd.checkForNotCleanupedRunWndsOfOtherRgas();
		}
	}

	/**
	 * On timer.
	 * On first tick inits player window.
	 * On every tick saves currenttime, checks script timers (if they have to go off),
	 * checks pending actions within player and checks possible player status change
	 * caused by script actions.
	 * On every tenth tick updates player with possible updates by other rungroups,
	 * including tutor and admin, and saves all changed rungroup status. (The latter
	 * will also be saved if the player is closed.)
	 *
	 * @param aEvent the a event
	 */
	public void onTimer(Event aEvent) {
		if (runWnd == null)
			return;
		if (!pIdle)
			return;
		pIdle = false;
		if (pStartUp) {
			pStartUp = false;
			setDelay(delay);
			runWnd.init();
			pIdle = true;
			return;
		}
		timerTicks1 ++;

		// check if run tree has to scrolled to selected item
		if (getRunTreeForScrollIntoView() != null) {
			getRunTreeForScrollIntoView().scrollIntoView();
			setRunTreeForScrollIntoView(null);
		}

		// get current time
		currenttime = System.currentTimeMillis();
		// save current time
		if (timerTicks1%maxTimerTicks1_1 == 0) {
			if (casecasecomponent != null)
				CDesktopComponents.sSpring().setRunComponentStatus(casecasecomponent,
						AppConstants.statusKeyCurrentTime, "" + currenttime, true, AppConstants.statusTypeRunGroup, true);
		}
/*		// show time to user
		if (counter == maxTimerTicks2) {
			CRunTitleArea lArea = (CRunTitleArea)CDesktopComponents.vView().getComponent("runTitleArea");
			if (lArea != null)
				lArea.setCaseTime(""+CDesktopComponents.sSpring().getCaseTime());
		} */
		// check script timers
		if (timerTicks1%maxTimerTicks1_1 == 0) {
			CDesktopComponents.sSpring().getSScriptHelper().checkScriptTimers(true);
		}
		// check for status changes by other users in memory
		if (timerTicks1%maxTimerTicks1_1 == 0) {
			runWnd.checkStatusChange();
		}
		// check for status changes by other users in table rungroupcasecomponentupdates
		if (timerTicks1 == maxTimerTicks1_2) {
			CDesktopComponents.sSpring().getSUpdateHelper().checkUpdateRunTags(AppConstants.statusTypeRunGroup);
		}
		// check for status changes by other users in table rungroupcasecomponentupdates, still needed for (neo)classic skin
		if (timerTicks1 == maxTimerTicks1_2) {
			runWnd.checkUpdateExternalTags();
		}
		// save all status from memory to database
		if (timerTicks1 == maxTimerTicks1_2) {
			runWnd.saveAllStatus();
		}
		if (timerTicks1 >= maxTimerTicks1_2) {
			timerTicks1 = 0;
		}

		timerTicks2 ++;
		// check if player desktops of other players are not cleaned up, possible due to connection problems or errors, and clean up
		if (timerTicks2 == maxTimerTicks2_1) {
			//runWnd.checkForNotCleanupedRunWndsOfOtherRgas();
		}
		if (timerTicks2 >= maxTimerTicks2_1) {
			timerTicks2 = 0;
		}
		
		if (isDetachComponents()) {
			stop();
			runWnd.detachComponents();
		}
		pIdle = true;
	}

	/**
	 * Stores current time. 
	 */
	public void storeCurrenTime() {
		if (casecasecomponent != null) {
			// get current time
			currenttime = System.currentTimeMillis();
			CDesktopComponents.sSpring().setRunComponentStatus(casecasecomponent,
					AppConstants.statusKeyCurrentTime, "" + currenttime, false, AppConstants.statusTypeRunGroup, true);
		}
	}
	
	/* TEST CODE	
	@Override
	public void stop() {
		super.stop();
		
		starttime = 0;
		currenttime = 0;
		currentstarttime = 0;
	}
	*/
	
	/**
	 * Gets the case time, the total time the player is opened summarised over
	 * all player sessions.
	 *
	 * @return the case time
	 */
	public long getCaseTime() {
		if (starttime == 0) {
			return 0;
		}
		return (System.currentTimeMillis() - starttime);
	}

	/**
	 * Gets the case start time. The total time the Emergo player has been open till
	 * it was opened the last time, for the current rungroup.
	 *
	 * @return the case start time
	 */
	public long getCaseStartTime() {
		return (currentstarttime - starttime);
	}

	/**
	 * Gets the real time in milliseconds since januari 1, 1970,
	 * which is given by System.currentTimeMillis().
	 *
	 * @return the real time
	 */
	public long getRealTime() {
		long lRealCurrenttime = System.currentTimeMillis();
		return lRealCurrenttime;
	}

	/**
	 * Gets the real time in milliseconds till the start of the run.
	 *
	 * @return the real time from start of run.
	 */
	public long getRealTimeFromStartOfRun() {
		long lRealCurrenttime = getRealTime();
		Date lStartDate = CDesktopComponents.sSpring().getRun().getStartdate();
		long lRealStarttime = lStartDate.getTime();
		return (lRealCurrenttime - lRealStarttime);
	}

}
