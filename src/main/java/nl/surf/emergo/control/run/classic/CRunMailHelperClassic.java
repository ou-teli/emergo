/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Label;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.Treerow;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CTree;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunMailHelper.
 */
public class CRunMailHelperClassic extends CRunComponentHelperClassic {

	/**
	 * Instantiates a new c run mail helper.
	 * 
	 * @param aTree the ZK mail tree
	 * @param aShowTagNames the a show tag names, comma separated, only tags with these names are shown
	 * @param aCaseComponent the mail case component
	 * @param aRunComponent the a run component, the ZK mail component
	 */
	public CRunMailHelperClassic(CTree aTree, String aShowTagNames, IECaseComponent aCaseComponent, CRunComponentClassic aRunComponent) {
		super(aTree, aShowTagNames, aCaseComponent, aRunComponent);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTreeHelper#renderTreecell(org.zkoss.zul.Treeitem, org.zkoss.zul.Treerow, nl.surf.emergo.business.IXMLTag, java.lang.String, boolean, java.lang.String)
	 */
	@Override
	protected Treecell renderTreecell(Treeitem aTreeitem, Treerow aTreerow,
			IXMLTag aItem, String aKeyValues, boolean aAccIsAuthor) {
		Treecell lTreecell = getTreerowTreecell(aTreerow, 0);
		String lStatus = "active";
		if (isOpened(aItem))
			lStatus = "opened";
		lTreecell.setZclass("CRunMail_treecell_"+lStatus);
		lTreecell.setStyle(CDesktopComponents.vView().getLabel("CRunComponent_treecell_style"));
		if (aItem.getName().equals("map")) {
			String lImg = CDesktopComponents.sSpring().getStyleImgSrc("icon_folder_" + lStatus);
			lTreecell.setImage(lImg);
		} else {
			String lImg = CDesktopComponents.sSpring().getStyleImgSrc("icon_email_" + lStatus);
			lTreecell.setImage(lImg);
		}
		String lMailTitle = CDesktopComponents.sSpring().unescapeXML(aItem.getChildValue("title"));
		if (lMailTitle.equals(""))
			lMailTitle = aKeyValues;
//		String lVersion = getStatusValue(aItem, AppConstants.statusKeyVersion);
//		if (!(lVersion.equals("")) || (lVersion.equals("1")))
//			lMailTitle = lMailTitle + " (" + lVersion + ")";
//		String lTime = getStatusTime(aItem, AppConstants.statusKeySent);
//		lMailTitle = lMailTitle + " (" + lTime + ")";
		if (lMailTitle.length() > 80) {
			lTreecell.setTooltiptext(lMailTitle);
			lMailTitle = lMailTitle.substring(0, 80) + "...";
		}
		lTreecell.setLabel(lMailTitle);
		String lReceiverName = CDesktopComponents.sSpring().unescapeXML(aItem.getChildValue("receivername"));
		if (lReceiverName.length() > 25)
			lReceiverName = lReceiverName.substring(0, 24) + " ...";
		String lSenderName = CDesktopComponents.sSpring().unescapeXML(aItem.getChildValue("sendername"));
		if (!lReceiverName.equals("")) {
			Label lLabel = getComponentLabel(lTreecell, "receiver", " : " + CDesktopComponents.vView().getLabel("run_mail.receiver") + " " + lReceiverName);
		}
		if (!lSenderName.equals("")) {
			Label lLabel = getComponentLabel(lTreecell, "contentelement", " : " + CDesktopComponents.vView().getLabel("run_mail.sender") + " " + lSenderName);
		}
		return lTreecell;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunComponentHelper#xmlChildsToContentItems(java.util.List, org.zkoss.zk.ui.Component, org.zkoss.zul.Treeitem, java.lang.String)
	 */
	@Override
	public List<Component> xmlChildsToContentItems(List<IXMLTag> aChildTags, Component aParent, Component aInsertBefore) {
		if ((aChildTags == null) || (aChildTags.size() == 0))
			return null;
		// There are root mail tags which have inbox attribute set
		List<IXMLTag> lRootMailTags = new ArrayList<IXMLTag>();
		for (IXMLTag xmltag : aChildTags) {
			IXMLTag xmlchild = xmltag.getChild(AppConstants.statusElement);
			if (xmlchild != null) {
				boolean lPresent = (!(CDesktopComponents.sSpring().getCurrentTagStatus(xmltag,
						AppConstants.statusKeyPresent)).equals(AppConstants.statusValueFalse));
				boolean lSent = true;
				boolean lDeleted = false;
				boolean lOriginalTag = false;
				String lName = xmltag.getName();
				if (!lName.equals("map")) {
					// sent mails are always present
					lPresent = true;
					lSent = ((CDesktopComponents.sSpring().getCurrentTagStatus(xmltag,
							AppConstants.statusKeySent)).equals(AppConstants.statusValueTrue));
					lDeleted = ((CDesktopComponents.sSpring().getCurrentTagStatus(xmltag,
							AppConstants.statusKeyDeleted)).equals(AppConstants.statusValueTrue));
					lOriginalTag = (!xmltag.getAttribute(AppConstants.keyRefstatusids).equals(""));
					if ((lPresent) && lSent && (!lDeleted) && (!lOriginalTag)) {
						String lInbox = xmltag.getChildValue("inbox");
						if (!lInbox.equals(""))
							lRootMailTags.add(xmltag);
					}
				}
			}
		}
		for (IXMLTag xmltag : lRootMailTags) {
			String lId = xmltag.getAttribute(AppConstants.keyId);
			String lInbox = xmltag.getChildValue("inbox");
			if (!lInbox.equals("")) {
				IXMLTag xmlmaptag = null;
				for (IXMLTag xmltag2 : aChildTags) {
					String lName2 = xmltag2.getName();
					if (lName2.equals("map")) {
						String lMapTitle = CDesktopComponents.sSpring().unescapeXML(xmltag2.getChildValue("name"));
						if (lMapTitle.equals(lInbox))
							xmlmaptag = xmltag2;
					}
				}
				if (xmlmaptag == null) {
					// add map with name lInbox
					xmlmaptag = CDesktopComponents.sSpring().getXmlManager().newXMLTag("map", "");
					xmlmaptag = CDesktopComponents.sSpring().getXmlManager().newNodeTag(caseComponent.getEComponent().getXmldefinition(), "map");
					xmlmaptag.setParentTag(xmltag.getParentTag());
					xmltag.getParentTag().getChildTags().add(xmlmaptag);
					aChildTags.add(xmlmaptag);
					xmlmaptag.getChild("name").setValue(CDesktopComponents.sSpring().escapeXML(lInbox));
					xmlmaptag.getChild(AppConstants.statusElement).setAttribute(AppConstants.statusKeyOutfolded, AppConstants.statusValueTrue);
				}
				if (xmlmaptag != null) {
					IXMLTag lParentTag = xmlmaptag.getParentTag();
					List<IXMLTag> lXmlChildTags = lParentTag.getChildTags();
					for (int k = (lXmlChildTags.size()-1);k>=0; k--) {
						IXMLTag xmltag3 = (IXMLTag) lXmlChildTags.get(k);
						if (xmltag3.getAttribute(AppConstants.keyId).equals(lId))
							lXmlChildTags.remove(k);
					}
					for (int k = (aChildTags.size()-1);k>=0; k--) {
						IXMLTag xmltag3 = (IXMLTag) aChildTags.get(k);
						if (xmltag3.getAttribute(AppConstants.keyId).equals(lId))
							aChildTags.remove(k);
					}
					xmltag.setParentTag(xmlmaptag);
					xmlmaptag.getChildTags().add(xmltag);
				}
			}
		}
		
		return pXmlChildsToContentItems(aChildTags, aParent, aInsertBefore);
	}

	/**
	 * P xml childs to content items.
	 * 
	 * @param aChildTags the a child tags
	 * @param aParent the a parent
	 * @param aInsertBefore the a insert before
	 * @param aStyle the a style
	 * 
	 * @return the list
	 */
	protected List<Component> pXmlChildsToContentItems(List<IXMLTag> aChildTags, Component aParent,
			Component aInsertBefore) {
		if ((aChildTags == null) || (aChildTags.size() == 0))
			return null;
		List<Component> lContentItems = new ArrayList<Component>();
		for (IXMLTag xmltag : aChildTags) {
			IXMLTag xmlchild = xmltag.getChild(AppConstants.statusElement);
			if (xmlchild != null) {
				boolean lPresent = (!(CDesktopComponents.sSpring().getCurrentTagStatus(xmltag,
						AppConstants.statusKeyPresent)).equals(AppConstants.statusValueFalse));
				boolean lSent = true;
				boolean lDeleted = false;
				boolean lOriginalTag = false;
				String lName = xmltag.getName();
				if (!lName.equals("map")) {
					// sent mails are always present
					lPresent = true;
					lSent = ((CDesktopComponents.sSpring().getCurrentTagStatus(xmltag,
							AppConstants.statusKeySent)).equals(AppConstants.statusValueTrue));
					lDeleted = ((CDesktopComponents.sSpring().getCurrentTagStatus(xmltag,
							AppConstants.statusKeyDeleted)).equals(AppConstants.statusValueTrue));
					lOriginalTag = (!xmltag.getAttribute(AppConstants.keyRefstatusids).equals(""));
				}
				if ((lPresent) && lSent && (!lDeleted) && (!lOriginalTag) && !lName.equals("attachment")) {
					//NOTE don't render attachments in tree
					Component lContentItem = renderTreeitem(aParent, null, xmltag);
					if (lContentItem != null) {
						lContentItems.add(lContentItem);
						boolean lOpened = ((CDesktopComponents.sSpring().getCurrentTagStatus(xmltag,
								AppConstants.statusKeyOutfolded)).equals(AppConstants.statusValueTrue));
						if (!lOpened)
							((Treeitem)lContentItem).setOpen(false);
						boolean lChildrenHidden = ((CDesktopComponents.sSpring().getCurrentTagStatus(
								xmltag, AppConstants.statusKeyOutfoldable)).equals(AppConstants.statusValueFalse));
						if (!lChildrenHidden)
							pXmlChildsToContentItems(xmltag.getChildTags(AppConstants.defValueNode),
									lContentItem, null);
					}
				}
			}
		}
		return lContentItems;
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunComponentHelper#isPresent(nl.surf.emergo.business.IXMLTag)
	 */
	@Override
	public boolean isPresent(IXMLTag aItem) {
		String lName = aItem.getName();
		if (!lName.equals("map"))
			// sent mails are always present
			return ((CDesktopComponents.sSpring().getCurrentTagStatus(aItem, AppConstants.statusKeySent)).equals(AppConstants.statusValueTrue));
		return super.isPresent(aItem);
	}
}
