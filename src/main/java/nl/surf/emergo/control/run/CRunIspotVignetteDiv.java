/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.Date;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;

import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.def.CDefHelper;
import nl.surf.emergo.utilities.PropsValues;

public class CRunIspotVignetteDiv extends CRunIspotInterventionDiv {

	private static final long serialVersionUID = -3627975366171387149L;

	@Override
	public void onCreate(CreateEvent aEvent) {
		super.onCreate(aEvent);
		interventionType = runIspot.interventionTypeVignette;
	}

	public void onUpdate(Event aEvent) {
		setAttribute("dataElement", aEvent.getData());
		setAttribute("redirectToNext", false);
		Events.sendEvent("onIspotInit", this, null);
		if ((Boolean) getAttribute("redirectToNext") == true) {
			Events.postEvent("onNext", vView.getComponent("ispotVignettesListbox"), null);
		} else {
			// default show instruction
			Events.sendEvent("onIspotShowInstruction", this, null);
		}
	}

	public void onIspotInit(Event aEvent) {
		Events.sendEvent("onIspotInitHeader", this, null);

		CRunIspotIntervention dataElement = (CRunIspotIntervention) getAttribute("dataElement");
		setAttribute("vignetteType", vView.getComponent("ispotVignettes").getAttribute("vignetteType"));
		// next_if_ready: next vignette only accessible after finishing (or sending)
		// recording
		setAttribute("nextIfReady", dataElement.isVignetteNextIfPublished());
		boolean isStartVignette = getAttribute("vignetteType").equals("start");

		((Image) vView.getComponent("ispotVignetteState"))
				.setSrc(runIspot.stylePath + "ispot-" + dataElement.getReactionState() + ".png");
		((Image) vView.getComponent("ispotVignetteState"))
				.setTooltiptext(vView.getLabel("run_ispot.listcell.reaction." + dataElement.getReactionState()));
		((Label) vView.getComponent("ispotVignetteTitle"))
				.setValue(dataElement.getSubjectName() + ": " + dataElement.getVignetteName());

		boolean done = dataElement.getReactionState().equals("done");
		// allow review of recording
		setAttribute("allowViewRecording", dataElement.isReviewable());
		setAttribute("canInterruptRecording", dataElement.isInterruptable());
		setAttribute("canSendRecording", dataElement.isPublishable());
		setAttribute("allowPrevious", dataElement.isVignetteAllowPrevious());
		int numberOfResits = dataElement.getNumberOfResits();
		boolean recordingAvailable = dataElement.getReactionUrl() != null && !dataElement.getReactionUrl().equals("");
		int numberOfReactions = dataElement.getNumberOfReactions();

		// NOTE set global variables for webcam
		// if autostart and no vignette is present, then do show start recording button,
		// because instruction is shown and should be closed before recording
		runIspot.recordingCanBeStarted = (!dataElement.isAutoStart() || dataElement.getVignetteUrl().equals(""));
		runIspot.recordingMaxDuration = dataElement.getMaxDuration();
		runIspot.recordingShowProgress = runIspot.recordingMaxDuration > -1;
		runIspot.recordingTimerCount = 0;

		setAttribute("previous", dataElement.getPrevious());
		setAttribute("next", dataElement.getNext());
		// if this vignette has been handled already, and is not allowed to be reviewed,
		// switch to next one if that exists
		if ((done || ((numberOfResits != -1) && (numberOfReactions > numberOfResits)))
				&& (Boolean) getAttribute("nextIfReady")
				&& !(dataElement.isVignetteReviewable() || (Boolean) getAttribute("allowViewRecording"))
				&& (getAttribute("next") != null)) {
			if (!done) {
				// something went wrong: cannot react any more, but recording not saved
				runIspot.logMessage(
						"iSpot.initVignette - redirecting to next vignette: ERROR: recording not saved, but number of allowed attempts exceeded; mark as finished - "
								+ getClass().getName());
				dataElement.setReactionState("done");
				dataElement.setReactionDoneDate(CDefHelper.getDateStrAsYMD(new Date()));
				runIspot.setInterventionStatus(dataElement, AppConstants.statusKeyFinished,
						AppConstants.statusValueTrue);
				runIspot.setInterventionStatus(dataElement, "finisheddate", dataElement.getReactionDoneDate());
			} else {
				runIspot.logMessage(
						"iSpot.initVignette - redirecting to next vignette: vignette finished, nothing reviewable - "
								+ getClass().getName());
			}
			setAttribute("redirectToNext", true);
		} else {
			setAttribute("showInstruction", !dataElement.getVignetteInstruction().equals(""));
			setAttribute("showVignette", !dataElement.getVignetteUrl().equals(""));
			setAttribute("showFeedback", recordingAvailable
					&& (dataElement.getFeedbackTexts().size() > 0 || dataElement.getFeedbackFragments().size() > 0));
			setAttribute("showRecording", recordingAvailable && (Boolean) getAttribute("allowViewRecording"));
			// NOTE no start recording button if auto start
			setAttribute("startRecording", runIspot.recordingCanBeStarted && !done && !recordingAvailable);
			setAttribute("stopRecording", true);
			// NOTE redo recording button even if auto start
			setAttribute("redoRecording",
					!done && recordingAvailable && (numberOfResits == -1 || numberOfReactions <= numberOfResits));
			// allow review of vignette video
			// if this property is false, video's can be seen until user cannot make another
			// recording any more
			setAttribute("allowReviewVignette", dataElement.isVignetteReviewable() || (numberOfResits == -1)
					|| (numberOfReactions <= numberOfResits));
			setAttribute("sendRecording",
					!done && recordingAvailable && !isStartVignette && (Boolean) getAttribute("canSendRecording"));

			// if vignette is handled, allow switch to previous one, so overrule
			// 'allowPrevious' setting
			runIspot.setComponentVisible(vView.getComponent("ispotPreviousButton" + interventionType),
					(getAttribute("previous") != null) && ((Boolean) getAttribute("allowPrevious") || done));
			// if review of recording is not allowed, then immediately proceed with next
			// vignette when ready
			// but if already finished, show 'next' button because else student can get
			// stuck
			runIspot.setComponentVisible(vView.getComponent("ispotNextButton" + interventionType),
					(getAttribute("next") != null) && (!(Boolean) getAttribute("nextIfReady")
							|| (Boolean) getAttribute("allowViewRecording") || done));
			Events.sendEvent("onIspotHideViewers", vView.getComponent("ispotViewers"), null);
			Events.sendEvent("onIspotShowButtons", this, null);
			runIspot.setComponentVisible(vView.getComponent("ispotStartRecordingButton" + interventionType),
					(Boolean) getAttribute("startRecording"));
			runIspot.setComponentVisible(vView.getComponent("ispotPauseRecordingButton" + interventionType), false);
			runIspot.setComponentVisible(vView.getComponent("ispotResumeRecordingButton" + interventionType), false);
			runIspot.setComponentVisible(vView.getComponent("ispotStopRecordingButton" + interventionType), false);
			runIspot.setComponentVisible(vView.getComponent("ispotRedoRecordingButton" + interventionType),
					(Boolean) getAttribute("redoRecording"));
			runIspot.setComponentVisible(vView.getComponent("ispotSendRecordingButton" + interventionType),
					(Boolean) getAttribute("sendRecording"));

			Events.postEvent("onUpdate", vView.getComponent("ispotFeedbackBtnsViewer"), dataElement);
		}
	}

	public void onIspotInitHeader(Event aEvent) {
		CRunIspotIntervention dataElement = (CRunIspotIntervention) getAttribute("dataElement");
		((Image) vView.getComponent("ispotVignetteState"))
				.setSrc(runIspot.stylePath + "ispot-" + dataElement.getReactionState() + ".png");
		((Label) vView.getComponent("ispotVignetteTitle"))
				.setValue(dataElement.getSubjectName() + ": " + dataElement.getVignetteName());
	}

	public void onIspotPreviousVignette(Event aEvent) {
		runIspot.logMessage("iSpot.onIspotPreviousVignette - " + getClass().getName());
		Events.sendEvent("onIspotCancelRecording", this, null);
		Events.postEvent("onPrevious", vView.getComponent("ispotVignettesListbox"), null);
	}

	public void onIspotNextVignette(Event aEvent) {
		runIspot.logMessage("iSpot.onIspotNextVignette - " + getClass().getName());
		Events.sendEvent("onIspotCancelRecording", this, null);
		Events.postEvent("onNext", vView.getComponent("ispotVignettesListbox"), null);
	}

	public void onIspotShowFeedback(Event aEvent) {
		runIspot.logMessage("iSpot.onIspotShowFeedback - " + getClass().getName());
		Events.sendEvent("onIspotShowFeedbackButtons", this, null);
		Events.sendEvent("onIspotHideButton", this, vView.getComponent("ispotShowFeedbackButton" + interventionType));
	}

	public void onIspotShowRecording(Event aEvent) {
		runIspot.logMessage("iSpot.onIspotShowRecording - " + getClass().getName());
		Events.sendEvent("onIspotShowPlayer", this,
				((CRunIspotIntervention) getAttribute("dataElement")).getReactionUrl());
		Events.sendEvent("onIspotHideButton", this, vView.getComponent("ispotShowRecordingButton" + interventionType));
	}

	@Override
	protected String getInterventionId() {
		return runIspot.getVignetteInterventionId();
	}

	public void onIspotSaveRecording(Event aEvent) {
		runIspot.logMessage("iSpot.onIspotSaveRecording - " + getClass().getName());
		CRunIspotIntervention dataElement = (CRunIspotIntervention) getAttribute("dataElement");
		int numberOfReactions = dataElement.getNumberOfReactions() + 1;
		dataElement.setNumberOfReactions(numberOfReactions);
		// set numberofreactions state of dataElement
		runIspot.setInterventionStatus(dataElement, "numberofreactions", "" + dataElement.getNumberOfReactions());
		String lRecName = "";
		if (PropsValues.STREAMING_SERVER_STORE) {
			lRecName = runIspot.getAbsoluteRecordingUrl((String) aEvent.getData());
		} else {
			// NOTE if recording is saved on emergo server, use relative url.
			lRecName = (String) aEvent.getData();
		}
		dataElement.setReactionUrl(lRecName);
		// save reaction blob for dataElement
		runIspot.saveInterventionReaction(dataElement, (String) aEvent.getData());
		runIspot.logMessage("iSpot - name of recording: " + lRecName + " - " + getClass().getName());
		Clients.evalJavaScript("showWebcamMessage('');");
		int numberOfResits = dataElement.getNumberOfResits();
		// if neither possible to review video's nor to redo or publish recording and
		// next if ready, then immediately jump to next vignette
		if (!(Boolean) getAttribute("allowViewRecording") && !dataElement.isVignetteReviewable()
				&& !(Boolean) getAttribute("canSendRecording") && (Boolean) getAttribute("nextIfReady")
				&& (numberOfResits != -1) && (numberOfReactions > numberOfResits)) {
			// cannot send recording, and nothing left to do here, so consider this vignette
			// as finished
			dataElement.setReactionState("done");
			dataElement.setReactionDoneDate(CDefHelper.getDateStrAsYMD(new Date()));
			runIspot.setInterventionStatus(dataElement, AppConstants.statusKeyFinished, AppConstants.statusValueTrue);
			runIspot.setInterventionStatus(dataElement, "finisheddate", dataElement.getReactionDoneDate());
			if (getAttribute("next") != null) {
				runIspot.logMessage(
						"iSpot.onIspotSaveRecording - no attempt left, not publishable, nothing reviewable; mark as finished; redirecting to next vignette - "
								+ getClass().getName());
				Events.postEvent("onNext", vView.getComponent("ispotVignettesListbox"), null);
			} else {
				runIspot.logMessage(
						"iSpot.onIspotSaveRecording - no attempt left, not publishable, nothing reviewable; mark as finished; last vignette handled - "
								+ getClass().getName());
				// last vignette handled; let proceeding depend on script?
				Events.sendEvent("onIspotInit", this, null);
			}
		} else {
			Events.sendEvent("onIspotInit", this, null);
		}
	}

	public void onIspotSendRecording(Event aEvent) {
		runIspot.logMessage("iSpot.onIspotSendRecording - " + getClass().getName());
		CRunIspotIntervention dataElement = (CRunIspotIntervention) getAttribute("dataElement");
		dataElement.setReactionState("done");
		dataElement.setReactionDoneDate(CDefHelper.getDateStrAsYMD(new Date()));
		// set finished state of dataElement
		runIspot.setInterventionStatus(dataElement, AppConstants.statusKeyFinished, AppConstants.statusValueTrue);
		runIspot.setInterventionStatus(dataElement, "finisheddate", dataElement.getReactionDoneDate());
		Events.postEvent("onUpdateState", vView.getComponent("ispotVignettesListbox"), dataElement);
		if (((Boolean) getAttribute("nextIfReady") && !(Boolean) getAttribute("allowViewRecording"))) {
			if (getAttribute("next") != null) {
				runIspot.logMessage(
						"iSpot.onIspotSendRecording - next if ready, nothing reviewable; redirecting to next vignette - "
								+ getClass().getName());
				Events.postEvent("onNext", vView.getComponent("ispotVignettesListbox"), null);
			} else {
				runIspot.logMessage(
						"iSpot.onIspotSendRecording - next if ready, nothing reviewable; last vignette handled - "
								+ getClass().getName());
				// last vignette handled; let proceeding depend on script?
				runIspot.setComponentVisible(vView.getComponent("ispotRedoRecordingButton" + interventionType), false);
				runIspot.setComponentVisible(vView.getComponent("ispotSendRecordingButton" + interventionType), false);
			}
		} else {
			Events.sendEvent("onIspotInit", this, null);
		}
	}

	public void onIspotStreamCompleted(Event aEvent) {
		runIspot.logMessage("iSpot.onIspotStreamCompleted - " + getClass().getName());
		if (vView.getComponent("ispotVignette").isVisible()
				&& !vView.getComponent("ispotShowVignetteButton" + interventionType).isVisible()
				&& (Boolean) getAttribute("allowReviewVignette")) {
			// NOTE if vignette div is visible and the show vignette button not, the last
			// user action must have been a click on
			// the show vignette button, so the vignette stream is completed.
			CRunIspotIntervention dataElement = (CRunIspotIntervention) getAttribute("dataElement");
			if (dataElement.getReactionUrl() == null || dataElement.getReactionUrl().equals("")) {
				// NOTE only start recording automatically if there is no recording available
				// yet.
				Events.sendEvent("onIspotStartRecording", this, null);
			}
		}
	}

	@Override
	public void onIspotShowButtons(Event aEvent) {
		runIspot.setComponentVisible(vView.getComponent("ispotShowInstructionButton" + interventionType),
				(Boolean) getAttribute("showInstruction"));
		runIspot.setComponentVisible(vView.getComponent("ispotShowVignetteButton" + interventionType),
				(Boolean) getAttribute("showVignette") && (Boolean) getAttribute("allowReviewVignette"));
		runIspot.setComponentVisible(vView.getComponent("ispotShowFeedbackButton" + interventionType),
				(Boolean) getAttribute("showFeedback"));
		runIspot.setComponentVisible(vView.getComponent("ispotShowRecordingButton" + interventionType),
				(Boolean) getAttribute("showRecording"));
	}

}
