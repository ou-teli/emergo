/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit;

import org.zkoss.zk.ui.Component;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;

public class CRunPVToolkitNavigationImage extends CRunPVToolkitDefImage {

	private static final long serialVersionUID = -6645016435693240646L;

	protected String _componentFromId = "";

	protected String _componentToId = "";

	public CRunPVToolkitNavigationImage(Component parent, String[] keys, Object[] values) {
		super(parent, keys, values);
	}
	
	public void init(String componentFromId, String componentToId) {
		_componentFromId = componentFromId;
		_componentToId = componentToId;
	}

	public void onClick() {
		Component componentFrom = CDesktopComponents.vView().getComponent(_componentFromId);
		if (componentFrom != null) {
			componentFrom.setVisible(false);
		}
		Component componentTo = CDesktopComponents.vView().getComponent(_componentToId);
		if (componentTo != null) {
			componentTo.setVisible(true);
		}
	}

}
