/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.OpenEvent;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Div;
import org.zkoss.zul.Html;
import org.zkoss.zul.Label;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CIObserved;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.def.CDefHtml;
import nl.surf.emergo.control.def.CDefLabel;
import nl.surf.emergo.control.def.CDefPopup;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.zkspring.STriggeredReference;

/**
 * The Class CRunAlert is used to show alerts within the run choice area of the Emergo player.
 * If an alert is shown on top of another alert, the previous alert is saved temporarily.
 */
public class CRunAlert extends CRunArea {

	private static final long serialVersionUID = -6436010262700491607L;

	/** The run component. */
	protected CRunComponent runComponent = null;

	/** The case components, used to temporarily save case components. */
	protected List<IECaseComponent> caseComponents = new ArrayList<IECaseComponent>();

	/** The alert tags, used to temporarily save alert tags. */
	protected List<IXMLTag> alertTags = new ArrayList<IXMLTag>();

	/** The titles, used to temporarily save alert titles. */
	protected List<String> titles = new ArrayList<String>();

	/** The contents, used to temporarily save contents. */
	protected List<String> contents = new ArrayList<String>();
	
	protected Boolean accumulate;
	
	protected CDefPopup runAlertPopup;
	
	/**
	 * Instantiates a new c run conversation interaction, a title and a text area.
	 */
	public CRunAlert() {
		super();
		setId("runAlert");
		runComponent = null;
		init();
	}

	/**
	 * Instantiates a new c run conversation interaction, a title and a text area.
	 *
	 * @param aId the a id
	 * @param aRunComponent the a run component
	 */
	public CRunAlert(String aId, CRunComponent aRunComponent) {
		super();
		setId(aId);
		runComponent = aRunComponent;
		init();
	}

	/**
	 * Sets run component.
	 */
	public void setRunComponent(CRunComponent aRunComponent) {
		runComponent = aRunComponent;
	}

	/**
	 * Instantiates a title and a text area.
	 */
	public void init() {
		String lType = "";
		if (runWnd != null)
			lType = runWnd.getPlayerStatusString("ChoiceAreaLayoutStatus", null);
		setZclass(getClassName() + lType);

		CRunHbox lHbox = new CRunHbox();
		appendChild(lHbox);
		createTitleArea(lHbox);
		createTextArea(this);
		createButtonsArea(this);
	}

	/**
	 * Creates a title area with a fixed title. This area also has a
	 * close button to close the alert.
	 *
	 * @param aParent the a parent
	 */
	protected void createTitleArea(Component aParent) {
		String lType = "";
		if (runWnd != null)
			lType = runWnd.getPlayerStatusString("ChoiceAreaLayoutStatus", null);
		CRunArea lArea = new CRunArea();
		aParent.appendChild(lArea);
		lArea.setZclass(getClassName()+lType+"_title_area_left");
		CRunHbox lHbox = new CRunHbox();
		lArea.appendChild(lHbox);
		Label lLabel = new CDefLabel();
		lLabel.setId(getId()+"Title");
		lLabel.setZclass(getClassName() + "_title_area_label");
		lHbox.appendChild(lLabel);

		lArea = new CRunArea();
		aParent.appendChild(lArea);
		lArea.setZclass(getClassName()+"_title_area_right");
		CRunCloseBtn lButton = newCloseBtn();
		lButton.setCanHaveStatusSelected(false);
//		lButton.registerObserver(CControl.runWnd);
		lButton.registerObserver("runAlert");
		lArea.appendChild(lButton);
	}

	/**
	 * Creates new close button.
	 *
	 * @return the button
	 */
	protected CRunCloseBtn newCloseBtn() {
		return new CRunCloseBtn(getId() + "CloseBtn", "active","endAlert", this, "close", "");
	}

	/**
	 * Creates alert text area.
	 *
	 * @param aParent the a parent
	 *
	 * @return the area
	 */
	protected CRunArea createTextArea(Component aParent) {
		String lType = "";
		if (runWnd != null)
			lType = runWnd.getPlayerStatusString("ChoiceAreaLayoutStatus", null);
		CRunArea lTextArea = new CRunArea();
		lTextArea.setZclass(getClassName() + lType + "_text_area");
		Div lDiv = new CDefDiv();
		lTextArea.appendChild(lDiv);
		lDiv.setZclass(getClassName() + lType + "_text_area_div");
		Html lHtml = new CDefHtml();
		lDiv.appendChild(lHtml);
		lHtml.setId(getId()+"Html");
		lHtml.setZclass(getClassName() + "_title_area_label");
		aParent.appendChild(lTextArea);
		return lTextArea;
	}

	/**
	 * Creates alert buttons area.
	 *
	 * @param aParent the a parent
	 *
	 * @return the area
	 */
	protected CRunArea createButtonsArea(Component aParent) {
		String lType = "";
		if (runWnd != null)
			lType = runWnd.getPlayerStatusString("ChoiceAreaLayoutStatus", null);
		CRunArea lButtonsArea = new CRunArea();
		aParent.appendChild(lButtonsArea);
		lButtonsArea.setId(getId()+"ButtonsArea");
		lButtonsArea.setZclass(getClassName() + lType + "_buttons_area");
		return lButtonsArea;
	}

	/**
	 * Sets the alert content and adds it to previous state.
	 *
	 * @param aContent the a content
	 */
	public void setContent(String aContent) {
		setContent(null, aContent);
	}

	/**
	 * Sets the alert title and content and adds it to previous state.
	 *
	 * @param aTitle the a title
	 * @param aContent the a content
	 */
	public void setContent(String aTitle, String aContent) {
		setContent(null, null, aTitle, aContent);
	}

	/**
	 * Sets the alert title and content and adds it to previous state.
	 *
	 * @param aCaseComponent the a case component
	 * @param aAlertTag the a alert tag
	 * @param aTitle the a title
	 */
	public void setContent(IECaseComponent aCaseComponent, IXMLTag aAlertTag, String aTitle) {
		setContent(aCaseComponent, aAlertTag, aTitle, "");
	}

	/**
	 * Sets the alert title and content and adds it to previous state.
	 *
	 * @param aCaseComponent the a case component
	 * @param aAlertTag the a alert tag
	 * @param aTitle the a title
	 * @param aContent the a content
	 */
	protected void setContent(IECaseComponent aCaseComponent, IXMLTag aAlertTag, String aTitle, String aContent) {
		boolean lPopup = false;
		if (aAlertTag != null) {
			aContent = CDesktopComponents.sSpring().unescapeXML(aAlertTag.getChildValue("richtext"));
			lPopup = aAlertTag.getChild(AppConstants.statusElement).getDefAttribute(AppConstants.statusKeyPopup).equals(AppConstants.statusValueTrue);
			if (accumulate == null) {
				//NOTE accumulate is a property of the alerts component
				IXMLTag lRootTag = aAlertTag.getParentTag();
				while (lRootTag != null && !lRootTag.getName().equals(AppConstants.rootElement)) {
					lRootTag = lRootTag.getParentTag();
				}
				if (lRootTag != null) {
					accumulate = new Boolean(!lRootTag.getChild(AppConstants.componentElement).getChild(AppConstants.statusElement).getDefAttribute(AppConstants.statusKeyAccumulate).equals(AppConstants.statusValueFalse));
				}
				else {
					accumulate = new Boolean(true);
				}
			}
		}
		//NOTE default accumulate is true. It is the behavior for alerts before accumulate could be set.
		boolean lAccumulate = true;
		if (accumulate != null) {
			lAccumulate = accumulate.booleanValue();
		}
		setAlertBehavior(lPopup);
		if (StringUtils.isEmpty(aTitle)) {
			aTitle = CDesktopComponents.vView().getLabel("run_alert.title");
		}
		if (!lAccumulate) {
			caseComponents.clear();
			alertTags.clear();
			titles.clear();
			contents.clear();
		}
		caseComponents.add(aCaseComponent);
		alertTags.add(aAlertTag);
		titles.add(aTitle);
		contents.add(aContent);

		pSetTitle(aTitle);
		pSetContent(aContent);
		pSetButtons(aAlertTag);
		if (aCaseComponent != null && aAlertTag != null) {
			//NOTE set opened of alert to true
			setRunTagStatus(aCaseComponent, aAlertTag, AppConstants.statusKeyOpened, AppConstants.statusValueTrue);
		}					
	}

	/**
	 * Sets the alert behavior, either popup or not.
	 * 
	 * @param aPopup the a popup
	 */
	protected void setAlertBehavior(boolean aPopup) {
		if (aPopup) {
			if (runAlertPopup == null) {
				runAlertPopup = new CDefPopup();
				runWnd.appendChild(runAlertPopup);
				runAlertPopup.setStyle("color:transparent;background:transparent;border:0px;box-shadow:0 0 0px rgba(0,0,0,0.35);");
				runAlertPopup.addEventListener("onOpen",
					new EventListener<OpenEvent>() {
						public void onEvent(OpenEvent event) {
							if (!event.isOpen()) {
								//popup is closed
								observedNotify(null, "endAlert", null);
							}
						}
					});
			}
			setParent(runAlertPopup);
			setStyle("left:0px;top:0px;");
			runAlertPopup.open(runWnd);
			Clients.evalJavaScript("setSizeToSizeOfOtherComponent('" + runAlertPopup.getUuid() + "','" + getUuid() + "');");
			Clients.evalJavaScript("centerOnOtherComponent('" + runAlertPopup.getUuid() + "','" + runWnd.getUuid() + "');");
		}
		else {
			setParent(runWnd);
			setStyle("");
			if (runAlertPopup != null) {
				runAlertPopup.detach();
				runAlertPopup = null;
			}
		}
	}

	/**
	 * Sets the alert title.
	 *
	 * @param aContent the a content
	 */
	private void pSetTitle(String aTitle) {
		Label lTitle = (Label) CDesktopComponents.vView().getComponent(getId()+"Title");
		if (lTitle != null) {
			lTitle.setValue(aTitle);
		}
	}

	/**
	 * Sets the alert content within the text area.
	 *
	 * @param aContent the a content
	 */
	private void pSetContent(String aContent) {
		Html lHtml = (Html) CDesktopComponents.vView().getComponent(getId()+"Html");
		if (lHtml != null) {
			lHtml.setContent(aContent);
		}
	}

	/**
	 * Sets the alert buttons within the buttons area.
	 *
	 * @param aAlertTag the a alert tag
	 */
	private void pSetButtons(IXMLTag aAlertTag) {
		if (aAlertTag == null) {
			return;
		}
		Component lButtonsArea = CDesktopComponents.vView().getComponent(getId()+"ButtonsArea");
		lButtonsArea.getChildren().clear();
		for (IXMLTag lButtonTag : aAlertTag.getChilds("clickableobject")) {
			renderButton(lButtonsArea, lButtonTag);
		}
	}

	protected Component renderButton(Component aButtonsArea, IXMLTag aItem) {
		HtmlMacroComponent macro = new HtmlMacroComponent();
		aButtonsArea.appendChild(macro);
		macro.setStyle(getStylePosition(aItem));
		macro.setDynamicProperty("a_observer", this.getId());
		macro.setDynamicProperty("a_event", "onButtonClicked");
		macro.setDynamicProperty("a_eventdata", aItem);
		macro.setDynamicProperty("a_label", CDesktopComponents.sSpring().unescapeXML(aItem.getChildValue("text")));
		macro.setDynamicProperty("a_classtype", "DarkGreen");
		macro.setMacroURI("../run_input_labelbtn_macro.zul");
		return macro;
	}

	protected String getStylePosition(IXMLTag tag) {
		String stylePosition = "";
		String[] position = tag.getChildValue("position").split(",");
		if (position.length == 2) {
			String left = getNumberStr(position[0], "");
			String top = getNumberStr(position[1], "");
			if (!left.equals("") && !top.equals("")) {
				stylePosition += "left:" + left + "px;top:" + top + "px;";
			}
		}
		if (!stylePosition.equals("")) {
			stylePosition = "position:absolute;" + stylePosition;
		}
		return stylePosition;
	}

	protected String getNumberStr(String aNumber, String aDefaultStr) {
		String number = aDefaultStr;
		try {
			number = "" + Integer.parseInt(aNumber);
		} catch (NumberFormatException e) {
			number = aDefaultStr;
		}
		return number;
	}

	protected int getNumber(String aNumber, int aDefaultNumber) {
		int number = aDefaultNumber;
		try {
			number = Integer.parseInt(aNumber);
		} catch (NumberFormatException e) {
			number = aDefaultNumber;
		}
		return number;
	}

	/**
	 * Has previous content.
	 *
	 * @return if true
	 */
	public boolean hasPreviousContent() {
		return (contents.size() > 1);
	}

	/**
	 * Restores previous content item within the text area.
	 */
	public void restoreContent() {
		int lSize = caseComponents.size();
		if (lSize > 1) {
			// remove last added case component, alert tag, title and content
			caseComponents.remove(lSize-1);
			alertTags.remove(lSize-1);
			titles.remove(lSize-1);
			contents.remove(lSize-1);

			IXMLTag lAlertTag = alertTags.get(lSize-2);
			boolean lPopup = false;
			if (lAlertTag != null) {
				lPopup = lAlertTag.getChild(AppConstants.statusElement).getDefAttribute(AppConstants.statusKeyPopup).equals(AppConstants.statusValueTrue);
			}
			setAlertBehavior(lPopup);
			
			// set previous title and content
			pSetTitle(titles.get(lSize-2));
			pSetContent(contents.get(lSize-2));
			pSetButtons(lAlertTag);
		}
	}

	/**
	 * Is called by several other components for notification.
	 * Calls method onAction, so see this method for possible aActions.
	 *
	 * @param aObserved the observed object, it should have interface CIObserved implemented
	 * @param aAction the action
	 * @param aStatus the status object, can be String or other class
	 */
	@Override
	public void observedNotify(CIObserved aObserved, String aAction, Object aStatus) {
		if (aAction.equals("endAlert")) {
			int lSize = caseComponents.size();
			if (lSize > 0 && caseComponents.get(lSize-1) != null && alertTags.get(lSize-1) != null) {
				//NOTE set opened of alert to false
				setRunTagStatus(caseComponents.get(lSize-1), alertTags.get(lSize-1), AppConstants.statusKeyOpened, AppConstants.statusValueFalse);
			}
			if (hasPreviousContent()) {
				restoreContent();
			}
			else {
				detach();
				if (runAlertPopup != null) {
					runAlertPopup.detach();
					runAlertPopup = null;
				}
			}
		}
	}

	/**
	 * Sets run group tag status. Used when content item status changes.
	 *
	 * @param aCaseComponent the a case component used by the run component
	 * @param aTag the a tag
	 * @param aStatusKey the a status key
	 * @param aStatusValue the a status value
	 *
	 * @return the XML status tag
	 */
	public IXMLTag setRunTagStatus(IECaseComponent aCaseComponent, IXMLTag aTag, String aStatusKey, String aStatusValue) {
		return CDesktopComponents.sSpring().setRunTagStatus(aCaseComponent, aTag, aStatusKey, aStatusValue, null,
				true, AppConstants.statusTypeRunGroup, true, false);
	}

	
	/**
	 * Handles status change due to firing of script actions.
	 *
	 * @param aTriggeredReference the a triggered reference
	 */
	public void handleStatusChange(STriggeredReference aTriggeredReference) {
		// overwrite this method
	}

	/**
	 * Called when clickable object corresponding to clickable object tag is clicked.
	 */
	public void onButtonClicked(Event aEvent) {
		IXMLTag tag = (IXMLTag)aEvent.getData();
		//NOTE reforiginalid must be set for a clickable object.
		//During the sending of an alert a copy of the alert tag is made and also of its child tags, including the clickable object tags.
		//The copied clickable object tags then will get an attribute reforiginalid that contains the original data tag id.
		//Use this id to get the original datatag, because status has to be saved for this tag. Otherwise script won't function correctly.
		if (tag == null || !tag.getName().equals("clickableobject") || tag.getAttribute(AppConstants.keyReforiginalid).equals(""))
			return;
		int lSize = caseComponents.size();
		if (lSize > 0 && caseComponents.get(lSize-1) != null) {
			tag = CDesktopComponents.sSpring().getCaseComponentXmlDataTagById(caseComponents.get(lSize-1), tag.getAttribute(AppConstants.keyReforiginalid));
			setRunTagStatus(caseComponents.get(lSize-1), tag, AppConstants.statusKeySelected, AppConstants.statusValueTrue);
		}
		observedNotify(null, "endAlert", null);
	}

}
