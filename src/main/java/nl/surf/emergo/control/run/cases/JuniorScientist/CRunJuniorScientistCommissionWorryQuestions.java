/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.JuniorScientist;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.event.Event;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;

/**
 * This class is used to show a worry question with two options during phase 5. There are 7 worry questions. During a conversation a worry question can be shown. After answering the worry question the conversation continues.
 * An editforms component is used for storage of worry questions, possible answers and answer given. And two states are used for correct operation.
 */
public class CRunJuniorScientistCommissionWorryQuestions extends CRunJuniorScientistFeedbackDinaBox {

	private static final long serialVersionUID = -3185007566865250710L;

	/** Defines background images to be used for each worry question. Images are located in skins/cases/JuniorScientist/assets folder.  */
	protected static final String[] imageNames = new String[]
			{
					"hilbers-skeptisch",
					"janssen-skeptisch",
					"munstermann-skeptisch",
					"hilbers-skeptisch",
					"janssen-skeptisch",
					"munstermann-skeptisch",
					"janssen-skeptisch"
			};
	
	/** Two states used for correct operation.
	 *  The first is set in script and is used to know which worry question to present.
	 *  The second is set in this class when an answer is chosen and is used in script to show the correct feedback of Hella. */
	protected String commissionCurrentWorryQuestionStateKeyPid = "";
	protected String commissionNumberOfCorrectAnswersStateKeyPid = ""; 

	@Override
	public void onInit() {
		super.onInitMacro();
		
		feedbackCaseComponentName = "phase_5_commission_worry_questions";
		commissionCurrentWorryQuestionStateKeyPid = "game_5_commission_current_worry_question";
		commissionNumberOfCorrectAnswersStateKeyPid = "game_5_commission_number_of_correct_answers";
		feedbackReadyStateKeyPid = "game_5_commission_ready";

		feedbackAppCaseComponent = getFeedbackAppCaseComponent();

		piecePopupMacroId = getUuid() + "_piece_popup_macro";
		piecePopupId = getUuid() + "_piece_popup";
		
		referencesCaseComponent = getReferencesComponent();
		//NOTE super ancestor CRunComponent needs case component to be set
		setCaseComponent(referencesCaseComponent);

		//determine references properties to be used within the child macro
		setCommissionProperties();
		
		//add the child macro
		addChildMacro("JuniorScientist_commission_worry_questions_view_macro.zul");
	}
	
	@Override
	protected int getNumberOfSelectors() {
		return 7;
	}
	
	@Override
	protected boolean useScrollIntoView() {
		return false;
	}
	
	@Override
	protected void setCommissionProperties() {
		// General
		
		propertyMap.put("sender", getStateTagValue(employeeCardNameStateKey));

		// Selectors
		
		int currentWorryQuestionNumber = Integer.parseInt(getStateTagValue(commissionCurrentWorryQuestionStateKeyPid));
		String currentWorryQuestionPid = selectorPidPrefix + currentWorryQuestionNumber;
		
		numberOfGivenAnswers = 0;
		numberOfCorrectAnswers = 0;

		selectorTags = new ArrayList<IXMLTag>();
		
		StringBuffer selectorsBuffer = new StringBuffer();
		
		//put all node tags in a map per pid to be able to get them by number later on
		Map<String,IXMLTag> nodeTagsByPid = getNodeTagsByPid();
		int counter = 1;
		boolean render = true;
		
		Iterator<Map.Entry<String,IXMLTag>> iterator = nodeTagsByPid.entrySet().iterator();
	    while(iterator.hasNext()) {
			//content to be rendered is entered in two content elements per selector
			IXMLTag selectorTag = iterator.next().getValue();
			String pid = sSpring.unescapeXML(selectorTag.getChildValue("pid"));
			if (pid.startsWith(selectorPidPrefix)) {
				selectorTags.add(selectorTag);
				
				StringBuffer jsonBuffer = new StringBuffer();
				boolean isCurrent = pid.equals(currentWorryQuestionPid);
				addJsonKeyValue(jsonBuffer, "isCurrent", "" + isCurrent, true);
				addJsonKeyValue(jsonBuffer, "tagId", selectorTag.getAttribute(AppConstants.keyId), true);
				addJsonKeyValue(jsonBuffer, "counter", "" + counter, true);
				addJsonKeyValue(jsonBuffer, "render", "" + render, true);
				
				addJsonKeyValue(jsonBuffer, "title", sSpring.unescapeXML(selectorTag.getChildValue("title")), true);
				addJsonKeyValue(jsonBuffer, "correctAnswer", selectorTag.getChildValue("correctoptions"), true);

				//get default or given answer
				String answer = getAnswer(selectorTag);
				addJsonKeyValue(jsonBuffer, "answer", answer, true);

				boolean isAnswerEmpty = answer.equals("");
				if (!isAnswerEmpty) {
					numberOfGivenAnswers++;
				}
				else {
					render = false;
				}
				//is answer given correct
				boolean isAnswerCorrect = isAnswerCorrect(selectorTag, answer);
				if (isAnswerCorrect) {
					numberOfCorrectAnswers++;
				}

				addJsonKeyValue(jsonBuffer, "disabled", "" + isAnswerCorrect, true);
				
				StringBuffer selectorOptionsBuffer = new StringBuffer();

				//get options within selector
				List<String> options = Arrays.asList(selectorTag.getChildValue("options").split("\n"));
				int optionCounter = 1;
				int selectedIndex = -1;
				for (String option : options) {
					StringBuffer jsonBuffer2 = new StringBuffer();
					addJsonKeyValue(jsonBuffer2, "option", option, true);
					addJsonKeyValue(jsonBuffer2, "optionCounter", "" + optionCounter, false);
					//preselect given answer
					if (("" + optionCounter).equals(answer)) {
						selectedIndex = optionCounter - 1;
					}

					if (selectorOptionsBuffer.length() > 0) {
						selectorOptionsBuffer.append(",");
					}
					selectorOptionsBuffer.append("{");
					selectorOptionsBuffer.append(jsonBuffer2);
					selectorOptionsBuffer.append("}");

					optionCounter++;
				}
				selectorOptionsBuffer.insert(0, "[");
				selectorOptionsBuffer.append("]");

				addJsonKeyValue(jsonBuffer, "options", selectorOptionsBuffer, true);
				addJsonKeyValue(jsonBuffer, "selectedIndex", "" + selectedIndex, false);

				if (selectorsBuffer.length() > 0) {
					selectorsBuffer.append(",");
				}
				selectorsBuffer.append("{");
				selectorsBuffer.append(jsonBuffer);
				selectorsBuffer.append("}");
				
				counter++;
			}
	    }

		selectorsBuffer.insert(0, "[");
		selectorsBuffer.append("]");
		
		propertyMap.put("imageName", imageNames[currentWorryQuestionNumber - 1]);

		propertyMap.put("selectors", selectorsBuffer.toString());
		
		propertyMap.put("numberOfSelectors", getNumberOfSelectors());
		propertyMap.put("numberOfGivenAnswers", numberOfGivenAnswers);
		propertyMap.put("numberOfCorrectAnswers", numberOfCorrectAnswers);

		// Pieces
		
		propertyMap.put("referencesTitle", sSpring.unescapeXML(sSpring.getCaseComponentRoleName("", referencesCaseComponent.getName())));
		
		//NOTE get pieces within folder 'Fase 3 - Data-analyse'
		List<Map<String,Object>> pieces = new ArrayList<Map<String,Object>>();
		propertyMap.put("pieces", pieces);
		IXMLTag rootTag = sSpring.getXmlDataPlusRunStatusTree(referencesCaseComponent, AppConstants.statusTypeRunGroup);
		if (rootTag == null) {
			return;
		}
		IXMLTag contentTag = rootTag.getChild(AppConstants.contentElement);
		if (contentTag == null) {
			return;
		}
		
		IXMLTag correctFolderTag = null;
		//loop through folders
		for (IXMLTag folderTag : contentTag.getChilds("map")) {
			if (sSpring.unescapeXML(folderTag.getChildValue("name")).equals("Fase 5 - Reflectie")) {
				correctFolderTag = folderTag;
				break;
			}
		}
		if (correctFolderTag != null) {
			for (IXMLTag pieceTag : correctFolderTag.getChilds("piece")) {
				if (!pieceTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse)) {
					Map<String,Object> hPieceData = new HashMap<String,Object>();
					pieces.add(hPieceData);
					hPieceData.put("tag", pieceTag);
					hPieceData.put("pieceName", sSpring.unescapeXML(pieceTag.getChildValue("name")));
				}
			}
		}

		propertyMap.put("pieceData", getPieceData(null));
		
	}
	
	protected void addJsonKeyValue(StringBuffer element, String key, String value, boolean separator) {
		element.append("\"");
		element.append(key);
		element.append("\":\"");
		element.append(value);
		element.append("\"");
		if (separator) {
			element.append(",");
		}
	}

	protected void addJsonKeyValue(StringBuffer element, String key, StringBuffer value, boolean separator) {
		element.append("\"");
		element.append(key);
		element.append("\":");
		element.append(value);
		if (separator) {
			element.append(",");
		}
	}

	@Override
	public void onGiveAnswer(Event event) {
		super.onGiveAnswer(event);
		
		setStateTagValue(commissionNumberOfCorrectAnswersStateKeyPid, "" + numberOfCorrectAnswers());
	}

	protected int numberOfCorrectAnswers() {
		//put all node tags in a map per pid to be able to get them by number later on
		Map<String,IXMLTag> nodeTagsByPid = getNodeTagsByPid();
	    Iterator<Map.Entry<String,IXMLTag>> iterator = nodeTagsByPid.entrySet().iterator();
		int number = 0;
	    while(iterator.hasNext()) {
			//content to be rendered is entered in two content elements per selector
			IXMLTag selectorTag = iterator.next().getValue();
			String pid = sSpring.unescapeXML(selectorTag.getChildValue("pid"));
			if (pid.startsWith(selectorPidPrefix) && isAnswerCorrect(selectorTag, getAnswer(selectorTag))) {
				number++;
			}
		}
		return number;
	}
	
}
