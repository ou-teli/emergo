/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Html;
import org.zkoss.zul.Label;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.Window;

import nl.surf.emergo.business.IXMLAttributeValueTime;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.business.impl.XMLAttributeValueTime;
import nl.surf.emergo.control.CCaseHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CIObserved;
import nl.surf.emergo.control.def.CDefHtml;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;

/**
 * The Class CRunAssessments is used to show assessments within the run view area of the
 * Emergo player.
 */
public class CRunAssessments extends CRunComponent {

	private static final long serialVersionUID = -6607588081551633473L;

	/** The refitem, the selected refitem xml tag within the assessments case component data. */
	protected IXMLTag refitem = null;

	/** The itemscarid, the case role id for the items case component. */
	protected String itemscarid = "0";

	/** The itemscacid, the case component id of the items case component. */
	protected String itemscacid = "0";

	/** The item, the selected item xml tag within the items case component data. */
	protected IXMLTag item = null;

	/** The radio button which is checked. */
	protected CRunAssessmentItemRadio radioChecked = null;

	/** The renderAssLock, used to prevent rendering assessments tree if it is already rendered. */
	protected boolean renderAssLock = false;

	/** The directFeedback, is feedback is shown directly, if possible. */
	protected boolean mkeaDirectFeedback = false;

	/** The showFeedback, is feedback shown. */
	protected boolean showFeedback = false;

	/**
	 * Instantiates a new c run assessments.
	 */
	public CRunAssessments() {
		super("runAssessments", null);
		mkeaDirectFeedback = CDesktopComponents.sSpring().getCurrentRunComponentStatus(caseComponent, AppConstants.statusKeyDirectfeedback, getRunStatusType()).equals(AppConstants.statusValueTrue);
	}

	/**
	 * Instantiates a new c run assessments.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the assessments case component
	 */
	public CRunAssessments(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
		mkeaDirectFeedback = CDesktopComponents.sSpring().getCurrentRunComponentStatus(caseComponent, AppConstants.statusKeyDirectfeedback, getRunStatusType()).equals(AppConstants.statusValueTrue);
	}

	protected boolean itemHasDirectFeedback(IXMLTag aItemTag) {
		if (!getItemType(aItemTag).equals("mkea")) {
			return false;
		}
		return mkeaDirectFeedback;
	}
	
	protected String getItemType(IXMLTag aItemTag) {
		return aItemTag.getChildValue("type");
	}

	/**
	 * Creates title area, content area to show assessments tree, item area to show
	 * selected item and buttons area with close button.
	 * Renders assessments tree and if there are autostart assessments, starts them.
	 * Non autostart assessments get a start or an end assessment button within the tree.
	 */
	@Override
	protected void createComponents() {
		CRunVbox lVbox = new CRunVbox();
		appendChild(lVbox);

		createTitleArea(lVbox);
		CRunHbox lHbox = new CRunHbox();
		lVbox.appendChild(lHbox);
//		initially only render assessment tags
//		if one starts assessment by clicking on start button render assessment items
//		check if number of wanted items is smaller then available items
//		if so randomize items to show
//		if assessment is ended by clicking on ready button, determine and save assessment score, and freeze items (using Accessible?)
//		if one starts assessment again by clicking on start button again render new assessment items
		createContentArea(lHbox);
		CRunArea lItemArea = createItemArea(lHbox);
		lItemArea.setId(getId()+"ItemArea");
		createButtonsArea(lVbox);

		CRunTree lRunTree = (CRunTree)getRunContentComponent();
		List<IXMLTag> lAutostartAssessments = new ArrayList<IXMLTag>();
		if ((lRunTree != null) && (lRunTree.getTreechildren() != null)) {
			List<Component> lTreeitems = lRunTree.getTreechildren().getChildren();
			for (Component lTreeitem : lTreeitems) {
				IXMLTag lAssessmentTag = lRunTree.getContentItemTag(lTreeitem);
				if (lAssessmentTag != null) {
					boolean lStarted = false;
					boolean lAutostart = false;
					lStarted = lAssessmentTag.getCurrentStatusAttribute(AppConstants.statusKeyStarted).equals(AppConstants.statusValueTrue);
					lAutostart = (CDesktopComponents.sSpring().getCurrentRunTagStatus(caseComponent,lAssessmentTag,AppConstants.statusKeyAutostart, getRunStatusType()).equals(AppConstants.statusValueTrue));
					if ((!lStarted) && (lAutostart)) {
						lAutostartAssessments.add(lAssessmentTag);
					}
				}
			}
		}
		for (IXMLTag lAssessmentTag : lAutostartAssessments) {
			startAssessment(lAssessmentTag);
		}
	}

	/**
	 * Creates new content component, the assessments tree.
	 *
	 * @return the component
	 */
	@Override
	protected Component newContentComponent() {
		return (new CRunAssessmentsTree("runAssessmentsTree", caseComponent, this));
	}

	/**
	 * Creates buttons area and adds close button.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run hbox
	 */
	@Override
	protected CRunHbox createButtonsArea(Component aParent) {
		CRunHbox lButtonsHbox = super.createButtonsArea(aParent);
		createCloseButton(lButtonsHbox);
		return lButtonsHbox;
	}

	/**
	 * Does contentitem action, clicking on an assessment item.
	 * Shows item within item area.
	 *
	 * @param aContentItem the a contentitem, the assessment item clicked
	 */
	@Override
	public void doContentItemAction(Component aContentItem) {
		Treeitem lTreeitem = (Treeitem)aContentItem;
		IXMLTag tag = (IXMLTag)lTreeitem.getAttribute("item");
		String carrefid = (String)lTreeitem.getAttribute("carrefid");
		String cacrefid = (String)lTreeitem.getAttribute("cacrefid");
		IXMLTag tagref = (IXMLTag)lTreeitem.getAttribute("tagref");
		if ((tag == null) || (tagref == null) || (!tag.getName().equals("refitem")))
			return;
		refitem = tag;
		itemscarid = carrefid;
		itemscacid = cacrefid;
		item = tagref;
		CRunArea lItemArea = (CRunArea)CDesktopComponents.vView().getComponent(getId()+"ItemArea");
		if (lItemArea != null)
			createItem(lItemArea,tag,tagref);
	}

	/**
	 * Shows item within item area. First removes previous item.
	 * If assessment is started but not yet finished, the item choice can be changed.
	 * If assessment is ended last choice is shown, but can not be changed. That is also
	 * the case if the item is not accessible anymore.
	 * Item contains item text possibly having references and item options possibly
	 * having references.
	 * If a choice is already made and there should be direct feedback the feedback is shown.
	 *
	 * @param aParent the ZK parent
	 * @param aRefItem the a ref item within the assessments case component
	 * @param aItem the a item within the items case component
	 *
	 * @return the c run vbox
	 */
	protected CRunVbox createItem(Component aParent,IXMLTag aRefItem,IXMLTag aItem) {
		radioChecked = null;
		removeItem(aParent);
		IXMLTag lParentTag = aRefItem.getParentTag();
		String lStartedTime = "" + lParentTag.getCurrentStatusAttributeTime(AppConstants.statusKeyStarted);
		String lFinishedTime = "" + lParentTag.getCurrentStatusAttributeTime(AppConstants.statusKeyFinished);
		boolean lReadOnly = true;
		if ((lStartedTime != null) && (!lStartedTime.equals(""))) {
			if ((lFinishedTime == null) || (lFinishedTime.equals("")))
				lReadOnly = false;
			else
				lReadOnly = (!(Double.parseDouble(lStartedTime) > Double.parseDouble(lFinishedTime)));
		}
		boolean lBAccessible = (!CDesktopComponents.sSpring().getCurrentRunTagStatus(caseComponent, aRefItem.getAttribute(AppConstants.keyId), AppConstants.statusKeyAccessible, getRunStatusType()).equals(AppConstants.statusValueFalse));
		if (!lBAccessible)
			lReadOnly = true;
		CRunArea lArea = new CRunArea();
		aParent.appendChild(lArea);
		lArea.setZclass(getClassName()+"_item");
		CRunVbox lVbox = new CRunVbox();
		lArea.appendChild(lVbox);
//		createRichtext(lVbox, aItem);
		CRunVbox lVbox3 = new CRunVbox();
		createRichtext(lVbox3, aItem);
//		append pieces
		createPieces(lVbox,aItem);
		createBreak(lVbox);
		lVbox3.setZclass(getClassName()+"_vbox1");
		lVbox.appendChild(lVbox3);
		
		if (getItemType(aItem).equals("mkea")) {
			createRadioGroup(lVbox, aRefItem, aItem, lReadOnly);
		}
		else if (getItemType(aItem).equals("mkma")) {
			createCheckboxGroup(lVbox, aRefItem, aItem, lReadOnly);
		}
		
		showFeedback = CDesktopComponents.sSpring().getCurrentRunTagStatus(caseComponent,aRefItem.getParentTag(),AppConstants.statusKeyShowfeedback, getRunStatusType()).equals(AppConstants.statusValueTrue);
		if (lReadOnly)
			showFeedback = showFeedback || CDesktopComponents.sSpring().getCurrentRunTagStatus(caseComponent, aRefItem.getParentTag(), AppConstants.statusKeyAlwaysinspectfeedback, getRunStatusType()).equals(AppConstants.statusValueTrue);
		createFeedback(lVbox);
		createFeedbackOrShowFeedbackButton(aRefItem, aItem);

		return lVbox;
	}

	/**
	 * Removes item from item area.
	 *
	 * @param aParent the a parent
	 */
	protected void removeItem(Component aParent) {
		if (aParent.getChildren() != null)
			aParent.getChildren().clear();
	}

	/**
	 * Shows richtext within item area.
	 *
	 * @param aParent the a parent
	 * @param aItem the a item containing the rich text within the items case component
	 */
	protected void createRichtext(Component aParent,IXMLTag aItem) {
		String lText = "";
		lText = CDesktopComponents.sSpring().unescapeXML(aItem.getChildValue("richtext"));
		Html lHtml = new CDefHtml(lText);
		aParent.appendChild(lHtml);
		lHtml.setZclass(getClassName()+"_item_html");
	}

	/**
	 * Shows break within item area.
	 *
	 * @param aParent the a parent
	 */
	protected void createBreak(Component aParent) {
		Html lHtml = new CDefHtml("<br/>");
		lHtml.setZclass(getClassName()+"_item_html");
		aParent.appendChild(lHtml);
	}

	/**
	 * Shows radio group within item area.
	 *
	 * @param aParent the a parent
	 * @param aRefItem the refitem within the assessments case component
	 * @param aItem the item within the items case component
	 * @param aReadOnly the a read only state
	 */
	protected void createRadioGroup(Component aParent, IXMLTag aRefItem, IXMLTag aItem, boolean aReadOnly) {
		List<IXMLTag> lAltTags = aItem.getChilds("alternative");
		for (IXMLTag lAltTag : lAltTags) {
			CRunHbox lHbox = new CRunHbox();
			aParent.appendChild(lHbox);
			// create one radio group per radio button, so rich text can be placed to the right
			String lAltId = CDesktopComponents.sSpring().getCurrentTagStatus(aRefItem, AppConstants.statusKeyAnswer);
			CRunAssessmentItemRadiogroup lRadiogroup = new CRunAssessmentItemRadiogroup();
			lHbox.appendChild(lRadiogroup);
			createRadio(lRadiogroup,lAltTag, aReadOnly, lAltId);
			CRunVbox lVbox = new CRunVbox();
			lHbox.appendChild(lVbox);
			createRichtext(lVbox, lAltTag);
//			append pieces
			createPieces(lVbox,lAltTag);
			lVbox.setZclass(getClassName()+"_vbox2");
		}
	}

	/**
	 * Shows radio button within item area. Checks radio button if corresponds to
	 * an alternative id.
	 *
	 * @param aParent the a parent
	 * @param aItem the alternative within the items case component
	 * @param aReadOnly the a read only state
	 * @param aAltId the a alt id
	 */
	protected void createRadio(Component aParent, IXMLTag aItem, boolean aReadOnly, String aAltId) {
		CRunAssessmentItemRadio lRadio = new CRunAssessmentItemRadio();
		aParent.appendChild(lRadio);
		String lAltTagId = aItem.getAttribute(AppConstants.keyId);
		lRadio.setAttribute("alt_id",lAltTagId);
		if (lAltTagId.equals(aAltId)) {
			lRadio.setChecked(true);
			radioChecked = lRadio;
		}
		lRadio.setDisabled(aReadOnly);
		lRadio.setZclass(getClassName()+"_item_radio");
	}

	/**
	 * Shows checkbox group within item area.
	 *
	 * @param aParent the a parent
	 * @param aRefItem the refitem within the assessments case component
	 * @param aItem the item within the items case component
	 * @param aReadOnly the a read only state
	 */
	protected void createCheckboxGroup(Component aParent, IXMLTag aRefItem, IXMLTag aItem, boolean aReadOnly) {
		List<IXMLTag> lAltTags = aItem.getChilds("alternative");
		for (IXMLTag lAltTag : lAltTags) {
			CRunHbox lHbox = new CRunHbox();
			aParent.appendChild(lHbox);
			// create one checkbox group per checkbox, so rich text can be placed to the right
			String lAltIds = CDesktopComponents.sSpring().getCurrentTagStatus(aRefItem, AppConstants.statusKeyAnswer);
			CRunHbox lCheckboxgroup = new CRunHbox();
			lHbox.appendChild(lCheckboxgroup);
			createCheckbox(lCheckboxgroup,lAltTag, aReadOnly, lAltIds);
			CRunVbox lVbox = new CRunVbox();
			lHbox.appendChild(lVbox);
			createRichtext(lVbox, lAltTag);
//			append pieces
			createPieces(lVbox,lAltTag);
			lVbox.setZclass(getClassName()+"_vbox2");
		}
	}

	/**
	 * Shows checkbox within item area. Checks checkbox if corresponds to
	 * alternative ids. aAltIds is a comma separated list of alternative ids.
	 *
	 * @param aParent the a parent
	 * @param aItem the alternative within the items case component
	 * @param aReadOnly the a read only state
	 * @param aAltIds the a alt id
	 */
	protected void createCheckbox(Component aParent, IXMLTag aItem, boolean aReadOnly, String aAltIds) {
		CRunAssessmentItemCheckbox lCheckbox = new CRunAssessmentItemCheckbox();
		aParent.appendChild(lCheckbox);
		String lAltTagId = aItem.getAttribute(AppConstants.keyId);
		lCheckbox.setAttribute("alt_id",lAltTagId);
		if ((AppConstants.statusValueSeparator + aAltIds + AppConstants.statusValueSeparator).indexOf(AppConstants.statusValueSeparator + lAltTagId + AppConstants.statusValueSeparator) >= 0) {
			lCheckbox.setChecked(true);
		}
		lCheckbox.setDisabled(aReadOnly);
		lCheckbox.setZclass(getClassName()+"_item_checkbox");
	}

	/**
	 * Shows pieces within item area. Pieces can be entered within the items
	 * case component, but can also be references to pieces entered within a
	 * references case component.
	 *
	 * @param aParent the a parent
	 * @param aItem the item containing the pieces within the items case component
	 */
	protected void createPieces(Component aParent,IXMLTag aItem) {
		List<IXMLTag> lPieces = aItem.getChilds("piece");
		for (IXMLTag lPiece : lPieces) {
			Label lLabel = new CRunBlobLabel(lPiece);
			aParent.appendChild(lLabel);
		}
		List<IXMLTag> lRefPieces = aItem.getChilds("refpiece");
		CCaseHelper cCaseHelper = new CCaseHelper();
		for (IXMLTag lRefPiece : lRefPieces) {
			String lReftype = lRefPiece.getChild("ref").getDefTag().getAttribute(AppConstants.defKeyReftype);
			List<IXMLTag> lRefTags = new ArrayList<IXMLTag>();
			lRefTags = cCaseHelper.getRefTags(lReftype,""+AppConstants.statusKeySelectedIndex,CDesktopComponents.sSpring().getSCaseRoleHelper().getCaseRole(Integer.parseInt(itemscarid)),CDesktopComponents.sSpring().getCaseComponent(Integer.parseInt(itemscacid)),lRefPiece);
//			only one ref
			if (lRefTags.size() == 1) {
				Label lLabel = new CRunBlobLabel((IXMLTag)lRefTags.get(0),CDesktopComponents.sSpring().unescapeXML(lRefPiece.getChildValue("name")));
				aParent.appendChild(lLabel);
			}
		}
	}

	/**
	 * Shows feedback or feedback button.
	 *
	 * @param aRefItem the refitem within the assessments case component
	 * @param aItem the item within the items case component
	 */
	protected void createFeedbackOrShowFeedbackButton(IXMLTag aRefItem, IXMLTag aItem) {
		if (showFeedback) {
			CRunHbox lHbox = (CRunHbox)CDesktopComponents.vView().getComponent(getId()+"FeedbackBox");
			if (lHbox != null) {
				IXMLTag lFeedbackCondition = getFeedbackCondition(refitem, item);
				if (lFeedbackCondition != null) {
					if (itemHasDirectFeedback(aItem)) {
						showFeedback(lHbox, refitem, item);
					}
					else {
						showFeedbackButton(lHbox, item);
					}
				}
				else {
					hideFeedback(lHbox);
				}
			}
		}
	}

	/**
	 * Creates feedback area within item area.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run hbox
	 */
	protected CRunHbox createFeedback(Component aParent) {
		CRunHbox lHbox = new CRunHbox();
		aParent.appendChild(lHbox);
		lHbox.setId(getId()+"FeedbackBox");
		return lHbox;
	}

	/**
	 * Creates feedback button within item area.
	 *
	 * @param aParent the a parent
	 * @param aItem the a item
	 *
	 * @return the component
	 */
	protected Component createShowFeedbackButton (Component aParent, IXMLTag aItem) {
		CRunButton lButton = new CRunButton("showFeedbackBtn","showFeedback",aItem,CDesktopComponents.vView().getLabel("run_assessments.button.show_feedback"),"_assessments_100","");
		lButton.registerObserver("runAssessments");
		aParent.appendChild(lButton);
		return lButton;
	}

	/**
	 * Gets the feedback condition tag applicable for the given aAltId.
	 *
	 * @param aRefItem the a ref item within the assessments case component
	 * @param aItem the a item within the items case component
	 *
	 * @return the feedback condition tag
	 */
	protected IXMLTag getFeedbackCondition(IXMLTag aRefItem, IXMLTag aItem) {
		List<IXMLTag> lPresentFCTags = getPresentFeedbackconditionTags(CDesktopComponents.sSpring().getRunGroupAccount(), aItem);
		//NOTE!!! lGeneratedAltTags and lGeneratedFCTags are lists of alternatives and feedback conditions tags generated out of refitem status
		//An item is part of an Items component and can be used by multiple assessments, therefore answer and triggered feedback condition id are stored in refitem!
		//To be able to check the condition, that is defined on item alternatives and/or feedback conditions,
		//the history of answers and feedback condition ids is converted to alternative and feedback condition tags with the right status attribute set, namely opened.
		//These tags are then used to check the condition.
		Hashtable<String, IXMLTag> lGeneratedAltTags = getItemChildTagsFromRefItem(aRefItem, aItem, "alternative");
		Hashtable<String, IXMLTag> lGeneratedFCTags = getFeedbackConditionTagsFromRefItem(aRefItem, aItem, "feedbackcondition");
		for (IXMLTag lFCTag : lPresentFCTags) {
			if (CDesktopComponents.sSpring().getSScriptHelper().evaluateAssessmentConditionTag(lFCTag, lGeneratedAltTags, lGeneratedFCTags, AppConstants.statusTypeRunGroup))
				return lFCTag;
		}
		return null;
	}

	/**
	 * Gets a hashtable with per original item child tag id, a generated child tag with a history of status opened set.
	 * Child tags are generated using the history of the answer stored in the refitem status.
	 * For mkea (multiple choice) questions the answer is equal to the chose alternative tag id, e.g. '8'.
	 * For mkma (multiple answer) questions, the answer may be equal to a combination of alternative tag ids, e.g., '2_and_4_and_7'
	 * 
	 * @param aRefItem the a ref item within the assessments case component
	 * @param aItem the a item within the items case component
	 * @param aChildName the a child name
	 *
	 * @return hashtable
	 */
	protected Hashtable<String, IXMLTag> getItemChildTagsFromRefItem(IXMLTag aRefItem, IXMLTag aItem, String aChildName) {
		Hashtable<String, IXMLTag> hTags = new Hashtable<String, IXMLTag>(); 

		//get item child tags
		List<IXMLTag> lItemChildTags = aItem.getChilds(aChildName);
		if (lItemChildTags.size() == 0) {
			return hTags;
		}

		//get refitem answers
		List<IXMLAttributeValueTime> lValueTimes= aRefItem.getStatusAttribute(AppConstants.statusKeyAnswer); 
		if (lValueTimes == null || lValueTimes.size() == 0) {
			return hTags;
		}
		Hashtable<String, List<IXMLAttributeValueTime>> hTagValueTimes = new Hashtable<String, List<IXMLAttributeValueTime>>();
		//init hashtable
		for (IXMLTag lItemChildTag : lItemChildTags) {
			hTagValueTimes.put(lItemChildTag.getAttribute(AppConstants.keyId), new ArrayList<IXMLAttributeValueTime>());
		}
		String[] lIds = null;
		String[] lPreviousIds = new String[0];
		for (IXMLAttributeValueTime lValueTime : lValueTimes) {
			//NOTE value can contain multiple alternative ids in case of multi answer question, so split
			//NOTE value for instance is 'a_and_b', where '_and_' is AppConstants.statusValueSeparator 
			lIds = lValueTime.getValue().split(AppConstants.statusValueSeparator);
			//NOTE check if ids are in previous ids. If not the user has checked an alternative so add value true. 
			for (int i=0;i<lIds.length;i++) {
				//id must be of existing alternative
				if (hTagValueTimes.containsKey(lIds[i])) {
					boolean lFound = false;
					for (int j=0;j<lPreviousIds.length;j++) {
						if (lPreviousIds[j].equals(lIds[i])) {
							lFound = true;
							break;
						}
					}
					if (!lFound) {
						hTagValueTimes.get(lIds[i]).add(new XMLAttributeValueTime(AppConstants.statusValueTrue, lValueTime.getTime()));
					}
				}
			}
			//NOTE check if previous ids are in ids. If not the user has unchecked an alternative so add value false.
			for (int i=0;i<lPreviousIds.length;i++) {
				//id must be of existing alternative
				if (hTagValueTimes.containsKey(lPreviousIds[i])) {
					boolean lFound = false;
					for (int j=0;j<lIds.length;j++) {
						if (lIds[j].equals(lPreviousIds[i])) {
							lFound = true;
							break;
						}
					}
					if (!lFound) {
						hTagValueTimes.get(lPreviousIds[i]).add(new XMLAttributeValueTime(AppConstants.statusValueFalse, lValueTime.getTime()));
					}
				}
			}
			lPreviousIds = lIds;
		}
		
		for (Enumeration<String> lKeys = hTagValueTimes.keys(); lKeys.hasMoreElements();) {
			String lKey = lKeys.nextElement();
			if (hTagValueTimes.get(lKey).size() == 0) {
				// opened is default false within xml definition
				hTagValueTimes.get(lKey).add(new XMLAttributeValueTime(AppConstants.statusValueFalse, -1));
			}
			//NOTE create data tag with aChildName
			IXMLTag lDataTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag(aChildName, "");
			
			IXMLTag lStatusTag = CDesktopComponents.sSpring().newRunNodeTag(aChildName, "", null, null);
			if (lStatusTag != null) {
				lDataTag.addStatusTag(lStatusTag);
				IXMLTag lStatusStatusTag = lStatusTag.getChild(AppConstants.statusElement);
				if (lStatusStatusTag != null) {
					lStatusStatusTag.setAttributeAsList(AppConstants.statusKeyOpened, hTagValueTimes.get(lKey));
				}
			}
			hTags.put(lKey, lDataTag);
		}
		
		return hTags;
	}

	/**
	 * Gets a hashtable with per original item feedback condition child tag id, a generated feedback condition tag with a history of status opened set.
	 * Feedback condition tags are generated using the history of the fired feedback condition id stored in the refitem status.
	 *
	 * @param aRefItem the a ref item within the assessments case component
	 * @param aItem the a item within the items case component
	 * @param aChildName the a child name
	 *
	 * @return hashtable
	 */
	protected Hashtable<String, IXMLTag> getFeedbackConditionTagsFromRefItem(IXMLTag aRefItem, IXMLTag aItem, String aChildName) {
		Hashtable<String, IXMLTag> hTags = new Hashtable<String, IXMLTag>(); 

		//get item feedback conditions
		List<IXMLTag> lItemChildTags = aItem.getChilds(aChildName);
		if (lItemChildTags.size() == 0) {
			return hTags;
		}

		//get refitem feedback condition ids
		List<IXMLAttributeValueTime> lValueTimes= aRefItem.getStatusAttribute(AppConstants.statusKeyFeedbackConditionId); 
		if (lValueTimes == null || lValueTimes.size() == 0) {
			return hTags;
		}
		Hashtable<String, List<IXMLAttributeValueTime>> hTagValueTimes = new Hashtable<String, List<IXMLAttributeValueTime>>();
		//init hashtable
		for (IXMLTag lItemChildTag : lItemChildTags) {
			hTagValueTimes.put(lItemChildTag.getAttribute(AppConstants.keyId), new ArrayList<IXMLAttributeValueTime>());
		}
		String lPreviousId = "";
		String lId = "";
		for (IXMLAttributeValueTime lValueTime : lValueTimes) {
			lId = lValueTime.getValue();
			if (hTagValueTimes.containsKey(lId)) {
				hTagValueTimes.get(lId).add(new XMLAttributeValueTime(AppConstants.statusValueTrue, lValueTime.getTime()));
			}
			if (!lPreviousId.equals("") && hTagValueTimes.containsKey(lPreviousId)) {
				hTagValueTimes.get(lPreviousId).add(new XMLAttributeValueTime(AppConstants.statusValueFalse, lValueTime.getTime()));
			}
			lPreviousId = lId;
		}
		
		for (Enumeration<String> lKeys = hTagValueTimes.keys(); lKeys.hasMoreElements();) {
			String lKey = lKeys.nextElement();
			if (hTagValueTimes.get(lKey).size() == 0) {
				// opened is default false within xml definition
				hTagValueTimes.get(lKey).add(new XMLAttributeValueTime(AppConstants.statusValueFalse, -1));
			}
			//NOTE create data tag with aChildName
			IXMLTag lDataTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag(aChildName, "");
			
			IXMLTag lStatusTag = CDesktopComponents.sSpring().newRunNodeTag(aChildName, "", null, null);
			if (lStatusTag != null) {
				lDataTag.addStatusTag(lStatusTag);
				IXMLTag lStatusStatusTag = lStatusTag.getChild(AppConstants.statusElement);
				if (lStatusStatusTag != null) {
					lStatusStatusTag.setAttributeAsList(AppConstants.statusKeyOpened, hTagValueTimes.get(lKey));
				}
			}
			hTags.put(lKey, lDataTag);
		}
		
		return hTags;
	}

	/**
	 * Shows feedback possibly having references.
	 *
	 * @param aParent the a parent
	 * @param aRefItem the a ref item
	 * @param aItem the a item within the items case component
	 *
	 * @return the c run v box
	 */
	protected CRunVbox showFeedback(Component aParent,IXMLTag aRefItem,IXMLTag aItem) {
		aParent.getChildren().clear();
		String lFeedback = "";
		IXMLTag lFeedbackTag = getFeedbackCondition(aRefItem, aItem);
		if (lFeedbackTag != null) {
			lFeedback = CDesktopComponents.sSpring().unescapeXML(lFeedbackTag.getChildValue("richtext"));
		}
		CRunVbox lVbox = new CRunVbox();
		aParent.appendChild(lVbox);
		Html lHtml = new CDefHtml("<br/>");
		lVbox.appendChild(lHtml);
		lHtml = new CDefHtml(lFeedback);
		lVbox.appendChild(lHtml);
		lHtml.setZclass(getClassName()+"_item_html");
		if (lFeedbackTag != null)
//			append pieces
			createPieces(lVbox,lFeedbackTag);
		return lVbox;
	}

	/**
	 * Shows feedback button within item area.
	 *
	 * @param aParent the a parent
	 * @param aItem the a item
	 *
	 * @return the component
	 */
	protected Component showFeedbackButton (Component aParent, IXMLTag aItem) {
		Component lButton = (Component)CDesktopComponents.vView().getComponent("showFeedbackBtn");
		if (lButton == null) {
			aParent.getChildren().clear();
			lButton = createShowFeedbackButton(aParent, aItem);
		}
		return lButton;
	}

	/**
	 * Hides feedback.
	 *
	 * @param aParent the a parent
	 */
	protected void hideFeedback(Component aParent) {
		aParent.getChildren().clear();
	}

	/**
	 * Does item action. Is called when radio button aRadio corresponding to
	 * aAltId is clicked.
	 * The alternative id is saved as answer. If there is a corresponding feedback condition its id,
	 * its score and its correct status are saved too, and the feedback is shown.
	 * Within the assessments tree the item is rendered again to show its changed
	 * status.
	 *
	 * @param aRadio the a radio
	 * @param aAltId the a alt id
	 * @param aChecked the a checked
	 */
	public void doItemAction(CRunAssessmentItemRadio aRadio, String aAltId, boolean aChecked) {
		if (radioChecked != null)
			radioChecked.setChecked(false);
		radioChecked = aRadio;
		if (refitem == null || item == null)
			return;
		// overwrite previous answer.
		if (!aChecked) {
			// no alternative chosen
			aAltId = "";
		}
		IECaseComponent lItemsCaseComponent = CDesktopComponents.sSpring().getCaseComponent(Integer.parseInt(itemscacid));
		String lPreviousAltId = CDesktopComponents.sSpring().getCurrentTagStatus(refitem, AppConstants.statusKeyAnswer);
		setRunTagStatus(caseComponent, refitem, AppConstants.statusKeyAnswer, aAltId, true);
		List<IXMLTag> lAltTags = item.getChilds("alternative");
		for (IXMLTag lAltTag : lAltTags) {
			if (lAltTag.getAttribute(AppConstants.keyId).equals(aAltId))
				// set opened of alt to true
				setRunTagStatus(lItemsCaseComponent,lAltTag,AppConstants.statusKeyOpened,AppConstants.statusValueTrue, true);
		}
		for (IXMLTag lAltTag : lAltTags) {
			if (lAltTag.getAttribute(AppConstants.keyId).equals(lPreviousAltId))
				// set opened of previous alt to false
				//NOTE alternative does not have singleopen within definition because multiple alternatives can be selected for
				//multiple answer questions. So singleopen behaviour is implemented here.
				setRunTagStatus(lItemsCaseComponent,lAltTag,AppConstants.statusKeyOpened,AppConstants.statusValueFalse, true);
		}
		IXMLTag lFeedbackConditionTag = getFeedbackCondition(refitem, item);
		String lPreviousFeedbackConditionId = CDesktopComponents.sSpring().getCurrentTagStatus(refitem, AppConstants.statusKeyFeedbackConditionId);
		String lFeedbackConditionId = "";
		if (lFeedbackConditionTag != null) {
			lFeedbackConditionId = lFeedbackConditionTag.getAttribute(AppConstants.keyId);
		}
		List<IXMLTag> lFeedbackConditionTags = item.getChilds("feedbackcondition");
		for (IXMLTag lTag : lFeedbackConditionTags) {
			if (lTag.getAttribute(AppConstants.keyId).equals(lFeedbackConditionId))
				// set opened of feedback condition to true
				setRunTagStatus(lItemsCaseComponent,lTag,AppConstants.statusKeyOpened,AppConstants.statusValueTrue, true);
		}
		for (IXMLTag lTag : lFeedbackConditionTags) {
			if (lTag.getAttribute(AppConstants.keyId).equals(lPreviousFeedbackConditionId))
				// set opened of previous feedback condition to false
				//NOTE feedback condition does not have singleopen within definition because in principle multiple feedback conditions
				//could become true, and their feedback texts could be aggregated. But in Java code, we presume only condition can
				//become true, so we set opened to false.
				setRunTagStatus(lItemsCaseComponent,lTag,AppConstants.statusKeyOpened,AppConstants.statusValueFalse, true);
		}
		setRunTagStatus(caseComponent, refitem, AppConstants.statusKeyFeedbackConditionId, lFeedbackConditionId, true);
		if (lFeedbackConditionTag != null) {
			setRunTagStatus(caseComponent, refitem, AppConstants.statusKeyScore, lFeedbackConditionTag.getChildValue("score"), true);
			setRunTagStatus(caseComponent, refitem, AppConstants.statusKeyCorrect, lFeedbackConditionTag.getChildValue("correct"), true);
		}
		else {
			setRunTagStatus(caseComponent, refitem, AppConstants.statusKeyScore, "", true);
			setRunTagStatus(caseComponent, refitem, AppConstants.statusKeyCorrect, "", true);
		}
		createFeedbackOrShowFeedbackButton(refitem, item);
		CRunAssessmentsTree lTree = (CRunAssessmentsTree)CDesktopComponents.vView().getComponent("runAssessmentsTree");
		if (lTree != null) {
			Treeitem lTreeitem = lTree.getContentItem(refitem.getAttribute(AppConstants.keyId));
			if (lTreeitem != null)
				lTree.reRenderTreeitem(lTreeitem,refitem,false);
		}
	}

	/**
	 * Does item action. Is called when checkbox aCheckbox corresponding to
	 * aAltId is clicked.
	 * The chosen alternative ids are saved as answer. If there is a corresponding feedback condition its id,
	 * its score and its correct status are saved too, and the feedback is shown.
	 * Within the assessments tree the item is rendered again to show its changed
	 * status.
	 *
	 * @param aCheckbox the a checkbox
	 * @param aAltId the a alt id
	 * @param aChecked the a checked
	 */
	public void doItemAction(CRunAssessmentItemCheckbox aCheckbox, String aAltId, boolean aChecked) {
		if (refitem == null || item == null)
			return;
		String lAltIds = CDesktopComponents.sSpring().getCurrentTagStatus(refitem, AppConstants.statusKeyAnswer);
		if (aChecked) {
			lAltIds = addStringItemIfPossible(lAltIds, aAltId);
		}
		else {
			lAltIds = removeStringItemIfPossible(lAltIds, aAltId);
		}
		IECaseComponent lItemsCaseComponent = CDesktopComponents.sSpring().getCaseComponent(Integer.parseInt(itemscacid));
		setRunTagStatus(caseComponent, refitem, AppConstants.statusKeyAnswer, lAltIds, true);
		List<IXMLTag> lAltTags = item.getChilds("alternative");
		for (IXMLTag lAltTag : lAltTags) {
			if (lAltTag.getAttribute(AppConstants.keyId).equals(aAltId)) {
				setRunTagStatus(lItemsCaseComponent,lAltTag,AppConstants.statusKeyOpened,""+aChecked, true);
			}
//			maybe add something for shared component
		}
		IXMLTag lFeedbackConditionTag = getFeedbackCondition(refitem, item);
		if (lFeedbackConditionTag != null) {
			String lPreviousFeedbackConditionId = CDesktopComponents.sSpring().getCurrentTagStatus(refitem, AppConstants.statusKeyFeedbackConditionId);
			List<IXMLTag> lFeedbackConditionTags = item.getChilds("feedbackcondition");
			for (IXMLTag lTag : lFeedbackConditionTags) {
				if (lTag.getAttribute(AppConstants.keyId).equals(lFeedbackConditionTag.getAttribute(AppConstants.keyId)))
					setRunTagStatus(lItemsCaseComponent,lTag,AppConstants.statusKeyOpened,AppConstants.statusValueTrue, true);
				else if (lTag.getAttribute(AppConstants.keyId).equals(lPreviousFeedbackConditionId))
					setRunTagStatus(lItemsCaseComponent,lTag,AppConstants.statusKeyOpened,AppConstants.statusValueFalse, true);
//				maybe add something for shared component
			}
			setRunTagStatus(caseComponent, refitem, AppConstants.statusKeyFeedbackConditionId, lFeedbackConditionTag.getAttribute(AppConstants.keyId), true);
			setRunTagStatus(caseComponent, refitem, AppConstants.statusKeyScore, lFeedbackConditionTag.getChildValue("score"), true);
			setRunTagStatus(caseComponent, refitem, AppConstants.statusKeyCorrect, lFeedbackConditionTag.getChildValue("correct"), true);
		}
//		maybe add something for shared component
		createFeedbackOrShowFeedbackButton(refitem, item);
		CRunAssessmentsTree lTree = (CRunAssessmentsTree)CDesktopComponents.vView().getComponent("runAssessmentsTree");
		if (lTree != null) {
			Treeitem lTreeitem = lTree.getContentItem(refitem.getAttribute(AppConstants.keyId));
			if (lTreeitem != null)
				lTree.reRenderTreeitem(lTreeitem,refitem,false);
		}
	}

	/**
	 * Adds a string item to a comma separated list, if it does not exist yet on the list.
	 *
	 * @param aStringItems the a string items
	 * @param aStringItem the a string item
	 *
	 * @return the adjusted list
	 */
	protected String addStringItemIfPossible(String aStringItems, String aStringItem) {
		int lIndex = (AppConstants.statusValueSeparator + aStringItems + AppConstants.statusValueSeparator).indexOf(AppConstants.statusValueSeparator + aStringItem + AppConstants.statusValueSeparator);
		// add string item if not already added
		if (lIndex < 0) {
			if (!aStringItems.equals("")) {
				aStringItems += AppConstants.statusValueSeparator;
			}
			aStringItems += aStringItem;
		}
		return aStringItems;
	}

	/**
	 * Removes a string item from a comma separated list, if it does exist on the list.
	 *
	 * @param aStringItems the a string items
	 * @param aStringItem the a string item
	 *
	 * @return the adjusted list
	 */
	protected String removeStringItemIfPossible(String aStringItems, String aStringItem) {
		int lIndex = (AppConstants.statusValueSeparator + aStringItems + AppConstants.statusValueSeparator).indexOf(AppConstants.statusValueSeparator + aStringItem + AppConstants.statusValueSeparator);
		// remove alt id if not already removed
		if (lIndex >= 0) {
			String[] lAltIdsArr = aStringItems.split(AppConstants.statusValueSeparator);
			aStringItems = "";
			for (int i=0;i<lAltIdsArr.length;i++) {
				if (!lAltIdsArr[i].equals(aStringItem)) {
					if (!aStringItems.equals("")) {
						aStringItems += AppConstants.statusValueSeparator;
					}
					aStringItems += lAltIdsArr[i];
				}
			}
		}
		return aStringItems;
	}

	/**
	 * Is called by start or end assessment button and will start or finish the
	 * current assessment.
	 *
	 * @param aObserved the observed object, it should have interface CIObserved implemented
	 * @param aAction the action
	 * @param aStatus the status object, can be String or other class
	 */
	@Override
	public void observedNotify(CIObserved aObserved, String aAction, Object aStatus) {
		if (aAction.equals("startAssessment")) {
			startAssessment((IXMLTag)aStatus);
		}
		else if (aAction.equals("endAssessment")) {
			endAssessment((IXMLTag)aStatus);
		}
		else if (aAction.equals("showFeedback")) {
			CRunHbox lHbox = (CRunHbox)CDesktopComponents.vView().getComponent(getId()+"FeedbackBox");
			if (lHbox != null) {
				showFeedback(lHbox,refitem,item);
			}
		}

	}

	/**
	 * Starts and renders assessment.
	 * If not first start confirmation is asked.
	 * Sets started of aAssessment to true and resets items involved within assessment.
	 *
	 * @param aAssessment the a assessment
	 */
	public void startAssessment(IXMLTag aAssessment) {
		CRunAssessmentsTree lTree = (CRunAssessmentsTree)getRunContentComponent();
		if (lTree == null)
			return;
		Treeitem lTreeitem = lTree.getContentItem(aAssessment.getAttribute(AppConstants.keyId));
		if (lTreeitem == null)
			return;
//		ask if sure if not first time
		boolean lStarted = (CDesktopComponents.sSpring().getCurrentTagStatus(aAssessment,AppConstants.statusKeyStarted).equals(AppConstants.statusValueTrue));
		if (lStarted) {
			Map<String,Object> lParams = new HashMap<String,Object>();
			String lValue = (String) lTreeitem.getAttribute("keyvalues");
			lParams.put("title", CDesktopComponents.vView().getLabel("run_assessment_start.title") + " '" + lValue + "'");
			lParams.put("text", CDesktopComponents.vView().getLabel("run_assessment_start.a") + " '" + lValue + "'" + CDesktopComponents.vView().getLabel("run_assessment_start.b"));
			Window lWindow = CDesktopComponents.vView().modalPopup(VView.v_run_assessment_start, null, lParams, "center");
			if (lWindow.getAttribute("ok") == null)
				return;
		}
//		set status of assessment to started
		setRunTagStatus(caseComponent,aAssessment,AppConstants.statusKeyStarted,AppConstants.statusValueTrue, true);
		if (!CDesktopComponents.sSpring().getCurrentRunTagStatus(caseComponent,aAssessment,AppConstants.statusKeyOutfolded, getRunStatusType()).equals(AppConstants.statusValueTrue))
			setRunTagStatus(caseComponent,aAssessment,AppConstants.statusKeyOutfolded,AppConstants.statusValueTrue, true);
//		maybe add something for shared component
		List<IXMLTag> lRefItems = aAssessment.getChilds("refitem");
//		if already started earlier clear answers of used items
		if (lStarted) {
			for (IXMLTag lTag : lRefItems) {
				boolean lPresent = !CDesktopComponents.sSpring().getCurrentTagStatus(lTag,AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse);
				// NOTE If assessment with randomly selected items, deleted for not selected items was set to true
				boolean lDeleted = CDesktopComponents.sSpring().getCurrentTagStatus(lTag,AppConstants.statusKeyDeleted).equals(AppConstants.statusValueTrue);
				if (lPresent && !lDeleted) {
					setRunTagStatus(caseComponent,lTag,AppConstants.statusKeyAnswer,"",true);
					setRunTagStatus(caseComponent,lTag,AppConstants.statusKeyFeedbackConditionId,"",true);
					setRunTagStatus(caseComponent,lTag,AppConstants.statusKeyScore,"",true);
					setRunTagStatus(caseComponent,lTag,AppConstants.statusKeyOpened,AppConstants.statusValueFalse,true);
//					maybe add something for shared component
				}
			}
		}
		renderAssessment(aAssessment);
	}

	/**
	 * Sets item tag status. Item is refitem within assessment.
	 *
	 * @param aAssessmentId the a assessment id
	 * @param aItemId the a item id
	 * @param aStatusKey the a status key
	 * @param aStatusValue the a status value
	 */
	public void setItemTagStatus(String aAssessmentId, String aItemId, String aStatusKey, String aStatusValue) {
		CRunAssessmentsTree lTree = (CRunAssessmentsTree)getRunContentComponent();
		if (lTree == null)
			return;
		Treeitem lTreeitem = lTree.getContentItem(aAssessmentId);
		if (lTreeitem == null)
			return;
		IXMLTag lAssessmentTag = lTree.getContentItemTag(lTreeitem);
		List<IXMLTag> lItems = lAssessmentTag.getChilds("refitem");
		for (IXMLTag lTag : lItems) {
			if (lTag.getAttribute(AppConstants.keyId).equals(aItemId)) {
				CDesktopComponents.sSpring().setRunTagStatusValue(CDesktopComponents.sSpring().getStatusTagOfDataTag(lTag), aStatusKey, aStatusValue);
			}
		}
	}


	/**
	 * (Re)renders assessment.
	 *
	 * @param aAssessmentId the a assessment id
	 */
	public void renderAssessment(String aAssessmentId) {
		CRunAssessmentsTree lTree = (CRunAssessmentsTree)getRunContentComponent();
		if (lTree == null)
			return;
		Treeitem lTreeitem = lTree.getContentItem(aAssessmentId);
		if (lTreeitem == null)
			return;
		IXMLTag lTag = lTree.getContentItemTag(lTreeitem);
		if (lTag == null)
			return;
		renderAssessment(lTag);
	}

	/**
	 * (Re)renders assessment, a subtree with items within the assessmentstree. If a subset of the items should be shown, it
	 * is determined randomly. Removes possible item from item area.
	 *
	 * @param aAssessment the a assessment
	 */
	public void renderAssessment(IXMLTag aAssessment) {
		if (renderAssLock)
			return;
		renderAssLock = true;
		CRunAssessmentsTree lTree = (CRunAssessmentsTree)getRunContentComponent();
		if (lTree == null) {
			renderAssLock = false;
			return;
		}
		Treeitem lTreeitem = lTree.getContentItem(aAssessment.getAttribute(AppConstants.keyId));
		if (lTreeitem == null) {
			renderAssLock = false;
			return;
		}
		List<IXMLTag> lItems = aAssessment.getChilds("refitem");
//		check if wanted number of items is smaller then total number of items
		int lTotalNumber = 0;
		for (IXMLTag lTag : lItems) {
			boolean lPresent = true;
			lPresent = !CDesktopComponents.sSpring().getCurrentTagStatus(lTag,AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse);
			if (lPresent) {
				lTotalNumber ++;
			}
		}
		int lWantedNumber = lTotalNumber;
		String lNumberOfItemsStr = aAssessment.getChildValue("numberofitems");
		if (!lNumberOfItemsStr.equals(""))
			lWantedNumber = Integer.parseInt(lNumberOfItemsStr);
		if (lWantedNumber > lTotalNumber)
			lWantedNumber = lTotalNumber;
//		if so randomize items and hide items not used
		List<IXMLTag> lPresentItems = new ArrayList<IXMLTag>();
		if (lWantedNumber < lTotalNumber) {
//			get random wanted numbers out of totalnumber
			List<String> lNumbersToUse = new ArrayList<String>();
			while (lNumbersToUse.size()<lWantedNumber) {
				int rand = Integer.parseInt(""+Math.round(Math.random() * lTotalNumber));
				if (rand == lTotalNumber)
					rand = 0;
				rand = rand + 1;
				if (!lNumbersToUse.contains(""+rand))
					lNumbersToUse.add(""+rand);
			}
			int lCounter = 0;
			for (IXMLTag lTag : lItems) {
				lCounter = lCounter + 1;
				String lDeleted = "";
				if (lNumbersToUse.contains(""+lCounter)) {
					lDeleted = AppConstants.statusValueFalse;
					lPresentItems.add(lTag);
				}
				else
					lDeleted = AppConstants.statusValueTrue;
				setRunTagStatus(caseComponent,lTag,AppConstants.statusKeyDeleted,lDeleted,true);
//				maybe add something for shared component
			}
		}
		else {
			for (IXMLTag lTag : lItems) {
				boolean lPresent = true;
				boolean lDeleted = false;
				lPresent = !CDesktopComponents.sSpring().getCurrentTagStatus(lTag,AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse);
				lDeleted = CDesktopComponents.sSpring().getCurrentTagStatus(lTag,AppConstants.statusKeyDeleted).equals(AppConstants.statusValueTrue);
				if (lPresent) {
					lPresentItems.add(lTag);
				}
				if (lDeleted) {
					setRunTagStatus(caseComponent,lTag,AppConstants.statusKeyDeleted,AppConstants.statusValueFalse,true);
				}
//				maybe add something for shared component
			}
		}
//		set question numbers
		int lQCounter = 0;
		for (IXMLTag lTag : lPresentItems) {
			lQCounter = lQCounter + 1;
			setRunTagStatus(caseComponent,lTag,"question_number",""+lQCounter,true);
		}

//		render assessment items
		lTree.reRenderTreeitem(lTreeitem,aAssessment,true);
//		remove item
		CRunArea lItemArea = (CRunArea)CDesktopComponents.vView().getComponent(getId()+"ItemArea");
		if (lItemArea != null)
			removeItem(lItemArea);
		renderAssLock = false;
	}

	/**
	 * Extracts the items for the current assessment.
	 * 
	 * @param aTree the a assessmentstree
	 * @param aAssessment the a assessment
	 */
	private List<IXMLTag> getAssessmentItemsList(CRunAssessmentsTree aTree, IXMLTag aAssessment) {
		List<IXMLTag> lItems = null;
		IXMLTag lAssTag = (IXMLTag)aTree.getAttribute("item");
		List<IXMLTag> lItems2 = lAssTag.getChildTags();
		for (IXMLTag lTag : lItems2) {
			if (lTag.getAttribute(AppConstants.keyId).equals(aAssessment.getAttribute(AppConstants.keyId))) {
				lItems = lTag.getChilds("refitem");
			}
		}
		return lItems;
	}

	/**
	 * Ends assessment after confirmation.
	 * Determines assessment score and saves it within assessment.
	 * Finished of assessment is set to true.
	 * Removes possible item from item area.
	 *
	 * @param aAssessment the a assessment
	 */
	public void endAssessment(IXMLTag aAssessment) {
		CRunAssessmentsTree lTree = (CRunAssessmentsTree)CDesktopComponents.vView().getComponent("runAssessmentsTree");
		if (lTree == null)
			return;
		Treeitem lTreeitem = lTree.getContentItem(aAssessment.getAttribute(AppConstants.keyId));
		if (lTreeitem == null)
			return;
//		ask if sure
		Map<String,Object> lParams = new HashMap<String,Object>();
		String lValue = (String) lTreeitem.getAttribute("keyvalues");
		lParams.put("title", CDesktopComponents.vView().getLabel("run_assessment_end.title") + " '" + lValue + "'");
		lParams.put("text", CDesktopComponents.vView().getLabel("run_assessment_end.a") + " '" + lValue + "'" + CDesktopComponents.vView().getLabel("run_assessment_end.b"));
		Window lWindow = CDesktopComponents.vView().modalPopup(VView.v_run_assessment_end, null, lParams, "center");
		if (lWindow.getAttribute("ok") == null)
			return;
//		determine assessment score and save it
		int lScore = 0;
//NO		List lItems = aAssessment.getChilds("refitem");
		List<IXMLTag> lItems = getAssessmentItemsList (lTree,aAssessment);
		if (lItems != null) {
			for (IXMLTag lTag : lItems) {
				boolean lPresent = (!CDesktopComponents.sSpring().getCurrentTagStatus(lTag,AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse));
				// NOTE If assessment with randomly selected items, deleted for not selected items was set to true
				boolean lDeleted = CDesktopComponents.sSpring().getCurrentTagStatus(lTag,AppConstants.statusKeyDeleted).equals(AppConstants.statusValueTrue);
				if (lPresent && !lDeleted) {
					String lItemScore = CDesktopComponents.sSpring().getCurrentTagStatus(lTag,AppConstants.statusKeyScore);
					String lItemWeighting = lTag.getChildValue("weighting");
					if ((!lItemScore.equals("")) && (!lItemWeighting.equals(""))) {
						lScore = lScore + (Integer.parseInt(lItemScore)*Integer.parseInt(lItemWeighting));
					}
				}
			}
		}
		setRunTagStatus(caseComponent,aAssessment,AppConstants.statusKeyScore,""+lScore,true);
//		set status of assessment to finished
		setRunTagStatus(caseComponent,aAssessment,AppConstants.statusKeyFinished,AppConstants.statusValueTrue,true);
//		maybe add something for shared component
//		render assessment items
		lTree.reRenderTreeitem(lTreeitem,aAssessment,true);
//		remove item
		CRunArea lItemArea = (CRunArea)CDesktopComponents.vView().getComponent(getId()+"ItemArea");
		if (lItemArea != null)
			removeItem(lItemArea);
	}

	@Override
	protected boolean updateContent(IXMLTag aDataTag, IXMLTag aStatusTag, String aStatusKey, String aStatusValue) {
		if ((aDataTag != null) && (aStatusKey.equals(AppConstants.statusKeyPresent))) {
			setItemTagStatus(aDataTag.getParentTag().getAttribute(AppConstants.keyId), aDataTag.getAttribute(AppConstants.keyId), aStatusKey, aStatusValue);
			renderAssessment(aDataTag.getParentTag().getAttribute(AppConstants.keyId));
		}
		return true;
	}

	
	
	/**
	 * Closes assessments.
	 */
	public void close() {
	}

	/**
	 * Handle assessment.
	 *
	 * @param aAssessmentTagId the assessment tag id
	 */
	public void handleAssessment(String aAssessmentTagId) {
	}
	
	/**
	 * Prepair assessment item.
	 *
	 * @param aAssessmentTagId the ref item tag id
	 */
	public void prepairAssessmentItem(String aAssessmentTagId) {
	}
	
	/**
	 * Handle assessment item.
	 *
	 * @param aRefItemTagId the ref item tag id
	 */
	public void handleAssessmentItem(String aRefItemTagId) {
	}
	
	/**
	 * Start assessment.
	 */
	public void startAssessment() {
	}

	/**
	 * Restart assessment.
	 */
	public void restartAssessment() {
	}

	/**
	 * Resume assessment.
	 */
	public void resumeAssessment() {
	}

	/**
	 * Inspect assessment.
	 */
	public void inspectAssessment() {
	}

	/**
	 * Finish assessment.
	 */
	public void finishAssessment() {
	}

	/**
	 * Update assessment.
	 */
	public void updateAssessments() {
	}

	/**
	 * Update assessment.
	 */
	public void updateAssessment() {
	}

	/**
	 * Previous assessment item.
	 */
	public void handlePreviousAssessmentItem() {
	}

	/**
	 * Next assessment item.
	 */
	public void handleNextAssessmentItem() {
	}

	/**
	 * Handle assessment item alt.
	 *
	 * @param aAssessmentItemAltTagId the assessment item alt tag id
	 * @param aChecked the a checked
	 */
	public void handleAssessmentItemAlt(String aAssessmentItemAltTagId, boolean aChecked) {
	}

	/**
	 * Handle assessment item field.
	 *
	 * @param aAssessmentItemFieldTagId the assessment item field tag id
	 * @param aText the a text
	 */
	public void handleAssessmentItemField(String aAssessmentItemFieldTagId, String aText) {
	}

	/**
	 * Handle assessment feedback.
	 */
	public void handleAssessmentItemFeedback() {
	}

}
