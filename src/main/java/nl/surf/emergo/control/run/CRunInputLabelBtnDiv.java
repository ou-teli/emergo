/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zhtml.Table;
import org.zkoss.zhtml.Td;
import org.zkoss.zhtml.Tr;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;

import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.def.CDefImage;
import nl.surf.emergo.control.def.CDefLabel;

public class CRunInputLabelBtnDiv extends CDefDiv {
	private static final Logger _log = LogManager.getLogger(CRunInputLabelBtnDiv.class);
	private static final long serialVersionUID = 8038471876345693327L;

	public String classPrefix = "CRunInputLabelBtn";
	public String classType = "";
	public boolean textOrientationVertical = false;
	public int horizontalButtonHeight = 32;
	public int verticalButtonWidth = 268;
	public boolean isAccessible = true;

	public void onCreate(CreateEvent aEvent) {
		if (StringUtils.isEmpty((String) getAttribute("imageSrc"))) {
			// Green is default style
			classType = "Green";
			if (!StringUtils.isEmpty((String) getAttribute("classtype"))) {
				classType = (String) getAttribute("classtype");
			}
			textOrientationVertical = classType.contains("Vertical");
			if (!textOrientationVertical) {
				initHorizontalBtn();
			} else {
				initVerticalBtn();
			}
		} else {
			initImageBtn();
		}
		if (!StringUtils.isEmpty((String) getAttribute("accessible"))) {
			isAccessible = !((String) getAttribute("accessible")).equals(AppConstants.statusValueFalse);
		}
		if (!isAccessible) {
			String style = getStyle();
			if (style == null) {
				style = "";
			}
			// NOTE style class default has cursor pointer
			style += "opacity:0.3;cursor:default;";
			setStyle(style);
		}
	}

	protected void initHorizontalBtn() {
		Table table = appendTable(this);
		table.setClientAttribute("height", "" + horizontalButtonHeight);

		Tr tr = appendTr(table);
		appendTd(tr, "Left");
		appendTd(tr, "Spacer");
		Td td = appendTd(tr, "Label");
		td.setStyle((String) getAttribute("textstyle"));
		Label label = appendLabel(td, "Label");
		label.setStyle((String) getAttribute("textstyle"));
		appendTd(tr, "Spacer");
		appendTd(tr, "Right");

		Events.echoEvent("onInitLabelClient", this, null);
	}

	protected void initVerticalBtn() {
		Table table = appendTable(this);
		table.setClientAttribute("width", "" + verticalButtonWidth);

		Tr tr = appendTr(table);
		appendTd(tr, "Top");

		tr = appendTr(table);
		appendTd(tr, "Spacer");

		tr = appendTr(table);
		Td td = appendTd(tr, "Label");
		td.setStyle((String) getAttribute("textstyle"));
		Label label = appendLabel(td, "Label");
		label.setStyle((String) getAttribute("textstyle"));

		tr = appendTr(table);
		appendTd(tr, "Spacer");

		tr = appendTr(table);
		appendTd(tr, "Bottom");

		Events.echoEvent("onInitLabelClient", this, null);
	}

	protected void initImageBtn() {
		Image image = appendImage(this, "Image");
		Label label = appendLabel(this, "Label");
		label.setStyle((String) getAttribute("textstyle"));

		Events.echoEvent("onInitImageClient", this, new Object[] { image, label });
	}

	protected Table appendTable(Component parent) {
		Table table = new Table();
		parent.appendChild(table);
		table.setClientAttribute("border", "0");
		table.setClientAttribute("cellspacing", "0");
		table.setClientAttribute("cellpadding", "0");
		return table;
	}

	protected Tr appendTr(Table table) {
		Tr tr = new Tr();
		table.appendChild(tr);
		return tr;
	}

	protected Td appendTd(Tr tr, String sclassExtension) {
		Td td = new Td();
		tr.appendChild(td);
		td.setSclass(classPrefix + classType + sclassExtension);
		return td;
	}

	protected Label appendLabel(Component parent, String sclassExtension) {
		Label label = new CDefLabel();
		parent.appendChild(label);
		label.setSclass(classPrefix + classType + sclassExtension);
		if (getAttribute("label") != null) {
			label.setValue((String) getAttribute("label"));
		}
		if (getAttribute("labelKey") != null) {
			label.setValue(CDesktopComponents.vView().getLabel((String) getAttribute("labelKey")));
		}
		if (!StringUtils.isEmpty((String) getAttribute("maxLabelLength"))) {
			CDesktopComponents.vView().setLabelTooltiptext(label,
					Integer.parseInt((String) getAttribute("maxLabelLength")));
		}
		return label;
	}

	protected Image appendImage(Component parent, String sclassExtension) {
		Image image = new CDefImage();
		parent.appendChild(image);
		image.setSclass(classPrefix + sclassExtension);
		if (getAttribute("imageSrc") != null) {
			_log.info(getAttribute("imageSrc"));
			image.setSrc((String) getAttribute("imageSrc"));
		}
		return image;
	}

	public void onInitLabelClient(Event aEvent) {
		if (!textOrientationVertical && getUuid() != null) {
			Clients.evalJavaScript("setInputLabelWidth('" + getUuid() + "'," + horizontalButtonHeight + ");");
		}
	}

	public void onInitImageClient(Event aEvent) {
		Image image = (Image) ((Object[]) aEvent.getData())[0];
		Label label = (Label) ((Object[]) aEvent.getData())[1];
		Clients.evalJavaScript("setInputLabelPositionAndSize('" + getUuid() + "','" + image.getUuid() + "','"
				+ label.getUuid() + "');");
	}

	public void onClick(Event aEvent) {
		if (isAccessible) {
			Events.postEvent((String) getAttribute("event"),
					CDesktopComponents.vView().getComponent((String) getAttribute("observer")),
					getAttribute("eventdata"));
		}
	}

}
