/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.JuniorScientist;

import org.zkoss.zk.ui.event.Event;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.zkspring.STriggeredReference;

/**
 * This class is used to show a passage button.
 * 
 * The explanation child tag may be used to set certain parameters to be used to render the object:
 * - 'location', the location to open
 * - 'label', the label to be shown
 */
public class CRunJuniorScientistPassageBox extends CRunJuniorScientistBox {

	private static final long serialVersionUID = 4529790612831528349L;

	protected final static String paramKeyLocation = "location";
	protected final static String paramKeyLabel = "label";

	@Override
	public void onInit() {
		super.onInitMacro();
		
		//NOTE super ancestor CRunComponent needs case component to be set
		setCaseComponent(getNavigationCaseComponent());

		//determine passage tag properties to be used within the child macro
		setPassageProperties();

		//add the child macro
		addChildMacro("JuniorScientist_passage_view_macro.zul");
	}
	
	public void setPassageProperties() {
		propertyMap.put("extraMacroClass", getExtraMacroClass(currentTag));
		propertyMap.put("iconClass", getIconClass(currentTag));
		propertyMap.put("label", getLabel(currentTag));
	}
	
	public void onButtonClick(Event event) {
		possiblyPlaySound();
		//just like for the original passage tag set selected of the macro tag
		setRunTagStatusJS(getNavigationCaseComponent(), currentTag, AppConstants.statusKeySelected, AppConstants.statusValueTrue, false);
		toLocation(getLocation(currentTag));
	}
	
	protected String getLocation(IXMLTag macroTag) {
		return getParamValue(macroTag, paramKeyLocation);
	}
	
	protected String getLabel(IXMLTag macroTag) {
		return getParamValue(macroTag, paramKeyLabel);
	}
	
	@Override
	public void handleStatusChange(STriggeredReference aTriggeredReference) {
	}

}
