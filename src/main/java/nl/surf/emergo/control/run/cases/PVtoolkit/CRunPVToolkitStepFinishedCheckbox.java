/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Messagebox;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefCheckbox;
import nl.surf.emergo.view.VView;

public class CRunPVToolkitStepFinishedCheckbox extends CRunPVToolkitDefCheckbox {

	private static final long serialVersionUID = -3941328350476809392L;
	
	protected CRunPVToolkitSubStepsDiv _subStepsDiv;

	protected String _classPrefix = "subSteps";
	
	public CRunPVToolkitStepFinishedCheckbox(Component parent, String[] keys, Object[] values) {
		super(parent, keys, values);
	}
	
	public void init(CRunPVToolkitSubStepsDiv subStepsDiv) {
		_subStepsDiv = subStepsDiv;
	}
	
	public void setDisabled (boolean disabled) {
		super.setDisabled(disabled);
		String checkboxClass = "font " + _classPrefix + "FinishStepCheckbox";
		if (disabled) {
			checkboxClass += "Inactive";
		}
		else {
			checkboxClass += "Active";
		}
		setClass(checkboxClass);
	}
	
	public void onCheck() {
		super.onCheck();
		if (isChecked()) {
			VView vView = CDesktopComponents.vView();
			int choice = vView.showMessagebox(getRoot(), vView.getLabel("PV-toolkit.finish.step.finish.confirmation"), vView.getLabel("PV-toolkit.finish.step.finish"), Messagebox.CANCEL | Messagebox.OK, Messagebox.QUESTION);
			if (choice == Messagebox.OK) {
				setDisabled(true);
				_subStepsDiv.setStepFinished(true);
			}
			else {
				setChecked(false);
			}
		}
	}

}
