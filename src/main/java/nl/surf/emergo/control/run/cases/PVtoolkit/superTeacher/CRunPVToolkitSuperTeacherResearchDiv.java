/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.superTeacher;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Timer;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.IXMLAttributeValueTime;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CCaseHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitInitBox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefTextbox;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTag;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;

public class CRunPVToolkitSuperTeacherResearchDiv extends CDefDiv {

	private static final long serialVersionUID = 7117768699729917186L;

	public String zulfilepath = ((CRunPVToolkitInitBox) CDesktopComponents.vView().getComponent("PV-toolkit_initBox"))
			.getZulfilepath();

	protected CRunPVToolkit pvToolkit;

	protected IERunGroup _actor;
	protected boolean _editable;

	protected IAccountManager accountManager = (IAccountManager) CDesktopComponents.sSpring().getBean("accountManager");

	protected boolean isPreviewRun = false;

	protected String _idPrefix = "superTeacher";
	protected String _classPrefix = "superTeacher";

	protected boolean busy = false;
	protected boolean ready = false;
	protected int studentNumber = 0;
	protected List<IERunGroupAccount> students;
	protected IECaseComponent rubricsCaseComponent;
	protected IXMLTag skillTag;
	protected int numberOfSubSkillLevels = 0;
	protected int numberOfSkillClusterLevels = 0;
	protected int maxCycleNumber = 0;

	protected String[] settingIDs = new String[] {};
	protected String[] settingMasks = new String[] {};
	protected List<String[]> settingAssessments = new ArrayList<String[]>();

	protected String[] assessmentsCaseComponentNamesTPS = new String[] {};
	protected String[] assessmentsCaseComponentNamesPS = new String[] {};
	protected String[] assessmentsCaseComponentNamesR1R3 = new String[] {};
	protected String[] assessmentsCaseComponentNamesR2R4 = new String[] {};

	protected StringBuffer researchResults;
	protected String researchDataType = "";

	protected CCaseHelper caseHelper = new CCaseHelper();
	
	protected static final String tipTopGoalCrlf = "CR/LF";
	protected static final String onClickEvent = "onClick";

	protected static final String researchScoresStr = "scores";
	protected static final String researchAnswersStr = "answers";
	protected static final String researchTimesStr = "times";
	protected static final String researchDeadlinesStr = "deadlines";
	protected static final String researchTipstopsStr = "tipstops";
	protected static final String researchGoalsStr = "goals";
	protected static final String researchFBRatingsStr = "feedbackratings";
	
	public void init(IERunGroup actor, boolean editable) {
		_actor = actor;
		_editable = editable;

		setClass(_classPrefix);

		pvToolkit = (CRunPVToolkit) CDesktopComponents.vView().getComponent("pvToolkit");

		String lAss_1_2_N = "PV_assessment_cycle1_step2_N";
		String lAss_1_4_PS = "PV_assessment_cycle1_step4_PS";
		String lAss_1_4_TPS = "PV_assessment_cycle1_step4_TPS";
		String lAss_1_5_N = "PV_assessment_cycle1_step5_N";
		String lAss_2_4_TPS = "PV_assessment_cycle2_step4_TPS";
		String lAss_2_4_N = "PV_assessment_cycle2_step4_N";
		String lAss_3_2_N = "PV_assessment_cycle3_step2_N";
		String lAss_3_4_PS = "PV_assessment_cycle3_step4_PS";
		String lAss_3_4_TPS = "PV_assessment_cycle3_step4_TPS";
		String lAss_3_5_N = "PV_assessment_cycle3_step5_N";
		String lAss_4_4_TPS_N = "PV_assessment_cycle4_step4_TPS_N";
		String lAss_4_4_N = "PV_assessment_cycle4_step4_N";
		if (pvToolkit.getCurrentRubricCode().equals("JP")) {
			assessmentsCaseComponentNamesTPS = new String[] { lAss_1_2_N,
					lAss_1_4_TPS, lAss_1_5_N, lAss_3_2_N,
					lAss_3_4_TPS, lAss_3_5_N, lAss_4_4_N, };
			assessmentsCaseComponentNamesPS = new String[] { lAss_1_2_N,
					lAss_1_4_PS, lAss_1_5_N, lAss_3_2_N,
					lAss_3_4_PS, lAss_3_5_N, lAss_4_4_N, };
			assessmentsCaseComponentNamesR1R3 = new String[] { lAss_1_2_N,
					lAss_1_4_TPS, lAss_1_5_N, lAss_3_2_N,
					lAss_3_4_TPS, lAss_3_5_N, lAss_4_4_N, };
			assessmentsCaseComponentNamesR2R4 = new String[] { lAss_1_2_N,
					lAss_1_5_N, lAss_2_4_TPS, lAss_3_2_N,
					lAss_3_5_N, lAss_4_4_TPS_N, };
		} else if (pvToolkit.getCurrentRubricCode().equals("PP")) {
			assessmentsCaseComponentNamesTPS = new String[] { lAss_1_2_N,
					lAss_1_4_TPS, lAss_1_5_N, lAss_2_4_N, };
			assessmentsCaseComponentNamesPS = new String[] { lAss_1_2_N,
					lAss_1_4_PS, lAss_1_5_N, lAss_2_4_N, };
		}
		
		initExperimentalSetting();

		update();

		setVisible(true);
	}

	protected void initExperimentalSetting() {
		// Students will be separated in groups depending on experimental conditions.
		// For each condition a userid mask must be present, and a list of assessment component names 
		
		IECaseComponent lCaseComponent = CDesktopComponents.sSpring().getCaseComponent(CDesktopComponents.sSpring().getCase(), "", CRunPVToolkit.gCaseComponentNameStatesExperiment);
		if (lCaseComponent == null) {
			return;
		}
		String lIds = pvToolkit.getRunGroupStatusValue(_actor, lCaseComponent, "state", "str_experiment_setting_IDs", AppConstants.statusKeyValue).replace(AppConstants.statusCommaReplace, ",");
		if (!StringUtils.isEmpty(lIds)) {
			settingIDs = lIds.split(",");
			String lMasks = pvToolkit.getRunGroupStatusValue(_actor, lCaseComponent, "state", "str_experiment_setting_masks", AppConstants.statusKeyValue).replace(AppConstants.statusCommaReplace, ",");
			if (!StringUtils.isEmpty(lMasks)) {
				settingMasks = lMasks.split(",");
			}
			for (String lId: settingIDs) {
				String lAssessments = pvToolkit.getRunGroupStatusValue(_actor, lCaseComponent, "state", "str_experiment_assessments_" + lId, AppConstants.statusKeyValue).replace(AppConstants.statusCommaReplace, ",");
				if (!StringUtils.isEmpty(lAssessments)) {
					settingAssessments.add(lAssessments.split(","));
				} else
					settingAssessments.add(new String[] {});
			}
		}
	}
	
	protected boolean isStudentInSetting(IERunGroupAccount pRunGroupAccount, String pSettingMask) {
		IERunGroup lRunGroup = pRunGroupAccount.getERunGroup();
		if (!pvToolkit.isUsedInExperiment(lRunGroup)) {
			return false;
		}
		if (StringUtils.isEmpty(pSettingMask))
			return false;
		String lUserid = "";
		boolean lIsPreviewRun = CDesktopComponents.sSpring().getRun().getStatus() == AppConstants.run_status_test;
		if (lIsPreviewRun) {
			lUserid = lRunGroup.getName();
		} else {
			lUserid = pRunGroupAccount.getEAccount().getUserid();
		}
		return lUserid.matches(pSettingMask);
	}

	public void update() {
		getChildren().clear();

		pvToolkit.setMemoryCaching(true);

		isPreviewRun = CDesktopComponents.sSpring().getRun().getStatus() == AppConstants.run_status_test;
		
		String lClassStr = "class";
		String lLabelKeyStr = "labelKey";
		String lTkBtnClassStartStr = "font pvtoolkitButton " + _classPrefix;

		Div div = new CRunPVToolkitDefDiv(this, new String[] { lClassStr }, new Object[] { "titleRight" });

		new CRunPVToolkitDefLabel(div, new String[] { lClassStr, lLabelKeyStr },
				new Object[] { "font titleRight", "PV-toolkit-superTeacher.header.research" });

		Button btn = new CRunPVToolkitDefButton(this, new String[] { lClassStr, "cLabelKey" },
				new Object[] { lTkBtnClassStartStr + "BackButton", "PV-toolkit.back" });
		addBackToRunsOnClickEventListener(btn, this, CDesktopComponents.vView().getComponent("superTeacherRunsDiv"));

		div = new CRunPVToolkitDefDiv(this, new String[] { lClassStr }, new Object[] { _classPrefix + "ResearchDiv" });

		Textbox textbox = new CRunPVToolkitDefTextbox(div, new String[] { "id", "rows", "width" },
				new Object[] { _idPrefix + "ResearchTextbox", "24", "1000px" });

		btn = new CRunPVToolkitDefButton(this, new String[] { lClassStr, lLabelKeyStr },
				new Object[] { lTkBtnClassStartStr + "ResearchScoresButton",
						"PV-toolkit-superTeacher.button.researchScores" });
		addScoresButtonOnClickEventListener(btn, textbox);

		btn = new CRunPVToolkitDefButton(this, new String[] { lClassStr, lLabelKeyStr },
				new Object[] { lTkBtnClassStartStr + "ResearchAnswersButton",
						"PV-toolkit-superTeacher.button.researchAnswers" });
		addAnswersButtonOnClickEventListener(btn, textbox);

		btn = new CRunPVToolkitDefButton(this, new String[] { lClassStr, lLabelKeyStr },
				new Object[] { lTkBtnClassStartStr + "ResearchTimesButton",
						"PV-toolkit-superTeacher.button.researchTimes" });
		addTimesButtonOnClickEventListener(btn, textbox);

		btn = new CRunPVToolkitDefButton(this, new String[] { lClassStr, lLabelKeyStr },
				new Object[] { lTkBtnClassStartStr + "ResearchDeadlinesButton",
						"PV-toolkit-superTeacher.button.researchDeadlines" });
		addDeadlinesButtonOnClickEventListener(btn, textbox);

		btn = new CRunPVToolkitDefButton(this, new String[] { lClassStr, lLabelKeyStr },
				new Object[] { lTkBtnClassStartStr + "ResearchTipsTopsButton",
						"PV-toolkit-superTeacher.button.researchTipsTops" });
		addTipsTopsButtonOnClickEventListener(btn, textbox);

		btn = new CRunPVToolkitDefButton(this, 
				new String[]{lClassStr, lLabelKeyStr},
				new Object[]{lTkBtnClassStartStr + "ResearchGoalsButton", "PV-toolkit-superTeacher.button.researchGoals"}
		);
		addGoalsButtonOnClickEventListener(btn, textbox);
		
		btn = new CRunPVToolkitDefButton(this, 
				new String[]{lClassStr, lLabelKeyStr},
				new Object[]{lTkBtnClassStartStr + "ResearchFBRatingButton", "PV-toolkit-superTeacher.button.researchFeedbackRating"}
		);
		addFeedbackRatingButtonOnClickEventListener(btn, textbox);
		
		Timer researchProgressTimer = new CRunPVToolkitSuperTeacherResearchProgressTimer();
		appendChild(researchProgressTimer);
		researchProgressTimer.setId(_idPrefix + "ResearchProgressTimer");
		researchProgressTimer.setRunning(false);
		researchProgressTimer.setRepeats(false);

		new CRunPVToolkitDefLabel(this, new String[] { "id", "class", "value" }, new Object[] {
				_idPrefix + "ResearchProgressLabel", "font " + _classPrefix + "ResearchProgressLabel", "" });

		students = getStudents();
		rubricsCaseComponent = pvToolkit.getCurrentRubricsCaseComponent();
		skillTag = pvToolkit.getCurrentSkillTag();
		numberOfSubSkillLevels = pvToolkit.getNumberOfSubSkills(rubricsCaseComponent, skillTag);
		numberOfSkillClusterLevels = pvToolkit.getNumberOfSkillClusters(rubricsCaseComponent, skillTag);
		maxCycleNumber = pvToolkit.getMaxCycleNumber();

		pvToolkit.setMemoryCaching(false);
	}

	protected List<IERunGroupAccount> getStudents() {
		List<IERunGroupAccount> runGroupAccounts = new ArrayList<IERunGroupAccount>();
		for (IERunGroupAccount runGroupAccount : pvToolkit.getActiveRunGroupAccounts()) {
			if (pvToolkit.hasStudentRole(runGroupAccount.getERunGroup())) {
				runGroupAccounts.add(runGroupAccount);
			}
		}
		if (!isPreviewRun) {
			Collections.sort(runGroupAccounts, new HashtableSortByRespondentNumber());
		}
		return runGroupAccounts;
	}

	protected void addBackToRunsOnClickEventListener(Component component, Component fromComponent,
			Component toComponent) {
		component.addEventListener(onClickEvent, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) {
				if (fromComponent != null) {
					fromComponent.setVisible(false);
				}
				if (toComponent != null) {
					toComponent.setVisible(true);
				}
			}
		});
	}

	protected void addScoresButtonOnClickEventListener(Component component, Textbox textbox) {
		component.addEventListener(onClickEvent, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) {
				showScores();
			}
		});
	}

	protected void addAnswersButtonOnClickEventListener(Component component, Textbox textbox) {
		component.addEventListener(onClickEvent, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) {
				showAnswers();
			}
		});
	}

	protected void addTimesButtonOnClickEventListener(Component component, Textbox textbox) {
		component.addEventListener(onClickEvent, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) {
				showTimes();
			}
		});
	}

	protected void addDeadlinesButtonOnClickEventListener(Component component, Textbox textbox) {
		component.addEventListener(onClickEvent, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) {
				showDeadlines();
			}
		});
	}

	protected void addTipsTopsButtonOnClickEventListener(Component component, Textbox textbox) {
		component.addEventListener(onClickEvent, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) {
				showTipsTops();
			}
		});
	}

	protected void addGoalsButtonOnClickEventListener(Component component, Textbox textbox) {
		component.addEventListener(onClickEvent, new EventListener<Event>() {
			public void onEvent(Event event) {
				showGoals();
			}
		});
	}
	
	protected void addFeedbackRatingButtonOnClickEventListener(Component component, Textbox textbox) {
		component.addEventListener(onClickEvent, new EventListener<Event>() {
			public void onEvent(Event event) {
				showFeedbackRating();
			}
		});
	}
	
	protected class HashtableSortByRespondentNumber implements Comparator<IERunGroupAccount> {
		@Override
		public int compare(IERunGroupAccount o1, IERunGroupAccount o2) {
			// TODO better general solution
			// for the research all user ids start with 'um_pv_001' where 001 is the
			// respondent number
			return o1.getEAccount().getUserid().substring(6, 9).compareTo(o2.getEAccount().getUserid().substring(6, 9));
		}
	}

	protected void showScores() {
		researchDataType = researchScoresStr;
		initResearchFeedbackGiverData();
	}

	protected void showAnswers() {
		researchDataType = researchAnswersStr;
		initResearchFeedbackGiverData();
	}

	protected void showTimes() {
		researchDataType = researchTimesStr;
		initResearchFeedbackGiverData();
	}

	protected void showDeadlines() {
		researchDataType = researchDeadlinesStr;
		initResearchFeedbackGiverData();
	}

	protected void showTipsTops() {
		researchDataType = researchTipstopsStr;
		initResearchFeedbackGiverData();
	}

	protected void showGoals() {
		researchDataType = researchGoalsStr;
		initResearchFeedbackGiverData();
	}

	protected void showFeedbackRating() {
		researchDataType = researchFBRatingsStr;
		initResearchFeedbackGiverData();
	}

	protected void initResearchFeedbackGiverData() {
		pvToolkit.setMemoryCaching(true);

		busy = false;
		ready = false;
		studentNumber = 0;
		researchResults = new StringBuffer();

		Timer researchProgressTimer = (Timer) CDesktopComponents.vView()
				.getComponent(_idPrefix + "ResearchProgressTimer");
		Label researchProgressLabel = (Label) CDesktopComponents.vView()
				.getComponent(_idPrefix + "ResearchProgressLabel");
		Textbox researchTextbox = (Textbox) CDesktopComponents.vView().getComponent(_idPrefix + "ResearchTextbox");
		researchProgressTimer.stop();
		researchProgressLabel.setValue("");
		researchTextbox.setValue("");
		// NOTE asynchronously presenting results using timer
		if (students.size() > 0) {
			Events.echoEvent("onStart", researchProgressTimer, null);
		}
	}

	public void onGenerateResearchFeedbackGiverData() {
		Timer researchProgressTimer = (Timer) CDesktopComponents.vView()
				.getComponent(_idPrefix + "ResearchProgressTimer");
		Label researchProgressLabel = (Label) CDesktopComponents.vView()
				.getComponent(_idPrefix + "ResearchProgressLabel");
		int studentsSize = students.size();
		if (studentNumber >= studentsSize) {
			researchProgressTimer.stop();
			if (!ready) {
				ready = true;
				Events.postEvent("onReady", this, null);
			}
		} else {
			busy = true;
			if (researchDataType.equals(researchScoresStr)) {
				showStudentScores();
			} else if (researchDataType.equals(researchAnswersStr)) {
				showStudentAnswers();
			} else if (researchDataType.equals(researchTimesStr)) {
				showStudentTimes();
			} else if (researchDataType.equals(researchTipstopsStr)) {
				showStudentTipsTops();
			}
			else if (researchDataType.equals(researchGoalsStr)) {
				showStudentGoals();
			}
			else if (researchDataType.equals(researchFBRatingsStr)) {
				showStudentFeedbackRating();
			}
			studentNumber++;
			long percentage = (100 * studentNumber) / studentsSize;
			researchProgressLabel.setValue("" + percentage + "%");
			busy = false;
			Events.echoEvent("onStart", researchProgressTimer, null);
		}
	}

	public void onReady() {
		pvToolkit.setMemoryCaching(false);

		Textbox researchTextbox = (Textbox) CDesktopComponents.vView().getComponent(_idPrefix + "ResearchTextbox");
		researchTextbox.setValue(researchResults.toString());
	}

	boolean isBusy() {
		return busy;
	}

	protected void showStudentScores() {
		IERunGroupAccount student = students.get(studentNumber);

		// add student id
		researchResults.append(student.getEAccount().getUserid());

		List<List<IXMLTag>> sharedFeedbackTagsPerFeedbackStep = pvToolkit
				.getSharedFeedbackTagsPerFeedbackStep(student.getERunGroup(), true, null, null);
		int runGroupAccountmaxCycleNumber = Math.min(maxCycleNumber, sharedFeedbackTagsPerFeedbackStep.size());

		for (int cycleNumber = 1; cycleNumber <= maxCycleNumber; cycleNumber++) {
			List<IXMLTag> feedbackTags = null;
			if (cycleNumber <= runGroupAccountmaxCycleNumber) {
				feedbackTags = sharedFeedbackTagsPerFeedbackStep.get(cycleNumber - 1);
			}

			researchResults.append(getCycleScores(student, feedbackTags));
		}

		researchResults.append("\n");

	}

	protected StringBuffer getCycleScores(IERunGroupAccount student, List<IXMLTag> feedbackTags) {
		IERunGroupAccount teacher = null;
		List<Integer> teacherSubSkillLevels = new ArrayList<Integer>();
		Integer teacherSkillLevel = -1;
		List<IERunGroupAccount> peers = new ArrayList<IERunGroupAccount>();
		List<List<Integer>> peersSubSkillLevels = new ArrayList<List<Integer>>();
		List<Integer> peersSkillLevels = new ArrayList<Integer>();
		List<Integer> selfSubSkillLevels = new ArrayList<Integer>();
		Integer selfSkillLevel = -1;
		if (feedbackTags != null) {
			for (IXMLTag feedbackTag : feedbackTags) {
				IERunGroupAccount feedbackGiver = pvToolkit
						.getRunGroupAccount(feedbackTag.getAttribute("feedbackrgaid"));
				if (feedbackGiver != null) {
					IERunGroup lFBGiverRunGroup = feedbackGiver.getERunGroup();
					IXMLTag feedbackStepTag = pvToolkit.getStepTagByChildTagOfStatusTag(lFBGiverRunGroup, feedbackTag);

					String feedbackGiverRole = pvToolkit.getPeerGroupFeedbackGiverRole(student.getERunGroup(), lFBGiverRunGroup);
					boolean isSelf = false;
					boolean isPeer = false;
					boolean isTeacher = false;
					List<Integer> peerSubSkillLevels = null;
					if (feedbackGiverRole.equals("")) {
						//NOTE no common peer group, so either feedback giver or practitioner has been moved to other peer group
						//Assume feedback giver has same role in each peer group
						if (pvToolkit.hasTeacherRole(lFBGiverRunGroup) || pvToolkit.hasStudentAssistantRole(lFBGiverRunGroup)) {
							feedbackGiverRole = CRunPVToolkit.peerGroupTeacherRole;
						} else {
							feedbackGiverRole = CRunPVToolkit.peerGroupStudentRole;
						}
					}
					if (feedbackGiverRole.equals("self")) {
						isSelf = true;
					} else if (feedbackGiverRole.equals(CRunPVToolkit.peerGroupStudentRole)) {
						isPeer = true;
						peers.add(feedbackGiver);
						peerSubSkillLevels = new ArrayList<Integer>();
						peersSubSkillLevels.add(peerSubSkillLevels);
					} else {
						//NOTE assume only one teacher, either in role 'peerGroupTeacherRole' or in role 'peerGroupStudentAssistantRole' or in role 'peerGroupPeerStudentRole'
						//TODO: code has to be changed when peer group can have multiple teachers
						isTeacher = true;
						teacher = feedbackGiver;
					}
					for (IXMLTag skillclusterTag : skillTag.getChilds("skillcluster")) {
						if (pvToolkit.isRunSpecificTagPresent(
								new CRunPVToolkitCacAndTag(rubricsCaseComponent, skillclusterTag))) {
							for (IXMLTag subskillTag : skillclusterTag.getChilds("subskill")) {
								if (pvToolkit.isRunSpecificTagPresent(
										new CRunPVToolkitCacAndTag(rubricsCaseComponent, subskillTag))) {
									int level = (new Double(pvToolkit.getFeedbackLevel(lFBGiverRunGroup,
											feedbackStepTag, feedbackTag, true, subskillTag))).intValue();
									if (isTeacher) {
										teacherSubSkillLevels.add(level);
									} else if (isPeer) {
										if (peerSubSkillLevels != null) {
											peerSubSkillLevels.add(level);
										}
									} else if (isSelf) {
										selfSubSkillLevels.add(level);
									}
								}
							}
						}
					}
					int level = (new Double(pvToolkit.getFeedbackLevel(lFBGiverRunGroup, feedbackStepTag,
							feedbackTag, true, skillTag))).intValue();
					if (isTeacher) {
						teacherSkillLevel = level;
					} else if (isPeer) {
						if (peerSubSkillLevels != null) {
							peersSkillLevels.add(level);
						}
					} else if (isSelf) {
						selfSkillLevel = level;
					}
				}
			}
		}

		StringBuffer result = new StringBuffer();
		// add teacher id and teacher scores
		if (teacher != null && !teacherSubSkillLevels.isEmpty()) {
			appendRunGroupAccountLevels(teacher, teacherSubSkillLevels, teacherSkillLevel, result);
		} else {
			appendEmptyLevels(numberOfSubSkillLevels, result);
		}
		// add peer ids and peers scores
		if (!peers.isEmpty() && !peersSubSkillLevels.isEmpty()) {
			for (int i = 0; i < peers.size(); i++) {
				appendRunGroupAccountLevels(peers.get(i), peersSubSkillLevels.get(i), peersSkillLevels.get(i), result);
			}
		}
		for (int i = peers.size(); i < 3; i++) {
			appendEmptyLevels(numberOfSubSkillLevels, result);
		}
		// add student id and self scores
		if (!selfSubSkillLevels.isEmpty()) {
			appendRunGroupAccountLevels(student, selfSubSkillLevels, selfSkillLevel, result);
		} else {
			appendEmptyLevels(numberOfSubSkillLevels, result);
		}

		return result;
	}

	protected void appendRunGroupAccountLevels(IERunGroupAccount runGroupAccount, List<Integer> subSkillLevels,
			Integer skillLevel, StringBuffer result) {
		result.append("\t");
		result.append(runGroupAccount.getEAccount().getUserid());
		Double total = 0.0;
		for (Integer level : subSkillLevels) {
			result.append("\t");
			result.append(level.toString());
			total += level;
		}
		result.append("\t");
		result.append(skillLevel.toString());
		result.append("\t");
		result.append(Double.toString(total / subSkillLevels.size()));
	}

	protected void appendEmptyLevels(int numberOfSubSkillLevels, StringBuffer result) {
		result.append("\tNaN");
		for (int i = 0; i < numberOfSubSkillLevels; i++) {
			result.append("\t-1");
		}
		result.append("\t-1");
		result.append("\t-1");
	}

	protected void showStudentAnswers() {
		IERunGroupAccount student = students.get(studentNumber);
		IECaseRole caseRole = CDesktopComponents.sSpring().getSCaseRoleHelper().getCaseRole();

		String[] assessmentsCaseComponentNames = null;
		String lAssessments = pvToolkit.getExperimentAssessments(student.getERunGroup());
		if (!StringUtils.isEmpty(lAssessments)) {
			assessmentsCaseComponentNames = lAssessments.split(",");
		} else {
			if (settingIDs.length > 0) {
				//NOTE legacy code
				for (int lInd = 0; lInd < settingIDs.length; lInd++) {
					if (settingMasks.length > lInd) {
						if (isStudentInSetting(student, settingMasks[lInd])) {
							assessmentsCaseComponentNames = settingAssessments.get(lInd);
							break;
						}
					}
				}
			}
		}
		
		if (assessmentsCaseComponentNames == null) {
			//NOTE even legacier code
			if (pvToolkit.participatesInVariantR1R3(student)) {
				assessmentsCaseComponentNames = assessmentsCaseComponentNamesR1R3;
			} else if (pvToolkit.participatesInVariantR2R4(student)) {
				assessmentsCaseComponentNames = assessmentsCaseComponentNamesR2R4;
			} else if (pvToolkit.hasPeersWithTeacherRole(student.getERunGroup())) {
				assessmentsCaseComponentNames = assessmentsCaseComponentNamesTPS;
			} else {
				assessmentsCaseComponentNames = assessmentsCaseComponentNamesPS;
			}
		}

		// add student id
		researchResults.append(student.getEAccount().getUserid());

		for (int i = 0; i < assessmentsCaseComponentNames.length; i++) {
			String assessmentsCaseComponentName = assessmentsCaseComponentNames[i];
			List<String> answers = getStudentAssessmentAnswers(student, caseRole, assessmentsCaseComponentName);
			if (assessmentsCaseComponentName.contains("_PS") && answers.get(0).equals("-1")) {
				// NOTE due to a bug some students in round 1 got the TPS questionnaire instead
				// of the PS questionnaire
				// So if no answers are given to the PS, try the TPS
				assessmentsCaseComponentName = assessmentsCaseComponentNamesTPS[i];
				answers = getStudentAssessmentAnswers(student, caseRole, assessmentsCaseComponentName);
				// clear last 7 answers given on teacher questions
				for (int j = 7; j < answers.size(); j++) {
					answers.set(j, "-1");
				}
			}
			for (String answer : answers) {
				researchResults.append("\t");
				researchResults.append(answer);
			}
			if (assessmentsCaseComponentName.contains("_PS")) {
				// NOTE PS questionnaire contains 7 questions and TPS questionnaire 14
				// questions, so add 7 empty alternative numbers in case of PS questionnaire.
				for (int j = 0; j < 7; j++) {
					researchResults.append("\t-1");
				}
			}
		}

		researchResults.append("\n");

	}

	protected List<String> getStudentAssessmentAnswers(IERunGroupAccount student, IECaseRole caseRole,
			String assessmentsCaseComponentName) {
		List<String> answers = new ArrayList<String>();
		IECaseComponent assessmentsCaseComponent = CDesktopComponents.sSpring().getCaseComponent("",
				assessmentsCaseComponentName);
		IXMLTag rootTag = pvToolkit.getXmlDataPlusStatusRootTag(student.getERunGroup(), assessmentsCaseComponent,
				AppConstants.statusTypeRunGroup);
		List<IXMLTag> nodeTags = CDesktopComponents.cScript().getNodeTags(rootTag);
		for (IXMLTag nodeTag : nodeTags) {
			if (nodeTag.getName().equals("refitem")) {
				String itemAlternativeTagId = nodeTag.getCurrentStatusAttribute(AppConstants.statusKeyAnswer);
				String reftype = nodeTag.getChild("ref").getDefTag().getAttribute(AppConstants.defKeyReftype);
				List<String> refIds = caseHelper.getRefTagIds(reftype, "" + AppConstants.statusKeySelectedIndex,
						caseRole, assessmentsCaseComponent, nodeTag);
				String refId = refIds.get(0);
				String answer = "-1";
				if (!StringUtils.isEmpty(refId)) {
					String[] idArr = refId.split(",");
					if (idArr.length == 3) {
						String cacId = idArr[1];
						String tagId = idArr[2];
						IXMLTag itemTag = CDesktopComponents.sSpring()
								.getTag(CDesktopComponents.sSpring().getCaseComponent(Integer.parseInt(cacId)), tagId);
						if (itemTag != null) {
							for (IXMLTag tempAlternativeTag : itemTag.getChilds("alternative")) {
								if (tempAlternativeTag.getAttribute(AppConstants.keyId).equals(itemAlternativeTagId)) {
									String alternativeKey = tempAlternativeTag.getChildValue("pid");
									answer = alternativeKey.substring(alternativeKey.lastIndexOf("-") + 1);
									break;
								}
							}
						}
					}
				}
				answers.add(answer);
			}
		}
		return answers;
	}

	protected void showStudentTimes() {
		IERunGroupAccount student = students.get(studentNumber);

		// NOTE first get tags to determine times
		// Because step 1 within cycle 1 is mostly done within the game, time spent has
		// to be added to time spent in PV-tool phase

		// NOTE get tags for clickableobject PV-tool Open, clickableobject back-to-game
		// and location PV_tool_intro closed located within the navigation component
		// Clicks on PV-tool Open and back-to-game determine time spent within the game.
		// Clicking PV-tool Open opens the PV-tool. Clicking back-to-game closes
		// PV-tool.
		// Note that number of clicks on back-to-game may be larger than number of
		// clicks on PV-tool Open, because student may try to close PV-tool, but may not
		// be ready yet, so stays in PV-tool. However, back-to-game is clicked.
		// Opened of location PV_tool_intro is set to false if the PV-tool phase is
		// started. This then determines start of step 1 in cycle 1 within the PV-tool
		// phase
		IECaseComponent navigationCaseComponent = CDesktopComponents.sSpring().getCaseComponent("", "game navigation");
		IXMLTag rootTag = pvToolkit.getXmlDataPlusStatusRootTag(student.getERunGroup(), navigationCaseComponent,
				AppConstants.statusTypeRunGroup);
		List<IXMLTag> nodeTags = CDesktopComponents.cScript().getNodeTags(rootTag);
		// get tags for clickableobject PV-tool Open, clickableobject back-to-game and
		// location PV_tool_intro
		IXMLTag pvToolOpenClickableObjectTag = null;
		IXMLTag backToGameClickableObjectTag = null;
		IXMLTag pvToolIntroLocationTag = null;
		for (IXMLTag nodeTag : nodeTags) {
			if (nodeTag.getChildValue("pid").equals("PV-tool Open") && nodeTag.getName().equals("clickableobject")) {
				pvToolOpenClickableObjectTag = nodeTag;
			} else if (nodeTag.getChildValue("pid").equals("back-to-game")
					&& nodeTag.getName().equals("clickableobject")) {
				backToGameClickableObjectTag = nodeTag;
			} else if (nodeTag.getChildValue("pid").equals("PV_tool_intro") && nodeTag.getName().equals("location")) {
				pvToolIntroLocationTag = nodeTag;
			}
		}
		if (pvToolOpenClickableObjectTag == null || backToGameClickableObjectTag == null
				|| pvToolIntroLocationTag == null) {
			// all tags must be found
			return;
		}

		// NOTE get step tags in correct order to determine times when they were
		// finished
		//NOTE for the moment, steps should have ids like 'stap_1_1', with 6th char equaling cycle number and 8th char equaling step number
		IECaseComponent pvProjectsCaseComponent = CDesktopComponents.sSpring().getCaseComponent("", CRunPVToolkit.gCaseComponentNameProjects);
		rootTag = pvToolkit.getXmlDataPlusStatusRootTag(student.getERunGroup(), pvProjectsCaseComponent,
				AppConstants.statusTypeRunGroup);
		nodeTags = CDesktopComponents.cScript().getNodeTags(rootTag);
		IXMLTag[] stepTags = new IXMLTag[20];
		for (IXMLTag nodeTag : nodeTags) {
			if (nodeTag.getName().equals("step")) {
				String pid = nodeTag.getChildValue("pid");
				String cycle = pid.substring(5, 6);
				String step = pid.substring(7, 8);
				try {
					int index = (Integer.parseInt(cycle) - 1) * 5 + Integer.parseInt(step) - 1;
					stepTags[index] = nodeTag;
				} catch (NumberFormatException nFE) {
					CDesktopComponents.sSpring().getSLogHelper().logAction("PV tool superteacher research: showStudentTimes: step id not valid: " + pid);
				}
			}
		}
		for (int i = 0; i < stepTags.length; i++) {
			if (stepTags[i] == null) {
				// all step tags must be found
				return;
			}
		}

		// determine time spent within steps
		double[] stepTimes = new double[stepTags.length];

		// NOTE determine time spent in step 1 of cycle 1 within game
		// NOTE get events when PV-tool Open back-to-game were clicked (= selected)
		List<IXMLAttributeValueTime> pvToolOpenSelectedAttrTimes = pvToolOpenClickableObjectTag
				.getStatusAttribute(AppConstants.statusKeySelected);
		List<IXMLAttributeValueTime> pvBackToGameSelectedAttrTimes = backToGameClickableObjectTag
				.getStatusAttribute(AppConstants.statusKeySelected);
		// loop through events when PV-tool has been opened
		for (IXMLAttributeValueTime pvToolOpenSelectedAttrTime : pvToolOpenSelectedAttrTimes) {
			// get time pvTool is opened
			double pvToolOpenedTime = pvToolOpenSelectedAttrTime.getTime();
			// Note that number of clicks on back-to-game may be larger than number of
			// clicks on PV-tool Open, because student may try to close PV-tool, but may not
			// be ready yet, so stays in PV-tool. However, back-to-game is clicked.
			// Therefore determine next pvTool opened time. If next pvTool opened time
			// exists, click on back-to-game must before next pvTool opened time.
			double nextPvToolOpenedTime = -1.0;
			int index = pvToolOpenSelectedAttrTimes.indexOf(pvToolOpenSelectedAttrTime);
			if (index < (pvToolOpenSelectedAttrTimes.size() - 1)) {
				// only get next pvTool opened time if there is a next pvTool opened time.
				nextPvToolOpenedTime = pvToolOpenSelectedAttrTimes.get(index + 1).getTime();
			}
			// determine time pvTool is closed
			double pvToolClosedTime = -1.0;
			// loop through events when student tried to close PV-tool
			for (IXMLAttributeValueTime pvBackToGameSelectedAttrTime : pvBackToGameSelectedAttrTimes) {
				if (nextPvToolOpenedTime < 0) {
					// if no next pvTool opened time, get last time student tried to close PV-tool
					if (pvBackToGameSelectedAttrTime.getTime() > pvToolOpenedTime) {
						// time of closing has to be after time of opening
						pvToolClosedTime = pvBackToGameSelectedAttrTime.getTime();
					}
				} else if (pvBackToGameSelectedAttrTime.getTime() > pvToolOpenedTime
						&& pvBackToGameSelectedAttrTime.getTime() < nextPvToolOpenedTime) {
					// if next pvTool opened time exists, get last time student tried to close
					// PV-tool before next pvTool opened time
					pvToolClosedTime = pvBackToGameSelectedAttrTime.getTime();
				}
			}
			// increase time spent in step 1 of cycle 1
			if (pvToolClosedTime > 0) {
				// only increase time spent in step 1 of cycle 1 if pvTool is closed
				stepTimes[0] += (pvToolClosedTime - pvToolOpenedTime);
			}
		}

		// NOTE get the time when the pvTool was started during the PV-tool phase, i.e.
		// when location pvTool intro was closed
		List<IXMLAttributeValueTime> pvToolIntroClosedAttrTimes = pvToolIntroLocationTag
				.getStatusAttribute(AppConstants.statusKeyOpened);
		double pvToolStartedTime = -1.0;
		for (IXMLAttributeValueTime pvToolIntroClosedAttrTime : pvToolIntroClosedAttrTimes) {
			if (pvToolIntroClosedAttrTime.getValue().equals(AppConstants.statusValueFalse)) {
				pvToolStartedTime = pvToolIntroClosedAttrTime.getTime();
			}
		}
		if (pvToolStartedTime < 0) {
			// pvTool must have been started
			return;
		}
		// increase time spent in step 1 of cycle 1 by time spent within PV-tool phase
		stepTimes[0] += stepTags[0].getCurrentStatusAttributeTime(AppConstants.statusKeyFinished) - pvToolStartedTime;
		// determine time spent in other steps by using finishing of step and previous
		// step
		for (int i = 1; i < stepTimes.length; i++) {
			stepTimes[i] = stepTags[i].getCurrentStatusAttributeTime(AppConstants.statusKeyFinished)
					- stepTags[i - 1].getCurrentStatusAttributeTime(AppConstants.statusKeyFinished);
		}

		// add student id
		researchResults.append(student.getEAccount().getUserid());

		for (int i = 0; i < stepTimes.length; i++) {
			researchResults.append("\t");
			// result in seconds
			researchResults.append(Math.round(stepTimes[i]));
		}

		researchResults.append("\n");

	}

	protected void showStudentTipsTops() {
		IERunGroupAccount student = students.get(studentNumber);

		// add student id
		researchResults.append(student.getEAccount().getUserid());

		List<List<IXMLTag>> sharedFeedbackTagsPerFeedbackStep = pvToolkit
				.getSharedFeedbackTagsPerFeedbackStep(student.getERunGroup(), true, null, null);
		int runGroupAccountmaxCycleNumber = Math.min(maxCycleNumber, sharedFeedbackTagsPerFeedbackStep.size());

		for (int cycleNumber = 1; cycleNumber <= maxCycleNumber; cycleNumber++) {
			List<IXMLTag> feedbackTags = null;
			if (cycleNumber <= runGroupAccountmaxCycleNumber) {
				feedbackTags = sharedFeedbackTagsPerFeedbackStep.get(cycleNumber - 1);
			}

			researchResults.append(getCycleTipsTops(student, feedbackTags));
		}

		researchResults.append("\n");

	}

	protected StringBuffer getCycleTipsTops(IERunGroupAccount student, List<IXMLTag> feedbackTags) {
		IERunGroupAccount teacher = null;
		List<String> teacherSkillClusterTipsTops = new ArrayList<String>();
		List<IERunGroupAccount> peers = new ArrayList<IERunGroupAccount>();
		List<List<String>> peersSkillClusterTipsTops = new ArrayList<List<String>>();
		List<String> selfSkillClusterTipsTops = new ArrayList<String>();
		if (feedbackTags != null) {
			for (IXMLTag feedbackTag : feedbackTags) {
				IERunGroupAccount feedbackGiver = pvToolkit
						.getRunGroupAccount(feedbackTag.getAttribute("feedbackrgaid"));
				if (feedbackGiver != null) {
					IERunGroup lFBGiverRunGroup = feedbackGiver.getERunGroup();
					IXMLTag feedbackStepTag = pvToolkit.getStepTagByChildTagOfStatusTag(lFBGiverRunGroup, feedbackTag);

					String feedbackGiverRole = pvToolkit.getPeerGroupFeedbackGiverRole(student.getERunGroup(), lFBGiverRunGroup);
					boolean isSelf = false;
					boolean isPeer = false;
					boolean isTeacher = false;
					List<String> peerSkillClusterTipsTops = null;
					if (feedbackGiverRole.equals("")) {
						//NOTE no common peer group, so either feedback giver or practitioner has been moved to other peer group
						//Assume feedback giver has same role in each peer group
						if (pvToolkit.hasTeacherRole(lFBGiverRunGroup) || pvToolkit.hasStudentAssistantRole(lFBGiverRunGroup)) {
							feedbackGiverRole = CRunPVToolkit.peerGroupTeacherRole;
						} else {
							feedbackGiverRole = CRunPVToolkit.peerGroupStudentRole;
						}
					}

					if (feedbackGiverRole.equals("self")) {
						isSelf = true;
					} else if (feedbackGiverRole.equals(CRunPVToolkit.peerGroupStudentRole)) {
						isPeer = true;
						peers.add(feedbackGiver);
						peerSkillClusterTipsTops = new ArrayList<String>();
						peersSkillClusterTipsTops.add(peerSkillClusterTipsTops);
					} else {
						//NOTE assume only one teacher, either in role 'peerGroupTeacherRole' or in role 'peerGroupStudentAssistantRole' or in role 'peerGroupPeerStudentRole'
						//TODO: code has to be changed when peer group can have multiple teachers
						isTeacher = true;
						teacher = feedbackGiver;
					}
					for (IXMLTag skillclusterTag : skillTag.getChilds("skillcluster")) {
						if (pvToolkit.isRunSpecificTagPresent(
								new CRunPVToolkitCacAndTag(rubricsCaseComponent, skillclusterTag))) {
							String tops = pvToolkit.getFeedbackTops(lFBGiverRunGroup, feedbackStepTag,
									feedbackTag, true, skillclusterTag).replace("\n", tipTopGoalCrlf);
							String tips = pvToolkit.getFeedbackTips(lFBGiverRunGroup, feedbackStepTag,
									feedbackTag, true, skillclusterTag).replace("\n", tipTopGoalCrlf);
							if (isTeacher) {
								teacherSkillClusterTipsTops.add(tops);
								teacherSkillClusterTipsTops.add(tips);
							} else if (isPeer) {
								if (peerSkillClusterTipsTops != null) {
									peerSkillClusterTipsTops.add(tops);
									peerSkillClusterTipsTops.add(tips);
								}
							} else if (isSelf) {
								selfSkillClusterTipsTops.add(tops);
								selfSkillClusterTipsTops.add(tips);
							}
						}
					}
				}
			}
		}

		StringBuffer result = new StringBuffer();
		// add teacher id and teacher scores
		if (teacher != null && !teacherSkillClusterTipsTops.isEmpty()) {
			appendRunGroupAccountTipsTops(teacher, teacherSkillClusterTipsTops, result);
		} else {
			// NOTE number of tips/tops is 2 * number of skill cluster levels
			appendEmptyTipsTops(2 * numberOfSkillClusterLevels, result);
		}
		// add peer ids and peers scores
		if (!peers.isEmpty() && !peersSkillClusterTipsTops.isEmpty()) {
			for (int i = 0; i < peers.size(); i++) {
				appendRunGroupAccountTipsTops(peers.get(i), peersSkillClusterTipsTops.get(i), result);
			}
		}
		for (int i = peers.size(); i < 3; i++) {
			// NOTE number of tips/tops is 2 * number of skill cluster levels
			appendEmptyTipsTops(2 * numberOfSkillClusterLevels, result);
		}
		// add student id and self scores
		if (!selfSkillClusterTipsTops.isEmpty()) {
			appendRunGroupAccountTipsTops(student, selfSkillClusterTipsTops, result);
		} else {
			// NOTE number of tips/tops is 2 * number of skill cluster levels
			appendEmptyTipsTops(2 * numberOfSkillClusterLevels, result);
		}

		return result;
	}

	protected void appendRunGroupAccountTipsTops(IERunGroupAccount runGroupAccount, List<String> skillClusterTipsTops,
			StringBuffer result) {
		result.append("\t");
		result.append(runGroupAccount.getEAccount().getUserid());
		for (String tipsOrTops : skillClusterTipsTops) {
			result.append("\t");
			result.append(tipsOrTops);
		}
	}

	protected void appendEmptyTipsTops(int numberOfTipsTops, StringBuffer result) {
		result.append("\tNaN");
		for (int i = 0; i < numberOfTipsTops; i++) {
			result.append("\t");
		}
	}

	protected void showStudentGoals() {
		IERunGroupAccount student = students.get(studentNumber);
			
		//add student id
		researchResults.append(student.getEAccount().getUserid());

		for (IXMLTag lGoalsStepTag : pvToolkit.getStepTags(CRunPVToolkit.defineGoalsStepType)) {
			researchResults.append("\t");
			researchResults.append(pvToolkit.getGoals(student.getERunGroup(), lGoalsStepTag).replace("\n", tipTopGoalCrlf));
		}

		researchResults.append("\n");
	}
	
	protected void showStudentFeedbackRating() {
		IERunGroupAccount student = students.get(studentNumber);
		//add student id
		researchResults.append(student.getEAccount().getUserid());
		List<String> rolesToFilterOn = new ArrayList<>();
		// only practicing peer students give feedback ratingpractice, so filter on student role
		rolesToFilterOn.add(CRunPVToolkit.peerGroupStudentRole);
		List<IERunGroupAccount> lPeerRgas = pvToolkit.getPeerRungroupAccounts(student.getERunGroup(), false, rolesToFilterOn, null);
		
		List<List<String>> lFBRatings = new ArrayList<>();
		String lStuRgaId = Integer.toString(student.getRgaId());
		for (IERunGroupAccount lPeerRga : lPeerRgas) {
			// feedback rating that lPeerRga has given to student is included in practice status tag
			List<IXMLTag> sharedPracticeTagsPerFeedbackStep = pvToolkit.getSharedPracticeTagsPerFeedbackStep(lPeerRga.getERunGroup());
			int lInd = 0;
			for (IXMLTag sharedPracticeTagPerFeedbackStep : sharedPracticeTagsPerFeedbackStep) {
				for (IXMLTag lFeedbackRatingTag : pvToolkit.getStatusChildTag(sharedPracticeTagPerFeedbackStep).getChilds(CRunPVToolkit._feedbackRatingKeyStr)) {
					String lFBRgaId = pvToolkit.getStatusChildTagAttribute(lFeedbackRatingTag, CRunPVToolkit._feedbackRatingRgaIdStr);
					if(lStuRgaId.equals(lFBRgaId)) {
						String lFBRating = lPeerRga.getRgaId() + "," + pvToolkit.getStatusChildTagAttribute(lFeedbackRatingTag, CRunPVToolkit._feedbackRatingLevelStr);
						while (lFBRatings.size() <= lInd) {
							lFBRatings.add(new ArrayList<>());
						}
						lFBRatings.get(lInd).add(lFBRating);
					}
				}
				lInd++;
			}
		}
		
		int lCycleNumber = 1;
		for (List<String> lCycleRatings : lFBRatings) {
			int lTotal = 0;
			int lNumber = 0;
			for (String lRating : lCycleRatings) {
				if (!StringUtils.isEmpty(lRating)) {
					lNumber++;
					String[] lFBArr = lRating.split(",");
					researchResults.append("\t");
					researchResults.append(pvToolkit.getRunGroupAccount(lFBArr[0]).getEAccount().getUserid());
					researchResults.append("\t");
					researchResults.append(lFBArr[1]);
					lTotal += Integer.parseInt(lFBArr[1]);
				}
			}
			//NOTE make sure cycle average is rendered in same column for all students, assuming no more than 5 peers
			for (int i = lNumber; i < 6; i++) {
				researchResults.append("\tNONE\t ");
			}
			researchResults.append("\t");
			researchResults.append("ronde " + lCycleNumber);
			researchResults.append("\t");
			if (lNumber > 0)
				researchResults.append(String.format("%1.2f", (float) lTotal/lNumber));
			else
				researchResults.append("0");
			lCycleNumber++;
		}
		
		researchResults.append("\n");
	}
	
}
