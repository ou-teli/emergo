/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Div;
import org.zkoss.zul.Image;
import org.zkoss.zul.Include;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CIObserved;
import nl.surf.emergo.control.CImportExportHelper;
import nl.surf.emergo.control.def.CDefHtmlMacroComponent;
import nl.surf.emergo.control.def.CDefInclude;
import nl.surf.emergo.control.def.CDefMacro;
import nl.surf.emergo.control.run.CRunBackgroundImage;
import nl.surf.emergo.control.run.CRunHoverBtn;
import nl.surf.emergo.control.run.CRunLocationArea;
import nl.surf.emergo.control.run.CRunLocationReference;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.utilities.SimpleImageInfo;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpring;
import nl.surf.emergo.zkspring.STriggeredReference;

/**
 * The Class CRunLocationAreaOunl. Used to show a location.
 * Notifies Emergo player when image is clicked.
 */
public class CRunLocationAreaOunl extends CRunLocationArea {

	private static final long serialVersionUID = 2326667538567917039L;

	/** The run parallax. Used for parallax effect. */
	protected boolean parallaxEffect = false;

	/** The run location objects. Used for show objects on location. */
	protected Div runLocationObjects = null;

	/** The run parallax. Used for parallax effect. */
	protected Div runParallax = null;

	protected String styleCursorMove = "cursor:move;";

	protected int locationObjectLeftCorrection = 0;

	protected int locationObjectTopCorrection = 0;
	
	protected int locationObjectWidthCorrection = 0;
	
	protected int locationObjectHeightCorrection = 0;
	
	protected CRunHoverBtn selectedHoverBtn = null;
	
	protected List<Component> runWndComponents = new ArrayList<Component>();

	/**
	 * Tags without position are placed on the screen using a lath.
	 * The order is determined by the order made by the case developer (default)
	 * and by the time stamp of the present=true script action. So you get an
	 * order from old to new.  
	 */
	protected List<IXMLTag> tagsWithoutPosition = new ArrayList<IXMLTag>();
	
	protected Hashtable<String,String> tagsWithoutPositionLeft = new Hashtable<String,String>();

	protected Hashtable<String,String> tagsWithoutPositionTop = new Hashtable<String,String>();

	protected String groupOffsetXAttr = "groupOffsetX";

	protected String groupOffsetYAttr = "groupOffsetY";

	/**
	 * Instantiates a new c run location area.
	 */
	public CRunLocationAreaOunl() {
		super("runLocationArea");
	}

	/**
	 * Instantiates a new c run location area.
	 *
	 * @param aId the a id
	 */
	public CRunLocationAreaOunl(String aId) {
		super(aId);
	}

	/**
	 * Init
	 */
	public void init() {
		// adds background image
		super.init();
		// set background not clickable
		runLocationImage.setZclass(runLocationImage.getClassName() + "_inactive");
	}

	/**
	 * Init locaton object corrections
	 */
	public void initLocationObjectCorrections() {
		// NOTE image isn't shown at 0,0 position within button but on rel position 6,2
		locationObjectLeftCorrection = 0;
		locationObjectTopCorrection = 0;
		if (parallaxEffect && !Executions.getCurrent().getBrowser().equals("ie")) {
			// NOTE this is needed to get effect running both on IE other other browsers
//			locationObjectTopCorrection = -374;
		}
		// NOTE correct width and height accordingly
		locationObjectWidthCorrection = -locationObjectLeftCorrection;
		locationObjectHeightCorrection = -locationObjectTopCorrection;
		locationObjectHeightCorrection = 0;
	}

	/**
	 * Gets the navigation component. There is only only one within a case.
	 *
	 * @return the navigation component
	 */
	protected IECaseComponent getNavigationComponent() {
		return ((CRunWndOunl)runWnd).getNavigationComponent();
	}

	/**
	 * Shows location.
	 *
	 * @param aStatus the status
	 */
	public void showLocation(Object aStatus) {
		// remove old location objects within run location area
		List<Component> components = getChildren();
		for (int i=components.size()-1;i>=0;i--) {
			Component component = (Component)components.get(i);
			if (!(component instanceof CRunBackgroundImage)) {
				component.detach();
			}
		}
		// remove old location objects within run wnd area
		components = runWndComponents;
		for (int i=components.size()-1;i>=0;i--) {
			Component component = (Component)components.get(i);
			component.detach();
		}
		// and clear run wnd components
		runWndComponents.clear();
		
		// reset tags without position
		tagsWithoutPosition = new ArrayList<IXMLTag>();
		tagsWithoutPositionLeft = new Hashtable<String,String>();
		tagsWithoutPositionTop = new Hashtable<String,String>();

		// maybe center image if image is to big, or better in parent object?
		// render passages and objects on image
		IXMLTag locationTag = (IXMLTag)aStatus;
		if (locationTag == null)
			return;

		// determine content tag for objects that are visible on every location
		IXMLTag contentTag = null;
		IXMLTag parentTag = locationTag.getParentTag();
		while (parentTag != null) {
			if (parentTag.getName().equals(AppConstants.contentElement)) {
				contentTag = parentTag;
				break;
			}
			else {
				parentTag = parentTag.getParentTag();
			}
		}
		// get current background tag for objects only visible on current location
		IXMLTag backgroundTag = CDesktopComponents.sSpring().getSBlobHelper().getBackgroundTag(locationTag);

		String backgroundWidth = "";
		if (backgroundTag != null) {
			parallaxEffect = backgroundTag.getChildValue("parallax").equals(AppConstants.statusValueTrue);
			backgroundWidth = backgroundTag.getChildValue("parallaxwidth");
		}
		if (backgroundWidth.equals("")) {
			backgroundWidth = "" + CDesktopComponents.sSpring().getSCaseSkinHelper().getCaseSkinWidth(getNavigationComponent().getECase());
		}

		if (runParallax == null) {
			runParallax = (Div)CDesktopComponents.vView().getComponent("runParallax");
		}
		if (runLocationObjects == null) {
			runLocationObjects = (Div)CDesktopComponents.vView().getComponent("runLocationObjects");
		}
		if (!parallaxEffect) {
			super.showLocation(aStatus);
			if (runLocationObjects != null) {
				runLocationObjects.getChildren().clear();
				runLocationObjects.setVisible(true);
			}
			if (runParallax != null) {
				runParallax.getChildren().clear();
				runParallax.setVisible(false);
			}
		}
		else {
			runLocationImage.setVisible(false);
			if (runLocationObjects != null) {
				runLocationObjects.getChildren().clear();
				runLocationObjects.setVisible(false);
			}
			if (runParallax != null) {
				// first clear src otherwise zul file isn't reloaded
				runParallax.getChildren().clear();
			  	if (StringUtils.isEmpty(backgroundWidth)) {
			  		// if no width specified, set it larger then the player width so the parallax effect will be visible
			  		backgroundWidth = "2340";
			  	}

			  	HtmlMacroComponent macro = new CDefMacro();
			  	runParallax.appendChild(macro);
				Map<String,Object> propertyMap = new HashMap<String,Object>();
				propertyMap.put("currentEmergoComponent", this);
				propertyMap.put("background_width", backgroundWidth);
				macro.setDynamicProperty("a_propertyMap", propertyMap);
				macro.setMacroURI(VView.v_run_parallax_fr);
				runParallax.setVisible(true);
			}

			for (int j=1;j<=7;j++) {
				Div runLocationLayer = (Div)CDesktopComponents.vView().getComponent("runLocationLayer" + j);
				if (runLocationLayer != null) {
					components = runLocationLayer.getChildren();
					for (int i=components.size()-1;i>=0;i--) {
//						Component component = (Component)components.get(i);
//						component.detach();
					}
				}
			}

			Div runLocationBackground = (Div)CDesktopComponents.vView().getComponent("runLocationBackground");
			if (runLocationBackground != null) {
				runLocationBackground.setStyle("no-repeat 0px 0px;width:" + backgroundWidth + "px;");
			}

			Image runLocationBackgroundImage = (Image)CDesktopComponents.vView().getComponent("runLocationBackgroundImage");
			if (runLocationBackgroundImage != null) {
				runLocationBackgroundImage.setSrc(getBackgroundSrc(backgroundTag));
			}
		}

		initLocationObjectCorrections();

		List<IXMLTag> childs = new ArrayList<IXMLTag>();
		// first add childs of content tag that should be present on every page
		if (contentTag != null) {
			childs.addAll(contentTag.getChildTags(AppConstants.defValueNode));
		}
		// then add childs of background tag
		if (backgroundTag != null) {
			childs.addAll(backgroundTag.getChildTags(AppConstants.defValueNode));
		}
		if (childs.size() == 0) {
			return;
		}

		List<IXMLTag> lGroups = new ArrayList<IXMLTag>();
		// render objects on top of background
		// even look at layer of objects, so first layer 1 etc.
		for (int i=1;i<=4;i++) {
			for (IXMLTag child : childs) {
				if (child.getName().equals("passage")) {
					if ((((child.getChildValue("layer") == null) || child.getChildValue("layer").equals("")) && (i == 1)) ||
							(child.getChildValue("layer").equals("" + i)))
						renderPassage(child, i, lGroups);
				}
				else if (child.getName().equals("object") && child.getChildValue("layer").equals("" + i)) {
					renderObject(child, i, lGroups);
				}
				else if (child.getName().equals("clickableobject") && child.getChildValue("layer").equals("" + i)) {
					renderClickableObject(child, i, lGroups);
				}
				else if (child.getName().equals("panel") && child.getChildValue("layer").equals("" + i)) {
					renderPanel(child, i, lGroups);
				}
				else if (child.getName().equals("plugin") && child.getChildValue("layer").equals("" + i)) {
					renderPlugin(child, i, lGroups);
				}
				else if (child.getName().equals("piece") && child.getChildValue("layer").equals("" + i)) {
					renderPiece(child, i, lGroups);
				}
				else if (child.getName().equals("button") && child.getChildValue("layer").equals("" + i)) {
					renderButton(child, i, lGroups);
				}
				else if (child.getName().equals("macro") && child.getChildValue("layer").equals("" + i)) {
					renderMacro(child, i, lGroups);
				}
				else if (child.getName().equals("group") && (i == 1)) {
					renderGroup(child, lGroups, 0, 0);
				}
			}
		}

		handleTagsWithoutPosition();
	}

	/**
	 * Handles tags without position, by setting them using a lath.
	 */
	public void handleTagsWithoutPosition() {
		List<IXMLTag> lSortedTagsWithoutPosition = getSortedTagsWithoutPosition();
		if (lSortedTagsWithoutPosition.size() == 0) {
			return;
		}
		//NOTE background tag is parent of passages etc.
		IXMLTag lBackgroundTag = lSortedTagsWithoutPosition.get(0).getParentTag();
		if (lBackgroundTag == null) {
			return;
		}
		int lMargin = 20;
		int lLeft = 0;
		String lLeftStr = getBackgroundObjectTagChildValue(lBackgroundTag, "objectsareaposition", 0);
		if (!lLeftStr.equals("")) {
			lLeft = Integer.parseInt(lLeftStr);
		}
		int lTop = 40;
		String lTopStr = getBackgroundObjectTagChildValue(lBackgroundTag, "objectsareaposition", 1);
		if (!lTopStr.equals("")) {
			lTop = Integer.parseInt(lTopStr);
		}
		int lMaxWidth = CDesktopComponents.sSpring().getSCaseSkinHelper().getCaseSkinWidth(getNavigationComponent().getECase());
		String lWidthStr = getBackgroundObjectTagChildValue(lBackgroundTag, "objectsareawidth", 0);
		if (!lWidthStr.equals("")) {
			lMaxWidth = Integer.parseInt(lWidthStr);
		}
		int lLeftPos = lLeft;
		int lTopPos = lTop;
		int lMaxHeight = 0;
		for (IXMLTag lTagWithoutPosition : lSortedTagsWithoutPosition) {
			String lId = lTagWithoutPosition.getAttribute(AppConstants.keyId);
			String lBtnId = "location" + lId + "Object";
			HtmlBasedComponent lLocationObject = (HtmlBasedComponent)CDesktopComponents.vView().getComponent(lBtnId);
			if (lLocationObject != null) {
				String lSizeWidth = getLocationObjectTagChildValue(lTagWithoutPosition, "size", 0, locationObjectWidthCorrection);
				String lSizeHeight = getLocationObjectTagChildValue(lTagWithoutPosition, "size", 1, locationObjectHeightCorrection);
				int lWidth = 0;
				int lHeight = 0;
				if (!lSizeWidth.equals("")) {
					lWidth = Integer.parseInt(lSizeWidth);
				}
				if (!lSizeHeight.equals("")) {
					lHeight = Integer.parseInt(lSizeHeight);
				}
				if (lHeight > lMaxHeight) {
					lMaxHeight = lHeight;
				}
				getStyleAttributes(lLocationObject).add(new String[]{"left", "" + lLeftPos + "px"});
				getStyleAttributes(lLocationObject).add(new String[]{"top", "" + lTopPos + "px"});
				lLocationObject.setStyle(generateStyle(lLocationObject));
				
				tagsWithoutPositionLeft.put(lId, "" + lLeftPos);
				tagsWithoutPositionTop.put(lId, "" + lTopPos);
				if (!lSizeWidth.equals("")) {
					if (lLeftPos + lWidth + lMargin <= lMaxWidth) {
						lLeftPos += lWidth + lMargin;
					}
					else {
						lLeftPos = lLeft;
						lTopPos += lMaxHeight + lMargin;
					}
				}
			}
		}
	}

	/**
	 * Get sorted tags without position. Order by present=true action.
	 *
	 * @return sorted tags without position
	 */
	public List<IXMLTag> getSortedTagsWithoutPosition() {
		List<IXMLTag> lXmlTags = new ArrayList<IXMLTag>();
	    TreeMap<Double,List<IXMLTag>> lSort = new TreeMap<Double,List<IXMLTag>>();
		for (IXMLTag lTagWithoutPosition : tagsWithoutPosition) {
			String lTimeStr = CDesktopComponents.sSpring().getCurrentRunTagStatusTime(getNavigationComponent(), lTagWithoutPosition, AppConstants.statusKeyPresent, AppConstants.statusTypeRunGroup);
			double lTime = -1.0;
			if (!lTimeStr.equals("")) {
				lTime = Double.parseDouble(lTimeStr);
			}
			if (!lSort.containsKey(lTime)) {
				lSort.put(lTime, new ArrayList<IXMLTag>());
			}
			lSort.get(lTime).add(lTagWithoutPosition);
		}
	    Iterator<Map.Entry<Double,List<IXMLTag>>> iterator = lSort.entrySet().iterator();
	    while(iterator.hasNext() )
	    {
	    	Map.Entry<Double,List<IXMLTag>> entry = iterator.next();
	      	List<IXMLTag> lXmltags = (List<IXMLTag>)entry.getValue();
	      	for (IXMLTag lXmltag : lXmltags) {
	      		lXmlTags.add(lXmltag);
	      	}
	    }
		return lXmlTags;
	}

	/**
	 * Get background src.
	 *
	 * @param aBackgroundTag the a background tag containing the blob id
	 */
	public String getBackgroundSrc(IXMLTag aBackgroundTag) {
		if (aBackgroundTag == null)
			return "";
		String lBlobId = aBackgroundTag.getChildValue("picture");
		String lBlobtype = aBackgroundTag.getChildAttribute("picture", AppConstants.keyBlobtype);
		String lSrc = "";
		if (lBlobtype.equals(AppConstants.blobtypeIntUrl)) {
			// url
			lSrc = CDesktopComponents.sSpring().getSBlobHelper().getUrl(aBackgroundTag);
		}
		else {
			// blob
			lSrc = CDesktopComponents.sSpring().getSBlobHelper().getImageSrc(lBlobId);
		}
		return lSrc;
	}

	/**
	 * Renders passage.
	 *
	 * @param aTag the tag
	 * @param layer the layer
	 * @param aGroups the list with group IXML tags
	 */
	public void renderPassage(IXMLTag aTag, int layer, List<IXMLTag> aGroups) {
		//determine tag ids of related locations and if these locations are possibly located in another navigation component
		String lCrossReferenceId = "locationtags_passagetag";
		String lTrigger = "" + AppConstants.statusKeySelectedIndex;
		IECaseRole lCaseRole = CDesktopComponents.sSpring().getSCaseRoleHelper().getCaseRole();
		IECaseComponent lCaseComponent = getNavigationComponent();
		//get tag ids coupled to passage, multiple ref ids are possible
		List<String> lRefIds = runWnd.cCaseHelper.getRefTagIds(lCrossReferenceId, lTrigger, lCaseRole, lCaseComponent, aTag);
		if (lRefIds.size() == 0)
			return;
		//determine tag ids and possible other cac id
		String lCarId = "" + lCaseRole.getCarId();
		String lCacId = "" + lCaseComponent.getCacId();
		String lTagIds = "";
		String lPossibleOtherCacId = "";
		for (String lRefId : lRefIds) {
			String[] lIds = lRefId.split(",");
			//if case role is identical
			if (lIds[0].equals(lCarId)) {
				if (lPossibleOtherCacId.equals("") && !lIds[1].equals(lCacId)) {
					//NOTE passage to locations in other navigation component
					lPossibleOtherCacId = lIds[1];
				}
				if (!lTagIds.equals("")) {
					lTagIds += ",";
				}
				lTagIds += lIds[2];
			}
		}
		//NOTE if passage to location in other navigation component, use CRunLocationReference
		CRunLocationReference lLocationReference = null;
		if (!lPossibleOtherCacId.equals("")) {
			lLocationReference = new CRunLocationReference();
			lLocationReference.setCacId(lPossibleOtherCacId);
			lLocationReference.setLocationIds(lTagIds);
		}

		//determine action
		String lAction = "";
		if (lRefIds.size() == 1) {
			lAction = "showLocation";
		}
		else {
			lAction = "showLocations";
		}

		//determine action status
		Object lActionStatus = null;
		if (lPossibleOtherCacId.equals("")) {
			lActionStatus = lTagIds;
		}
		else {
			lActionStatus = lLocationReference;
		}

		CRunHoverBtn lLocationObject = newHoverBtn(
				"location" + aTag.getAttribute(AppConstants.keyId) + "Object", 
				getTagStatus(aTag), 
				lAction, 
				lActionStatus, 
				"", 
				CDesktopComponents.sSpring().unescapeXML(aTag.getChildValue("name")), 
				aTag);
		lLocationObject.setAttribute("groups", aGroups);
		appendLocationObjectAsChild(aTag, lLocationObject, layer);
		lLocationObject.setCanHaveStatusSelected(false);
		setButtonLayout(lLocationObject, aTag);
		decorateLocationObject(lLocationObject, aTag);
	}

	/**
	 * Renders object.
	 *
	 * @param aTag the tag
	 * @param layer the layer
	 * @param aGroups the list with group IXML tags
	 */
	public void renderObject(IXMLTag aTag, int layer, List<IXMLTag> aGroups) {
		//NOTE for object a button is used as well
		CRunHoverBtn lLocationObject = newHoverBtn("location" + aTag.getAttribute(AppConstants.keyId) + "Object", 
				getTagStatus(aTag), 
				"", 
				"", 
				"", 
				CDesktopComponents.sSpring().unescapeXML(aTag.getChildValue("name")), 
				aTag);
		lLocationObject.setAttribute("groups", aGroups);
		appendLocationObjectAsChild(aTag, lLocationObject, layer);
		setButtonLayout(lLocationObject, aTag);
		decorateLocationObject(lLocationObject, aTag);
	}

	/**
	 * Renders clickable object.
	 *
	 * @param aTag the tag
	 * @param layer the layer
	 * @param aGroups the list with group IXML tags
	 */
	public void renderClickableObject(IXMLTag aTag, int layer, List<IXMLTag> aGroups) {
		//determine possible cac id and tag id of related case component and tag
		String lCrossReferenceId = "tag_clickableobjecttag";
		String lTrigger = "" + AppConstants.statusKeySelectedIndex;
		IECaseRole lCaseRole = CDesktopComponents.sSpring().getSCaseRoleHelper().getCaseRole();
		IECaseComponent lCaseComponent = getNavigationComponent();
		//get ref ids coupled to clickable object, only one ref id is possible
		List<String> lRefIds = runWnd.cCaseHelper.getRefTagIds(lCrossReferenceId, lTrigger, lCaseRole, lCaseComponent, aTag);
		//determine cac id and tag id
		String lCarId = "" + lCaseRole.getCarId();
		String lCacId = "0";
		String lTagId = "0";
		for (String lRefId : lRefIds) {
			String[] lIds = lRefId.split(",");
			//if case role is identical
			if (lIds[0].equals(lCarId)) {
				lCacId = lIds[1];
				lTagId = lIds[2];
				//if item found
				break;
			}
		}
		
		//determine action
		String lAction = "";
		if (lCacId.equals("") || lCacId.equals("0")) {
			//no related case component or tag. Handling of clicking (selecting) clickable object is managed within script
			lAction = "";
		}
		else if (isTabletAppComponent(lCacId)) {
			//if the case component is a tablet app
			lAction = "showTabletApp";
		}
		else {
			// app
			lAction = "showApp";
		}
		
		//determine action status
		List<String> lActionStatus = new ArrayList<String>();
		lActionStatus.add(lCacId);
		lActionStatus.add(lTagId);
		
		CRunHoverBtn lLocationObject = newHoverBtn(
				"location" + aTag.getAttribute(AppConstants.keyId) + "Object",
				getTagStatus(aTag), 
				lAction, 
				lActionStatus, 
				"", 
				CDesktopComponents.sSpring().unescapeXML(aTag.getChildValue("name")), 
				aTag);
		lLocationObject.setAttribute("groups", aGroups);
		appendLocationObjectAsChild(aTag, lLocationObject, layer);
		lLocationObject.setCanHaveStatusSelected(!runWnd.getTagSelectedImageSrc(aTag).equals(""));
		setButtonLayout(lLocationObject, aTag);
		decorateLocationObject(lLocationObject, aTag);
		
		handleClickableObjectOnClick(lLocationObject, aTag);
	}

	/**
	 * Handles clickable object onclick behavior.
	 *
	 * If there is a reference to a resource added to the clickable object, show the resource in a popup window when the object is clicked.
	 *
	 * @param aPiece the piece
	 * @param aTag the tag
	 */
	protected void handleClickableObjectOnClick(CRunHoverBtn aClickableObject, IXMLTag aTag) {
		IXMLTag lBlobChild = aTag.getChild("blob");
		//NOTE only render widgetListener if blob resource is added, because else the active or hover image of the object itself will be shown in a popup window.
		if ((lBlobChild != null) && (!StringUtils.isEmpty(lBlobChild.getValue()))) {
			//NOTE 'blob' is the first type of child that will be searched for.
			//To be future-proof we better only check on blob childs.
			IXMLTag lResTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag("ClickableObject", "");
			lResTag.getChildTags().add(lBlobChild);
			handlePieceOnClick(aClickableObject, lResTag);
		}
	}

	/**
	 * Is a component a tablet component?
	 *
	 * @param aCacId the a cac id
	 * 
	 * @return boolean
	 */
	public boolean isTabletAppComponent(String aCacId) {
		return (((CRunWndOunl)runWnd).isTabletAppComponent(aCacId));
	}

	/**
	 * Renders panel.
	 *
	 * @param aTag the tag
	 * @param layer the layer
	 * @param aGroups the list with group IXML tags
	 */
	public void renderPanel(IXMLTag aTag, int layer, List<IXMLTag> aGroups) {
		//NOTE include may exist already
		Include lLocationObject = (Include)CDesktopComponents.vView().getComponent("location" + aTag.getAttribute(AppConstants.keyId) + "Object");
		if (lLocationObject == null) {
			lLocationObject = new CDefInclude();
			lLocationObject.setAttribute("groups", aGroups);
			appendLocationObjectAsChild(aTag, lLocationObject, layer);
			lLocationObject.setId("location" + aTag.getAttribute(AppConstants.keyId) + "Object");
		}
		setPanelLayout(lLocationObject, aTag);
		decorateLocationObject(lLocationObject, aTag);
	}

	/**
	 * Renders plugin.
	 *
	 * @param aTag the tag
	 * @param layer the layer
	 * @param aGroups the list with group IXML tags
	 */
	public void renderPlugin(IXMLTag aTag, int layer, List<IXMLTag> aGroups) {
		//NOTE include may exist already
		Include lLocationObject = (Include)CDesktopComponents.vView().getComponent("location" + aTag.getAttribute(AppConstants.keyId) + "Object");
		if (lLocationObject == null) {
			lLocationObject = new CDefInclude();
			lLocationObject.setAttribute("groups", aGroups);
			appendLocationObjectAsChild(aTag, lLocationObject, layer);
			lLocationObject.setId("location" + aTag.getAttribute(AppConstants.keyId) + "Object");
		}
		setPluginLayout(lLocationObject, aTag, false);
		decorateLocationObject(lLocationObject, aTag);
	}


	/**
	 * Renders piece.
	 *
	 * @param aTag the tag
	 * @param layer the layer
	 * @param aGroups the list with group IXML tags
	 */
	public void renderPiece(IXMLTag aTag, int layer, List<IXMLTag> aGroups) {
		//NOTE for piece a button is used as well
		CRunHoverBtn lLocationObject = newHoverBtn("location" + aTag.getAttribute(AppConstants.keyId) + "Object", 
				getTagStatus(aTag), 
				"", 
				"", 
				"", 
				CDesktopComponents.sSpring().unescapeXML(aTag.getChildValue("name")), 
				aTag);
		lLocationObject.setAttribute("groups", aGroups);
		appendLocationObjectAsChild(aTag, lLocationObject, layer);
		setButtonLayout(lLocationObject, aTag);
		lLocationObject.setCanHaveStatusSelected(false);
		decorateLocationObject(lLocationObject, aTag);

		
		handlePieceOnClick(lLocationObject, aTag);
	}

	/**
	 * Handles piece onclick behavior.
	 *
	 * @param aPiece the piece
	 * @param aTag the tag
	 */
	protected void handlePieceOnClick(CRunHoverBtn aPiece, IXMLTag aTag) {
		String lUrl = CDesktopComponents.sSpring().getSBlobHelper().convertHrefForCertainMediaTypes(CDesktopComponents.sSpring().getSBlobHelper().getUrl(aTag));
		if (lUrl.length() > 0) {
			if (!CDesktopComponents.vView().isAbsoluteUrl(lUrl)) {
				lUrl = CDesktopComponents.vView().getAbsoluteUrl(lUrl);
			}
			if (lUrl.contains(".zul")) {
				aPiece.setWidgetListener("onClick", CDesktopComponents.vView().getJavascriptWindowOpenFullscreen(lUrl));
			} 
			else {
				aPiece.setWidgetListener("onClick", CDesktopComponents.vView().getJavascriptWindowLocationHref(lUrl));
			}
		}
	}

	/**
	 * Renders macro.
	 *
	 * @param aTag the tag
	 * @param layer the layer
	 * @param aGroups the list with group IXML tags
	 */
	public void renderMacro(IXMLTag aTag, int layer, List<IXMLTag> aGroups) {
		//NOTE macro may exist already
		HtmlMacroComponent lLocationObject = (HtmlMacroComponent)CDesktopComponents.vView().getComponent("location" + aTag.getAttribute(AppConstants.keyId) + "Object");
		if (lLocationObject == null) {
			lLocationObject = new CDefHtmlMacroComponent();
			lLocationObject.setAttribute("groups", aGroups);
			appendLocationObjectAsChild(aTag, lLocationObject, layer);
			lLocationObject.setId("location" + aTag.getAttribute(AppConstants.keyId) + "Object");
		}
		setMacroLayout(lLocationObject, aTag, false);
		decorateLocationObject(lLocationObject, aTag);
	}


	/**
	 * Renders button.
	 *
	 * @param aTag the tag
	 * @param layer the layer
	 * @param aGroups the list with group IXML tags
	 */
	public void renderButton(IXMLTag aTag, int layer, List<IXMLTag> aGroups) {
		HtmlMacroComponent lLocationObject = new HtmlMacroComponent();
		lLocationObject.setAttribute("groups", aGroups);
		appendLocationObjectAsChild(aTag, lLocationObject, layer);
		lLocationObject.setId("location" + aTag.getAttribute(AppConstants.keyId) + "Object");
		lLocationObject.setMacroURI("../run_input_labelbtn_macro.zul");
		lLocationObject.setDynamicProperty("a_observer", this.getId());
		lLocationObject.setDynamicProperty("a_event", "onButtonClicked");
		lLocationObject.setDynamicProperty("a_eventdata", aTag);
		lLocationObject.setDynamicProperty("a_label", CDesktopComponents.sSpring().unescapeXML(aTag.getChildValue("text")));
		if (aTag.getChildValue("textorientation").equals("vertical")) {
			lLocationObject.setDynamicProperty("a_classtype", "DarkGreenVertical");
		}
		else {
			lLocationObject.setDynamicProperty("a_classtype", "DarkGreen");
		}
		lLocationObject.setDynamicProperty("a_textstyle", aTag.getChildValue("textstyle"));
		//NOTE recreate macro within method setLabelButtonLayout, so the button will get a onCreate message that is used to set the width of the button within run_input_labelbtn_macro.zul
		setLabelButtonLayout(lLocationObject, aTag);
		decorateLocationObject(lLocationObject, aTag);
	}

	/**
	 * Renders piece.
	 *
	 * @param aTag the tag
	 * @param aParentGroups the list with parent group IXML tags
	 */
	public void renderGroup(IXMLTag aTag, List<IXMLTag> aParentGroups, int aOffsetX, int aOffsetY) {
		List<IXMLTag> childs = new ArrayList<IXMLTag>();
		childs.addAll(aTag.getChildTags(AppConstants.defValueNode));
		if (childs.size() == 0) {
			return;
		}
		List<IXMLTag> lGroups = new ArrayList<IXMLTag>();
		if (aParentGroups.size() > 0)
			lGroups.addAll(aParentGroups);
		lGroups.add(aTag);
		int lOffsetX = getGroupObjectTagChildValueInt(aTag, "position", 0, aOffsetX);
		int lOffsetY = getGroupObjectTagChildValueInt(aTag, "position", 1, aOffsetY);
		for (int i=1;i<=4;i++) {
			for (IXMLTag child : childs) {
				child.setAttribute(groupOffsetXAttr, "" + lOffsetX);
				child.setAttribute(groupOffsetYAttr, "" + lOffsetY);
				if (child.getName().equals("passage")) {
					if ((((child.getChildValue("layer") == null) || child.getChildValue("layer").equals("")) && (i == 1)) ||
							(child.getChildValue("layer").equals("" + i)))
						renderPassage(child, i, lGroups);
				}
				else if (child.getName().equals("object") && child.getChildValue("layer").equals("" + i)) {
					renderObject(child, i, lGroups);
				}
				else if (child.getName().equals("clickableobject") && child.getChildValue("layer").equals("" + i)) {
					renderClickableObject(child, i, lGroups);
				}
				else if (child.getName().equals("panel") && child.getChildValue("layer").equals("" + i)) {
					renderPanel(child, i, lGroups);
				}
				else if (child.getName().equals("plugin") && child.getChildValue("layer").equals("" + i)) {
					renderPlugin(child, i, lGroups);
				}
				else if (child.getName().equals("piece") && child.getChildValue("layer").equals("" + i)) {
					renderPiece(child, i, lGroups);
				}
				else if (child.getName().equals("button") && child.getChildValue("layer").equals("" + i)) {
					renderButton(child, i, lGroups);
				}
				else if (child.getName().equals("macro") && child.getChildValue("layer").equals("" + i)) {
					renderMacro(child, i, lGroups);
				}
				else if (child.getName().equals("group") && (i == 1)) {
					renderGroup(child, lGroups, lOffsetX, lOffsetY);
				}
			}
		}
	}

	/**
	 * Called when button is clicked.
	 */
	public void onButtonClicked(Event aEvent) {
		setRunTagStatus(getNavigationComponent(), (IXMLTag)aEvent.getData(), AppConstants.statusKeySelected, AppConstants.statusValueTrue, true);
	}

	/**
	 * Appends location object to right parent.
	 *
	 * @param aTag the a tag
	 * @param aLocationObject the a location object
	 * @param aLayer the a layer
	 */
	protected void appendLocationObjectAsChild(IXMLTag aTag, HtmlBasedComponent aLocationObject, int aLayer) {
		if (!aTag.getDefChildValue("z-index").equals("")) {
			//NOTE if z-index is set, append location object to runWnd so it can be positioned in every layer you want 
			runWnd.appendChild(aLocationObject);
			//NOTE add the location object to the list of runWnd components, so it will be removed if another location is opened. So it will still act as being coupled to one location.
			runWndComponents.add(aLocationObject);
			return;
		}
		if (!parallaxEffect) {
			//NOTE if no parallax effect append location object to runLocationObjects
			if (runLocationObjects != null) {
				runLocationObjects.appendChild(aLocationObject);
			}
		}
		else {
			//NOTE if parallax effect append location object to right layer
			String lLayerId = "runLocationLayer";
			if (aLayer == 1) lLayerId += "1";
			else if (aLayer == 2) lLayerId += "4";
			else if (aLayer == 3) lLayerId += "7";
			else if (aLayer == 4) lLayerId = "runLocationForeground";

			Div lRunLocationLayer = (Div)CDesktopComponents.vView().getComponent(lLayerId);
			if (lRunLocationLayer != null) {
				lRunLocationLayer.appendChild(aLocationObject);
			}
		}
	}

	/**
	 * Get location object style: left, top, width, height.
	 *
	 * @param tag the tag
	 * @param aTagChildName name of tag containing position or size
	 * @param index 0 or 1
	 * @param correction
	 * 
	 * @return string
	 */
	protected String getLocationObjectTagChildValue(IXMLTag tag, String aTagChildName, int index, int correction) {
		if (tag == null) {
			return "";
		}
		String values = tag.getChildValue(aTagChildName);
		if (values.equals("") && aTagChildName.equals("size")) {
			//NOTE if no size entered, get size from picture
			try {
				File lFile = CDesktopComponents.sSpring().getSBlobHelper().getBlobFile(tag.getChildValue("pictureactive"));
				if (lFile != null) {
					SimpleImageInfo lSimpleImageInfo = new SimpleImageInfo(lFile);
					values = "" + lSimpleImageInfo.getWidth() + "," + lSimpleImageInfo.getHeight();
				}
			} catch (IOException e) {
			}
		}
		if (values.equals("")) {
			return "";
		}
		int value = 0;
		String[] arr = values.split(",");
		if (index >= arr.length) {
			return "";
		}
		try {
			value = Integer.parseInt(arr[index]);
		} catch (NumberFormatException e) {
			return "";
		}
		value += correction;
		return "" + value;
	}

	/**
	 * Get group object style: left, top
	 *
	 * @param tag the tag
	 * @param aTagChildName name of tag containing position
	 * @param index 0 or 1
	 * @param correction
	 * 
	 * @return int, 0 if child not present
	 */
	protected int getGroupObjectTagChildValueInt(IXMLTag tag, String aTagChildName, int index, int correction) {
		if (tag == null) {
			return correction;
		}
		String values = tag.getChildValue(aTagChildName);
		if (values.equals("")) {
			return correction;
		}
		int value = 0;
		String[] arr = values.split(",");
		if (index >= arr.length) {
			return correction;
		}
		try {
			value = Integer.parseInt(arr[index]);
		} catch (NumberFormatException e) {
			return correction;
		}
		value += correction;
		return value;
	}

	/**
	 * Get background object style: left, top, width.
	 *
	 * @param tag the tag
	 * @param aTagChildName name of tag containing position or width
	 * @param index 0 or 1
	 * @param correction
	 * 
	 * @return string
	 */
	protected String getBackgroundObjectTagChildValue(IXMLTag tag, String aTagChildName, int index) {
		if (tag == null) {
			return "";
		}
		String values = tag.getChildValue(aTagChildName);
		if (values.equals("")) {
			return "";
		}
		int value = 0;
		String[] arr = values.split(",");
		if (index >= arr.length) {
			return "";
		}
		try {
			value = Integer.parseInt(arr[index]);
		} catch (NumberFormatException e) {
			return "";
		}
		return "" + value;
	}

	/**
	 * Get location object style: left, top, width, height and cursor.
	 *
	 * @param tag the tag
	 * @param aTagChildName name of tag containing position or size
	 * @param index 0 or 1
	 * @param correction
	 * 
	 * @return string
	 */
	protected String getLocationObjectTagChildValuePx(IXMLTag tag, String aTagChildName, int index, int correction) {
		String value = getLocationObjectTagChildValue(tag, aTagChildName, index, correction);
		if (!value.equals("")) {
			value += "px";
		}
		return value;
	}

	/**
	 * Get style attributes of location object.
	 *
	 * @param aLocationObject the location object
	 * 
	 * @return style attributes
	 */
	public List<String[]> getStyleAttributes(HtmlBasedComponent aLocationObject) {
		//NOTE List is used so the order of style attributes can be arranged
		List<String[]> lStyleAttributes = (List<String[]>)aLocationObject.getAttribute("styleAttributes");
		if (lStyleAttributes == null) {
			lStyleAttributes = new ArrayList<String[]>();
			aLocationObject.setAttribute("styleAttributes", lStyleAttributes);
		}
		return lStyleAttributes;
	}

	/**
	 * Generates style of location object out of the style attributes.
	 *
	 * @param aLocationObject the location object
	 * 
	 * @return style
	 */
	public String generateStyle(HtmlBasedComponent aLocationObject) {
		String lStyle = "";
		for (String[] lEntry : getStyleAttributes(aLocationObject)) {
			//value must be not empty
			if (!StringUtils.isEmpty(lEntry[0]) && !StringUtils.isEmpty(lEntry[1])) {
				if (lEntry[0].equals("customstyle"))
					lStyle += lEntry[1];
				else
					lStyle += lEntry[0] + ":" + lEntry[1] + ";";
			}
		}
		return lStyle;
	}

	/**
	 * Set common location object style attributes: z-index, left, top, width, height and cursor.
	 *
	 * @param aLocationObject the location object
	 * @param aTag the tag
	 */
	public void setLocationObjectCommonStyleAttributes(HtmlBasedComponent aLocationObject, IXMLTag aTag) {
		boolean lAccessible = CDesktopComponents.sSpring().getCurrentTagStatus(aTag, AppConstants.statusKeyAccessible).equals(AppConstants.statusValueTrue);

		//z-index
		getStyleAttributes(aLocationObject).add(new String[]{"z-index", aTag.getDefChildValue("z-index")});

		//position and size
		String lOffsetX = aTag.getAttribute(groupOffsetXAttr);
		int lOffsX = 0;
		try {
			lOffsX = Integer.parseInt(lOffsetX) + locationObjectLeftCorrection;
		} catch (NumberFormatException e) {
			lOffsX = locationObjectLeftCorrection;
		}
		String lOffsetY = aTag.getAttribute(groupOffsetYAttr);
		int lOffsY = 0;
		try {
			lOffsY = Integer.parseInt(lOffsetY) + locationObjectTopCorrection;
		} catch (NumberFormatException e) {
			lOffsY = locationObjectTopCorrection;
		}
		String lPositionLeft = getLocationObjectTagChildValuePx(aTag, "position", 0, lOffsX);
		String lPositionTop = getLocationObjectTagChildValuePx(aTag, "position", 1, lOffsY);
		String lSizeWidth = getLocationObjectTagChildValuePx(aTag, "size", 0, locationObjectWidthCorrection);
		String lSizeHeight = getLocationObjectTagChildValuePx(aTag, "size", 1, locationObjectHeightCorrection);
		getStyleAttributes(aLocationObject).add(new String[]{"left", lPositionLeft});
		getStyleAttributes(aLocationObject).add(new String[]{"top", lPositionTop});
		getStyleAttributes(aLocationObject).add(new String[]{"width", lSizeWidth});
		getStyleAttributes(aLocationObject).add(new String[]{"height", lSizeHeight});

		//cursor
		String lCursor = "default";
		if (lAccessible && (aTag.getName().equals("passage") || aTag.getName().equals("piece") || aTag.getName().equals("button") || aTag.getName().equals("clickableobject"))) {
			//passages and clickable objects may be clicked if they are accessible
			lCursor = "pointer";
		}
		else if (parallaxEffect) {
			//if parallax effect the default cursor is move to show that by moving the mouse one sees the effect
			lCursor = "move";
		}
		getStyleAttributes(aLocationObject).add(new String[]{"cursor", lCursor});
		
		//tags without position
		if (tagsWithoutPosition.contains(aTag)) {
			//NOTE is the case if location object does not have a position filled in and if it is being re-rendered.
			String lId = aTag.getAttribute(AppConstants.keyId);
			if (tagsWithoutPositionLeft.containsKey(lId)) {
				getStyleAttributes(aLocationObject).add(new String[]{"left", tagsWithoutPositionLeft.get(lId) + "px"});
			}
			if (tagsWithoutPositionTop.containsKey(lId)) {
				getStyleAttributes(aLocationObject).add(new String[]{"top", tagsWithoutPositionTop.get(lId) + "px"});
			}
		}
		else if (lPositionLeft.equals("") && lPositionTop.equals("")) {
			//NOTE is the case if location object does not have a position filled in and if it is being rendered for the first time.
			//Position will be set in handleTagsWithoutPosition().
			tagsWithoutPosition.add(aTag);
		}
	}

	/**
	 * Get location object style: left, top, width, height and cursor.
	 *
	 * @param tag the tag
	 * 
	 * @return string
	 */
	protected String adjustLocationObjectStyle(String aStyle, IXMLTag aTag) {
		if (aStyle.contains("width:") && aStyle.contains("height:")) {
			return aStyle;
		}
		String lVideoSizeWidth = "";
		String lVideoSizeHeight = "";
		try {
			String lBlobId = aTag.getChildValue("pictureactive");
			byte[] lContent = CDesktopComponents.sSpring().getSBlobHelper().getBlobContent(lBlobId);
			if (lContent != null) {
				SimpleImageInfo lSimpleImageInfo = new SimpleImageInfo(lContent);
				lVideoSizeWidth = "" + lSimpleImageInfo.getWidth() + "px";
				lVideoSizeHeight = "" + lSimpleImageInfo.getHeight() + "px";
			}
		} catch (IOException e) {
		}
		if (!aStyle.contains("width:") && !lVideoSizeWidth.equals("")) {
			aStyle += "width:" + lVideoSizeWidth + ";";
		}
		if (!aStyle.contains("height:") && !lVideoSizeHeight.equals("")) {
			aStyle += "height:" + lVideoSizeHeight + ";";
		}
		return aStyle;
	}

	public String getTextPaddingForButtons() {
		return "0px 0px !important";
	}

	/**
	 * Sets button layout.
	 *
	 * @param aLocationObject the location object
	 * @param aTag the tag
	 */
	public void setButtonLayout(CRunHoverBtn aLocationObject, IXMLTag aTag) {
		boolean lPresent = true;
		List<IXMLTag> lGroups = (List<IXMLTag>)aLocationObject.getAttribute("groups");
		if (lGroups.size() > 0) {
			for (IXMLTag lGroup : lGroups) {
				lPresent = lPresent && !CDesktopComponents.sSpring().getCurrentTagStatus(lGroup, AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse);
			}
		}
		lPresent = lPresent && !CDesktopComponents.sSpring().getCurrentTagStatus(aTag, AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse);
		
		//NOTE necessary for else custom text style of initially not-present buttons will not be properly shown
		getStyleAttributes(aLocationObject).clear();
		
		boolean lAccessible = CDesktopComponents.sSpring().getCurrentTagStatus(aTag, AppConstants.statusKeyAccessible).equals(AppConstants.statusValueTrue);
		//change status of location object
		if (lAccessible) {
			if (!(aLocationObject.getStatus().equals("active") || aLocationObject.getStatus().equals("selected"))) {
				//if it was not active or selected (so it was empty or inactive) set it to active
				aLocationObject.setStatus("active");
			}
		}
		else {
			if (!aLocationObject.getStatus().equals("inactive")) {
				//if it was not inactive set it to inactive
				aLocationObject.setStatus("inactive");
			}
		}
		
		//NOTE why only object?
		if (aTag.getName().equals("object")) {
			aLocationObject.setOrient("vertical");
			aLocationObject.setDir("reverse");
		}
		
		//set common style attributes: z-index, left, top, width, height and cursor
		setLocationObjectCommonStyleAttributes(aLocationObject, aTag);

		
		//if button label set style attribute for it
		//TODO should probably be better managed through input element for location objects
		if (!StringUtils.isEmpty(aLocationObject.getLabel())) {
			getStyleAttributes(aLocationObject).add(new String[]{"padding", getTextPaddingForButtons()});
		}
		if (!StringUtils.isEmpty(aTag.getChildValue("textstyle"))) {
			getStyleAttributes(aLocationObject).add(new String[]{"customstyle", aTag.getChildValue("textstyle")});
		}

		//save style as hover style without background style attribute
		String hoverStyle = generateStyle(aLocationObject);
		
		//get image src
		String src = "";
		if (aLocationObject.getStatus().equals("selected")) {
			src = runWnd.getTagSelectedImageSrc(aTag);
		}
		else {
			if (!lAccessible && (aTag.getName().equals("passage") || aTag.getName().equals("clickableobject"))) {
				//NOTE inactive image only exists if location object can be in-accessible, which is the case for passage and clickable object 
				src = runWnd.getTagInactiveImageSrc(aTag);
			}
			else {
				//so in all other cases use active image
				src = runWnd.getTagActiveImageSrc(aTag);
			}
		}
		//set background style attribute if image
		if (!src.equals("")) {
			getStyleAttributes(aLocationObject).add(new String[]{"background", "url(" + src + ")"});
			getStyleAttributes(aLocationObject).add(new String[]{"background-repeat", "no-repeat"});
		}

		String style = generateStyle(aLocationObject);
		
		if (((aLocationObject.getAttribute("style") == null) || !(aLocationObject.getAttribute("style").equals(style)))) {
			//NOTE if style has changed, we need to clear style class to set customized text style (why??)
			if (aLocationObject.getAttribute("style") != null) {
				String lZClass = aLocationObject.getZclass();
				aLocationObject.setZclass("");
				aLocationObject.setStyle(style);
				aLocationObject.setZclass(lZClass);
			} else {
				aLocationObject.setStyle(style);
			}
			aLocationObject.setAttribute("style", style);
		}

		//NOTE first set style, then set present, for else text style of initially not-present objects will not be correctly shown when they become visible
		aLocationObject.setVisible(lPresent);
		if (!lPresent) {
			return;
		}
		
		//no hover effect if status is selected
		if (!aLocationObject.getStatus().equals("selected")) {
			//get hover image src
			String hoverSrc = runWnd.getTagHoverImageSrc(aTag);
			//set hover effect if hover image
			if (!hoverSrc.equals("")) {
				//set hover style
				hoverStyle += "background:url(" + hoverSrc + ");background-repeat:no-repeat;";
				//set hover effect on client
				aLocationObject.setWidgetListener("onMouseOver", "zk.Widget.$(this).setStyle('" + hoverStyle + "');");
				aLocationObject.setWidgetListener("onMouseOut", "zk.Widget.$(this).setStyle('" + style + "');");
			}
		}

		aLocationObject.setAttribute("casecomponent", getNavigationComponent());
		aLocationObject.setAttribute("tag", aTag);
	}

	/**
	 * Sets panel layout.
	 *
	 * @param aLocationObject the location object
	 * @param aTag the tag
	 */
	public void setPanelLayout(Include aLocationObject, IXMLTag aTag) {
		//NOTE problems with rendering if it is not yet present and is set present on the current location. Include src is set.
		//Using echoEvent it is fixed
		Events.echoEvent("onSetPanelLayout", this, new Object[] {aLocationObject, aTag});
	}

	/**
	 * Set panel layout.
	 *
	 * @param aEvent the event
	 */
	public void onSetPanelLayout(Event aEvent) {
		Include lLocationObject = (Include)((Object[])aEvent.getData())[0];
		IXMLTag lTag = (IXMLTag)((Object[])aEvent.getData())[1];

		boolean lPresent = true;
		List<IXMLTag> lGroups = (List<IXMLTag>)lLocationObject.getAttribute("groups");
		if (lGroups.size() > 0) {
			for (IXMLTag lGroup : lGroups) {
				lPresent = lPresent && !CDesktopComponents.sSpring().getCurrentTagStatus(lGroup, AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse);
			}
		}
		lPresent = lPresent && !CDesktopComponents.sSpring().getCurrentTagStatus(lTag, AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse);
		if (!lPresent) {
			lLocationObject.setVisible(false);
			//clear src to stop playing
			lLocationObject.setSrc(VView.v_run_empty_fr);
			return;
		}

		//get URL
		SSpring lSSpring = CDesktopComponents.sSpring();
		String lUrl = lSSpring.getSBlobHelper().getUrl(lTag);
		if (lUrl.equals("")) {
			return;
		}
		//if not absolute, make it absolute
		if (!CDesktopComponents.vView().isAbsoluteUrl(lUrl)) {
			lUrl = CDesktopComponents.vView().getEmergoWebappsRoot() + lUrl;
		}
		//convert to UTF URL
		lUrl = lSSpring.getSBlobHelper().toUTFUrl(lUrl);

		//set common style attributes: z-index, left, top, width, height and cursor
		setLocationObjectCommonStyleAttributes(lLocationObject, lTag);
		getStyleAttributes(lLocationObject).add(new String[]{"position", "absolute"});
		lLocationObject.setStyle(generateStyle(lLocationObject));

		//NOTE Set attributes to be used by player
		String lVideoSizeWidth = getLocationObjectTagChildValuePx(lTag, "size", 0, locationObjectWidthCorrection);
		String lVideoSizeHeight = getLocationObjectTagChildValuePx(lTag, "size", 1, locationObjectHeightCorrection);
		String lMuted = lTag.getChildValue("muted");
		String lRepeat = CDesktopComponents.sSpring().getCurrentTagStatus(lTag, AppConstants.statusKeyRepeat);
		lLocationObject.setAttribute("casecomponent", getNavigationComponent());
		lLocationObject.setAttribute("tag", lTag);
		lLocationObject.setAttribute("url", lUrl);
		lLocationObject.setAttribute("video_size_width", lVideoSizeWidth);
		lLocationObject.setAttribute("video_size_height", lVideoSizeHeight);
		lLocationObject.setAttribute("muted", lMuted);
		lLocationObject.setAttribute("repeat", lRepeat);
		lLocationObject.setSrc(VView.v_run_flash_panel_fr);
		lLocationObject.setVisible(true);
	}

	/**
	 * Sets plug-in layout.
	 *
	 * @param aLocationObject the location object
	 * @param aTag the tag
	 * @param aUpdate the update boolean; set when 'update' key is triggered (for macro or plugin)
	 */
	public void setPluginLayout(Include aLocationObject, IXMLTag aTag, boolean aUpdate) {
		//NOTE possibly problems with rendering if it is not yet present and is set present on the current location. Include src is set.
		//Using echoEvent it is fixed
		Events.echoEvent("onSetPluginLayout", this, new Object[] {aLocationObject, aTag, aUpdate});
	}

	/**
	 * Set plugin layout.
	 *
	 * @param aEvent the event
	 */
	public void onSetPluginLayout(Event aEvent) {
		Include lLocationObject = (Include)((Object[])aEvent.getData())[0];
		IXMLTag lTag = (IXMLTag)((Object[])aEvent.getData())[1];

		boolean lPresent = true;
		List<IXMLTag> lGroups = (List<IXMLTag>)lLocationObject.getAttribute("groups");
		if (lGroups.size() > 0) {
			for (IXMLTag lGroup : lGroups) {
				lPresent = lPresent && !CDesktopComponents.sSpring().getCurrentTagStatus(lGroup, AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse);
			}
		}
		lPresent = lPresent && !CDesktopComponents.sSpring().getCurrentTagStatus(lTag, AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse);
		if (!lPresent) {
			lLocationObject.setVisible(false);
			//clear src to stop plug-in
			lLocationObject.setSrc(VView.v_run_empty_fr);
			return;
		}

		//check if URL is ok and unpack the plug-in if needed 
		//get URL
		SSpring lSSpring = CDesktopComponents.sSpring();
		String lUrl = lSSpring.getSBlobHelper().getUrl(lTag);
		if (lUrl.equals("")) {
			return;
		}
		//the plug-in package file is always located in sub folder equal to the blob id so it should contain a / 
		int lPos = lUrl.lastIndexOf("/");
		if (lPos <= 0) {
			return;
		}
		//get package file
		String lPackageFile = lUrl.substring(lPos + 1);
		//get URL prefix
		String lUrlPrefix = lUrl.substring(0, lPos + 1);
		//the plug-in package file is always located in the blob folder so the URL prefix should contain a / 
		lPos = lUrlPrefix.substring(0, lUrlPrefix.length() - 2).lastIndexOf("/");
		if (lPos <= 0) {
			return;
		}
		String lBlobPath = lUrlPrefix.substring(lPos + 1);
		String lDiskPath = lSSpring.getAppManager().getAbsoluteBlobPath();
		//determine plugin main file, the file that should be shown within the plug-in after unpacking the package
		String lPluginMainFile = lTag.getChildValue("packagemainfile");
		if (lPluginMainFile.equals("")) {
			lPluginMainFile = VView.v_default_plugin_file;
		}
		//if plugin main file does not exist yet, the package should be unpacked. It is unpacked in the same folder as in which the package is located
		if (!lSSpring.getFileManager().fileExists(lDiskPath + lBlobPath + lPluginMainFile)) {
			//if plugin main file does not exist unpack package
			CImportExportHelper helper = new CImportExportHelper();
			helper.unpackageZipToFiles(lDiskPath + lBlobPath, new File(lDiskPath + lBlobPath + lPackageFile));
		}
		//if plugin main file does not exist, stop
		if (!lSSpring.getFileManager().fileExists(lDiskPath + lBlobPath + lPluginMainFile)) {
			return;
		}
		
		//enable loading the plugin files in this session. For security reasons it is prevented
		List<String> lPluginFiles = lSSpring.getFileManager().getFileNamesInPathSubTree(lDiskPath + lBlobPath);
		String lBlobId = Paths.get(lBlobPath).getName(0).toString();
		for (String lFile : lPluginFiles) {
			lSSpring.getSBlobHelper().setBlobInSessionVar(lFile, lBlobId);
		}
		
		//create URL to plug-in main file
		lUrl = lUrlPrefix + lPluginMainFile;
		//if not absolute, create relative URL
		if (!CDesktopComponents.vView().isAbsoluteUrl(lUrl)) {
			lUrl = CDesktopComponents.vView().getRelativeUrl(CDesktopComponents.vView().getEmergoRootUrl() + CDesktopComponents.vView().getEmergoWebappsRoot() + lUrl);
		}
		//still necessary?
		lPos = lUrl.lastIndexOf("/");
		if (lPos <= 0) {
			return;
		}
		
		lUrlPrefix = lUrl.substring(0, lPos + 1);
		lLocationObject.setAttribute(VView.parameterPluginPath, lUrlPrefix);

		//convert to UTF URL
		lUrl = lSSpring.getSBlobHelper().toUTFUrl(lUrl);

		//set common style attributes: z-index, left, top, width, height and cursor
		setLocationObjectCommonStyleAttributes(lLocationObject, lTag);
		getStyleAttributes(lLocationObject).add(new String[]{"position", "absolute"});
		lLocationObject.setStyle(generateStyle(lLocationObject));

		lLocationObject.setAttribute("casecomponent", getNavigationComponent());
		lLocationObject.setAttribute("tag", lTag);

		lLocationObject.setSrc(lUrl);
		lLocationObject.setVisible(true);
	}

	/**
	 * Sets macro layout.
	 *
	 * @param aLocationObject the location object
	 * @param aTag the tag
	 * @param aUpdate the update boolean; set when 'update' key is triggered (for macro or plugin)
	 */
	public void setMacroLayout(HtmlMacroComponent aLocationObject, IXMLTag aTag, boolean aUpdate) {
		//NOTE possibly problems with rendering if it is not yet present and is set present on the current location. Macro URI is set.
		//Using echoEvent it is fixed
		Events.echoEvent("onSetMacroLayout", this, new Object[] {aLocationObject, aTag, aUpdate});
	}

	/**
	 * Set macro layout.
	 *
	 * @param aEvent the event
	 */
	public void onSetMacroLayout(Event aEvent) {
		HtmlMacroComponent lLocationObject = (HtmlMacroComponent)((Object[])aEvent.getData())[0];
		IXMLTag lTag = (IXMLTag)((Object[])aEvent.getData())[1];
		boolean lUpdate = (boolean)((Object[])aEvent.getData())[2];

		boolean lPresent = true;
		List<IXMLTag> lGroups = (List<IXMLTag>)lLocationObject.getAttribute("groups");
		if (lGroups.size() > 0) {
			for (IXMLTag lGroup : lGroups) {
				lPresent = lPresent && !CDesktopComponents.sSpring().getCurrentTagStatus(lGroup, AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse);
			}
		}
		lPresent = lPresent && !CDesktopComponents.sSpring().getCurrentTagStatus(lTag, AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse);
		if (!lPresent) {
			lLocationObject.setVisible(false);
			//clear uri to stop macro
			lLocationObject.setMacroURI(VView.v_run_empty_fr);
			return;
		}

		//set common style attributes: z-index, left, top, width, height and cursor
		setLocationObjectCommonStyleAttributes(lLocationObject, lTag);
		getStyleAttributes(lLocationObject).add(new String[]{"position", "absolute"});
		lLocationObject.setStyle(generateStyle(lLocationObject));

		String lZulFile = getZulfile(lTag);
		if (lZulFile.length() > 0) {
			String zulfilepath = getZulfilePath(lTag);
			lLocationObject.setDynamicProperty("a_casecomponent", getNavigationComponent());
			lLocationObject.setDynamicProperty("a_tag", lTag);
			lLocationObject.setDynamicProperty("a_zulfilepath", zulfilepath);
			lLocationObject.setMacroURI(lZulFile);
			lLocationObject.setVisible(true);
		}
		if (lUpdate)
			((HtmlMacroComponent)lLocationObject).recreate();
	}

	/**
	 * Sets label button layout.
	 *
	 * @param aLocationObject the location object
	 * @param aTag the tag
	 */
	public void setLabelButtonLayout(HtmlBasedComponent aLocationObject, IXMLTag aTag) {
		boolean lPresent = true;
		List<IXMLTag> lGroups = (List<IXMLTag>)aLocationObject.getAttribute("groups");
		if (lGroups.size() > 0) {
			for (IXMLTag lGroup : lGroups) {
				lPresent = lPresent && !CDesktopComponents.sSpring().getCurrentTagStatus(lGroup, AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse);
			}
		}
		lPresent = lPresent && !CDesktopComponents.sSpring().getCurrentTagStatus(aTag, AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse);
		aLocationObject.setVisible(lPresent);
		if (!lPresent) {
			return;
		}
		//set common style attributes: z-index, left, top, width, height and cursor
		getStyleAttributes(aLocationObject).add(new String[]{"position", "absolute"});
		setLocationObjectCommonStyleAttributes(aLocationObject, aTag);
		aLocationObject.setStyle(generateStyle(aLocationObject));

		aLocationObject.setAttribute("casecomponent", getNavigationComponent());
		aLocationObject.setAttribute("tag", aTag);

		((HtmlMacroComponent)aLocationObject).setDynamicProperty("a_accessible", "" + CDesktopComponents.sSpring().getCurrentTagStatus(aTag, AppConstants.statusKeyAccessible).equals(AppConstants.statusValueTrue));

		//NOTE recreate macro, so the button will get a onCreate message that is used to set the width of the button within run_input_labelbtn_macro.zul
		((HtmlMacroComponent)aLocationObject).recreate();
	}

	/**
	 * Sets button layout.
	 *
	 * @param tag the tag
	 */
	public void setBtnLayout(IXMLTag tag) {
		CRunHoverBtn btn = (CRunHoverBtn)CDesktopComponents.vView().getComponent("location" + tag.getAttribute(AppConstants.keyId) + "Object");
		if (btn != null) {
			setButtonLayout(btn, tag);
		}
	}

	/**
	 * Sets clickable object layout.
	 *
	 * @param tag the tag
	 */
	public void setClObjLayout(IXMLTag tag) {
		CRunHoverBtn btn = (CRunHoverBtn)CDesktopComponents.vView().getComponent("location" + tag.getAttribute(AppConstants.keyId) + "Object");
		if (btn != null) {
			setButtonLayout(btn, tag);
			//NOTE have to set onclick behavior every time, because if visible of the piece is false, ZK does not seem to set widgetlistener for onclick.
			handleClickableObjectOnClick(btn, tag);
		}
	}

	/**
	 * Sets panel layout.
	 *
	 * @param tag the tag
	 */
	public void setPanelLayout(IXMLTag tag) {
		Include panel = (Include)CDesktopComponents.vView().getComponent("location" + tag.getAttribute(AppConstants.keyId) + "Object");
		if (panel != null) {
			setPanelLayout(panel, tag);
		}
	}

	/**
	 * Sets plug-in layout.
	 *
	 * @param tag the tag
	 * @param aUpdate the update boolean; set when 'update' key is triggered (for macro or plugin)
	 */
	public void setPluginLayout(IXMLTag tag, boolean aUpdate) {
		Include plugin = (Include)CDesktopComponents.vView().getComponent("location" + tag.getAttribute(AppConstants.keyId) + "Object");
		if (plugin != null) {
			setPluginLayout(plugin, tag, aUpdate);
		}
	}

	/**
	 * Sets piece layout.
	 *
	 * @param tag the tag
	 */
	public void setPieceLayout(IXMLTag tag) {
		CRunHoverBtn btn = (CRunHoverBtn)CDesktopComponents.vView().getComponent("location" + tag.getAttribute(AppConstants.keyId) + "Object");
		if (btn != null) {
			setButtonLayout(btn, tag);
			//NOTE have to set onclick behavior every time, because if visible of the piece is false, ZK does not seem to set widgetlistener for onclick, although is the code of method handlePieceOnClick.
			handlePieceOnClick(btn, tag);
		}
	}

	/**
	 * Sets macro layout.
	 *
	 * @param tag the tag
	 * @param aUpdate the update boolean; set when 'update' key is triggered (for macro or plugin)
	 */
	public void setMacroLayout(IXMLTag tag, boolean aUpdate) {
		HtmlMacroComponent macro = (HtmlMacroComponent)CDesktopComponents.vView().getComponent("location" + tag.getAttribute(AppConstants.keyId) + "Object");
		if (macro != null) {
			setMacroLayout(macro, tag, aUpdate);
		}
	}

	/**
	 * Sets label button layout.
	 *
	 * @param tag the tag
	 */
	public void setLabelButtonLayout(IXMLTag tag) {
		HtmlBasedComponent lLabelBtn = (HtmlBasedComponent)CDesktopComponents.vView().getComponent("location" + tag.getAttribute(AppConstants.keyId) + "Object");
		if (lLabelBtn != null) {
			setLabelButtonLayout(lLabelBtn, tag);
		}
	}

	/**
	 * Sets group layout.
	 *
	 * @param tag the tag
	 * @param aUpdate the update boolean
	 */
	public void setGroupLayout(IXMLTag aTag, boolean aUpdate) {
		List<IXMLTag> childs = new ArrayList<IXMLTag>();
		childs.addAll(aTag.getChildTags(AppConstants.defValueNode));
		if (childs.size() == 0)
			return;
		for (IXMLTag child : childs)
			setTagLayout(child.getAttribute(AppConstants.keyId), aUpdate);
	}

	/**
	 * Sets layout.
	 *
	 * @param aTagId the a tag id
	 * @param aUpdate the update boolean; set when 'update' key is triggered (for macro or plugin)
	 */
	public void setTagLayout(String aTagId, boolean aUpdate) {
		IXMLTag tag = CDesktopComponents.sSpring().getXmlDataPlusRunStatusTag(getNavigationComponent(), AppConstants.statusTypeRunGroup, aTagId);
		if (tag == null) {
			return;
		}
		String lTagName = tag.getName();
		if (lTagName.equals("passage") || lTagName.equals("object")) {
			setBtnLayout(tag);
		}
		if (lTagName.equals("clickableobject")) {
			setClObjLayout(tag);
		}
		else if (lTagName.equals("panel")) {
			setPanelLayout(tag);
		}
		else if (lTagName.equals("plugin")) {
			setPluginLayout(tag, aUpdate);
		}
		else if (lTagName.equals("piece")) {
			setPieceLayout(tag);
		}
		else if (lTagName.equals("button")) {
			setLabelButtonLayout(tag);
		}
		else if (lTagName.equals("macro")) {
			setMacroLayout(tag, aUpdate);
		}
		else if (lTagName.equals("group")) {
			setGroupLayout(tag, aUpdate);
		}
	}

	/**
	 * Gets the tag status for the button identified by aTag.
	 *
	 * @param aLocationTag the a location tag
	 *
	 * @return the tag status
	 */
	protected String getTagStatus(IXMLTag aTag) {
		boolean lPresent = ((CDesktopComponents.sSpring().getCurrentTagStatus(aTag,
				AppConstants.statusKeyPresent))
				.equals(AppConstants.statusValueTrue));
		boolean lAccessible = ((CDesktopComponents.sSpring().getCurrentTagStatus(aTag,
				AppConstants.statusKeyAccessible))
				.equals(AppConstants.statusValueTrue));
		boolean lOpened = ((CDesktopComponents.sSpring().getCurrentTagStatus(aTag,
				AppConstants.statusKeyOpened))
				.equals(AppConstants.statusValueTrue));
		return getTagStatus(lPresent, lAccessible, lOpened);
	}

	/**
	 * Gets the tag status as string.
	 *
	 * @param aVisible the a visible
	 * @param aEnabled the a enabled
	 * @param aSelected the a selected
	 *
	 * @return the tag status
	 */
	public String getTagStatus(boolean aVisible, boolean aEnabled, boolean aSelected) {
		String lStatus = "active";
		if (!aVisible)
			lStatus = "empty";
		else {
			if (!aEnabled)
				lStatus = "inactive";
			else {
				if (aSelected)
					lStatus = "selected";
			}
		}
		return lStatus;
	}

	/**
	 * Creates new hover btn.
	 *
	 * @param aId the a id
	 * @param aStatus the a status the button should get
	 * @param aAction the a action to be send when clicked
	 * @param aActionStatus the a action status of the action
	 * @param aPosition the a position, where on the screen the button appears for layout
	 * @param aImgPrefix the a img prefix of the hover images
	 * @param aLabel the a label to be shown above the button
	 * @param aXMLTag the a xml tag related to the button
	 *
	 * @return the c run hover btn
	 */
	protected CRunHoverBtn newHoverBtn(String aId, String aStatus, String aAction,
			Object aActionStatus, String aImgPrefix, String aLabel, IXMLTag aXMLTag) {
		CRunHoverBtn cHoverBtn = createHoverBtn(aId, aStatus, aAction, aActionStatus, aImgPrefix, aLabel);
		cHoverBtn.setAttribute("xmltag", aXMLTag);
		cHoverBtn.registerObserver("runLocationArea");
		cHoverBtn.registerObserver(CControl.runWnd);
		cHoverBtn.setVisible(!aStatus.equals("empty"));
		if (runWnd != null) {
			cHoverBtn.setWidgetListener("onClick", runWnd.getMediaplayerStopAction());
		}
		return cHoverBtn;
	}

	/**
	 * Creates new hover btn.
	 *
	 * @param aId the a id
	 * @param aStatus the a status the button should get
	 * @param aAction the a action to be send when clicked
	 * @param aActionStatus the a action status of the action
	 * @param aImgPrefix the a img prefix of the hover images
	 * @param aLabel the a label to be shown above the button
	 *
	 * @return the c run hover btn
	 */
	protected CRunHoverBtn createHoverBtn(String aId, String aStatus, String aAction,
			Object aActionStatus, String aImgPrefix, String aLabel) {
		return new CRunHoverBtn(aId, aStatus, aAction, aActionStatus, aImgPrefix, aLabel, "");
	}

	/**
	 * Determines if transition effect has to be shown when moving from one location to another.
	 *
	 * @param aFromLocationTag the from location tag
	 * @param aToLocationTag the to location tag
	 *
	 * @return boolean
	 */
	public boolean doShowTransitionEffect(IXMLTag aFromLocationTag, IXMLTag aToLocationTag) {
		if (aFromLocationTag == null || aToLocationTag == null)
			return false;
		//determine rootTag
		IXMLTag rootTag = null;
		IXMLTag parentTag = aToLocationTag.getParentTag();
		while (parentTag != null) {
			if (parentTag.getName().equals(AppConstants.rootElement)) {
				rootTag = parentTag;
				break;
			}
			else {
				parentTag = parentTag.getParentTag();
			}
		}
		if (rootTag == null)
			return false;
		return rootTag.getChild(AppConstants.componentElement).getChildAttribute(AppConstants.statusElement, AppConstants.statusKeyTransitioneffect).equals(AppConstants.statusValueTrue) ||
				(aFromLocationTag.getChildValue("transitioneffect").equals(AppConstants.statusValueTrue) &&
				 aToLocationTag.getChildValue("transitioneffect").equals(AppConstants.statusValueTrue));
	}

	/**
	 * Is called when passage, object or clickable object is clicked.
	 *
	 * @param aObserved the observed object, it should have interface CIObserved implemented
	 * @param aAction the action
	 * @param aStatus the status object, can be String or other class
	 */
	@Override
	public void observedNotify(CIObserved aObserved, String aAction, Object aStatus) {
		CRunHoverBtn lBtn = (CRunHoverBtn)aObserved;
		if (lBtn.getCanHaveStatusSelected()) {
			//deselect possible other button
			if (selectedHoverBtn != null && selectedHoverBtn.getStatus().equals("selected")) {
				selectedHoverBtn.setStatus("active");
				setButtonLayout(selectedHoverBtn, (IXMLTag)selectedHoverBtn.getAttribute("xmltag"));
				setRunTagStatus(getNavigationComponent(), (IXMLTag)selectedHoverBtn.getAttribute("xmltag"), AppConstants.statusKeySelected, AppConstants.statusValueFalse, false);
			}
			//select button
			selectedHoverBtn = lBtn;
			if (selectedHoverBtn.getStatus().equals("selected")) {
				//button already got status selected when clicked
				setButtonLayout(selectedHoverBtn, (IXMLTag)selectedHoverBtn.getAttribute("xmltag"));
			}
		}
		setRunTagStatus(getNavigationComponent(), (IXMLTag)lBtn.getAttribute("xmltag"), AppConstants.statusKeySelected, AppConstants.statusValueTrue, true);
	}

	/**
	 * Sets run group tag status. Used when content item status changes.
	 *
	 * @param aCaseComponent the a case component used by the run component
	 * @param aTag the a tag
	 * @param aStatusKey the a status key
	 * @param aStatusValue the a status value
	 * @param aSaveInDb the a save in db
	 *
	 * @return the XML status tag
	 */
	public IXMLTag setRunTagStatus(IECaseComponent aCaseComponent, IXMLTag aTag,
			String aStatusKey, String aStatusValue, boolean aSaveInDb) {
		return CDesktopComponents.sSpring().setRunTagStatus(aCaseComponent, aTag, aStatusKey, aStatusValue, true, AppConstants.statusTypeRunGroup, aSaveInDb, false);
	}

	/**
	 * Handles status change due to firing of script actions.
	 *
	 * @param aTriggeredReference the a triggered reference
	 */
	public void handleStatusChange(STriggeredReference aTriggeredReference) {
		if (aTriggeredReference.getDataTag() == null) {
			return;
		}
		CRunWndOunl lRunWnd = (CRunWndOunl)runWnd;
		if (lRunWnd == null) {
			return;
		}
		boolean lRerenderNavigationObject =
			(aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyPresent) ||
					aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyAccessible)) &&
			(aTriggeredReference.getDataTag().getName().equals("passage") ||
					aTriggeredReference.getDataTag().getName().equals("object") ||
					aTriggeredReference.getDataTag().getName().equals("clickableobject") ||
					aTriggeredReference.getDataTag().getName().equals("panel")  ||
					aTriggeredReference.getDataTag().getName().equals("plugin") ||
					aTriggeredReference.getDataTag().getName().equals("piece") ||
					aTriggeredReference.getDataTag().getName().equals("button") ||
					aTriggeredReference.getDataTag().getName().equals("macro") ||
					aTriggeredReference.getDataTag().getName().equals("group"));
		boolean lUpdateNavigationPluginOrMacro = aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyUpdate) && (aTriggeredReference.getDataTag().getName().equals("plugin") || aTriggeredReference.getDataTag().getName().equals("macro"));
		boolean lHighlightElement = aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyHighlight);
		boolean lDrawAttentionToElement = aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyDrawAttention);
		if (lRerenderNavigationObject || lUpdateNavigationPluginOrMacro) {
			// NOTE Use tag id, because lDataTag does not contain the status change yet.
			// Run location area should get the current data plus status tag.
			//NOTE location object tags (e.g. clickable objects) may have no location tag parent if they are directly positioned under the root element.
			//So only check on location tag id if the location tag parent is found.
			IXMLTag lLocationTag = lRunWnd.getLocation(aTriggeredReference.getDataTag().getParentTag().getParentTag().getAttribute(AppConstants.keyId));
			if (lLocationTag == null || lRunWnd.currentLocationTagId.equals(lLocationTag.getAttribute(AppConstants.keyId))) {
				// only rerender if change on current location.
				String lId = aTriggeredReference.getDataTag().getAttribute(AppConstants.keyId);
				if (tagsWithoutPosition.size() == 0 || aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyAccessible)) {
					setTagLayout(lId, lUpdateNavigationPluginOrMacro);
				}
				else if (aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyPresent)) {
					if (tagsWithoutPositionTop.containsKey(lId) || tagsWithoutPositionLeft.containsKey(lId)) {
						//NOTE if tag without position, rerender location, because positions can change
						//TODO group or subgroup of group may contain object without position
						showLocation(lLocationTag);
					}
					else {
						setTagLayout(lId, lUpdateNavigationPluginOrMacro);
					}
				}
			}
		}
		else {
			boolean lRerenderNavigationBackground = aTriggeredReference.getDataTag().getName().equals("background") &&
				aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyOpened) &&
				aTriggeredReference.getStatusValue().equals(AppConstants.statusValueTrue);
			if (lRerenderNavigationBackground) {
				// show location background
				IXMLTag lLocationTag = lRunWnd.getLocation(aTriggeredReference.getDataTag().getParentTag().getAttribute(AppConstants.keyId));
				if (lRunWnd.currentLocationTagId.equals(lLocationTag.getAttribute(AppConstants.keyId))) {
					// only refresh background if background change of current location.
					showLocation(lLocationTag);
				}
			}
		}
		if (lHighlightElement) {
			runWnd.highlightComponent("location" + aTriggeredReference.getDataTag().getAttribute(AppConstants.keyId) + "Object");
		}
		if (lDrawAttentionToElement) {
			runWnd.drawAttentionToComponent("location" + aTriggeredReference.getDataTag().getAttribute(AppConstants.keyId) + "Object", aTriggeredReference.getStatusValue().equals(AppConstants.statusValueTrue));
		}
	}

}
