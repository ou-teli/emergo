/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.superTeacher;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.CheckEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Image;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitInitBox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefCheckbox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.cases.PVtoolkit.util.CRunPVToolkitSendMessagesHelper;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.view.VView;

public class CRunPVToolkitSuperTeacherSendMessageDiv extends CDefDiv {

	private static final long serialVersionUID = 8108009488407603330L;

	protected VView vView = CDesktopComponents.vView();
	
	protected String zulfilepath = ((CRunPVToolkitInitBox)vView.getComponent("PV-toolkit_initBox")).getZulfilepath();
	
	protected CRunPVToolkit pvToolkit;
	
	protected CRunPVToolkitSuperTeacherMessagesDiv messagesDiv;
	
	protected CRunPVToolkitSendMessagesHelper sendMessagesHelper;

	protected IECaseComponent _messagesCaseComponent;
	protected IXMLTag _messageTag;
	protected IXMLTag _stepTag;
	
	protected List<IERunGroupAccount> runGroupAccounts;
	
	protected IAccountManager accountManager = (IAccountManager)CDesktopComponents.sSpring().getBean("accountManager");

	//NOTE isPreviewRun is true if preview option in authoring environment is used
	protected boolean isPreviewRun = false;
	
	protected String _idPrefix = "superTeacherSend";
	protected String _classPrefix = "superTeacherSend";
	
	public void init(IECaseComponent messagesCaseComponent, IXMLTag messageTag, IXMLTag stepTag) {
		_messageTag = messageTag;
		_stepTag = stepTag;
		_messagesCaseComponent = messagesCaseComponent;
		
		pvToolkit = (CRunPVToolkit)vView.getComponent("pvToolkit");
		
		messagesDiv = (CRunPVToolkitSuperTeacherMessagesDiv)vView.getComponent("superTeacherMessagesDiv");
		
		sendMessagesHelper = new CRunPVToolkitSendMessagesHelper(CDesktopComponents.sSpring(), pvToolkit.getCurrentProjectsCaseComponent(), pvToolkit.getCurrentMethodTag());

		update();
		
		setVisible(true);
	}
	
	public void update() {
		
		getChildren().clear();
		
		pvToolkit.setMemoryCaching(true);
		
		runGroupAccounts = pvToolkit.getActiveRunGroupAccounts();
		isPreviewRun = CDesktopComponents.sSpring().getRun().getStatus() == AppConstants.run_status_test;

		new CRunPVToolkitDefImage(this, 
				new String[]{"class"}, 
				new Object[]{"popupBackground"}
		);

		new CRunPVToolkitDefImage(this, 
				new String[]{"class", "src"}, 
				new Object[]{_classPrefix + "MessageBackground", zulfilepath + "popup-large-background.svg"}
		);

		Div div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "MessageTitle"}
		);
		
		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font " + _classPrefix + "MessageTitle", "PV-toolkit-superTeacher.header.message"}
		);
		
		Image closeImage = new CRunPVToolkitDefImage(this, 
				new String[]{"class", "src"}, 
				new Object[]{_classPrefix + "MessageCloseButton", zulfilepath + "close.svg"}
		);
		addCloseButtonOnClickEventListener(closeImage);

		Button btn = new CRunPVToolkitDefButton(this, 
				new String[]{"id", "class", "cLabelKey"}, 
				new Object[]{_idPrefix + "MessageSendButton", "font pvtoolkitButton " + _classPrefix + "MessageSendButton", "PV-toolkit.send"}
		);
		addSendButtonOnClickEventListener(btn);

		div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "MessageToDiv"}
		);
		
		Rows rows = appendPeersGrid(div);
		int rowNumber = 1;
		rowNumber = appendPeersToGrid(rows, rowNumber);

		pvToolkit.setMemoryCaching(false);
	}

	protected void addCloseButtonOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				Component componentToHide = vView.getComponent(_idPrefix + "MessageDiv");
				if (componentToHide != null) {
					componentToHide.setVisible(false);
				}
			}
		});
	}

	protected void addSendButtonOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				sendMessagesHelper.sendMessage(getRunGroupAccountsToSend(), _messagesCaseComponent, _messageTag, _stepTag, isPreviewRun);

				setVisible(false);
			}
		});
	}

	protected List<IERunGroupAccount> getRunGroupAccountsToSend() {
		List<IERunGroupAccount> runGroupAccountsToSend = new ArrayList<IERunGroupAccount>();
		for (IERunGroupAccount runGroupAccount : runGroupAccounts) {
			Checkbox checkbox = (Checkbox)vView.getComponent(_idPrefix + "SendMessageCheckbox_" + runGroupAccount.getRgaId());
			if (checkbox != null && checkbox.isChecked()) {
				runGroupAccountsToSend.add(runGroupAccount);
			}
		}
		return runGroupAccountsToSend;
	}
	
    protected Rows appendPeersGrid(Component parent) {
    	return pvToolkit.appendGrid(
    			parent, 
    			getId() + "Grid",
    			"position:absolute;left:0px;top:0px;width:1060px;height:440px;overflow:auto;", 
    			null,
    			new String[] {"3%", "36%", "36%", "15%", "10%"}, 
    			new boolean[] {false, true, true, true, true},
    			new boolean[] {false, false, false, false, true},
    			"PV-toolkit-superTeacher.message.column.label.", 
    			new String[] {"", "name", "email", "userid", "send"});
    }

	protected int appendPeersToGrid(Rows rows, int rowNumber) {
		return appendPeersToGrid(
				rows, 
				rowNumber,
				new boolean[] {true, true, true, true, true} 
				);
	}

	protected int appendPeersToGrid(
			Rows rows, 
			int rowNumber,
    		boolean[] showColumn 
			) {
		for (IERunGroupAccount runGroupAccount : runGroupAccounts) {
			rowNumber = appendPeerRowToGrid(
					rows, 
					rowNumber,
					showColumn,
					runGroupAccount
					);
		}

		return rowNumber;
	}
	
	protected int appendPeerRowToGrid(
			Rows rows, 
			int rowNumber,
    		boolean[] showColumn,
    		IERunGroupAccount runGroupAccount
			) {
		Row row = new Row();
		rows.appendChild(row);
			
		int columnNumber = 0;
		if (showColumn.length > columnNumber && showColumn[columnNumber]) {
			new CRunPVToolkitDefImage(row, 
					new String[]{"class", "src"}, 
					new Object[]{"gridImage", zulfilepath + "user.svg"}
					);
		}

		columnNumber++;
		if (showColumn.length > columnNumber && showColumn[columnNumber]) {
			new CRunPVToolkitDefLabel(row, 
					new String[]{"class", "value"}, 
					new Object[]{"font gridLabel", runGroupAccount.getERunGroup().getName()}
					);
		}

		columnNumber++;
		if (showColumn.length > columnNumber && showColumn[columnNumber]) {
			new CRunPVToolkitDefLabel(row, 
					new String[]{"class", "value"}, 
					new Object[]{"font gridLabel", runGroupAccount.getEAccount().getEmail()}
					);
		}

		columnNumber++;
		if (showColumn.length > columnNumber && showColumn[columnNumber]) {
			new CRunPVToolkitDefLabel(row, 
					new String[]{"class", "value"}, 
					new Object[]{"font gridLabel", runGroupAccount.getEAccount().getUserid()}
					);
		}

		columnNumber++;
		if (showColumn.length > columnNumber && showColumn[columnNumber]) {
			CRunPVToolkitDefCheckbox checkbox = new CRunPVToolkitDefCheckbox(row, 
					new String[]{"id", "checked"}, 
					new Object[]{_idPrefix + "SendMessageCheckbox_" + runGroupAccount.getRgaId(), sendMessagesHelper.mayReceiveMessage(runGroupAccount, _stepTag, true)}
			);
			
			setCheckboxOrderValue(checkbox);
			addSendOnCheckEventListener(checkbox);
		}

		rowNumber ++;

		return rowNumber;
	}

	protected void addSendOnCheckEventListener(Component component) {
		component.addEventListener("onCheck", new EventListener<CheckEvent>() {
			public void onEvent(CheckEvent event) {
				setCheckboxOrderValue((Checkbox)event.getTarget());
			}
		});
	}
	
	protected void setCheckboxOrderValue(Checkbox checkbox) {
		if (checkbox.isChecked()) {
			checkbox.setAttribute("orderValue", "1");
		}
		else {
			checkbox.setAttribute("orderValue", "2");
		}
	}
	
}
