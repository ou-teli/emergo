/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl;

import org.zkoss.zul.Treeitem;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.run.CRunAssessmentsTree;
import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.control.run.CRunComponentHelper;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunAssessmentsTreeOunl.
 */
public class CRunAssessmentsTreeOunl extends CRunAssessmentsTree {

	private static final long serialVersionUID = -7153587315147505947L;

	/**
	 * Instantiates a new c run assessments tree.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the assessments case component
	 * @param aRunComponent the a run component, the ZK assessments component
	 */
	public CRunAssessmentsTreeOunl(String aId, IECaseComponent aCaseComponent, CRunComponent aRunComponent) {
		super(aId, aCaseComponent, aRunComponent);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTree#getRunComponentHelper()
	 */
	@Override
	public CRunComponentHelper getRunComponentHelper() {
		return new CRunAssessmentsHelperOunl(this, tagopenednames, caseComponent, runComponent);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTree#setTreeitemStatus(org.zkoss.zul.Treeitem)
	 */
	@Override
	public Treeitem setTreeitemStatus(Treeitem aTreeitem) {
		String lPreviousSelectedItemId = "";
		IXMLTag tag = getContentItemTag(aTreeitem);
		boolean select = tag != null && tag.getName().equals("refitem");
		if (select) {
			// get previous selected item id
			lPreviousSelectedItemId = getSelectedTagId();
			saveSelectedTagId(tag);
		}
		Treeitem lTreeitem = super.setTreeitemStatus(aTreeitem);
		if (select && !lPreviousSelectedItemId.equals("")) {
			// rerender previous selected question to remove selection arrow
			Treeitem lPreviousTreeitem = getContentItem(lPreviousSelectedItemId);
			if (lPreviousTreeitem != null) {
				IXMLTag lPreviousTag = getContentItemTag(lPreviousTreeitem);
				if (lPreviousTag != null) {
					reRenderTreeitem(lPreviousTreeitem, lPreviousTag, false);
				}
			}
		}
		return lTreeitem;
	}

}
