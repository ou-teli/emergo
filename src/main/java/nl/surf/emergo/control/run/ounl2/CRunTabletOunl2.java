/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl2;

import org.zkoss.zk.ui.Component;

import nl.surf.emergo.control.run.CRunArea;
import nl.surf.emergo.control.run.CRunHbox;
import nl.surf.emergo.control.run.ounl.CRunComponentDecoratorOunl;
import nl.surf.emergo.control.run.ounl.CRunTabletOunl;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunTabletOunl2 is used to show the tablet apps.
 */
public class CRunTabletOunl2 extends CRunTabletOunl {

	private static final long serialVersionUID = -6016771600834871039L;

	/**
	 * Instantiates a new c run tablet.
	 */
	public CRunTabletOunl2() {
		super();
	}

	/**
	 * Instantiates a new c run tablet.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the tablet case component
	 */
	public CRunTabletOunl2(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
	}

	@Override
	protected CRunArea createTitleArea(Component aParent) {
		CRunHbox lHbox = new CRunHbox();
		aParent.appendChild(lHbox);
		// NOTE Don't create title area
		CRunComponentDecoratorOunl decorator = createDecorator();
		decorator.createCloseArea(caseComponent, lHbox, getClassName(), getId(), this);
		return null;
	}

	@Override
	protected CRunComponentDecoratorOunl createDecorator() {
		return new CRunComponentDecoratorOunl2();
	}

	@Override
	protected Component newContentComponent() {
		return (new CRunTabletAppBtnsOunl2(this, "runTabletAppBtns"));
	}

}
