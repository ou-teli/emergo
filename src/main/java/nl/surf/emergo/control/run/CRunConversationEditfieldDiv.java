/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vbox;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.def.CDefTextbox;
import nl.surf.emergo.control.def.CDefVbox;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpringHelper;

/**
 * The Class CRunConversationImageView is used to show a still image during conversations.
 */
public class CRunConversationEditfieldDiv extends CDefDiv {

	private static final long serialVersionUID = 5567849477266991732L;

	protected CRunConversations currentEmergoComponent;
	
	protected IXMLTag editFieldTag;

	/** The run wnd. */
	protected CRunWnd runWnd = (CRunWnd) CDesktopComponents.vView().getComponent(CControl.runWnd);

	public void onCreate(CreateEvent aEvent) {
		currentEmergoComponent = (CRunConversations)VView.getParameter("emergoComponent", this, null);
		editFieldTag = (IXMLTag)VView.getParameter("tag", this, null);
		SSpringHelper sSpringHelper = new SSpringHelper();
		String editFieldTagId = sSpringHelper.getTagAttribute(editFieldTag, AppConstants.keyId);
		setId("editFieldDiv" + editFieldTagId);
		setStyle(runWnd.getStylePosition(editFieldTag));

		//get status value
		String value = sSpringHelper.getCurrentStatusTagStatusChildAttribute(editFieldTag, "string", "");
		if (value.equals("")) {
			//if empty get default value
			value = sSpringHelper.getTagChildValue(editFieldTag, "text", "");
		}
		int numberOfLines = Integer.parseInt(sSpringHelper.getTagChildValue(editFieldTag, "numberoflines", "3"));
		
		Vbox vbox = new CDefVbox();
		appendChild(vbox);
		
		Textbox textbox = new CDefTextbox();
		vbox.appendChild(textbox);
		textbox.setValue(value);
		textbox.setRows(numberOfLines);
		textbox.setFocus(true);
		textbox.setSclass("CRunConversationsEditfield");
		textbox.addEventListener("onChange",
				new EventListener<Event>() {
					public void onEvent(Event event) {
						if (currentEmergoComponent != null) {
							currentEmergoComponent.saveFieldText(editFieldTag, textbox.getValue());
						}
					}
				});
		
		HtmlMacroComponent macro = new HtmlMacroComponent();
		vbox.appendChild(macro);
		macro.setSclass("CRunConversationEditfieldReadyBtn");
		macro.setDynamicProperty("a_observer", getId());
		macro.setDynamicProperty("a_event", "onReady");
		macro.setDynamicProperty("a_label", CDesktopComponents.vView().getCLabel("ready"));
		macro.setDynamicProperty("a_classtype", "DarkGreenVertical");
		macro.setMacroURI("../run_input_labelbtn_macro.zul");
	}

	public void onReady(Event aEvent) {
		if (currentEmergoComponent != null) {
			currentEmergoComponent.showFeedback(editFieldTag);
		}
	}
	
}
