/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl2;

import org.zkoss.util.media.Media;

//import nl.surf.emergo.control.def.CDefFCKeditor;
import nl.surf.emergo.control.def.CDefTextbox;
import nl.surf.emergo.control.run.CRunArea;
import nl.surf.emergo.control.run.CRunHbox;
import nl.surf.emergo.control.run.ounl.CRunNewMailOunl;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CRunNewMailOunl2. Is used to show input elements for new mail.
 */
public class CRunNewMailOunl2 extends CRunNewMailOunl {

	private static final long serialVersionUID = -2885465738868144900L;

	/**
	 * Instantiates a new c run new mail.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the a case component
	 */
	public CRunNewMailOunl2(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
	}

	@Override
	protected CRunArea createNewNewMailButton(String aId, String aAction, Object aActionStatus, String aLabel) {
		return new CRunLabelButtonOunl2(aId, aAction, aActionStatus, aLabel, "", "");
	}

	@Override
	protected CDefTextbox getFCKEditor() {
		CDefTextbox lEditor = super.getFCKEditor();
		lEditor.setWidth("834px");
		return lEditor;
	}

	@Override
	protected CRunHbox getNewMailAttachment(Media aMedia) {
		return new CRunNewMailAttachmentOunl2(aMedia);
	}

}