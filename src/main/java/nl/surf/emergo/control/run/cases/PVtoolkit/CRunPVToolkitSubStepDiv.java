/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit;

import org.zkoss.zk.ui.Component;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.feedback.CRunPVToolkitFeedbackRubricDiv;
import nl.surf.emergo.domain.IERunGroup;

public class CRunPVToolkitSubStepDiv extends CDefDiv {

	private static final long serialVersionUID = 6224893585404783361L;

	public String zulfilepath = ((CRunPVToolkitInitBox)CDesktopComponents.vView().getComponent("PV-toolkit_initBox")).getZulfilepath();
	
	protected CRunPVToolkit pvToolkit;
	
	protected IERunGroup _actor;
	protected boolean _editable;
	protected String _subStepType;
		
	public void init(IERunGroup actor, boolean reset, boolean editable, String subStepType) {
		pvToolkit = (CRunPVToolkit)CDesktopComponents.vView().getComponent("pvToolkit");
		
		_actor = actor;
		_editable = editable;
		_subStepType = subStepType;
		
		if (reset) {
			reset();
		}
		update();
	}
	
	public void reset() {
		for (Component subSubStep : getChildren()) {
			subSubStep.setVisible(false);
		}
		//NOTE init the first sub sub step
		if (getChildren().size() > 0) {
			if (getChildren().get(0) instanceof CRunPVToolkitFeedbackRubricDiv) {
				((CRunPVToolkitFeedbackRubricDiv)getChildren().get(0)).init(_actor, null, null, false, true);
			}
			if (getChildren().get(0) instanceof CRunPVToolkitSkillWheelLevel2Div) {
				((CRunPVToolkitSkillWheelLevel2Div)getChildren().get(0)).init(_actor, false, 0, 0);
			}
			else if (getChildren().get(0) instanceof CRunPVToolkitSubSubStepDiv) {
				((CRunPVToolkitSubSubStepDiv)getChildren().get(0)).init(_actor, true, _editable, _subStepType);
			}
		}
		setVisible(true);
	}
	
	public void update() {
		//NOTE should be overwritten by extending class
	}

}
