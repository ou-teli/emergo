/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CIObserved;
import nl.surf.emergo.control.def.CDefLabel;
import nl.surf.emergo.control.def.CDefTextbox;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.zkspring.STriggeredReference;

/**
 * The Class CRunNote is used to show the note window within the run choice area of the
 * Emergo player. Notes can be edited within the logbook too, so this class has a public
 * method to save content.
 */
public class CRunNote extends CRunArea {

	private static final long serialVersionUID = -1803748175544295978L;

	/** The run component. */
	protected CRunComponent runComponent = null;

	/** The case component, the note casecomponent. */
	protected IECaseComponent caseComponent = null;

	/** The case component for notes, the case component for which to take notes. */
	protected IECaseComponent caseComponentForNotes = null;

	/** The tag for notes, the tag for which to take notes. */
	protected IXMLTag tagForNotes = null;

	/** The textbox rows. */
	protected int textboxRows = 7;

	/** The max string length. */
	protected int maxStringLength = 70;

	/**
	 * The tag container for notes, the tag container for which to take notes.
	 * For instance for a question it is the conversation.
	 */
	protected IXMLTag tagContainerForNotes = null;

	/** The note title. */
	protected String noteTitle = "";

	/** The max string length. */
	protected static final String noteTitleSeparator = ": ";

	/**
	 * Instantiates a new c run note.
	 * Creates title area with close box and edit field for note.
	 */
	public CRunNote() {
		super();
		setId("runNote");
		runComponent = null;
		init();
	}

	/**
	 * Instantiates a new c run note.
	 * Creates title area with close box and edit field for note.
	 *
	 * @param aId the a id
	 * @param aRunComponent the a run component
	 */
	public CRunNote(String aId, CRunComponent aRunComponent) {
		super();
		setId(aId);
		runComponent = aRunComponent;
		init();
	}

	/**
	 * Sets run component.
	 */
	public void setRunComponent(CRunComponent aRunComponent) {
		runComponent = aRunComponent;
	}

	/**
	 * Gets the active note case component.
	 *
	 * @return the active note case component
	 */
	public IECaseComponent getActiveNoteCaseComponent() {
		if (runWnd == null) {
			return null;
		}
		return runWnd.getActiveCaseComponent("note");
	}

	/**
	 * Is case component switched.
	 *
	 * @return boolean
	 */
	public boolean isCaseComponentSwitched() {
		if (runWnd == null || caseComponent == null) {
			return false;
		}
		IECaseComponent lCaseComponent = runWnd.getActiveCaseComponent("note");
		if (lCaseComponent != null && lCaseComponent.getCacId() != caseComponent.getCacId()) {
			return true;
		}
		return false;
	}

	/**
	 * Creates title area with close box and edit field for note.
	 */
	public void init() {
		if (runWnd != null) {
			caseComponentForNotes = runWnd.getCurrentCaseComponentForNotes();
			tagForNotes = runWnd.getCurrentTagForNotes();
		}
		String lType = "";
		if (runWnd != null)
			lType = runWnd.getPlayerStatusString("ChoiceAreaLayoutStatus", null);
		setZclass(getClassName() + lType);

		createTitleArea(this);
		createTextArea(this);
	}

	/**
	 * Creates title area, a title label and close box.
	 *
	 * @param aParent the a parent
	 */
	protected void createTitleArea(Component aParent) {
		CRunHbox lSuperHbox = new CRunHbox();
		aParent.appendChild(lSuperHbox);

		String lType = "";
		CRunArea lArea = new CRunArea();
		lSuperHbox.appendChild(lArea);
		lArea.setZclass(getClassName()+lType+"_title_area_left");
		CRunHbox lHbox = new CRunHbox();
		lArea.appendChild(lHbox);
		Label lLabel = new CDefLabel();
		lLabel.setId(getId() + "Title");
		lLabel.setValue(CDesktopComponents.vView().getLabel("run_note.title"));
		lLabel.setZclass(getClassName() + "_title_area_label");
		lHbox.appendChild(lLabel);

		lArea = new CRunArea();
		lSuperHbox.appendChild(lArea);
		lArea.setZclass(getClassName()+"_title_area_right");
		CRunCloseBtn lButton = new CRunCloseBtn(getId() + "CloseBtn",
				"active","endNote", null, "close", "");
		lButton.setCanHaveStatusSelected(false);
		lButton.registerObserver(getId());
		lButton.registerObserver(CControl.runWnd);
		lArea.appendChild(lButton);
	}

	/**
	 * Creates text area for note.
	 *
	 * @param aParent the a parent
	 *
	 * @return the c run area
	 */
	protected CRunArea createTextArea(Component aParent) {
		String lType = "";
		if (runWnd != null)
			lType = runWnd.getPlayerStatusString("ChoiceAreaLayoutStatus", null);
		CRunArea lTextArea = new CRunArea();
		aParent.appendChild(lTextArea);
		lTextArea.setZclass(getClassName() + lType + "_text_area");
		createText(lTextArea);
		return lTextArea;
	}

	/**
	 * Creates richtext field for note.
	 *
	 * @param aParent the a parent
	 */
	protected void createText(Component aParent) {
		Textbox lTextbox = new CDefTextbox();
		aParent.appendChild(lTextbox);
		lTextbox.setId(getId()+"NoteField");
		lTextbox.setZclass(getClassName() + "_text_area_div");
		lTextbox.setWidth(getTextboxWidth());
		lTextbox.setRows(textboxRows);
		lTextbox.setAttribute("changed", AppConstants.statusValueFalse);
	}

	/**
	 * Get textbox width.
	 *
	 * @return the width
	 */
	protected String getTextboxWidth() {
		String lWidth = "832px";
		String lType = "";
		if (runWnd != null)
			lType = runWnd.getPlayerStatusString("ChoiceAreaLayoutStatus", null);
		if (runWnd != null && (lType.equals("Left") || lType.equals("Right")))
			lWidth = "832px";
		else {
			if (runWnd != null && lType.equals("Max"))
				lWidth = "994px";
			else
				lWidth = "670px";
		}
		return lWidth;
	}

	/**
	 * Gets the content, the saved content for the current note case component and tag.
	 *
	 * @return the content
	 */
	protected String getContent() {
		String lContent = "";
		caseComponent = getActiveNoteCaseComponent();
		if (caseComponent != null) {
			// notes are private
			IXMLTag lStatusRootTag = CDesktopComponents.sSpring().getXmlRunStatusTree(caseComponent, AppConstants.statusTypeRunGroup);
			if (lStatusRootTag != null) {
				IXMLTag lStatusContentTag = lStatusRootTag.getChild(AppConstants.contentElement);
				if ((lStatusContentTag != null) && (caseComponentForNotes != null)) {
					String lCacId = "" + caseComponentForNotes.getCacId();
					boolean lExistsTagForNotes = (tagForNotes != null);
					String lDataTagId = "";
					String lStatusTagId = "";
					if (lExistsTagForNotes) {
						lStatusTagId = tagForNotes.getAttribute(AppConstants.keyRefstatusid);
						lDataTagId = tagForNotes.getAttribute(AppConstants.keyId);
					}
					else {
						lDataTagId = "-1";
						lStatusTagId = "-1";
					}
					IXMLTag lStatusTag = getNoteTag(lStatusRootTag, lStatusContentTag, lExistsTagForNotes, lCacId, lDataTagId, lStatusTagId);
					if (lStatusTag != null) {
						lContent = CDesktopComponents.sSpring().unescapeXML(lStatusTag.getChildValue("text"));
					}
				}
			}
		}
		return lContent;
	}

	/**
	 * Sets the content for the given parameters. Called from logbook to save note!
	 *
	 * @param aNoteCaseComponent the a note case component
	 * @param aNoteCacId the a note cac id
	 * @param aNoteDataTagId the a note data tag id
	 * @param aNoteStatusTagId the a note status tag id
	 * @param aContent the a content
	 */
	public void setContent(IECaseComponent aCaseComponent, String aNoteCacId, String aNoteDataTagId, String aNoteStatusTagId, String aContent) {
		if (aCaseComponent != null) {
			// notes are private
			IXMLTag lStatusRootTag = CDesktopComponents.sSpring().getXmlRunStatusTree(aCaseComponent, AppConstants.statusTypeRunGroup);
			if (lStatusRootTag != null) {
				IXMLTag lStatusContentTag = lStatusRootTag.getChild(AppConstants.contentElement);
				if (lStatusContentTag != null) {
					String lCacId = aNoteCacId;
					boolean lExistsTagForNotes = ((!aNoteDataTagId.equals("")) || (!aNoteStatusTagId.equals("")));
					String lDataTagId = "";
					String lStatusTagId = "";
					if (lExistsTagForNotes) {
						lDataTagId = aNoteDataTagId;
						lStatusTagId = aNoteStatusTagId;
					}
					else {
						lDataTagId = "-1";
						lStatusTagId = "-1";
					}
					IXMLTag lStatusTag = getNoteTag(lStatusRootTag, lStatusContentTag, lExistsTagForNotes, lCacId, lDataTagId, lStatusTagId);
					if (lStatusTag != null) {
						String lTagContainerForNotesId = lStatusTag.getAttribute("refcontainerid");
						String lNoteTitle = lStatusTag.getChildValue("pid");
						lStatusTag = createNoteTag(aCaseComponent, lStatusRootTag, lStatusContentTag, lCacId, lDataTagId, lStatusTagId, lTagContainerForNotesId, lNoteTitle, aContent);
						if (lStatusTag != null) {
							saveNoteTag(aCaseComponent, lStatusRootTag, lStatusTag);
						}
					}
				}
			}
		}
	}

	/**
	 * Sets the content for the current note case component and tag.
	 * If status tag does not exist, it is created.
	 *
	 * @param aContent the a content
	 */
	protected void setContent(String aContent) {
		caseComponent = getActiveNoteCaseComponent();
		if (caseComponent != null && !noteTitle.equals("")) {
			// notes are private
			IXMLTag lStatusRootTag = CDesktopComponents.sSpring().getXmlRunStatusTree(caseComponent, AppConstants.statusTypeRunGroup);
			if (lStatusRootTag != null) {
				IXMLTag lStatusContentTag = lStatusRootTag.getChild(AppConstants.contentElement);
				if (lStatusContentTag != null) {
					String lCacId = "" + caseComponentForNotes.getCacId();
					boolean lExistsTagForNotes = (tagForNotes != null);
					String lDataTagId = "";
					String lStatusTagId = "";
					if (lExistsTagForNotes) {
						if (tagForNotes.getAttribute(AppConstants.keyExtended).equals(AppConstants.statusValueTrue))
							lStatusTagId = tagForNotes.getAttribute(AppConstants.keyRefstatusid);
						else
							lDataTagId = tagForNotes.getAttribute(AppConstants.keyId);
					}
					else {
						lDataTagId = "-1";
						lStatusTagId = "-1";
					}
					String lTagContainerForNotesId = "";
					if (tagContainerForNotes != null) {
						lTagContainerForNotesId = tagContainerForNotes.getAttribute(AppConstants.keyId);
					}
					String lNoteTitle = noteTitle;
					IXMLTag lStatusTag = createNoteTag(caseComponent, lStatusRootTag, lStatusContentTag, lCacId, lDataTagId, lStatusTagId, lTagContainerForNotesId, lNoteTitle, aContent);
					if (lStatusTag != null) {
						saveNoteTag(caseComponent, lStatusRootTag, lStatusTag);
					}
				}
			}
		}
	}

	/**
	 * Gets note tag. If it does not exist creates one.
	 *
	 * @param aStatusRootTag the a status root tag
	 * @param aStatusContentTag the a status content tag
	 * @param aExistsTagForNotes the a exists tag for notes
	 * @param aCacId the a cac id
	 * @param aDataTagId the a data tag id
	 * @param aStatusTagId the a status tag id
	 *
	 * @return the note tag
	 */
	protected IXMLTag getNoteTag(IXMLTag aStatusRootTag, IXMLTag aStatusContentTag, boolean aExistsTagForNotes, String aCacId,
			String aDataTagId, String aStatusTagId) {
		IXMLTag lStatusTag = null;
		for (IXMLTag lTempStatusTag : aStatusContentTag.getChilds("note")) {
			if (lTempStatusTag.getAttribute(AppConstants.keyRefcacid).equals(aCacId)) {
				boolean lOk = ((!aExistsTagForNotes) &&
					(lTempStatusTag.getAttribute(AppConstants.keyRefstatusid).equals("-1")) &&
					(lTempStatusTag.getAttribute(AppConstants.keyRefdataid).equals("-1")));
				if (!lOk) {
					lOk = ((!aStatusTagId.equals("")) &&
						(lTempStatusTag.getAttribute(AppConstants.keyRefstatusid).equals(aStatusTagId)));
				}
				if (!lOk) {
					lOk = ((!aDataTagId.equals("")) &&
						(lTempStatusTag.getAttribute(AppConstants.keyRefdataid).equals(aDataTagId)));
				}
				if (lOk)
					lStatusTag = lTempStatusTag;
			}
		}
		return lStatusTag;
	}

	/**
	 * Creates note tag.
	 *
	 * @param aCaseComponent the a case component
	 * @param aStatusRootTag the a status root tag
	 * @param aStatusContentTag the a status content tag
	 * @param aCacId the a cac id
	 * @param aDataTagId the a data tag id
	 * @param aStatusTagId the a status tag id
	 * @param aContent the a content
	 *
	 * @return the note tag
	 */
	protected IXMLTag createNoteTag(IECaseComponent aCaseComponent, IXMLTag aStatusRootTag, IXMLTag aStatusContentTag, String aCacId,
			String aDataTagId, String aStatusTagId, String aTagContainerForNotesId, String aTitle, String aContent) {
		IXMLTag lStatusTag = CDesktopComponents.sSpring().getXmlManager().newNodeTag(aCaseComponent.getEComponent().getXmldefinition(), "note");
		lStatusTag.setAttribute(AppConstants.keyRefcacid, aCacId);
		lStatusTag.setAttribute(AppConstants.keyRefdataid, aDataTagId);
		lStatusTag.setAttribute(AppConstants.keyRefstatusid, aStatusTagId);
		lStatusTag.setAttribute("refcontainerid", aTagContainerForNotesId);
		lStatusTag.getChild("pid").setValue(aTitle);
		lStatusTag.getChild("text").setValue(CDesktopComponents.sSpring().escapeXML(aContent));
		List<String[]> lErrors = new ArrayList<String[]>();
		CDesktopComponents.sSpring().getXmlManager().newChildNode(aStatusRootTag, lStatusTag, aStatusContentTag, lErrors);
		return lStatusTag;
	}

	/**
	 * Saves note tag.
	 *
	 * @param aCaseComponent the a case component
	 * @param aStatusRootTag the a status root tag
	 * @param aStatusTag the a status tag
	 */
	protected void saveNoteTag(IECaseComponent aCaseComponent, IXMLTag aStatusRootTag, IXMLTag aStatusTag) {
		// NOTE Notes are saved without calling CDesktopComponents.sSpring().setRunTagStatus, because they are user generated content,
		// not related to any data tag. setRunTagStatus is meant to check script, but script does not work on
		// user generated content.
		if (aCaseComponent != null) {
			CDesktopComponents.sSpring().setRunTagStatusValue(aStatusTag, AppConstants.statusKeySaved, AppConstants.statusValueTrue);
			CDesktopComponents.sSpring().getSLogHelper().logSetRunTagAction("setruntagstatus", AppConstants.statusTypeRunGroup, CDesktopComponents.sSpring().getXmlManager().unescapeXML(aCaseComponent.getName()), aStatusTag.getName(),
					aStatusTag.getChildValue("pid"), AppConstants.statusKeySaved, AppConstants.statusValueTrue, false);
			// save status
			CDesktopComponents.sSpring().setSaveRunGroupCaseComponentStatusRootTag(CDesktopComponents.sSpring().getRunGroup(), aCaseComponent, aStatusRootTag);
		}
	}

	/**
	 * Sets the content of the note component for the given parameters.
	 *
	 * @param aCaseComponentForNotes the a case component for notes
	 * @param aTagForNotes the a tag for notes
	 */
	public void setContent(IECaseComponent aCaseComponentForNotes, IXMLTag aTagForNotes) {
		if (aCaseComponentForNotes == null) {
			return;
		}
		if (!isCaseComponentSwitched()) {
			saveContent();
		}
		caseComponentForNotes = aCaseComponentForNotes;
		String lCaseComponentName = caseComponentForNotes.getName();
		String lShowName = CDesktopComponents.sSpring().getCaseComponentRoleName("", lCaseComponentName);
		if (!lShowName.equals("")) {
			lCaseComponentName = lShowName;
		}
		tagForNotes = aTagForNotes;
		tagContainerForNotes = null;
		noteTitle = "";
		Label lLabel = (Label) CDesktopComponents.vView().getComponent(getId()+"Title");
		if (lLabel != null) {
			if (caseComponentForNotes != null) {
				//TODO Components should be responsible for giving note titles
				String lComponentCode = caseComponentForNotes.getEComponent().getCode();
				if (lComponentCode.equals("locations") || lComponentCode.equals("navigation")) {
					if (tagForNotes == null)
						// show navigator name
						noteTitle = lCaseComponentName;
					else
						// only show location name
						noteTitle = noteTitle + CDesktopComponents.sSpring().getXmlManager().getTagKeyValues(tagForNotes, tagForNotes.getDefTag().getAttribute(AppConstants.defKeyKey));
				}
				else {
					if (((lComponentCode.equals("conversations")) || (lComponentCode.equals("assessments"))) && (tagForNotes != null)) {
						String lTagName = "";
						if (lComponentCode.equals("conversations"))
							lTagName = "conversation";
						if (lComponentCode.equals("assessments"))
							lTagName = "assessment";
						IXMLTag lParentTag = tagForNotes;
						while ((lParentTag != null) && (!lParentTag.getName().equals(lTagName)))
							lParentTag = lParentTag.getParentTag();
						if (lParentTag != null) {
							if (tagForNotes.getName().equals(lTagName)) {
								// only show conversation or assessment name
								String lName = tagForNotes.getChildValue("name");
								if (lName.equals("") && lTagName.equals("conversation")) {
									// otherwise no notes can be made by conversation
									lName = tagForNotes.getChildValue("pid");
								}
								noteTitle = CDesktopComponents.sSpring().unescapeXML(lName);
							}
							else {
								// show conversation or assessment name and tag name
								tagContainerForNotes = lParentTag;
								noteTitle = CDesktopComponents.sSpring().unescapeXML(tagContainerForNotes.getChildValue("name"));
								noteTitle = noteTitle + noteTitleSeparator + CDesktopComponents.sSpring().getXmlManager().getTagKeyValues(tagForNotes, tagForNotes.getDefTag().getAttribute(AppConstants.defKeyKey));
							}
						}
						else
							noteTitle = lCaseComponentName;
					}
					else if (lComponentCode.equals("tutorial") || lComponentCode.equals("ispot")) {
						// show case component name
						noteTitle = lCaseComponentName;
						if (tagForNotes != null) {
							// show tag name too
							noteTitle = noteTitle + noteTitleSeparator + CDesktopComponents.sSpring().unescapeXML(tagForNotes.getChildValue("shorttitle"));
						}
					}
					else {
						// show case component name
						noteTitle = lCaseComponentName;
						if (tagForNotes != null) {
							// show tag name too
							noteTitle = noteTitle + noteTitleSeparator + CDesktopComponents.sSpring().getXmlManager().getTagKeyValues(tagForNotes, tagForNotes.getDefTag().getAttribute(AppConstants.defKeyKey));
						}
					}
				}
			}
			String lValue = CDesktopComponents.vView().getLabel("run_note.title");
			lValue = lValue + noteTitleSeparator + "'" + noteTitle + "'";
			if (lValue.length() > maxStringLength) {
				lLabel.setTooltiptext(lValue);
				lValue = lValue.substring(0, maxStringLength) + "...";
			}
			lLabel.setValue(lValue);
		}
		Textbox lTextbox = (Textbox) CDesktopComponents.vView().getComponent(getId()+"NoteField");
		if (lTextbox != null) {
			lTextbox.setValue(getContent());
			lTextbox.setFocus(true);
		}
	}

	/**
	 * Saves the current content of the note component.
	 */
	public void saveContent() {
		// if content changed, save it
		Textbox lTextbox = (Textbox) CDesktopComponents.vView().getComponent(getId()+"NoteField");
		if (lTextbox != null) {
			String lOldContent = getContent();
			String lNewContent = lTextbox.getValue();
			if (!lNewContent.equals(lOldContent)) {
				setContent(lNewContent);
			}
		}
	}

	/**
	 * Is called by end note close box, saves the note and restores the status of the run choice area.
	 *
	 * @param aObserved the observed object, it should have interface CIObserved implemented
	 * @param aAction the action
	 * @param aStatus the status object, can be String or other class
	 */
	@Override
	public void observedNotify(CIObserved aObserved, String aAction,
			Object aStatus) {
		if (aAction.equals("endNote")) {
			if (!isCaseComponentSwitched()) {
				saveContent();
			}
		}
	}

	/**
	 * Handles status change due to firing of script actions.
	 *
	 * @param aTriggeredReference the a triggered reference
	 */
	public void handleStatusChange(STriggeredReference aTriggeredReference) {
		if (aTriggeredReference.getCaseComponent() != null && aTriggeredReference.getDataTag() == null &&
				(aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyPresent) || aTriggeredReference.getStatusKey().equals(AppConstants.statusKeyAccessible))) {
			//xml component present or accessible change
			setVisible(false);
		}
	}

	/**
	 * Gets the note main title.
	 * 
	 * @param aNoteTitle the a note title
	 *
	 * @return the note main title
	 */
	public String getNoteMainTitle(String aNoteTitle) {
		int lIndex = aNoteTitle.indexOf(noteTitleSeparator);
		if (lIndex < 0) {
			return aNoteTitle;
		}
		return aNoteTitle.substring(0, lIndex);
	}

	/**
	 * Gets the note main title.
	 *
	 * @return the note main title
	 */
	public String getNoteMainTitle() {
		return getNoteMainTitle(noteTitle);
	}

	/**
	 * Gets the note sub title.
	 * 
	 * @param aNoteTitle the a note title
	 *
	 * @return the note sub title
	 */
	public String getNoteSubTitle(String aNoteTitle) {
		int lIndex = aNoteTitle.indexOf(noteTitleSeparator);
		if (lIndex < 0) {
			return "";
		}
		return aNoteTitle.substring(lIndex + noteTitleSeparator.length());
	}

	/**
	 * Gets the note sub title.
	 *
	 * @return the note sub title
	 */
	public String getNoteSubTitle() {
		return getNoteSubTitle(noteTitle);
	}

}
