/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

import org.zkoss.util.media.Media;
import org.zkoss.zul.Treeitem;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.IAppManager;
import nl.surf.emergo.business.IRunAccountManager;
import nl.surf.emergo.business.IRunGroupAccountManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IECaseComponentRole;
import nl.surf.emergo.domain.IEMail;
import nl.surf.emergo.domain.IERunAccount;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SStatustagEnrichment;

/**
 * The Class CRunMailTree.
 */
public class CRunMailTreeClassic extends CRunTreeClassic {

	private static final long serialVersionUID = 7731818122093029494L;

	/**
	 * Instantiates a new c run mail tree.
	 * 
	 * @param aId the a id
	 * @param aCaseComponent the mail case component
	 * @param aRunComponent the a run component, the ZK mail component
	 */
	public CRunMailTreeClassic(String aId, IECaseComponent aCaseComponent, CRunComponentClassic aRunComponent) {
		super(aId, aCaseComponent, aRunComponent);
		tagopenednames = "inmailpredef,inmailhelp,outmailpredef,outmailhelp";
		setRows(23);
		update();
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTree#getRunComponentHelper()
	 */
	@Override
	public CRunComponentHelperClassic getRunComponentHelper() {
		return new CRunMailHelperClassic(this, tagopenednames, caseComponent, runComponent);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunTree#doTreeitemAction(org.zkoss.zul.Treeitem)
	 */
	@Override
	public void doTreeitemAction(Treeitem aTreeitem) {
		IXMLTag tag = getContentItemTag(aTreeitem);
		if (tag == null)
			return;
		String lName = tag.getName();
		if (lName.equals("map"))
			return;
		CRunComponentHelperClassic lTreeHelper = getRunComponentHelper();
		String lBody = CDesktopComponents.sSpring().unescapeXML(lTreeHelper.getNodeChildValue(tag, "richtext"));
		String lPcTag = "&lt;pc&gt;";
		int lIndex = lBody.indexOf(lPcTag);
		while (lIndex >= 0) {
			String lNewBody = "";
			if (lIndex > 0)
				lNewBody = lNewBody + lBody.substring(0, lIndex);
			lNewBody = lNewBody + CDesktopComponents.sSpring().getRunGroup().getName();
			if ((lIndex + lPcTag.length()) < lBody.length())
				lNewBody = lNewBody + lBody.substring(lIndex + lPcTag.length(), lBody.length());
			lBody = lNewBody;
			lIndex = lBody.indexOf(lPcTag);
		}
		Map<String,Object> lParams = new HashMap<String,Object>();
		lParams.put("item", tag);
		lParams.put("body", lBody);
		lParams.put("casecomponent", caseComponent);
		lParams.put("runcomponent", runComponent);
		CDesktopComponents.vView().modalPopupWithoutWaiting(VView.v_run_mail, null, lParams, "center");
	}

	/**
	 * Called when new mail is sent. A new mail can be either send by a tutor or
	 * a student.
	 * 
	 * @param aDataTagId the a data tag id
	 * @param aPersonName the a person name
	 * @param aSubject the a subject
	 * @param aBody the a body
	 * @param aAttachments the a attachments
	 */
	public void mailIsSent(String aDataTagId,
			String aPersonName, String aSubject, String aBody, List<Media> aAttachments) {
		if (CDesktopComponents.sSpring().isTutorRun())
			pMailIsSentByTutor(aDataTagId,aPersonName,aSubject,aBody,aAttachments);
		else
			pMailIsSent(aDataTagId,aPersonName,aSubject,aBody,aAttachments);
	}

	/**
	 * Sends mail send by student. Sends real email notification to tutor(s).
	 * And if applicable sends mail to other Emergo users, playing characters.
	 * 
	 * @param aDataTagId the a data tag id
	 * @param aPersonName the a person name
	 * @param aSubject the a subject
	 * @param aBody the a body
	 * @param aAttachments the a attachments
	 */
	private void pMailIsSent(String aDataTagId,
			String aPersonName, String aSubject, String aBody, List<Media> aAttachments) {
		// set status sent to true: new statustag is created because of sent
		// statustag is returned, but without status childs
		// create clone of datatag, so datatag template status is not set.	
		IXMLTag lDataTag = CDesktopComponents.sSpring().getTag(caseComponent, aDataTagId);
		IXMLTag lStatusTag = CDesktopComponents.sSpring().newRunNodeTag(lDataTag.getName(), lDataTag.getAttribute(AppConstants.keyId), null, null);
		addReceiver(lStatusTag, aPersonName);
		addSubject(lStatusTag, aSubject);
		addBody(lStatusTag, aBody);
		addAttachments(lStatusTag,aAttachments);
		SStatustagEnrichment lStatustagEnrichment = new SStatustagEnrichment(); 
		lStatustagEnrichment.setStatusChildTags(lStatusTag.getChild(AppConstants.statusElement).getChildTags());
		lStatusTag = CDesktopComponents.sSpring().setRunTagStatus(caseComponent,
				aDataTagId, "", AppConstants.statusKeySent,
				AppConstants.statusValueTrue, lStatustagEnrichment, true, null, getRunStatusType(), true, false);
//		maybe add something for shared component
		if (lStatusTag == null)
			return;
		CRunComponentHelperClassic lTreeHelper = getRunComponentHelper();
		// if composite rungroup get changes from other online rungroupaccounts
		lTreeHelper.updateWithExternalTags();
		// save status tag
		CDesktopComponents.sSpring().setRunTag(caseComponent, lStatusTag, getRunStatusType());
		// render new treeitem
		IXMLTag lRootTag = CDesktopComponents.sSpring().getXmlDataPlusRunStatusTree(caseComponent, getRunStatusType());
		String lInbox = "";
		if (lDataTag != null) {
			lInbox = lDataTag.getChildValue("inbox");
			// setRunTagStatus with statusKeySent can generate script that will render mailtree again, so new situation will be already present
			// so check if datatag not already present within tree
			//NOTE use data tag of status tag that is created in SSpring method setRunTagStatus to render the new tree item
			IXMLTag lNewDataTag = lStatusTag.getDataTag();
			boolean lPresentInTree = (getContentItem(lNewDataTag.getAttribute(AppConstants.keyId)) != null);
			if (!lPresentInTree) {
				// only add if not present
				addTreeitem(lNewDataTag.getParentTag(),lNewDataTag);
			}
			// if composite rungroup send changes to other online rungroupaccounts
			lTreeHelper.addExternalUpdateTag("newchild", lDataTag.getParentTag(), lDataTag);
			// send notification to tutor(s)
			sendTutorNotification(lDataTag, aPersonName, aSubject);
		}
		
		// check if pc's should get e-message and send it
		// save status tag
		CRunMailClassic lMail = (CRunMailClassic)this.runComponent;
		if (lMail == null)
			return;
		IXMLTag lOriginalDataTag = CDesktopComponents.sSpring().getXmlManager().getTagById(lRootTag, aDataTagId);
		List<IERunGroup> lToRunGroups = lMail.getToRunGroups(lOriginalDataTag);
		if (lToRunGroups == null || lToRunGroups.size() == 0)
			return;
		List<Integer> lRugIds = new ArrayList<Integer>();
		for (IERunGroup lToRunGroup : lToRunGroups) {
			IXMLTag lClonedStatusTag = CDesktopComponents.sSpring().getXmlManager().copyTag(lStatusTag, lStatusTag.getParentTag());
			// rename outmail to inmail
			if (lClonedStatusTag.getName().equals("outmailpredef"))
				lClonedStatusTag.setName("inmailpredef");
			if (lClonedStatusTag.getName().equals("outmailhelp"))
				lClonedStatusTag.setName("inmailhelp");
			lClonedStatusTag.setAttribute(AppConstants.updateStatusKey, AppConstants.statusKeySent);
			lClonedStatusTag.setAttribute(AppConstants.updateStatusValue, AppConstants.statusValueTrue);
			lClonedStatusTag.setAttribute(AppConstants.updateCheckScript, AppConstants.statusValueTrue);
			// empty refdataid, because for other pc's the mail should be received within the content root
			lClonedStatusTag.setAttribute(AppConstants.keyRefdataid, "");
			// add sendername to status
			addSender(lClonedStatusTag, CDesktopComponents.sSpring().getRunGroup().getName());
			// copy attachments for lClonedStatusTag
			removeAttachments(lClonedStatusTag);
			addAttachments(lClonedStatusTag,aAttachments);
			// add inbox
			addInbox(lClonedStatusTag, lInbox);
			int lCacIdForReceiver = pGetCacIdForReceiver(caseComponent, lToRunGroup);
			if (lCacIdForReceiver != 0) {
				CDesktopComponents.sSpring().getSUpdateHelper().addUpdateRunTag(lToRunGroup.getRugId(), lCacIdForReceiver, lClonedStatusTag, getRunStatusType(), false);
				if (!lRugIds.contains(lToRunGroup.getRugId()))
					lRugIds.add(lToRunGroup.getRugId());
			}
		}
		// send notification to receivers of e-message
		String lBccs = "";
		List<IERunGroupAccount> lRunGroupAccounts = ((IRunGroupAccountManager)CDesktopComponents.sSpring().getBean("runGroupAccountManager")).getAllRunGroupAccountsByRugIds(lRugIds);
		for (IERunGroupAccount lRunGroupAccount : lRunGroupAccounts) {
			String lEmail = lRunGroupAccount.getEAccount().getEmail();
			if (!lEmail.equals("") && ("," + lBccs + ",").indexOf("," + lEmail + ",") < 0) {
				if (!lBccs.equals(""))
					lBccs += ",";
				lBccs += lEmail;
			}
		}
		if (!lBccs.equals(""))
			sendNotification(lDataTag, aPersonName, aSubject, lBccs);
	}

	/**
	 * P get cac id for receiver. Gets the cacid of the mail component of the receiving rungroup.
	 * In principle this could be another component then the component used to send
	 * the mail.
	 *
	 * @param aCaseComponentForSender the a case component for sender
	 * @param aRunGroupReceiver the a run group receiver
	 *
	 * @return the int
	 */
	private int pGetCacIdForReceiver(IECaseComponent aCaseComponentForSender, IERunGroup aRunGroupReceiver) {
		String lComponentCode = aCaseComponentForSender.getEComponent().getCode();
		List<IECaseComponentRole> lCaseComponentRolesForReceiver = 
				CDesktopComponents.sSpring().getCaseComponentRolesByCarId(aRunGroupReceiver.getECaseRole().getCarId());
		List<IECaseComponent> lCaseComponents = new ArrayList<IECaseComponent>(); 
		for (IECaseComponentRole lCaseComponentRoleForSender : lCaseComponentRolesForReceiver) {
			// receiver may have casecomponents that are not present in sender's run (perhaps even other mail component)
			IECaseComponent lCaseComponent = CDesktopComponents.sSpring().getCaseComponent(lCaseComponentRoleForSender.getCacCacId(), false);
			if (lCaseComponent != null) {
				if (lCaseComponent.getEComponent().getCode().equals(lComponentCode)) {
					lCaseComponents.add(lCaseComponent);
				}
			}
		}
		if (lCaseComponents.size() == 1) {
			return lCaseComponents.get(0).getCacId();
		}
		if (lCaseComponents.size() > 1) {
			//NOTE if more mail components then send to component with same name
			for (IECaseComponent lCaseComponent : lCaseComponents) {
				if (lCaseComponent.getName().equals(aCaseComponentForSender.getName())) {
					return lCaseComponent.getCacId();
				}
			}
			//NOTE if not same name then pick first one
			return lCaseComponents.get(0).getCacId();
		}
		return 0;
	}

	/**
	 * Sends mail send by tutor.
	 * 
	 * @param aDataTagId the a data tag id
	 * @param aPersonName the a person name
	 * @param aSubject the a subject
	 * @param aBody the a body
	 * @param aAttachments the a attachments
	 */
	private void pMailIsSentByTutor(String aDataTagId,
			String aPersonName, String aSubject, String aBody, List<Media> aAttachments) {
		// set status sent to true: new statustag is created because of sent
		// statustag is returned, but without status childs
		// create clone of datatag, so datatag template status is not set.	
		IXMLTag lDataTag = CDesktopComponents.sSpring().getTag(caseComponent, aDataTagId);
		IXMLTag lStatusTag = CDesktopComponents.sSpring().newRunNodeTag(lDataTag.getName(), lDataTag.getAttribute(AppConstants.keyId), null, null);
		if (lStatusTag == null)
			return;
		CDesktopComponents.sSpring().addSentStatusChilds(caseComponent, lDataTag, lStatusTag);
		addSubject(lStatusTag, aSubject);
		addBody(lStatusTag, aBody);
		addAttachments(lStatusTag,aAttachments);
		// save status tag
		IXMLTag lClonedStatusTag = CDesktopComponents.sSpring().getXmlManager().copyTag(lStatusTag, lStatusTag.getParentTag());
		lClonedStatusTag.setAttribute(AppConstants.updateStatusKey, AppConstants.statusKeySent);
		lClonedStatusTag.setAttribute(AppConstants.updateStatusValue, AppConstants.statusValueTrue);
		lClonedStatusTag.setAttribute(AppConstants.updateCheckScript, AppConstants.statusValueTrue);
		CDesktopComponents.sSpring().getSUpdateHelper().addUpdateRunTag(CDesktopComponents.sSpring().getRunGroup().getRugId(), caseComponent.getCacId(), lClonedStatusTag, getRunStatusType(), false);
	}

	/**
	 * Adds receiver child to aStatusTag.
	 * 
	 * @param aStatusTag the a status tag
	 * @param aPersonName the a person name
	 */
	private void addReceiver(IXMLTag aStatusTag, String aPersonName) {
		// add person as childtag
		IXMLTag lStatusStatusTag = aStatusTag.getChild(AppConstants.statusElement);
		List<IXMLTag> lXmlChildTags = lStatusStatusTag.getChildTags();
		IXMLTag lChildTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag("receivername", aPersonName);
		lChildTag.setParentTag(lStatusStatusTag);
		lXmlChildTags.add(lChildTag);
	}

	/**
	 * Adds subject child to aStatusTag.
	 * 
	 * @param aStatusTag the a status tag
	 * @param aSubject the a subject
	 */
	private void addSubject(IXMLTag aStatusTag, String aSubject) {
		if (!aSubject.equals("")) {
			// add subject as childtag
			IXMLTag lStatusStatusTag = aStatusTag.getChild(AppConstants.statusElement);
			List<IXMLTag> lXmlChildTags = lStatusStatusTag.getChildTags();
			IXMLTag lChildTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag("title", aSubject);
			lChildTag.setParentTag(lStatusStatusTag);
			lXmlChildTags.add(lChildTag);
		}
	}

	/**
	 * Adds body child to aStatusTag.
	 * 
	 * @param aStatusTag the a status tag
	 * @param aBody the a body
	 */
	private void addBody(IXMLTag aStatusTag, String aBody) {
		// add richtext as childtag
		IXMLTag lStatusStatusTag = aStatusTag.getChild(AppConstants.statusElement);
		List<IXMLTag> lXmlChildTags = lStatusStatusTag.getChildTags();
		IXMLTag lChildTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag("richtext", aBody);
		lChildTag.setParentTag(lStatusStatusTag);
		lXmlChildTags.add(lChildTag);
	}

	/**
	 * Adds attachment childs to aStatusTag.
	 * 
	 * @param aStatusTag the a status tag
	 * @param aAttachments the a attachments
	 */
	private void addAttachments(IXMLTag aStatusTag, List<Media> aAttachments) {
		// add attachments as node childtags
		IXMLTag lStatusStatusTag = aStatusTag.getChild(AppConstants.statusElement);
		List<IXMLTag> lXmlChildTags = lStatusStatusTag.getChildTags();
		for (Media lMedia : aAttachments) {
			String lBlobtype = AppConstants.blobtypeDatabase;
			String lBlobId = CDesktopComponents.sSpring().getSBlobHelper().setBlobMedia("", lMedia);
			IXMLTag lChildTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag("attachment", "");
			lChildTag.setAttribute(AppConstants.defKeyType, AppConstants.defValueNode);
			lChildTag.setParentTag(lStatusStatusTag);
			lXmlChildTags.add(lChildTag);
			List<IXMLTag> lXmlChildChildTags = lChildTag.getChildTags();
			IXMLTag lChildChildTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag("name", CDesktopComponents.sSpring().escapeXML(lMedia
					.getName()));
			lChildChildTag.setParentTag(lChildTag);
			lXmlChildChildTags.add(lChildChildTag);
			lChildChildTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag("blob", lBlobId);
			lChildChildTag.setAttribute(AppConstants.keyBlobtype, lBlobtype);
			lChildChildTag.setParentTag(lChildTag);
			lXmlChildChildTags.add(lChildChildTag);
		}
	}

	/**
	 * Removes attachments, without clearing blobs.
	 *
	 * @param aStatusTag the a status tag
	 */
	private void removeAttachments(IXMLTag aStatusTag) {
		// add attachments as node childtags
		IXMLTag lStatusStatusTag = aStatusTag.getChild(AppConstants.statusElement);
		List<IXMLTag> lXmlChildTags = lStatusStatusTag.getChildTags();
		for (int i = (lXmlChildTags.size()-1); i>=0; i--) {
			IXMLTag lXmlChildTag = (IXMLTag)lXmlChildTags.get(i);
			if (lXmlChildTag.getName().equals("attachment"))
				lXmlChildTags.remove(i);
		}
	}

	/**
	 * Adds inbox child to aStatusTag.
	 * 
	 * @param aStatusTag the a status tag
	 * @param aInboxName the a inbox name
	 */
	private void addInbox(IXMLTag aStatusTag, String aInboxName) {
		// add inbox as childtag
		IXMLTag lStatusStatusTag = aStatusTag.getChild(AppConstants.statusElement);
		List<IXMLTag> lXmlChildTags = lStatusStatusTag.getChildTags();
		IXMLTag lChildTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag("inbox", aInboxName);
		lChildTag.setParentTag(lStatusStatusTag);
		lXmlChildTags.add(lChildTag);
	}

	/**
	 * Adds sender child to aStatusTag.
	 * 
	 * @param aStatusTag the a status tag
	 * @param aPersonName the a person name
	 */
	private void addSender(IXMLTag aStatusTag, String aPersonName) {
		// add person as childtag
		IXMLTag lStatusStatusTag = aStatusTag.getChild(AppConstants.statusElement);
		List<IXMLTag> lXmlChildTags = lStatusStatusTag.getChildTags();
		IXMLTag lChildTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag("sendername", aPersonName);
		lChildTag.setParentTag(lStatusStatusTag);
		lXmlChildTags.add(lChildTag);
	}

	/**
	 * Sends real email notification to tutor(s) if student sends mail.
	 *
	 * @param aMailTag the a mail tag
	 * @param aPersonName the a person name
	 * @param aSubject the a subject
	 */
	public void sendTutorNotification(IXMLTag aMailTag, String aPersonName, String aSubject) {
		if (aMailTag == null)
			return;
		if (CDesktopComponents.sSpring().isTutorRun())
			// don't send notification if tutor sends mail to student as npc
			return;
		
		int lRunId = CDesktopComponents.sSpring().getRun().getRunId();
		//get all runaccounts in the current run
		IRunAccountManager runAccountManager = (IRunAccountManager)CDesktopComponents.sSpring().getBean("runAccountManager");
		List<IERunAccount> lRunAccounts = runAccountManager.getAllRunAccountsByRunId(lRunId);
		IAccountManager accountManager = (IAccountManager)CDesktopComponents.sSpring().getBean("accountManager");
		String lBccs = "";
		//loop through them
		for (IERunAccount lRunAccount : lRunAccounts) {
			if (lRunAccount.getTutactive()) {
				IEAccount lAccount = lRunAccount.getEAccount();
				// account must still have tutor role
				if (lAccount.getActive() && accountManager.hasRole(lAccount, AppConstants.c_role_tut) &&
						!lAccount.getEmail().equals("")) {
					if (("," + lBccs + ",").indexOf("," + lAccount.getEmail() + ",") < 0) {
						if (!lBccs.equals(""))
							lBccs += ",";
						lBccs += lAccount.getEmail();
					}
				}
			}
		}
		if (!lBccs.equals(""))
			sendNotification(aMailTag, aPersonName, aSubject, lBccs);
	}

	/**
	 * Sends real email notification to tutor(s) if student sends mail.
	 *
	 * @param aMailTag the a mail tag
	 * @param aPersonName the a person name
	 * @param aSubject the a subject
	 * @param aBccs the a bccs, comma separated
	 */
	public void sendNotification(IXMLTag aMailTag, String aPersonName, String aSubject, String aBccs) {
		if (aMailTag == null || aBccs.equals(""))
			return;
		IAppManager appManager = CDesktopComponents.sSpring().getAppManager();
		IAccountManager accountManager = (IAccountManager)CDesktopComponents.sSpring().getBean("accountManager");
		IEMail lMail = null;
		if (aMailTag.getName().equals("outmailpredef"))
			lMail = appManager.getMail(AppConstants.mai_id_outmailpredef_sent);
		else if (aMailTag.getName().equals("outmailhelp"))
			lMail = appManager.getMail(AppConstants.mai_id_outmailhelp_sent);
		else if (aMailTag.getName().equals("inmailpredef"))
			lMail = appManager.getMail(AppConstants.mai_id_inmailpredef_sent);
		else if (aMailTag.getName().equals("inmailhelp"))
			lMail = appManager.getMail(AppConstants.mai_id_inmailhelp_sent);
		if (lMail == null)
			return;
		String lSender = "";
		if (aMailTag.getName().equals("outmailpredef") || aMailTag.getName().equals("outmailhelp"))
			lSender = accountManager.getAccountName(CDesktopComponents.sSpring().getAccountInRun());
		else if (aMailTag.getName().equals("inmailpredef") || aMailTag.getName().equals("inmailhelp"))
			lSender = aPersonName;
		String lSubject2 = aSubject;
		IXMLTag lStatusTag = aMailTag.getChild(AppConstants.statusElement);
		if ((lStatusTag != null) && (lSubject2.equals(""))) {
			lSubject2 = CDesktopComponents.sSpring().unescapeXML(lStatusTag.getChildValue("title"));
			if (lSubject2.equals(""))
				lSubject2 = CDesktopComponents.sSpring().unescapeXML(aMailTag.getChildValue("title"));
		}
		String lSubject = lMail.getSubject();
		lSubject = lSubject.replaceAll("<sender>",lSender);
		lSubject = lSubject.replaceAll("<subject>",Matcher.quoteReplacement(lSubject2));
		String lBody = lMail.getBody();
		lBody = lBody.replaceAll("<sender>",lSender);
		lBody = lBody.replaceAll("<subject>",Matcher.quoteReplacement(lSubject2));
		lBody = lBody.replaceAll("<runname>",Matcher.quoteReplacement(CDesktopComponents.sSpring().getRun().getName()));
		appManager.sendMail(appManager.getSysvalue(AppConstants.syskey_smtpnoreplysender), null, null, aBccs, lSubject, lBody);
	}

}
