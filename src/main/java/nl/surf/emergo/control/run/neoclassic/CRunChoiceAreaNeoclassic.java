/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.neoclassic;

import java.util.ArrayList;
import java.util.List;

import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CIObserved;
import nl.surf.emergo.control.run.CRunAlert;
import nl.surf.emergo.control.run.CRunArea;
import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.control.run.CRunConversationInteraction;

/**
 * The Class CRunChoiceAreaNeoclassic is a run area in which choices will be made that
 * influence the content of the CRunViewArea.
 * It can contain buttons to choose locations, buttons to open empack case components
 * or questions to be asked during conversations.
 * Further it is used to show alerts or to make contextualised notes.
 */
public class CRunChoiceAreaNeoclassic extends CRunArea {

	private static final long serialVersionUID = -3935745255487769436L;

	/** The run wnd. */
	protected CRunWndNeoclassic runWnd = (CRunWndNeoclassic) CDesktopComponents.vView().getComponent(CControl.runWnd);

	/** The locationbtns, used to choose locations. */
	protected CRunLocationBtnsNeoclassic locationbtns = null;

	/** The note, used to make contextualised notes. */
	protected CRunNoteNeoclassic note = null;

	/** The empackactionbtns, used to open empack case components. */
	protected CRunEmpackActionBtnsNeoclassic empackactionbtns = null;

	/** The conversationinteraction, used to ask questions during conversations. */
	protected CRunConversationInteraction conversationinteraction = null;

	/** The alert, used to show alerts. */
	protected CRunAlert alert = null;

	/** The previous status, used to be able to present a previous state, for instance after closing an alert. */
	protected List<String> previousStatus = new ArrayList<String>();

	/**
	 * Instantiates a new c run choice area.
	 *
	 * @param aId the a id
	 * @param aStatus the a status of the choice area
	 */
	public CRunChoiceAreaNeoclassic(String aId, String aStatus) {
		super(aId);
		setStatus(aStatus);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.run.CRunArea#setStatus(java.lang.String)
	 */
	@Override
	public void setStatus(String aStatus) {
		setStatus(aStatus, null);
	}

	/**
	 * Restores previous status of choice area. In case of alert the alert content
	 * is also restored.
	 */
	public void restoreStatus() {
		String lPreviousStatus = "";
		int lSize = previousStatus.size();
		if (lSize > 1) {
			// get previous state
			lPreviousStatus = previousStatus.get(lSize-2);
			// remove last added state
			previousStatus.remove(lSize-1);
			// set previous state
			pSetStatus(lPreviousStatus, null);
			if ((lPreviousStatus.equals("alert")) && (alert != null))
				alert.restoreContent();
		}
	}

	/**
	 * Sets status and adds it to previous state.
	 * aReferenceObject can be null but otherwise is another ZK component that is
	 * necessary to render the content of the choice area.
	 *
	 * @param aStatus the a status
	 * @param aReferenceObject the a reference object
	 */
	public void setStatus(String aStatus, CRunComponent aReferenceObject) {
		previousStatus.add(aStatus);
		pSetStatus(aStatus, aReferenceObject);
	}

	/**
	 * Sets status of choice area. It is either 'locations', 'empack', 'conversation',
	 * 'alert' or 'note'.
	 * It creates the appropiate ZK components if they don't exist yet.
	 * And for 'locations' or 'empack' scroll arrows are shown if the choice area is
	 * too small to show all choices.
	 * aReferenceObject can be null but otherwise is another ZK component that is
	 * necessary to render the content of the choice area. For instance for 'conversation'
	 * it is equal to the CRunConversations component.
	 *
	 * @param aStatus the a status
	 * @param aReferenceObject the a reference object
	 */
	protected void pSetStatus(String aStatus, CRunComponent aReferenceObject) {
		super.setStatus(aStatus);
		boolean lEmpack = (aStatus.equals("empack"));
		boolean lLocations = (aStatus.equals("locations"));
		boolean lConversation = (aStatus.equals("conversation"));
		boolean lAlert = (aStatus.equals("alert"));
		boolean lNote = (aStatus.equals("note"));
		if ((lEmpack) && (empackactionbtns == null)) {
			empackactionbtns = new CRunEmpackActionBtnsNeoclassic(this, "runEmpackActionBtns");
			appendChild(empackactionbtns);
		}
		if ((lLocations) && (locationbtns == null)) {
			locationbtns = new CRunLocationBtnsNeoclassic(this, "runLocationBtns");
			appendChild(locationbtns);
		}
		if ((lConversation) && (conversationinteraction == null)) {
			conversationinteraction = new CRunConversationInteraction("runConversationInteraction", aReferenceObject, "runConversations");
			appendChild(conversationinteraction);
		}
		if ((lAlert) && (alert == null)) {
			alert = new CRunAlert("runAlert", aReferenceObject);
			appendChild(alert);
		}
		if ((lNote) && (note == null)) {
			note = new CRunNoteNeoclassic("runNote", aReferenceObject);
			appendChild(note);
		}
		if (empackactionbtns != null) {
			empackactionbtns.showArrows(lEmpack);
			empackactionbtns.setVisible(lEmpack);
		}
		if (locationbtns != null) {
			locationbtns.showArrows(lLocations);
			locationbtns.setVisible(lLocations);
		}
		if (conversationinteraction != null)
			conversationinteraction.setVisible(lConversation);
		if (alert != null)
			alert.setVisible(lAlert);
		if (note != null)
			note.setVisible(lNote);
	}

	/**
	 * Is called by other input elements to change state of choice area.
	 *
	 * @param aObserved the observed object, it should have interface CIObserved implemented
	 * @param aAction the action
	 * @param aStatus the status object, can be String or other class
	 */
	@Override
	public void observedNotify(CIObserved aObserved, String aAction, Object aStatus) {
		if (aAction.equals("showEmpackActionButtons"))
			setStatus("empack");
		if (aAction.equals("showLocationButtons"))
			setStatus("locations");
		if (aAction.equals("showConversationInteraction"))
			setStatus("conversation");
		if (aAction.equals("showAlert"))
			setStatus("alert");
		if (aAction.equals("showNote"))
			setStatus("note");
	}

}
