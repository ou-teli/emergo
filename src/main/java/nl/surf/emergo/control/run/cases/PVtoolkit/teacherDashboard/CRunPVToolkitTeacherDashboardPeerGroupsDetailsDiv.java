/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.teacherDashboard;

import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Rows;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.domain.IERunGroup;

public class CRunPVToolkitTeacherDashboardPeerGroupsDetailsDiv extends CRunPVToolkitTeacherDashboardStudentsDetailsDiv {

	private static final long serialVersionUID = -4849802434517189919L;

	@Override
	public void init(IERunGroup actor, boolean editable, List<IXMLTag> commonPeerGroupTags) {
		_classPrefix = "dashboardPeerGroupsDetails";
		
		super.init(actor, editable, commonPeerGroupTags);
	}
	
	@Override
	protected boolean showGroups() {
		return true;
	}

	@Override
	protected String getTitleLabelKey() {
		return "PV-toolkit-dashboard.header.peergroups";
	}
	
	@Override
    protected Rows appendGrid(Component parent) {
    	return pvToolkit.appendGrid(
    			parent, 
    			getId() + "Grid",
    			"position:absolute;left:0px;top:0px;width:1060px;height:480px;overflow:auto;", 
				new boolean[] {true, true, pvToolkit.hasCycleTags(), true, true}, 
    			new String[] {"3%", "30%", "14%", "10%", "50%"}, 
    			new boolean[] {false, true, true, true, true},
    			null,
    			"PV-toolkit-dashboard.column.label.", 
    			new String[] {"", "name", "meanCycle", "development", "progress"});

    }

	@Override
	protected int appendDataToGrid(Rows rows, int rowNumber) {
		return appendDataToGrid(
				rows, 
				rowNumber,
				new boolean[] {true, true, pvToolkit.hasCycleTags(), true, true} 
				);
	}

}
