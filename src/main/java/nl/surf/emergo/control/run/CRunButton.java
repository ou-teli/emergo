/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zk.ui.event.UploadEvent;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefButton;

/**
 * The Class CRunButton, is ancestor of all buttons within the Emergo player.
 */
public class CRunButton extends CDefButton {

	private static final long serialVersionUID = -6180936170928908980L;

	/** The event action. */
	protected String eventAction = "";

	/** The event action status. */
	protected Object eventActionStatus = null;

	public String getClassName() {
		return CDesktopComponents.vView().getRunClassName(className);
	}

	/**
	 * Instantiates a new c run button.
	 */
	public CRunButton() {
	}

	/**
	 * Instantiates a new c run button.
	 * 
	 * @param aId the a id
	 * @param aEventAction the a event action, to be send when button is clicked
	 * @param aEventActionStatus the a event action status, when button is clicked
	 * @param aLabel the a label of the button
	 * @param aZclassExtension the a s class extension for the style class
	 * @param aClientOnClickAction the a client action to be executed when clicked
	 */
	public CRunButton(String aId, String aEventAction,
			Object aEventActionStatus, String aLabel, String aZclassExtension,
			String aClientOnClickAction) {
		if (!aId.equals(""))
			setId(aId);
		setEventAction(aEventAction);
		setEventActionStatus(aEventActionStatus);
		if (!aLabel.equals(""))
			setLabel(aLabel);
		if (!aZclassExtension.equals(""))
			setZclass(getClassName() + aZclassExtension);
		if ((aClientOnClickAction != null) && (!aClientOnClickAction.equals("")))
			setWidgetListener("onClick", aClientOnClickAction);
	}

	/**
	 * Sets the event action.
	 * 
	 * @param aEventAction the new event action
	 */
	public void setEventAction(String aEventAction) {
		eventAction = aEventAction;
	}

	/**
	 * Sets the event action status.
	 * 
	 * @param aEventActionStatus the new event action status
	 */
	public void setEventActionStatus(Object aEventActionStatus) {
		eventActionStatus = aEventActionStatus;
	}

	/**
	 * On click notify observers with eventAction and eventActionStatus.
	 */
	public void onClick() {
		if (StringUtils.isEmpty(this.getUpload())) {
			notifyObservers(eventAction, eventActionStatus);
		}
	}
	
	/**
	 * On click notify observers with eventAction and eventActionStatus.
	 */
	public void onUpload(UploadEvent aUploadEvent) {
		notifyObservers(eventAction, aUploadEvent.getMedia());
	}
	
}
