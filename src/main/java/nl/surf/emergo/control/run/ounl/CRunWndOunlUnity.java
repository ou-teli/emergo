/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.CRunConversations;
import nl.surf.emergo.control.run.CRunTablet;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;

/**
 * The Class CRunWndOunlUnity. The Emergo player window used for Unity player demo.
 */
public class CRunWndOunlUnity extends CRunWndOunl
 {

	private static final long serialVersionUID = -8525539395746563821L;

	/**
	 * Instantiates a new c run unity wnd.
	 */
	public CRunWndOunlUnity() {
		super();
	}

	/**
	 * On action. Is called by several components change the state of the player.
	 * Actions handled here refer to the unity player:
	 * - showNote: notify unity player that note window is opened
	 * - endNote: notify unity player that note window is closed
	 * - onExternalEvent: used to notify conversations component when unity player sends message to outside world 
	 *
	 * @param sender the sender
	 * @param action the action
	 * @param status the status
	 */
	@Override
	public void onAction(String sender, String action, Object status) {
		// unity player is always on top, so hide conversation if unity player is loaded, when tablet is opened.
		// and show it again if the tablet is closed.
		if (action.equals("showAlert")) {
//			hideUnityPlayer();
			super.onAction(sender, action, status);
		}
		else if (action.equals("endAlert")) {
			super.onAction(sender, action, status);
//			showUnityPlayer();
		}
		else if (action.equals("showTablet")) {
			hideUnityPlayer();
			super.onAction(sender, action, status);
		}
		else if (action.equals("endTablet")) {
			super.onAction(sender, action, status);
			showUnityPlayer();
		}

		// communication with unity player
		else if (action.equals("showNote")) {
			super.onAction(sender, action, status);
			emergoEventToClient("unityplayer", "", "open note", "");
		} 
		else if (action.equals("endNote")) {
			boolean noteIsOpened = runNoteBtn.getStatus().equals("selected");
			super.onAction(sender, action, status);
			if (noteIsOpened) {
				emergoEventToClient("unityplayer", "", "close note", "");
			}
		} 
		else if (action.equals("onExternalEvent")) {
			if (sender.equals("unityplayer")) {
				String[] lStatus = (String[]) status;
				String lSender = lStatus[0];
				String lReceiver = lStatus[1];
				String lEvent = lStatus[2];
				String lEventData = lStatus[3];
				String[] lEventDataValues = lEventData.split(",");
				String lDataKey = lEventDataValues[0];
				String lDataValue = lEventDataValues[1];
				if (lDataKey.equals("scaredCount")) {
					String lScaredCount = lDataValue;
					int lCount = Integer.parseInt(lScaredCount);
					if (lCount > 4) {
						lCount = 4;
						emergoEventToClient("unityplayer", "", "exiled", "");
					}
					sendNotification("birdscared" + lScaredCount);
				} 
				else if (lDataKey.equals("startOfGame")) {
					if (lDataValue.equals(AppConstants.statusValueTrue)) {
						sendNotification("startofgame");
						IXMLTag lLocationTag = getLocation(currentLocationTagId);
						if (lLocationTag != null) {
							String lPid = lLocationTag.getChildValue("pid");
							if (lPid.equals("Unity start")) {
								if (sSpring != null)
									emergoEventToClient("unityplayer", "", sSpring.getActiveRunGroupNames(), "");
							}
							else
								emergoEventToClient("unityplayer", "", lLocationTag.getChildValue("pid"), "");
						}
					}
				} 
			};
			super.onAction(sender, action, status);
		} else
			super.onAction(sender, action, status);
	}
	
	protected void hideUnityPlayer() {
		CRunConversations lConversations = (CRunConversations)CDesktopComponents.vView().getComponent("runConversations");
		if (lConversations != null && lConversations.getFragmentView() != null && lConversations.getFragmentView().getSrc().equals(VView.v_run_unity_fr)) {
			emergoEventToClient("unityplayer", "", "hideplayer", "");
		}
	}

	protected void showUnityPlayer() {
		CRunTablet lRunTablet = (CRunTablet)CDesktopComponents.vView().getComponent("runTablet");
//		CRunAlert lRunAlert = (CRunAlert)CDesktopComponents.vView().getComponent("runAlert");
//		if (lRunTablet == null && lRunAlert == null) {
		if (lRunTablet == null) {
			CRunConversations lConversations = (CRunConversations)CDesktopComponents.vView().getComponent("runConversations");
			if (lConversations != null && lConversations.getFragmentView() != null && lConversations.getFragmentView().getSrc().equals(VView.v_run_unity_fr)) {
				emergoEventToClient("unityplayer", "", "showplayer", "");
			}
		}
	}

	/**
	 * Sends notification.
	 *
	 * @param aNotificationPid pid of notification tag
	 */
	public void sendNotification(String aNotificationPid) {
		if (sSpring == null)
			return;
		IECaseComponent lCaseComponent = sSpring.getCaseComponent("notifications", "");
		IXMLTag lTag = null;
		if (lCaseComponent != null) {
			for (IXMLTag lNotification : getCaseComponentRootTagChildTags(lCaseComponent)) {
				String lKey = lNotification.getChildValue("pid");
				if (lKey.equals(aNotificationPid))
					lTag = lNotification;
			}
		}
		if (lTag != null) {
			sSpring.setRunTagStatus(lCaseComponent, lTag, AppConstants.statusKeySent, AppConstants.statusValueTrue, true, AppConstants.statusTypeRunGroup, true, false);
			emergoEventToClient("unityplayer", "", sSpring.unescapeXML(lTag.getChildValue("richtext")), "");
		}
	}

}
