/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl2;

import org.zkoss.zk.ui.Component;

import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefLabel;
import nl.surf.emergo.control.run.CRunArea;
import nl.surf.emergo.control.run.CRunCloseBtn;
import nl.surf.emergo.control.run.ounl.CRunComponentDecoratorOunl;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IEComponent;

/**
 * The Class CRunComponentDecoratorOunl2 is used to decorate OUNL run components like CRunReferencesOunl.
 */
public class CRunComponentDecoratorOunl2 extends CRunComponentDecoratorOunl {

	@Override
	public CRunArea createTitleArea(IECaseComponent aCaseComponent, Component aParent, String aClassNamePrefix) {
		return super.createTitleArea(aCaseComponent, aParent, aClassNamePrefix);
	}

	@Override
	public Component createTitleArea(Component aParent, String aClassNamePrefix, String aTitle) {
		CRunArea lArea = new CRunArea();
		aParent.appendChild(lArea);
		lArea.setZclass(aClassNamePrefix + "_title_area_label_div");
		lArea.appendChild(new CDefLabel(aTitle));
		return lArea;
	}

	@Override
	public CRunArea createCloseArea(IECaseComponent aCaseComponent, Component aParent, String aClassNamePrefix, String aIdPrefix, Object aActionStatus) {
		CRunArea lCloseArea = createCloseArea(aCaseComponent, aParent, aClassNamePrefix, aIdPrefix, aActionStatus, "");
		if (isCaseComponentOnTablet(aCaseComponent)) {
			CDefLabel lLabel = new CDefLabel(CDesktopComponents.vView().getLabel("run_tabletapp.home"));
			lCloseArea.appendChild(lLabel);
			lLabel.setZclass(aClassNamePrefix + "_home_label");
			
			CRunCloseBtn lButton = createNewTabletAppCloseTabletButton(aIdPrefix + "TabletCloseBtn", "active", "endComponentAndTablet", aActionStatus, "");
			lButton.registerObserver(CControl.runWnd);
			lCloseArea.appendChild(lButton);
		}
		return lCloseArea;
	}

	/**
	 * Is case component situated on tablet?
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return boolean
	 */
	protected boolean isCaseComponentOnTablet(IECaseComponent aCaseComponent) {
		IEComponent lComponent = aCaseComponent.getEComponent();
		if (lComponent.getCode().equals("tasks")) {
			//TODO tasks should have parent empack, but is not defined within db.
			return true;
		}
		IEComponent lParent = null;
		if (lComponent.getComComId() > 0) {
			lParent = CDesktopComponents.sSpring().getComponent("" + lComponent.getComComId());
		}
		if (lParent != null && lParent.getCode().equals("empack")) {
			return true;
		}
		return false;
	}

	@Override
	public CRunCloseBtn createNewTabletCloseButton(String aId, String aStatus, String aAction, Object aActionStatus, String aLabel) {
		CRunCloseBtn lButton = new CRunTabletCloseBtnOunl2(aId, aStatus, aAction, aActionStatus, "", aLabel, "");
		lButton.setCanHaveStatusSelected(false);
		return lButton;
	}

	@Override
	public CRunCloseBtn createNewTabletAppCloseButton(String aId, String aStatus, String aAction, Object aActionStatus, String aLabel) {
		CRunCloseBtn lButton = new CRunTabletAppCloseBtnOunl2(aId, aStatus, aAction, aActionStatus, "", aLabel, "");
		lButton.setCanHaveStatusSelected(false);
		return lButton;
	}

	@Override
	public CRunCloseBtn createNewTabletAppCloseTabletButton(String aId, String aStatus, String aAction, Object aActionStatus, String aLabel) {
		CRunCloseBtn lButton = new CRunTabletAppCloseTabletBtnOunl2(aId, aStatus, aAction, aActionStatus, "", aLabel, "");
		lButton.setCanHaveStatusSelected(false);
		return lButton;
	}

}
