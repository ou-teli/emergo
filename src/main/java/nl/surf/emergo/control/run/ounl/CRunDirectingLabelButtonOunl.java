/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.ounl;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.util.Clients;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.CRunDirecting;
import nl.surf.emergo.control.run.CRunVideoDirectingInitBox;
import nl.surf.emergo.view.VView;

/**
 * The Class CRunLabelButton, is ancestor of all label buttons within the Emergo player.
 */
public class CRunDirectingLabelButtonOunl extends CRunLabelButtonOunl {

	private static final long serialVersionUID = 6703922835775806601L;

	/**
	 * Instantiates a new c run label button.
	 */
	public CRunDirectingLabelButtonOunl() {
		super();
	}

	/**
	 * Instantiates a new c run label button.
	 *
	 * @param aId the a id
	 * @param aEventAction the a event action, to be send when button is clicked
	 * @param aEventActionStatus the a event action status, when button is clicked
	 * @param aLabel the a label of the button
	 * @param aZclassExtension the a s class extension for the style class
	 * @param aClientOnClickAction the a client action to be executed when clicked
	 */
	public CRunDirectingLabelButtonOunl(String aId, String aEventAction,
			Object aEventActionStatus, String aLabel, String aZclassExtension,
			String aClientOnClickAction) {
		super(aId, aEventAction, aEventActionStatus, aLabel, aZclassExtension, aClientOnClickAction);
	}

	/**
	 * Shows label button.
	 *
	 */
	@Override
	protected void createLabelButton() {
		super.createLabelButton();
		String lClassName = classNamePlusExtension;
		if (disabled)
			lClassName += "Disabled";
		this.setSclass(lClassName);
	}

	public void onCreate(CreateEvent aEvent) {
		CRunVideoDirectingInitBox runVideoDirectingInitBox = (CRunVideoDirectingInitBox)CDesktopComponents.vView().getComponent("runVideoDirectingInitBox");
		if (runVideoDirectingInitBox == null) {
			return;
		}
		setLabel(runVideoDirectingInitBox.viewlabels.get(runVideoDirectingInitBox.buttoncounter - 1));
		setAttribute("viewNumber", "" + runVideoDirectingInitBox.buttoncounter);
		if (runVideoDirectingInitBox.buttoncounter == 1) {
			setDisabled(true);
		}
		else {
			setStyle("position:relative;top:62px;");
		}
		runVideoDirectingInitBox.buttoncounter++;
	}

	public void onClick(Event aEvent) {
		CRunVideoDirectingInitBox runVideoDirectingInitBox = (CRunVideoDirectingInitBox)CDesktopComponents.vView().getComponent("runVideoDirectingInitBox");
		if (runVideoDirectingInitBox == null) {
			return;
		}
		if (!isDisabled()) {
			VView vView = CDesktopComponents.vView();
			for (int i=1;i<=runVideoDirectingInitBox.numberOfViews;i++) {
				((CRunLabelButtonOunl)vView.getComponent("viewbutton" + i)).setDisabled(false);
			}
			String viewNumber = (String)getAttribute("viewNumber");
			Clients.evalJavaScript("changeView(" + viewNumber + ");");
			((CRunLabelButtonOunl)vView.getComponent("viewbutton" + viewNumber)).setDisabled(true);
			CRunDirecting runDirecting = (CRunDirecting)vView.getComponent("runDirecting");
			runDirecting.setView(viewNumber);
		}
	}

}
