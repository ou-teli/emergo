/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.prepare;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitSubStepsDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.cases.PVtoolkit.feedback.CRunPVToolkitFeedbackRubricDiv;
import nl.surf.emergo.domain.IERunGroup;

public class CRunPVToolkitPrepareRubricRubricDiv extends CRunPVToolkitFeedbackRubricDiv {

	private static final long serialVersionUID = 2638882905295006821L;

	public void init(IERunGroup peer, IXMLTag currentStepTag, IXMLTag currentTag, boolean editable, boolean unfolded) {
		_showRubric = true;
		_classPrefix = "rubric";
		
		super.init(peer, currentStepTag, currentTag, editable, unfolded);
		
		setVisible(true);
	}

	public void update() {
		super.update();

		Div div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{"titleRight"}
		);
		
		CRunPVToolkitSubStepsDiv runPVToolkitSubStepsDiv = (CRunPVToolkitSubStepsDiv)CDesktopComponents.vView().getComponent("prepareSubStepsDiv");
		new CRunPVToolkitDefLabel(div, 
				new String[]{"class", "value"}, 
				new Object[]{"font titleRight", (String)runPVToolkitSubStepsDiv.getAttribute("divTitle")}
		);
		
		Button btn = new CRunPVToolkitDefButton(this, 
				new String[]{"class", "cLabelKey"}, 
				new Object[]{"font pvtoolkitButton subStepReadyButton", "PV-toolkit.ready"}
		);
		addBackToPrepareSubStepssOnClickEventListener(btn, _stepPrefix + "SubStepDiv1");

	}
	
	protected void addBackToPrepareSubStepssOnClickEventListener(Component component, String fromComponentId) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				Component fromComponent = CDesktopComponents.vView().getComponent(fromComponentId);
				if (fromComponent != null) {
					fromComponent.setVisible(false);
				}
				CRunPVToolkitSubStepsDiv toComponent = (CRunPVToolkitSubStepsDiv)CDesktopComponents.vView().getComponent(_stepPrefix + "SubStepsDiv");
				if (toComponent != null) {
					toComponent.update();
					toComponent.setVisible(true);
				}
			}
		});
	}

}
