/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.feedback;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.def.CDefHelper;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitSubStepsDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitCacAndTags;
import nl.surf.emergo.domain.IERunGroup;

public class CRunPVToolkitFeedbackSubStepsDiv extends CRunPVToolkitSubStepsDiv {

	private static final long serialVersionUID = 3519380061062508299L;

	@Override
	protected List<String> getComponentToNavigateToIds(List<IXMLTag> subStepTags) {
		List<String> componentIds = new ArrayList<String>();
		for (IXMLTag subStepTag : subStepTags) {
			String subSteptype = subStepTag.getChildValue("substeptype");
			if (subSteptype.equals(CRunPVToolkit.feedbackGiveFeedbackSubStepType)) {
				componentIds.add("feedbackSubStepDiv");
			}
			else if (subSteptype.equals(CRunPVToolkit.feedbackSendFeedbackSubStepType)) {
				componentIds.add("feedbackSubStepDiv");
			}
		}
		return componentIds;
	}
	
	@Override
	protected List<String> getComponentToInitIds(List<IXMLTag> subStepTags) {
		List<String> componentIds = new ArrayList<String>();
		for (IXMLTag subStepTag : subStepTags) {
			String subSteptype = subStepTag.getChildValue("substeptype");
			if (subSteptype.equals(CRunPVToolkit.feedbackGiveFeedbackSubStepType)) {
				componentIds.add("feedbackSubStepDiv");
			}
			else if (subSteptype.equals(CRunPVToolkit.feedbackSendFeedbackSubStepType)) {
				componentIds.add("feedbackSubStepDiv");
			}
		}
		return componentIds;
	}
	
	@Override
	protected boolean updateSubSteps(List<IXMLTag> subStepTags) {
		List<IXMLTag> sharedPracticeTags = pvToolkit.getFeedbackPracticeTags(_actor, pvToolkit.getPreviousStepTagInCurrentCycle(getStepTagForSubSteps(), CRunPVToolkit.practiceStepType), false, null, null);
		//NOTE no change if no shared practice tags
		if (sharedPracticeTags == null) {
			return false;
		}
		List<Boolean> subStepsDone = getSubStepsDone(subStepTags, sharedPracticeTags);
		boolean lChanged = false;
		for (IXMLTag subStepTag : subStepTags) {
			int lInd = subStepTags.indexOf(subStepTag);
			if (updateSubStepAccessible(subStepTags, subStepsDone, lInd)) {
				lChanged = true;
			}
			if (updateSubStepFinished(subStepTags, subStepsDone, lInd)) {
				lChanged = true;
			}
		}
		return lChanged;
	}

	protected List<Boolean> getSubStepsDone(List<IXMLTag> subStepTags, List<IXMLTag> sharedPracticeTags) {
		return pvToolkit.getFeedbackSubStepsDone(_actor, getStepTagForSubSteps(), subStepTags, sharedPracticeTags);
	}

	@Override
	protected boolean specificStepFinished() {
		List<IXMLTag> sharedPracticeTags = pvToolkit.getFeedbackPracticeTags(_actor, pvToolkit.getPreviousStepTagInCurrentCycle(getStepTagForSubSteps(), CRunPVToolkit.practiceStepType), false, null, null);
		List<IERunGroup> usersThatPractice = pvToolkit.getUsersThatPractice(_actor);

		//NOTE get possible deadline. For step 3 deadline is also start of next step, so users have to wait until deadline has expired
		String currentStepTagId = pvToolkit.getCurrentStepTag().getAttribute(AppConstants.keyId);
		Date deadlineDate = null;
		CRunPVToolkitCacAndTags steps = pvToolkit.getStepMessageTags(_actor);
		for (IXMLTag stepMessageTag : steps.getXmlTags()) {
			String stepTagId = pvToolkit.getStatusChildTagAttribute(stepMessageTag, "steptagid");
			if (stepTagId.equals(currentStepTagId)) {
				deadlineDate = CDefHelper.getDateFromStrYMDAndHMS(
						pvToolkit.getStatusChildTagAttribute(stepMessageTag, AppConstants.statusKeyDeadlineDate), 
						pvToolkit.getStatusChildTagAttribute(stepMessageTag, AppConstants.statusKeyDeadlineTime));
				break;
			}
		}
		
		boolean waitForDeadline = deadlineDate != null && (new Date()).before(deadlineDate);
		
		return sharedPracticeTags.size() >= usersThatPractice.size() && !waitForDeadline;
	}
	
}
