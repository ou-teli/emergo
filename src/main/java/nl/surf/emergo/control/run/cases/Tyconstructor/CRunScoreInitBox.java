/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.Tyconstructor;

import java.util.ArrayList;
import java.util.List;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefBox;
import nl.surf.emergo.domain.IECaseComponent;

public class CRunScoreInitBox extends CDefBox {

	private static final long serialVersionUID = 1883952149218069978L;

	protected String caseComponentName = "";
	
	public List<IXMLTag> getNodeTags(IECaseComponent caseComponent, String nodeTagName, String matchString) {
		List<IXMLTag> foundNodeTags = new ArrayList<IXMLTag>();
		if (caseComponent != null) {
			List<IXMLTag> nodeTags = CDesktopComponents.cScript().getRunGroupNodeTags(caseComponent, nodeTagName);
			for (IXMLTag nodeTag : nodeTags) {
				String nodeTagPid = nodeTag.getChildValue("pid");
				if (nodeTagPid.matches(matchString)) {
					foundNodeTags.add(nodeTag);
				}
			}
		}
		return foundNodeTags;
	}

	public String[] getAnswerData(IECaseComponent caseComponent, String nodeTagName, String matchString) {
		String[] answerdata = new String[3];
		List<IXMLTag> nodeTags = getNodeTags(caseComponent, nodeTagName, matchString);
		List<String> errorMessages = new ArrayList<String>();
		for (IXMLTag nodeTag : nodeTags) {
			boolean correct = false;
			if (nodeTag.getDefChildValue("correct").equals("true")) {
				correct = true;
				answerdata[0] = CDesktopComponents.sSpring().unescapeXML(nodeTag.getChildValue("text"));
			}
			if (CDesktopComponents.sSpring().getCurrentRunTagStatus(caseComponentName, nodeTagName, nodeTag.getChildValue("pid"), AppConstants.statusKeySelected, AppConstants.statusTypeRunGroup, errorMessages).equals("true")) {
				if (correct) {
					answerdata[1] = "thumbs-up.png";
				}
				else {
					answerdata[1] = "thumbs-down.png";
				}
				answerdata[2] = CDesktopComponents.sSpring().unescapeXML(nodeTag.getChildValue("text"));
			}
		}
		return answerdata;
	}

	public List<String[]> getAnswersData(IECaseComponent caseComponent, String nodeTagName, String[] matchStrings) {
		List<String[]> answersdata = new ArrayList<String[]>();
		for (int i=0;i<matchStrings.length;i++) {
			String[] answerdata = getAnswerData(caseComponent, nodeTagName, matchStrings[i]);
			if (answerdata[2] != null) {
				//only if answer is given add answer data
				answersdata.add(answerdata);
			}
		}
		return answersdata;
	}

}
