/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.util.StringUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.STriggeredReference;

/**
 * The Class CRunEditforms is used to show editforms component within the run view area of the emergo player..
 */
public class CRunEditforms extends CRunForms {

	private static final long serialVersionUID = 7602242383099195245L;

	/**
	 * Instantiates a new c run editforms.
	 */
	public CRunEditforms() {
		super("runEditforms", null);
		conversationsFeedbackIdSuffix = "Editforms";
		init();
	}

	/**
	 * Instantiates a new c run editforms.
	 *
	 * @param aId the a id
	 * @param aCaseComponent the case component
	 */
	public CRunEditforms(String aId, IECaseComponent aCaseComponent) {
		super(aId, aCaseComponent);
		conversationsFeedbackIdSuffix = "Editforms";
		init();
	}

	/**
	 * Creates new content component, the tree.
	 *
	 * @return the component
	 */
	@Override
	protected Component newContentComponent() {
		return newContentComponent("runEditformsView");
	}

	/**
	 * Creates view.
	 */
	public void init() {
		formChildTagNames = "section,selector,set,container,plugin";

		Events.postEvent("onInitZulfile", this, null);
	}

	/**
	 * Saves section text.
	 *
	 * @param aSectionTag the a section tag
	 * @param aText the a text
	 */
	public void saveSectionText(IXMLTag aSectionTag, String aText) {
		//NOTE because the text is saved as an attribute value \n has to be replaced by a string constant.
		//Otherwise the text is not reproduced correctly, crlf's are then replaced by spaces.
		aText = aText.replace("\n", AppConstants.statusCrLfReplace);
		setRunTagStatus(getCaseComponent(), aSectionTag, AppConstants.statusKeyString, CDesktopComponents.sSpring().escapeXML(aText), true);
	}

	/**
	 * Saves selection.
	 *
	 * @param aSelectionTag the a selection tag
	 * @param aSelection the a selection, comma separated row numbers of chosen options
	 */
	public void saveSelection(IXMLTag aSelectionTag, String aSelection) {
		setRunTagStatus(getCaseComponent(), aSelectionTag, AppConstants.statusKeyAnswer, aSelection, true);
		setRunTagStatus(getCaseComponent(), aSelectionTag, AppConstants.statusKeySelected, AppConstants.statusValueTrue, true);
		//NOTE if aSelection contains same number of chosen options as the number of correct options, match aSelection with correct options and set statusKeyCorrect.
		//NOTE aSelection and correct options both contain numbers in ascending order.
		//NOTE so if selector input element is singleselect statusKeyCorrect is set at every choice,
		//but if multiselect it is only set if the number of chosen options equals the number of correct options.
		String lCorrectOptions = aSelectionTag.getChildValue("correctoptions");
		if (StringUtils.countOccurrencesOf(aSelection, ",") == StringUtils.countOccurrencesOf(lCorrectOptions, ",")) {
			Pattern p = Pattern.compile(lCorrectOptions, Pattern.DOTALL);
			Matcher m = p.matcher(aSelection);
			setRunTagStatus(getCaseComponent(), aSelectionTag, AppConstants.statusKeyCorrect, "" + m.matches(), true);
		}
	}

	/**
	 * Saves alternative selection.
	 *
	 * @param aAlternativeTag the a alternative tag
	 * @param aSelected the a selected
	 */
	public void saveAlternativeSelection(IXMLTag aAlternativeTag, String aSelected) {
		setRunTagStatus(getCaseComponent(), aAlternativeTag, AppConstants.statusKeySelected, aSelected, true);
		if (aSelected.equals(AppConstants.statusValueTrue)) {
			//if alternative is selected
			IXMLTag lSetTag = aAlternativeTag.getParentTag();
			if (!lSetTag.getChildValue("multiselect").equals(AppConstants.statusValueTrue)) {
				//if single select
				for (IXMLTag lAlternativeTag : getAllItemTags(lSetTag, "alternative")) {
					if (lAlternativeTag != aAlternativeTag && lAlternativeTag.getCurrentStatusAttribute(AppConstants.statusKeySelected).equals(AppConstants.statusValueTrue)) {
						setRunTagStatus(getCaseComponent(), lAlternativeTag, AppConstants.statusKeySelected, AppConstants.statusValueFalse, true);
					}
				}
			}
		}
	}

	@Override
	public String getFeedbackTitle() {
		return CDesktopComponents.vView().getLabel("run_editforms.alert.feedback.title");
	}

	@Override
	public String getDefaultFeedbackText() {
		return CDesktopComponents.vView().getLabel("run_editforms.alert.feedback.defaulttext");
	}

	@Override
	public void handleStatusChange(STriggeredReference aTriggeredReference) {
		Events.postEvent("onHandleStatusChange", this, aTriggeredReference);
	}

	
	/**
	 * Methods originally used in ZUL file.
	 */

	public void onInitZulfile(Event aEvent) {
		String componentStyle = "";
		String url = getBackgroundUrl();
		if (!url.equals("")) {
			componentStyle = "background-image:url('" + url + "');";
		}

		//NOTE showFeedback and showScore are for all forms!
		showFeedback = false;
		showScore = false;
		dataElements = new ArrayList<Hashtable<String,Object>>();
		List<IXMLTag> formTags = getFormTags();
		for (IXMLTag formTag : formTags) {
			Hashtable<String,Object> hFormDataElements = new Hashtable<String,Object>();
			hFormDataElements.put("tag", formTag);
			hFormDataElements.put("title", sSpringHelper.getTagChildValue(formTag, "title", ""));
			hFormDataElements.put("hovertext", sSpringHelper.getTagChildValue(formTag, "hovertext", ""));
			hFormDataElements.put("numberofresits", sSpringHelper.getTagChildValue(formTag, "numberofresits", "-1"));
			hFormDataElements.put("showfeedback", sSpringHelper.getTagStatusChildAttribute(formTag, "showfeedback", "true"));
			if (hFormDataElements.get("showfeedback").equals("true")) {
				showFeedback = true;
			}
			hFormDataElements.put("showscore", sSpringHelper.getTagStatusChildAttribute(formTag, "showscore", "false"));
			if (hFormDataElements.get("showscore").equals("true")) {
				showScore = true;
			}

			//NOTE following list is needed so you can do nested foreach looping! see below.
			List<Hashtable<String,Object>> pieceOrRefpieceDataElementsList = new ArrayList<Hashtable<String,Object>>();
			List<IXMLTag> pieceOrRefpieceTags = getPieceAndRefpieceTags(formTag);
			handlepieceOrRefpieces(pieceOrRefpieceDataElementsList, pieceOrRefpieceTags); 
			hFormDataElements.put("pieces", pieceOrRefpieceDataElementsList);

			//NOTE following list is needed so you can do nested foreach looping! see below.
			List<Hashtable<String,Object>> itemDataElementsList = new ArrayList<Hashtable<String,Object>>();
			//NOTE get all item tags, also non present ones
			List<IXMLTag> itemTags = getAllItemTags(formTag);
			for (IXMLTag itemTag : itemTags) {
				boolean isShowIfRightOrWrong = isShowIfRightOrWrong(itemTag);

				Hashtable<String,Object> hItemDataElements = new Hashtable<String,Object>();
				hItemDataElements.put("tag", itemTag);
				boolean overwrite = true;
				if (itemTag.getName().equals("section")) {
					overwrite = sSpringHelper.getTagStatusChildAttribute(itemTag, "overwrite", "true").equals("true");
					String value = "";
					if (overwrite) {
						value = sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, "string", "");
					}
					if (value.equals("")) {
						value = sSpringHelper.getTagChildValue(itemTag, "defaulttext", "");
					}
					hItemDataElements.put("title", sSpringHelper.getTagChildValue(itemTag, "title", ""));
					//NOTE for flat text \n has to be replaced.
					hItemDataElements.put("flatvalue", value.replace("\n", "<br/>"));
					hItemDataElements.put("value", value);
					hItemDataElements.put("overwrite", "" + overwrite);
					hItemDataElements.put("numberoflines", sSpringHelper.getTagChildValue(itemTag, "numberoflines", "5"));
				}
				if (itemTag.getName().equals("selector")) {
					hItemDataElements.put("title", sSpringHelper.getTagChildValue(itemTag, "title", ""));
					hItemDataElements.put("answer", sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, "answer", ""));
					hItemDataElements.put("options", sSpringHelper.getTagChildValue(itemTag, "options", ""));
					hItemDataElements.put("multiselect", sSpringHelper.getTagChildValue(itemTag, "multiselect", "false"));
					hItemDataElements.put("numberofrows", sSpringHelper.getTagChildValue(itemTag, "numberofrows", "-1"));
				}
				if (itemTag.getName().equals("set")) {
					hItemDataElements.put("title", sSpringHelper.getTagChildValue(itemTag, "title", ""));
					hItemDataElements.put("multiselect", sSpringHelper.getTagChildValue(itemTag, "multiselect", "false"));
					hItemDataElements.put("maxnumberofalternativestoselect", sSpringHelper.getTagChildValue(itemTag, "maxnumberofalternativestoselect", "-1"));
					hItemDataElements.put("childorientation", sSpringHelper.getTagChildValue(itemTag, "childorientation", "vertical"));
					hItemDataElements.put("childcheckposition", sSpringHelper.getTagChildValue(itemTag, "childcheckposition", "left"));
					hItemDataElements.put("items", getSetChildList(itemTag, isShowIfRightOrWrong));
				}
				if (itemTag.getName().equals("container")) {
					hItemDataElements.put("title", sSpringHelper.getTagChildValue(itemTag, "title", ""));
					hItemDataElements.put("hovertext", sSpringHelper.getTagChildValue(itemTag, "hovertext", ""));
					hItemDataElements.put("childorientation", sSpringHelper.getTagChildValue(itemTag, "childorientation", "vertical"));
					hItemDataElements.put("items", getContainerChildList(itemTag, isShowIfRightOrWrong));
				}
				if (itemTag.getName().equals("plugin")) {
					hItemDataElements.put("title", sSpringHelper.getTagChildValue(itemTag, "title", ""));
					hItemDataElements.put("hovertext", sSpringHelper.getTagChildValue(itemTag, "hovertext", ""));
					hItemDataElements.put("childorientation", sSpringHelper.getTagChildValue(itemTag, "childorientation", "vertical"));
					hItemDataElements.put("items", getContainerChildList(itemTag, isShowIfRightOrWrong));
				}

				hItemDataElements.put("styleposition", runWnd.getStylePosition(itemTag));
				hItemDataElements.put("stylewidth", runWnd.getStyleWidth(itemTag));
				hItemDataElements.put("stylesize", runWnd.getStyleSize(itemTag));
				//TODO
				if (overwrite && isShowIfRightOrWrong) {
					hItemDataElements.put("isinputcorrect", isInputCorrect(itemTag));
				}
				else {
					hItemDataElements.put("isinputcorrect", "");
				}
				hItemDataElements.put("present", sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, AppConstants.statusKeyPresent, "true"));

				itemDataElementsList.add(hItemDataElements);
			}
			hFormDataElements.put("items", itemDataElementsList);

			//NOTE following list is needed so you can do nested foreach looping! see below.
			pieceOrRefpieceDataElementsList = new ArrayList<Hashtable<String,Object>>();
			IXMLTag feedbackConditionTag = getCurrentFeedbackConditionTag(formTag);
			pieceOrRefpieceTags = getPieceAndRefpieceTags(feedbackConditionTag);
			handlepieceOrRefpieces(pieceOrRefpieceDataElementsList, pieceOrRefpieceTags); 
			hFormDataElements.put("feedbackpieces", pieceOrRefpieceDataElementsList);

			dataElements.add(hFormDataElements);
		}

		Map<String,Object> propertyMap = new HashMap<String,Object>();
		propertyMap.put("currentEmergoComponent", this);
		propertyMap.put("dataElements", dataElements);
		propertyMap.put("componentStyle", componentStyle);
		propertyMap.put("feedbackButtonUrl", sSpringHelper.stripEmergoPath(getFeedbackButtonUrl()));
		((HtmlMacroComponent)getRunContentComponent()).setDynamicProperty("a_propertyMap", propertyMap);
		((HtmlMacroComponent)getRunContentComponent()).setMacroURI(VView.v_run_editforms_fr);
		getRunContentComponent().setVisible(true);
		
		Events.postEvent("onInitForm", this, null);
		Events.postEvent("onInitFeedback", this, null);
		Events.postEvent("onSetPreviousNext", this, null);
	}

	public void onInitForm(Event aEvent) {
		//get form tag id of last shown form
		String lastShownFormTagId = sSpringHelper.getCurrentStatusComponentStatusChildAttribute(this, "selectedtagid", "0");
		//default form tag to show is in dataElements with index 0
		currentFormIndex = 0;
		if (lastShownFormTagId.equals("0")) {
			//if no last shown form tag id yet, save form tag id of default form
			Events.postEvent("onSaveShownFormTagId", this, null);
		}
		else {
			//determine index within dataElements of last shown form
			for (int i=0;i<dataElements.size();i++) {
				if (((IXMLTag)dataElements.get(i).get("tag")).getAttribute("id").equals(lastShownFormTagId)) {
					currentFormIndex = i;
				}
			}
		}
		//show form indicated by currentFormIndex
		if (currentFormIndex >= 0 && currentFormIndex < dataElements.size()) {
			Component lEditForm = vView.getComponent("editform_" + ((IXMLTag)dataElements.get(currentFormIndex).get("tag")).getAttribute("id"));
			if (lEditForm != null)
				lEditForm.setVisible(true);
		}
	}

	public void onInitFeedback(Event aEvent) {
		if (currentFormIndex >= 0 && currentFormIndex < dataElements.size()) {
			IXMLTag triggeredFeedbackConditionTag = getTriggeredFeedbackConditionTag((IXMLTag)dataElements.get(currentFormIndex).get("tag"));
			if (triggeredFeedbackConditionTag != null) {
				//NOTE only show feedback if it is an instruction, which is indicated by a score of -1
				if (sSpringHelper.getTagChildValue(triggeredFeedbackConditionTag, "score", "-1").equals("-1")) {
					Events.postEvent("onShowFeedback", this, null);
				}
			}
		}
	}
	
	public void onShowFeedback(Event aEvent) {
		if (currentFormIndex >= 0 && currentFormIndex < dataElements.size()) {
			if (showFeedback) {
				IXMLTag currentFormTag = (IXMLTag)dataElements.get(currentFormIndex).get("tag");
				IXMLTag triggeredFeedbackConditionTag = showFeedback(currentFormTag);
				//NOTE score -1 means 'no relevant score yet' because tool instruction is shown, so feedback mechanism is misused to show reactions of npc's.
				//TODO rename feedback in reaction in Java and zul
				if (triggeredFeedbackConditionTag != null) {
					if (!sSpringHelper.getTagChildValue(triggeredFeedbackConditionTag, "score", "-1").equals("-1")) {
						List<Component> editformItems = vView.getComponentsByPrefix("editformItem_");
						List<IXMLTag> handledTags = new ArrayList<IXMLTag>();
						for (Component editformItem : editformItems) {
							//NOTE editformItemOrContainer is used to keep the number of attempts
							Component editformItemOrContainer = getEditformItemOrContainer(editformItem);
							//NOTE editformItemDiv is set visible or not using script
							Component editformItemDiv = getEditformItemDiv(editformItem);
							if (editformItemOrContainer != null && editformItemDiv != null && editformItemDiv.isVisible()) {
								//NOTE only update number of attempts and send onUpdate if form tag is equal to current form tag and editformItemDiv tag is present 
								IXMLTag formTag = getFormTag((IXMLTag)editformItemOrContainer.getAttribute("tag"));
								if (formTag == currentFormTag) {
									if (!handledTags.contains(editformItemOrContainer.getAttribute("tag"))) {
										handledTags.add((IXMLTag)editformItemOrContainer.getAttribute("tag"));
										increaseNumberOfAttempts((IXMLTag)editformItemOrContainer.getAttribute("tag"));
									}
									Events.postEvent("onUpdate", editformItem, null);
								}
							}
						}
					}
					for (Hashtable<String,Object> dataElement : dataElements) {
						//for each form
						Events.postEvent("onUpdate", vView.getComponent("feedbackPieces_" + ((IXMLTag)dataElement.get("tag")).getAttribute("id")), triggeredFeedbackConditionTag);
					}
				}
			}
			else {
				showDefaultFeedback();
			}
		}
	}

	public void onHandleStatusChange(Event aEvent) {
		//NOTE to handle pieces and refpieces call super
		super.onHandleStatusChange(aEvent, "editformItemDiv_");
		STriggeredReference triggeredReference = (STriggeredReference)aEvent.getData();
		if (triggeredReference != null) {
			if (triggeredReference.getDataTag() != null &&
				(triggeredReference.getDataTag().getName().equals("section") ||
				triggeredReference.getDataTag().getName().equals("selector") ||
				triggeredReference.getDataTag().getName().equals("set") ||
				triggeredReference.getDataTag().getName().equals("alternative") ||
				triggeredReference.getDataTag().getName().equals("container") ||
				triggeredReference.getDataTag().getName().equals("text") ||
				triggeredReference.getDataTag().getName().equals("plugin")) &&
				triggeredReference.getStatusKey().equals(AppConstants.statusKeyPresent)) {
				Component lComponent = CDesktopComponents.vView().getComponent("editformItemDiv_" + triggeredReference.getDataTag().getAttribute(AppConstants.keyId));
				if (lComponent == null) {
					lComponent = CDesktopComponents.vView().getComponent("editformItem_" + triggeredReference.getDataTag().getAttribute(AppConstants.keyId));
				}
				if (lComponent != null) {
					lComponent.setVisible(triggeredReference.getStatusValue().equals(AppConstants.statusValueTrue));
				}
			}

			if (triggeredReference.getDataTag() != null &&
				triggeredReference.getDataTag().getName().equals("form") &&
				triggeredReference.getStatusKey().equals(AppConstants.statusKeyCorrect)) {
				Events.postEvent("onSetPreviousNext", this, null);
			}
		}
	}
	
	public void onSaveShownFormTagId(Event aEvent) {
		if (currentFormIndex >= 0 && currentFormIndex < dataElements.size()) {
			sSpringHelper.setRunComponentStatus(this, "selectedtagid", ((IXMLTag)dataElements.get(currentFormIndex).get("tag")).getAttribute("id"), true);
		}
	}
	
	public void onSetPreviousNext(Event aEvent) {
		Component lPrevBtn = vView.getComponent("previousButton_" + getId());
		if (lPrevBtn != null)
			Events.postEvent("onInit", lPrevBtn, currentFormIndex > 0);
		//only show next button if current form ever was filled in correctly
		Component lNextBtn = vView.getComponent("nextButton_" + getId());
		if (lNextBtn != null) {
			if (currentFormIndex >= 0 && currentFormIndex < dataElements.size()) {
				boolean currentFormEverCorrect = sSpringHelper.getStatusTagStatusChildAttributeCount((IXMLTag)dataElements.get(currentFormIndex).get("tag"), "correct", "true") > 0;
				Events.postEvent("onInit", lNextBtn, (currentFormIndex < (dataElements.size() - 1)) && currentFormEverCorrect);
			}
			else {
				Events.postEvent("onInit", lNextBtn, false);
			}
		}
	}

	public void onPreviousForm(Event aEvent) {
		showPreviousOrNextForm("previous");
	}

	public void onNextForm(Event aEvent) {
		showPreviousOrNextForm("next");
	}
	
	public void showPreviousOrNextForm(String previousOrNext) {
		//hide feedback
		hideFeedback();
		//hide previous form
		if (currentFormIndex >= 0 && currentFormIndex < dataElements.size()) {
			Component previousForm = vView.getComponent("editform_" + ((IXMLTag)dataElements.get(currentFormIndex).get("tag")).getAttribute("id"));
			previousForm.setVisible(false);
			if (previousOrNext.equals("previous")) {
				currentFormIndex--;
			}
			else if (previousOrNext.equals("next")) {
				currentFormIndex++;
			}
			//show current form
			Component currentForm = vView.getComponent("editform_" + ((IXMLTag)dataElements.get(currentFormIndex).get("tag")).getAttribute("id"));
			currentForm.setVisible(true);
			Events.postEvent("onUpdate", currentForm, null);
			Events.postEvent("onInitFeedback", this, null);
			Events.postEvent("onSaveShownFormTagId", this, null);
			Events.postEvent("onSetPreviousNext", this, null);
		}
	}

	public List<Hashtable<String,Object>> getSetChildList(IXMLTag setTag, boolean isShowIfRightOrWrong) {
		String childCheckPosition = sSpringHelper.getTagChildValue(setTag, "childcheckposition", "left");
		//NOTE following list is needed so you can do nested foreach looping! see below.
		List<Hashtable<String,Object>> itemDataElementsList = new ArrayList<Hashtable<String,Object>>();
		//NOTE get all item tags, also non present ones
		List<IXMLTag> itemTags = getAllItemTags(setTag, "alternative");
		boolean isSetMaxNumberOfAlternativesSelected = isSetMaxNumberOfAlternativesSelected(setTag);
		for (IXMLTag itemTag : itemTags) {
			Hashtable<String,Object> hItemDataElements = new Hashtable<String,Object>();
			hItemDataElements.put("tag", itemTag);
			hItemDataElements.put("content", sSpringHelper.getTagChildValue(itemTag, "richtext", ""));
			hItemDataElements.put("selected", sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, AppConstants.statusKeySelected, "false"));
			if (isShowIfRightOrWrong) {
				hItemDataElements.put("isinputcorrect", isInputCorrect(itemTag));
			}
			else {
				hItemDataElements.put("isinputcorrect", "");
			}
			hItemDataElements.put("present", sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, AppConstants.statusKeyPresent, "true"));
			hItemDataElements.put("disabled", "" + (isSetMaxNumberOfAlternativesSelected && hItemDataElements.get("selected").equals("false")));
			hItemDataElements.put("childcheckposition", childCheckPosition);
			itemDataElementsList.add(hItemDataElements);
		}
		return itemDataElementsList;
	}

	public List<Hashtable<String,Object>> getContainerChildList(IXMLTag setTag, boolean isShowIfRightOrWrong) {
		//NOTE following list is needed so you can do nested foreach looping! see below.
		List<Hashtable<String,Object>> itemDataElementsList = new ArrayList<Hashtable<String,Object>>();
		//NOTE get all item tags, also non present ones
		List<IXMLTag> itemTags = getAllItemTags(setTag, "text");
		for (IXMLTag itemTag : itemTags) {
			Hashtable<String,Object> hItemDataElements = new Hashtable<String,Object>();
			hItemDataElements.put("tag", itemTag);
			hItemDataElements.put("content", sSpringHelper.getTagChildValue(itemTag, "richtext", ""));
			if (isShowIfRightOrWrong) {
				hItemDataElements.put("isinputcorrect", isInputCorrect(itemTag));
			}
			else {
				hItemDataElements.put("isinputcorrect", "");
			}
			hItemDataElements.put("present", sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, AppConstants.statusKeyPresent, "true"));
			itemDataElementsList.add(hItemDataElements);
		}
		return itemDataElementsList;
	}

	public boolean isSetMultiselect(IXMLTag setTag) {
		return sSpringHelper.getTagChildValue(setTag, "multiselect", "false").equals("true");
	}

	public boolean isSetMaxNumberOfAlternativesSelected(IXMLTag setTag) {
		int maxNumberToSelect =  Integer.parseInt(sSpringHelper.getTagChildValue(setTag, "maxnumberofalternativestoselect", "-1"));
		if (maxNumberToSelect == -1) {
			return false;
		}
		//NOTE get all item tags, also non present ones
		List<IXMLTag> itemTags = getAllItemTags(setTag, "alternative");
		int numberOfSelected = 0;
		for (IXMLTag itemTag : itemTags) {
			if (sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, AppConstants.statusKeySelected, "false").equals("true")) {
				numberOfSelected++;
			}
		}
		return numberOfSelected >= maxNumberToSelect;
	}

	@Override
	public boolean isShowIfRightOrWrong(IXMLTag tag) {
		//get form tag
		IXMLTag formTag = getFormTag(tag);
		if (formTag == null) {
			return false;
		}
		int maxNumber = runWnd.getNumber(sSpringHelper.getTagChildValue(formTag, "maxnumberofattempts", "-1"), -1);
		int number = runWnd.getNumber(sSpringHelper.getCurrentStatusTagStatusChildAttribute(tag, AppConstants.statusKeyNumberofattempts, "0"), 0);
		return maxNumber > -1 && number >= maxNumber; 
	}

	public String isInputCorrect(IXMLTag itemTag) {
		if (itemTag.getName().equals("section")) {
			Pattern p = Pattern.compile(sSpringHelper.getTagChildValue(itemTag, "correctanswer", ""), Pattern.DOTALL);
			Matcher m = p.matcher(sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, "string", ""));
			return "" + m.matches();
		}
		if (itemTag.getName().equals("selector")) {
			Pattern p = Pattern.compile(sSpringHelper.getTagChildValue(itemTag, "correctoptions", ""), Pattern.DOTALL);
			Matcher m = p.matcher(sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, "answer", ""));
			return "" + m.matches();
		}
		if (itemTag.getName().equals("alternative")) {
			boolean correct = sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, "correct", "false").equals("true");
			boolean selected = sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, "selected", "false").equals("true");
			if (selected) {
				return "" + correct;
			}
			else {
				return "";
			}
		}
		if (itemTag.getName().equals("text")) {
			boolean correct = sSpringHelper.getCurrentStatusTagStatusChildAttribute(itemTag, "correct", "false").equals("true");
			return "" + correct;
		}
		if (itemTag.getName().equals("plugin")) {
			boolean correct = sSpringHelper.getCurrentStatusTagStatusChildAttribute(getFormTag(itemTag), "correct", "false").equals("true");
			return "" + correct;
		}

		return "false";
	}

	public Component getEditformItemOrContainer(Component zulItem) {
		//NOTE returns editformItem or for alternatives and texts the parent container
		IXMLTag tag = (IXMLTag)zulItem.getAttribute("tag");
		if (tag == null) {
			return null;
		}
		Component editformItemOrContainer = zulItem;
		if (tag.getName().equals("alternative") || tag.getName().equals("text")) {
			while (editformItemOrContainer != null && (editformItemOrContainer.getAttribute("tag") == null || editformItemOrContainer.getAttribute("tag") == zulItem.getAttribute("tag"))) {
				editformItemOrContainer = editformItemOrContainer.getParent();
			}
			if (editformItemOrContainer == null || editformItemOrContainer.getAttribute("tag") == null) {
				return null;
			}
		}
		return editformItemOrContainer;
	}

	public Component getEditformItemDiv(Component zulItem) {
		Component editformItemDiv = zulItem;
		while (editformItemDiv != null && !editformItemDiv.getId().startsWith("editformItemDiv_", 0)) {
			editformItemDiv = editformItemDiv.getParent();
		}
		return editformItemDiv;
	}

	public void updateEditformItem(HtmlBasedComponent zulItem, String sclassPrefix, String hideRightWrongStr) {		
		//NOTE editformItemOrContainer is used to keep the number of attempts
		Component editformItemOrContainer = getEditformItemOrContainer(zulItem);
		if (editformItemOrContainer == null) {
			return;
		}
		boolean hideRightWrong = hideRightWrongStr != null && hideRightWrongStr.equals("hideRightWrong");
		String sclass = sclassPrefix;
		if (isShowIfRightOrWrong((IXMLTag)editformItemOrContainer.getAttribute("tag")) && !hideRightWrong) {
			String isInputCorrect = isInputCorrect((IXMLTag)zulItem.getAttribute("tag"));
			if (isInputCorrect.equals("true")) {
				sclass += "Right";
			}
			else if (isInputCorrect.equals("false")) {
				sclass += "Wrong";
			}
		}
		if (!zulItem.getSclass().equals(sclass)) {
			zulItem.setSclass(sclass);
		}
	}

}
