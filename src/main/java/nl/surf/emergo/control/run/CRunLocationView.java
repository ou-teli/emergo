/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CControl;

/**
 * The Class CRunLocationView. Used to show a location image within the run view area.
 * Notifies Emergo player when image is clicked.
 */
public class CRunLocationView extends CRunHbox {

	private static final long serialVersionUID = -2948159657795793205L;

	/** The run location image. */
	protected CRunBackgroundImage runLocationImage = null;

	/**
	 * Instantiates a new c run location view.
	 *
	 * @param aId the a id
	 */
	public CRunLocationView(String aId) {
		super();
		setId(aId);
		runLocationImage = new CRunBackgroundImage("runLocationImage", "showLocationActions", null, "", "", "");
		runLocationImage.setZclass(runLocationImage.getClassName() + "_active");
		runLocationImage.registerObserver(CControl.runWnd);
		appendChild(runLocationImage);
	}

	/**
	 * Gets the run background image.
	 *
	 * @return the run background image
	 */
	public CRunBackgroundImage getRunBackgroundImage() {
		return runLocationImage;
	}

	/**
	 * Sets the run background image.
	 *
	 * @param status the status
	 */
	public void setRunBackgroundImage(Object status) {
		CRunBackgroundImage lImage = getRunBackgroundImage();
		if (lImage != null) {
			lImage.setEventActionStatus(status);
			lImage.showImage((IXMLTag) status);
		}
	}
}
