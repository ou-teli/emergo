/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.JuniorScientist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.event.Event;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.zkspring.STriggeredReference;

/**
 * This class is used to show a news release where certain paragraphs may be blurred and/or present or not. It uses four states that each contains an answer given to one out of four dilemma's.
 * An empty editforms case component is used to store the opening and closing of the news release.
 * The used editforms case component should have present=false and present should stay false, to prevent rendering of the original editforms component on the tablet.
 */
public class CRunJuniorScientistNewsReleaseBox extends CRunJuniorScientistBox {

	private static final long serialVersionUID = -4542998212167966013L;

	protected final static String newsReleaseCaseComponentName = "phase_1_news_release";

	protected IECaseComponent newsReleaseAppCaseComponent;
	
	/** Prefix of pids of states entered by a case developer. */
	protected static final String newsDilemmaAnswerPrefix = "game_1_news_dilemma_answer_";
	
	@Override
	public void onInit() {
		super.onInitMacro();
		
		newsReleaseAppCaseComponent = getNewsReleaseAppCaseComponent();
		//NOTE super ancestor CRunComponent needs case component to be set
		setCaseComponent(newsReleaseAppCaseComponent);
		
		//just like for the original editforms component set selected and opened
		setRunComponentStatusJS(newsReleaseAppCaseComponent, AppConstants.statusKeySelected, AppConstants.statusValueTrue);
		setRunComponentStatusJS(newsReleaseAppCaseComponent, AppConstants.statusKeyOpened, AppConstants.statusValueTrue);

		//determine press release app properties to be used within the child macro
		setNewsReleaseAppProperties();

		//add the child macro
		addChildMacro("JuniorScientist_news_release_view_macro.zul");
	}
	
	@Override
	public void onUpdate() {
		//NOTE not used yet
		//rerender child macro with adjusted properties
		childMacro.recreate();
	}

	public void setNewsReleaseAppProperties() {
		//NOTE sections may be correct or not and blurred or not, and belong to one out of four dillema's.
		//For every dilemma a student has to choose if it is correct or not. Every dilemma is correct.
		//The first dilemma has five sections. The first section is always correct. It is followed by two correct sections (shown if student has chosen correct) and to incorrect sections (shown if student has chosen incorrect).
		//The second, third and fourth dilemma's all have two sections. One correct section and one incorrect section.
		//If a student has made a choice for a dilemma either the correct or incorrect sections are shown (not blurred).
		//If a student has made no choice for a dilemma yet, the correct sections are shown blurred.
		
		//NOTE get answers of student to dilemma's, If answer is empty, no answer is given yet. If answer is 1 correct choice is made. If answer is 2 incorrect choice is made.
		//depending on answers show and/or blur paragraphs
		List<Boolean> parPresent = new ArrayList<Boolean>();
		List<Boolean> parBlurred = new ArrayList<Boolean>();
		for (int i=1;i<=4;i++) {
			//answers are stored as state tag value
			String answer = getStateTagValue(newsDilemmaAnswerPrefix + i);
			boolean answerEmpty = answer.equals("");
			boolean answerCorrect = answer.equals("1");
			if (i == 1) {
				//NOTE five paragraphs
				//first paragraph always visible
				parPresent.add(true);
				parBlurred.add(answerEmpty);

				//correct paragraphs
				for (int j=1;j<=2;j++) {
					parPresent.add(answerCorrect || answerEmpty);
					parBlurred.add(answerEmpty);
				}

				//incorrect paragraphs
				for (int j=1;j<=2;j++) {
					parPresent.add(!(answerCorrect || answerEmpty));
					parBlurred.add(answerEmpty);
				}
			}
			else {
				//correct paragraph
				parPresent.add(answerCorrect || answerEmpty);
				parBlurred.add(answerEmpty);

				//incorrect paragraph
				parPresent.add(!(answerCorrect || answerEmpty));
				parBlurred.add(answerEmpty);
			}
		}

		//NOTE fill paragraph data
		List<Map<String,Object>> paragraphs = new ArrayList<Map<String,Object>>();
		propertyMap.put("paragraphs", paragraphs);
		
		for (int i=0;i<parPresent.size();i++) {
			Map<String,Object> hParagraphData = new HashMap<String,Object>();
			paragraphs.add(hParagraphData);
			hParagraphData.put("present", parPresent.get(i));
			hParagraphData.put("blurred", parBlurred.get(i));
		}
		
	}
	
	protected IECaseComponent getNewsReleaseAppCaseComponent() {
		return sSpring.getCaseComponent("", newsReleaseCaseComponentName);
	}
	
	public void onButtonClick(Event event) {
		//NOTE user clicks button to continue. This macro is used several times in a row.
		if (event.getData().equals("seen")) {
			//just like for the original editforms component set opened
			setRunComponentStatusJS(newsReleaseAppCaseComponent, AppConstants.statusKeyOpened, AppConstants.statusValueFalse);
			if (currentTag.getName().equals("macro")) {
				//NOTE if macro zul file is used within the junior scientist references component, the case component is a references component and the tag is a piece tag, so don't set present
				//hide press release macro
				setRunTagStatusJS(getNavigationCaseComponent(), currentTag, AppConstants.statusKeyPresent, AppConstants.statusValueFalse, false);
			}

			//show conversation to ask if student is ready or not
			int currentDilemmaNr = 0;
			IXMLTag gameStateTag = getStateTag("game_1_news_current_dilemma");
			if (gameStateTag != null) {
				currentDilemmaNr = Integer.parseInt(gameStateTag.getCurrentStatusAttribute(AppConstants.statusKeyValue));
			}
			showConversation("phase_1_conversations", "1_dilemma_" + currentDilemmaNr + "_ready");
		}
	}
	
	@Override
	public void handleStatusChange(STriggeredReference aTriggeredReference) {
	}

}
