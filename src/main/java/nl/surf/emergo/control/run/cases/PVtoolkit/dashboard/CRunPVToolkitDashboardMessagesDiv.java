/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.PVtoolkit.dashboard;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitInitBox;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefButton;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefDiv;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefImage;
import nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel;
import nl.surf.emergo.control.run.cases.PVtoolkit.model.CRunPVToolkitMessage;
import nl.surf.emergo.control.run.cases.PVtoolkit.util.CRunPVToolkitCustomAttributeHelper;
import nl.surf.emergo.domain.IERunGroup;

public class CRunPVToolkitDashboardMessagesDiv extends CDefDiv {

	private static final long serialVersionUID = -7890791282536471392L;

	public String zulfilepath = ((CRunPVToolkitInitBox)CDesktopComponents.vView().getComponent("PV-toolkit_initBox")).getZulfilepath();
	
	protected CRunPVToolkit pvToolkit;
	
	protected IERunGroup _actor;
	protected boolean _editable;
		
	public boolean _initialized = false;
	
	protected String _idPrefix = "dashboard";
	protected String _classPrefix = "dashboardMessages";

	public void onCreate(CreateEvent aEvent) {
		_idPrefix = CRunPVToolkitCustomAttributeHelper.handleCustomAttribute(this, "_idPrefix", _idPrefix);
	}
	
	public void init(IERunGroup actor, boolean editable) {
		_actor = actor;
		_editable = editable;
		
		//NOTE always initialize, because recordings and feedback my be added during a session
		setClass(_classPrefix);

		pvToolkit = (CRunPVToolkit)CDesktopComponents.vView().getComponent("pvToolkit");
		
		if (!pvToolkit.isActiveInPeerGroup(_actor)) {
			setVisible(false);
		}
		else {
			update();
		}

		_initialized = true;
	}
	
	public void update() {
		getChildren().clear();
		
		pvToolkit.setMemoryCaching(true);
		
		new CRunPVToolkitDefLabel(this, 
				new String[]{"class", "labelKey"}, 
				new Object[]{"font " + _classPrefix + "Title", "PV-toolkit-dashboard.header.messages"}
		);
		
		new CRunPVToolkitDefImage(this, 
				new String[]{"class", "src"}, 
				new Object[]{_classPrefix + "Background", zulfilepath + "background_wit_smaller.svg"}
		);

		Div div = new CRunPVToolkitDefDiv(this, 
				new String[]{"class"}, 
				new Object[]{_classPrefix + "Div"}
		);
		
		Rows rows = appendMessagesGrid(div);
		int rowNumber = 1;
		rowNumber = appendMessagesToGrid(rows, rowNumber);
		
		Component btn = new CRunPVToolkitDefButton(this, 
				new String[]{"class", "cLabelKey"}, 
				new Object[]{"font pvtoolkitButton " + _classPrefix + "DetailsButton", "PV-toolkit-dashboard.button.details"}
				);
		addDetailsOnClickEventListener(btn);

		pvToolkit.setMemoryCaching(false);
		
	}

    protected Rows appendMessagesGrid(Component parent) {
    	return pvToolkit.appendGrid(
    			parent, 
    			getId() + "Grid",
    			"position:absolute;left:0px;top:0px;width:460px;height:180px;overflow:auto;", 
    			new boolean[] {true, true, true, pvToolkit.hasCycleTags()},
    			new String[] {"10%", "40%", "40%", "10%"}, 
    			new boolean[] {false, true, true, true},
    			null,
    			"PV-toolkit-dashboard.column.label.", 
    			new String[] {"", "title", "sender", "cycle"});

    }

	protected int appendMessagesToGrid(Rows rows, int rowNumber) {
		return appendMessagesToGrid(
				rows, 
				rowNumber,
				new boolean[] {true, true, true, pvToolkit.hasCycleTags()} 
				);
	}

	protected int appendMessagesToGrid(
			Rows rows, 
			int rowNumber,
    		boolean[] showColumn 
			) {
		for (CRunPVToolkitMessage message : pvToolkit.getMessages(_actor, true)) {
			Row row = new Row();
			rows.appendChild(row);
				
			int columnNumber = 0;
			if (showColumn.length > columnNumber && showColumn[columnNumber]) {
				new CRunPVToolkitDefImage(row, 
						new String[]{"class", "src"}, 
						new Object[]{"gridImage", zulfilepath + "envelope.svg"}
						);
			}
	
			columnNumber++;
			if (showColumn.length > columnNumber && showColumn[columnNumber]) {
				new CRunPVToolkitDefLabel(row, 
						new String[]{"class", "value"}, 
						new Object[]{"font gridLabel", CDesktopComponents.sSpring().unescapeXML(message.getMessageTag().getChildValue("shorttitle"))}
						);
			}
	
			columnNumber++;
			if (showColumn.length > columnNumber && showColumn[columnNumber]) {
				new CRunPVToolkitDefLabel(row, 
						new String[]{"class", "value"}, 
						new Object[]{"font gridLabel", (message.getSender() != null ? message.getSender().getERunGroup().getName() : CDesktopComponents.vView().getLabel("PV-toolkit"))}
						);
			}
	
			columnNumber++;
			if (showColumn.length > columnNumber && showColumn[columnNumber]) {
				int cycleNumber = 0;
				if (message.getStepTag() != null) {
					cycleNumber = pvToolkit.getCycleNumber(message.getStepTag().getParentTag());
				}
				Label label = new CRunPVToolkitDefLabel(row, 
						new String[]{"class", "value"}, 
						new Object[]{"font gridLabel", (cycleNumber > 0 ? "" + cycleNumber : "") }
						);
	
				String orderValue = "" + cycleNumber;
				while (orderValue.length() < 4) {
					orderValue = "0" + orderValue;
				}
				label.setAttribute("orderValue", orderValue);
			}
	
			rowNumber ++;
		}

		return rowNumber;
	}

	protected void addDetailsOnClickEventListener(Component component) {
		component.addEventListener("onClick", new EventListener<Event>() {
			public void onEvent(Event event) {
				CDesktopComponents.vView().getComponent(_idPrefix + "Div").setVisible(false);
				CRunPVToolkitDashboardMessagesDetailsDiv div = (CRunPVToolkitDashboardMessagesDetailsDiv)CDesktopComponents.vView().getComponent(_idPrefix + "MessagesDetailsDiv");
				div.init(_actor, true);
				div.setVisible(true);
			}
		});
	}

}
