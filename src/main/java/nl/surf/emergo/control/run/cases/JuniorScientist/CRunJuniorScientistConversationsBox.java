/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.cases.JuniorScientist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.STriggeredReference;

/**
 * This class is used to show the active fragment (opened=true) of a certain conversation within a certain conversations case component. And to walk through the conversation fragments or conversations using buttons.
 * Conversations rendered with this class all should have present=false and present should stay false, to prevent the original conversations component to render the conversation.
 * Because this class is location independent, locations entered within a conversation are ignored. However, they should be entered, otherwise the conversation cannot be saved.
 * NOTE that for fragments only their pid, their text and opened property are used. Other input like, e.g., position and size, is ignored, so does not have to be entered by a case developer. For buttons,
 * only their pid is used and for some kind of buttons their text as well, so this is the only data that has to be entered.
 * For both fragments and buttons the explanation child tag may be used to set certain parameters to be used to render the fragment or button.
 * 
 * The pids of conversation fragments or conversations and buttons to walk trough them, apply to a naming convention that enables the walk through.
 * E.g., if a certain fragment has pid '0_meeting_2', buttons to walk to 'fragment '0_meeting_1' and  to 'fragment '0_meeting_3', respectively have pids 'LEFT_TO_FRAGMENT_0_meeting_1' and 'RIGHT_TO_FRAGMENT_0_meeting_3'.
 * 'LEFT' and 'RIGHT' indicate the position of the button and the direction of the arrow within it.
 * It is possible to define a button with text, so no left or right arrow, e.g., 'BUTTON_TO_FRAGMENT_0_meeting_3'. For this button text has to be entered by a case developer. These buttons are horizontally placed next to
 * each other above the fragment text.
 * It is possible to define a button with text, e.g., 'JUMP_TO_LOCATION_workplace'. For this button text has to be entered by a case developer. This button is placed next to the fragment text.
 * It is possible to walk through conversations within the same conversations case component as well.
 * E.g., by a pid 'RIGHT_TO_CONVERSATION_0_meeting'. Then the active (opened = true) fragment of the conversation will be shown.
 * 
 * Buttons may have another purpose than walking through fragments, e.g.:
 * - 'RIGHT_CLOSE_CONVERSATION', if clicked the conversation is closed, meaning present of the conversations macro is set to false
 * - 'JUMP_TO_LOCATION_workplace', if clicked the conversation is closed and the student is beamed to location with pid 'workplace'
 * - 'JUMP_TO_RESOURCE_press_release', if clicked the conversation is closed, and game script must be used to further handle the click
 * - 'JUMP_TO_APP_press_release', if clicked the conversation is closed, and game script must be used to further handle the click
 * 
 * Conversations, fragments and buttons may have parameters that indicate how to decorate the fragment or button. These parameters are stored in the explanation child tag of the fragment or button.
 * E.g., 'person=npc&icon=hella&image=hella_neutral' or 'icon-class=fas fa-newspaper'.
 * For conversations:
 * - 'background-image' (optional) indicates an image (1200x650px) to be shown as background of the conversation. E.g., 'commission'. In this case the image file is 'commission.png'. It is possible to define multiple images, comma separated. In this case images will be shown on top of each other.
 * For fragments:
 * - 'image' (optional) indicates an image (1200x650px) to be shown as background of the fragment. E.g., 'hella_neutral'. In this case the image file is 'npc_hella_neutral.png'. It is possible to define multiple images, comma separated. In this case images will be shown on top of each other.
 * - 'slide-in' (optional) if the image or images should slide into view. This is used to introduce an NPC within the game.
 * - 'person' (required) is 'npc' or 'pc' indicating if the fragment is spoken by a npc or pc. The layout is different.
 * - 'icon' (optional) indicates the icon to be shown next to the fragment text. E.g., 'hella'. The icon file to be shown is, e.g., 'ui_portrait_hella_lbl.png'. For 'pc' a pc icon or an uploaded employee card image is shown.
 * For JUMP buttons:
 * - 'icon-class' (optional) indicates the icon to be shown within a jump button'. It is a fontawesome icon.
 */
public class CRunJuniorScientistConversationsBox extends CRunJuniorScientistBox {

	private static final long serialVersionUID = -8155519028937694536L;
	
	/** The current fragment is defined by a conversations case component, a conversation tag and a fragment tag. */
	protected IECaseComponent conversationsCaseComponent;
	protected IXMLTag conversationTag;
	protected IXMLTag fragmentTag;
	
	/** Constants needed to render a fragment, to walk through fragments and end conversations. */
	protected final static String buttonTagPidPrefixLeft = "LEFT_";
	protected final static String buttonTagPidPrefixRight = "RIGHT_";
	protected final static String buttonTagPidPrefixClose = "CLOSE_";
	protected final static String buttonTagPidPrefixJump = "JUMP_";
	protected final static String buttonTagPidPrefixButton = "BUTTON_";
	protected final static String buttonTagPidToFragment = "TO_FRAGMENT_";
	protected final static String buttonTagPidToConversation = "TO_CONVERSATION_";
	protected final static String buttonTagPidToLocation = "TO_LOCATION_";
	protected final static String buttonTagPidToResource = "TO_RESOURCE_";
	protected final static String buttonTagPidToApp = "TO_APP_";
	/** A button may have a tooltip. It is defined as fragment child tag, with pid 'TOOLTIP', of the button tag. */
	protected final static String fragmentTagPidTooltip = "TOOLTIP";

	protected final static String paramKeyBackgroundImage = "background-image";
	protected final static String paramKeyImage = "image";
	protected final static String paramKeySlideIn = "slide-in";
	protected final static String paramKeyPerson = "person";
	protected final static String paramKeyIcon = "icon";
	protected final static String paramKeyIconClass = "icon-class";

	@Override
	public void onInit() {
		super.onInitMacro();
		
		initConversation();
		
		//determine fragment properties to be used within the child macro
		setFragmentProperties();
		
		//add the child macro
		addChildMacro("JuniorScientist_conversations_view_macro.zul");
	}
	
	public void initConversation() {
		//NOTE get conversations case component from a state that contains its name
		conversationsCaseComponent = getConversationsComponent();
		if (conversationsCaseComponent == null) {
			return;
		}
		//NOTE super ancestor CRunComponent needs case component to be set
		setCaseComponent(conversationsCaseComponent);

		//NOTE get conversation tag from a state that contains its pid
		conversationTag = getConversationTag();
		if (conversationTag == null) {
			return;
		}

		//just like for the original conversations component set selected and opened for the conversation
		setRunTagStatusJS(conversationsCaseComponent, conversationTag, AppConstants.statusKeySelected, AppConstants.statusValueTrue, false);
		setRunTagStatusJS(conversationsCaseComponent, conversationTag, AppConstants.statusKeyOpened, AppConstants.statusValueTrue, false);
		//get current fragment tag, i.e., with opened=true
		fragmentTag = getCurrentFragmentTag(conversationTag);
	}
	
	@Override
	public void onUpdate() {
		//when updated, e.g., to show another fragment, fragment properties have to be set again
		setFragmentProperties();

		//rerender parts of child macro
		String id = getUuid() + "_div";
		Events.sendEvent("onSetFragmentTag", vView.getComponent(id), propertyMap.get("fragmentTag"));
		if (propertyMap.get("fragmentTag") == null) {
			return;
		}
		Events.sendEvent("onSetConversationStyle", vView.getComponent(id), propertyMap.get("conversationStyle"));
		Events.sendEvent("onSetPersonsStyle", vView.getComponent(id), propertyMap.get("personsStyle"));
		Events.sendEvent("onSetPersonsSlideIn", vView.getComponent(id), propertyMap.get("personsSlideIn"));
		
		id = getUuid() + "_buttons";
		Events.sendEvent("onSetButtons", vView.getComponent(id), propertyMap.get("buttons"));

		id = getUuid() + "_dialogue";
		Events.sendEvent("onSetDialogueIsVisible", vView.getComponent(id), propertyMap.get("dialogueIsVisible"));
		Events.sendEvent("onSetPersonIsPc", vView.getComponent(id), propertyMap.get("personIsPc"));
		Events.sendEvent("onSetUrl", vView.getComponent(id), propertyMap.get("fragmentUrl"));

		id = getUuid() + "_left_button";
		Events.sendEvent("onSetButtonTag", vView.getComponent(id), propertyMap.get("leftButtonTag"));
		if (propertyMap.get("leftButtonTag") != null) {
			Events.sendEvent("onSetButtonTooltip", vView.getComponent(id), propertyMap.get("btnLeftTooltip"));
		}

		id = getUuid() + "_portrait";
		Events.sendEvent("onSetPortraitClass", vView.getComponent(id), propertyMap.get("personPortraitClass"));
		Events.sendEvent("onSetPortraitUrl", vView.getComponent(id), propertyMap.get("personPortraitUrl"));
		Events.sendEvent("onSetPortraitName", vView.getComponent(id), propertyMap.get("personPortraitName"));

		id = getUuid() + "_text";
		Events.sendEvent("onSetText", vView.getComponent(id), propertyMap.get("fragmentText"));

		id = getUuid() + "_video";
		Events.sendEvent("onSetUrl", vView.getComponent(id), propertyMap.get("fragmentUrl"));

		id = getUuid() + "_right_button";
		Events.sendEvent("onSetButtonTag", vView.getComponent(id), propertyMap.get("rightButtonTag"));
		if (propertyMap.get("rightButtonTag") != null) {
			Events.sendEvent("onSetButtonTooltip", vView.getComponent(id), propertyMap.get("btnRightTooltip"));
		}

		id = getUuid() + "_close_button";
		Events.sendEvent("onSetButtonTag", vView.getComponent(id), propertyMap.get("closeButtonTag"));
		if (propertyMap.get("closeButtonTag") != null) {
			Events.sendEvent("onSetButtonTooltip", vView.getComponent(id), propertyMap.get("btnCloseTooltip"));
		}

		id = getUuid() + "_jump_button";
		Events.sendEvent("onSetButtonTag", vView.getComponent(id), propertyMap.get("jumpButtonTag"));
		if (propertyMap.get("jumpButtonTag") != null) {
			Events.sendEvent("onSetButtonText", vView.getComponent(id), propertyMap.get("jumpButtonText"));
			Events.sendEvent("onSetButtonIconClass", vView.getComponent(id), propertyMap.get("jumpButtonIconClass"));
		}
	}

	protected void setFragmentProperties() {
		propertyMap.put("fragmentTag", fragmentTag);

		//conversationStyle contains the conversation background photo(s)
		String conversationStyle = getConversationStyle(conversationTag);
		propertyMap.put("conversationStyle", conversationStyle);
		//personsStyle contains the npc photo(s)
		String personsStyle = getPersonsStyle(fragmentTag);
		String fragmentUrl = getFragmentUrl(fragmentTag);
		if (!propertyMap.containsKey("personsStyle") || !personsStyle.equals("") || !fragmentUrl.equals("")) {
			//NOTE only change persons style if not empty and url is empty, so keep previous persons style if not defined for the fragment, like for a pc fragment
			propertyMap.put("personsStyle", personsStyle);
		}
		propertyMap.put("personsSlideIn", getPersonsSlideIn(fragmentTag));
		boolean personIsPc = personIsPc(fragmentTag);
		propertyMap.put("personIsPc", personIsPc);
		String personPortraitClass = getPersonPortraitClass(fragmentTag);
		propertyMap.put("personPortraitClass", personPortraitClass);
		String portraitUrl = "";
		if (personIsPc) {
			//gets PC portrait
			portraitUrl = getPersonPortraitUrl();
		}
		propertyMap.put("personPortraitUrl", portraitUrl);
		String name = "";
		if (personIsPc) {
			name = getStateTagValue(employeeCardNameStateKey);
			if (name.equals("")) {
				name = defaultEmployeeCardName;
			}
		}
		else {
			name = getPersonPortraitName(fragmentTag);
		}
		propertyMap.put("personPortraitName", name);
		String fragmentText = getFragmentText(fragmentTag);
		propertyMap.put("fragmentText", fragmentText);
		propertyMap.put("fragmentUrl", fragmentUrl);
		
		IXMLTag leftButtonTag = null;
		IXMLTag rightButtonTag = null;
		IXMLTag closeButtonTag = null;
		IXMLTag jumpButtonTag = null;
		List<Map<String,Object>> buttons = new ArrayList<Map<String,Object>>();
		propertyMap.put("buttons", buttons);
		if (fragmentTag == null) {
			return;
		}

		for (IXMLTag buttonTag : fragmentTag.getChilds("button")) {
			boolean isPresent = !buttonTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent).equals(AppConstants.statusValueFalse);
			String buttonTagPid = sSpring.unescapeXML(buttonTag.getChildValue("pid"));
			if (buttonTagPid.startsWith(buttonTagPidPrefixLeft)) {
				if (isPresent) {
					leftButtonTag = buttonTag;
				}
				//set properties for left button
				propertyMap.put("btnLeftTooltip", getButtonTooltip(buttonTag));
			}
			else if (buttonTagPid.startsWith(buttonTagPidPrefixRight)) {
				if (isPresent) {
					rightButtonTag = buttonTag;
				}
				//set properties for right button
				propertyMap.put("btnRightTooltip", getButtonTooltip(buttonTag));
			}
			else if (buttonTagPid.startsWith(buttonTagPidPrefixClose)) {
				if (isPresent) {
					closeButtonTag = buttonTag;
				}
				//set properties for right button
				propertyMap.put("btnCloseTooltip", getButtonTooltip(buttonTag));
			}
			else if (buttonTagPid.startsWith(buttonTagPidPrefixJump)) {
				if (isPresent) {
					jumpButtonTag = buttonTag;
				}
				//set properties for jump button
				propertyMap.put("jumpButtonText", sSpring.unescapeXML(buttonTag.getChildValue("text")));
				propertyMap.put("jumpButtonIconClass", getButtonIconClass(buttonTag));

			}
			else if (buttonTagPid.startsWith(buttonTagPidPrefixButton)) {
				if (isPresent) {
					Map<String,Object> hButtonData = new HashMap<String,Object>();
					buttons.add(hButtonData);
					hButtonData.put("buttonTag", buttonTag);
					hButtonData.put("buttonText", sSpring.unescapeXML(buttonTag.getChildValue("text")));
				}
			}
		}
		propertyMap.put("leftButtonTag", leftButtonTag);
		propertyMap.put("rightButtonTag", rightButtonTag);
		propertyMap.put("closeButtonTag", closeButtonTag);
		propertyMap.put("jumpButtonTag", jumpButtonTag);
		
		boolean dialogueIsVisible = 
				!personPortraitClass.equals("") ||
				!portraitUrl.equals("") ||
				!fragmentText.equals("") ||
				leftButtonTag != null ||
				rightButtonTag != null ||
				closeButtonTag != null;
		propertyMap.put("dialogueIsVisible", dialogueIsVisible);

		//just like for the original conversations component set started for the fragment
		setRunTagStatusJS(conversationsCaseComponent, fragmentTag, AppConstants.statusKeyStarted, AppConstants.statusValueTrue, false);
	}
	
	protected IECaseComponent getConversationsComponent() {
		return sSpring.getCaseComponent("", getConversationsComponentName());
	}
	
	protected IXMLTag getConversationTag(String tagPid) {
		return getTagByPid(conversationsCaseComponent, tagPid);
	}
	
	protected IXMLTag getConversationTag() {
		return getConversationTag(getConversationTagPid());
	}
	
	protected String getOtherConversationPid(String fragmentPid){
		String currentConversationPid = getConversationTagPid();
		String otherConversationPid = "";
		for (IXMLTag nodeTag : cScript.getRunGroupNodeTags(conversationsCaseComponent)) {
			if (nodeTag.getName().equals("conversation")) {
				String conversationPid = sSpring.unescapeXML(nodeTag.getChildValue("pid"));
				String fragmentPidPrefix = conversationPid + "_";
				if (!conversationPid.equals(currentConversationPid) && fragmentPid.startsWith(fragmentPidPrefix)) {
					 otherConversationPid = conversationPid;
					 break;
				}
			}
		}
		return otherConversationPid;
	}
	
	protected IXMLTag getCurrentFragmentTag(IXMLTag parentTag) {
		if (parentTag == null)
			return null;
		IXMLTag openedFragmentTag = getOpenedFragmentTag(parentTag);
		String fragmentTagPid = getFragmentTagPid();
		if (!fragmentTagPid.equals("")) {
			setFragmentTagPid("");
			IXMLTag fragmentTag = getFragmentTag(fragmentTagPid);
			if (fragmentTag != null) {
				boolean opened = fragmentTag.getCurrentStatusAttribute(AppConstants.statusKeyOpened).equals(AppConstants.statusValueTrue);
				if (!opened) {
					if (openedFragmentTag != null) {
						setRunTagStatusJS(conversationsCaseComponent, openedFragmentTag, AppConstants.statusKeyOpened, AppConstants.statusValueFalse, false);
					}
					setRunTagStatusJS(conversationsCaseComponent, fragmentTag, AppConstants.statusKeyOpened, AppConstants.statusValueTrue, false);
				}
				return fragmentTag;
			}
		}
		return openedFragmentTag;
	}

	protected IXMLTag getFragmentTag(String fragmentTagPid) {
		return getTagByPid(conversationsCaseComponent, fragmentTagPid);
	}

	protected IXMLTag getOpenedFragmentTag(IXMLTag parentTag) {
		if (parentTag == null)
			return null;
		for (IXMLTag childTag : parentTag.getChildTags(AppConstants.defValueNode)) {
			String name = childTag.getName();
			if (name.equals("fragment")) {
				boolean present = childTag.getCurrentStatusAttribute(AppConstants.statusKeyPresent).equals(AppConstants.statusValueTrue);
				boolean opened = childTag.getCurrentStatusAttribute(AppConstants.statusKeyOpened).equals(AppConstants.statusValueTrue);
				if (present && opened)
					// get first opened
					return childTag;
			}
		}
		return null;
	}

	protected String getConversationStyle(IXMLTag conversationTag) {
		List<String> paramValues = getParamValues(conversationTag, paramKeyBackgroundImage);
		String urls = "";
		for (String paramValue : paramValues) {
			if (!urls.equals("")) {
				urls += ",";
			}
			//NOTE conversation background should be pngs
			urls += "url('" + zulfilepath + assetSubPath + paramValue + ".png" + "')";
		}
		if (!urls.equals("")) {
			return "background-image:" + urls + ";";
		}
		return "";
	}
	
	protected String getPersonsStyle(IXMLTag fragmentTag) {
		List<String> paramValues = getParamValues(fragmentTag, paramKeyImage);
		String urls = "";
		for (String paramValue : paramValues) {
			if (!urls.equals("")) {
				urls += ",";
			}
			//NOTE fragment background should be pngs
			urls += "url('" + zulfilepath + assetSubPath + paramValue + ".png" + "')";
		}
		if (!urls.equals("")) {
			return "background-image:" + urls + ";";
		}
		return "";
	}
	
	protected boolean getPersonsSlideIn(IXMLTag fragmentTag) {
		return getParamValue(fragmentTag, paramKeySlideIn).equals(AppConstants.statusValueTrue);
	}
	
	protected boolean personIsPc(IXMLTag fragmentTag) {
		return getParamValue(fragmentTag, paramKeyPerson).equals("pc");
	}
	
	protected String getPersonPortraitClass(IXMLTag fragmentTag) {
		return getParamValue(fragmentTag, paramKeyIcon);
	}

	protected String getPersonPortraitUrl() {
		String url = getEmployeeCardUrl();
		if (url.equals("")) {
			url = getZulfilepath() + "assets/avatar-placeholder-white.svg";
		}
		else {
			url = vView.getEmergoWebappsRoot() + url;
		}
		return url;
	}

	protected String getPersonPortraitName(IXMLTag fragmentTag) {
		return VView.getCapitalizeFirstChar(getParamValue(fragmentTag, paramKeyIcon));
	}

	protected String getFragmentText(IXMLTag fragmentTag) {
		if (fragmentTag == null) {
			return "";
		}
		return sSpring.unescapeXML(fragmentTag.getChildValue("text"));
	}
	
	protected String getFragmentUrl(IXMLTag fragmentTag) {
		if (fragmentTag == null) {
			return "";
		}
		String url = sSpring.getSBlobHelper().getUrl(fragmentTag, "blob");
		if (!vView.isAbsoluteUrl(url)) {
			url = vView.getAbsoluteUrl(url);
		}
		return url;
	}
	
	protected String getButtonTooltip(IXMLTag buttonTag) {
		// A button may have a tooltip. It is defined as fragment child tag, with pid 'TOOLTIP', of the button tag.
		String tooltip = "";
		List<IXMLTag> subFragmentTags = buttonTag.getChilds("fragment");
		for (IXMLTag subFragmentTag : subFragmentTags) {
			String subFragmentTagPid = sSpring.unescapeXML(subFragmentTag.getChildValue("pid"));
			if (subFragmentTagPid.equals(fragmentTagPidTooltip)) {
				tooltip = sSpring.unescapeXML(subFragmentTag.getChildValue("text"));
				break;
			}
		}
		return tooltip;
	}

	protected String getButtonIconClass(IXMLTag buttonTag) {
		return getParamValue(buttonTag, paramKeyIconClass);
	}
	
	/** If a button is clicked within the child macro file, following event is triggered. */
	public void onButtonClick(Event event) {
		IXMLTag buttonTag = (IXMLTag)event.getData();
		if (buttonTag == null) {
			return;
		}
		//just like for the original conversations component set finished for the fragment
		setRunTagStatusJS(conversationsCaseComponent, fragmentTag, AppConstants.statusKeyFinished, AppConstants.statusValueTrue, false);

		//handle the button click internally
		String buttonTagPid = sSpring.unescapeXML(buttonTag.getChildValue("pid"));
		handleButtonClick(buttonTag, buttonTagPid, buttonTagPid.startsWith(buttonTagPidPrefixLeft));

	  	//NOTE use an echo event to set selected of the button to be sure all other events are handled
	  	Events.echoEvent("onSetButtonSelected", this, buttonTag);
	}
	
	public void onSetButtonSelected(Event event) {
		IXMLTag buttonTag = (IXMLTag)event.getData();
		//just like for the original conversations component set selected for the button
		setRunTagStatusJS(conversationsCaseComponent, buttonTag, AppConstants.statusKeySelected, AppConstants.statusValueTrue, false);
	}

	protected void handleButtonClick(IXMLTag buttonTag, String buttonTagPid, boolean left) {
		if (buttonTagPid.contains(buttonTagPidToFragment)) {
			//get fragment pid from buttontag pid
			String fragmentPid = buttonTagPid.substring(buttonTagPid.indexOf(buttonTagPidToFragment) + buttonTagPidToFragment.length());
			toFragment(fragmentPid);
		}
		if (buttonTagPid.contains(buttonTagPidToConversation)) {
			//get conversation pid from buttontag pid
			String conversationPid = buttonTagPid.substring(buttonTagPid.indexOf(buttonTagPidToConversation) + buttonTagPidToConversation.length());
			toConversation(conversationPid);
		}
		else if (buttonTagPid.contains(buttonTagPidToLocation)) {
			closeMacro();
			//get location pid from buttontagpid
			String locationPid = buttonTagPid.substring(buttonTagPid.indexOf(buttonTagPidToLocation) + buttonTagPidToLocation.length());
			toLocation(locationPid);
		}
		else if (buttonTagPid.contains(buttonTagPidToResource)) {
			closeMacro();
			//NOTE handle continuation within script
		}
		else if (buttonTagPid.contains(buttonTagPidToApp)) {
			closeMacro();
			//NOTE handle continuation within script
		}
		else if (buttonTagPid.startsWith(buttonTagPidPrefixClose)) {
			closeMacro();
			//NOTE handle continuation within script
		}
	}
	
	protected void toFragment(String fragmentPid) {
		//NOTE fragment may be fragment in other conversation in same conversations case component
		//get fragment tag
		IXMLTag fragmentTag = getTagByPid(conversationsCaseComponent, fragmentPid);
		if (fragmentTag != null) {
			//just like for the original conversations component set opened for the fragment
			setRunTagStatusJS(conversationsCaseComponent, fragmentTag, AppConstants.statusKeyOpened, AppConstants.statusValueTrue, false);
			String otherConversationPid = getOtherConversationPid(fragmentPid);
			if (otherConversationPid.equals("")) {
				//set fragment tag for new fragment
				this.fragmentTag = fragmentTag;
				//rerender macro to show new situation
				Events.echoEvent("onUpdate", this, null);
			}
			else {
				toConversation(otherConversationPid);
			}
		}
	}
	
	protected void toConversation(String conversationPid) {
		//NOTE a conversation is used for instruction or later on, for feedback
		switchConversation(conversationsCaseComponent.getName(), conversationPid);
	}
	
	protected void closeConversation() {
		//just like for the original conversations component set opened and finished for the conversation
		setRunTagStatusJS(conversationsCaseComponent, conversationTag, AppConstants.statusKeyOpened, AppConstants.statusValueFalse, false);
		setRunTagStatusJS(conversationsCaseComponent, conversationTag, AppConstants.statusKeyFinished, AppConstants.statusValueTrue, false);
		
		eventQueueJuniorScientistEvents.publish(
			new Event(
				onMacroEvent, 
				null, 
				new STriggeredReference(
					conversationsCaseComponent, 
					conversationTag, 
					null, 
					AppConstants.statusKeyOpened, 
					AppConstants.statusValueFalse, 
					false)
				)
			);
	}
	
	protected void closeMacro() {
		closeConversation();
		//hide macro component
		setRunTagStatusJS(getNavigationCaseComponent(), currentTag, AppConstants.statusKeyPresent, AppConstants.statusValueFalse, false);
	}
	
	protected void switchConversation(String conversationName, String conversationPid) {
		closeConversation();

		setConversationsComponentName(conversationName);
		setConversationTagPid(conversationPid);

		initConversation();
		
		//rerender macro to show new situation
		Events.echoEvent("onUpdate", this, null);
	}

	@Override
	public void handleStatusChange(STriggeredReference aTriggeredReference) {
	}

	@Override
	public void onAction(String sender, String action, Object status) {
		if (action.equals("onExternalEvent")) {
			if (sender.matches("jwplayer")) {
				String[] lStatus = (String[]) status;
				String lEvent = lStatus[2];
				if (lEvent.equals("onComplete")) {
					if (sSpring.unescapeXML(fragmentTag.getChildValue("pid")).equals("2_conversation_1")) {
						IXMLTag buttonTag = getTagByPid(conversationsCaseComponent, "JUMP_2_conversation_TO_LOCATION_workplace");
						if (buttonTag != null) {
							setRunTagStatusJS(conversationsCaseComponent, buttonTag, AppConstants.statusKeyPresent, AppConstants.statusValueTrue, false);
							propertyMap.put("jumpButtonTag", buttonTag);
							//rerender macro to show new situation
							Events.echoEvent("onUpdateJumpButton", this, null);
						}
					} 
				} 
			}
		}
	}

	public void onUpdateJumpButton() {
		//rerender jump button
		String id = getUuid() + "_jump_button";
		Events.sendEvent("onSetButtonTag", vView.getComponent(id), propertyMap.get("jumpButtonTag"));
		if (propertyMap.get("jumpButtonTag") != null) {
			Events.sendEvent("onSetButtonText", vView.getComponent(id), propertyMap.get("jumpButtonText"));
			Events.sendEvent("onSetButtonIconClass", vView.getComponent(id), propertyMap.get("jumpButtonIconClass"));
		}
	}

}
