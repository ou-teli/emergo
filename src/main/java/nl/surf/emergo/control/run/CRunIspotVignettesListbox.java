/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run;

import java.util.List;

import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefListbox;
import nl.surf.emergo.view.VView;

public class CRunIspotVignettesListbox extends CDefListbox {

	private static final long serialVersionUID = 4171651356772065798L;

	protected VView vView = CDesktopComponents.vView();

	protected CRunIspot runIspot = null;

	protected boolean showRanking = false;

	public void onCreate(CreateEvent aEvent) {
		runIspot = (CRunIspot)vView.getComponent("runIspot");
	}

	public void onUpdate(Event aEvent) {
		setAttribute("showVignettesButton", true);
		String vignetteType = (String)aEvent.getData();
		((Label)vView.getComponent("auxlabel")).setValue(vView.getLabel("run_ispot.menuoption." + vignetteType));
		((Listheader)vView.getComponent("listheader")).setLabel(vView.getLabel("cde_s_component.singleselect.item." + vignetteType));
		//remove listbox items
		getItems().clear();
		//get dataElements
		List<CRunIspotIntervention> dataElements = runIspot.getInterventions(vignetteType);
		showRanking = false;
		for (CRunIspotIntervention dataElement : dataElements) {
			if (!dataElement.getVignetteRanking().equals("")) {
				showRanking = true;
			}
		}
		//auxheader.setColspan(lShowRanking ? 5 : 4);
		vView.getComponent("rankheader").setVisible(showRanking ? true : false);
		((HtmlBasedComponent)vView.getComponent("subjectheader")).setWidth(showRanking ? "350px" : "375px");
		((HtmlBasedComponent)vView.getComponent("listheader")).setWidth(showRanking ? "350px" : "375px");
     	//stateheader.setWidth(lShowRanking ? "40px" : "50px");
		//add listbox items
		for (CRunIspotIntervention dataElement : dataElements) {
			Listitem listitem = new Listitem();
			listitem.setAttribute("dataElement", dataElement);
			appendChild(listitem);
			Listcell listcell = new Listcell(dataElement.getVignetteRanking());
			listcell.setStyle(runIspot.listboxStyle);
			listitem.appendChild(listcell);
			//0
			listcell = new Listcell(dataElement.getSubjectName());
			listcell.setStyle(runIspot.listboxStyle);
			listitem.appendChild(listcell);
			//1
			listcell = new Listcell(dataElement.getVignetteName());
			listcell.setStyle(runIspot.listboxStyle);
			listitem.appendChild(listcell);
			//2
			listcell = new Listcell();
			listcell.setImage(runIspot.stylePath + "ispot-"+ dataElement.getReactionState() + ".png");
			listcell.setTooltiptext(vView.getLabel("run_ispot.listcell.reaction." + dataElement.getReactionState()));
			listcell.setStyle(runIspot.listboxStyle);
			listitem.setAttribute("stateCell", listcell);
			listitem.appendChild(listcell);
			//3
			listcell = new Listcell();
			listcell.setLabel(dataElement.getReactionDoneDate());
			listcell.setStyle(runIspot.listboxStyle);
			listitem.setAttribute("doneDateCell", listcell);
			listitem.appendChild(listcell);
		}
		if (showRanking) {
		//TO DO sort only once per vignette type?
			((Listheader)vView.getComponent("rankheader")).sort(true,true);
		}
	}

	public void onSkipOverview(Event aEvent) {
		setAttribute("showVignettesButton", false);
		String vignetteType = (String)aEvent.getData();
		getItems().clear();
		int lSelected = 0;
		int lIndex = 0;
		//get dataElements
		List<CRunIspotIntervention> dataElements = runIspot.getInterventions(vignetteType);
		//add listbox items
		for (CRunIspotIntervention dataElement : dataElements) {
			Listitem listitem = new Listitem();
			listitem.setAttribute("dataElement", dataElement);
			appendChild(listitem);

			//needed, for else zk error Attempt to invoke method setSrc on null value??
			//0
			Listcell listcell = new Listcell(dataElement.getSubjectName());
			listitem.appendChild(listcell);
			//1
			listcell = new Listcell(dataElement.getVignetteName());
			listitem.appendChild(listcell);
			//2
			listcell = new Listcell();
			listcell.setImage(runIspot.stylePath + "ispot-"+ dataElement.getReactionState() + ".png");
			listcell.setTooltiptext(vView.getLabel("run_ispot.listcell.reaction." + dataElement.getReactionState()));
			listcell.setStyle(runIspot.listboxStyle);
			listitem.setAttribute("stateCell", listcell);
			listitem.appendChild(listcell);
			//3
			listcell = new Listcell();
			listcell.setLabel(dataElement.getReactionDoneDate());
			listcell.setStyle(runIspot.listboxStyle);
			listitem.setAttribute("doneDateCell", listcell);
			listitem.appendChild(listcell);
				
			//resume with last handled vignette
			if (runIspot.getInterventionStatus(dataElement, AppConstants.statusKeySelected).equals(AppConstants.statusValueTrue))
				lSelected = lIndex;
			lIndex++;
		}
		setSelectedIndex(lSelected);
		Events.sendEvent("onSelect", this, null);
	}
	
	public void onSelect(Event aEvent) {
		runIspot.setComponentVisible(vView.getComponent("ispotVignettes"), false);
		runIspot.setComponentVisible(vView.getComponent("ispotVignettesButton") , (boolean)getAttribute("showVignettesButton"));
		CRunIspotIntervention dataElement = (CRunIspotIntervention)getSelectedItem().getAttribute("dataElement");
		if (dataElement.getReactionState().equals("empty")) {
			dataElement.setReactionState("busy");
		}
		//make vignettes single select, so we can use this attribute to retrieve current vignette
		List<Listitem> lAllItems = getItems();
		for (Listitem lItem : lAllItems) {
			runIspot.setInterventionStatus((CRunIspotIntervention)lItem.getAttribute("dataElement"), AppConstants.statusKeySelected, AppConstants.statusValueFalse);
		}
		//set selected state of dataElement
		runIspot.setInterventionStatus(dataElement, AppConstants.statusKeySelected, AppConstants.statusValueTrue);
		//set opened state of dataElement
		runIspot.setInterventionStatus(dataElement, AppConstants.statusKeyOpened, AppConstants.statusValueTrue);
		Events.sendEvent("onUpdateState", this, dataElement);
		Events.postEvent("onUpdate", vView.getComponent("ispotVignette"), dataElement);
		runIspot.setComponentVisible(vView.getComponent("ispotViewers"), true);
		runIspot.setComponentVisible(vView.getComponent("ispotVignette"), true);
	}

	public void onUpdateState(Event aEvent) {
		CRunIspotIntervention dataElement = (CRunIspotIntervention)getSelectedItem().getAttribute("dataElement");
		getSelectedItem().setAttribute("dataElement", dataElement);
		((Listcell)getSelectedItem().getAttribute("stateCell")).setImage(runIspot.stylePath + "ispot-"+ dataElement.getReactionState() + ".png");
		((Listcell)getSelectedItem().getAttribute("stateCell")).setTooltiptext(vView.getLabel("run_ispot.listcell.reaction." + dataElement.getReactionState()));
		((Listcell)getSelectedItem().getAttribute("doneDateCell")).setLabel(dataElement.getReactionDoneDate());
	}

	public void onPrevious(Event aEvent) {
		setSelectedIndex(getSelectedIndex() - 1);
		if (getSelectedItem() != null) {
			Events.sendEvent("onSelect", this, null);
		}
	}

	public void onNext(Event aEvent) {
		setSelectedIndex(getSelectedIndex() + 1);
		if (getSelectedItem() != null) {
			Events.sendEvent("onSelect", this, null);
		}
	}

}
