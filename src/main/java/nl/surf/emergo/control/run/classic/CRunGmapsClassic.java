/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.run.classic;

import java.util.List;

import org.zkoss.gmaps.Gmarker;
import org.zkoss.gmaps.event.MapMouseEvent;
import org.zkoss.zk.ui.Component;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CContentHelper;
import nl.surf.emergo.control.CContentItemMenuPopup;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CGmaps;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.zkspring.SSpring;

/**
 * The Class CRunGmapsClassic is used to show google maps within the run view area of the emergo player.
 * It is shown within an iframe otherwise it wouldn't work.
 */
public class CRunGmapsClassic extends CGmaps {

	private static final long serialVersionUID = -1694042215594825981L;

	/** The run wnd. */
	protected CRunWndClassic runWnd = null;

	/** The case component. */
	protected IECaseComponent caseComponent = null;

	/** The tagopenednames. */
	protected String tagopenednames = "";

	/** The selectedcontentitem. */
	protected Component selectedcontentitem = null;

	/** The selectedtagid. */
	protected String selectedtagid = "";

	/** The run component. */
	protected CRunComponentClassic runComponent;

	/** The run status type. */
	protected String runStatusType = AppConstants.statusTypeRunGroup;

	public String getClassName() {
		return CDesktopComponents.vView().getRunClassName(className);
	}

	/**
	 * Sets init values.
	 */
	public void setInitValues () {
		super.setInitValues();
		setWidth("938px");
		setHeight("428px");
	}

	/**
	 * Gets the run wnd.
	 * 
	 * @return the run wnd
	 */
	public CRunWndClassic getRunWnd() {
		if (runWnd == null)
			runWnd = (CRunWndClassic) CDesktopComponents.cControl().getRunSessAttr(CControl.runWnd);
		return runWnd;
	}

	/**
	 * Gets the sspring.
	 *
	 * @return the sspring
	 */
	@Override
	public SSpring getSSpring() {
		return (SSpring)CDesktopComponents.cControl().getRunSessAttr("sspring");
	}

	/**
	 * Gets the run status type.
	 *
	 * @return the run status type
	 */
	public String getRunStatusType() {
		if (runComponent != null)
			return runComponent.getRunStatusType();
		return runStatusType;
	}

	/**
	 * Sets the run component.
	 *
	 */
	protected void setRunComponent() {
		CControl cControl = CDesktopComponents.cControl();
		runComponent = (CRunComponentClassic)cControl.getRunSessAttr("runcomponent");
	}

	/**
	 * Sets the run status type.
	 *
	 * @param aRunStatusType the new run status type
	 */
	public void setRunStatusType(String aRunStatusType) {
		runStatusType = aRunStatusType;
	}

	/**
	 * Instantiates a new c run gmaps classic. Constructor has no parameters like CRunTree, because it is shown within iframe
	 * using the use= attribute.
	 * Sessions vars contains 'parameters'.
	 * Updates map with author/user generated content.
	 */
	public CRunGmapsClassic() {
		super(false);
		setInitValues();
		CControl cControl = CDesktopComponents.cControl();
		caseComponent = (IECaseComponent)cControl.getRunSessAttr("casecomponent");
		setRunComponent();
		tagopenednames = "piece";
		initMap();
		if (getSSpring() != null) {
			update();
		}
	}

	/**
	 * Get Content Helper.
	 *
	 * @return the content helper
	 */
	@Override
	protected CContentHelper getContentHelper() {
		return getRunComponentHelperClassic();
	}

	/**
	 * Updates google maps.
	 */
	@Override
	public void update() {
		update(null);
	}

	/**
	 * Updates google maps.
	 * 
	 * @param aTag the a tag
	 */
	public void update(IXMLTag aTag) {
		getChildren().clear();

		CRunComponentHelperClassic cComponent = getRunComponentHelperClassic();
		if (aTag == null) {
			aTag = cComponent.getXmlDataPlusStatusTree();
			if (aTag != null) {
				cComponent.setItemAttributes(this, aTag.getChild(AppConstants.contentElement));
			}
		}
		if (aTag != null) {
			cComponent.xmlContentToContentItems(aTag, this);
		}
	}

	/**
	 * Updates existing status with external tags. Gets all update tags for current user and
	 * processes them.
	 * Added for polymorphism. Used for user generated content in player.
	 */
	public void updateWithExternalTags() {
		CRunGmapsClassic lRunGmaps = (CRunGmapsClassic)CDesktopComponents.vView().getComponent("runGooglemapsGmap");
		if (lRunGmaps != null) {
			// check if still zk component
			CRunComponentHelperClassic lRunComponentHelper = getRunComponentHelperClassic();
			if (getSSpring() != null) {
				lRunComponentHelper.updateWithExternalTags();
			}
		}
	}

	/**
	 * Gets the run component helper.
	 * 
	 * @return the run component helper
	 */
	public CRunComponentHelperClassic getRunComponentHelperClassic() {
		return new CRunGooglemapsHelperClassic(this, tagopenednames, caseComponent, runComponent, getSSpring());
	}

	/**
	 * Gets the run component helper.
	 * 
	 * @return the run component helper
	 */
	public CRunComponentHelperClassic getRunComponentHelper() {
		return new CRunGooglemapsHelperClassic(this, tagopenednames, caseComponent, runComponent, getSSpring());
	}

	/**
	 * Get Component status.
	 *
	 * @param aStatusKey the a status key
	 *
	 * @return the component status
	 */
	@Override
	public String getComponentStatus(String aStatusKey) {
		if (getSSpring() != null) {
			return getSSpring().getCurrentRunComponentStatus(caseComponent, aStatusKey, AppConstants.statusTypeRunGroup);
		}
		else {
			return "";
		}
	}

	/**
	 * Set Component status.
	 *
	 * @param aStatusKey the a status key
	 * @param aStatusValue the a status value
	 */
	@Override
	public void setComponentStatus(String aStatusKey, String aStatusValue) {
		if (getSSpring() != null) {
			getSSpring().setRunComponentStatus(caseComponent, aStatusKey, aStatusValue, true, AppConstants.statusTypeRunGroup, true);
		}
	}

	/**
	 * Sets contentitem status.
	 * 
	 * @param aContentItem the a contentitem
	 * 
	 * @return the contentitem
	 */
	protected Component setContentItemStatus(Component aContentItem) {
		Component lContentItem = aContentItem;
		IXMLTag tag = getContentItemTag(lContentItem);
		if ((tag != null) && (!tag.getAttribute(AppConstants.keyId).equals(""))) {
			CRunComponentHelperClassic lContentHelper = getRunComponentHelperClassic();
			lContentHelper.updateWithExternalTags();
			lContentItem = getContentItem(tag.getAttribute(AppConstants.keyId));
			lContentItem = setRunTagStatus(lContentItem, tag,
					AppConstants.statusKeySelected, AppConstants.statusValueTrue,
					tagopenednames, true);
			lContentHelper.addExternalUpdateStatusTag(tag, AppConstants.statusKeySelected, AppConstants.statusValueTrue);
			lContentItem = setRunTagStatus(lContentItem, tag,
					AppConstants.statusKeyOpened, AppConstants.statusValueTrue,
					tagopenednames, true);
			lContentHelper.addExternalUpdateStatusTag(tag, AppConstants.statusKeyOpened, AppConstants.statusValueTrue);
			if (getRunWnd() != null) {
				getRunWnd().setNoteTag(caseComponent, tag);
			}
		}
		return lContentItem;
	}

	/**
	 * Sets run group tag status.
	 *
	 * @param aContentItem the a contentitem
	 * @param aTag the a tag
	 * @param aKey the a key
	 * @param aValue the a value
	 * @param aTagNames the a tag names
	 * @param aSaveInDb the a save in db
	 *
	 * @return the contentitem
	 */
	public Component setRunTagStatus(Component aContentItem, IXMLTag aTag, String aKey, String aValue, String aTagNames, boolean aSaveInDb) {
		// set RunGroupTagstatus of tag in db
		if (getSSpring() != null) {
			getSSpring().setRunTagStatus(caseComponent, aTag, aKey, aValue, true, getRunStatusType(), aSaveInDb, false);
		}
		String lTagNames = "," + aTagNames + ",";
		boolean lNameInTagNames = ((aTagNames.equals("")) || (lTagNames.indexOf("," + aTag.getName() + ",") >= 0));
		if (lNameInTagNames) {
			// replace marker to show new status
			return reRenderContentItem(aContentItem, aTag, false);
		}
		return null;
	}

	/**
	 * Re render contentitem.
	 * 
	 * @param aContentItem the a contentitem
	 * @param aTag the a tag
	 * @param aReRenderChilds the a re render childs
	 * 
	 * @return the contentitem
	 */
	public Component reRenderContentItem(Component aContentItem, IXMLTag aTag, boolean aReRenderChilds) {
		CRunComponentHelperClassic cComponent = getRunComponentHelperClassic();
		Component lNewContentItem = cComponent.renderGmapsItem(aContentItem.getParent(), null, aTag);
		((Gmarker)lNewContentItem).setStyle("");
		aContentItem.detach();
		return lNewContentItem;
	}

	/**
	 * Adds contentitem.
	 * 
	 * @param aParentTag the a parent tag
	 * @param aTag the a tag
	 * 
	 * @return the contentitem
	 */
	public Component addContentItem(IXMLTag aParentTag,IXMLTag aTag) {
		CRunComponentHelperClassic cComponent = getRunComponentHelperClassic();
		Component lParent = getContentItem(aParentTag.getAttribute(AppConstants.keyId));
		Component lNewContentItem = cComponent.renderGmapsItem(lParent, null, aTag);
		((Gmarker)lNewContentItem).setStyle("");
		return lNewContentItem;
	}
	
	/**
	 * Get Content status.
	 * 
	 * @param aStatusKey the a status key
	 * 
	 * @return the content status
	 */
	public String getContentStatus(String aStatusKey) {
		CRunComponentHelperClassic cComponent = getRunComponentHelperClassic();
		// cComponent uses session variables to get current xml tree
		IXMLTag lRootTag = cComponent.getXmlDataTree();
		if (lRootTag == null)
			return "";
		IXMLTag lComponentTag = lRootTag.getChild(AppConstants.componentElement);
		if (lComponentTag == null)
			return "";
		return lComponentTag.getChildAttribute(AppConstants.statusElement, aStatusKey);
	}

	/**
	 * setContentItem.
	 * 
	 * @param aDraggedTag the a dragged tag
	 * 
	 * @return the list
	 */
	protected List<String[]> setContentItem(IXMLTag aDraggedTag) {
		CRunComponentHelperClassic cComponent = getRunComponentHelperClassic();
		// do move xml tag within xml tag tree
		List<String[]> lErrors = cComponent.updateNode(aDraggedTag, true);
		cComponent.addExternalUpdateTag("update", null, aDraggedTag);
		return lErrors;
	}

	/**
	 * On onMapClick. If marker is clicked show content of marker.
	 *
	 * @param aEvent the a event
	 */
	@Override
	public void onMapClick(MapMouseEvent aEvent) {
		Gmarker gmarker = aEvent.getGmarker();
		if (gmarker != null) {
			openMarker(gmarker);
		}
		else {
			super.onMapClick(aEvent);
		}
	}

	/**
	 * On onMapRightClick. If marker is clicked show menu for marker, depending
	 * on your rights. If no marker is clicked show menu to add marker, depending on your rights.
	 *
	 * @param aEvent the a event
	 */
	@Override
	public void onMapRightClick(MapMouseEvent aEvent) {
		Gmarker gmarker = aEvent.getGmarker();
		boolean lExtendable = runComponent.getContentExtendable();
		if (lExtendable) {
			if (gmarker == null) {
				super.onMapRightClick(aEvent);
			}
			else {
				boolean lContentOwnership = runComponent.getContentOwnership();
				String lOwnerRugId = "";
				IXMLTag lTag = (IXMLTag)gmarker.getAttribute("item");
				if (lTag != null)
					lOwnerRugId = lTag.getAttribute(AppConstants.keyOwner_rgaid);
				boolean lOwner = (getSSpring() != null) && (lOwnerRugId.equals(""+getSSpring().getRunGroupAccount().getRgaId()));
				boolean lEditDelete = ((!lContentOwnership) || (lOwner));
				if (lEditDelete)
					super.onMapRightClick(aEvent);
			}
		}
	}

	/**
	 * Open marker.
	 *
	 * @param aGmarker the a gmarker
	 */
	@Override
	public void openMarker(Gmarker aGmarker) {
		if (aGmarker != null) {
			String lContent = aGmarker.getContent();
			if (lContent == null || lContent.equals("")) {
				IXMLTag lTag = (IXMLTag)aGmarker.getAttribute("item");
				CRunComponentHelperClassic lHelper = getRunComponentHelperClassic();
				if (lTag != null && lHelper.isAccessible(lTag)) {
					super.openMarker(aGmarker);
				}
			}
		}
	}

	/**
	 * On drop, move a gmarker on the map, and change the corresponding
	 * xml tag within the xml tag tree.
	 * On error or if dropping not possible do nothing
	 *
	 * @param aDragged the dragged gmarker
	 * @param aDraggedLat the dragged latitude
	 * @param aDraggedLng the dragged longitude
	 */
	@Override
	public void onDrop(Gmarker aDragged, double aDraggedLat, double aDraggedLng) {
		//CContentHelperClassic lHelper = getContentHelper();
		CRunComponentHelperClassic lHelper = getRunComponentHelperClassic();
		IXMLTag lDraggedTag = (IXMLTag) aDragged.getAttribute("item");
		//NOTE following two statements create status childs if they don't exist
		//Childs must exist otherwise values are saved in data tag instead of status tag
		lHelper.getXmlNodeChild(lDraggedTag, "exlatitude");
		lHelper.getXmlNodeChild(lDraggedTag, "exlongitude");
		super.onDrop(aDragged, aDraggedLat, aDraggedLng);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.CGmaps#getMenuPopup()
	 */
	@Override
	protected CContentItemMenuPopup getMenuPopup() {
		return (CContentItemMenuPopup)CDesktopComponents.vView().getComponent("runMenuPopup");
	}

}
