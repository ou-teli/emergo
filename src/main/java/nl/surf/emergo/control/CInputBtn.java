/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import java.util.HashMap;
import java.util.Map;

import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Window;

import nl.surf.emergo.control.def.CDefButton;

/**
 * The Class CInputBtn.
 */
public class CInputBtn extends CDefButton {

	private static final long serialVersionUID = -7553563517040037622L;

	protected Listbox listbox = null;

	protected Listitem listitem = null;

	protected Object item = null;

	protected Map<String, Object> params = new HashMap<String, Object>();

	protected Window window = null;

	public Window showPopup(String aView) {
		/*
		 * Window lWindow = CDesktopComponents.vView().modalPopup(aView, null, params,
		 * "center"); if (lWindow.getAttribute("item") != null) {
		 * handleItem(lWindow.getAttribute("item")); } else if
		 * (lWindow.getAttribute("items") != null) {
		 * handleItem(lWindow.getAttribute("item")); } else if
		 * (lWindow.getAttribute("ok") != null) { deleteItem(); } else if
		 * (lWindow.hasAttribute("item") || lWindow.hasAttribute("items") ||
		 * lWindow.hasAttribute("ok")) { cancelItem(); }
		 */
		Window lWindow = CDesktopComponents.vView().quasiModalPopup(aView, getRoot(), params, "center");
		lWindow.setAttribute("notifyComponent", this);
		return lWindow;
	}

	public Window showModalPopup(final String aView) {
		Window lWindow = CDesktopComponents.vView().modalPopup(aView, null, params, "center");
		if (lWindow.getAttribute("item") != null) {
			handleItem(lWindow.getAttribute("item"));
		} else if (lWindow.getAttribute("items") != null) {
			handleItem(lWindow.getAttribute("item"));
		} else if (lWindow.getAttribute("ok") != null) {
			deleteItem();
		} else if (lWindow.hasAttribute("item") || lWindow.hasAttribute("items") || lWindow.hasAttribute("ok")) {
			cancelItem();
		}
		lWindow.setAttribute("notifyComponent", this);
		return lWindow;
	}

	@Override
	public void observedNotify(CIObserved aObserved, String aAction, Object aStatus) {
		if (aAction.equals("item")) {
			handleItem(aStatus);
		} else if (aAction.equals("items")) {
			handleItems(aStatus);
		} else if (aAction.equals("ok")) {
			deleteItem();
		} else if (aAction.equals("cancel")) {
			cancelItem();
		}
	}

	protected void handleItem(Object aStatus) {
	}

	protected void handleItems(Object aStatus) {
	}

	protected void deleteItem() {
	}

	protected void cancelItem() {
	}

}
