/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTML.Tag;
import javax.swing.text.html.HTMLEditorKit.ParserCallback;
import javax.swing.text.html.parser.ParserDelegator;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.util.HtmlUtils;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.IXmlManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.def.CDefHelper;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.domain.IEComponent;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpring;

/**
 * The Class CXmlHelper. Ancestor of all xml helper classes.
 */
public class CXmlHelper extends CDefHelper {
	private static final Logger _log = LogManager.getLogger(CXmlHelper.class);
	protected static IXmlManager xmlManager = null;
	protected IXMLTag xmlDefTree = null;
	protected IXMLTag xmlDataTree = null;

	public CXmlHelper() {
	}

	/**
	 * Instantiates a new c xml helper.
	 * 
	 * @param aSSpring the a s spring
	 */
	public CXmlHelper(SSpring aSSpring) {
		CDesktopComponents.setSSpring(aSSpring);
	}

	/**
	 * Gets the current cas id.
	 * 
	 * @return the cas id
	 */
	protected String getCasId() {
		return (String) CDesktopComponents.cControl().getAccSessAttr("casId");
	}

	/**
	 * Gets the component id of the current case component.
	 * 
	 * @return the cac com id
	 */
	protected String getCacComId() {
		return (String) CDesktopComponents.cControl().getAccSessAttr("cacComId");
	}

	/**
	 * Gets the current cac id.
	 * 
	 * @return the cac id
	 */
	protected String getCacId() {
		return (String) CDesktopComponents.cControl().getAccSessAttr("cacId");
	}

	/**
	 * Gets the current xml definition tree.
	 * 
	 * @return the xml def tree
	 */
	public IXMLTag getXmlDefTree() {
		if (xmlDefTree == null) {
			xmlDefTree = CDesktopComponents.sSpring().getXmlManager().getXmlTree(null, getXmlDef(),
					AppConstants.definition_tag);
		}
		return xmlDefTree;
	}

	/**
	 * Gets the xml definition tree given by aComId.
	 * 
	 * @param aComId the a com id
	 * 
	 * @return the xml def tree
	 */
	public IXMLTag getXmlDefTree(String aComId) {
		if (xmlDefTree == null) {
			xmlDefTree = CDesktopComponents.sSpring().getXmlManager().getXmlTree(null, getXmlDef(aComId),
					AppConstants.definition_tag);
		}
		return xmlDefTree;
	}

	/**
	 * Gets the current xml data tree.
	 * 
	 * @return the xml data tree
	 */
	public IXMLTag getXmlDataTree() {
		return CDesktopComponents.sSpring().getXmlDataTree(getCacId());
	}

	/**
	 * Gets the xml data tree for aComId and aCacId.
	 * 
	 * @param aComId the a com id
	 * @param aCacId the a cac id
	 * 
	 * @return the xml data tree
	 */
	public IXMLTag getXmlDataTree(String aComId, String aCacId) {
		if (xmlDataTree == null) {
			xmlDataTree = CDesktopComponents.sSpring().getXmlManager().getXmlTree(getXmlDefTree(aComId),
					getXmlData(aCacId), AppConstants.data_tag);
		}
		return xmlDataTree;
	}

	/**
	 * Gets the current xml definition string.
	 * 
	 * @return the xml def
	 */
	public String getXmlDef() {
		return CDesktopComponents.sSpring().getComponentDefinition(getCacComId());
	}

	/**
	 * Gets the xml definition string for aComId.
	 * 
	 * @param aComId the a com id
	 * 
	 * @return the xml def
	 */
	public String getXmlDef(String aComId) {
		return CDesktopComponents.sSpring().getComponentDefinition(aComId);
	}

	/**
	 * Gets the current xml data string.
	 * 
	 * @return the xml data
	 */
	public String getXmlData() {
		return CDesktopComponents.sSpring().getCaseComponentData(getCacId());
	}

	/**
	 * Gets the xml data string for aCacId.
	 * 
	 * @param aCacId the a cac id
	 * 
	 * @return the xml data
	 */
	public String getXmlData(String aCacId) {
		return CDesktopComponents.sSpring().getCaseComponentData(aCacId);
	}

	/**
	 * Sets the current xml data.
	 * 
	 * @param aXmlData the new xml data
	 */
	public void setXmlData(String aXmlData) {
		CDesktopComponents.sSpring().setCaseComponentData(getCacId(), aXmlData);
	}

	/**
	 * Sets the current xml data.
	 * 
	 * @param aRootTag the new xml data
	 */
	public void setXmlData(IXMLTag aRootTag) {
	}

	/**
	 * Sets xml data for aCacId.
	 * 
	 * @param aCacId   the a cac id
	 * @param aXmlData the a xml data
	 */
	public void setXmlData(String aCacId, String aXmlData) {
		CDesktopComponents.sSpring().setCaseComponentData(aCacId, aXmlData);
	}

	/**
	 * Gets the current component.
	 * 
	 * @return the component
	 */
	public IEComponent getComponent() {
		return CDesktopComponents.sSpring().getComponent(getCacComId());
	}

	/**
	 * Gets the case roles by cac id. See SSpring.
	 * 
	 * @return the case roles by cac id
	 */
	public List<IECaseRole> getCaseRolesByCacId() {
		return CDesktopComponents.sSpring().getSCaseRoleHelper().getCaseRolesByCacId(getCacId());
	}

	/**
	 * Gets the case roles by master slave cac ids. See SSpring.
	 * 
	 * @param aSCacId the a s cac id
	 * 
	 * @return the case roles by master slave cac ids
	 */
	public List<IECaseRole> getCaseRolesByMasterSlaveCacIds(String aSCacId) {
		return CDesktopComponents.sSpring().getSCaseRoleHelper().getCaseRolesByMasterSlaveCacIds(getCacId(), aSCacId);
	}

	/**
	 * Gets the current case component.
	 * 
	 * @return the case component
	 */
	public IECaseComponent getCaseComponent() {
		return CDesktopComponents.sSpring().getCaseComponent(getCacId());
	}

	/**
	 * Determines if aNodename is a method.
	 *
	 * @param aNodename the a node name
	 *
	 * @return boolean
	 */
	public static final boolean isMethod(String aNodename) {
		return (aNodename.equals("evalrungrouptagstatus") || aNodename.equals("setrungrouptagstatus")
				|| aNodename.equals("setruntagstatus") || aNodename.equals("evalrungroupcomponentstatus")
				|| aNodename.equals("setrungroupcomponentstatus") || aNodename.equals("setruncomponentstatus"));
	}

	/**
	 * Determines if aNodename is a template method.
	 *
	 * @param aNodename the a node name
	 *
	 * @return boolean
	 */
	public static final boolean isTemplateMethod(String aNodename) {
		return (aNodename.equals("evalrungrouptagtemplate") || aNodename.equals("setrungrouptagtemplate")
				|| aNodename.equals("evalrungroupcomponenttemplate")
				|| aNodename.equals("setrungroupcomponenttemplate"));
	}

	/**
	 * Determines if aNodename is using tags.
	 *
	 * @param aNodename the a node name
	 *
	 * @return boolean
	 */
	public static final boolean isMethodUsingTags(String aNodename) {
		if (aNodename.equals("evalrungroupcomponentstatus") || aNodename.equals("setrungroupcomponentstatus")
				|| aNodename.equals("setruncomponentstatus") || aNodename.equals("evalrungroupcomponenttemplate")
				|| aNodename.equals("setrungroupcomponenttemplate"))
			return false;
		return (aNodename.equals("evalrungrouptagstatus") || aNodename.equals("setrungrouptagstatus")
				|| aNodename.equals("setruntagstatus") || aNodename.equals("evalrungrouptagtemplate")
				|| aNodename.equals("setrungrouptagtemplate"));
	}

	/**
	 * Determines if aNodename is used in a condition.
	 *
	 * @param aNodename the a node name
	 *
	 * @return boolean
	 */
	public static final boolean isMethodUsedInCondition(String aNodename) {
		return (aNodename.equals("evalrungrouptagstatus") || aNodename.equals("evalrungroupcomponentstatus")
				|| aNodename.equals("evalrungrouptagtemplate") || aNodename.equals("evalrungroupcomponenttemplate"));
	}

	/**
	 * Determines if aTag is a content tag.
	 *
	 * @param aTag the a tag
	 *
	 * @return boolean
	 */
	public static final boolean isContentTag(IXMLTag aTag) {
		return aTag != null && aTag.getName().equals(AppConstants.contentElement);
	}

	/**
	 * Determines if aMethodTag is used in a main condition.
	 *
	 * @param aMethodTag the a method tag
	 *
	 * @return boolean
	 */
	public static final boolean isMethodUsedInMainCondition(IXMLTag aMethodTag) {
		if (aMethodTag == null) {
			return false;
		}
		IXMLTag lParentTag = aMethodTag.getParentTag();
		while (lParentTag != null && !lParentTag.getName().equals("condition")) {
			lParentTag = lParentTag.getParentTag();
		}
		return lParentTag != null && lParentTag.getName().equals("condition") && lParentTag.getParentTag() != null
				&& lParentTag.getParentTag().getName().equals(AppConstants.contentElement);
	}

	/**
	 * Determines if aNodename is used in an action.
	 *
	 * @param aNodename the a node name
	 *
	 * @return boolean
	 */
	public static final boolean isMethodUsedInAction(String aNodename) {
		return (aNodename.equals("setrungrouptagstatus") || aNodename.equals("setrungroupcomponentstatus")
				|| aNodename.equals("setrungrouptagtemplate") || aNodename.equals("setrungroupcomponenttemplate"));
	}

	/**
	 * Determines if aNodename is used in a run action.
	 *
	 * @param aNodename the a node name
	 *
	 * @return boolean
	 */
	public static final boolean isMethodUsedInRunAction(String aNodename) {
		return (aNodename.equals("setruntagstatus") || aNodename.equals("setruncomponentstatus"));
	}

	/**
	 * Determines if aNodename is reference to a tag.
	 *
	 * @param aNodename the a node name
	 *
	 * @return boolean
	 */
	public static final boolean isMethodReferenceToTag(String aNodename) {
		return (aNodename.equals("reftag"));
	}

	/**
	 * Determines if a method is used in item feedback.
	 *
	 * @param aMethodSubtype the a method sub type
	 *
	 * @return boolean
	 */
	public static final boolean isMethodUsedInItemfeedback(String aMethodSubtype) {
		return (aMethodSubtype.equals("itemfeedback"));
	}

	/**
	 * Determines if a method is used in assessment feedback.
	 *
	 * @param aMethodSubtype the a method sub type
	 *
	 * @return boolean
	 */
	public static final boolean isMethodUsedInAssessmentfeedback(String aMethodSubtype) {
		return (aMethodSubtype.equals("assessmentfeedback"));
	}

	/**
	 * Determines if a method is used in script.
	 *
	 * @param aMethodSubtype the a method sub type
	 *
	 * @return boolean
	 */
	public static final boolean isMethodUsedInScript(String aMethodSubtype) {
		return (aMethodSubtype.equals("__script"));
	}

	/**
	 * Determines if aRelation is relation between elements, either
	 * component-component, component-tag, tag-component or tag-tag.
	 *
	 * @param aRelation the a relation
	 *
	 * @return boolean
	 */
	public static final boolean isRelationBetweenElements(String aRelation) {
		return (isRelationToComponent(aRelation) || isRelationToTags(aRelation));
	}

	/**
	 * Detemines if aRelation is relation to component.
	 *
	 * @param aRelation the a relation
	 *
	 * @return boolean
	 */
	public static final boolean isRelationToComponent(String aRelation) {
		return (aRelation.equals("canoncomponent_canonresultroot") || aRelation.equals("notecomponent_logbookroot")
				|| aRelation.equals("memocomponent_memosroot") || aRelation.equals("referencescomponent_tutorialtag") ||
				// NOTE general solution so relation type can be added to XML definition without
				// needing to change Java code
				aRelation.matches(".*component_.*") || aRelation.matches(".*components_.*"));
	}

	/**
	 * Detemines if aRelation is relation to tag elements.
	 *
	 * @param aRelation the a relation
	 *
	 * @return boolean
	 */
	public static final boolean isRelationToTags(String aRelation) {
		return (aRelation.equals("locationtags_conversationtags") || aRelation.equals("locationtags_componentroot")
				|| aRelation.equals("piecetag_refpiecetag") || aRelation.equals("piecetag_itemstag")
				|| aRelation.equals("itemtag_assessmenttag") || aRelation.equals("persontags_mailtags")
				|| aRelation.equals("locationtags_passagetag") || aRelation.equals("tag_clickableobjecttag")
				|| aRelation.equals("statetag_statecollectortag")
				|| aRelation.equals("compositestatetag_statecollectortag")
				|| aRelation.equals("statecollectortag_statecollectortag") || aRelation.equals("statetag_formulatag")
				|| aRelation.equals("formulatag_formulatag") || aRelation.equals("conversationtag_tutorialtag")
				|| aRelation.equals("tutorialtag_tutorialtag") || aRelation.equals("statetag_statetag")
				|| aRelation.equals("statetag_scoretag") || aRelation.equals("resourcetag_resourcetag")
				|| aRelation.equals("resourcetypetag_resourcetag") || aRelation.equals("tag_ratingtag")
				|| aRelation.equals("statetag_ratingtag") || aRelation.equals("tag_statetag")
				|| aRelation.equals("fragmenttag_interventiontag") || aRelation.equals("fragmenttag_conversationstag")
				|| aRelation.equals("fragmenttag_editformstag") || aRelation.equals("fragmenttag_dragdropformstag")
				|| aRelation.equals("fragmenttag_selectionformstag")
				|| aRelation.equals("fragmenttag_videosceneselectortag") || aRelation.equals("containertag_texttag") ||
				// NOTE general solution so relation type can be added to XML definition without
				// needing to change Java code
				aRelation.matches(".*tag_.*") || aRelation.matches(".*tags_.*"));
	}

	/**
	 * Detemines if aRelation is relation to case role.
	 *
	 * @param aRelation the a relation
	 *
	 * @return boolean
	 */
	public static final boolean isRelationToCaseRole(String aRelation) {
		return (aRelation.equals("caseroles_npc_mailtags") || aRelation.equals("caseroles_pc_mailtags"));
	}

	/**
	 * Detemines if aRelation is relation with case role.
	 *
	 * @param aRelation the a relation
	 *
	 * @return boolean
	 */
	public static final boolean isRelationWithCaserole(String aRelation) {
		return (aRelation.indexOf("caseroles_") == 0);
	}

	/**
	 * Gets the script method child id, is value of child.
	 *
	 * @param aMethodTag the script method tag
	 * @param aChildName the child name
	 *
	 * @return the script method child id, 0 if not found
	 */
	public static final String getMethodChildId(IXMLTag aMethodTag, String aChildName) {
		if (aMethodTag == null)
			return "0";
		IXMLTag lTag = aMethodTag.getChild(aChildName);
		if (lTag == null)
			return "0";
		String lValue = lTag.getValue();
		if (lValue.equals(""))
			return "0";
		return lValue;
	}

	/**
	 * Sets script method child id, is value of child. If child does not exist, it
	 * is created
	 *
	 * @param aMethodTag the script method tag
	 * @param aChildName the child name
	 * @param aId        the id
	 */
	public static final void setMethodChildId(IXMLTag aMethodTag, String aChildName, String aId) {
		if (aMethodTag == null)
			return;
		IXMLTag lTag = aMethodTag.getChild(aChildName);
		if (lTag == null) {
			lTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag(aChildName, aId);
			lTag.setParentTag(aMethodTag);
			aMethodTag.getChildTags().add(lTag);
		} else
			lTag.setValue(aId);
	}

	/**
	 * Gets the script method cac id.
	 *
	 * @param aMethodTag the script method tag
	 *
	 * @return the script method cac id
	 */
	public static final String getMethodCacId(IXMLTag aMethodTag) {
		return getMethodChildId(aMethodTag, "cacid");
	}

	/**
	 * Sets script method cac id.
	 *
	 * @param aMethodTag the script method tag
	 * @param aId        the id
	 */
	public static final void setMethodCacId(IXMLTag aMethodTag, String aId) {
		setMethodChildId(aMethodTag, "cacid", aId);
	}

	/**
	 * Gets the script method cac name.
	 *
	 * @param aMethodTag the script method tag
	 *
	 * @return the script method cac name
	 */
	public static final String getMethodCacName(IXMLTag aMethodTag) {
		if (aMethodTag == null)
			return "";
		IXMLTag lTag = aMethodTag.getChild("cacid");
		if (lTag == null)
			return "";
		return CDesktopComponents.sSpring().getCaseComponentName(lTag.getValue());
	}

	/**
	 * Gets the script method car ids.
	 *
	 * @param aMethodTag the script method tag
	 *
	 * @return the script method car ids
	 */
	public static final String[] getMethodCarIds(IXMLTag aMethodTag) {
		if (aMethodTag == null)
			return new String[0];
		List<IXMLTag> lCarIdTags = aMethodTag.getChilds("carid");
		if ((lCarIdTags == null) || (lCarIdTags.size() == 0))
			return new String[0];
		String[] lCarIds = new String[lCarIdTags.size()];
		for (int i = 0; i < lCarIdTags.size(); i++) {
			IXMLTag lCarIdTag = lCarIdTags.get(i);
			String lCarId = lCarIdTag.getValue();
			lCarIds[i] = lCarId;
		}
		return lCarIds;
	}

	/**
	 * Gets the script method car id list.
	 *
	 * @param aMethodTag the script method tag
	 *
	 * @return the script method car id list
	 */
	public static final List<String> getMethodCarIdList(IXMLTag aMethodTag) {
		List<String> lCarIdList = new ArrayList<String>();
		String[] lCarIds = getMethodCarIds(aMethodTag);
		for (int i = 0; i < lCarIds.length; i++) {
			lCarIdList.add(lCarIds[i]);
		}
		return lCarIdList;
	}

	/**
	 * Gets the script method car names.
	 *
	 * @param aMethodTag the script method tag
	 *
	 * @return the script method car names
	 */
	public static final String[] getMethodCarNames(IXMLTag aMethodTag) {
		if (aMethodTag == null)
			return new String[0];
		String[] lCarIds = getMethodCarIds(aMethodTag);
		String[] lCarNames = new String[lCarIds.length];
		for (int i = 0; i < lCarIds.length; i++) {
			String lCarId = lCarIds[i];
			String lCarName = CDesktopComponents.sSpring().getSCaseRoleHelper().getCaseRoleName(lCarId);
			lCarNames[i] = lCarName;
		}
		return lCarNames;
	}

	/**
	 * Gets the script method com id.
	 *
	 * @param aMethodTag the script method tag
	 *
	 * @return the script method com id
	 */
	public static final String getMethodComId(IXMLTag aMethodTag) {
		return getMethodChildId(aMethodTag, "comid");
	}

	/**
	 * Sets script method com type.
	 *
	 * @param aMethodTag the script method tag
	 * @param aType      the type
	 */
	public static final void setMethodComId(IXMLTag aMethodTag, String aType) {
		setMethodChildId(aMethodTag, "comid", aType);
	}

	/**
	 * Gets the script method com type.
	 *
	 * @param aMethodTag the script method tag
	 *
	 * @return the script method com type
	 */
	public static final String getMethodComType(IXMLTag aMethodTag) {
		if (aMethodTag == null)
			return "";
		IXMLTag lTag = aMethodTag.getChild("comid");
		if (lTag == null)
			return "";
		IEComponent lComponent = CDesktopComponents.sSpring().getComponent(lTag.getValue());
		if (lComponent == null)
			return "";
		return CDesktopComponents.vView().getLabel(VView.componentLabelKeyPrefix + lComponent.getCode());
	}

	/**
	 * Gets the script method tag name.
	 *
	 * @param aMethodTag the script method tag
	 *
	 * @return the script method tag name
	 */
	public static final String getMethodTagName(IXMLTag aMethodTag) {
		return getMethodChildId(aMethodTag, "tagname");
	}

	/**
	 * Gets the script method tag ids.
	 *
	 * @param aMethodTag the script method tag
	 *
	 * @return the script method tag ids
	 */
	public static final String[] getMethodTagIds(IXMLTag aMethodTag) {
		if (aMethodTag == null)
			return new String[0];
		IXMLTag lTag = aMethodTag.getChild("cacid");
		if (lTag == null)
			return new String[0];
		List<IXMLTag> lTagIdTags = lTag.getChilds("tagid");
		if ((lTagIdTags == null) || (lTagIdTags.size() == 0))
			return new String[0];
		String[] lTagIds = new String[lTagIdTags.size()];
		for (int i = 0; i < lTagIdTags.size(); i++) {
			IXMLTag lTagIdTag = lTagIdTags.get(i);
			String lTagId = lTagIdTag.getValue();
			lTagIds[i] = lTagId;
		}
		return lTagIds;
	}

	/**
	 * Gets the script method component templates.
	 *
	 * @param aMethodTag the script method tag
	 *
	 * @return the script method component templates, comma separated
	 */
	public static final String getMethodComponentTemplates(IXMLTag aMethodTag) {
		if (aMethodTag == null)
			return "";
		IXMLTag lTag = aMethodTag.getChild("comid");
		if (lTag == null)
			return "";
		List<IXMLTag> lTagTemplates = lTag.getChilds("componenttemplate");
		if ((lTagTemplates == null) || (lTagTemplates.size() == 0))
			return "";
		String lTagTemplatesTagValues = "";
		for (IXMLTag lTagTemplatesTag : lTagTemplates) {
			String lTagTemplatesTagValue = lTagTemplatesTag.getValue();
			if (!lTagTemplatesTagValues.equals(""))
				lTagTemplatesTagValues = lTagTemplatesTagValues + ",";
			lTagTemplatesTagValues = lTagTemplatesTagValues + lTagTemplatesTagValue;
		}
		return lTagTemplatesTagValues;
	}

	/**
	 * Gets the script method tag templates.
	 *
	 * @param aMethodTag the script method tag
	 *
	 * @return the script method tag templates, comma separated
	 */
	public static final String getMethodTagTemplates(IXMLTag aMethodTag) {
		if (aMethodTag == null)
			return "";
		// NOTE method tag template is used in status and template conditions and
		// actions
		// When status cacid should be chosen. When template comid should be chosen.
		IXMLTag lTag = aMethodTag.getChild("cacid");
		if (lTag == null) {
			lTag = aMethodTag.getChild("comid");
		}
		if (lTag == null)
			return "";
		List<IXMLTag> lTagTemplates = lTag.getChilds("tagtemplate");
		if ((lTagTemplates == null) || (lTagTemplates.size() == 0))
			return "";
		String lTagTemplatesTagValues = "";
		for (IXMLTag lTagTemplatesTag : lTagTemplates) {
			String lTagTemplatesTagValue = lTagTemplatesTag.getValue();
			if (!lTagTemplatesTagValues.equals(""))
				lTagTemplatesTagValues = lTagTemplatesTagValues + ",";
			lTagTemplatesTagValues = lTagTemplatesTagValues + lTagTemplatesTagValue;
		}
		return lTagTemplatesTagValues;
	}

	/**
	 * Gets the script method component templates or operation.
	 *
	 * @param aMethodTag the script method tag
	 *
	 * @return the script method component templates or operation
	 */
	public static final boolean getMethodComponentTemplatesOrOperation(IXMLTag aMethodTag) {
		if (aMethodTag == null)
			return false;
		IXMLTag lTag = aMethodTag.getChild("comid");
		if (lTag == null)
			return true;
		IXMLTag lTemplateOrOperationTag = lTag.getChild("componenttemplateoroperation");
		if (lTemplateOrOperationTag == null)
			return true;
		return !lTemplateOrOperationTag.getValue().equals(AppConstants.statusValueFalse);
	}

	/**
	 * Gets the script method tag templates or operation.
	 *
	 * @param aMethodTag the script method tag
	 *
	 * @return the script method tag templates or operation
	 */
	public static final boolean getMethodTagTemplatesOrOperation(IXMLTag aMethodTag) {
		if (aMethodTag == null)
			return false;
		// NOTE method tag template is used in status and template conditions and
		// actions
		// When status cacid should be chosen. When template comid should be chosen.
		IXMLTag lTag = aMethodTag.getChild("cacid");
		if (lTag == null) {
			lTag = aMethodTag.getChild("comid");
		}
		if (lTag == null)
			return true;
		IXMLTag lTemplateOrOperationTag = lTag.getChild("tagtemplateoroperation");
		if (lTemplateOrOperationTag == null)
			return true;
		return !lTemplateOrOperationTag.getValue().equals(AppConstants.statusValueFalse);
	}

	/**
	 * Gets the script method tag names.
	 *
	 * @param aSubtype   the subtype, not used
	 * @param aMethodTag the script method tag
	 *
	 * @return the method tag names
	 */
	public static final String[] getMethodTagNames(String aSubtype, IXMLTag aMethodTag) {
		if (aMethodTag == null)
			return new String[0];
		String lCacId = getMethodCacId(aMethodTag);
		if (lCacId.equals("0"))
			return new String[0];
		String[] lTagIds = getMethodTagIds(aMethodTag);
		String[] lTagNames = new String[lTagIds.length];
		for (int i = 0; i < lTagIds.length; i++) {
			String lTagId = lTagIds[i];
			String lTagName = CDesktopComponents.sSpring().getTagName(lCacId, lTagId);
			lTagNames[i] = lTagName;
		}
		return lTagNames;
	}

	/**
	 * Gets the script method cac ids.
	 *
	 * @param aMethodTag the script method tag
	 *
	 * @return the script method cac ids
	 */
	public static final String[] getMethodCacIds(IXMLTag aMethodTag) {
		if (aMethodTag == null)
			return new String[0];
		List<IXMLTag> lCacIdTags = aMethodTag.getChilds("cacid");
		if ((lCacIdTags == null) || (lCacIdTags.size() == 0))
			return new String[0];
		String[] lCacIds = new String[lCacIdTags.size()];
		for (int i = 0; i < lCacIdTags.size(); i++) {
			IXMLTag lCacIdTag = lCacIdTags.get(i);
			String lCacId = lCacIdTag.getValue();
			lCacIds[i] = lCacId;
		}
		return lCacIds;
	}

	/**
	 * Gets the script method status id.
	 *
	 * @param aMethodTag the script method tag
	 *
	 * @return the script method status id
	 */
	public static final String getMethodStatusId(IXMLTag aMethodTag) {
		return getMethodChildId(aMethodTag, "statusid");
	}

	/**
	 * Sets script method status id.
	 *
	 * @param aMethodTag the script method tag
	 * @param aId        the id
	 */
	public static final void setMethodStatusId(IXMLTag aMethodTag, String aId) {
		setMethodChildId(aMethodTag, "statusid", aId);
	}

	/**
	 * Gets the script method function id.
	 *
	 * @param aMethodTag the script method tag
	 *
	 * @return the script method function id
	 */
	public static final String getMethodFunctionId(IXMLTag aMethodTag) {
		return getMethodChildId(aMethodTag, "functionid");
	}

	/**
	 * Sets script method function id.
	 *
	 * @param aMethodTag the script method tag
	 * @param aId        the id
	 */
	public static final void setMethodFunctionId(IXMLTag aMethodTag, String aId) {
		setMethodChildId(aMethodTag, "functionid", aId);
	}

	/**
	 * Gets the script method value.
	 *
	 * @param aMethodTag the script method tag
	 *
	 * @return the script method value
	 */
	public static final String getMethodValue(IXMLTag aMethodTag) {
		// NOTE To keep supporting old situation where count was a count of value true,
		// set default value to true.
		String lDefaultValue = AppConstants.statusValueTrue;
		if (aMethodTag == null)
			return lDefaultValue;
		IXMLTag lTag = aMethodTag.getChild("value");
		if (lTag == null)
			return lDefaultValue;
		String lValue = lTag.getValue();
		if (lValue.equals(""))
			return lDefaultValue;
		return lValue;
	}

	/**
	 * Sets script method value.
	 *
	 * @param aMethodTag the script method tag
	 * @param aValue     the value
	 */
	public static final void setMethodValue(IXMLTag aMethodTag, String aValue) {
		if (aMethodTag == null)
			return;
		IXMLTag lTag = aMethodTag.getChild("value");
		if (lTag == null) {
			lTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag("value", "");
			lTag.setParentTag(aMethodTag);
			aMethodTag.getChildTags().add(lTag);
		} else
			lTag.setValue(aValue);
	}

	/**
	 * Gets the script method operator id.
	 *
	 * @param aMethodTag the script method tag
	 *
	 * @return the script method operator id
	 */
	public static final String getMethodOperatorId(IXMLTag aMethodTag) {
		return getMethodChildId(aMethodTag, "operatorid");
	}

	/**
	 * Sets script method operator id.
	 *
	 * @param aMethodTag the script method tag
	 * @param aId        the id
	 */
	public static final void setMethodOperatorId(IXMLTag aMethodTag, String aId) {
		setMethodChildId(aMethodTag, "operatorid", aId);
	}

	/**
	 * Gets the script method operator values.
	 *
	 * @param aMethodTag the script method tag
	 *
	 * @return the script method operator values, comma separated
	 */
	public static final String getMethodOperatorValues(IXMLTag aMethodTag) {
		if (aMethodTag == null)
			return "";
		List<IXMLTag> lOperatorValueTags = null;
		IXMLTag lTag = aMethodTag.getChild("operatorid");
		// NOTE operator values can be childs of method tag or operatorid tag, not both
		if (lTag == null)
			lOperatorValueTags = aMethodTag.getChilds("operatorvalue");
		else
			lOperatorValueTags = lTag.getChilds("operatorvalue");
		if ((lOperatorValueTags == null) || (lOperatorValueTags.size() == 0))
			return "";
		String lOperatorValues = "";
		for (IXMLTag lOperatorValueTag : lOperatorValueTags) {
			String lOperatorValue = lOperatorValueTag.getValue();
			if (!lOperatorValues.equals(""))
				lOperatorValues = lOperatorValues + ",";
			lOperatorValues = lOperatorValues + lOperatorValue;
		}
		return lOperatorValues;
	}

	/**
	 * Sets script method operator values.
	 *
	 * @param aMethodTag      the script method tag
	 * @param aOperatorValues the script operator values, comma separated
	 */
	public static final void setMethodOperatorValues(IXMLTag aMethodTag, String aOperatorValues) {
		if (aMethodTag == null)
			return;
		IXMLTag lTag = aMethodTag.getChild("operatorid");
		if (lTag == null)
			return;
		List<IXMLTag> lOperatorValueTags = new ArrayList<IXMLTag>(0);
		if (aOperatorValues.equals("")) {
			lTag.setChildTags(lOperatorValueTags);
			return;
		}
		String[] lOperatorValueArr = aOperatorValues.split(",");
		for (int i = 0; i < lOperatorValueArr.length; i++) {
			lOperatorValueArr[i] = lOperatorValueArr[i].replace(AppConstants.statusCommaReplace, ",");
			IXMLTag lOperatorValueTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag("operatorvalue",
					lOperatorValueArr[i]);
			lOperatorValueTag.setParentTag(lTag);
			lOperatorValueTags.add(lTag);
		}
		lTag.setChildTags(lOperatorValueTags);
	}

	/**
	 * Gets the script method operator value id.
	 *
	 * @param aMethodTag the script method tag
	 *
	 * @return the script method operator value id
	 */
	public static final String getMethodOperatorValueId(IXMLTag aMethodTag) {
		if (aMethodTag == null)
			// NOTE default set operator value to true.
			return "1";
		return getMethodChildId(aMethodTag, "operatorvalue");
	}

	/**
	 * Strips html p tags if only one pair surround string.
	 *
	 * @param aCKeditorString the a fck editor string
	 *
	 * @return stripped string
	 */
	public String stripCKeditorString(String aCKeditorString) {
		String lParStart = "<p>";
		String lParEnd = "</p>";
		int lStartIndex = aCKeditorString.indexOf(lParStart);
		int lEndIndex = aCKeditorString.indexOf(lParEnd);
		if (lStartIndex == 0 && aCKeditorString.lastIndexOf(lParStart) == lStartIndex
				&& lEndIndex == (aCKeditorString.length() - lParEnd.length())
				&& aCKeditorString.lastIndexOf(lParEnd) == lEndIndex) {
			// NOTE only one pair of p tags surrounds text
			// Strip them
			aCKeditorString = aCKeditorString.substring(0, lEndIndex);
			aCKeditorString = aCKeditorString.substring(lStartIndex + lParStart.length());
		}
		// NOTE can start with space, strip it
		String lSpace = "&nbsp;";
		int lIndex = aCKeditorString.indexOf(lSpace);
		if (lIndex == 0) {
			aCKeditorString = aCKeditorString.substring(lIndex + lSpace.length());
		}
		return aCKeditorString;
	}

	public String stripHtmlFromCKeditorString(String html) {
		final StringBuffer buffer = new StringBuffer();

		ParserDelegator parserDelegator = new ParserDelegator();
		ParserCallback parserCallback = new ParserCallback() {
			@Override
			public void handleText(final char[] data, final int pos) {
				if (buffer.length() > 0) {
					buffer.append(" ");
				}
				buffer.append(data);
			}

			@Override
			public void handleStartTag(Tag tag, MutableAttributeSet attribute, int pos) {
			}

			@Override
			public void handleEndTag(Tag t, final int pos) {
			}

			@Override
			public void handleSimpleTag(Tag t, MutableAttributeSet a, final int pos) {
			}

			@Override
			public void handleComment(final char[] data, final int pos) {
			}

			@Override
			public void handleError(final java.lang.String errMsg, final int pos) {
			}
		};
		try {
			parserDelegator.parse(new StringReader(html), parserCallback, true);
		} catch (IOException e) {
			_log.error(e);
		}

		return buffer.toString();
	}

	public List<Object> splitHtmlCodesAndTexts(String html) {
		/*
		 * Removes all text strings from html returns them as a list of strings. Text
		 * strings in original html are replaced by [#%1#], [#%2#] .. [#%n#] and this
		 * adjusted html is returned as well.
		 */

		// NOTE replace \n characters by string, otherwise they are stripped by the
		// parser below.
		html = html.replaceAll("\n", AppConstants.statusCrLfReplace);

		List<Object> result = new ArrayList<Object>();
		final StringBuffer htmlCodes = new StringBuffer();
		final List<String> htmlTexts = new ArrayList<String>();

		ParserDelegator parserDelegator = new ParserDelegator();
		// NOTE ParserDelegator adds <html>, <head> and <body> tags previously not
		// existing within html parameter of this method.
		// Therefore methods of ParserCallback method below filter out this tags.
		ParserCallback parserCallback = new ParserCallback() {
			@Override
			public void handleText(final char[] data, final int pos) {
				htmlCodes.append(
						AppConstants.labelVarPrefix + "%" + (htmlTexts.size() + 1) + AppConstants.labelVarPostfix);
				// NOTE replace string by \n character
				htmlTexts.add(new String(data).replaceAll(AppConstants.statusCrLfReplace, "\n"));
			}

			private StringBuffer getAttributes(MutableAttributeSet attribute) {
				StringBuffer attributesBuffer = new StringBuffer();
				Enumeration<?> keys = attribute.getAttributeNames();
				while (keys.hasMoreElements()) {
					Object key = keys.nextElement();
					attributesBuffer.append(" ");
					attributesBuffer.append(key.toString());
					attributesBuffer.append("=\"");
					attributesBuffer.append(attribute.getAttribute(key));
					attributesBuffer.append("\"");
				}
				return attributesBuffer;
			}

			@Override
			public void handleStartTag(Tag tag, MutableAttributeSet attribute, int pos) {
				if (tag != HTML.Tag.HTML && tag != HTML.Tag.HEAD && tag != HTML.Tag.BODY) {
					htmlCodes.append("<");
					htmlCodes.append(tag.toString());
					htmlCodes.append(getAttributes(attribute));
					htmlCodes.append(">");
				}
			}

			@Override
			public void handleEndTag(Tag tag, final int pos) {
				if (tag != HTML.Tag.HTML && tag != HTML.Tag.HEAD && tag != HTML.Tag.BODY) {
					htmlCodes.append("</");
					htmlCodes.append(tag.toString());
					htmlCodes.append(">");
				}
			}

			@Override
			public void handleSimpleTag(Tag tag, MutableAttributeSet attribute, final int pos) {
				if (tag != HTML.Tag.HTML && tag != HTML.Tag.HEAD && tag != HTML.Tag.BODY) {
					htmlCodes.append("<");
					htmlCodes.append(tag.toString());
					htmlCodes.append(getAttributes(attribute));
					htmlCodes.append(" />");
				}
			}

			@Override
			public void handleComment(final char[] data, final int pos) {
			}

			@Override
			public void handleError(final java.lang.String errMsg, final int pos) {
			}
		};
		try {
			parserDelegator.parse(new StringReader(html), parserCallback, true);
		} catch (IOException e) {
			_log.error(e);
		}

		String htmlCodesStr = htmlCodes.toString();
		if (htmlCodesStr.equals(AppConstants.labelVarPrefix + "%1" + AppConstants.labelVarPostfix)) {
			// NOTE if parameter html does not contain html codes clear the string
			htmlCodesStr = "";
		}
		result.add(htmlCodesStr);
		result.add(htmlTexts);
		return result;
	}

	public String replaceSpecialCharacters(String string) {
		// NOTE use HTML parser to replace special characters, for instance &eacute;

		// NOTE replace \n characters by string, otherwise they are stripped by the
		// parser below.
		string = string.replaceAll("\n", AppConstants.statusCrLfReplace);

		final StringBuffer stringBuffer = new StringBuffer();

		ParserDelegator parserDelegator = new ParserDelegator();
		ParserCallback parserCallback = new ParserCallback() {
			@Override
			public void handleText(final char[] data, final int pos) {
				stringBuffer.append(new String(data).replaceAll(AppConstants.statusCrLfReplace, "\n"));
			}

			private StringBuffer getAttributes(MutableAttributeSet attribute) {
				StringBuffer attributesBuffer = new StringBuffer();
				Enumeration<?> keys = attribute.getAttributeNames();
				while (keys.hasMoreElements()) {
					Object key = keys.nextElement();
					attributesBuffer.append(" ");
					attributesBuffer.append(key.toString());
					attributesBuffer.append("=\"");
					attributesBuffer.append(attribute.getAttribute(key));
					attributesBuffer.append("\"");
				}
				return attributesBuffer;
			}

			@Override
			public void handleStartTag(Tag tag, MutableAttributeSet attribute, int pos) {
				if (tag != HTML.Tag.HTML && tag != HTML.Tag.HEAD && tag != HTML.Tag.BODY) {
					stringBuffer.append("<");
					stringBuffer.append(tag.toString());
					stringBuffer.append(getAttributes(attribute));
					stringBuffer.append(">");
				}
			}

			@Override
			public void handleEndTag(Tag tag, final int pos) {
				if (tag != HTML.Tag.HTML && tag != HTML.Tag.HEAD && tag != HTML.Tag.BODY) {
					stringBuffer.append("</");
					stringBuffer.append(tag.toString());
					stringBuffer.append(">");
				}
			}

			@Override
			public void handleSimpleTag(Tag tag, MutableAttributeSet attribute, final int pos) {
				if (tag != HTML.Tag.HTML && tag != HTML.Tag.HEAD && tag != HTML.Tag.BODY) {
					stringBuffer.append("<");
					stringBuffer.append(tag.toString());
					stringBuffer.append(getAttributes(attribute));
					stringBuffer.append(" />");
				}
			}

			@Override
			public void handleComment(final char[] data, final int pos) {
			}

			@Override
			public void handleError(final java.lang.String errMsg, final int pos) {
			}
		};
		try {
			parserDelegator.parse(new StringReader(string), parserCallback, true);
		} catch (IOException e) {
			_log.error(e);
		}

		return stringBuffer.toString();
	}

	/**
	 * Needed as global variables for subclass ParserCallback
	 */
	private int lCurrentLevel = 0;
	private String lBareText = "";

	/**
	 * Splits html body content into chunks.
	 *
	 * @param pHtml      the html content string (escaped)
	 * @param pChunkSize the (desired) maximum number of plaintext characters in the
	 *                   chunk
	 *
	 * @return list with html chunks
	 */
	public ArrayList<String> splitHtmlStringInChunks(String pHtml, int pChunkSize) {
		final ArrayList<String> lBareTextItems = new ArrayList<String>();
		final ArrayList<Integer> lTagLevel = new ArrayList<Integer>();
		final ArrayList<Integer> lItemPosInHtml = new ArrayList<Integer>();
		final ArrayList<Integer> lItemPosInBareText = new ArrayList<Integer>();
		lCurrentLevel = 0;
		lBareText = "";

		// NOTE before parsing html replace &rsquo; by '
		pHtml = pHtml.replaceAll("&rsquo;", "'");

		ParserDelegator parserDelegator = new ParserDelegator();
		ParserCallback parserCallback = new ParserCallback() {

			@Override
			public void handleText(final char[] data, final int pos) {
				String lItem = new String(data);
				lBareTextItems.add(new String(data));
				// lBareTextItems are unescaped!!
				lTagLevel.add(lCurrentLevel);
				lItemPosInHtml.add(pos);
				lItemPosInBareText.add(lBareText.length());
				lBareText += lItem;
			}

			@Override
			public void handleStartTag(Tag tag, MutableAttributeSet attribute, int pos) {
				// if (!tag.toString().matches("head|body|html"))
				if (tag.toString().matches("body"))
					// we only want to check HTML-body text
					// parserDelegator automatically tries to convert string to valid HTML, so adds
					// <html>, <head> etc if needed
					lCurrentLevel = 0;
				else
					lCurrentLevel++;
			}

			@Override
			public void handleEndTag(Tag t, final int pos) {
				// if (!t.toString().matches("head|body|html"))
				lCurrentLevel--;
			}

			@Override
			public void handleSimpleTag(Tag t, MutableAttributeSet a, final int pos) {
			}

			@Override
			public void handleComment(final char[] data, final int pos) {
			}

			@Override
			public void handleError(final java.lang.String errMsg, final int pos) {
			}
		};
		try {
			parserDelegator.parse(new StringReader(pHtml), parserCallback, true);
		} catch (IOException e) {
			_log.error(e);
		}

		// StringEscapeUtils doesn't escape quote; or '. HtmlUtils does, and escapes it
		// as decimal escape code (as is done in original html content string)
		pHtml = HtmlUtils.htmlEscape(pHtml);

		int lBareStartPos = 0;
		int lNewBareStartPos = 0;
		int lHtmlStartPos = 0;
		int lSEndIndex = -1;
		int lWEndIndex = -1;
		ArrayList<String> lHtmlChunks = new ArrayList<String>();
		for (int lInd = 0; lInd < lBareTextItems.size(); lInd++) {
			int lLevel = lTagLevel.get(lInd);
			String lCText = lBareTextItems.get(lInd);
			int lIPInBareText = lItemPosInBareText.get(lInd);
			int lIPInHtml = lItemPosInHtml.get(lInd);
			if (lLevel == 0) {
				boolean lHandled = false;
				while (!lHandled) {
					// level 0 elements may be split; we only split at end of sentence (preferably)
					// or whitespace
					// int lNewBareStartPos = lIPInBareText + lCText.length();
					if (lIPInBareText - lBareStartPos + lCText.length() > pChunkSize) {
						// more characters than wanted in current chunk, so try to truncate and store
						if (lIPInBareText - lBareStartPos < pChunkSize) {
							// border is crossed in current element, so try to find last allowed splitpoint
							String lText = lCText.substring(0, pChunkSize - lIPInBareText + lBareStartPos);
							int lCSEndIndex = priorityBalloonSplitEnd(lText);
							if (lCSEndIndex >= 0) {
								// end of line found in current chunk
								lSEndIndex = calculateEscapedPosition(lText, lCSEndIndex) + lIPInHtml;
								lNewBareStartPos = lIPInBareText + lCSEndIndex + 1;
							} else {
								if (lSEndIndex == -1) {
									// try to find whitespace, but only if previous elements don't contain end of
									// line
									int lCWEndIndex = lText.lastIndexOf(" ");
									if (lCWEndIndex >= 0) {
										lWEndIndex = calculateEscapedPosition(lText, lCWEndIndex) + lIPInHtml;
										lNewBareStartPos = lIPInBareText + lCWEndIndex + 1;
									}
								}
							}
						}
						if ((lSEndIndex == -1) && (lWEndIndex == -1)) {
							// no splitpoint found in desired range; try to find first available splitpoint
							// in current element
							int lP = priorityBalloonSplitStart(lCText);
							if (lP == -1) {
								lP = lCText.indexOf(" ");
								if (lP >= 0) {
									lWEndIndex = calculateEscapedPosition(lCText, lP) + lIPInHtml;
								}
							} else
								lSEndIndex = calculateEscapedPosition(lCText, lP) + lIPInHtml;
							if (lP >= 0)
								lNewBareStartPos = lIPInBareText + lP + 1;
						}
						if (lSEndIndex == -1)
							lSEndIndex = lWEndIndex;
						lHandled = true;
						if (lSEndIndex > -1) {
							// valid splitpoint found; store chunk, use original html because it may contain
							// tags removed by parser
							// unescape here?
							lHtmlChunks.add(
									StringEscapeUtils.unescapeHtml4(pHtml.substring(lHtmlStartPos, lSEndIndex + 1)));
							lHtmlStartPos = lSEndIndex + 1;
							lBareStartPos = lNewBareStartPos;
							lSEndIndex = -1;
							lWEndIndex = -1;
							if ((lBareStartPos > lIPInBareText)
									&& (lBareStartPos < (lIPInBareText + lCText.length()))) {
								// splitpoint found in current element; remainder of element can contain next
								// split point!
								lCText = lCText.substring(lBareStartPos - lIPInBareText);
								lIPInBareText = lBareStartPos;
								// position of remaining chunk in html is equal to starting position of next
								// html chunk
								lIPInHtml = lHtmlStartPos;
								lHandled = false;
							}
						} // else we cannot split; go on with next element
					} else {
						// maximum number of characters not yet reached; search for possible splitpoints
						int lCSEndIndex = priorityBalloonSplitEnd(lCText);
						if (lCSEndIndex >= 0) {
							lSEndIndex = calculateEscapedPosition(lCText, lCSEndIndex) + lIPInHtml;
							lNewBareStartPos = lIPInBareText + lCSEndIndex + 1;
						} else {
							if (lSEndIndex == -1) {
								int lCWEndIndex = lCText.lastIndexOf(" ");
								if (lCWEndIndex >= 0) {
									lWEndIndex = calculateEscapedPosition(lCText, lCWEndIndex) + lIPInHtml;
									lNewBareStartPos = lIPInBareText + lCWEndIndex + 1;
								}
							}
						}
						lHandled = true;
					}
				}
			} else {
				// level > 0 elements cannot be split because they are surrounded by HTML tag;
				// go on with next element
			}
		}
		if (lHtmlStartPos < pHtml.length()) {
			lHtmlChunks.add(StringEscapeUtils.unescapeHtml4(pHtml.substring(lHtmlStartPos)));
		}

		return lHtmlChunks;
	}

	private final static String balloonSplitPattern = ";|\\.|\\?|!|,";

	private int priorityBalloonSplitEnd(String pBareText) {
//		int lInd = Math.max(Math.max(pBareText.lastIndexOf("."), pBareText.lastIndexOf("!")), pBareText.lastIndexOf("?"));
		for (int lInd = pBareText.length() - 1; lInd > -1; lInd--) {
			if (pBareText.substring(lInd, lInd + 1).matches(balloonSplitPattern))
				return lInd;
		}
		return -1;
	}

	private int priorityBalloonSplitStart(String pBareText) {
		for (int lInd = 0; lInd < pBareText.length(); lInd++) {
			if (pBareText.substring(lInd, lInd + 1).matches(balloonSplitPattern))
				return lInd;
		}
		return -1;
	}

	private int calculateEscapedPosition(String pUnescaped, int pUntilIndex) {
		String lString = pUnescaped.substring(0, pUntilIndex);
		// StringEscapeUtils doesn't escape quote; or '. HtmlUtils does, and escapes it
		// as decimal escape code (as is done in original html content string)
		lString = HtmlUtils.htmlEscape(lString);
		return lString.length();
	}

}
