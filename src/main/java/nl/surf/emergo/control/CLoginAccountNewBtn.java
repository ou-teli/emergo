/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import java.util.Date;
import java.util.List;

import nl.surf.emergo.business.IRunManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.view.VView;

/**
 * The Class CLoginAccountNewBtn.
 * 
 * Enables a visitor to create an account, if open runs are present.
 */
public class CLoginAccountNewBtn extends CInputBtn {

	private static final long serialVersionUID = -218028609252310735L;

	public void onCreate() {
		setVisible(false);
		try {
			List<IERun> lOpenRuns = ((IRunManager)CDesktopComponents.sSpring().getBean("runManager")).getAllRunnableOpenAccessRuns();
			boolean lVisible = false;
			for (IERun lOpenRun : lOpenRuns) {
				if (isRunning(lOpenRun)) {
					lVisible = true;
					break;
				}
			}
			setVisible(lVisible);
		} catch (Exception e) {
		}
	}
	
	/**
	 * Checks if aRun is running.
	 *
	 * @param aRun the a run
	 *
	 * @return true, if is running
	 */
	private boolean isRunning(IERun aRun) {
		//student can only access active runs
		if (!aRun.getActive())
			return false;
		if (aRun.getStatus() != AppConstants.run_status_runnable)
			return false;
		boolean lRunning = true;
		Date lCurrentDate = new Date();
		Date lStartDate = aRun.getStartdate();
		//NOTE end date is not checked, it is only an indication. Students should be able to open the case after the end date.
		if ((lStartDate != null) && (lStartDate.after(lCurrentDate)))
			lRunning = false;
		return lRunning;
	}

	/**
	 * On click show account creation window.
	 */
	public void onClick() {
		showPopup(VView.v_login_s_account);
	}
	
}
