/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import java.util.HashMap;
import java.util.Map;

import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.Executions;

import com.xuggle.xuggler.IContainer;

import nl.surf.emergo.control.script.CScript;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpring;

/**
 * The Class DesktopComponents.
 *
 * <p>Gives access to desktop components.</p>
 */
public class CDesktopComponents {
	
	public static final String desktopAttributeKeyVView = "vViewInstance";

	public static final String desktopAttributeKeyCControl = "cControlInstance";

	public static final String desktopAttributeKeySSpring = "sSpringInstance";

	public static final String desktopAttributeKeyCScript = "cScriptInstance";
	
	public static final String desktopAttributeKeyStreamsContainer = "streamsContainerInstance";
	
	protected static Map<String,Object> objectMap = new HashMap<String,Object>();

	/** Prevents instantiation */
	private CDesktopComponents() { }
	
	public static final VView vView() {
		return (VView)getComponent(desktopAttributeKeyVView);
	}

	public static final CControl cControl() {
		return (CControl)getComponent(desktopAttributeKeyCControl);
	}

	public static final SSpring sSpring() {
		return (SSpring)getComponent(desktopAttributeKeySSpring);
	}

	public static final CScript cScript() {
		return (CScript)getComponent(desktopAttributeKeyCScript);
	}

	public static final IContainer streamsContainer() {
		return (IContainer)getComponent(desktopAttributeKeyStreamsContainer);
	}

	public static final void setVView(VView vView) {
		setComponent(desktopAttributeKeyVView, vView);
	}

	public static final void setCControl(CControl cControl) {
		setComponent(desktopAttributeKeyCControl, cControl);
	}

	public static final void setSSpring(SSpring sSpring) {
		setComponent(desktopAttributeKeySSpring, sSpring);
	}

	public static final void setCScript(CScript cScript) {
		setComponent(desktopAttributeKeyCScript, cScript);
	}

	public static final void setStreamsContainer(IContainer streamsContainer) {
		setComponent(desktopAttributeKeyStreamsContainer, streamsContainer);
	}

	protected static final Object getComponent(String desktopAttributeKey) {
		Desktop desktop = null;
		if (Executions.getCurrent() != null) {
			desktop = Executions.getCurrent().getDesktop();
		}
		Object object = null;
		if (desktop == null && objectMap.containsKey(desktopAttributeKey)) {
			return objectMap.get(desktopAttributeKey);
		}
		if (desktop == null || desktop.getAttribute(desktopAttributeKey) == null) {
			if (desktopAttributeKey.equals(desktopAttributeKeyVView)) {
				object = new VView();
			}
			else if (desktopAttributeKey.equals(desktopAttributeKeyCControl)) {
				object = new CControl();
			}
			else if (desktopAttributeKey.equals(desktopAttributeKeySSpring)) {
				object = new SSpring();
			}
			else if (desktopAttributeKey.equals(desktopAttributeKeyCScript)) {
				object = new CScript();
			}
			else if (desktopAttributeKey.equals(desktopAttributeKeyStreamsContainer)) {
				object = IContainer.make();
			}
			if (desktop != null && object != null) {
				desktop.setAttribute(desktopAttributeKey, object);
			}
		}
		if (desktop == null) {
			return object;
		}
		else {
			return desktop.getAttribute(desktopAttributeKey);
		}
	}

	protected static final void setComponent(String desktopAttributeKey, Object component) {
		Desktop desktop = null;
		if (Executions.getCurrent() != null) {
			desktop = Executions.getCurrent().getDesktop();
		}
		if (desktop == null) {
			if (!objectMap.containsKey(desktopAttributeKey) || objectMap.get(desktopAttributeKey) != component) {
				objectMap.put(desktopAttributeKey, component);
			}
		}
		else if (desktop.getAttribute(desktopAttributeKey) == null || desktop.getAttribute(desktopAttributeKey) != component) {
			desktop.setAttribute(desktopAttributeKey, component);
		}
	}

}