/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.util.DesktopCleanup;

import nl.surf.emergo.control.run.CRunWnd;
import nl.surf.emergo.utilities.FileHelper;

/**
 * The Class CRunDesktopCleanup.
 * A listener which is called by ZK if the desktop, the browser instance, is closed.
 * If the player is closed this class cleans up the player, that is saves unsaved status.
 */
public class CDesktopCleanup implements DesktopCleanup {

	/** The run wnd. */
	protected CRunWnd runWnd = null;

	public CDesktopCleanup () {
		Object object = CDesktopComponents.vView().getComponent(CControl.runWnd);
		if (object != null && object instanceof CRunWnd) {
			runWnd = (CRunWnd)object;
		}
	}

	/* (non-Javadoc)
	 * @see org.zkoss.zk.ui.util.DesktopCleanup#cleanup(org.zkoss.zk.ui.Desktop)
	 */
	public void cleanup(Desktop desktop) throws Exception {
		if ((runWnd != null) && (desktop.getId().equals(runWnd.desktopId))) {
			//NOTE runWnd.cleanup handles removing temporary files possibly created during uploading files.
			runWnd.cleanup();
		}
		else {
			//NOTE for other desktops remove temporary files possibly created during uploading files.
			removeTempFiles();
		}
	}

	/**
	 * Removes temp files from 'temp' folder.
	 *
	 */
	protected synchronized void removeTempFiles() {
		//every desktop has a unique temp folder, remove this temp folder
		String lSubPath = CDesktopComponents.vView().getUniqueTempSubPath();
		if (!lSubPath.equals("")) {
			(new FileHelper()).deleteDir(CDesktopComponents.sSpring().getAppManager().getAbsoluteTempPath() + lSubPath);
		}
	}
	
}
