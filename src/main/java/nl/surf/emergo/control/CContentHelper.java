/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.gmaps.Gmarker;
import org.zkoss.util.media.Media;
import org.zkoss.zhtml.Hr;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Html;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Row;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treechildren;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.Treerow;
import org.zkoss.zul.Vbox;
import org.zkoss.zul.impl.InputElement;
import org.zkoss.zul.impl.XulElement;

import bsh.EvalError;
import bsh.Interpreter;
import nl.surf.emergo.business.IAppManager;
import nl.surf.emergo.business.IComponentManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.cde.CCdeComponentItemWnd;
import nl.surf.emergo.control.cde.CCdeComponentWnd;
import nl.surf.emergo.control.cde.CCdeMakePlainTextBtn;
import nl.surf.emergo.control.cde.CCdePreviewRichtextBtn;
import nl.surf.emergo.control.def.CDefCKeditor;
import nl.surf.emergo.control.def.CDefCheckbox;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.def.CDefHbox;
import nl.surf.emergo.control.def.CDefHtml;
import nl.surf.emergo.control.def.CDefImage;
import nl.surf.emergo.control.def.CDefLabel;
import nl.surf.emergo.control.def.CDefListbox;
import nl.surf.emergo.control.def.CDefRadio;
import nl.surf.emergo.control.def.CDefRow;
import nl.surf.emergo.control.def.CDefTextbox;
import nl.surf.emergo.control.def.CDefTreechildren;
import nl.surf.emergo.control.def.CDefVbox;
import nl.surf.emergo.control.script.CScript;
import nl.surf.emergo.control.script.CScriptCacCombo;
import nl.surf.emergo.control.script.CScriptCarListbox;
import nl.surf.emergo.control.script.CScriptComCombo;
import nl.surf.emergo.control.script.CScriptFunctionCombo;
import nl.surf.emergo.control.script.CScriptLabel;
import nl.surf.emergo.control.script.CScriptMethodHbox;
import nl.surf.emergo.control.script.CScriptOperatorCombo;
import nl.surf.emergo.control.script.CScriptOperatorValueCombo;
import nl.surf.emergo.control.script.CScriptStatusCombo;
import nl.surf.emergo.control.script.CScriptTagCombo;
import nl.surf.emergo.control.script.CScriptTagContentCombo;
import nl.surf.emergo.control.script.CScriptTagListbox;
import nl.surf.emergo.control.script.CScriptTagNameCombo;
import nl.surf.emergo.domain.IEBlob;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.domain.IEComponent;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpring;

/**
 * The Class CContentHelper.
 *
 * Used to render and update the case component author page containing
 * a number of checkboxes for the component part of the xml data and
 * a zk components for the content part of the xml data.
 *
 * Used to add or edit a case component xml data node tag.
 * Does this by rendering the tag as input elements.
 */
public class CContentHelper extends CXmlHelper {

	/** The component. */
	protected Component component = null;

	protected IAppManager appManager = (IAppManager)CDesktopComponents.sSpring().getAppManager();

	/** The current case component code. */
	public String currentCaseComponentCode = "";
	
	/** Constants used in method showStatus. Note that adding one item in a String[] array also implies adding one in the other two arrays. */
	protected static final String showStatusLabelKeyPrefix = "statuskey.caption.";

	protected static final String[] showStatusKeys = {
			AppConstants.statusKeyPresent,
			AppConstants.statusKeyOpened,
			AppConstants.statusKeyOutfolded, 
			AppConstants.statusKeyOutfoldable,
			AppConstants.statusKeyRequired, 
			AppConstants.statusKeyAccessible
			}; 

	protected static final String[] defaultShowStatusValues = {
			"false", 
			"false", 
			"false", 
			"false", 
			"false", 
			"false"
			};
	
	protected static final String[] showStatusLabelValues = {
			"[" + CDesktopComponents.vView().getLabel(showStatusLabelKeyPrefix + AppConstants.statusKeyPresentIndex) + "]",
			"[" + CDesktopComponents.vView().getLabel(showStatusLabelKeyPrefix + AppConstants.statusKeyOpenedIndex) + "]",
			"[" + CDesktopComponents.vView().getLabel(showStatusLabelKeyPrefix + AppConstants.statusKeyOutfoldedIndex) + "]",
			"[" + CDesktopComponents.vView().getLabel(showStatusLabelKeyPrefix + AppConstants.statusKeyOutfoldableIndex) + "]",
			"[" + CDesktopComponents.vView().getLabel(showStatusLabelKeyPrefix + AppConstants.statusKeyRequiredIndex) + "]",
			"[" + CDesktopComponents.vView().getLabel(showStatusLabelKeyPrefix + AppConstants.statusKeyAccessibleIndex) + "]"
			};

	/**
	 * Instantiates a new component helper.
	 */
	public CContentHelper() {
		super();
	}

	/**
	 * Instantiates a new component helper.
	 *
	 * @param aComponent the component
	 */
	public CContentHelper(Component aComponent) {
		super();
		component = aComponent;
	}

	/**
	 * Instantiates a new component helper.
	 *
	 * @param aComponent the component
	 * @param aSSpring the spring to use
	 */
	public CContentHelper(Component aComponent, SSpring aSSpring) {
		super(aSSpring);
		component = aComponent;
	}

	/**
	 * Gets the edit view, the page for editing a component item and its associated xml tag.
	 *
	 * @return the edit view
	 */
	public String getEditView() {
		return VView.v_cde_s_component;
	}

	/**
	 * Gets new choose file button.
	 *
	 * @return new choose file button
	 */
	public CChooseFileButton newChooseFileButton() {
		return new CChooseFileButton();
	}

	/**
	 * Gets the delete view, the page for deleting a componentitem and its associated xml tag.
	 *
	 * @return the delete view
	 */
	public String getDeleteView() {
		return VView.v_delete_item;
	}

	/**
	 * Gets the drop as sibling or child view, the page to choose to drop a componentitem and its associated xml tag as sibling or child.
	 *
	 * @return the delete view
	 */
	public String getDropItemAsSiblingOrChildView() {
		return VView.v_drop_item_as_sibling_or_child;
	}

	/**
	 * Gets the preview view, the page for previewing a componentitem and its associated xml tag.
	 *
	 * @return the preview view
	 */
	public String getPreviewView() {
		return VView.v_preview_item;
	}

	/**
	 * Gets the script view, the page for script authoring of a componentitem and its associated xml tag.
	 * Not implemented yet.
	 *
	 * @return the script view
	 */
	public String getScriptView() {
		return VView.v_cde_s_script;
	}

	/**
	 * Gets the blob. See SSpring.
	 *
	 * @param bloId the db blo id
	 *
	 * @return the blob
	 */
	public IEBlob getBlob(String bloId) {
		return CDesktopComponents.sSpring().getSBlobHelper().getBlob(bloId);
	}

	/**
	 * Gets the media src. See SSpring.
	 *
	 * @param bloId the db blo id
	 *
	 * @return the media src
	 */
	public String getMediaSrc(String bloId) {
		return CDesktopComponents.sSpring().getSBlobHelper().getMediaSrc(bloId);
	}

	/**
	 * Gets the image src. See SSpring.
	 *
	 * @param bloId the db blo id
	 *
	 * @return the image src
	 */
	public String getImageSrc(String bloId) {
		return CDesktopComponents.sSpring().getSBlobHelper().getImageSrc(bloId);
	}

	/**
	 * Gets the audio src. See SSpring.
	 *
	 * @param bloId the db blo id
	 *
	 * @return the audio src
	 */
	public String getAudioSrc(String bloId) {
		return CDesktopComponents.sSpring().getSBlobHelper().getAudioSrc(bloId);
	}

	/**
	 * Sets media. See SSpring.
	 *
	 * @param bloId the db blo id
	 * @param aMedia the media
	 *
	 * @return the (new) blo id
	 */
	public String setBlobMedia(String bloId, Media aMedia) {
		return CDesktopComponents.sSpring().getSBlobHelper().setBlobMedia(bloId, aMedia);
	}

	/**
	 * Sets content. See SSpring.
	 *
	 * @param bloId the db blo id
	 * @param aContent the content
	 *
	 * @return the (new) blo id
	 */
	public String setBlobContent(String bloId, byte[] aContent) {
		return CDesktopComponents.sSpring().getSBlobHelper().setBlobContent(bloId, aContent);
	}

	/**
	 * Sets url. See SSpring.
	 *
	 * @param bloId the db blo id
	 * @param aUrl the url
	 *
	 * @return the (new) blo id
	 */
	public String setBlobUrl(String bloId, String aUrl) {
		return CDesktopComponents.sSpring().getSBlobHelper().setBlobUrl(bloId, aUrl);
	}

	/**
	 * Deletes blob. See SSpring.
	 *
	 * @param bloId the db blo id
	 */
	public void deleteBlob(String bloId) {
		CDesktopComponents.sSpring().getSBlobHelper().deleteBlob(bloId);
	}

	/**
	 * Check if current account is author of the current case component.
	 *
	 * @param aObject a ZK object on the case component author page
	 *
	 * @return true, if successful
	 */
	public boolean accIsAuthor(Component aObject) {
		if (aObject == null) return false;
		if (aObject.getRoot() == null) return false;
		if ((aObject.getRoot()) instanceof CCdeComponentWnd)
			// CCdeComponentWnd is case component author window
			return ((CCdeComponentWnd) aObject.getRoot()).accIsAuthor();
		else {
			if (((String)CDesktopComponents.cControl().getAccSessAttr("accisauthor")).equals("true"))
				return true;
			else
				return false;
		}
	}

	/**
	 * Gets the script.
	 *
	 * @return the script
	 */
	protected CScript getScript() {
		return CDesktopComponents.cScript();
	}

	/**
	 * Renders checkbox.
	 *
	 * @param aRows a ZK parent
	 * @param aType the type of the property to be checked
	 * @param aKey the key of the property
	 * @param aValue the current value of the property
	 *
	 * @return the new checkbox
	 */
	public CDefCheckbox renderCheckbox(Component aRows, String aType, String aKey,	String aValue) {
		CDefCheckbox lCheckbox = new CComponentStatusCb();
		lCheckbox.setId(aKey);
		lCheckbox.setChecked(aValue.equals("true"));
		lCheckbox.setAttribute("type", aType);
		lCheckbox.setSclass("CCdeComponentWnd_row_" + aKey + "_" + aType + "_" + "checkbox");
		if (!accIsAuthor(aRows))
			// if no author disable
			lCheckbox.setDisabled(true);
		return lCheckbox;
	}

	/**
	 * Renders line.
	 *
	 * @param aRows a ZK parent
	 * @param aType the type of the property to be checked
	 * @param aKey the key of the property
	 * @param aValue the current value of the property
	 *
	 * @return the new line
	 */
	public CDefTextbox renderLine(Component aRows, String aType, String aKey, String aValue) {
		CDefTextbox lTextbox = new CComponentStatusTb();
		lTextbox.setId(aKey);
		lTextbox.setValue(aValue);
		lTextbox.setAttribute("type", aType);
		lTextbox.setSclass("CCdeComponentWnd_row_" + aKey + "_" + aType + "_" + "textbox");
		lTextbox.setWidth("" + CDesktopComponents.vView().getColWidth2() + "px");
		if (!accIsAuthor(aRows))
			// if no author disable
			lTextbox.setDisabled(true);
		return lTextbox;
	}

	/**
	 * Gets node tag label.
	 *
	 * @param aComponentCode the component code
	 * @param aNodeTagName the node tag name
	 *
	 * @return label
	 */
	public static String getNodeTagLabel(String aComponentCode, String aNodeTagName) {
		/*
		NOTE Labels for node tags may exist on different levels from lower to higher.
		Higher labels overrule lower labels.
		For instance, if there is a general label for the 'location' node tag, it may be overruled by a label for a specific case component.
		
		There are two levels of labels for node tags, where higher level labels overrule lower level labels, e.g.:
		1 nodetag.feedbackcondition					- label key for all feedback conditions
		2 nodetag.conversations.feedbackcondition	- label key for feedback condition within the conversations component
		*/

		String lLabelKey = "";
		String lLabel = "";
		String[] lLabelKeyParts = new String[] 
			{
				aComponentCode + "." + aNodeTagName,
				aNodeTagName
			};
		for (int i=0;i<lLabelKeyParts.length;i++) {
			if (lLabel.length() == 0) {
				lLabelKey = VView.nodetagLabelKeyPrefix + lLabelKeyParts[i];
				lLabel = CDesktopComponents.vView().getLabel(lLabelKey);
			}
		}
		if (lLabel.length() == 0) {
			lLabel = aNodeTagName;
		}
		return lLabel;
	}

	/**
	 * Gets label key and value.
	 *
	 * @param aKey the key
	 * @param aLabelKeyPrefix the label key prefix
	 *
	 * @return label key and value
	 */
	protected String[] getLabelKeyAndValue(String aKey, String aLabelKeyPrefix) {
		String lLabelKey = "";
		String lLabelValue = "";
		lLabelKey = aLabelKeyPrefix + "caption." + aKey;
		lLabelValue = CDesktopComponents.vView().getCLabel(lLabelKey);
		if (lLabelValue.length() == 0) {
			lLabelKey = aLabelKeyPrefix + aKey;
			lLabelValue = CDesktopComponents.vView().getCLabel(lLabelKey);
		}
		if (lLabelValue.length() == 0) {
			lLabelValue = VView.getCapitalizeFirstChar(aKey);
		}
		return new String[] {lLabelKey, lLabelValue};
	}

	/**
	 * Gets key for specific input row.
	 *
	 * @param aRow the ZK rows parent
	 *
	 * @return key
	 */
	protected String getKeyForRow(Hashtable<String,Object> aRow) {
		String lKey = "";
		if (aRow.get("attribute").equals("")) {
			lKey = (String)aRow.get("name");
		}
		else {
			int lKeyIndex = appManager.getStatusKeyIndex((String)aRow.get("attribute"));
			if (lKeyIndex != -1) {
				lKey = "" + lKeyIndex;
			}
			else {
				lKey = VView.getCapitalizeFirstChar((String)aRow.get("attribute"));
			}
		}
		return lKey;
	}

	/**
	 * Gets label key prefix for specific input row.
	 *
	 * @param aRow the ZK rows parent
	 *
	 * @return label key prefix
	 */
	protected String getLabelKeyPrefixForRow(Hashtable<String,Object> aRow) {
		if (aRow.get("attribute").equals("")) {
			return VView.childtagLabelKeyPrefix;
		}
		else {
			return VView.statuskeyLabelKeyPrefix;
		}
	}

	/**
	 * Gets label key and value for specific input row.
	 *
	 * @param aRow the ZK rows parent
	 *
	 * @return label key and value
	 */
	protected String[] getLabelKeyAndValue(Hashtable<String,Object> aRow) {
		return getLabelKeyAndValue(getKeyForRow(aRow), getLabelKeyPrefixForRow(aRow));
	}

	/**
	 * Gets label text. If aLabelKeyPrefix equals VView.nodetagLabelKeyPrefix, it is a label text for a node tag (aKey is neglected), 
	 * otherwise it is a label text for a status key of tag content child.
	 *
	 * @param aComponentCode the component code
	 * @param aNodeTagName the node tag name
	 * @param aKey the key
	 * @param aLabelKeyPrefix the label key prefix
	 *
	 * @return help text
	 */
	public static String getLabelText(String aComponentCode, String aNodeTagName, String aKey, String aLabelKeyPrefix) {
		return getLabelTextOrHelpText(aComponentCode, aNodeTagName, aKey, aLabelKeyPrefix, "");
	}
	
	/**
	 * Gets help text. If aLabelKeyPrefix equals VView.nodetagLabelKeyPrefix, it is a help text for a node tag (aKey is neglected), otherwise it is a help text for a status key of tag content child.
	 *
	 * @param aComponentCode the component code
	 * @param aNodeTagName the node tag name
	 * @param aKey the key
	 * @param aLabelKeyPrefix the label key prefix
	 *
	 * @return help text
	 */
	public static String getHelpText(String aComponentCode, String aNodeTagName, String aKey, String aLabelKeyPrefix) {
		return getLabelTextOrHelpText(aComponentCode, aNodeTagName, aKey, aLabelKeyPrefix, "help");
	}
	
	/**
	 * Gets label text or help text. If aLabelKeyPrefix equals VView.nodetagLabelKeyPrefix, it is a help text for a node tag (aKey is neglected), 
	 * otherwise it is a help text for a status key of tag content child.
	 *
	 * @param aComponentCode the component code
	 * @param aNodeTagName the node tag name
	 * @param aKey the key
	 * @param aLabelKeyPrefix the label key prefix
	 * @param aPostfix the label key postfix
	 *
	 * @return help text
	 */
	public static String getLabelTextOrHelpText(String aComponentCode, String aNodeTagName, String aKey, String aLabelKeyPrefix, String aPostfix) {
		/*
		NOTE Help labels for node tags, node tag status keys and node tag content children may exist on different levels from lower to higher.
		Higher labels overrule lower labels.
		For instance, if there is a general label for the 'present' property, it may be overruled by a label for a specific case component or node tag.
		The same accounts for the node tag content children like for instance a name or a text.
		
		There are two levels of help labels for node tags, where higher level labels overrule lower level labels, e.g.:
		1 nodetag.feedbackcondition.help								- help label key for all feedback conditions
		2 nodetag.conversations.feedbackcondition.help	- help label key for feedback condition within the conversations component
		
		There are two levels of help labels for status keys of case components, where higher level labels overrule lower level labels, e.g.:
		1 statuskey.casecomponent.3.help		- label key for present, for all case components
		2 statuskey.navigation.3.help			- label key for present, specifically for the navigation component
		There are three levels of help labels for status keys of node tags, where higher level labels overrule lower level labels, e.g.:
		1 statuskey.nodetag.3.help				- label key for present, for all node tags
		2 statuskey.passage.3.help				- label key for present, specifically for passage tags
		3 statuskey.navigation.passage.3.help	- label key for present, specifically for passage tags within the navigation component
		
		There are four levels of help labels for child tags of node tags, where higher level labels overrule lower level labels, e.g.:
		1 childtag.nodetag.text.help			- help label key for text, for all node tags
		2 childtag.passage.text.help			- help label key for text, specifically for passage tags
		3 childtag.navigation.text.help			- help label key for text, specifically for the navigation component
		4 childtag.navigation.passage.text.help	- help label key for text, specifically for passage tags within the navigation component
		
		For status keys the label key may contain 'caption.' as well. A key with 'caption.' overrules the same one without 'caption.' 
		*/

		String lLabelKey = "";
		String lHelpText = "";
		String lSubject = "";
		String[] lLabelKeyParts = null;
		String lComponentCode = aComponentCode;
		String lNodeTagName = aNodeTagName;
		if (aLabelKeyPrefix.startsWith(VView.nodetagLabelKeyPrefix)) {
			//Help text for a node tag
			if (!(aNodeTagName.equals("") || aPostfix.equals("")))
				lNodeTagName = lNodeTagName + ".";
			if (!(aNodeTagName.equals("") && aPostfix.equals("")))
				if (!lComponentCode.equals(""))
					lComponentCode = lComponentCode + ".";
			lLabelKeyParts = new String[] 
				{
					lComponentCode + lNodeTagName,
					lNodeTagName
				};
			for (int i=0;i<lLabelKeyParts.length;i++) {
				if (lHelpText.length() == 0) {
					lLabelKey = aLabelKeyPrefix + lLabelKeyParts[i] + aPostfix;
					lHelpText = CDesktopComponents.vView().getLabel(lLabelKey);
				}
			}
		}
		else if (aLabelKeyPrefix.startsWith(VView.statuskeyLabelKeyPrefix) || aLabelKeyPrefix.startsWith(VView.childtagLabelKeyPrefix)) {
			//Help text for a status key of tag content child
			String lKey = aKey;
			if (!(lKey.equals("") || aPostfix.equals(""))) {
				lKey = lKey + ".";
			}
			if (!(aKey.equals("") && aPostfix.equals(""))) {
				if (!lComponentCode.equals(""))
					lComponentCode = lComponentCode + ".";
				if (!lNodeTagName.equals(""))
					lNodeTagName = lNodeTagName + ".";
			}
			boolean lIsForCaseComponent = aNodeTagName.equals(AppConstants.componentElement);
			if (lIsForCaseComponent) {
				lSubject = "casecomponent";
				if (!(aKey.equals("") && aPostfix.equals(""))) {
					lSubject = lSubject + ".";
					if (!lComponentCode.equals(""))
						lComponentCode = lComponentCode + ".";
				}
				lLabelKeyParts = new String[] 
					{
						"caption." + lComponentCode, 
						lComponentCode, 
						"caption." + lSubject, 
						lSubject,
						""
					};
			}
			else {
				lSubject = "nodetag";
				String lComponentCode2 = aComponentCode + ".";
				if (!(aKey.equals("") && aPostfix.equals(""))) {
					lSubject = lSubject + ".";
				} else
					if (aNodeTagName.equals("")) {
						lComponentCode2 = aComponentCode;
					}
				lLabelKeyParts = new String[]
					{
						"caption." + lComponentCode2 + lNodeTagName,
						lComponentCode2 + lNodeTagName,
						"caption." + lComponentCode,
						lComponentCode,
						"caption." + lNodeTagName,
						lNodeTagName,
						"caption." + lSubject,
						lSubject,
						""
					};
			}
			for (int i=0;i<lLabelKeyParts.length;i++) {
				if (lHelpText.length() == 0) {
					lLabelKey = aLabelKeyPrefix + lLabelKeyParts[i] + lKey + aPostfix;
					lHelpText = CDesktopComponents.vView().getLabel(lLabelKey);
				}
			}
		}
		if (lHelpText.length() > 0) {
			lHelpText = lHelpText
					.replace("%1", CDesktopComponents.vView().getLabel(lSubject + ".article"))
					.replace("%2", CDesktopComponents.vView().getLabel(lSubject));
		}
		return lHelpText;
	}

	/**
	 * Gets help text for specific input row.
	 *
	 * @param aIsNodeTag the is node tag
	 *
	 * @return help text
	 */
	protected String getHelpText(Hashtable<String,Object> aRow) {
		return getHelpText(currentCaseComponentCode, ((IXMLTag)aRow.get("defnodetag")).getName(), getKeyForRow(aRow), getLabelKeyPrefixForRow(aRow));
	}

	/**
	 * Renders row with label and input element. Currently only checkbox is supported.
	 *
	 * @param aRows the ZK rows parent
	 * @param aRow the ZK rows parent
	 */
	public void renderComponentRow(Component aRows, Hashtable<String,Object> aRow) {
		String lType = (String)aRow.get("type");
		String lKey = "";
		if (aRow.get("attribute").equals("")) {
			lKey = (String)aRow.get("name");
		}
		else {
			lKey = (String)aRow.get("attribute");
		}
		String[] lLabelKeyAndValue = getLabelKeyAndValue(aRow);
		String lLabelKey = lLabelKeyAndValue[0];
		String lLabelValue = lLabelKeyAndValue[1];
		String lValue = (String)aRow.get("value");
		Row lRow = new CDefRow();
		Hbox lParentHbox = new CDefHbox();
		lRow.appendChild(lParentHbox);
		Div lDiv = new CDefDiv();
		lDiv.setWidth("" + (CDesktopComponents.vView().getColWidth1() - 15) + "px");
		lParentHbox.appendChild(lDiv);
		Label lLabel = new CDefLabel();
		lLabel.setValue(lLabelValue);
		lDiv.appendChild(lLabel);
		if (lType.equals("boolean"))
			lRow.appendChild(renderCheckbox(aRows, lType, lKey, lValue));
		else if (lType.equals("line"))
			lRow.appendChild(renderLine(aRows, lType, lKey, lValue));
		lRow.setSclass("CCdeComponentWnd_row_" + lKey + "_" + lType);
		aRows.appendChild(lRow);
		
		CDesktopComponents.vView().setParentHelpText(lParentHbox, getHelpText(aRow));
	}

	/**
	 * Converts xml data component part to data rows that are rendered.
	 *
	 * @param aXmlTree the xml tree containing the xml data
	 * @param aRows the ZK rows to render the data rows in
	 */
	public void xmlComponentToRows(IXMLTag aXmlTree, Component aRows) {
		currentCaseComponentCode = getCaseComponent().getEComponent().getCode();
		List<Hashtable<String,Object>> rows = CDesktopComponents.sSpring().getXmlManager().getComponentData(aXmlTree);
		for (Hashtable<String,Object> row : rows) {
			renderComponentRow(aRows, row);
		}
	}

	/**
	 * Sets attributes of ZK component using xml tag aItem.
	 *
	 * @param aObject the ZK component
	 * @param aItem the xml tag
	 *
	 * @return the key values of the xml tag, comma separated
	 */
	public String setItemAttributes(Component aObject, IXMLTag aItem) {
		// save xml tag
		aObject.setAttribute("casecomponent", getCaseComponent());
		aObject.setAttribute("item", aItem);
		String lKey = aItem.getDefAttribute(AppConstants.defKeyKey);
		String lKeyValues = CDesktopComponents.sSpring().unescapeXML(CDesktopComponents.sSpring().getXmlManager().getTagKeyValues(aItem, lKey));
		// save key values
		aObject.setAttribute("keyvalues", lKeyValues);
		return lKeyValues;
	}

	/**
	 * Sets the context menu of a ZK component within the case component author page.
	 *
	 * @param aObject the ZK component
	 */
	public void setContextMenu(Component aObject) {
		// menu popup is an element within a zul page
		((XulElement) aObject).setContext("menuPopup");
	}

	/**
	 * Shows status info of xml tag within treecell.
	 *
	 * @param aObject the treecell
	 * @param aItem the xml tag
	 */
	protected void showStatus(Treecell aObject, IXMLTag aItem) {
		String[] lStatusValues = defaultShowStatusValues.clone();
		IXMLTag lChild = aItem.getChild(AppConstants.statusElement);
		if (lChild != null) {
			for (int i = 0; i < showStatusKeys.length; i++) {
				if (lChild.getAttribute(showStatusKeys[i]).equals(AppConstants.statusValueTrue))
					lStatusValues[i] = "true";
			}
		}
		for (int i = 0; i < showStatusKeys.length; i++) {
			Label lLabel = getComponentLabel(aObject, showStatusKeys[i], showStatusLabelValues[i]);
			lLabel.setSclass("CCdeComponentWnd_status_label");
			lLabel.setAttribute("statuskey", showStatusKeys[i]);
			lLabel.setVisible(lStatusValues[i].equals("true"));
		}
	}

	/**
	 * Renders treecell using xml tag aItem.
	 * If treecell doesn't exist all elements within the treecell are created.
	 * If treecell exists it is decorated, so existing elements should be updated!
	 *
	 * @param aTreeitem the treeitem
	 * @param aTreerow the treerow parent
	 * @param aItem the xml tag
	 * @param aKeyValues the key values
	 * @param aAccIsAuthor the account is author state
	 *
	 * @return the new treecell
	 */
	protected Treecell renderTreecell(Treeitem aTreeitem, Treerow aTreerow,
			IXMLTag aItem, String aKeyValues, boolean aAccIsAuthor) {
		Treecell lTreecell = getTreerowTreecell(aTreerow, 0);
		String lTagName = getNodeTagLabel(currentCaseComponentCode, aItem.getName());
		Label lLabel = getTreecellLabel(lTreecell, 0, lTagName + " ");
		lLabel.setSclass("CCdeComponentWnd_treecell_label");
		lLabel = getTreecellLabel(lTreecell, 1, aKeyValues);
		return lTreecell;
	}

	/**
	 * Gets treerow treecell.
	 *
	 * @param aTreerow the treerow parent
	 * @param aIndex the a index
	 *
	 * @return the treecell
	 */
	protected Treecell getTreerowTreecell(Treerow aTreerow, int aIndex) {
		Treecell lTreecell = null;
		if (aTreerow.getChildren().size() > aIndex) {
			lTreecell = (Treecell)aTreerow.getChildren().get(aIndex);
		}
		else {
			lTreecell = new CTreecell();
			aTreerow.appendChild(lTreecell);
		}
		return lTreecell;
	}

	/**
	 * Gets treerow treecell.
	 *
	 * @param aTreerow the treerow parent
	 * @param aIndex the a index
	 * @param aLabel the a label
	 *
	 * @return the treecell
	 */
	protected Treecell getTreerowTreecell(Treerow aTreerow, int aIndex, String aLabel) {
		Treecell lTreecell = getTreerowTreecell(aTreerow, aIndex);
		lTreecell.setLabel(aLabel);
		return lTreecell;
	}

	/**
	 * Gets treecell label.
	 *
	 * @param aTreecell the treecell parent
	 * @param aIndex the a index
	 *
	 * @return the label
	 */
	protected Label getTreecellLabel(Treecell aTreecell, int aIndex) {
		Label lLabel = null;
		if (aTreecell.getChildren().size() > aIndex) {
			lLabel = (Label)aTreecell.getChildren().get(aIndex);
		}
		else {
			lLabel = new CDefLabel();
			aTreecell.appendChild(lLabel);
		}
		return lLabel;
	}

	/**
	 * Gets treecell label.
	 *
	 * @param aTreecell the treecell parent
	 * @param aIndex the a index
	 * @param aValue the a value
	 *
	 * @return the label
	 */
	protected Label getTreecellLabel(Treecell aTreecell, int aIndex, String aValue) {
		Label lLabel = getTreecellLabel(aTreecell, aIndex);
		lLabel.setValue(aValue);
		return lLabel;
	}

	/**
	 * Gets component label.
	 *
	 * @param aComponent the component parent
	 * @param aId the a id
	 *
	 * @return the label
	 */
	protected Label getComponentLabel(Component aComponent, String aId) {
		Label lLabel = null;
		for (Component lComponent : aComponent.getChildren()) {
			if (lComponent instanceof Label && ((String)lComponent.getAttribute("id")).equals(aId)) {
				return (Label)lComponent;
			}
		}
		lLabel = new CDefLabel();
		lLabel.setAttribute("id", aId);
		aComponent.appendChild(lLabel);
		return lLabel;
	}

	/**
	 * Gets component label.
	 *
	 * @param aComponent the component parent
	 * @param aId the a id
	 * @param aValue the a value
	 *
	 * @return the label
	 */
	protected Label getComponentLabel(Component aComponent, String aId, String aValue) {
		Label lLabel = getComponentLabel(aComponent, aId);
		lLabel.setValue(aValue);
		return lLabel;
	}

	/**
	 * Gets component html.
	 *
	 * @param aComponent the component parent
	 * @param aId the a id
	 *
	 * @return the html
	 */
	protected Html getComponentHtml(Component aComponent, String aId) {
		Html lHtml = null;
		for (Component lComponent : aComponent.getChildren()) {
			if (lComponent instanceof Html && ((String)lComponent.getAttribute("id")).equals(aId)) {
				return (Html)lComponent;
			}
		}
		lHtml = new CDefHtml();
		lHtml.setAttribute("id", aId);
		aComponent.appendChild(lHtml);
		return lHtml;
	}

	/**
	 * Gets component html.
	 *
	 * @param aComponent the component parent
	 * @param aId the a id
	 * @param aContent the a content
	 *
	 * @return the html
	 */
	protected Html getComponentHtml(Component aComponent, String aId, String aContent) {
		Html lHtml = getComponentHtml(aComponent, aId);
		lHtml.setContent(aContent);
		return lHtml;
	}

	/**
	 * Renders info treecell containing xml tag status, using xml tag aItem.
	 * If treecell doesn't exist all elements within the treecell are created.
	 * If treecell exists it is decorated, so existing elements should be updated!
	 *
	 * @param aTreeitem the treeitem
	 * @param aTreerow the treerow parent
	 * @param aItem the xml tag
	 * @param aKeyValues the key values
	 * @param aAccIsAuthor the account is author state
	 *
	 * @return the new info treecell
	 */
	protected Treecell renderInfoTreecell(Treeitem aTreeitem, Treerow aTreerow,
			IXMLTag aItem, String aKeyValues, boolean aAccIsAuthor) {
		Treecell lTreecell = getTreerowTreecell(aTreerow, 1);
		showStatus(lTreecell, aItem);
		return lTreecell;
	}

	/**
	 * Renders treerow containing two treecells using xml tag aItem.
	 * Inits dragging and dropping of treeitem.
	 * If treerow doesn't exist all elements within the treerow are created.
	 * If treerow exists it is decorated, so existing elements should be updated!
	 *
	 * @param aTreeitem the parent treeitem
	 * @param aItem the xml tag
	 * @param aKeyValues the key values
	 * @param aAccIsAuthor the account is author state
	 *
	 * @return the new treerow
	 */
	protected Treerow renderTreerow(Treeitem aTreeitem, IXMLTag aItem,
			String aKeyValues, boolean aAccIsAuthor) {
		Treerow lTreerow = getTreeitemTreerow(aTreeitem);
		// drag and drop must be set to treerow, not to treeitem!
		if (aAccIsAuthor) {
			// author can drag and drop tree items
			if (!aItem.getName().equals(AppConstants.contentElement)) {
				// root element may not be dragged
				lTreerow.setDraggable("true");
			}
			lTreerow.setDroppable("true");
		}
		renderTreecell(aTreeitem, lTreerow, aItem, aKeyValues, aAccIsAuthor);
		renderInfoTreecell(aTreeitem, lTreerow, aItem, aKeyValues,	aAccIsAuthor);
		return lTreerow;
	}

	/**
	 * Gets treeitem treerow.
	 *
	 * @param aTreeitem the treeitem parent
	 *
	 * @return the treerow
	 */
	protected Treerow getTreeitemTreerow(Treeitem aTreeitem) {
		Treerow lTreerow = null;
		if (aTreeitem.getChildren().size() > 0) {
			lTreerow = (Treerow)aTreeitem.getChildren().get(0);
		}
		else {
			lTreerow = new CTreerow();
			aTreeitem.appendChild(lTreerow);
		}
		return lTreerow;
	}

	/**
	 * Get new treeitem.
	 *
	 * @param aParent the parent
	 * @param aInsertBefore the content item to insert before, if null it will be appended
	 *
	 * @return the new treeitem
	 */
	protected Treeitem getNewTreeitem(Component aParent, Component aInsertBefore) {
		if ((aParent instanceof Tree) || (aParent instanceof Treeitem)) {
			// within ZK treechildren is parent of treeitems
			// so if aParent is tree or treeitem get or create treechildren object
			Treechildren lTreechildren = null;
			if (aParent instanceof Tree)
				lTreechildren = ((Tree) aParent).getTreechildren();
			if (aParent instanceof Treeitem)
				lTreechildren = ((Treeitem) aParent).getTreechildren();
			if (lTreechildren == null) {
				// if not existing, add them
				lTreechildren = new CDefTreechildren();
				aParent.appendChild(lTreechildren);
			}
			aParent = lTreechildren;
		}
		Treeitem lTreeitem = new CTreeitem();
		super.insertTreeitem((Treechildren) aParent, lTreeitem, (Treeitem)aInsertBefore);
		return lTreeitem;
	}

	/**
	 * Renders treeitem using xml tag aItem.
	 * Inits context menu of treeitem.
	 *
	 * @param aParent the parent
	 * @param aInsertBefore the content item to insert before, if null it will be appended
	 * @param aItem the xml tag
	 *
	 * @return the treeitem
	 */
	public Treeitem renderTreeitem(Component aParent, Component aInsertBefore,
			IXMLTag aItem) {
		return decorateTreeitem(aParent, getNewTreeitem(aParent, aInsertBefore), aItem);
	}

	/**
	 * Rerenders treeitem using xml tag aItem.
	 * Inits context menu of treeitem.
	 *
	 * @param aComponent the a component
	 * @param aItem the xml tag
	 *
	 * @return the treeitem
	 */
	public Treeitem reRenderTreeitem(Component aComponent, IXMLTag aItem) {
		return decorateTreeitem(aComponent, aComponent, aItem);
	}

	/**
	 * Decorates treeitem using xml tag aItem.
	 * Inits context menu of treeitem.
	 *
	 * @param aExistingComponent the a existing component
	 * @param aComponent the a component
	 * @param aItem the xml tag
	 *
	 * @return the treeitem
	 */
	protected Treeitem decorateTreeitem(Component aExistingComponent, Component aComponent, IXMLTag aItem) {
		Treeitem lTreeitem = (Treeitem)aComponent;
		String lKeyValues = setItemAttributes(lTreeitem, aItem);
		// context must be set to treeitem, not to treerow!
		setContextMenu(lTreeitem);
		boolean lAccIsAuthor = accIsAuthor(aExistingComponent);
		renderTreerow(lTreeitem, aItem, lKeyValues, lAccIsAuthor);
		return (Treeitem)aComponent;
	}

	/**
	 * Gets the label showing opened status of xml tag within rendered within treeitem.
	 *
	 * @param aTreeitem the treeitem
	 *
	 * @return the label
	 */
	public Component getLabelShowingOpened(Component aTreeitem) {
		List<Component> lTreerows = aTreeitem.getChildren();
		for (Component lTreerow : lTreerows) {
			List<Component> lTreecells = lTreerow.getChildren();
			for (Component lTreecell : lTreecells) {
				List<Component> lChilds = lTreecell.getChildren();
				for (Component lChild : lChilds) {
					String lStatusKey = (String) lChild.getAttribute("statuskey");
					if ((lStatusKey != null) && (lStatusKey.equals(AppConstants.statusKeyOpened)))
						return lChild;
				}
			}
		}
		return null;
	}

	/**
	 * Gets the selected treeitem, in case of single opened of xml tag with respect to siblings.
	 *
	 * @param aTreeitem the treeitem
	 *
	 * @return true, if treeitem is selected
	 */
	public boolean getTreeitemSelected(Component aTreeitem) {
		Component lLabelOpened = getLabelShowingOpened(aTreeitem);
		if (lLabelOpened != null)
			// if label is visible than treeitem is selected
			return lLabelOpened.isVisible();
		return false;
	}

	/**
	 * Sets the treeitem selected, in case of single opened of xml tag with respect to siblings.
	 *
	 * @param aTreeitem the treeitem
	 * @param aSelected the selected state
	 */
	public void setTreeitemSelected(Component aTreeitem, boolean aSelected) {
		Component lLabelOpened = getLabelShowingOpened(aTreeitem);
		if (lLabelOpened != null)
			// if label is visible than treeitem is selected
			lLabelOpened.setVisible(aSelected);
	}

	/**
	 * Deselects other sibling treeitems than aTreeitem if corresponding xml tag is single open.
	 *
	 * @param aTreeitem the treeitem
	 */
	public void deselectOtherTreeitems(Component aTreeitem) {
		if (!((IXMLTag) aTreeitem.getAttribute("item")).getDefTag()
				.getAttribute(AppConstants.defKeySingleopen).equals(AppConstants.statusValueTrue))
			// if corresponding xml tag is not single open, other treeitem(s) don't have to be deselected
			return;
		if (!getTreeitemSelected(aTreeitem))
			// if the treeitem is not selected, other treeitem(s) don't have to be deselected
			return;
		// get siblings
		List<Component> lTreeitems = aTreeitem.getParent().getChildren();
		for (Component lTreeitem : lTreeitems) {
			if (!((IXMLTag) lTreeitem.getAttribute("item")).getAttribute(AppConstants.keyId)
					.equals(((IXMLTag) aTreeitem.getAttribute("item")).getAttribute(AppConstants.keyId)))
				// if treeitem unequal to aTreeitem, corresponding tag ids are unequal, deselect
				setTreeitemSelected(lTreeitem, false);
		}
	}

	/**
	 * Renders gmap item using xml tag aItem.
	 *
	 * @param aParent the parent
	 * @param aInsertBefore the content item to insert before, if null it will be appended, not relevant for gmap items
	 * @param aItem the xml tag
	 *
	 * @return the new gmarker
	 */
	public Component renderGmapsItem(Component aParent, Component aInsertBefore,
			IXMLTag aItem) {
		if (aItem.getName().equals(AppConstants.contentElement)) {
			// only set item attributes and don't create a marker
			setItemAttributes(aParent, aItem);
			return aParent;
		}
		Gmarker lGmarker = new CGmarker();
		aParent.appendChild(lGmarker);
		// NOTE set id of marker because it isn't set by ZK!! Make it unique!
		lGmarker.setId("marker" + aItem.getAttribute(AppConstants.keyId) + System.currentTimeMillis() + Math.random());
		decorateGmapsItem(aParent, lGmarker, aItem);
		return lGmarker;
	}

	/**
	 * Rerenders gmap item using xml tag aItem.
	 *
	 * @param aParent the parent
	 * @param aComponent the component
	 * @param aItem the xml tag
	 *
	 * @return the new gmarker
	 */
	public Component reRenderGmapsItem(Component aParent, Component aComponent, IXMLTag aItem) {
		//NOTE setTooltipText for gmarker doesn't overwrite old value so remove old gmarker and create new one!
		aComponent.detach();
		return renderGmapsItem(aParent, null, aItem);
/*		if (aItem.getName().equals(AppConstants.contentElement)) {
			// only set item attributes and don't create a marker
			setItemAttributes(aParent, aItem);
			return aParent;
		}
		Gmarker lGmarker = (CGmarker)aComponent;
		decorateGmapsItem(lGmarker, lGmarker, aItem);
		return lGmarker; */
	}

	/**
	 * Decorates gmap item using xml tag aItem.
	 *
	 * @param aExistingComponent the existing component
	 * @param aComponent the component
	 * @param aItem the xml tag
	 *
	 * @return the gmarker
	 */
	protected Component decorateGmapsItem(Component aExistingComponent, Component aComponent, IXMLTag aItem) {
		Gmarker lGmarker = (CGmarker)aComponent;
		String lKeyValues = setItemAttributes(lGmarker, aItem);
		// no context menu for gmarker
		boolean lAccIsAuthor = accIsAuthor(aExistingComponent);
		// add content to lGmarker
		//NOTE use local getChildValue method to enable polymorphism in player
		lGmarker.setLat(Double.parseDouble(getNodeChildValue(aItem, "exlatitude")));
		lGmarker.setLng(Double.parseDouble(getNodeChildValue(aItem, "exlongitude")));
		if (lAccIsAuthor)
			lGmarker.setDraggingEnabled(true);
		else
			lGmarker.setDraggingEnabled(false);
//		lGmarker.setIconImage("");
		lGmarker.setTooltiptext(lKeyValues);
//		String lContent = lKeyValues + "\n";
//		lContent = lContent + CDesktopComponents.sSpring().unescapeXML(getChildValue(aItem, "description"));
//		lGmarker.setContent(lContent);
		return lGmarker;
	}

	/**
	 * Gets the node child. Method is needed for polymorphism.
	 * 
	 * @param aDataTag the data node tag
	 * @param aChildName the child name
	 * 
	 * @return the child
	 */
	public IXMLTag getNodeChild(IXMLTag aDataTag, String aChildName) {
		return aDataTag.getChild(aChildName);
	}
	
	/**
	 * Gets the node child value. Method is needed for polymorphism.
	 * 
	 * @param aDataTag the data node tag
	 * @param aChildName the child name
	 * 
	 * @return the child value
	 */
	public String getNodeChildValue(IXMLTag aDataTag, String aChildName) {
		return aDataTag.getChildValue(aChildName);
	}
	
	/**
	 * Gets the node child value. Method is needed for polymorphism.
	 * 
	 * @param aChildTag the child tag
	 * 
	 * @return the child value
	 */
	public String getNodeChildValue(IXMLTag aChildTag) {
		return aChildTag.getValue();
	}
	
	/**
	 * Gets the node child attribute value. Method is needed for polymorphism.
	 * 
	 * @param aDataTag the data node tag
	 * @param aChildName the child name
	 * @param aKey the attribute key
	 * 
	 * @return the child attribute value
	 */
	public String getNodeChildAttribute(IXMLTag aDataTag, String aChildName, String aKey) {
		return aDataTag.getChildAttribute(aChildName, aKey);
	}
	
	/**
	 * Gets the node child attribute value. Method is needed for polymorphism.
	 * 
	 * @param aChildTag the child tag
	 * @param aKey the attribute key
	 * 
	 * @return the child attribute value
	 */
	public String getNodeChildAttribute(IXMLTag aChildTag, String aKey) {
		return aChildTag.getAttribute(aKey);
	}
	
	/**
	 * Sets the child attribute value. Method is needed for polymorphism.
	 * 
	 * @param aChildTag the child tag
	 * @param aKey the attribute key
	 * @param aValue the attribute value
	 */
	public void setChildAttribute(IXMLTag aChildTag, String aKey, String aValue) {
		aChildTag.setAttribute(aKey, aValue);
	}
	
	/**
	 * Converts xml data content part to contentitems.
	 *
	 * @param aXmlTree the xml data tree
	 * @param aParent the a parent
	 */
	public void xmlContentToContentItems(IXMLTag aXmlTree, Component aParent) {
		if (aXmlTree == null)
			return;
		IXMLTag lContentTag = aXmlTree.getChild(AppConstants.contentElement);
		if (lContentTag == null)
			return;
		currentCaseComponentCode = getCaseComponent().getEComponent().getCode();
		//NOTE only show content tree if it has node children or menu option preview or has own content children (number of children > number of node children)
		boolean lShowTree = !lContentTag.getDefAttribute(AppConstants.defKeyChildnodes).equals("") ||
				lContentTag.getDefAttribute(AppConstants.defKeyPreview).equals(AppConstants.statusValueTrue) ||
				lContentTag.getDefTag().getChildTags().size() > lContentTag.getDefTag().getChildTags(AppConstants.defValueNode).size();
		if (lShowTree) {
			List<IXMLTag> lChildTags = new ArrayList<IXMLTag>(0);
			lChildTags.add(lContentTag);
			xmlChildsToContentItems(lChildTags, aParent, null);
		}
		else {
			aParent.setVisible(false);
		}
	}

	/**
	 * Converts xml node  tags to contentitems. Nested.
	 *
	 * @param aNodeTags the xml node tags
	 * @param aParent the ZK parent component
	 * @param aInsertBefore the content item to insert before, if null it will be appended
	 *
	 * @return the new content items
	 */
	public List<Component> xmlChildsToContentItems(List<IXMLTag> aNodeTags, Component aParent, Component aInsertBefore) {
		if (aNodeTags == null || aNodeTags.size() == 0)
			return null;
		List<Component> lContentItems = new ArrayList<Component>(0);
		for (IXMLTag lNodeTag : aNodeTags) {
			Component lContentItem = null;
			if (currentCaseComponentCode.equals("googlemaps")) {
				lContentItem = renderGmapsItem(aParent, aInsertBefore, lNodeTag);
			}
			else {
				lContentItem = renderTreeitem(aParent, aInsertBefore, lNodeTag);
			}
			if (lContentItem != null) {
				lContentItems.add(lContentItem);
				// nesting
				xmlChildsToContentItems(lNodeTag.getChildTags(AppConstants.defValueNode), lContentItem, null);
			}
		}
		return lContentItems;
	}

	/**
	 * Saves a row checkbox value as component status key value.
	 * Some status key values exclude other ones. This is handled too.
	 * Always saves in memory.
	 *
	 * @param aCheckbox the checkbox used for changing a component status
	 * @param aSaveInDb the save in db state, also save in db
	 */
	public void xmlRowToComponentStatus(Checkbox aCheckbox, boolean aSaveInDb) {
		IXMLTag lRootTag = getXmlDataTree();
		String lStatusKey = aCheckbox.getId();
		String lStatusValue = "" + aCheckbox.isChecked();
		CDesktopComponents.sSpring().getXmlManager().setCompInitialstatus(lRootTag, lStatusKey, lStatusValue);
		handleExcludingStatusValues(lRootTag, lStatusKey, lStatusValue);
		if (aSaveInDb) {
			setXmlData(CDesktopComponents.sSpring().getXmlManager().xmlTreeToDoc(lRootTag));
		}
	}

	/**
	 * Saves a row textbox value as component status key value.
	 *
	 * @param aTextbox the textbox used for changing a component status
	 * @param aSaveInDb the save in db state, also save in db
	 */
	public void xmlRowToComponentStatus(Textbox aTextbox, boolean aSaveInDb) {
		IXMLTag lRootTag = getXmlDataTree();
		String lStatusKey = aTextbox.getId();
		String lStatusValue = aTextbox.getValue();
		CDesktopComponents.sSpring().getXmlManager().setCompInitialstatus(lRootTag, lStatusKey, lStatusValue);
		if (aSaveInDb)
			setXmlData(CDesktopComponents.sSpring().getXmlManager().xmlTreeToDoc(lRootTag));
	}

	/**
	 * Handles excluding status key values for user generated content.
	 * Extendable false excludes ownership true.
	 * Shared true excludes sharedbyteam true.
	 * Sharedbyteam true excludes shared true.
	 *
	 * @param aRootTag the root xml tag
	 * @param aStatusKey the status key
	 * @param aStatusValue the status value
	 */
	protected void handleExcludingStatusValues(IXMLTag aRootTag, String aStatusKey, String aStatusValue) {
		// same component status values exclude other values,
		// that is some values cannot be true of other values are true or false
		boolean lStatusValueIsTrue = aStatusValue.equals(AppConstants.statusValueTrue);
		IXMLTag lComponentTag = aRootTag.getChild(AppConstants.componentElement);
		String lExtendableStr = lComponentTag.getChildAttribute(AppConstants.statusElement, AppConstants.statusKeyExtendable);
		if (lExtendableStr.equals(""))
			lExtendableStr = lComponentTag.getDefTag().getChildAttribute(AppConstants.statusElement, AppConstants.statusKeyExtendable);
		String lOwnershipStr = lComponentTag.getChildAttribute(AppConstants.statusElement, AppConstants.statusKeyOwnership);
		if (lOwnershipStr.equals(""))
			lOwnershipStr = lComponentTag.getDefTag().getChildAttribute(AppConstants.statusElement, AppConstants.statusKeyOwnership);
		String lSharedStr = lComponentTag.getChildAttribute(AppConstants.statusElement, AppConstants.statusKeyShared);
		if (lSharedStr.equals(""))
			lSharedStr = lComponentTag.getDefTag().getChildAttribute(AppConstants.statusElement, AppConstants.statusKeyShared);
		String lSharedbyteamStr = lComponentTag.getChildAttribute(AppConstants.statusElement, AppConstants.statusKeySharedbyteam);
		if (lSharedbyteamStr.equals(""))
			lSharedbyteamStr = lComponentTag.getDefTag().getChildAttribute(AppConstants.statusElement, AppConstants.statusKeySharedbyteam);
		boolean lExtendable = (lExtendableStr.equals(AppConstants.statusValueTrue));
		boolean lOwnership = (lOwnershipStr.equals(AppConstants.statusValueTrue));
		boolean lShared = (lSharedStr.equals(AppConstants.statusValueTrue));
		boolean lSharedbyteam = (lSharedbyteamStr.equals(AppConstants.statusValueTrue));
		if (aStatusKey.equals(AppConstants.statusKeyExtendable) && (!lStatusValueIsTrue) && (lOwnership)) {
			CDesktopComponents.sSpring().getXmlManager().setCompInitialstatus(aRootTag, AppConstants.statusKeyOwnership, AppConstants.statusValueFalse);
			((Checkbox)CDesktopComponents.vView().getComponent(AppConstants.statusKeyOwnership)).setChecked(false);
		}
		if (aStatusKey.equals(AppConstants.statusKeyOwnership) && (lStatusValueIsTrue) && (!lExtendable)) {
			CDesktopComponents.sSpring().getXmlManager().setCompInitialstatus(aRootTag, AppConstants.statusKeyOwnership, AppConstants.statusValueFalse);
			((Checkbox)CDesktopComponents.vView().getComponent(AppConstants.statusKeyOwnership)).setChecked(false);
		}
		if (aStatusKey.equals(AppConstants.statusKeyShared) && (lStatusValueIsTrue) && (lSharedbyteam)) {
			CDesktopComponents.sSpring().getXmlManager().setCompInitialstatus(aRootTag, AppConstants.statusKeySharedbyteam, AppConstants.statusValueFalse);
			((Checkbox)CDesktopComponents.vView().getComponent(AppConstants.statusKeySharedbyteam)).setChecked(false);
		}
		if (aStatusKey.equals(AppConstants.statusKeySharedbyteam) && (lStatusValueIsTrue) && (lShared)) {
			CDesktopComponents.sSpring().getXmlManager().setCompInitialstatus(aRootTag, AppConstants.statusKeyShared, AppConstants.statusValueFalse);
			((Checkbox)CDesktopComponents.vView().getComponent(AppConstants.statusKeyShared)).setChecked(false);
		}
	}

	/**
	 * Deletes contenttem and corresponding xml tag.
	 * Always saves in memory.
	 *
	 * @param aContentItem the contentitem
	 * @param aSaveInDb the save in db state
	 */
	public void deleteContentItem(Component aContentItem, boolean aSaveInDb) {
		currentCaseComponentCode = getCaseComponent().getEComponent().getCode();
		IXMLTag lItem = (IXMLTag) aContentItem.getAttribute("item");
		// delete xml tag
		deleteNode(lItem, aSaveInDb);
		if (!currentCaseComponentCode.equals("googlemaps")) {
			// tree, so ...
			Treechildren lTreechildren = ((Treeitem)aContentItem).getTreechildren();
			if (lTreechildren != null) {
				List<Component> lChilds = lTreechildren.getChildren();
				if ((lChilds == null) || (lChilds.size() == 0))
					// if no siblings left, delete treechildren
					lTreechildren.detach();
			}
		}
		// delete contentitem
		aContentItem.detach();
	}

	/**
	 * Creates new contentitem using xml tag aItem.
	 *
	 * @param aParent the parent component of aContentItem
	 * @param aContentItem the contentitem right clicked
	 * @param aSelectedItem the xml tag corresponding to aContentItem
	 * @param aItem the xml tag to use for the new contentitem
	 * @param aNewItemType the new item type, either sibling or child
	 *
	 * @return the new contentitem
	 */
	public Component newContentItem(Component aParent, Component aContentItem, IXMLTag aSelectedItem, IXMLTag aItem, String aNewItemType) {
		currentCaseComponentCode = getCaseComponent().getEComponent().getCode();
		if (currentCaseComponentCode.equals("googlemaps")) {
			aContentItem = renderGmapsItem(aParent, null, aItem);
		}
		else {
			// tree, so ...
			Component lInsertBefore = aContentItem;
			if (aContentItem != null) {
				if (aNewItemType.equals("child")) {
					// if type is child, add new treeitem as child of aTreeitem
					// otherwise add treeitem as sibling of aTreeitem
					aParent = aContentItem;
					// so insertbefore is empty, append
					lInsertBefore = null;
				}
			}
			aContentItem = renderTreeitem(aParent, lInsertBefore, aItem);
			// deselect other treeitems
			deselectOtherTreeitems(aContentItem);
		}
		return aContentItem;
	}

	/**
	 * Edits contentitem using xml tag aItem.
	 *
	 * @param aParent the a parent
	 * @param aContentItem the a contentitem
	 * @param aItem the a item
	 *
	 * @return the edited contentitem
	 */
	public Component editContentItem(Component aParent, Component aContentItem, IXMLTag aItem) {
		// inserting new contentitem and deleting old contentitem is more easy than trying to update one item
		// create new contentitem
		currentCaseComponentCode = getCaseComponent().getEComponent().getCode();
		if (currentCaseComponentCode.equals("googlemaps")) {
			return reRenderGmapsItem(aParent, aContentItem, aItem);
		}
		else {
			return reRenderTreeitem(aContentItem, aItem);
		}
	}

	/**
	 * Deletes contentitem.
	 *
	 * @param aContentItem the contentitem
	 */
	public void deleteContentItem(Component aContentItem) {
		currentCaseComponentCode = getCaseComponent().getEComponent().getCode();
		if (currentCaseComponentCode.equals("googlemaps")) {
			aContentItem.detach();
		}
		else {
			// tree, so ...
			Treechildren lTreechildren = ((Treeitem)aContentItem).getTreechildren();
			// delete treeitem
			aContentItem.detach();
			if (lTreechildren != null) {
				List<Component> lChilds = lTreechildren.getChildren();
				if ((lChilds == null) || (lChilds.size() == 0))
					// if no siblings left, delete treechildren
					lTreechildren.detach();
			}
		}
	}

	/**
	 * Deletes xml node tag.
	 * Always saves in memory.
	 *
	 * @param aItem the xml tag
	 * @param aSaveInDb the save in db state
	 */
	public void deleteNode(IXMLTag aItem, boolean aSaveInDb) {
		// remove references to tag in system casecomponent 'case'
		removeReferences(aItem);
		// remove as child in parenttag
		removeInParent(aItem);
		IXMLTag lRootTag = getXmlDataTree();
		CDesktopComponents.sSpring().getXmlManager().deleteNode(lRootTag, aItem);
		if (aSaveInDb) {
			setXmlData(CDesktopComponents.sSpring().getXmlManager().xmlTreeToDoc(lRootTag));
		}
	}

	/**
	 * Removes references to xml tag aItem within xml data of system casecomponent 'case'.
	 *
	 * @param aItem the xml tag
	 */
	public void removeReferences(IXMLTag aItem) {
		// get all caseroles for used casecomponent
		List<IECaseRole> lCaseRoles = getCaseRolesByCacId();
		if (lCaseRoles == null)
			return;
		// get used casecomponent
		IECaseComponent lCaseComponent = getCaseComponent();
		CCaseHelper lCaseHelper = new CCaseHelper(CDesktopComponents.sSpring());
		IXMLTag lContentTag = lCaseHelper.getCaseCaseComponentContentTag();
		// Loop through caseroles
		for (IECaseRole lCaseRole : lCaseRoles) {
			// remove references to caserole/casecomponent combo
			removeTagId(lContentTag, lCaseRole, lCaseComponent, aItem, lCaseHelper);
		}
		lCaseHelper.setCaseCaseComponentContentTag(lContentTag);
	}

	/**
	 * Removes references to xml tag aItem by caserole aCaseRole and casecomponent
	 * aCaseComponent within xml data of system casecomponent 'case'. Nested.
	 *
	 * @param aContentTag the content tag
	 * @param aCaseRole the case role
	 * @param aCaseComponent the case component
	 * @param aTag the xml tag
	 * @param aCaseHelper the case helper
	 */
	public void removeTagId(IXMLTag aContentTag, IECaseRole aCaseRole, IECaseComponent aCaseComponent, IXMLTag aTag,
			CCaseHelper aCaseHelper) {
		// also remove references to childs
		List<IXMLTag> lChildTags = aTag.getChildTags(AppConstants.defValueNode);
		if (lChildTags == null)
			return;
		for (IXMLTag lXmlTag : lChildTags) {
			// nesting
			removeTagId(aContentTag, aCaseRole, aCaseComponent, lXmlTag, aCaseHelper);
		}
		aCaseHelper.removeTagId(aContentTag, AppConstants.contentElement, aCaseRole, aCaseComponent, aTag);
	}

	/**
	 * Removes an xml tag within the parent xml tag.
	 *
	 * @param aItem the xml tag
	 */
	public void removeInParent(IXMLTag aItem) {
		if (aItem == null)
			return;
		IXMLTag lParent = aItem.getParentTag();
		if (lParent == null)
			return;
		List<IXMLTag> lChilds = lParent.getChildTags();
		for (int i = (lChilds.size() - 1); i >= 0; i--) {
			IXMLTag lChild = (IXMLTag) lChilds.get(i);
			if (lChild.getAttribute(AppConstants.keyId).equals(aItem.getAttribute(AppConstants.keyId)))
				lChilds.remove(i);
		}
	}


	/**
	 * Renders line input, so text field for one line.
	 *
	 * @param aIsAuthor the is author state
	 * @param aPrivate the private state, if true only visible to author
	 * @param aRow the row containing data
	 * @param aKey the key of the element
	 *
	 * @return the new component
	 */
	protected CDefTextbox renderLine(boolean aIsAuthor, boolean aPrivate, Hashtable<String,Object> aRow, String aKey) {
		CDefTextbox lTextbox = new CDefTextbox();
		lTextbox.setId(aKey);
		String lValue = (String) aRow.get("value");
		if (aRow.get("name").equals("status")) // already unescaped!
			lValue = lValue.replace(AppConstants.statusCommaReplace, ",");
		lValue = CDesktopComponents.sSpring().unescapeXMLWithoutReplace(lValue); // necessary?
		//TODO Value already unescaped in tree by documentbuilder; error when value in tree is unescaped while other value is added or edited.
		// Old value will not be re-escaped when storing complete tree, and error will be generated when old value is addressed.
		// For the moment, statusCommaReplace must not contain characters that must be escaped. 
		lTextbox.setValue(lValue);
		lTextbox.setAttribute("type", (String) aRow.get("type"));
		lTextbox.setAttribute("changed", "" + ((String) aRow.get("initialvalue")).equals(AppConstants.statusValueTrue));
		lTextbox.setWidth("" + CDesktopComponents.vView().getColWidth2() + "px");
		lTextbox.setSclass("CCdeComponentWnd_row_" + aKey + "_" + (String) aRow.get("type") + "_" + "line");
		if (!aIsAuthor)
			lTextbox.setDisabled(true);
		return lTextbox;
	}

	/**
	 * Renders textbox input.
	 *
	 * @param aIsAuthor the is author state
	 * @param aPrivate the private state, if true only visible to author
	 * @param aRow the row containing data
	 * @param aKey the key of the element
	 *
	 * @return the new component
	 */
	protected CDefTextbox renderText(boolean aIsAuthor, boolean aPrivate, Hashtable<String,Object> aRow, String aKey) {
		CDefTextbox lTextbox = new CDefTextbox();
		lTextbox.setId(aKey);
		String lValue = CDesktopComponents.sSpring().unescapeXMLWithoutReplace((String) aRow.get("value"));
		lTextbox.setValue(lValue);
		lTextbox.setAttribute("type", (String) aRow.get("type"));
		lTextbox.setAttribute("changed", "" + ((String) aRow.get("initialvalue")).equals(AppConstants.statusValueTrue));
		lTextbox.setRows(3);
		lTextbox.setWidth("" + CDesktopComponents.vView().getColWidth2() + "px");
		lTextbox.setSclass("CCdeComponentWnd_row_" + aKey + "_" + (String) aRow.get("type") + "_" + "text");
		if (!aIsAuthor)
			lTextbox.setDisabled(true);
		return lTextbox;
	}

	/**
	 * Renders richtextbox input.
	 *
	 * @param aSimple the simple state, if simple only restricted edit options
	 * @param aIsAuthor the is author state
	 * @param aPrivate the private state, if true only visible to author
	 * @param aRow the row containing data
	 * @param aKey the key of the element
	 *
	 * @return the new component
	 */
	protected CDefVbox renderRichtext(boolean aSimple, boolean aIsAuthor, boolean aPrivate, Hashtable<String,Object> aRow, String aKey) {
		CDefVbox lVbox = new CDefVbox();
		CDefCKeditor lTextbox = new CDefCKeditor();
		lTextbox.setId(aKey);
		lTextbox.setAttribute("simple", "" + aSimple);
		/*
		String lValue = CDesktopComponents.sSpring().unescapeXMLWithoutReplace((String) aRow.get("value"));
		String lValue = (String) aRow.get("value");
		//NOTE rich text is not unescaped because ckeditor uses unescaped input
		 */
		String lValue = (String) aRow.get("value");

		lTextbox.setValue(lValue);
		lTextbox.setAttribute("type", (String) aRow.get("type"));
		lTextbox.setAttribute("changed", "" + ((String) aRow.get("initialvalue")).equals(AppConstants.statusValueTrue));
		if (aSimple)
			lTextbox.setHeight("200px");
		else
			lTextbox.setHeight("300px");
		lTextbox.setWidth("" + CDesktopComponents.vView().getColWidth2() + "px");
		if (aSimple) {
//			lTextbox.setToolbar("Basic");
			lTextbox.setCustomConfigurationsPath(VView.getInitParameter("ckeditor.cde.config.file"));
		}
		// ckeditor cannot be disabled
		lVbox.appendChild(lTextbox);

		CDefHbox lHbox = new CDefHbox();
		lVbox.appendChild(lHbox);
		CCdePreviewRichtextBtn lButton1 = new CCdePreviewRichtextBtn();
		lButton1.setContentHelper(this);
		lButton1.setContentComp(lTextbox);
		lButton1.setLabel(CDesktopComponents.vView().getLabel("cde_s_component.richtext.preview_text"));
		lHbox.appendChild(lButton1);
		CCdeMakePlainTextBtn lButton2 = new CCdeMakePlainTextBtn();
		lButton2.setContentHelper(this);
		lButton2.setContentComp(lTextbox);
		lButton2.setLabel(CDesktopComponents.vView().getLabel("cde_s_component.richtext.toplaintext"));
		lHbox.appendChild(lButton2);

		return lVbox;
	}

	/**
	 * Renders picture input. The picture itself and an upload and delete button.
	 *
	 * @param aIsAuthor the is author state
	 * @param aPrivate the private state, if true only visible to author
	 * @param aRow the row containing data
	 * @param aKey the key of the element
	 *
	 * @return the new component
	 */
	protected CDefHbox renderPicture(boolean aIsAuthor, boolean aPrivate,	Hashtable<String,Object> aRow, String aKey) {
		CDefHbox lHbox = new CDefHbox();
		// show image
		Image lImage = new CDefImage();
		lImage.setId(aKey);
		lImage.setWidth("150px");
		if (((String) aRow.get("value")).equals(""))
			lImage.setContent((org.zkoss.image.Image) null);
		else {
			lImage.setContent((org.zkoss.image.Image) null);
			lImage.setSrc(getImageSrc((String) aRow.get("value")));
		}
		lImage.setAttribute("type", (String) aRow.get("type"));
		lImage.setAttribute("changed", "" + ((String) aRow.get("initialvalue")).equals(AppConstants.statusValueTrue));
		lImage.setSclass("CCdeComponentWnd_row_" + aKey + "_" + (String) aRow.get("type") + "_" + "image");
		if (aIsAuthor) {
			// show buttons
			Vbox lVbox = new CDefVbox();
			// show upload button
			CUploadPictureButton lButton = new CUploadPictureButton();
			if (((String) aRow.get("value")).equals(""))
				// if no picture uploaded
				lButton.setLabel(CDesktopComponents.vView().getLabel("add"));
			else
				// if picture is already uploaded
				lButton.setLabel(CDesktopComponents.vView().getLabel("overwrite"));
			// couple existing image to upload button
			lButton.setUploadImage(lImage);
			lVbox.appendChild(lButton);
			if (!((String) aRow.get("value")).equals("")) {
				// show delete button if picture is already uploaded
				CDeletePictureButton lButton2 = new CDeletePictureButton();
				lButton2.setLabel(CDesktopComponents.vView().getLabel("delete"));
				// couple existing image to delete button
				lButton2.setUploadImage(lImage);
				lVbox.appendChild(lButton2);
			}
			lHbox.appendChild(lVbox);
		}
		lHbox.appendChild(lImage);
		return lHbox;
	}

	/**
	 * Renders checkbox input.
	 *
	 * @param aIsAuthor the is author state
	 * @param aPrivate the private state, if true only visible to author
	 * @param aRow the row containing data
	 * @param aKey the key of the element
	 *
	 * @return the new component
	 */
	protected CDefCheckbox renderCheckbox(boolean aIsAuthor, boolean aPrivate, Hashtable<String,Object> aRow, String aKey) {
		CDefCheckbox lCheckbox = new CDefCheckbox();
		lCheckbox.setId(aKey);
		lCheckbox.setChecked(((String)aRow.get("value")).equals(AppConstants.statusValueTrue));
		lCheckbox.setAttribute("type", (String) aRow.get("type"));
		lCheckbox.setAttribute("changed", "" + ((String) aRow.get("initialvalue")).equals(AppConstants.statusValueTrue));
		lCheckbox.setSclass("CCdeComponentWnd_row_" + aKey + "_" + (String) aRow.get("type") + "_" + "checkbox");
		if (!aIsAuthor)
			lCheckbox.setDisabled(true);
		return lCheckbox;
	}

	/**
	 * Renders combobox input with items from xml definition.
	 *
	 * @param aIsAuthor the is author state
	 * @param aPrivate the private state, if true only visible to author
	 * @param aRow the row containing data
	 * @param aKey the key of the element
	 *
	 * @return the new component
	 */
	protected CDefListbox renderCombobox(boolean aIsAuthor, boolean aPrivate, Hashtable<String,Object> aRow, String aKey) {
		CSimpleCombo lInput = new CSimpleCombo(aKey);
		lInput.setAttribute("type", (String) aRow.get("type"));
		lInput.setAttribute("changed", "" + ((String) aRow.get("initialvalue")).equals(AppConstants.statusValueTrue));
		if (!aRow.containsKey("deftag"))
			return lInput;
		IXMLTag lDefTag = (IXMLTag) aRow.get("deftag");
		if (lDefTag == null)
			return lInput;
		String lItems = lDefTag.getAttribute(AppConstants.defKeyItems);
		String lSelectedItem = (String) aRow.get("value");
		lInput.showItems(lItems, lSelectedItem);
		lInput.setSclass("CCdeComponentWnd_row_" + aKey + "_" + (String) aRow.get("type") + "_" + "combobox");
		lInput.setWidth("" + CDesktopComponents.vView().getColWidth2() + "px");
		if (!aIsAuthor)
			lInput.setDisabled(true);
		return lInput;
	}

	/**
	 * Renders image listbox input with image with an imageprefix from the xml definition.
	 *
	 * @param aIsAuthor the is author state
	 * @param aPrivate the private state, if true only visible to author
	 * @param aRow the row containing data
	 * @param aKey the key of the element
	 *
	 * @return the new component
	 */
	protected CDefListbox renderImageListbox(boolean aIsAuthor, boolean aPrivate, Hashtable<String,Object> aRow, String aKey) {
		IXMLTag lDefTag = (IXMLTag) aRow.get("deftag");
		String lImageprefix = "";
		if (lDefTag != null) {
			lImageprefix = lDefTag.getAttribute(AppConstants.defKeyImageprefix);
		}
		CSimpleImageListbox lInput = new CSimpleImageListbox(aKey, lImageprefix, CDesktopComponents.sSpring());
		lInput.setAttribute("type", (String) aRow.get("type"));
		lInput.setAttribute("changed", "" + ((String) aRow.get("initialvalue")).equals(AppConstants.statusValueTrue));
		if (!aRow.containsKey("deftag"))
			return lInput;
		if (lDefTag == null)
			return lInput;
		String lSelectedItem = (String) aRow.get("value");
		lInput.showItems(lSelectedItem);
		lInput.setSclass("CCdeComponentWnd_row_" + aKey + "_" + (String) aRow.get("type") + "_" + "combobox");
		lInput.setWidth("" + CDesktopComponents.vView().getColWidth2() + "px");
		if (!aIsAuthor)
			lInput.setDisabled(true);
		return lInput;
	}

	/**
	 * Renders blob input. This includes input elements for different
	 * blobtypes, for previewing blob and for choosing blob mediatype.
	 *
	 * @param aIsAuthor the is author state
	 * @param aPrivate the private state, if true only visible to author
	 * @param aRow the row containing data
	 * @param aKey the key of the element
	 *
	 * @return the new component
	 */
	protected CDefVbox renderBlob(boolean aIsAuthor, boolean aPrivate, Hashtable<String,Object> aRow, String aKey) {
		CDefVbox lVboxp = new CDefVbox();
		lVboxp.setId(aKey);

		// get blob
		IEBlob lBlob = null;
		if (!((String) aRow.get("value")).equals(""))
			lBlob = getBlob((String) aRow.get("value"));

		Vbox lVbox = new CDefVbox();
		if (!aRow.containsKey("deftag"))
			return lVboxp;
		IXMLTag lDefTag = (IXMLTag) aRow.get("deftag");
		if (lDefTag == null)
			return lVboxp;
		lVboxp.setAttribute("deftag", lDefTag);
		String lBlobtypes = lDefTag.getAttribute(AppConstants.defKeyBlobtypes);
		String[] lBlobtypearr = lBlobtypes.split(",");
		String lSelBlobtype = "";
		if (aRow.containsKey("tag")) {
			IXMLTag lChildTag = (IXMLTag) aRow.get("tag");
			//NOTE use local getChildValue method to enable polymorphism in player
			lSelBlobtype = getNodeChildAttribute(lChildTag, AppConstants.keyBlobtype);
		}
		lVboxp.setAttribute(AppConstants.keyBlobtype, lSelBlobtype);
		Label lLabel = new CDefLabel();
		lLabel.setValue(CDesktopComponents.vView().getLabel("cde_s_component.blob.blobtype"));
		lVbox.appendChild(lLabel);

		// show tabs for different blob types
		CRadioGroupTab lRadiogroup = new CRadioGroupTab();
		lRadiogroup.setId("blobtype_radiogroup");
		for (int i = 0; i < lBlobtypearr.length; i++) {
			String lBlobtype = lBlobtypearr[i];
			Radio lRadio = new CRadioTab(CDesktopComponents.vView().getLabel("cde_s_component.blob.blobtype." + lBlobtype));
			lRadio.setId("blob_" + lBlobtype + "_radio");
			lRadio.setAttribute(AppConstants.keyBlobtype, lBlobtype);
			lRadio.setAttribute("blobtypearr", lBlobtypearr);
			if (lBlobtype.equals(lSelBlobtype))
				lRadio.setChecked(true);
			lRadiogroup.appendChild(lRadio);
		}
		lVbox.appendChild(lRadiogroup);
		lVboxp.appendChild(lVbox);

		// show different blob type input elements
		for (int i = 0; i < lBlobtypearr.length; i++) {
			Hbox lHbox = new CDefHbox();
			String lBlobtype = lBlobtypearr[i];
			boolean lSelected = (lSelBlobtype.equals(lBlobtype));
			lHbox.setId("blob_" + lBlobtype + "_radio_tab");
			lHbox.setVisible(lSelected);
			String lCompId = "blob_" + lBlobtype + "_radio_comp";
			lHbox.setAttribute("compid", lCompId);
			String lValue = "";
			Component lBlobComponent = null;
			Component lSMComponent = null;
			if (lBlobtype.equals(AppConstants.blobtypeDatabase)) {
				// render input elements for handling file
				if ((lBlob != null) && (lSelected))
					lValue = lBlob.getFilename();
				lSMComponent = renderBlobMedia(aIsAuthor, lCompId, lValue, "line", "false", lBlob, lBlobtype);
				lBlobComponent = lSMComponent.getParent();
			} else {
				if (lBlobtype.equals(AppConstants.blobtypeIntUrl)) {
					// render input elements for handling internal url
					if ((lBlob != null) && (lSelected))
						lValue = lBlob.getUrl();
					lSMComponent = renderIntUrlLine(aIsAuthor, lCompId, lValue, "line", "false", lBlobtype);
					lBlobComponent = lSMComponent.getParent();
				}
				else {
					// render input elements for handling external url
					if ((lBlob != null) && (lSelected))
						lValue = lBlob.getUrl();
					lSMComponent = renderBlobLine(aIsAuthor, lCompId, lValue, "line", "false", lBlobtype);
					lBlobComponent = lSMComponent;
				}
			}
			lHbox.appendChild(lBlobComponent);
			// render preview media button
			CShowMediaBtn lButton = new CShowMediaBtn();
			String lId = "show_media_" + lBlobtype;
			lButton.setId(lId);
			lButton.setContentComp(lSMComponent);
			lButton.setHref("");
			lButton.setLabel(CDesktopComponents.vView().getLabel("preview"));
			((CBlobTextbox)lSMComponent).setBlobPreviewBtn(lButton);
			lHbox.appendChild(lButton);
			lVboxp.appendChild(lHbox);
		}

		// show mediatypes input
		String lMediatypes = lDefTag.getAttribute(AppConstants.defKeyMediatypes);
		if (!lMediatypes.equals("")) {
			lVbox = new CDefVbox();
			String[] lMediatypearr = lMediatypes.split(",");
			lVboxp.setAttribute("mediatypearr", lMediatypearr);
			String lSelMediatype = "";
			if (aRow.containsKey("tag")) {
				IXMLTag lChildTag = (IXMLTag) aRow.get("tag");
				//NOTE use local getChildValue method to enable polymorphism in player
				lSelMediatype = getNodeChildAttribute(lChildTag, AppConstants.keyMediatype);
			}
			lVboxp.setAttribute(AppConstants.keyMediatype, lSelMediatype);
			lLabel = new CDefLabel();
			lLabel.setValue(CDesktopComponents.vView().getLabel("cde_s_component.blob.mediatype"));
			lVbox.appendChild(lLabel);
			lRadiogroup = new CRadioGroupTab();
			lRadiogroup.setId("mediatype_radiogroup");
			for (int i = 0; i < lMediatypearr.length; i++) {
				String lMediatype = lMediatypearr[i];
				Radio lRadio = new CDefRadio();
				lRadio.setLabel(CDesktopComponents.vView().getLabel("cde_s_component.blob.mediatype." + lMediatype));
				lRadio.setId("blob_" + lMediatype + "_radio");
				lRadio.setAttribute(AppConstants.keyMediatype, lMediatype);
				if (lMediatype.equals(lSelMediatype))
					lRadio.setChecked(true);
				lRadiogroup.appendChild(lRadio);
			}
			lVbox.appendChild(lRadiogroup);
			lVboxp.appendChild(lVbox);
		}
		return lVboxp;
	}

	/**
	 * Renders blob line input for external url.
	 *
	 * @param aIsAuthor the is author state
	 * @param aKey the key of the element
	 * @param aValue the value of the element
	 * @param aType the type of the element
	 * @param aInitialvalue the initialvalue of the element
	 * @param aBlobtype the blobtype
	 *
	 * @return the new component
	 */
	protected CDefTextbox renderBlobLine(boolean aIsAuthor, String aKey, String aValue, String aType, String aInitialvalue, String aBlobtype) {
		// render text line input
		CDefTextbox lTextbox = new CBlobTextbox();
		lTextbox.setId(aKey);
		lTextbox.setValue(aValue);
		lTextbox.setAttribute("type", aType);
		lTextbox.setAttribute("changed", "" + (aInitialvalue.equals(AppConstants.statusValueTrue)));
		lTextbox.setAttribute(AppConstants.keyBlobtype, aBlobtype);
		lTextbox.setSclass("CCdeComponentWnd_row_" + aKey + "_" + aType + "_" + "blobline");
		if (!aIsAuthor)
			lTextbox.setDisabled(true);
		return lTextbox;
	}

	/**
	 * Renders blob line input for internal url and file choose button to choose one file on the server.
	 *
	 * @param aIsAuthor the is author state
	 * @param aKey the key of the element
	 * @param aValue the value of the element
	 * @param aType the type of the element
	 * @param aInitialvalue the initialvalue of the element
	 * @param aBlobtype the blobtype
	 *
	 * @return the new component
	 */
	protected CDefTextbox renderIntUrlLine(boolean aIsAuthor, String aKey, String aValue, String aType, String aInitialvalue, String aBlobtype) {
		Hbox lHbox = new CDefHbox();
		// render text line input
		CDefTextbox lTextbox = new CBlobTextbox();
		lTextbox.setId(aKey);
		lTextbox.setValue(aValue);
		lTextbox.setAttribute("type", aType);
		lTextbox.setAttribute("changed", "" + (aInitialvalue.equals(AppConstants.statusValueTrue)));
		lTextbox.setAttribute(AppConstants.keyBlobtype, aBlobtype);
		if (aIsAuthor) {
			// render file choose button
			Vbox lVbox = new CDefVbox();
			CChooseFileButton lButton = newChooseFileButton();
			lButton.setLabel(CDesktopComponents.vView().getLabel("choose"));
			lButton.setFileTarget(lTextbox);
			lVbox.appendChild(lButton);
			lHbox.appendChild(lVbox);
		}
		else
			lTextbox.setDisabled(true);
		lTextbox.setSclass("CCdeComponentWnd_row_" + aKey + "_" + aType + "_" + "inturlline");
		lHbox.appendChild(lTextbox);
		return lTextbox;
	}

	/**
	 * Renders input elements for uploading/deleting a file and show filename.
	 *
	 * @param aIsAuthor the is author state
	 * @param aKey the key of the element
	 * @param aValue the value of the element
	 * @param aType the type of the element
	 * @param aInitialvalue the initialvalue of the element
	 * @param aBlob the blob
	 * @param aBlobtype the blobtype
	 *
	 * @return the new component
	 */
	protected CDefTextbox renderBlobMedia(boolean aIsAuthor, String aKey,
			String aValue, String aType, String aInitialvalue, IEBlob aBlob,
			String aBlobtype) {
		Hbox lHbox = new CDefHbox();
		// render text line showing name of uploaded file
		CDefTextbox lTextbox = new CBlobTextbox();
		lTextbox.setId(aKey);
		lTextbox.setValue(aValue);
		lTextbox.setAttribute("type", aType);
		lTextbox.setAttribute("changed", "" + (aInitialvalue.equals(AppConstants.statusValueTrue)));
		lTextbox.setAttribute(AppConstants.keyBlobtype, aBlobtype);
		lTextbox.setAttribute("content", null);
		lTextbox.setAttribute("blob", aBlob);
		// set disabled, content of textbox is set when file is uploaded
		lTextbox.setDisabled(true);
		if (aIsAuthor) {
			Vbox lVbox = new CDefVbox();
			// show upload button
			CUploadMediaButton lButton = new CUploadMediaButton();
			if (aValue.equals(""))
				// no file uploaded yet
				lButton.setLabel(CDesktopComponents.vView().getLabel("add"));
			else
				// a file is uploaded
				lButton.setLabel(CDesktopComponents.vView().getLabel("overwrite"));
			lButton.setUploadTarget(lTextbox);
			lVbox.appendChild(lButton);
			if (!aValue.equals("")) {
				// if a file is uploaded it can be deleted
				CDeleteMediaButton lButton2 = new CDeleteMediaButton();
				lButton2.setLabel(CDesktopComponents.vView().getLabel("delete"));
				lButton2.setUploadTarget(lTextbox);
				lVbox.appendChild(lButton2);
			}
			lHbox.appendChild(lVbox);
		}
		lTextbox.setSclass("CCdeComponentWnd_row_" + aKey + "_" + aType + "_" + "blobmedia");
		lHbox.appendChild(lTextbox);
		return lTextbox;
	}

	/**
	 * Renders formula input, a one line text field.
	 *
	 * @param aIsAuthor the is author state
	 * @param aPrivate the private state, if true only visible to author
	 * @param aRow the row containing data
	 * @param aKey the key of the element
	 *
	 * @return the new component
	 */
	protected CDefTextbox renderFormula(boolean aIsAuthor, boolean aPrivate, Hashtable<String,Object> aRow, String aKey) {
		CDefTextbox lTextbox = new CDefTextbox();
		lTextbox.setId(aKey);
		String lValue = CDesktopComponents.sSpring().unescapeXMLWithoutReplace((String) aRow.get("value"));
		lTextbox.setValue(lValue);
		lTextbox.setAttribute("type", (String) aRow.get("type"));
		lTextbox.setAttribute("changed", "" + ((String) aRow.get("initialvalue")).equals(AppConstants.statusValueTrue));
		lTextbox.setWidth("" + CDesktopComponents.vView().getColWidth2() + "px");
		lTextbox.setSclass("CCdeComponentWnd_row_" + aKey + "_" + (String) aRow.get("type") + "_" + "line");
		if (!aIsAuthor)
			lTextbox.setDisabled(true);
		return lTextbox;
	}


	/**
	 * Renders script label.
	 *
	 * @param aParent the ZK parent
	 * @param aAttributes the attributes
	 * @param aLabelStr the label string
	 *
	 * @return the new scriptlabel
	 */
	private CDefLabel renderScriptLabel(Component aParent, Hashtable<String,Object> aAttributes, String aLabelStr) {
		aAttributes.put("type", ((IXMLTag)aAttributes.get("tag")).getName());
		aAttributes.put("item", aAttributes.get("tag"));
		CDefLabel lLabel = new CScriptLabel(aAttributes, aLabelStr);
		aParent.appendChild(lLabel);
		return lLabel;
	}

	/**
	 * Renders script labels vbox.
	 * It uses labels for showing a readable form of a script condition, action or logical operator.
	 *
	 * @param aSubtype the script subtype
	 * @param aParent the ZK parent
	 * @param aInsertBefore the insert before ZK component
	 * @param aClickedNodeTag the a clicked node tag
	 * @param aItem the xml tag
	 * @param aNewnodetype the a new node type
	 * @param aIsMainCondition the is main condition
	 *
	 * @return the new vbox
	 */
	public CDefVbox renderScriptLabelsVbox(String aSubtype, Component aParent, Component aInsertBefore, IXMLTag aItem, String aNewnodetype, boolean aIsMainCondition) {
		// Set changed to true, because it is very difficult to detect if a script condition
		// or action is changed. So assume it is changed, so it will be saved always.
		aParent.setAttribute("changed", "true");
		String lType = aItem.getName();
		CDefVbox lVbox = new CDefVbox();
		lVbox.setAttribute("tag", aItem);

		Hashtable<String,Object> lScriptLabelAttributes = new Hashtable<String,Object>();
		lScriptLabelAttributes.put("scripttagsubtype", aSubtype);
		lScriptLabelAttributes.put("tag", aItem);
		lScriptLabelAttributes.put("newnodetype", aNewnodetype);
		lScriptLabelAttributes.put("ismaincondition", aIsMainCondition);

		// if logical operator
		if ((lType.equals("parenthesisopen"))
				|| (lType.equals("parenthesisclose")) || (lType.equals("and"))
				|| (lType.equals("or")) || (lType.equals("not"))) {
			// show only one label
			lScriptLabelAttributes.put("scripttagtype", "condition");
			renderScriptLabel(lVbox, lScriptLabelAttributes, CDesktopComponents.vView().getLabel("cde_s_script." + lType));
		}
		else {
			boolean lIsMethod = isMethod(lType);
			boolean lIsTemplateMethod =  isTemplateMethod(lType);
			// if script method
			if (lIsMethod || lIsTemplateMethod) {
				// show a number of labels within the vbox
				String lLabelType = "";
				String lSeparator = "";
				if (isMethodUsedInCondition(lType)) {
					lLabelType = "condition";
					lSeparator = " " + CDesktopComponents.vView().getLabel("cde_s_script.or") + " ";
				}
				else if (isMethodUsedInAction(lType)) {
					lLabelType = "action";
					lSeparator = " " + CDesktopComponents.vView().getLabel("cde_s_script.and") + " ";
				}
				lScriptLabelAttributes.put("scripttagtype", lLabelType);
				// show label with type of method
				renderScriptLabel(lVbox, lScriptLabelAttributes, CDesktopComponents.vView().getLabel("cde_s_script." + aItem.getName()));
	
				String lCacId = getMethodCacId(aItem);
				String lComId = getMethodComId(aItem);
				String lTagName = "";
				String lTemp = "";
				String lLabelValue = "";
				if (lCacId.equals("0") && lComId.equals("0")) {
					if (lIsMethod) {
						// show label with case component name, no casecomponent chosen
						renderScriptLabel(lVbox, lScriptLabelAttributes, CDesktopComponents.vView().getLabel("casecomponent") + " = ");
					}
					else {
						// show label with component type, no component chosen
						renderScriptLabel(lVbox, lScriptLabelAttributes, CDesktopComponents.vView().getLabel("cde_s_script.com.type") + " = ");
					}
				}
				else {
					if (lIsMethod) {
						// show label with case component name
						renderScriptLabel(lVbox, lScriptLabelAttributes, CDesktopComponents.vView().getLabel("casecomponent") + " = " + getMethodCacName(aItem));
						// show label with case roles
						lTemp = "";
						String[] lCarNames = getMethodCarNames(aItem);
						for (int j = 0; j < lCarNames.length; j++) {
							if (!lTemp.equals(""))
								lTemp = lTemp + lSeparator;
							lTemp = lTemp + lCarNames[j];
						}
						if (!lTemp.equals(""))
							lTemp = "{" + lTemp + "}";
						renderScriptLabel(lVbox, lScriptLabelAttributes, CDesktopComponents.vView().getLabel("caseroles") + " = " + lTemp);
					}
					else if (lIsTemplateMethod) {
						// show label with component name
						renderScriptLabel(lVbox, lScriptLabelAttributes, CDesktopComponents.vView().getLabel("cde_s_script.com.type") + " = " + getMethodComType(aItem));
						lTemp = "";
						String[] lComponentTemplates = getMethodComponentTemplates(aItem).split(",");
						lSeparator = " " + CDesktopComponents.vView().getLabel("cde_s_script.and") + " ";
						for (int j = 0; j < lComponentTemplates.length; j++) {
							if (!lTemp.equals(""))
								lTemp = lTemp + lSeparator;
							lTemp = lTemp + lComponentTemplates[j];
						}
						//NOTE if template always render template even if empty
						lTemp = "{" + lTemp + "}";
						if (aIsMainCondition && getMethodComponentTemplatesOrOperation(aItem)) {
							lTemp += " " + CDesktopComponents.vView().getLabel("cde_s_script.templatesoroperation.component.name") + "=" + AppConstants.statusValueTrue;
						}
						renderScriptLabel(lVbox, lScriptLabelAttributes, CDesktopComponents.vView().getLabel("cde_s_script.componenttemplate.name") + " = " + lTemp);
					}
	
					if (isMethodUsingTags(lType)) {
						// if rungroup show label with content type, so chosen tag name
						lTagName = getMethodTagName(aItem);
						if ((lTagName.equals("")) || (lTagName.equals("0"))) {
							String[] lTagIds = getMethodTagIds(aItem);
							if (lTagIds.length > 0) {
								IXMLTag lTag = CDesktopComponents.sSpring().getTag(lCacId, lTagIds[0]);
								if (lTag != null)
									lTagName = lTag.getName();
							}
						}
						if ((lTagName.equals("")) || (lTagName.equals("0")))
							lLabelValue = CDesktopComponents.vView().getLabel("cde_s_script.tagname.name") + " = ";
						else
							lLabelValue = CDesktopComponents.vView().getLabel("cde_s_script.tagname.name") + " = " + getNodeTagLabel(currentCaseComponentCode, lTagName);
						renderScriptLabel(lVbox, lScriptLabelAttributes, lLabelValue);
						if (lIsMethod) {
							// if rungroup show label with content, that is key values of chosen tags
							lTemp = "";
							String[] lTagNames = getMethodTagNames(aSubtype, aItem);
							lSeparator = " " + CDesktopComponents.vView().getLabel("cde_s_script.and") + " ";
							for (int j = 0; j < lTagNames.length; j++) {
								if (!lTemp.equals(""))
									lTemp = lTemp + lSeparator;
								lTemp = lTemp + lTagNames[j];
							}
							if (!lTemp.equals("")) {
								lTemp = "{" + lTemp + "}";
								//NOTE templatesOrOperation also affects list of tags. It cannot be renamed to orOperation due to legacy.
								if (aIsMainCondition && getMethodTagTemplatesOrOperation(aItem)) {
									lTemp += " " + CDesktopComponents.vView().getLabel("cde_s_script.oroperation.tag.name") + "=" + AppConstants.statusValueTrue;
								}
							}
							renderScriptLabel(lVbox, lScriptLabelAttributes, CDesktopComponents.vView().getLabel("cde_s_script.tag.name") + " = " + lTemp);
						}
						lTemp = "";
						String[] lTagTemplates = getMethodTagTemplates(aItem).split(",");
						lSeparator = " " + CDesktopComponents.vView().getLabel("cde_s_script.and") + " ";
						for (int j = 0; j < lTagTemplates.length; j++) {
							if (!lTemp.equals(""))
								lTemp = lTemp + lSeparator;
							lTemp = lTemp + lTagTemplates[j];
						}
						if (!lTemp.equals("") || lIsTemplateMethod) {
							//NOTE if template always render template even if empty
							lTemp = "{" + lTemp + "}";
							if (aIsMainCondition && getMethodTagTemplatesOrOperation(aItem)) {
								lTemp += " " + CDesktopComponents.vView().getLabel("cde_s_script.templatesoroperation.tag.name") + "=" + AppConstants.statusValueTrue;
							}
							renderScriptLabel(lVbox, lScriptLabelAttributes, CDesktopComponents.vView().getLabel("cde_s_script.tagtemplate.name") + " = " + lTemp);
						}
					}
				}
	
				String lStatusId = getMethodStatusId(aItem);
				String lStatusKey = "";
				String lFunction = "";
				String lValue = "";
				String lOperator = "";
				String lOperatorValues = "";
				boolean lShowStatus = !(lCacId.equals("0") && lComId.equals("0"));
				lShowStatus = lShowStatus && !(lStatusId.equals("") || lStatusId.equals("0"));
				lShowStatus = lShowStatus && (!isMethodUsingTags(lType) || ((!lTagName.equals("")) && (!lTagName.equals("0"))));
				if (lShowStatus) {
					if (lLabelType.equals("condition")) {
						lTemp = "";
						lStatusKey = CDesktopComponents.vView().getLabel(VView.statuskeyLabelKeyPrefix + lStatusId);
						if (lStatusKey.equals("")) {
							lStatusKey = lStatusId;
						}
						String lFunctionId = getMethodFunctionId(aItem);
						lFunction = CDesktopComponents.vView().getLabel("cde_s_script.condition.function." + lFunctionId);
						lValue = getMethodValue(aItem);
						boolean lBoolValueInput = appManager.getTagOperatorValueType(lComId, lCacId, lTagName, lStatusId, "0").equals("boolean");
						if (lBoolValueInput) {
							if (lValue.equals("")) {
								// NOTE To keep supporting old situation where count was a count of value true, set default value to true.
								lValue = "true";
							}
							if (!lValue.equals("true") && !lValue.equals("false")) {
								lValue = CDesktopComponents.vView().getLabel("cde_s_script.operatorvalue." + lValue);
							}
						}
						String lOperatorId = getMethodOperatorId(aItem);
						lOperator = " " + CDesktopComponents.vView().getLabel("cde_s_script.operator." + lOperatorId) + " ";
	
						if (!lOperatorId.equals("0")) {
							lOperatorValues = getMethodOperatorValues(aItem);
							int lOperatorIdInt = Integer.parseInt(lOperatorId);
							if (lOperatorIdInt == AppConstants.operatorIntervalIndex)
								lOperatorValues = "[" + lOperatorValues + "]";
							else {
								if (lOperatorIdInt == AppConstants.operatorInIndex)
									lOperatorValues = "{" + lOperatorValues + "}";
							}
							boolean lBoolOperatorValueInput = appManager.getTagOperatorValueType(lComId, lCacId, lTagName, lStatusId, lFunctionId).equals("boolean");
							if (lBoolOperatorValueInput)
								lOperatorValues = CDesktopComponents.vView().getLabel("cde_s_script.operatorvalue." + lOperatorValues);
						}
						if (lFunctionId.equals("0")) {
							if (lOperatorId.equals("0"))
								lLabelValue = lStatusKey + "=";
							else
								lLabelValue = lStatusKey + lOperator + lOperatorValues;
						}
						else {
							if (lOperatorId.equals("0"))
								lLabelValue = lFunction + "(" + lStatusKey + "='" + lValue + "')" + "=";
							else
								lLabelValue = lStatusKey + "='" + lValue + "': " + lFunction + lOperator + lOperatorValues;
						}
						renderScriptLabel(lVbox, lScriptLabelAttributes, lLabelValue);
					}
					else if (lLabelType.equals("action")) {
						lTemp = "";
						//NOTE action label may be different from condition label
						lStatusKey = CDesktopComponents.vView().getLabel(VView.statuskeyLabelKeyPrefix + "action." + lStatusId);
						if (lStatusKey.equals("")) {
							//get general label
							lStatusKey = CDesktopComponents.vView().getLabel(VView.statuskeyLabelKeyPrefix + lStatusId);
						}
						if (lStatusKey.equals("")) {
							lStatusKey = lStatusId;
						}
						String lFunctionId = getMethodFunctionId(aItem);
						lFunction = CDesktopComponents.vView().getLabel("cde_s_script.action.function."	+ lFunctionId);
						String lOperatorValueId = getMethodOperatorValueId(aItem);
						boolean lBoolInput = appManager.getTagOperatorValueType(lComId, lCacId, lTagName, lStatusId, "").equals("boolean");
						boolean lStringInput = appManager.getTagOperatorValueType(lComId, lCacId, lTagName, lStatusId, "").equals("string");
						lOperatorValues = lOperatorValueId;
						if (lBoolInput)
							lOperatorValues = CDesktopComponents.vView().getLabel("cde_s_script.operatorvalue." + lOperatorValues);
						if (lStringInput)
							lOperatorValues = getMethodOperatorValues(aItem);
	
						if (lFunctionId.equals("0")) {
							lLabelValue = lStatusKey + "=" + lOperatorValues;
						}
						else {
							lLabelValue = lStatusKey + ": " + lFunction + " " + lOperatorValues;
						}
	
						renderScriptLabel(lVbox, lScriptLabelAttributes, lLabelValue);
					}
				}
			}
		}
		if (aInsertBefore == null)
			aParent.appendChild(lVbox);
		else
			aParent.insertBefore(lVbox, aInsertBefore);
		return lVbox;
	}

	/**
	 * Deletes script label.
	 *
	 * @param aParent the a parent
	 * @param aComponent the a component
	 * @param aItem the a item
	 */
	public void deleteScriptLabel(Component aParent, Component aComponent, IXMLTag aItem) {
		aParent.setAttribute("changed", "true");
	}

	/**
	 * Renders script condition labels.
	 *
	 * @param aClickedNodeTag the a clicked node tag
	 * @param aNewnodetype the a new node type
	 * @param aSubtype the a subtype
	 * @param aChildTags the a child tags
	 * @param aParent the a parent
	 */
	private void renderScriptConditionLabels(IXMLTag aClickedNodeTag, String aNewnodetype, String aSubtype, List<IXMLTag> aChildTags, Component aParent) {
		if (aParent == null)
			return;
		boolean lIsMainCondition = false;
		if (aChildTags != null) {
			IXMLTag lIntendedParentNodeTag = null;
			if (aClickedNodeTag != null) {
				lIntendedParentNodeTag = aClickedNodeTag;
				if (aNewnodetype != null && !aNewnodetype.equals("child")) {
					lIntendedParentNodeTag = lIntendedParentNodeTag.getParentTag();
				}
			}
			lIsMainCondition = lIntendedParentNodeTag != null && CXmlHelper.isContentTag(lIntendedParentNodeTag);
			for (IXMLTag lChildTag : aChildTags) {
				renderScriptLabelsVbox(aSubtype, aParent, null, lChildTag, aNewnodetype, lIsMainCondition);
			}
		}
		CDefVbox lVbox = new CDefVbox();
		Hashtable<String,Object> lAttributes = new Hashtable<String,Object>();
		lAttributes.put("scripttagtype", "condition");
		lAttributes.put("scripttagsubtype", aSubtype);
		lAttributes.put("clickednodetag", aClickedNodeTag);
		lAttributes.put("newnodetype", aNewnodetype);
		lAttributes.put("ismaincondition", lIsMainCondition);
		lAttributes.put("type", "empty");
		Label lLabel = new CScriptLabel(lAttributes, CDesktopComponents.vView().getLabel("cde_s_script.empty"));
		lLabel.setSclass("CCdeComponentWnd_script_condition_label");
		lVbox.appendChild(lLabel);
		aParent.appendChild(lVbox);
	}

	/**
	 * Renders script action labels.
	 *
	 * @param aClickedNodeTag the a clicked node tag
	 * @param aNewnodetype the a new node type
	 * @param aSubtype the a subtype
	 * @param aChildTags the a child tags
	 * @param aParent the a parent
	 */
	private void renderScriptActionLabels(IXMLTag aClickedNodeTag, String aNewnodetype, String aSubtype, List<IXMLTag> aChildTags, Component aParent) {
		if (aParent == null)
			return;
		boolean lIsMainCondition = false;
		if (aChildTags != null) {
			for (IXMLTag lChildTag : aChildTags) {
				renderScriptLabelsVbox(aSubtype, aParent, null, lChildTag, aNewnodetype, lIsMainCondition);
			}
		}
		CDefVbox lVbox = new CDefVbox();
		Hashtable<String,Object> lAttributes = new Hashtable<String,Object>();
		lAttributes.put("scripttagtype", "action");
		lAttributes.put("scripttagsubtype", aSubtype);
		lAttributes.put("clickednodetag", aClickedNodeTag);
		lAttributes.put("newnodetype", aNewnodetype);
		lAttributes.put("ismaincondition", lIsMainCondition);
		lAttributes.put("type", "empty");
		Label lLabel = new CScriptLabel(lAttributes, CDesktopComponents.vView().getLabel("cde_s_script.empty"));
		lLabel.setSclass("CCdeComponentWnd_script_action_label");
		lVbox.appendChild(lLabel);
		aParent.appendChild(lVbox);
	}

	/**
	 * Renders condition.
	 *
	 * @param aIsAuthor the a is author
	 * @param aPrivate the a private
	 * @param aRow the a row
	 * @param aKey the a key
	 *
	 * @return the component
	 */
	protected CDefHbox renderCondition(boolean aIsAuthor, boolean aPrivate,
			Hashtable<String,Object> aRow, String aKey) {
		// generate labels using condition
		CDefHbox lHbox = new CDefHbox();
		lHbox.setSpacing("5px");
		lHbox.setId(aKey);
		lHbox.setSclass("CCdeComponentWnd_script_condition_box");
		lHbox.setAttribute("type", aRow.get("type"));
		lHbox.setAttribute("changed", ""
				+ ((String) aRow.get("initialvalue"))
						.equals(AppConstants.statusValueTrue));

		String lSubtype = "";
		if (aRow.containsKey("deftag")) {
			IXMLTag lDefTag = (IXMLTag) aRow.get("deftag");
			if (lDefTag != null)
				lSubtype = lDefTag.getAttribute(AppConstants.defKeySubtype);
		}

		IXMLTag lClickedNodeTag = (IXMLTag) aRow.get("clickednodetag");
		String lNewnodetype = (String) aRow.get("newnodetype");
		IXMLTag lTag = (IXMLTag) aRow.get("tag");
		lHbox.setAttribute("tag", lTag);
		List<IXMLTag> lChildTags = null;
		if (lTag != null)
			lChildTags = lTag.getChildTags();
		renderScriptConditionLabels(lClickedNodeTag, lNewnodetype, lSubtype, lChildTags, lHbox);
		return lHbox;
	}

	/**
	 * Renders action.
	 *
	 * @param aIsAuthor the a is author
	 * @param aPrivate the a private
	 * @param aRow the a row
	 * @param aKey the a key
	 *
	 * @return the component
	 */
	protected CDefHbox renderAction(boolean aIsAuthor, boolean aPrivate,
			Hashtable<String,Object> aRow, String aKey) {
		// generate labels using action
		CDefHbox lHbox = new CDefHbox();
		lHbox.setSclass("CCdeComponentWnd_script_action_box");
		lHbox.setSpacing("5px");
		lHbox.setId(aKey);
		lHbox.setAttribute("type", aRow.get("type"));
		lHbox.setAttribute("changed", ""
				+ ((String) aRow.get("initialvalue"))
						.equals(AppConstants.statusValueTrue));

		String lSubtype = "";
		if (aRow.containsKey("deftag")) {
			IXMLTag lDefTag = (IXMLTag) aRow.get("deftag");
			if (lDefTag != null)
				lSubtype = lDefTag.getAttribute(AppConstants.defKeySubtype);
		}

		IXMLTag lClickedNodeTag = (IXMLTag) aRow.get("clickednodetag");
		String lNewnodetype = (String) aRow.get("newnodetype");
		IXMLTag lTag = (IXMLTag) aRow.get("tag");
		lHbox.setAttribute("tag", lTag);
		List<IXMLTag> lChildTags = null;
		if (lTag != null)
			lChildTags = lTag.getChildTags();
		renderScriptActionLabels(lClickedNodeTag, lNewnodetype, lSubtype, lChildTags, lHbox);
		return lHbox;
	}

	/**
	 * Renders ref. A reference to a tag or component, can be other component
	 * than current component.
	 *
	 * @param aIsAuthor the a is author
	 * @param aPrivate the a private
	 * @param aRow the a row
	 * @param aKey the a key
	 *
	 * @return the component
	 */
	protected CDefHbox renderRef(boolean aIsAuthor, boolean aPrivate,
			Hashtable<String,Object> aRow, String aKey) {
		CDefHbox lHbox = new CScriptMethodHbox();
		lHbox.setSpacing("");
		lHbox.setWidth("" + CDesktopComponents.vView().getColWidth2() + "px");

		lHbox.setId(aKey);
		lHbox.setAttribute("type", aRow.get("type"));
		lHbox.setAttribute("changed", "true");
		lHbox.setAttribute("ref", "true");

		IXMLTag lTag = (IXMLTag) aRow.get("tag");
		lHbox.setAttribute("tag", lTag);
		IXMLTag lNodeTag = null;
		if (lTag != null)
			lNodeTag = lTag.getParentTag();
		IXMLTag lDefTag = (IXMLTag) aRow.get("deftag");
		String lNodename = lDefTag.getName();
		lHbox.setAttribute("nodename", lNodename);
		String lReftype = lDefTag.getAttribute(AppConstants.defKeyReftype);
		String lRefcomp = lDefTag.getAttribute(AppConstants.defKeyRefcomp);
		String lReftag = lDefTag.getAttribute(AppConstants.defKeyReftag);
		String lReftagcontent = lDefTag.getAttribute(AppConstants.defKeyReftagcontent);
		boolean lRefmultiple = (lDefTag.getAttribute(AppConstants.defKeyMultiple)
				.equals(AppConstants.statusValueTrue));
		boolean lPreselectIfOneItem = !lDefTag.getAttribute(AppConstants.defKeyChoiceempty).equals(AppConstants.statusValueTrue);
		lHbox.setAttribute("preselectIfOneItem", "" + lPreselectIfOneItem);

		List<IECaseRole> lCaseRoles = getCaseRolesByMasterSlaveCacIds(getCacId());
		if (lCaseRoles == null)
			return lHbox;
		IECaseComponent lCac = CDesktopComponents.sSpring().getCaseComponent(getCacId());
		if (lCac == null)
			return lHbox;
		CCaseHelper cCaseHelper = new CCaseHelper(CDesktopComponents.sSpring());
		// Loop through caseroles
		List<String> lRefIds = new ArrayList<String>(0);
		for (int k = 0; k < lCaseRoles.size(); k++) {
			IECaseRole lCaseRole = (IECaseRole) lCaseRoles.get(k);
			List<String> lRefIds2 = cCaseHelper.getRefTagIds(lReftype, ""
					+ AppConstants.statusKeySelectedIndex, lCaseRole, lCac,
					lNodeTag);
			if (lRefIds != null)
				lRefIds.addAll(lRefIds2);
		}
		xmlNodeToRefHbox(lTag, lDefTag, lNodename, lReftype, lRefcomp, lReftag, lReftagcontent,
				lRefmultiple, lHbox, lRefIds, lPreselectIfOneItem);
		return lHbox;
	}

	/**
	 * Renders other. If no input element yet render only label.
	 *
	 * @param aIsAuthor the a is author
	 * @param aPrivate the a private
	 * @param aRow the a row
	 * @param aKey the a key
	 *
	 * @return the component
	 */
	protected CDefLabel renderOther(boolean aIsAuthor, boolean aPrivate,
			Hashtable<String,Object> aRow, String aKey) {
		CDefLabel lLabel = new CDefLabel();
		lLabel.setId(aKey);
		lLabel.setValue((String) aRow.get("value"));
		lLabel.setAttribute((String) "type", aRow.get("type"));
		lLabel.setAttribute("changed", ""
				+ ((String) aRow.get("initialvalue"))
						.equals(AppConstants.statusValueTrue));
		return lLabel;
	}

	/**
	 * Is value of content row part of the node tag key.
	 *
	 * @param aChildDefTag the a row
	 * @param aKey the a key
	 *
	 * @return if part of node tag key
	 */
	protected boolean isContentRowValuePartOfNodeTagKey(IXMLTag aChildDefTag, String aKey) {
		boolean lIsPartOfKey = false;
		if (aChildDefTag == null) {
			//if attribute row is not required
			return false;
		}
		//it is required if the child tag contains a key (element) of the parent node tag
		IXMLTag lParentNodeDefTag = aChildDefTag.getParentTag();
		while (lParentNodeDefTag != null && !lParentNodeDefTag.getAttribute(AppConstants.defKeyType).equals(AppConstants.defValueNode)) {
			lParentNodeDefTag = lParentNodeDefTag.getParentTag();
		}
		if (lParentNodeDefTag != null) {
			String[] lKeyTagNames = lParentNodeDefTag.getAttribute(AppConstants.defKeyKey).split(",");
			for (int i = 0; i < lKeyTagNames.length; i++) {
				if (lKeyTagNames[i].equals(aKey)) {
					lIsPartOfKey = true;
					break;
				}
			}
		}
		return lIsPartOfKey;
	}

	/**
	 * Is value for content row required, meaning a value may not be empty.
	 *
	 * @param aRow the a row
	 *
	 * @return if required
	 */
	protected boolean isContentRowValueRequired(Hashtable<String,Object> aRow) {
		boolean lRequired = false;
		if (!aRow.get("attribute").equals("")) {
			//if attribute row is not required
			return false;
		}
		//so child row
		if (!aRow.containsKey("deftag")) {
			//if no child def tag
			return false;
		}
		//NOTE check if child value is required
		IXMLTag lChildDefTag = (IXMLTag)aRow.get("deftag");
		String lKey = (String)aRow.get("name");
		//it is required if the child tag contains a key (element) of the parent node tag
		lRequired = isContentRowValuePartOfNodeTagKey(lChildDefTag, lKey);
		//it is required if the child tag is a reference to another case component or node tag and the choice may not be empty
		if (!lRequired) {
			lRequired = lChildDefTag.getAttribute(AppConstants.defKeyType).equals("ref") && 
					!(lChildDefTag.getAttribute(AppConstants.defKeyChoiceempty).equals(AppConstants.statusValueTrue) ||
					lChildDefTag.getAttribute(AppConstants.defKeyTagChoiceempty).equals(AppConstants.statusValueTrue));
		}
		//it is required if "notempty" of the child tag is true
		if (!lRequired) {
			lRequired = lChildDefTag.getAttribute(AppConstants.defKeyNotempty).equals(AppConstants.statusValueTrue);
		}
		return lRequired;
	}

	/**
	 * Renders row within tag edit window. A row has a title and one or more input elements.
	 *
	 * @param aRows the a rows
	 * @param aRow the a row
	 */
	protected void renderContentRow(Component aRows, Hashtable<String,Object> aRow) {
		String lType = (String)aRow.get("type");
		String lKey = "";
		if (aRow.get("attribute").equals("")) {
			//child tag is rendered
			lKey = (String)aRow.get("name");
		}
		else {
			//attribute is rendered
			lKey = (String)aRow.get("attribute");
		}
		String[] lLabelKeyAndValue = getLabelKeyAndValue(aRow);
		String lLabelKey = lLabelKeyAndValue[0];
		String lLabelValue = lLabelKeyAndValue[1];
		//NOTE if required, add required label
		if (isContentRowValueRequired(aRow)) {
			lLabelValue += " " + CDesktopComponents.vView().getLabel(VView.childtagLabelKeyPrefix + "value.required");
		}
		boolean lPrivate = ((String)aRow.get("private")).equals(AppConstants.statusValueTrue);
		boolean lHidden = ((String)aRow.get("hidden")).equals(AppConstants.statusValueTrue);
		boolean lIsAuthor = ((CCdeComponentItemWnd) aRows.getRoot()).accIsAuthor();

		Row lRow = new CDefRow();
		Hbox lParentHbox = new CDefHbox();
		lRow.appendChild(lParentHbox);
		Div lDiv = new CDefDiv();
 		lDiv.setWidth("" + (CDesktopComponents.vView().getColWidth1() - 15) + "px");
		lParentHbox.appendChild(lDiv);
		Label lLabel = new CDefLabel();
		lLabel.setValue(lLabelValue);
		if (!lPrivate && 
				(lType.equals("line") || 
				lType.equals("text") ||
				lType.equals("simplerichtext") ||
				lType.equals("richtext")))
			lRow.setSclass("CCdeComponentWnd_visible_to_student");
		else
			lRow.setSclass("CCdeComponentWnd_row_" + lKey + "_" + lType);
		lLabel.setSclass("CCdeComponentWnd_row_" + lKey + "_" + lType + "_label");
		lDiv.appendChild(lLabel);
		if (lType.equals("line"))
			lRow.appendChild(renderLine(lIsAuthor, lPrivate, aRow, lKey));
		else if (lType.equals("text"))
			lRow.appendChild(renderText(lIsAuthor, lPrivate, aRow, lKey));
		else if (lType.equals("simplerichtext"))
			lRow.appendChild(renderRichtext(true, lIsAuthor, lPrivate, aRow, lKey));
		else if (lType.equals("richtext"))
			lRow.appendChild(renderRichtext(false, lIsAuthor, lPrivate, aRow, lKey));
		else if (lType.equals("picture"))
			lRow.appendChild(renderPicture(lIsAuthor, lPrivate, aRow, lKey));
		else if (lType.equals("boolean"))
			lRow.appendChild(renderCheckbox(lIsAuthor, lPrivate, aRow, lKey));
		else if (lType.equals("singleselect"))
			lRow.appendChild(renderCombobox(lIsAuthor, lPrivate, aRow, lKey));
		else if (lType.equals("singleselectimage"))
			lRow.appendChild(renderImageListbox(lIsAuthor, lPrivate, aRow, lKey));
		else if (lType.equals("blob"))
			lRow.appendChild(renderBlob(lIsAuthor, lPrivate, aRow, lKey));
		else if (lType.equals("condition"))
			lRow.appendChild(renderCondition(lIsAuthor, lPrivate, aRow, lKey));
		else if (lType.equals("action"))
			lRow.appendChild(renderAction(lIsAuthor, lPrivate, aRow, lKey));
		else if (lType.equals("ref"))
			lRow.appendChild(renderRef(lIsAuthor, lPrivate, aRow, lKey));
		else if (lType.equals("formula"))
			lRow.appendChild(renderFormula(lIsAuthor, lPrivate, aRow, lKey));
		else
			lRow.appendChild(renderOther(lIsAuthor, lPrivate, aRow, lKey));

		aRows.appendChild(lRow);
		if (lHidden)
			lRow.setVisible(false);
		
		CDesktopComponents.vView().setParentHelpText(lParentHbox, getHelpText(aRow));
	}

	/**
	 * Xml node to rows. Converts xml node tag childs to rows.
	 *
	 * @param aXmlTree the a xml tree
	 * @param aNew the a new
	 * @param aClickedNodeTag the a clicked node tag
	 * @param aNodename the a nodename
	 * @param aNewnodetype the a new node type
	 * @param aRows the a rows
	 */
	public void xmlNodeToRows(IXMLTag aXmlTree, boolean aNew,
			IXMLTag aClickedNodeTag, String aNodename, String aNewnodetype, Component aRows) {
		currentCaseComponentCode = getCaseComponent().getEComponent().getCode();
		List<Hashtable<String,Object>> rows = CDesktopComponents.sSpring().getXmlManager().getContentData(getXmlDefTree(), aXmlTree,
				aNew, aClickedNodeTag, aNodename, aNewnodetype);
		aRows.setAttribute("rows", rows);
		for (Hashtable<String,Object> row : rows) {
			renderContentRow(aRows, row);
		}
		//NOTE CKeditor must exist before methods setFilebrowserImageBrowseUrl and setFilebrowserFlashBrowseUrl
		//can be set! So they cannot be set within method renderRichtext.
		CDefCKeditor lTextbox = (CDefCKeditor)CDesktopComponents.vView().getComponent("richtext");
		if (lTextbox != null && lTextbox.getAttribute("simple").equals("true")) {
			lTextbox.setFilebrowserImageBrowseUrl(VView.getInitParameter("ckeditor.cde.image.url"));
			lTextbox.setFilebrowserFlashBrowseUrl(VView.getInitParameter("ckeditor.cde.flash.url"));
		}
	}

	/**
	 * Returns case components used in script conditions and actions.
	 *
	 * @param aSubtype the a subtype
	 * @param aCacs the a cacs
	 * @param aComponentCodes the a component codes, comma separated
	 *
	 * @return the case components
	 */
	protected List<IECaseComponent> getScriptCaseComponents(String aSubtype, List<IECaseComponent> aCacs, String aComponentCodes) {
		if (aCacs != null) {
			return aCacs;
		}
		else if (aComponentCodes.equals(AppConstants.selectAll)) {
			return CDesktopComponents.sSpring().getCaseComponents(CDesktopComponents.sSpring().getCase());
		}
		else if (isRelationBetweenElements(aSubtype)) {
			return CDesktopComponents.sSpring().getCaseComponentsByMasterCacId(getCacId(), aComponentCodes);
		}
		else {
			return CDesktopComponents.sSpring().getCaseComponents(aComponentCodes);
		}
	}

	/**
	 * Returns components used in script conditions and actions.
	 *
	 * @param aSubtype the a subtype
	 * @param aCacs the a cacs
	 * @param aComponentCodes the a component codes, comma separated
	 *
	 * @return the components
	 */
	protected List<IEComponent> getScriptComponents(String aSubtype, List<IECaseComponent> aCacs, String aComponentCodes) {
		List<IECaseComponent> lCacs = getScriptCaseComponents(aSubtype, aCacs, aComponentCodes);
		Hashtable<String,IEComponent> lHComs = new Hashtable<String,IEComponent>();
		if (lCacs != null) {
			for (Object lCac : lCacs) {
				lHComs.put(((IECaseComponent)lCac).getEComponent().getCode(), ((IECaseComponent)lCac).getEComponent());
			}
		}
		List<IEComponent> lComs = new ArrayList<IEComponent>();
		if (lHComs.size() > 0) {
			for (Enumeration<String> keys = lHComs.keys(); keys.hasMoreElements();) {
				String key = keys.nextElement();
				lComs.add(lHComs.get(key));
			}
		}
		return lComs;
	}

	protected class ComponentSortByCode implements Comparator<IEComponent>{
		public int compare(IEComponent o1, IEComponent o2) {
			return CDesktopComponents.vView().getLabel(VView.componentLabelKeyPrefix + o1.getCode()).compareTo(CDesktopComponents.vView().getLabel(VView.componentLabelKeyPrefix + o2.getCode()));
		}
	}
	
	/**
	 * Returns components to be used in script conditions and actions.
	 *
	 * @return the components
	 */
	protected List<IEComponent> getScriptComponents() {
		List<IEComponent> lAllComponents = ((IComponentManager)CDesktopComponents.sSpring().getBean("componentManager")).getAllComponents(true);
		//NOTE filter lAllComponents on supported components per skin.
		List<IEComponent> lAllAllowedComponents = new ArrayList<IEComponent>();
		for (IEComponent lComponent : lAllComponents) {
			if (CDesktopComponents.sSpring().isComponentAllowed(lComponent)) {
				lAllAllowedComponents.add(lComponent);
			}
		}
		Collections.sort(lAllAllowedComponents, new ComponentSortByCode());
		return lAllAllowedComponents;
	}

	/**
	 * Renders com type input for building/updating script condition or action.
	 *
	 * @param aParent the a parent
	 * @param aInsertBefore the a insert before
	 * @param aId the a id
	 * @param aSubtype the a subtype
	 * @param aComId the a com id
	 * @param aPreselectIfOneItem the a select if one item
	 *
	 * @return the component
	 */
	protected CCombo renderComIdInput(Component aParent, Component aInsertBefore,
			String aId, String aSubtype, String aComId, boolean aPreselectIfOneItem) {
		CCombo lInput = new CScriptComCombo(aId);
		insertInput(aParent, lInput, aInsertBefore);
		List lComs = getScriptComponents();
		lInput.showItems(lComs, aComId, true, aPreselectIfOneItem, true);
		return lInput;
	}

	/**
	 * Renders cac input for building/updating script condition or action.
	 *
	 * @param aParent the a parent
	 * @param aInsertBefore the a insert before
	 * @param aId the a id
	 * @param aSubtype the a subtype
	 * @param aCacId the a cac id
	 * @param aCacs the a cacs
	 * @param aComponentCodes the a component codes, comma separated
	 * @param aPreselectIfOneItem the a select if one item
	 *
	 * @return the component
	 */
	protected CCombo renderCacInput(Component aParent, Component aInsertBefore,
			String aId, String aSubtype, String aCacId, List<IECaseComponent> aCacs, String aComponentCodes, boolean aPreselectIfOneItem) {
		CCombo lInput = new CScriptCacCombo(aId, aComponentCodes);
		insertInput(aParent, lInput, aInsertBefore);
		List lCacs = getScriptCaseComponents(aSubtype, aCacs, aComponentCodes);
		lInput.showItems(lCacs, aCacId, true, aPreselectIfOneItem, true);
		return lInput;
	}

	/**
	 * Renders cars input for building/updating script condition or action.
	 *
	 * @param aParent the a parent
	 * @param aInsertBefore the a insert before
	 * @param aId the a id
	 * @param aCacId the a cac id
	 * @param aCarIds the a car ids
	 * @param aSubtype the a subtype
	 * @param aRadio the a radio
	 * @param aNpc the a npc
	 * @param aPreselectIfOneItem preselect if only one item
	 *
	 * @return the component
	 */
	protected CListbox renderCarsInput(Component aParent,
			Component aInsertBefore, String aId, String aCacId,
			String[] aCarIds, String aSubtype, boolean aNpc, boolean aRadio, boolean aPreselectIfOneItem) {
		CListbox lInput = new CScriptCarListbox(aId);
		insertInput(aParent, lInput, aInsertBefore);
		if (aRadio) {
			lInput.setMultiple(false);
		}

		List lCars = null;
		if (isRelationBetweenElements(aSubtype))
			lCars = CDesktopComponents.sSpring().getSCaseRoleHelper().getCaseRolesByMasterSlaveCacIds(getCacId(), aCacId);
		else {
			lCars = CDesktopComponents.sSpring().getSCaseRoleHelper().getCaseRolesByCacId(aCacId);
			if (lCars != null) {
				for (int i = (lCars.size()-1); i >= 0; i--) {
					IECaseRole lCar = (IECaseRole)lCars.get(i);
					if (lCar.getNpc() != aNpc)
						lCars.remove(i);
				}
			}
		}
		if (isRelationWithCaserole(aSubtype))
			lCars = CDesktopComponents.sSpring().getSCaseRoleHelper().getCaseRoles(aNpc);
		lInput.showItems(lCars, aCarIds, aPreselectIfOneItem, true);
		return lInput;
	}

	/**
	 * Renders tag name input for building/updating script condition or action.
	 * Either aCacId or aComId is not empty.
	 *
	 * @param aParent the a parent
	 * @param aInsertBefore the a insert before
	 * @param aId the a id
	 * @param aSubtype the a subtype
	 * @param aNodename the a nodename
	 * @param aCacId the a cac id
	 * @param aComId the a com id
	 * @param aTagName the a tag name
	 *
	 * @return the component
	 */
	protected CCombo renderTagNameInput(Component aParent,
			Component aInsertBefore, String aId, String aSubtype,
			String aNodename, String aCacId, String aComId, String aTagName) {
		boolean lIsTemplate = isTemplateMethod(aNodename);
		String lTagName = "";
		String lStatusElement = "";
		List lTagNames = null;
		if (isMethodUsedInCondition(aNodename)) {
			lTagName = "condition";
			lStatusElement = AppConstants.getstatusElement;
		}
		else if (isMethodUsedInAction(aNodename) || isMethodUsedInRunAction(aNodename)) {
			lTagName = "action";
			lStatusElement = AppConstants.setstatusElement;
		}
		else if (isMethodReferenceToTag(aNodename)) {
			lIsTemplate = false;
			lTagName = "reftag";
			lStatusElement = AppConstants.setstatusElement;
		}
		if (!lStatusElement.equals("")) {
			if (!lIsTemplate) {
				lTagNames = getScript().getNodeTagNames(aCacId, lStatusElement);
			}
			else {
				lTagNames = getScript().getDefNodeTagNames(aComId, lStatusElement);
			}
		}
		CCombo lInput = new CScriptTagNameCombo(aId, lTagName);
		insertInput(aParent, lInput, aInsertBefore);
		lInput.showItems(lTagNames, aTagName, true, true, true);
		return lInput;
	}

	/**
	 * Renders tag input for building/updating script condition or action.
	 *
	 * @param aParent the a parent
	 * @param aInsertBefore the a insert before
	 * @param aId the a id
	 * @param aSubtype the a subtype
	 * @param aTags the a tags
	 * @param aCacId the a cac id
	 * @param aTagId the a tag id
	 * @param aTagNames the a tag names, comma separated
	 *
	 * @return the component
	 */
	protected CCombo renderTagInput(Component aParent, Component aInsertBefore,
			String aId, String aSubtype, List<IXMLTag> aTags, String aCacId,
			String aTagId, String aTagNames) {
		CCombo lInput = new CScriptTagCombo(aId, aTagNames);
		insertInput(aParent, lInput, aInsertBefore);
		List lTags = null;
		if (aTagNames.equals(AppConstants.selectAll)) {
			lTags = getScript().getNodeTags(aCacId);
		}
		else {
			lTags = getScript().getNodeTags(aCacId, aTagNames);
		}
		lInput.showItems(lTags, aTagId, true, true, false);
		return lInput;
	}

	/**
	 * Renders tags input for building/updating script condition or action.
	 *
	 * @param aParent the a parent
	 * @param aInsertBefore the a insert before
	 * @param aId the a id
	 * @param aSubtype the a subtype
	 * @param aTags the a tags
	 * @param aCacId the a cac id
	 * @param aTagIds the a tag ids
	 * @param aTagNames the a tag names, comma separated
	 * @param aPreselectIfOneItem preselect if only one item
	 *
	 * @return the component
	 */
	protected CListbox renderTagsInput(Component aParent,
			Component aInsertBefore, String aId, String aSubtype, List<IXMLTag> aTags,
			String aCacId, String[] aTagIds, String aTagNames, boolean aPreselectIfOneItem) {
		CListbox lInput = new CScriptTagListbox(aId, aTagNames);
		lInput.setSizedByContent(true);
		insertInput(aParent, lInput, aInsertBefore);
		List lTags = null;
		if (aTagNames.equals(AppConstants.selectAll)) {
			lTags = getScript().getNodeTags(aCacId);
		}
		else {
			lTags = getScript().getNodeTags(aCacId, aTagNames);
		}
		lInput.showItems(lTags, aTagIds, aPreselectIfOneItem, false);
		return lInput;
	}

	/**
	 * Renders status input for building/updating script condition or action.
	 *
	 * @param aParent the a parent
	 * @param aInsertBefore the a insert before
	 * @param aId the a id
	 * @param aStatusId the a status id
	 * @param aNodename the a nodename
	 * @param aCacId the a cac id
	 * @param aComId the a com id
	 * @param aTagName the a tag name
	 *
	 * @return the component
	 */
	protected CCombo renderStatusInput(Component aParent,
			Component aInsertBefore, String aId, String aStatusId,
			String aNodename, String aCacId, String aComId, String aTagName) {
		boolean lIsTemplate = isTemplateMethod(aNodename);
		String lTagName = "";
		String lStatusElement = "";
		List lTagStatusKeyIds = null;
		
		String lSessRole = (String)CDesktopComponents.cControl().getAccSessAttr("role");
		if (isMethodUsedInCondition(aNodename)) {
			lTagName = "condition";
			if (!(lSessRole.equals(AppConstants.c_role_adm) || lSessRole.equals(AppConstants.c_role_crm))) {
				// if administrator or case run manager, then all student status may be altered
				lStatusElement = AppConstants.getstatusElement;
			}
		}
		else if (isMethodUsedInAction(aNodename) || isMethodUsedInRunAction(aNodename)) {
			lTagName = "action";
			if (!(lSessRole.equals(AppConstants.c_role_adm) || lSessRole.equals(AppConstants.c_role_crm))) {
				// if administrator or case run manager, then all student status may be altered
				lStatusElement = AppConstants.setstatusElement;
			}
		}
		if (!lIsTemplate) {
			lTagStatusKeyIds = getScript().getTagStatusKeyIds(aCacId, aTagName, lStatusElement);
		}
		else {
			lTagStatusKeyIds = getScript().getDefTagStatusKeyIds(aComId, aTagName, lStatusElement);
		}
		CCombo lInput = new CScriptStatusCombo(aId, lTagName);
		insertInput(aParent, lInput, aInsertBefore);
		lInput.showItems(lTagStatusKeyIds,aStatusId, true, true, true);
		return lInput;
	}

	/**
	 * Renders function input for building/updating script condition or action.
	 *
	 * @param aParent the a parent
	 * @param aInsertBefore the a insert before
	 * @param aId the a id
	 * @param aStatusId the a status id
	 * @param aFunctionId the a function id
	 * @param aLabelPrefix the a label prefix
	 * @param aCondition the a condition
	 *
	 * @return the component
	 */
	protected CCombo renderFunctionInput(Component aParent,
			Component aInsertBefore, String aId, String aStatusId, String aFunctionId, String aLabelPrefix, boolean aCondition) {
		CCombo lInput = new CScriptFunctionCombo(aId, aLabelPrefix);
		insertInput(aParent, lInput, aInsertBefore);
		lInput.setAttribute("condition", "" + aCondition);
		List lTagFunctionKeyIds = null;
		if (!aStatusId.equals("0")) {
			if (aCondition)
				lTagFunctionKeyIds = getScript().getTagConditionFunctionKeyIds(aStatusId);
			else
				lTagFunctionKeyIds = getScript().getTagActionFunctionKeyIds(aStatusId);
		}
//		boolean lShow = ((!aFunctionId.equals("")) && (!aFunctionId.equals("0"))&& (lTagFunctionKeyIds != null) && (lTagFunctionKeyIds.size() > 0));
		boolean lShow = ((lTagFunctionKeyIds != null) && (lTagFunctionKeyIds.size() > 0));
		lInput.showItems(lTagFunctionKeyIds,aFunctionId, true, false, true);
		//if no functions can be found hide combobox, else let parent decide (depending on visibility of status combobox)
		if (!lShow)
			aParent.setVisible(lShow);
		return lInput;
	}

	/**
	 * Renders value textbox input for building/updating script condition or action.
	 *
	 * @param aParent the a parent
	 * @param aInsertBefore the a insert before
	 * @param aId the a id
	 * @param aValue the a value
	 *
	 * @return the component
	 */
	protected CDefTextbox renderValueTextboxInput(Component aParent,
			Component aInsertBefore, String aId, String aValue) {
		CDefTextbox lInput = new CDefTextbox();
		insertInput(aParent, lInput, aInsertBefore);
		lInput.setId(aId);
		lInput.setValue(aValue);
		lInput.setCols(20);
		return lInput;
	}

	/**
	 * Renders value combo box input for building/updating script condition or action.
	 *
	 * @param aParent the a parent
	 * @param aInsertBefore the a insert before
	 * @param aId the a id
	 * @param aValue the a value
	 *
	 * @return the component
	 */
	protected CCombo renderValueComboInput(Component aParent,
			Component aInsertBefore, String aId, String aValue) {
		CCombo lInput = new CScriptOperatorValueCombo(aId);
		insertInput(aParent, lInput, aInsertBefore);
		List lTagOperatorValueIds = getScript().getTagOperatorValueIds();
		if (aValue.equals("") || aValue.equals("true")) {
			//NOTE if no value, default true is preselected
			aValue = "1";
		}
		lInput.showItems(lTagOperatorValueIds, aValue, true, false, true);
		return lInput;
	}

	/**
	 * Renders operator input for building/updating script condition or action.
	 *
	 * @param aParent the a parent
	 * @param aInsertBefore the a insert before
	 * @param aId the a id
	 * @param aStatusId the a status id
	 * @param aFunctionId the a function id
	 * @param aOperatorId the a operator id
	 *
	 * @return the component
	 */
	protected CCombo renderOperatorInput(Component aParent,
			Component aInsertBefore, String aId, String aStatusId, String aFunctionId, String aOperatorId) {
		CCombo lInput = new CScriptOperatorCombo(aId);
		insertInput(aParent, lInput, aInsertBefore);
		List lTagOperatorKeyIds = null;
		if (!aStatusId.equals("0"))
			lTagOperatorKeyIds = getScript().getTagOperatorKeyIds(aStatusId, aFunctionId);
		lInput.showItems(lTagOperatorKeyIds,aOperatorId, true, true, false);
		return lInput;
	}

	/**
	 * Renders operator values input for building/updating script condition or action.
	 *
	 * @param aParent the a parent
	 * @param aInsertBefore the a insert before
	 * @param aId the a id
	 * @param aOperatorValues the a operator values
	 *
	 * @return the component
	 */
	protected CDefTextbox renderOperatorValuesInput(Component aParent,
			Component aInsertBefore, String aId, String aOperatorValues) {
		CDefTextbox lInput = new CDefTextbox();
		insertInput(aParent, lInput, aInsertBefore);
		lInput.setId(aId);
		lInput.setValue(aOperatorValues);
		lInput.setCols(20);
		return lInput;
	}

	/**
	 * Renders operator value input for building/updating script condition or action.
	 *
	 * @param aParent the a parent
	 * @param aInsertBefore the a insert before
	 * @param aId the a id
	 * @param aOperatorValue the a operator value
	 *
	 * @return the component
	 */
	protected CCombo renderOperatorValueInput(Component aParent,
			Component aInsertBefore, String aId, String aOperatorValue) {
		CCombo lInput = new CScriptOperatorValueCombo(aId);
		insertInput(aParent, lInput, aInsertBefore);
		List lTagOperatorValueIds = getScript().getTagOperatorValueIds();
		if (aOperatorValue.equals("")) {
			//NOTE if no value, default true is preselected
			aOperatorValue = "1";
		}
		lInput.showItems(lTagOperatorValueIds, aOperatorValue, true, false, true);
		return lInput;
	}

	/**
	 * Renders tag content name input for building/updating script condition or action.
	 *
	 * @param aParent the a parent
	 * @param aInsertBefore the a insert before
	 * @param aId the a id
	 * @param aCacId the a cac id
	 * @param aTagName the a tag name
	 * @param aTagContentName the a tag content name
	 * @param aNodeName the a tag content name
	 *
	 * @return the component
	 */
	protected CCombo renderTagContentNameInput(Component aParent, Component aInsertBefore,
			String aId, String aCacId, String aTagName, String aTagContentName, String aNodeName) {
		CCombo lInput = new CScriptTagContentCombo(aId, aNodeName);
		insertInput(aParent, lInput, aInsertBefore);
		List lTagContentNames = getScript().getNodeTagContentNames(aCacId, aTagName);
		lInput.showItems(lTagContentNames, aTagContentName, true, true, true);
		return lInput;
	}

	/**
	 * Inserts input element.
	 *
	 * @param aParent the a parent
	 * @param aComponent the a component
	 * @param aInsertBefore the a insert before
	 */
	private void insertInput(Component aParent, Component aComponent,
			Component aInsertBefore) {
		if (aComponent == null)
			return;
		if (aInsertBefore == null)
			aParent.appendChild(aComponent);
		else
			aParent.insertBefore(aComponent, aInsertBefore);
	}

	/**
	 * Renders script label.
	 *
	 * @param aParent the a parent
	 * @param aLabelKeyPrefix the a label key prefix 
	 *
	 * @return the component
	 */
	protected CDefLabel renderScriptLabel(Component aParent, String aLabelKeyPrefix) {
		CDefHbox lHbox = new CDefHbox();
		lHbox.setSpacing("4px");
		aParent.appendChild(lHbox);
		CDefLabel lLabel = new CDefLabel();
		lLabel.setSclass("CCdeComponentWnd_script_label");
		lLabel.setValue(CDesktopComponents.vView().getLabel(aLabelKeyPrefix + ".label"));
		lHbox.appendChild(lLabel);
		CDesktopComponents.vView().setParentHelpTextByLabelKey(lHbox, aLabelKeyPrefix + ".help");
		return lLabel;
	}

	/**
	 * Renders script label and replaces '%1', '%2', etc by labels given by aLabelKeys.
	 *
	 * @param aParent the a parent
	 * @param aLabelKey the a label key
	 * @param aLabelKeys the a label keys
	 *
	 * @return the component
	 */
	protected CDefLabel renderScriptLabel(Component aParent, String aLabelKey, String[] aLabelKeys) {
		String lLabelStr = CDesktopComponents.vView().getLabel(aLabelKey);
		if (aLabelKeys != null && aLabelKeys.length > 0) {
			int lLabelStringNumber = 0;
			for (int i=0;i<aLabelKeys.length;i++) {
				String lReplaceStr = CDesktopComponents.vView().getLabel(aLabelKeys[i]);
				if (lReplaceStr.length() > 0) {
					lLabelStr = lLabelStr.replaceAll("%" + (i + 1), CDesktopComponents.vView().getLabel(aLabelKeys[i]));
				}
				lLabelStringNumber++;
			}
		}
		CDefLabel lLabel = new CDefLabel();
		lLabel.setSclass("CCdeComponentWnd_script_label");
		lLabel.setValue(lLabelStr);
		aParent.appendChild(lLabel);
		return lLabel;
	}

	/**
	 * Renders script component types for building/updating script condition or action.
	 *
	 * @param aParent the a parent
	 * @param aInsertBefore the a insert before
	 * @param aMethodTag the a methodtag
	 * @param aDefMethodTag the a defmethodtag
	 * @param aNodename the a nodename
	 * @param aSubtype the a subtype
	 * @param aComId the a com id
	 * @param aVisible the a visible
	 * @param aPreselectIfOneItem the a select if one item
	 *
	 * @return the component
	 */
	protected CCombo renderScriptCom(Component aParent,
			Component aInsertBefore, IXMLTag aMethodTag, IXMLTag aDefMethodTag, String aNodename,
			String aSubtype, String aComId, boolean aVisible, boolean aPreselectIfOneItem) {
		CDefVbox lVbox = new CDefVbox();
		lVbox.setSclass("CCdeScriptMethodWnd_script_content_box");
		aParent.appendChild(lVbox);
		lVbox.setVisible(aVisible);
		renderScriptLabel(lVbox,"cde_s_script.com");
		CCombo lInput = null;
		if (isTemplateMethod(aNodename)) {
			lInput = renderComIdInput(lVbox, aInsertBefore, aNodename+"scriptComId",
					aSubtype, aComId, aPreselectIfOneItem);
			lInput.setSclass("CCdeScriptMethodWnd_script_content_box");
		}
		return lInput;
	}

	/**
	 * Renders script cac for building/updating script condition or action.
	 *
	 * @param aParent the a parent
	 * @param aInsertBefore the a insert before
	 * @param aMethodTag the a methodtag
	 * @param aDefMethodTag the a defmethodtag
	 * @param aNodename the a nodename
	 * @param aSubtype the a subtype
	 * @param aCacId the a cac id
	 * @param aCacs the a cacs
	 * @param aComponentCodes the a component codes, comma separated
	 * @param aVisible the a visible
	 * @param aPreselectIfOneItem the a select if one item
	 *
	 * @return the component
	 */
	protected CCombo renderScriptCac(Component aParent,
			Component aInsertBefore, IXMLTag aMethodTag, IXMLTag aDefMethodTag, String aNodename,
			String aSubtype, String aCacId, List aCacIds, String aComponentCodes, boolean aVisible, boolean aPreselectIfOneItem) {
		CDefVbox lVbox = new CDefVbox();
		lVbox.setSclass("CCdeScriptMethodWnd_script_content_box");
		aParent.appendChild(lVbox);
		lVbox.setVisible(aVisible);
		renderScriptLabel(lVbox,"cde_s_script.cac");
		CCombo lInput = null;
		boolean lRenderInput = false;
		boolean lDisableInput = false;
		if (isMethod(aNodename)) {
			lRenderInput = true;
			if (isMethodUsedInItemfeedback(aSubtype)) {
				lDisableInput = true;
			}
		}
		if (isRelationBetweenElements(aSubtype)) {
			lRenderInput = true;
		}
		if (lRenderInput) {
			lInput = renderCacInput(lVbox, aInsertBefore, aNodename+"scriptCacId",
					aSubtype, aCacId, aCacIds, aComponentCodes, aPreselectIfOneItem);
			lInput.setSclass("CCdeScriptMethodWnd_script_content_box");
			if (lInput != null && lDisableInput) {
				lInput.setDisabled(true);
			}
		}
		return lInput;
	}

	/**
	 * Renders script cars for building/updating script condition or action.
	 *
	 * @param aParent the a parent
	 * @param aInsertBefore the a insert before
	 * @param aMethodTag the a methodtag
	 * @param aDefMethodTag the a defmethodtag
	 * @param aNodename the a nodename
	 * @param aSubtype the a subtype
	 * @param aCacId the a cac id
	 * @param aCarIds the a car ids
	 * @param aVisible the a visible
	 * @param aRadio the a radio
	 * @param aPreselectIfOneItem the a select if one item
	 *
	 * @return the component
	 */
	protected CListbox renderScriptCars(Component aParent,
			Component aInsertBefore, IXMLTag aMethodTag, IXMLTag aDefMethodTag, String aNodename,
			String aSubtype, String aCacId, String[] aCarIds,
			boolean aVisible, boolean aRadio, boolean aPreselectIfOneItem) {
		CDefVbox lVbox = new CDefVbox();
		lVbox.setSclass("CCdeScriptMethodWnd_script_content_box");
		aParent.appendChild(lVbox);
		lVbox.setVisible(aVisible);
		String lLabelKeyPrefix = "cde_s_script.car";
		renderScriptLabel(lVbox, lLabelKeyPrefix);
		boolean lNpc = false;
		if ((aMethodTag != null) && (aMethodTag.getDefTag() != null))
			lNpc = (aMethodTag.getDefTag().getAttribute(AppConstants.defKeyNpc).equals(AppConstants.statusValueTrue));
		else {
			if (aDefMethodTag != null)
				lNpc = (aDefMethodTag.getDefAttribute(AppConstants.defKeyNpc).equals(AppConstants.statusValueTrue));
		}
		CListbox lInput = renderCarsInput(lVbox, aInsertBefore, aNodename+"scriptCarIds", aCacId, aCarIds, aSubtype, lNpc, aRadio, aPreselectIfOneItem);
		lInput.setSclass("CCdeScriptMethodWnd_script_content_box");
		if (isMethodUsedInItemfeedback(aSubtype))
			lInput.setDisabled(true);
		return lInput;
	}

	/**
	 * Renders script tag name for building/updating script condition or action.
	 *
	 * @param aParent the a parent
	 * @param aInsertBefore the a insert before
	 * @param aMethodTag the a methodtag
	 * @param aDefMethodTag the a defmethodtag
	 * @param aNodename the a nodename
	 * @param aSubtype the a subtype
	 * @param aCacId the a cac id
	 * @param aComId the a com id
	 * @param aTagName the a tag name
	 * @param aVisible the a visible
	 *
	 * @return the component
	 */
	protected CCombo renderScriptTagName(Component aParent,
			Component aInsertBefore, IXMLTag aMethodTag, IXMLTag aDefMethodTag, String aNodename,
			String aSubtype, String aCacId, String aComId, String aTagName, boolean aVisible) {
		CDefVbox lVbox = new CDefVbox();
		lVbox.setSclass("CCdeScriptMethodWnd_script_content_box");
		aParent.appendChild(lVbox);
		lVbox.setVisible(aVisible);
		renderScriptLabel(lVbox,"cde_s_script.tagname");
		CCombo lInput = renderTagNameInput(lVbox, aInsertBefore, aNodename+"scriptTagName",
				aSubtype, aNodename, aCacId, aComId, aTagName);
		lInput.setSclass("CCdeScriptMethodWnd_script_content_box");
		return lInput;
	}

	/**
	 * Renders script tag for building/updating script condition or action.
	 *
	 * @param aParent the a parent
	 * @param aInsertBefore the a insert before
	 * @param aMethodTag the a methodtag
	 * @param aDefMethodTag the a defmethodtag
	 * @param aNodename the a nodename
	 * @param aSubtype the a subtype
	 * @param aCacId the a cac id
	 * @param aTagId the a tag id
	 * @param aRefTagName the a ref tag name
	 * @param aRefTagNames the a ref tag names, comma separated
	 * @param aVisible the a visible
	 *
	 * @return the component
	 */
	protected CCombo renderScriptTag(Component aParent,
			Component aInsertBefore, IXMLTag aMethodTag, IXMLTag aDefMethodTag, String aNodename,
			String aSubtype, String aCacId, String aTagId, String aRefTagNames, boolean aVisible) {
		CDefVbox lVbox = new CDefVbox();
		lVbox.setSclass("CCdeScriptMethodWnd_script_content_box");
		aParent.appendChild(lVbox);
		lVbox.setVisible(aVisible);
		CDefLabel lScriptLabel = renderScriptLabel(lVbox, "cde_s_script.tag");
		List<IXMLTag> lTags = new ArrayList<IXMLTag>(0);
		// NOTE Not necessary anymore
/*		if (isMethodUsedInItemfeedback(aSubtype)) {
			CCdeComponentItemWnd lWindow = (CCdeComponentItemWnd) CDesktopComponents.vView()
					.getComponent("cdeComponentItemWnd");
			if (lWindow != null) {
				IXMLTag lItem = lWindow.getItem();
				boolean lNewitem = lWindow.getNewitem();
				if (!lNewitem)
					lItem = lItem.getParentTag();
				lTags = lItem.getChilds("alternative");
			}
		} */

		CCombo lInput = null;
		if (isRelationToTags(aSubtype) || isMethodUsedInRunAction(aNodename)) {
			lInput = renderTagInput(lVbox, aInsertBefore, aNodename+"scriptTagId",
					aSubtype, lTags, aCacId, aTagId, aRefTagNames);
			lInput.setSclass("CCdeScriptMethodWnd_script_content_box");
		}
		if (lInput == null) {
			lScriptLabel.setVisible(false);
		}
		return lInput;
	}

	/**
	 * Renders templates input field for building/updating script condition or action.
	 *
	 * @param aParent the a parent
	 * @param aInsertBefore the a insert before
	 * @param aId the a id
	 * @param aTemplates the tag templates string
	 *
	 * @return the component
	 */
	protected CDefTextbox renderTemplatesInput(Component aParent,
			Component aInsertBefore, String aId, String aTemplates) {
		CDefTextbox lInput = new CDefTextbox();
		insertInput(aParent, lInput, aInsertBefore);
		lInput.setId(aId);
		lInput.setValue(aTemplates);
		lInput.setCols(50);
		return lInput;
	}

	/**
	 * Renders templates check box for building/updating script condition or action.
	 *
	 * @param aParent the a parent
	 * @param aInsertBefore the a insert before
	 * @param aId the a id
	 * @param aChecked the a checked
	 * @param aUsedInTemplateCondition the a used in template condition
	 * @param aUsedInTagTemplate the a used in tag template
	 *
	 * @return the component
	 */
	protected CDefCheckbox renderTemplatesCheckbox(Component aParent,
			Component aInsertBefore, String aId, boolean aChecked, boolean aUsedInTemplateCondition, boolean aUsedInTagTemplate) {
		CDefHbox lHbox = new CDefHbox();
		insertInput(aParent, lHbox, aInsertBefore);
		CDefCheckbox lCheckbox = new CDefCheckbox();
		lHbox.appendChild(lCheckbox);
		lCheckbox.setId(aId);
		lCheckbox.setChecked(aChecked);
		if (aUsedInTagTemplate) {
			if (aUsedInTemplateCondition) {
				renderScriptLabel(lHbox,"cde_s_script.templates.tag.checkbox");
			}
			else {
				//NOTE if tag input list and template input field templatesOrOperation is also used. It cannot be renamed to orOperation due to legacy.
				renderScriptLabel(lHbox,"cde_s_script.tag.checkbox");
			}
		}
		else {
			renderScriptLabel(lHbox,"cde_s_script.templates.component.checkbox");
		}
		return lCheckbox;
	}

	/**
	 * Renders script component templates input area for building/updating script condition or action.
	 *
	 * @param aParent the a parent
	 * @param aInsertBefore the a insert before
	 * @param aMethodTag the a methodtag
	 * @param aDefMethodTag the a defmethodtag
	 * @param aNodename the a nodename
	 * @param aSubtype the a subtype
	 * @param aComponentTemplates the tag templates string
	 * @param aRenderTemplatesCheckbox the render templates checkbox
	 * @param aOrOperation the a or operation
	 * @param aVisible the a visible
	 *
	 * @return the component
	 */
	protected CDefTextbox renderScriptComponentTemplates(Component aParent,
			Component aInsertBefore, IXMLTag aMethodTag, IXMLTag aDefMethodTag, String aNodename,
			String aSubtype, String aComponentTemplates, boolean aRenderTemplatesCheckbox, boolean aOrOperation, boolean aVisible) {
		CDefVbox lVbox = new CDefVbox();
		lVbox.setSclass("CCdeScriptMethodWnd_script_content_box");
		aParent.appendChild(lVbox);
		lVbox.setVisible(aVisible);
		renderScriptLabel(lVbox,"cde_s_script.componenttemplates");
		CDefTextbox lInput = renderTemplatesInput(lVbox, aInsertBefore,
				aNodename+"scriptComponentTemplates", aComponentTemplates);
		lInput.setSclass("CCdeScriptMethodWnd_script_content_box");
		if (aRenderTemplatesCheckbox) {
			CDefCheckbox lCheckbox = renderTemplatesCheckbox(lVbox, aInsertBefore,
					aNodename+"scriptComponentTemplatesCheckbox", aOrOperation, false, false);
		}
		return lInput;
	}

	/**
	 * Renders script tag templates input area for building/updating script condition or action.
	 *
	 * @param aParent the a parent
	 * @param aInsertBefore the a insert before
	 * @param aMethodTag the a methodtag
	 * @param aDefMethodTag the a defmethodtag
	 * @param aNodename the a nodename
	 * @param aSubtype the a subtype
	 * @param aTagTemplates the tag templates string
	 * @param aRenderTemplatesCheckbox the render templates checkbox
	 * @param aOrOperation the a or operation
	 * @param aVisible the a visible
	 *
	 * @return the component
	 */
	protected CDefTextbox renderScriptTagTemplates(Component aParent,
			Component aInsertBefore, IXMLTag aMethodTag, IXMLTag aDefMethodTag, String aNodename,
			String aSubtype, String aTagTemplates, boolean aRenderTemplatesCheckbox, boolean aOrOperation, boolean aVisible) {
		CDefVbox lVbox = new CDefVbox();
		lVbox.setSclass("CCdeScriptMethodWnd_script_content_box");
		aParent.appendChild(lVbox);
		lVbox.setVisible(aVisible);
		renderScriptLabel(lVbox,"cde_s_script.tagtemplates");
		CDefTextbox lInput = renderTemplatesInput(lVbox, aInsertBefore,
				aNodename+"scriptTagTemplates", aTagTemplates);
		lInput.setSclass("CCdeScriptMethodWnd_script_content_box");
		if (aRenderTemplatesCheckbox) {
			CDefCheckbox lCheckbox = renderTemplatesCheckbox(lVbox, aInsertBefore,
					aNodename+"scriptTagTemplatesCheckbox", aOrOperation, aNodename.matches("evalrungrouptagtemplate|setrungrouptagtemplate"), true);
		}
		return lInput;
	}

	/**
	 * Renders script tags for building/updating script condition or action.
	 *
	 * @param aParent the a parent
	 * @param aInsertBefore the a insert before
	 * @param aMethodTag the a methodtag
	 * @param aDefMethodTag the a defmethodtag
	 * @param aNodename the a nodename
	 * @param aSubtype the a subtype
	 * @param aCacId the a cac id
	 * @param aTagIds the a tag ids
	 * @param aRefTagNames the a ref tag names, comma separated
	 * @param aVisible the a visible
	 * @param aPreselectIfOneItem the a select if one item
	 * @param aIsMethodUsedInMainCondition the a is method used in main condition
	 *
	 * @return the component
	 */
	protected CListbox renderScriptTags(Component aParent,
			Component aInsertBefore, IXMLTag aMethodTag, IXMLTag aDefMethodTag, String aNodename,
			String aSubtype, String aCacId, String[] aTagIds,
			String aRefTagNames, boolean aVisible, boolean aPreselectIfOneItem, boolean aIsMethodUsedInMainCondition) {
		CDefVbox lVbox = new CDefVbox();
		lVbox.setSclass("CCdeScriptMethodWnd_script_content_box");
		aParent.appendChild(lVbox);
		lVbox.setVisible(aVisible);
		renderScriptLabel(lVbox, "cde_s_script.tags");
		List<IXMLTag> lTags = new ArrayList<IXMLTag>(0);
		// NOTE Not necessary anymore
/*		if (isMethodUsedInItemfeedback(aSubtype)) {
			CCdeComponentItemWnd lWindow = (CCdeComponentItemWnd) CDesktopComponents.vView()
					.getComponent("cdeComponentItemWnd");
			if (lWindow != null) {
				IXMLTag lItem = lWindow.getItem();
				boolean lNewitem = lWindow.getNewitem();
				if (!lNewitem)
					lItem = lItem.getParentTag();
				lTags = lItem.getChilds("alternative");
			}
		} */

		CListbox lInput = renderTagsInput(lVbox, aInsertBefore,
				aNodename+"scriptTagIds", aSubtype, lTags, aCacId, aTagIds, aRefTagNames, aPreselectIfOneItem);
		lInput.setSclass("CCdeScriptMethodWnd_script_content_box");

		if (aNodename.matches("evalrungrouptagstatus|setrungrouptagstatus")) {
			String lTagTemplates = getMethodTagTemplates(aMethodTag);
			boolean lTemplatesOrOperation = getMethodTagTemplatesOrOperation(aMethodTag);
			renderScriptTagTemplates(lVbox, aInsertBefore, aMethodTag, aDefMethodTag, aNodename, aSubtype, lTagTemplates, aIsMethodUsedInMainCondition, lTemplatesOrOperation, aVisible);		
		}

		return lInput;
	}

	/**
	 * Renders script status for building/updating script condition or action.
	 *
	 * @param aParent the a parent
	 * @param aInsertBefore the a insert before
	 * @param aMethodTag the a methodtag
	 * @param aDefMethodTag the a defmethodtag
	 * @param aNodename the a nodename
	 * @param aSubtype the a subtype
	 * @param aStatusId the a status id
	 * @param aCacId the a cac id
	 * @param aComId the a com id
	 * @param aTagName the a tag name
	 * @param aVisible the a visible
	 *
	 * @return the component
	 */
	protected CCombo renderScriptStatus(Component aParent,
			Component aInsertBefore, IXMLTag aMethodTag, IXMLTag aDefMethodTag, String aNodename,
			String aSubtype, String aStatusId, String aCacId, String aComId, String aTagName, boolean aVisible) {
		CDefVbox lVbox = new CDefVbox();
		lVbox.setSclass("CCdeScriptMethodWnd_script_status_box");
		aParent.appendChild(lVbox);
		lVbox.setVisible(aVisible);
		renderScriptLabel(lVbox,"cde_s_script.statuskey");
		CCombo lInput = renderStatusInput(lVbox, aInsertBefore,
				aNodename+"scriptStatusId", aStatusId, aNodename, aCacId, aComId, aTagName);
		lInput.setSclass("CCdeScriptMethodWnd_script_status_box");
		return lInput;
	}

	/**
	 * Renders script function for building/updating script condition or action.
	 *
	 * @param aParent the a parent
	 * @param aInsertBefore the a insert before
	 * @param aMethodTag the a methodtag
	 * @param aDefMethodTag the a defmethodtag
	 * @param aNodename the a nodename
	 * @param aSubtype the a subtype
	 * @param aStatusId the a status id
	 * @param aFunctionId the a function id
	 * @param aVisible the a visible
	 * @param aLabelPrefix the a label prefix
	 * @param aCondition the a condition
	 *
	 * @return the component
	 */
	protected CCombo renderScriptFunction(Component aParent,
			Component aInsertBefore, IXMLTag aMethodTag, IXMLTag aDefMethodTag, String aNodename,
			String aSubtype, String aStatusId, String aFunctionId, boolean aVisible, String aLabelPrefix, boolean aCondition) {
		CDefVbox lVbox = new CDefVbox();
		lVbox.setSclass("CCdeScriptMethodWnd_script_status_box");
		aParent.appendChild(lVbox);
		lVbox.setVisible(aVisible);
		renderScriptLabel(lVbox,"cde_s_script.function");
		CCombo lInput = renderFunctionInput(lVbox, aInsertBefore,
				aNodename+"scriptFunctionId", aStatusId, aFunctionId, aLabelPrefix, aCondition);
		lInput.setSclass("CCdeScriptMethodWnd_script_status_box");
		return lInput;
	}

	/**
	 * Renders script value textbox for building/updating script condition or action.
	 *
	 * @param aParent the a parent
	 * @param aInsertBefore the a insert before
	 * @param aMethodTag the a methodtag
	 * @param aDefMethodTag the a defmethodtag
	 * @param aNodename the a nodename
	 * @param aSubtype the a subtype
	 * @param aValue the a value
	 * @param aVisible the a visible
	 *
	 * @return the component
	 */
	protected CDefTextbox renderScriptValueTextbox(Component aParent,
			Component aInsertBefore, IXMLTag aMethodTag, IXMLTag aDefMethodTag, String aNodename,
			String aSubtype, String aValue, boolean aVisible) {
		CDefVbox lVbox = new CDefVbox();
		lVbox.setSclass("CCdeScriptMethodWnd_script_status_box");
		aParent.appendChild(lVbox);
		lVbox.setVisible(aVisible);
		renderScriptLabel(lVbox,"cde_s_script.give_operatorvalue");
		CDefTextbox lInput = renderValueTextboxInput(lVbox, aInsertBefore,
				aNodename+"scriptValueTextbox", aValue);
		lInput.setSclass("CCdeScriptMethodWnd_script_status_box");
		return lInput;
	}

	/**
	 * Renders script value combo box for building/updating script condition or action.
	 *
	 * @param aParent the a parent
	 * @param aInsertBefore the a insert before
	 * @param aMethodTag the a methodtag
	 * @param aDefMethodTag the a defmethodtag
	 * @param aNodename the a nodename
	 * @param aSubtype the a subtype
	 * @param aValue the a value
	 * @param aVisible the a visible
	 *
	 * @return the component
	 */
	protected CCombo renderScriptValueCombo(Component aParent,
			Component aInsertBefore, IXMLTag aMethodTag, IXMLTag aDefMethodTag, String aNodename,
			String aSubtype, String aValue, boolean aVisible) {
		//NOTE script value input only is shown if function count is selected.
		CDefVbox lVbox = new CDefVbox();
		lVbox.setSclass("CCdeScriptMethodWnd_script_status_box");
		aParent.appendChild(lVbox);
		lVbox.setVisible(aVisible);
		renderScriptLabel(lVbox,"cde_s_script.choose_operatorvalue");
		CCombo lInput = renderValueComboInput(lVbox, aInsertBefore,
				aNodename+"scriptValueCombo", aValue);
		lInput.setSclass("CCdeScriptMethodWnd_script_status_box");
		return lInput;
	}

	/**
	 * Renders script operator for building/updating script condition or action.
	 *
	 * @param aParent the a parent
	 * @param aInsertBefore the a insert before
	 * @param aMethodTag the a methodtag
	 * @param aDefMethodTag the a defmethodtag
	 * @param aNodename the a nodename
	 * @param aSubtype the a subtype
	 * @param aStatusId the a status id
	 * @param aFunctionId the a function id
	 * @param aOperatorId the a operator id
	 * @param aVisible the a visible
	 *
	 * @return the component
	 */
	protected CCombo renderScriptOperator(Component aParent,
			Component aInsertBefore, IXMLTag aMethodTag, IXMLTag aDefMethodTag, String aNodename,
			String aSubtype, String aStatusId, String aFunctionId, String aOperatorId, boolean aVisible) {
		CDefVbox lVbox = new CDefVbox();
		lVbox.setSclass("CCdeScriptMethodWnd_script_status_box");
		aParent.appendChild(lVbox);
		lVbox.setVisible(aVisible);
		renderScriptLabel(lVbox,"cde_s_script.operator");
		CCombo lInput = renderOperatorInput(lVbox, aInsertBefore,
				aNodename+"scriptOperatorId", aStatusId, aFunctionId, aOperatorId);
		lInput.setSclass("CCdeScriptMethodWnd_script_status_box");
		return lInput;
	}

	/**
	 * Renders script operator values for building/updating script condition or action.
	 *
	 * @param aParent the a parent
	 * @param aInsertBefore the a insert before
	 * @param aMethodTag the a methodtag
	 * @param aDefMethodTag the a defmethodtag
	 * @param aNodename the a nodename
	 * @param aSubtype the a subtype
	 * @param aOperatorValues the a operator values
	 * @param aVisible the a visible
	 * @param aEnterCount the a enter count
	 *
	 * @return the component
	 */
	protected CDefTextbox renderScriptOperatorValues(Component aParent,
			Component aInsertBefore, IXMLTag aMethodTag, IXMLTag aDefMethodTag, String aNodename,
			String aSubtype, String aOperatorValues, boolean aVisible, boolean aEnterCount) {
		CDefVbox lVbox = new CDefVbox();
		lVbox.setSclass("CCdeScriptMethodWnd_script_status_box");
		aParent.appendChild(lVbox);
		lVbox.setVisible(aVisible);
		if (aEnterCount) {
			renderScriptLabel(lVbox,"cde_s_script.operatorvalue.count");
		}
		else {
			renderScriptLabel(lVbox,"cde_s_script.give_operatorvalues");
		}			
		CDefTextbox lInput = renderOperatorValuesInput(lVbox, aInsertBefore,
				aNodename+"scriptOperatorValues", aOperatorValues);
		lInput.setSclass("CCdeScriptMethodWnd_script_status_box");
		return lInput;
	}

	/**
	 * Renders script operator value for building/updating script condition or action.
	 *
	 * @param aParent the a parent
	 * @param aInsertBefore the a insert before
	 * @param aMethodTag the a methodtag
	 * @param aDefMethodTag the a defmethodtag
	 * @param aNodename the a nodename
	 * @param aSubtype the a subtype
	 * @param aOperatorValueId the a operator value id
	 * @param aVisible the a visible
	 *
	 * @return the component
	 */
	protected CCombo renderScriptOperatorValue(Component aParent,
			Component aInsertBefore, IXMLTag aMethodTag, IXMLTag aDefMethodTag, String aNodename,
			String aSubtype, String aOperatorValueId, boolean aVisible) {
		CDefVbox lVbox = new CDefVbox();
		lVbox.setSclass("CCdeScriptMethodWnd_script_status_box");
		aParent.appendChild(lVbox);
		lVbox.setVisible(aVisible);
		renderScriptLabel(lVbox,"cde_s_script.choose_operatorvalue");
		CCombo lInput = renderOperatorValueInput(lVbox, aInsertBefore,
				aNodename+"scriptOperatorValue", aOperatorValueId);
		lInput.setSclass("CCdeScriptMethodWnd_script_status_box");
		return lInput;
	}

	/**
	 * Renders script tag content name for building/updating script condition or action.
	 *
	 * @param aParent the a parent
	 * @param aInsertBefore the a insert before
	 * @param aMethodTag the a methodtag
	 * @param aDefMethodTag the a defmethodtag
	 * @param aNodename the a nodename
	 * @param aSubtype the a subtype
	 * @param aCacId the a cac id
	 * @param aTagName the a tag name
	 * @param aTagContentName the a tag content name
	 * @param aVisible the a visible
	 *
	 * @return the component
	 */
	protected CCombo renderScriptTagContentName(Component aParent,
			Component aInsertBefore, IXMLTag aMethodTag, IXMLTag aDefMethodTag, String aNodename,
			String aSubtype, String aCacId, String aTagName, String aTagContentName, boolean aVisible) {
		CDefVbox lVbox = new CDefVbox();
		lVbox.setSclass("CCdeScriptMethodWnd_script_status_box");
		aParent.appendChild(lVbox);
		lVbox.setVisible(aVisible);
		renderScriptLabel(lVbox,"cde_s_script.tagcontentname");
		CCombo lInput = renderTagContentNameInput(lVbox, aInsertBefore, aNodename+"scriptTagContentName",
				aCacId, aTagName, aTagContentName, aNodename);
		lInput.setSclass("CCdeScriptMethodWnd_script_status_box");
		return lInput;
	}

	/**
	 * Xml node to method hbox. Renders script xml node tag to hbox containing
	 * input elements to build/change the script element.
	 *
	 * @param aClickedTag the a clicked tag
	 * @param aDefTag the a def tag
	 * @param aNodename the a nodename
	 * @param aSubtype the a subtype
	 * @param aHbox the a hbox
	 * @param aPreselectIfOneItem the a select if one item
	 * @param aIsMainCondition the a is main condition
	 */
	public void xmlNodeToMethodHbox(IXMLTag aClickedTag, IXMLTag aDefTag, String aNodename,
			String aSubtype, Component aHbox, boolean aPreselectIfOneItem, boolean aIsMainCondition) {
		((CDefHbox)aHbox).setSpacing("");
		CDefVbox lVbox = new CDefVbox();
		CDefHbox lHbox1 = new CDefHbox();
		lHbox1.setSpacing("10px");
		CDefVbox lVbox1 = new CDefVbox();
		CDefVbox lVbox2 = new CDefVbox();
		Hr lHr = new Hr();
		lHr.setId("scriptHr");
		lHr.setVisible(false);
		CDefHbox lHbox2 = new CDefHbox();
		lHbox2.setSpacing("10px");
		lVbox.appendChild(lHbox1);
		lHbox1.appendChild(lVbox1);
		lHbox1.appendChild(lVbox2);
		lVbox.appendChild(lHr);
		lVbox.appendChild(lHbox2);
		aHbox.appendChild(lVbox);
		String lCacId = getMethodCacId(aClickedTag);
		String[] lCarIds = getMethodCarIds(aClickedTag);
		String lComId = getMethodComId(aClickedTag);
		String lComponentTemplates = getMethodComponentTemplates(aClickedTag);
		//NOTE default or operation is false
		boolean lComponentTemplatesOrOperation = false;
		if (aClickedTag != null) {
			//NOTE if clicked tag unequal to null, so existing method is edited, get last saved value
			lComponentTemplatesOrOperation = getMethodComponentTemplatesOrOperation(aClickedTag);
		}
		else {
			//NOTE else a new method is edited, in which case aParentTag is unequal to null
			lComponentTemplatesOrOperation = aIsMainCondition;
		}
		String lTagName = getMethodTagName(aClickedTag);
		String[] lTagIds = getMethodTagIds(aClickedTag);
		String lTagTemplates = getMethodTagTemplates(aClickedTag);
		//NOTE default or operation is false
		boolean lTagTemplatesOrOperation = false;
		if (aClickedTag != null) {
			//NOTE if clicked tag unequal to null, so existing one is edited, get last saved value
			lTagTemplatesOrOperation = getMethodTagTemplatesOrOperation(aClickedTag);
		}
		else {
			//NOTE else a new method is edited, in which case aParentTag is unequal to null
			lTagTemplatesOrOperation = aIsMainCondition;
		}
		String lStatusId = getMethodStatusId(aClickedTag);
		String lFunctionId = getMethodFunctionId(aClickedTag);
		String lValue = getMethodValue(aClickedTag);
		String lOperatorId = getMethodOperatorId(aClickedTag);
//		if condition then possibly more operator values under operator tag
		String lOperatorValues = getMethodOperatorValues(aClickedTag);
//		if action only one operator value and no operator tag
		String lOperatorValueId = getMethodOperatorValueId(aClickedTag);
		
		boolean lStringInput = appManager.getTagOperatorValueType(lComId, lCacId, lTagName, lStatusId, "").equals("string");
		
		boolean lIsMethod = isMethod(aNodename);
		boolean lIsTemplateMethod = isTemplateMethod(aNodename);
		boolean lIsMethodUsingTags = isMethodUsingTags(aNodename);
		boolean lIsMethodUsedInCondition = isMethodUsedInCondition(aNodename);
		boolean lIsMethodUsedInMainCondition = lIsMethodUsedInCondition && (isMethodUsedInMainCondition(aClickedTag) || aIsMainCondition);
		boolean lIsMethodUsedInAction = isMethodUsedInAction(aNodename);
		boolean lIsMethodUsedInRunAction = isMethodUsedInRunAction(aNodename);
		boolean lIsMethodUsedInItemfeedback = isMethodUsedInItemfeedback(aSubtype);
		boolean lIsMethodUsedInAssessmentfeedbac = isMethodUsedInAssessmentfeedback(aSubtype);
		boolean lIsMethodUsedInScript = isMethodUsedInScript(aSubtype);
		
		boolean lIsFunctionCount = !lFunctionId.equals("") && Integer.parseInt(lFunctionId) == AppConstants.functionCountIndex;

		if (lIsMethod) {
			lVbox.setSclass("CCdeScriptMethodWnd_method_box");
		}
		else if (lIsTemplateMethod) {
			lVbox.setSclass("CCdeScriptMethodWnd_templatemethod_box");
		}
		
		// save in clicked tag
		if (lIsMethodUsedInItemfeedback || lIsMethodUsedInAssessmentfeedbac) {
			// get default
			if (lCacId.equals("0")) {
				lCacId = (String) getCacId();
			}
			// save in clicked tag
			setMethodCacId(aClickedTag, lCacId);
		}

		if (lIsMethod || lIsMethodUsedInItemfeedback) {
			CCombo lCombo = renderScriptCac(lVbox1, null, aClickedTag, aDefTag, aNodename, aSubtype,
					lCacId, null, "", true, aPreselectIfOneItem);
			if (lCacId.equals("0")) {
				String lId = lCombo.getSelectedItemId();
				if (!(lId.equals("") || lId.equals("0"))) {
					lCacId = lId;
				}
			}
		}

		if (lIsTemplateMethod) {
			CCombo lCombo = renderScriptCom(lVbox1, null, aClickedTag, aDefTag, aNodename, aSubtype,
					lComId, true, aPreselectIfOneItem);
			if (lComId.equals("0")) {
				String lId = lCombo.getSelectedItemId();
				if (!(lId.equals("") || lId.equals("0"))) {
					lComId = lId;
				}
			}
		}

		boolean lShow = (!lCacId.equals("0") || !lComId.equals("0"));
		if (!lIsTemplateMethod) {
			if (!lIsMethodUsedInRunAction) {
				renderScriptCars(lVbox1, null, aClickedTag, aDefTag, aNodename, aSubtype, lCacId,
						lCarIds, lShow, false, aPreselectIfOneItem);
			}
		}
		else {
			renderScriptComponentTemplates(lVbox1, null, aClickedTag, aDefTag, aNodename, aSubtype, lComponentTemplates, lIsMethodUsedInMainCondition, lComponentTemplatesOrOperation, lShow);
		}

		if (lIsMethodUsingTags) {
			if (lIsMethodUsedInScript) {
				// get default
				// save in clicked tag
			}
			if ((lTagName.equals("0")) && (lTagIds.length > 0)) {
				IXMLTag lTag = CDesktopComponents.sSpring().getTag(lCacId, lTagIds[0]);
				if (lTag != null)
					lTagName = lTag.getName();
			}
			Component lTagNameCombo = renderScriptTagName(lVbox2, null, aClickedTag, aDefTag, aNodename, aSubtype,
					lCacId, lComId, lTagName, lShow);
			if (lTagName.equals("0") && (lTagNameCombo != null)) {
				CScriptMethodHbox lHbox = null;
				Component lComponent = lTagNameCombo.getParent();
				while ((lComponent != null) && (!(lComponent instanceof CScriptMethodHbox))) {
					lComponent = lComponent.getParent();
				}
				if (lComponent != null) {
					lHbox = (CScriptMethodHbox) lComponent;
					lTagName = lHbox.getChosenTagName();
				}
			}
			lShow = (lShow && (!lTagName.equals("0")));
			if (!lIsTemplateMethod) {
				if (!lIsMethodUsedInRunAction) {
					renderScriptTags(lVbox2, null, aClickedTag, aDefTag, aNodename, aSubtype,
							lCacId, lTagIds, lTagName, lShow, aPreselectIfOneItem, lIsMethodUsedInMainCondition);
				}
				else {
					String lTagId = "0";
					if (lTagIds.length > 0) {
						lTagId = lTagIds[0];
					}
					renderScriptTag(lVbox2, null, aClickedTag, aDefTag, aNodename, aSubtype,
							lCacId, lTagId, lTagName, lShow);
				}
			}
			else {
				renderScriptTagTemplates(lVbox2, null, aClickedTag, aDefTag, aNodename, aSubtype, lTagTemplates, lIsMethodUsedInMainCondition, lTagTemplatesOrOperation, lShow);		
			}
		}

		if (lIsMethodUsedInItemfeedback) {
			// get default
			if (lStatusId.equals("0"))
				lStatusId = "" + AppConstants.statusKeyOpenedIndex;
			// save in clicked tag
			setMethodStatusId(aClickedTag, lStatusId);
		}
		if (lIsMethodUsedInAssessmentfeedbac) {
			// get default
			if (lStatusId.equals("0"))
				lStatusId = "" + AppConstants.statusKeyScoreIndex;
			// save in clicked tag
			setMethodStatusId(aClickedTag, lStatusId);
		}

		boolean lBooleanOperatorValueInput =
				(appManager.getTagOperatorValueType(lComId, lCacId, lTagName, lStatusId, lFunctionId).equals("boolean"));
		if (lIsMethodUsedInCondition) {
			if (lIsMethodUsingTags && (lTagName.equals("") || lTagName.equals("0")))
				lShow = false;
			renderScriptStatus(lHbox2, null, aClickedTag, aDefTag, aNodename, aSubtype,
					lStatusId, lCacId, lComId, lTagName, lShow);

			lShow = (lShow && (!lStatusId.equals("")) && (!lStatusId.equals("0")));
			if (lIsMethodUsedInItemfeedback || lIsMethodUsedInAssessmentfeedbac) {
				// get default
				if (lFunctionId.equals("0"))
					lFunctionId = "0";
				// save in clicked tag
				setMethodFunctionId(aClickedTag, lFunctionId);
			}
			renderScriptFunction(lHbox2, null, aClickedTag, aDefTag, aNodename, aSubtype,
					lStatusId, lFunctionId, lShow, "cde_s_script.condition.function.", true);

			boolean lBooleanValueInput =
					(appManager.getTagOperatorValueType(lComId, lCacId, lTagName, lStatusId, "0").equals("boolean"));
			renderScriptValueTextbox(lHbox2, null, aClickedTag, aDefTag, aNodename,
					aSubtype, lValue, (lShow && lIsFunctionCount && !lBooleanValueInput));
			renderScriptValueCombo(lHbox2, null, aClickedTag, aDefTag, aNodename,
					aSubtype, lValue, (lShow && lIsFunctionCount && lBooleanValueInput));

			if (lIsMethodUsedInItemfeedback || lIsMethodUsedInAssessmentfeedbac) {
				// get default
				if (lOperatorId.equals("0"))
					lOperatorId = "" + AppConstants.operatorEqIndex;
				// save in clicked tag
				setMethodOperatorId(aClickedTag, lOperatorId);
			}
			renderScriptOperator(lHbox2, null, aClickedTag, aDefTag, aNodename, aSubtype,
					lStatusId, lFunctionId, lOperatorId, lShow);

			if (lIsMethodUsedInItemfeedback) {
				// get default
				if (lOperatorValues.equals(""))
					lOperatorValues = "" + AppConstants.statusValueTrueIndex;
				// save in clicked tag
				setMethodOperatorValues(aClickedTag, lOperatorValues);
			}

			renderScriptOperatorValues(lHbox2, null, aClickedTag, aDefTag, aNodename,
					aSubtype, lOperatorValues, (lShow && !lBooleanOperatorValueInput), lIsFunctionCount);
			renderScriptOperatorValue(lHbox2, null, aClickedTag, aDefTag, aNodename,
					aSubtype, lOperatorValues, (lShow && lBooleanOperatorValueInput));
		}
		if (lIsMethodUsedInAction || lIsMethodUsedInRunAction) {
			if (lIsMethodUsingTags && (lTagName.equals("") || lTagName.equals("0")))
				lShow = false;
			renderScriptStatus(lHbox2, null, aClickedTag, aDefTag, aNodename, aSubtype,
					lStatusId, lCacId, lComId, lTagName, lShow);

			lShow = (lShow && (!lStatusId.equals("")) && (!lStatusId.equals("0")));
			renderScriptFunction(lHbox2, null, aClickedTag, aDefTag, aNodename, aSubtype,
					lStatusId, lFunctionId, lShow, "cde_s_script.action.function.", false);

			String lOperatorValue = lOperatorValueId;
			if (lStringInput) {
				lOperatorValue = lOperatorValues;
			}
			renderScriptOperatorValues(lHbox2, null, aClickedTag, aDefTag, aNodename,
					aSubtype, lOperatorValue, (lShow && !lBooleanOperatorValueInput), lIsFunctionCount);
			renderScriptOperatorValue(lHbox2, null, aClickedTag, aDefTag, aNodename,
					aSubtype, lOperatorValue, (lShow && (lBooleanOperatorValueInput)));
		}
	}

	/**
	 * Xml node to ref hbox. Renders ref xml node tag to hbox containing
	 * input elements to build/change the reference.
	 *
	 * @param aClickedTag the a clicked tag
	 * @param aDefTag the a def tag
	 * @param aNodename the a nodename
	 * @param aReftype the a reftype
	 * @param aRefcomp the a refcomp
	 * @param aReftag the a reftag
	 * @param aReftagconent the a reftag content
	 * @param aRefmultiple the a refmultiple
	 * @param aHbox the a hbox
	 * @param aRefIds the a ref ids
	 * @param aPreselectIfOneItem the a select if one item
	 */
	protected void xmlNodeToRefHbox(IXMLTag aClickedTag, IXMLTag aDefTag, String aNodename,
			String aReftype, String aRefcomp, String aReftag, String aReftagcontent,
			boolean aRefmultiple, Component aHbox, List<String> aRefIds, boolean aPreselectIfOneItem) {
		String lCacId = "0";
		String lCarId = "0";
		String[] lCarIds = null;
		String lTagId = "0";
		String[] lTagIds = null;

		if ((aRefIds != null) && (aRefIds.size() > 0)) {
			lCarIds = new String[aRefIds.size()];
			lTagIds = new String[aRefIds.size()];
			for (int i = 0; i < aRefIds.size(); i++) {
				String lIds = aRefIds.get(i);
				lTagIds[i] = "0";
				if ((lIds != null) && (!lIds.equals(""))) {
					String[] lIdArr = lIds.split(",");
					if (lIdArr.length == 3) {
						lCacId = lIdArr[1];
						lCarId = lIdArr[0];
						lCarIds[i] = lCarId;
						lTagId = lIdArr[2];
						lTagIds[i] = lTagId;
					}
				}
			}
		}
		if (lCarIds == null)
			lCarIds = new String[] { "0" };
		if (lTagIds == null)
			lTagIds = new String[] { "0" };

		boolean lCaseRolesRef = isRelationWithCaserole(aReftype);

		Vbox lVbox = new CDefVbox();
		aHbox.appendChild(lVbox);
		
		boolean lCanChooseTagContent = (!aReftagcontent.equals(""));
		// get lCacs containing relevant tags or tag content
		List<IECaseComponent> lCacs = null;
		if (aRefcomp.equals(AppConstants.selectAll)) {
			lCacs = CDesktopComponents.sSpring().getCaseComponents(CDesktopComponents.sSpring().getCase());
		}
		
		if (lCacs != null) {
			List<IECaseComponent> lCaseComponents = new ArrayList<IECaseComponent>();
			for (IECaseComponent lCac : lCacs) {
				String lId = "" + lCac.getCacId();
				List<IXMLTag> lNodeTags = getScript().getNodeTags(lId);
				if (lCanChooseTagContent) {
					boolean lAdd = false;
					Hashtable<String,String> hTagNames = new Hashtable<String,String>(0);
					for (IXMLTag lNodeTag : lNodeTags) {
						if (!hTagNames.containsKey(lNodeTag.getName())) {
							if (getScript().getNodeTagContentNames(lId, lNodeTag.getName()).size() > 0) {
								lAdd = true;
								break;
							}
							hTagNames.put(lNodeTag.getName(), lNodeTag.getName());
						}
					}
					if (lAdd) {
						lCaseComponents.add(lCac);
					}
				}
				else {
					if (lNodeTags.size() > 0) {
						lCaseComponents.add(lCac);
					}
				}
			}
			lCacs = lCaseComponents;
		}
		
		if (!lCaseRolesRef) {
			CCombo lCombo = renderScriptCac(lVbox, null, aClickedTag, aDefTag, aNodename, aReftype, lCacId, lCacs, aRefcomp, true, aPreselectIfOneItem);
			if (lCombo != null) {
				if (lCacId.equals("0")) {
					String lId = lCombo.getSelectedItemId();
					if (!(lId.equals("") || lId.equals("0"))) {
						lCacId = lId;
					}
				}
			}
		}

		boolean lShow = ((!lCacId.equals("0")) || (lCaseRolesRef));

		if (!lCaseRolesRef || aRefmultiple)
			renderScriptCars(lVbox, null, aClickedTag, aDefTag, aNodename, aReftype, lCacId, lCarIds, lShow, false, aPreselectIfOneItem);
		else
			renderScriptCars(lVbox, null, aClickedTag, aDefTag, aNodename, aReftype, lCacId, lCarIds, lShow, true, aPreselectIfOneItem);

		lShow = (!lCacId.equals("0"));
		if (!lCaseRolesRef) {
			String lTagName = getMethodTagName(aClickedTag);
			if (lCanChooseTagContent) {
				if ((lTagName.equals("0")) && (lTagIds.length > 0)) {
					IXMLTag lTag = CDesktopComponents.sSpring().getTag(lCacId, lTagIds[0]);
					if (lTag != null)
						lTagName = lTag.getName();
				}
				renderScriptTagName(lVbox, null, aClickedTag, aDefTag, aNodename, "",
						lCacId, "", lTagName, lShow);
				if (!lTagName.equals("0")) {
					//filter on tag name
					aReftag = lTagName;
				}
				lShow = (lShow && (!lTagName.equals("0")));
			}
			if (!aReftag.equals("")) {
				if (aRefmultiple) {
					renderScriptTags(lVbox, null, aClickedTag, aDefTag, aNodename, aReftype,
							lCacId, lTagIds, aReftag, lShow, aPreselectIfOneItem, false);
				}
				else {
					renderScriptTag(lVbox, null, aClickedTag, aDefTag, aNodename, aReftype,
							lCacId, lTagId, aReftag, lShow);
					if (lCanChooseTagContent) {
						String lTagContentName = "";
						if (aClickedTag != null) {
							lTagContentName = (String)aClickedTag.getAttribute(AppConstants.nodeChildKeyTagContentName);
						}
						renderScriptTagContentName(lVbox, null, aClickedTag, aDefTag, aNodename, aReftype,
								lCacId, lTagName, lTagContentName, lShow);
					}
				}
			}
		}
	}

	/**
	 * Xml rows to node. If input elements in rows are updated xmlNode is updated
	 * correspondingly.
	 * For condition, action or ref tag aRefTagIds is filled with references to other
	 * tags/components which have to be saved in the case case component xml data.
	 * For picture or blob tag aBlobTagDatas is filled with blob tag data
	 * to be handled later if everything is ok.
	 *
	 * @param aRows the a rows
	 * @param aParentTag the parent tag
	 * @param aNodeTag the node tag
	 * @param aNew the new
	 * @param aRefTagIds the a ref tag ids
	 * @param aBlobTagDatas the a blob tag datas
	 * @param aErrors the a errors, empty if no errors
	 *
	 * @return true, if xml node tag is updated
	 */
	public boolean xmlRowsToNode(Component aRows, IXMLTag aParentTag, IXMLTag aNodeTag, boolean aNew, Hashtable<String,List<String>> aRefTagIds,
			List<Hashtable<String,Object>> aBlobTagDatas, List<String[]> aErrors) {
		List<Hashtable<String,Object>> rows = (List)aRows.getAttribute("rows");
		//child tag attributes of aNodeTag to be set, if no errors. Only changed attributes
		Hashtable<String,String> lChildTagAttributesToBeSet = new Hashtable<String,String>(); 
		//child tag values of aNodeTag to be set, if no errors. Only changed values
		Hashtable<String,String> lChildTagValuesToBeSet = new Hashtable<String,String>(); 
		//child tag child tags of aNodeTag to be set, if no errors. Only changed child tags
		Hashtable<String,List<IXMLTag>> lChildTagChildTagsToBeSet = new Hashtable<String,List<IXMLTag>>();
		//child tag values of aNodeTag to be checked for errors. All values.
		Hashtable<String,String> lChildTagValuesToBeChecked = new Hashtable<String,String>();
		boolean lNodeChanged = false;
		for (Hashtable<String,Object> row : rows) {
			IXMLTag lDefTag = (IXMLTag) row.get("deftag");
			String lType = (String) row.get("type");
			String lName = (String) row.get("name");
			String lAttribute = (String) row.get("attribute");
			String lValue = (String) row.get("value");
			Component lInput = null;
			List<Component> lSubInputs = new ArrayList<Component>(0);
			// if attribute
			if (!lAttribute.equals("")) {
				lInput = aRows.getFellowIfAny(lAttribute);
			}
			else {
				if (lType.equals("blob")) {
					lInput = aRows.getFellowIfAny(lName);
					lDefTag = (IXMLTag) lInput.getAttribute("deftag");
					if (lDefTag != null) {
						String lBlobtypes = lDefTag.getAttribute(AppConstants.defKeyBlobtypes);
						if (!lBlobtypes.equals("")) {
							String[] lBlobtypearr = lBlobtypes.split(",");
							for (int i = 0; i < lBlobtypearr.length; i++) {
								Radio lRadio = (Radio)aRows.getFellowIfAny("blob_" + lBlobtypearr[i] + "_radio");
								if (lRadio.isChecked()) {
									Component lSubComp = aRows.getFellowIfAny(lRadio.getId() + "_comp");
									lSubInputs.add(lSubComp);
									String lChangedStr = (String)lRadio.getAttribute("changed");
									if (lChangedStr != null && lChangedStr.equals("true")) {
										// blob type change is sufficient to store as new item
										lInput.setAttribute("changed", "true");
									}
									else {
										lChangedStr = (String)lSubComp.getAttribute("changed");
										if (lChangedStr != null && lChangedStr.equals("true")) {
											// blob value has changed
											lInput.setAttribute("changed", "true");
										}
									}
								}
							}
						}
						String lMediatypes = lDefTag.getAttribute(AppConstants.defKeyMediatypes);
						if (!lMediatypes.equals("")) {
							String[] lMediatypearr = lMediatypes.split(",");
							for (int i = 0; i < lMediatypearr.length; i++) {
								Radio lRadio = (Radio)aRows.getFellowIfAny("blob_" + lMediatypearr[i] + "_radio");
								if (lRadio.isChecked()) {
									lSubInputs.add(lRadio);
									String lChangedStr = (String)lRadio.getAttribute("changed");
									if (lChangedStr != null && lChangedStr.equals("true")) {
										lInput.setAttribute("changed", "true");
									}
								}
							}
						}
					}
				}
				else {
					lInput = aRows.getFellowIfAny(lName);
				}
			}

			if (lInput != null) {
				boolean lChanged = false;
				String lChangedStr = (String)lInput.getAttribute("changed");
				if (lChangedStr != null && lChangedStr.equals("true")) {
					lChanged = true;
				}
				if (lChanged) {
					lNodeChanged = true;
				}
				boolean lHidden = (aNodeTag.getDefAttribute(AppConstants.defKeyHidden).equals(AppConstants.statusValueTrue));
				if (lChanged && !lHidden) {
					if (lType.equals("line") || lType.equals("text") || lType.equals("formula")) {
						lValue = ((Textbox)lInput).getValue().trim();
						if (lType.equals("line") && lName.equals("status")) {
							lValue = lValue.replace(",", AppConstants.statusCommaReplace);
						}
						lValue = CDesktopComponents.sSpring().escapeXML(lValue);
						if (lType.equals("formula")) {
							updateFormula(lValue, aRefTagIds, aErrors);
						}
					}
					else if (lType.equals("simplerichtext") || lType.equals("richtext")) {
						lValue = stripCKeditorString(((CDefCKeditor) lInput).getValue().trim());
						// value of CKeditor already is escaped but not if you use '<' or '>' as input,
						// then '<' for instance is represented by '&lt;'.
						// The next line replaces it by '&amp;lt;', otherwise the input is not correctly showed
						// the next time you edit the rich text.
						lValue = CDesktopComponents.sSpring().escapeXML(lValue);
					}
					else if (lType.equals("picture")) {
						Hashtable<String,Object> lHBlobTagData = new Hashtable<String,Object>(0);
						lHBlobTagData.put("childtag", getXmlNodeChild(aNodeTag,lName));
						lHBlobTagData.put("attribute", lAttribute);
						lHBlobTagData.put(AppConstants.keyBlobtype, AppConstants.blobtypeDatabase);
						lHBlobTagData.put("value", lValue);
						if (((Image) lInput).getContent() != null) {
							lHBlobTagData.put("content", ((Image)lInput).getContent());
						}
						aBlobTagDatas.add(lHBlobTagData);
					}
					else if (lType.equals("boolean")) {
						lValue = "" + ((Checkbox) lInput).isChecked();
					}
					else if (lType.equals("singleselect") || lType.equals("singleselectimage")) {
						Listitem lItem = ((Listbox) lInput).getSelectedItem();
						if (lItem != null) {
							lValue = (String)lItem.getValue();
						}
					}
					else if (lType.equals("blob") && (lSubInputs.size() > 0)) {
						String lBlobtype = "";
						String lMediatype = "";
						for (int k = 0; k < lSubInputs.size(); k++) {
							Component lSubInput = (Component) lSubInputs.get(k);
							String lBType = (String) lSubInput.getAttribute(AppConstants.keyBlobtype);
							String lMType = (String) lSubInput.getAttribute(AppConstants.keyMediatype);
							if (!StringUtils.isEmpty(lBType)) {
								lBlobtype = lBType;
								Hashtable<String,Object> lHBlobTagData = new Hashtable<String,Object>(0);
								lHBlobTagData.put("childtag", getXmlNodeChild(aNodeTag,lName));
								lHBlobTagData.put("attribute", lAttribute);
								lHBlobTagData.put(AppConstants.keyBlobtype, lBlobtype);
								lHBlobTagData.put("value", lValue);
								lHBlobTagData.put("fileName", ((InputElement)lSubInput).getText());
								if (lBType.equals(AppConstants.blobtypeDatabase)) {
									if ((Media)lSubInput.getAttribute("content") != null) {
										lHBlobTagData.put("content", (Media)lSubInput.getAttribute("content"));
									}
								}
								else if (lBType.equals(AppConstants.blobtypeIntUrl) ||
										lBType.equals(AppConstants.blobtypeExtUrl)) {
									lHBlobTagData.put("text", ((Textbox)lSubInput).getValue().trim());
								}
								aBlobTagDatas.add(lHBlobTagData);
							}
							if (lMType != null) {
								lMediatype = lMType;
							}
						}
						lChildTagAttributesToBeSet.put(lName + AppConstants.statusCommaReplace + AppConstants.keyBlobtype, lBlobtype);
						lChildTagAttributesToBeSet.put(lName + AppConstants.statusCommaReplace + AppConstants.keyMediatype, lMediatype);
					}
					else if (lType.equals("condition")) {
						List<IXMLTag> lChildTags = updateCondition(getXmlNodeChild(aNodeTag, lName), lInput, lDefTag, aRefTagIds, aErrors);
						if (aErrors.size() == 0) {
							lChildTagChildTagsToBeSet.put(lName, lChildTags);
						}
					}
					else if (lType.equals("action")) {
						List<IXMLTag> lChildTags = updateAction(getXmlNodeChild(aNodeTag, lName), lInput, lDefTag, aRefTagIds, aErrors);
						if (aErrors.size() == 0) {
							lChildTagChildTagsToBeSet.put(lName, lChildTags);
						}
					}
					else if (lType.equals("ref")) {
						String lSelectedTagContentNameId = updateRef(lInput, aNodeTag, lName, lDefTag, aRefTagIds, aErrors);
						if (aErrors.size() == 0 && !lSelectedTagContentNameId.equals("")) {
							lChildTagAttributesToBeSet.put(lName + AppConstants.statusCommaReplace + AppConstants.nodeChildKeyTagContentName, lSelectedTagContentNameId);
						}
					}
					// for picture and blob setting value/attribute is taken care of later
					// condition, action and ref take care of setting value/attribute
					if (!lType.equals("picture")
							&& !lType.equals("blob")
							&& !lType.equals("condition")
							&& !lType.equals("action")
							&& !lType.equals("ref")) {
						// if not attribute
						if (lAttribute.equals("")) {
							lChildTagValuesToBeSet.put(lName, lValue);
						}
						else {
							lChildTagAttributesToBeSet.put(lName + AppConstants.statusCommaReplace + lAttribute, lValue);
						}
					}
				}
			}
			// if not attribute
			if (lAttribute.equals("")) {
				lChildTagValuesToBeChecked.put(lName, lValue);
			}
		}
		if (aErrors.size() == 0) {
			List<String> lWarnings = new ArrayList<String>(0);
			CDesktopComponents.sSpring().getXmlManager().checkChildTagValues(
					aNodeTag.getDefAttribute(AppConstants.defKeyKey), 
					aNodeTag.getAttribute(AppConstants.keyId), 
					aNodeTag.getName(), 
					lChildTagValuesToBeChecked, 
					aNodeTag.getDefTag(), 
					aParentTag, 
					aNew, 
					false, 
					aErrors, 
					lWarnings);
		}
		if (aErrors.size() == 0) {
			for (Enumeration<String> lIds = lChildTagAttributesToBeSet.keys(); lIds.hasMoreElements();) {
				String lId = lIds.nextElement();
				String lValue = lChildTagAttributesToBeSet.get(lId);
				String[] lIdArr = lId.split(AppConstants.statusCommaReplace);
				String lName = lIdArr[0];
				String lAttribute = lIdArr[1];
				setChildAttribute(getXmlNodeChild(aNodeTag, lName), lAttribute, lValue);
			}
			for (Enumeration<String> lNames = lChildTagValuesToBeSet.keys(); lNames.hasMoreElements();) {
				String lName = lNames.nextElement();
				String lValue = lChildTagValuesToBeSet.get(lName);
				getXmlNodeChild(aNodeTag,lName).setValue(lValue);
			}
			for (Enumeration<String> lNames = lChildTagChildTagsToBeSet.keys(); lNames.hasMoreElements();) {
				String lName = lNames.nextElement();
				List<IXMLTag> lChildTags = lChildTagChildTagsToBeSet.get(lName);
				getXmlNodeChild(aNodeTag, lName).setChildTags(lChildTags);
			}
		}
		return lNodeChanged;
	}

	/**
	 * Gets the xml node child.
	 *
	 * @param xmlNode the xml node
	 * @param aChildName the a child name
	 *
	 * @return the xml node child
	 */
	public IXMLTag getXmlNodeChild(IXMLTag xmlNode, String aChildName) {
//		if child does not exist add it. xml definition can be extended in which case child must be added in data
		IXMLTag lChild = xmlNode.getChild(aChildName);
		if (lChild == null) {
			IXMLTag lDefChild = xmlNode.getDefTag().getChild(aChildName);
			if (lDefChild == null)
				return null;
			lChild = lDefChild.cloneWithoutParentAndChildren();
			lChild.setDefTag(lDefChild);
			lChild.setParentTag(xmlNode);
			xmlNode.getChildTags().add(lChild);
		}
		return lChild;
	}

	/**
	 * Handles blob tag datas. This data is created before in xmlRowsToNode.
	 * Blobs are created, updated or deleted according to the data.
	 *
	 * @param aBlobTagDatas the a blob tag datas
	 */
	public void handleBlobTagDatas(List<Hashtable<String,Object>> aBlobTagDatas) {
		for (Hashtable<String,Object> lHBlobTagData : aBlobTagDatas) {
			IXMLTag lChildTag = (IXMLTag) lHBlobTagData.get("childtag");
			String lAttribute = (String) lHBlobTagData.get("attribute");
			String lBlobtype = (String) lHBlobTagData.get(AppConstants.keyBlobtype);
			String lFileName = (String)lHBlobTagData.get("fileName");
			//NOTE lOldValue is the old value of the blob
			String lOldValue = lChildTag.getValue();
			String lNewValue = lOldValue;
			if (lBlobtype.equals(AppConstants.blobtypeDatabase)) {
				Media lContent = null;
				if (lHBlobTagData.containsKey("content")) {
					lContent = (Media) lHBlobTagData.get("content");
				}
				if (lContent == null) {
					if (StringUtils.isEmpty(lFileName)) {
						// if file name is empty the blob is deleted
						deleteBlob(lOldValue);
						lNewValue = "";
					}
				} else {
					// if lContent not null replace blob, lValue gets new value
					lNewValue = setBlobMedia(lOldValue, lContent);
				}
			} else { // int_url or ext_url
				String lText = (String) lHBlobTagData.get("text");
				lNewValue = setBlobUrl(lOldValue, lText);
			}
			if (!lNewValue.equals(lOldValue)) {
				if (lAttribute.equals("")) {
					lChildTag.setValue(lNewValue);
				}
				else {
					lChildTag.setAttribute(lAttribute, lNewValue);
				}
			}
		}
	}

	/**
	 * Creates new sibling xml node tag aItem within xml data of current case component.
	 *
	 * @param aItem the a item
	 * @param aBeforenode the a beforenode
	 * @param aSaveInDb the a save in db
	 *
	 * @return the error list, empty if ok
	 */
	public List<String[]> newSiblingNode(IXMLTag aItem, IXMLTag aBeforenode, boolean aSaveInDb) {
		List<String[]> lErrors = new ArrayList<String[]>(0);
		IXMLTag lRootTag = getXmlDataTree();
		CDesktopComponents.sSpring().getXmlManager().newSiblingNode(lRootTag, aItem, aBeforenode, lErrors);
		if (lErrors.size() == 0) {
			if (aSaveInDb)
				setXmlData(CDesktopComponents.sSpring().getXmlManager().xmlTreeToDoc(lRootTag));
		}
		return lErrors;
	}

	/**
	 * Creates new child xml node tag aItem within xml data of current case component.
	 *
	 * @param aItem the a item
	 * @param aParentnode the a parentnode
	 * @param aSaveInDb the a save in db
	 *
	 * @return the error list, empty if ok
	 */
	public List<String[]> newChildNode(IXMLTag aItem, IXMLTag aParentnode, boolean aSaveInDb) {
		List<String[]> lErrors = new ArrayList<String[]>(0);
		IXMLTag lRootTag = getXmlDataTree();
		CDesktopComponents.sSpring().getXmlManager().newChildNode(lRootTag, aItem, aParentnode, lErrors);
		if (lErrors.size() == 0) {
			if (aSaveInDb)
				setXmlData(CDesktopComponents.sSpring().getXmlManager().xmlTreeToDoc(lRootTag));
		}
		return lErrors;
	}

	/**
	 * Updates existing xml node tag aItem within xml data of current case component.
	 *
	 * @param aItem the a item
	 * @param aSaveInDb the a save in db
	 *
	 * @return the error list, empty if ok
	 */
	public List<String[]> updateNode(IXMLTag aItem, boolean aSaveInDb) {
		List<String[]> lErrors = new ArrayList<String[]>(0);
		IXMLTag lRootTag = getXmlDataTree();
//		lRootTag = CDesktopComponents.sSpring().getXmlManager().copyTag(lRootTag, null);
		CDesktopComponents.sSpring().getXmlManager().replaceNode(lRootTag, aItem, lErrors);
		if (lErrors.size() == 0) {
			if (aSaveInDb)
				setXmlData(CDesktopComponents.sSpring().getXmlManager().xmlTreeToDoc(lRootTag));
		}
//		appManager.addError(lErrors, aItem.getName(), "error_empty");
		return lErrors;
	}

	/**
	 * Updates condition using childs of input element aComponent for condition and
	 * aRefTagIds, references to other tags/components.
	 *
	 * @param aConditionStringTag the a condition string tag, parent of all condition sub tags
	 * @param aComponent the a component, the input element for the condition
	 * @param aDefTag the definition tag
	 * @param aRefTagIds the a ref tag ids, references to other tags/components
	 * @param aErrors the a errors, empty if no errors
	 *
	 * @return new child tags of aConditionStringTag, if successful
	 */
	protected List<IXMLTag> updateCondition(IXMLTag aConditionStringTag,
			Component aComponent, IXMLTag aDefTag, Hashtable<String,List<String>> aRefTagIds, List<String[]> aErrors) {
		if (aConditionStringTag == null || aComponent == null)
			return null;
		initRefTagsIds(aRefTagIds);

		boolean lError = false;
		List<IXMLTag> lChildTags = new ArrayList<IXMLTag>(0);
		List<Component> lChilds = aComponent.getChildren();
		String lJava = "";
		// create new tag structure for conditionstring, new Java code and new
		// reftagids
		if (lChilds != null) {
			for (Component lChild : lChilds) {
				IXMLTag lChildTag = (IXMLTag) lChild.getAttribute("tag");
				if (lChildTag != null) {
					lChildTags.add(lChildTag);
					lJava = lJava + scriptLabelTagNameToJava(lChildTag);
					String lName = lChildTag.getName();
					if (isMethodUsedInCondition(lName)) {
						if (!isTemplateMethod(lName)) {
							List<String> lCarIds = getMethodCarIdList(lChildTag);
							String lCacId = getMethodCacId(lChildTag);
							String lStatusId = getMethodStatusId(lChildTag);
							String lTempStatusId = lStatusId;
							if (lTempStatusId.equals(""))
								lTempStatusId = "0";
							String[] lData = updateRefTagIdsAndGetData(lChildTag, lCarIds, lTempStatusId, aRefTagIds);
							String lTagIds = lData[0]; 
							boolean lTagTemplatesEmpty = lData[1].equals(AppConstants.statusValueTrue);
							String lFunctionId = getMethodFunctionId(lChildTag);
							String lValue = getMethodValue(lChildTag);
							String lOperatorId = getMethodOperatorId(lChildTag);
							String lOperatorValues = getMethodOperatorValues(lChildTag);
							boolean lStringValueInput = appManager.getTagOperatorValueType("", lCacId, lChildTag.getChildValue("tagname"), lStatusId, "0").equals("string");
							lError = (lCarIds.size() == 0
									|| lCacId.equals("") || lCacId.equals("0")
									|| lStatusId.equals("") || lStatusId.equals("0")
									|| (lFunctionId.equals("1") && lValue.equals(""))
									|| lOperatorId.equals("") || lOperatorId.equals("0")
									|| (lOperatorValues.equals("") && !lStringValueInput)
									|| (isMethodUsingTags(lName) && (lTagIds.equals("") || lTagIds.equals("0")) && lTagTemplatesEmpty));
						}
						else {
							String lComId = getMethodComId(lChildTag);
							String lStatusId = getMethodStatusId(lChildTag);
							String lTempStatusId = lStatusId;
							if (lTempStatusId.equals(""))
								lTempStatusId = "0";
							String[] lData = updateTemplateRefTagIdsAndGetData(lChildTag, lTempStatusId, aRefTagIds);
							boolean lComponentTemplatesEmpty = lData[0].equals(AppConstants.statusValueTrue);
							boolean lTagTemplatesEmpty = lData[1].equals(AppConstants.statusValueTrue);
							String lFunctionId = getMethodFunctionId(lChildTag);
							String lValue = getMethodValue(lChildTag);
							String lOperatorId = getMethodOperatorId(lChildTag);
							String lOperatorValues = getMethodOperatorValues(lChildTag);
							boolean lStringValueInput = appManager.getTagOperatorValueType(lComId, "", lChildTag.getChildValue("tagname"), lStatusId, "0").equals("string");
							lError = (lComId.equals("") || lComId.equals("0")
									|| lComponentTemplatesEmpty
									|| lStatusId.equals("") || lStatusId.equals("0")
									|| (lFunctionId.equals("1") && lValue.equals(""))
									|| lOperatorId.equals("") || lOperatorId.equals("0")
									|| (lOperatorValues.equals("") && !lStringValueInput)
									|| (isMethodUsingTags(lName) && lTagTemplatesEmpty));
						}
					}
				}
			}
		}
		// evaluate new Java code
		if (lError || (!lJava.equals("") && !isScriptValid(lJava)))
			appManager.addError(aErrors,
					"condition", "error_no_valid_script");

		if (aErrors.size() == 0) {
			return lChildTags;
		}
		return null;
	}

	/**
	 * Updates action using childs of input element aComponent for action and
	 * aRefTagIds, references to other tags/components.
	 *
	 * @param aActionStringTag the a action string tag, parent of all action sub tags
	 * @param aComponent the a component, the input element for the action
	 * @param aDefTag the definition tag
	 * @param aRefTagIds the a ref tag ids, references to other tags/components
	 * @param aErrors the a errors, empty if no errors
	 *
	 * @return new child tags of aActionStringTag, if successful
	 */
	protected List<IXMLTag> updateAction(IXMLTag aActionStringTag,
			Component aComponent, IXMLTag aDefTag, Hashtable<String,List<String>> aRefTagIds, List<String[]> aErrors) {
		if ((aActionStringTag == null) || (aComponent == null))
			return null;
		initRefTagsIds(aRefTagIds);

		boolean lError = false;
		List<IXMLTag> lChildTags = new ArrayList<IXMLTag>(0);
		List<Component> lChilds = aComponent.getChildren();
		String lJava = "";
		// create new tag structure for actionstring, new Java code and new
		// reftagids
		if (lChilds != null) {
			for (Component lChild : lChilds) {
				IXMLTag lChildTag = (IXMLTag) lChild.getAttribute("tag");
				if (lChildTag != null) {
					lChildTags.add(lChildTag);
					lJava = lJava + scriptLabelTagNameToJava(lChildTag);
					String lName = lChildTag.getName();
					if (isMethodUsedInAction(lName)) {
						if (!isTemplateMethod(lName)) {
							List<String> lCarIds = getMethodCarIdList(lChildTag);
							String lCacId = getMethodCacId(lChildTag);
							String lStatusId = getMethodStatusId(lChildTag);
							String lTempStatusId = lStatusId;
							if (lTempStatusId.equals(""))
								lTempStatusId = "0";
							String[] lData = updateRefTagIdsAndGetData(lChildTag, lCarIds, lTempStatusId, aRefTagIds);
							String lTagIds = lData[0]; 
							Boolean lTagTemplatesEmpty = lData[1].equals(AppConstants.statusValueTrue);
							String lOperatorValues = getMethodOperatorValues(lChildTag);
							boolean lStringValueInput = appManager.getTagOperatorValueType("", lCacId, lChildTag.getChildValue("tagname"), lStatusId, "0").equals("string");
							lError = ((lCarIds.size() == 0)
									|| lCacId.equals("") || lCacId.equals("0")
									|| lStatusId.equals("") || lStatusId.equals("0")
									|| (lOperatorValues.equals("") && !lStringValueInput)
									|| (isMethodUsingTags(lName) && (lTagIds.equals("") || lTagIds.equals("0")) && lTagTemplatesEmpty));
						}
						else {
							String lComId = getMethodComId(lChildTag);
							String lStatusId = getMethodStatusId(lChildTag);
							String lTempStatusId = lStatusId;
							if (lTempStatusId.equals(""))
								lTempStatusId = "0";
							String[] lData = updateTemplateRefTagIdsAndGetData(lChildTag, lTempStatusId, aRefTagIds);
							boolean lComponentTemplatesEmpty = lData[0].equals(AppConstants.statusValueTrue);
							boolean lTagTemplatesEmpty = lData[1].equals(AppConstants.statusValueTrue);
							String lOperatorValues = getMethodOperatorValues(lChildTag);
							boolean lStringValueInput = appManager.getTagOperatorValueType(lComId, "", lChildTag.getChildValue("tagname"), lStatusId, "0").equals("string");
							lError = (lComId.equals("") || lComId.equals("0")
									|| lComponentTemplatesEmpty
									|| lStatusId.equals("") || lStatusId.equals("0")
									|| (lOperatorValues.equals("") && !lStringValueInput)
									|| (isMethodUsingTags(lName) && lTagTemplatesEmpty));
						}
					}
				}
			}
		}
		// evaluate new Java code
		if (lError || (!lJava.equals("") && !isScriptValid(lJava)))
			appManager.addError(aErrors,
					"action", "error_no_valid_script");

		if (aErrors.size() == 0) {
			return lChildTags;
		}
		return null;
	}

	/**
	 * Gets template condition ref tag ids.
	 *
	 * @param aConditionTag the a condition tag
	 *
	 * @return template condition ref tag ids
	 */
	public Hashtable<String,List<String>> getTemplateConditionRefTagIds(IXMLTag aConditionTag) {
		Hashtable<String,List<String>> lRefTagIds = new Hashtable<String,List<String>>(); 
		if (aConditionTag == null)
			return lRefTagIds;
		IXMLTag lConditionStringTag = aConditionTag.getChild("conditionstring");
		if (lConditionStringTag == null)
			return lRefTagIds;
		initRefTagsIds(lRefTagIds);

		for (IXMLTag lChildTag : lConditionStringTag.getChildTags()) {
			String lName = lChildTag.getName();
			if (isMethodUsedInCondition(lName)) {
				if (isTemplateMethod(lName)) {
					String lStatusId = getMethodStatusId(lChildTag);
					String lTempStatusId = lStatusId;
					if (lTempStatusId.equals(""))
						lTempStatusId = "0";
					updateTemplateRefTagIdsAndGetData(lChildTag, lTempStatusId, lRefTagIds);
				}
			}
		}
		return lRefTagIds;
	}

	/**
	 * Gets template action ref tag ids.
	 *
	 * @param aActionTag the a action tag
	 *
	 * @return template action ref tag ids
	 */
	public Hashtable<String,List<String>> getTemplateActionRefTagIds(IXMLTag aActionTag) {
		Hashtable<String,List<String>> lRefTagIds = new Hashtable<String,List<String>>(); 
		if ((aActionTag == null))
			return lRefTagIds;
		IXMLTag lActionStringTag = aActionTag.getChild("actionstring");
		if (lActionStringTag == null)
			return lRefTagIds;
		initRefTagsIds(lRefTagIds);

		for (IXMLTag lChildTag : lActionStringTag.getChildTags()) {
			String lName = lChildTag.getName();
			if (isMethodUsedInAction(lName)) {
				if (isTemplateMethod(lName)) {
					String lStatusId = getMethodStatusId(lChildTag);
					String lTempStatusId = lStatusId;
					if (lTempStatusId.equals(""))
						lTempStatusId = "0";
					updateTemplateRefTagIdsAndGetData(lChildTag, lTempStatusId, lRefTagIds);
				}
			}
		}
		return lRefTagIds;
	}

	/**
	 * Inits aRefTagIds by adding entries used for (template)script tags.
	 *
	 * @param aRefTagIds the a ref tag ids, references to other tags/components
	 */
	protected void initRefTagsIds(Hashtable<String,List<String>> aRefTagIds) {
		if (!aRefTagIds.containsKey(AppConstants.defValueReftypeAllScriptTags)) {
			List<String> lRefTagIds = new ArrayList<String>(0);
			aRefTagIds.put(AppConstants.defValueReftypeAllScriptTags, lRefTagIds);
		}
		if (!aRefTagIds.containsKey(AppConstants.defValueReftypeAllTemplateScriptTags)) {
			List<String> lRefTagIds = new ArrayList<String>(0);
			aRefTagIds.put(AppConstants.defValueReftypeAllTemplateScriptTags, lRefTagIds);
		}
	}

	/**
	 * Updates aRefTagIds and returns tag ids and if tag template is empty.
	 *
	 * @param aMethodTag the method tag
	 * @param aCarIds the car ids
	 * @param aStatusId the status id
	 * @param aRefTagIds the a ref tag ids, references to other tags/components
	 *
	 * @return tag ids and if tag template is empty
	 */
	protected String[] updateRefTagIdsAndGetData(IXMLTag aMethodTag, List<String> aCarIds, String aStatusId,
			Hashtable<String,List<String>> aRefTagIds) {
		IXMLTag lCacIdTag = aMethodTag.getChild("cacid");
		if (lCacIdTag == null)
			return new String[]{"", "true"};
		String lCacId = lCacIdTag.getValue();
		String lTagIds = "";
		List<IXMLTag> lTagIdTags = lCacIdTag.getChilds("tagid");
		boolean lTagIdsEmpty = (lTagIdTags == null || lTagIdTags.size() == 0);
		if (!lTagIdsEmpty) {
			for (String lCarId : aCarIds) {
				for (IXMLTag lTagIdTag : lTagIdTags) {
					String lTagId = lTagIdTag.getValue();
					if (!lTagIds.equals("")) {
						lTagIds = lTagIds + ",";
					}
					lTagIds = lTagIds + lTagId;
					aRefTagIds.get(AppConstants.defValueReftypeAllScriptTags).add(aStatusId + ","
							+ lCarId + "," + lCacId + ","
							+ lTagId);
				}
			}
		}
		List<IXMLTag> lTagTemplateTags = lCacIdTag.getChilds("tagtemplate");
		boolean lTagTemplatesEmpty = (lTagTemplateTags == null || lTagTemplateTags.size() == 0);
		// NOTE add also casecase reference for templates, for else condition will not be checked if tagid tags are empty
		// use '-1' as tag id
		if (!lTagTemplatesEmpty) {
			for (String lCarId : aCarIds) {
				aRefTagIds.get(AppConstants.defValueReftypeAllScriptTags).add(aStatusId + "," + lCarId
						+ "," + lCacId + "," + "-1");
			}
		}
		//casecomponent referenced; not possible to add templates for casecomponent:
		if (lTagIdsEmpty && lTagTemplatesEmpty)
			for (String lCarId : aCarIds)
				aRefTagIds.get(AppConstants.defValueReftypeAllScriptTags).add(aStatusId + "," + lCarId
						+ "," + lCacId + "," + "0");
		return new String[]{lTagIds, "" + lTagTemplatesEmpty};
	}

	/**
	 * Updates aRefTagIds for template and returns if component template and if tag template are empty.
	 *
	 * @param aMethodTag the method tag
	 * @param aStatusId the status id
	 * @param aRefTagIds the a ref tag ids, references to other tags/components
	 *
	 * @return tag ids and if tag template is empty
	 */
	protected String[] updateTemplateRefTagIdsAndGetData(IXMLTag aMethodTag, String aStatusId,
			Hashtable<String,List<String>> aRefTagIds) {
		IXMLTag lComIdTag = aMethodTag.getChild("comid");
		if (lComIdTag == null)
			return new String[]{"true", "true"};
		String lComId = lComIdTag.getValue();
		List<IXMLTag> lTagTemplateTags = lComIdTag.getChilds("tagtemplate");
		boolean lTagTemplatesEmpty = (lTagTemplateTags == null || lTagTemplateTags.size() == 0);
		if (!lTagTemplatesEmpty) {
			// component and tag referenced
			// NOTE use '-1' as indicator
			// add '0' to get same structure as for non templates
			aRefTagIds.get(AppConstants.defValueReftypeAllTemplateScriptTags).add(aStatusId + "," + lComId
					+ "," + "-1");
		}
		List<IXMLTag> lComponentTemplateTags = lComIdTag.getChilds("componenttemplate");
		boolean lComponentTemplatesEmpty = (lComponentTemplateTags == null || lComponentTemplateTags.size() == 0);
		if (!lComponentTemplatesEmpty && lTagTemplatesEmpty) {
			// component referenced
			// NOTE use '0' as indicator
			// add '0' to get same structure as for non templates
			aRefTagIds.get(AppConstants.defValueReftypeAllTemplateScriptTags).add(aStatusId + "," + lComId
					+ "," + "0");
		}
		return new String[]{"" + lComponentTemplatesEmpty, "" + lTagTemplatesEmpty};
	}

	/**
	 * Is script valid. Checks if aJava is valid Java code.
	 *
	 * @param aJava the a java
	 *
	 * @return true, if successful
	 */
	private boolean isScriptValid(String aJava) {
		String lScriptComponent = CControl.scriptComponent;
		String lJava = new String(aJava);
		Interpreter bsh = new Interpreter();
		try {
			bsh.set(lScriptComponent, CDesktopComponents.cScript());
			bsh.eval("lBoolean = new Boolean(" + lJava + ");");
		} catch (EvalError ee) {
			return false;
		}
		return true;
	}

	/**
	 * Script label tag name to java. Converts name of aScriptLabelTag to valid Java.
	 *
	 * @param aScriptLabelTag the a script label tag
	 *
	 * @return the string
	 */
	private String scriptLabelTagNameToJava(IXMLTag aScriptLabelTag) {
		String lName = aScriptLabelTag.getName();
		if (lName.equals("parenthesisopen"))
			return "(";
		else if (lName.equals("parenthesisclose"))
			return ")";
		else if (lName.equals("and"))
			return "&&";
		else if (lName.equals("or"))
			return "||";
		else if (lName.equals("not"))
			return "!";
		else if (lName.equals("evalrungrouptagstatus")
				|| lName.equals("evalrungroupcomponentstatus")
				|| lName.equals("setrungrouptagstatus")
				|| lName.equals("setrungroupcomponentstatus"))
			return "false";
	
		else if (lName.equals("evalrungrouptagtemplate")
				|| lName.equals("evalrungroupcomponenttemplate")
				|| lName.equals("setrungrouptagtemplate")
				|| lName.equals("setrungroupcomponenttemplate"))
			return "false";
		return "";
	}

	/**
	 * Updates ref using childs of input element aComponent and
	 * aRefTagIds, references to other tags/components.
	 *
	 * @param aComponent the a component, the input element for the ref
	 * @param aNodeTag the node tag
	 * @param aNodeName the a node name
	 * @param aDefTag the definition tag
	 * @param aRefTagIds the a ref tag ids, references to other tags/components
	 * @param aErrors the a errors, empty if no errors
	 *
	 * @return selected tag content name id, if successful and unequal to 0
	 */
	protected String updateRef(Component aComponent, IXMLTag aNodeTag, String aRefTagName, IXMLTag aDefTag, Hashtable<String,List<String>> aRefTagIds, List<String[]> aErrors) {
		if (aComponent == null)
			return "";

		String lRefType = "";
		if (aDefTag != null)
			lRefType = aDefTag.getAttribute(AppConstants.defKeyReftype);
		if (lRefType.equals(""))
			return "";
		if (!aRefTagIds.containsKey(lRefType)) {
			List<String> lRefTagIds = new ArrayList<String>(0);
			aRefTagIds.put(lRefType, lRefTagIds);
		}
		boolean lChoiceMayBeEmpty = aDefTag.getAttribute(AppConstants.defKeyChoiceempty).equals(AppConstants.statusValueTrue);
		boolean lTagChoiceMayBeEmpty = aDefTag.getAttribute(AppConstants.defKeyTagChoiceempty).equals(AppConstants.statusValueTrue);

		CCombo lCacCombo = (CCombo) aComponent.getFellowIfAny(aRefTagName+"scriptCacId");
		CListbox lCarListbox = (CListbox) aComponent.getFellowIfAny(aRefTagName+"scriptCarIds");
		// single or multiple select tag: combo is used for single, listbox for multiple
		CCombo lTagCombo = (CCombo) aComponent.getFellowIfAny(aRefTagName+"scriptTagId");
		CListbox lTagListbox = (CListbox) aComponent.getFellowIfAny(aRefTagName+"scriptTagIds");
		if (lCacCombo == null && lCarListbox != null) {
			// if no case components chosen update references to case roles only
			updateRefCaseRolesRef(aComponent, aRefTagName, aDefTag, aRefTagIds, aErrors);
			return "";
		}
		if (lCacCombo == null || lCarListbox == null) {
			if (!lChoiceMayBeEmpty)
				appManager.addError(aErrors, "refs", "error_no_choice");
			return "";
		}
		CScriptTagContentCombo lTagContentNameCombo = (CScriptTagContentCombo) aComponent.getFellowIfAny(aRefTagName+"scriptTagContentName");

		Listitem lSelectedCacItem = null;
		if (lCacCombo != null)
			lSelectedCacItem = lCacCombo.getSelectedItem();
		Set<Listitem> lSelectedCarItems = lCarListbox.getSelectedItems();
		Listitem lSelectedTagItem = null;
		Set<Listitem> lSelectedTagItems = null;
		if (lTagCombo != null)
			lSelectedTagItem = lTagCombo.getSelectedItem();
		if (lTagListbox != null)
			lSelectedTagItems = lTagListbox.getSelectedItems();
		Listitem lSelectedTagContentNameItem = null;
		if (lTagContentNameCombo != null)
			lSelectedTagContentNameItem = ((Listbox) lTagContentNameCombo).getSelectedItem();
		boolean lNoCacChosen = lSelectedCacItem == null;
		boolean lNoCarsChosen = lSelectedCarItems == null || lSelectedCarItems.size() == 0;
		boolean lNoTagChosen = !lTagChoiceMayBeEmpty && lSelectedTagItem == null && lTagCombo != null;
		boolean lNoTagsChosen = !lTagChoiceMayBeEmpty && (lSelectedTagItems == null || lSelectedTagItems.size() == 0) && lTagListbox != null;
		boolean lNoTagContentChosen = lSelectedTagContentNameItem == null && lTagContentNameCombo != null;
		if (lNoCacChosen || lNoCarsChosen || lNoTagChosen || lNoTagsChosen || lNoTagContentChosen) {
			if (!lChoiceMayBeEmpty)
				appManager.addError(aErrors, "refs", "error_no_choice");
			return "";
		}

		String lSelectedCacId = "0";
		if (lSelectedCacItem != null)
			lSelectedCacId = (String) lSelectedCacItem.getValue();
		String[] lSelectedCarIds = null;
		if (lSelectedCarItems != null && lSelectedCarItems.size() > 0) {
			lSelectedCarIds = new String[lSelectedCarItems.size()];
			int lCounter = 0;
			for (Listitem lItem : lSelectedCarItems) {
				lSelectedCarIds[lCounter] = (String) lItem.getValue();
				lCounter = lCounter + 1;
			}
		}
		String lSelectedTagId = "0";
		if (lSelectedTagItem != null)
			lSelectedTagId = (String) lSelectedTagItem.getValue();
		String[] lSelectedTagIds = null;
		if (lSelectedTagItems != null && lSelectedTagItems.size() > 0) {
			lSelectedTagIds = new String[lSelectedTagItems.size()];
			int lCounter = 0;
			for (Listitem lItem : lSelectedTagItems) {
				lSelectedTagIds[lCounter] = (String) lItem.getValue();
				lCounter = lCounter + 1;
			}
		}
		// default id 0 corresponds to none
		String lSelectedTagContentNameId = "0";
		if (lSelectedTagContentNameItem != null)
			lSelectedTagContentNameId = (String) lSelectedTagContentNameItem.getValue();

		lNoCacChosen = lSelectedCacId.equals("0");
		lNoCarsChosen = lSelectedCarIds == null;
		lNoTagChosen = !lTagChoiceMayBeEmpty && lSelectedTagId.equals("0") && lTagCombo != null;
		lNoTagsChosen = !lTagChoiceMayBeEmpty && lSelectedTagIds == null && lTagListbox != null;
		lNoTagContentChosen = lSelectedTagContentNameId.equals("0") && lTagContentNameCombo != null;
		if (lNoCacChosen || lNoCarsChosen || lNoTagChosen || lNoTagsChosen || lNoTagContentChosen) {
			if (!lChoiceMayBeEmpty)
				appManager.addError(aErrors, "refs", "error_no_choice");
			return "";
		}

		for (int i = 0; i < lSelectedCarIds.length; i++) {
			if (!lSelectedTagId.equals("0")) {
				String lIds = lSelectedCarIds[i] + "," + lSelectedCacId + "," + lSelectedTagId;
				aRefTagIds.get(lRefType).add("" + AppConstants.statusKeySelectedIndex + "," + lIds);
			}
			else {
				if (lSelectedTagIds != null) {
					for (int j = 0; j < lSelectedTagIds.length; j++) {
						String lIds = lSelectedCarIds[i] + "," + lSelectedCacId + "," + lSelectedTagIds[j];
						aRefTagIds.get(lRefType).add("" + AppConstants.statusKeySelectedIndex + "," + lIds);
					}
				}
				else {
					String lIds = lSelectedCarIds[i] + "," + lSelectedCacId + ",0";
					aRefTagIds.get(lRefType).add("" + AppConstants.statusKeySelectedIndex + "," + lIds);
				}
			}
		}
		if (!lSelectedTagContentNameId.equals("0")) {
			IXMLTag lRefTag = getXmlNodeChild(aNodeTag, aRefTagName);
			if (lRefTag != null) {
				lRefTag.setAttribute(AppConstants.nodeChildKeyTagContentName, lSelectedTagContentNameId);
			}
		}

		// if no errors do nothing, data is saved in case casecomponent
		if (aErrors.size() == 0 && !lSelectedTagContentNameId.equals("0"))
			return lSelectedTagContentNameId;
		return "";
	}

	/**
	 * Updates formula using references to number or time states within formula string and
	 * aRefTagIds, references to other tags/components.
	 * And checks number and time state keys within a string.
	 *
	 * @param aFormulaText the a formula text
	 * @param aRefTagIds the a ref tag ids, references to other tags/components
	 * @param aErrors the a errors, empty if no errors
	 */
	protected void updateFormula(String aFormulaText, Hashtable<String,List<String>> aRefTagIds, List<String[]> aErrors) {
		//find first state key
		int lStart = aFormulaText.indexOf(AppConstants.stringVarPrefix);
		int lEnd = aFormulaText.indexOf(AppConstants.stringVarPostfix);
		if (!(lStart >= 0 && lEnd > lStart)) {
			// no state keys within string
			return;
		}
		//get state components and their state tags
		Hashtable<Integer, List<IXMLTag>> lStateTagsPerCacId = new Hashtable<Integer, List<IXMLTag>>();
		Hashtable<Integer, List<IECaseRole>> lCaseRolesPerCacId = new Hashtable<Integer, List<IECaseRole>>();
		List<IECaseComponent> lCaseComponents = CDesktopComponents.sSpring().getCaseComponentsByComponentCode(CDesktopComponents.sSpring().getCase(), "states");
		for (IECaseComponent lCaseComponent : lCaseComponents) {
			IXMLTag lRootTag = CDesktopComponents.sSpring().getXmlDataTree(lCaseComponent);
			lStateTagsPerCacId.put(lCaseComponent.getCacId(), CDesktopComponents.cScript().getNodeTags(lRootTag));
			lCaseRolesPerCacId.put(lCaseComponent.getCacId(), CDesktopComponents.sSpring().getSCaseRoleHelper().getCaseRolesByCacId("" + lCaseComponent.getCacId()));
		}
		
		// for all actions this is the ref type
		String lRefTypeState = "statetag_formulatag";
		String lRefTypeFormula = "formulatag_formulatag";
		if (!aRefTagIds.containsKey(lRefTypeState)) {
			List<String> lRefTagIds = new ArrayList<String>(0);
			aRefTagIds.put(lRefTypeState, lRefTagIds);
		}
		if (!aRefTagIds.containsKey(lRefTypeFormula)) {
			List<String> lRefTagIds = new ArrayList<String>(0);
			aRefTagIds.put(lRefTypeFormula, lRefTagIds);
		}

		while (lStart >= 0 && lEnd > lStart) {
			String lStateKey = aFormulaText.substring(lStart + AppConstants.stringVarPrefix.length(), lEnd);
			//find state value within state tags
			String lStateValue = "1";
			boolean lFound = false;
			for (Enumeration<Integer> lCacIds = lStateTagsPerCacId.keys(); lCacIds.hasMoreElements();) {
				Integer lCacId = lCacIds.nextElement();
				List<IXMLTag> lStateTagsForCac = lStateTagsPerCacId.get(lCacId);
				for (IXMLTag lStateTag : lStateTagsForCac) {
					if (lStateTag.getChildValue(AppConstants.defKeyKey).equals(lStateKey)) {
						boolean lAddReftagid = false;
						String lRefType = "";
						if (lStateTag.getName().equals("state")) {
							lFound = true;
							if (!(lStateTag.getChildValue("keytype").equals("number") || lStateTag.getChildValue("keytype").equals("time"))) {
								appManager.addError(aErrors, "formula", "no_number_or_time_state");
							}
							else {
								lAddReftagid = true;
								lRefType = lRefTypeState;
							}
						}
						else if (lStateTag.getName().equals("formula")) {
							lFound = true;
							lAddReftagid = true;
							lRefType = lRefTypeFormula;
						}
						if (lAddReftagid) {
							//add reftag ids
							for (IECaseRole lCaseRole : lCaseRolesPerCacId.get(lCacId)) {
								String lIds = "" + lCaseRole.getCarId() + "," + lCacId + "," + lStateTag.getAttribute(AppConstants.keyId);
								aRefTagIds.get(lRefType).add("" + AppConstants.statusKeySelectedIndex + "," + lIds);
							}
						}
						if (lFound) {
							break;
						}
					}
				}
			}
			if (!lFound) {
				appManager.addError(aErrors, "formula", "state_not_found");
			}
			aFormulaText = aFormulaText.substring(0, lStart) +
				lStateValue +
				aFormulaText.substring(lEnd + AppConstants.stringVarPostfix.length());
			lStart = aFormulaText.indexOf(AppConstants.stringVarPrefix);
			lEnd = aFormulaText.indexOf(AppConstants.stringVarPostfix);
		}
		Interpreter bsh = new Interpreter();
		try {
			bsh.eval("(" + aFormulaText + ");");
		} catch (EvalError ee) {
			appManager.addError(aErrors, "formula", "not_valid");
		}
	}

	/**
	 * Updates case roles references for ref tag.
	 *
	 * @param aComponent the a component, the input element for the ref
	 * @param aNodeName the a node name
	 * @param aDefTag the definition tag
	 * @param aRefTagIds the a ref tag ids, references to other tags/components
	 * @param aErrors the a errors, empty if no errors
	 *
	 * @return true, if successful
	 */
	protected boolean updateRefCaseRolesRef(Component aComponent, String aNodeName, IXMLTag aDefTag, Hashtable<String,List<String>> aRefTagIds, List<String[]> aErrors) {
		if (aComponent == null)
			return false;

		String lRefType = "";
		if (aDefTag != null)
			lRefType = aDefTag.getAttribute(AppConstants.defKeyReftype);
		if (lRefType.equals(""))
			return false;
		if (!aRefTagIds.containsKey(lRefType)) {
			List<String> lRefTagIds = new ArrayList<String>(0);
			aRefTagIds.put(lRefType, lRefTagIds);
		}
		boolean lChoiceEmpty = (aDefTag.getAttribute(AppConstants.defKeyChoiceempty).equals(AppConstants.statusValueTrue));

		CListbox lCarListbox = (CListbox) aComponent.getFellowIfAny(aNodeName+"scriptCarIds");
		if (lCarListbox == null) {
			if (lChoiceEmpty)
				return true;
			appManager.addError(aErrors, "refs", "error_no_choice");
			return false;
		}

		Set<Listitem> lSelectedCarItems = lCarListbox.getSelectedItems();
		if (lSelectedCarItems == null) {
			if (lChoiceEmpty)
				return true;
			appManager.addError(aErrors, "refs", "error_no_choice");
			return false;
		}

		String[] lSelectedCarIds = null;
		if ((lSelectedCarItems != null) && (lSelectedCarItems.size() > 0)) {
			lSelectedCarIds = new String[lSelectedCarItems.size()];
			int lCounter = 0;
			for (Listitem lItem : lSelectedCarItems) {
				lSelectedCarIds[lCounter] = (String) lItem.getValue();
				lCounter = lCounter + 1;
			}
		}

		if (lSelectedCarIds == null) {
			if (lChoiceEmpty)
				return true;
			appManager.addError(aErrors, "refs", "error_no_choice");
			return false;
		}

		for (int i = 0; i < lSelectedCarIds.length; i++) {
			String lIds = lSelectedCarIds[i] + ",0" + ",0";
			aRefTagIds.get(lRefType).add("" + AppConstants.statusKeySelectedIndex + "," + lIds);
		}

		// if no errors do nothing, data is saved in case casecomponent
		if (aErrors.size() == 0)
			;
		return (aErrors.size() == 0);
	}

	/**
	 * Adds external update tag. A tag that should be added in status of other users.
	 * Added for polymorphism. Used for user generated content in player.
	 *
	 * @param aUpdateType the update type
	 * @param aSelectedTag the selected tag
	 * @param aTag the tag
	 */
	public void addExternalUpdateTag(String aUpdateType, IXMLTag aSelectedTag, IXMLTag aTag) {
	}

	/**
	 * Adds external update status tag. A tag that should change the status attributes of other users.
	 * Added for polymorphism. Used for user generated content in player.
	 *
	 * @param aTag the tag
	 * @param aStatusKey the status key
	 * @param aStatusValue the status value
	 */
	public void addExternalUpdateStatusTag(IXMLTag aTag, String aStatusKey, String aStatusValue) {
	}

	/**
	 * Updates existing status with external tags. Gets all update tags for current user and
	 * processes them.
	 * Added for polymorphism. Used for user generated content in player.
	 */
	public void updateWithExternalTags() {
	}

}
