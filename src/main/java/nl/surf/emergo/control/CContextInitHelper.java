/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.context.ApplicationContext;

import nl.surf.emergo.business.IAppManager;
import nl.surf.emergo.business.IXmlManager;
import nl.surf.emergo.zkspring.SSpring;

/**
 * The Class CContextInitHelper. Helper class for updating components when context is started.
 */
public class CContextInitHelper extends CImportExportHelper {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5278885409239998567L;

	/** The xml manager. */
	protected IXmlManager xmlManager = null;

	/** The application manager. */
	protected IAppManager appManager = null;

	/** The application context. */
	protected ApplicationContext appContext = null;

	/** The SSpring. */
	protected SSpring sSpring = null;

	/** The temporary directory name, random name for each component update check. */
	protected String randomTempDir = (int)(Math.random() * 1000) + "/";
	
	/**
	 * Constructor for CContextInitHelper class.
	 */
	public CContextInitHelper(ApplicationContext aAppContext) {
		appContext = aAppContext;
		xmlManager = (IXmlManager) getBean("xmlManager");
		appManager = (IAppManager) getBean("appManager");
	}

	/**
	 * Gets the SSpring instantiation.
	 *
	 * @return the sSpring
	 */
	protected SSpring getSpring() {
		if (sSpring == null)
			sSpring = new SSpring();
		return sSpring;
	}

	/**
	 * Gets the bean, given by aBeanId. A bean is an object defined within the
	 * Spring configuration file, that is injected by Spring at runtime.
	 *
	 * @param aBeanId the a bean id
	 *
	 * @return the bean
	 */
	protected Object getBean(String aBeanId) {
		return appContext.getBean(aBeanId);
	}

	/**
	 * Gets the XML manager.
	 *
	 * @return the XML manager
	 */
	protected IXmlManager getXmlManager() {
		if (xmlManager == null)
			xmlManager = (IXmlManager) getBean("xmlManager");
		return xmlManager;
	}

	public String getInitParameter(String aKey) {
		return appManager.getInitParameter(aKey);
	}

	protected String getAbsolutePath(String aPath) {
		String lPath = appManager.getAbsoluteAppPath();
		Path lP = Paths.get(lPath, aPath);
		return lP.toString();
	}

	/**
	 * Returns temp path for this session.
	 *
	 * @return temp path
	 */
	public String getTempPath() {
		String lTempPath = getAbsolutePath(getInitParameter("emergo.temp.path"));
		return Paths.get(lTempPath, randomTempDir).toString();
	}

	/**
	 * Gets the unescaped Xml string
	 *
	 * @param aXmlString the Xml string to unescaspe
	 *
	 * @return the unescaped Xml string
	 */
	protected String unescapeXML(String aXmlString) {
		return getXmlManager().unescapeXML(aXmlString);
	}
	
}