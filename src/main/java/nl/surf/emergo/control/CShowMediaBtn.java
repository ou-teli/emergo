/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.impl.InputElement;

import nl.surf.emergo.business.IFileManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.domain.IEBlob;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.view.VView;

/**
 * The Class CShowMediaBtn.
 *
 * Is used to preview uploaded media.
 */
public class CShowMediaBtn extends COpenMediaUsingJavascriptBtn {

	private static final long serialVersionUID = -7074518350342110212L;

	/** The content comp, the input element containing the uploaded content. */
	protected Component contentComp = null;

	/**
	 * Sets the content component.
	 *
	 * @param aContentComp the content component
	 */
	public void setContentComp(Component aContentComp) {
		contentComp = aContentComp;
	}

	/**
	 * On focus, set href for button, so on click this href will be shown.
	 * Onfocus is before onclick. Google Chrome doesn't generate onFocus, so then
	 * setHref is called by CBlobTextBox onChange method.
	 * But click on this button is before onChange, so preview can show previous preview if losing focus
	 * of CBlobTextBox is handled before.
	 */
	public void onFocus() {
		setHref("");
	}

	/**
	 * set href
	 *
	 * @param aHref the href, it isn't used but needed for polymorphism
	 */
	public void setHref(String aHref) {
		IECase lCase = CDesktopComponents.sSpring().getCase();
		if (lCase == null)
			return;
		// input element containing uploaded content
		InputElement lInput = (InputElement) contentComp;
		if (lInput == null)
			return;
		String lText = lInput.getText();
		if (lText.equals(""))
			// no uploaded content
			return;
		// internal streaming video file; add name to session variable, so file can be previewed
		CDesktopComponents.sSpring().getSBlobHelper().setBlobInSessionVar(lText, "0");

		String lBlobtype = (String) lInput.getAttribute(AppConstants.keyBlobtype);
		String lHref = "";
		if (lBlobtype.equals(AppConstants.blobtypeDatabase)) {
			// means uploaded file on server, files not in db anymore!
			// lText is blob on server, so create blob file in temp map and path to blob
			String lSubPath = CDesktopComponents.vView().getUniqueTempSubPath();
			// path within browser
			String lPath = VView.getInitParameter("emergo.temp.path") + lSubPath;
			// path on disk
			String lDiskPath = CDesktopComponents.sSpring().getAppManager().getAbsoluteTempPath() + lSubPath;
			IFileManager fileManager = (IFileManager) CDesktopComponents.sSpring().getBean("fileManager");
			Media lMedia = (Media) lInput.getAttribute("content");
			if (lMedia != null) {
				// if content within input element, it is just uploaded so create temp file to show
				String lFilename = fileManager.createFile(lDiskPath, lText,CDesktopComponents.sSpring().getSBlobHelper().mediaToByteArray(lMedia));
				if (!lFilename.equals("")) {
					lHref = lPath + lText;
				}
			}
			else {
				// if content not within input element, it is uploaded before or nothing has
				// been uploaded yet.
				IEBlob lBlob = (IEBlob)lInput.getAttribute("blob");
				if (lBlob != null) {
					// existing blob, so file is uploaded before on server
					lHref = VView.getInitParameter("emergo.blob.path")+lBlob.getBloId()+"/"+lBlob.getFilename();
				}
			}
		}
		if (lBlobtype.equals(AppConstants.blobtypeIntUrl)) {
			// lText is local url to file on streaming server so put streaming
			// path before lText.
			lHref = CDesktopComponents.sSpring().getSBlobHelper().getIntUrl(lCase, lText, false);
		}
		if (lBlobtype.equals(AppConstants.blobtypeExtUrl)) {
			// lText is url.
			lHref = CDesktopComponents.sSpring().getSBlobHelper().getExtUrl(lText);
		}
		lHref = CDesktopComponents.sSpring().getSBlobHelper().convertHrefForCertainMediaTypes(lHref);
		if (!CDesktopComponents.vView().isAbsoluteUrl(lHref)) {
			if (!lHref.equals("") && lHref.indexOf("/") != 0)
				lHref = "/" + lHref;
			lHref = CDesktopComponents.vView().getEmergoWebappsRoot() + lHref;
		}
		// avoid javascript unterminated string error
		lHref = lHref.replace("'", "\\'");
		javascriptOnClickAction = CDesktopComponents.vView().getJavascriptWindowOpenFullscreen(lHref);
	}

}
