/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.IXmlManager;
import nl.surf.emergo.control.cde.CCdeComponentItemWnd;
import nl.surf.emergo.control.def.CDefButton;

/**
 * The Class CContentItemOkBtn.
 */
public class CContentItemOkBtn extends CDefButton {

	private static final long serialVersionUID = -3116657241236139677L;
	
	public void onCreate() {
		setVisible(((String)CDesktopComponents.cControl().getAccSessAttr("is_author")).equals("true"));
	}

	/**
	 * On click create new content item or update content item if no errors and close edit window.
	 * 
	 * In case of user generated content where content is shared between different
	 * run group accounts, before the content item is added/changed the content is updated
	 * with possible external tags of other run group accounts. So keeping the content
	 * equal for all run group accounts. And after adding/changing the content item is made
	 * available to other run group accounts.
	 * 
	 * If the content item involves references to other content items or components,
	 * the case case component xml data is updated accordingly.
	 */
	public void onClick() {
		CCdeComponentItemWnd lWindow = (CCdeComponentItemWnd) getRoot();
		CContentHelper lContentHelper = lWindow.getContentHelper();

		String lXmlDef = lContentHelper.getXmlDef();
		IXmlManager	xmlManager = CDesktopComponents.sSpring().getXmlManager();

		IXMLTag lSelectedTag = lWindow.getItem();
		boolean lNew = lWindow.getNewitem();
		String lNewnodetype = lWindow.getNewnodetype();

		//determine tag
		IXMLTag lTag = null;
		if (lNew) {
			lTag = xmlManager.newNodeTag(lXmlDef, lWindow.getNodename());
		}
		else {
			lTag = lSelectedTag;
		}
		// determine parent tag
		IXMLTag lParentTag = null;
		if (lNew && lNewnodetype.equals("child")) {
			lParentTag = lSelectedTag;
		}
		else {
			lParentTag = lSelectedTag.getParentTag();
		}

		// references to other tags are returned in lRefTagIdsPerRefType and have to be saved in case case component data
		// can be empty if no references are added or references are removed
		Hashtable<String,List<String>> lRefTagIdsPerRefType = new Hashtable<String,List<String>>(0);
		// tag data that has to do with blobs is returned in lBlobTagDatas
		// can be empty if no blobs are added or blobs are not changed
		List<Hashtable<String,Object>> lBlobTagDatas = new ArrayList<Hashtable<String,Object>>(0);
		// input errors are returned in lErrors
		// lErrors will be empty if there are no input errors
		List<String[]> lErrors = new ArrayList<String[]>(0);

		// xmlRowsToNode returns true if there are any changes
		if (lContentHelper.xmlRowsToNode(getFellowIfAny("contentRws"), lParentTag, lTag, lNew, lRefTagIdsPerRefType, lBlobTagDatas, lErrors) || 
					isItemcontextChanged(lTag, lWindow.getNodecontext())) {
			if (lParentTag != null && lErrors.size() == 0) {
				// set parent tag
				lTag.setParentTag(lParentTag);
				// in case of new tag add it to the parent tag in memory
				if (lNew) {
					if (lNewnodetype.equals("sibling")) {
						// insert lItem above lSelectedItem
						lParentTag.getChildTags().add(lParentTag.getChildTags().indexOf(lSelectedTag), lTag);
					}
					else {
						// add lItem to childtags of parent
						lParentTag.getChildTags().add(lTag);
					}
				}
				
				setItemcontext(lTag, lWindow.getNodecontext(), false);
				
				// handle blob tag data for lTag
				lContentHelper.handleBlobTagDatas(lBlobTagDatas);

				// possible tag updates by other users are added (in case of components allowing user generated content)
				lContentHelper.updateWithExternalTags();

				// at this point the tag tree in memory is adjusted correctly
				// errors are already checked so don't use result of methods newSiblingNode, newChildNode or updateNode
				if (lNew) {
					if (!lNewnodetype.equals("child")) {
						lContentHelper.newSiblingNode(lTag, lSelectedTag, true);
						lContentHelper.addExternalUpdateTag("newsibling", lSelectedTag, lTag);
					}
					else {
						lContentHelper.newChildNode(lTag, lSelectedTag, true);
						lContentHelper.addExternalUpdateTag("newchild", lSelectedTag, lTag);
					}
				} 
				else {
					lContentHelper.updateNode(lTag, true);
					lContentHelper.addExternalUpdateTag("update", null, lTag);
				}
				
				// at this point tag tree in the database is adjusted correctly
				
				// save references to other tags in case case component data
				CCaseHelper cCaseHelper = new CCaseHelper();
				for (Enumeration<String> lRefTypes = lRefTagIdsPerRefType.keys(); lRefTypes.hasMoreElements();) {
					String lRefType = lRefTypes.nextElement();
					if (lRefType != null && !lRefType.equals("")) {
						List<String> lRefTagIds = lRefTagIdsPerRefType.get(lRefType);
						if (lRefTagIds != null) {
							cCaseHelper.setReferenceIds(lContentHelper.getCaseComponent(), lRefType, lTag, lRefTagIds);
						}
					}
				}
				lWindow.setAttribute("item", lTag);
			} else {
				lWindow.setAttribute("item", null);
				CDesktopComponents.cControl().showErrors(this, lErrors);
			}
		}
		lWindow.detach();
	}
	
	/**
	 * Check for changes in automatically passed item properties.
	 * 
	 * @param aItemTag		the item XML tag
	 * @param aNodecontext	hashtable automatically passed item properties
	 * 
	 * @return	boolean changed
	 */
	private boolean isItemcontextChanged (IXMLTag aItemTag, Hashtable<String,Object> aNodecontext) {
		return setItemcontext (aItemTag, aNodecontext, true);
	}

	/**
	 * Fill item XML tag with automatically passed item properties.
	 * 
	 * @param aItemTag		the item XML tag
	 * @param aNodecontext	hashtable automatically passed item properties
	 * @param aOnlyCheck	boolean whether or not to change the tag
	 * 
	 * @return	boolean changed
	 * 
	 * TODO: deal with player user generated content
	 */
	private boolean setItemcontext (IXMLTag aItemTag, Hashtable<String,Object> aNodecontext, boolean aOnlyCheck) {
		boolean lNodecontextChanged = false;
		if (aNodecontext != null) {
			for (Enumeration<String> keys = aNodecontext.keys(); keys.hasMoreElements();) {
				String key = keys.nextElement();
				String value = (String) aNodecontext.get(key);
				IXMLTag lChildTag = aItemTag.getChild(key);
				if (lChildTag != null && !lChildTag.getValue().equals(value)) {
					if (!aOnlyCheck)
						lChildTag.setValue(value);
					lNodecontextChanged = true;
				}
			}
		}
		return lNodecontextChanged;
	}

}
