/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import org.zkoss.zk.ui.Component;

import nl.surf.emergo.business.IXMLTag;

/**
 * The Class CContentItemDeleteMi.
 * 
 * Handles deleting of contentitem.
 */
public class CContentItemDeleteMi extends CContentItemMi {

	private static final long serialVersionUID = -7764454871257856639L;

	/**
	 * Instantiates a contentitem delete menu item.
	 * 
	 * @param aContentComponent the content xml tree
	 * @param aContentHelper the content helper
	 * @param aIsAuthor the is author state
	 */
	public CContentItemDeleteMi(Component aContentComponent, CContentHelper aContentHelper, boolean aIsAuthor) {
		super(aContentComponent, aContentHelper, aIsAuthor);
	}

	/**
	 * On click show delete popup and when confirmed delete contentitem.
	 */
	public void onClick() {
		contentRoot = getComponent();
		contentItem = (Component)contentRoot.getAttribute("target");
		String lValue = (String) contentItem.getAttribute("keyvalues");
		params.put("item", lValue);
		params.put("contenthelper", getContentHelper());
		IXMLTag lItem = (IXMLTag) contentItem.getAttribute("item");
		params.put("itemtype", CContentHelper.getNodeTagLabel(getContentHelper().currentCaseComponentCode, lItem.getName()));

		CTree lTree = (CTree)contentRoot;
		lTree.typeOfAction = "delete";
		lTree.contentItem = contentItem;
		showPopup(getContentHelper().getDeleteView());
	}

}
