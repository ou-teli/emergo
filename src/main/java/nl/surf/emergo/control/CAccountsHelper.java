/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import org.apache.commons.lang3.StringUtils;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.view.VView;

/**
 * The Class CAccountHelper.
 */
public class CAccountsHelper extends CControlHelper {

	/**
	 * Gets landing page.
	 * 
	 * @param account the account
	 * @param rolCode the rol code
	 * 
	 * @return all roles
	 */
	public String getLandingpage(int accId, String rolCode) {
		// first check if a alternative landing page is available
		String lLandingpage = null;
		IAccountManager lAccountManager = (IAccountManager)CDesktopComponents.sSpring().getBean("accountManager");
		IEAccount lAccount = lAccountManager.getAccount(accId);
		if (lAccount != null) {
			lLandingpage = lAccountManager.getRoleLandingpage(lAccount, rolCode);
		}
		if (StringUtils.isEmpty(lLandingpage)) {
			// if no alternative landing page
			lLandingpage = CDesktopComponents.vView().v_account_role.get(rolCode);
		}
		else {
			// else add path for landing pages
			lLandingpage = VView.getInitParameter("emergo.landingpages.path") + lLandingpage;
		}
		return lLandingpage;
	}
	
}