/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import org.apache.commons.lang3.StringUtils;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.domain.IEAccount;

public class CAccountRolesWnd extends CDecoratedInputWndCC {

	private static final long serialVersionUID = -2157085554520901466L;

	public CAccountRolesWnd() {
		handleExternalStudentId();
	}

	public static void handleExternalStudentId() {
    	//other institutions may use a student id as request parameter. It is stored in the session. Now store it in the EMERGO database.
		String external_student_id = (String)CDesktopComponents.cControl().getAccSessAttr("login_external_student_id");
		if (!StringUtils.isEmpty(external_student_id)) {
			IEAccount account = CDesktopComponents.sSpring().getAccount();
			if (account != null) {
				IAccountManager accountManager = (IAccountManager)CDesktopComponents.sSpring().getBean("accountManager");
				if (!StringUtils.isEmpty(account.getStudentid())) {
					account.setStudentid(external_student_id);
					accountManager.updateAccount(account);
					CDesktopComponents.sSpring().getSLogHelper().logAction("EXTERNAL-STUDENT-SET-STUDENTID " + "userid=" + account.getUserid() + " - studentid=" + account.getStudentid());
				}
			}
		}
	}

}
