/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

//import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.PathResource;
import org.springframework.mail.javamail.ConfigurableMimeFileTypeMap;
import org.springframework.web.util.UriUtils;
import org.zkoss.lang.Strings;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WebApp;
import org.zkoss.zk.ui.http.WebManager;
import org.zkoss.zk.ui.sys.SessionsCtrl;

import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.security.spring.zk.CustomSessionCControl;

public class CContentFilesFilter extends HttpServlet {

	private static final long serialVersionUID = -2686426785520780755L;

	private static final Logger _log = LogManager.getLogger(CContentFilesFilter.class);

	private static final String ETAG = "W/\"%s-%s\"";
	private static final Pattern RANGE_PATTERN = Pattern.compile("^bytes=[0-9]*-[0-9]*(,[0-9]*-[0-9]*)*$");
	private static final String CONTENT_DISPOSITION_HEADER = "%s;filename=\"%2$s\"; filename*=UTF-8''%2$s";
	private static final String MULTIPART_BOUNDARY = UUID.randomUUID().toString();
	private static final int DEFAULT_STREAM_BUFFER_SIZE = 10240;
	
	private static ArrayList<String> onlyDownloadResources = new ArrayList<String>();
	
	protected WebApp webApp = null;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		//CControl is used to get and set session attributes.
		Session zksession = getZKSession(request);
		CControl cControl = new CustomSessionCControl(zksession);
		ArrayList<String> lMediaFiles = new ArrayList<String>();
		Object lSessVar = cControl.getAccSessAttr(AppConstants.sessKeyContentFiles);
		if (lSessVar != null) {
			lMediaFiles = (ArrayList<String>)lSessVar;
		}
		boolean lBlock = true;
	    String lFileName = request.getRequestURI();
		Path lFilePath = Paths.get(lFileName);
		String lTempFolder = this.getServletContext().getInitParameter("emergo.temp.path");
		//NOTE check if file is located in temp folder; if so, file may be accessed by default, else file is blocked by default
		if (lTempFolder != null && !lTempFolder.isEmpty()) {
			Path lTempPath = Paths.get(lTempFolder);
			lTempPath = lTempPath.getName(lTempPath.getNameCount() - 1);
			for (Path lPath: lFilePath) {
				if (lPath.endsWith(lTempPath)) {
					lBlock = false;
					break;
				}
			}
		}
		if (!lMediaFiles.isEmpty()) {
			boolean lTestBlob = false;
			int lTestBlobInd = 0;  // lTestBlobInd is the index of the blob id subpath in the file path
			String lBlobFolder = this.getServletContext().getInitParameter("emergo.blob.path");
			Path lBlobPath = Paths.get(lBlobFolder);
			if (lBlobFolder != null && !lBlobFolder.isEmpty()) {
				lBlobPath = lBlobPath.getName(lBlobPath.getNameCount() - 1);
				for (Path lPath: lFilePath) {
					lTestBlobInd++;
					if (lPath.endsWith(lBlobPath)) {
						lTestBlob = true;
						break;
					}
				}
			}
			String lFName = lFilePath.getFileName().toString();
			lFName = UriUtils.decode(lFName, StandardCharsets.UTF_8.toString());
			int lInd = lMediaFiles.indexOf(lFName);
			if (lInd >= 0) {
				if (lTestBlob) {
					lSessVar = cControl.getAccSessAttr(AppConstants.sessKeyContentFileBlobIds);
					if (lSessVar != null) {
						ArrayList<String> lBlobIds = ((ArrayList<ArrayList<String>>)lSessVar).get(lInd);
						String lBlobIdInFile = lFilePath.getName(lTestBlobInd).toString();
						if (lBlobIds.contains(lBlobIdInFile)) {
							lBlock = false;
						}
					}
				} else {
					//video file; can be in streaming root or in case subfolder
					lBlock = false;
				}
			}
		}
		
		if (!lBlock) {
			this.doPostChecked(request, response);
		}
		else {
			_log.info("Access denied: {}", request.getRequestURI());
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			out.println("Access denied.");
		}
	}

	protected void doPostChecked(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		response.reset();
		ServletOutputStream out=response.getOutputStream();
	    String lFName = request.getRequestURI();
	    String lCPath = request.getContextPath();
	    String lSubName = lFName.substring(lFName.indexOf(lCPath) + lCPath.length());
        _log.info("requestSub: {}", lSubName);
        lFName =  UriUtils.decode(getServerPath(lSubName), StandardCharsets.UTF_8.toString());
        _log.info("posting: {}", lFName);
        try{
	    	File lFile=new File(lFName);
	        if (lFile.exists()) {
	        	if (lFile.canRead()) {
	        		Long lDefExpTimeS = TimeUnit.DAYS.toSeconds(30);
	        		response.setHeader("Cache-Control", "public,max-age=" + lDefExpTimeS + ",must-revalidate");
	        		response.setDateHeader("Expires", System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(lDefExpTimeS));
	        		response.setHeader("Pragma", ""); // Explicitly set pragma to prevent container from overriding it.
	        		long lLastMod = lFile.lastModified();
	        		String lEncName = URLEncoder.encode(lFile.getName(), StandardCharsets.UTF_8.toString());
	        		String lETag = String.format(ETAG, lEncName, lLastMod);
	        		response.setHeader("ETag", lETag);
	        		response.setDateHeader("Last-Modified", lLastMod);

	        		if (notModified(request, lETag, lLastMod)) {
	        			response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
	        			return;
	        		}
	        		
	    		    int lFileSize = (int)lFile.length();
	        		List<Range> lRanges = getRanges(request, lETag, lLastMod, lFileSize);
	        		if (lRanges == null) {
	        	        _log.info("error handling file: {}; error in requested range.", lFName);
	        			response.setHeader("Content-Range", "bytes */" + lFileSize);
	        			response.sendError(HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE);
	        			return;
	        		}
	        		if (!lRanges.isEmpty()) {
	        			response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);
	        		}
	        		else {
	        			lRanges.add(new Range(0, lFileSize - 1)); // Full content.
	        		}

	        		// add content type to response header
	    		    ConfigurableMimeFileTypeMap lMimeMap = new ConfigurableMimeFileTypeMap();
	    			String lMimetypesPath = this.getServletContext().getInitParameter("emergo.mimetypes.path");
	    			Path lMimetypesFile = Paths.get(this.getServletContext().getRealPath(lMimetypesPath), "mime.types");
	    			lMimeMap.setMappingLocation(new PathResource(lMimetypesFile));
	        		String lMimeType = lMimeMap.getContentType(lFName);//defaults to application/octet-stream
        			String lDisposition = isAttachment(request, lFName, lMimeType) ? "attachment" : "inline";
        		
	        		response.setHeader("Content-Disposition", String.format(CONTENT_DISPOSITION_HEADER, lDisposition, lEncName));
	        		response.setHeader("Accept-Ranges", "bytes");
	        		if (lRanges.size() == 1) {
	        			Range lRange = lRanges.get(0);
	        			response.setContentType(lMimeType);
	        			response.setHeader("Content-Range", "bytes " + lRange.start + "-" + lRange.end + "/" + lFileSize);
	        			response.setHeader("Content-Length", String.valueOf(lRange.length));
	        			stream(lFile, out, lRange.start, lRange.length);
	        		}
	        		else {
	        			response.setContentType("multipart/byteranges; boundary=" + MULTIPART_BOUNDARY);
	        			for (Range lRange : lRanges) {
	        				out.println();
	        				out.println("--" + MULTIPART_BOUNDARY);
	        				out.println("Content-Type: " + lMimeType);
	        				out.println("Content-Range: bytes " + lRange.start + "-" + lRange.end + "/" + lFileSize);
	        				stream(lFile, out, lRange.start, lRange.length);
	        			}

	        			out.println();
	        			out.println("--" + MULTIPART_BOUNDARY + "--");
	        		}
	        	} else {
			        _log.info("file not accessible: {}", lFName);
	        	}
	        } else {
	        	_log.info("file not found: {}", lFName);
	        }
	    } catch (IOException e) {
	        _log.info("error handling file: {}", lFName);
	        _log.error(e);
	    } finally {
	        if (out != null) {
	    	    try{
	    	    	out.flush();
		        	out.close();
	    	    } catch (IOException | IllegalStateException e) {
	    	        _log.info("error flushing file: {}", lFName);
	    	    }
	        }
	    }
	}
	
	protected boolean isAttachment(HttpServletRequest request, String pUrl, String pMimeType) {
		if (checkIfOnlyDownloadResource(pUrl))
			return true;
		String lAccept = request.getHeader("Accept");
		return !(startsWithOneOf(pMimeType, "text", "image", "application/pdf") && accepts(lAccept, pMimeType));
	}

	/**
	 * Returns true if the given accept header accepts the given value.
	 */
	private static boolean accepts(String acceptHeader, String toAccept) {
		if (acceptHeader == null)
			return false;
		String[] acceptValues = acceptHeader.split("\\s*(,|;)\\s*");
		Arrays.sort(acceptValues);
		return Arrays.binarySearch(acceptValues, toAccept) > -1
			|| Arrays.binarySearch(acceptValues, toAccept.replaceAll("/.*$", "/*")) > -1
			|| Arrays.binarySearch(acceptValues, "*/*") > -1;
	}

	protected static boolean startsWithOneOf(String string, String... prefixes) {
		for (String prefix : prefixes) {
			if (string.startsWith(prefix)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Returns true if it's a conditional request which must return 304.
	 */
	private boolean notModified(HttpServletRequest request, String pETag, long pLastMod) {
		String noMatch = request.getHeader("If-None-Match");
		long modified = request.getDateHeader("If-Modified-Since");
		return (noMatch != null) ? matches(noMatch, pETag) : (modified != -1 && !modified(modified, pLastMod));
	}

	/**
	 * Get requested ranges. If this is null, then we must return 416. If this is empty, then we must return full file.
	 */
	private List<Range> getRanges(HttpServletRequest request, String pETag, long pLastMod, long pFileSize) {
		List<Range> ranges = new ArrayList<>(1);
		String rangeHeader = request.getHeader("Range");

		if (rangeHeader == null) {
			return ranges;
		}
		else if (!RANGE_PATTERN.matcher(rangeHeader).matches()) {
			return null; // Syntax error.
		}

		String ifRange = request.getHeader("If-Range");

		if (ifRange != null && !ifRange.equals(pETag)) {
			try {
				long ifRangeTime = request.getDateHeader("If-Range");

				if (ifRangeTime != -1 && modified(ifRangeTime, pLastMod)) {
					return ranges;
				}
			}
			catch (IllegalArgumentException ifRangeHeaderIsInvalid) {
				return ranges;
			}
		}

		for (String rangeHeaderPart : rangeHeader.split("=")[1].split(",")) {
			Range range = parseRange(rangeHeaderPart, pFileSize);

			if (range == null) {
				return null; // Logic error.
			}

			ranges.add(range);
		}

		return ranges;
	}

	/**
	 * Parse range header part. Returns null if there's a logic error (i.e. start after end).
	 */
	private Range parseRange(String range, long length) {
		long start = sublong(range, 0, range.indexOf('-'));
		long end = sublong(range, range.indexOf('-') + 1, range.length());

		if (start == -1) {
			start = length - end;
			end = length - 1;
		}
		else if (end == -1 || end > length - 1) {
			end = length - 1;
		}

		if (start > end) {
			return null; // Logic error.
		}

		return new Range(start, end);
	}

	/**
	 * Returns true if the given match header matches the given ETag value.
	 */
	private static boolean matches(String matchHeader, String eTag) {
		String[] matchValues = matchHeader.split("\\s*,\\s*");
		Arrays.sort(matchValues);
		return Arrays.binarySearch(matchValues, eTag) > -1
			|| Arrays.binarySearch(matchValues, "*") > -1;
	}

	/**
	 * Returns true if the given modified header is older than the given last modified value.
	 */
	private static boolean modified(long modifiedHeader, long lastModified) {
		return (modifiedHeader + TimeUnit.SECONDS.toMillis(1) <= lastModified); // That second is because the header is in seconds, not millis.
	}

	/**
	 * Returns a substring of the given string value from the given begin index to the given end index as a long.
	 * If the substring is empty, then -1 will be returned.
	 */
	private static long sublong(String value, int beginIndex, int endIndex) {
		String substring = value.substring(beginIndex, endIndex);
		return substring.isEmpty() ? -1 : Long.parseLong(substring);
	}

	/**
	 * Convenience class for a byte range.
	 */
	private static class Range {
		private final long start;
		private final long end;
		private final long length;

		public Range(long start, long end) {
			this.start = start;
			this.end = end;
			length = end - start + 1;
		}
	}

	/**
	 * Stream the given input to the given output via NIO {@link Channels} and a directly allocated NIO
	 * {@link ByteBuffer}. Both the input and output streams will implicitly be closed after streaming,
	 * regardless of whether an exception is been thrown or not.
	 * @param input The input stream.
	 * @param output The output stream.
	 * @return The length of the written bytes.
	 * @throws IOException When an I/O error occurs.
	 */
	public static long stream(InputStream input, OutputStream output) throws IOException {
		try (ReadableByteChannel inputChannel = Channels.newChannel(input);
			WritableByteChannel outputChannel = Channels.newChannel(output))
		{
			ByteBuffer buffer = ByteBuffer.allocateDirect(DEFAULT_STREAM_BUFFER_SIZE);
			long size = 0;

			while (inputChannel.read(buffer) != -1) {
				buffer.flip();
				size += outputChannel.write(buffer);
				buffer.clear();
			}

			return size;
		}
	}

	/**
	 * Stream a specified range of the given file to the given output via NIO {@link Channels} and a directly allocated
	 * NIO {@link ByteBuffer}. The output stream will only implicitly be closed after streaming when the specified range
	 * represents the whole file, regardless of whether an exception is been thrown or not.
	 * @param file The file.
	 * @param output The output stream.
	 * @param start The start position (offset).
	 * @param length The (intented) length of written bytes.
	 * @return The (actual) length of the written bytes. This may be smaller when the given length is too large.
	 * @throws IOException When an I/O error occurs.
	 * @since 2.2
	 */
	public static long stream(File file, OutputStream output, long start, long length) throws IOException {
		if (start == 0 && length >= file.length()) {
			return stream(new FileInputStream(file), output);
		}

		try (FileChannel fileChannel = (FileChannel) Files.newByteChannel(file.toPath(), StandardOpenOption.READ)) {
			WritableByteChannel outputChannel = Channels.newChannel(output);
			ByteBuffer buffer = ByteBuffer.allocateDirect(DEFAULT_STREAM_BUFFER_SIZE);
			long size = 0;

			while (fileChannel.read(buffer, start + size) != -1) {
				buffer.flip();

				if (size + buffer.limit() > length) {
					buffer.limit((int) (length - size));
				}

				size += outputChannel.write(buffer);

				if (size >= length) {
					break;
				}

				buffer.clear();
			}

			return size;
		}
	}
	
	//TODO: could get desktop from webmanager getdesktopcache and dtid from the request (desktop is created after the filter, so this would be more of a hack)
	protected Session getZKSession(HttpServletRequest request) {
		initializeFromZK();
		Session zksession = null;
		
		if (webApp != null) {
			zksession = SessionsCtrl.getSession(webApp,request.getSession());
		
			if (zksession == null)
				_log.info("Could not find zk session");
		}
		else
			_log.debug("Could not find zk web app to get session from");
		
		return zksession;
	}

	synchronized private void initializeFromZK() {
		if (webApp == null) {
			//http://www.zkoss.org/javadoc/7.0.1/zk/org/zkoss/zk/ui/http/WebManager.html
			webApp = WebManager.getWebAppIfAny(getServletContext());
			//webManager = WebManager.getWebManagerIfAny(getServletContext());
			
			if (webApp == null)
				_log.info("ZK not inialized (yet), could not find zk webApp in servletcontext");
			else
				_log.info("ZK webApp initialized from servletcontext");
		}
	}
	
	/**
	 * Add path and filename of resourece file that must be downloaded and may not be opened inline
	 * NOTE: Set resources uploaded by players always to 'attachment'. For security reasons they should not be shown inline.
	 * 
	 * @param hRef The path and name of the resource file.
	 */
	public static void addOnlyDownloadResource(String hRef) {
		if (!onlyDownloadResources.contains(hRef)) {
			onlyDownloadResources.add(hRef);
		}
	}
	
	private boolean checkIfOnlyDownloadResource(String hRef) {
		_log.info("checkIfOnlyDownloadResource, hRef: {}", hRef);
		for (String s : onlyDownloadResources) {
			s = UriUtils.decode(getServerPath(s), StandardCharsets.UTF_8.toString());
			if (hRef.equals(s)) {
				return true;
			}
		}
		return false;
	}
	
	private String getServerPath(String pResource) {
        String lRealPath = this.getServletContext().getRealPath(pResource);
        if (!Strings.isBlank(lRealPath)) {
    		_log.info("getServerPath: {}", lRealPath);
        } else {
        	lRealPath = this.getServletContext().getRealPath("/") + pResource.substring(1);
    		_log.info("getServerPath, server path from root: {}", lRealPath);
        }
    	return lRealPath;
	}
	
}
