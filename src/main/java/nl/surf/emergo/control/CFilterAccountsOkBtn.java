/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zul.Textbox;

import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.def.CDefButton;

/**
 * The Class CAdmFilterAccountsOkBtn. When clicked it opens the accounts filtered by the entered values.
 */
public abstract class CFilterAccountsOkBtn extends CDefButton {

	private static final long serialVersionUID = -5622853836818512807L;

	private void pSaveFilterValues(String[] pFieldIds, String pSessAttrId) {
		Map<String, String> keysAndValueParts = (Map<String, String>)CDesktopComponents.cControl().getAccSessAttr(pSessAttrId);
		if (keysAndValueParts == null) {
			keysAndValueParts = new HashMap<String, String>();
			CDesktopComponents.cControl().setAccSessAttr(pSessAttrId, keysAndValueParts);
		}
		for (int i=0;i<pFieldIds.length;i++) {
			String lValuePart = "";
			Textbox lTextbox = (Textbox)CDesktopComponents.vView().getComponent(pFieldIds[i]);
			if (lTextbox != null) {
				lValuePart = lTextbox.getValue().trim();
			}
			keysAndValueParts.put(pFieldIds[i], lValuePart);
		}
	}

	protected void saveFilterValues() {
		pSaveFilterValues(new String[]{"userid", "studentid", "lastname", "email", "extradata"}, AppConstants.account_filter_map);
		
		if ("true".equals(getAttribute("includeRunFilter"))) {
			pSaveFilterValues(new String[]{"run"}, AppConstants.run_filter_map);
			pSaveFilterValues(new String[]{"case"}, AppConstants.case_filter_map);
		}
	}

	protected boolean areFilterValuesEmpty(String pSessAttrId) {
		Map<String, String> keysAndValueParts = (Map<String, String>)CDesktopComponents.cControl().getAccSessAttr(pSessAttrId);
		if (keysAndValueParts == null || keysAndValueParts.size() == 0) {
			return true;
		}
		for (Map.Entry<String, String> entry : keysAndValueParts.entrySet()) {
			if (!StringUtils.isEmpty(entry.getValue())) {
				return false;
			}
		}
		return true;
	}

}
