/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import java.util.ArrayList;
import java.util.Date;

import org.zkoss.zk.ui.AbstractComponent;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Timebox;

import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.def.CDefHelper;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.domain.IEDomainEntity;

/**
 * The Class CControlHelper.
 */
public class CControlHelper extends CDefHelper {

	/**
	 * Renders one run with redirect buttons for run group accounts and run teams.
	 *
	 * @param aZKComponent the a zk component
	 * @param aInsertBefore the a insert before
	 * @param aItem the a item
	 */
	public void showTooltiptextIfAdmin(HtmlBasedComponent aZKComponent, String aText) {
		if (CDesktopComponents.sSpring().hasRole(AppConstants.c_role_adm)) {
			aZKComponent.setTooltiptext(aText);
		}
	}

	public static void showInputControlIdError(AbstractComponent aTarget, String aComponentId, IEDomainEntity aEntity) {
		CDesktopComponents.cControl().showErrors(aTarget, new ArrayList<String[]>(0), 
			"For EMERGO developmentteam:\n" + 
			"Input control in ZUL file '" + CDesktopComponents.vView().getCurrentUrl() + "' has id equal to '" + aComponentId + "'. " + 
			"However domain class '" + aEntity.getClass().getName() + "' does not have property '" + aComponentId + "'!");
	}

	public static void showPossibleInputControlIdError(Component aComponent, AbstractComponent aTarget, String aComponentId) {
		if (aComponent == null) {
			CDesktopComponents.cControl().showErrors(aTarget, new ArrayList<String[]>(0),
				"For EMERGO developmentteam:\n" + "" +
				"There is no input control in ZUL file '" + CDesktopComponents.vView().getCurrentUrl() + "' with id equal to '" + aComponentId + "'!");
		}
	}

	public static Component getInputComponent(AbstractComponent aTarget, String aComponentId) {
		Component lComponent = (Component)aTarget.getFellowIfAny(aComponentId);
		showPossibleInputControlIdError(lComponent, aTarget, aComponentId);
		return lComponent;
	}
	
	public static String getTextboxValue(AbstractComponent aTarget, String aComponentId) {
		Textbox lComponent = (Textbox)aTarget.getFellowIfAny(aComponentId);
		showPossibleInputControlIdError(lComponent, aTarget, aComponentId);
		return lComponent.getValue();
	}
	
	public static int getTextboxValueAsInt(AbstractComponent aTarget, String aComponentId, int aDefault) {
		int lResult = aDefault;
		try {
			lResult = Integer.parseInt(getTextboxValue(aTarget, aComponentId));
		} catch (NumberFormatException e) {
		}
		return lResult;
	}
	
	public static boolean isCheckboxChecked(AbstractComponent aTarget, String aComponentId) {
		Checkbox lComponent = (Checkbox)aTarget.getFellowIfAny(aComponentId);
		showPossibleInputControlIdError(lComponent, aTarget, aComponentId);
		return lComponent.isChecked();
	}
	
	public static Object getListboxValue(AbstractComponent aTarget, String aComponentId) {
		Listbox lComponent = (Listbox)aTarget.getFellowIfAny(aComponentId);
		showPossibleInputControlIdError(lComponent, aTarget, aComponentId);
		return lComponent.getSelectedItem().getValue();
	}
	
	public static int getListboxValueAsInt(AbstractComponent aTarget, String aComponentId) {
		int lResult = 0;
		try {
			lResult = Integer.parseInt((String)getListboxValue(aTarget, aComponentId));
		} catch (NumberFormatException e) {
		}
		return lResult;
	}
	
	public static Date getDateboxValue(AbstractComponent aTarget, String aComponentId) {
		Datebox lComponent = (Datebox)aTarget.getFellowIfAny(aComponentId);
		showPossibleInputControlIdError(lComponent, aTarget, aComponentId);
		return lComponent.getValue();
	}
	
	public static Date getTimeboxValue(AbstractComponent aTarget, String aComponentId) {
		Timebox lComponent = (Timebox)aTarget.getFellowIfAny(aComponentId);
		showPossibleInputControlIdError(lComponent, aTarget, aComponentId);
		return lComponent.getValue();
	}
	
	public static boolean isCaseAuthor() {
		int accId = ((IECase)CDesktopComponents.cControl().getAccSessAttr("case")).getEAccount().getAccId();
		return accId == CDesktopComponents.cControl().getAccId();
	}
	
}