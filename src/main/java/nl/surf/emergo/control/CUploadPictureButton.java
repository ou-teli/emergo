/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zul.Image;
import org.zkoss.zul.Messagebox;

import nl.surf.emergo.control.def.CDefButton;

/**
 * The Class CUploadPictureButton. Used to upload image file content.
 */
public class CUploadPictureButton extends CDefButton {

	private static final long serialVersionUID = -2411101673752532362L;

	/** The uploadimage. */
	protected Image uploadimage = null;

	/**
	 * Instantiates a new CUploadPictureButton.
	 */
	public CUploadPictureButton() {
		/*
		NOTE don't use native! Media may be checked on Image type.
		maxsize: the maximal allowed upload size of the component, in kilobytes, or a negative value if no limit.
		native: treating the uploaded file(s) as binary, i.e., not to convert it to image, audio or text files.
		multiple: treating the file chooser allows multiple files to upload, the setting only works with HTML5 supported browsers (since ZK 6.0.0).
		*/
		setUpload("true,maxsize=-1,multiple=false");
	}

	/**
	 * Sets the upload image, the ZK Image for which the content should be set
	 * to the uploaded content.
	 *
	 * @param aUploadimage the new upload image
	 */
	public void setUploadImage(Image aUploadimage) {
		uploadimage = aUploadimage;
	}

	/**
	 * On upload upload image to the upload image.
	 */
	public void onUpload(UploadEvent event) {
		Media media = event.getMedia();
		if (media instanceof org.zkoss.image.Image) {
			uploadimage.setContent((org.zkoss.image.Image) media);
			uploadimage.setAttribute("changed", "true");
		} else if (media != null) {
			CDesktopComponents.vView().showMessagebox(getRoot(), CDesktopComponents.vView().getLabel("cde_s_component.upload.error.novalidmedia.body") + media, CDesktopComponents.vView().getLabel("cde_s_component.upload.error.novalidmedia.title"),
					Messagebox.OK, Messagebox.ERROR);
		}
	}

}
