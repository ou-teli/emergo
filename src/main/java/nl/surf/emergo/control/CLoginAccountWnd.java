/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.Matcher;

import org.zkoss.zk.ui.AbstractComponent;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zul.Captcha;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

import nl.surf.emergo.business.IAccountRequestManager;
import nl.surf.emergo.business.IAppManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.utilities.PropsValues;
import nl.surf.emergo.utilities.pwned.PwndHelper;

/**
 * The Class CLoginAccountWnd.
 */
public class CLoginAccountWnd extends CWnd {

	private static final long serialVersionUID = -5338460439232263658L;

	private PwndHelper pwndHelper = new PwndHelper();

	/** The saved email input value. */
	protected String email_save = "";

	/** The saved last name input value. */
	protected String lastname_save = "";

	/** The email textbox. */
	protected Textbox emailB = null;
	protected Textbox passwordB = null;

	/** The user id textbox. */
	protected Textbox useridB = null;

	/** The lastname textbox. */
	protected Textbox lastnameB = null;

	/**
	 * Returns the email textbox.
	 *
	 * @return the textbox
	 */
	private Textbox emailBox() {
		if (emailB == null)
			emailB = (Textbox) getFellowIfAny("email");
		return emailB;
	}

	private Textbox passwordBox() {
		if (passwordB == null)
			passwordB = (Textbox) getFellowIfAny("password");
		return passwordB;
	}

	/**
	 * Returns the user id textbox.
	 *
	 * @return the textbox
	 */
	private Textbox useridBox() {
		if (useridB == null)
			useridB = (Textbox) getFellowIfAny("new_userid");
		return useridB;
	}

	/**
	 * Returns the lastname textbox.
	 *
	 * @return the textbox
	 */
	private Textbox lastnameBox() {
		if (lastnameB == null)
			lastnameB = (Textbox) getFellowIfAny("lastname");
		return lastnameB;
	}

	/**
	 * On create show correct title.
	 *
	 * @param aEvent the a event
	 */
	public void onCreate(CreateEvent aEvent) {
	}

	/**
	 * Check if email address has changed.
	 *
	 */
	public void checkInput() {
		String lEmail = emailBox().getValue();
		if (!lEmail.equals(email_save)) {
			useridBox().setValue(lEmail);
			String lLastName = lastnameBox().getValue();
			if (lLastName.equals(lastname_save)) {
				lastname_save = getLastNameFromEmail(lEmail);
				lastnameBox().setValue(lastname_save);
			}
			email_save = lEmail;
		}
	}

	/**
	 * Strips an email address and returns the "name" part.
	 *
	 * @param aEMail the email address
	 */
	private String getLastNameFromEmail(String aEMail) {
		int lPos = aEMail.indexOf("@");
		if (lPos > 0)
			return aEMail.substring(0, lPos);
		return "";
	}

	/**
	 * User clicked on "OK" button. Data for new account are gathered and tested.
	 *
	 */
	public void newAccountRequest() {
		Captcha lCap = (Captcha) getFellowIfAny("cap");
		String lCapStr = lCap.getValue();
		String lCapCheck = ((Textbox) getFellowIfAny("capcheck")).getValue();
		IAppManager appManager = (IAppManager) CDesktopComponents.sSpring().getBean("appManager");
		List<String[]> lErrors = new ArrayList<String[]>(0);
		String lSpecError = "";
		AbstractComponent lErrorTarget = null;
		if (!lCapStr.equalsIgnoreCase(lCapCheck)) {
			appManager.addError(lErrors, "captcha", "error_not_equal");
			lErrorTarget = lCap;
		}
		String lEmail = emailBox().getValue();
		int lAtPos = lEmail.indexOf("@");
		if ((lEmail.equals("")) || (lAtPos < 1) || (lEmail.lastIndexOf("@") != lAtPos)
				|| (lAtPos == (lEmail.length() - 1))) {
			appManager.addError(lErrors, "mail.receiver", "error_not_valid");
			lErrorTarget = emailBox();
		}

		String lPassword = ((Textbox) getFellowIfAny("password")).getValue();
		if (lPassword == null || lPassword.equals("") || lPassword.length() < 12 || !lPassword.matches("(?=.*[a-z]).*")
				|| !lPassword.matches("(?=.*[A-Z]).*") || !lPassword.matches("(?=.*[0-9]).*")
				|| !lPassword.matches("(?=.*[~!@#$%^&*()_-]).*")) {
			appManager.addError(lErrors, "password", "too_weak");
			lErrorTarget = passwordBox();
		}
		if (pwndHelper.isPwned(lPassword)) {
			appManager.addError(lErrors, "password", "is_pwned");
			lErrorTarget = passwordBox();
		}

		if ((lErrors == null) || (lErrors.size() == 0)) {
			Hashtable<String, String> lItem = new Hashtable<String, String>(0);
			lItem.put("email", lEmail);
			String lUserId = useridBox().getValue();
			lItem.put("userid", lUserId);
			lItem.put("studentid", "");
			lItem.put("title", ((Textbox) getFellowIfAny("title")).getValue());
			lItem.put("initials", ((Textbox) getFellowIfAny("initials")).getValue());
			lItem.put("nameprefix", ((Textbox) getFellowIfAny("nameprefix")).getValue());
			lItem.put("lastname", lastnameBox().getValue());
			lItem.put("password", lPassword);
			IAccountRequestManager accountRequestManager = (IAccountRequestManager) CDesktopComponents.sSpring()
					.getBean("accountRequestManager");
			lErrors = accountRequestManager.newAccountRequest(lItem, PropsValues.ENCODE_PASSWORD);
			if ((lErrors == null) || (lErrors.size() == 0)) {
				if (!PropsValues.ENCODE_PASSWORD) {
					String lSubject = CDesktopComponents.vView().getLabel("login_s_account.mail.subject");
					String lBody = convertSentMailBody(CDesktopComponents.vView().getLabel("login_s_account.mail.body"),
							lUserId, lPassword, accountRequestManager.getAccountRequestName(lUserId));
					String lError = appManager.sendMail(appManager.getSysvalue(AppConstants.syskey_smtpnoreplysender),
							lEmail, null, null, lSubject, lBody);
					if ((lError != null) && (!lError.equals(""))) {
						lSpecError = lError;
						lErrors = new ArrayList<String[]>(0);
						appManager.addError(lErrors, "mail.receiver", "error_not_sent");
						accountRequestManager.deleteAccountRequest(lUserId);
						lErrorTarget = (CLoginAccountOkBtn) getFellowIfAny("okbutton");
					} else {
						CDesktopComponents.vView().showMessagebox(getRoot(),
								CDesktopComponents.vView().getLabel("login_s_account.mail.sent_body"),
								CDesktopComponents.vView().getLabel("login_s_account.mail.sent_title"), Messagebox.OK,
								Messagebox.NONE);
					}
				} else {
					CDesktopComponents.vView().showMessagebox(getRoot(),
							CDesktopComponents.vView().getLabel("login_s_account.mail.sent_body2"),
							CDesktopComponents.vView().getLabel("login_s_account.mail.sent_title"), Messagebox.OK,
							Messagebox.NONE);
				}
			} else {
				lErrorTarget = useridBox();
			}
		}
		if ((lErrors != null) && (lErrors.size() > 0)) {
			lCap.randomValue();
			CDesktopComponents.cControl().showErrors(lErrorTarget, lErrors, lSpecError);
		}
		this.detach();
	}

	/**
	 * Replaces tags in "account sent" mail body text.
	 *
	 * @param aMailBody the email body text
	 * @param aUserId   the new userid of the account
	 * @param aPassword the new password of the account
	 */
	private String convertSentMailBody(String aMailBody, String aUserId, String aPassword, String aName) {
		String lBody = aMailBody.replaceAll("<emergo-login>",
				CDesktopComponents.vView().getEmergoRootUrl() + CDesktopComponents.vView().getEmergoWebappsRoot());
		lBody = lBody.replaceAll("<userid>", Matcher.quoteReplacement(aUserId));
		lBody = lBody.replaceAll("<password>", Matcher.quoteReplacement(aPassword));
		lBody = lBody.replaceAll("<username>", Matcher.quoteReplacement(aName));
		lBody = lBody.replaceAll("<br>", "\n");
		return lBody;
	}
}
