/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import org.zkoss.zul.Button;

import nl.surf.emergo.control.def.CDefTextbox;

/**
 * The Class CBlobTextbox.
 *
 * Used to show blob.
 */
public class CBlobTextbox extends CDefTextbox {

	private static final long serialVersionUID = -5368078758229470101L;

	/** The blob preview button. */
	private Button blobPreviewBtn = null;

	/**
	 * Set blob preview button.
	 *
	 * @param aBtn the button
	 */
	public void setBlobPreviewBtn(Button aBtn) {
		blobPreviewBtn = aBtn;
	}

	/**
	 * On change update corresponding blob preview button.
	 */
	public void onChange() {
		super.onChange();
		if (blobPreviewBtn != null)
			blobPreviewBtn.setHref("");
	}

	/**
	 * Set text.
	 *
	 * @param aText the text
	 */
	public void setText(String aText) {
		super.setText(aText);
		if (blobPreviewBtn != null)
			blobPreviewBtn.setHref("");
	}

}
