/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.extra;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zk.ui.HtmlMacroComponent;
import org.zkoss.zk.ui.event.CreateEvent;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefHtml;
import nl.surf.emergo.control.run.CRunComponent;
import nl.surf.emergo.view.VView;

public class CHelpDeskEmailHtml extends CDefHtml {

	private static final long serialVersionUID = -1500553203373091960L;

	public void onCreate(CreateEvent aEvent) {
		HtmlMacroComponent macro = (HtmlMacroComponent)CRunComponent.getMacroParent(this);
		VView vView = CDesktopComponents.vView();
		String helpdeskEmail = "";
		if (macro != null) {
			helpdeskEmail = (String)macro.getDynamicProperty("a_helpdeskEmail");
		}
		if (StringUtils.isEmpty(helpdeskEmail)) {
			helpdeskEmail = vView.getLabel("helpdesk.email");
		}
		String caseInfo = "";
		if (macro != null) {
			caseInfo = (String)macro.getDynamicProperty("a_case");
			if (!StringUtils.isEmpty(caseInfo)) {
				String label = vView.getLabel("create_account.window.title");
				if (!label.equals("")) {
					caseInfo = label.replace("%1", caseInfo);
				}
			}
		}
		if (StringUtils.isEmpty(caseInfo)) {
			String labelKey = (String)getAttribute("labelKey");
			if (StringUtils.isEmpty(labelKey)) {
				caseInfo = "";
			}
			else {
				caseInfo = vView.getLabel(labelKey);
			}
		}
		setContent(vView.getLabel("create_account.message.mail_helpdesk") +
				"<br/><a href=\"mailto:"+ helpdeskEmail +
				"?Subject=" + caseInfo + 
				"\" target=\"_top\" style=\"color:white;\">" + helpdeskEmail + "</a>");
	}

}
