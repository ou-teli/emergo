package nl.surf.emergo.control.extra;

import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class CToken {

	//expiration time in minutes
    private static final int EXPIRATION = 15;

    private String token;

    private int accId;

    private List<Integer> rugId = new ArrayList<>();

    private Date expiryDate;

    public CToken() {
        super();

        setToken(new String(Base64.getEncoder().encode(UUID.randomUUID().toString().getBytes())));
        setExpiryDate(calculateExpiryDate(EXPIRATION));
    }

    public CToken(final int accId) {
        super();

        setToken(new String(Base64.getEncoder().encode(UUID.randomUUID().toString().getBytes())));
        setAccId(accId);
        setExpiryDate(calculateExpiryDate(EXPIRATION));
    }

    public CToken(final int accId, final List<Integer> rugId) {
        super();

        setToken(new String(Base64.getEncoder().encode(UUID.randomUUID().toString().getBytes())));
        setAccId(accId);
        setRugId(rugId);
        setExpiryDate(calculateExpiryDate(EXPIRATION));
    }

    public String getToken() {
        return token;
    }

    public void setToken(final String token) {
        this.token = token;
    }

    public int getAccId() {
        return accId;
    }

    public void setAccId(final int accId) {
        this.accId = accId;
    }

    public List<Integer> getRugId() {
        return rugId;
    }

    public void setRugId(final List<Integer> rugId) {
        this.rugId = rugId;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(final Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    private Date calculateExpiryDate(final int expiryTimeInMinutes) {
        final Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(new Date().getTime());
        cal.add(Calendar.MINUTE, expiryTimeInMinutes);
        return new Date(cal.getTime().getTime());
    }

    public boolean isExpired() {
        final Calendar cal = Calendar.getInstance();
        return getExpiryDate().before(cal.getTime());
    }
    
}
