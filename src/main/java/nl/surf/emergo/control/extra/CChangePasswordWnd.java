/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.extra;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.utilities.PropsValues;
import nl.surf.emergo.view.VView;

public class CChangePasswordWnd extends CAccountWnd {

	private static final long serialVersionUID = -7593314317393440586L;

	@Override
	public void onCreate(CreateEvent aEvent) {
		if (handleAccess()) {
			super.onCreate(aEvent);
		}
	}

	@Override
	public void onOk(Event aEvent) {

		errorTarget = null;
		String lMessage = "";
		boolean showMessageBox = false;
		boolean lClearFields = false;

		// NOTE token is not necessary because user is already logged in when he changes
		// his password
		Textbox lUseridBox = useridBox();
		Textbox lOldPasswordBox = oldPasswordBox();
		Textbox lPasswordBox = passwordBox();
		Textbox lPasswordrepeatBox = passwordrepeatBox();
		List<String[]> lErrors = new ArrayList<String[]>(0);

		String lUserid = lUseridBox.getValue().replace("\t", "").trim();
		String lOldPassword = lOldPasswordBox.getValue().replace("\t", "").trim();
		String lPassword = lPasswordBox.getValue().replace("\t", "").trim();
		String lPasswordrepeat = lPasswordrepeatBox.getValue().replace("\t", "").trim();

		// check valid user id and old password
		IEAccount lAccount = accountManager.getAccount(lUserid);
		boolean lPasswordOk = false;
		if (lAccount != null) {
			if (!PropsValues.ENCODE_PASSWORD) {
				lPasswordOk = lAccount.getPassword().equals(lOldPassword);
			} else {
				lPasswordOk = accountManager.matchesEncodedPassword(lOldPassword, lAccount.getPassword());
			}
		}
		if (lAccount == null || !lPasswordOk) {
			lMessage = vView.getLabel("change_password.passwords.wrong_userid_or_password");
			setErrorTarget(lUseridBox);
		}
		// check valid password
		else if (!validatePassword(lPassword, lErrors)) {
			setErrorTarget(lPasswordBox);
		}
		// password and repeated password should be equal
		else if (!lPasswordrepeat.equals(lPassword)) {
			lMessage = vView.getLabel("change_password.passwords.unequal");
			setErrorTarget(passwordrepeatBox());
		}

		if (lErrors.size() == 0 && lMessage.equals("")) {
			if (!PropsValues.ENCODE_PASSWORD) {
				lAccount.setPassword(lPassword);
			} else {
				lAccount.setPassword(accountManager.encodePassword(lPassword));
			}
			accountManager.updateAccount(lAccount);
			lMessage = vView.getLabel("change_password.password.change");
			showMessageBox = true;
			lClearFields = true;
		}

		if (lClearFields) {
			lPasswordBox.setValue("");
			lPasswordrepeatBox.setValue("");
			Div lDiv = (Div) vView.getComponent("passwordMeterInner");
			if (lDiv != null) {
				lDiv.setWidth("0px");
			}
			Label lLabel = (Label) vView.getComponent("passwordMessage");
			if (lLabel != null) {
				// NOTE label value is only set on client, so on server it is still empty.
				// Therefore first set it not empty to be able to empty it, which is transferred
				// to client by ZK.
				lLabel.setValue("DUMMY");
				lLabel.setValue("");
			}
		}

		if (lErrors.size() > 0 || !lMessage.equals("")) {
			if (!showMessageBox) {
				cControl.showErrors(errorTarget, lErrors, lMessage);
			} else {
				vView.showMessagebox(getRoot(), lMessage, vView.getLabel("change_password.message.title"),
						Messagebox.OK, Messagebox.NONE);
				vView.redirectToView(vView.getAbsoluteUrl(VView.v_login));
			}
		}
	}

}
