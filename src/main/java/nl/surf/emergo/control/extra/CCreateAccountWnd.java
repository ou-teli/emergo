/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.extra;

import java.util.ArrayList;
import java.util.Base64;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

import nl.surf.emergo.business.ICaseRoleManager;
import nl.surf.emergo.business.IRunGroupManager;
import nl.surf.emergo.business.IRunManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.crm.CCrmRunGroupAccountsHelper;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.domain.IERole;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.utilities.PropsValues;
import nl.surf.emergo.view.VView;

public class CCreateAccountWnd extends CAccountWnd {

	private static final long serialVersionUID = 6420682661284810257L;

	protected IRunManager runManager = (IRunManager) sSpring.getBean("runManager");
	protected IRunGroupManager runGroupManager = (IRunGroupManager) sSpring.getBean("runGroupManager");
	protected ICaseRoleManager caseRoleManager = (ICaseRoleManager) sSpring.getBean("caseRoleManager");

	int numberOfAttempts1 = 0;

	int numberOfAttempts2 = 0;

	int numberOfAttempts3 = 0;

	protected IEAccount account = null;
	protected List<IERunGroup> runGroups = new ArrayList<IERunGroup>();

	@Override
	public void onCreate(CreateEvent aEvent) {
		super.onCreate(aEvent);
	}

	@Override
	public void onInit(Event aEvent) {
		register = getStringAttribute("register").equals("true");
		userIdPrefix = getStringAttribute("userIdPrefix");
		oldRunIds = getStringAttribute("oldRunIds");
		runIds = getStringAttribute("runIds");
		experimentalRunIds = getStringAttribute("experimentalRunIds");
		hasExperimentalRuns = experimentalRunIds.length() > 0;
		accountRole = getStringAttribute("accountrole");
		isOuStudentId = getStringAttribute("isOuStudentId").equals("true");
		helpdeskEmail = getStringAttribute("helpdeskEmail");
		useCaptcha = getStringAttribute("useCaptcha").equals("true");
		sendAccountMail = getStringAttribute("sendAccountMail").equals("true");

		if (register) {
			int accId = 0;
			List<Integer> lRugIds = new ArrayList<>();
			String accIdAndRugId = new String(Base64.getDecoder().decode(token.getBytes()));
			if (accIdAndRugId.contains(",")) {
				String[] accIdAndRugIdArr = accIdAndRugId.split(",");
				try {
					accId = Integer.parseInt(accIdAndRugIdArr[0]);
					for (int lInd = 1; lInd < accIdAndRugIdArr.length; lInd++) {
						int lRugId = Integer.parseInt(accIdAndRugIdArr[lInd]);
						if (lRugId > 0)
							lRugIds.add(lRugId);
					}
				} catch (NumberFormatException e) {
				}
			}
			boolean tokenError = false;
			if (accId == 0 || lRugIds.isEmpty()) {
				tokenError = true;
			} else {
				account = accountManager.getAccount(accId);
				for (int lRugId : lRugIds) {
					if (lRugId > 0)
						runGroups.add(runGroupManager.getRunGroup(lRugId));
				}
				if (account == null || runGroups.isEmpty()) {
					tokenError = true;
				}
			}
			if (tokenError) {
				vView.showMessagebox(getRoot(), vView.getLabel("register.token.error"),
						vView.getLabel("register.message.title"), Messagebox.OK, Messagebox.NONE);
				vView.redirectToView(vView.getAbsoluteUrl(VView.v_login));
				return;
			}
		}

		Label titleLabel = (Label) vView.getComponent("windowTitle");
		if (titleLabel != null) {
			String windowTitle = "";
			if (!register) {
				windowTitle = getStringAttribute("case");
			} else {
				windowTitle = runGroups.get(0).getERun().getECase().getName();
			}
			if (!windowTitle.equals("")) {
				String label = vView.getLabel("create_account.window.title");
				if (!label.equals("")) {
					windowTitle = label.replace("<casename>", windowTitle);
				}
			}
			titleLabel.setValue(windowTitle);
		}

		Label passwordLabel = (Label) vView.getComponent("passwordLabel");
		if (passwordLabel != null) {
			String passwordLabelKey = "";
			if (!register) {
				passwordLabelKey = "create_account.password";
			} else {
				if (!account.getActive()) {
					passwordLabelKey = "register.password.new";
				} else {
					passwordLabelKey = "register.password.existing";
				}
			}
			passwordLabel.setValue(vView.getLabel(passwordLabelKey));
		}
		if (register) {
			studentnumberBox().setDisabled(true);
			studentnumberBox().setText(account.getUserid());
			emailBox().setDisabled(true);
			emailBox().setText(account.getEmail());
			initialsBox().setDisabled(true);
			initialsBox().setText(account.getInitials());
			nameprefixBox().setDisabled(true);
			nameprefixBox().setText(account.getNameprefix());
			lastnameBox().setDisabled(true);
			lastnameBox().setText(account.getLastname());
			passwordStrength().setVisible(!account.getActive());
			okButton().setDisabled(!account.getActive());
		}

		setVisible(!hasExperimentalRuns);
		vView.getComponent("researchPermissionWnd").setVisible(hasExperimentalRuns);

		if (account != null) {
			Clients.evalJavaScript("usePasswordMeter = " + !account.getActive() + ";");
		}
	}

	public void onUseridIsEntered(Event aEvent) {
		String userid = (String) aEvent.getData();
		IEAccount lAccount = accountManager.getAccount(userid);

		Label passwordLabel = (Label) vView.getComponent("passwordLabel");
		if (lAccount == null) {
			if (userExists) {
				//re-initialize entry fields
				emailBox().setDisabled(false);
				emailBox().setText("");
				initialsBox().setDisabled(false);
				initialsBox().setText("");
				nameprefixBox().setDisabled(false);
				nameprefixBox().setText("");
				lastnameBox().setDisabled(false);
				lastnameBox().setText("");
				if (passwordLabel != null) {
					passwordLabel.setValue(vView.getLabel("create_account.password"));
				}
				passwordBox().setText("");
				passwordStrength().setVisible(false);
				userExists = false;
				okButton().setDisabled(true);
				Clients.evalJavaScript("usePasswordMeter = true;");
			}
			return;
		}

		userExists = true;

		//studentnumberBox().setDisabled(true);
		emailBox().setDisabled(true);
		emailBox().setText(replaceCharsByStars(lAccount.getEmail()));
		initialsBox().setDisabled(true);
		initialsBox().setText(replaceCharsByStars(lAccount.getInitials()));
		nameprefixBox().setDisabled(true);
		nameprefixBox().setText(replaceCharsByStars(lAccount.getNameprefix()));
		lastnameBox().setDisabled(true);
		lastnameBox().setText(replaceCharsByStars(lAccount.getLastname()));
		if (passwordLabel != null) {
			passwordLabel.setValue(vView.getLabel("register.password.existing"));
		}
		passwordBox().setText("");
		passwordStrength().setVisible(false);
		okButton().setDisabled(false);

		Clients.evalJavaScript("usePasswordMeter = false;");
	}

	protected String replaceCharsByStars(String string) {
		String newString = "";
		for (int i = 0; i < string.length(); i++) {
			newString += "*";
		}
		return newString;
	}

	@Override
	public void onOk(Event aEvent) {
		if (!register) {
			createUser();
		} else {
			registerUser();
		}
	}

	protected void createUser() {
		String lStudentnumber = studentnumberBox().getValue().toLowerCase().replace("\t", "").trim();
		String lEmail = emailBox().getValue().toLowerCase().replace("\t", "").trim();
		String lInitials = initialsBox().getValue().replace("\t", "").trim();
		String lNameprefix = nameprefixBox().getValue().replace("\t", "").trim();
		String lLastname = lastnameBox().getValue().replace("\t", "").trim();
		String lPassword = "";
		Textbox lPasswordBox = passwordBox();
		if (PropsValues.ENCODE_PASSWORD && lPasswordBox != null) {
			lPassword = lPasswordBox.getValue().replace("\t", "").trim();
		} else {
			lPassword = accountManager.randomPassword();
		}
		String lCaptcha = captcha().getValue();
		String lCaptchainput = captchainputBox().getValue();

		errorTarget = null;
		List<String[]> lErrors = new ArrayList<String[]>(0);
		String lSpecificMessage = "";
		boolean showMessageBox = false;
		boolean lClearFields = false;

		if (useCaptcha) {
			// check valid captcha
			if (!validateCaptcha(lCaptchainput, lCaptcha, lErrors)) {
				setErrorTarget(captchainputBox());
			}
		}

		if (lErrors.size() == 0) {
			// if captcha ok
			// check valid student number
			if (!userExists && !validateStudentnumber(lStudentnumber, lErrors, isOuStudentId)) {
				setErrorTarget(studentnumberBox());
			}
			// check valid email address
			if (!userExists && !validateEmail(lEmail, lErrors)) {
				setErrorTarget(emailBox());
			}
			// check valid lastname
			if (!userExists && !validateLastname(lLastname, lErrors)) {
				setErrorTarget(lastnameBox());
			}
			if (!userExists && PropsValues.ENCODE_PASSWORD && lPasswordBox != null) {
				// check valid password
				if (!validatePassword(lPassword, lErrors)) {
					setErrorTarget(lPasswordBox);
				}
			}
		}

		List<IERun> lOldRuns = getRuns(oldRunIds);
		List<IERun> lValidRuns = null;
		if (participateInExperiment) {
			lValidRuns = getRuns(experimentalRunIds);
		} else {
			lValidRuns = getRuns(runIds);
		}
		String lActRunIds = runIds;
		if (!StringUtils.isEmpty(experimentalRunIds)) {
			if (!StringUtils.isEmpty(lActRunIds))
				lActRunIds += "," + experimentalRunIds;
			else
				lActRunIds = experimentalRunIds;
		}
		List<IERun> lActualRuns = getRuns(lActRunIds);
		IERun lRun = null;
		if (lErrors.size() == 0 && lValidRuns.size() > 0) {
			// if captcha ok and other input values too
			String lUserId = userIdPrefix + lStudentnumber;

			IEAccount lAccount = accountManager.getAccount(lUserId);
			if (userIdPrefix.equals("") && lAccount == null) {
				// NOTE earlier created users (for another Emergo case) may have 'ou_' as
				// prefix, so if prefix and account are empty try to get account using prefix
				// 'ou_'
				String lOuUserId = "ou_" + lStudentnumber;
				lAccount = accountManager.getAccount(lOuUserId);
			}

			if (lAccount != null) {
				lEmail = lAccount.getEmail();

				// if account already exists, mail must be equal
				if (lAccount.getEmail().equalsIgnoreCase(lEmail)) {
					// if email address equals existing email address
					// ok, account already exists, do nothing
				} else {
					// typo in email address or lastname? or typo/misuse in student number?
					if (numberOfAttempts1 == 0) {
						lSpecificMessage = vView.getLabel("create_account.message.account_exists_but_problems1");
						setErrorTarget(studentnumberBox());
					} else {
						lSpecificMessage = vView.getLabel("create_account.message.account_exists_but_problems2");
						setErrorTarget(studentnumberBox());
					}
					numberOfAttempts1++;
				}

				if (PropsValues.ENCODE_PASSWORD) {
					if (!accountManager.matchesEncodedPassword(lPassword, lAccount.getPassword())) {
						// typo in password?
						if (numberOfAttempts3 == 0) {
							lSpecificMessage = vView.getLabel("create_account.message.account_exists_but_problems3");
							setErrorTarget(passwordBox());
						} else {
							lSpecificMessage = vView.getLabel("create_account.message.account_exists_but_problems4");
							setErrorTarget(passwordBox());
						}
						numberOfAttempts3++;
					}
				} else
					lPassword = lAccount.getPassword();
			} else {
				// if account does not exist, mail may not exist in db.
				List<IEAccount> lAccountsByEmail = accountManager.getAllAccountsByEmail(lEmail);
				boolean lMailExists = false;
				for (IEAccount lAccountByEmail : lAccountsByEmail) {
					if (lAccountByEmail.getEmail().equalsIgnoreCase(lEmail)) {
						lMailExists = true;
						break;
					}
				}
				if (lMailExists) {
					// if email address equals existing email address
					// typo in email address? or typo/misuse in student number? What to do? Ask to
					// check input. If problem persists suggest to send email to admin.
					if (numberOfAttempts2 == 0) {
						lSpecificMessage = vView
								.getLabel("create_account.message.account_does_not_exists_but_problems1");
						setErrorTarget(emailBox());
					} else {
						lSpecificMessage = vView
								.getLabel("create_account.message.account_does_not_exists_but_problems2");
						setErrorTarget(emailBox());
					}
					numberOfAttempts2++;
				} else {
					// ok, add account
					lAccount = accountManager.getNewAccount();
					// just choose one admin as parent of this account
					List<IEAccount> lAdmins = accountManager.getAllAccounts(true, AppConstants.c_role_adm);
					int lAdmId = 0;
					if ((lAdmins != null) && (lAdmins.size() > 0))
						lAdmId = (lAdmins.get(0)).getAccId();
					lAccount.setAccAccId(lAdmId);
					lAccount.setUserid(lUserId);
					if (!PropsValues.ENCODE_PASSWORD) {
						lAccount.setPassword(lPassword);
					} else {
						lAccount.setPassword(accountManager.encodePassword(lPassword));
					}
					lAccount.setStudentid(lStudentnumber);
					lAccount.setTitle("");
					lAccount.setInitials(lInitials);
					lAccount.setNameprefix(lNameprefix);
					lAccount.setLastname(lLastname);
					lAccount.setEmail(lEmail);
					lAccount.setPhonenumber("");
					lAccount.setJob("");
					lAccount.setExtradata("");
					lAccount.setActive(!PropsValues.ENCODE_PASSWORD);
					lAccount.setOpenaccess(false);
					// set student role for this account
					List<IERole> lAllRoles = accountManager.getAllRoles();
					if (lAllRoles != null) {
						Set<IERole> lRoles = new HashSet<IERole>();
						for (IERole lObject : lAllRoles) {
							String lCode = lObject.getCode();
							if (lCode.equals(AppConstants.c_role_stu))
								lRoles.add(lObject);
						}
						lAccount.setERoles(lRoles);
					}
					accountManager.newAccount(lAccount);
				}
			}

			if (lErrors.size() == 0 && lSpecificMessage.equals("")) {
				// only search in active runs for active run groups:
				lRun = getCurrentRun(lAccount, lOldRuns);
				// if member of old run then do not add to one of the current runs
				boolean lIsMemberOfOldRun = false;
				if (lRun == null) {
					// if not member of old run check if member of one the current runs
					lRun = getCurrentRun(lAccount, lActualRuns);
				} else {
					lIsMemberOfOldRun = true;
				}
				boolean lRunGroupIsAdded = false;
				List<Integer> lRugIds = new ArrayList<Integer>();
				if (lRun == null) {
					// if not member of old run or one the current runs then get run to assign to
					lRun = getRunToAssignTo(lValidRuns);
					List<IECaseRole> caseRoles = caseRoleManager.getAllCaseRolesByCasId(lRun.getECase().getCasId(),
							false);
					CCrmRunGroupAccountsHelper cHelper = new CCrmRunGroupAccountsHelper();
					for (IECaseRole lCaseRole : caseRoles) {
						if ((accountRole).indexOf("'" + lCaseRole.getName() + "'") >= 0) {
							IERunGroup lRunGroup = cHelper.getRungroup(lAccount.getAccId(), lRun.getRunId(),
									lCaseRole.getCarId());
							if (lRunGroup == null) {
								lRunGroup = cHelper.addRungroup(lAccount, lRun, lCaseRole,
										!PropsValues.ENCODE_PASSWORD);
								if (lRunGroup != null) {
									IERunGroupAccount lRunGroupAccount = cHelper.addRungroupaccount(lAccount,
											lRunGroup);
									if (lRunGroupAccount != null) {
										// lRugId = lRunGroup.getRugId();
										lRugIds.add(lRunGroup.getRugId());
										lRunGroupIsAdded = true;
									}
								}
							} else {
								// lRunGroup is not active
								if (!lRunGroup.getActive()) {
									lRugIds.add(lRunGroup.getRugId());
									// lRunGroup.setActive(true);
									// runGroupManager.updateRunGroup(lRunGroup);
									// resend information:
									lRunGroupIsAdded = true;
								}
							}
						}
					}
				}
				if (!lRunGroupIsAdded && !lIsMemberOfOldRun) {
					// if student already is run member of current run
					lSpecificMessage = vView.getLabel("create_account.message.rungroup_already_exists");
					showMessageBox = true;
				} else {
					if (sendAccountMail) {
						String lMailError = "";
						if (!PropsValues.ENCODE_PASSWORD) {
							lMailError = sendAccountMail(lAccount, lRun, !lIsMemberOfOldRun, lPassword);
						} else {
							// set token
							CToken token = createToken(lAccount.getAccId(), lRugIds);
							lMailError = sendCompleteRegistrationMail(lAccount.getEmail(), lRun,
									lRun.getECase().getName(), lRun.getName(), token.getToken(), !lIsMemberOfOldRun);
						}
						String lMessagePart1 = "";
						String lMessagePart2 = "";
						if (lIsMemberOfOldRun) {
							lMessagePart1 = "resend account details for run ";
							lMessagePart2 = "";
						} else {
							lMessagePart1 = "create account by user for run ";
							lMessagePart2 = "; notification to crm: " + lRun.getEAccount().getEmail();
						}
						if (lMailError.equals("")) {
							sSpring.getSLogHelper()
									.logAction(lMessagePart1 + lRun.getRunId() + ": accountmail sent to account id: "
											+ lAccount.getAccId() + "; mail address: " + lAccount.getEmail()
											+ lMessagePart2);
							if (!PropsValues.ENCODE_PASSWORD) {
								lSpecificMessage = vView.getLabel("create_account.message.rungroup_added");
							} else {
								lSpecificMessage = vView.getLabel("create_account.message.rungroup_added2");
							}
							lClearFields = true;
							showMessageBox = true;
						} else {
							sSpring.getSLogHelper()
									.logAction(lMessagePart1 + lRun.getRunId()
											+ ": error in sending accountmail to account id: " + lAccount.getAccId()
											+ "; mail address: " + lAccount.getEmail() + lMessagePart2 + "; error: "
											+ lMailError);
							lSpecificMessage = vView.getLabel("create_account.message.sendmail_problem");
							setErrorTarget(okButton());
						}
					} else {
						if (!PropsValues.ENCODE_PASSWORD) {
							lSpecificMessage = vView.getLabel("create_account.message.rungroup_added");
						} else {
							lSpecificMessage = vView.getLabel("create_account.message.rungroup_added2");
						}
						setErrorTarget(okButton());
						lClearFields = true;
						showMessageBox = true;
					}
				}
			}
		}

		if (lClearFields) {
			studentnumberBox().setValue("");
			emailBox().setValue("");
			initialsBox().setValue("");
			nameprefixBox().setValue("");
			lastnameBox().setValue("");

			if (lPasswordBox != null) {
				lPasswordBox.setValue("");
			}
			Div lDiv = (Div) vView.getComponent("passwordMeterInner");
			if (lDiv != null) {
				lDiv.setWidth("0px");
			}
			Label lLabel = (Label) vView.getComponent("passwordMessage");
			if (lLabel != null) {
				// NOTE label value is only set on client, so on server it is still empty.
				// Therefore first set it not empty to be able to empty it, which is transferred
				// to client by ZK.
				lLabel.setValue("DUMMY");
				lLabel.setValue("");
			}
		}
		captcha().randomValue();
		captchainputBox().setValue("");

		if (lErrors.size() > 0 || !lSpecificMessage.equals("")) {
			String lCaseName = "";
			String lRunName = "";
			if (lRun != null) {
				lRunName = lRun.getName();
				lCaseName = lRun.getECase().getName();
			}
			lSpecificMessage = convertMessage(lSpecificMessage, lCaseName, lRunName, lEmail, helpdeskEmail);
			if (lErrors.size() > 0 || !showMessageBox) {
				cControl.showErrors(errorTarget, lErrors, lSpecificMessage);
			} else {
				vView.showMessagebox(getRoot(), lSpecificMessage, vView.getLabel("create_account.message.title"),
						Messagebox.OK, Messagebox.NONE);
				vView.redirectToView(vView.getAbsoluteUrl(VView.v_login));
			}
		}
	}

	protected void registerUser() {
		Textbox lPasswordBox = passwordBox();
		String lPassword = lPasswordBox.getValue().replace("\t", "").trim();
		String lCaptcha = captcha().getValue();
		String lCaptchainput = captchainputBox().getValue();

		errorTarget = null;
		List<String[]> lErrors = new ArrayList<String[]>(0);
		String lSpecificMessage = "";
		boolean showMessageBox = false;
		boolean lClearFields = false;

		if (useCaptcha) {
			// check valid captcha
			if (!validateCaptcha(lCaptchainput, lCaptcha, lErrors)) {
				setErrorTarget(captchainputBox());
			}
		}
		// check valid password if new account
		if (!account.getActive() && !validatePassword(lPassword, lErrors)) {
			setErrorTarget(lPasswordBox);
		}

		if (lErrors.size() == 0) {
			if (account.getActive()) {
				// if account is active password is set already, so check if entered password
				// equals existing password
				boolean passwordCorrect = false;
				if (!PropsValues.ENCODE_PASSWORD) {
					passwordCorrect = lPassword.equals(account.getPassword());
				} else {
					passwordCorrect = accountManager.matchesEncodedPassword(lPassword, account.getPassword());
				}
				if (!passwordCorrect) {
					lSpecificMessage = vView.getLabel("register.message.wrong_password");
				}
			} else {
				// if account is not active password is not set yet, so set password. Password
				// already is validated in ZUL file
				if (!PropsValues.ENCODE_PASSWORD) {
					account.setPassword(lPassword);
				} else {
					account.setPassword(accountManager.encodePassword(lPassword));
				}
				accountManager.updateAccount(account);
			}
		}

		if (lErrors.size() == 0 && lSpecificMessage.equals("")) {
			boolean lRGActive = false;
			for (IERunGroup lRunGroup : runGroups) {
				if (lRunGroup.getActive())
					lRGActive = true;
			}
			if (lRGActive) {
				// account already has access to case
				showMessageBox = true;
				lSpecificMessage = vView.getLabel("register.message.already_access");
				// don't send mail because already access
				sendAccountMail = false;
			}

			if (sendAccountMail) {
				String lMailError = "";
				if (!PropsValues.ENCODE_PASSWORD) {
					lMailError = sendAccountMail(account, runGroups.get(0).getERun(), true, lPassword);
				} else {
					// set token
					List<Integer> lRugIds = new ArrayList<Integer>();
					for (IERunGroup lRunGroup : runGroups) {
						lRugIds.add(lRunGroup.getRugId());
					}
					CToken token = createToken(account.getAccId(), lRugIds);
					lMailError = sendCompleteRegistrationMail(account.getEmail(), runGroups.get(0).getERun(),
							runGroups.get(0).getERun().getECase().getName(), runGroups.get(0).getERun().getName(), token.getToken(), true);
				}
				String lMessagePart1 = "create account by user for run ";
				String lMessagePart2 = "; notification to crm: " + runGroups.get(0).getERun().getEAccount().getEmail();
				if (lMailError.equals("")) {
					sSpring.getSLogHelper()
							.logAction(lMessagePart1 + runGroups.get(0).getERun().getRunId()
									+ ": accountmail sent to account id: " + account.getAccId() + "; mail address: "
									+ account.getEmail() + lMessagePart2);
					if (!PropsValues.ENCODE_PASSWORD) {
						lSpecificMessage = vView.getLabel("create_account.message.rungroup_added");
					} else {
						lSpecificMessage = vView.getLabel("create_account.message.rungroup_added2");
					}
					lClearFields = true;
					showMessageBox = true;
				} else {
					sSpring.getSLogHelper().logAction(lMessagePart1 + runGroups.get(0).getERun().getRunId()
							+ ": error in sending accountmail to account id: " + account.getAccId() + "; mail address: "
							+ account.getEmail() + lMessagePart2 + "; error: " + lMailError);
					lSpecificMessage = vView.getLabel("create_account.message.sendmail_problem");
					setErrorTarget(okButton());
				}
			}
		}

		if (lClearFields) {
			if (lPasswordBox != null) {
				lPasswordBox.setValue("");
			}
			Div lDiv = (Div) vView.getComponent("passwordMeterInner");
			if (lDiv != null) {
				lDiv.setWidth("0px");
			}
			Label lLabel = (Label) vView.getComponent("passwordMessage");
			if (lLabel != null) {
				// NOTE label value is only set on client, so on server it is still empty.
				// Therefore first set it not empty to be able to empty it, which is transferred
				// to client by ZK.
				lLabel.setValue("DUMMY");
				lLabel.setValue("");
			}
		}
		captcha().randomValue();
		captchainputBox().setValue("");

		lSpecificMessage = convertMessage(lSpecificMessage, runGroups.get(0).getERun().getECase().getName(), runGroups.get(0).getERun().getName(),
				account.getEmail(), helpdeskEmail);
		if (lErrors.size() > 0 || !showMessageBox) {
			cControl.showErrors(errorTarget, lErrors, lSpecificMessage);
		} else {
			vView.showMessagebox(getRoot(), lSpecificMessage, vView.getLabel("create_account.message.title"),
					Messagebox.OK, Messagebox.NONE);
			vView.redirectToView(vView.getAbsoluteUrl(VView.v_login));
		}
	}

	private List<IERun> getRuns(String aRunIds) {
		List<IERun> lRuns = new ArrayList<IERun>();
		if (!StringUtils.isEmpty(aRunIds)) {
			String[] lRunIdArr = aRunIds.split(",");
			for (int i = 0; i < lRunIdArr.length; i++) {
				try {
					int lRunId = Integer.parseInt(lRunIdArr[i]);
					IERun lRun = runManager.getRun(lRunId);
					if (lRun != null) {
						lRuns.add(lRun);
					}
				} catch (NumberFormatException e) {
				}
			}
		}
		return lRuns;
	}

	private IERun getCurrentRun(IEAccount aAccount, List<IERun> aRuns) {
		CCrmRunGroupAccountsHelper cHelper = new CCrmRunGroupAccountsHelper();
		for (IERun lRun : aRuns) {
			if (lRun.getActive()) {
				List<IECaseRole> caseRoles = caseRoleManager.getAllCaseRolesByCasId(lRun.getECase().getCasId(), false);
				for (IECaseRole lCaseRole : caseRoles) {
					IERunGroup lRunGroup = cHelper.getRungroup(aAccount.getAccId(), lRun.getRunId(),
							lCaseRole.getCarId());
					if ((lRunGroup != null) && lRunGroup.getActive()) {
						// if account exists in run return run
						return lRun;
					}
				}
			}
		}
		return null;
	}

	private IERun getRunToAssignTo(List<IERun> aRuns) {
		// return run with least members
		int lMin = Integer.MAX_VALUE;
		IERun lMinRun = null;
		for (IERun lRun : aRuns) {
			if (lRun.getActive()) {
				int lNumber = runGroupManager.getAllRunGroupsByRunId(lRun.getRunId()).size();
				if (lNumber < lMin) {
					// if multiple runs with same minimum, the first one is returned
					lMin = lNumber;
					lMinRun = lRun;
				}
			}
		}
		return lMinRun;
	}

	private String convertMessage(String aMessage, String aCaseName, String aRunName, String aEmail, String aHelpdeskEmail) {
		String lText = aMessage.replace("<emergo-login>", vView.getEmergoRootUrl() + vView.getEmergoWebappsRoot());
		lText = lText.replace("<casename>", Matcher.quoteReplacement(aCaseName));
		lText = lText.replace("<runname>", Matcher.quoteReplacement(aRunName));
		lText = lText.replace("<email>", aEmail);
		lText = lText.replace("<helpdeskemail>", aHelpdeskEmail);
		lText = lText.replace("<br>", "\n");
		return lText;
	}

	protected String sendAccountMail(IEAccount aAccount, IERun aRun, boolean aNotifyCrm, String aPassword) {
		String lUserName = accountManager.getAccountName(aAccount);
		String lCaseName = "";
		String lRunName = "";
		String lMailsubject = vView.getLabel("mail.access_to_run.subject");
		String lMailbody = vView.getLabel("mail.access_to_run.body");
		if (aRun != null) {
			lRunName = aRun.getName();
			lCaseName = aRun.getECase().getName();
			if (!aRun.getAccessmailsubject().equals("")) {
				lMailsubject = aRun.getAccessmailsubject();
			}
			if (!aRun.getAccessmailbody().equals("")) {
				lMailbody = aRun.getAccessmailbody();
			}
		}
		String lSubject = convertMailText(lMailsubject, aAccount.getUserid(), aPassword, lUserName, lCaseName, lRunName);
		String lBody = convertMailText(lMailbody, aAccount.getUserid(), aPassword, lUserName, lCaseName, lRunName);
		String lCrmMailAddress = "";
		if (aNotifyCrm) {
			lCrmMailAddress = aRun.getEAccount().getEmail();
		}
		return appManager.sendMail(appManager.getSysvalue(AppConstants.syskey_smtpnoreplysender), aAccount.getEmail(),
				null, lCrmMailAddress, lSubject, lBody);
	}

	private String convertMailText(String aMailText, String aUserId, String aPassword, String aUserName,
			String aCaseName, String aRunName) {
		String lText = aMailText.replace("<emergo-login>", vView.getEmergoRootUrl() + vView.getEmergoWebappsRoot());
		lText = lText.replace("<userid>", Matcher.quoteReplacement(aUserId));
		lText = lText.replace("<password>", Matcher.quoteReplacement(aPassword));
		lText = lText.replace("<username>", Matcher.quoteReplacement(aUserName));
		lText = lText.replace("<casename>", Matcher.quoteReplacement(aCaseName));
		lText = lText.replace("<runname>", Matcher.quoteReplacement(aRunName));
		lText = lText.replace("<br>", "\n");
		return lText;
	}

	protected String sendCompleteRegistrationMail(String aEmail, IERun aRun, String aCaseName, String aRunName, String aToken,
			boolean aNotifyCrm) {
		String lSubject = convertMailText(vView.getLabel("create_account.mail.subject"), aCaseName, aRunName, aToken);
		String lBody = convertMailText(vView.getLabel("create_account.mail.body"), aCaseName, aRunName, aToken);
		String lCrmMailAddress = "";
		if (aNotifyCrm) {
			lCrmMailAddress = aRun.getEAccount().getEmail();
		}
		return appManager.sendMail(appManager.getSysvalue(AppConstants.syskey_smtpnoreplysender), aEmail, null,
				lCrmMailAddress, lSubject, lBody);
	}

	private String convertMailText(String aMailText, String aCaseName, String aRunName, String aToken) {
		String lText = aMailText.replace("<casename>", Matcher.quoteReplacement(aCaseName));
		lText = lText.replace("<runname>", Matcher.quoteReplacement(aRunName));
		lText = lText.replace("<complete-registration>", vView.getEmergoRootUrl() + vView.getEmergoWebappsRoot()
				+ "/extra/complete_registration.zul?token=" + aToken);
		lText = lText.replace("<br>", "\n");
		return lText;
	}

}
