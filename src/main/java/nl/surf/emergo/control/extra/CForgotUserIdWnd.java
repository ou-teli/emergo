/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.extra;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Messagebox;

import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.view.VView;

public class CForgotUserIdWnd extends CAccountWnd {

	private static final long serialVersionUID = -7201809023487509224L;

	public void onCreate(CreateEvent aEvent) {
		super.onCreate(aEvent);
	}

	@Override
    public void onOk(Event aEvent) {
		String lEmail = emailBox().getValue().toLowerCase().replace("\t", "").trim();

		errorTarget = null;
		List<String[]> lErrors = new ArrayList<String[]>(0);
		String lSpecificMessage = "";
		boolean showMessageBox = false;
		boolean lClearFields = false;
		
		if (lErrors.size() == 0) {
			//check valid email address
			if (!validateEmail(lEmail, lErrors)) {
				setErrorTarget(emailBox());
			}
		}
		
		if (lErrors.size() == 0) {
			//if check if mail exists in db.
			List<IEAccount> lAccountsByEmail = accountManager.getAllAccountsByEmail(lEmail);
			boolean lMailExists = lAccountsByEmail.size() > 0;
			List<String> lUserIds = new ArrayList<String>();
			for (IEAccount lAccount : lAccountsByEmail) {
				lUserIds.add(lAccount.getUserid());
			}

			//NOTE to prevent misuse: if mail does not exist, act as if mail is found but don't send a mail with userid
			if (lErrors.size() == 0 && lSpecificMessage.equals("")) {
				if (lMailExists) {
					CToken token = createToken(lAccountsByEmail.get(0).getAccId());
					sendForgotUserIdMail(lEmail, lUserIds, token.getToken());
				}
				
				lSpecificMessage = vView.getLabel("forget_userid.message.mail_sent").replaceAll("<email>", lEmail);
				setErrorTarget(okButton());
				lClearFields = true;
				showMessageBox = true;
			}
		}

		if (lClearFields) {
			emailBox().setValue("");
		}

		if (lErrors.size() > 0 || !lSpecificMessage.equals("")) {
			if (lErrors.size() > 0 || !showMessageBox) {
				cControl.showErrors(errorTarget, lErrors, lSpecificMessage);
			}
			else {
				vView.showMessagebox(getRoot(), lSpecificMessage, vView.getLabel("forget_userid.message.title"), Messagebox.OK, Messagebox.NONE);
				vView.redirectToView(vView.getAbsoluteUrl(VView.v_login));
			}
		}
	}

	protected String sendForgotUserIdMail(String aEmail, List<String> aUserIds, String aToken) {
		String lSubject = vView.getLabel("forget_userid.mail.subject");
		String lBody = "";
		if (aUserIds.size() == 1) {
			lBody = vView.getLabel("forget_userid.mail.body");
		}
		else {
			lBody = vView.getLabel("forget_userid.mail.body.m");
		}
		lBody = convertMailText(aEmail, lBody, aUserIds, aToken);
		return appManager.sendMail(appManager.getSysvalue(AppConstants.syskey_smtpnoreplysender), aEmail, null, null, lSubject, lBody);
	}

	private String convertMailText(String aEmail, String aMailText, List<String> aUserIds, String aToken) {
		String lText = aMailText.replaceAll("<email>", aEmail); 
		String lUserIds = "";
		for (String lUserId : aUserIds) {
			if (!lUserIds.equals("")) {
				lUserIds += "\n";
			}
			lUserIds += lUserId;
		}
		lText = lText.replaceAll("<userids>", lUserIds); 
		lText = lText.replaceAll("<br>","\n");
		return lText;
	}

}
