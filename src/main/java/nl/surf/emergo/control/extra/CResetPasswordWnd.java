/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.extra;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.utilities.PropsValues;
import nl.surf.emergo.view.VView;

public class CResetPasswordWnd extends CAccountWnd {

	private static final long serialVersionUID = -6842924633631517224L;

	@Override
	public void onCreate(CreateEvent aEvent) {
		super.onCreate(aEvent);
	}

	@Override
	public void onOk(Event aEvent) {

		errorTarget = null;
		String lMessage = "";
		boolean expired = false;
		boolean showMessageBox = false;
		boolean lClearFields = false;

		// token must be found
		boolean tokenOk = true;
		if (token == null) {
			tokenOk = false;
		}
		int accId = 0;
		if (tokenOk) {
			CToken tokenInstance = getTokenByToken(token);
			if (tokenInstance == null) {
				// 15 minutes have past
				tokenOk = false;
			} else {
				accId = tokenInstance.getAccId();
			}
		}
		if (!tokenOk) {
			lMessage = vView.getLabel("reset_password.token.expired");
			expired = true;
			showMessageBox = true;
			lClearFields = true;
		}

		Textbox lPasswordBox = passwordBox();
		Textbox lPasswordrepeatBox = passwordrepeatBox();
		List<String[]> lErrors = new ArrayList<String[]>(0);
		if (tokenOk) {
			String lPassword = lPasswordBox.getValue().replace("\t", "").trim();
			String lPasswordrepeat = lPasswordrepeatBox.getValue().replace("\t", "").trim();

			// check valid password
			if (!validatePassword(lPassword, lErrors)) {
				setErrorTarget(lPasswordBox);
			}

			// password and repeated password should be equal
			if (!lPasswordrepeat.equals(lPassword)) {
				lMessage = vView.getLabel("reset_password.passwords.unequal");
				setErrorTarget(passwordrepeatBox());
			}

			if (lErrors.size() == 0 && lMessage.equals("")) {
				IEAccount lAccount = accountManager.getAccount(accId);
				if (!PropsValues.ENCODE_PASSWORD) {
					lAccount.setPassword(lPassword);
				} else {
					lAccount.setPassword(accountManager.encodePassword(lPassword));
				}
				accountManager.updateAccount(lAccount);
				lMessage = vView.getLabel("reset_password.password.reset");
				showMessageBox = true;
				lClearFields = true;

				removeToken();
			}
		}

		if (lClearFields) {
			lPasswordBox.setValue("");
			lPasswordrepeatBox.setValue("");
			Div lDiv = (Div) vView.getComponent("passwordMeterInner");
			if (lDiv != null) {
				lDiv.setWidth("0px");
			}
			Label lLabel = (Label) vView.getComponent("passwordMessage");
			if (lLabel != null) {
				// NOTE label value is only set on client, so on server it is still empty.
				// Therefore first set it not empty to be able to empty it, which is transferred
				// to client by ZK.
				lLabel.setValue("DUMMY");
				lLabel.setValue("");
			}
		}

		if (lErrors.size() > 0 || !lMessage.equals("")) {
			if (!showMessageBox) {
				cControl.showErrors(errorTarget, lErrors, lMessage);
			} else {
				vView.showMessagebox(getRoot(), lMessage, vView.getLabel("reset_password.message.title"), Messagebox.OK,
						Messagebox.NONE);
				if (expired) {
					vView.redirectToView(vView.getAbsoluteUrl("extra/forgot_password.zul"));
				} else {
					vView.redirectToView(vView.getAbsoluteUrl(VView.v_login));
				}
			}
		}
	}

}
