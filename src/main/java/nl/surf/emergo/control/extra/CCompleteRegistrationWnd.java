/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.extra;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Messagebox;

import nl.surf.emergo.business.IRunGroupManager;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.view.VView;

public class CCompleteRegistrationWnd extends CAccountWnd {

	private static final long serialVersionUID = 31722642050124969L;

	protected IRunGroupManager runGroupManager = (IRunGroupManager)sSpring.getBean("runGroupManager");

	public void onCreate(CreateEvent aEvent) {
		super.onCreate(aEvent);
	}

	@Override
    public void onOk(Event aEvent) {
		
		errorTarget = null;
		String lMessage = "";
		boolean showMessageBox = false;
		boolean lClearFields = false;
		
		//token must be found
		boolean tokenOk = true;
		if (token == null) {
			tokenOk = false;
		}
		int accId = 0;
//		int rugId = 0;
		List<Integer> lRugIds = new ArrayList<Integer>();
		if (tokenOk) {
			CToken tokenInstance = getTokenByToken(token);
			if (tokenInstance == null) {
				//15 minutes have past
				tokenOk = false;
			}
			else {
				accId = tokenInstance.getAccId();
				lRugIds = tokenInstance.getRugId();
				//rugId = lRugIds.get(0);
			}
		}
		if (!tokenOk) {
			lMessage = vView.getLabel("complete_registration.token.expired");
			showMessageBox = true;
			lClearFields = true;
		}
		
		if (tokenOk) {
			if (lMessage.equals("")) {
				IEAccount lAccount = accountManager.getAccount(accId);
				if (lAccount != null) {
					lAccount.setActive(true);
					accountManager.updateAccount(lAccount);
				}
				List<IERunGroup> lRunGroups = new ArrayList<IERunGroup>();
				for (int lRugId : lRugIds) {
					if (lRugId > 0) {
						IERunGroup lRunGroup = runGroupManager.getRunGroup(lRugId);
						if (lRunGroup != null) {
							lRunGroup.setActive(true);
							runGroupManager.updateRunGroup(lRunGroup);
							lRunGroups.add(lRunGroup);
						}
					}
				}
				
				lMessage = vView.getLabel("complete_registration.registration.completed");
				if (lRunGroups.size() > 0) {
					lMessage = lMessage.replace("<casename>", lRunGroups.get(0).getERun().getECase().getName());
				}
				showMessageBox = true;
				lClearFields = true;
				
				removeToken();
			}
		}

		if (lClearFields) {
		}

		if (!lMessage.equals("")) {
			if (!showMessageBox) {
				List<String[]> lErrors = new ArrayList<String[]>(0);
				cControl.showErrors(errorTarget, lErrors, lMessage);
			}
			else {
				vView.showMessagebox(getRoot(), lMessage, vView.getLabel("complete_registration.message.title"), Messagebox.OK, Messagebox.NONE);
				vView.redirectToView(vView.getAbsoluteUrl(VView.v_login));
			}
		}
	}

}
