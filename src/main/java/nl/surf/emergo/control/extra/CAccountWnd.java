/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.extra;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.zkoss.web.Attributes;
import org.zkoss.zk.ui.AbstractComponent;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Captcha;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Textbox;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.IAppManager;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CWnd;
import nl.surf.emergo.utilities.pwned.PwndHelper;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpring;

public class CAccountWnd extends CWnd {
	private static final long serialVersionUID = 8959721816629427283L;

	protected VView vView = CDesktopComponents.vView();

	protected CControl cControl = CDesktopComponents.cControl();

	protected SSpring sSpring = CDesktopComponents.sSpring();

	private PwndHelper pwndHelper = new PwndHelper();

	public boolean register = false;
	public String userIdPrefix = "";
	public String oldRunIds = "";
	public String runIds = "";
	public String experimentalRunIds = "";
	public boolean hasExperimentalRuns = false;
	public String accountRole = "";
	public boolean isOuStudentId = true;
	public String helpdeskEmail = "";
	public boolean useCaptcha = true;
	public boolean sendAccountMail = true;
	public boolean participateInExperiment = false;

	public boolean userExists = false;

	protected IAppManager appManager = (IAppManager) sSpring.getBean("appManager");
	protected IAccountManager accountManager = (IAccountManager) sSpring.getBean("accountManager");

	protected AbstractComponent errorTarget = null;

	protected static final String tokenInstances = "tokenInstances";

	protected String token = "";

	protected List<CToken> getTokenInstances() {
		if (sSpring.getAppContainer().get(tokenInstances) == null)
			sSpring.getAppContainer().put(tokenInstances, new ArrayList<CToken>(0));
		return (List<CToken>) sSpring.getAppContainer().get(tokenInstances);
	}

	protected CToken createToken(int accId, List<Integer> rugIds) {
		// cleanup: remove expired tokens for all users
		removeExpiredTokens();
		// token is UUID so should not exist yet, but try to remove it anyway
		removeToken();
		CToken tokenInstance = new CToken(accId, rugIds);
		token = tokenInstance.getToken();
		getTokenInstances().add(tokenInstance);
		return tokenInstance;
	}

	protected CToken createToken(int accId) {
		List<Integer> lRugIds = new ArrayList<Integer>();
		lRugIds.add(0);
		return createToken(accId, lRugIds);
	}

	protected CToken getTokenByToken(String token) {
		// cleanup: remove expired tokens for all users
		removeExpiredTokens();
		for (CToken tokenInstance : getTokenInstances()) {
			if (tokenInstance.getToken().equals(token)) {
				return tokenInstance;
			}
		}
		return null;
	}

	protected List<CToken> getTokensByAccId(int accId) {
		// cleanup: remove expired tokens for all users
		removeExpiredTokens();
		List<CToken> tokenInstances = new ArrayList<CToken>();
		for (CToken tokenInstance : getTokenInstances()) {
			if (tokenInstance.getAccId() == accId) {
				tokenInstances.add(tokenInstance);
			}
		}
		return tokenInstances;
	}

	protected void removeToken() {
		// cleanup: remove expired tokens for all users
		removeExpiredTokens();
		CToken tokenInstance = getTokenByToken(token);
		if (tokenInstance != null) {
			getTokenInstances().remove(tokenInstance);
			token = "";
		}
	}

	protected void removeExpiredTokens() {
		for (int i = (getTokenInstances().size() - 1); i >= 0; i--) {
			if (getTokenInstances().get(i).isExpired()) {
				getTokenInstances().remove(i);
			}
		}
	}

	public String getStringAttribute(String key) {
		Object object = getAttribute(key);
		if (object instanceof String) {
			return object == null ? "" : (String) object;
		}
		return "";
	}

	protected boolean handleAccess() {
		boolean hasAccess = false;
		if (CDesktopComponents.cControl().getAccId() > 0) {
			String role = (String) getAttribute("role");
			if (role != null) {
				if (role.equals("*")) {
					hasAccess = true;
				} else {
					hasAccess = CDesktopComponents.sSpring().hasRole(role);
				}
			}
		}
		if (hasAccess) {
			setVisible(true);
		} else {
			CDesktopComponents.vView().forbidden();
		}
		return hasAccess;
	}

	public void onCreate(CreateEvent aEvent) {
		// NOTE if both locale string values are empty, it corresponds to Dutch
		String localeLanguage = "";
		String localeCountry = "";

		// set locale if not equal to current locale
		String currentLocaleLanguage = "";
		String currentLocaleCountry = "";
		Locale currentLocale = (Locale) cControl.getSession().getAttribute(Attributes.PREFERRED_LOCALE);
		if (currentLocale != null) {
			currentLocaleLanguage = currentLocale.getLanguage();
			currentLocaleCountry = currentLocale.getCountry();
		}
		if (!localeLanguage.equals(currentLocaleLanguage) || !localeCountry.equals(currentLocaleCountry)) {
			Locale locale = new Locale(localeLanguage, localeCountry);
			cControl.getSession().setAttribute(Attributes.PREFERRED_LOCALE, locale);
			vView.reloadView();
		}

		token = VView.getReqPar("token");
		if (token == null) {
			token = "";
		}

		doOverlapped();
		setPosition("center,center");
		setVisible(false);

		Events.echoEvent("onInit", this, null);
	}

	public void onInit(Event aEvent) {
		setVisible(true);
	}

	protected Textbox studentnumberBox() {
		return (Textbox) getFellowIfAny("studentnumber");
	}

	protected Textbox emailBox() {
		return (Textbox) getFellowIfAny("email");
	}

	protected Textbox initialsBox() {
		return (Textbox) getFellowIfAny("initials");
	}

	protected Textbox nameprefixBox() {
		return (Textbox) getFellowIfAny("nameprefix");
	}

	protected Textbox lastnameBox() {
		return (Textbox) getFellowIfAny("lastname");
	}

	protected Textbox passwordBox() {
		return (Textbox) getFellowIfAny("password");
	}

	protected Hbox passwordStrength() {
		return (Hbox) getFellowIfAny("passwordStrength");
	}

	protected Textbox passwordrepeatBox() {
		return (Textbox) getFellowIfAny("passwordrepeat");
	}

	protected Captcha captcha() {
		return (Captcha) getFellowIfAny("captcha");
	}

	protected Textbox captchainputBox() {
		return (Textbox) getFellowIfAny("captchainput");
	}

	protected Textbox useridBox() {
		return (Textbox) getFellowIfAny("userid");
	}

	protected Textbox oldPasswordBox() {
		return (Textbox) getFellowIfAny("oldPassword");
	}

	protected Button okButton() {
		return (Button) getFellowIfAny("okbutton");
	}

	protected boolean validateCaptcha(String aInputValue, String aValue, List<String[]> aErrors) {
		if (!aInputValue.equalsIgnoreCase(aValue)) {
			appManager.addError(aErrors, "captcha", "error_not_equal");
			return false;
		}
		return true;
	}

	protected boolean validateStudentnumber(String aInputValue, List<String[]> aErrors, boolean aIsOuStudentId) {
		if (aInputValue == null) {
			return false;
		} else {
			if (aIsOuStudentId) {
				if (aInputValue.length() != 9 || !aInputValue.matches("\\d{9}")) {
					appManager.addError(aErrors, "studentnumber", "error_not_valid");
					return false;
				}
			} else {
				if (aInputValue.equals("")) {
					appManager.addError(aErrors, "studentnumber", "error_not_valid");
					return false;
				}
			}
		}
		return true;
	}

	protected boolean validateEmail(String aInputValue, List<String[]> aErrors) {
		if (aInputValue == null || !aInputValue.matches(".+@.+\\.[a-z]+")) {
			appManager.addError(aErrors, "mail.receiver", "error_not_valid");
			return false;
		}
		return true;
	}

	protected boolean validateLastname(String aInputValue, List<String[]> aErrors) {
		if (aInputValue == null || aInputValue.equals("")) {
			appManager.addError(aErrors, "lastname", "error_empty");
			return false;
		}
		return true;
	}

	protected boolean validateUserid(String aInputValue, List<String[]> aErrors) {
		if (aInputValue == null || aInputValue.equals("")) {
			appManager.addError(aErrors, "userid", "error_empty");
			return false;
		}
		return true;
	}

	protected boolean validatePassword(String aInputValue, List<String[]> aErrors) {
		if (aInputValue == null || aInputValue.equals("") || aInputValue.length() < 12
				|| !aInputValue.matches("(?=.*[a-z]).*") || !aInputValue.matches("(?=.*[A-Z]).*")
				|| !aInputValue.matches("(?=.*[0-9]).*") || !aInputValue.matches("(?=.*[^a-zA-Z\\d]).*")) {
			appManager.addError(aErrors, "password", "too_weak");
			return false;
		}
		if (pwndHelper.isPwned(aInputValue)) {
			appManager.addError(aErrors, "password", "is_pwned");
			return false;
		}
		return true;
	}

	protected void setErrorTarget(AbstractComponent aComponent) {
		if (errorTarget == null) {
			errorTarget = aComponent;
			if (errorTarget instanceof Textbox) {
				((Textbox) errorTarget).setFocus(true);
			}
		}
	}

	public void onOk(Event aEvent) {
		// to be overridden
	}

}
