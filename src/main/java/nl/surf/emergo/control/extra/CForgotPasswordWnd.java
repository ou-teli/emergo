/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.extra;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Messagebox;

import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.view.VView;

public class CForgotPasswordWnd extends CAccountWnd {

	private static final long serialVersionUID = 5997586054392388305L;
	
	public void onCreate(CreateEvent aEvent) {
		super.onCreate(aEvent);
	}

	@Override
    public void onOk(Event aEvent) {
		String lUserId = useridBox().getValue().toLowerCase().replace("\t", "").trim();

		errorTarget = null;
		List<String[]> lErrors = new ArrayList<String[]>(0);
		String lSpecificMessage = "";
		boolean showMessageBox = false;
		boolean lClearFields = false;
		
		//check valid user id
		if (!validateUserid(lUserId, lErrors)) {
			setErrorTarget(useridBox());
		}
		
		if (lErrors.size() == 0) {
			//check if mail user id in db.
			IEAccount lAccount = accountManager.getAccount(lUserId);

			//NOTE to prevent misuse: if account does not exist, act as if account is found but don't send a mail to reset password
			if (lAccount != null) {
				CToken token = createToken(lAccount.getAccId());
				sendForgotPasswordMail(lAccount.getEmail(), token.getToken());
			}
			
			if (lAccount != null) {
				sSpring.getSLogHelper().logAction("Reset password for user: " + lUserId);
				lSpecificMessage = vView.getLabel("forget_password.message.mail_sent").replaceAll("<email>", lAccount.getEmail());
			}
			else {
				sSpring.getSLogHelper().logAction("Reset password: invalid userid: " + lUserId);
				lSpecificMessage = vView.getLabel("forget_password.message.mail_sent.invalid_email");
			}
			setErrorTarget(okButton());
			lClearFields = true;
			showMessageBox = true;
		}

		if (lClearFields) {
			useridBox().setValue("");
		}

		if (lErrors.size() > 0 || !lSpecificMessage.equals("")) {
			if (lErrors.size() > 0 || !showMessageBox) {
				cControl.showErrors(errorTarget, lErrors, lSpecificMessage);
			}
			else {
				vView.showMessagebox(getRoot(), lSpecificMessage, vView.getLabel("forget_password.message.title"), Messagebox.OK, Messagebox.NONE);
				vView.redirectToView(vView.getAbsoluteUrl(VView.v_login));
			}
		}
	}

	protected String sendForgotPasswordMail(String aEmail, String aToken) {
		String lSubject = vView.getLabel("forget_password.mail.subject");
		String lBody = vView.getLabel("forget_password.mail.body");
		lBody = convertMailText(lBody, aToken);
		return appManager.sendMail(appManager.getSysvalue(AppConstants.syskey_smtpnoreplysender), aEmail, null, null, lSubject, lBody);
	}

	private String convertMailText(String aMailText, String aToken) {
		String lText = aMailText.replaceAll("<reset-password>", 
				vView.getEmergoRootUrl() + vView.getEmergoWebappsRoot() + "/extra/reset_password.zul?token=" + aToken);
		lText = lText.replaceAll("<br>","\n");
		return lText;
	}

}
