/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import java.util.Date;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.web.Attributes;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.IRunManager;
import nl.surf.emergo.control.def.CDefWindow;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpring;

/**
 * The Class CEmergoWnd.
 * 
 * Used to handle Emergo IDM access on emergo page.
 * 
 * @author Aad Slootmaker
 * @version $Revision$, $Date$
 */
public class CEmergoWnd extends CDefWindow {

	private static final long serialVersionUID = 8148006211499698609L;

	private static final Logger _log = LogManager.getLogger(CEmergoWnd.class);

	/** The spring. */
	protected SSpring sSpring = new SSpring();

	/** The control */
	protected CControl cControl = new CControl();

	/** The view. */
	protected VView vView = new VView();

	/**
	 * On create get idm user and check if he has access to app.
	 * If no access, clear session variables and show no access popup.
	 * If access, set session variables to use within app and redirect to either
	 * a page to choose a role (if more than one role) or to the start page for
	 * the role (if one role).
	 * If access and run then open player.
	 */
	public void onCreate() {
		//NOTE if used on emergo.studienet.ou.nl, an alternative landing page may be used as default instead of e.g. account_roles.zul.
		
		String emergoAlternativeLandingPageState = (String)cControl.getAccSessAttr("emergo-alternative-landing-page-state");
		boolean handleEmergoAlternativeLandingPage = emergoAlternativeLandingPageState != null && emergoAlternativeLandingPageState.equals("login-required");
		if (handleEmergoAlternativeLandingPage) {
			detach();
			return;
		}
		
		// if no login screen then login not in breadcrumb
		cControl.setAccSessAttr("no_login","true");

	  	// get Language from IDM
		Locale locale = new Locale("nl");

		locale = (Locale)cControl.getAccSessAttr("idm_locale");
		//Note clear session attribute to reduce the number of session attributes
		cControl.setAccSessAttr("idm_locale", null);

		// set Language
  		cControl.setSessAttr(Attributes.PREFERRED_LOCALE,locale);

  		// get User id from IDM
		String lUserId = "admin";

		lUserId = (String)cControl.getAccSessAttr("idm_userid");
		//Note clear session attribute to reduce the number of session attributes
		cControl.setAccSessAttr("idm_userid", null);
		
		// not necessary yet, but maybe usefull in future
		// String lUserType = (String)cControl.getAccSessAttr("idm_usertype");
		//Note clear session attribute to reduce the number of session attributes
		cControl.setAccSessAttr("idm_usertype", null);

		// get Account
		IAccountManager accountManager = (IAccountManager) sSpring.getBean("accountManager");
		IEAccount lAccount = accountManager.getAccount(lUserId);
		if ((lAccount == null) || (!lAccount.getActive())) {
			_log.info("IDM user has no EMERGO account (user: " + lUserId + "): Free memory: " + Runtime.getRuntime().freeMemory() + ": " + new Date());
			if (vView.getIncludeThatRunsEmergo() == null) {
				doOverlapped();
				setPosition("top,center");
				setVisible(true);
			}
		}

		// get Run id parameter
		String lRunId = VView.getReqPar("runId");
		int lRunInt = -1;
		if ((lRunId != null) && (!lRunId.equals("")))
			lRunInt = Integer.parseInt(lRunId);
		// get Run
		IRunManager runManager = (IRunManager) sSpring.getBean("runManager");
		IERun lRun = runManager.getRun(lRunInt);

		CHandleLogin lHandleLogin = new CHandleLogin();
		lHandleLogin.handleLogin(lAccount, lRun);
	}
	
}
