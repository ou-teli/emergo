/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.impl.InputElement;

import nl.surf.emergo.control.def.CDefButton;

/**
 * The Class CDeleteMediaButton.
 * 
 * Used to to delete an earlier uploaded file.
 */
public class CDeleteMediaButton extends CDefButton {

	private static final long serialVersionUID = 3944340359408343363L;

	/** The target that will get the name of the uploaded file as text and will own the uploaded media as content. */
	protected Component uploadtarget = null;

	/**
	 * Sets the upload target.
	 * 
	 * @param aUploadtarget the upload target
	 */
	public void setUploadTarget(Component aUploadtarget) {
		uploadtarget = aUploadtarget;
	}

	/**
	 * On click clear media content.
	 */
	public void onClick() {
		uploadtarget.setAttribute("content", null);
		uploadtarget.setAttribute("changed", "true");
		// show empty file name
		((InputElement) uploadtarget).setText("");
	}

}
