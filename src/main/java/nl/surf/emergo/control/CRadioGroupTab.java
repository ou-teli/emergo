/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Textbox;

import nl.surf.emergo.control.def.CDefRadio;
import nl.surf.emergo.control.def.CDefRadiogroup;

/**
 * The Class CRadioGroupTab.
 * 
 * Used for CRadioTab buttons and to show corresponding tab if radio button is checked
 */
public class CRadioGroupTab extends CDefRadiogroup {

	private static final long serialVersionUID = 3505460258745959067L;

	/**
	 * On check set attribute changed of clicked radio button to true (super).
	 * Show corresponding tab.
	 */
	@Override
	public void onCheck() {
		super.onCheck();
		CDefRadio lRadio = (CDefRadio)getSelectedItem();
		for (Object lRadio2 : getItems()) {
			// should be done by framework, but goes wrong if initially last item of radio group is selected!
			// only selection of other item, then reselect last item and then again select other item will trigger setting checked of last item to false
			// so, here we do it by hand:
			if (!((CDefRadio)lRadio2).getId().equals(lRadio.getId()))
				((CDefRadio)lRadio2).setChecked(false);
		}
		String[] lBlobtypearr = (String[])lRadio.getAttribute("blobtypearr");
		if (lBlobtypearr != null) {
			// hide tabs
			for (int i = 0; i < lBlobtypearr.length; i++) {
				String lBlobtype = lBlobtypearr[i];
				Component lTab = getFellowIfAny("blob_" + lBlobtype + "_radio_tab");
				if (lTab != null)
					lTab.setVisible(false);
			}
		}
		// show corresponding tab
		Component lSelTab = getFellowIfAny(lRadio.getId() + "_tab");
		if (lSelTab != null) {
			lSelTab.setVisible(true);
			Textbox lEditComponent = (Textbox)getFellowIfAny(lSelTab.getAttribute("compid").toString());
			if (lEditComponent != null && !lEditComponent.isDisabled()) {
				lEditComponent.setFocus(true);
			}
		}
	}
}
