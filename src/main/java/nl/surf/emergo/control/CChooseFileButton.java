/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.impl.InputElement;

import nl.surf.emergo.view.VView;

/**
 * The Class CChooseFileButton.
 * 
 * Used to show a button to choose a file to upload.
 */
public class CChooseFileButton extends CInputBtn {

	private static final long serialVersionUID = -8511978887154469580L;

	/** The target that will get the name of the uploaded file as text and will own the uploaded media as content. */
	protected Component filetarget = null;

	/**
	 * Gets the choose file view.
	 *
	 * @return the choose file view
	 */
	public String getChooseFileView() {
		return VView.v_cde_s_choose_file;
	}

	/**
	 * Sets the file target.
	 * 
	 * @param aFiletarget the file target
	 */
	public void setFileTarget(Component aFiletarget) {
		filetarget = aFiletarget;
	}

	/**
	 * On click show choose file dialog.
	 */
	public void onClick() {
		// parameter is old file name
		params.put("item", ((InputElement) filetarget).getText());
		// show choose file dialog
		showPopup(getChooseFileView());
	}
	
	@Override
	protected void handleItem(Object aStatus) {
		filetarget.setAttribute("changed", "true");
		// show new file name
		String lFileName = (String)aStatus;
		// internal streaming video file; add name to session variable, so file can be previewed
		CDesktopComponents.sSpring().getSBlobHelper().setBlobInSessionVar(lFileName, "0");
		((InputElement)filetarget).setText(lFileName);
	}

}
