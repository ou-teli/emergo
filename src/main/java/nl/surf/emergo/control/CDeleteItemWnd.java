/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zk.ui.event.CreateEvent;

/**
 * The Class CDeleteItemWnd.
 */
public class CDeleteItemWnd extends CInputWnd {

	private static final long serialVersionUID = 4426177072167873061L;
	
	public CDeleteItemWnd() {
		super();
		//NOTE most input dialogues set attribute 'item' of the input window to the item in question if the 'ok' button is clicked.
		//However, in case of deleting an item the attribute 'ok' is set to true if the 'ok' button is clicked, see class CDeleteItemWnd.
		//In case the 'cancel' button is clicked either attribute 'item' or 'ok' is set to null.
		attributeKey = "ok";
	}

	/**
	 * On create fill item and show correct title.
	 * 
	 * @param aEvent the a event
	 */
	public void onCreate(CreateEvent aEvent) {
		super.onCreate(aEvent);
		if (StringUtils.isEmpty((String)aEvent.getArg().get("item"))) {
			setTitle(CDesktopComponents.vView().getCLabel("delete") + " " + aEvent.getArg().get("itemtype"));
		}
		else {
			setTitle(CDesktopComponents.vView().getCLabel("delete") + " " + aEvent.getArg().get("itemtype") + " '" + aEvent.getArg().get("item") + "'");
		}
	}

}
