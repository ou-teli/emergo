/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treechildren;
import org.zkoss.zul.Treeitem;

/**
 * The Class CContentItemFoldMi.
 */
public class CContentItemFoldMi extends CContentItemMi {

	private static final long serialVersionUID = -3725852234316605369L;

	/**
	 * Instantiates a contentitem fold menu item.
	 * 
	 * @param aContentComponent the content xml tree
	 * @param aContentHelper the content helper
	 * @param aIsAuthor the is author state
	 */
	public CContentItemFoldMi(Component aContentComponent, CContentHelper aContentHelper, boolean aIsAuthor) {
		super(aContentComponent, aContentHelper, aIsAuthor);
	}

	/**
	 * On click fold all content item children.
	 */
	public void onClick() {
		Component lContentItem = (Component)getComponent().getAttribute("target");
		Treechildren lChildren = null;
		if (lContentItem instanceof Tree) {
			lChildren = ((Tree)lContentItem).getTreechildren();
		}
		if (lContentItem instanceof Treeitem) {
			lChildren = ((Treeitem)lContentItem).getTreechildren();
		}
		if (lChildren == null) {
			return;
		}
		for (Component lComponent :lChildren.getChildren()) {
			if (lComponent instanceof Treeitem) {
				((Treeitem)lComponent).setOpen(false);
			}
		}
	}
}
