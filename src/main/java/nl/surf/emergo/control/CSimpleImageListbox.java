/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.control.def.CDefListbox;
import nl.surf.emergo.zkspring.SSpring;

/**
 * The Class CSimpleImageListbox.
 * 
 * Used to show a simple listbox.
 */
public class CSimpleImageListbox extends CDefListbox {

	private static final long serialVersionUID = 2592341790271341533L;

	private String imagePrefix = "";

	/**
	 * Instantiates a new singleselect combo.
	 * 
	 * @param aId the id
	 */
	public CSimpleImageListbox(String aId, String aImagePrefix, SSpring sSpring) {
		setId(aId);
		imagePrefix = aImagePrefix;
		setCheckmark(true);
		setRows(10);
	}

	/**
	 * Shows combobox items.
	 * 
	 * @param aSelectedItem the selected item to preselect
	 */
	public void showItems(String aSelectedItem) {
		int lNumber = 1;
		String lImageSrc = CDesktopComponents.sSpring().getStyleImgSrc(imagePrefix + lNumber);
		while (!lImageSrc.equals("")) {
			Listitem lListitem = new Listitem();
			appendChild(lListitem);
			lListitem.setValue("" + lNumber);
			Listcell lListcell = new Listcell();
			lListitem.appendChild(lListcell);
			lListcell.setImage(lImageSrc);
			if (("" + lNumber).equals(aSelectedItem)) {
				setSelectedItem(lListitem);
			}
			lNumber ++;
			lImageSrc = CDesktopComponents.sSpring().getStyleImgSrc(imagePrefix + lNumber);
		}
	}
}
