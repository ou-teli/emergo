/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.adm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.IXMLAttributeValueTime;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CCaseHelper;
import nl.surf.emergo.control.CControlHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IEBlob;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunGroupAccount;

/**
 * The Class CAdmRunLoggingHelper.
 */
public class CAdmRunLoggingHelper extends CControlHelper {

	private static final Logger _log = LogManager.getLogger(CAdmRunLoggingHelper.class);

	/** The case components. */
	protected List<IECaseComponent> caseComponents = null;

	/** The counter. */
	protected long counter = 0;

	/** The case helper. */
	protected CCaseHelper caseHelper = null;

	private static final String none = "none";
	private static final String system = "system";
	private static final String user = "user";
	private static final String systemuser = "system/user";
	private static final String usersystem = "user/system";

	/**
	 * Gets the case helper.
	 * 
	 * @return the case helper
	 */
	protected CCaseHelper getCaseHelper() {
		if (caseHelper == null)
			caseHelper = new CCaseHelper();
		return caseHelper;
	}

	/**
	 * Get case components.
	 * 
	 * @return case components
	 */
	public List<IECaseComponent> getCaseComponents() {
		if (caseComponents == null) {
			caseComponents = CDesktopComponents.sSpring().getCaseComponents(CDesktopComponents.sSpring().getRunGroupAccount().getERunGroup().getERun().getECase());
		}
		return caseComponents;
	}

	/**
	 * Get data plus status root tag of component.
	 * 
	 * @param aCacId the a cac id
	 * 
	 * @return the XML tag
	 */
	protected IXMLTag getDataPlusStatusRootTag(String aCacId) {
		return CDesktopComponents.sSpring().getXmlDataPlusRunStatusTree(aCacId, AppConstants.statusTypeRunGroup);
	}

	/**
	 * Get status root tag of component.
	 * 
	 * @param aCacId the a cac id
	 * 
	 * @return the XML tag
	 */
	protected IXMLTag getStatusRootTag(String aCacId) {
		return CDesktopComponents.sSpring().getXmlRunStatusTree(aCacId, AppConstants.statusTypeRunGroup);
	}

	/**
	 * Get value.
	 * 
	 * @param aItem the a item
	 * @param aAddHeaders the a add headers
	 * 
	 * @return value
	 */
	public String getValue(Hashtable<String,Object> aItem, boolean aAddHeaders) {
		List<IERunGroupAccount> lRunGroupAccounts = (List)aItem.get("rungroupaccounts");

		IAccountManager accountManager = (IAccountManager)CDesktopComponents.sSpring().getBean("accountManager");
		
		StringBuilder lBodyBuilder = new StringBuilder();
		lBodyBuilder.append("");
		for (IERunGroupAccount runGroupAccount : lRunGroupAccounts) {
			CDesktopComponents.sSpring().setRunStatus(AppConstants.runStatusRun);
			CDesktopComponents.sSpring().setRunGroupAccount(runGroupAccount);
			String lAccountUserId = runGroupAccount.getEAccount().getUserid();
			String lAccountEmail = runGroupAccount.getEAccount().getEmail();
			String lAccountName = accountManager.getAccountName(runGroupAccount.getEAccount());
			IECaseRole lCaseRole = CDesktopComponents.sSpring().getRunGroupAccount().getERunGroup().getECaseRole();
			IERun lRun = runGroupAccount.getERunGroup().getERun(); 
			List<IECaseComponent> lCaseComponents = getCaseComponents();
			if (lCaseComponents == null || lCaseComponents.size() == 0)
				lBodyBuilder.append("\ncasecomponenten niet aanwezig");
			else {
				TreeMap<String,String> treeMap = new TreeMap<String,String>();
				counter = 1;
				for (int j=0;j<lCaseComponents.size();j++) {
					IECaseComponent lCaseComponent = (IECaseComponent)lCaseComponents.get(j);
//					IXMLTag lDataPlusStatusRootTag = getDataPlusStatusRootTag("" + lCaseComponent.getCacId());
					IXMLTag lDataPlusStatusRootTag = CDesktopComponents.sSpring().getXmlDataPlusRunGroupStatusTree(runGroupAccount.getERunGroup().getRugId(), lCaseComponent);
//					IXMLTag lStatusRootTag = getStatusRootTag("" + lCaseComponent.getCacId());
					IXMLTag lStatusRootTag = CDesktopComponents.sSpring().getXmlRunGroupStatusTree(runGroupAccount.getERunGroup().getRugId(), lCaseComponent);
					if (lDataPlusStatusRootTag == null)
						lBodyBuilder.append("");
					else {
						List<IXMLTag> lTags = CDesktopComponents.cScript().getNodeTags(lDataPlusStatusRootTag);
						IXMLTag lComponentTag = lDataPlusStatusRootTag.getChild(AppConstants.componentElement);
						if (lComponentTag != null) {
							IXMLTag lTag = lComponentTag;
							addStatus(treeMap, lRun, lCaseRole, lCaseComponent, 
									lTag, lStatusRootTag, 
									lAccountUserId, lAccountEmail, lAccountName, "", false);
						}
						if (lTags == null || lTags.size() == 0)
							lBodyBuilder.append("");
						else {
							for (IXMLTag lTag : lTags) {
								if (!lTag.getAttribute(AppConstants.keyRefstatusid).equals("")) {
									addStatus(treeMap, lRun, lCaseRole, lCaseComponent, 
											lTag, lStatusRootTag, 
											lAccountUserId, lAccountEmail, lAccountName, "", false);
								}
							}
						}
					}
				}
			    Collection<String> c = treeMap.values();
			    //obtain an Iterator for Collection
			    Iterator<String> itr = c.iterator();
			    //iterate through TreeMap values iterator
			    while(itr.hasNext()) {
					lBodyBuilder.append((String)itr.next());
			    }
			}
		}
		return lBodyBuilder.toString();
	}

	/**
	 * Add status.
	 * 
	 * @param aTreeMap the a tree map
	 * @param aRun the a run
	 * @param aCaseComponent the a case component
	 * @param aTag the a tag
	 * @param aStatusRootTag the a status root tag
	 * @param aAccountUserId the a account user id
	 * @param aAccountEmail the a account email
	 * @param aAccountName the a account name
	 * @param aFilterOnStatusKeys the a filter on status keys, comma separated
	 * @param aOnlyShowLastValue the a only show last value
	 */
	protected void addStatus(TreeMap<String,String> aTreeMap, IERun aRun, IECaseRole aCaseRole, IECaseComponent aCaseComponent,
			IXMLTag aTag, IXMLTag aStatusRootTag,
			String aAccountUserId, String aAccountEmail, String aAccountName, String aFilterOnStatusKeys, boolean aOnlyShowLastValue) {
		int lCacId = aCaseComponent.getCacId();
		String lCaseComponentName = aCaseComponent.getName();
		String lTagId = aTag.getAttribute(AppConstants.keyId);
		String lTagName = aTag.getName();
		String lTagKey = "";
		if (!aTag.getDefAttribute(AppConstants.defKeyKey).equals("")) {
			lTagKey = aTag.getChildValue(aTag.getDefAttribute(AppConstants.defKeyKey));
		}
		if (lTagName.equals(AppConstants.componentElement))
			lTagKey = lTagName;
//		IXMLTag lStatusTag = aTag.getChild(AppConstants.statusElement);
		List<IXMLTag> lStatusTags = aTag.getStatusTags();
		if (lStatusTags == null) {
			return;
		}
		
		for (IXMLTag lStatusTag : lStatusTags) {
			IXMLTag lStatusStatusTag = lStatusTag.getChild(AppConstants.statusElement);
			if (lStatusStatusTag != null) {
			for (Enumeration<String> keys = lStatusStatusTag.getAttributes().keys(); keys.hasMoreElements();) {
				String key = (String) keys.nextElement();
				boolean lAddKey = aFilterOnStatusKeys.equals("") || ("," + aFilterOnStatusKeys + ",").contains("," + key + ",");
				// get relevant data
				List<IXMLAttributeValueTime> lValueTimes = lStatusStatusTag.getAttributeAsList(key);
				if (lAddKey && lValueTimes != null) {
					int lCounter = 0;
					for (IXMLAttributeValueTime lValueTime : lValueTimes) {
						boolean lAddValue = (!aOnlyShowLastValue || (lCounter == lValueTimes.size())) && lValueTime.getTime() >= 0;
						if (lAddValue) {
							double time = lValueTime.getTime();
							String mapkey = "" + 1000 * time;
							while (mapkey.length() < 13) {
								mapkey = "0" + mapkey;
							}
							String arrValue = lValueTime.getValue();
							if (arrValue.equals("")) {
								arrValue = "EMPTY";
							}
							// determine if system or user event or possibly both
							String[] lEventData = getEventData(aCaseRole, aCaseComponent, aCaseComponent.getEComponent().getCode(), 
									aTag, aStatusRootTag, 
									lTagName, key, arrValue);
							if (!lEventData[4].equals(none)) {
								lTagKey = lEventData[4];
							}
							aTreeMap.put(mapkey + counter,
									"\n" +
									aRun.getECase().getCasId() + "\t" +
									aRun.getRunId() + "\t" +
									aAccountUserId + "\t" +
									aAccountEmail + "\t" +
									aAccountName + "\t" +
									time + "\t" + 
									lCacId + "\t" +
									lCaseComponentName + "\t" + 
									lTagId + "\t" + 
									lTagName + "\t" + 
									lTagKey + "\t" + 
									key + "\t" + 
									arrValue + "\t" +
									lEventData[0] + "\t" +
									lEventData[1] + "\t" +
									lEventData[2]);
							counter++;
						}
						lCounter++;
					}
				}
			}
			}
		}
	}

	/**
	 * Get event data.
	 * 
	 * @param aCaseRole the a case role
	 * @param aCaseComponent the a case component
	 * @param aComponentCode the a component code
	 * @param aTag the a tag
	 * @param aStatusRootTag the a status root tag
	 * @param aTagName the a tag name
	 * @param aKey the a key
	 * @param aValue the a value
	 * 
	 * @return event data
	 */
	protected String[] getEventData(IECaseRole aCaseRole, IECaseComponent aCaseComponent, String aComponentCode, 
			IXMLTag aTag, IXMLTag aStatusRootTag,
			String aTagName, String aKey, String aValue) {

		//NOTE if new component or change in component tags, this method has to be adjusted!!
		
		// TODO Must have
		// Try to avoid user/system or system/user events, by looking at history
		// For example, if action generates switching location, the latter event must be
		// system instead of user/system.
/*
		script actions
		locationtag opened true
		conversationtag started true
		empackcomponent started true

		location actions
		conversations opened true
		references opened true
		assessments opened true		
*/
		// TODO Nice to have
		// Add other user generated content besides mail, for instance adding of pieces.
		
		String[] lEventData = new String[]{none, none, none, none, none};
		// first entry indicates actor: user, system, usersystem or systemuser
		// second entry contains user data, data entered by the student, if applicable
		// third entry contains author data, data entered by the developer, if applicable
		// fourth entry contains tag name in case of reference to other tag, if applicable
		// fifth entry contains tag id in case of reference to other tag, if applicable
			
		if (aComponentCode.equals("alerts")) {
			lEventData = getAlertsEventData(aCaseRole, aCaseComponent, aComponentCode, aTag, aTagName, aKey, aValue, lEventData);
		}
		else if (aComponentCode.equals("assessments")) {
			lEventData = getAssessmentsEventData(aCaseRole, aCaseComponent, aComponentCode, aTag, aTagName, aKey, aValue, lEventData);
		}
		else if (aComponentCode.equals("canon")) {
			lEventData = getCanonEventData(aCaseRole, aCaseComponent, aComponentCode, aTag, aTagName, aKey, aValue, lEventData);
		}
		else if (aComponentCode.equals("canonresult")) {
			lEventData = getCanonresultEventData(aCaseRole, aCaseComponent, aComponentCode, aTag, aTagName, aKey, aValue, lEventData);
		}
		else if (aComponentCode.equals("case")) {
			lEventData = getCaseEventData(aCaseRole, aCaseComponent, aComponentCode, aTag, aTagName, aKey, aValue, lEventData);
		}
		else if (aComponentCode.equals("chat")) {
			lEventData = getChatEventData(aCaseRole, aCaseComponent, aComponentCode, aTag, aTagName, aKey, aValue, lEventData);
		}
		else if (aComponentCode.equals("conversations")) {
			lEventData = getConversationsEventData(aCaseRole, aCaseComponent, aComponentCode, aTag, aTagName, aKey, aValue, lEventData);
		}
		else if (aComponentCode.equals("directing")) {
			lEventData = getDirectingEventData(aCaseRole, aCaseComponent, aComponentCode, aTag, aTagName, aKey, aValue, lEventData);
		}
		else if (aComponentCode.equals("empack")) {
			lEventData = getEmpackEventData(aCaseRole, aCaseComponent, aComponentCode, aTag, aTagName, aKey, aValue, lEventData);
		}
		else if (aComponentCode.equals("googlemaps")) {
			lEventData = getGooglemapsEventData(aCaseRole, aCaseComponent, aComponentCode, aTag, aTagName, aKey, aValue, lEventData);
		}
		else if (aComponentCode.equals("items")) {
			lEventData = getItemsEventData(aCaseRole, aCaseComponent, aComponentCode, aTag, aTagName, aKey, aValue, lEventData);
		}
		else if (aComponentCode.equals("locations")) {
			lEventData = getLocationsEventData(aCaseRole, aCaseComponent, aComponentCode, aTag, aTagName, aKey, aValue, lEventData);
		}
		else if (aComponentCode.equals("logbook")) {
			lEventData = getLogbookEventData(aCaseRole, aCaseComponent, aComponentCode, aTag, aTagName, aKey, aValue, lEventData);
		}
		else if (aComponentCode.equals("mail")) {
			lEventData = getMailEventData(aCaseRole, aCaseComponent, aComponentCode, aTag, aTagName, aKey, aValue, lEventData);
		}
		else if (aComponentCode.equals("memo")) {
			lEventData = getMemoEventData(aCaseRole, aCaseComponent, aComponentCode, 
					aTag, aStatusRootTag, 
					aTagName, aKey, aValue, lEventData);
		}
		else if (aComponentCode.equals("memos")) {
			lEventData = getMemosEventData(aCaseRole, aCaseComponent, aComponentCode, 
					aTag, aStatusRootTag, 
					aTagName, aKey, aValue, lEventData);
		}
		else if (aComponentCode.equals("navigation")) {
			lEventData = getNavigationEventData(aCaseRole, aCaseComponent, aComponentCode, aTag, aTagName, aKey, aValue, lEventData);
		}
		else if (aComponentCode.equals("note")) {
			lEventData = getNoteEventData(aCaseRole, aCaseComponent, aComponentCode, aTag, aTagName, aKey, aValue, lEventData);
		}
		else if (aComponentCode.equals("notifications")) {
			lEventData = getNotificationsEventData(aCaseRole, aCaseComponent, aComponentCode, aTag, aTagName, aKey, aValue, lEventData);
		}
		else if (aComponentCode.equals("persons")) {
			lEventData = getPersonsEventData(aCaseRole, aCaseComponent, aComponentCode, aTag, aTagName, aKey, aValue, lEventData);
		}
		else if (aComponentCode.equals("profile")) {
			lEventData = getProfileEventData(aCaseRole, aCaseComponent, aComponentCode, aTag, aTagName, aKey, aValue, lEventData);
		}
		else if (aComponentCode.equals("references")) {
			lEventData = getReferencesEventData(aCaseRole, aCaseComponent, aComponentCode, aTag, aTagName, aKey, aValue, lEventData);
		}
		else if (aComponentCode.equals("scores")) {
			lEventData = getScoresEventData(aCaseRole, aCaseComponent, aComponentCode, aTag, aTagName, aKey, aValue, lEventData);
		}
		else if (aComponentCode.equals("scripts")) {
			lEventData = getScriptsEventData(aCaseRole, aCaseComponent, aComponentCode, aTag, aTagName, aKey, aValue, lEventData);
		}
		else if (aComponentCode.equals("states")) {
			lEventData = getStatesEventData(aCaseRole, aCaseComponent, aComponentCode, aTag, aTagName, aKey, aValue, lEventData);
		}
		else if (aComponentCode.equals("tasks")) {
			lEventData = getTasksEventData(aCaseRole, aCaseComponent, aComponentCode, aTag, aTagName, aKey, aValue, lEventData);
		}
		else if (aComponentCode.equals("tutorial")) {
			lEventData = getTutorialEventData(aCaseRole, aCaseComponent, aComponentCode, aTag, aTagName, aKey, aValue, lEventData);
		}
		else if (aComponentCode.equals("videomanual")) {
			lEventData = getVideomanualEventData(aCaseRole, aCaseComponent, aComponentCode, aTag, aTagName, aKey, aValue, lEventData);
		}
		else if (aComponentCode.equals("ispot")) {
			//TODO get ispot event data
		}
		else {
			_log.info("CAdmRunLoggingHelper, method getEventData: could not add event data because of unknown component '" + aComponentCode + "'!");
		}
		return lEventData;
	}

	protected void logProblem(String aComponentCode, String aTagName) {
		_log.info("CAdmRunLoggingHelper, method getEventData: could not add event data for component '" + aComponentCode + "' because of unknown tag '" + aTagName + "'!");
	}

	protected String[] getAlertsEventData(IECaseRole aCaseRole, IECaseComponent aCaseComponent, String aComponentCode, IXMLTag aTag, String aTagName, String aKey, String aValue, String[] aEventData) {
		if (aTagName.equals(AppConstants.componentElement)) {
			// user cannot open alerts component himself, is done by script, closing isn't saved
			aEventData[0] = system;
		}
		else if (aTagName.equals("alert")) {
			// user cannot open alert himself, is done by script, closing isn't saved
			aEventData[0] = system;
			if (aKey.equals(AppConstants.statusKeySent)) {
				aEventData[2] = aTag.getChildValue("richtext").replaceAll("\n", "");
			}
		}
		else if (aTagName.equals("audioalert")) {
			// user cannot open alert himself, is done by script, closing isn't saved
			aEventData[0] = system;
			if (aKey.equals(AppConstants.statusKeySent)) {
				aEventData[2] = getBlobData(aTag);
			}
		}
		else {
			logProblem(aComponentCode, aTagName);
		}
		return aEventData;
	}

	protected String[] getAssessmentsEventData(IECaseRole aCaseRole, IECaseComponent aCaseComponent, String aComponentCode, IXMLTag aTag, String aTagName, String aKey, String aValue, String[] aEventData) {
		if (aTagName.equals(AppConstants.componentElement)) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened)) {
				// user normally opens assessments component himself, but also could be done if
				// component is opened automatically either by script or as location action
				aEventData[0] = usersystem;
			}
			else {
				aEventData[0] = system;
			}
		}
		else if (aTagName.equals("instruction")) {
			// instruction is shown by system
			aEventData[0] = system;
		}
		else if (aTagName.equals("assessment")) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened)) {
				// user normally opens assessment himself, but also could be opened
				// automatically if assessment is first one in list, or by script
				aEventData[0] = usersystem;
			}
			else if (aKey.equals(AppConstants.statusKeyStarted)) {
				// user normally starts assessment, but not if autostart is true
				aEventData[0] = usersystem;
			}
			else if (aKey.equals(AppConstants.statusKeyFinished)) {
				// user finishes assessment or system using script, autostart assessment isn't finished
				aEventData[0] = usersystem;
			}
			else {
				aEventData[0] = system;
			}
		}
		else if (aTagName.equals("refitem")) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened)) {
				// user opens item
				aEventData[0] = user;
				IXMLTag lReferencedTag = getDataTagByDataTagReference(aCaseRole, aCaseComponent, aTag);
				if (lReferencedTag != null) {
					aEventData[2] = lReferencedTag.getChildValue("richtext").replaceAll("\n", "");
				}
			}
			else {
				aEventData[0] = system;
			}
		}
		else if (aTagName.equals("feedbackcondition")) {
			// feedback condition is result of answer, so system
			aEventData[0] = system;
		}
		else {
			logProblem(aComponentCode, aTagName);
		}
		return aEventData;
	}

	protected String[] getCanonEventData(IECaseRole aCaseRole, IECaseComponent aCaseComponent, String aComponentCode, IXMLTag aTag, String aTagName, String aKey, String aValue, String[] aEventData) {
		// TODO set correct even data
		if (aTagName.equals(AppConstants.componentElement)) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened)) {
				// user opens component
				aEventData[0] = user;
			}
			else {
				aEventData[0] = system;
			}
		}
		return aEventData;
	}

	protected String[] getCanonresultEventData(IECaseRole aCaseRole, IECaseComponent aCaseComponent, String aComponentCode, IXMLTag aTag, String aTagName, String aKey, String aValue, String[] aEventData) {
		// TODO set correct even data
		if (aTagName.equals(AppConstants.componentElement)) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened)) {
				// user opens component
				aEventData[0] = user;
			}
			else {
				aEventData[0] = system;
			}
		}
		return aEventData;
	}

	protected String[] getCaseEventData(IECaseRole aCaseRole, IECaseComponent aCaseComponent, String aComponentCode, IXMLTag aTag, String aTagName, String aKey, String aValue, String[] aEventData) {
		// system component
		aEventData[0] = system;
		return aEventData;
	}

	protected String[] getChatEventData(IECaseRole aCaseRole, IECaseComponent aCaseComponent, String aComponentCode, IXMLTag aTag, String aTagName, String aKey, String aValue, String[] aEventData) {
		// TODO set correct even data
		// tag name 'chat', child tag 'text'
		if (aTagName.equals(AppConstants.componentElement)) {
			//only user can open component
			aEventData[0] = user;
		}
		else if (aTagName.equals("chat")) {
			// user adds chat
			aEventData[0] = user;
			aEventData[1] = aTag.getChildValue("text").replaceAll("\n", " ");
		}
		else {
			logProblem(aComponentCode, aTagName);
		}
		return aEventData;
	}

	protected String[] getConversationsEventData(IECaseRole aCaseRole, IECaseComponent aCaseComponent, String aComponentCode, IXMLTag aTag, String aTagName, String aKey, String aValue, String[] aEventData) {
		if (aTagName.equals(AppConstants.componentElement)) {
			// user normally does not open or close conversations component himself, is done by script or otherwise,
			// but if more then one location action, a user can choose a conversation so
			// conversations himself
			aEventData[0] = systemuser;
		}
		else if (aTagName.equals("conversation")) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened)) {
				// user normally does not open conversation, because conversation starts
				// automatically on location, but if more then one location action, a user
				// can choose a conversation so opens it
				aEventData[0] = systemuser;
			}
			else if (aKey.equals(AppConstants.statusKeyFinished)) {
				// user always finishes conversation
				aEventData[0] = user;
			}
			else {
				aEventData[0] = system;
			}
		}
		else if (aTagName.equals("background")) {
			// user cannot open background himself, is done by script
			aEventData[0] = system;
		}
		else if (aTagName.equals("map")) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened) || aKey.equals(AppConstants.statusKeyOutfolded)) {
				// user does open or outfold map
				aEventData[0] = user;
			}
			else {
				aEventData[0] = system;
			}
		}
		else if (aTagName.equals("question")) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened) || aKey.equals(AppConstants.statusKeyOutfolded)) {
				// user does open or outfold question
				aEventData[0] = user;
			}
			else {
				aEventData[0] = system;
			}
		}
		else if (aTagName.equals("fragment")) {
			// user cannot open fragment himself, is result of question or start fragment
			aEventData[0] = system;
			if (aKey.equals(AppConstants.statusKeyOpened)) {
				aEventData[2] = getBlobData(aTag);
			}
		}
		else {
			logProblem(aComponentCode, aTagName);
		}
		return aEventData;
	}
	
	protected String[] getDirectingEventData(IECaseRole aCaseRole, IECaseComponent aCaseComponent, String aComponentCode, IXMLTag aTag, String aTagName, String aKey, String aValue, String[] aEventData) {
		if (aTagName.equals(AppConstants.componentElement)) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened)) {
				// user opens component
				aEventData[0] = user;
			}
			else {
				aEventData[0] = system;
			}
		}
		else if (aTagName.equals("setting")) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened)) {
				aEventData[0] = user;
			}
			else {
				aEventData[0] = system;
			}
			aEventData[2] = getBlobData(aTag);
		}
		else if (aTagName.equals("view")) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened)) {
				aEventData[0] = user;
			}
			else {
				aEventData[0] = system;
			}
		}
		return aEventData;
	}
	
	protected String[] getEmpackEventData(IECaseRole aCaseRole, IECaseComponent aCaseComponent, String aComponentCode, IXMLTag aTag, String aTagName, String aKey, String aValue, String[] aEventData) {
		if (aTagName.equals(AppConstants.componentElement)) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened)) {
				// user opens empack component, closing isn't logged
				aEventData[0] = user;
			}
			else {
				aEventData[0] = system;
			}
		}
		else {
			logProblem(aComponentCode, aTagName);
		}
		return aEventData;
	}
	
	protected String[] getGooglemapsEventData(IECaseRole aCaseRole, IECaseComponent aCaseComponent, String aComponentCode, IXMLTag aTag, String aTagName, String aKey, String aValue, String[] aEventData) {
		if (aTagName.equals(AppConstants.componentElement)) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened)) {
				if (aKey.equals(AppConstants.statusKeyOpened) || aValue.equals(AppConstants.statusValueFalse)) {
					// either user closes component directly or it is done automatically by choosing another component
					aEventData[0] = usersystem;
				}
				else {
					// user opens googlemaps component
					aEventData[0] = user;
				}
			}
			else {
				aEventData[0] = system;
			}
		}
		else if (aTagName.equals("piece")) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened)) {
				// user opens piece
				aEventData[0] = user;
				aEventData[2] = aTag.getChildValue("description");
			}
			else {
				aEventData[0] = system;
			}
		}
		else {
			logProblem(aComponentCode, aTagName);
		}
		return aEventData;
	}
	
	protected String[] getItemsEventData(IECaseRole aCaseRole, IECaseComponent aCaseComponent, String aComponentCode, IXMLTag aTag, String aTagName, String aKey, String aValue, String[] aEventData) {
		if (aTagName.equals(AppConstants.componentElement)) {
			// user cannot open items component
			aEventData[0] = system;
		}
		else if (aTagName.equals("item")) {
			// user cannot open item
			aEventData[0] = system;
		}
		else if (aTagName.equals("alternative")) {
			if (aKey.equals(AppConstants.statusKeyOpened) && aValue.equals(AppConstants.statusValueTrue)) {
				// user opens alternative
				aEventData[0] = user;
				aEventData[2] = aTag.getChildValue("richtext").replaceAll("\n", "");
			}
			else {
				aEventData[0] = system;
			}
		}
		else if (aTagName.equals("feedbackcondition")) {
			// user cannot open item
			aEventData[0] = system;
		}
		else if (aTagName.equals("piece")) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened)) {
				// user opens piece
				aEventData[0] = user;
				aEventData[2] = getBlobData(aTag);
			}
			else {
				aEventData[0] = system;
			}
		}
		else if (aTagName.equals("refpiece")) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened)) {
				// user opens refpiece
				aEventData[0] = user;
				IXMLTag lReferencedTag = getDataTagByDataTagReference(aCaseRole, aCaseComponent, aTag);
				if (lReferencedTag != null) {
					aEventData[2] = getBlobData(lReferencedTag);
				}
			}
			else {
				aEventData[0] = system;
			}
		}
		else {
			logProblem(aComponentCode, aTagName);
		}
		return aEventData;
	}
	
	protected String[] getLocationsEventData(IECaseRole aCaseRole, IECaseComponent aCaseComponent, String aComponentCode, IXMLTag aTag, String aTagName, String aKey, String aValue, String[] aEventData) {
		if (aTagName.equals(AppConstants.componentElement)) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened)) {
				// user normally opens locations component himself, but also could be done if
				// component is opened automatically by script or otherwise
				aEventData[0] = usersystem;
			}
			else {
				aEventData[0] = system;
			}
		}
		else if (aTagName.equals("location")) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened)) {
				// user normally opens location himself, but also could be done if
				// locations component is opened automatically by script or otherwise
				aEventData[0] = usersystem;
			}
			else {
				aEventData[0] = system;
			}
		}
		else if (aTagName.equals("background")) {
			// user cannot open background
			aEventData[0] = system;
		}
		else {
			logProblem(aComponentCode, aTagName);
		}
		return aEventData;
	}
	
	protected String[] getLogbookEventData(IECaseRole aCaseRole, IECaseComponent aCaseComponent, String aComponentCode, IXMLTag aTag, String aTagName, String aKey, String aValue, String[] aEventData) {
		if (aTagName.equals(AppConstants.componentElement)) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened)) {
				if (aKey.equals(AppConstants.statusKeyOpened) || aValue.equals(AppConstants.statusValueFalse)) {
					// either user closes logbook component directly or it is done
					// automatically by choosing another component
					aEventData[0] = usersystem;
				}
				else {
					// user opens logbook component
					aEventData[0] = user;
				}
			}
			else {
				aEventData[0] = system;
			}
		}
		else {
			logProblem(aComponentCode, aTagName);
		}
		return aEventData;
	}
	
	protected String[] getMailEventData(IECaseRole aCaseRole, IECaseComponent aCaseComponent, String aComponentCode, IXMLTag aTag, String aTagName, String aKey, String aValue, String[] aEventData) {
		if (aTagName.equals(AppConstants.componentElement)) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened)) {
				// user normally opens mail component himself, but also could be done if
				// component is opened automatically by script or otherwise
				aEventData[0] = usersystem;
			}
			else {
				aEventData[0] = system;
			}
		}
		else if (aTagName.equals("map")) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened) || aKey.equals(AppConstants.statusKeyOutfolded)) {
				// user does open or outfold map
				aEventData[0] = user;
			}
			else {
				aEventData[0] = system;
			}
		}
		else if (aTagName.equals("inmailpredef")) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened)) {
				// user opens inmailpredef
				aEventData[0] = user;
				aEventData[2] = aTag.getChildValue("richtext").replaceAll("\n", "");
			}
			else {
				aEventData[0] = system;
				if (aKey.equals(AppConstants.statusKeySent)) {
					aEventData[2] = aTag.getChildValue("richtext").replaceAll("\n", "");
				}
			}
		}
		else if (aTagName.equals("inmailhelp")) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened)) {
				// user opens inmailpredef
				aEventData[0] = user;
				aEventData[1] = aTag.getChildValue("richtext").replaceAll("\n", "");
			}
			else {
				aEventData[0] = system;
			}
		}
		else if (aTagName.equals("outmailpredef")) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened) || aKey.equals(AppConstants.statusKeySent)) {
				// user opens and sends outmailpredef
				aEventData[0] = user;
				aEventData[1] = aTag.getChildValue("richtext").replaceAll("\n", "") +
						":" + getAttachmentsUserData(aTag);
			}
			else {
				aEventData[0] = system;
			}
		}
		else if (aTagName.equals("outmailhelp")) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened) || aKey.equals(AppConstants.statusKeySent)) {
				// user opens and sends outmailpredef
				aEventData[0] = user;
				aEventData[1] = aTag.getChildValue("title") + ":" +
						aTag.getChildValue("richtext").replaceAll("\n", "") + ":" +
						getAttachmentsUserData(aTag);
			}
			else {
				aEventData[0] = system;
			}
		}
		else if (aTagName.equals("attachment")) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened)) {
				// user opens attachment
				aEventData[0] = user;
				aEventData[2] = getBlobData(aTag);
			}
			else {
				aEventData[0] = system;
			}
		}
		else {
			logProblem(aComponentCode, aTagName);
		}
		return aEventData;
	}
	
	protected String[] getMemoEventData(IECaseRole aCaseRole, IECaseComponent aCaseComponent, String aComponentCode,
			IXMLTag aTag,  IXMLTag aStatusRootTag,
			String aTagName, String aKey, String aValue, String[] aEventData) {
		if (aTagName.equals(AppConstants.componentElement)) {
			if (aKey.equals(AppConstants.statusKeyStarted) || aKey.equals(AppConstants.statusKeyFinished)) {
				aEventData[0] = usersystem;
			}
			else {
				aEventData[0] = system;
			}
		}
		else if (aTagName.equals("memo")) {
			// get the id of the fragment played
			aEventData[0] = system;
			IXMLTag lReferencedDataPlusStatusTag = getDataByStatusTagReference(aStatusRootTag, aTag).get(1);
			if (lReferencedDataPlusStatusTag != null) {
				aEventData[2] = getBlobData(lReferencedDataPlusStatusTag);
				aEventData[3] = lReferencedDataPlusStatusTag.getName();
				aEventData[4] = CDesktopComponents.sSpring().getXmlManager().getTagKeyValues(lReferencedDataPlusStatusTag, lReferencedDataPlusStatusTag.getDefAttribute(AppConstants.defKeyKey));
			}
		}
		else {
			logProblem(aComponentCode, aTagName);
		}
		return aEventData;
	}
	
	protected String[] getMemosEventData(IECaseRole aCaseRole, IECaseComponent aCaseComponent, String aComponentCode, 
			IXMLTag aTag,  IXMLTag aStatusRootTag, 
			String aTagName, String aKey, String aValue, String[] aEventData) {
		if (aTagName.equals(AppConstants.componentElement)) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened)) {
				aEventData[0] = user;
			}
			else {
				aEventData[0] = system;
			}
		}
		else if (aTagName.equals("memo")) {
			aEventData[0] = user;
			List<IXMLTag> lDataByStatusTagReference = getDataByStatusTagReference(aStatusRootTag, aTag);
			IXMLTag lReferencedStatusRootTag = lDataByStatusTagReference.get(0);
			IXMLTag lReferencedDataPlusStatusTag = lDataByStatusTagReference.get(1);
			if (lReferencedStatusRootTag != null && lReferencedDataPlusStatusTag != null) {
				lDataByStatusTagReference = getDataByStatusTagReference(lReferencedStatusRootTag, lReferencedDataPlusStatusTag);
				lReferencedDataPlusStatusTag = lDataByStatusTagReference.get(1);
				if (lReferencedDataPlusStatusTag != null) {
					aEventData[2] = getBlobData(lReferencedDataPlusStatusTag);
					aEventData[3] = lReferencedDataPlusStatusTag.getName();
					aEventData[4] = CDesktopComponents.sSpring().getXmlManager().getTagKeyValues(lReferencedDataPlusStatusTag, lReferencedDataPlusStatusTag.getDefAttribute(AppConstants.defKeyKey));
				}
			}
		}
		else {
			logProblem(aComponentCode, aTagName);
		}
		return aEventData;
	}
	
	protected String[] getNavigationEventData(IECaseRole aCaseRole, IECaseComponent aCaseComponent, String aComponentCode, IXMLTag aTag, String aTagName, String aKey, String aValue, String[] aEventData) {
		if (aTagName.equals(AppConstants.componentElement)) {
			// user cannot open navigation component himself
			aEventData[0] = system;
		}
		else if (aTagName.equals("location")) {
			// user cannot open navigation component himself directly,
			// is result of clicking on passage
			aEventData[0] = system;
		}
		else if (aTagName.equals("background")) {
			// user cannot open background
			aEventData[0] = system;
		}
		else if (aTagName.equals("passage")) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened)) {
				// user opens passage
				aEventData[0] = user;
			}
			else {
				aEventData[0] = system;
			}
		}
		else if (aTagName.equals("object")) {
			// user cannot open object
			aEventData[0] = system;
		}
		else if (aTagName.equals("clickableobject")) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened)) {
				// user clicks on object
				aEventData[0] = user;
			}
			else {
				aEventData[0] = system;
			}
		}
		else if (aTagName.equals("panel")) {
			// user cannot open object
			aEventData[0] = system;
			aEventData[2] = getBlobData(aTag);
		}
		else if (aTagName.equals("group")) {
			// user cannot open group
			aEventData[0] = system;
		}
		else {
			logProblem(aComponentCode, aTagName);
		}
		return aEventData;
	}
	
	protected String[] getNoteEventData(IECaseRole aCaseRole, IECaseComponent aCaseComponent, String aComponentCode, IXMLTag aTag, String aTagName, String aKey, String aValue, String[] aEventData) {
		if (aTagName.equals(AppConstants.componentElement)) {
			if (aTagName.equals(AppConstants.componentElement)) {
				if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened)) {
					if (aKey.equals(AppConstants.statusKeyOpened) || aValue.equals(AppConstants.statusValueFalse)) {
						// either user closes component directly or it is done automatically by choosing another component
						aEventData[0] = usersystem;
					}
					else {
						// user opens note component
						aEventData[0] = user;
					}
				}
				else {
					aEventData[0] = system;
				}
			}
		}
		else if (aTagName.equals("note")) {
			aEventData[0] = user;
			aEventData[1] = aTag.getChildValue("text").replaceAll("\n", " ");
		}
		else {
			logProblem(aComponentCode, aTagName);
		}
		return aEventData;
	}
	
	protected String[] getNotificationsEventData(IECaseRole aCaseRole, IECaseComponent aCaseComponent, String aComponentCode, IXMLTag aTag, String aTagName, String aKey, String aValue, String[] aEventData) {
		if (aTagName.equals(AppConstants.componentElement)) {
			if (aKey.equals(AppConstants.statusKeyOpened) || aValue.equals(AppConstants.statusValueFalse)) {
				aEventData[0] = user;
			}
			else {
				aEventData[0] = system;
			}
		}
		else if (aTagName.equals("notification")) {
			aEventData[0] = system;
			aEventData[2] = aTag.getChildValue("richtext").replaceAll("\n", "");
		}
		else {
			logProblem(aComponentCode, aTagName);
		}
		return aEventData;
	}
	
	protected String[] getPersonsEventData(IECaseRole aCaseRole, IECaseComponent aCaseComponent, String aComponentCode, IXMLTag aTag, String aTagName, String aKey, String aValue, String[] aEventData) {
		if (aTagName.equals(AppConstants.componentElement)) {
			// user cannot open persons component
			aEventData[0] = system;
		}
		else if (aTagName.equals("person")) {
			// user cannot open person
			aEventData[0] = system;
		}
		else {
			logProblem(aComponentCode, aTagName);
		}
		return aEventData;
	}
	
	protected String[] getProfileEventData(IECaseRole aCaseRole, IECaseComponent aCaseComponent, String aComponentCode, IXMLTag aTag, String aTagName, String aKey, String aValue, String[] aEventData) {
		if (aTagName.equals(AppConstants.componentElement)) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened)) {
				aEventData[0] = user;
			}
			else {
				aEventData[0] = system;
			}
		}
		else if (aTagName.equals("avatar")) {
			aEventData[0] = user;
			aEventData[1] = getBlobData(aTag);
		}
		else if (aTagName.equals("mood")) {
			aEventData[0] = user;
			aEventData[1] = aTag.getChildValue("text").replaceAll("\n", " ");
		}
		else if (aTagName.equals("state")) {
			// user cannot open state
			aEventData[0] = system;
		}
		else {
			logProblem(aComponentCode, aTagName);
		}
		return aEventData;
	}
	
	protected String[] getReferencesEventData(IECaseRole aCaseRole, IECaseComponent aCaseComponent, String aComponentCode, IXMLTag aTag, String aTagName, String aKey, String aValue, String[] aEventData) {
		if (aTagName.equals(AppConstants.componentElement)) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened)) {
				// user normally opens references component himself, but also could be done if
				// component is opened automatically either by script or as location action
				aEventData[0] = usersystem;
			}
			else {
				aEventData[0] = system;
			}
		}
		else if (aTagName.equals("map")) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened) || aKey.equals(AppConstants.statusKeyOutfolded)) {
				// user does open or outfold map
				aEventData[0] = user;
			}
			else {
				aEventData[0] = system;
			}
		}
		else if (aTagName.equals("piece")) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened)) {
				// user opens piece
				aEventData[0] = user;
				aEventData[2] = getBlobData(aTag);
			}
			else {
				aEventData[0] = system;
			}
		}
		else if (aTagName.equals("refpiece")) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened)) {
				// user opens refpiece
				aEventData[0] = user;
				IXMLTag lReferencedTag = getDataTagByDataTagReference(aCaseRole, aCaseComponent, aTag);
				if (lReferencedTag != null) {
					aEventData[2] = getBlobData(lReferencedTag);
				}
			}
			else {
				aEventData[0] = system;
			}
		}
		else {
			logProblem(aComponentCode, aTagName);
		}
		return aEventData;
	}
	
	protected String[] getScoresEventData(IECaseRole aCaseRole, IECaseComponent aCaseComponent, String aComponentCode, IXMLTag aTag, String aTagName, String aKey, String aValue, String[] aEventData) {
		if (aTagName.equals(AppConstants.componentElement)) {
			//only system can hide/show component
			aEventData[0] = system;
		}
		else if (aTagName.equals("score")) {
			aEventData[0] = system;
		}
		return aEventData;
	}
	
	protected String[] getScriptsEventData(IECaseRole aCaseRole, IECaseComponent aCaseComponent, String aComponentCode, IXMLTag aTag, String aTagName, String aKey, String aValue, String[] aEventData) {
		// user cannot open scripts component or conditions within it
		aEventData[0] = system;
		return aEventData;
	}
	
	protected String[] getStatesEventData(IECaseRole aCaseRole, IECaseComponent aCaseComponent, String aComponentCode, IXMLTag aTag, String aTagName, String aKey, String aValue, String[] aEventData) {
		// user cannot open states component or states within it
		aEventData[0] = system;
		return aEventData;
	}
	
	protected String[] getTasksEventData(IECaseRole aCaseRole, IECaseComponent aCaseComponent, String aComponentCode, IXMLTag aTag, String aTagName, String aKey, String aValue, String[] aEventData) {
		if (aTagName.equals(AppConstants.componentElement)) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened)) {
				// user opens tasks component
				aEventData[0] = user;
			}
			else {
				aEventData[0] = system;
			}
		}
		else if (aTagName.equals("task")) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened) || aKey.equals(AppConstants.statusKeyOutfolded)) {
				// user does open or outfold task
				aEventData[0] = user;
			}
			else if (aKey.equals(AppConstants.statusKeyFinished)) {
				// user normally does not finish task, but tasks component can be
				// configured by author to let users finish tasks
				aEventData[0] = systemuser;
			}
			else {
				aEventData[0] = system;
			}
		}
		else {
			logProblem(aComponentCode, aTagName);
		}
		return aEventData;
	}
	
	protected String[] getTutorialEventData(IECaseRole aCaseRole, IECaseComponent aCaseComponent, String aComponentCode, IXMLTag aTag, String aTagName, String aKey, String aValue, String[] aEventData) {
		// TODO set correct even data
		return aEventData;
	}
	
	protected String[] getVideomanualEventData(IECaseRole aCaseRole, IECaseComponent aCaseComponent, String aComponentCode, IXMLTag aTag, String aTagName, String aKey, String aValue, String[] aEventData) {
		if (aTagName.equals(AppConstants.componentElement)) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened)) {
				// user opens videomanual component
				aEventData[0] = user;
			}
			else {
				aEventData[0] = system;
			}
		}
		else if (aTagName.equals("piece")) {
			if (aKey.equals(AppConstants.statusKeySelected) || aKey.equals(AppConstants.statusKeyOpened)) {
				// user opens piece
				aEventData[0] = user;
				aEventData[2] = getBlobData(aTag);
			}
			else {
				aEventData[0] = system;
			}
		}
		else {
			logProblem(aComponentCode, aTagName);
		}
		return aEventData;
	}
	
	protected IXMLTag getDataTagByDataTagReference(IECaseRole aCaseRole, IECaseComponent aCaseComponent, IXMLTag aItem) {
		IXMLTag lItem = null;
		String lCarId = "0";
		String lCacId = "0";
		String lTagId = "0";
		if (aItem.getName().equals("refitem") ||
			aItem.getName().equals("refpiece")) {
//			get item from associated component			
			String lReftype = aItem.getChild("ref").getDefTag().getAttribute(AppConstants.defKeyReftype);
			List<String> lRefIds = getCaseHelper().getRefTagIds(lReftype,""+AppConstants.statusKeySelectedIndex,aCaseRole,aCaseComponent,aItem);
//			only one ref
			String lRefId = (String)lRefIds.get(0);
			if ((lRefId != null) && (!lRefId.equals(""))) {
				String[] lIdArr = lRefId.split(",");
				if (lIdArr.length == 3) {
					lCarId = lIdArr[0];
					lCacId = lIdArr[1];
					lTagId = lIdArr[2];
					lItem = CDesktopComponents.sSpring().getTag(CDesktopComponents.sSpring().getCaseComponent(Integer.parseInt(lCacId)),lTagId);
				}
			}
		}
		return lItem;
	}
	
	protected String getAttachmentsUserData(IXMLTag aStatusTag) {
		String lUserData = "none";
		List<IXMLTag> lAttachmentTags = aStatusTag.getChilds("attachment");
		for (IXMLTag lAttachmentTag : lAttachmentTags) {
			String lData = getBlobData(lAttachmentTag);
			if (!lData.equals("none")) {
				if (lUserData.equals("none")) {
					lUserData = lData;
				}
				else {
					lUserData += "," + lData;
				}
			}
		}
		return lUserData;
	}
	
	protected String getBlobData(IXMLTag aTag) {
		String lData = "none";
		String lBlobId = "";
		for (String childTagName : AppConstants.nodeChildTagsWithBlob) {
			if (lBlobId.equals("")) {
				lBlobId = aTag.getChildValue(childTagName);
			}
		}
		IEBlob lBlob = CDesktopComponents.sSpring().getSBlobHelper().getBlob(lBlobId);
		if (lBlob != null) {
			lData = lBlob.getFilename();
			if (lData == null || lData.equals("")) {
				lData = lBlob.getUrl();
				if (lData == null) {
					lData = "";
				}
			}
		}
		return lData;
	}

	protected List<IXMLTag> getDataByStatusTagReference(IXMLTag aStatusRootTag, IXMLTag aDataPlusStatusTag) {
		List<IXMLTag> lResult = new ArrayList<IXMLTag>();
		IXMLTag lStatusTag = CDesktopComponents.sSpring().getXmlManager().getTagById(aStatusRootTag, aDataPlusStatusTag.getAttribute(AppConstants.keyRefstatusid));
		if (lStatusTag != null) {
			IXMLTag lReferencedDataPlusStatusRootTag = getDataPlusStatusRootTag(lStatusTag.getAttribute(AppConstants.keyRefcacid));
			if (lReferencedDataPlusStatusRootTag != null) {
				lResult.add(getStatusRootTag(lStatusTag.getAttribute(AppConstants.keyRefcacid)));
				lResult.add(CDesktopComponents.sSpring().getXmlManager().getTagById(lReferencedDataPlusStatusRootTag, lStatusTag.getAttribute(AppConstants.keyRefdataid)));
			}
		}
		while (lResult.size() < 2) {
			lResult.add(null);
		}
		return lResult;
	}
	
}