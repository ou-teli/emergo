/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.adm;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefListbox;

/**
 * The Class CAdmMaintainFilesSubFoldersLb.
 */
public class CAdmMaintainFilesSubFoldersLb extends CDefListbox {

	private static final long serialVersionUID = 9026828077925371207L;

	protected CAdmMaintainFilesWnd root = (CAdmMaintainFilesWnd)CDesktopComponents.vView().getComponent("maintainFilesWnd");

	public void onInit() {
		clearSelection();
		getChildren().clear();

		String subPathStr = root.getAdmMaintenanceSubPath();
		List<String> folders = root.fileHelper.getSubFolders(root.getAbsoluteAppPath(), subPathStr);
		String cleanedPath = root.fileHelper.combinePath(root.getAbsoluteAppPath(), subPathStr);
		boolean isRoot = cleanedPath.length() == 3;
		if (!isRoot) {
			folders.add(0, "..");
		}
		for (String folder : folders) {
			Listitem listitem = new Listitem();
			listitem.setAttribute("toAboveFolder", folder.equals(".."));
			listitem.setValue(folder);
			appendChild(listitem);
			Listcell listcell = new Listcell(folder);
			listitem.appendChild(listcell);
		}
	}

	public void onSelect() {
		String subPathStr = root.getAdmMaintenanceSubPath();
		
		//adjust sub path
		Listitem selectedItem = getSelectedItem();
		if ((Boolean)selectedItem.getAttribute("toAboveFolder")) {
			if (subPathStr.equals("") || subPathStr.endsWith("..\\")) {
				subPathStr += "..\\";
			}
			else {
				//strip last sub path
				//strip last '\'
				subPathStr = subPathStr.substring(0, subPathStr.length() - 1);
				//find last '\'
				int pos = subPathStr.lastIndexOf("\\");
				if (pos < 0) {
					subPathStr = "";
				}
				else {
					//strip last sub path
					subPathStr = subPathStr.substring(0, pos + 1);
				}
			}
		}
		else {
			subPathStr += getSelectedItem().getValue() + "\\";
		}

		//clean sub path, e.g., may be '..\..\emergo\'. Remove unnecessary stuff.
		String cleanedPath = root.fileHelper.combinePath(root.getAbsoluteAppPath(), subPathStr) + "\\";
		if (cleanedPath.startsWith(root.getAbsoluteAppPath())) {
			subPathStr = cleanedPath.substring(root.getAbsoluteAppPath().length());
		}
		else if (root.getAbsoluteAppPath().startsWith(cleanedPath)) {
			int numberOfSlashesInPath = StringUtils.countMatches(root.getAbsoluteAppPath(), "\\");
			int numberOfSlashes = StringUtils.countMatches(cleanedPath, "\\");
			subPathStr = "";
			for (int i=0;i<=(numberOfSlashesInPath - numberOfSlashes - 1);i++) {
				subPathStr += "..\\";
			} 
		}

		root.setAdmMaintenanceSubPath(subPathStr);
		((Label)root.getComponent("subPathLabel")).setValue(subPathStr);
		
		Events.postEvent("onInitAll", root.getComponent("webappAndFileMaintenance"), null);
	}
	
}
