/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.adm;

import java.util.Set;

import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.control.CControlHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IEAccountRole;
import nl.surf.emergo.domain.IERole;

/**
 * The Class CAdmAccountsHelper.
 */
public class CAdmAccountsHelper extends CControlHelper {

	/**
	 * Renders one account.
	 *
	 * @param aListbox the a listbox
	 * @param aInsertBefore the a insert before
	 * @param aItem the a item
	 */
	public void renderItem(Listbox aListbox,Listitem aInsertBefore,IEAccount aItem) {
		Listitem lListitem = super.newListitem();
		lListitem.setValue(aItem);
		super.appendListcell(lListitem,""+aItem.getAccId());
		super.appendListcell(lListitem,aItem.getUserid());
		super.appendListcell(lListitem,aItem.getPassword());
		super.appendListcell(lListitem,aItem.getStudentid());
		super.appendListcell(lListitem,aItem.getTitle());
		super.appendListcell(lListitem,aItem.getInitials());
		super.appendListcell(lListitem,aItem.getNameprefix());
		super.appendListcell(lListitem,aItem.getLastname());
		super.appendListcell(lListitem,aItem.getEmail());
		super.appendListcell(lListitem,aItem.getExtradata());
		super.appendListcell(lListitem,""+aItem.getActive());
		super.appendListcell(lListitem,""+aItem.getOpenaccess());
		super.appendListcell(lListitem,getDateStrAsYMD(aItem.getCreationdate()));
		super.appendListcell(lListitem,getRoles(aItem));
		super.appendListcell(lListitem,getLandingpages(aItem));
		super.insertListitem(aListbox,lListitem,aInsertBefore);
	}

	/**
	 * Gets the roles of the account.
	 *
	 * @param aItem the a item
	 *
	 * @return the role codes, comma separated
	 */
	public String getRoles(IEAccount aItem) {
		String lRoles = "";
		Set<IERole> lItems = aItem.getERoles();
		for (IERole lItem : lItems) {
			if (!lRoles.equals("")) lRoles += ", ";
			lRoles += CDesktopComponents.vView().getLabel(lItem.getCode());
		}
		return lRoles;
	}

	/**
	 * Gets the landingpages of the account.
	 *
	 * @param aItem the a item
	 *
	 * @return the landing pages, comma separated
	 */
	public String getLandingpages(IEAccount aItem) {
		String lPages = "";
		Set<IEAccountRole> lItems = aItem.getEAccountRoles();
		for (IEAccountRole lItem : lItems) {
			if (!lPages.equals("")) lPages += ", ";
			lPages += lItem.getLandingpage();
		}
		return lPages;
	}

}