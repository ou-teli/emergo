/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.adm;

import java.util.Collection;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;

import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CInputWnd;
import nl.surf.emergo.utilities.AppHelper;
import nl.surf.emergo.utilities.FileHelper;
import nl.surf.emergo.utilities.ZipHelper;
import nl.surf.emergo.view.VView;

/**
 * The Class CAdmMaintainFilesWnd.
 */
public class CAdmMaintainFilesWnd extends CInputWnd {

	private static final long serialVersionUID = -8693739872090536049L;

	public Boolean isLoggedInViaTomcat = false;

	public static final String admMaintenanceAbsoluteAppPathKey = "admMaintenanceAbsoluteAppPathKey";

	public static final String admMaintenanceWebappRootKey = "admMaintenanceWebappRootKey";

	public static final String admMaintenanceSubPathKey = "admMaintenanceSubPathKey";
	
	public static final String webappZipFileName = "webapp.zip";

	public static final String filesZipFileName = "files.zip";

	public static final String originalEmergoServerProtocol = "https";

	public AppHelper appHelper = new AppHelper();

	public FileHelper fileHelper = new FileHelper();

	public ZipHelper zipHelper = new ZipHelper();
	
	public CControl cControl = CDesktopComponents.cControl();
	
	Boolean lIsLoggedInViaEmergo = null;

	public boolean getIsLoggedInViaEmergo() {
		if (lIsLoggedInViaEmergo == null) {
			lIsLoggedInViaEmergo = (Boolean)cControl.getAccSessAttr("logged_in_via_emergo");
			if (lIsLoggedInViaEmergo == null) {
				lIsLoggedInViaEmergo = new Boolean(false);
			}
			else {
				//Note clear session attribute to reduce the number of session attributes
				cControl.setAccSessAttr("logged_in_via_emergo", null);
			}
		}
		return lIsLoggedInViaEmergo.booleanValue();
	}

	public boolean getIsLoggedInViaTomcat() {
		return isLoggedInViaTomcat;
	}

	public void setIsLoggedInViaTomcat(boolean aValue) {
		isLoggedInViaTomcat = aValue;
	}

	public String getAdmMaintenanceSubPath() {
		String admMaintenanceSubPath = (String)getDesktop().getAttribute("desktop_" + admMaintenanceSubPathKey);
		if (admMaintenanceSubPath == null) {
			admMaintenanceSubPath = "";
			setAdmMaintenanceSubPath(admMaintenanceSubPath);
		}
		return admMaintenanceSubPath;
	}

	public void setAdmMaintenanceSubPath(String path) {
		getDesktop().setAttribute("desktop_" + admMaintenanceSubPathKey, path);
	}

	public String getAbsoluteAppPath() {
		String sessionValue = (String)getDesktop().getAttribute("desktop_" + admMaintenanceAbsoluteAppPathKey);
		if (sessionValue == null) {
			if (!getIsLoggedInViaEmergo()) {
				sessionValue = Executions.getCurrent().getDesktop().getWebApp().getServletContext().getRealPath("");
			}
			else {
				sessionValue = CDesktopComponents.sSpring().getAppManager().getAbsoluteAppPath();
			}
			getDesktop().setAttribute("desktop_" + admMaintenanceAbsoluteAppPathKey, sessionValue);
		}
		return sessionValue;
	}

	public String getAbsoluteAppTempPath() {
		String tempPath = getAbsoluteAppPath() + appHelper.getAbsolutePath(getInitParameter("emergo.temp.path"));
		String subPath = "";
		if (Sessions.getCurrent() != null) {
			subPath = CDesktopComponents.vView().getUniqueTempSubPath();
		}
		tempPath += subPath;
		return tempPath;
	}
	
	public String getWebappRootUrl() {
		if (!getIsLoggedInViaEmergo()) {
			String port = (Executions.getCurrent().getServerPort() == 80 ) ? "" : (":" + Executions.getCurrent().getServerPort());
			return originalEmergoServerProtocol + "://" + Executions.getCurrent().getServerName() + port;
		}
		else {
			return CDesktopComponents.vView().getEmergoRootUrl();
		} 
	}

	public String getWebappRoot() {
		String sessionValue = (String)getDesktop().getAttribute("desktop_" + admMaintenanceWebappRootKey);
		if (sessionValue == null) {
			if (!getIsLoggedInViaEmergo()) {
				sessionValue = Executions.getCurrent().getContextPath();
			}
			else {
				sessionValue = CDesktopComponents.vView().getEmergoWebappsRoot();
			} 
			getDesktop().setAttribute("desktop_" + admMaintenanceWebappRootKey, sessionValue);
		}
		return sessionValue;
	}

	public String getInitParameter(String aKey) {
		if (!getIsLoggedInViaEmergo()) {
			String lValue = Executions.getCurrent().getDesktop().getWebApp().getServletContext().getInitParameter(aKey);
			if (lValue == null) {
				lValue = "";
			}
			return lValue;
		}
		else {
			return VView.getInitParameter(aKey);
		} 
	}
	
	public Component getComponent(String id) {
		if (!getIsLoggedInViaEmergo()) {
			Collection<Component> components = Executions.getCurrent().getDesktop().getComponents();
			if (components != null) {
				for (Component component : components) {
					if (component.getId().equals(id)) {
						return component;
					}
				}
			}
		}
		else {
			return CDesktopComponents.vView().getComponent(id);
		}
		return null;
	}

}
