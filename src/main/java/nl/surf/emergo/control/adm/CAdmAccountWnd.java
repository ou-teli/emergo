/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.adm;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zul.Textbox;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CInputWnd;
import nl.surf.emergo.utilities.PropsValues;

/**
 * The Class CAdmAccountWnd.
 */
public class CAdmAccountWnd extends CInputWnd {

	private static final long serialVersionUID = 2700004392087270793L;

	/** The item. */
	protected Object item;

	public Object getAllERoles() {
		return ((IAccountManager) CDesktopComponents.sSpring().getBean("accountManager")).getAllRoles();
	}

	/**
	 * Gets the item.
	 * 
	 * @param event the event
	 * 
	 * @return the item
	 */
	public Object getItem(CreateEvent event) {
		return event.getArg().get("item");
	}

	/**
	 * Gets the item.
	 * 
	 * @return the item
	 */
	public Object getItem() {
		return item;
	}

	/**
	 * Sets the item.
	 * 
	 * @param aItem the new item
	 */
	public void setItem(Object aItem) {
		item = aItem;
	}

	protected boolean isPasswordDisabled() {
		return item != null && PropsValues.ENCODE_PASSWORD;
	}

	/**
	 * On create fill item and show correct title.
	 * 
	 * @param aEvent the a event
	 */
	@Override
	public void onCreate(CreateEvent aEvent) {
		super.onCreate(aEvent);
		setItem(aEvent.getArg().get("item"));
		// show correct window title
		String lAction = "";
		if (getItem() == null) {
			lAction = CDesktopComponents.vView().getCLabel("new");
		} else {
			lAction = CDesktopComponents.vView().getCLabel("edit");
		}
		String lSubject = CDesktopComponents.vView().getLabel("account");
		setTitle(CDesktopComponents.vView().getLabel("title.template").replace("%1", lAction).replace("%2", lSubject));
		Textbox passwordTextbox = (Textbox) CDesktopComponents.vView().getComponent("password");
		if (passwordTextbox != null) {
			passwordTextbox.setDisabled(isPasswordDisabled());
		}
	}

}
