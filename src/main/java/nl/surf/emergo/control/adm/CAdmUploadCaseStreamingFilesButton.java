/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.adm;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.IFileManager;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefButton;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.utilities.FileHelper;
import nl.surf.emergo.zkspring.SSpring;

/**
 * The Class CUploadCaseStreamingFilesButton. Used to upload files into map for
 * a certain case.
 */
public class CAdmUploadCaseStreamingFilesButton extends CDefButton {

	private static final long serialVersionUID = 8797882457366929904L;

	/**
	 * Instantiates a new c adm upload case streaming files button.
	 *
	 * @param aSpring the a spring
	 */
	public CAdmUploadCaseStreamingFilesButton(SSpring aSpring) {
		/*
		 * NOTE use native! All media are accepted. Use multiple=true! maxsize: the
		 * maximal allowed upload size of the component, in kilobytes, or a negative
		 * value if no limit. native: treating the uploaded file(s) as binary, i.e., not
		 * to convert it to image, audio or text files. multiple: treating the file
		 * chooser allows multiple files to upload, the setting only works with HTML5
		 * supported browsers (since ZK 6.0.0).
		 */
		setUpload("true,maxsize=-1,native,multiple=true");
	}

	/**
	 * On upload, upload files.
	 */
	public void onUpload(UploadEvent event) {
		Object lObject = ((Listitem) getParent().getParent()).getValue();
		if (!(lObject instanceof IECase)) {
			return;
		}
		IECase lItem = (IECase) lObject;
		String lCasId = "" + lItem.getCasId();

		Media[] medias = event.getMedias();
		if (medias != null) {
			IFileManager fileManager = (IFileManager) CDesktopComponents.sSpring().getBean("fileManager");
			String lStreamingPath = CDesktopComponents.sSpring().getAppManager().getAbsoluteStreamingPath() + lCasId
					+ File.separator;
			for (Media media : medias) {
				List<String> errors = new ArrayList<>();
				String fileName = media.getName();
				if (FileHelper.isMediaValid(media, errors)) {
					byte[] lBytes = CDesktopComponents.sSpring().getSBlobHelper().mediaToByteArray(media);
					fileManager.createFile(lStreamingPath, fileName, lBytes);
				} else {
					FileHelper.showUploadErrorMessagebox(media, errors);
				}
			}
		}
	}

}