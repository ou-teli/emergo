/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.adm;

import java.util.List;

import nl.surf.emergo.business.IAppManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CInputBtn;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.view.VView;

/**
 * The Class CAdmRunGroupAccountStatusBtn.
 */
public class CAdmRunGroupAccountStatusBtn extends CInputBtn {

	private static final long serialVersionUID = -1025448409862728585L;

	/** The rungroupaccount. */
	protected IERunGroupAccount rungroupaccount = null;

	/** The run. */
	protected IERun run = null;

	/** The nodename. */
	protected String nodename = "";

	/** The statusType. */
	protected String statusType = "";

	/**
	 * Instantiates a new c adm run group account status btn.
	 */
	public CAdmRunGroupAccountStatusBtn() {
	}

	/**
	 * Instantiates a new c adm run group account status btn.
	 * 
	 * @param aId the a id
	 */
	public CAdmRunGroupAccountStatusBtn(String aId) {
		if (!aId.equals("")) {
			setId(aId);
		}
	}

	/**
	 * Sets the run group account.
	 * 
	 * @param aRunGroupAccount the new run group account
	 */
	public void setRunGroupAccount(IERunGroupAccount aRunGroupAccount) {
		rungroupaccount = aRunGroupAccount;
	}

	/**
	 * Sets the run.
	 * 
	 * @param aRun the new run
	 */
	public void setRun(IERun aRun) {
		run = aRun;
	}

	/**
	 * Set session vars.
	 */
	public void setSessionVars() {
//		set desktop attribute for caserole to use within script window		
		getDesktop().setAttribute("desktop_caserole",rungroupaccount.getERunGroup().getECaseRole());
	}

	/**
	 * Get edit view.
	 * 
	 * @return the view
	 */
	public String getEditView() {
		return VView.v_adm_s_rungroupaccountstatus;
	}

	/**
	 * On click show popup to change status by creating a script action
	 * and if ok change status for run group accordingly.
	 */
	public void onClick() {
		setSessionVars();
		params.put("item", null);
		params.put("new", "false");
		params.put("nodename", nodename);
		params.put("subject", CDesktopComponents.vView().getLabel("adm_s_rungroupaccountstatus." + statusType));

		CDesktopComponents.cControl().setAccSessAttr("is_author", "true");

		//NOTE set rungroupaccount so status data of this rungroupaccount is available in edit popup dialogue. 
		//NOTE set run status, so run group account data will be cached within SSpring
		//TODO setting run status seems to have no effect, so remove?
		CDesktopComponents.sSpring().setRunStatus(AppConstants.runStatusRun);
		//NOTE
		if (run != null) {
			CDesktopComponents.sSpring().setRun(run);
		}
		//NOTE setting rungroupaccount will clear cached run group account data
		if (rungroupaccount != null) {
			CDesktopComponents.sSpring().setRunGroupAccount(rungroupaccount);
		}

		showPopup(getEditView());
	}

	@Override
	protected void handleItem(Object aStatus) {
		clearCachedData();
//		Inserting item and deleting old item is more easy than trying to update one item.
		IXMLTag lActionTag = (IXMLTag)aStatus;
		validateAndHandleActionTag(lActionTag);
	}

	@Override
	protected void cancelItem() {
		clearCachedData();
	}

	protected void clearCachedData() {
		//NOTE clear clear cached data of last run group account
		CDesktopComponents.sSpring().setRunGroupAccount(null);
		CDesktopComponents.sSpring().setRun(null);
		//NOTE clear run status, so run group account data will no longer be cached within SSpring
		CDesktopComponents.sSpring().setRunStatus("");
	}

	/**
	 * Validates and handles script action tag if validation is ok.
	 * 
	 * @param aActionTag the a action tag
	 */
	public void validateAndHandleActionTag(IXMLTag aActionTag) {
		if (aActionTag == null)
			return;
		String lCacId = aActionTag.getChildValue("cacid");
		if ((lCacId.equals("")) || (lCacId.equals("0")))
			return;
		String lTagname = aActionTag.getChildValue("tagname");
		String lStatusid = aActionTag.getChildValue("statusid");
		if ((lStatusid.equals("")) || (lStatusid.equals("0")))
			return;
		IAppManager lBean = (IAppManager)CDesktopComponents.sSpring().getBean("appManager");
		String lStatusKey = lStatusid;
		try {
			lStatusKey = lBean.getStatusKey(Integer.parseInt(lStatusid));
		} catch (NumberFormatException e) {
		}
		if (lStatusKey.equals(""))
			return;
		String lOperatorvalue = aActionTag.getChildValue("operatorvalue");
		if (lOperatorvalue == null)
		//if (lOperatorvalue.equals(""))
			return;
		String lStatusValue = lOperatorvalue;
		String lType = CDesktopComponents.sSpring().getAppManager().getTagOperatorValueType("", lCacId, lTagname, lStatusid, "");
		if (lType.equals("boolean"))
			lStatusValue = lBean.getStatusValue(Integer.parseInt(lStatusValue));
		handleActionTag(aActionTag, lCacId, lStatusKey, lStatusValue);
	}

	/**
	 * Handles action tag. Creates xml data for action and saves it.
	 * 
	 * @param aActionTag the a action tag
	 * @param aCacId the a cac id
	 * @param aStatusKey the a status key
	 * @param aStatusValue the a status value
	 */
	public void handleActionTag(IXMLTag aActionTag, String aCacId, String aStatusKey, String aStatusValue) {
		if (statusType.equals("component")) {
			handleActionTagForComponent(aActionTag, aCacId, aStatusKey, aStatusValue);
		}
		else if (statusType.equals("tag")) {
			handleActionTagForTag(aActionTag, aCacId, aStatusKey, aStatusValue);
		}
	}

	/**
	 * Handles action tag for component.
	 * 
	 * @param aActionTag the a action tag
	 * @param aCacId the a cac id
	 * @param aStatusKey the a status key
	 * @param aStatusValue the a status value
	 */
	public void handleActionTagForComponent(IXMLTag aActionTag, String aCacId, String aStatusKey, String aStatusValue) {
		String lXmlData = CDesktopComponents.sSpring().getSUpdateHelper().getXmlDataTagForComponentStatusUpdate(aStatusKey, aStatusValue);
		saveAction(Integer.parseInt(aCacId), lXmlData);
	}
	
	/**
	 * Handles action tag for tag.
	 * 
	 * @param aActionTag the a action tag
	 * @param aCacId the a cac id
	 * @param aStatusKey the a status key
	 * @param aStatusValue the a status value
	 */
	public void handleActionTagForTag(IXMLTag aActionTag, String aCacId, String aStatusKey, String aStatusValue) {
		String lTagname = aActionTag.getChildValue("tagname");
		if (lTagname.equals(""))
			return;
		List<IXMLTag> lTagIds = aActionTag.getChild("cacid").getChilds("tagid");
		if (lTagIds.size() > 0) {
			for (IXMLTag lTagId : lTagIds) {
				String lXmlData = CDesktopComponents.sSpring().getSUpdateHelper().getXmlDataTagForTagStatusUpdate(lTagname, lTagId.getValue(), aStatusKey, aStatusValue, false);
				saveAction(Integer.parseInt(aCacId), lXmlData);
			}
		}
		List<IXMLTag> lTagTemplates = aActionTag.getChild("cacid").getChilds("tagtemplate");
		if (lTagTemplates.size() > 0) {
			for (IXMLTag lTagTemplate : lTagTemplates) {
				String lXmlData = CDesktopComponents.sSpring().getSUpdateHelper().getXmlDataTagForTagStatusUpdate(lTagname, lTagTemplate.getValue(), aStatusKey, aStatusValue, true);
				saveAction(Integer.parseInt(aCacId), lXmlData);
			}
		}
	}
	
	/**
	 * Saves action in table rungroupcasecomponentupdate to be processed if run group
	 * has Emergo player opened.
	 * 
	 * @param aCacId the a cac id
	 * @param aXmldata the a xmldata
	 */
	public void saveAction(int aCacId, String aXmldata) {
		CDesktopComponents.sSpring().getSUpdateHelper().saveXmlDataComponentOrTagStatusUpdate(rungroupaccount.getERunGroup(), aCacId, aXmldata);
	}

}
