/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.adm;

import org.zkoss.zul.Textbox;

import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefButton;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpring;

/**
 * The Class CAdmConfigureOkBtn.
 */
public class CAdmConfigureOkBtn extends CDefButton {

	private static final long serialVersionUID = 9044260735254872150L;

	/** The view. */
	private VView vView = CDesktopComponents.vView();

	/** The spring. */
	private SSpring sSpring = CDesktopComponents.sSpring();

	/**
	 * On click save configuration.
	 */
	public void onClick() {
		String[] lSysValueIds = new String[] {AppConstants.syskey_smtpserver, AppConstants.syskey_smtpnoreplysender, AppConstants.syskey_gmapskey};
		for (int i=0;i<lSysValueIds.length;i++) {
			//if input value is not equal to stored value, update it
			if (!((Textbox)vView.getComponent(lSysValueIds[i])).getValue().equals(sSpring.getAppManager().getSysvalue(lSysValueIds[i]))) {
				sSpring.getAppManager().setSysvalue(lSysValueIds[i], ((Textbox)vView.getComponent(lSysValueIds[i])).getValue());
			}
		}
	}

}
