/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.adm;

import java.util.List;

import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefListbox;

/**
 * The Class CAdmMaintainFilesWebappsLb.
 */
public class CAdmMaintainFilesWebappsLb extends CDefListbox {

	private static final long serialVersionUID = 2337608295879615406L;

	protected CAdmMaintainFilesWnd root = (CAdmMaintainFilesWnd)CDesktopComponents.vView().getComponent("maintainFilesWnd");

	public void onInit() {
		clearSelection();
		getChildren().clear();

		String webapp = root.getWebappRoot().substring(1);
		String subPathStr = "..\\";
		List<String> folders = root.fileHelper.getSubFolders(root.getAbsoluteAppPath(), subPathStr);
		String cleanedPath = root.fileHelper.combinePath(root.getAbsoluteAppPath(), subPathStr);
		boolean isRoot = cleanedPath.length() == 3;
		if (!isRoot) {
			folders.add(0, "..");
		}
		for (String folder : folders) {
			Listitem listitem = new Listitem();
			listitem.setAttribute("toAboveFolder", folder.equals(".."));
			listitem.setValue(folder);
			appendChild(listitem);
			if (folder.equals(webapp)) {
				setSelectedItem(listitem);
			}
			Listcell listcell = new Listcell(folder);
			listitem.appendChild(listcell);
		}
	}

	public void onSelect() {
		String absoluteAppPathStr = root.getAbsoluteAppPath();
		String webapp = root.getWebappRoot().substring(1);
		if (absoluteAppPathStr.endsWith("\\" + webapp + "\\")) {
			String newWebapp = getSelectedItem().getValue();
			String newAbsoluteAppPathStr = absoluteAppPathStr.substring(0, absoluteAppPathStr.length() - ("\\" + webapp + "\\").length()) + "\\" + newWebapp + "\\";
			getDesktop().setAttribute("desktop_" + CAdmMaintainFilesWnd.admMaintenanceAbsoluteAppPathKey, newAbsoluteAppPathStr);
			getDesktop().setAttribute("desktop_" + CAdmMaintainFilesWnd.admMaintenanceWebappRootKey, "/" + newWebapp);
			getDesktop().setAttribute("desktop_" + CAdmMaintainFilesWnd.admMaintenanceSubPathKey, "");
			((Label)root.getComponent("subPathLabel")).setValue("");
		}
		Events.postEvent("onInitAll", root.getComponent("webappAndFileMaintenance"), null);
	}
	
}
