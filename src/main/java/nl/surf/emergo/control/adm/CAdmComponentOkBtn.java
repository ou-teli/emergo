/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.adm;

import java.util.ArrayList;
import java.util.List;

import org.doomdark.uuid.UUIDGenerator;

import nl.surf.emergo.business.IComponentManager;
import nl.surf.emergo.control.CControlHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefButton;
import nl.surf.emergo.domain.IEComponent;

/**
 * The Class CAdmComponentOkBtn.
 */
public class CAdmComponentOkBtn extends CDefButton {

	private static final long serialVersionUID = -5199922262381249163L;

	/**
	 * On click create new item or update item if no errors and close edit window.
	 */
	public void onClick() {
		CAdmComponentWnd lWindow = (CAdmComponentWnd)getRoot();
		IEComponent lItem = (IEComponent)lWindow.getItem();
		IComponentManager componentManager = (IComponentManager)CDesktopComponents.sSpring().getBean("componentManager");
		Boolean lNew = false;
		if (lItem == null) {
			//new
			lNew = true;
			lItem = componentManager.getNewComponent();
			String lUUID = UUIDGenerator.getInstance().generateTimeBasedUUID().toString();
			lItem.setUuid(lUUID);
			lItem.setEAccount(CDesktopComponents.sSpring().getAccount());
		}
		else
			//update
			lItem = (IEComponent)lItem.clone();
		lItem.setComComId(CControlHelper.getListboxValueAsInt(this, "comComId"));
		lItem.setCode(CControlHelper.getTextboxValue(this, "code"));
		lItem.setType(CControlHelper.getListboxValueAsInt(this, "type"));
		lItem.setVersion(CControlHelper.getTextboxValueAsInt(this, "version", 1));
		lItem.setXmldefinition(CControlHelper.getTextboxValue(this, "xmldefinition"));
		lItem.setMultiple(CControlHelper.isCheckboxChecked(this, "multiple"));
		lItem.setActive(CControlHelper.isCheckboxChecked(this, "active"));

		List<String[]> lErrors = new ArrayList<String[]>(0);
		if (lNew)
			lErrors = componentManager.newComponent(lItem);
		else
			lErrors = componentManager.updateComponent(lItem);
		if ((lErrors == null) || (lErrors.size() == 0)) {
//		Get component so date format, name etc. is correct.
			lItem = componentManager.getComponent(lItem.getComId());
			lWindow.setAttribute("item",lItem);
		}
		else {
			lWindow.setAttribute("item",null);
			CDesktopComponents.cControl().showErrors(this,lErrors);
		}
		lWindow.detach();
	}
}
