/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.adm;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zul.Textbox;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefButton;

/**
 * The Class CAdmQueryGetSqlBtn. When clicked it gets SQL.
 */
public class CAdmQueryGetSqlBtn extends CDefButton {

	private static final long serialVersionUID = 3082658934563792945L;

	public void onClick() {
		Textbox lSqlGetT = (Textbox)getFellow("sqlGetArbitrary");
		Textbox lResultGetT = (Textbox)getFellow("resultGetArbitrary");
		lResultGetT.setValue("");
		List<String> lErrors = new ArrayList<String>();
		List<Object> lItems = CDesktopComponents.sSpring().getAppManager().getSqlResult(lSqlGetT.getValue(), lErrors);
		StringBuffer lResult = new StringBuffer();
		if (lErrors.size() > 0) {
			for (String lError : lErrors) {
				lResult.append(lError);
				lResult.append("\n");
			}
		}
		else {
			if (lItems == null || lItems.size() == 0) {
				lResult.append(CDesktopComponents.vView().getLabel("adm_query.nothing_found"));
			}
			else {
				String lPrefix = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
				int lPrefixLength = lPrefix.length();
				lResult.append(lPrefix);
				lResult.append("\n<sqldata>\n");
				for (Object lItem : lItems) {
					Object[] lObject = null;
					if (lItem instanceof Object[]) {
						lObject = (Object[])lItem;
					}
					else {
						lObject = new Object[]{lItem};
					}
					lResult.append("<object>");
					int lFieldNumber = 1;
					for (int i = 0; i < lObject.length; i++) {
						lResult.append("<value");
						lResult.append(lFieldNumber);
						lResult.append(">");
						if (lObject[i] instanceof String) {
							String lValue = (String)lObject[i];
							if (lValue.indexOf(lPrefix) == 0) {
								lValue = lValue.substring(lPrefixLength);
							}
							lResult.append(lValue);
						}
						else {
							lResult.append(lObject[i]);
						}
						lResult.append("</value");
						lResult.append(lFieldNumber);
						lResult.append(">");
						lFieldNumber++;
					}
					lResult.append("</object>");
					lResult.append("\n");
				}
				lResult.append("</sqldata>");
			}
		}
		lResultGetT.setValue(lResult.toString());

	}
	
}
