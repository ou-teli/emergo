/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.adm;

import java.util.List;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefLabel;
import nl.surf.emergo.domain.IEAccount;

/**
 * The Class CAdmRunsLabel.
 */
public class CAdmRunsLabel extends CDefLabel {

	private static final long serialVersionUID = 7470794784638751625L;

	/**
	 * On create set value.
	 */
	public void onCreate() {
		String lValue = "";
		List<IEAccount> lSelectedAccounts = (List<IEAccount>)CDesktopComponents.cControl().getAccSessAttr(AppConstants.selected_accounts);
		if (lSelectedAccounts != null && lSelectedAccounts.size() == 1) {
			lValue = CDesktopComponents.vView().getLabel("adm_runs.title_one_account").
					replace("%1", ((IAccountManager)CDesktopComponents.sSpring().getBean("accountManager")).getAccountName((IEAccount)lSelectedAccounts.get(0)));
		}
		else {
			lValue = CDesktopComponents.vView().getCLabel("runs");
		}
		setValue(lValue);
	}

}
