/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.adm;

import org.zkoss.zul.Listitem;

import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefButton;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.view.VView;

/**
 * The Class CAdmRunRedirectBtn. When clicked it opens the run group accounts window for the selected run.
 */
public class CAdmRunRedirectBtn extends CDefButton {

	private static final long serialVersionUID = -6072779687840768433L;

	/**
	 * Instantiates a new c adm run redirect btn.
	 */
	public CAdmRunRedirectBtn() {
		setLabel(CDesktopComponents.vView().getLabel("members"));
	}

	/**
	 * On click open run group accounts window for the selected run.
	 */
	public void onClick() {
		IERun lItem = (IERun)((Listitem)getParent().getParent()).getValue();
		CControl cControl = CDesktopComponents.cControl();
		cControl.setAccSessAttr("case",lItem.getECase());
		cControl.setAccSessAttr("run",lItem);
		cControl.setAccSessAttr("runId",""+lItem.getRunId());
		cControl.setAccSessAttr("runName",""+lItem.getName());
		CDesktopComponents.vView().redirectToView(VView.v_adm_rungroupaccounts);
	}
}
