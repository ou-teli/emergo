/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.adm;

import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefButton;
import nl.surf.emergo.view.VView;

/**
 * The Class CAdmMaintainFilesRestoreFileBtn.
 */
public class CAdmMaintainFilesRestoreFileBtn extends CDefButton {

	private static final long serialVersionUID = 6301465916864083973L;

	protected CAdmMaintainFilesWnd root = (CAdmMaintainFilesWnd)CDesktopComponents.vView().getComponent("maintainFilesWnd");

	public void onClick() {
		VView vView = CDesktopComponents.vView();
		Listbox listbox = (Listbox)root.getComponent("filesToRestore");
		if (listbox.getSelectedItem() == null) {
			vView.showMessagebox(getRoot(), vView.getLabel("adm_maintain_files.choose_file.warning"), vView.getLabel("messagebox.title.warnings"), Messagebox.OK, Messagebox.EXCLAMATION);
		}
		else {
			if (root.fileHelper.restoreFile(root.getAbsoluteAppPath(), root.getAdmMaintenanceSubPath(), listbox.getSelectedItem().getValue())) {
				Events.postEvent("onInitAll", root.getComponent("webappAndFileMaintenance"), null);
			}
		}
	}

}
