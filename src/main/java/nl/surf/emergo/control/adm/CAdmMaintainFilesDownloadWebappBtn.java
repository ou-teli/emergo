/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.adm;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.zkoss.zhtml.A;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Messagebox;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefButton;
import nl.surf.emergo.view.VView;

/**
 * The Class CAdmMaintainFilesDownloadWebappBtn.
 */
public class CAdmMaintainFilesDownloadWebappBtn extends CDefButton {

	private static final long serialVersionUID = 9066784421235616336L;

	protected CAdmMaintainFilesWnd root = (CAdmMaintainFilesWnd)CDesktopComponents.vView().getComponent("maintainFilesWnd");

	public void onClick() {
		root.getComponent("emergoDownloadLinkDiv").setVisible(false);
		
		String absoluteAppPath = root.getAbsoluteAppPath();
		List<String> subPathsToExclude = new ArrayList<String>();
		subPathsToExclude.add(absoluteAppPath + "blob\\");
		subPathsToExclude.add(absoluteAppPath + "community\\");
		subPathsToExclude.add(absoluteAppPath + "experiments\\");
		subPathsToExclude.add(absoluteAppPath + "landingpages\\");
		subPathsToExclude.add(absoluteAppPath + "streaming\\");
		subPathsToExclude.add(absoluteAppPath + "temp\\");
		if (!((Checkbox)root.getComponent("includeLibFolder")).isChecked()) {
			subPathsToExclude.add(absoluteAppPath + "WEB-INF\\lib\\");
		}
		List<String> emergoFileNames = root.fileHelper.getFileNamesExcludingSubPaths(absoluteAppPath, true, subPathsToExclude);
		String downloadFileName = root.getAbsoluteAppTempPath() + CAdmMaintainFilesWnd.webappZipFileName;

		boolean success = root.zipHelper.packageFilesToZip(emergoFileNames, absoluteAppPath, "", "", new File(downloadFileName));
		
		VView vView = CDesktopComponents.vView();
		if (success) {
			String subUrl = "";
			if (Sessions.getCurrent() != null) {
				subUrl = CDesktopComponents.vView().getUniqueTempSubPath();
			}
			String url = root.getWebappRootUrl() + root.getWebappRoot() + root.getInitParameter("emergo.temp.path") + subUrl + CAdmMaintainFilesWnd.webappZipFileName;
			A downloadLink = (A)root.getComponent("emergoDownloadLink");
			downloadLink.setDynamicProperty("href", url);
			downloadLink.setDynamicProperty("innerHTML", CAdmMaintainFilesWnd.webappZipFileName);
			root.getComponent("emergoDownloadLinkDiv").setVisible(true);
			vView.showMessagebox(getRoot(), vView.getLabel("adm_maintain_files.download_webapp.info").
					replace("%1", CAdmMaintainFilesWnd.webappZipFileName), vView.getLabel("messagebox.title.information"), Messagebox.OK, Messagebox.INFORMATION);
		}
		else {
			root.getComponent("emergoDownloadLinkDiv").setVisible(false);
			vView.showMessagebox(getRoot(), vView.getLabel("adm_maintain_files.download_webapp.error").
					replace("%1", CAdmMaintainFilesWnd.webappZipFileName), vView.getLabel("messagebox.title.errors"), Messagebox.OK, Messagebox.ERROR);
		}
	}

}
