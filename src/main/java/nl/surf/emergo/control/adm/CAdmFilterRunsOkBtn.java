/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.adm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.ICaseManager;
import nl.surf.emergo.business.IRunManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CFilterAccountsOkBtn;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.view.VView;

/**
 * The Class CAdmFilterRunsOkBtn. When clicked it opens the runs filtered by the entered values.
 */
public class CAdmFilterRunsOkBtn extends CFilterAccountsOkBtn {

	private static final long serialVersionUID = -4187002006889270058L;

	/**
	 * On click open runs window for the selected accounts.
	 */
	public void onClick() {
		saveFilterValues();
		
		saveFilteredItems(AppConstants.account_filter_map, AppConstants.selected_accounts);
		saveFilteredItems(AppConstants.run_filter_map, AppConstants.selected_runs);
		saveFilteredItems(AppConstants.case_filter_map, AppConstants.selected_cases);
		
		CDesktopComponents.vView().redirectToView(VView.v_adm_runs);
	}

	protected void saveFilteredItems(String pMapId, String pSessAttrSaveId) {
		if (areFilterValuesEmpty(pMapId)) {
			CDesktopComponents.cControl().setAccSessAttr(pSessAttrSaveId, null);
		}
		else {
			if (pMapId.equals(AppConstants.account_filter_map)) {
				List<IEAccount> lItems = ((IAccountManager)CDesktopComponents.sSpring().getBean("accountManager")).getAllAccountsFilter((Map<String, String>)CDesktopComponents.cControl().getAccSessAttr(pMapId));
				CDesktopComponents.cControl().setAccSessAttr(pSessAttrSaveId, lItems);
			} else {
				List<Integer> lItemIds = new ArrayList<Integer>();
				Map<String, String> lFilterMap = (Map<String, String>)CDesktopComponents.cControl().getAccSessAttr(pMapId);
				Map<String, String> lFilterMapCopy = new HashMap<String, String>();
				//NOTE transform session run name and case name ids to valid keys for rundao and casedao
		        for (Map.Entry<String, String> lEntry : lFilterMap.entrySet()) {
		            String lKey = lEntry.getKey();
		            String lValue = lEntry.getValue();
		            if (lKey.equals("run") || lKey.equals("case"))
		            	lKey = "name";
		            lFilterMapCopy.put(lKey, lValue);
		        }
				if (pMapId.equals(AppConstants.run_filter_map)) {
					List<IERun> lItems = ((IRunManager)CDesktopComponents.sSpring().getBean("runManager")).getAllRunsFilter(lFilterMapCopy);
					for (IERun lRun : lItems) {
						lItemIds.add(lRun.getRunId());
					}
				} else {
					List<IECase> lItems = ((ICaseManager)CDesktopComponents.sSpring().getBean("caseManager")).getAllCasesFilter(lFilterMapCopy);
					for (IECase lCase : lItems) {
						lItemIds.add(lCase.getCasId());
					}
				}
				CDesktopComponents.cControl().setAccSessAttr(pSessAttrSaveId, lItemIds);
			}
		}
	}

}
