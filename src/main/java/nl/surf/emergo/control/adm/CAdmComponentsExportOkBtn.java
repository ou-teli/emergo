/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.adm;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zhtml.A;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CImportExportHelper;
import nl.surf.emergo.control.CImportExportWnd;
import nl.surf.emergo.control.def.CDefButton;
import nl.surf.emergo.domain.IEComponent;

/**
 * The Class CAdmComponentsExportOkBtn.
 */
public class CAdmComponentsExportOkBtn extends CDefButton {

	private static final long serialVersionUID = -8099680137972101124L;

	/**
	 * On click create ZIP file.
	 *
	 */
	public void onClick() {
		List<IEComponent> lComponents = new ArrayList<IEComponent>();
		for (Listitem lListitem : ((Listbox)getFellow("comComId")).getSelectedItems()) {
			if (lListitem.getAttribute("component") != null) {
				lComponents.add((IEComponent)lListitem.getAttribute("component"));
			}
		}
		CImportExportHelper lHelper = ((CImportExportWnd)getRoot()).getImportExportHelper();
		String lUrl = lHelper.exportComponents(lComponents);
		if (lUrl.equals("")) {
			CDesktopComponents.vView().showMessagebox(getRoot(), CDesktopComponents.vView().getLabel("adm_s_components_export.error.body"), CDesktopComponents.vView().getLabel("adm_s_components_export.error.title"), Messagebox.OK, Messagebox.ERROR);
		}
		else {
			A lDownloadLink = (A)CDesktopComponents.vView().getComponent("downloadLink");
			lDownloadLink.setDynamicProperty("href", lUrl);
			lDownloadLink.setDynamicProperty("innerHTML", CDesktopComponents.vView().getLabel("components"));
			getFellow("componentListboxWnd").setVisible(false);
			CDesktopComponents.vView().getComponent("downloadLinkWnd").setVisible(true);
		}
	}
	
}
