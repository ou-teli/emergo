/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.adm;

import java.util.ArrayList;
import java.util.List;

import nl.surf.emergo.business.IAccountContextManager;
import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefCheckbox;
import nl.surf.emergo.domain.IEAccountContext;

/**
 * The Class CAdmContextAccountCb.
 */
public class CAdmContextAccountCb extends CDefCheckbox {

	private static final long serialVersionUID = 5089192489590463971L;

	/** The account manager. */
	protected IAccountManager accountManager = (IAccountManager)CDesktopComponents.sSpring().getBean("accountManager");
	
	/** The account manager. */
	protected IAccountContextManager accountContextManager = (IAccountContextManager)CDesktopComponents.sSpring().getBean("accountContextManager");
	
	/** The errors. */
	protected List<String[]> errors = new ArrayList<String[]>(0);

	/**
	 * On check add run group and run group account for chosen account and case role, if it does not exist.
	 * And set active of run group to value of checked.
	 */
	public void onCheck() {
		int lAccId = Integer.parseInt((String)getAttribute("accId"));
//		get context
		int lConId = Integer.parseInt((String)CDesktopComponents.cControl().getAccSessAttr("contextId"));
//		determine if accountContext exists
		IEAccountContext lAccountContext = getAccountContext(lAccId,lConId);
		if (lAccountContext == null) {
//			add account context
			lAccountContext = addAccountContext(lAccId,lConId,isChecked());
		}
		if (lAccountContext != null) {
//			set checked/unchecked
			lAccountContext.setActive(isChecked());
			accountContextManager.updateAccountContext(lAccountContext);
		}
	}

	/**
	 * Gets the account context.
	 * 
	 * @param aAccId the account id
	 * @param aConId the context id
	 * 
	 * @return the account context
	 */
	private IEAccountContext getAccountContext(int aAccId,int aConId) {
		for (IEAccountContext accountContext : accountContextManager.getAllAccountContextsByAccountId(aAccId)) {
			if (accountContext.getConConId() == aConId)
				return accountContext; 
		}
		return null;
	}

	/**
	 * Adds account context.
	 * 
	 * @param aAccId the account id
	 * @param aConId the context id
	 * @param aActive whether or not the account context is active
	 * 
	 * @return the account context
	 */
	private IEAccountContext addAccountContext(int aAccId,int aConId, boolean aActive) {
		CAdmContextAccountsHelper cHelper = new CAdmContextAccountsHelper();
		IEAccountContext lAccountContext = cHelper.addAccountContext(aAccId, aConId, aActive);
		if (lAccountContext == null) {
			errors = cHelper.getErrors();
		}
		return lAccountContext;
	}

	/**
	 * Gets the errors.
	 * 
	 * @return the errors
	 */
	public List<String[]> getErrors() {
		return errors;
	}
}
