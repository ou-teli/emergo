/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.adm;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Div;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefButton;
import nl.surf.emergo.view.VView;

/**
 * The Class CAdmMaintainFilesDeleteFilesBtn.
 */
public class CAdmMaintainFilesDeleteFilesBtn extends CDefButton {

	private static final long serialVersionUID = -6053388254774633125L;

	protected CAdmMaintainFilesWnd root = (CAdmMaintainFilesWnd)CDesktopComponents.vView().getComponent("maintainFilesWnd");

	public void onClick() {
		VView vView = CDesktopComponents.vView();
		Listbox filesToDelete = (Listbox)root.getComponent("filesToDelete");
		if (filesToDelete.getSelectedItems().size() == 0) {
			vView.showMessagebox(getRoot(), vView.getLabel("adm_maintain_files.choose_files.warning"), vView.getLabel("messagebox.title.warnings"), Messagebox.OK, Messagebox.EXCLAMATION);
		}
		else {
			Div lTransparentDiv = vView.initQuasiModalBehavior(getRoot());
			Messagebox.show(
					vView.getLabel("adm_maintain_files.choose_files.confirmation"), 
					vView.getLabel("messagebox.title.confirmation"), 
					Messagebox.OK | Messagebox.CANCEL, 
					Messagebox.QUESTION,
					new EventListener<Event>() {
				        public void onEvent(Event evt) {
				            switch (((Integer)evt.getData()).intValue()) {
				            case Messagebox.OK: 
								String subPathStr = root.getAdmMaintenanceSubPath();
								String resultPath = root.fileHelper.combinePath(root.getAbsoluteAppPath(), subPathStr) + "/";
								for (Listitem listitem : filesToDelete.getSelectedItems()) {
									root.fileHelper.deleteFile(resultPath + listitem.getValue());
								}
								Events.postEvent("onInitAll", root.getComponent("webappAndFileMaintenance"), null);
								vView.undoQuasiModalBehavior(lTransparentDiv);
				            	break;
				            case Messagebox.CANCEL: 
								vView.undoQuasiModalBehavior(lTransparentDiv);
				            	break;
				            }
				        }
				    }
				);
		}
	}

}
