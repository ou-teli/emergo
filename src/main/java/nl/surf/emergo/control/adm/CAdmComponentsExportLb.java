/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.adm;

import java.util.List;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listhead;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.IComponentManager;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefListbox;
import nl.surf.emergo.domain.IEComponent;

/**
 * The Class CAdmComponentsExportLb.
 */
public class CAdmComponentsExportLb extends CDefListbox {

	private static final long serialVersionUID = -8552777901199123650L;

	/**
	 * On create show components to export.
	 *
	 * @param aEvent the a event
	 */
	public void onCreate(CreateEvent aEvent) {
		Listhead lListhead = new Listhead();
		appendChild(lListhead);
		Listheader lListheader = new Listheader(CDesktopComponents.vView().getCLabel("component"));
		lListhead.appendChild(lListheader);
		lListheader = new Listheader(CDesktopComponents.vView().getCLabel("active"));
		lListhead.appendChild(lListheader);
		List<IEComponent> lComponents = ((IComponentManager)CDesktopComponents.sSpring().getBean("componentManager")).getAllComponents();
		for (IEComponent lComponent : lComponents) {
			Listitem listitem = new Listitem();
			listitem.setAttribute("component", lComponent);
			Listcell listcell = new Listcell(lComponent.getCode());
			listitem.appendChild(listcell);
			listcell = new Listcell("" + lComponent.getActive());
			listitem.appendChild(listcell);
			appendChild(listitem);
			if (lComponent.getActive()) {
				addItemToSelection(listitem);
			}
		}
	}
	
}
