/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.adm;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zul.Button;

import nl.surf.emergo.business.IContextManager;
import nl.surf.emergo.business.IRunGroupAccountManager;
import nl.surf.emergo.business.IRunManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefListbox;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IEContext;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunGroupAccount;

/**
 * The Class CAdmRunsLb.
 */
public class CAdmRunsLb extends CDefListbox {

	private static final long serialVersionUID = -4812390233838560962L;

	/** The contextManager. */
	protected IContextManager contextManager = null;
	
	/** The hasContexts. */
	protected boolean hasContexts = false;

	/**
	 * On create render listitems.
	 */
	public void onCreate() {
		contextManager = (IContextManager)CDesktopComponents.sSpring().getBean("contextManager");
		List<IEContext> lContexts = contextManager.getAllContexts();
		hasContexts = (lContexts.size() > 0);
		
		getItems().clear();
		
		List<IERun> lItems = null;
		//NOTE account may be selected in adm_accounts.zul which is the case if an adm wants to get an overview of runs for this account.
		//or multiple accounts may be selected when filtering on accounts
		List<IEAccount> lSelectedAccounts = (List<IEAccount>)CDesktopComponents.cControl().getAccSessAttr(AppConstants.selected_accounts);
		if (lSelectedAccounts == null) {
			//show all runs
			lItems = ((IRunManager)CDesktopComponents.sSpring().getBean("runManager")).getAllRuns();
		}
		else {
			//show runs for selected accounts
			List<Integer> lAccIds = new ArrayList<Integer>();
			for (IEAccount lSelectedAccount : lSelectedAccounts) {
				lAccIds.add(lSelectedAccount.getAccId());
			}
			List<IERunGroupAccount> lRunGroupAccounts = ((IRunGroupAccountManager)CDesktopComponents.sSpring().getBean("runGroupAccountManager")).getAllRunGroupAccountsByAccIds(lAccIds);
			List<Integer> lAccountRunIds = new ArrayList<Integer>();
			for (IERunGroupAccount lRunGroupAccount : lRunGroupAccounts) {
				int lRunId = lRunGroupAccount.getERunGroup().getERun().getRunId();
				if (!lAccountRunIds.contains(lRunId) && lRunGroupAccount.getERunGroup().getActive()) {
					lAccountRunIds.add(lRunId);
				}
			}
			lItems = ((IRunManager)CDesktopComponents.sSpring().getBean("runManager")).getAllRunsByRunIds(lAccountRunIds);
		}
		
		List<Integer> lFilterRunIds = (List<Integer>)CDesktopComponents.cControl().getAccSessAttr(AppConstants.selected_runs);
		List<Integer> lFilterCaseIds = (List<Integer>)CDesktopComponents.cControl().getAccSessAttr(AppConstants.selected_cases);
		
		CAdmRunsHelper cHelper = new CAdmRunsHelper();
		for (IERun lItem : lItems) {
			Boolean lRender = true;
			if (lFilterRunIds != null) {
				if (!lFilterRunIds.contains(lItem.getRunId()))
					lRender = false;
			}
			if (lFilterCaseIds != null) {
				if (!lFilterCaseIds.contains(lItem.getECase().getCasId()))
					lRender = false;
			}
			if (lRender && (lItem.getStatus() == AppConstants.run_status_runnable)) {
				cHelper.renderItem(this,null,lItem);
			}
		}
		restoreSettings();
	}

	/**
	 * Has Contexts.
	 * 
	 * @return if has contexts
	 */
	public boolean hasContexts() {
		return hasContexts;
	}

	/**
	 * On select update buttons.
	 */
	public void onSelect() {
		((Button)getFellow("editBtn")).setDisabled(false);
	}

}
