/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.adm;

import java.io.File;
import java.util.List;

import org.zkoss.zhtml.A;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Messagebox;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefButton;
import nl.surf.emergo.view.VView;

/**
 * The Class CAdmMaintainFilesDownloadZipfileBtn.
 */
public class CAdmMaintainFilesDownloadZipfileBtn extends CDefButton {

	private static final long serialVersionUID = -8294599778172949907L;

	protected CAdmMaintainFilesWnd root = (CAdmMaintainFilesWnd)CDesktopComponents.vView().getComponent("maintainFilesWnd");

	public void onClick() {
		root.getComponent("downloadLinkDiv").setVisible(false);
		
		String resultPath = root.fileHelper.combinePath(root.getAbsoluteAppPath(), root.getAdmMaintenanceSubPath());
		List<String> fileNames = root.fileHelper.getFileNamesExcludingSubPaths(resultPath, ((Checkbox)root.getComponent("includeSubFolders")).isChecked(), null);
		String downloadFileName = root.getAbsoluteAppTempPath() + CAdmMaintainFilesWnd.filesZipFileName;

		boolean success = root.zipHelper.packageFilesToZip(fileNames, resultPath, "", "", new File(downloadFileName));
		
		VView vView = CDesktopComponents.vView();
		if (success) {
			String subUrl = "";
			if (Sessions.getCurrent() != null) {
				subUrl = CDesktopComponents.vView().getUniqueTempSubPath();
			}
			String url = root.getWebappRootUrl() + root.getWebappRoot() + root.getInitParameter("emergo.temp.path") + subUrl + CAdmMaintainFilesWnd.filesZipFileName;
			A downloadLink = (A)root.getComponent("downloadLink");
			downloadLink.setDynamicProperty("href", url);
			downloadLink.setDynamicProperty("innerHTML", CAdmMaintainFilesWnd.filesZipFileName);
			root.getComponent("downloadLinkDiv").setVisible(true);
			vView.showMessagebox(getRoot(), vView.getLabel("adm_maintain_files.download_zipfile.info").replace("%1", CAdmMaintainFilesWnd.filesZipFileName), vView.getLabel("messagebox.title.information"), Messagebox.OK, Messagebox.INFORMATION);
		}
		else {
			root.getComponent("downloadLinkDiv").setVisible(false);
			vView.showMessagebox(getRoot(), vView.getLabel("adm_maintain_files.download_zipfile.error").replace("%1", CAdmMaintainFilesWnd.filesZipFileName), vView.getLabel("messagebox.title.errors"), Messagebox.OK, Messagebox.ERROR);
		}
	}

}
