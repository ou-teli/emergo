/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.adm;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CAccountsImportBtn;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IERole;
import nl.surf.emergo.utilities.PropsValues;

/**
 * The Class CAdmAccountsImportBtn.
 * 
 * Adds accounts from XML file to system. New rungroups are defined, or existing
 * rungroups are updated.
 * 
 * template file for import:
 * 
 * <?xml version="1.0" encoding="ISO-8859-1"?> <accounts>
 * <account id="" password="" update="" title="" initials="" nameprefix=""
 * lastname="" email="" phonenumber="" job="" roles="," active="true" openaccess
 * ="true"></account>
 * <account id="" password="" update="" title="" initials="" nameprefix=""
 * lastname="" email="" phonenumber="" job="" roles="," active="true" openaccess
 * ="false"></account> </accounts>
 * 
 * Could be the same file as used for adding an account to a run (see
 * CCrmRunGroupAccountsImportBtn) - if update = "true" id must refer to existing
 * account; password must be valid (cannot be changed here) - if update is not
 * "true", id must not refer to existing account; if password is empty, random
 * password is generated - roles, if not empty, must belong to EMERGO roles
 *
 */
public class CAdmAccountsImportBtn extends CAccountsImportBtn {

	private static final long serialVersionUID = 7926777128505113123L;

	/** The label preface for the indices in the i3-label files. */
	protected static String labelPreface = "adm_accounts.import.";

	/**
	 * Processes the import of one account. A new account is added to the database,
	 * or an existing account is updated.
	 * 
	 * @param aAccount the account to import
	 * 
	 * @return hashtable with account import information
	 */
	@Override
	protected Hashtable<String, Object> handleAccount(IXMLTag aAccount) {
		Boolean lValid = true;
		String lUserid = CDesktopComponents.sSpring().unescapeXML(aAccount.getAttribute("id"));
		StringBuilder lAccInfo = new StringBuilder();
		if (lUserid == null || lUserid.equals("")) {
			lAccInfo.append(CDesktopComponents.vView().getLabel("error.userid.empty") + "\n");
			lValid = false;
		} else
			lAccInfo.append(lUserid);
		IEAccount lItem = null;
		Boolean lChanged = false;
		if (lValid) {
			String lUpdate = aAccount.getAttribute("update");
			Boolean lNew = !stringToBoolean(lUpdate);
			String lPassword = "TO-BE-SET";
			if (!PropsValues.ENCODE_PASSWORD) {
				// NOTE if password is encoded, password in XML file is ignored. It will be
				// filled in by a user if he registers for a run
				lPassword = CDesktopComponents.sSpring().unescapeXML(aAccount.getAttribute("password"));
			}
			Boolean lActive = stringToBoolean(aAccount.getAttribute("active"));
			// default openaccess is true
			Boolean lOpenaccess = stringToBoolean(aAccount.getAttribute("openaccess"), true);
			IEAccount lExistingAccount = accountManager.getAccount(lUserid);
			if (!lNew) {
				lAccInfo.append(makeTabLine(CDesktopComponents.vView().getLabel("to_modify")));
				if (lExistingAccount != null) {
					if (!PropsValues.ENCODE_PASSWORD) {
						// NOTE only check if password is valid if password is not encoded. If encoded
						// password, it may not be modified using the XML file
						boolean lPasswordValid = lPassword.equals(lExistingAccount.getPassword());
						if (lPasswordValid) {
							// valid update account
							lItem = lExistingAccount;
						} else {
							// don't allow batch change of password!
							lAccInfo.append(
									makeTabLine(CDesktopComponents.vView().getLabel("error.password.not_valid")));
							lValid = false;
						}
					} else
						// valid update account
						lItem = lExistingAccount;
				} else {
					lAccInfo.append(makeTabLine(CDesktopComponents.vView().getLabel("error.userid.not_found")));
					lValid = false;
				}
			} else {
				lAccInfo.append(makeTabLine(CDesktopComponents.vView().getLabel("to_add")));
				if (lExistingAccount == null) {
					lItem = accountManager.getNewAccount();
					lItem.setAccAccId(CDesktopComponents.cControl().getAccId());
				} else {
					// only allow updating existing account when update = "true"
					lAccInfo.append(makeTabLine(CDesktopComponents.vView().getLabel("error.userid.not_unique")));
					lValid = false;
				}
			}
			if (lValid) {
				if (lNew) {
					lItem.setUserid(lUserid);
					if (!PropsValues.ENCODE_PASSWORD) {
						// NOTE only set password if it will not be encoded. If encoded password, it may
						// not be set using the XML file
						if (lPassword.equals("")) {
							// add random password for new accounts if not in file
							lPassword = accountManager.randomPassword();
							lAccInfo.append(
									makeTabLine(CDesktopComponents.vView().getLabel(labelPreface + "password_set") + ":"
											+ stringTab + lPassword));
						}
					}
					lItem.setPassword(lPassword);
					if (PropsValues.ENCODE_PASSWORD) {
						// NOTE if password will be encoded, set active false. It will be set to true if
						// a user registers for a run
						lActive = new Boolean(false);
					}
				}
				String lAttr = CDesktopComponents.sSpring().unescapeXML(aAccount.getAttribute("studentid"));
				if (!lNew && !lChanged && !lItem.getStudentid().equals(lAttr))
					lChanged = true;
				lItem.setStudentid(lAttr);
				lAttr = CDesktopComponents.sSpring().unescapeXML(aAccount.getAttribute("title"));
				if (!lNew && !lChanged && !lItem.getTitle().equals(lAttr))
					lChanged = true;
				lItem.setTitle(lAttr);
				lAttr = CDesktopComponents.sSpring().unescapeXML(aAccount.getAttribute("initials"));
				if (!lNew && !lChanged && !lItem.getInitials().equals(lAttr))
					lChanged = true;
				lItem.setInitials(lAttr);
				lAttr = CDesktopComponents.sSpring().unescapeXML(aAccount.getAttribute("nameprefix"));
				if (!lNew && !lChanged && !lItem.getNameprefix().equals(lAttr))
					lChanged = true;
				lItem.setNameprefix(lAttr);
				lAttr = CDesktopComponents.sSpring().unescapeXML(aAccount.getAttribute("lastname"));
				if (!lNew && !lChanged && !lItem.getLastname().equals(lAttr))
					lChanged = true;
				lItem.setLastname(lAttr);
				lAttr = CDesktopComponents.sSpring().unescapeXML(aAccount.getAttribute("email"));
				if (!lNew && !lChanged && !lItem.getEmail().equals(lAttr))
					lChanged = true;
				lItem.setEmail(lAttr);
				lAttr = CDesktopComponents.sSpring().unescapeXML(aAccount.getAttribute("phonenumber"));
				if (!lNew && !lChanged && !lItem.getPhonenumber().equals(lAttr))
					lChanged = true;
				lItem.setPhonenumber(lAttr);
				lAttr = CDesktopComponents.sSpring().unescapeXML(aAccount.getAttribute("job"));
				if (!lNew && !lChanged && !lItem.getJob().equals(lAttr))
					lChanged = true;
				lItem.setJob(lAttr);
				lAttr = CDesktopComponents.sSpring().unescapeXML(aAccount.getAttribute("extradata"));
				if (!lNew && !lChanged && !lItem.getExtradata().equals(lAttr))
					lChanged = true;
				lItem.setExtradata(lAttr);
				if (!lNew && !lChanged && lItem.getActive() != lActive)
					lChanged = true;
				lItem.setActive(lActive);
				if (!lNew && !lChanged && lItem.getOpenaccess() != lOpenaccess)
					lChanged = true;
				lItem.setOpenaccess(lOpenaccess);
				List<IERole> lAllRoles = accountManager.getAllRoles();
				String lAccRoles = CDesktopComponents.sSpring().unescapeXML(aAccount.getAttribute("roles"));
				String[] lAccRolesArr = lAccRoles.split(",");
				for (int i = 0; i < lAccRolesArr.length; i++) {
					// strip quotes, xml file contains for instance 'student'
					lAccRolesArr[i] = lAccRolesArr[i].replaceAll("'", "");
				}
				String lAccLandingpages = CDesktopComponents.sSpring()
						.unescapeXML(aAccount.getAttribute("landingpages"));
				String[] lAccLandingpagesArr = lAccLandingpages.split(",");
				for (int i = 0; i < lAccLandingpagesArr.length; i++) {
					// strip quotes, xml file contains instance 'landingpage'
					lAccLandingpagesArr[i] = lAccLandingpagesArr[i].replaceAll("'", "");
				}
				Hashtable<String, String> lLandingpages = new Hashtable<String, String>();
				// NOTE lAccRoles contains 'student' or 'tutor' while role codes are adm, cde,
				// crm, tut or stu.
				// Following code only works 'student' and 'tutor' because they contain the role
				// code.
				// If for instance 'developer' is added in the xml file the code will not work
				// because cde is no part of 'developer'.
				// So better use role codes within xml file.
				if (lAllRoles != null) {
					Set<IERole> lRoles = new HashSet<IERole>();
					for (IERole lObject : lAllRoles) {
						String lCode = lObject.getCode();
						if (lAccRoles.indexOf(lCode) >= 0) {
							lRoles.add(lObject);
							if (!lNew && !lChanged && !accountManager.hasRole(lItem, lCode))
								lChanged = true;
						} else {
							if (!lNew && !lChanged && accountManager.hasRole(lItem, lCode))
								lChanged = true;
						}
						// NOTE following if statement is added for handling landing pages, code above
						// within for statement is unchanged
						if (lAccRoles.indexOf(lCode) >= 0) {
							for (int i = 0; i < lAccRolesArr.length; i++) {
								if (lAccRolesArr[i].indexOf(lCode) == 0) {
									String lLandingpage = "";
									if (i < lAccLandingpagesArr.length) {
										lLandingpage = lAccLandingpagesArr[i];
									}
									lLandingpages.put(lCode, lLandingpage);
									if (!lNew && !lChanged) {
										String lOldLandingpage = accountManager.getRoleLandingpage(lItem, lCode);
										if (lOldLandingpage == null && !lLandingpage.equals("")) {
											lChanged = true;
										}
										if (lOldLandingpage != null
												&& !lAccLandingpagesArr[i].equals(lOldLandingpage)) {
											lChanged = true;
										}
									}
								}
							}
						}
					}
					lItem.setERoles(lRoles);
				}
				List<String[]> lErrors = new ArrayList<String[]>(0);
				if (lNew) {
					lErrors = accountManager.newAccount(lItem);
				} else {
					if (lChanged)
						lErrors = accountManager.updateAccount(lItem);
				}
				if (lErrors == null || lErrors.size() == 0) {
					accountManager.setRoleLandingpages(lItem, lLandingpages);
				}
				if (!(lErrors == null) && (lErrors.size() > 0)) {
					lAccInfo.append(makeTabLine(CDesktopComponents.vView().getLabel("error.userid.import")));
					lAccInfo.append(makeTabLine(CDesktopComponents.cControl().getErrorString(lErrors)));
					lValid = false;
				}
				lChanged = lValid && (lChanged || lNew);
			}
		}
		Hashtable<String, Object> lResult = new Hashtable<String, Object>(0);
		lResult.put("message", lAccInfo);
		lResult.put("valid", lValid);
		lResult.put("changed", lChanged);
		if (lItem != null)
			lResult.put("account", lItem);
		return lResult;
	}

	/**
	 * Regenerates the accounts listbox after account import.
	 * 
	 */
	@Override
	protected void regeneratePage() {
//		regenerate the accounts listbox	
		CAdmAccountsLb lListbox = (CAdmAccountsLb) getFellowIfAny("accountsLb");
		lListbox.update();
	}
}
