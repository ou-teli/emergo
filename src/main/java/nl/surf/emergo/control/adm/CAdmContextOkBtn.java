/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.adm;

import java.util.ArrayList;
import java.util.List;

import nl.surf.emergo.business.IContextManager;
import nl.surf.emergo.control.CControlHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefButton;
import nl.surf.emergo.domain.IEContext;

/**
 * The Class CAdmContextOkBtn.
 */
public class CAdmContextOkBtn extends CDefButton {

	private static final long serialVersionUID = -5210060476093601394L;

	/**
	 * On click create new item or update item if no errors and close edit window.
	 */
	public void onClick() {
		CAdmContextWnd lWindow = (CAdmContextWnd)getRoot();
		IEContext lItem = (IEContext)lWindow.getItem();
		IContextManager contextManager = (IContextManager)CDesktopComponents.sSpring().getBean("contextManager");
		Boolean lNew = false;
		if (lItem == null) {
//			new item
			lNew = true;
			lItem = contextManager.getNewContext();
			lItem.setContext("");
		}
		else {
			//update
			lItem = (IEContext)lItem.clone();
		}
		lItem.setContext(CControlHelper.getTextboxValue(this, "context"));
		lItem.setActive(CControlHelper.isCheckboxChecked(this, "active"));
		List<String[]> lErrors = new ArrayList<String[]>(0);
		if (lNew)
			lErrors = contextManager.newContext(lItem);
		else
			lErrors = contextManager.updateContext(lItem);
		if ((lErrors == null) || (lErrors.size() == 0)) {
//			Get account so date format etc. is correct.
			lItem = contextManager.getContext(lItem.getConId());
			lWindow.setAttribute("item",lItem);
		}
		else {
			lWindow.setAttribute("item",null);
			CDesktopComponents.cControl().showErrors(this,lErrors);
		}
		lWindow.detach();
	}
}
