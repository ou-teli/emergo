/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.adm;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zul.Textbox;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefButton;

/**
 * The Class CAdmQueryExecuteSqlBtn. When clicked it executes a SQL statement.
 */
public class CAdmQueryExecuteSqlBtn extends CDefButton {

	private static final long serialVersionUID = 6853546661890210182L;

	public void onClick() {
		List<String> lErrors = new ArrayList<String>();
		CDesktopComponents.sSpring().getAppManager().executeSql(((Textbox)getFellow("sqlExecute")).getValue(), lErrors);
		String lResult = "";
		if (lErrors.size() > 0) {
			for (String lError : lErrors) {
				lResult += lError + "\n";
			}
		}
		else {
			lResult = CDesktopComponents.vView().getLabel("adm_query.ok");
		}
		((Textbox)getFellow("resultExecute")).setValue(lResult);
	}
	
}
