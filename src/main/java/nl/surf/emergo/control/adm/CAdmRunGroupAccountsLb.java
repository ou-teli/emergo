/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.adm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import nl.surf.emergo.business.ICaseComponentManager;
import nl.surf.emergo.business.IRunGroupAccountManager;
import nl.surf.emergo.business.IRunGroupManager;
import nl.surf.emergo.business.IRunTeamManager;
import nl.surf.emergo.business.IRunTeamRunGroupManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefListbox;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.domain.IERunTeam;
import nl.surf.emergo.domain.IERunTeamRunGroup;

/**
 * The Class CAdmRunGroupAccountsLb.
 */
public class CAdmRunGroupAccountsLb extends CDefListbox {

	private static final long serialVersionUID = 4454238861244069031L;

	/** The run group ids. */
	protected List<Integer> runGroupIds = null;
	
	/** The run team run groups. */
	protected List<IERunTeamRunGroup> runTeamRunGroups = null;

	/** The run teams. */
	protected List<IERunTeam> runTeams = null;

	protected class HashtableSortByAccountLastname implements Comparator<Hashtable<String,Object>>{
		public int compare(Hashtable<String,Object> o1, Hashtable<String,Object> o2) {
			return ((IEAccount)o1.get("item")).getLastname().compareTo(((IEAccount)o2.get("item")).getLastname());
		}
	}
	
	/**
	 * On create render listitems.
	 */
	public void onCreate() {
		getItems().clear();
		
//		clear casecomponent and cacId because it can interfere with admin in role of case developer
		CControl cControl = CDesktopComponents.cControl();
		cControl.setAccSessAttr("casecomponent",null);
		cControl.setAccSessAttr("cacId",null);
		int lRunId = Integer.parseInt((String)cControl.getAccSessAttr("runId"));
		runTeams = ((IRunTeamManager)CDesktopComponents.sSpring().getBean("runTeamManager")).getAllRunTeamsByRunId(lRunId);
		List<IERunGroup> lRunGroups = ((IRunGroupManager)CDesktopComponents.sSpring().getBean("runGroupManager")).getAllRunGroupsByRunId(lRunId);
		List<Integer> lRugIds = new ArrayList<Integer>();
		for (IERunGroup lRunGroup : lRunGroups) {
			if (!lRugIds.contains(lRunGroup.getRugId()))
				lRugIds.add(lRunGroup.getRugId());
		}
		
		List<IERunGroupAccount> lItems = null;
		//NOTE account may be selected in adm_accounts.zul which is the case if an adm wants to see only run group accounts for this accounts.
		//or multiple accounts may be selected when filtering on accounts
		List<IEAccount> lSelectedAccounts = (List<IEAccount>)CDesktopComponents.cControl().getAccSessAttr(AppConstants.selected_accounts);
		if (lSelectedAccounts == null) {
			//show all run group accounts
			lItems = ((IRunGroupAccountManager)CDesktopComponents.sSpring().getBean("runGroupAccountManager")).getAllRunGroupAccountsByRugIds(lRugIds);
		}
		else {
			//show only run group accounts for selected accounts
			List<Integer> lAccIds = new ArrayList<Integer>();
			for (IEAccount lSelectedAccount : lSelectedAccounts) {
				lAccIds.add(lSelectedAccount.getAccId());
			}
			lItems = ((IRunGroupAccountManager)CDesktopComponents.sSpring().getBean("runGroupAccountManager")).getAllRunGroupAccountsByAccIds(lAccIds);
		}
		
		List<IEAccount> lAccounts = new ArrayList<IEAccount>();
		runGroupIds = new ArrayList<Integer>();
		List<Hashtable<String,Object>> lStuItems = new ArrayList<Hashtable<String,Object>>(0);
		for (IERunGroupAccount lRunGroupAccount : lItems) {
			IEAccount lAccount = lRunGroupAccount.getEAccount();
			if (!lAccounts.contains(lAccount)) {
				List<IERunGroupAccount> lRunGroupAccounts = getRunGroupAccounts(lAccount.getAccId(),lRunId,lItems);
				if (lRunGroupAccounts.size() > 0) {
					lAccounts.add(lAccount);
					if (!runGroupIds.contains(lRunGroupAccount.getERunGroup().getRugId()))
						runGroupIds.add(lRunGroupAccount.getERunGroup().getRugId());
					Hashtable<String,Object> lStuItem = new Hashtable<String,Object>(0);
					lStuItem.put("item",lAccount);
					lStuItem.put("rungroupaccounts",lRunGroupAccounts);
					lStuItem.put("runteams",getRunTeams(lRunGroupAccounts));
					lStuItems.add(lStuItem);
				}
			}
			
		}
		Collections.sort(lStuItems, new HashtableSortByAccountLastname());

		CAdmRunGroupAccountsHelper cHelper = new CAdmRunGroupAccountsHelper();
		for (Hashtable<String,Object> lStuItem : lStuItems) {
			cHelper.renderItem(this, null, lStuItem);
		}

		restoreSettings();
	}

	/**
	 * Gets the relevant run group accounts out of aRunGroupAccounts.
	 * 
	 * @param aAccId the a acc id
	 * @param aRunId the a run id
	 * @param aRunGroupAccounts the a run group accounts
	 * 
	 * @return the run group accounts
	 */
	private List<IERunGroupAccount> getRunGroupAccounts(int aAccId,int aRunId,List<IERunGroupAccount> aRunGroupAccounts) {
		List<IERunGroupAccount> lRunGroupAccounts = new ArrayList<IERunGroupAccount>(0);
		for (IERunGroupAccount lRunGroupAccount : aRunGroupAccounts) {
			IEAccount lAccount = lRunGroupAccount.getEAccount();
			IERunGroup lRunGroup = lRunGroupAccount.getERunGroup();
			if ((lRunGroup.getActive()) && (lAccount.getAccId() == aAccId) && (lRunGroup.getERun().getRunId() == aRunId)) {
				lRunGroupAccounts.add(lRunGroupAccount);
			}
		}
		return lRunGroupAccounts;
	}

	/**
	 * Gets the run teams for aRunGroupAccounts.
	 * 
	 * @param aRunGroupAccounts the a run group accounts
	 * 
	 * @return the run teams
	 */
	public List<IERunTeam> getRunTeams(List<IERunGroupAccount> aRunGroupAccounts) {
		List<IERunTeam> lRunTeams = new ArrayList<IERunTeam>(0);
		Hashtable<String,IERunTeam> lRunTeamsHash = new Hashtable<String,IERunTeam>(0);
		for (IERunGroupAccount lRunGroupAccount : aRunGroupAccounts) {
			if (runTeamRunGroups == null)
				runTeamRunGroups = ((IRunTeamRunGroupManager)CDesktopComponents.sSpring().getBean("runTeamRunGroupManager")).getAllRunTeamRunGroupsByRugIds(runGroupIds);
			for (IERunTeamRunGroup lRunTeamRunGroup : runTeamRunGroups) {
				if (lRunTeamRunGroup.getRugRugId() == lRunGroupAccount.getERunGroup().getRugId()) {
					IERunTeam lRunTeam = null;
					for (IERunTeam lRut : runTeams) {
						if (lRut.getRutId() == lRunTeamRunGroup.getRutRutId())
							lRunTeam = lRut;
					}
					if (lRunTeam != null) {
						lRunTeamsHash.put(""+lRunTeam.getRutId(), lRunTeam);
					}
				}
			}
		}
		for (Enumeration<String> lKeys = lRunTeamsHash.keys(); lKeys.hasMoreElements();) {
			lRunTeams.add(lRunTeamsHash.get(lKeys.nextElement()));
		}
		return lRunTeams;
	}

	/**
	 * Deletes run group status.
	 * 
	 * @param aRunGroup the a run group
	 */
	public void deleteRoleStatus(IERunGroup aRunGroup) {
		IECase lCase = aRunGroup.getERun().getECase();
		List<IECaseComponent> lCaseComponents = ((ICaseComponentManager) (CDesktopComponents.sSpring().getBean("caseComponentManager"))).getAllCaseComponentsByCasId(lCase.getCasId());
		for (IECaseComponent lCaseComponent : lCaseComponents) {
			// clear status
			CDesktopComponents.sSpring().setRunGroupCaseComponentStatus(aRunGroup.getRugId(), lCaseComponent.getCacId(), "");
		}
	}
}
