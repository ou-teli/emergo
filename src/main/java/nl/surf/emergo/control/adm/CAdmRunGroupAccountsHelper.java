/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.adm;

import java.util.Hashtable;
import java.util.List;

import org.zkoss.zul.Html;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControlHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefListcell;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.domain.IERunTeam;

/**
 * The Class CAdmRunGroupAccountsHelper.
 */
public class CAdmRunGroupAccountsHelper extends CControlHelper {

	/** The team case components. */
	protected List<IECaseComponent> teamCaseComponents = null;

	/**
	 * Renders one account and buttons and combo boxes to act on the account.
	 * 
	 * @param aListbox the a listbox
	 * @param aInsertBefore the a insert before
	 * @param aItem the a item
	 */
	public void renderItem(Listbox aListbox,Listitem aInsertBefore,Hashtable<String,Object> aItem) {
		Listitem lListitem = super.newListitem();
		List<IERunGroupAccount> lRunGroupAccounts = (List)aItem.get("rungroupaccounts");
		List<IERunTeam> lRunTeams = (List)aItem.get("runteams");
		IEAccount lItem = (IEAccount)aItem.get("item");
		lListitem.setValue(aItem);
		String lRgaIdsStr = "";
		for (IERunGroupAccount lRunGroupAccount : lRunGroupAccounts) {
			if (lRgaIdsStr.length() > 0) {
				lRgaIdsStr += ",";
			}
			lRgaIdsStr += "" + lRunGroupAccount.getRgaId();
		}
		if (lRunGroupAccounts.size() > 1) {
			lRgaIdsStr = "rgaIds=" + lRgaIdsStr;
		}
		else {
			lRgaIdsStr = "rgaId=" + lRgaIdsStr;
		}
		showTooltiptextIfAdmin(lListitem, lRgaIdsStr);
		super.appendListcell(lListitem,lItem.getStudentid());
		super.appendListcell(lListitem,lItem.getTitle());
		super.appendListcell(lListitem,lItem.getInitials());
		super.appendListcell(lListitem,lItem.getNameprefix());
		super.appendListcell(lListitem,lItem.getLastname());

		Listcell lListcell = new CDefListcell();
		boolean lShowCaseRoleCombo = (lRunGroupAccounts.size() > 1);
		if (lShowCaseRoleCombo) {
			CAdmRunGroupAccountCaseRoleCombo lCaseRoleCombo = new CAdmRunGroupAccountCaseRoleCombo("rungroupaccountsLbCaseRoleCombo"+lItem.getAccId(),lItem,lRunGroupAccounts);
			lListcell.appendChild(lCaseRoleCombo);
			lListcell.appendChild(new Html("<br/>"));
		}
		lListitem.appendChild(lListcell);

		lListcell = new CDefListcell();
		boolean lShowRunTeamCombo = ((lShowCaseRoleCombo && (lRunTeams.size() == 1)) || (lRunTeams.size() > 1));
		if (lShowRunTeamCombo) {
			CAdmRunGroupAccountRunTeamCombo lRunTeamCombo = new CAdmRunGroupAccountRunTeamCombo("rungroupaccountsLbRunTeamCombo"+lItem.getAccId(),lItem,lRunTeams);
			lRunTeamCombo.setDisabled(lShowCaseRoleCombo);
			if (lRunGroupAccounts.size() == 1)
				lRunTeamCombo.setRunGroupAccount((IERunGroupAccount)lRunGroupAccounts.get(0));
			lListcell.appendChild(lRunTeamCombo);
			lListcell.appendChild(new Html("<br/>"));
		}
		lListitem.appendChild(lListcell);

		lListcell = new CDefListcell();
		CAdmRunGroupAccountRedirectBtn lRedirect = new CAdmRunGroupAccountRedirectBtn("rungroupaccountsLbRedirectButton"+lItem.getAccId(),lItem);
		lRedirect.setLabel(CDesktopComponents.vView().getLabel("open_case"));
		lRedirect.setDisabled(lShowCaseRoleCombo || lShowRunTeamCombo);
		if (lRunGroupAccounts.size() == 1) {
			lRedirect.setTeamCaseComponents(getTeamCaseComponents(((IERunGroupAccount)lRunGroupAccounts.get(0)).getERunGroup().getERun()));
			lRedirect.setRunGroupAccount((IERunGroupAccount)lRunGroupAccounts.get(0));
		}
		if (lRunTeams.size() == 1) {
			lRedirect.setTeamCaseComponents(getTeamCaseComponents(((IERunTeam)lRunTeams.get(0)).getERun()));
			lRedirect.setRunTeam((IERunTeam)lRunTeams.get(0));
		}
		lListcell.appendChild(lRedirect);
		lListitem.appendChild(lListcell);

		lListcell = new CDefListcell();
		CAdmRunGroupAccountTagStatusBtn lStatus = new CAdmRunGroupAccountTagStatusBtn("rungroupaccountsLbTagStatusButton"+lItem.getAccId());
		lStatus.setLabel(CDesktopComponents.vView().getLabel("adm_rungroupaccounts.tagstatus"));
		lStatus.setDisabled(lShowCaseRoleCombo);
		if (lRunGroupAccounts.size() == 1)
			lStatus.setRunGroupAccount((IERunGroupAccount)lRunGroupAccounts.get(0));
		lListcell.appendChild(lStatus);
		lListitem.appendChild(lListcell);

		lListcell = new CDefListcell();
		CAdmRunGroupAccountComponentStatusBtn lStatus2 = new CAdmRunGroupAccountComponentStatusBtn("rungroupaccountsLbComponentStatusButton"+lItem.getAccId());
		lStatus2.setLabel(CDesktopComponents.vView().getLabel("adm_rungroupaccounts.componentstatus"));
		lStatus2.setDisabled(lShowCaseRoleCombo);
		if (lRunGroupAccounts.size() == 1)
			lStatus2.setRunGroupAccount((IERunGroupAccount)lRunGroupAccounts.get(0));
		lListcell.appendChild(lStatus2);
		lListitem.appendChild(lListcell);

		lListcell = new CDefListcell();
		CAdmRunGroupAccountDeleteStatusBtn lStatus3 = new CAdmRunGroupAccountDeleteStatusBtn("rungroupaccountsLbDeleteStatusButton"+lItem.getAccId());
		lStatus3.setLabel(CDesktopComponents.vView().getLabel("delete_status"));
		lStatus3.setDisabled(lShowCaseRoleCombo);
		if (lRunGroupAccounts.size() == 1)
			lStatus3.setRunGroupAccount((IERunGroupAccount)lRunGroupAccounts.get(0));
		lListcell.appendChild(lStatus3);
		lListitem.appendChild(lListcell);

		super.insertListitem(aListbox,lListitem,aInsertBefore);
	}

	/**
	 * Gets the team case components. Case components defined to be used within a run team.
	 * 
	 * @param aRun the a run
	 * 
	 * @return the team case components
	 */
	public List<IECaseComponent> getTeamCaseComponents(IERun aRun) {
		if (teamCaseComponents == null)
			teamCaseComponents = CDesktopComponents.sSpring().getCaseComponentsByStatusType(aRun.getECase(), AppConstants.statusTypeRunTeam);
		return teamCaseComponents;
	}

}