/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.adm;

import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.control.CControlHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefListcell;
import nl.surf.emergo.domain.IECase;

/**
 * The Class CAdmCasesHelper.
 */
public class CAdmCasesHelper extends CControlHelper {

	/** The account manager. */
	protected IAccountManager accountManager = (IAccountManager)CDesktopComponents.sSpring().getBean("accountManager");

	/**
	 * Renders one case.
	 * 
	 * @param aListbox the a listbox
	 * @param aInsertBefore the a insert before
	 * @param aItem the a item
	 */
	public void renderItem(Listbox aListbox,Listitem aInsertBefore,IECase aItem) {
		Listitem lListitem = super.newListitem();
		lListitem.setValue(aItem);
		super.appendListcell(lListitem,""+aItem.getCasId());
		super.appendListcell(lListitem,aItem.getCode());
		super.appendListcell(lListitem,aItem.getName());
		super.appendListcell(lListitem,accountManager.getAccountName(aItem.getEAccount()));
		super.appendListcell(lListitem,""+aItem.getVersion());
		super.appendListcell(lListitem,""+aItem.getActive());
		super.appendListcell(lListitem,getDateStrAsYMD(aItem.getCreationdate()));
		
		Listcell lListcell = new CDefListcell();
		CAdmUploadCaseStreamingFilesButton lButton = new CAdmUploadCaseStreamingFilesButton(CDesktopComponents.sSpring());
		lButton.setLabel(CDesktopComponents.vView().getLabel("adm_cases.uploadfiles"));
		lListcell.appendChild(lButton);
		lListitem.appendChild(lListcell);

		super.insertListitem(aListbox,lListitem,aInsertBefore);
	}

}