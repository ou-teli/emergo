/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.adm;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefListbox;
import nl.surf.emergo.domain.IEAccount;

/**
 * The Class CAdmContextAccountsLb.
 */
public class CAdmContextAccountsLb extends CDefListbox {

	private static final long serialVersionUID = -366880273843649335L;

	/** The account manager. */
	protected IAccountManager accountManager = (IAccountManager)CDesktopComponents.sSpring().getBean("accountManager");

	/**
	 * On create render listitems.
	 */
	public void onCreate() {
		update();
		restoreSettings();
	}

	/**
	 * (Re)render listitems.
	 */
	public void update() {
		getItems().clear();
		List<IEAccount> lItems = null;
		CControl cControl = CDesktopComponents.cControl();
		// get student and tutor accounts
		lItems = accountManager.getAllAccountsFilter(true,AppConstants.c_role_tut + "," + AppConstants.c_role_stu, (Map<String, String>)cControl.getAccSessAttr(AppConstants.account_filter_map));
		List<Hashtable<String,IEAccount>> lStuItems = new ArrayList<Hashtable<String,IEAccount>>(0);
		for (IEAccount lItem : lItems) {
			Hashtable<String,IEAccount> lStuItem = new Hashtable<String,IEAccount>(0);
			lStuItem.put("item",lItem);
			lStuItems.add(lStuItem);
		}
		CAdmContextAccountsHelper cHelper = new CAdmContextAccountsHelper();
		for (Hashtable<String,IEAccount> lStuItem : lStuItems) {
			cHelper.renderItem(this, null, lStuItem);
		}
	}

}
