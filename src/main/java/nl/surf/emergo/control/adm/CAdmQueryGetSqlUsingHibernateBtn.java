/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.adm;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefButton;
import nl.surf.emergo.domain.IEDomainEntity;

/**
 * The Class CAdmQueryGetSqlUsingHibernateBtn. When clicked it gets SQL using Hibernate mapping.
 */
public class CAdmQueryGetSqlUsingHibernateBtn extends CDefButton {

	private static final long serialVersionUID = 8812064378775184997L;

	public void onClick() {
		Textbox lSqlGetT = (Textbox)getFellow("sqlGet");
		Listbox lClassnameL = (Listbox)getFellow("classname");
		Textbox lResultGetT = (Textbox)getFellow("resultGet");
		lResultGetT.setValue("");
		if (lClassnameL.getSelectedItem() != null) {
			List<String> lErrors = new ArrayList<String>();
			List<Object> lItems = CDesktopComponents.sSpring().getAppManager().getSqlResult(lSqlGetT.getValue(),
					(Class)lClassnameL.getSelectedItem().getAttribute("class"), lErrors);
			String lClassname = (String)lClassnameL.getSelectedItem().getLabel();
			StringBuffer lResult = new StringBuffer();
			if (lErrors.size() > 0) {
				for (String lError : lErrors) {
					lResult.append(lError);
					lResult.append("\n");
				}
			}
			else {
				if (lItems == null || lItems.size() == 0) {
					lResult.append(CDesktopComponents.vView().getLabel("adm_query.nothing_found"));
				}
				else {
					String lPrefix = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
					int lPrefixLength = lPrefix.length();
					lResult.append(lPrefix);
					lResult.append("\n<sqldata>\n");
					for (Object lItem : lItems) {
						Hashtable<String,String> lProperties = ((IEDomainEntity)lItem).getProperties();
						lResult.append("<");
						lResult.append(lClassname);
						lResult.append(">");
						for (Enumeration<String> lKeys = lProperties.keys(); lKeys.hasMoreElements();) {
							String lKey = (String) lKeys.nextElement();
							String lValue = (String) lProperties.get(lKey);
							if (lKey.equals("xmldata")) {
								lValue = lValue.substring(lPrefixLength);
							}
							else {
								lValue = CDesktopComponents.sSpring().getXmlManager().escapeXML(lValue);
							}
							lResult.append("<");
							lResult.append(lKey);
							lResult.append(">");
							lResult.append(lValue);
							lResult.append("</");
							lResult.append(lKey);
							lResult.append(">");
						}
						lResult.append("</");
						lResult.append(lClassname);
						lResult.append(">");
						lResult.append("\n");
					}
					lResult.append("</sqldata>");
				}
			}
			lResultGetT.setValue(lResult.toString());
		}
	}
	
}
