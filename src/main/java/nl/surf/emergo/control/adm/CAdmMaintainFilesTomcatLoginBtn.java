/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.adm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefButton;
import nl.surf.emergo.view.VView;

/**
 * The Class CAdmMaintainFilesTomcatLoginBtn.
 */
public class CAdmMaintainFilesTomcatLoginBtn extends CDefButton {

	private static final long serialVersionUID = 9026218469178375011L;

	protected CAdmMaintainFilesWnd root = (CAdmMaintainFilesWnd)CDesktopComponents.vView().getComponent("maintainFilesWnd");

	public void onClick() {
		String lFileName = "tomcat-users.xml";
		Document lDocument = null;
		NodeList lNodes = null;
		try {
			File lFile = new File(root.getAbsoluteAppPath() + "..\\..\\conf\\" + lFileName);
			if (lFile.exists()) {
				FileInputStream lFis = new FileInputStream(lFile);
				InputSource lInputSource = new InputSource(lFis);
				DocumentBuilder builder = VView.getXmlTree().getDocumentBuilder();
				lDocument = builder.parse(lInputSource);
				if (lDocument != null) {
					lNodes = lDocument.getElementsByTagName("user");
				}	   			
				lFis.close();
			}
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		} catch (SAXException e) {
		}

		VView vView = CDesktopComponents.vView();
		boolean lIsLoginOk = false;
		if (lDocument == null) {
			vView.showMessagebox(getRoot(), vView.getLabel("adm_maintain_files.tomcat.error.read_file").replace("%1", lFileName), vView.getLabel("messagebox.title.errors"), Messagebox.OK, Messagebox.ERROR);
		}
		else {
			lNodes = lDocument.getElementsByTagName("user");
			if (lNodes == null || lNodes.getLength() == 0) {
				vView.showMessagebox(getRoot(), vView.getLabel("adm_maintain_files.tomcat.error.no_user_tags").replace("%1", lFileName), vView.getLabel("messagebox.title.errors"), Messagebox.OK, Messagebox.ERROR);
			}
			else {
				String lUsername = ((Textbox)root.getComponent("username")).getValue();
				String lPassword = ((Textbox)root.getComponent("password")).getValue();
				for (int i = 0; i < lNodes.getLength(); i++) {
					Node lNode = lNodes.item(i);
					NamedNodeMap lNodeAttributes = lNode.getAttributes();
					boolean lIsUsernameOk = false;
					boolean lIsPasswordOk = false;
					if (lNodeAttributes != null) {
						for (int j = 0; j < lNodeAttributes.getLength(); j++) {
							Node lNodeAttribute = lNodeAttributes.item(j);
							String lAttributeName = lNodeAttribute.getNodeName();
							if (lAttributeName.equals("username") && lNodeAttribute.getNodeValue().equals(lUsername)) {
								lIsUsernameOk = true;
							}
							else if (lAttributeName.equals("password") && lNodeAttribute.getNodeValue().equals(lPassword)) {
								lIsPasswordOk = true;
							}
						}
					}
					if (lIsUsernameOk && lIsPasswordOk) {
						root.setIsLoggedInViaTomcat(true);
						lIsLoginOk = true;
						break;
					}
				}
				if (!lIsLoginOk) {
					vView.showMessagebox(getRoot(), vView.getLabel("adm_maintain_files.tomcat.error.user_not_defined").replace("%1", lFileName), vView.getLabel("messagebox.title.errors"), Messagebox.OK, Messagebox.ERROR);
				}
			}
		}
	
		if (lIsLoginOk) {
			Events.postEvent("onInit", root.getComponent("maintenanceBox"), null);
			root.getComponent("tomcatLoginWnd").detach();
		}
	}

}
