/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.adm;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.IAccountContextManager;
import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.control.CControlHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefListcell;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IEAccountContext;
import nl.surf.emergo.domain.IERole;

/**
 * The Class CAdmContextAccountsHelper.
 */
public class CAdmContextAccountsHelper extends CControlHelper {

	/** The context accounts. */
	protected List<Integer> contextAccounts = null; 
	
	/** The account manager. */
	protected IAccountManager accountManager = (IAccountManager)CDesktopComponents.sSpring().getBean("accountManager");

	/** The account context manager. */
	protected IAccountContextManager accountContextManager = (IAccountContextManager)CDesktopComponents.sSpring().getBean("accountContextManager");

	/** The errors. */
	protected List<String[]> errors = new ArrayList<String[]>(0);
	
	/**
	 * Renders one account and for each case role a check box to add to run.
	 * 
	 * @param aListbox the a listbox
	 * @param aInsertBefore the a insert before
	 * @param aHItem the a h item
	 */
	public void renderItem(Listbox aListbox,Listitem aInsertBefore,Hashtable<String,IEAccount> aHItem) {
		Listitem lListitem = super.newListitem();
		IEAccount lItem = aHItem.get("item");
		lListitem.setValue(lItem);
		Listcell lListcell = new CDefListcell();
		CAdmContextAccountCb lCheckbox = new CAdmContextAccountCb();
		lCheckbox.setAttribute("accId",""+lItem.getAccId());
		int lConId = Integer.parseInt((String)CDesktopComponents.cControl().getAccSessAttr("contextId"));
		List<Integer> lConIds = new ArrayList<Integer>();
		lConIds.add(lConId);
		if (contextAccounts == null) {
			contextAccounts = accountContextManager.getAccountContextAccIdsByContextIdsActive(lConIds, true);
			contextAccounts.addAll(accountContextManager.getAccountContextAccIdsByContextIdsActive(lConIds, false));
		}
		lCheckbox.setChecked(contextAccounts.contains(lItem.getAccId()));
		lListcell.appendChild(lCheckbox);
		lListitem.appendChild(lListcell);
		super.insertListitem(aListbox,lListitem,aInsertBefore);
		super.appendListcell(lListitem,lItem.getUserid());
		super.appendListcell(lListitem,lItem.getStudentid());
		super.appendListcell(lListitem,lItem.getTitle());
		super.appendListcell(lListitem,lItem.getInitials());
		super.appendListcell(lListitem,lItem.getNameprefix());
		super.appendListcell(lListitem,lItem.getLastname());
		super.appendListcell(lListitem,lItem.getEmail());
		super.appendListcell(lListitem,lItem.getExtradata());
		super.appendListcell(lListitem,getRoles(lItem));
	}

	/**
	 * Gets the roles.
	 * 
	 * @param aItem the a item
	 * 
	 * @return the rolecodes, comma separated
	 */
	public String getRoles(IEAccount aItem) {
		String lRoles = "";
		for (IERole lItem : aItem.getERoles()) {
			if ((lItem.getCode().equals("tut")) || (lItem.getCode().equals("stu"))) {
				if (!lRoles.equals("")) lRoles = lRoles + ", ";
				lRoles = lRoles + CDesktopComponents.vView().getLabel(lItem.getCode());
			}
		}
		return lRoles;
	}

	/**
	 * Adds account context.
	 * 
	 * @param aAccId the account id
	 * @param aConId the context id
	 * @param aActive whether or not the account context is active
	 * 
	 * @return the account context
	 */
	public IEAccountContext addAccountContext(int aAccId, int aConId, boolean aActive) {
		IEAccountContext lAccountContext = accountContextManager.getNewAccountContext();
		lAccountContext.setAccAccId(aAccId);
		lAccountContext.setConConId(aConId);
		lAccountContext.setActive(aActive);
		errors = accountContextManager.newAccountContext(lAccountContext);
		if (!(errors == null) && (errors.size() > 0))
			return null;
		return lAccountContext;
	}

	/**
	 * Gets the errors.
	 * 
	 * @return the errors
	 */
	public List<String[]> getErrors() {
		return errors;
	}
}