/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.adm;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.control.CControlHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefButton;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IERole;
import nl.surf.emergo.utilities.PropsValues;

/**
 * The Class CAdmAccountOkBtn.
 */
public class CAdmAccountOkBtn extends CDefButton {

	private static final long serialVersionUID = -5210060476093601394L;

	/**
	 * On click create new item or update item if no errors and close edit window.
	 */
	public void onClick() {
		CAdmAccountWnd lWindow = (CAdmAccountWnd) getRoot();
		IEAccount lItem = (IEAccount) lWindow.getItem();
		IAccountManager accountManager = (IAccountManager) CDesktopComponents.sSpring().getBean("accountManager");
		Boolean lNew = false;
		if (lItem == null) {
//			new item
			lNew = true;
			lItem = accountManager.getNewAccount();
			lItem.setAccAccId(CDesktopComponents.cControl().getAccId());
			lItem.setEmail("");
			lItem.setPhonenumber("");
			lItem.setJob("");
		} else {
			// update
			lItem = (IEAccount) lItem.clone();
		}
		lItem.setUserid(CControlHelper.getTextboxValue(this, "userid"));
		String lPassword = CControlHelper.getTextboxValue(this, "password");
		if (lNew && !lPassword.equals("") && PropsValues.ENCODE_PASSWORD) {
			// NOTE only encode if new password. Encoded password may not be changed by
			// admin anymore.
			lPassword = accountManager.encodePassword(lPassword);
		}
		lItem.setPassword(lPassword);
		lItem.setStudentid(CControlHelper.getTextboxValue(this, "studentid"));
		lItem.setTitle(CControlHelper.getTextboxValue(this, "title"));
		lItem.setInitials(CControlHelper.getTextboxValue(this, "initials"));
		lItem.setNameprefix(CControlHelper.getTextboxValue(this, "nameprefix"));
		lItem.setLastname(CControlHelper.getTextboxValue(this, "lastname"));
		lItem.setEmail(CControlHelper.getTextboxValue(this, "email"));
		lItem.setExtradata(CControlHelper.getTextboxValue(this, "extradata"));
		lItem.setActive(CControlHelper.isCheckboxChecked(this, "active"));
		lItem.setOpenaccess(CControlHelper.isCheckboxChecked(this, "openaccess"));
		// set roles
		List<IERole> lAllRoles = accountManager.getAllRoles();
		Hashtable<String, String> lLandingpages = new Hashtable<String, String>();
		if (lAllRoles != null) {
			Set<IERole> lRoles = new HashSet<IERole>();
			for (IERole lObject : lAllRoles) {
				String lCode = lObject.getCode();
				if (CControlHelper.isCheckboxChecked(this, lCode)) {
					lRoles.add(lObject);
					String lLandingpage = CControlHelper.getTextboxValue(this, lCode + "_landingpage");
					if (lLandingpage == null) {
						lLandingpage = "";
					}
					lLandingpages.put(lCode, lLandingpage);
				}
			}
			lItem.setERoles(lRoles);
		}

		List<String[]> lErrors = new ArrayList<String[]>(0);
		if (lNew)
			lErrors = accountManager.newAccount(lItem);
		else
			lErrors = accountManager.updateAccount(lItem);
		if ((lErrors == null) || (lErrors.size() == 0)) {
//			Get account so date format, name etc. is correct.
			lItem = accountManager.getAccount(lItem.getAccId());
			accountManager.setRoleLandingpages(lItem, lLandingpages);
			lWindow.setAttribute("item", lItem);
		} else {
			lWindow.setAttribute("item", null);
			CDesktopComponents.cControl().showErrors(this, lErrors);
		}
		lWindow.detach();
	}
}
