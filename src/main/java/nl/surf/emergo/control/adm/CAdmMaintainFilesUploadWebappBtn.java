/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.adm;

import java.io.File;

import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zul.Messagebox;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefButton;
import nl.surf.emergo.view.VView;

/**
 * The Class CAdmMaintainFilesUploadWebappBtn.
 */
public class CAdmMaintainFilesUploadWebappBtn extends CDefButton {

	private static final long serialVersionUID = -2050992269844601993L;

	protected CAdmMaintainFilesWnd root = (CAdmMaintainFilesWnd)CDesktopComponents.vView().getComponent("maintainFilesWnd");

	public void onUpload(UploadEvent event) {
		if (event.getMedias() != null && event.getMedias().length == 1) {
			root.fileHelper.setNotifyComponent(this);
			
			if (root.fileHelper.uploadMedias(event.getMedias(), root.getAbsoluteAppTempPath(), "", false, false)) {
				Events.postEvent("onSucces", this, event.getMedias());
			}
		}
	}

	public void onSucces(Event event) {
		root.fileHelper.setNotifyComponent(null);
		
		boolean success = false;									
		String uploadFileName = root.getAbsoluteAppTempPath() + ((Media[])event.getData())[0].getName();
		success = root.zipHelper.unpackageZipToFiles(root.getAbsoluteAppPath(), new File(uploadFileName));
		
		VView vView = CDesktopComponents.vView();
		if (success) {
			Events.postEvent("onInitAll", root.getComponent("webappAndFileMaintenance"), null);
			vView.showMessagebox(getRoot(), vView.getLabel("adm_maintain_files.upload_webapp.info"), vView.getLabel("messagebox.title.information"), Messagebox.OK, Messagebox.INFORMATION);
		}
		else {
			vView.showMessagebox(getRoot(), vView.getLabel("adm_maintain_files.upload_webapp.error"), vView.getLabel("messagebox.title.errors"), Messagebox.OK, Messagebox.ERROR);
		}
	}

}
