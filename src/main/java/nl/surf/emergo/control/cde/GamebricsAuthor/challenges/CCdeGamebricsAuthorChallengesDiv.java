/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde.GamebricsAuthor.challenges;

import java.util.Arrays;
import java.util.List;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.cde.GamebricsAuthor.CCdeGamebricsAuthorDiv;

public class CCdeGamebricsAuthorChallengesDiv extends CCdeGamebricsAuthorDiv {

	public static final String gaChallengesComponentCode = "GB_challenges";
	public static final String gaChallengesDefaultComponentName = "GB_challenges";
	public static final String gaChallengesMainContentJS = "Challenges";
	public static final String gaChallengesRootElementsMessageLabel = "challenges";

	public final String gaChallengesElementTypes = vView.getLabel("cde_gamebricsauthor.challenges.type.items");
	public final String gaChallengesElementTypesLabels = vView.getLabel("cde_gamebricsauthor.challenges.type.items.labels");
	
	private static final long serialVersionUID = -2989150181145019005L;

	@Override
	protected String getRootElementTypes() {
		return gaChallengesElementTypes;
	}

	@Override
	public void update() {
		updateContent(getComponentCode(gaChallengesComponentCode), getRootElementTypes(), showDuplicatesInList());
	}
	
	@Override
	protected boolean showDuplicatesInList() {
		return false;
	}
	
	@Override
	protected String getComponentCode(String pElementType) {
		return gaChallengesComponentCode;
	}

	@Override
	protected String getDefaultComponentName(String pElementType) {
		return gaChallengesDefaultComponentName;
	}

	@Override
	protected String getGAJSContentString(String pContentName) {
		if (isRootElementType(pContentName)) {
			return gaChallengesMainContentJS;
		} else
			return super.getGAJSContentString(pContentName);
	}
		
	@Override
	protected String getUpdateElementTypes(String pElementType) {
		if (isRootElementType(pElementType)) {
			return getRootElementTypes();
		}
		return pElementType;
	}
		
	@Override
	protected String getUpdateElementForbiddenLabel() {
		return "cde_gamebricsauthor.updatechallenge.message.component.forbidden";
	}
	
	@Override
	protected String getDeleteElementForbiddenLabel() {
		return "cde_gamebricsauthor.deletechallenge.message.component.forbidden";
	}
	
	@Override
	protected String getElementTypeMessageLabel(String pElementType) {
		if (isRootElementType(pElementType)) {
			return gaChallengesRootElementsMessageLabel;
		} else {
			return super.getElementTypeMessageLabel(pElementType);
		}
	}
	
	@Override
	protected String getElementTypeFromString(String pElementTypeString) {
		List<String> lTypesList = Arrays.asList(gaChallengesElementTypesLabels.split(","));
		int lInd = lTypesList.indexOf(pElementTypeString);
		if (lInd >= 0)
			return Arrays.asList(getRootElementTypes().split(",")).get(lInd);
		return pElementTypeString;
	}
	
	@Override
	protected StringBuilder getElementContent(IXMLTag pElementTag) {
		StringBuilder lElementContent = new StringBuilder();
		addElementPart(lElementContent, "elementtype", sSpring.unescapeXML(pElementTag.getName()), true);
		addElementPart(lElementContent, "name", getValidJSONString(pElementTag.getChildValue("name")), true);
		addElementPart(lElementContent, "typeforstudent", getValidJSONString(pElementTag.getChildValue("typeforstudent")), true);
		addElementPart(lElementContent, "description", getValidJSONString(pElementTag.getChildValue("description")), true);
		addElementPart(lElementContent, "explanation", getValidJSONString(pElementTag.getChildValue("explanation")), true);
		addElementPart(lElementContent, "normtimeminutes", sSpring.unescapeXML(pElementTag.getChildValue("normtimeminutes")), false);
		lElementContent.insert(0, "[{");
		lElementContent.append("}]");
		return lElementContent;
	}
		
}
