/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde;

import java.util.ArrayList;
import java.util.List;

import nl.surf.emergo.business.ICaseComponentManager;
import nl.surf.emergo.business.ICaseComponentRoleManager;
import nl.surf.emergo.business.ICaseManager;
import nl.surf.emergo.business.ICaseRoleManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControlHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefButton;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IECaseComponentRole;
import nl.surf.emergo.domain.IECaseRole;

/**
 * The Class CCdeCaseRoleOkBtn.
 */
public class CCdeCaseRoleOkBtn extends CDefButton {

	private static final long serialVersionUID = -670525269449389033L;

	/**
	 * On click create new item or update item if no errors and close edit window.
	 * If new case role and it is for pc's case component roles have to be added for each case component within the case.
	 * If existing case role and it is changed from pc to npc or vice versa, also case component roles have to be added or deleted.
	 */
	public void onClick() {
		CCdeCaseRoleWnd lWindow = (CCdeCaseRoleWnd)getRoot();
		IECaseRole lItem = (IECaseRole)lWindow.getItem();
		ICaseRoleManager caseRoleManager = (ICaseRoleManager)CDesktopComponents.sSpring().getBean("caseRoleManager");
		ICaseManager caseManager = (ICaseManager)CDesktopComponents.sSpring().getBean("caseManager");
		Boolean lNew = false;
		if (lItem == null) {
			//new
			lNew = true;
			lItem = caseRoleManager.getNewCaseRole();
			int lCasId = Integer.parseInt((String)CDesktopComponents.cControl().getAccSessAttr("casId"));
			lItem.setECase(caseManager.getCase(lCasId));
		}
		else
			//update
			lItem = (IECaseRole)lItem.clone();
		lItem.setName(CControlHelper.getTextboxValue(this, "name"));
		lItem.setNpc(CControlHelper.isCheckboxChecked(this, "npc"));

		List<String[]> lErrors = new ArrayList<String[]>(0);
		if (lNew)
			lErrors = caseRoleManager.newCaseRole(lItem);
		else
			lErrors = caseRoleManager.updateCaseRole(lItem);
		if ((lErrors == null) || (lErrors.size() == 0)) {
//			Get caserole so date format, name etc. is correct.
			lItem = caseRoleManager.getCaseRole(lItem.getCarId());
			// set casecomponentroles for system components, because cannot do this himself
			int lCarId = lItem.getCarId();
			ICaseComponentManager caseComponentManager = (ICaseComponentManager)CDesktopComponents.sSpring().getBean("caseComponentManager");
			int lCasId = Integer.parseInt((String)CDesktopComponents.cControl().getAccSessAttr("casId"));
			List<IECaseComponent> lCacs = caseComponentManager.getAllCaseComponentsByCasId(lCasId);
			ICaseComponentRoleManager caseComponentRoleManager = (ICaseComponentRoleManager)CDesktopComponents.sSpring().getBean("caseComponentRoleManager");
			if (!lItem.getNpc()) {
				// pc
				// add casecomponentrole for system casecomponents if caserole is pc and if it does not exist yet
				for (IECaseComponent lCac : lCacs) {
					int lCacId = lCac.getCacId();
					if (lCac.getEComponent().getType() == AppConstants.system_component) {
						// only system components
						if (caseComponentRoleManager.getAllCaseComponentRolesByCacIdCarId(lCacId, lCarId).size() == 0) {
							// only if not existing
							IECaseComponentRole lCaseComponentRole = caseComponentRoleManager.getNewCaseComponentRole();
							lCaseComponentRole.setCacCacId(lCac.getCacId());
							lCaseComponentRole.setCarCarId(lItem.getCarId());
							lCaseComponentRole.setName(lCac.getName());
							caseComponentRoleManager.newCaseComponentRole(lCaseComponentRole);
						}
					}
				}
			}
			else {
				// npc
				// if npc check if casecomponentrole exists, also for functional components, if so delete it
				for (IECaseComponent lCac : lCacs) {
					int lCacId = lCac.getCacId();
					List<IECaseComponentRole> lCcrs = caseComponentRoleManager.getAllCaseComponentRolesByCacIdCarId(lCacId, lCarId);
					// remove existing
					for (IECaseComponentRole lCcr : lCcrs) {
						caseComponentRoleManager.deleteCaseComponentRole(lCcr);
					}
				}
			}
			lWindow.setAttribute("item",lItem);
		}
		else {
			lWindow.setAttribute("item",null);
			CDesktopComponents.cControl().showErrors(this,lErrors);
		}
		lWindow.detach();
	}
}
