/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Textbox;

import nl.surf.emergo.business.ICaseComponentManager;
import nl.surf.emergo.business.ICaseComponentRoleManager;
import nl.surf.emergo.business.ICaseRoleManager;
import nl.surf.emergo.business.IComponentManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CCaseHelper;
import nl.surf.emergo.control.CContentHelper;
import nl.surf.emergo.control.CControlHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IECaseComponentRole;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.domain.IEComponent;
import nl.surf.emergo.view.VView;

/**
 * The Class CCdeCaseComponentHelper.
 */
public class CCdeCaseComponentHelper extends CControlHelper {

	protected CContentHelper contentHelper = new CContentHelper();

	protected CCaseHelper caseHelper = new CCaseHelper();

	/**
	 * Gets all case roles.
	 * 
	 * @return all case roles
	 */
	public List<IECaseRole> getAllCaseRoles() {
		return ((ICaseRoleManager)CDesktopComponents.sSpring().getBean("caseRoleManager")).getAllCaseRolesByCasId(((IECase)CDesktopComponents.cControl().getAccSessAttr("case")).getCasId(), false);
	}

	protected class ComponentSortByCode implements Comparator<IEComponent>{
		public int compare(IEComponent o1, IEComponent o2) {
			return CDesktopComponents.vView().getLabel(VView.componentLabelKeyPrefix + o1.getCode()).compareTo(CDesktopComponents.vView().getLabel(VView.componentLabelKeyPrefix + o2.getCode()));
		}
	}
	
	/**
	 * Get all possible components.
	 * 
	 * @param aCasId the a cas id
	 * @param aForNewCaseComponent the a for new case component
	 * 
	 * @return all possible components
	 */
	public List<IEComponent> getPossibleComponents(int aCasId, boolean aForNewCaseComponent) {
		List<IECaseComponent> lAllCaseComponents = ((ICaseComponentManager)CDesktopComponents.sSpring().getBean("caseComponentManager")).getAllCaseComponentsByCasId(aCasId);
		List<IECaseRole> lAllCaseRoles = ((ICaseRoleManager)CDesktopComponents.sSpring().getBean("caseRoleManager")).getAllCaseRolesByCasId(aCasId);
		int lCaseRoleCount = lAllCaseRoles.size();
		ICaseComponentRoleManager caseComponentRoleManager = (ICaseComponentRoleManager)CDesktopComponents.sSpring().getBean("caseComponentRoleManager");

		IComponentManager componentManager = (IComponentManager)CDesktopComponents.sSpring().getBean("componentManager");
		List<IEComponent> lAllComponents = componentManager.getAllComponents(true);
		//NOTE filter lAllComponents on supported components per skin.
		List<IEComponent> lAllAllowedComponents = new ArrayList<IEComponent>();
		for (IEComponent lComponent : lAllComponents) {
			if (CDesktopComponents.sSpring().isComponentAllowed(lComponent)) {
				lAllAllowedComponents.add(lComponent);
			}
		}
		Collections.sort(lAllAllowedComponents, new ComponentSortByCode());
		List<IEComponent> lPossibleComponents = new ArrayList<IEComponent>();
		for (IEComponent lComponent : lAllAllowedComponents) {
			// only functional components can be added by user
			boolean lOk = lComponent.getType() == AppConstants.functional_component;
			boolean lMultiple = CDesktopComponents.sSpring().getComponentMultiple(lComponent);
			if (aForNewCaseComponent && !lMultiple) {
				// if component which isn't allowed to be multiple count existing case components roles for component
				int lCaseComponentRoleCount = 0;
				for (IECaseComponent lCaseComponent : lAllCaseComponents) {
					if (lCaseComponent.getEComponent().getCode().equals(lComponent.getCode())) {
						// if component is already used get case component roles defined for it
						List<IECaseComponentRole> lCaseComponentRoles = caseComponentRoleManager.getAllCaseComponentRolesByCacId(lCaseComponent.getCacId());
						// and increment count with size
						lCaseComponentRoleCount += lCaseComponentRoles.size();
					}
				}
				if (lCaseComponentRoleCount >= lCaseRoleCount) {
					// if component already used for all case roles, don't show it
					lOk = false;
				}
			}
			if (lOk) {
				lPossibleComponents.add(lComponent);
			}
		}
		return lPossibleComponents;
	}

	public void handleCaseComponentRoles(int aCasId, IECaseComponent aCaseComponent, Component aZkComponent) {
		ICaseRoleManager caseRoleManager = (ICaseRoleManager)CDesktopComponents.sSpring().getBean("caseRoleManager");
		ICaseComponentRoleManager caseComponentRoleManager = (ICaseComponentRoleManager)CDesktopComponents.sSpring().getBean("caseComponentRoleManager");
		ICaseComponentManager caseComponentManager = (ICaseComponentManager)CDesktopComponents.sSpring().getBean("caseComponentManager");

		//	add/delete casecomponentroles
		int lCacId = aCaseComponent.getCacId();
		List<IECaseRole> lAllCaseRoles = caseRoleManager.getAllCaseRolesByCasId(aCasId, false);
		if (lAllCaseRoles == null) {
			return;
		}
		for (IECaseRole lCaseRole : lAllCaseRoles) {
			int lCarId = lCaseRole.getCarId();
			IECaseComponentRole lCaseComponentRole = caseComponentRoleManager.getCaseComponentRole(lCacId,lCarId);
			boolean lCaseComponentRoleExists = (lCaseComponentRole != null);
			Checkbox lCb = (Checkbox)aZkComponent.getFellowIfAny(""+lCarId);
			Textbox lTe = (Textbox)aZkComponent.getFellowIfAny("text_"+lCarId);
			//NOTE allow for setting roles from code, apart from developer environment; in that case, case component is added for all case roles
			boolean lChecked = true;
			if (lCb != null) {
				lChecked = lCb.isChecked();
			}
			if ((lChecked) && (!lCaseComponentRoleExists)) {
//					add casecomponentrole
				// if case component which isn't allowed to be multiple
				boolean lOk = true;
				boolean lMultiple = CDesktopComponents.sSpring().getComponentMultiple(aCaseComponent.getEComponent());
				if (!lMultiple) {
					// if case component which isn't allowed to be multiple
					List<IECaseComponent> lCaseComponents = caseComponentManager.getAllCaseComponentsByCasId(aCasId);
					for (IECaseComponent lCaseComponent : lCaseComponents) {
						if (lCaseComponent.getEComponent().getCode().equals(aCaseComponent.getEComponent().getCode())) {
							// if case component is already check if case role already defined for it
							List<IECaseComponentRole> lCaseComponentRoles = caseComponentRoleManager.getAllCaseComponentRolesByCacIdCarId(lCaseComponent.getCacId(), lCaseRole.getCarId());
							if (lCaseComponentRoles.size() > 0)
								lOk = false;
						}
					}
				}
				if (lOk) {
					lCaseComponentRole = caseComponentRoleManager.getNewCaseComponentRole();
					lCaseComponentRole.setCacCacId(aCaseComponent.getCacId());
					lCaseComponentRole.setCarCarId(lCaseRole.getCarId());
					String lName = "";
					//NOTE when importing multiple case components the textbox for an alternative name is not shown!
					if (lTe != null) {
						lName = lTe.getValue();
					}
					if (lName.equals("")) {
						lName = aCaseComponent.getName();
					}
					lCaseComponentRole.setName(lName);
					caseComponentRoleManager.newCaseComponentRole(lCaseComponentRole);
				}
			}
			if ((lChecked) && (lCaseComponentRoleExists)) {
//					save name
				lCaseComponentRole.setName(lTe.getValue());
				caseComponentRoleManager.updateCaseComponentRole(lCaseComponentRole);
			}
			if ((!lChecked) && (lCaseComponentRoleExists)) {
//					remove references to car/cac combination in system casecasecomponent
				CCaseHelper lCaseHelper = new CCaseHelper();
				lCaseHelper.removeCarCacId(AppConstants.contentElement, lCaseComponentRole.getCarCarId(), lCaseComponentRole.getCacCacId());
//					delete casecomponentrole
				caseComponentRoleManager.deleteCaseComponentRole(lCaseComponentRole);
			}
		}
	}

	/**
	 * Replaces all iso entities by special characters.
	 *
	 * @param aCaseComponent the a case component
	 * @param aDataRootTag the a string
	 */
	public void updateTemplateReferenceIds(IECaseComponent aCaseComponent) {
		List<IXMLTag> lNodeTags = CDesktopComponents.cScript().getNodeTags(aCaseComponent);
		List<String> lReferenceTypeList = new ArrayList<String>();
		List<IXMLTag> lTagList = new ArrayList<IXMLTag>();
		List<List<String>> lRefTagIdsList = new ArrayList<List<String>>();
		for (IXMLTag lNodeTag : lNodeTags) {
			Hashtable<String,List<String>> lRefTagIdsPerRefType = null;
			if (lNodeTag.getName().equals("condition")) {
				lRefTagIdsPerRefType = contentHelper.getTemplateConditionRefTagIds(lNodeTag);
			}
			if (lNodeTag.getName().equals("action")) {
				lRefTagIdsPerRefType = contentHelper.getTemplateActionRefTagIds(lNodeTag);
			}
			if (lRefTagIdsPerRefType != null) {
				for (Enumeration<String> lRefTypes = lRefTagIdsPerRefType.keys(); lRefTypes.hasMoreElements();) {
					String lRefType = lRefTypes.nextElement();
					if (lRefType != null && !lRefType.equals("")) {
						List<String> lRefTagIds = lRefTagIdsPerRefType.get(lRefType);
						if (lRefTagIds != null && lRefTagIds.size() > 0) {
							lReferenceTypeList.add(lRefType);
							lTagList.add(lNodeTag);
							lRefTagIdsList.add(lRefTagIds);
						}
					}
				}
				
			}
		}
		caseHelper.setReferenceIds(aCaseComponent, lReferenceTypeList, lTagList, lRefTagIdsList);
	}

}