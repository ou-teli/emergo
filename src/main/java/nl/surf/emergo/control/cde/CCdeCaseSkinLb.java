/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CInputListbox;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.view.VView;

/**
 * The Class CCdeCaseSkinLb.
 */
public class CCdeCaseSkinLb extends CInputListbox {

	private static final long serialVersionUID = 5499500584611163284L;

	public CCdeCaseSkinLb() {
		super();
	}

	/**
	 * On create show possible skins and preselect one if applicable.
	 *
	 * @param aEvent the a event
	 */
	public void onCreate(CreateEvent aEvent) {
		String lActiveSkinNumbersStr = VView.getInitParameter("emergo.activeskinnumbers");
		if (lActiveSkinNumbersStr.equals("")) {
			return;
		}
		lActiveSkinNumbersStr = "," + lActiveSkinNumbersStr + ",";

		IECase lItem = (IECase)((CCdeCaseWnd)getRoot()).getItem(aEvent);
		int lCounter = 1;
		String lDefaultSkinNumber = VView.getInitParameter("emergo.default.skinnumber");
		if (lDefaultSkinNumber.equals("")) {
			lDefaultSkinNumber = "6";
		}
		String lSkinId = VView.getInitParameter("emergo.skin." + lCounter);
		String lCurrentSkinId = "";
		if (lItem != null) {
			lCurrentSkinId = CDesktopComponents.sSpring().getSCaseSkinHelper().getCaseSkin(lItem);
		}
		while (!lSkinId.equals("")) {
			//Only show active skins or inactive if it is the current skin for the case
			boolean lShow = lActiveSkinNumbersStr.contains("," + lCounter + ",") || lSkinId.equals(lCurrentSkinId);
			if (lShow) {
				Listitem lListitem = super.insertListitem(lSkinId, CDesktopComponents.vView().getLabel("skin." + lSkinId), null);
				//Select if equal to current skin or if no current skin, if equal to default skin 
				boolean lSelect = lSkinId.equals(lCurrentSkinId) || (lCurrentSkinId.equals("") && ("" + lCounter).equals(lDefaultSkinNumber));
				if (lSelect) {
					setSelectedItem(lListitem);
				}
			}
			lCounter += 1;
			lSkinId = VView.getInitParameter("emergo.skin."+lCounter);
		}
		
	}

}