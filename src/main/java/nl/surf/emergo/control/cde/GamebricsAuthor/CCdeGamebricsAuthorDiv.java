/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde.GamebricsAuthor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.ICaseComponentManager;
import nl.surf.emergo.business.ICaseManager;
import nl.surf.emergo.business.IComponentManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CCaseHelper;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.cde.CCdeCaseComponentHelper;
import nl.surf.emergo.control.cde.GamebricsAuthor.def.CCdeGamebricsAuthorDefDiv;
import nl.surf.emergo.control.cde.GamebricsAuthor.def.CCdeGamebricsAuthorDefHbox;
import nl.surf.emergo.control.cde.GamebricsAuthor.def.CCdeGamebricsAuthorDefHelper;
import nl.surf.emergo.control.cde.GamebricsAuthor.def.CCdeGamebricsAuthorDefImage;
import nl.surf.emergo.control.cde.GamebricsAuthor.utils.CCdeGamebricsAuthorCustomAttributeHelper;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.control.script.CScript;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IEComponent;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpring;

public class CCdeGamebricsAuthorDiv extends CDefDiv {

	private static final long serialVersionUID = -2989150181145019005L;
	
	protected VView vView = CDesktopComponents.vView();
	protected CControl cControl = CDesktopComponents.cControl();
	protected SSpring sSpring = CDesktopComponents.sSpring();
	protected CScript cScript = CDesktopComponents.cScript();
	protected CCaseHelper cCaseHelper = new CCaseHelper();

	protected String currentRubricCode = "";
	protected boolean _editable;
	protected Map<String,String> _idMap;
	
	protected String _idPrefix = "gamebricsAuthor";
	protected String _classPrefix = "GamebricsAuthor";
	
	protected int _casId;

	protected static final String _cacIdStr = "cacId";
	protected static final String _tagIdStr = "tagId";
	protected static final String _elementTypeStr = "elementType";
	protected static final String _changedChildrenStr = "changedChildren";
	
	public static String cde_GA_rootID;
	
	public static final String cde_GA_redirectBack = "redirectBack";
	
	public String zulfilepath = "";
	
	public static final String cde_GA_CompositeAttrIdStr = "_compositeParentId";
	
	public boolean isVisibleOnCreation() {
		return true;
	}
	
	public void onCreate(CreateEvent aEvent) {
		_idPrefix = CCdeGamebricsAuthorCustomAttributeHelper.handleCustomAttribute(this, "_idPrefix", _idPrefix);
		String lRootId = (String)this.getAttribute(CCdeGamebricsAuthorDefHelper.gRootIdAttrStr);
		if (!StringUtils.isEmpty(lRootId)) {
			cde_GA_rootID = lRootId;
			this.setId(lRootId);
		}
		_casId = Integer.parseInt((String)CDesktopComponents.cControl().getAccSessAttr("casId"));

		init(true, isVisibleOnCreation());
	}
	
	public void init(boolean editable, boolean pVisible) {
		_editable = editable;
		
		setClass(_classPrefix);

		update();
		
		setVisible(pVisible);
	}
	
	public void init(boolean editable, Map<String,String> idMap) {
		_idMap = idMap;
		init(editable, true);
	}
	
	public void update() {
		//override
	}
	
	public static String getRootId() {
		return cde_GA_rootID;
	}
		
	protected String getRootElementTypes() {
		return "";
	}

	protected boolean isRootElementType(String pElementType) {
		List<String> lNodeNamesList = Arrays.asList(getRootElementTypes().split(","));
		if (lNodeNamesList.contains(pElementType) || getRootElementTypes().equalsIgnoreCase(pElementType))
			return true;
		return false;
	}
	
	protected String getUniqueTagName(String elementType) {
		return "pid";
	}

	protected IEComponent getComponent(String pvComponentCode) {
		IComponentManager componentManager = (IComponentManager)sSpring.getBean("componentManager");
		IEComponent pvComponent = null;
		for (IEComponent component : componentManager.getAllComponents(true)) {
			if (component.getCode().equals(pvComponentCode)) {
				pvComponent = component;
				break;
			}
		}
		return pvComponent;
	}
	
	protected List<IECaseComponent> getCaseComponents(String pvComponentCode) {
		return getCaseCaseComponents(_casId, pvComponentCode);
	}
	
	protected List<IECaseComponent> getAllCaseComponents(String pvComponentCode) {
		IEComponent pvComponent = getComponent(pvComponentCode);
		if (pvComponent == null) {
			return new ArrayList<IECaseComponent>();
		}
		else {
			ICaseComponentManager caseComponentManager = (ICaseComponentManager)sSpring.getBean("caseComponentManager");
			List<IECaseComponent> lOrderedCCs = new ArrayList<IECaseComponent>();
			List<IECaseComponent> lOtherCCs = new ArrayList<IECaseComponent>();
			//NOTE case components of current game first in list! - So, always will be present when duplicate elements are rejected
			for (IECaseComponent lCC : caseComponentManager.getAllCaseComponentsByComId(pvComponent.getComId())) {
				if (lCC.getECase().getCasId() == _casId) {
					lOrderedCCs.add(lCC);
				} else {
					lOtherCCs.add(lCC);
				}
			}
			lOrderedCCs.addAll(lOtherCCs);
			return lOrderedCCs;
		}
	}
	
	protected List<IECaseComponent> getCaseCaseComponents(int pCaseId, String pvComponentCode) {
		IEComponent pvComponent = getComponent(pvComponentCode);
		if (pvComponent == null) {
			return new ArrayList<IECaseComponent>();
		}
		else {
			ICaseComponentManager caseComponentManager = (ICaseComponentManager)sSpring.getBean("caseComponentManager");
			List<IECaseComponent> lCCs = new ArrayList<IECaseComponent>();
			for (IECaseComponent lCC : caseComponentManager.getAllCaseComponentsByCasIdComId(pCaseId, pvComponent.getComId())) {
				lCCs.add(lCC);
			}
			return lCCs;
		}
	}
	
	protected void addElementPart(StringBuilder element, String key, String value, boolean separator) {
		element.append("\"");
		element.append(key);
		element.append("\":\"");
		element.append(value);
		element.append("\"");
		if (separator) {
			element.append(",");
		}
	}

	protected void addElementPart(StringBuilder element, String key, StringBuilder value, boolean separator) {
		element.append("\"");
		element.append(key);
		element.append("\":");
		element.append(value);
		if (separator) {
			element.append(",");
		}
	}
	
	protected String getValidJSONString(String pAttributeValue) {
		return sSpring.unescapeXML(pAttributeValue).replace("\n", "").replace("\\", "\\\\\\\\").replace("\"", "\\\\\"");
	}

	protected void addElement(IECaseComponent caseComponent, String owner, String elementType, IXMLTag nodeTag, StringBuilder myElements, StringBuilder allElements) {
		StringBuilder element = new StringBuilder();
		addElementPart(element, _elementTypeStr, elementType, true);
		addElementPart(element, _cacIdStr, "" + caseComponent.getCacId(), true);
		addElementPart(element, _tagIdStr, nodeTag.getAttribute(AppConstants.keyId), true);
		addElementPart(element, "name", sSpring.unescapeXML(nodeTag.getChildValue(getUniqueTagName(elementType))), false);
		if (caseComponent.getECase().getCasId() == _casId) {
			//only allow editing in current game
		//if (caseComponent.getEAccount().getAccId() == cControl.getAccId()) {
			//author of case component is current user
			if (myElements.length() > 0) {
				myElements.append(",");
			}
			myElements.append("{");
			myElements.append(element);
			myElements.append("}");
		}
		if (allElements.length() > 0) {
			allElements.append(",");
		}
		element.append(",");
		addElementPart(element, "owner", owner, false);
		allElements.append("{");
		allElements.append(element);
		allElements.append("}");
	}
		
	protected StringBuilder[] getContent(String pComponentCode, String pContentNames, boolean pAllowDuplicate) {
		//get my pv elements: cacid, tagid, title
		//get all pv elements: cacid, tagid, title, owner
		StringBuilder myElements = new StringBuilder();
		StringBuilder allElements = new StringBuilder();

		IAccountManager accountManager = (IAccountManager)sSpring.getBean("accountManager");
		//NOTE if an owner owns tags with same name, only add one tag
		Map<Integer,List<String>> lOwnerTags = new HashMap<Integer,List<String>>();
		List<String> lNodeNamesList = Arrays.asList(pContentNames.split(","));
		for (IECaseComponent caseComponent : getCaseComponents(pComponentCode)) {
			IEAccount lAcc = caseComponent.getEAccount();
			String owner = accountManager.getAccountName(lAcc);
			List<IXMLTag> nodeTags = cScript.getNodeTags(caseComponent);
			if (nodeTags != null) {
				for (IXMLTag nodeTag : nodeTags) {
					int lInd = lNodeNamesList.indexOf(nodeTag.getName());
					if (lInd >= 0) {
						boolean lAdd = true;
						if (!pAllowDuplicate) {
							int lAccId = lAcc.getAccId();
							String lElName = sSpring.unescapeXML(nodeTag.getChildValue(getUniqueTagName(nodeTag.getName())));
							List<String> lNameList = new ArrayList<String>();
							if (!lOwnerTags.containsKey(lAccId)) {
								lNameList.add(lElName);
								lOwnerTags.put(lAccId, lNameList);
							} else {
								lNameList = lOwnerTags.get(lAccId);
								if (!lNameList.contains(lElName)) {
									lNameList.add(lElName);
								} else
									lAdd = false;
							}
						}
						if (lAdd)
							addElement(caseComponent, owner, lNodeNamesList.get(lInd), nodeTag, myElements, allElements);
					}
				}
			}
		}

		myElements.insert(0, "[");
		myElements.append("]");
		allElements.insert(0, "[");
		allElements.append("]");
		
		return new StringBuilder[]{myElements, allElements};
	}
		
	protected void updateContent(String pComponentCode, String pContentNames, boolean pAllowDuplicate) {
		StringBuilder[] content = getContent(pComponentCode, pContentNames, pAllowDuplicate);
		
		Clients.evalJavaScript("init" + getGAJSContentString(pContentNames) + "('" + content[0].toString() + "','" + content[1].toString() + "');");
	}
	
	protected StringBuilder getElementContent(IXMLTag pElementTag) {
		return new StringBuilder();
	}
		
	protected void selectElement(String pContentNames, IXMLTag pElementTag) {
		StringBuilder lElementContent = getElementContent(pElementTag);
		if (lElementContent.length() > 0 ) {
			Clients.evalJavaScript("select" + getGAJSContentString(pContentNames) + "('" + lElementContent.toString() + "');");
		}
	}
	
	protected String getGAJSContentString(String pContentName) {
		return VView.getCapitalizeFirstChar(pContentName) + "s";
	}
		
	protected void initAllContent(String pComponentCode, String pContentName, boolean pDuplicate) {
		StringBuilder[] content = getContent(pComponentCode, pContentName, pDuplicate);
		
		Clients.evalJavaScript("initAll" + getGAJSContentString(pContentName) + "('" + content[1].toString() + "');");
	}
	
	protected boolean showDuplicatesInList() {
		return true;
	}
	
	protected String getComponentCode(String pElementType) {
		return "GA_" + pElementType + "s";
	}
		
	protected String getDefaultComponentName(String pElementType) {
		return "GA_" + pElementType + "s";
	}
		
	protected String getUpdateElementTypes(String pElementType) {
		return pElementType;
	}
		
	public void onNotify(Event event) {
		JSONObject jsonObject = (JSONObject)event.getData();
		String action = (String)jsonObject.get("action");
		if (action.equals("navigate_forward") || action.equals("navigate_backward")) {
			String toId = (String)jsonObject.get("toId");
			CCdeGamebricsAuthorDiv div = (CCdeGamebricsAuthorDiv)CDesktopComponents.vView().getComponent(toId);
			setVisible(false);
			if (div != null) {
				div.init(true, _idMap);
			}
			else if (action.equals("navigate_backward")) {
				CDesktopComponents.vView().redirectToView(VView.v_cde_gamebricsauthor);
			}
		}
		else if (action.equals("new_element")) {
			String elementType = getElementTypeFromString((String)jsonObject.get(_elementTypeStr));
			updateContent(getComponentCode(elementType), getUpdateElementTypes(elementType), showDuplicatesInList());
		}
		else if (action.equals("edit_element")) {
			String elementType = getElementTypeFromString((String)jsonObject.get(_elementTypeStr));
			String cacId = (String)jsonObject.get(_cacIdStr);
			String tagId = (String)jsonObject.get(_tagIdStr);
			String elementName = (String)jsonObject.get("elementName");
			if (elementName.equals("")) {
				Clients.evalJavaScript("showDefaultAlertPopup('" + vView.getLabel("cde_gamebricsauthor." + getElementTypeMessageLabel(elementType) + ".message.body.element.empty") + "');");
				return;
			}
			IXMLTag elementTag = sSpring.getTag(sSpring.getCaseComponent(cacId), tagId);
			if (elementTag != null) {
				editElementTag(elementType, cacId, elementName, elementTag);
				updateContent(getComponentCode(elementType), getUpdateElementTypes(elementType), showDuplicatesInList());
				selectElement(getUpdateElementTypes(elementType), elementTag);
			}
		}
		else if (action.equals("delete_element")) {
			String elementType = getElementTypeFromString((String)jsonObject.get(_elementTypeStr));
			String cacId = (String)jsonObject.get(_cacIdStr);
			String tagId = (String)jsonObject.get(_tagIdStr);
			IXMLTag elementTag = sSpring.getTag(sSpring.getCaseComponent(cacId), tagId);
			if (elementTag != null) {
				deleteElementTag(elementType, cacId, elementTag);
				updateContent(getComponentCode(elementType), getUpdateElementTypes(elementType), showDuplicatesInList());
			}
		}
		else if (action.equals("show_element")) {
			String elementType = getElementTypeFromString((String)jsonObject.get(_elementTypeStr));
			String mode = (String)jsonObject.get("mode");
			String cacId = (String)jsonObject.get(_cacIdStr);
			String tagId = (String)jsonObject.get(_tagIdStr);
			String elementName = (String)jsonObject.get("elementName");
			if (mode.equals("new") && elementName.equals("")) {
				Clients.evalJavaScript("showDefaultAlertPopup('" + vView.getLabel("cde_gamebricsauthor." + getElementTypeMessageLabel(elementType) + ".message.body.element.empty") + "');");
				return;
			}
			setVisible(false);
			if (mode.equals("new")) {
				List<String> lIds = newElementTag(elementType, elementName);
				cacId = lIds.get(0);
				tagId = lIds.get(1);
			}
			if (mode.equals("update")) {
				//get node tag
				IXMLTag nodeTag = sSpring.getTag(sSpring.getCaseComponent(cacId), tagId);
				if (!isInputOk(elementType, nodeTag)) {
					//if not ok, change mode to new, so dialog is shown to set number of performance levels and/or add skill clusters and/or sub skills
					mode = "new";
				}
			}
			CCdeGamebricsAuthorDiv div = (CCdeGamebricsAuthorDiv)CDesktopComponents.vView().getComponent("gamebricsAuthor" + VView.getCapitalizeFirstChar(mode) + VView.getCapitalizeFirstChar(elementType) + "Div");
			if (div != null) {
				Map<String,String> idMap = new HashMap<String,String>();
				idMap.put(_cacIdStr, cacId);
				idMap.put(_tagIdStr, tagId);
				div.init(true, idMap);
			} else {
				setVisible(true);
				updateContent(getComponentCode(elementType), getUpdateElementTypes(elementType), showDuplicatesInList());
			}
		}
		else if (action.equals("copy_element")) {
			String elementType = getElementTypeFromString((String)jsonObject.get(_elementTypeStr));
			String cacId = (String)jsonObject.get(_cacIdStr);
			String tagId = (String)jsonObject.get(_tagIdStr);
			String elementName = (String)jsonObject.get("elementName");
			IXMLTag originalNodeTag = sSpring.getTag(sSpring.getCaseComponent(cacId), tagId);
			Map<String,String> childTagNameValueMap = new HashMap<String,String>();
			//NOTE for new element, make 'name' child equal to 'pid' child
			childTagNameValueMap.put("pid", elementName);
			childTagNameValueMap.put("name", elementName);
			IXMLTag nodeTag = copyElementTag("", elementType, originalNodeTag, childTagNameValueMap);

			StringBuilder myElements = new StringBuilder();
			StringBuilder allElements = new StringBuilder();

			IAccountManager accountManager = (IAccountManager)sSpring.getBean("accountManager");
			String owner = accountManager.getAccountName(accountManager.getAccount(cControl.getAccId())); 
			addElement(getCaseComponent(elementType), owner, elementType, nodeTag, myElements, allElements);

			myElements.insert(0, "[");
			myElements.append("]");
			
			Clients.evalJavaScript("add" + getGAJSContentString(elementType) + "('" + myElements.toString() + "');");
			updateContent(getComponentCode(elementType), getUpdateElementTypes(elementType), showDuplicatesInList());
		}
		else if (action.equals("select_element")) {
			String elementType = getElementTypeFromString((String)jsonObject.get(_elementTypeStr));
			String cacId = (String)jsonObject.get(_cacIdStr);
			String tagId = (String)jsonObject.get(_tagIdStr);
			IXMLTag selectedNodeTag = sSpring.getTag(sSpring.getCaseComponent(cacId), tagId);

			selectElement(getUpdateElementTypes(elementType), selectedNodeTag);
		}
		else if (action.equals("save_changes")) {
			String elementType = getElementTypeFromString((String)jsonObject.get(_elementTypeStr));
			String elementName = (String)jsonObject.get("elementName");
			String cacId = (String)jsonObject.get(_cacIdStr);
			String tagId = (String)jsonObject.get(_tagIdStr);
			IXMLTag elementTag = sSpring.getTag(sSpring.getCaseComponent(cacId), tagId);
			if (elementTag != null) {
				saveElementTag(elementType, cacId, elementName, elementTag, (JSONObject)jsonObject.get(_changedChildrenStr));
			}
		}

	}
	
	public void notifyInputChange(Component pComp, String pValue) {
        Clients.evalJavaScript("zulElementValueChanged('" + pComp.getId() + "', '" + pValue + "', '" + CCdeGamebricsAuthorDefHelper.getCustomAttribute(pComp, cde_GA_CompositeAttrIdStr) +  "');");
	}
	
	protected boolean isInputOk(String pElementType, IXMLTag tag) {
		if (tag == null || tag.getChildValue(getUniqueTagName("")).equals("")) {
			//name is empty
			return false;
		}
		return true;
	}
	
	protected IXMLTag getReferencedNodeTag(IECaseComponent caseComponent, IXMLTag nodeTag, String refChildTagName) {
		//get node tag referenced to by another node tag
		if (nodeTag == null) {
			return null;
		}
		String referencetype = nodeTag.getChild(refChildTagName).getDefTag().getAttribute(AppConstants.defKeyReftype);
		List<IXMLTag> referenceTags = cCaseHelper.getRefTags(referencetype, "" + AppConstants.statusKeySelectedIndex, CDesktopComponents.sSpring().getSCaseRoleHelper().getCaseRole(), caseComponent, nodeTag);
		//NOTE ref should not be multiple
		if (referenceTags.size() == 1) {
			return referenceTags.get(0);
		}
		return null;
	}

	protected IECaseComponent getCaseComponent(String elementType) {
		IECaseComponent caseComponent = null;
		//get case component, or create it if it doesn't exist.
		ICaseManager caseManager = (ICaseManager)CDesktopComponents.sSpring().getBean("caseManager");
		caseComponent = sSpring.getCaseComponent(caseManager.getCase(getCaseComponentCaseId(elementType)), getComponentCode(elementType), "");
		if (caseComponent == null)
			caseComponent = createCaseComponent(getComponentCode(elementType), getDefaultComponentName(elementType));
		return caseComponent;
	}
	
	protected int getCaseComponentCaseId(String pElementType) {
		return _casId;
	}

	protected String getElementTypeFromString(String pElementTypeString) {
		return pElementTypeString;
	}

	public IXMLTag addElementTag(String parentTagId, String elementType, String tagName, Map<String,String> childTagNameValueMap) {
		IECaseComponent caseComponent = getCaseComponent(elementType);
		IXMLTag elementTag = null;
		if (StringUtils.isEmpty(parentTagId)) {
			elementTag = addRootElementTag(elementType, tagName, childTagNameValueMap);
		}
		else {
			IXMLTag dataRootTag = sSpring.getXmlDataTree(caseComponent);
			IXMLTag tempParentTag = sSpring.getXmlManager().getTagById(dataRootTag, parentTagId);
			if (tempParentTag != null) {
				elementTag = addNodeTag(tempParentTag, tagName, childTagNameValueMap);
			}
			sSpring.setCaseComponentData("" + caseComponent.getCacId(), sSpring.getXmlManager().xmlTreeToDoc(dataRootTag));
		}
		//NOTE return data plus status element because it is always set
		return elementTag;
	}
	
	public IXMLTag addRootElementTag(String elementType, String tagName, Map<String,String> childTagNameValueMap) {
		IECaseComponent caseComponent = getCaseComponent(elementType);
		IXMLTag elementTag = null;
		IXMLTag dataRootTag = sSpring.getXmlDataTree(caseComponent);
		elementTag = addNodeTag(dataRootTag.getChild(AppConstants.contentElement), tagName, childTagNameValueMap);
		sSpring.setCaseComponentData("" + caseComponent.getCacId(), sSpring.getXmlManager().xmlTreeToDoc(dataRootTag));
		return elementTag;
	}
	
	public IXMLTag copyElementTag(String parentTagId, String elementType, IXMLTag originalElementTag, Map<String,String> childTagNameValueMap) {
		IXMLTag elementTag = null;
		IECaseComponent caseComponent = getCaseComponent(elementType);
		//NOTE update data tree. It is updated in application memory as well.
		IXMLTag dataRootTag = sSpring.getXmlDataTree(caseComponent);
		if (StringUtils.isEmpty(parentTagId)) {
			elementTag = copyNodeTag(dataRootTag.getChild(AppConstants.contentElement), originalElementTag, childTagNameValueMap);
		}
		else {
			IXMLTag tempParentTag = sSpring.getXmlManager().getTagById(dataRootTag, parentTagId);
			if (tempParentTag != null) {
				elementTag = copyNodeTag(tempParentTag, originalElementTag, childTagNameValueMap);
			}
		}
		sSpring.setCaseComponentData("" + caseComponent.getCacId(), sSpring.getXmlManager().xmlTreeToDoc(dataRootTag));
		return elementTag;
	}
	
	public List<String> newElementTag(String pElementType, String pElementName) {
		if (isRootElementType(pElementType)) {
			Map<String,String> childTagNameValueMap = new HashMap<String,String>();
			childTagNameValueMap.put("pid", pElementName);
			childTagNameValueMap.put("name", pElementName);
			IXMLTag nodeTag = addElementTag("", pElementType, pElementType, childTagNameValueMap);

			StringBuilder myElements = new StringBuilder();
			StringBuilder allElements = new StringBuilder();

			IAccountManager accountManager = (IAccountManager)sSpring.getBean("accountManager");
			String owner = accountManager.getAccountName(accountManager.getAccount(cControl.getAccId())); 
			
			addElement(getCaseComponent(pElementType), owner, pElementType, nodeTag, myElements, allElements);

			myElements.insert(0, "[");
			myElements.append("]");
			allElements.insert(0, "[");
			allElements.append("]");
			
			List<String> lElementIds = new ArrayList<String>();
			lElementIds.add("" + getCaseComponent(pElementType).getCacId());
			lElementIds.add(nodeTag.getAttribute(AppConstants.keyId));
			
			Clients.evalJavaScript("add" + getGAJSContentString(pElementType) + "('" + myElements.toString() + "');");
			return lElementIds;
		}
		return new ArrayList<String>(Arrays.asList("0", "0"));
	}
	
	protected String getUpdateElementForbiddenLabel() {
		return "";
	}
	
	protected String getDeleteElementForbiddenLabel() {
		return "";
	}
	
	protected String getElementTypeMessageLabel(String pElementType) {
		return pElementType + "s";
	}
	
	public IXMLTag editElementTag(String pElementType, String pCacId, String pElementName, IXMLTag pElementTag) {
		IECaseComponent lCaseComponent = null;
		IXMLTag lRootTag = null;
		if (isRootElementType(pElementType)) {
			lCaseComponent = getCaseComponent(pElementType);
			if (("" + lCaseComponent.getCacId()).equals(pCacId)) {
				//NOTE only allow changes in case component of current game
				lRootTag = sSpring.getXmlDataTree(lCaseComponent);
				pElementTag = editNodeTag(lRootTag, pElementTag, pElementName);
				sSpring.setCaseComponentData(pCacId, sSpring.getXmlManager().xmlTreeToDoc(lRootTag));
			} else {
				Clients.evalJavaScript("showDefaultAlertPopup('" + vView.getLabel(getUpdateElementForbiddenLabel()) + "');");
			}
		}
		return pElementTag;
	}
	
	protected IXMLTag editElementTagChildValue(IXMLTag elementTag, String elementType, String childTagName, String childTagValue) {
		IECaseComponent caseComponent = getCaseComponent(elementType);
		IXMLTag rootTag = sSpring.getXmlDataTree(caseComponent);
		elementTag = sSpring.getXmlManager().getTagById(rootTag, elementTag.getAttribute(AppConstants.statusKeyId));
		setChildTagValue(elementTag, childTagName, childTagValue);
		sSpring.setCaseComponentData("" + caseComponent.getCacId(), sSpring.getXmlManager().xmlTreeToDoc(rootTag));
		return elementTag;
	}
	
	public IXMLTag saveElementTag(String pElementType, String pCacId, String pElementName, IXMLTag pElementTag, JSONObject pChangedChildren) {
		IECaseComponent lCaseComponent = null;
		IXMLTag lRootTag = null;
		if (isRootElementType(pElementType)) {
			lCaseComponent = getCaseComponent(pElementType);
			if (("" + lCaseComponent.getCacId()).equals(pCacId)) {
				//NOTE only allow changes in case component of current game
				lRootTag = sSpring.getXmlDataTree(lCaseComponent);
				pElementTag = sSpring.getXmlManager().getTagById(lRootTag, pElementTag.getAttribute(AppConstants.statusKeyId));
				for (Object lKey : pChangedChildren.keySet()) {
					String lKeyStr = (String)lKey;
					setChildTagValue(pElementTag, lKeyStr, (String)(pChangedChildren.get(lKey)));
				}
				sSpring.setCaseComponentData(pCacId, sSpring.getXmlManager().xmlTreeToDoc(lRootTag));
			} else {
				Clients.evalJavaScript("showDefaultAlertPopup('" + vView.getLabel(getUpdateElementForbiddenLabel()) + "');");
			}
		}
		return pElementTag;
	}
	
	public IXMLTag deleteElementTag(String pElementType, String pCacId, IXMLTag pElementTag) {
		IECaseComponent lCaseComponent = null;
		IXMLTag lRootTag = null;
		if (isRootElementType(pElementType)) {
			lCaseComponent = getCaseComponent(pElementType);
			if (("" + lCaseComponent.getCacId()).equals(pCacId)) {
				//NOTE only allow delete in case component of current game
				lRootTag = sSpring.getXmlDataTree(lCaseComponent);
				deleteNodeTag(lRootTag, pElementTag);
				sSpring.setCaseComponentData(pCacId, sSpring.getXmlManager().xmlTreeToDoc(lRootTag));
			} else {
				Clients.evalJavaScript("showDefaultAlertPopup('" + vView.getLabel(getDeleteElementForbiddenLabel()) + "');");
			}
		}
		return pElementTag;
	}
	
	protected IXMLTag getNodeTag(IXMLTag parentTag, String tagName) {
		IXMLTag tag = null;
		for (IXMLTag nodeTag : cScript.getNodeTags(parentTag)) {
			if (nodeTag.getName().equals(tagName)) {
				tag = nodeTag;
				break;
			}
		}
		return tag;
	}

	protected IXMLTag addNodeTag(IXMLTag parentTag, String tagName, Map<String,String> childTagNameValueMap) {
		IXMLTag nodeTag = getNewNodeTag(parentTag, tagName, childTagNameValueMap);
		parentTag.getChildTags().add(nodeTag);
		return nodeTag;
	}

	protected IXMLTag addNodeTagAfterTag(IXMLTag parentTag, IXMLTag tagBeforeNewTag, String tagName, Map<String,String> childTagNameValueMap) {
		IXMLTag nodeTag = getNewNodeTag(parentTag, tagName, childTagNameValueMap);
		int index = parentTag.getChildTags().indexOf(tagBeforeNewTag);
		parentTag.getChildTags().add(index + 1, nodeTag);
		return nodeTag;
	}

	protected IXMLTag addNodeTagBeforeTag(IXMLTag parentTag, IXMLTag tagAfterNewTag, String tagName, Map<String,String> childTagNameValueMap) {
		IXMLTag nodeTag = getNewNodeTag(parentTag, tagName, childTagNameValueMap);
		int index = parentTag.getChildTags().indexOf(tagAfterNewTag);
		parentTag.getChildTags().add(index, nodeTag);
		return nodeTag;
	}

	protected IXMLTag getContentTag(IXMLTag tag) {
		while (tag != null) {
			if (tag.getName().equals(AppConstants.contentElement)) {
				return tag;
			}
			tag = tag.getParentTag();
		}
		return null;
	}

	protected IXMLTag getDefNodeTag(IXMLTag tag, String tagName) {
		if (tag == null) {
			return null;
		}
		IXMLTag contentTag = getContentTag(tag);
		if (contentTag != null && contentTag.getDefTag() != null) {
			return contentTag.getDefTag().getChild(tagName);
		}
		return null;
	}

	protected IXMLTag getNewNodeTag(IXMLTag parentTag, String tagName, Map<String,String> childTagNameValueMap) {
		IXMLTag nodeTag = sSpring.getXmlManager().newXMLTag(tagName, "");
		nodeTag.setParentTag(parentTag);
		nodeTag.setDefTag(getDefNodeTag(parentTag, tagName));
		nodeTag.setAttribute(AppConstants.defKeyType, AppConstants.defValueNode);
		sSpring.getXmlManager().setTagId(nodeTag);
		for (Map.Entry<String, String> entry : childTagNameValueMap.entrySet()) {
			setChildTagValue(nodeTag, entry.getKey(), entry.getValue());
	    }
		return nodeTag;
	}

	protected IXMLTag copyNodeTag(IXMLTag parentTag, IXMLTag originalNodeTag) {
		IXMLTag nodeTag = sSpring.getXmlManager().copyTagUnique(originalNodeTag, parentTag, true);
		//NOTE copyTagUnique does not add tag to children of parent
		parentTag.getChildTags().add(nodeTag);
		return nodeTag;
	}

	protected IXMLTag copyNodeTag(IXMLTag parentTag, IXMLTag originalNodeTag, Map<String,String> childTagNameValueMap) {
		IXMLTag nodeTag = copyNodeTag(parentTag, originalNodeTag);
		//NOTE set new pid and name
		for (Map.Entry<String, String> entry : childTagNameValueMap.entrySet()) {
			setChildTagValue(nodeTag, entry.getKey(), entry.getValue());
	    }
		return nodeTag;
	}

	protected IXMLTag addChildTag(IXMLTag tag, String childTagName, String childTagValue) {
		IXMLTag childTag = sSpring.getXmlManager().newXMLTag(childTagName, "");
		tag.getChildTags().add(childTag);
		childTag.setParentTag(tag);
		childTag.setDefTag(tag.getDefTag().getChild(childTagName));
		childTag.setValue(sSpring.escapeXML(childTagValue));
		return childTag;
	}

	protected IXMLTag editNodeTag(IXMLTag rootTag, IXMLTag elementTag, String elementName) {
		elementTag = sSpring.getXmlManager().getTagById(rootTag, elementTag.getAttribute(AppConstants.statusKeyId));
		elementName = sSpring.escapeXML(elementName);
		elementTag.setChildValue(getUniqueTagName(elementTag.getName()), elementName);
		return elementTag;
	}

	protected IXMLTag deleteNodeTag(IXMLTag rootTag, IXMLTag elementTag) {
		elementTag = sSpring.getXmlManager().getTagById(rootTag, elementTag.getAttribute(AppConstants.statusKeyId));
		elementTag.getParentTag().getChildTags().remove(elementTag);
		elementTag.setParentTag(null);
		return elementTag;
	}

	protected String getChildTagValue(IXMLTag tag, String childTagName) {
		return sSpring.unescapeXML(tag.getChildValue(childTagName));
	}

	protected void setChildTagValue(IXMLTag tag, String childTagName, String childTagValue) {
		if (tag.getChild(childTagName) == null) {
			addChildTag(tag, childTagName, childTagValue);
		}
		else {
			tag.setChildValue(childTagName, sSpring.escapeXML(childTagValue));
		}
	}

	protected IXMLTag moveNodeTag(IXMLTag parentTag, IXMLTag nodeTag) {
		nodeTag.getParentTag().getChildTags().remove(nodeTag);
		nodeTag.setParentTag(parentTag);
		parentTag.getChildTags().add(nodeTag);
		return nodeTag;
	}

	protected IXMLTag moveNodeTagAfterTag(IXMLTag parentTag, IXMLTag nodeTag, IXMLTag tagBeforeTag) {
		int index = parentTag.getChildTags().indexOf(tagBeforeTag);
		if (index >= 0) {
			if (tagBeforeTag.getParentTag() != nodeTag.getParentTag()) {
				index++;
			}
			nodeTag.getParentTag().getChildTags().remove(nodeTag);
			nodeTag.setParentTag(parentTag);
			parentTag.getChildTags().add(index, nodeTag);
		}
		return nodeTag;
	}

	protected IXMLTag moveNodeTagBeforeTag(IXMLTag parentTag, IXMLTag nodeTag, IXMLTag tagAfterTag) {
		int index = parentTag.getChildTags().indexOf(tagAfterTag);
		if (index >= 0) {
			nodeTag.getParentTag().getChildTags().remove(nodeTag);
			nodeTag.setParentTag(parentTag);
			parentTag.getChildTags().add(index, nodeTag);
		}
		return nodeTag;
	}

	public Div getSpecificDiv(Component parent) {
		// if you have a screen that partly is static and partly is dynamic, meaning it
		// may be partly updated, then you can use a specific div as the parent of the
		// dynamic part.
		// so if you want to update the screen, you just update the dynamic part by
		// calling this method (that will remove the div and its children and create a
		// new one) and add the children again (with new data).
		String specificDivId = parent.getId() + "SpecificDiv";
		Div specificDiv = (Div) vView.getComponent(specificDivId);
		if (specificDiv != null) {
			specificDiv.detach();
		}
		specificDiv = new CDefDiv();
		specificDiv.setId(specificDivId);
		if (parent.getChildren().isEmpty()) {
			parent.appendChild(specificDiv);

		} else {
			parent.insertBefore(specificDiv, parent.getChildren().get(0));
		}
		return specificDiv;
	}

	public String getCurrentRubricCode() {
		return currentRubricCode;
	}

	public IXMLTag getStatusChildTag(IXMLTag tag) {
		// NOTE get the status child tag with name "status" and if it not exists add it
		if (tag == null) {
			return null;
		}
		IXMLTag childTag = tag.getChild(AppConstants.statusElement);
		if (childTag == null) {
			childTag = sSpring.getXmlManager().newXMLTag(AppConstants.statusElement, "");
			tag.getChildTags().add(childTag);
		}
		return childTag;
	}

	public String getStatusChildTagAttribute(IXMLTag tag, String attributeKey) {
		return sSpring.unescapeXML(tag.getChildAttribute(AppConstants.statusElement, attributeKey))
				.replace(AppConstants.statusCommaReplace, ",");
	}

	public void setStatusChildTagAttribute(IXMLTag tag, String attributeKey, String attributeValue) {
		tag.setChildAttribute(AppConstants.statusElement, attributeKey,
				sSpring.escapeXML(attributeValue).replace(",", AppConstants.statusCommaReplace));
	}

	public String getStatusUrl(IXMLTag runStatusTag) {
		// a status url is determined by a blob id stored in progress with blobtype
		// 'database'
		return sSpring.getSBlobHelper().getUrl(runStatusTag.getValue(), AppConstants.blobtypeDatabase);
	}

	public String getDataUrl(IXMLTag dataTag, String aChildTagName) {
		// a data url is determined by a child tag of type blob of a data tag
		return sSpring.getSBlobHelper().getUrl(dataTag, aChildTagName);
	}

	/**
	 * Create new case component.
	 */
	protected IECaseComponent createCaseComponent(String pComCode, String pName) {
		ICaseComponentManager caseComponentManager = (ICaseComponentManager)CDesktopComponents.sSpring().getBean("caseComponentManager");
		ICaseManager caseManager = (ICaseManager)CDesktopComponents.sSpring().getBean("caseManager");
		IECaseComponent lItem = caseComponentManager.getNewCaseComponent();
		lItem.setECase(caseManager.getCase(_casId));
		
		lItem.setEComponent(getComponent(pComCode));
		lItem.setName(pName);
		lItem.setXmldata(CDesktopComponents.sSpring().getEmptyXml());

		IAccountManager lAccManager = (IAccountManager)sSpring.getBean("accountManager");
		lItem.setEAccount(lAccManager.getAccount(cControl.getAccId()));

			// set to 0 to get new record in db
		lItem.setCacId(0);
		List<String[]> lErrors = caseComponentManager.newCaseComponent(lItem);
		if ((lErrors == null) || (lErrors.isEmpty())) {
//			Get casecomponent so date format, name etc. is correct.
			lItem = caseComponentManager.getCaseComponent(lItem.getCacId());
			CCdeCaseComponentHelper caseComponentHelper = new CCdeCaseComponentHelper(); 
			caseComponentHelper.handleCaseComponentRoles(_casId, lItem, this);
			return lItem;
		}
		return null;

	}

	public void renderStars(Component parent, int numberOfStars, boolean selected, String classPrefix) {
		// renders rating stars
		parent.getChildren().clear();

		if (StringUtils.isEmpty(classPrefix)) {
			classPrefix = "score";
		}

		Div div = new CCdeGamebricsAuthorDefDiv(parent, new String[] { "class" },
				new Object[] { classPrefix + "LevelStars" });

		Hbox hbox = new CCdeGamebricsAuthorDefHbox(div, new String[] { "class" },
				new Object[] { classPrefix + "LevelStars" });

		for (int i = 0; i < numberOfStars; i++) {
			appendStar(hbox, selected, classPrefix);
		}
	}

	protected void appendStar(Component parent, boolean selected, String classPrefix) {
		// renders rating star
		String src = "";
		if (selected) {
			src = zulfilepath + "star-filled-blue.svg";
		} else {
			src = zulfilepath + "star-filled-black.svg";
		}
		new CCdeGamebricsAuthorDefImage(parent, new String[] { "class", "src" },
				new Object[] { classPrefix + "LevelStar", src });
	}

}
