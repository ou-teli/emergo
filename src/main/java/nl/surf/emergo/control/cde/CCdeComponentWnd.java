/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Treecol;
import org.zkoss.zul.Treecols;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CContentHelper;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDecoratedInputWndTC;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CTree;
import nl.surf.emergo.control.def.CDefIframe;
import nl.surf.emergo.control.def.CDefTreecol;
import nl.surf.emergo.control.def.CDefTreecols;
import nl.surf.emergo.control.def.CDefWindow;
import nl.surf.emergo.view.VView;

/**
 * The Class CCdeComponentWnd.
 *
 * Is used by case developer.
 * Is used to show the case component window, containing checkboxes for case component
 * properties and a component for the case component content.
 */
public class CCdeComponentWnd extends CDecoratedInputWndTC {

	private static final long serialVersionUID = 3239036942927360596L;

	/** The accisauthor state. */
	protected boolean accisauthor = false;

	/**
	 * Instantiates a new c cde component wnd.
	 * Gets google maps key for current server address out of .properties file and saves it as session var.
	 * Otherwise google maps will not work.
	 */
	public CCdeComponentWnd() {
		super();
		// We have to get the key of Google Maps since it is used as a component.
		// To add more keys: http://www.google.com/apis/maps/signup.html
		String gkey = CDesktopComponents.sSpring().getAppManager().getSysvalue(AppConstants.syskey_gmapskey);
		if (gkey != null && !gkey.equals("")) {
			Executions.getCurrent().getDesktop().getSession().setAttribute("gmapsKey", gkey);
		}
	}

	/**
	 * On create show edit content window for current case component.
	 * 
	 * @param aEvent the a event
	 */
	public void onCreate(CreateEvent aEvent) {
		super.onCreate(aEvent);
		// get current case component owner account id, is set before
		String lCacAccId = (String)CDesktopComponents.cControl().getAccSessAttr("cacAccId");
		// get current account id, is set before
		int lAccId = CDesktopComponents.cControl().getAccId();
		// determine if owner of current case component is current account
		// the owner is also the author
		accisauthor = Integer.parseInt(lCacAccId) == lAccId;
		CContentHelper cComponent = new CContentHelper(null);
		// cComponent uses session variables to get current xml tree
		IXMLTag lXmlTree = cComponent.getXmlDataTree();

		// render input form for case component properties within ZK rows object
		cComponent.xmlComponentToRows(lXmlTree, getFellowIfAny("componentRws"));

		// render input element for case component content
		if (cComponent.getCaseComponent().getEComponent().getCode().equals("googlemaps")) {
			// google maps has to be shown within an iframe, content is rendered within this iframe
			createGoogleMaps("contentMap");
		}
		else {
			if (CDesktopComponents.vView().getIncludeThatRunsEmergo() == null) {
				if (overlapped) {
					doOverlapped();
				}
				if (!StringUtils.isEmpty(position)) {
					setPosition(position);
				}
			}
			Component lComponent = createTree("contentTree");
			// render tree items for case component within ZK tree object on screen
			cComponent.xmlContentToContentItems(lXmlTree, lComponent);
		}
	}

	/**
	 * Creates tree.
	 *
	 * @param aId the a id
	 *
	 * @return the component
	 */
	protected Component createTree(String aId) {
		CTree lTree = new CTree();
		appendChild(lTree);
		//NOTE uncomment next line if temporarily multiple tree items have to be selected.
		//For instance if multiple tree items have to be deleted
//		lTree.setMultiple(true);
		lTree.setId(aId);
		lTree.setWidth("100%");
		lTree.setRows(0);
		Treecols lTreecols = new CDefTreecols();
		lTree.appendChild(lTreecols);
		lTreecols.setStyle("color:black;background-color:#EEEEEE;");
		lTreecols.setSizable(true);
		Treecol lTreecol = new CDefTreecol();
		lTreecols.appendChild(lTreecol);
		lTreecol.setLabel(CDesktopComponents.vView().getLabel("cde_components.tree.col.1"));
		lTreecol.setWidth("75%");
		lTreecol = new CDefTreecol();
		lTreecols.appendChild(lTreecol);
		lTreecol.setLabel(CDesktopComponents.vView().getLabel("cde_components.tree.col.2"));
		lTreecol.setWidth("20%");
		return lTree;
	}

	/**
	 * Creates google maps within iframe. Session attributes are set to be used within the iframe.
	 *
	 * @param aId the a id
	 *
	 * @return the component
	 */
	protected Component createGoogleMaps(String aId) {
		CDefWindow lWindow = new CDefWindow();
		appendChild(lWindow);
		lWindow.setTitle(CDesktopComponents.vView().getLabel("cde_components.googlemaps.title"));
		CControl lControl = CDesktopComponents.cControl();
		if (CDesktopComponents.sSpring().inRun()) {
			lControl.setRunSessAttr("sspring", CDesktopComponents.sSpring());
		}
		lControl.setAccSessAttr("accisauthor", ""+accIsAuthor());
		Iframe lIframe = new CDefIframe();
		lWindow.appendChild(lIframe);
		lIframe.setStyle("width:950px;height:434px;border:0px solid darkgray;margin: 0px 16px 0px 16px;vertical-align:middle;background-color:white;");
		lIframe.setSrc(VView.v_cde_gmaps_fr);
		return lIframe;
	}

	/**
	 * Checks if current account is author.
	 *
	 * @return true, if successful
	 */
	public boolean accIsAuthor() {
		return accisauthor;
	}

}
