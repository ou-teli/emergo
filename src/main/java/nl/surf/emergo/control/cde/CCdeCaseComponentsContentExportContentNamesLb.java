/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefListbox;
import nl.surf.emergo.view.VView;

/**
 * The Class CCdeCaseComponentsContentExportContentNamesLb.
 */
public class CCdeCaseComponentsContentExportContentNamesLb extends CDefListbox {
	
	private static final long serialVersionUID = -4600545483747043875L;

	protected VView vView = CDesktopComponents.vView();

	public void onCreate(CreateEvent aEvent) {
		for (Enumeration<String> contentNames = ((CCdeCaseComponentsContentExportWnd)getRoot()).hContentNames.keys(); contentNames.hasMoreElements();) {
			String contentName = contentNames.nextElement();
			Listitem listitem = CCdeCaseComponentsContentExportWnd.addListitem(this, contentName);
			listitem.setAttribute("contentName", contentName);
			listitem.setAttribute("caseComponentNames", ((CCdeCaseComponentsContentExportWnd)getRoot()).hContentNames.get(contentName).get("caseComponentNames"));
			listitem.setAttribute("tagNames", ((CCdeCaseComponentsContentExportWnd)getRoot()).hContentNames.get(contentName).get("tagNames"));
			listitem.setAttribute("contentTypes", ((CCdeCaseComponentsContentExportWnd)getRoot()).hContentNames.get(contentName).get("contentTypes"));
		}
		Events.sendEvent("onStoreSelectedItemNames", this, null);
	}

	public void onUpdate() {
		List<String> selectedCaseComponentNames = (List<String>)vView.getComponent("caseComponentNames").getAttribute("selectedItemNames");
		List<String> selectedTagNames = (List<String>)vView.getComponent("tagNames").getAttribute("selectedItemNames");
		List<String> selectedContentTypes = (List<String>)vView.getComponent("contentTypes").getAttribute("selectedItemNames");
		for (Listitem listitem : getItems()) {
			boolean visible = false;
			List<String> caseComponentNames = (List<String>)listitem.getAttribute("caseComponentNames");
			List<String> tagNames = (List<String>)listitem.getAttribute("tagNames");
			List<String> contentTypes = (List<String>)listitem.getAttribute("contentTypes");
			for (String caseComponentName : caseComponentNames) {
				for (String tagName : tagNames) {
					for (String contentType : contentTypes) {
						if (selectedCaseComponentNames.contains(caseComponentName) && 
								selectedTagNames.contains(tagName) &&
								selectedContentTypes.contains(contentType)) {
							visible = true;
							break;
						}
					}
				}
			}
			listitem.setVisible(visible);
		}
		Events.sendEvent("onStoreSelectedItemNames", this, null);
	}

	public void onSelect() {
		Events.sendEvent("onStoreSelectedItemNames", this, null);
	}
	
	public void onStoreSelectedItemNames() {
		List<String> selectedItemNames = new ArrayList<String>();
		for (Listitem listitem : getSelectedItems()) {
			if (listitem.isVisible()) {
				selectedItemNames.add((String)listitem.getAttribute("contentName"));
			}
		}
		setAttribute("selectedItemNames", selectedItemNames);
	}

}
