/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde.GamebricsAuthor.def;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;

import nl.surf.emergo.control.CIObserved;
import nl.surf.emergo.control.CIObserver;
import nl.surf.emergo.control.def.CDefTimer;

/**
 * The Class CDefTimer.
 * 
 * Is extension of ZK framework class.
 * All corresponding application classes extend from this one.
 * 
 * Implements CIObserved en CIObserver interfaces and uses observerManager
 * to implement Observer design pattern.
 */
public class CCdeGamebricsAuthorDefTimer extends CDefTimer implements CIObserved, CIObserver {

	private static final long serialVersionUID = 9136177579268329441L;
	
	private Component gNotifyObject;

	public void onStart(Event aEvent) {
		if (aEvent != null)
			gNotifyObject = (Component)aEvent.getData();
		start();
	}

	public void onTimer(Event event) {
		if (gNotifyObject == null)
			gNotifyObject = getParent();
		Events.echoEvent("onTimer", gNotifyObject, null);
	}

}
