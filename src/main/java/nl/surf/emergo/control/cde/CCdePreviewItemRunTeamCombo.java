/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde;

import java.util.List;

import org.zkoss.zul.Listitem;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefListbox;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunTeam;

/**
 * The Class CCdePreviewItemRunTeamCombo.
 */
public class CCdePreviewItemRunTeamCombo extends CDefListbox {

	private static final long serialVersionUID = 1671775450423828607L;

	/** The target. */
	protected CCdePreviewItemPreviewBtn target = null;

	/**
	 * Instantiates a new c cde preview item run team combo and renders possible run teams.
	 * 
	 * @param aId the a id
	 * @param aRunGroup the a run group
	 * @param aRunTeams the a run teams
	 * @param aTarget the a target
	 */
	public CCdePreviewItemRunTeamCombo(String aId,IERunGroup aRunGroup,List<IERunTeam> aRunTeams,CCdePreviewItemPreviewBtn aTarget) {
		setRows(1);
		setMold("select");
		if (!aId.equals(""))
			setId(aId);
		target = aTarget;
		update(aRunTeams);
	}

	/**
	 * (Re)render combobox items and preselect one if applicable.
	 * 
	 * @param aRunTeams the a run teams
	 */
	public void update(List<IERunTeam> aRunTeams) {
		getChildren().clear();
		Listitem lListitem = super.insertListitem(null,CDesktopComponents.vView().getLabel("none"),null);
		setSelectedItem(lListitem);
		for (IERunTeam lObject : aRunTeams) {
			lListitem = super.insertListitem(lObject,lObject.getName(),null);
			if (aRunTeams.size() == 1)
				setSelectedItem(lListitem);
		}
	}
	
	/**
	 * On select update redirect button with selected run team.
	 */
	public void onSelect() {
		boolean lRunTeamSelected = (getSelectedItem().getValue() != null);
		if (target != null) {
			target.setDisabled(!lRunTeamSelected);
			target.setRunTeam((IERunTeam)getSelectedItem().getValue());
		}
	}
}
