/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zhtml.A;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import nl.surf.emergo.control.CCancelBtn;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CImportExportHelper;
import nl.surf.emergo.control.CImportExportWnd;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;

/**
 * The Class CCdeCaseComponentsContentExportOkBtn.
 */
public class CCdeCaseComponentsContentExportOkBtn extends CCancelBtn {

	private static final long serialVersionUID = 7275659736320343616L;

	protected VView vView = CDesktopComponents.vView();

	public void onClick() {
		List<IECaseComponent> selectedCaseComponents = new ArrayList<IECaseComponent>();
		for (Listitem listitem : ((Listbox)vView.getComponent("caseComponentNames")).getSelectedItems()) {
			if (listitem.isVisible() && listitem.getAttribute("caseComponent") != null) {
				selectedCaseComponents.add((IECaseComponent)listitem.getAttribute("caseComponent"));
			}
		}
		List<String> selectedTagNames = (List<String>)vView.getComponent("tagNames").getAttribute("selectedItemNames");
		List<String> selectedContentTypes = (List<String>)vView.getComponent("contentTypes").getAttribute("selectedItemNames");
		List<String> selectedContentNames = (List<String>)vView.getComponent("contentNames").getAttribute("selectedItemNames");
		CImportExportHelper lHelper = ((CImportExportWnd)getRoot()).getImportExportHelper();

		//TODO export depending on chosen Locale
		String url = lHelper.exportCaseComponentContent(selectedCaseComponents, selectedTagNames, selectedContentTypes, selectedContentNames, 
				((Checkbox)vView.getComponent("exportAsExcel")).isChecked(), 
				((Checkbox)vView.getComponent("includeStreamingFolder")).isChecked(), 
				((Checkbox)vView.getComponent("includeHTMLStyle")).isChecked(), 
				((Checkbox)vView.getComponent("allContentEditable")).isChecked(), 
				((Checkbox)vView.getComponent("includeAttributes")).isChecked(),
				((Combobox)vView.getComponent("languageCodes")).getSelectedItem().getLabel());
		if (url.equals("")) {
			vView.showMessagebox(getRoot(), vView.getLabel("cde_s_casecomponents_content_export.error.body"), vView.getLabel("cde_s_casecomponents_content_export.error.title"), Messagebox.OK, Messagebox.ERROR);
		}
		else {
			((A)vView.getComponent("downloadLink")).setDynamicProperty("href", url);
			vView.getComponent("caseComponentListboxDiv").setVisible(false);
			vView.getComponent("downloadLinkDiv").setVisible(true);
			((Window)getRoot()).setPosition("center,center");
		}
	}

}
