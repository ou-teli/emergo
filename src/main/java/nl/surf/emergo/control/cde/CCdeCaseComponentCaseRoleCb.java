/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde;

import java.util.List;

import org.zkoss.zk.ui.event.CreateEvent;

import nl.surf.emergo.business.ICaseComponentManager;
import nl.surf.emergo.business.ICaseComponentRoleManager;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefCheckbox;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IECaseComponentRole;

/**
 * The Class CCdeCaseComponentCaseRoleCb.
 */
public class CCdeCaseComponentCaseRoleCb extends CDefCheckbox {

	private static final long serialVersionUID = -6554217338690033462L;

	/**
	 * On create show case role checkbox and set checked if it is set before, default is true.
	 * Disabled is set to true if not checked, component is not multiple and role is coupled to same component type. For one
	 * case role there can only be one case component defined if the (case) component is not multiple.
	 * If the case component is copied, disabled also is set set to true.
	 * 
	 * @param aEvent the a event
	 */
	public void onCreate(CreateEvent aEvent) {
		if (!(getRoot() instanceof CCdeCaseComponentWnd)) {
			setChecked(true);
			return;
		}
		CCdeCaseComponentWnd lWindow = (CCdeCaseComponentWnd)getRoot();
		IECaseComponent lItem = (IECaseComponent)lWindow.getItem(aEvent);
		String lAction = lWindow.getAction(aEvent);
		boolean lCopy = lAction != null && lAction.equals("copy");
		boolean lChecked = true;
		if (lItem != null) {
			int lCacId = lItem.getCacId();
			ICaseComponentRoleManager caseComponentRoleManager = (ICaseComponentRoleManager)CDesktopComponents.sSpring().getBean("caseComponentRoleManager");
			IECaseComponentRole lCaseComponentRole = caseComponentRoleManager.getCaseComponentRole(lCacId,Integer.parseInt(getId())); 
			lChecked = (lCaseComponentRole != null);
//			set disabled if not checked, component is not multiple and role is coupled to same component type
			if (!lChecked) {
				boolean lMultiple = CDesktopComponents.sSpring().getComponentMultiple(lItem.getEComponent());
				if (!lMultiple) {
					boolean lDisabled = false;
					ICaseComponentManager caseComponentManager = (ICaseComponentManager)CDesktopComponents.sSpring().getBean("caseComponentManager");
					List<IECaseComponent> lCaseComponents = caseComponentManager.getAllCaseComponentsByComId(lItem.getEComponent().getComId());
					for (IECaseComponent lCaseComponent : lCaseComponents) {
						lCaseComponentRole = caseComponentRoleManager.getCaseComponentRole(lCaseComponent.getCacId(),Integer.parseInt(getId())); 
						if (lCaseComponentRole != null) {
							lDisabled = true;
						}
					}
					setDisabled(lDisabled);
				}
			}
			if (lCopy) {
				// if copy checked can not be changed
				setDisabled(true);
			}
		}
		setChecked(lChecked);
	}
}
