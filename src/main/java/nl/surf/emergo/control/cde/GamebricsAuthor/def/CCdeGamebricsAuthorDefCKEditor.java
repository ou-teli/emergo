/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde.GamebricsAuthor.def;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.InputEvent;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefCKeditor;

public class CCdeGamebricsAuthorDefCKEditor extends CDefCKeditor {

	private static final long serialVersionUID = -8090274078742906813L;
	
	private static final String gTimerId = "RichtextTimer";
	
	private String gValue = ""; 

	//NOTE is called if class is used within ZUL file
	public CCdeGamebricsAuthorDefCKEditor() {
		super();

		//NOTE not firing?
    	CCdeGamebricsAuthorDefCKEditor lEditor = this;
        addEventListener(Events.ON_BLUR, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                // Trigger the change event when the CKEditor loses focus
            	notifyChange(lEditor.getValue());
            }
        });
        
	}
	
	/*
	public void onCreate(CreateEvent aEvent) {
		appendTimer();
	}
	
	//NOTE same problem as onChange: server side value only updated at certain moments, not necessarily related to client side input change
	public void appendTimer() {
        CCdeGamebricsAuthorDefTimer lRichTextTimer = new CCdeGamebricsAuthorDefTimer();
		getParent().appendChild(lRichTextTimer);
		lRichTextTimer.setId(getId() + gTimerId);
		lRichTextTimer.setRunning(false);
		lRichTextTimer.setRepeats(false);
		lRichTextTimer.setDelay(3000);
		Events.echoEvent("onStart", lRichTextTimer, this);
		gValue = getValue();
	}
	*/

	//NOTE not reliable
	public void onChange(InputEvent aEvent) {
		super.onChange(aEvent);
		notifyChange(aEvent.getValue());
	}
	
	protected void notifyChange(String pValue) {
	}

	//NOTE not firing?
	public void onBlur(Event aEvent) {
		notifyChange(getValue());
		//Events.postEvent("onChange", this, null);
	}

	public void onTimer() {
		CCdeGamebricsAuthorDefTimer lRichTextTimer = (CCdeGamebricsAuthorDefTimer) CDesktopComponents.vView()
				.getComponent(getId() + gTimerId);
		if (!getValue().equals(gValue)) {
			notifyChange(getValue());
			gValue = getValue();
		}
		Events.echoEvent("onStart", lRichTextTimer, this);
	}

}
