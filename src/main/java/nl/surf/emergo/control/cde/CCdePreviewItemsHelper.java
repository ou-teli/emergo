/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import org.zkoss.zul.Html;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.ICaseRoleManager;
import nl.surf.emergo.business.IRunGroupAccountManager;
import nl.surf.emergo.business.IRunGroupManager;
import nl.surf.emergo.business.IRunManager;
import nl.surf.emergo.business.IRunTeamManager;
import nl.surf.emergo.business.IRunTeamRunGroupManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CControlHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefListcell;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.domain.IERunTeam;
import nl.surf.emergo.domain.IERunTeamRunGroup;

/**
 * The Class CCdePreviewItemsHelper.
 */
public class CCdePreviewItemsHelper extends CControlHelper {

	/** The e run. */
	protected IERun eRun = null;

	/** The run group manager. */
	protected IRunGroupManager runGroupManager = null;

	/** The run group account manager. */
	protected IRunGroupAccountManager runGroupAccountManager = null;

	/** The e account. */
	protected IEAccount eAccount = null;

	/** The e case. */
	protected IECase eCase = null;

	/** The e case component. */
	protected IECaseComponent eCaseComponent = null;

	/** The item. */
	protected IXMLTag item = null;

	/** The team case. */
	protected boolean teamCase = false;

	/** The run group ids. */
	protected List<Integer> runGroupIds = null;

	/** The run team run groups. */
	protected List<IERunTeamRunGroup> runTeamRunGroups = null;

	/** The run group accounts. */
	protected List<IERunGroupAccount> runGroupAccounts = null;

	/** The run teams. */
	protected List<IERunTeam> runTeams = null;

	/**
	 * Gets the item.
	 *
	 * @return the item
	 */
	public IXMLTag getItem() {
		return item;
	}

	/**
	 * Instantiates a new c cde preview items helper.
	 * Adds initial test run groups and test run group accounts if not existing yet.
	 */
	public CCdePreviewItemsHelper() {
		CControl cControl = CDesktopComponents.cControl();
		eAccount = CDesktopComponents.sSpring().getAccount();
		eCase = (IECase) cControl.getAccSessAttr("case");
		eCaseComponent = (IECaseComponent) cControl.getAccSessAttr("casecomponent");
		boolean lHasTestRun = ((IRunManager) CDesktopComponents.sSpring().getBean("runManager")).hasTestRun(eAccount, eCase);
		// if test run does not exist it is created
		eRun = ((IRunManager) CDesktopComponents.sSpring().getBean("runManager")).getTestRun(eAccount, eCase);
		runTeams = ((IRunTeamManager)CDesktopComponents.sSpring().getBean("runTeamManager")).getAllRunTeamsByRunId(eRun.getRunId());
		runGroupManager = (IRunGroupManager) CDesktopComponents.sSpring().getBean("runGroupManager");
		runGroupAccountManager = (IRunGroupAccountManager) CDesktopComponents.sSpring().getBean("runGroupAccountManager");
		if (!lHasTestRun)
			addInitialTestRunGroups();
	}

	/**
	 * Adds initial test run group and test run group account for each case role.
	 */
	public void addInitialTestRunGroups() {
		List<IECaseRole> lCaseRoles = ((ICaseRoleManager)CDesktopComponents.sSpring().getBean("caseRoleManager")).getAllCaseRolesByCasId(((IECase)CDesktopComponents.cControl().getAccSessAttr("case")).getCasId(), false);
		for (IECaseRole lCaseRole : lCaseRoles) {
			IERunGroup lItem = (IERunGroup)runGroupManager.getNewRunGroup();
			lItem.setECaseRole(lCaseRole);
			lItem.setERun(eRun);
			lItem.setName(lCaseRole.getName());
			runGroupManager.newRunGroup(lItem);
			IERunGroupAccount lItem2 = (IERunGroupAccount)runGroupAccountManager.getNewRunGroupAccount();
			lItem2.setEAccount(eAccount);
			lItem2.setERunGroup(lItem);
			runGroupAccountManager.newRunGroupAccount(lItem2);
		}
	}

	/**
	 * Sets the selected item.
	 *
	 * @param aItem the new item
	 */
	public void setItem(IXMLTag aItem) {
		item = aItem;
	}

	/**
	 * Renders all run groups.
	 *
	 * @param aListbox the a listbox
	 */
	public void renderItems(CCdePreviewItemsLb aListbox) {
		aListbox.getItems().clear();
		teamCase = aListbox.isTeamCase();
		List<IERunGroup> lItems = runGroupManager.getAllRunGroupsByRunId(eRun.getRunId());
		runGroupIds = new ArrayList<Integer>();
		for (IERunGroup lItem : lItems) {
			runGroupIds.add(lItem.getRugId());
		}
		for (IERunGroup lItem : lItems) {
			renderItem(aListbox,null,lItem);
		}
	}

	/**
	 * Gets the run teams ids for aRunGroup.
	 *
	 * @param aRunGroup the a run group
	 *
	 * @return the run teams ids
	 */
	public List<String> getRunTeamIds(IERunGroup aRunGroup) {
		List<String> lRunTeamIds = new ArrayList<String>(0);
		Hashtable<String,String> lRunTeamsHash = new Hashtable<String,String>(0);
		List<IERunTeamRunGroup> lItems2 = null;
		if (runGroupIds != null) {
			if (runTeamRunGroups == null)
				runTeamRunGroups = ((IRunTeamRunGroupManager)CDesktopComponents.sSpring().getBean("runTeamRunGroupManager")).getAllRunTeamRunGroupsByRugIds(runGroupIds);
			lItems2 = runTeamRunGroups;
		}
		else
			lItems2 = ((IRunTeamRunGroupManager)CDesktopComponents.sSpring().getBean("runTeamRunGroupManager")).getAllRunTeamRunGroupsByRugId(aRunGroup.getRugId());
		for (IERunTeamRunGroup lRunTeamRunGroup : lItems2) {
			if (lRunTeamRunGroup.getRugRugId() == aRunGroup.getRugId()) {
				int lRunTeamId = lRunTeamRunGroup.getRutRutId();
				lRunTeamsHash.put(""+lRunTeamId, ""+lRunTeamId);
			}
		}
		for (Enumeration<String> lKeys = lRunTeamsHash.keys(); lKeys.hasMoreElements();) {
			lRunTeamIds.add(lRunTeamsHash.get(lKeys.nextElement()));
		}
		return lRunTeamIds;
	}

	/**
	 * Renders one run group and buttons and combo boxes to act on the run group.
	 *
	 * @param aListbox the a listbox
	 * @param aInsertBefore the a insert before
	 * @param aItem the a item
	 */
	public void renderItem(CCdePreviewItemsLb aListbox,Listitem aInsertBefore,IERunGroup aItem) {
		Listitem lListitem = super.newListitem();
		lListitem.setValue(aItem);
		List<String> lRunTeamIds = getRunTeamIds(aItem);
		List<IERunTeam> lRunTeams = new ArrayList<IERunTeam>();
		for (IERunTeam lRut : runTeams) {
			for (String lRutId : lRunTeamIds) {
				if ((""+lRut.getRutId()).equals(lRutId))
					lRunTeams.add(lRut);
			}
		}

		IERunGroup eRunGroup = aItem;
		List<IERunGroupAccount> eRunGroupAccounts = null;
		if (runGroupIds != null) {
			if (runGroupAccounts == null)
				runGroupAccounts = runGroupAccountManager.getAllRunGroupAccountsByAccId(eAccount.getAccId());
			eRunGroupAccounts = new ArrayList<IERunGroupAccount>();
			for (IERunGroupAccount lRunGroupAccount : runGroupAccounts) {
				if ((lRunGroupAccount.getERunGroup().getRugId() == eRunGroup.getRugId()) && (lRunGroupAccount.getEAccount().getAccId() == eAccount.getAccId()))
					eRunGroupAccounts.add(lRunGroupAccount);
			}
		}
		else
			eRunGroupAccounts = runGroupAccountManager.getAllRunGroupAccountsByRugIdAccId(eRunGroup.getRugId(), eAccount.getAccId());
//		There should be only one rungroupaccount per rungroup and account
		if (eRunGroupAccounts.size() != 1)
			return;
		IERunGroupAccount eRunGroupAccount = (IERunGroupAccount)eRunGroupAccounts.get(0);
		String lName = eRunGroup.getName();
		super.appendListcell(lListitem,lName);
		String lRole = eRunGroup.getECaseRole().getName();
		super.appendListcell(lListitem,lRole);
		Listcell lListcell = new CDefListcell();
		boolean lShowRunTeamCombo = ((teamCase) && (lRunTeamIds.size() > 0));
		CCdePreviewItemPreviewBtn lButton = new CCdePreviewItemPreviewBtn("",eRunGroup,eCaseComponent.getCacId(),
				getItem().getAttribute(AppConstants.keyId), eRunGroupAccount.getRgaId(), AppConstants.runStatusPreview, teamCase);
		lButton.setLabel(CDesktopComponents.vView().getLabel("preview"));
		lButton.setDisabled((teamCase) && (lRunTeamIds.size() > 1));
		IERunTeam lRunTeam = null;
		if (lRunTeamIds.size() == 1) {
			for (IERunTeam lRut : runTeams) {
				if ((""+lRut.getRutId()).equals((String)lRunTeamIds.get(0)))
					lRunTeam = lRut;
			}
		}
		if (lRunTeamIds.size() == 1) {
			if (lRunTeam != null) {
				lButton.setRunTeam(lRunTeam);
			}
		}
		if (lShowRunTeamCombo) {
			CCdePreviewItemRunTeamCombo lRunTeamCombo = new CCdePreviewItemRunTeamCombo("",eRunGroup,lRunTeams,lButton);
			lListcell.appendChild(lRunTeamCombo);
			lListcell.appendChild(new Html("<br/>"));
		}
		lListcell.appendChild(lButton);
		lListitem.appendChild(lListcell);
		lListcell = new CDefListcell();
		lButton = new CCdePreviewItemPreviewBtn("",eRunGroup,eCaseComponent.getCacId(),
				getItem().getAttribute(AppConstants.keyId), eRunGroupAccount.getRgaId(), AppConstants.runStatusPreviewReadOnly, teamCase);
		lButton.setLabel(CDesktopComponents.vView().getLabel("preview_item.preview_readonly"));
		lButton.setDisabled((teamCase) && (lRunTeamIds.size() > 1));
		if (lRunTeamIds.size() == 1) {
			if (lRunTeam != null) {
				lButton.setRunTeam(lRunTeam);
			}
		}
		if (lShowRunTeamCombo) {
			CCdePreviewItemRunTeamCombo lRunTeamCombo = new CCdePreviewItemRunTeamCombo("",eRunGroup,lRunTeams,lButton);
			lListcell.appendChild(lRunTeamCombo);
			lListcell.appendChild(new Html("<br/>"));
		}
		lListcell.appendChild(lButton);
		lListitem.appendChild(lListcell);

		lListcell = new CDefListcell();
		lButton = new CCdePreviewItemPreviewBtn("",eRunGroup,eCaseComponent.getCacId(),
				getItem().getAttribute(AppConstants.keyId), eRunGroupAccount.getRgaId(), AppConstants.runStatusTutorRun, teamCase);
		lButton.setLabel(CDesktopComponents.vView().getLabel("preview_item.preview_as_tutor"));
		lButton.setDisabled((teamCase) && (lRunTeamIds.size() > 1));
		if (lRunTeamIds.size() == 1) {
			if (lRunTeam != null) {
				lButton.setRunTeam(lRunTeam);
			}
		}
		if (lShowRunTeamCombo) {
			CCdePreviewItemRunTeamCombo lRunTeamCombo = new CCdePreviewItemRunTeamCombo("",eRunGroup,lRunTeams,lButton);
			lListcell.appendChild(lRunTeamCombo);
			lListcell.appendChild(new Html("<br/>"));
		}
		lListcell.appendChild(lButton);
		lListitem.appendChild(lListcell);

		List<IERunGroupAccount> lRunGroupAccounts = runGroupAccountManager.getAllRunGroupAccountsByRugId(aItem.getRugId());
		boolean lDisabled = (lRunGroupAccounts.size() > 1);
		
		lListcell = new CDefListcell();
		CCdeRunGroupAccountTagStatusBtn lStatus = new CCdeRunGroupAccountTagStatusBtn("");
		lStatus.setLabel(CDesktopComponents.vView().getLabel("adm_rungroupaccounts.tagstatus"));
		lStatus.setDisabled(lDisabled);
		if (lRunGroupAccounts.size() == 1)
			lStatus.setRunGroupAccount((IERunGroupAccount)lRunGroupAccounts.get(0));
		lListcell.appendChild(lStatus);
		lListitem.appendChild(lListcell);

		lListcell = new CDefListcell();
		CCdeRunGroupAccountComponentStatusBtn lStatus2 = new CCdeRunGroupAccountComponentStatusBtn("");
		lStatus2.setLabel(CDesktopComponents.vView().getLabel("adm_rungroupaccounts.componentstatus"));
		lStatus2.setDisabled(lDisabled);
		if (lRunGroupAccounts.size() == 1)
			lStatus2.setRunGroupAccount((IERunGroupAccount)lRunGroupAccounts.get(0));
		lListcell.appendChild(lStatus2);
		lListitem.appendChild(lListcell);
		
		lListcell = new CDefListcell();
		CCdePreviewItemDeleteStatusBtn lButton2 = new CCdePreviewItemDeleteStatusBtn(eRunGroupAccount);
		lButton2.setLabel(CDesktopComponents.vView().getLabel("preview_item.delete_status"));
		lListcell.appendChild(lButton2);
		lListitem.appendChild(lListcell);
		super.insertListitem(aListbox,lListitem,aInsertBefore);
	}
}