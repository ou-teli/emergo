/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.IFileManager;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CInputListbox;
import nl.surf.emergo.domain.IECase;

/**
 * The Class CCdeChooseFileFileNamesLb.
 */
public class CCdeChooseFileFileNamesLb extends CInputListbox {

	private static final long serialVersionUID = -5954798374877238382L;

	public CCdeChooseFileFileNamesLb() {
		super();
	}

	/**
	 * On create fill combox with all streaming files of the current case and general streaming files, to choose from.
	 *
	 * @param aEvent the a event
	 */
	public void onCreate(CreateEvent aEvent) {
		String lItem = (String)((CCdeChooseFileWnd)getRoot()).getItem(aEvent);
		IFileManager fileManager = (IFileManager)CDesktopComponents.sSpring().getBean("fileManager");
		Listitem lListitem = super.insertListitem("",CDesktopComponents.vView().getLabel("none"),null);
		setSelectedItem(lListitem);
		// get files in streaming map for case
		IECase lCase = CDesktopComponents.sSpring().getCase();
		List<String> lItems = new ArrayList<String>();
		getCaseFiles(lCase, lItems);
		// get files in general streaming map
		lItems.addAll(fileManager.getFileNames(CDesktopComponents.sSpring().getAppManager().getAbsoluteStreamingPath()));

		for (String lObject : lItems) {
			lListitem = super.insertListitem(lObject,lObject,null);
			if ((lItem != null) && (lObject.equals(lItem)))
				setSelectedItem(lListitem);
		}
	}

	/**
	 * Get case files.
	 *
	 * @param aCase the a case
	 * @param aFiles the a files, is filled
	 *
	 * @return the case files
	 */
	protected void getCaseFiles(IECase aCase, List<String> aFiles) {
		// get files in streaming map for case
		List<String> lFiles = CDesktopComponents.sSpring().getFileManager().getFileNames(CDesktopComponents.sSpring().getAppManager().getAbsoluteStreamingPath() + aCase.getCasId() + "/");
		aFiles.addAll(lFiles);
		if (aCase.getCasCasId() != 0) {
			// if streaming map of other case is used
			IECase lCase = CDesktopComponents.sSpring().getCaseManager().getCase(aCase.getCasCasId());
			if (lCase != null) {
				// cascading
				getCaseFiles(lCase, aFiles);
			}
		}
	}

}
