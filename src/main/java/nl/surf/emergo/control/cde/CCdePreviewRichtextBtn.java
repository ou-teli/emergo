/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde;

import nl.surf.emergo.control.CContentHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CInputBtn;
import nl.surf.emergo.control.def.CDefCKeditor;
import nl.surf.emergo.view.VView;

/**
 * The Class CShowMediaBtn.
 *
 * Is used to preview uploaded media.
 */
public class CCdePreviewRichtextBtn extends CInputBtn {

	private static final long serialVersionUID = -3573548215819222030L;

	/** The content comp, the input element containing the uploaded content. */
	protected CDefCKeditor contentComp = null;

	/** The content helper. */
	protected CContentHelper contentHelper = null;

	/**
	 * Sets the content component.
	 *
	 * @param aContentComp the content component
	 */
	public void setContentComp(CDefCKeditor aContentComp) {
		contentComp = aContentComp;
	}

	/**
	 * Sets the content helper.
	 *
	 * @param aContentHelper the content helper
	 */
	public void setContentHelper(CContentHelper aContentHelper) {
		contentHelper = aContentHelper;
	}

	/**
	 * On click, after confirmation delete selected item and remove item from listbox.
	 */
	public void onClick() {
		// input element containing uploaded content
		if (contentComp == null)
			return;
		String lHtml = CDesktopComponents.sSpring().unescapeXMLWithoutReplace(contentComp.getValue());
		params.put("html", lHtml);
		showPopup(VView.v_cde_s_preview_richtext);
	}

}
