/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde;

import java.util.ArrayList;
import java.util.List;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.ICaseManager;
import nl.surf.emergo.control.CControlHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefButton;
import nl.surf.emergo.domain.IECase;

/**
 * The Class CCdeCaseOkBtn.
 */
public class CCdeCaseOkBtn extends CDefButton {

	private static final long serialVersionUID = -7040852390558058983L;

	/**
	 * On click create new item or update item or copy item if no errors and close edit window.
	 * If a new case is created the case case component is created too. And one 'student' case
	 * role too.
	 */
	public void onClick() {
		CCdeCaseWnd lWindow = (CCdeCaseWnd)getRoot();
		IECase lItem = (IECase)lWindow.getItem();
		String lAction = lWindow.getAction();
		ICaseManager caseManager = (ICaseManager)CDesktopComponents.sSpring().getBean("caseManager");
		IAccountManager accountManager = (IAccountManager)CDesktopComponents.sSpring().getBean("accountManager");
		boolean lNew = (lAction.equals("new"));
		boolean lCopy = (lAction.equals("copy"));
		IECase lOldItem = lItem;
		if (lItem == null) {
			//new
			lItem = caseManager.getNewCase();
			lItem.setEAccount(accountManager.getAccount(CDesktopComponents.cControl().getAccId()));
			lItem.setSkin("");
		}
		else
			//update
			lItem = (IECase)lItem.clone();
		//update
		lItem.setCode(CControlHelper.getTextboxValue(this, "code"));
		lItem.setName(CControlHelper.getTextboxValue(this, "name"));
		lItem.setVersion(CControlHelper.getTextboxValueAsInt(this, "version", 1));
		lItem.setSkin((String)CControlHelper.getListboxValue(this, "skin"));
		lItem.setMultilingual(CControlHelper.isCheckboxChecked(this, "multilingual"));
		IECase lStreamingCase = (IECase)CControlHelper.getListboxValue(this, "casCasId");
		if (lStreamingCase != null) {
			lItem.setCasCasId(lStreamingCase.getCasId());
		}
		else {
			lItem.setCasCasId(0);
		}

		List<String[]> lErrors = new ArrayList<String[]>(0);
		if (lNew || lCopy) {
			// set to 0 to get new record in db
			lItem.setCasId(0);
			lErrors = caseManager.newCase(lItem, (!lCopy));
		}
		else
			lErrors = caseManager.updateCase(lItem);
		if ((lErrors == null) || (lErrors.size() == 0)) {
//			Get case so date format, name etc. is correct.
			lItem = caseManager.getCase(lItem.getCasId());
			if (lCopy)
				lItem = CDesktopComponents.sSpring().copyCaseData(lOldItem,lItem);
			lWindow.setAttribute("item",lItem);
		}
		else {
			lWindow.setAttribute("item",null);
			CDesktopComponents.cControl().showErrors(this,lErrors);
		}
		lWindow.detach();
	}
}
