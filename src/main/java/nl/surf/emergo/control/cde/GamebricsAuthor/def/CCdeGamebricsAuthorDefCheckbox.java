/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde.GamebricsAuthor.def;

import java.util.HashMap;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;

import nl.surf.emergo.control.def.CDefCheckbox;

public class CCdeGamebricsAuthorDefCheckbox extends CDefCheckbox {

	private static final long serialVersionUID = 2604106633705916922L;

	protected Map<String,Object> keyValueMap = new HashMap<String,Object>(); 
	
	//NOTE is called if class is used within ZUL file
	public CCdeGamebricsAuthorDefCheckbox() {
		super();
	}

	//NOTE is called if class is used within Java code
	public CCdeGamebricsAuthorDefCheckbox(Component parent, String[] keys, Object[] values) {
		if (parent != null) {
			parent.appendChild(this);
		}

		CCdeGamebricsAuthorDefHelper.fillKeyValueMap(this, keys, values, keyValueMap);

		CCdeGamebricsAuthorDefHelper.decorateComponent(this, keyValueMap);
		String[] valueAndTooltipText = CCdeGamebricsAuthorDefHelper.getValueAndTooltipText(keyValueMap);
		setLabel(valueAndTooltipText[0]);
		setTooltiptext(valueAndTooltipText[1]);
		boolean checked = false;
		if (keyValueMap.containsKey("checked")) {
			checked = (boolean)keyValueMap.get("checked");
		}
		setChecked(checked);
	}

	//NOTE is called if class is used within ZUL file, so only labels are used and constructor CCdeGamebricsAuthorDefLabel(Component parent, String[] keys, Object[] values) is not called 
	public void onDecorate(Event aEvent) {
		super.onDecorate(aEvent);

		CCdeGamebricsAuthorDefHelper.fillKeyValueMap(this, keyValueMap);
	}

	/*
	public void onRightClick(MouseEvent aEvent) {
		CCdeGamebricsAuthorDefHelper.editContentOrLabel(aEvent.getTarget());
	}
	 */
}