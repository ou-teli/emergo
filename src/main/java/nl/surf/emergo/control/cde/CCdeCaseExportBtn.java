/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde;

import java.util.HashMap;

import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CImportExportHelper;
import nl.surf.emergo.control.CInputBtn;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.view.VView;

/**
 * The Class CCdeCaseExportBtn.
 */
public class CCdeCaseExportBtn extends CInputBtn {

	private static final long serialVersionUID = -4948591201634005492L;

	/**
	 * On click convert selected case to zip file and show hyper link to zip file in popup window.
	 * If popup window is closed, the zip file is deleted.
	 */
	public void onClick() {
		listbox = (Listbox)getFellowIfAny("casesLb");
		listitem = listbox.getSelectedItem();
		if (listitem == null)
			return;
		item = listitem.getValue();
		CImportExportHelper cHelper = new CImportExportHelper();
		String lUrl = cHelper.exportCase((IECase)item);
		if (lUrl.equals("")) {
			CDesktopComponents.vView().showMessagebox(getRoot(), CDesktopComponents.vView().getLabel("cde_s_case_export.error.body"), CDesktopComponents.vView().getLabel("cde_s_case_export.error.title"), Messagebox.OK, Messagebox.ERROR);
		}
		else {
			params = new HashMap<String,Object>();
			params.put("casename", ((IECase)item).getName());
			params.put("url", lUrl);
			showPopup(VView.v_cde_s_case_export);
		}
	}
	
}
