/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.List;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.ICaseComponentManager;
import nl.surf.emergo.business.IComponentManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CImportExportWnd;
import nl.surf.emergo.control.script.CScript;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IEComponent;
import nl.surf.emergo.zkspring.SSpring;

/**
 * The Class CCdeCaseComponentsContentExportWnd.
 */
public class CCdeCaseComponentsContentExportWnd extends CImportExportWnd {

	private static final long serialVersionUID = 1707815817153600728L;

	public SSpring sSpring = CDesktopComponents.sSpring();

	public CScript cScript = CDesktopComponents.cScript();

	public List<IECaseComponent> caseComponents = ((ICaseComponentManager)sSpring.getBean("caseComponentManager")).getAllCaseComponentsByCasId(Integer.parseInt((String)CDesktopComponents.cControl().getAccSessAttr("casId")));

	public List<String> componentCodes = new ArrayList<String>();
	
	public Hashtable<IECaseComponent,IXMLTag> hDataTrees = new Hashtable<IECaseComponent,IXMLTag>();
	
	public Hashtable<String,Hashtable<String,List<String>>> hTagNames = new Hashtable<String,Hashtable<String,List<String>>>();
	
	public Hashtable<String,Hashtable<String,List<String>>> hContentTypes = new Hashtable<String,Hashtable<String,List<String>>>();
	
	public Hashtable<String,Hashtable<String,List<String>>> hContentNames = new Hashtable<String,Hashtable<String,List<String>>>();
	
	public List<IEComponent> allComponents = ((IComponentManager)sSpring.getBean("componentManager")).getAllComponents();
	
	public List<IEComponent> components = new ArrayList<IEComponent>();

	public List<String> languageCodes = new ArrayList<String>();

	public CCdeCaseComponentsContentExportWnd() {
		super();
		boolean multilingual = false;
		if (caseComponents.size() > 0) {
			multilingual = caseComponents.get(0).getECase().getMultilingual();
		}
		for (IECaseComponent caseComponent : caseComponents) {
			String componentCode = caseComponent.getEComponent().getCode();
			String caseComponentName = caseComponent.getName();
			if (!componentCodes.contains(componentCode)) {
				componentCodes.add(componentCode);
			}
			IXMLTag xmlDataTree = sSpring.getXmlDataTree(caseComponent);
			hDataTrees.put(caseComponent, xmlDataTree);
			
			List<IXMLTag> allTags = cScript.getAllTags(xmlDataTree);
			for (IXMLTag tag : allTags) {
				String contentType = tag.getDefAttribute(AppConstants.defKeyType);
				boolean isChildTag = false;
				boolean isStatusTag = tag.getName().equals(AppConstants.statusElement);
				if (contentType.equals(AppConstants.defValueNode)) {
					//tag names
					String tagName = tag.getName();
					handleNameAndData(hTagNames, tagName, "caseComponentNames", caseComponentName);
				}
				else if (!contentType.equals("")) {
					//content types
					String tagName = tag.getParentTag().getName();
					handleNameAndData(hContentTypes, contentType, "caseComponentNames", caseComponentName);
					handleNameAndData(hContentTypes, contentType, "tagNames", tagName);
					//content names
					String contentName = tag.getName();
					handleNameAndData(hContentNames, contentName, "caseComponentNames", caseComponentName);
					handleNameAndData(hContentNames, contentName, "tagNames", tagName);
					handleNameAndData(hContentNames, contentName, "contentTypes", contentType);
					isChildTag = true;
				}
				if (multilingual && (isChildTag || isStatusTag)) {
					for (IXMLTag childTag : tag.getChildTags()) {
						if (childTag.getName().matches("(.?)(.?)_(.?)(.?)")) {
							String name = childTag.getName();
							if (!languageCodes.contains(name)) {
								languageCodes.add(name);
							}
						}
					}
				}
			}
		}
		
		if (languageCodes.size() > 0) {
			languageCodes.sort( Comparator.comparing( String::toString ) );
		}
		
		for (IEComponent component : allComponents) {
			if (componentCodes.contains(component.getCode())) {
				components.add(component);
			}
		}
	}
	
	public void onCreate(CreateEvent aEvent) {
		super.onCreate(aEvent);
		setTitle(CDesktopComponents.vView().getCLabel("export") + " " + CDesktopComponents.vView().getCLabel("cde_s_casecomponents_content_export"));
	}

	public static void handleNameAndData(Hashtable<String,Hashtable<String,List<String>>> hNames, String name, String dataKey, String dataValue) {
		Hashtable<String,List<String>> hData = null;
		if (!hNames.containsKey(name)) {
			hData = new Hashtable<String,List<String>>();
			hNames.put(name, hData);
		}
		else {
			hData = hNames.get(name);
		}
		if (!hData.containsKey(dataKey)) {
			hData.put(dataKey, new ArrayList<String>());
		}
		if (!hData.get(dataKey).contains(dataValue)) {
			hData.get(dataKey).add(dataValue);  
		}
	}
	
	public static Listitem addListitem(Listbox parent, String label) {
		Listitem listitem = new Listitem();
		Listcell listcell = new Listcell(label);
		listitem.appendChild(listcell);
		//NOTE insert listitem in such a way that listitems are ordered alphabetically
		int index = -1;
		for (int i=0;i<parent.getItemCount();i++) {
			if (label.compareTo(((Listcell)parent.getItemAtIndex(i).getChildren().get(0)).getLabel()) <= 0) {
				//The first listitem with a label 'greater' than parameter label
				index = i;
				break;
			}
		}
		if (index == -1) {
			//No listitem with a label 'greater' than parameter label found
			parent.appendChild(listitem);
		}
		else {
			parent.insertBefore(listitem, parent.getItemAtIndex(index));
		}
		parent.addItemToSelection(listitem);
		return listitem;
	}
	
}
