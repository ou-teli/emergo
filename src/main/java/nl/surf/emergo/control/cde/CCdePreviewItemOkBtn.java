/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.ICaseManager;
import nl.surf.emergo.business.IRunGroupAccountManager;
import nl.surf.emergo.business.IRunGroupManager;
import nl.surf.emergo.business.IRunManager;
import nl.surf.emergo.control.CControlHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefButton;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;

/**
 * The Class CCdePreviewItemOkBtn.
 */
public class CCdePreviewItemOkBtn extends CDefButton {

	private static final long serialVersionUID = 8758360852896859430L;

	/**
	 * On click create new item or update item or copy item if no errors and close edit window.
	 */
	public void onClick() {
		CCdePreviewItemWnd lWindow = (CCdePreviewItemWnd)getRoot();
		IERunGroup lItem = (IERunGroup)lWindow.getItem();
		IERunGroupAccount lItem2 = null;
		String lAction = lWindow.getAction();
		IRunGroupManager runGroupManager = (IRunGroupManager)CDesktopComponents.sSpring().getBean("runGroupManager");
		IRunGroupAccountManager runGroupAccountManager = (IRunGroupAccountManager)CDesktopComponents.sSpring().getBean("runGroupAccountManager");
		boolean lNew = (lAction.equals("new"));
		boolean lCopy = (lAction.equals("copy"));
		IERunGroup lOldItem = lItem;
		int lCasId = Integer.parseInt((String) CDesktopComponents.cControl().getAccSessAttr("casId"));
		IECase eCase = ((ICaseManager) CDesktopComponents.sSpring().getBean("caseManager")).getCase(lCasId);
		if (lItem == null) {
			//new
			Listbox lListbox = (Listbox)CControlHelper.getInputComponent(this, "carCarId");
			Listitem lSelectedItem = lListbox.getSelectedItem();
			if (lSelectedItem == null) {
				lWindow.setAttribute("item",null);
				lWindow.detach();
				return;
			}
			lItem = runGroupManager.getNewRunGroup();
			lItem.setECaseRole((IECaseRole)lSelectedItem.getValue());
		}
		else {
			//update
			lItem = (IERunGroup)lItem.clone();
		}
		if (lNew || lCopy) { 
			IEAccount eAccount = CDesktopComponents.sSpring().getAccount();
			IERun eRun = ((IRunManager)CDesktopComponents.sSpring().getBean("runManager")).getTestRun(eAccount, eCase);
			lItem.setERun(eRun);
			lItem2 = runGroupAccountManager.getNewRunGroupAccount();
			lItem2.setEAccount(eAccount);
			lItem2.setERunGroup(lItem);
		}
		lItem.setName(CControlHelper.getTextboxValue(this, "name"));

		List<String[]> lErrors = new ArrayList<String[]>(0);
		if (lNew || lCopy) { 
			// set to 0 to get new record in db
			lItem.setRugId(0);
			lErrors = runGroupManager.newRunGroup(lItem);
		}
		else {
			lErrors = runGroupManager.updateRunGroup(lItem);
		}
		if (lErrors == null || lErrors.size() == 0) {
//			Get rungroup so date format, name etc. is correct.
			lItem = runGroupManager.getRunGroup(lItem.getRugId());
			if (lNew || lCopy) { 
				// set to 0 to get new record in db
				lItem2.setRgaId(0);
				runGroupAccountManager.newRunGroupAccount(lItem2);
//				Get rungroupaccount so date format, name etc. is correct.
				lItem2 = runGroupAccountManager.getRunGroupAccount(lItem2.getRgaId());
			}
			if (lCopy) {
				lItem = CDesktopComponents.sSpring().copyRunGroupCaseComponents(lOldItem,lItem,eCase);
			}
			lWindow.setAttribute("item",lItem);
		}
		else {
			lWindow.setAttribute("item",null);
			CDesktopComponents.cControl().showErrors(this,lErrors);
		}

		lWindow.detach();
	}

}
