/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zk.ui.event.CreateEvent;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CContentHelper;
import nl.surf.emergo.control.CDesktopComponents;

/**
 * The Class CCdeScriptMethodWnd.
 * 
 * Is used to show a modal popup dialog for creating/editing a script method.
 * A method can be either a part of a script condition or a script action.
 * A condition can be built with logical operators and methods. An action is
 * equal to a method.
 */
public class CCdeScriptMethodWnd extends CCdeComponentItemWnd {

	private static final long serialVersionUID = -5848782134440849417L;

	/**
	 * On create, get arguments given to window and save them.
	 * Show correct window title. Render input form.
	 * 
	 * @param aEvent the create event
	 */
	public void onCreate(CreateEvent aEvent) {
		handleAccess();
		// get arguments out of create event and save them
		setItem((IXMLTag) (aEvent.getArg()).get("item"));
		String lNew = (String)(aEvent.getArg()).get("new");
		setNewitem((lNew != null) && (lNew.equals("true")));
		setNodename((String) (aEvent.getArg()).get("nodename"));
		setSubtype((String) (aEvent.getArg()).get("subtype"));
		setSubject((String) (aEvent.getArg()).get("subject"));
		Boolean lIsMainCondition = (Boolean)(aEvent.getArg()).get("ismaincondition");
		if (lIsMainCondition == null)
			lIsMainCondition = false;
		
		if (StringUtils.isEmpty(getTitle())) {
			// show correct window title
			String lAction = "";
			if (getNewitem()) {
				lAction = CDesktopComponents.vView().getCLabel("new");
			}
			else {
				lAction = CDesktopComponents.vView().getCLabel("edit");
			}
			String lSubject = getSubject();
			if (lSubject.equals("")) {
				lSubject = CDesktopComponents.vView().getLabel("cde_s_script." + getNodename());
			}
			setTitle(CDesktopComponents.vView().getLabel("title.template").replace("%1", lAction).replace("%2", lSubject));
		}

		CContentHelper cContentHelper = new CContentHelper(null);

		// set correct window size
		String lSclass = "";
		if (CContentHelper.isMethod(getNodename())) {
			lSclass = "CCdeScriptMethodWnd_method";
		}
		else if (CContentHelper.isTemplateMethod(getNodename())) {
			lSclass = "CCdeScriptMethodWnd_templatemethod";
		}
		setSclass(lSclass);
		
		// render method input form within ZK hbox object on screen
		cContentHelper.xmlNodeToMethodHbox(getItem(), null, getNodename(), getSubtype(), getFellowIfAny("scriptMethodHbox"), true, lIsMainCondition);
	}
}
