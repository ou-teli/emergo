/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde;

import java.util.List;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CContentHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefButton;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.zkspring.SSpring;

/**
 * The Class CCdeComponentRichtextToPlaintextBtn.
 */
public class CCdeComponentRichtextToPlaintextBtn extends CDefButton {

	private static final long serialVersionUID = 8329492967384332558L;

	public CCdeComponentRichtextToPlaintextBtn() {
		super();
	}

	public void onClick() {
		IECaseComponent lCaseComponent = (IECaseComponent) CDesktopComponents.cControl().getAccSessAttr("casecomponent");
		SSpring sSpring = CDesktopComponents.sSpring();
		IXMLTag lRootTag = sSpring.getXmlDataTree(lCaseComponent);
		List<IXMLTag> lTags = CDesktopComponents.cScript().getAllTags(lRootTag);
		CContentHelper cHelper = new CContentHelper(null);
		boolean lUpdate = false;
		for (IXMLTag lTag : lTags) {
			String lType = lTag.getDefAttribute("type");
			if (lType.equals("richtext") || lType.equals("simplerichtext")) {
				String lOldValue = lTag.getValue();
				lTag.setValue(sSpring.getXmlManager().escapeXML(cHelper.stripHtmlFromCKeditorString(
						cHelper.stripCKeditorString(sSpring.unescapeXML(lTag.getValue())))));
				if (!lTag.getValue().equals(lOldValue)) {
					lUpdate = true;
				}
			}
		}
		if (lUpdate) {
			String lXmlData = sSpring.getXmlManager().xmlTreeToDoc(lRootTag);
			sSpring.setCaseComponentData("" + lCaseComponent.getCacId(), lXmlData);
			lCaseComponent = sSpring.getCaseComponent(lCaseComponent.getCacId());
			CDesktopComponents.cControl().setAccSessAttr("casecomponent", lCaseComponent);
			CDesktopComponents.vView().reloadView();
		}
	}

}
