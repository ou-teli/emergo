/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde.GamebricsAuthor.rubrics;

import org.zkforge.ckez.CKeditor;
import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.util.Clients;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.cde.GamebricsAuthor.CCdeGamebricsAuthorDiv;
import nl.surf.emergo.domain.IECaseComponent;

public class CCdeGamebricsAuthorUpdateRubricDiv extends CCdeGamebricsAuthorUpdateRubricSkillDiv {

	private static final long serialVersionUID = -4061130195403736736L;

	public void update() {
		if (_idMap != null) {
			//NOTE new rubric is always stored in XML data and status that is cached within the SSpring class
			IXMLTag nodeTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get(_cacIdStr)), _idMap.get(_tagIdStr));
			if (nodeTag != null) {
				initSkill(nodeTag);
				initSkillElements(nodeTag, false, true, false);
				
				Clients.evalJavaScript("initUpdateRubric('" + skill.toString() + "','" + skillElements.toString() + "','" +
						getColorsAsJsonStr(skillClusterColors) + "','" +
						getColorsAsJsonStr(performanceLevelSkillClusterColors) + "');");
			}
		}
	}
		
	public void onNotify (Event event) {
		JSONObject jsonObject = (JSONObject)event.getData();
		String action = (String)jsonObject.get("action");
		if (action.equals("update_rubric")) {
			setVisible(false);
			CCdeGamebricsAuthorDiv div = (CCdeGamebricsAuthorDiv)CDesktopComponents.vView().getComponent("gamebricsAuthorNewRubricDiv");
			if (div != null) {
				div.init(true, _idMap);
			}
		}
		else if (action.equals("show_richtext")) {
			String richtextFieldZkId = (String)jsonObject.get("richtextFieldZkId");
			String inputText = (String)jsonObject.get("inputText");
			CKeditor fCKeditor = (CKeditor)vView.getComponent(richtextFieldZkId);
			if (fCKeditor != null) {
				//NOTE create clone of fck editor, otherwise if inputText is empty, value shown might be value entered for other element.
				CKeditor newFCKeditor = (CKeditor)fCKeditor.clone();
				Component parent = fCKeditor.getParent();
				fCKeditor.detach();
				parent.appendChild(newFCKeditor);

				newFCKeditor.setValue(inputText);
				newFCKeditor.setCustomConfigurationsPath(vView.getLabel("cde_gamebricsauthor.ckeditor.custom.config.file"));
				
				//NOTE show div after value is set, otherwise previous value may be shown briefly
				Clients.evalJavaScript("showPromptRichtextPopupDiv();");
			}
		}
		else if (action.equals("save_richtext")) {
			String tagId = (String)jsonObject.get(_tagIdStr);
			String tagName = (String)jsonObject.get("tagName");
			String richtextFieldZkId = (String)jsonObject.get("richtextFieldZkId");
			String descriptionFieldId = (String)jsonObject.get("descriptionFieldId");
			String description = ((CKeditor)vView.getComponent(richtextFieldZkId)).getValue();
			IXMLTag elementTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get(_cacIdStr)), tagId);
			if (elementTag != null) {
				editElementTagChildValue(elementTag, gaRubricElementType, "description", description);
			}
		
			Clients.evalJavaScript("updateRichtext('" + tagName + "','" + descriptionFieldId + "','" + sSpring.escapeXMLAlt(description) + "');");
		}
		else {
			super.onNotify(event);
		}
	}
	
}
