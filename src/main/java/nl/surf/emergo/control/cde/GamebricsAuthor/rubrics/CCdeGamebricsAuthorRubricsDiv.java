/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde.GamebricsAuthor.rubrics;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.event.CreateEvent;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.cde.GamebricsAuthor.CCdeGamebricsAuthorDiv;
import nl.surf.emergo.domain.IECaseComponent;

public class CCdeGamebricsAuthorRubricsDiv extends CCdeGamebricsAuthorDiv {

	public static final String gaRubricsComponentCode = "PV_rubrics";
	public static final String gaRubricsDefaultComponentName = "GB_rubrics";
	public static final String gaRubricElementType = "rubric";
	public static final String gaRubricPerformancelevelTagName = "performancelevel";
	public static final String gaRubricSkillTagName = "skill";
	public static final String gaRubricSkillclusterTagName = "skillcluster";
	public static final String gaRubricSubskillTagName = "subskill";
	public static final String gaRubricPerformancelevelexampleTagName = "videoperformancelevelexample";
	/**
	 * Colors used for different skill clusters in inner segments of the skill
	 * wheel. When more than ten skill clusters, colors are repeated.
	 */
	public static final String[] performanceLevelSkillClusterColors = new String[] { "#FFD6C6", "#AAE5DF", "#F2E09A",
			"#A7C9EB", "#E7C6FF", "#CFE888", "#E4C4AF", "#A8EBF5", "#FFB8B8", "#97F3BC" };
	/**
	 * Colors used for different skill clusters in outer segments of the skill
	 * wheel. When more than ten skill clusters, colors are repeated.
	 */
	public static final String[] skillClusterColors = new String[] { "#FFA888", "#47CCBE", "#E2CC5C", "#699FD5",
			"#B37ADE", "#A5CF2D", "#CE9C7B", "#66D7E9", "#FF7878", "#62E295" };
	
	private static final long serialVersionUID = -2989150181145019005L;

	public void onCreate(CreateEvent aEvent) {
		super.onCreate(aEvent);
	}
	
	@Override
	public void update() {
		updateContent(getComponentCode(gaRubricElementType), gaRubricElementType, showDuplicatesInList());
	}
	
	@Override
	protected boolean showDuplicatesInList() {
		return false;
	}
	
	@Override
	protected String getRootElementTypes() {
		return gaRubricElementType;
	}

	@Override
	protected String getComponentCode(String pElementType) {
		return gaRubricsComponentCode;
	}

	@Override
	protected String getDefaultComponentName(String pElementType) {
		return gaRubricsDefaultComponentName;
	}
		
	@Override
	protected String getUniqueTagName(String elementType) {
		//NOTE, for rubric: use 'name', make 'pid' equal to name, 'name' must be unique; no option to add 'pid' as well as 'name'
		return "name";
	}

	@Override
	protected String getUpdateElementForbiddenLabel() {
		return "cde_gamebricsauthor.updaterubric.message.component.forbidden";
	}
	
	@Override
	protected String getDeleteElementForbiddenLabel() {
		return "cde_gamebricsauthor.deleterubric.message.component.forbidden";
	}
	
	@Override
	protected List<IECaseComponent> getCaseComponents(String pvComponentCode) {
		return getAllCaseComponents(pvComponentCode);
	}
	
	@Override
	protected boolean isInputOk(String pElementType, IXMLTag tag) {
		if (pElementType.equals(getRootElementTypes())) {
			//check for rubric tag if number of performance levels is set and if it has skill cluster or sub skill children
			if (tag == null || tag.getChildValue(getUniqueTagName("")).equals("") || tag.getChildValue("numberofperformancelevels").equals("")) {
				//name is empty or number of performance levels is not set
				return false;
			}
			IXMLTag skillTag = tag.getChild(gaRubricSkillTagName);
			if (skillTag == null || (skillTag.getChilds(gaRubricSkillclusterTagName).isEmpty() && skillTag.getChilds(gaRubricSubskillTagName).isEmpty())) {
				//it has no skill child or the skill child has no skill cluster or sub skill children
				return false;
			}
			return true;
		}
		return super.isInputOk(pElementType, tag);
	}
	
	protected void addPerformancelevelTags(IXMLTag parentTag) {
		IXMLTag rubricTag = parentTag.getParentTag();
		while (rubricTag != null && !rubricTag.getName().equals(getRootElementTypes())) {
			rubricTag = rubricTag.getParentTag();
		}
		if (rubricTag != null) {
			int numberOfPerformanceLevels = 5;
			try {
				numberOfPerformanceLevels = Integer.parseInt(rubricTag.getChildValue("numberofperformancelevels"));
			} catch (NumberFormatException e) {
			}
			for (int i=numberOfPerformanceLevels;i>=1;i--) {
				Map<String,String> childTagNameValueMap = new HashMap<String,String>();
				childTagNameValueMap.put("pid", "" + i);
				childTagNameValueMap.put("level", "" + i);
				addNodeTag(parentTag, gaRubricPerformancelevelTagName, childTagNameValueMap);
			}
		}
	}

	@Override
	public IXMLTag addRootElementTag(String elementType, String tagName, Map<String,String> childTagNameValueMap) {
		IECaseComponent caseComponent = getCaseComponent(elementType);
		IXMLTag elementTag = null;
		if (isRootElementType(elementType)) {
			IXMLTag dataRootTag = sSpring.getXmlDataTree(caseComponent);
			elementTag = addNodeTag(dataRootTag.getChild(AppConstants.contentElement), tagName, childTagNameValueMap);
			//NOTE already add skill tag - with same title as rubric
			IXMLTag skillTag = addNodeTag(elementTag, gaRubricSkillTagName, childTagNameValueMap);
			if (skillTag != null) {
				//NOTE already add performance level tags
				addPerformancelevelTags(skillTag);
			}
			sSpring.setCaseComponentData("" + caseComponent.getCacId(), sSpring.getXmlManager().xmlTreeToDoc(dataRootTag));
		}
		return elementTag;
	}
	
	@Override
	protected IXMLTag editNodeTag(IXMLTag rootTag, IXMLTag elementTag, String elementName) {
		elementTag = sSpring.getXmlManager().getTagById(rootTag, elementTag.getAttribute(AppConstants.statusKeyId));
		elementName = sSpring.escapeXML(elementName);
		elementTag.setChildValue("pid", elementName);
		elementTag.setChildValue("name", elementName);
		return elementTag;
	}
	
}
