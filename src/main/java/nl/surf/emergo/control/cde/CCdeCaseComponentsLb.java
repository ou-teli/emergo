/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zul.Button;

import nl.surf.emergo.business.ICaseComponentManager;
import nl.surf.emergo.business.ICaseComponentRoleManager;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CControlHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefListbox;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CCdeCaseComponentsLb.
 */
public class CCdeCaseComponentsLb extends CDefListbox {

	private static final long serialVersionUID = -7121707881859144035L;

	/**
	 * On create render listitems. Only functional case components because case developer view.
	 * New button is only shown for case author.
	 */
	public void onCreate() {
		getItems().clear();
		CControl cControl = CDesktopComponents.cControl();
		List<IECaseComponent> lItems = ((ICaseComponentManager)CDesktopComponents.sSpring().getBean("caseComponentManager")).getAllCaseComponentsByCasId(Integer.parseInt((String)cControl.getAccSessAttr("casId")));
		CCdeCaseComponentsHelper cHelper = new CCdeCaseComponentsHelper();
		ICaseComponentRoleManager caseComponentRoleManager = (ICaseComponentRoleManager)CDesktopComponents.sSpring().getBean("caseComponentRoleManager");
		List<Integer> lCacIds = new ArrayList<Integer>();
		for (IECaseComponent lCaseComponent : lItems) {
			if (lCaseComponent.getEComponent().getType() == 1)
//				only functional components
				lCacIds.add(lCaseComponent.getCacId());
		}
		cHelper.setCaseComponentRoles(caseComponentRoleManager.getAllCaseComponentRolesByCacIds(lCacIds));
		for (IECaseComponent lCaseComponent : lItems) {
			if (lCaseComponent.getEComponent().getType() == 1)
//				only show functional components
				cHelper.renderItem(this,null,lCaseComponent);
		}
		restoreSettings();
	}

	public void onSelect() {
		if (CControlHelper.isCaseAuthor()) {
			//Following buttons are only enabled for a case author.
			((Button)getFellow("editBtn")).setDisabled(false);
			((Button)getFellow("copyBtn")).setDisabled(false);
			((Button)getFellow("deleteBtn")).setDisabled(false);
			((Button)getFellow("importsContentBtn")).setDisabled(false);
		}
		((Button)getFellow("exportBtn")).setDisabled(false);
	}

}
