/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

import nl.surf.emergo.business.IAppManager;
import nl.surf.emergo.business.ICaseComponentManager;
import nl.surf.emergo.business.ICaseManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControlHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CImportExportHelper;
import nl.surf.emergo.control.def.CDefButton;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IEComponent;
import nl.surf.emergo.utilities.FileHelper;

/**
 * The Class CCdeCaseComponentsImportOkBtn.
 */
public class CCdeCaseComponentsImportOkBtn extends CDefButton {

	private static final long serialVersionUID = -3907924389269456598L;

	/**
	 * On click import case components using uploaded file if no errors and close edit window.
	 */
	public void onClick() {
		Component lWindow = getRoot();
		IAppManager appManager = (IAppManager)CDesktopComponents.sSpring().getBean("appManager");
		ICaseManager caseManager = (ICaseManager)CDesktopComponents.sSpring().getBean("caseManager");
		ICaseComponentManager caseComponentManager = (ICaseComponentManager)CDesktopComponents.sSpring().getBean("caseComponentManager");

		int lCasId = Integer.parseInt((String)CDesktopComponents.cControl().getAccSessAttr("casId"));

		IECase lCase = caseManager.getCase(lCasId);
		IEAccount lAccount = (IEAccount)CControlHelper.getListboxValue(this, "accAccId");

		List<String[]> lErrors = new ArrayList<String[]>(0);
		Textbox lTextbox = (Textbox)CControlHelper.getInputComponent(this, "filename");
		String lFileName = lTextbox.getValue();
		if (lFileName.equals("")) {
			appManager.addError(lErrors, "file", "error_no_choice");
		}

		if ((lErrors == null) || (lErrors.size() == 0)) {
			CCdeCaseComponentHelper caseComponentHelper = new CCdeCaseComponentHelper();
			// NOTE get possible components, some components are not multiple, so import is not possible
			// Do this before import of case component!
			List<IEComponent> lPossibleComponents = caseComponentHelper.getPossibleComponents(lCasId, true);
			List<IECaseComponent> lCurrentCaseComponents = caseComponentManager.getAllCaseComponentsByCasId(lCase.getCasId());

			// Import case component
			CImportExportHelper cHelper = new CImportExportHelper();
			Media lMedia = (Media)lTextbox.getAttribute(AppConstants.contentElement);
			if (lMedia == null)
				appManager.addError(lErrors, "file", "not_found");
			else {
				List<String> lMVErrors = new ArrayList<>();
				if (FileHelper.isMediaValid(lMedia, lMVErrors)) {
					byte[] lContent = CDesktopComponents.sSpring().getSBlobHelper().mediaToByteArray(lMedia);
					List<IECaseComponent> lCaseComponents = cHelper.importCaseComponents(lAccount, lCase, lFileName, lContent);
					
					if (lCaseComponents.size() > 0) {
						
						String lCurrentCaseComponentNames = "@@@";
						for (IECaseComponent lCurrentCaseComponent : lCurrentCaseComponents) {
							lCurrentCaseComponentNames += lCurrentCaseComponent.getName() + "@@@";
						}
						// NOTE if not allowed remove case components. Can only be done after import
						List<IECaseComponent> lNotAllowedCaseComponents = new ArrayList<IECaseComponent>();
						List<IECaseComponent> lRenamedCaseComponents = new ArrayList<IECaseComponent>();
						for (IECaseComponent lCaseComponent : lCaseComponents) {
							boolean lCaseComponentNotAllowed = true;
							for (IEComponent lComponent : lPossibleComponents) {
								if (lCaseComponent.getEComponent().getCode().equals(lComponent.getCode())) {
									lCaseComponentNotAllowed = false;
								}
							}
							if (lCaseComponentNotAllowed) {
								lNotAllowedCaseComponents.add(lCaseComponent);
							}
							for (IECaseComponent lCurrentCaseComponent : lCurrentCaseComponents) {
								if (lCurrentCaseComponent.getName().equals(lCaseComponent.getName())) {
									int lCounter = 1;
									String lAltName = lCaseComponent.getName() + " (copy " + lCounter + ")";
									while (lCurrentCaseComponentNames.indexOf("@@@" + lAltName + "@@@") >= 0) {
										lCounter = lCounter + 1;
										lAltName = lCaseComponent.getName() + " (copy " + lCounter + ")";
									}
									lCaseComponent.setName(lAltName);
									lRenamedCaseComponents.add(lCaseComponent);
								}
							}
						}
						if (lNotAllowedCaseComponents.size() > 0) {
							String lMessage = CDesktopComponents.vView().getLabel("cde_s_casecomponents_import.error.notallowed.body");
							for (IECaseComponent lNotAllowedCaseComponent : lNotAllowedCaseComponents) {
								lMessage += "\n" + lNotAllowedCaseComponent.getName();
							}
							//rollback
							for (int i=(lCaseComponents.size()-1);i>=0;i--) {
								caseComponentManager.deleteCaseComponent(lCaseComponents.get(i));
							}
							CDesktopComponents.vView().showMessagebox(getRoot(), lMessage, CDesktopComponents.vView().getLabel("cde_s_casecomponents_import.error.title"), Messagebox.OK, Messagebox.ERROR);
							lWindow.setAttribute("item",null);
						}
						else {
							for (IECaseComponent lCaseComponent : lCaseComponents) {
								caseComponentManager.saveCaseComponent(lCaseComponent);
								caseComponentHelper.handleCaseComponentRoles(lCasId, lCaseComponent, this);
								//NOTE add case component to all case roles
								if (lCaseComponent.getEComponent().getCode().equals("scripts")) {
									//NOTE update reference ids for template conditions and actions
									caseComponentHelper.updateTemplateReferenceIds(lCaseComponent);
								}
							}
							lWindow.setAttribute("item",lCaseComponents);
							if (lRenamedCaseComponents.size() > 0) {
								String lMessage = CDesktopComponents.vView().getLabel("cde_s_casecomponents_import.information.morethanone.body");
								for (IECaseComponent lRenamedCaseComponent : lRenamedCaseComponents) {
									lMessage += "\n" + lRenamedCaseComponent.getName();
								}
								CDesktopComponents.vView().showMessagebox(getRoot(), lMessage, CDesktopComponents.vView().getLabel("cde_s_casecomponents_import.information.title"), Messagebox.OK, Messagebox.INFORMATION);
							}
						}
					}
					else {
						CDesktopComponents.vView().showMessagebox(getRoot(), CDesktopComponents.vView().getLabel("cde_s_casecomponents_import.error.body"), CDesktopComponents.vView().getLabel("cde_s_casecomponents_import.error.title"), Messagebox.OK, Messagebox.ERROR);
						lWindow.setAttribute("item",null);
					}
					
				} else {
					FileHelper.showUploadErrorMessagebox(lMedia, lMVErrors);
					lWindow.setAttribute("item", null);
				}
			}
		}
		if (!((lErrors == null) || (lErrors.size() == 0))) {
			lWindow.setAttribute("item",null);
			CDesktopComponents.cControl().showErrors(this,lErrors);
		}
		lWindow.detach();
	}

}
