/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.COpenWebPageUsingJavascriptBtn;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunTeam;
import nl.surf.emergo.view.VView;

/**
 * The Class CCdePreviewItemPreviewBtn. Used to show preview buttons which open
 * the Emergo player within a new browser instance for a particular preview run
 * group account, possibly within in a particular preview run team.
 */
public class CCdePreviewItemPreviewBtn extends COpenWebPageUsingJavascriptBtn {

	private static final long serialVersionUID = 2836981889398340298L;

	/** The cac id. */
	protected String cacId = "";

	/** The tag id. */
	protected String tagId = "";

	/** The rga id. */
	protected String rgaId = "";

	/** The run status. */
	protected String runStatus = "";

	/** The team case. */
	protected boolean teamCase = false;

	/** The rungroup. */
	protected IERunGroup rungroup = null;

	/** The runteam. */
	protected IERunTeam runteam = null;
	
	/**
	 * Instantiates a new c cde preview item preview btn and creates appropriate client action.
	 *
	 * @param aId the a id
	 * @param aRunGroup the a run group
	 * @param aCacId the a cac id
	 * @param aTagId the a tag id
	 * @param aRgaId the a rga id
	 * @param aRunStatus the a run status
	 * @param aTeamCase the a team case
	 */
	public CCdePreviewItemPreviewBtn(String aId, IERunGroup aRunGroup, int aCacId, String aTagId, int aRgaId, String aRunStatus, boolean aTeamCase) {
		if (!aId.equals(""))
			setId(aId);
		cacId = ""+aCacId;
		rungroup = aRunGroup;
		tagId = aTagId;
		rgaId = ""+aRgaId;
		runStatus = aRunStatus;
		teamCase = aTeamCase;
		createAction();
	}

	/**
	 * Sets the run team and creates the appropriate client action.
	 *
	 * @param aRunTeam the new run team
	 */
	public void setRunTeam(IERunTeam aRunTeam) {
		runteam = aRunTeam;
		createAction();
	}

	/**
	 * Creates client action to open Emergo player in new browser instance for
	 * a preview run group account, possibly within a run team.
	 */
	private void createAction() {
		String lRutId = "";
		if (runteam != null)
			lRutId = "" + runteam.getRutId();
		boolean lShouldBeInTeam = false;
		if (runteam == null) {
			// check if user should be in team
			lShouldBeInTeam = teamCase;
		}
		String lParams =
			"&cacId="+cacId+
			"&tagId="+tagId+
			"&runstatus="+runStatus+
			"&rgaId="+rgaId+
			"&rutId="+lRutId+
			CDesktopComponents.cControl().getReqLangParams();

		if (lShouldBeInTeam) {
			javascriptOnClickAction = "alert('" + CDesktopComponents.vView().getLabel("preview_item.alert.notinteam") + "');";
		}
		else {
			String lUrl = CDesktopComponents.vView().uniqueView(CDesktopComponents.sSpring().getSCaseSkinHelper().getCaseSkinPath(rungroup) + VView.v_run);
			if (!CDesktopComponents.vView().isAbsoluteUrl(lUrl)) {
				lUrl = CDesktopComponents.vView().getEmergoWebappsRoot() + "/" + lUrl;
			}
			javascriptOnClickAction = CDesktopComponents.vView().getJavascriptWindowOpenFullscreen(lUrl + lParams);
		}
	}

}
