/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde.GamebricsAuthor.rubrics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.json.JSONObject;
import org.zkoss.lang.Strings;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.util.Clients;


import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.cde.GamebricsAuthor.CCdeGamebricsAuthorDiv;
import nl.surf.emergo.domain.IECaseComponent;

public class CCdeGamebricsAuthorUpdateRubricSkillDiv extends CCdeGamebricsAuthorNewRubricDiv {

	private static final long serialVersionUID = -4522501640293840196L;
	
	protected StringBuilder skillElements;

	public void update() {
		if (_idMap != null) {
			//NOTE rubric is always stored in XML data and status that is cached within the SSpring class, regardless if preview is read only or not.
			IXMLTag nodeTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get(_cacIdStr)), _idMap.get(_tagIdStr));
			if (nodeTag != null) {
				initSkillElements(nodeTag, true, false, true);
				
				Clients.evalJavaScript("initUpdateRubricSkill('" + skillElements.toString() + "','" + 
						getColorsAsJsonStr(skillClusterColors) + "','" +
						getColorsAsJsonStr(performanceLevelSkillClusterColors) + "');");
			}
		}
	}
		
	protected void initSkillElements(IXMLTag rubricTag, boolean addSkillElement, boolean addChildNodeTags, boolean addAddSkillElements) {
		skillElements = new StringBuilder();
		IXMLTag skillTag = rubricTag.getChild(gaRubricSkillTagName);
		if (addSkillElement) {
			addSkillElement(skillTag, addChildNodeTags);
		}
		//NOTE skill tag may have skill cluster tags
		for (IXMLTag skillclusterTag : skillTag.getChilds(gaRubricSkillclusterTagName)) {
			addSkillElement(skillclusterTag, addChildNodeTags);
			//NOTE skill cluster tag may have sub skill tags
			for (IXMLTag subskillTag : skillclusterTag.getChilds(gaRubricSubskillTagName)) {
				addSkillElement(subskillTag, addChildNodeTags);
			}
			if (addAddSkillElements) {
				//NOTE sub skill tag may be added to skill cluster tag
				addAddSkillElement(skillclusterTag, gaRubricSubskillTagName);
			}
		}
		if (addAddSkillElements) {
			//NOTE skill cluster tag may be added to skill tag
			addAddSkillElement(skillTag, gaRubricSkillclusterTagName);
		}
		//NOTE skill tag may have sub skill tags
		for (IXMLTag subskillTag : skillTag.getChilds(gaRubricSubskillTagName)) {
			addSkillElement(subskillTag, addChildNodeTags);
		}
		//NOTE sub skill tag may be added to skill tag
		if (addAddSkillElements) {
			addAddSkillElement(skillTag, gaRubricSubskillTagName);
		}
		skillElements.insert(0, "[");
		skillElements.append("]");
	}
		
	protected void addSkillElement(IXMLTag nodeTag, boolean addChildNodeTags) {
		addSkillElement(nodeTag.getParentTag(), nodeTag, "", addChildNodeTags);
	}

	protected void addAddSkillElement(IXMLTag parentTag, String tagName) {
		addSkillElement(parentTag, null, tagName, false);
	}

	protected void addSkillElement(IXMLTag parentTag, IXMLTag nodeTag, String tagName, boolean addChildNodeTags) {
		StringBuilder element = new StringBuilder();
		addElementPart(element, "parentTagId", parentTag.getAttribute(AppConstants.keyId), true);
		addElementPart(element, "parentTagName", parentTag.getName(), true);
		addElementPart(element, _tagIdStr, nodeTag != null ? nodeTag.getAttribute(AppConstants.keyId) : "", true);
		addElementPart(element, "tagName", nodeTag != null ? nodeTag.getName() : tagName, true);
		addElementPart(element, "name", nodeTag != null ? sSpring.unescapeXML(nodeTag.getChildValue("name")) : "", true);
		if ((nodeTag != null) && gaRubricSubskillTagName.equals(nodeTag.getName())) {
			String lWeighting = sSpring.unescapeXML(nodeTag.getChildValue("weighting"));
			addElementPart(element, "weighting", Strings.isEmpty(lWeighting) ? "1" : lWeighting, true);
		}
		addElementPart(element, "description", nodeTag != null ? sSpring.unescapeXML(nodeTag.getChildValue("description")).replace("\n", "") : "", addChildNodeTags);
		if (addChildNodeTags) {
			StringBuilder performancelevelElements = new StringBuilder();
			for (IXMLTag performancelevelTag : nodeTag.getChilds(gaRubricPerformancelevelTagName)) {
				addPerformanceLevel(nodeTag, performancelevelTag, addChildNodeTags, performancelevelElements);
			}			
			performancelevelElements.insert(0, "[");
			performancelevelElements.append("]");
			addElementPart(element, "performanceLevels", performancelevelElements, false);
		}
		if (skillElements.length() > 0) {
			skillElements.append(",");
		}
		skillElements.append("{");
		skillElements.append(element);
		skillElements.append("}");
	}

	protected void addPerformanceLevel(IXMLTag parentTag, IXMLTag nodeTag, boolean addChildNodeTags, StringBuilder performancelevelElements) {
		StringBuilder element = new StringBuilder();
		addElementPart(element, "parentTagId", parentTag.getAttribute(AppConstants.keyId), true);
		addElementPart(element, _tagIdStr, nodeTag.getAttribute(AppConstants.keyId), true);
		addElementPart(element, "tagName", nodeTag.getName(), true);
		addElementPart(element, "level", nodeTag.getChildValue("level"), true);
		addElementPart(element, "description", sSpring.unescapeXML(nodeTag.getChildValue("description")).replace("\n", ""), addChildNodeTags);
		if (addChildNodeTags) {
			StringBuilder performancelevelexampleElements = new StringBuilder();
			for (IXMLTag performancelevelexampleTag : nodeTag.getChilds(gaRubricPerformancelevelexampleTagName)) {
				addPerformanceLevelExample(nodeTag, performancelevelexampleTag, addChildNodeTags, performancelevelexampleElements);
			}			
			performancelevelexampleElements.insert(0, "[");
			performancelevelexampleElements.append("]");
			addElementPart(element, "performancelevelexamples", performancelevelexampleElements, false);
		}
		if (performancelevelElements.length() > 0) {
			performancelevelElements.append(",");
		}
		performancelevelElements.append("{");
		performancelevelElements.append(element);
		performancelevelElements.append("}");
	}

	protected void addPerformanceLevelExample(IXMLTag parentTag, IXMLTag nodeTag, boolean addChildNodeTags, StringBuilder performancelevelexampleElements) {
		StringBuilder element = new StringBuilder();
		addElementPart(element, "parentTagId", parentTag.getAttribute(AppConstants.keyId), true);
		addElementPart(element, _tagIdStr, nodeTag.getAttribute(AppConstants.keyId), true);
		addElementPart(element, "tagName", nodeTag.getName(), true);
		addElementPart(element, "blobId", nodeTag.getChildValue("blob"), true);
		addElementPart(element, "description", sSpring.unescapeXML(nodeTag.getChildValue("description")).replace("\n", ""), false);
		if (performancelevelexampleElements.length() > 0) {
			performancelevelexampleElements.append(",");
		}
		performancelevelexampleElements.append("{");
		performancelevelexampleElements.append(element);
		performancelevelexampleElements.append("}");
	}

	protected String getColorsAsJsonStr(String[] colorArr) {
		StringBuilder elements = new StringBuilder();
		for (int i=0;i<colorArr.length;i++) {
			addColor(colorArr[i], elements);
		}
		elements.insert(0, "[");
		elements.append("]");
		return elements.toString();
	}
	
	protected void addColor(String color, StringBuilder elements) {
		StringBuilder element = new StringBuilder();
		addElementPart(element, "color", color, false);
		if (elements.length() > 0) {
			elements.append(",");
		}
		elements.append("{");
		elements.append(element);
		elements.append("}");
	}

	public void onNotify (Event event) {
		JSONObject jsonObject = (JSONObject)event.getData();
		String action = (String)jsonObject.get("action");
		if (action.equals("create_skill_element")) {
			String parentTagId = (String)jsonObject.get("parentTagId");
			String tagName = (String)jsonObject.get("tagName");
			String elementName = (String)jsonObject.get("elementName");
			IXMLTag parentTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get(_cacIdStr)), parentTagId);
			if (parentTag.getName().equals(gaRubricSkillTagName)) {
				//NOTE insert skill element on right spot
				List<IXMLTag> beforeAndAfterTag = getSkillTagBeforeAndAfterTagForCreate(parentTag, tagName);
				//if skill element is sub skill add it after the last sub skill located directly under the skill tag, or if no sub skills add it before the first skill cluster tag
				IXMLTag tagBeforeNewTag = beforeAndAfterTag.get(0);
				IXMLTag tagAfterNewTag = beforeAndAfterTag.get(1);
				addSkillElementTag(parentTag, tagBeforeNewTag, tagAfterNewTag, tagName, elementName);
			}
			else {
				//NOTE parent is skill cluster, so just add to child tags of skill cluster
				addSkillElementTag(parentTag, null, null, tagName, elementName);
			}
			//rerendering content is more easy then updating client using javascript
			update();
		}
		else if (action.equals("edit_skill_element")) {
			String tagId = (String)jsonObject.get(_tagIdStr);
			String elementName = (String)jsonObject.get("elementName");
			IXMLTag elementTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get(_cacIdStr)), tagId);
			if (elementTag != null) {
				editElementTagChildValue(elementTag, gaRubricElementType, "pid", elementName);
				editElementTagChildValue(elementTag, gaRubricElementType, "name", elementName);
			}
		}
		else if (action.equals("set_skill_element_weighting")) {
			String tagId = (String)jsonObject.get(_tagIdStr);
			String elementWeighting = (String)jsonObject.get("elementWeighting");
			IXMLTag elementTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get(_cacIdStr)), tagId);
			if (elementTag != null) {
				editElementTagChildValue(elementTag, gaRubricElementType, "weighting", elementWeighting);
			}
		}
		else if (action.equals("delete_skill_element")) {
			String tagId = (String)jsonObject.get(_tagIdStr);
			boolean keepSubSkills = ((String)jsonObject.get("keepSubSkills")).equals("true");
			IXMLTag elementTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get(_cacIdStr)), tagId);
			if (elementTag != null) {
				deleteSkillElementTag(elementTag, keepSubSkills);
			}
			if (elementTag.getName().equals(gaRubricSkillclusterTagName)) {
				//rerendering content is more easy than updating client using javascript
				update();
			}
		}
		else if (action.equals("move_skill_element")) {
			String draggedElementTagId = (String)jsonObject.get("draggedElementTagId");
			String targetElementTagId = (String)jsonObject.get("targetElementTagId");
			boolean dropBelowTargetElement = ((String)jsonObject.get("dropBelowTargetElement")).equals("true");
			IXMLTag draggedElementTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get(_cacIdStr)), draggedElementTagId);
			IXMLTag targetElementTag = sSpring.getTag(sSpring.getCaseComponent(_idMap.get(_cacIdStr)), targetElementTagId);
			String draggedElementTagName = draggedElementTag.getName(); 
			String targetElementTagName = targetElementTag.getName();
			if (targetElementTagName.equals(gaRubricSkillTagName) && draggedElementTagName.equals(gaRubricSubskillTagName)) {
				//NOTE skill may only receive sub skills.
				List<IXMLTag> beforeAndAfterTag = getSkillTagBeforeAndAfterTagForMove(targetElementTag, draggedElementTag, targetElementTag, dropBelowTargetElement);
				IXMLTag tagBeforeTag = beforeAndAfterTag.get(0);
				IXMLTag tagAfterTag = beforeAndAfterTag.get(1);
				moveSkillElementTag(targetElementTag, draggedElementTag, tagBeforeTag, tagAfterTag);
			}
			else if (targetElementTagName.equals(gaRubricSkillclusterTagName)) {
				//NOTE skill cluster may receive skill clusters and sub skills 
				if (draggedElementTagName.equals(gaRubricSkillclusterTagName)) {
					List<IXMLTag> beforeAndAfterTag = getSkillTagBeforeAndAfterTagForMove(targetElementTag.getParentTag(), draggedElementTag, targetElementTag, dropBelowTargetElement);
					IXMLTag tagBeforeTag = beforeAndAfterTag.get(0);
					IXMLTag tagAfterTag = beforeAndAfterTag.get(1);
					if (dropBelowTargetElement) {
						//move just below target skill cluster
						tagAfterTag = null;
					}
					else {
						//move just above target skill cluster
						tagBeforeTag = null;
						tagAfterTag = targetElementTag;
					}
					moveSkillElementTag(targetElementTag.getParentTag(), draggedElementTag, tagBeforeTag, tagAfterTag);
				}
				else if (draggedElementTagName.equals(gaRubricSubskillTagName)) {
					//move as last child of target
					moveSkillElementTag(targetElementTag, draggedElementTag, null, null);
				}
			}
			else if (targetElementTagName.equals(gaRubricSubskillTagName) && draggedElementTagName.equals(gaRubricSubskillTagName)) {
				//NOTE sub skill may only receive sub skills.
				IXMLTag tagBeforeTag = null;
				IXMLTag tagAfterTag = null;
				if (dropBelowTargetElement) {
					//move just below target sub skill
					tagBeforeTag = targetElementTag;
				}
				else {
					//move just above target sub skill
					tagAfterTag = targetElementTag;
				}
				moveSkillElementTag(targetElementTag.getParentTag(), draggedElementTag, tagBeforeTag, tagAfterTag);
			}
			//rerendering content is more easy then updating client using javascript
			update();
		}
		else {
			super.onNotify(event);
		}
	}
	
	public List<IXMLTag> getSkillTagBeforeAndAfterTagForCreate(IXMLTag skillTag, String tagName) {
		IXMLTag tagBeforeTag = null;
		IXMLTag tagAfterTag = null;
		List<IXMLTag> subskillTags = skillTag.getChilds(gaRubricSubskillTagName);
		List<IXMLTag> skillclusterTags = skillTag.getChilds(gaRubricSkillclusterTagName);
		//if skill element is sub skill add it after the last sub skill located directly under the skill tag, or if no sub skills add it after the last skill cluster tag
		if (tagName.equals(gaRubricSubskillTagName)) {
			if (subskillTags.size() > 0) {
				tagBeforeTag = subskillTags.get(subskillTags.size() - 1); 
			}
			else if (skillclusterTags.size() > 0) {
				tagBeforeTag = skillclusterTags.get(skillclusterTags.size() - 1);
			}
		}
		//if skill element is skill cluster add it just after the last skill cluster or if no skill cluster just before the first sub skill located directly under the skill tag 
		else if (tagName.equals(gaRubricSkillclusterTagName)) {
			if (skillclusterTags.size() > 0) {
				tagBeforeTag = skillclusterTags.get(skillclusterTags.size() - 1);
			}
			else if (subskillTags.size() > 0) {
				tagAfterTag = subskillTags.get(0); 
			}
		}
		List<IXMLTag> beforeAndAfterTag = new ArrayList<IXMLTag>();
		beforeAndAfterTag.add(tagBeforeTag);
		beforeAndAfterTag.add(tagAfterTag);
		return beforeAndAfterTag;
	}
	
	public List<IXMLTag> getSkillTagBeforeAndAfterTagForMove(IXMLTag skillTag, IXMLTag draggedElementTag, IXMLTag targetElementTag, boolean dropBelowTargetElement) {
		IXMLTag tagBeforeTag = null;
		IXMLTag tagAfterTag = null;
		List<IXMLTag> subskillTags = skillTag.getChilds(gaRubricSubskillTagName);
		List<IXMLTag> skillclusterTags = skillTag.getChilds(gaRubricSkillclusterTagName);
		//if skill element is sub skill move it after the last sub skill located directly under the skill tag, or if no sub skills add it before the first skill cluster tag
		if (draggedElementTag.getName().equals(gaRubricSubskillTagName)) {
			if (subskillTags.size() > 0) {
				tagBeforeTag = subskillTags.get(subskillTags.size() - 1); 
			}
			else if (skillclusterTags.size() > 0) {
				tagAfterTag = skillclusterTags.get(0);
			}
		}
		//if skill element is skill cluster move it just below or above the target element, which always will be a skill cluster 
		else if (draggedElementTag.getName().equals(gaRubricSkillclusterTagName)) {
			if (dropBelowTargetElement) {
				//move just below target skill cluster
				tagBeforeTag = targetElementTag;
			}
			else {
				//move just above target skill cluster
				tagAfterTag = targetElementTag;
			}
		}
		List<IXMLTag> beforeAndAfterTag = new ArrayList<IXMLTag>();
		beforeAndAfterTag.add(tagBeforeTag);
		beforeAndAfterTag.add(tagAfterTag);
		return beforeAndAfterTag;
	}
	
	protected IXMLTag addSkillElementTag(IXMLTag parentTag, IXMLTag tagBeforeNewTag, IXMLTag tagAfterNewTag, String tagName, String elementName) {
		IECaseComponent caseComponent = sSpring.getCaseComponent(_idMap.get(_cacIdStr));
		Map<String,String> childTagNameValueMap = new HashMap<String,String>();
		childTagNameValueMap.put("pid", elementName);
		childTagNameValueMap.put("name", elementName);
		if (gaRubricSubskillTagName.equals(tagName))
			childTagNameValueMap.put("weighting", "1");
		//NOTE update data tree. It is updated in application memory as well.
		IXMLTag dataRootTag = sSpring.getXmlDataTree(caseComponent);
		parentTag = sSpring.getXmlManager().getTagById(dataRootTag, parentTag.getAttribute(AppConstants.statusKeyId));
		IXMLTag nodeTag = null;
		if (tagBeforeNewTag != null) {
			tagBeforeNewTag = sSpring.getXmlManager().getTagById(dataRootTag, tagBeforeNewTag.getAttribute(AppConstants.statusKeyId));
			nodeTag = addNodeTagAfterTag(parentTag, tagBeforeNewTag, tagName, childTagNameValueMap);
		}
		else if (tagAfterNewTag != null) {
			tagAfterNewTag = sSpring.getXmlManager().getTagById(dataRootTag, tagAfterNewTag.getAttribute(AppConstants.statusKeyId));
			nodeTag = addNodeTagBeforeTag(parentTag, tagAfterNewTag, tagName, childTagNameValueMap);
		}
		else {
			nodeTag = addNodeTag(parentTag, tagName, childTagNameValueMap);
		}
		if (nodeTag != null) {
			addPerformancelevelTags(nodeTag);
		}
		sSpring.setCaseComponentData("" + caseComponent.getCacId(), sSpring.getXmlManager().xmlTreeToDoc(dataRootTag));
		return nodeTag;
	}

	protected IXMLTag moveSkillElementTag(IXMLTag parentTag, IXMLTag draggedTag, IXMLTag tagBeforeTag, IXMLTag tagAfterTag) {
		IXMLTag elementTag = null;
		IECaseComponent caseComponent = sSpring.getCaseComponent(_idMap.get(_cacIdStr));
		//NOTE update data tree. It is updated in application memory as well.
		IXMLTag dataRootTag = sSpring.getXmlDataTree(caseComponent);
		parentTag = sSpring.getXmlManager().getTagById(dataRootTag, parentTag.getAttribute(AppConstants.statusKeyId));
		draggedTag = sSpring.getXmlManager().getTagById(dataRootTag, draggedTag.getAttribute(AppConstants.statusKeyId));
		if (tagBeforeTag != null) {
			tagBeforeTag = sSpring.getXmlManager().getTagById(dataRootTag, tagBeforeTag.getAttribute(AppConstants.statusKeyId));
			elementTag = moveNodeTagAfterTag(parentTag, draggedTag, tagBeforeTag);
		}
		else if (tagAfterTag != null) {
			tagAfterTag = sSpring.getXmlManager().getTagById(dataRootTag, tagAfterTag.getAttribute(AppConstants.statusKeyId));
			elementTag = moveNodeTagBeforeTag(parentTag, draggedTag, tagAfterTag);
		}
		else {
			elementTag = moveNodeTag(parentTag, draggedTag);
		}
		sSpring.setCaseComponentData("" + caseComponent.getCacId(), sSpring.getXmlManager().xmlTreeToDoc(dataRootTag));

		return elementTag;
	}

	protected IXMLTag deleteSkillElementTag(IXMLTag elementTag, boolean ifSkillClusterTagKeepSubSkillTags) {
		IECaseComponent caseComponent = getCaseComponent(gaRubricElementType);
		//NOTE update data tree. It is updated in application memory as well.
		IXMLTag rootTag = sSpring.getXmlDataTree(caseComponent);
		if (ifSkillClusterTagKeepSubSkillTags) {
			elementTag = sSpring.getXmlManager().getTagById(rootTag, elementTag.getAttribute(AppConstants.statusKeyId));
			ifSkillClusterTagKeepSubSkillTags(elementTag);
		}
		elementTag = deleteNodeTag(rootTag, elementTag);
		sSpring.setCaseComponentData("" + caseComponent.getCacId(), sSpring.getXmlManager().xmlTreeToDoc(rootTag));
		return elementTag;
	}
	
	protected void ifSkillClusterTagKeepSubSkillTags(IXMLTag elementTag) {
		if (!elementTag.getName().equals(gaRubricSkillclusterTagName)) {
			return;
		}
		IXMLTag skillTag = elementTag.getParentTag();
		List<IXMLTag> subskillTagsOfSkillTag = skillTag.getChilds(gaRubricSubskillTagName);
		IXMLTag tagBeforeNewTag = null; 
		if (subskillTagsOfSkillTag.size() > 0) {
			tagBeforeNewTag = subskillTagsOfSkillTag.get(subskillTagsOfSkillTag.size() - 1);
		}
		List<IXMLTag> subskillTags = elementTag.getChilds(gaRubricSubskillTagName);
		for (IXMLTag subSkillTag : subskillTags) {
			subSkillTag.getParentTag().getChildTags().remove(subSkillTag);
			subSkillTag.setParentTag(skillTag);
			int index = 0;
			if (tagBeforeNewTag != null) {
				index = skillTag.getChildTags().indexOf(tagBeforeNewTag) + 1;
			}
			skillTag.getChildTags().add(index, subSkillTag);
			tagBeforeNewTag = subSkillTag;
		}
	}
	
}