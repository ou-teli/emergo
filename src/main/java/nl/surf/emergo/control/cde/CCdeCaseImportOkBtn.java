/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.IAppManager;
import nl.surf.emergo.business.ICaseManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControlHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CImportExportHelper;
import nl.surf.emergo.control.def.CDefButton;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.utilities.FileHelper;

/**
 * The Class CCdeCaseImportOkBtn.
 */
public class CCdeCaseImportOkBtn extends CDefButton {

	private static final long serialVersionUID = 3910615284386188832L;

	/**
	 * On click import case using uploaded file if no errors and close edit window.
	 */
	public void onClick() {
		Component lWindow = getRoot();
		IAppManager appManager = (IAppManager)CDesktopComponents.sSpring().getBean("appManager");
		ICaseManager caseManager = (ICaseManager)CDesktopComponents.sSpring().getBean("caseManager");
		IAccountManager accountManager = (IAccountManager)CDesktopComponents.sSpring().getBean("accountManager");

		// check if name/version combo exists otherwise show error
		// create temporary case to do the check
		IECase lItem = caseManager.getNewCase();
		IEAccount lAccount = accountManager.getAccount(CDesktopComponents.cControl().getAccId());
		lItem.setEAccount(lAccount);
		lItem.setCode(CControlHelper.getTextboxValue(this, "code"));
		lItem.setName(CControlHelper.getTextboxValue(this, "name"));
		lItem.setVersion(CControlHelper.getTextboxValueAsInt(this, "version", 1));
		List<String[]> lErrors = new ArrayList<String[]>(0);
		lErrors = caseManager.validateCase(lItem);
		Textbox lTextbox = (Textbox)CControlHelper.getInputComponent(this, "filename");
		String lFileName = lTextbox.getValue();
		if (lFileName.equals(""))
			appManager.addError(lErrors, "file", "error_no_choice");

		if ((lErrors == null) || (lErrors.size() == 0)) {
			lItem = null;
			CImportExportHelper cHelper = new CImportExportHelper();
			Media lMedia = (Media)lTextbox.getAttribute(AppConstants.contentElement);
			if (lMedia == null)
				appManager.addError(lErrors, "file", "not_found");
			else {
				List<String> lMVErrors = new ArrayList<>();
				if (FileHelper.isMediaValid(lMedia, lMVErrors)) {
					byte[] lContent = CDesktopComponents.sSpring().getSBlobHelper().mediaToByteArray(lMedia);
					lItem = cHelper.importCase(lAccount, CControlHelper.getTextboxValue(this, "name"), lFileName, lContent);
					if (lItem != null) {
						lItem.setCode(CControlHelper.getTextboxValue(this, "code"));
						lItem.setVersion(CControlHelper.getTextboxValueAsInt(this, "version", 1));
						caseManager.saveCase(lItem);
						lWindow.setAttribute("item",lItem);
					}
					else {
						CDesktopComponents.vView().showMessagebox(getRoot(), CDesktopComponents.vView().getLabel("cde_s_case_import.error.body"), CDesktopComponents.vView().getLabel("cde_s_case_import.error.title"), Messagebox.OK, Messagebox.ERROR);
						lWindow.setAttribute("item",null);
					}
				} else {
					FileHelper.showUploadErrorMessagebox(lMedia, lMVErrors);
					lWindow.setAttribute("item", null);
				}
			}
		}
		if (!((lErrors == null) || (lErrors.size() == 0))) {
			lWindow.setAttribute("item",null);
			CDesktopComponents.cControl().showErrors(this,lErrors);
		}
		lWindow.detach();
	}
}
