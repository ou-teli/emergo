/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde;

import java.util.List;

import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.control.CControlHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CInputBtn;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;

/**
 * The Class CCdeCaseComponentsContentImportBtn.
 */
public class CCdeCaseComponentsContentImportBtn extends CInputBtn {

	private static final long serialVersionUID = 6482472387808425681L;

	public void onCreate() {
		setDisabled(!CControlHelper.isCaseAuthor());
	}
	
	/**
	 * On click show case components import window and if import is ok add new items in listbox.
	 */
	public void onClick() {
		listbox = (Listbox)getFellowIfAny("caseComponentsLb");
		showPopup(VView.v_cde_s_casecomponents_content_import);
	}

	@Override
	protected void handleItems(Object aStatus) {
		//rerender changed case components if name has been changed
		List<IECaseComponent> lCaseComponents = (List<IECaseComponent>)aStatus;
		if (lCaseComponents != null && lCaseComponents.size() > 0) {
			CCdeCaseComponentsHelper cHelper = new CCdeCaseComponentsHelper();
			List<Listitem> lListitems = listbox.getItems();
			for (IECaseComponent lCaseComponent : lCaseComponents) {
				Listitem lListitem = null;
				for (Listitem lTempListitem : lListitems) {
					IECaseComponent lTempCaseComponent = (IECaseComponent)lTempListitem.getValue();
					if (lTempCaseComponent.getCacId() == lCaseComponent.getCacId() && !lTempCaseComponent.getName().equals(lCaseComponent.getName())) {
						lListitem = lTempListitem;
						break;
					}
				}
				if (lListitem != null) {
					cHelper.renderItem(listbox, lListitem, lCaseComponent);
					lListitem.detach();
				}
			}
		}
		showPossibleWarnings();
	}

	@Override
	protected void cancelItem() {
		showPossibleWarnings();
	}

	protected void showPossibleWarnings() {
		if (window == null) {
			return;
		}
		List<String[]> lWarnings = (List<String[]>)window.getAttribute("warnings");
		if (lWarnings != null && lWarnings.size() > 0) {
			CDesktopComponents.cControl().showWarnings(null, lWarnings);
		}
	}

}
