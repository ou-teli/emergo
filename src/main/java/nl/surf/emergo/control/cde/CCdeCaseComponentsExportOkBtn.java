/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zhtml.A;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CImportExportHelper;
import nl.surf.emergo.control.CImportExportWnd;
import nl.surf.emergo.control.def.CDefButton;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CCdeCaseComponentsExportOkBtn.
 */
public class CCdeCaseComponentsExportOkBtn extends CDefButton {

	private static final long serialVersionUID = 4333265160885440941L;

	public void onClick() {
		List<IECaseComponent> caseComponents = new ArrayList<IECaseComponent>();
		for (Listitem listitem : ((Listbox)CDesktopComponents.vView().getComponent("cacCacId")).getSelectedItems()) {
			if (listitem.getAttribute("caseComponent") != null) {
				caseComponents.add((IECaseComponent)listitem.getAttribute("caseComponent"));
			}
		}
		CImportExportHelper lHelper = ((CImportExportWnd)getRoot()).getImportExportHelper();
		String url = lHelper.exportCaseComponents(caseComponents);
		if (url.equals("")) {
			CDesktopComponents.vView().showMessagebox(getRoot(), CDesktopComponents.vView().getLabel("cde_s_casecomponents_export.error.body"), CDesktopComponents.vView().getLabel("cde_s_casecomponents_export.error.title"), Messagebox.OK, Messagebox.ERROR);
		}
		else {
			A lDownloadLink = (A)CDesktopComponents.vView().getComponent("downloadLink");
			lDownloadLink.setDynamicProperty("href", url);
			lDownloadLink.setDynamicProperty("innerHTML", CDesktopComponents.vView().getLabel("casecomponents"));
			CDesktopComponents.vView().getComponent("caseComponentListboxDiv").setVisible(false);
			CDesktopComponents.vView().getComponent("downloadLinkDiv").setVisible(true);
		}
	}

}
