/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde;

import java.util.List;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CInputListbox;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CCdeCaseComponentAuthorLb.
 */
public class CCdeCaseComponentAuthorLb extends CInputListbox {

	private static final long serialVersionUID = 74512343457135533L;

	public CCdeCaseComponentAuthorLb() {
		super();
	}

	/**
	 * On create render possible case component authors, all Emergo users with role case developer.
	 * And preselect one if applicable.
	 * 
	 * @param aEvent the a event
	 */
	public void onCreate(CreateEvent aEvent) {
		IECaseComponent lItem = null;
		if ((getRoot() instanceof CCdeCaseComponentWnd)) {
			lItem = (IECaseComponent)((CCdeCaseComponentWnd)getRoot()).getItem(aEvent);;
		}
		IAccountManager accountManager = (IAccountManager)CDesktopComponents.sSpring().getBean("accountManager");
		List<IEAccount> lItems = accountManager.getAllAccounts(true,AppConstants.c_role_cde);
		for (IEAccount lObject : lItems) {
			Listitem lListitem = super.insertListitem(lObject,accountManager.getAccountName(lObject),null);
			if (lItem == null) {
				if (lObject.getAccId() == CDesktopComponents.cControl().getAccId())
					setSelectedItem(lListitem);
			}
			else
				if (lObject.getAccId() == lItem.getEAccount().getAccId())
					setSelectedItem(lListitem);
		}
	}
}
