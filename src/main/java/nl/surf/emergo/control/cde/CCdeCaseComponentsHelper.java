/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde;

import java.util.List;

import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.ICaseComponentRoleManager;
import nl.surf.emergo.business.ICaseRoleManager;
import nl.surf.emergo.control.CControlHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefListcell;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IECaseComponentRole;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.view.VView;

/**
 * The Class CCdeCaseComponentsHelper.
 */
public class CCdeCaseComponentsHelper extends CControlHelper {

	/** The account manager. */
	private IAccountManager accountManager = (IAccountManager)CDesktopComponents.sSpring().getBean("accountManager");

	/** The case component role manager. */
	private ICaseComponentRoleManager caseComponentRoleManager = (ICaseComponentRoleManager)CDesktopComponents.sSpring().getBean("caseComponentRoleManager");

	/** The case component role manager. */
	private ICaseRoleManager caseRoleManager = (ICaseRoleManager)CDesktopComponents.sSpring().getBean("caseRoleManager");

	/** The case roles. */
	private List<IECaseComponentRole> caseComponentRoles = null;

	/**
	 * Sets the case roles.
	 *
	 * @param aCaseRoles the new case roles
	 */
	public void setCaseComponentRoles(List<IECaseComponentRole> aCaseComponentRoles) {
		caseComponentRoles = aCaseComponentRoles;
	}

	/**
	 * Renders one case component.
	 *
	 * @param aListbox the a listbox
	 * @param aInsertBefore the a insert before
	 * @param aItem the a item
	 */
	public void renderItem(Listbox aListbox,Listitem aInsertBefore,IECaseComponent aItem) {
		Listitem lListitem = super.newListitem();
		lListitem.setValue(aItem);
		showTooltiptextIfAdmin(lListitem, "cacId=" + aItem.getCacId());
		super.appendListcell(lListitem,aItem.getName());
		String lLabel = CDesktopComponents.vView().getLabel(VView.componentLabelKeyPrefix + aItem.getEComponent().getCode());
		if (lLabel.length() == 0) {
			lLabel = aItem.getEComponent().getCode();
		}
		super.appendListcell(lListitem, lLabel);
		Listcell lCell = super.appendListcell(lListitem,accountManager.getAccountName(aItem.getEAccount()));
		showTooltiptextIfAdmin(lCell, "accId=" + aItem.getEAccount().getAccId());
		super.appendListcell(lListitem,getCaseRoles(aItem));
		super.appendListcell(lListitem,getDateStrAsYMD(aItem.getCreationdate()));
		Listcell lListcell = new CDefListcell();
		CCdeCaseComponentRedirectBtn lRedirect = new CCdeCaseComponentRedirectBtn();
		lListcell.appendChild(lRedirect);
		lListitem.appendChild(lListcell);
/*		if (aItem.getECase().getEAccount().getAccId() == CDesktopComponents.sSpring().getAccId()) {
			lListcell = new CDefListcell();
			CImportXmlBtn lImportXmlBtn = new CImportXmlBtn();
			lListcell.appendChild(lImportXmlBtn);
			lListitem.appendChild(lListcell);
		} */
		super.insertListitem(aListbox,lListitem,aInsertBefore);
	}

	/**
	 * Gets the case roles for a case component.
	 *
	 * @param aItem the a item
	 *
	 * @return the case roles
	 */
	public String getCaseRoles(IECaseComponent aItem) {
		String lCaseRoles = "";
		List<IECaseComponentRole> lIECaseComponentRoles = caseComponentRoles;
		if (lIECaseComponentRoles == null)
			lIECaseComponentRoles = caseComponentRoleManager.getAllCaseComponentRolesByCacId(aItem.getCacId());
		for (IECaseComponentRole lItem : lIECaseComponentRoles) {
			if (aItem.getCacId() == lItem.getCacCacId()) {
				IECaseRole lCar = caseRoleManager.getCaseRole(lItem.getCarCarId());
				if (lCar != null) {
					if (!lCaseRoles.equals(""))
						lCaseRoles = lCaseRoles + " ";
					lCaseRoles = lCaseRoles + lCar.getName();
				}
			}
		}
		return lCaseRoles;
	}
}