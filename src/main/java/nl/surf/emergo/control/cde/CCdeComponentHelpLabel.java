/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Html;
import org.zkoss.zul.Label;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treechildren;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.Treerow;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CContentHelper;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefLabel;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;

/**
 * The Class CCdeComponentHelpLabel.
 */
public class CCdeComponentHelpLabel extends CDefLabel {

	private static final long serialVersionUID = 1250093739522194920L;

	protected VView vView = CDesktopComponents.vView();

	protected CControl cControl = CDesktopComponents.cControl();
	
	protected IECaseComponent caseComponent = (IECaseComponent)cControl.getAccSessAttr("casecomponent");
	
	protected String cacComCode = (String)cControl.getAccSessAttr("cacComCode");

	public CCdeComponentHelpLabel() {
		super();
	}

	public void onCreate(CreateEvent aEvent) {
		vView.setHelpComponent(this, this);
	}

	public void onRenderHelpContent(Event aEvent) {
		//get root tag of current casecomponent definition
		IXMLTag rootTag = CDesktopComponents.sSpring().getXmlDefTree(caseComponent.getEComponent());
		//get content tag within root tag
		IXMLTag contentTag = rootTag.getChild(AppConstants.contentElement);

		List<Object[]> listNodeTagNames = new ArrayList<Object[]>();
		if (!contentTag.getDefAttribute(AppConstants.defKeyChildnodes).equals("")) {
			//get child tag names for content tag. Add commas to be able to search for a child tag name, see further.
			String contentTagChildNodeNamesStr = "," + contentTag.getAttribute(AppConstants.defKeyChildnodes) + ",";
			//get all node tags within the definition
			List<IXMLTag> nodeTags = CDesktopComponents.cScript().getNodeTags(rootTag);
			//if nodeTags not empty, add content tag as first node tag
			if (nodeTags.size() > 0) {
				nodeTags.add(0, contentTag);
			}
			
			//fill nested list of Object[String, List], where List is again of type Object[String, List] 
			for (IXMLTag nodeTag : nodeTags) {
				//does nameAndList exist for node tag?
				Object[] nameAndList = getNodeTagNameAndList(listNodeTagNames, nodeTag.getName());
				//if nameAndList does not exist yet, and the node tag is the content tag or the node tag exists as child of the content tag
				//NOTE within definitions sometimes node tags exist without a (content) parent, to define status tags created during playing the case.
				//These tags may not be entered by case authors, so should not be shown in the tree with available node tags.
				if (nameAndList == null && (nodeTag.getName().equals(AppConstants.contentElement) || contentTagChildNodeNamesStr.indexOf("," + nodeTag.getName() + ",") >= 0)) {
					//create nameAndList for the node tag and add it to list
					nameAndList = new Object[]{nodeTag.getName(), new ArrayList<Object[]>()};
					listNodeTagNames.add(nameAndList);
				}
				//if nameAndList is exists or is created
				if (nameAndList != null) {
					//add all child tag names to nameAndList
					String[] childNodeNames = nodeTag.getAttribute(AppConstants.defKeyChildnodes).split(",");
					for (int i=0;i<childNodeNames.length;i++) {
						//name may not be empty
						if (childNodeNames[i].length() > 0) {
							//create childNameAndList for the child tag and add it to list nameAndList[1]
							Object[] childNameAndList = new Object[]{childNodeNames[i], new ArrayList<Object[]>()};
							((List<Object[]>)nameAndList[1]).add(childNameAndList);
						}
					}
				}
			}
		}
		
		//show help text for casecomponent type
		Html html = new Html();
		((Component)aEvent.getData()).appendChild(html);
		html.setContent(vView.getLabel(VView.componentLabelKeyPrefix + cacComCode + ".help"));

		if (listNodeTagNames.size() > 0) {
			//show tree with allowed node tags and their help texts.
			Tree tree = new Tree();
			((Component)aEvent.getData()).appendChild(tree);
			putNodeTagNamesListInTree(listNodeTagNames, tree);
		}
	}
	


	protected Object[] getNodeTagNameAndList(List<Object[]> listNodeTagNames, String nodeTagName) {
		Object[] foundNameAndList = null;
		//search in list
		for (Object[] nameAndList : listNodeTagNames) {
			if (foundNameAndList == null && nameAndList[0].equals(nodeTagName)) {
				foundNameAndList = nameAndList;
			}
			if (foundNameAndList == null) {
				//search in list with children
				foundNameAndList = getNodeTagNameAndList((List<Object[]>)nameAndList[1], nodeTagName);
			}
		}
		return foundNameAndList;
	}

	protected Treeitem getNewTreeitem(Component parent) {
		//create one treeitem
		if (parent instanceof Tree || parent instanceof Treeitem) {
			// within ZK treechildren is parent of treeitems
			// so if parent is tree or treeitem get or create treechildren object
			Treechildren treechildren = null;
			if (parent instanceof Tree) {
				treechildren = ((Tree)parent).getTreechildren();
			}
			else if (parent instanceof Treeitem) {
				treechildren = ((Treeitem)parent).getTreechildren();
			}
			if (treechildren == null) {
				// if not existing, add them
				treechildren = new Treechildren();
				parent.appendChild(treechildren);
			}
			parent = treechildren;
		}
		Treeitem treeitem = new Treeitem();
		parent.appendChild(treeitem);
		return treeitem;
	}

	protected void putNodeTagNamesListInTree(List<Object[]> listNodeTagNames, Component parent) {
		//create treeitems in list
		for (Object[] nameAndList : listNodeTagNames) {
			Treeitem treeitem = getNewTreeitem(parent);
			Treerow treerow = new Treerow(); 
			treeitem.appendChild(treerow);
			Treecell treecell = new Treecell(); 
			treerow.appendChild(treecell);
			Label label = new Label();
			label.setValue(CContentHelper.getNodeTagLabel(cacComCode, (String)nameAndList[0]));
			label.setSclass("CCdeComponentWnd_treecell_label");
			treecell.appendChild(label);
			label = new Label(" ");
			treecell.appendChild(label);
			//set help text for parent, so help icon is positioned relative
			vView.setParentHelpText(treecell, CContentHelper.getHelpText(cacComCode, (String)nameAndList[0], "", VView.nodetagLabelKeyPrefix));
			//create treeitems in list with children
			putNodeTagNamesListInTree((List<Object[]>)nameAndList[1], treeitem);
		}
	}

}
