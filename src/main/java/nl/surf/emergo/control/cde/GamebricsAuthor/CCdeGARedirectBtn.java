/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde.GamebricsAuthor;

import org.apache.commons.lang3.StringUtils;

import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefDiv;
import nl.surf.emergo.view.VView;

/**
 * The Class CCdeGARedirectBtn.
 */
public class CCdeGARedirectBtn extends CDefDiv {

	private static final long serialVersionUID = 1812988043670760227L;
	
	private static final String lLastViewAttrId = "cde_GA_lastView";
	private static final String lCurrentViewAttrId = "cde_GA_currentView";

	/**
	 * On click open edit window for the selected Gamebrics author element.
	 */
	public void onClick() {
		String lRedirect = CDesktopComponents.vView().getAttributeString(this, "redirectFile");
		CControl cControl = CDesktopComponents.cControl();
		if (lRedirect.equals(CCdeGamebricsAuthorDiv.cde_GA_redirectBack)) {
			String lLastView = (String)cControl.getAccSessAttr(lLastViewAttrId);
			if (StringUtils.isEmpty(lLastView))
				lLastView = VView.v_cde_gamebricsauthor;
			lRedirect = lLastView;
		}
		String lCurrentView = (String)cControl.getAccSessAttr(lCurrentViewAttrId);
		if (!lRedirect.equals(VView.v_cde_gamebricsauthor) && !lRedirect.equals(lCurrentView)) {
			cControl.setAccSessAttr(lLastViewAttrId, lCurrentView);
			cControl.setAccSessAttr(lCurrentViewAttrId, lRedirect);
		}
		CDesktopComponents.vView().redirectToView(lRedirect);
	}
}
