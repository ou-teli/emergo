/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde.GamebricsAuthor.def;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.impl.InputElement;

import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.cde.GamebricsAuthor.rubrics.CCdeGamebricsAuthorCacAndTag;

public class CCdeGamebricsAuthorDefHelper {
	
	public static final String gRootIdAttrStr = "_rootID";

	public static void fillKeyValueMap(Component component, String[] keys, Object[] values, Map<String,Object> keyValueMap) {
		if (keys != null && values != null) {
			for (int i=0;i<keys.length;i++) {
				if (i < values.length) {
					keyValueMap.put(keys[i], values[i]);
				}
			}
			component.setAttribute("keyValueMap", keyValueMap);
		}
	}
	
	public static void fillKeyValueMap(Component component, Map<String,Object> keyValueMap) {
		String labelKey = CDesktopComponents.vView().getAttributeString(component, "labelKey");
		if (!labelKey.equals("")) {
			boolean labelCapitalizeFirstChar = CDesktopComponents.vView().getAttributeString(component, "labelCapitalizeFirstChar").equals(AppConstants.statusValueTrue);
			if (labelCapitalizeFirstChar) {
				keyValueMap.put("cLabelKey", labelKey);
			}
			else {
				keyValueMap.put("labelKey", labelKey);
			}
			component.setAttribute("keyValueMap", keyValueMap);
		}
	}
	
	public static void decorateComponent(Component component, Map<String,Object> keyValueMap) {
		if (keyValueMap.containsKey("id")) {
			component.setId((String)keyValueMap.get("id"));
		}
		if (keyValueMap.containsKey("class")) {
			if (component instanceof HtmlBasedComponent) {
				((HtmlBasedComponent)component).setZclass((String)keyValueMap.get("class"));
			}
		}
		if (keyValueMap.containsKey("style")) {
			if (component instanceof HtmlBasedComponent) {
				((HtmlBasedComponent)component).setStyle((String)keyValueMap.get("style"));
			}
		}
		if (keyValueMap.containsKey("width")) {
			if (component instanceof HtmlBasedComponent) {
				((HtmlBasedComponent)component).setWidth((String)keyValueMap.get("width"));
			}
		}
		if (keyValueMap.containsKey("height")) {
			if (component instanceof HtmlBasedComponent) {
				((HtmlBasedComponent)component).setHeight((String)keyValueMap.get("height"));
			}
		}
		if (keyValueMap.containsKey("tooltiptext")) {
			if (component instanceof HtmlBasedComponent) {
				((HtmlBasedComponent)component).setTooltiptext((String)keyValueMap.get("tooltiptext"));
			}
		}
		if (keyValueMap.containsKey("text")) {
			if (component instanceof InputElement) {
				((InputElement)component).setText((String)keyValueMap.get("text"));
			}
		}
		if (keyValueMap.containsKey("visible")) {
			component.setVisible(!((String)keyValueMap.get("visible")).equals("false"));
		}
	}

	public static String[] getValueAndTooltipText(Map<String,Object> keyValueMap) {
		String value = "";
		String tooltiptext = "";
		if (keyValueMap.containsKey("value")) {
			value = (String)keyValueMap.get("value");
		}
		else if (keyValueMap.containsKey("labelKey")) {
			value = CDesktopComponents.vView().getLabel((String)keyValueMap.get("labelKey"));
		}
		else if (keyValueMap.containsKey("cLabelKey")) {
			value = CDesktopComponents.vView().getCLabel((String)keyValueMap.get("cLabelKey"));
		}
		if (keyValueMap.containsKey("maxLabelLength")) {
			int maxLabelLength = (int)keyValueMap.get("maxLabelLength");
			if (value.length() > maxLabelLength) {
				tooltiptext = value;
				value = value.substring(0, maxLabelLength) + "...";
			}
		}
		return new String[]{value, tooltiptext};
	}

	public static String editContentOrLabel(Component component) {
		if (component == null) {
			return "";
		}
		Map<String,Object> keyValueMap = (Map<String,Object>)component.getAttribute("keyValueMap");
		if (keyValueMap == null) {
			return "";
		}
		//For content
		if (keyValueMap.containsKey("cacAndTag") && (keyValueMap.get("cacAndTag") instanceof CCdeGamebricsAuthorCacAndTag) && keyValueMap.containsKey("tagChildName")) {
		}
		//for labels
		else if (keyValueMap.containsKey("labelKey")) {
			//TODO enable personalizing of labels
			String value = CDesktopComponents.vView().getLabel((String)keyValueMap.get("labelKey"));
			CDesktopComponents.vView().showMessagebox(null, value, "Bewerk label", Messagebox.OK, Messagebox.INFORMATION);
			//TODO show pop-up to edit label value
			return value;
		}
		else if (keyValueMap.containsKey("cLabelKey")) {
			//TODO enable personalizing of labels
			String value = CDesktopComponents.vView().getCLabel((String)keyValueMap.get("cLabelKey"));
			CDesktopComponents.vView().showMessagebox(null, value, "Bewerk label", Messagebox.OK, Messagebox.INFORMATION);
			//TODO show pop-up to edit label value
			return value;
		}
		return "";
	}
	
	public static String getRootId(Component pComponent) {
		return getCustomAttribute(pComponent, gRootIdAttrStr);
	}
	
	public static String getCustomAttribute(Component pComponent, String pAttrId) {
		String lAttrValue = "";
		while (pComponent != null) {
			if (!StringUtils.isEmpty((String)pComponent.getAttribute(pAttrId))) {
				lAttrValue = (String)pComponent.getAttribute(pAttrId);
				pComponent = null;
			} else
				pComponent = pComponent.getParent();
		}
		return lAttrValue;
	}
	
}