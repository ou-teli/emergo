/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.ICaseManager;
import nl.surf.emergo.control.CControlHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.cde.GamebricsAuthor.CCdeCaseRedirectGamebricsAuthorBtn;
import nl.surf.emergo.control.def.CDefListcell;
import nl.surf.emergo.domain.IECase;

/**
 * The Class CCdeCasesHelper.
 */
public class CCdeCasesHelper extends CControlHelper {

	protected ICaseManager caseManager = (ICaseManager)CDesktopComponents.sSpring().getBean("caseManager");

	/**
	 * Renders one case.
	 * 
	 * @param aListbox the a listbox
	 * @param aInsertBefore the a insert before
	 * @param aItem the a item
	 */
	public void renderItem(Listbox aListbox,Listitem aInsertBefore,IECase aItem) {
		Listitem lListitem = super.newListitem();
		lListitem.setValue(aItem);
		showTooltiptextIfAdmin(lListitem, "casId=" + aItem.getCasId());
		super.appendListcell(lListitem,aItem.getCode());
		super.appendListcell(lListitem,aItem.getName());
		super.appendListcell(lListitem,""+aItem.getVersion());
		super.appendListcell(lListitem,getDateStrAsYMD(aItem.getCreationdate()));
		super.appendListcell(lListitem,""+(aItem.getEAccount().getAccId() == CDesktopComponents.cControl().getAccId()));
		super.appendListcell(lListitem, CDesktopComponents.sSpring().getSCaseSkinHelper().getCaseSkin(aItem));
		super.appendListcell(lListitem,"" + aItem.getMultilingual());
		String lStreamingCaseName = "";
		if (aItem.getCasCasId() != 0) {
			IECase lStreamingCase = caseManager.getCase(aItem.getCasCasId());
			if (lStreamingCase != null) {
				lStreamingCaseName = lStreamingCase.getName() + " (" + lStreamingCase.getVersion() + ")";
			}
		}
		super.appendListcell(lListitem, lStreamingCaseName);
		Listcell lListcell = new CDefListcell();
		CCdeCaseRedirectCaseRolesBtn lRedirect2 = new CCdeCaseRedirectCaseRolesBtn();
		lListcell.appendChild(lRedirect2);
		lListitem.appendChild(lListcell);
		lListcell = new CDefListcell();
		CCdeCaseRedirectCaseComponentsBtn lRedirect1 = new CCdeCaseRedirectCaseComponentsBtn();
		lListcell.appendChild(lRedirect1);
		lListitem.appendChild(lListcell);
		lListcell = new CDefListcell();
		CCdeCaseRedirectGamebricsAuthorBtn lRedirect3 = new CCdeCaseRedirectGamebricsAuthorBtn();
		lListcell.appendChild(lRedirect3);
		lListitem.appendChild(lListcell);
		super.insertListitem(aListbox,lListitem,aInsertBefore);
	}

	protected class CaseSortByName implements Comparator<IECase>{
		public int compare(IECase o1, IECase o2) {
			return o1.getName().compareTo(o2.getName());
		}
	}
	
	/**
	 * Get all possible cases.
	 * 
	 * @param aAccId the a acc id
	 * 
	 * @return all possible cases
	 */
	public List<IECase> getPossibleCases(int aAccId) {
		List<IECase> lAllCases = caseManager.getAllCasesByAccId(aAccId);
		Collections.sort(lAllCases, new CaseSortByName());
		return lAllCases;
	}


}