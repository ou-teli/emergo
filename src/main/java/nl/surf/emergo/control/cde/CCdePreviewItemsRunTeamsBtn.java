/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde;

import nl.surf.emergo.business.IRunManager;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CInputBtn;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.view.VView;

/**
 * The Class CCdePreviewItemsRunTeamsBtn.
 */
public class CCdePreviewItemsRunTeamsBtn extends CInputBtn {

	private static final long serialVersionUID = 2683529246769036142L;

	/** The e run. */
	protected IERun eRun = null;
	
	/** The e account. */
	protected IEAccount eAccount = null;
	
	/** The e case. */
	protected IECase eCase = null;

	/**
	 * Instantiates a new c cde preview items run teams btn.
	 */
	public CCdePreviewItemsRunTeamsBtn() {
		setLabel(CDesktopComponents.vView().getLabel("teams"));
	}

	/**
	 * On create set visible to true if team case, otherwise set false.
	 */
	public void onCreate() {
		CControl cControl = CDesktopComponents.cControl();
		eAccount = CDesktopComponents.sSpring().getAccount();
		eCase = (IECase) cControl.getAccSessAttr("case");
		eRun = ((IRunManager) CDesktopComponents.sSpring().getBean("runManager")).getTestRun(eAccount, eCase);
		CCdePreviewItemsLb lListbox = (CCdePreviewItemsLb)getFellowIfAny("previewItemsLb");
		setDisabled(!lListbox.isTeamCase());
	}

	/**
	 * On click show run teams popup.
	 */
	public void onClick() {
		IERun lItem = eRun;
		IECase eCase = lItem.getECase();
		CControl cControl = CDesktopComponents.cControl();
		cControl.setAccSessAttr("case",eCase);
		cControl.setAccSessAttr("casId",""+eCase.getCasId());
		cControl.setAccSessAttr("run",lItem);
		cControl.setAccSessAttr("runId",""+lItem.getRunId());
		cControl.setAccSessAttr("runName",""+lItem.getName());
		showPopup(VView.v_s_preview_item_runteams);
	}
	
	@Override
	protected void cancelItem() {
		CCdePreviewItemsLb lListbox = (CCdePreviewItemsLb)getFellowIfAny("previewItemsLb");
		lListbox.update();
	}

}
