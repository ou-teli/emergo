/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde;

import java.util.List;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zul.Button;

import nl.surf.emergo.business.ICaseComponentManager;
import nl.surf.emergo.business.IRunCaseComponentManager;
import nl.surf.emergo.business.IRunManager;
import nl.surf.emergo.business.IRunTeamCaseComponentManager;
import nl.surf.emergo.business.IRunTeamManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefListbox;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunCaseComponent;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.domain.IERunTeamCaseComponent;

/**
 * The Class CCdePreviewItemsLb.
 */
public class CCdePreviewItemsLb extends CDefListbox {

	private static final long serialVersionUID = -8934512750587127495L;

	/** The item. */
	protected IXMLTag item = null;

	/** The team case. */
	protected String teamCase = "";

	/** The case components by status type. */
	protected List<IECaseComponent> caseComponentsByStatusType = null;

	/**
	 * Gets the item.
	 *
	 * @return the item
	 */
	public IXMLTag getItem() {
		return item;
	}

	/**
	 * Sets the item.
	 *
	 * @param aItem the new item
	 */
	public void setItem(IXMLTag aItem) {
		item = aItem;
	}

	/**
	 * On create fill item and render list items.
	 *
	 * @param aEvent the a event
	 */
	public void onCreate(CreateEvent aEvent) {
		setItem((IXMLTag) (aEvent.getArg()).get("item"));
		update();
		restoreSettings();
	}

	public void onSelect() {
		((Button)getFellow("editBtn")).setDisabled(false);
		((Button)getFellow("copyBtn")).setDisabled(false);
		((Button)getFellow("deleteBtn")).setDisabled(false);
	}

	/**
	 * (Re)render list items.
	 */
	public void update() {
		CCdePreviewItemsHelper cHelper = new CCdePreviewItemsHelper();
		cHelper.setItem(getItem());
		cHelper.renderItems(this);
	}

	/**
	 * Deletes role status for aRunGroupAccount.
	 * Also removes references to aRunGroupAccount within run and run team status.
	 *
	 * @param aRunGroupAccount the a run group account
	 */
	public void deleteRoleStatus(IERunGroupAccount aRunGroupAccount) {
		IECase lCase = aRunGroupAccount.getERunGroup().getERun().getECase();
		List<IECaseComponent> lCaseComponents = ((ICaseComponentManager) (CDesktopComponents.sSpring().getBean("caseComponentManager")))
				.getAllCaseComponentsByCasId(lCase.getCasId());
		for (IECaseComponent lCaseComponent : lCaseComponents) {
			CDesktopComponents.sSpring().setRunGroupCaseComponentStatus(aRunGroupAccount.getERunGroup().getRugId(), lCaseComponent.getCacId(), "");
			removeFromRunStatus(lCaseComponent, aRunGroupAccount);
			removeFromRunTeamStatus(lCaseComponent, aRunGroupAccount);
		}
	}

	/**
	 * Removes references to aRunGroupAccount from run status of aCaseComponent.
	 *
	 * @param aCaseComponent the a case component
	 * @param aRunGroupAccount the a run group account
	 */
	private void removeFromRunStatus(IECaseComponent aCaseComponent, IERunGroupAccount aRunGroupAccount) {
		List<IERunCaseComponent> lRunCaseComponents = ((IRunCaseComponentManager) (CDesktopComponents.sSpring().getBean("runCaseComponentManager")))
				.getAllRunCaseComponentsByCacId(aCaseComponent.getCacId());
		int lRunId = aRunGroupAccount.getERunGroup().getERun().getRunId();
		for (IERunCaseComponent lRunCaseComponent : lRunCaseComponents) {
			if (lRunCaseComponent.getRunRunId() == lRunId) {
				IXMLTag lRootTag = CDesktopComponents.sSpring().getXmlRunStatusTree(lRunCaseComponent);
				boolean lChanged = removeReferencesToRunGroupAccount(lRootTag, aRunGroupAccount);
				if (lChanged) {
					String lXmlData = CDesktopComponents.sSpring().getXmlManager().xmlTreeToDoc(lRootTag);
					CDesktopComponents.sSpring().setRunCaseComponentStatus(aRunGroupAccount.getERunGroup().getERun().getRunId(), lRunCaseComponent.getCacCacId(), lXmlData);
				}
			}
		}
	}

	/**
	 * Removes references to aRunGroupAccount from run team status of aCaseComponent.
	 *
	 * @param aCaseComponent the a case component
	 * @param aRunGroupAccount the a run group account
	 */
	private void removeFromRunTeamStatus(IECaseComponent aCaseComponent, IERunGroupAccount aRunGroupAccount) {
		List<IERunTeamCaseComponent> lRunTeamCaseComponents = ((IRunTeamCaseComponentManager) (CDesktopComponents.sSpring().getBean("runTeamCaseComponentManager")))
				.getAllRunTeamCaseComponentsByCacId(aCaseComponent.getCacId());
		int lRunId = aRunGroupAccount.getERunGroup().getERun().getRunId();
		IRunTeamManager lBean = (IRunTeamManager)CDesktopComponents.sSpring().getBean("runTeamManager");
		for (IERunTeamCaseComponent lRunTeamCaseComponent : lRunTeamCaseComponents) {
			if (lBean.getRunTeam(lRunTeamCaseComponent.getRutRutId()).getERun().getRunId() == lRunId) {
				IXMLTag lRootTag = CDesktopComponents.sSpring().getXmlRunTeamStatusTree(lRunTeamCaseComponent);
				boolean lChanged = removeReferencesToRunGroupAccount(lRootTag, aRunGroupAccount);
				if (lChanged) {
					String lXmlData = CDesktopComponents.sSpring().getXmlManager().xmlTreeToDoc(lRootTag);
					CDesktopComponents.sSpring().setRunTeamCaseComponentStatus(lRunTeamCaseComponent.getRutRutId(), lRunTeamCaseComponent.getCacCacId(), lXmlData);
				}
			}
		}
	}

	/**
	 * Removes references to run group account within aStatusRootTag.
	 *
	 * @param aStatusRootTag the a status root tag
	 * @param aRunGroupAccount the a run group account
	 *
	 * @return true, if aRootTag tree is changed
	 */
	private boolean removeReferencesToRunGroupAccount(IXMLTag aStatusRootTag,  IERunGroupAccount aRunGroupAccount) {
		if (aStatusRootTag == null)
			return false;
		String lOwnerRgaId = "" + aRunGroupAccount.getRgaId();
		IXMLTag lStatusContentTag = aStatusRootTag.getChild(AppConstants.contentElement);
		if (lStatusContentTag == null)
			return false;
		boolean lChanged = false;
		for (IXMLTag lStatusTag : lStatusContentTag.getChildTags()) {
			if (lStatusTag.getAttribute(AppConstants.keyOwner_rgaid).equals(lOwnerRgaId)) {
				if (!lStatusTag.getCurrentStatusAttribute(AppConstants.statusKeyDeleted).equals(AppConstants.statusValueTrue)) {
					CDesktopComponents.sSpring().setRunTagStatusValue(lStatusTag, AppConstants.statusKeyDeleted, AppConstants.statusValueTrue);
					lChanged = true;
				}
			}
		}
		return lChanged;
	}

	/**
	 * Is team case.
	 *
	 * @return true, if successful
	 */
	public boolean isTeamCase() {
		if (teamCase.equals("")) {
			if (caseComponentsByStatusType == null) {
				CControl cControl = CDesktopComponents.cControl();
				IEAccount eAccount = CDesktopComponents.sSpring().getAccount();
				IECase eCase = (IECase) cControl.getAccSessAttr("case");
				IERun eRun = ((IRunManager) CDesktopComponents.sSpring().getBean("runManager")).getTestRun(eAccount, eCase);
				caseComponentsByStatusType = CDesktopComponents.sSpring().getCaseComponentsByStatusType(eRun.getECase(), AppConstants.statusTypeRunTeam);
			}
			teamCase = "" + (caseComponentsByStatusType.size() > 0);
		}
		return (teamCase.equals("true"));
	}
}
