/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde;

import org.zkoss.zul.Listitem;

import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefButton;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;

/**
 * The Class CCdeCaseComponentRedirectBtn.
 */
public class CCdeCaseComponentRedirectBtn extends CDefButton {

	private static final long serialVersionUID = -4456778221640791483L;

	/**
	 * Instantiates a new c cde case component redirect btn.
	 */
	public CCdeCaseComponentRedirectBtn() {
		setLabel(CDesktopComponents.vView().getLabel("content"));
	}

	/**
	 * On click open case component edit content window for the selected case component.
	 */
	public void onClick() {
		IECaseComponent lItem = (IECaseComponent)((Listitem)getParent().getParent()).getValue();
		CControl cControl = CDesktopComponents.cControl();
		cControl.setAccSessAttr("casecomponent",lItem);
		cControl.setAccSessAttr("cacId",""+lItem.getCacId());
		cControl.setAccSessAttr("cacAccId",""+lItem.getEAccount().getAccId());
		cControl.setAccSessAttr("cacComId",""+lItem.getEComponent().getComId());
		cControl.setAccSessAttr("cacComCode",""+lItem.getEComponent().getCode());
		cControl.setAccSessAttr("cacName",""+lItem.getName());
		cControl.setAccSessAttr("is_author",""+(Integer.parseInt((String)CDesktopComponents.cControl().getAccSessAttr("cacAccId")) == CDesktopComponents.cControl().getAccId()));
		CDesktopComponents.vView().redirectToView(VView.v_cde_components);
//		for popup, but Zk doesn't allow more popups of same zul-page, so using popup is not functional
//		CDesktopComponents.vView().redirectToView(CDesktopComponents.vView().v_cde_components,"null");
	}
}
