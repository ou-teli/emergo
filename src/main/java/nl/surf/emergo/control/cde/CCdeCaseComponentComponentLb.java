/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde;

import java.util.List;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CInputListbox;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IEComponent;
import nl.surf.emergo.view.VView;

/**
 * The Class CCdeCaseComponentComponentLb.
 */
public class CCdeCaseComponentComponentLb extends CInputListbox {

	private static final long serialVersionUID = 2800039125001836857L;

	public CCdeCaseComponentComponentLb() {
		super();
	}

	/**
	 * On create show possible components and preselect one if applicable. Only show functional components.
	 * 
	 * @param aEvent the a event
	 */
	public void onCreate(CreateEvent aEvent) {
		IECaseComponent lItem = (IECaseComponent)((CCdeCaseComponentWnd)getRoot()).getItem(aEvent);
		Listitem lListitem = super.insertListitem(null,CDesktopComponents.vView().getLabel("none"),null);
		setSelectedItem(lListitem);

		CCdeCaseComponentHelper caseComponentHelper = new CCdeCaseComponentHelper();
		int lCasId = Integer.parseInt((String)CDesktopComponents.cControl().getAccSessAttr("casId"));
		List<IEComponent> lPossibleComponents = caseComponentHelper.getPossibleComponents(lCasId, (lItem == null));
		for (IEComponent lComponent : lPossibleComponents) {
			String lLabel = CDesktopComponents.vView().getLabel(VView.componentLabelKeyPrefix + lComponent.getCode());
			if (lLabel.length() == 0) {
				lLabel = lComponent.getCode();
			}
			lListitem = super.insertListitem(lComponent, lLabel, null);
			if (lItem != null && lComponent.getComId() == lItem.getEComponent().getComId()) {
				setSelectedItem(lListitem);
				setDisabled(true);
			}
		}
	}

}
