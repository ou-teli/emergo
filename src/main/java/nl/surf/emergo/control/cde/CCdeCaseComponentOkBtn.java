/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde;

import java.util.ArrayList;
import java.util.List;

import nl.surf.emergo.business.ICaseComponentManager;
import nl.surf.emergo.business.ICaseManager;
import nl.surf.emergo.control.CControlHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefButton;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IEComponent;

/**
 * The Class CCdeCaseComponentOkBtn.
 */
public class CCdeCaseComponentOkBtn extends CDefButton {

	private static final long serialVersionUID = -6516875258112231842L;

	/**
	 * On click create new item or update item or copy item if no errors and close edit window.
	 * If copy, xml dependencies are copied too.
	 * Case component roles are created/updated/deleted too, depending on checked of checkboxes for case roles.
	 */
	public void onClick() {
		CCdeCaseComponentWnd lWindow = (CCdeCaseComponentWnd)getRoot();
		IECaseComponent lItem = (IECaseComponent)lWindow.getItem();
		String lAction = lWindow.getAction();
		ICaseComponentManager caseComponentManager = (ICaseComponentManager)CDesktopComponents.sSpring().getBean("caseComponentManager");
		ICaseManager caseManager = (ICaseManager)CDesktopComponents.sSpring().getBean("caseManager");
		int lCasId = Integer.parseInt((String)CDesktopComponents.cControl().getAccSessAttr("casId"));
		boolean lNew = (lAction.equals("new"));
		boolean lCopy = (lAction.equals("copy"));
		IECaseComponent lOldItem = lItem;
		if (lItem == null) {
			//new
			lItem = caseComponentManager.getNewCaseComponent();
			lItem.setECase(caseManager.getCase(lCasId));
		}
		else {
			//update
			lItem = (IECaseComponent)lItem.clone();
		}
		lItem.setEComponent((IEComponent)CControlHelper.getListboxValue(this, "comComId"));
		lItem.setName(CControlHelper.getTextboxValue(this, "name"));
		if (lItem.getXmldata() == null || lItem.getXmldata().equals("")) {
			lItem.setXmldata(CDesktopComponents.sSpring().getEmptyXml());
		}
		lItem.setEAccount((IEAccount)CControlHelper.getListboxValue(this, "accAccId"));

		List<String[]> lErrors = new ArrayList<String[]>(0);
		if (lNew || lCopy) {
			// set to 0 to get new record in db
			lItem.setCacId(0);
			lErrors = caseComponentManager.newCaseComponent(lItem);
		}
		else
			lErrors = caseComponentManager.updateCaseComponent(lItem);
		if ((lErrors == null) || (lErrors.size() == 0)) {
//			Get casecomponent so date format, name etc. is correct.
			lItem = caseComponentManager.getCaseComponent(lItem.getCacId());
			if (lCopy)
				lItem = CDesktopComponents.sSpring().copyXmlDependencies(lOldItem,lItem);
			CCdeCaseComponentHelper caseComponentHelper = new CCdeCaseComponentHelper(); 
			caseComponentHelper.handleCaseComponentRoles(lCasId, lItem, this);
			lWindow.setAttribute("item",lItem);
		}
		else {
			lWindow.setAttribute("item",null);
			CDesktopComponents.cControl().showErrors(this,lErrors);
		}

		lWindow.detach();
	}
}
