/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.cde;

import java.util.Hashtable;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.CreateEvent;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CContentHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.CInputWnd;

/**
 * The Class CCdeComponentItemWnd.
 * 
 * Is used by case developer.
 * Is used to show a modal popup dialog to create or edit a component item.
 * The component item is rendered using string nodename (for create) or xml tag item (for edit).
 */
public class CCdeComponentItemWnd extends CInputWnd {

	private static final long serialVersionUID = 2983769745598669256L;

	/** The parent item of the xml tag that should be edited. */
	protected IXMLTag parentitem = null;

	/** The item, the xml tag that should be edited. */
	protected IXMLTag item = null;

	/** If new item, that is new item. */
	protected boolean newitem = false;

	/** The nodename, the name of newly created item. */
	protected String nodename = "";

	/** The context, the context of the item. */
	protected Hashtable<String,Object> nodecontext = null;

	/** The newnodetype, the type of newly created item. */
	protected String newnodetype = "";

	/** The subtype. */
	protected String subtype = "";

	/** The subject. */
	protected String subject = "";

	/** The content helper. */
	protected CContentHelper contentHelper = null;
	
	/**
	 * Gets the item.
	 * 
	 * @return the item
	 */
	public IXMLTag getItem() {
		return item;
	}

	/**
	 * Sets the item.
	 * 
	 * @param aItem the item
	 */
	public void setItem(IXMLTag aItem) {
		item = aItem;
	}

	/**
	 * Gets the parent item.
	 * 
	 * @return the parent item
	 */
	public IXMLTag getParentitem() {
		return parentitem;
	}

	/**
	 * Sets the parent item.
	 * 
	 * @param aParentitem the parent item
	 */
	public void setParentitem(IXMLTag aParentitem) {
		parentitem = aParentitem;
	}

	/**
	 * Gets if new item.
	 * 
	 * @return true, if new item
	 */
	public boolean getNewitem() {
		return newitem;
	}

	/**
	 * Sets if new item.
	 * 
	 * @param aNewitem the state
	 */
	public void setNewitem(boolean aNewitem) {
		newitem = aNewitem;
	}

	/**
	 * Gets the node name.
	 * 
	 * @return the nodename
	 */
	public String getNodename() {
		return nodename;
	}

	/**
	 * Sets the node name.
	 * 
	 * @param aNodename the new nodename
	 */
	public void setNodename(String aNodename) {
		if (aNodename == null)
			aNodename = "";
		nodename = aNodename;
	}

	/**
	 * Gets the nodecontext.
	 * 
	 * @return the nodecontext
	 */
	public Hashtable<String,Object> getNodecontext() {
		return nodecontext;
	}

	/**
	 * Sets the nodecontext.
	 * 
	 * @param aNodecontext the new nodecontext
	 */
	public void setNodecontext(Hashtable<String,Object> aNodecontext) {
		nodecontext = aNodecontext;
	}

	/**
	 * Gets the new node type.
	 * 
	 * @return the newnodetype
	 */
	public String getNewnodetype() {
		return newnodetype;
	}

	/**
	 * Sets the new node type.
	 * 
	 * @param aNewnodetype the new nodename
	 */
	public void setNewnodetype(String aNewnodetype) {
		if (aNewnodetype == null)
			aNewnodetype = "";
		newnodetype = aNewnodetype;
	}

	/**
	 * Gets the subtype.
	 * 
	 * @return the subtype
	 */
	public String getSubtype() {
		return subtype;
	}

	/**
	 * Sets the subtype.
	 * 
	 * @param aSubtype the subtype
	 */
	public void setSubtype(String aSubtype) {
		if (aSubtype == null)
			aSubtype = "";
		subtype = aSubtype;
	}

	/**
	 * Gets the subject.
	 * 
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * Sets the subject.
	 * 
	 * @param aSubject the subject
	 */
	public void setSubject(String aSubject) {
		if (aSubject == null)
			aSubject = "";
		subject = aSubject;
	}

	/**
	 * Gets the content helper.
	 * 
	 * @return the content helper
	 */
	public CContentHelper getContentHelper() {
		return contentHelper;
	}

	/**
	 * Sets the content helper.
	 * 
	 * @param aContentHelper the a content helper
	 */
	public void setContentHelper(CContentHelper aContentHelper) {
		contentHelper = aContentHelper;
	}

	/**
	 * On create, get arguments given to window and save them.
	 * Show correct window title. Render input form.
	 * 
	 * @param aEvent the create event
	 */
	public void onCreate(CreateEvent aEvent) {
		super.onCreate(aEvent);
		// get arguments out of create event and save them
		setItem((IXMLTag) (aEvent.getArg()).get("item"));
		setNewitem(((aEvent.getArg()).get("new").equals("true")));
		setNodename((String) (aEvent.getArg()).get("nodename"));
		setNodecontext((Hashtable<String,Object>) (aEvent.getArg()).get("nodecontext"));
		setNewnodetype((String) (aEvent.getArg()).get("newnodetype"));
		setContentHelper((CContentHelper) (aEvent.getArg()).get("contenthelper"));

		// show correct window title
		String lAction = "";
		if (getNewitem()) {
			lAction = CDesktopComponents.vView().getCLabel("new");
		}
		else {
			lAction = CDesktopComponents.vView().getCLabel("edit");
		}
		CContentHelper cComponent = getContentHelper();
		String lSubject = CContentHelper.getNodeTagLabel(cComponent.currentCaseComponentCode, getNodename());
		setTitle(CDesktopComponents.vView().getLabel("title.template").replace("%1", lAction).replace("%2", lSubject));
		// get current xml tree
		// cComponent uses session variables to get current xml tree
		IXMLTag lXmlTree = cComponent.getXmlDataTree();
		// render xml tag input form within ZK rows object on screen
		cComponent.xmlNodeToRows(lXmlTree, getNewitem(), getItem(), getNodename(), getNewnodetype(), (Component)CDesktopComponents.vView().getComponent("contentRws"));
	}

	/**
	 * Checks if current account is author of current case component.
	 * 
	 * @return true, if successful
	 */
	public boolean accIsAuthor() {
		// get current case component owner account id, is set before
		String lCacAccId = (String)CDesktopComponents.cControl().getAccSessAttr("cacAccId");
		// get current account id, is set before
		int lAccId = CDesktopComponents.cControl().getAccId();
		if (lCacAccId == null || lAccId == 0)
			// should both be set
			return false;
		// must be equal
		return (Integer.parseInt(lCacAccId) == lAccId);
	}

}
