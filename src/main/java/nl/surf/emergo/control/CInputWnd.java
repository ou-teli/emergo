/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import org.zkoss.zk.ui.event.CreateEvent;

/**
 * The Class CInputWnd.
 */
public class CInputWnd extends CWnd {

	private static final long serialVersionUID = 2437183407898352178L;
	
	public CInputWnd() {
		setVisible(false);
	}
	
	/**
	 * On create set title and layout.
	 * 
	 * @param aEvent the a event
	 */
	public void onCreate(CreateEvent aEvent) {
		if (handleAccess()) {
			CDesktopComponents.cControl().handleLanguage(CDesktopComponents.vView().getCurrentUrl());
		}
	}
	
	protected boolean handleAccess() {
		boolean hasAccess = false;
		if (CDesktopComponents.cControl().getAccId() > 0) {
			String role = (String)getAttribute("role");
			if (role != null) {
				if (role.equals("*")) {
					hasAccess = true;
				}
				else {
					hasAccess = CDesktopComponents.sSpring().hasRole(role);
				}
			}
		}
		if (hasAccess) {
			setVisible(true);
		}
		else {
			CDesktopComponents.vView().forbidden();
		}
		return hasAccess;
	}
	
}
