/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import java.util.ArrayList;
import java.util.List;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.zkspring.SSpring;

/**
 * The Class CCaseHelper.
 *
 * Is used for manipulating xml data of the case system component 'case'.
 * This component holds all references between other case components, for instance
 * at which locations a conversation can take place. It functions as a cross table
 * for the xml data of all other case components. There are different reference types
 * as child of the content root which can be seen as different cross tables.
 */
public class CCaseHelper extends CXmlHelper {

	protected IXMLTag caseCaseComponentRootTag = null;
	
	/**
	 * Instantiates a new case helper.
	 */
	public CCaseHelper() {
		super();
	}

	/**
	 * Instantiates a new case helper.
	 *
	 * @param sSpring the spring object to use
	 */
	public CCaseHelper(SSpring sSpring) {
		super(sSpring);
	}

	/**
	 * Gets the 'case' case component content tag.
	 *
	 * @return the 'case' case component content tag
	 */
	public IXMLTag getCaseCaseComponentContentTag() {
		if (CDesktopComponents.sSpring().inRun()) {
			if (caseCaseComponentRootTag == null) {
				IECaseComponent lCaseCaseComponent = CDesktopComponents.sSpring().getCaseComponent(CDesktopComponents.sSpring().getCase(), "case", "");
				caseCaseComponentRootTag = CDesktopComponents.sSpring().getXmlDataTree(lCaseCaseComponent);
			}
			if (caseCaseComponentRootTag == null)
				return null;
			return caseCaseComponentRootTag.getChild(AppConstants.contentElement);
		}
		else {
			// there is only one instance of the 'case' case component
			// every time read the content before it is changed, because other authors can change the case case component content too, simultanously
			IECaseComponent lCaseCaseComponent = CDesktopComponents.sSpring().getCaseComponent(CDesktopComponents.sSpring().getCase(), "case", "");
			return CDesktopComponents.sSpring().getCaseComponentContentTag(lCaseCaseComponent);
		}
	}

	/**
	 * Sets the 'case' case component content tag.
	 *
	 * @param aContentTag the 'case' case component content tag
	 */
	public void setCaseCaseComponentContentTag(IXMLTag aContentTag) {
		// there is only one instance of the 'case' case component
		// every time read the content before it is changed, because other authors can change the case case component content too, simultanously
		IECaseComponent lCaseCaseComponent = CDesktopComponents.sSpring().getCaseComponent(CDesktopComponents.sSpring().getCase(), "case", "");
		IXMLTag lContentTag = CDesktopComponents.sSpring().getCaseComponentContentTag(lCaseCaseComponent);
		if (lContentTag == null)
			return;
		lContentTag.setChildTags(aContentTag.getChildTags());
		String lXml = CDesktopComponents.sSpring().getXmlManager().xmlTreeToDoc(lContentTag.getParentTag());
		setXmlData("" + lCaseCaseComponent.getCacId(), lXml);
	}

	/**
	 * Sets reference ids for tag aTag of case component given by aCacId.
	 *
	 * @param aCaseComponent the case component
	 * @param aReferenceType the reference type
	 * @param aTag the tag
	 * @param aRefTagIds the new ref tag ids
	 */
	public void setReferenceIds(IECaseComponent aCaseComponent, String aReferenceType, IXMLTag aTag, List<String> aRefTagIds) {
		IXMLTag lCaseCaseComponentContentTag = getCaseCaseComponentContentTag();
		if (lCaseCaseComponentContentTag == null)
			return;
		List<String> lReferenceTypeList = new ArrayList<String>();
		lReferenceTypeList.add(aReferenceType);
		List<IXMLTag> lTagList = new ArrayList<IXMLTag>();
		lTagList.add(aTag);
		List<List<String>> lRefTagIdsList = new ArrayList<List<String>>();
		lRefTagIdsList.add(aRefTagIds);
		setReferenceIds(lCaseCaseComponentContentTag, aCaseComponent, lReferenceTypeList, lTagList, lRefTagIdsList);			
		setCaseCaseComponentContentTag(lCaseCaseComponentContentTag);
	}

	/**
	 * Sets reference ids for tag list aTagList of case component given by aCacId.
	 *
	 * @param aCaseComponent the case component
	 * @param aReferenceTypeList the reference type list
	 * @param aTagList the tag list
	 * @param aRefTagIdsList the new ref tag ids list
	 */
	public void setReferenceIds(IECaseComponent aCaseComponent, List<String> aReferenceTypeList, List<IXMLTag> aTagList, List<List<String>> aRefTagIdsList) {
		IXMLTag lCaseCaseComponentContentTag = getCaseCaseComponentContentTag();
		if (lCaseCaseComponentContentTag == null)
			return;
		setReferenceIds(lCaseCaseComponentContentTag, aCaseComponent, aReferenceTypeList, aTagList, aRefTagIdsList);			
		setCaseCaseComponentContentTag(lCaseCaseComponentContentTag);
	}

	/**
	 * Sets reference ids for tag list aTagList of case component given by aCacId.
	 *
	 * @param aCaseCaseComponentContentTag the a case case component content tag
	 * @param aCaseComponent the case component
	 * @param aReferenceTypeList the reference type list
	 * @param aTagList the tag list
	 * @param aRefTagIdsList the new ref tag ids list
	 */
	public void setReferenceIds(IXMLTag aCaseCaseComponentContentTag, IECaseComponent aCaseComponent, List<String> aReferenceTypeList, List<IXMLTag> aTagList, List<List<String>> aRefTagIdsList) {
		if (aCaseCaseComponentContentTag == null)
			return;
		if (aCaseComponent == null)
			return;
		if (aReferenceTypeList == null || aReferenceTypeList.size() == 0)
			return;
		if (aTagList == null || aTagList.size() != aReferenceTypeList.size())
			return;
		if (aRefTagIdsList == null || aRefTagIdsList.size() != aTagList.size())
			return;
		// get all caseroles for casecomponent
//		List<IECaseRole> lCaseRoles = getCaseRolesByCacId();
		List<IECaseRole> lCaseRoles = CDesktopComponents.sSpring().getSCaseRoleHelper().getCaseRolesByCacId("" + aCaseComponent.getCacId());
		if (lCaseRoles == null)
			return;
		for (int i=0;i<aReferenceTypeList.size();i++) {
			setReferenceIds(aCaseCaseComponentContentTag, aCaseComponent, lCaseRoles, aReferenceTypeList.get(i), aTagList.get(i), aRefTagIdsList.get(i));
		}
	}

	/**
	 * Sets reference ids for tag aTag of case component given by aCacId.
	 *
	 * @param aCaseCaseComponentContentTag the a case case component content tag
	 * @param aCaseComponent the case component
	 * @param aCaseRoles the case roles
	 * @param aReferenceType the reference type
	 * @param aTag the tag
	 * @param aRefTagIds the new ref tag ids
	 */
	public void setReferenceIds(IXMLTag aCaseCaseComponentContentTag, IECaseComponent aCaseComponent, List<IECaseRole> aCaseRoles, String aReferenceType, IXMLTag aTag, List<String> aRefTagIds) {
		IXMLTag lReferenceTypeTag = getXmlTag(aCaseCaseComponentContentTag, aReferenceType, "");
		String lTagId = aTag.getAttribute(AppConstants.keyId);
		// Loop through caseroles
		for (IECaseRole lCaseRole : aCaseRoles) {
			String lSearchIds = "" + lCaseRole.getCarId() + "," + aCaseComponent.getCacId() + "," + lTagId;
			// Loop through reftagids and add ids
			List<String> lRefTagIds = new ArrayList<String>(0);
			if (aRefTagIds != null) {
				for (String lRefTagId : aRefTagIds) {
					lRefTagIds.add(lRefTagId + "," + lSearchIds);
				}
			}
			// Get old reftagids
			List<String> lOldRefTagIds = new ArrayList<String>(0);
			getOldRefTagIds(aReferenceType, lReferenceTypeTag, lSearchIds, lOldRefTagIds);
			// Loop through old ones, if not in new ones remove
			if (lOldRefTagIds != null) {
				for (String lOldRefTagId : lOldRefTagIds) {
					boolean lFound = false;
					if (lRefTagIds != null) {
						for (String lRefTagId : lRefTagIds) {
							if (lRefTagId.equals(lOldRefTagId))
								lFound = true;
						}
					}
					if (!lFound) {
						// remove
						removeTagById(aReferenceType, lReferenceTypeTag, lOldRefTagId);
					}
				}
			}
			// Loop through new ones, if not in old ones add
			if (lRefTagIds != null) {
				for (String lRefTagId : lRefTagIds) {
					boolean lFound = false;
					if (lOldRefTagIds != null) {
						for (String lOldRefTagId : lOldRefTagIds) {
							if (lOldRefTagId.equals(lRefTagId))
								lFound = true;
						}
					}
					if (!lFound) {
						// add
						addTagById(aReferenceType, lReferenceTypeTag, lRefTagId);
					}
				}
			}
		}
	}

	/**
	 * Gets the old ref tag ids.
	 *
	 * @param aReferenceType the reference type
	 * @param aReferenceTypeTag the reference type tag
	 * @param aSearchIds the ids to get ref tag ids for
	 * @param aRefTagIds used to return ref tag ids
	 */
	private void getOldRefTagIds(String aReferenceType, IXMLTag aReferenceTypeTag, String aSearchIds, List<String> aRefTagIds) {
		if (aReferenceTypeTag == null)
			return;
		List<IXMLTag> lChildTags = aReferenceTypeTag.getChildTags();
		// the method which calls this method will alter the children of the reference type tag
		// also deleting tags. that's why childtags are walked through reverse.
		for (int i = (lChildTags.size() - 1); i >= 0; i--) {
			if (lChildTags.size() > 0) {
				// while looping, child tags can be removed so check if still child tags left
				IXMLTag lChildTag = (IXMLTag) lChildTags.get(i);
				if (!lChildTag.getName().equals("reftag")) {
					// if child is not reftag, nest till you find reftag
					// this is deepest item, the leave of the tree
					getOldRefTagIds(aReferenceType, lChildTag, aSearchIds, aRefTagIds);
				}
				else {
					if (aReferenceType.equals(AppConstants.defValueReftypeAllTemplateScriptTags)) {
						pGetTemplateOldRefTagIds(lChildTag, aSearchIds, aRefTagIds);
					}
					else {
						pGetOldRefTagIds(lChildTag, aSearchIds, aRefTagIds);
					}
				}
			}
		}
	}

	/**
	 * Gets the old ref tag ids.
	 *
	 * @param aRefTag the ref tag
	 * @param aReferenceTypeTag the reference type tag
	 * @param aSearchIds the ids to get ref tag ids for
	 * @param aRefTagIds used to return ref tag ids
	 */
	private void pGetOldRefTagIds(IXMLTag aRefTag, String aSearchIds, List<String> aRefTagIds) {
		if (aRefTag == null) {
			return;
		}
		// check if searched reftag id
		String[] lSearchIdsArr = aSearchIds.split(",");
		String lSearchedRefCarId = lSearchIdsArr[0];
		String lSearchedRefCacId = lSearchIdsArr[1];
		String lSearchedRefTagId = lSearchIdsArr[2];
		// if child is reftag, the deepest item, the leave of the tree
		String lRefTagId = aRefTag.getValue();
		if (!lRefTagId.equals(lSearchedRefTagId)) {
			return;
		}
		IXMLTag lParentTag = aRefTag.getParentTag();
		String lRefCacId = "";
		String lRefCarId = "";
		// parent must be refcac
		if (lParentTag != null) {
			lRefCacId = lParentTag.getValue();
			lParentTag = lParentTag.getParentTag();
		}
		// parent must be refcar
		if (lParentTag != null) {
			lRefCarId = lParentTag.getValue();
			lParentTag = lParentTag.getParentTag();
		}
		// parent must be either cac or tag
		String lTagId = "";
		String lCacId = "";
		String lCarId = "";
		String lTriggerId = "";
		if (lParentTag != null) {
			if (lParentTag.getName().equals("tag")) {
				lTagId = lParentTag.getValue();
				if (lParentTag != null) {
					lParentTag = lParentTag.getParentTag();
					// parent must be cac
					lCacId = lParentTag.getValue();
				}
			} else {
				lTagId = "0";
				lCacId = lParentTag.getValue();
			}
		}
		if (lParentTag != null) {
			lParentTag = lParentTag.getParentTag();
			// parent must be car
			lCarId = lParentTag.getValue();
		}
		if (lParentTag != null) {
			// must have parent trigger
			lParentTag = lParentTag.getParentTag();
			lTriggerId = lParentTag.getValue();
		}
		if (!(lTagId.equals("") || lCacId.equals("")
				|| lCarId.equals("") || lTriggerId.equals(""))) {
			if ((lRefCarId.equals(lSearchedRefCarId))
					&& (lRefCacId.equals(lSearchedRefCacId)))
				// found, add it
				aRefTagIds.add(lTriggerId + "," +
						lCarId + "," + lCacId + "," + lTagId + "," +
						aSearchIds);
		}
	}

	/**
	 * Gets the template old ref tag ids.
	 *
	 * @param aReferenceType the reference type
	 * @param aReferenceTypeTag the reference type tag
	 * @param aSearchIds the ids to get ref tag ids for
	 * @param aRefTagIds used to return ref tag ids
	 */
	private void pGetTemplateOldRefTagIds(IXMLTag lChildTag, String aSearchIds, List<String> aRefTagIds) {
		if (lChildTag == null) {
			return;
		}
		// check if searched reftag id
		String[] lSearchIdsArr = aSearchIds.split(",");
		String lSearchedRefCarId = lSearchIdsArr[0];
		String lSearchedRefCacId = lSearchIdsArr[1];
		String lSearchedRefTagId = lSearchIdsArr[2];
		// if child is reftag, the deepest item, the leave of the tree
		String lRefTagId = lChildTag.getValue();
		if (lRefTagId.equals(lSearchedRefTagId)) {
			// check if searched reftag id
			IXMLTag lParentTag = lChildTag.getParentTag();
			String lRefCacId = "";
			String lRefCarId = "";
			// parent must be refcac
			if (lParentTag != null) {
				lRefCacId = lParentTag.getValue();
				lParentTag = lParentTag.getParentTag();
			}
			// parent must be refcar
			if (lParentTag != null) {
				lRefCarId = lParentTag.getValue();
				lParentTag = lParentTag.getParentTag();
			}
			// parent must be either com or tag
			String lTagId = "";
			String lComId = "";
			String lTriggerId = "";
			if (lParentTag != null) {
				if (lParentTag.getName().equals("tag")) {
					lTagId = lParentTag.getValue();
					if (lParentTag != null) {
						lParentTag = lParentTag.getParentTag();
						// parent must be com
						lComId = lParentTag.getValue();
					}
				} else {
					lTagId = "0";
					lComId = lParentTag.getValue();
				}
			}
			if (lParentTag != null) {
				// must have parent trigger
				lParentTag = lParentTag.getParentTag();
				lTriggerId = lParentTag.getValue();
			}
			if (!(lTagId.equals("") || lComId.equals("")
					|| lComId.equals("") || lTriggerId.equals(""))) {
				if ((lRefCarId.equals(lSearchedRefCarId))
						&& (lRefCacId.equals(lSearchedRefCacId)))
					// found, add it
					aRefTagIds.add(lTriggerId + "," +
							lComId + "," + lTagId + "," +
							aSearchIds);
			}
		}
	}

	/**
	 * Removes tag by id.
	 *
	 * @param aReferenceType the reference type
	 * @param aRootTag the root tag
	 * @param aRemoveIds the ids to identify the tag to be removed
	 */
	private void removeTagById(String aReferenceType, IXMLTag aRootTag, String aRemoveIds) {
		if (aReferenceType.equals(AppConstants.defValueReftypeAllTemplateScriptTags)) {
			pRemoveTemplateTagById(aReferenceType, aRootTag, aRemoveIds);
		}
		else {
			pRemoveTagById(aReferenceType, aRootTag, aRemoveIds);
		}
	}

	/**
	 * Removes tag by id.
	 *
	 * @param aReferenceType the reference type
	 * @param aRootTag the root tag
	 * @param aRemoveIds the ids to identify the tag to be removed
	 */
	private void pRemoveTagById(String aReferenceType, IXMLTag aRootTag, String aRemoveIds) {
		String[] lIds = aRemoveIds.split(",");
		IXMLTag lTriggerTag = getChildTag(aRootTag, "trigger", lIds[0]);
		IXMLTag lCarTag = getChildTag(lTriggerTag, "car", lIds[1]);
		IXMLTag lCacTag = getChildTag(lCarTag, "cac", lIds[2]);
		IXMLTag lRefCarTag = null;
		if (lIds[3].equals("0"))
			lRefCarTag = getChildTag(lCacTag, "refcar", lIds[4]);
		else {
			IXMLTag lTagTag = getChildTag(lCacTag, "tag", lIds[3]);
			lRefCarTag = getChildTag(lTagTag, "refcar", lIds[4]);
		}
		IXMLTag lRefCacTag = getChildTag(lRefCarTag, "refcac", lIds[5]);
		if (lRefCacTag == null)
			return;
		removeXmlTag(aReferenceType, lRefCacTag, lIds[6]);
	}

	/**
	 * Removes template tag by id.
	 *
	 * @param aReferenceType the reference type
	 * @param aRootTag the root tag
	 * @param aRemoveIds the ids to identify the tag to be removed
	 */
	private void pRemoveTemplateTagById(String aReferenceType, IXMLTag aRootTag, String aRemoveIds) {
		String[] lIds = aRemoveIds.split(",");
		IXMLTag lTriggerTag = getChildTag(aRootTag, "trigger", lIds[0]);
		IXMLTag lComTag = getChildTag(lTriggerTag, "com", lIds[1]);
		IXMLTag lRefCarTag = null;
		if (lIds[2].equals("0"))
			lRefCarTag = getChildTag(lComTag, "refcar", lIds[3]);
		else {
			IXMLTag lTagTag = getChildTag(lComTag, "tag", lIds[2]);
			lRefCarTag = getChildTag(lTagTag, "refcar", lIds[3]);
		}
		IXMLTag lRefCacTag = getChildTag(lRefCarTag, "refcac", lIds[4]);
		if (lRefCacTag == null)
			return;
		removeXmlTag(aReferenceType, lRefCacTag, lIds[5]);
	}

	/**
	 * Adds tag by id.
	 *
	 * @param aReferenceType the reference type
	 * @param aRootTag the root tag
	 * @param aAddIds the ids that define the tag to be added
	 */
	private void addTagById(String aReferenceType, IXMLTag aRootTag, String aAddIds) {
		if (aReferenceType.equals(AppConstants.defValueReftypeAllTemplateScriptTags)) {
			pAddTemplateTagById(aRootTag, aAddIds);
		}
		else {
			pAddTagById(aRootTag, aAddIds);
		}
	}

	/**
	 * Adds tag by id.
	 *
	 * @param aRootTag the root tag
	 * @param aAddIds the ids that define the tag to be added
	 */
	private void pAddTagById(IXMLTag aRootTag, String aAddIds) {
		String[] lIds = aAddIds.split(",");
		IXMLTag lTriggerTag = getXmlTag(aRootTag, "trigger", lIds[0]);
		IXMLTag lCarTag = getXmlTag(lTriggerTag, "car", lIds[1]);
		IXMLTag lCacTag = getXmlTag(lCarTag, "cac", lIds[2]);
		IXMLTag lRefCarTag = null;
		if (lIds[3].equals("0"))
			lRefCarTag = getXmlTag(lCacTag, "refcar", lIds[4]);
		else {
			IXMLTag lTagTag = getXmlTag(lCacTag, "tag", lIds[3]);
			lRefCarTag = getXmlTag(lTagTag, "refcar", lIds[4]);
		}
		IXMLTag lRefCacTag = getXmlTag(lRefCarTag, "refcac", lIds[5]);
		// adds xmltag
		getXmlTag(lRefCacTag, "reftag", lIds[6]);
	}

	/**
	 * Adds template tag by id.
	 *
	 * @param aRootTag the root tag
	 * @param aAddIds the ids that define the tag to be added
	 */
	private void pAddTemplateTagById(IXMLTag aRootTag, String aAddIds) {
		String[] lIds = aAddIds.split(",");
		IXMLTag lTriggerTag = getXmlTag(aRootTag, "trigger", lIds[0]);
		IXMLTag lComTag = getXmlTag(lTriggerTag, "com", lIds[1]);
		IXMLTag lRefCarTag = null;
		if (lIds[2].equals("0"))
			lRefCarTag = getXmlTag(lComTag, "refcar", lIds[3]);
		else {
			IXMLTag lTagTag = getXmlTag(lComTag, "tag", lIds[2]);
			lRefCarTag = getXmlTag(lTagTag, "refcar", lIds[3]);
		}
		IXMLTag lRefCacTag = getXmlTag(lRefCarTag, "refcac", lIds[4]);
		// adds xmltag
		getXmlTag(lRefCacTag, "reftag", lIds[5]);
	}

	/**
	 * Gets the xml tag with given name and value within childs of aParentTag.
	 * If it does not exist yet it will be created and added as child.
	 *
	 * @param aParentTag the parent tag
	 * @param aTagName the tag name
	 * @param aTagValue the tag value
	 *
	 * @return the xml tag
	 */
	private IXMLTag getXmlTag(IXMLTag aParentTag, String aTagName, String aTagValue) {
		List<IXMLTag> lChildTags = aParentTag.getChilds(aTagName);
		IXMLTag lChildTag = null;
		for (IXMLTag lTag : lChildTags) {
			if (lTag.getValue().equals(aTagValue))
				lChildTag = lTag;
		}
		if (lChildTag == null) {
			lChildTag = CDesktopComponents.sSpring().getXmlManager().newXMLTag(aTagName, aTagValue);
			lChildTag.setParentTag(aParentTag);
			lChildTags = aParentTag.getChildTags();
			lChildTags.add(lChildTag);
		}
		return lChildTag;
	}

	/**
	 * Removes xml tag with value aValue out of childs of aParentTag.
	 * If it is the last child of aParentTag, the parent tag will be removed also.
	 * This nesting stops when the tag with name aReferenceType or 'content' is reached.
	 *
	 * @param aReferenceType the reference type
	 * @param aParentTag the parent tag
	 * @param aValue the value
	 */
	private void removeXmlTag(String aReferenceType, IXMLTag aParentTag, String aValue) {
		if (aParentTag == null)
			return;
		List<IXMLTag> lChildTags = aParentTag.getChildTags();
		int lChildTagsOriginalSize = lChildTags.size();
		// tags will be deleted, so walk in reverse order
		for (int i = (lChildTags.size() - 1); i >= 0; i--) {
			if (lChildTags.size() > 0 && i < lChildTags.size()) {
				// while looping, child tags can be removed so check if still child tags left
				IXMLTag lChildTag = (IXMLTag) lChildTags.get(i);
				if (lChildTag.getValue().equals(aValue) && lChildTag.getChildTags().size() == 0) {
					lChildTags.remove(i);
				}
			}
		}
		if ((lChildTagsOriginalSize > 0) && (lChildTags.size() == 0)
				&& (!aParentTag.getName().equals(aReferenceType))
				&& (!aParentTag.getName().equals(AppConstants.contentElement))) {
			// if originally there were childtags and not anymore after loop
			// and if name of parent tag is unequal to aReferenceType and 'content'
			// remove parent tag
			
			removeXmlTag(aReferenceType, aParentTag.getParentTag(), aParentTag.getValue());
		}
	}

	/**
	 * Gets the child tags.
	 *
	 * @param aParent the parent
	 * @param aTagName the tag name
	 *
	 * @return the child tags
	 */
	private List<IXMLTag> getChildTags(IXMLTag aParent, String aTagName) {
		if (aParent == null)
			return null;
		return aParent.getChilds(aTagName);
	}

	/**
	 * Gets the ref ids to given case role and case component.
	 *
	 * @param aReferenceType the reference type
	 * @param aTriggerId the trigger id
	 * @param aCaseRole the case role
	 * @param aCaseComponent the case component
	 *
	 * @return the cac ref ids
	 */
	public List<String> getCacRefIds(String aReferenceType, String aTriggerId, IECaseRole aCaseRole, IECaseComponent aCaseComponent) {
		List<String> lIds = new ArrayList<String>(0);
		if (aCaseComponent == null)
			return lIds;
		if (aCaseRole == null)
			return lIds;
		IXMLTag lContentTag = getCaseCaseComponentContentTag();
		if (lContentTag == null)
			return lIds;
		IXMLTag lReferenceTypeTag = getXmlTag(lContentTag, aReferenceType, "");
		String lCarId = "" + aCaseRole.getCarId();
		String lCacId = "" + aCaseComponent.getCacId();
		IXMLTag lTriggerTag = getChildTag(lReferenceTypeTag, "trigger",
				aTriggerId);
		IXMLTag lCarTag = getChildTag(lTriggerTag, "car", lCarId);
		IXMLTag lCacTag = getChildTag(lCarTag, "cac", lCacId);
		IXMLTag lRefCarTag = getChildTag(lCacTag, "refcar", lCarId);
		List<IXMLTag> lRefCacTags = getChildTags(lRefCarTag, "refcac");
		if (lRefCacTags == null)
			return lIds;
		for (IXMLTag lRefCacTag : lRefCacTags) {
			List<IXMLTag> lRefTagTags = lRefCacTag.getChildTags();
			for (IXMLTag lRefTagTag : lRefTagTags) {
				lIds.add(lCarId + "," + lRefCacTag.getValue() + "," + lRefTagTag.getValue());
			}
		}
		return lIds;
	}

	/**
	 * Gets the template ref ids to given case role and case component.
	 *
	 * @param aReferenceType the reference type
	 * @param aTriggerId the trigger id
	 * @param aCaseRole the case role
	 * @param aCaseComponent the case component
	 *
	 * @return the cac ref ids
	 */
	public List<String> getTemplateCacRefIds(String aReferenceType, String aTriggerId, IECaseRole aCaseRole, IECaseComponent aCaseComponent) {
		List<String> lIds = new ArrayList<String>(0);
		if (aCaseComponent == null)
			return lIds;
		IXMLTag lContentTag = getCaseCaseComponentContentTag();
		if (lContentTag == null)
			return lIds;
		IXMLTag lReferenceTypeTag = getXmlTag(lContentTag, aReferenceType, "");
		String lComId = "" + aCaseComponent.getEComponent().getComId();
		IXMLTag lTriggerTag = getChildTag(lReferenceTypeTag, "trigger",
				aTriggerId);
		IXMLTag lComTag = getChildTag(lTriggerTag, "com", lComId);
		String lCarId = "" + aCaseRole.getCarId();
		IXMLTag lRefCarTag = getChildTag(lComTag, "refcar", lCarId);
		List<IXMLTag> lRefCacTags = getChildTags(lRefCarTag, "refcac");
		if (lRefCacTags == null)
			return lIds;
		for (IXMLTag lRefCacTag : lRefCacTags) {
			List<IXMLTag> lRefTagTags = lRefCacTag.getChildTags();
			for (IXMLTag lRefTagTag : lRefTagTags) {
				lIds.add(lCarId + "," + lRefCacTag.getValue() + "," + lRefTagTag.getValue());
			}
		}
		return lIds;
	}


	/**
	 * Gets the ref ids to given case role and case component and tag.
	 *
	 * @param aReferenceType the reference type
	 * @param aTriggerId the trigger id
	 * @param aCaseRole the case role
	 * @param aCaseComponent the case component
	 * @param aTag the tag
	 * @param aForAllCaseRoles the for all case roles
	 *
	 * @return the tag ref ids
	 */
	public List<String> getTagRefIds(String aReferenceType, String aTriggerId, IECaseRole aCaseRole, IECaseComponent aCaseComponent, IXMLTag aTag,  boolean aForAllCaseRoles) {
		List<String> lIds = new ArrayList<String>(0);
		if (aCaseComponent == null)
			return lIds;
		if (aCaseRole == null)
			return lIds;
		if (aTag == null)
			return lIds;
		IXMLTag lContentTag = getCaseCaseComponentContentTag();
		if (lContentTag == null)
			return lIds;
		IXMLTag lReferenceTypeTag = getXmlTag(lContentTag, aReferenceType, "");
		String lCarId = "" + aCaseRole.getCarId();
		String lCacId = "" + aCaseComponent.getCacId();
		String lTagId = aTag.getAttribute(AppConstants.keyId);
		IXMLTag lTriggerTag = getChildTag(lReferenceTypeTag, "trigger",
				aTriggerId);
		IXMLTag lCarTag = getChildTag(lTriggerTag, "car", lCarId);
		IXMLTag lCacTag = getChildTag(lCarTag, "cac", lCacId);
		List<IXMLTag> lTagTags = new ArrayList<IXMLTag>();
		IXMLTag lTagTag = getChildTag(lCacTag, "tag", lTagId);
		if (lTagTag != null) {
			lTagTags.add(lTagTag);
		}
		// '-1' if tag template string in condition, so always check that condition
		lTagTag = getChildTag(lCacTag, "tag", "-1");
		if (lTagTag != null) {
			lTagTags.add(lTagTag);
		}
		lIds = getRefIds (lTagTags, lCarId, aForAllCaseRoles);
		return lIds;
	}

	/**
	 * Gets the script condition ids for conditions that only have template references to component content tags.
	 *
	 * @param aTriggerId the trigger id
	 * @param aCaseRole the case role
	 * @param aCaseComponent the case component
	 * @param aTag the tag
	 * @param aForAllCaseRoles the for all case roles
	 *
	 * @return the tag ref ids
	 */
	public List<String> getScriptConditionTemplateIds(String aTriggerId, IECaseRole aCaseRole, IECaseComponent aCaseComponent, IXMLTag aTag,  boolean aForAllCaseRoles) {
		List<String> lIds = new ArrayList<String>(0);
		if (aCaseComponent == null)
			return lIds;
		if (aCaseRole == null)
			return lIds;
		if (aTag == null)
			return lIds;
		IXMLTag lContentTag = getCaseCaseComponentContentTag();
		if (lContentTag == null)
			return lIds;
		IXMLTag lReferenceTypeTag = getXmlTag(lContentTag, AppConstants.defValueReftypeAllScriptTags, "");
		String lCarId = "" + aCaseRole.getCarId();
		String lCacId = "" + aCaseComponent.getCacId();
		String lTagId = aTag.getAttribute(AppConstants.keyId);
		IXMLTag lTriggerTag = getChildTag(lReferenceTypeTag, "trigger",
				aTriggerId);
		IXMLTag lCarTag = getChildTag(lTriggerTag, "car", lCarId);
		IXMLTag lCacTag = getChildTag(lCarTag, "cac", lCacId);
		List<String> lTagIds = new ArrayList<String>(0);
		IXMLTag lTagTag = getChildTag(lCacTag, "tag", lTagId);
		if (lTagTag != null) {
			List<IXMLTag> lTagTags = new ArrayList<IXMLTag>();
			lTagTags.add(lTagTag);
			lTagIds = getRefIds (lTagTags, lCarId, aForAllCaseRoles);
		}
		List<String> lTemplateIds = new ArrayList<String>(0);
		// '-1' if tag template string in condition, so always check that condition
		lTagTag = getChildTag(lCacTag, "tag", "-1");
		if (lTagTag != null) {
			List<IXMLTag> lTagTags = new ArrayList<IXMLTag>();
			lTagTags.add(lTagTag);
			lTemplateIds = getRefIds (lTagTags, lCarId, aForAllCaseRoles);
		}
		// now from the list of ids of conditions with templates, remove ids of conditions that also have direct references to content tags:
		if ((lTagIds.size() > 0) && (lTemplateIds.size() > 0))
			for (String lId : lTagIds)
				if (lTemplateIds.contains(lId))
					lTemplateIds.remove(lId);
		return lTemplateIds;
	}
	
	/**
	 * Gets the template ref ids to given case role and case component and tag.
	 *
	 * @param aReferenceType the reference type
	 * @param aTriggerId the trigger id
	 * @param aCaseRole the case role
	 * @param aCaseComponent the case component
	 * @param aTag the tag
	 * @param aForAllCaseRoles the for all case roles
	 *
	 * @return the tag ref ids
	 */
	public List<String> getTemplateTagRefIds(String aReferenceType, String aTriggerId, IECaseRole aCaseRole, IECaseComponent aCaseComponent, IXMLTag aTag,  boolean aForAllCaseRoles) {
		List<String> lIds = new ArrayList<String>(0);
		if (aCaseComponent == null)
			return lIds;
		if (aCaseRole == null)
			return lIds;
		if (aTag == null)
			return lIds;
		IXMLTag lContentTag = getCaseCaseComponentContentTag();
		if (lContentTag == null)
			return lIds;
		IXMLTag lReferenceTypeTag = getXmlTag(lContentTag, aReferenceType, "");
		String lComId = "" + aCaseComponent.getEComponent().getComId();
		String lTagId = aTag.getAttribute(AppConstants.keyId);
		IXMLTag lTriggerTag = getChildTag(lReferenceTypeTag, "trigger",
				aTriggerId);
		IXMLTag lComTag = getChildTag(lTriggerTag, "com", lComId);
		List<IXMLTag> lTagTags = new ArrayList<IXMLTag>();
		IXMLTag lTagTag = getChildTag(lComTag, "tag", lTagId);
		if (lTagTag != null) {
			lTagTags.add(lTagTag);
		}
		// '-1' if tag template string in condition, so always check that condition
		lTagTag = getChildTag(lComTag, "tag", "-1");
		if (lTagTag != null) {
			lTagTags.add(lTagTag);
		}
		String lCarId = "" + aCaseRole.getCarId();
		lIds = getRefIds (lTagTags, lCarId, aForAllCaseRoles);
		return lIds;
	}

	/**
	 * Extracts the reference ids from a set of reference tags
	 *
	 * @param aTags the reference tags
	 * @param aCarId the case role id
	 * @param aForAllCaseRoles the boolean for all case roles
	 *
	 * @return the tag ref ids
	 */
	private List<String> getRefIds(List<IXMLTag> aTags, String aCarId, boolean aForAllCaseRoles) {
		List<String> lIds = new ArrayList<String>(0);
		List<IXMLTag> lRefCarTags = new ArrayList<IXMLTag>();
		for (IXMLTag lTTag : aTags) {
			if (aForAllCaseRoles) {
				if (lTTag != null) {
					lRefCarTags.addAll(lTTag.getChilds("refcar"));
				}
			}
			else {
				IXMLTag lRefCarTag = getChildTag(lTTag, "refcar", aCarId);
				if (lRefCarTag != null) {
					lRefCarTags.add(lRefCarTag);
				}
			}
		}
		for (IXMLTag lRefCarTag : lRefCarTags) {
			List<IXMLTag> lRefCacTags = getChildTags(lRefCarTag, "refcac");
			if (lRefCacTags != null) {
				for (IXMLTag lRefCacTag : lRefCacTags) {
					List<IXMLTag> lRefTagTags = lRefCacTag.getChildTags();
					for (IXMLTag lRefTagTag : lRefTagTags) {
						String lId = lRefCarTag.getValue() + "," + lRefCacTag.getValue() + "," + lRefTagTag.getValue();
						if (!lIds.contains(lId))
							lIds.add(lId);
					}
				}
			}
		}
		return lIds;
	}
	
	/**
	 * Gets the ref tag ids to given case role id and case component id and tag.
	 *
	 * @param aReferenceType the reference type
	 * @param aTriggerId the trigger id
	 * @param aCarId the db car id
	 * @param aCacId the db cac id
	 * @param aTag the tag
	 *
	 * @return the ref tag ids
	 */
	public List<String> getRefTagIds(String aReferenceType, String aTriggerId, String aCarId, String aCacId, IXMLTag aTag) {
		List<String> lIds = new ArrayList<String>(0);
		if (aTag == null)
			return lIds;
		IXMLTag lContentTag = getCaseCaseComponentContentTag();
		if (lContentTag == null)
			return lIds;
		IXMLTag lReferenceTypeTag = getXmlTag(lContentTag, aReferenceType, "");
		String lCarId = "" + aCarId;
		String lCacId = "" + aCacId;
		String lTagId = aTag.getAttribute(AppConstants.keyId);
		IXMLTag lTriggerTag = getChildTag(lReferenceTypeTag, "trigger", aTriggerId);
		getRefTagIds(lTriggerTag, "reftag", lCarId, lCacId, lTagId, lIds);
		return lIds;
	}

	/**
	 * Gets the ref tag ids to given case role and case component and tag.
	 *
	 * @param aReferenceType the reference type
	 * @param aTriggerId the trigger id
	 * @param aCaseRole the case role
	 * @param aCaseComponent the case component
	 * @param aTag the tag
	 *
	 * @return the ref tag ids
	 */
	public List<String> getRefTagIds(String aReferenceType, String aTriggerId, IECaseRole aCaseRole, IECaseComponent aCaseComponent, IXMLTag aTag) {
		List<String> lIds = new ArrayList<String>(0);
		if ((aCaseComponent == null) || (aCaseRole == null)|| (aTag == null))
			return lIds;
		return getRefTagIds(aReferenceType, aTriggerId, "" + aCaseRole.getCarId(), "" + aCaseComponent.getCacId(), aTag);
	}

	/**
	 * Gets the ref tags to given case role and case component and tag.
	 *
	 * @param aReferenceType the reference type
	 * @param aTriggerId the trigger id
	 * @param aCaseRole the case role
	 * @param aCaseComponent the case component
	 * @param aTag the tag
	 *
	 * @return the ref tags
	 */
	public List<IXMLTag> getRefTags(String aReferenceType, String aTriggerId, IECaseRole aCaseRole, IECaseComponent aCaseComponent, IXMLTag aTag) {
		List<IXMLTag> lRefTags = new ArrayList<IXMLTag>(0);
		List<String> lRefIds = getRefTagIds(aReferenceType,aTriggerId,aCaseRole,aCaseComponent,aTag);
		if (lRefIds.size() == 0)
			return lRefTags;
		for (String lRefId : lRefIds) {
			if ((lRefId != null) && (!lRefId.equals(""))) {
				String[] lIdArr = lRefId.split(",");
				if (lIdArr.length == 3) {
//					String lCarId = lIdArr[0];
					String lCacId = lIdArr[1];
					String lTagId = lIdArr[2];
					// note getCaseComponent for player returns casecomponent for current caserole!
					IXMLTag lRefTag = CDesktopComponents.sSpring().getTag(CDesktopComponents.sSpring().getCaseComponent(lCacId),lTagId);
					if (lRefTag != null)
						lRefTags.add(lRefTag);
				}
			}
		}
		return lRefTags;
	}

	/**
	 * Gets the child tag given by parent, name and value.
	 *
	 * @param aParentTag the parent tag
	 * @param aTagName the tag name
	 * @param aTagValue the tag value
	 *
	 * @return the child tag
	 */
	private IXMLTag getChildTag(IXMLTag aParentTag, String aTagName,
			String aTagValue) {
		if (aParentTag == null)
			return null;
		List<IXMLTag> lChildTags = aParentTag.getChilds(aTagName);
		for (int i = (lChildTags.size() - 1); i >= 0; i--) {
			if (lChildTags.size() > 0) {
				IXMLTag lChildTag = (IXMLTag) lChildTags.get(i);
				if (lChildTag.getValue().equals(aTagValue))
					return lChildTag;
			}
		}
		return null;
	}

	/**
	 * Removes tag id of given caserole and casecomponent out of 'case' case component data.
	 *
	 * @param aContentTag the content tag
	 * @param aReferenceType the reference type
	 * @param aCaseRole the case role
	 * @param aCaseComponent the case component
	 * @param aTag the tag
	 */
	public void removeTagId(IXMLTag aContentTag, String aReferenceType, IECaseRole aCaseRole, IECaseComponent aCaseComponent, IXMLTag aTag) {
		if ((aCaseComponent == null) || (aCaseRole == null)|| (aTag == null))
			return;
		String lCarId = "" + aCaseRole.getCarId();
		String lCacId = "" + aCaseComponent.getCacId();
		String lTagId = aTag.getAttribute(AppConstants.keyId);
		// remove all tags
		String lTagTagName = "tag";
		removeTags(aReferenceType, aContentTag, lTagTagName, lCarId, lCacId, lTagId);
		// remove all reftags
		lTagTagName = "reftag";
		removeTags(aReferenceType, aContentTag, lTagTagName, lCarId, lCacId, lTagId);
	}

	/**
	 * Removes xml tag tags with name aTagTagName, value aTagId and parents given by aCarId and aCacId
	 * out of childs of aParentTag.
	 *
	 * @param aReferenceType the reference type
	 * @param aParentTag the parent tag
	 * @param aCarId the db car id
	 * @param aCacId the db cac id
	 * @param aTagId the tag id
	 * @param aTagName the a tag name
	 */
	private void removeTags(String aReferenceType, IXMLTag aParentTag, String aTagName, String aCarId, String aCacId, String aTagId) {
		if (aParentTag == null)
			return;
		List<IXMLTag> lChildTags = aParentTag.getChildTags();
		int lChildTagsOriginalSize = lChildTags.size();
		// tags will be deleted, so walk in reverse order
		for (int i = (lChildTags.size() - 1); i >= 0; i--) {
			if (lChildTags.size() > 0 && i < lChildTags.size()) {
				// while looping, child tags can be removed so check if still child tags left
				IXMLTag lChildTag = (IXMLTag) lChildTags.get(i);
				boolean lRemoveTag = false;
				if ((lChildTag.getName().equals(aTagName)) && (lChildTag.getValue().equals(aTagId))) {
					IXMLTag lParentTag = lChildTag.getParentTag();
					// Parent of (ref)tag is (ref)cac.
					if ((lParentTag != null) && (lParentTag.getValue().equals(aCacId))) {
						// id is correct
						lParentTag = lParentTag.getParentTag();
						// Parent of (ref)cac is (ref)car
						if ((lParentTag != null) && (lParentTag.getValue().equals(aCarId)))
							// id is correct
							lRemoveTag = true;
					}
				}
				if (lRemoveTag) {
					lChildTags.remove(i);
				}
				else {
					// nesting
					removeTags(aReferenceType, lChildTag, aTagName, aCarId, aCacId, aTagId);
				}
			}
		}
		if ((lChildTagsOriginalSize > 0) && (lChildTags.size() == 0)
				&& (!aParentTag.getName().equals(aReferenceType))
				&& (!aParentTag.getName().equals(AppConstants.contentElement))) {
			// if originally there were childtags and not anymore after loop
			// and if name of parent tag is unequal to aReferenceType and 'content'
			// remove parent tag

			removeXmlTag(aReferenceType, aParentTag.getParentTag(), aParentTag.getValue());
		}
	}

	/**
	 * Gets the ref tag ids given by tag tag name and case role id and case component id and tag id.
	 *
	 * @param aParentTag the parent tag
	 * @param aTagTagName the tag name of the tag tag
	 * @param aCarId the db car id
	 * @param aCacId the db cac id
	 * @param aTagId the tag id
	 * @param aIds used to return the ids
	 */
	private void getRefTagIds(IXMLTag aParentTag, String aTagTagName, String aCarId, String aCacId, String aTagId, List<String> aIds) {
		if (aParentTag == null)
			return;
		List<IXMLTag> lChildTags = aParentTag.getChildTags();
		for (int i = (lChildTags.size() - 1); i >= 0; i--) {
			if (lChildTags.size() > 0) {
				IXMLTag lChildTag = (IXMLTag) lChildTags.get(i);
				boolean lAddTag = false;
				String lTagId = "0";
				String lCacId = "";
				String lCarId = "";
				if ((lChildTag.getName().equals(aTagTagName)) && (lChildTag.getValue().equals(aTagId))) {
					IXMLTag lParentTag = lChildTag.getParentTag();
					// Parent of reftag is refcac
					if ((lParentTag != null) && (lParentTag.getValue().equals(aCacId))) {
						lParentTag = lParentTag.getParentTag();
						// Parent of refcac is refcar
						if ((lParentTag != null) && (lParentTag.getValue().equals(aCarId))) {
							// id is correct
							lParentTag = lParentTag.getParentTag();
							if ((lParentTag != null) && (lParentTag.getName().equals("tag"))) {
								// Parent of refcar is tag or ...
								lTagId = lParentTag.getValue();
								lParentTag = lParentTag.getParentTag();
							}
							if ((lParentTag != null) && (lParentTag.getName().equals("cac"))) {
								// cac
								lCacId = lParentTag.getValue();
							}
							if (lParentTag != null)
								lParentTag = lParentTag.getParentTag();
							if ((lParentTag != null) && (lParentTag.getName().equals("car"))) {
								// Parent of cac is car
								lCarId = lParentTag.getValue();
								lAddTag = true;
							}
						}
					}
				}
				if (lAddTag) {
					if ((!lCarId.equals("")) && (!lCacId.equals("")))
						aIds.add(lCarId + "," + lCacId + "," + lTagId);
				} else {
					getRefTagIds(lChildTag, aTagTagName, aCarId, aCacId, aTagId, aIds);
				}
			}
		}
	}

	/**
	 * Removes car tags given by aCaseRole out of 'case' case component data.
	 *
	 * @param aReferenceType the reference type
	 * @param aCaseRole the case role
	 */
	public void removeCarId(String aReferenceType, IECaseRole aCaseRole) {
		if (aCaseRole == null)
			return;
		IXMLTag lContentTag = getCaseCaseComponentContentTag();
		// remove all cars
		String lTagName = "car";
		String lTagValue = "" + aCaseRole.getCarId();
		removeTags(aReferenceType, lContentTag, lTagName, lTagValue);
		// remove all refcars
		lTagName = "refcar";
		removeTags(aReferenceType, lContentTag, lTagName, lTagValue);
		setCaseCaseComponentContentTag(lContentTag);
	}

	/**
	 * Removes cac tags given by aCaseComponent out of 'case' case component data.
	 *
	 * @param aReferenceType the reference type
	 * @param aCaseComponent the case component
	 */
	public void removeCacId(String aReferenceType, IECaseComponent aCaseComponent) {
		if (aCaseComponent == null)
			return;
		IXMLTag lContentTag = getCaseCaseComponentContentTag();
		// remove all cacs
		String lTagName = "cac";
		String lTagValue = "" + aCaseComponent.getCacId();
		removeTags(aReferenceType, lContentTag, lTagName, lTagValue);
		// remove all refcacs
		lTagName = "refcac";
		removeTags(aReferenceType, lContentTag, lTagName, lTagValue);
		setCaseCaseComponentContentTag(lContentTag);
	}

	/**
	 * Removes tags with combination of caserole as parent and casecomponent as child.
	 *
	 * @param aReferenceType the reference type
	 * @param aCarId the car id
	 * @param aCacId the cac id
	 */
	public void removeCarCacId(String aReferenceType, int aCarId, int aCacId) {
		// remove tags with combination of car and cac
		if ((aCarId == 0) || (aCacId == 0))
			return;
		IXMLTag lContentTag = getCaseCaseComponentContentTag();
		String lPTagValue = "" + aCarId;
		String lCTagValue = "" + aCacId;
		// remove car cac combo's
		String lPTagName = "car";
		String lCTagName = "cac";
		removeParentChildCombinationTags(aReferenceType, lContentTag, lPTagName, lPTagValue, lCTagName, lCTagValue);
		// remove refcar refcac combo's
		lPTagName = "refcar";
		lCTagName = "refcac";
		removeParentChildCombinationTags(aReferenceType, lContentTag, lPTagName, lPTagValue, lCTagName, lCTagValue);
		setCaseCaseComponentContentTag(lContentTag);
	}

	/**
	 * Removes xml tags with name aTagName and value aTagValue out of childs of aParentTag. Does this nested.
	 * If it is the last child, its parent tag will be removed also. This nesting stops when
	 * the tag with name aReferenceType or 'content' is reached.
	 *
	 * @param aReferenceType the reference type
	 * @param aParentTag the parent tag
	 * @param aTagName the tag name
	 * @param aTagValue the tag value
	 */
	private void removeTags(String aReferenceType, IXMLTag aParentTag, String aTagName, String aTagValue) {
		if (aParentTag == null)
			return;
		List<IXMLTag> lChildTags = aParentTag.getChildTags();
		int lChildTagsOriginalSize = lChildTags.size();
		// tags will be deleted, so walk in reverse order
		for (int i = (lChildTags.size() - 1); i >= 0; i--) {
			if (lChildTags.size() > 0 && i < lChildTags.size()) {
				// while looping, child tags can be removed so check if still child tags left
				IXMLTag lChildTag = (IXMLTag) lChildTags.get(i);
				if ((lChildTag.getName().equals(aTagName)) && (lChildTag.getValue().equals(aTagValue))) {
					lChildTags.remove(i);
				}
				else
					// nesting
					removeTags(aReferenceType, lChildTag, aTagName, aTagValue);
			}
		}
		if ((lChildTagsOriginalSize > 0) && (lChildTags.size() == 0)
				&& (!aParentTag.getName().equals(aReferenceType))
				&& (!aParentTag.getName().equals(AppConstants.contentElement))) {
			// if originally there were childtags and not anymore after loop
			// and if name of parent tag is unequal to aReferenceType and 'content'
			// remove parent tag

			removeXmlTag(aReferenceType, aParentTag.getParentTag(), aParentTag.getValue());
		}
	}

	/**
	 * Removes tags with combination given parent name and value and child name and value.
	 *
	 * @param aReferenceType the reference type
	 * @param aParentTag the parent tag
	 * @param aPTagName the parent tag name
	 * @param aPTagValue the parent tag value
	 * @param aCTagName the child tag name
	 * @param aCTagValue the child tag value
	 */
	private void removeParentChildCombinationTags(String aReferenceType, IXMLTag aParentTag,
			String aPTagName, String aPTagValue, String aCTagName, String aCTagValue) {
		if (aParentTag == null)
			return;
		List<IXMLTag> lChildTags = aParentTag.getChildTags();
		int lChildTagsOriginalSize = lChildTags.size();
		// tags will be deleted, so walk in reverse order
		for (int i = (lChildTags.size() - 1); i >= 0; i--) {
			if (lChildTags.size() > 0) {
				// while looping, child tags can be removed so check if still child tags left
				IXMLTag lChildTag = (IXMLTag) lChildTags.get(i);
				boolean lRemoveTag = false;
				if ((lChildTag.getName().equals(aCTagName)) && (lChildTag.getValue().equals(aCTagValue))) {
					IXMLTag lParentTag = lChildTag.getParentTag();
					if ((lParentTag != null)
							&& (lParentTag.getName().equals(aPTagName))
							&& (lParentTag.getValue().equals(aPTagValue)))
						lRemoveTag = true;
				}
				if (lRemoveTag) {
					lChildTags.remove(i);
				}
				else
					// nesting
					removeParentChildCombinationTags(aReferenceType, lChildTag, aPTagName,
							aPTagValue, aCTagName, aCTagValue);
			}
		}
		if ((lChildTagsOriginalSize > 0) && (lChildTags.size() == 0)
				&& (!aParentTag.getName().equals(aReferenceType))
				&& (!aParentTag.getName().equals(AppConstants.contentElement))) {
			// if originally there were childtags and not anymore after loop
			// and if name of parent tag is unequal to aReferenceType and 'content'
			// remove parent tag

			removeXmlTag(aReferenceType, aParentTag.getParentTag(), aParentTag.getValue());
		}
	}

}