/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Hashtable;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zul.Div;
import org.zkoss.zul.Messagebox;

import com.glaforge.i18n.io.CharsetToolkit;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.def.CDefButton;
import nl.surf.emergo.view.VView;

/**
 * The Class CAccountsImportBtn.
 */
public class CAccountsImportBtn extends CDefButton {
	private static final Logger _log = LogManager.getLogger(CAccountsImportBtn.class);
	private static final long serialVersionUID = 7583512022733871597L;

	/** The account manager. */
	protected IAccountManager accountManager = (IAccountManager) CDesktopComponents.sSpring().getBean("accountManager");

	/** The label preface, to be overruled by extending class. */
	protected static String labelPreface = "";

	/** The summary title. */
	protected static String summaryTitle = labelPreface + "summary_title";

	/** The string tab. */
	protected String stringTab = " \t";

	protected boolean changed;

	/**
	 * Instantiates a new c adm upload case streaming files button.
	 *
	 * @param aSpring the a spring
	 */
	public CAccountsImportBtn() {
		/*
		 * NOTE use native! All media are accepted. maxsize: the maximal allowed upload
		 * size of the component, in kilobytes, or a negative value if no limit. native:
		 * treating the uploaded file(s) as binary, i.e., not to convert it to image,
		 * audio or text files. multiple: treating the file chooser allows multiple
		 * files to upload, the setting only works with HTML5 supported browsers (since
		 * ZK 6.0.0).
		 */
		super();
		setUpload("true,maxsize=-1,native,multiple=false");
	}

	/**
	 * On upload read XML file with import data, import accounts, and show results.
	 */
	public void onUpload(UploadEvent event) {
		String lXmlStr = getXMLStringFromMedia(event.getMedia());
		if (!((lXmlStr == null) || (lXmlStr.equals("")))) {
			// sometimes when decoding extra char is added; remove it or else xml parser
			// will fail
			Integer lPos = lXmlStr.indexOf("<");
			if (lPos > 0)
				lXmlStr = lXmlStr.substring(lPos);
			IXMLTag lRootTag = CDesktopComponents.sSpring().getXmlManager().getXmlTreeFromImport(lXmlStr, "accounts",
					AppConstants.other_tag);
			List<IXMLTag> lAccounts = null;
			if (lRootTag != null)
				lAccounts = lRootTag.getChilds("account");
			if (lAccounts != null && !lAccounts.isEmpty()) {
				StringBuilder lMessage = new StringBuilder();
				changed = false;
				for (IXMLTag lAccount : lAccounts) {
					Hashtable<String, Object> lAccInfo = handleAccount(lAccount);
					lMessage.append(lAccInfo.get("message").toString() + "\n");
					changed = (changed || (lAccInfo.get("changed").toString().equals("true")));
				}
//				formatting doesn't work for default messagebox so use custom template:
				String lMessTemp = Messagebox.getTemplate();

				Messagebox.setTemplate("/" + VView.v_messagebox);

				Div lTransparentDiv = CDesktopComponents.vView().initQuasiModalBehavior(getRoot());
				Messagebox.show(lMessage.toString(), CDesktopComponents.vView().getLabel(summaryTitle), Messagebox.OK,
						Messagebox.INFORMATION, new EventListener<Event>() {
							@Override
							public void onEvent(Event evt) {
								switch (((Integer) evt.getData()).intValue()) {
								case Messagebox.OK:
									Messagebox.setTemplate(lMessTemp);
									if (changed) {
//										regenerate the accounts listbox
										regeneratePage();
									}
									CDesktopComponents.vView().undoQuasiModalBehavior(lTransparentDiv);
									break;
								}
							}
						});
			} else {
				CDesktopComponents.vView().showMessagebox(getRoot(),
						CDesktopComponents.vView().getLabel("error.file.no_valid_format"),
						CDesktopComponents.vView().getLabel("error.file.import_title"), Messagebox.OK,
						Messagebox.ERROR);
			}
		}
	}

	/**
	 * Gets the XML string from media (an xml file) with accounts to import.
	 *
	 * @param aMedia the media containing the xml file
	 *
	 * @return the XML string from the file
	 */
	protected String getXMLStringFromMedia(Media aMedia) {
		String lXmlStr = "";
		if (aMedia != null) {
			byte lText[] = aMedia.getByteData();
			// try to find actual encoding used to create xml file
			CharsetToolkit lCsKit = new CharsetToolkit(lText);
			Charset lCharset = lCsKit.guessEncoding();
			String lCodeName = lCharset.name();
			try {
				lXmlStr = new String(lText, lCodeName);
			} catch (UnsupportedEncodingException e) {
				_log.error(e);
				// assume default server encoding
				String lEncodingTest = new String(lText);
				// check if character encoding of file is attribute in xml tag, if so, use it
				DocumentBuilder builder = VView.getXmlTree().getDocumentBuilder();
				InputSource src = new InputSource();
				src.setCharacterStream(new StringReader(lEncodingTest));
				Document doc;
				try {
					doc = builder.parse(src);
					String lEncoding = doc.getXmlEncoding();
					if (!((lEncoding == null) || (lEncoding.equals("")))) {
						lXmlStr = new String(lText, lEncoding);
					} else {
						lXmlStr = lEncodingTest;
					}
				} catch (SAXException lXmlExc) {
					lXmlExc.printStackTrace();
					lXmlStr = lEncodingTest;
				} catch (IOException lXmlExc) {
					lXmlExc.printStackTrace();
					lXmlStr = lEncodingTest;
				}
			}
		}
		return lXmlStr;
	}

	/**
	 * Processes the XML account. To be overruled by extending class.
	 *
	 * @param aAccount the account in XML format
	 *
	 * @return the hashtable with account parameters
	 */
	protected Hashtable<String, Object> handleAccount(IXMLTag aAccount) {
		Hashtable<String, Object> lResult = new Hashtable<String, Object>(0);
		return lResult;
	}

	/**
	 * formats string line. Adds tab in front, and new line at the back.
	 *
	 * @param aText the line to format
	 *
	 * @return the formatted string
	 */
	protected String makeTabLine(String aText) {
		String lResult = stringTab + aText + "\n";
		return lResult;
	}

	/**
	 * Regenerates the page after the account import.
	 *
	 */
	protected void regeneratePage() {

	}

	/**
	 * Converts string to boolean.
	 *
	 * @param aText the string to check (@param aDefault the default boolean in case
	 *              aText is empty or undefined)
	 *
	 * @return the boolean value
	 */
	protected Boolean stringToBoolean(String aText) {
		return stringToBoolean(aText, false);
	}

	protected Boolean stringToBoolean(String aText, Boolean aDefault) {
		if (aText == null || (aText.equals("")))
			return aDefault;
		return (aText.equalsIgnoreCase("true") || aText.equals("1"));
	}

}
