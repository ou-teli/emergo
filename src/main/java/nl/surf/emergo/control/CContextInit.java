/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.IAppManager;
import nl.surf.emergo.business.IComponentManager;
import nl.surf.emergo.business.IFileManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.business.impl.AppManager;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.view.VView;

/**
 * The Class CContextInit. A listener which is called by Spring, when
 * application events occur. Used to intercept contextrefreshedevent, sent after
 * (re)loading Spring beans. Checks if database has to be initialized or
 * updated, and if components have to be updated.
 */
public class CContextInit implements ApplicationListener<ContextRefreshedEvent> {

	private static final Logger _log = LogManager.getLogger(CContextInit.class);

	/** The v view. */
	protected VView vView = null;

	/** The app manager. */
	protected IAppManager appManager;

	/** the file manager. */
	protected IFileManager fileManager;

	/** the component manager. */
	protected IComponentManager componentManager;

	/** the account manager. */
	protected IAccountManager accountManager;

	/** The context helper. */
	protected CContextInitHelper contextHelper = null;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		_log.info("contextrefresh: check database");
		ApplicationContext lAppContext = event.getApplicationContext();
		AppManager.setAppContext(lAppContext);
		contextHelper = new CContextInitHelper(lAppContext);
		initDatabase();
	}

	/**
	 * Sets the app manager.
	 * 
	 * @param aAppManager the app manager
	 */
	public void setAppManager(IAppManager aAppManager) {
		this.appManager = aAppManager;
	}

	/**
	 * Sets the file manager.
	 * 
	 * @param aFileManager the file manager
	 */
	public void setFileManager(IFileManager aFileManager) {
		this.fileManager = aFileManager;
	}

	/**
	 * Sets the component manager.
	 * 
	 * @param aComponentManager the component manager
	 */
	public void setComponentManager(IComponentManager aComponentManager) {
		this.componentManager = aComponentManager;
	}

	/**
	 * Sets the account manager.
	 * 
	 * @param aAccountManager the account manager
	 */
	public void setAccountManager(IAccountManager aAccountManager) {
		this.accountManager = aAccountManager;
	}

	/**
	 * Gets the app manager.
	 *
	 * @return the app manager
	 */
	protected IAppManager getAppManager() {
		return appManager;
	}

	/**
	 * Gets the file manager.
	 *
	 * @return the file manager
	 */
	protected IFileManager getFileManager() {
		return fileManager;
	}

	/**
	 * Reads sql file.
	 *
	 * @param aFileName the a file name
	 *
	 * @return list of sql statements
	 */
	protected List<String> readSqlFile(String aFileName) {
		List<String> sqlStatements = new ArrayList<>();
		List<String> sqlLines = getFileManager().readTextFile(aFileName);
		String sqlStatement = "";
		for (String sqlLine : sqlLines) {
			sqlLine = sqlLine.trim();
			if (!sqlLine.equals("")) {
				if (!sqlStatement.equals("")) {
					sqlStatement += " ";
				}
				sqlStatement += sqlLine;
				if (sqlLine.indexOf(";") == (sqlLine.length() - 1)) {
					sqlStatement = isValidSql(sqlStatement);
					if (!sqlStatement.equals("")) {
						sqlStatements.add(sqlStatement);
					}
					sqlStatement = "";
				}
			}
		}
		return sqlStatements;
	}

	/**
	 * Validates sql statement if statement is starting with Emergo specific
	 * instruction. Strips instruction from sql statement. Instruction example:
	 * <<<emergo,dropTable,courseaccountroles>>>
	 *
	 * @param aSqlStatement the a sql statement
	 *
	 * @return the sql statement
	 */
	protected String isValidSql(String aSqlStatement) {
		String lInstructionStart = "<<<emergo,";
		String lInstructionEnd = ">>>";
		int lStartIndex = aSqlStatement.indexOf(lInstructionStart);
		if (lStartIndex == 0) {
			int lEndIndex = aSqlStatement.indexOf(lInstructionEnd);
			if (lEndIndex > lStartIndex + lInstructionStart.length()) {
				String lInstruction = aSqlStatement.substring(lStartIndex + lInstructionStart.length(), lEndIndex);
				aSqlStatement = aSqlStatement.substring(lEndIndex + lInstructionEnd.length());
				String[] lInstructionData = lInstruction.split(",");
				if (lInstructionData[0].equals("createTable")) {
					if (getAppManager().sqlTableExists(lInstructionData[1])) {
						return "";
					}
				} else if (lInstructionData[0].equals("alterTable")) {
					if (!getAppManager().sqlTableExists(lInstructionData[1])) {
						return "";
					}
				} else if (lInstructionData[0].equals("dropTable")) {
					if (!getAppManager().sqlTableExists(lInstructionData[1])) {
						return "";
					}
				} else if (lInstructionData[0].equals("addColumn")) {
					if (getAppManager().sqlTableColumnExists(lInstructionData[1], lInstructionData[2])) {
						return "";
					}
				} else if ((lInstructionData[0].equals("changeColumn") || lInstructionData[0].equals("dropColumn"))
						&& !getAppManager().sqlTableColumnExists(lInstructionData[1], lInstructionData[2])) {
					return "";
				}
			} else {
				_log.info("ERROR initDatabase: error in sql statement: {}", aSqlStatement);
				return "";
			}
		}
		return aSqlStatement;
	}

	/**
	 * Executes sql statements.
	 *
	 * @param aSqlStatements the a sql statements
	 */
	protected void executeSqlStatements(List<String> aSqlStatements) {
		List<String> lErrors = new ArrayList<>();
		for (String sqlStatement : aSqlStatements) {
			_log.info("initDatabase: update tables: executing: {}", sqlStatement);
			lErrors.clear();
			getAppManager().executeSql(sqlStatement, lErrors);
			if (!lErrors.isEmpty()) {
				for (String lError : lErrors) {
					_log.info(lError);
				}
			}
		}
	}

	/**
	 * Is database initialized.
	 *
	 * @return if true
	 */
	protected boolean isDatabaseInitialized() {
		return appManager.sqlTableExists("accounts");
	}

	/**
	 * Is table components initialized.
	 *
	 * @return if true
	 */
	protected boolean isTableComponentsInitialized() {
		if (!appManager.sqlTableExists("components")) {
			return false;
		}
		return !componentManager.getAllComponents().isEmpty();
	}

	/**
	 * Inits database.
	 *
	 * @return if succeeded
	 */
	protected Boolean initDatabase() {
		// Attribute is used to ensure database is only updated once during lifetime of
		// ZK WebApp.

		if (!isDatabaseInitialized()) {
			// initialize database
			String fileName = contextHelper
					.getAbsolutePath(contextHelper.getInitParameter("emergo.sql.initial.path.and.file"));
			if (getFileManager().fileExists(fileName)) {
				List<String> sqlStatements = readSqlFile(fileName);
				executeSqlStatements(sqlStatements);
				_log.info("OK initDatabase: initialize tables: file: {}", fileName);
			} else {
				_log.info("ERROR initDatabase: file not found: {}", fileName);
			}
		}

		if (isDatabaseInitialized()) {
			int lastUpdateNumber = 0;
			try {
				lastUpdateNumber = Integer.parseInt(getAppManager().getSysvalue("sqllastupdatenumber"));
			} catch (NumberFormatException e) {
				lastUpdateNumber = 0;
			}
			int updateNumber = lastUpdateNumber;
			String lUpdateTempl = contextHelper.getInitParameter("emergo.sql.updates.path.and.filemask");
			String fileName = lUpdateTempl.replace(".sql", "" + (updateNumber + 1) + ".sql");
			fileName = contextHelper.getAbsolutePath(fileName);
			while (getFileManager().fileExists(fileName)) {
				// update database
				List<String> sqlStatements = readSqlFile(fileName);
				executeSqlStatements(sqlStatements);
				updateNumber++;
				_log.info("OK initDatabase: update tables: file: {}", fileName);
				fileName = lUpdateTempl.replace(".sql", "" + (updateNumber + 1) + ".sql");
				fileName = contextHelper.getAbsolutePath(fileName);
			}
			if (updateNumber > lastUpdateNumber) {
				getAppManager().setSysvalue("sqllastupdatenumber", "" + updateNumber);
			}

			fileName = contextHelper.getAbsolutePath(contextHelper.getInitParameter("emergo.components.path.and.file"));
			if (getFileManager().fileExists(fileName)) {
				// update component records
				// NOTE component must have admin as owner
				// get all admin accounts
				List<IEAccount> adminAccounts = accountManager.getAllAccounts(true, AppConstants.c_role_adm);
				if (!adminAccounts.isEmpty()) {
					// set owner to first admin account
					contextHelper.importComponents(adminAccounts.get(0), fileName);
				}
				// NOTE remove file to prevent it is used again
				getFileManager().deleteFile(fileName);
				_log.info("OK initDatabase: initialize/update components: file: {}", fileName);
			}
		}

		return true;
	}

}
