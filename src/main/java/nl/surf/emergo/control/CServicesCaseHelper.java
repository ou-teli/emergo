/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import java.util.ArrayList;
import java.util.List;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.zkspring.SSpring;

/**
 * The Class EmergoServicesCaseHelper.
 *
 * Is used in stead of CCaseHelper, when web services are involved.
 * CCaseHelper uses properties stored in CDesktopComponents, but the ZK desktop is unreachable if called as a web service.
 */
public class CServicesCaseHelper extends CCaseHelper {

	private SSpring sSpring = null;
	
	/**
	 * Instantiates a new case helper.
	 */
	public CServicesCaseHelper() {
		super();
	}

	/**
	 * Instantiates a new case helper.
	 *
	 * @param sSpring the spring object to use
	 */
	public CServicesCaseHelper(SSpring aSpring) {
		sSpring = aSpring;
	}

	/**
	 * Gets the 'case' case component content tag.
	 *
	 * @return the 'case' case component content tag
	 */
	public IXMLTag getCaseCaseComponentContentTag() {
		if (sSpring == null)
			return super.getCaseCaseComponentContentTag();
		if (sSpring.inRun()) {
			if (caseCaseComponentRootTag == null) {
				IECaseComponent lCaseCaseComponent = sSpring.getCaseComponent(sSpring.getCase(), "case", "");
				caseCaseComponentRootTag = sSpring.getXmlDataTree(lCaseCaseComponent);
			}
			if (caseCaseComponentRootTag == null)
				return null;
			return caseCaseComponentRootTag.getChild(AppConstants.contentElement);
		}
		else {
			// there is only one instance of the 'case' case component
			// every time read the content before it is changed, because other authors can change the case case component content too, simultanously
			IECaseComponent lCaseCaseComponent = sSpring.getCaseComponent(sSpring.getCase(), "case", "");
			return sSpring.getCaseComponentContentTag(lCaseCaseComponent);
		}
	}

	/**
	 * Sets the 'case' case component content tag.
	 *
	 * @param aContentTag the 'case' case component content tag
	 */
	public void setCaseCaseComponentContentTag(IXMLTag aContentTag) {
		if (sSpring == null) {
			super.setCaseCaseComponentContentTag(aContentTag);
			return;
		}
		// there is only one instance of the 'case' case component
		// every time read the content before it is changed, because other authors can change the case case component content too, simultanously
		IECaseComponent lCaseCaseComponent = sSpring.getCaseComponent(sSpring.getCase(), "case", "");
		IXMLTag lContentTag = sSpring.getCaseComponentContentTag(lCaseCaseComponent);
		if (lContentTag == null)
			return;
		lContentTag.setChildTags(aContentTag.getChildTags());
		String lXml = sSpring.getXmlManager().xmlTreeToDoc(lContentTag.getParentTag());
		setXmlData("" + lCaseCaseComponent.getCacId(), lXml);
	}

	/**
	 * Gets the ref tags to given case role and case component and tag.
	 *
	 * @param aReferenceType the reference type
	 * @param aTriggerId the trigger id
	 * @param aCaseRole the case role
	 * @param aCaseComponent the case component
	 * @param aTag the tag
	 *
	 * @return the ref tags
	 */
	public List<IXMLTag> getRefTags(String aReferenceType, String aTriggerId, IECaseRole aCaseRole, IECaseComponent aCaseComponent, IXMLTag aTag) {
		if (sSpring == null)
			return super.getRefTags(aReferenceType, aTriggerId, aCaseRole, aCaseComponent, aTag);
		List<IXMLTag> lRefTags = new ArrayList<IXMLTag>(0);
		List<String> lRefIds = getRefTagIds(aReferenceType,aTriggerId,aCaseRole,aCaseComponent,aTag);
		if (lRefIds.size() == 0)
			return lRefTags;
		for (String lRefId : lRefIds) {
			if ((lRefId != null) && (!lRefId.equals(""))) {
				String[] lIdArr = lRefId.split(",");
				if (lIdArr.length == 3) {
//					String lCarId = lIdArr[0];
					String lCacId = lIdArr[1];
					String lTagId = lIdArr[2];
					// note getCaseComponent for player returns casecomponent for current caserole!
					IXMLTag lRefTag = sSpring.getTag(sSpring.getCaseComponent(lCacId),lTagId);
					if (lRefTag != null)
						lRefTags.add(lRefTag);
				}
			}
		}
		return lRefTags;
	}

}