/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;


import java.util.HashMap;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Window;

import nl.surf.emergo.control.def.CDefMenuitem;

/**
 * The Class CContentItemMi.
 */
public class CContentItemMi extends CDefMenuitem {

	private static final long serialVersionUID = -815541828863572002L;

	/** The component. */
	protected CTree component = null;
	
	/** The content helper. */
	protected CContentHelper contentHelper = null;
	
	/** The is author. */
	protected boolean isAuthor = false;
	
	protected Component contentRoot = null;

	protected Component contentItem = null;

	protected Component target = null;
	
	protected Component parent = null;

	protected Object item = null;
	
	protected Map<String,Object> params = new HashMap<String,Object>();

	/**
	 * Instantiates a new c componentitem mi.
	 * 
	 * @param aComponent the a component
	 * @param aContentHelper the a content helper
	 * @param aIsAuthor the a is author
	 */
	public CContentItemMi(Component aComponent, CContentHelper aContentHelper, boolean aIsAuthor) {
		component = (CTree)aComponent;
		contentHelper = aContentHelper;
		isAuthor = aIsAuthor;
	}

	/**
	 * Gets the component.
	 * 
	 * @return the component
	 */
	protected Component getComponent() {
		return component;
	}

	/**
	 * Gets the content helper.
	 * 
	 * @return the content helper
	 */
	protected CContentHelper getContentHelper() {
		return contentHelper;
	}

	/**
	 * Gets the checks if is author.
	 * 
	 * @return the checks if is author
	 */
	protected boolean getIsAuthor() {
		return isAuthor;
	}
	
	/**
	 * On click. To be implemented by inherited class.
	 */
	public void onClick() {
	}
	
	public Window showPopup(String aView) {
/*
 		Window lWindow = CDesktopComponents.vView().modalPopup(aView, null, params, "center");
		if (lWindow.getAttribute("item") != null) {
			component.handleItem(lWindow.getAttribute("item"));
		}
		else if (lWindow.getAttribute("items") != null) {
			component.handleItem(lWindow.getAttribute("item"));
		}
		else if (lWindow.getAttribute("ok") != null) {
 			component.deleteItem();
		}
		else if (lWindow.hasAttribute("item") || lWindow.hasAttribute("items") || lWindow.hasAttribute("ok")) {
			component.cancelItem();
		}
*/
 		Window lWindow = CDesktopComponents.vView().quasiModalPopup(aView, getRoot(), params, "center");
 		//NOTE set notifyComponent to component that is equal to the CTree instance.
 		//Menu items don't exist anymore if the quasi popup is shown.
 		lWindow.setAttribute("notifyComponent", component);
		return lWindow;
	}

}
