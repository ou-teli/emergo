/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.util.Clients;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.IRunGroupAccountManager;
import nl.surf.emergo.business.IRunTeamManager;
import nl.surf.emergo.business.IRunTeamRunGroupManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.domain.IERole;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.domain.IERunTeam;
import nl.surf.emergo.view.VView;

/**
 * The Class CHandleLogin.
 *
 * Used to handle Login.
 */
public class CHandleLogin extends Object {

	private static final Logger _log = LogManager.getLogger(CHandleLogin.class);

	/**
	 * Handles redirecting setting user session variables and redirecting user to
	 * appropriate page.
	 * If no access, clear session variables.
	 * If access, set session variables to use within app and redirect to either
	 * a page to choose a role (if more than one role) or to the start page for
	 * the role (if one role).
	 * If access and run then open player.
	 *
	 * @param aAccount the a account
	 * @param aRun the a run
	 */
	public void handleLogin(IEAccount aAccount, IERun aRun) {
		// reinitialize session variables that refer to content files that can pass the content files filter servlet
		CControl cControl = CDesktopComponents.cControl();
		cControl.setAccSessAttr(AppConstants.sessKeyContentFiles, null);
		cControl.setAccSessAttr(AppConstants.sessKeyContentFileBlobIds, null);
		// determine if user has access to app
		// account must be active
		if (aAccount == null || !aAccount.getActive()) {
			// no access to app
			cControl.clearSession();
			return;
		}
		if (aAccount.getERoles().size() == 0) {
			_log.info("Account with accid=" + aAccount.getAccId() + " has no Emergo roles!");
			return;
		}
		if (aRun == null) {
			if (aAccount.getERoles().size() != 1) {
				// if more than one role redirect to the page to choose a account role
				setSessionVars(aAccount, aRun);
				CDesktopComponents.vView().redirectToView(VView.v_account_roles);
			}
			else {
				// if one role, set this role and redirect to the start page for this account role
				String lRoleCode = setSessionVars(aAccount, aRun);
				boolean lHasAccess = (!lRoleCode.equals(AppConstants.c_role_stu) || aAccount.getOpenaccess());
				if (!lHasAccess) {
					List<IERunGroup> lInRunGroups = getAccountRunGroups(aAccount, null);
					lHasAccess = lInRunGroups.size() > 0;
				}
				if (lHasAccess) {
					CAccountsHelper helper = new CAccountsHelper();
					CDesktopComponents.vView().redirectToView(helper.getLandingpage(aAccount.getAccId(), lRoleCode));
				}
				else {
					_log.info("Account with accid=" + aAccount.getAccId() + " has no access to (open) runs!");
				}
			}
		}
		else {
			// start player if user has one run group and is member of one team or there are no teams
			// otherwise stay on current window
			// get run groups
			List<IERunGroup> lInRunGroups = getAccountRunGroups(aAccount, aRun);
			if (lInRunGroups.size() == 0) {
				// if no run group do nothing
				return;
			}
			// get run teams
			List<IERunTeam> lInRunTeams = new ArrayList<IERunTeam>();
			List<IERunTeam> lRunTeams = getRunTeams(aRun);
			if (lRunTeams.size() > 0) {
				lInRunTeams = getAccountRunTeams(lInRunGroups, lRunTeams);				
			}
			if (lInRunGroups.size() == 1 &&
				(lRunTeams.size() == 0 || lInRunTeams.size() == 1)) {
				// start player if user has one run group and is member of one team or there are no teams
				IERunGroup lRunGroup = (IERunGroup)lInRunGroups.get(0);
				IECaseRole lCaseRole = lRunGroup.getECaseRole();
				IRunGroupAccountManager runGroupAccountManager = (IRunGroupAccountManager) CDesktopComponents.sSpring().getBean("runGroupAccountManager");
				List<IERunGroupAccount> lRunGroupAccounts = runGroupAccountManager.getAllRunGroupAccountsByRugIdAccId(lRunGroup.getRugId(), aAccount.getAccId());
				IERunGroupAccount lRunGroupAccount = null;
				if (lRunGroupAccounts.size() == 1) {
					lRunGroupAccount = (IERunGroupAccount)lRunGroupAccounts.get(0);
				}
				IERunTeam lRunTeam = null;
				if (lInRunTeams.size() == 1)
					lRunTeam = (IERunTeam)lInRunTeams.get(0);
				String lAction = createAction(aAccount, lCaseRole, aRun, lRunGroupAccount, lRunTeam);
				if (!lAction.equals("")) {
					setSessionVars(aAccount, aRun);
					Clients.evalJavaScript(lAction);
				}
			}
			else {
				// otherwise stay on current window
				_log.info("Account with accid=" + aAccount.getAccId() + " has not access to run '" + aRun.getName() + "' with runid=" + aRun.getRunId());
			}
		}
	}

	/**
	 * Gets run groups for aAccount.
	 *
	 * @param aAccount the a account
	 * @param aRun the a run, can be null
	 * 
	 * @return account run groups
	 */
	private List<IERunGroup> getAccountRunGroups(IEAccount aAccount, IERun aRun) {
		List<IERunGroup> lInRunGroups = new ArrayList<IERunGroup>();
		// get run groups
		IRunGroupAccountManager runGroupAccountManager = (IRunGroupAccountManager) CDesktopComponents.sSpring().getBean("runGroupAccountManager");
		List<IERunGroupAccount> lRunGroupAccounts = runGroupAccountManager.getAllRunGroupAccountsByAccId(aAccount.getAccId());
		if (lRunGroupAccounts.size() == 0)
			// if no run group account do nothing
			return lInRunGroups;
		for (IERunGroupAccount lRga : lRunGroupAccounts) {
			if (aRun == null || lRga.getERunGroup().getERun().getRunId() == aRun.getRunId()) {
				if (lRga.getERunGroup().getActive() && !lInRunGroups.contains(lRga.getERunGroup())) {
					lInRunGroups.add(lRga.getERunGroup());
				}
			}
		}
		return lInRunGroups;
	}

	/**
	 * Gets run teams for aRun.
	 *
	 * @param aRun the a run
	 * 
	 * @return run teams
	 */
	private List<IERunTeam> getRunTeams(IERun aRun) {
		IRunTeamManager runTeamManager = (IRunTeamManager) CDesktopComponents.sSpring().getBean("runTeamManager");
		return runTeamManager.getAllRunTeamsByRunId(aRun.getRunId());
	}

	/**
	 * Gets run teams for account aRunGroups.
	 *
	 * @param aRunGroups the a run groups
	 * @param aRunTeams the a run teams
	 * 
	 * @return account run teams
	 */
	private List<IERunTeam> getAccountRunTeams(List<IERunGroup> aRunGroups, List<IERunTeam> aRunTeams) {
		List<IERunTeam> lInRunTeams = new ArrayList<IERunTeam>();
		IRunTeamRunGroupManager runTeamRunGroupManager = (IRunTeamRunGroupManager) CDesktopComponents.sSpring().getBean("runTeamRunGroupManager");
		for (IERunTeam lRunTeam : aRunTeams) {
			if (lRunTeam.getActive()) {
				for (IERunGroup lRunGroup : aRunGroups) {
					if (runTeamRunGroupManager.getAllRunTeamRunGroupsByRutIdRugId(lRunTeam.getRutId(), lRunGroup.getRugId()).size() > 0) {
						if (!lInRunTeams.contains(lRunTeam)) {
							lInRunTeams.add(lRunTeam);
						}
					}
				}
			}
		}
		return lInRunTeams;
	}

	/**
	 * Sets session vars for account and return rolecode.
	 *
	 * @param aAccount the a account
	 * @param aRun the a run
	 * 
	 * @return role code or empty if no role
	 */
	private String setSessionVars(IEAccount aAccount, IERun aRun) {
		CControl cControl = CDesktopComponents.cControl();
		IAccountManager accountManager = (IAccountManager) CDesktopComponents.sSpring().getBean("accountManager");
		// set session variables to be used on other app pages
		cControl.setAccId(aAccount.getAccId());
		// get roles in correct order, this order is given by getAllRoles
		List<IERole> lERoles = new ArrayList<IERole>(0);
		for (IERole lRole : accountManager.getAllRoles()) {
			for (IERole lARole : aAccount.getERoles()) {
				if (lARole.getRolId() == lRole.getRolId())
					lERoles.add(lRole);
			}
		}
		// set for each role if account has this role
		String lRoleCode = "";
		for (IERole lRole : aAccount.getERoles()) {
			lRoleCode = lRole.getCode();
		}
		if (aRun == null) {
			if (aAccount.getERoles().size() != 1) {
				cControl.setAccSessAttr("multi_role", "true");
			}
			else {
				cControl.setAccSessAttr("role", lRoleCode);
				cControl.setAccSessAttr("multi_role", "false");
			}
			//NOTE set in session so tomcat admin may see which acc ids are busy
			cControl.setSessAttr("acc", "" + aAccount.getAccId());
			for (IERole lRole : aAccount.getERoles()) {
				//NOTE set in session so tomcat admin may see which roles are busy
				cControl.setSessAttr(lRole.getCode(), "true");
			}
		}
		return lRoleCode;
	}

	/**
	 * Creates client action to open Emergo player in new browser instance for
	 * a run group account, possibly within a run team.
	 *
	 * @return action string
	 */
	private String createAction(IEAccount aAccount, IECaseRole aCaseRole, IERun aRun, IERunGroupAccount aRunGroupAccount, IERunTeam aRunTeam) {
		String lRgaId = "";
		if (aRunGroupAccount != null)
			lRgaId = "" + aRunGroupAccount.getRgaId();
		String lRutId = "";
		if (aRunTeam != null)
			lRutId = "" + aRunTeam.getRutId();
		String lRunId = "";
		if (aRun != null)
			lRunId = "" + aRun.getRunId();
		String lAccId = "";
		if (aAccount != null)
			lAccId = "" + aAccount.getAccId();
		String lCarId = "";
		if (aCaseRole != null)
			lCarId = "" + aCaseRole.getCarId();
		String lParams =
			"&cacId=0"+
			"&tagId=0"+
			"&runstatus="+AppConstants.runStatusRun+
			"&rgaId="+lRgaId+
			"&rutId="+lRutId+
			"&runId="+lRunId+
			"&accId="+lAccId+
			"&carId="+lCarId;

		String lUrl = CDesktopComponents.vView().uniqueView(CDesktopComponents.sSpring().getSCaseSkinHelper().getCaseSkinPath(aRun) + VView.v_run);
		if (!CDesktopComponents.vView().isAbsoluteUrl(lUrl)) {
			lUrl = CDesktopComponents.vView().getEmergoWebappsRoot() + "/" + lUrl;
		}
		return CDesktopComponents.vView().getJavascriptWindowOpenFullscreen(lUrl + lParams);
	}
}
