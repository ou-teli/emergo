/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import org.zkoss.gmaps.Gmaps;
import org.zkoss.gmaps.Gmarker;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.OpenEvent;
import org.zkoss.zul.Menu;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Menupopup;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.Window;
import org.zkoss.zul.impl.XulElement;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.cde.CCdeComponentWnd;
import nl.surf.emergo.control.def.CDefMenu;
import nl.surf.emergo.control.def.CDefMenupopup;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.view.VView;

/**
 * The Class CContentItemMenuPopup is used to show a contextualised popup menu within the Emergo author environment.
 * Popup menu items are mainly crud operations and depend on the XML tag referenced by the ZK component right clicked.
 */
public class CContentItemMenuPopup extends CDefMenupopup {

	private static final long serialVersionUID = 1233377397096853800L;

	/** The tree. */
	protected Component component = null;

	/** The tree helper. */
	protected CContentHelper contentHelper = null;

	/** The ZK parent window of the menu. */
	protected Window window = null;

	/** The render edit root, if it is possible to edit the root element */
	protected boolean renderEditRoot = true;

	/** The render new option. */
	protected boolean renderNew = true;

	/** The render edit option. */
	protected boolean renderEdit = true;

	/** The render delete option. */
	protected boolean renderDelete = true;

	/** The render inspect option. */
	protected boolean renderInspect = true;

	/** The render preview option. */
	protected boolean renderPreview = true;

	/** The render fold option. */
	protected boolean renderFold = true;

	/**
	 * Gets the component.
	 *
	 * @return the component
	 */
	protected Component getComponent() {
		return component;
	}

	/**
	 * Sets the component.
	 *
	 * @param aTarget (a child) of the component
	 */
	protected void setComponent(Component aTarget) {
		if (component == null) {
			Component lTarget = aTarget;
			while (lTarget != null && !(lTarget instanceof Tree)) {
				lTarget = lTarget.getParent();
			}
			if (lTarget == null) {
				lTarget = aTarget;
				while (lTarget != null && !(lTarget instanceof Gmaps)) {
					lTarget = lTarget.getParent();
				}
			}
			component = lTarget;
		}
	}

	/**
	 * Gets the content helper.
	 *
	 * @return the content helper
	 */
	protected CContentHelper getContentHelper() {
		contentHelper = new CContentHelper(component);
		return contentHelper;
	}

	/**
	 * Gets the window.
	 *
	 * @return the window
	 */
	protected Window getWindow() {
		if (window == null) {
			window = (Window) getRoot();
		}
		return window;
	}

	/**
	 * Acc is author.
	 *
	 * @return true, if successful
	 */
	public boolean accIsAuthor() {
		if (getWindow() == null)
			return false;
		if (getWindow() instanceof CCdeComponentWnd) {
			return ((CCdeComponentWnd)getWindow()).accIsAuthor();
		}
		else {
			return ((String)CDesktopComponents.cControl().getAccSessAttr("accisauthor")).equals("true");
		}

	}


	private boolean busy = false;
	private int menuIndex = 0;
	private int menuIndexMax = 0;
	/**
	 * On open (re)render menu options depending on definition of xml tag for which menu is shown.
	 * If only one option, this is chosen automatically.
	 *
	 * @param aEvent the a event
	 */
	public void onOpen(OpenEvent aEvent) {
		if (!aEvent.isOpen()) {
			getChildren().clear();
			return;
		}
		Component lTarget = aEvent.getReference();
		boolean lOpenTree = lTarget instanceof Tree || lTarget instanceof Treeitem;
		boolean lOpenGmaps = lTarget instanceof Gmaps || lTarget instanceof Gmarker;
		if (lOpenGmaps && getAttribute("target") != null) {
			lTarget = (Component)getAttribute("target");
		}
		if (!(lOpenTree || lOpenGmaps))
			return;

		if (busy)
			return;
		busy = true;

		menuIndex = 0;
		menuIndexMax = getChildren().size() - 1;

		setComponent(lTarget);
		boolean lIsAuthor = accIsAuthor();
		getComponent().setAttribute("target", lTarget);
		IXMLTag lXMLTag = (IXMLTag)lTarget.getAttribute("item");
		String lNodename = lXMLTag.getName();
		String lNodetype = lXMLTag.getDefAttribute(AppConstants.defKeyType);
		boolean lContent = lTarget instanceof Tree || lTarget instanceof Gmaps;
//		boolean lContentItem = lTarget instanceof Treeitem || lTarget instanceof Gmarker;

		IECaseComponent lCaseComponent = (IECaseComponent)lTarget.getAttribute("casecomponent");
		String lComponentCode = lCaseComponent.getEComponent().getCode();
		
		if (lIsAuthor) {
			if (renderNew) {
				String lSiblingnodes = lXMLTag.getParentTag().getDefAttribute(AppConstants.defKeyChildnodes);
				if (!lSiblingnodes.equals("")) {
					Menu lSubMenu = appendSubMenu(this, CDesktopComponents.vView().getLabel("cde_components.menuitem.new.sibling"));
					Menupopup lSubMenupopup = appendSubMenuPopup(lSubMenu);
					String[] lSiblings = lSiblingnodes.split(",");
					for (int i = 0; i < lSiblings.length; i++) {
						//NOTE help text is shown as tooltiptext so replace HTML breaks
						String lHelpText = CContentHelper.getHelpText(lComponentCode, lSiblings[i], "", VView.nodetagLabelKeyPrefix).replaceAll("<br/>", " ");
						appendMenuitem(lSubMenupopup, getMenuitemNew(), lSiblings[i], CContentHelper.getNodeTagLabel(lComponentCode, lSiblings[i]), lHelpText, "sibling");
					}
				}
				String lChildnodes = lXMLTag.getDefAttribute(AppConstants.defKeyChildnodes);
				boolean lOk = !lChildnodes.equals("");
				String lMaxnumberofchildnodes = lXMLTag.getDefAttribute(AppConstants.defKeyMaxnumberofchildnodes);
				if (lOk && !lMaxnumberofchildnodes.equals("")) {
					if (lXMLTag.getChildTags(AppConstants.defValueNode).size() >= Integer.parseInt(lMaxnumberofchildnodes)) {
						lOk = false;
					}
				}
				if (lOk) {
					Menu lSubMenu = appendSubMenu(this, CDesktopComponents.vView().getLabel("cde_components.menuitem.new.child"));
					Menupopup lSubMenupopup = appendSubMenuPopup(lSubMenu);
					String[] lChilds = lChildnodes.split(",");
					for (int i = 0; i < lChilds.length; i++) {
						//NOTE help text is shown as tooltiptext so replace HTML breaks
						String lHelpText = CContentHelper.getHelpText(lComponentCode, lChilds[i], "", VView.nodetagLabelKeyPrefix).replaceAll("<br/>", " ");
						appendMenuitem(lSubMenupopup, getMenuitemNew(), lChilds[i], CContentHelper.getNodeTagLabel(lComponentCode, lChilds[i]), lHelpText, "child");
					}
				}
			}

			if (renderEdit) {
				if (!lContent && lNodetype.equals(AppConstants.defValueNode)) {
					appendMenuitem(this, getMenuitemEdit(), lNodename, CDesktopComponents.vView().getLabel("edit"), "", "");
				}
				if (!lContent && lNodetype.equals(AppConstants.rootElement) && renderEditRoot) {
					boolean lPropertyChilds = lXMLTag.getDefTag().getChildTags().size() > lXMLTag.getDefTag().getChildTags("node").size();
					if (lPropertyChilds) {
						appendMenuitem(this, getMenuitemEdit(), lNodename, CDesktopComponents.vView().getLabel("edit"), "", "");
					}
				}
			}

			if (renderDelete) {
				if (!lContent && lNodetype.equals(AppConstants.defValueNode)) {
					appendMenuitem(this, getMenuitemDelete(), lNodename, CDesktopComponents.vView().getLabel("delete"), "", "");
				}
			}
		} else {
			if (renderInspect) {
				if (!lContent && lNodetype.equals(AppConstants.defValueNode)) {
					appendMenuitem(this, getMenuitemInspect(), lNodename, CDesktopComponents.vView().getLabel("inspect"), "", "");
				}
			}
		}

		if (renderPreview) {
			if (lXMLTag.getDefAttribute(AppConstants.defKeyPreview).equals(AppConstants.statusValueTrue)) {
				appendMenuitem(this, getMenuitemPreview(), lNodename, CDesktopComponents.vView().getLabel("preview"), CDesktopComponents.vView().getLabel("preview.help"), "");
			}
		}

		if (renderFold) {
			if (hasFoldableNodeChilds(lXMLTag)) {
				appendMenuitem(this, getMenuitemFold(), lNodename, CDesktopComponents.vView().getLabel("fold"), "", "");
			}
		}

		for (int i = menuIndexMax; i >= menuIndex; i--) {
			removeChild ((Component)getChildren().get(i));
		}

		if (getChildren().size() == 1 && getChildren().get(0) instanceof CContentItemMi) {
			((CContentItemMi)getChildren().get(0)).onClick();
		}
		busy = false;
	}

	/**
	 * Appends sub menu.
	 *
	 * @param aParent the a parent
	 * @param aLabel the a label
	 *
	 * @return the sub menu
	 */
	public Menu appendSubMenu(Component aParent, String aLabel) {
		Menu lSubMenu = new CDefMenu();
		if (menuIndex <= menuIndexMax) {
			Component lExMenu = (Component)getChildren().get(menuIndex);
			menuIndex++;
			if (!(lExMenu instanceof Menu)) {
				insertBefore(lSubMenu,lExMenu);
				menuIndexMax++;
			}
			else {
				lSubMenu = (Menu)lExMenu;
				while (lSubMenu.getLastChild() != null) {
					lSubMenu.removeChild(lSubMenu.getLastChild());
				}
			}
		} else {
			aParent.appendChild(lSubMenu);
		}
		lSubMenu.setLabel(aLabel);
		setZclass(lSubMenu);
		return lSubMenu;
	}

	/**
	 * Appends sub menu popup.
	 *
	 * @param aParent the a parent
	 *
	 * @return the sub menu popup
	 */
	public Menupopup appendSubMenuPopup(Component aParent) {
		Menupopup lSubMenupopup = new CDefMenupopup();
		aParent.appendChild(lSubMenupopup);
		setZclass(lSubMenupopup);
		return lSubMenupopup;
	}

	/**
	 * Appends menuitem.
	 *
	 * @param aParentMenupopup the a parent menu popup
	 * @param aMenuitem the a menuitem
	 * @param aNodename the a nodename
	 * @param aLabel the a label
	 * @param aTooltiptext the a tool tip text
	 * @param aNewNodetype the a new nodetype
	 *
	 * @return the menu item
	 */
	public Menuitem appendMenuitem(Menupopup aParentMenupopup, Menuitem aMenuitem, String aNodename, String aLabel, String aTooltiptext,String aNewNodetype) {
		aMenuitem.setLabel(aLabel);
		//NOTE setTooltip results in flickering, so use tool tip text. And replace possible <br/> by space. 
		aMenuitem.setTooltiptext(aTooltiptext.replace("<br/>", " "));
		aMenuitem.setAttribute("nodename", aNodename);
		aMenuitem.setAttribute("nodecontext", getAttribute("nodecontext"));
		aMenuitem.setAttribute("newnodetype", aNewNodetype);

		if (aParentMenupopup == this) {
			if (menuIndex <= menuIndexMax) {
				Component lExMenu = (Component)getChildren().get(menuIndex);
				menuIndex++;
				if (!(lExMenu instanceof Menuitem)) {
					insertBefore(aMenuitem,lExMenu);
					menuIndexMax++;
				}
				else {
					aMenuitem = (Menuitem)lExMenu;
					while (aMenuitem.getLastChild() != null) {
						aMenuitem.removeChild(aMenuitem.getLastChild());
					}
				}
			} 
			else {
				appendChild(aMenuitem);
			}
		} 
		else {
			aParentMenupopup.appendChild(aMenuitem);
		}
		setZclass(aMenuitem);
		return aMenuitem;
	}

	/**
	 * Sets zclass for menu, menupopup or menuitem.
	 *
	 * @param aComponent the a component
	 */
	public void setZclass(XulElement aComponent) {
		// override
	}

	/**
	 * Gets the menuitem new.
	 *
	 * @return the menuitem new
	 */
	public Menuitem getMenuitemNew() {
		return (new CContentItemNewMi(getComponent(), getContentHelper(), accIsAuthor()));
	}

	/**
	 * Gets the menuitem edit.
	 *
	 * @return the menuitem edit
	 */
	public Menuitem getMenuitemEdit() {
		return (new CContentItemEditMi(getComponent(), getContentHelper(), accIsAuthor()));
	}

	/**
	 * Gets the menuitem delete.
	 *
	 * @return the menuitem delete
	 */
	public Menuitem getMenuitemDelete() {
		return (new CContentItemDeleteMi(getComponent(), getContentHelper(), accIsAuthor()));
	}

	/**
	 * Gets the menuitem inspect.
	 *
	 * @return the menuitem inspect
	 */
	public Menuitem getMenuitemInspect() {
		return (new CContentItemInspectMi(getComponent(), getContentHelper(), accIsAuthor()));
	}

	/**
	 * Gets the menuitem script.
	 *
	 * @return the menuitem script
	 */
	public Menuitem getMenuitemScript() {
		return (new CContentItemScriptMi(getComponent(), getContentHelper(), accIsAuthor()));
	}

	/**
	 * Gets the menuitem preview.
	 *
	 * @return the menuitem preview
	 */
	public Menuitem getMenuitemPreview() {
		return (new CContentItemPreviewMi(getComponent(), getContentHelper(), accIsAuthor()));
	}

	/**
	 * Gets the menuitem fold.
	 *
	 * @return the menuitem fold
	 */
	public Menuitem getMenuitemFold() {
		return (new CContentItemFoldMi(getComponent(), getContentHelper(), accIsAuthor()));
	}

	/**
	 * Has foldable node childs.
	 *
	 * @param aTag the a tag
	 * 
	 * @return if true
	 */
	public boolean hasFoldableNodeChilds(IXMLTag aTag) {
		for (IXMLTag lNodeChild : aTag.getChildTags(AppConstants.defValueNode)) {
			if (lNodeChild.getChildTags(AppConstants.defValueNode).size() > 0) {
				return true;
			}
		}
		return false;
	}

}