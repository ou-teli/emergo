/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zul.impl.InputElement;

import nl.surf.emergo.control.def.CDefButton;

/**
 * The Class CUploadMediaButton. Used to upload file content.
 */
public class CUploadMediaButton extends CDefButton {

	private static final long serialVersionUID = -6162028484130859741L;

	/** The uploadtarget. */
	protected Component uploadtarget = null;

	/**
	 * Instantiates a new CUploadMediaButton.
	 */
	public CUploadMediaButton() {
		/*
		NOTE use native! All media are accepted.
		maxsize: the maximal allowed upload size of the component, in kilobytes, or a negative value if no limit.
		native: treating the uploaded file(s) as binary, i.e., not to convert it to image, audio or text files.
		multiple: treating the file chooser allows multiple files to upload, the setting only works with HTML5 supported browsers (since ZK 6.0.0).
		*/
		setUpload("true,maxsize=-1,native,multiple=false");
	}

	/**
	 * On create set upload target if it exists.
	 */
	public void onCreate(CreateEvent aEvent) {
		Component lComponent = getFellowIfAny("filename");
		if (lComponent != null) {
			setUploadTarget(lComponent);
		}
	}

	/**
	 * Sets the upload target, the ZK component which should get the uploaded content
	 * as attribute.
	 *
	 * @param aUploadtarget the new upload target
	 */
	public void setUploadTarget(Component aUploadtarget) {
		uploadtarget = aUploadtarget;
	}

	/**
	 * On upload upload file content to the upload target.
	 */
	public void onUpload(UploadEvent aEvent) {
		Media media = aEvent.getMedia();
		uploadtarget.setAttribute("content", media);
		uploadtarget.setAttribute("changed", "true");
		((InputElement) uploadtarget).setText(media.getName());
	}

}
