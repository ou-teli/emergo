/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.doomdark.uuid.UUIDGenerator;
import org.xml.sax.InputSource;
import org.zkoss.zk.ui.Sessions;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.IAppManager;
import nl.surf.emergo.business.IBlobManager;
import nl.surf.emergo.business.ICaseComponentManager;
import nl.surf.emergo.business.ICaseComponentRoleManager;
import nl.surf.emergo.business.ICaseManager;
import nl.surf.emergo.business.ICaseRoleManager;
import nl.surf.emergo.business.IComponentManager;
import nl.surf.emergo.business.IFileManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.IXMLTree;
import nl.surf.emergo.business.IXmlManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.business.impl.XMLTree;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IEBlob;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IECaseComponentRole;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.domain.IEComponent;
import nl.surf.emergo.utilities.ExcelHelper;
import nl.surf.emergo.utilities.ZipHelper;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpring;

/**
 * The Class CImportExportHelper. Helper class for importing or exporting a
 * case. A case is exported including its blob files, but streaming files are
 * not exported! Likewise importing is also including blobs, but without
 * streaming media files! This is to prevent long waiting times while
 * exporting/importing and overload of server (disk space). Streaming files have
 * to be copied by an admin who has access to the server.
 */
public class CImportExportHelper implements Serializable {
	private static final Logger _log = LogManager.getLogger(CImportExportHelper.class);
	private static final long serialVersionUID = 3409185275865533685L;

	IXMLTree xmlTree = new XMLTree();
	protected IAppManager appManager = null;
	protected IAccountManager accountManager = null;
	protected IComponentManager componentManager = null;
	protected IXmlManager xmlManager = null;
	protected IBlobManager blobManager = null;
	protected String exportFileName = "";
	protected CXmlHelper xmlHelper = new CXmlHelper();
	protected String caseComponentsContentExcelSheetName = "casecomponentscontent";
	protected String[] caseComponentsContentExcelSheetInfoHeaders = new String[] { "Locale, e.g., 'en_UK' or 'de_DE'" };
	protected String caseComponentsContentExcelSheetLocaleStrId = "localeStrId";
	protected String[] caseComponentsContentExcelSheetContentHeaders = new String[] { "CacId", "CarId", "TagId", "Type",
			"Key", "Status", "Index", "HTML", "Value" };
	protected CellType[] caseComponentsContentExcelCellTypes = new CellType[] { null, null, null, CellType.STRING,
			CellType.STRING, CellType.STRING, null, CellType.STRING, CellType.STRING };
	protected String caseComponentsContentExcelSheetStatusDefault = "DEFAULT";
	protected String caseComponentsContentExcelSheetStatusDelete = "DELETE";

	/**
	 * Gets the App manager.
	 *
	 * @return the App manager
	 */
	protected IAppManager getAppManager() {
		if (appManager == null)
			appManager = (IAppManager) getSpring().getBean("appManager");
		return appManager;
	}

	/**
	 * Gets the Account manager.
	 *
	 * @return the Account manager
	 */
	protected IAccountManager getAccountManager() {
		if (accountManager == null)
			accountManager = (IAccountManager) getSpring().getBean("accountManager");
		return accountManager;
	}

	/**
	 * Gets the Component manager.
	 *
	 * @return the Component manager
	 */
	protected IComponentManager getComponentManager() {
		if (componentManager == null)
			componentManager = (IComponentManager) getSpring().getBean("componentManager");
		return componentManager;
	}

	/**
	 * Gets the XML manager.
	 *
	 * @return the XML manager
	 */
	protected IXmlManager getXmlManager() {
		if (xmlManager == null)
			xmlManager = getSpring().getXmlManager();
		return xmlManager;
	}

	/**
	 * Gets the Blob manager.
	 *
	 * @return the Blob manager
	 */
	protected IBlobManager getBlobManager() {
		if (blobManager == null)
			blobManager = (IBlobManager) getBean("blobManager");
		return blobManager;
	}

	/**
	 * Gets the SSpring instantiation for the current desktop.
	 *
	 * @return the sSpring
	 */
	protected SSpring getSpring() {
		return CDesktopComponents.sSpring();
	}

	/**
	 * Gets the bean, given by aBeanId. A bean is an object defined within the
	 * Spring configuration file, that is injected by Spring at runtime.
	 *
	 * @param aBeanId the a bean id
	 *
	 * @return the bean
	 */
	protected Object getBean(String aBeanId) {
		return getSpring().getBean(aBeanId);
	}

	/**
	 * Gets the escaped Xml string
	 *
	 * @param aXmlString the Xml string to escaspe
	 *
	 * @return the escaped Xml string
	 */
	protected String escapeXML(String aXmlString) {
		return getSpring().escapeXML(aXmlString);
	}

	/**
	 * Gets the unescaped Xml string
	 *
	 * @param aXmlString the Xml string to unescaspe
	 *
	 * @return the unescaped Xml string
	 */
	protected String unescapeXML(String aXmlString) {
		return getSpring().unescapeXML(aXmlString);
	}

	/**
	 * Adds child tag with aName and aValue to aParentTag.
	 *
	 * @param aParentTag the a parent tag
	 * @param aName      the a name
	 * @param aValue     the a value
	 *
	 * @return the iXML tag
	 */
	private IXMLTag addChildTag(IXMLTag aParentTag, String aName, String aValue) {
		IXMLTag lTag = getXmlManager().newXMLTag(aName, aValue);
		aParentTag.getChildTags().add(lTag);
		lTag.setParentTag(aParentTag);
		return lTag;
	}

	/**
	 * Adds child tag aChildTag to aParentTag.
	 *
	 * @param aParentTag the a parent tag
	 * @param aChildTag  the a child tag
	 *
	 * @return the iXML tag
	 */
	private IXMLTag addChildTag(IXMLTag aParentTag, IXMLTag aChildTag) {
		aParentTag.getChildTags().add(aChildTag);
		aChildTag.setParentTag(aParentTag);
		return aChildTag;
	}

	/**
	 * Gets the blob ids within aRootTag and returns them in aBlobIds. World wide
	 * unique ids for this blob ids are returned as values in aUUIDBlobIds, keys are
	 * blob ids.
	 *
	 * @param aRootTag             the a root tag
	 * @param aBlobIds             the a blob ids
	 * @param aUUIDBlobIds         the a uuid blob ids, keys contain blob ids,
	 *                             values contain world wide unique ids
	 * @param aReplaceBlobIdByUUID the a replace blob id by UUID
	 */
	private void getBlobIds(IXMLTag aRootTag, List<String> aBlobIds, Hashtable<String, String> aUUIDBlobIds,
			boolean aReplaceBlobIdByUUID) {
		if (aRootTag == null) {
			return;
		}
		List<IXMLTag> lNodeTags = CDesktopComponents.cScript().getNodeTags(aRootTag);
		// NOTE component and content tag might have blob childs too, so add them
		lNodeTags.add(aRootTag.getChild(AppConstants.componentElement));
		lNodeTags.add(aRootTag.getChild(AppConstants.contentElement));
		for (IXMLTag lTag : lNodeTags) {
			List<IXMLTag> lBlobTags = new ArrayList<IXMLTag>();
			for (String childTagName : AppConstants.nodeChildTagsWithBlob) {
				lBlobTags.addAll(lTag.getChilds(childTagName));
			}
			for (IXMLTag lBlobTag : lBlobTags) {
				String lBlobId = lBlobTag.getValue();
				if (!lBlobId.equals("")) {
					aBlobIds.add(lBlobId);
					String lUUID = UUIDGenerator.getInstance().generateTimeBasedUUID().toString();
					aUUIDBlobIds.put(lBlobId, lUUID);
					if (aReplaceBlobIdByUUID) {
						lBlobTag.setValue(lUUID);
					}
				}
			}
		}
	}

	/**
	 * Replaces all references to car ids, com ids and cac ids within aRootTag by
	 * world wide unique ids.
	 *
	 * @param aRootTag the a root tag
	 * @param aCarIds  the a car ids, keys contain car ids, values contain world
	 *                 wide unique ids
	 * @param aComIds  the a com ids, keys contain com ids, values contain world
	 *                 wide unique ids
	 * @param aCacIds  the a cac ids, keys contain cac ids, values contain world
	 *                 wide unique ids
	 */
	private void replaceIdsByUUIDS(IXMLTag aRootTag, Hashtable<String, String> aCarIds,
			Hashtable<String, String> aComIds, Hashtable<String, String> aCacIds) {
		List<IXMLTag> lNodeTags = CDesktopComponents.cScript().getAllTags(aRootTag);
		if (lNodeTags == null)
			return;
		for (IXMLTag lTag : lNodeTags) {
			if (lTag.getName().equals("car") || lTag.getName().equals("refcar") || lTag.getName().equals("carid")) {
				String lValue = "" + aCarIds.get(lTag.getValue());
				if (lValue.equals("null"))
					lValue = "0";
				lTag.setValue(lValue);
			}
			if (lTag.getName().equals("com") || lTag.getName().equals("refcom") || lTag.getName().equals("comid")) {
				String lValue = "" + aComIds.get(lTag.getValue());
				if (lValue.equals("null")) {
					lValue = getComponentManager().getComponent(Integer.parseInt(lTag.getValue())).getUuid();
					aComIds.put(lTag.getValue(), lValue);
				}
				lTag.setValue(lValue);
			}
			if (lTag.getName().equals("cac") || lTag.getName().equals("refcac") || lTag.getName().equals("cacid")) {
				String lValue = "" + aCacIds.get(lTag.getValue());
				if (lValue.equals("null"))
					lValue = "0";
				lTag.setValue(lValue);
			}
		}
	}

	/**
	 * Saves data as file.
	 *
	 * @param aData     the a data
	 * @param aTempDir  the a temp dir
	 * @param aFileName the a file name
	 */
	private void saveDataAsFile(String aData, String aTempDir, String aFileName) {
		try {
			IFileManager lFileManager = (IFileManager) getBean("fileManager");
			lFileManager.createDir(aTempDir);
			File lOutputFile = new File(aTempDir + aFileName);
			if (new File(lOutputFile.getAbsolutePath()).exists())
				lOutputFile.getAbsoluteFile().delete();
			lOutputFile.createNewFile();
			Writer lWriter = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream(lOutputFile.getAbsolutePath()), "UTF-8"));
			lWriter.write(aData);
			lWriter.flush();
			lWriter.close();
		} catch (IOException ioe) {
			;
		}
	}

	/**
	 * Saves xml data as file.
	 *
	 * @param aXmlData  the a xml data
	 * @param aTempDir  the a temp dir
	 * @param aFileName the a file name
	 */
	private void saveXMLAsFile(String aXmlData, String aTempDir, String aFileName) {
		saveDataAsFile(aXmlData, aTempDir, aFileName);
	}

	/**
	 * Saves html data as file.
	 *
	 * @param aHtmlData the a html data
	 * @param aTempDir  the a temp dir
	 * @param aFileName the a file name
	 */
	private void saveHTMLAsFile(String aHtmlData, String aTempDir, String aFileName) {
		saveDataAsFile(aHtmlData, aTempDir, aFileName);
	}

	/**
	 * Returns temp path for this session.
	 *
	 * @return temp path
	 */
	public String getTempPath() {
		String lTempPath = getAppManager().getAbsoluteTempPath();
		String lSubPath = "";
		if (Sessions.getCurrent() != null) {
			lSubPath = CDesktopComponents.vView().getUniqueTempSubPath();
		}
		lTempPath = lTempPath + lSubPath;
		return lTempPath;
	}

	/**
	 * Export case. Exports case, case roles, case components, case component roles
	 * and blobs as xml files and zips them with the blob files in one zip file,
	 * which is returned as path plus filename. Db ids, such as cas id, car ids, cac
	 * ids, ccr ids an blo ids are converted to world wide unique ids and references
	 * to these ids within the xml data of the case components are replaced by these
	 * world wide unique ids.
	 *
	 * @param aCase the a case
	 *
	 * @return export file, empty if not successful
	 */
	public String exportCase(IECase aCase) {
		if (aCase == null)
			return "";
		Hashtable<String, String> lComIds = new Hashtable<String, String>(0);
		Hashtable<String, String> lCacIds = new Hashtable<String, String>(0);
		Hashtable<String, String> lCarIds = new Hashtable<String, String>(0);
		List<String> lBlobIds = new ArrayList<String>();
		Hashtable<String, String> lUUIDBlobIds = new Hashtable<String, String>(0);
		List<String> toZipList = new ArrayList<String>();
		List<String> removeList = new ArrayList<String>();

		String lUUIDCasId = createCaseXmlFile(aCase, toZipList, removeList);
		createCaseRolesXmlFile(aCase.getCasId(), lUUIDCasId, lCarIds, toZipList, removeList);
		createCaseComponentsXmlFile(aCase.getCasId(), lUUIDCasId, lComIds, lCacIds, lCarIds, lBlobIds, lUUIDBlobIds,
				toZipList, removeList);
		createCaseComponentRolesXmlFile(lCacIds, lCarIds, toZipList, removeList);
		createComponentsXmlFile(lComIds, toZipList, removeList);
		createBlobsXmlFileAndBlobFiles(lBlobIds, lUUIDBlobIds, toZipList, removeList, false, 0);

		String lTempPath = getTempPath();
		String lBlobPath = getAppManager().getAbsoluteBlobPath();
		exportFileName = lTempPath + "case.zip";
		String lSubUrl = "";
		if (Sessions.getCurrent() != null) {
			lSubUrl = CDesktopComponents.vView().getUniqueTempSubPath();
		}
		String lUrl = CDesktopComponents.vView().getEmergoRootUrl() + CDesktopComponents.vView().getEmergoWebappsRoot()
				+ VView.getInitParameter("emergo.temp.path") + lSubUrl + "case.zip";
		if (packageFilesToZip(toZipList, lTempPath, lBlobPath, "", new File(exportFileName))) {
			// remove files in removeList
			for (String lFileName : removeList) {
				removeFile(lFileName);
			}
			return lUrl;
		}
		return "";
	}

	/**
	 * Export case component. Exports case component and blobs as xml files and zips
	 * them with the blob files in one zip file, which is returned as path plus
	 * filename. Db ids, such as cac ids and blo ids are converted to world wide
	 * unique ids and references to these ids within the xml data of the case
	 * component are replaced by these world wide unique ids.
	 *
	 * @param aCaseComponent the a case component
	 *
	 * @return export file, empty if not successful
	 */
	public String exportCaseComponent(IECaseComponent aCaseComponent) {
		List<IECaseComponent> lCaseComponents = new ArrayList<IECaseComponent>();
		lCaseComponents.add(aCaseComponent);
		return exportCaseComponents(lCaseComponents, "casecomponent.zip");
	}

	/**
	 * Export case components. Exports case components and blobs as xml files and
	 * zips them with the blob files in one zip file, which is returned as path plus
	 * filename. Db ids, such as cac ids and blo ids are converted to world wide
	 * unique ids and references to these ids within the xml data of the case
	 * component are replaced by these world wide unique ids.
	 *
	 * @param aCaseComponents the a case components
	 *
	 * @return export file, empty if not successful
	 */
	public String exportCaseComponents(List<IECaseComponent> aCaseComponents) {
		return exportCaseComponents(aCaseComponents, "casecomponents.zip");
	}

	/**
	 * Export case component content. Exports case component content and blobs as
	 * xml files and zips them with the blob files in one zip file, which is
	 * returned as path plus filename. Db ids, such as cac ids and blo ids are
	 * converted to world wide unique ids and references to these ids within the xml
	 * data of the case component are replaced by these world wide unique ids.
	 *
	 * @param aCaseComponents         the a case components
	 * @param aTagNames               the a tag names
	 * @param aContentTypes           the a content types
	 * @param aContentNames           the a content names
	 * @param aExportAsExcel          the a export as Excel
	 * @param aIncludeStreamingFolder the a include streaming folder
	 * @param aIncludeHTMLStyle       the a include html style
	 * @param aAllContentEditable     the a include all content
	 * @param aIncludeAttributes      the a include attributes
	 * @param aLocaleStr              the a locale str
	 *
	 * @return export file, empty if not successful
	 */
	public String exportCaseComponentContent(List<IECaseComponent> aCaseComponents, List<String> aTagNames,
			List<String> aContentTypes, List<String> aContentNames, boolean aExportAsExcel,
			boolean aIncludeStreamingFolder, boolean aIncludeHTMLStyle, boolean aAllContentEditable,
			boolean aIncludeAttributes, String aLocaleStr) {
		if (aExportAsExcel) {
			return exportCaseComponentContentAsExcel(aCaseComponents, aTagNames, aContentTypes, aContentNames,
					aAllContentEditable, aIncludeAttributes, aLocaleStr, "casecomponentscontent.xlsx");
		} else {
			return exportCaseComponentContentAsZip(aCaseComponents, aTagNames, aContentTypes, aContentNames,
					aIncludeStreamingFolder, aIncludeHTMLStyle, aAllContentEditable, aIncludeAttributes, aLocaleStr,
					"casecomponentscontent.zip");
		}
	}

	/**
	 * Export case components. Exports case components and blobs as xml files and
	 * zips them with the blob files in one zip file, which is returned as path plus
	 * filename. Db ids, such as cac ids and blo ids are converted to world wide
	 * unique ids and references to these ids within the xml data of the case
	 * component are replaced by these world wide unique ids.
	 *
	 * @param aCaseComponents the a case components
	 * @param aZipFileName    the a zip file name
	 *
	 * @return export file, empty if not successful
	 */
	public String exportCaseComponents(List<IECaseComponent> aCaseComponents, String aZipFileName) {
		if (aCaseComponents == null || aCaseComponents.size() == 0)
			return "";
		Hashtable<String, String> lComIds = new Hashtable<String, String>(0);
		Hashtable<String, String> lCacIds = new Hashtable<String, String>(0);
		Hashtable<String, String> lCarIds = new Hashtable<String, String>(0);
		List<String> lBlobIds = new ArrayList<String>();
		Hashtable<String, String> lUUIDBlobIds = new Hashtable<String, String>(0);
		List<String> toZipList = new ArrayList<String>();
		List<String> removeList = new ArrayList<String>();

		createCaseComponentsXmlFile(aCaseComponents, "0", lComIds, lCacIds, lCarIds, lBlobIds, lUUIDBlobIds, toZipList,
				removeList);
		createComponentsXmlFile(lComIds, toZipList, removeList);
		createBlobsXmlFileAndBlobFiles(lBlobIds, lUUIDBlobIds, toZipList, removeList, false, 0);

		String lTempPath = getTempPath();
		String lBlobPath = getAppManager().getAbsoluteBlobPath();
		exportFileName = lTempPath + aZipFileName;
		String lSubUrl = "";
		if (Sessions.getCurrent() != null) {
			lSubUrl = CDesktopComponents.vView().getUniqueTempSubPath();
		}
		String lUrl = CDesktopComponents.vView().getEmergoRootUrl() + CDesktopComponents.vView().getEmergoWebappsRoot()
				+ VView.getInitParameter("emergo.temp.path") + lSubUrl + aZipFileName;
		if (packageFilesToZip(toZipList, lTempPath, lBlobPath, "", new File(exportFileName))) {
			// remove files in removeList
			for (String lFileName : removeList) {
				removeFile(lFileName);
			}
			return lUrl;
		}
		return "";
	}

	/**
	 * Export case component content. Exports case component content and blobs as
	 * xml files and zips them with the blob files in one zip file, which is
	 * returned as path plus filename. Db ids, such as cac ids and blo ids are
	 * converted to world wide unique ids and references to these ids within the xml
	 * data of the case component are replaced by these world wide unique ids.
	 *
	 * @param aCaseComponents         the a case components
	 * @param aTagNames               the a tag names
	 * @param aContentTypes           the a content types
	 * @param aContentNames           the a content names
	 * @param aIncludeStreamingFolder the a include streaming folder
	 * @param aIncludeHTMLStyle       the a include html style
	 * @param aAllContentEditable     the a include all content
	 * @param aIncludeAttributes      the a include attributes
	 * @param aLocaleStr              the a locale str
	 * @param aFileName               the a file name
	 *
	 * @return export file, empty if not successful
	 */
	public String exportCaseComponentContentAsZip(List<IECaseComponent> aCaseComponents, List<String> aTagNames,
			List<String> aContentTypes, List<String> aContentNames, boolean aIncludeStreamingFolder,
			boolean aIncludeHTMLStyle, boolean aAllContentEditable, boolean aIncludeAttributes, String aLocaleStr,
			String aFileName) {
		if (aCaseComponents == null || aCaseComponents.size() == 0)
			return "";
		Hashtable<String, String> lComIds = new Hashtable<String, String>(0);
		Hashtable<String, String> lCacIds = new Hashtable<String, String>(0);
		Hashtable<String, String> lCarIds = new Hashtable<String, String>(0);
		List<String> lBlobIds = new ArrayList<String>();
		Hashtable<String, String> lUUIDBlobIds = new Hashtable<String, String>(0);
		List<String> toZipList = new ArrayList<String>();
		List<String> removeList = new ArrayList<String>();

		createCaseComponentContentHTMLFile(aCaseComponents, aTagNames, aContentTypes, aContentNames,
				aIncludeStreamingFolder, aIncludeHTMLStyle, aAllContentEditable, aIncludeAttributes, aLocaleStr, "0",
				lComIds, lCacIds, lCarIds, lBlobIds, lUUIDBlobIds, toZipList, removeList);
		int lCasId = 0;
		if (aIncludeStreamingFolder && aCaseComponents.size() > 0) {
			IECase lCase = aCaseComponents.get(0).getECase();
			if (lCase.getCasCasId() == 0) {
				lCasId = lCase.getCasId();
			} else {
				lCasId = getSpring().getCaseManager().getCase(lCase.getCasCasId()).getCasId();
			}
		}
		createBlobsXmlFileAndBlobFiles(lBlobIds, lUUIDBlobIds, toZipList, removeList, aIncludeStreamingFolder, lCasId);

		String lTempPath = getTempPath();
		String lBlobPath = getAppManager().getAbsoluteBlobPath();
		exportFileName = lTempPath + aFileName;
		String lSubUrl = "";
		if (Sessions.getCurrent() != null) {
			lSubUrl = CDesktopComponents.vView().getUniqueTempSubPath();
		}
		String lUrl = CDesktopComponents.vView().getEmergoRootUrl() + CDesktopComponents.vView().getEmergoWebappsRoot()
				+ VView.getInitParameter("emergo.temp.path") + lSubUrl + aFileName;
		if (packageFilesToZip(toZipList, lTempPath, lBlobPath, getAppManager().getAbsoluteAppPath(),
				new File(exportFileName))) {
			// remove files in removeList
			for (String lFileName : removeList) {
				removeFile(lFileName);
			}
			return lUrl;
		}
		return "";
	}

	/**
	 * Export case component content. Exports case component content as excel file,
	 * which is returned as path plus filename.
	 *
	 * @param aCaseComponents     the a case components
	 * @param aTagNames           the a tag names
	 * @param aContentTypes       the a content types
	 * @param aContentNames       the a content names
	 * @param aAllContentEditable the a include all content
	 * @param aIncludeAttributes  the a include attributes
	 * @param aLocaleStr          the a locale str
	 * @param aFileName           the a file name
	 *
	 * @return export file, empty if not successful
	 */
	public String exportCaseComponentContentAsExcel(List<IECaseComponent> aCaseComponents, List<String> aTagNames,
			List<String> aContentTypes, List<String> aContentNames, boolean aAllContentEditable,
			boolean aIncludeAttributes, String aLocaleStr, String aFileName) {
		if (aCaseComponents == null || aCaseComponents.size() == 0)
			return "";

		String lTempPath = getTempPath();

		createCaseComponentContentExcelFile(aCaseComponents, aTagNames, aContentTypes, aContentNames,
				aAllContentEditable, aIncludeAttributes, aLocaleStr, lTempPath, aFileName);

		exportFileName = lTempPath + aFileName;
		String lSubUrl = "";
		if (Sessions.getCurrent() != null) {
			lSubUrl = CDesktopComponents.vView().getUniqueTempSubPath();
		}
		String lUrl = CDesktopComponents.vView().getEmergoRootUrl() + CDesktopComponents.vView().getEmergoWebappsRoot()
				+ VView.getInitParameter("emergo.temp.path") + lSubUrl + aFileName;
		return lUrl;
	}

	/**
	 * Export components. Exports components as xml files and zips them in one zip
	 * file, which is returned as path plus filename. Db ids, such as com ids are
	 * converted to world wide unique ids.
	 *
	 * @param aComponents the components
	 *
	 * @return export file, empty if not successful
	 */
	public String exportComponents(List<IEComponent> aComponents) {
		if (aComponents == null)
			return "";
		Hashtable<String, String> lComIds = new Hashtable<String, String>(0);
		for (IEComponent lComponent : aComponents) {
			lComIds.put("" + lComponent.getComId(), lComponent.getUuid());
		}

		List<String> toZipList = new ArrayList<String>();
		List<String> removeList = new ArrayList<String>();

		createComponentsXmlFile(lComIds, toZipList, removeList);

		String lTempPath = getTempPath();
		String lBlobPath = getAppManager().getAbsoluteBlobPath();
		exportFileName = lTempPath + "components.zip";
		String lSubUrl = "";
		if (Sessions.getCurrent() != null) {
			lSubUrl = CDesktopComponents.vView().getUniqueTempSubPath();
		}
		String lUrl = CDesktopComponents.vView().getEmergoRootUrl() + CDesktopComponents.vView().getEmergoWebappsRoot()
				+ VView.getInitParameter("emergo.temp.path") + lSubUrl + "components.zip";
		if (packageFilesToZip(toZipList, lTempPath, lBlobPath, "", new File(exportFileName))) {
			// remove files in removeList
			for (String lFileName : removeList) {
				removeFile(lFileName);
			}
			return lUrl;
		}
		return "";
	}

	/**
	 * Create case xml file.
	 *
	 * @param aCase       the a case
	 * @param aToZipList  the a to zip list, is updated within this method
	 * @param aRemoveList the a remove list, is updated within this method
	 *
	 * @return uuid for case
	 */
	public String createCaseXmlFile(IECase aCase, List<String> aToZipList, List<String> aRemoveList) {
		if (aCase == null)
			return "";
		// create xml file for case
		String lXml = getSpring().getEmptyXml();
		IXMLTag lRootTag = getXmlManager().getXmlTree(null, lXml, AppConstants.other_tag);
		IXMLTag lComponentTag = lRootTag.getChild(AppConstants.componentElement);
		IXMLTag lDbTag = addChildTag(lComponentTag, "db_case", "");
		IXMLTag lTag = addChildTag(lDbTag, "item", "");
		String lUUIDCasId = UUIDGenerator.getInstance().generateTimeBasedUUID().toString();
		addChildTag(lTag, "casid", lUUIDCasId);
		addChildTag(lTag, "originalcasid", "" + aCase.getCasId());
		addChildTag(lTag, "code", escapeXML("" + aCase.getCode()));
		addChildTag(lTag, "name", escapeXML("" + aCase.getName()));
		addChildTag(lTag, "status", "" + aCase.getStatus());
		addChildTag(lTag, "version", "" + aCase.getVersion());
		addChildTag(lTag, "active", "" + aCase.getActive());
		addChildTag(lTag, "skin", "" + getSpring().getSCaseSkinHelper().getCaseSkin(aCase));
		addChildTag(lTag, "multilingual", "" + aCase.getMultilingual());
		String lDbXmlData = getXmlManager().xmlTreeToDoc(lRootTag, "UTF-8");
		String lTempPath = getTempPath();
		saveXMLAsFile(lDbXmlData, lTempPath, "case.xml");
		aToZipList.add(lTempPath + "case.xml");
		aRemoveList.add(lTempPath + "case.xml");
		return lUUIDCasId;
	}

	/**
	 * Create case roles xml file.
	 *
	 * @param aCasId      the a cas id
	 * @param aUUIDCasId  the a UUID cas id
	 * @param aCarIds     the a car ids, is updated within this method
	 * @param aToZipList  the a to zip list, is updated within this method
	 * @param aRemoveList the a remove list, is updated within this method
	 */
	public void createCaseRolesXmlFile(int aCasId, String aUUIDCasId, Hashtable<String, String> aCarIds,
			List<String> aToZipList, List<String> aRemoveList) {
		// create xml file for caseroles
		ICaseRoleManager caseRoleManager = (ICaseRoleManager) getBean("caseRoleManager");
		List<IECaseRole> lCaseRoles = caseRoleManager.getAllCaseRolesByCasId(aCasId);
		String lXml = getSpring().getEmptyXml();
		IXMLTag lRootTag = getXmlManager().getXmlTree(null, lXml, AppConstants.other_tag);
		IXMLTag lComponentTag = lRootTag.getChild(AppConstants.componentElement);
		IXMLTag lDbTag = addChildTag(lComponentTag, "db_caseroles", "");
		for (IECaseRole lItem : lCaseRoles) {
			IXMLTag lTag = addChildTag(lDbTag, "item", "");
			// maybe use world wide unique ids
			String lUUID = UUIDGenerator.getInstance().generateTimeBasedUUID().toString();
			addChildTag(lTag, "carid", lUUID);
			addChildTag(lTag, "originalcarid", "" + lItem.getCarId());
			addChildTag(lTag, "cascasid", aUUIDCasId);
			addChildTag(lTag, "name", escapeXML("" + lItem.getName()));
			addChildTag(lTag, "npc", "" + lItem.getNpc());
			aCarIds.put("" + lItem.getCarId(), lUUID);
		}
		String lDbXmlData = getXmlManager().xmlTreeToDoc(lRootTag, "UTF-8");
		String lTempPath = getTempPath();
		saveXMLAsFile(lDbXmlData, lTempPath, "caseroles.xml");
		aToZipList.add(lTempPath + "caseroles.xml");
		aRemoveList.add(lTempPath + "caseroles.xml");
	}

	/**
	 * Create case components xml file.
	 *
	 * @param aCasId       the a cas id
	 * @param aUUIDCasId   the a UUID cas id
	 * @param aComIds      the a com ids, is updated within this method
	 * @param aCacIds      the a cac ids, is updated within this method
	 * @param aCarIds      the a car ids
	 * @param aBlobIds     the a blob ids, is updated within this method
	 * @param aUUIDBlobIds the a UUID blob ids, is updated within this method
	 * @param aToZipList   the a to zip list, is updated within this method
	 * @param aRemoveList  the a remove list, is updated within this method
	 */
	public void createCaseComponentsXmlFile(int aCasId, String aUUIDCasId, Hashtable<String, String> aComIds,
			Hashtable<String, String> aCacIds, Hashtable<String, String> aCarIds, List<String> aBlobIds,
			Hashtable<String, String> aUUIDBlobIds, List<String> aToZipList, List<String> aRemoveList) {
		// create xml file for casecomponents
		ICaseComponentManager caseComponentManager = (ICaseComponentManager) getBean("caseComponentManager");
		List<IECaseComponent> lCaseComponents = caseComponentManager.getAllCaseComponentsByCasId(aCasId);
		createCaseComponentsXmlFile(lCaseComponents, aUUIDCasId, aComIds, aCacIds, aCarIds, aBlobIds, aUUIDBlobIds,
				aToZipList, aRemoveList);
	}

	/**
	 * Create case components xml file.
	 *
	 * @param aCasId       the a cas id
	 * @param aUUIDCasId   the a UUID cas id
	 * @param aComIds      the a com ids, is updated within this method
	 * @param aCacIds      the a cac ids, is updated within this method
	 * @param aCarIds      the a car ids
	 * @param aBlobIds     the a blob ids, is updated within this method
	 * @param aUUIDBlobIds the a UUID blob ids, is updated within this method
	 * @param aToZipList   the a to zip list, is updated within this method
	 * @param aRemoveList  the a remove list, is updated within this method
	 */
	public void createCaseComponentsXmlFile(List<IECaseComponent> aCaseComponents, String aUUIDCasId,
			Hashtable<String, String> aComIds, Hashtable<String, String> aCacIds, Hashtable<String, String> aCarIds,
			List<String> aBlobIds, Hashtable<String, String> aUUIDBlobIds, List<String> aToZipList,
			List<String> aRemoveList) {
		// create xml file for casecomponents
		String lXml = getSpring().getEmptyXml();
		IXMLTag lRootTag = getXmlManager().getXmlTree(null, lXml, AppConstants.other_tag);
		IXMLTag lComponentTag = lRootTag.getChild(AppConstants.componentElement);
		IXMLTag lDbTag = addChildTag(lComponentTag, "db_casecomponents", "");
		List<IXMLTag> lDataRootTags = new ArrayList<IXMLTag>();
		for (IECaseComponent lItem : aCaseComponents) {
			IXMLTag lTag = addChildTag(lDbTag, "item", "");
			// maybe use world wide unique ids
			String lUUID = UUIDGenerator.getInstance().generateTimeBasedUUID().toString();
			addChildTag(lTag, "cacid", lUUID);
			addChildTag(lTag, "originalcacid", "" + lItem.getCacId());
			addChildTag(lTag, "cascasid", aUUIDCasId);
			addChildTag(lTag, "comcomid", "" + lItem.getEComponent().getComId());
			addChildTag(lTag, "comcomuuid", lItem.getEComponent().getUuid());
			addChildTag(lTag, "name", escapeXML("" + lItem.getName()));
			IXMLTag lXmlDataTag = addChildTag(lTag, "xmldata", "");
			String lXmlData = lItem.getXmldata();
			if ((lXmlData == null) || (lXmlData.equals("")))
				lXmlData = getSpring().getEmptyXml();
			IXMLTag lDataRootTag = getXmlManager().getXmlTree(null, lXmlData, AppConstants.other_tag);
			lDataRootTags.add(lDataRootTag);
			addChildTag(lXmlDataTag, lDataRootTag);
			// get references to blobids and replace them with UUIDs
			getBlobIds(lDataRootTag, aBlobIds, aUUIDBlobIds, true);
			aCacIds.put("" + lItem.getCacId(), lUUID);
			aComIds.put("" + lItem.getEComponent().getComId(), lItem.getEComponent().getUuid());
		}
		for (IXMLTag lDataRootTag : lDataRootTags) {
			// replace all references to carids, comids or cacids within xml by UUIDS
			// in principle all casecomponents can have references to carids, comids or
			// cacids
			replaceIdsByUUIDS(lDataRootTag, aCarIds, aComIds, aCacIds);
		}
		String lDbXmlData = getXmlManager().xmlTreeToDoc(lRootTag, "UTF-8");
		String lTempPath = getTempPath();
		saveXMLAsFile(lDbXmlData, lTempPath, "casecomponents.xml");
		aToZipList.add(lTempPath + "casecomponents.xml");
		aRemoveList.add(lTempPath + "casecomponents.xml");
	}

	private StringBuffer getHtmlData(IECaseComponent aCaseComponent, IECaseRole aCaseRole, IXMLTag aTag, String aType,
			String aKey, String aValue, boolean aEditable, boolean aIncludeHTMLStyle) {
		StringBuffer lKeyStyle = new StringBuffer();
		StringBuffer lSubKeyStyle = new StringBuffer();
		StringBuffer lValueStyle = new StringBuffer();
		if (aIncludeHTMLStyle) {
			lKeyStyle.append(" style=\"");
			lKeyStyle.append("margin-left:20px;color:white;");
			if (aType.equals("casecomponent")) {
				lKeyStyle.append("color:#990000;");
			} else if (aType.equals("node")) {
				lKeyStyle.append("color:#009900;");
			} else if (aType.equals("child")) {
				lKeyStyle.append("color:#000099;");
			} else if (aType.equals("attribute")) {
				lKeyStyle.append("color:#009999;");
			}
			lKeyStyle.append("\"");
			lSubKeyStyle.append(" style=\"");
			lSubKeyStyle.append("margin-left:40px;");
			lSubKeyStyle.append("\"");
			lValueStyle.append(" style=\"");
			lValueStyle.append("margin-left:40px;");
			if (aEditable) {
				lValueStyle.append("border:1px solid gray;padding:10px;");
			}
			lValueStyle.append("\"");
		}
		StringBuffer lRow = new StringBuffer();
		if (aKey.length() > 0) {
			lRow.append("<div");
			lRow.append(lKeyStyle);
			lRow.append(">");
			if (aIncludeHTMLStyle) {
				lRow.append("<b><i>");
				lRow.append(aKey);
				lRow.append("</i></b>");
			} else {
				lRow.append(aKey);
			}
			lRow.append("</div>");
		}
		if (aCaseRole != null) {
			lRow.append("<div");
			lRow.append(lSubKeyStyle);
			lRow.append(">");
			if (aIncludeHTMLStyle) {
				lRow.append("<i>");
				lRow.append("for caserole '" + aCaseRole.getName() + "':");
				lRow.append("</i>");
			} else {
				lRow.append("for caserole '" + aCaseRole.getName() + "':");
			}
			lRow.append("</div>");
		}
		lRow.append("<div");
		if (aEditable) {
			lRow.append(" id=\"");
			lRow.append("emergo_cacid=");
			lRow.append(aCaseComponent.getCacId());
			lRow.append("_");
			if (aCaseRole != null) {
				lRow.append("carid=");
				lRow.append(aCaseRole.getCarId());
				lRow.append("_");
			}
			if (aTag != null) {
				lRow.append("tagid=");
				lRow.append(aTag.getAttribute(AppConstants.keyId));
				lRow.append("_");
			}
			lRow.append(aType);
			lRow.append("=");
			lRow.append(aKey);
			lRow.append("\"");
		}
		lRow.append(lValueStyle);
		lRow.append(">");
		lRow.append(aValue);
		lRow.append("</div>");
		return lRow;
	}

	private String[] getExcelData(IECaseComponent aCaseComponent, IECaseRole aCaseRole, IXMLTag aTag, String aType,
			String aKey, String aStatus, int aIndex, String aHelp, String aValue) {
		String[] lData = new String[] { "", "", "", "", "", "", "", "", "" };
		lData[0] = "" + aCaseComponent.getCacId();
		if (aCaseRole != null) {
			lData[1] = "" + aCaseRole.getCarId();
		}
		if (aTag != null) {
			lData[2] = "" + aTag.getAttribute(AppConstants.keyId);
		}
		if (!StringUtils.isEmpty(aType)) {
			lData[3] = aType;
		}
		if (!StringUtils.isEmpty(aKey)) {
			lData[4] = aKey;
		}
		lData[5] = aStatus;
		lData[6] = "" + aIndex;
		lData[7] = aHelp;
		lData[8] = aValue;
		return lData;
	}

	/**
	 * Create case components content html file.
	 *
	 * @param aCasId                  the a cas id
	 * @param aTagNames               the a tag names
	 * @param aContentTypes           the a content types
	 * @param aContentNames           the a content names
	 * @param aIncludeStreamingFolder the a include streaming folder
	 * @param aIncludeHTMLStyle       the a include html style
	 * @param aAllContentEditable     the a include all content
	 * @param aIncludeAttributes      the a include attributes
	 * @param aLocaleStr              the a locale str
	 * @param aUUIDCasId              the a UUID cas id
	 * @param aComIds                 the a com ids, is updated within this method
	 * @param aCacIds                 the a cac ids, is updated within this method
	 * @param aCarIds                 the a car ids
	 * @param aBlobIds                the a blob ids, is updated within this method
	 * @param aUUIDBlobIds            the a UUID blob ids, is updated within this
	 *                                method
	 * @param aToZipList              the a to zip list, is updated within this
	 *                                method
	 * @param aRemoveList             the a remove list, is updated within this
	 *                                method
	 */
	public void createCaseComponentContentHTMLFile(List<IECaseComponent> aCaseComponents, List<String> aTagNames,
			List<String> aContentTypes, List<String> aContentNames, boolean aIncludeStreamingFolder,
			boolean aIncludeHTMLStyle, boolean aAllContentEditable, boolean aIncludeAttributes, String aLocaleStr,
			String aUUIDCasId, Hashtable<String, String> aComIds, Hashtable<String, String> aCacIds,
			Hashtable<String, String> aCarIds, List<String> aBlobIds, Hashtable<String, String> aUUIDBlobIds,
			List<String> aToZipList, List<String> aRemoveList) {

		// TODO create depending on aLocaleStr

		// create html file for casecomponents
		String lHtml = "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">";
		lHtml += "<html>";
		lHtml += "<head>";
		lHtml += "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">";
		lHtml += "<title>$title</title>";
		lHtml += "</head>";
		lHtml += "<body>$body</body>";
		lHtml += "</html>";
		lHtml = lHtml.replace("$title", "Case Component Content");
		String lXml = getSpring().getEmptyXml();
		IXMLTag lRootTag = getXmlManager().getXmlTree(null, lXml, AppConstants.other_tag);
		IXMLTag lComponentTag = lRootTag.getChild(AppConstants.componentElement);
		IXMLTag lDbTag = addChildTag(lComponentTag, "db_casecomponents", "");
		List<IXMLTag> lDataRootTags = new ArrayList<IXMLTag>();
		ICaseComponentRoleManager caseComponentRoleManager = (ICaseComponentRoleManager) getBean(
				"caseComponentRoleManager");
		String lBody = "<div>";
		for (IECaseComponent lCaseComponent : aCaseComponents) {
			IXMLTag lTag = addChildTag(lDbTag, "item", "");
			// maybe use world wide unique ids
			String lUUID = UUIDGenerator.getInstance().generateTimeBasedUUID().toString();
			lBody += "<div>";
			List<IECaseComponentRole> lCaseComponentRoles = caseComponentRoleManager
					.getAllCaseComponentRolesByCacId(lCaseComponent.getCacId());
			int lCounter = 0;
			for (IECaseComponentRole lCaseComponentRole : lCaseComponentRoles) {
				String lKey = "";
				if (lCounter == 0) {
					lKey = "casecomponent";
				}
				String lName = lCaseComponentRole.getName();
				// NOTE strip possible other languages then the default one. These languages may
				// be added by importing other language content.
				// TODO if aLocaleStr is set get correct language from lName
				String lStartLocale = AppConstants.labelVarPrefix;
				int lStart = lName.indexOf(lStartLocale);
				if (lStart >= 0) {
					lName = lName.substring(0, lStart);
				}
				lBody += getHtmlData(lCaseComponent,
						getSpring().getSCaseRoleHelper().getCaseRole(lCaseComponentRole.getCarCarId()), null,
						"casecomponent", lKey, lName, true, aIncludeHTMLStyle).toString();
				addChildTag(lTag, "name", lName);
				lCounter++;
			}
			IXMLTag lXmlDataTag = addChildTag(lTag, "xmldata", "");
//			String lXmlDefinition = lItem.getEComponent().getXmldefinition();
//			if ((lXmlDefinition == null) || (lXmlDefinition.equals("")))
//				lXmlDefinition = getSpring().getEmptyXml();
//			IXMLTag lDefinitionRootTag = getXmlManager().getXmlTree(null, lXmlDefinition, AppConstants.definition_tag);
//			String lXmlData = lItem.getXmldata();
//			if ((lXmlData == null) || (lXmlData.equals("")))
//				lXmlData = getSpring().getEmptyXml();
//			IXMLTag lDataRootTag = getXmlManager().getXmlTree(lDefinitionRootTag, lXmlData, AppConstants.other_tag);
			IXMLTag lDataRootTag = getSpring().getXmlDataTree(lCaseComponent);
			IXMLTag lDataContentTag = lDataRootTag.getChild(AppConstants.contentElement);
			if (lDataRootTag != null) {
				if (lDataContentTag != null) {
					filterData(lDataContentTag.getChildTags(), aTagNames, aContentTypes, aContentNames,
							aIncludeAttributes);
				}
			}
			lDataRootTags.add(lDataRootTag);
			addChildTag(lXmlDataTag, lDataRootTag);
			// get references to blobids and replace them with UUIDs
			getBlobIds(lDataRootTag, aBlobIds, aUUIDBlobIds, false);
			lBody += convertXMLDataToHtml(lCaseComponent, lDataContentTag.getChildTags(), aIncludeStreamingFolder,
					aIncludeHTMLStyle, aAllContentEditable, aIncludeAttributes).toString();
			lBody += "</div>";
			aCacIds.put("" + lCaseComponent.getCacId(), lUUID);
			aComIds.put("" + lCaseComponent.getEComponent().getComId(), lCaseComponent.getEComponent().getUuid());
		}
		for (IXMLTag lDataRootTag : lDataRootTags) {
			// replace all references to carids, comids or cacids within xml by UUIDS
			// in principle all casecomponents can have references to carids, comids or
			// cacids
			replaceIdsByUUIDS(lDataRootTag, aCarIds, aComIds, aCacIds);
		}
		lBody += "</div>";
		lHtml = lHtml.replace("$body", lBody);
		String lTempPath = getTempPath();
		saveHTMLAsFile(lHtml, lTempPath, "casecomponentscontent.html");
		aToZipList.add(lTempPath + "casecomponentscontent.html");
		aRemoveList.add(lTempPath + "casecomponentscontent.html");
	}

	/**
	 * Create case components content excel file.
	 *
	 * @param aCasId              the a cas id
	 * @param aTagNames           the a tag names
	 * @param aContentTypes       the a content types
	 * @param aContentNames       the a content names
	 * @param aAllContentEditable the a include all content
	 * @param aIncludeAttributes  the a include attributes
	 * @param aLocaleStr          the a locale str
	 * @param aTempPath           the a temp path
	 * @param aFileName           the a file name
	 */
	public void createCaseComponentContentExcelFile(List<IECaseComponent> aCaseComponents, List<String> aTagNames,
			List<String> aContentTypes, List<String> aContentNames, boolean aAllContentEditable,
			boolean aIncludeAttributes, String aLocaleStr, String aTempPath, String aFileName) {
		// create html excel file for casecomponents
		ExcelHelper excelHelper = new ExcelHelper();

		Workbook workbook = excelHelper.createOrLoadWorkbook(true, true, aTempPath, aFileName);
		// Create a Font for styling header cells
		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		// Create a CellStyle with the font
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);

		Sheet sheet = workbook.createSheet(caseComponentsContentExcelSheetName);
		workbook.setActiveSheet(workbook.getSheetIndex(caseComponentsContentExcelSheetName));

		int maxContentCols = caseComponentsContentExcelSheetContentHeaders.length;
		int maxInfoCols = caseComponentsContentExcelSheetInfoHeaders.length;
		int rowNum = 0;
		int colNum = 0;

		// NOTE Track all columns for auto sizing. Has to be done before rows are
		// written to the sheet, if SXSSFWorkbook (streaming workbook) is used.
		for (int i = colNum; i < (colNum + maxContentCols); i++) {
			((SXSSFSheet) sheet).trackColumnForAutoSizing(i);
		}

		CellStyle[] cellStyles = new CellStyle[maxInfoCols];
		for (int i = colNum; i < (colNum + maxInfoCols); i++) {
			cellStyles[i] = headerCellStyle;
		}
		excelHelper.createOrUpdateXLSXRow(sheet, rowNum, colNum, null, cellStyles,
				caseComponentsContentExcelSheetInfoHeaders, false);
		rowNum++;

		// NOTE put info values
		if (!StringUtils.isEmpty(aLocaleStr)) {
			excelHelper.createOrUpdateXLSXRow(sheet, rowNum, colNum, null, null, new String[] { aLocaleStr }, false);
		}
		rowNum++;
		// NOTE skip one line
		rowNum++;

		cellStyles = new CellStyle[maxContentCols];
		for (int i = colNum; i < (colNum + maxContentCols); i++) {
			cellStyles[i] = headerCellStyle;
		}
		excelHelper.createOrUpdateXLSXRow(sheet, rowNum, colNum, caseComponentsContentExcelCellTypes, cellStyles,
				caseComponentsContentExcelSheetContentHeaders, false);
		rowNum++;

		boolean lUseLocale = !StringUtils.isEmpty(aLocaleStr);

		// Create a CellStyle with wrap text to wrap the value cell, the last cell
		CellStyle valueCellStyle = workbook.createCellStyle();
		valueCellStyle.setWrapText(true);

		cellStyles = new CellStyle[maxContentCols];
		for (int i = colNum; i < (colNum + maxContentCols); i++) {
			if (i < (colNum + maxContentCols - 1)) {
				cellStyles[i] = null;
			} else {
				cellStyles[i] = valueCellStyle;
			}
		}

		ICaseComponentRoleManager caseComponentRoleManager = (ICaseComponentRoleManager) getBean(
				"caseComponentRoleManager");
		for (IECaseComponent lCaseComponent : aCaseComponents) {
			List<IECaseComponentRole> lCaseComponentRoles = caseComponentRoleManager
					.getAllCaseComponentRolesByCacId(lCaseComponent.getCacId());
			for (IECaseComponentRole lCaseComponentRole : lCaseComponentRoles) {
				String lName = lCaseComponentRole.getName();
				// NOTE strip possible other languages then the default one. These languages may
				// be added by importing other language content.
				boolean lNoLocaleFound = true;
				String lStatus = "";
				if (lUseLocale) {
					String lStartLocale = AppConstants.labelVarPrefix + aLocaleStr + AppConstants.labelVarPostfix;
					String lEndLocale = AppConstants.labelVarPrefix + "/" + aLocaleStr + AppConstants.labelVarPostfix;
					int lStart = lName.indexOf(lStartLocale);
					int lEnd = lName.indexOf(lEndLocale);
					if (lStart >= 0 && lEnd >= (lStart + lStartLocale.length())) {
						lName = lName.substring(lStart + lStartLocale.length(), lEnd);
						lNoLocaleFound = false;
					} else {
						// NOTE use default value
						lStatus = caseComponentsContentExcelSheetStatusDefault;
					}
				}
				if (lNoLocaleFound) {
					String lStartLocale = AppConstants.labelVarPrefix;
					int lStart = lName.indexOf(lStartLocale);
					if (lStart >= 0) {
						lName = lName.substring(0, lStart);
					}
				}
				excelHelper.createOrUpdateXLSXRow(sheet, rowNum, colNum, caseComponentsContentExcelCellTypes,
						cellStyles,
						getExcelData(lCaseComponent,
								getSpring().getSCaseRoleHelper().getCaseRole(lCaseComponentRole.getCarCarId()), null,
								"casecomponent", "", lStatus, 1, "", lName),
						false);
				rowNum++;
			}
			IXMLTag lDataRootTag = getSpring().getXmlDataTree(lCaseComponent);
			IXMLTag lDataContentTag = lDataRootTag.getChild(AppConstants.contentElement);
			if (lDataRootTag != null && lDataContentTag != null) {
				filterData(lDataContentTag.getChildTags(), aTagNames, aContentTypes, aContentNames, aIncludeAttributes);
			}
			List<String[]> strings = convertXMLDataToExcel(lCaseComponent, lDataContentTag.getChildTags(),
					aAllContentEditable, aIncludeAttributes, aLocaleStr);
			for (String[] string : strings) {
				excelHelper.createOrUpdateXLSXRow(sheet, rowNum, colNum, caseComponentsContentExcelCellTypes,
						cellStyles, string, false);
				rowNum++;
			}
		}

		// Resize all columns to fit the content size
		for (int i = colNum; i < (colNum + maxContentCols); i++) {
			if (i == (colNum + maxContentCols - 3)) {
				// index column, do nothing
			} else if (i == (colNum + maxContentCols - 1)) {
				// value column, auto size
				sheet.autoSizeColumn(i);
			} else {
				// other columns, set width to 0
				sheet.setColumnWidth(i, 0);
			}
		}

		excelHelper.writeAndCloseWorkbook(workbook, getTempPath(), aFileName, null, CDesktopComponents.vView());
	}

	/**
	 * Filters data.
	 *
	 * @param aDataChildTags     the a data child tags
	 * @param aTagNames          the a tag names
	 * @param aContentTypes      the a content types
	 * @param aContentNames      the a content names
	 * @param aIncludeAttributes the a include attributes
	 */
	protected void filterData(List<IXMLTag> aDataChildTags, List<String> aTagNames, List<String> aContentTypes,
			List<String> aContentNames, boolean aIncludeAttributes) {
		for (int i = (aDataChildTags.size() - 1); i >= 0; i--) {
			IXMLTag lDataChildTag = aDataChildTags.get(i);
			String contentType = lDataChildTag.getDefAttribute(AppConstants.defKeyType);
			if (contentType.equals(AppConstants.defValueNode)) {
				filterData(lDataChildTag.getChildTags(), aTagNames, aContentTypes, aContentNames, aIncludeAttributes);
				if (!aTagNames.contains(lDataChildTag.getName())
						&& lDataChildTag.getChildTags(AppConstants.defValueNode).size() == 0) {
					aDataChildTags.remove(lDataChildTag);
				}
			} else {
				if (contentType.equals("")
						|| !(aContentTypes.contains(contentType) && aContentNames.contains(lDataChildTag.getName()))) {
					// NOTE don't remove status child tag if attributes have to be included
					if (!(aIncludeAttributes && lDataChildTag.getName().equals(AppConstants.statusElement))) {
						aDataChildTags.remove(lDataChildTag);
					}
				} else {
					lDataChildTag.setValue(getXmlManager().unescapeXML(lDataChildTag.getValue()));
				}
			}
		}
	}

	/**
	 * Converts XML data to HTML.
	 *
	 * @param aCaseComponent          the a case component
	 * @param aDataChildTags          the a data child tags
	 * @param aIncludeStreamingFolder the a include streaming folder
	 * @param aIncludeHTMLStyle       the a include html style
	 * @param aAllContentEditable     the a include all content
	 * @param aIncludeAttributes      the a include attributes
	 */
	protected StringBuffer convertXMLDataToHtml(IECaseComponent aCaseComponent, List<IXMLTag> aDataChildTags,
			boolean aIncludeStreamingFolder, boolean aIncludeHTMLStyle, boolean aAllContentEditable,
			boolean aIncludeAttributes) {
		String lStreamingPath = getAppManager().getAbsoluteStreamingPath();
		String lStreamingSubPath = lStreamingPath + aCaseComponent.getECase().getCasId() + "\\";
		// NOTE remove first '/' character from streaming path
		String lUrlPath = VView.getInitParameter("emergo.streaming.path").substring(1);
		String lUrlSubPath = lUrlPath + aCaseComponent.getECase().getCasId() + "/";

		StringBuffer lData = new StringBuffer();
		for (IXMLTag lDataChildTag : aDataChildTags) {
			String contentType = lDataChildTag.getDefAttribute(AppConstants.defKeyType);
			if (contentType.equals(AppConstants.defValueNode)) {
				lData.append("<div");
				if (aIncludeHTMLStyle) {
					lData.append(" style=\"margin-left:20px;\"");
				}
				lData.append(">");

				lData.append(getHtmlData(aCaseComponent, null, lDataChildTag, "node", "node", lDataChildTag.getName(),
						false, aIncludeHTMLStyle));

				if (aIncludeAttributes) {
					// loop through all setable attributes of node tag
					String lDefAttributeNames = "";
					IXMLTag lDefChildTag = lDataChildTag.getDefChild(AppConstants.initialstatusElement);
					if (lDefChildTag != null) {
						lDefAttributeNames = lDefChildTag.getAttribute(AppConstants.defKeyAttributes);
					}
					if (!lDefAttributeNames.equals("")) {
						String[] lDefAttributeNameArr = lDefAttributeNames.split(",");
						for (int i = 0; i < lDefAttributeNameArr.length; i++) {
							lData.append("<div");
							if (aIncludeHTMLStyle) {
								lData.append(" style=\"margin-left:20px;\"");
							}
							lData.append(">");
							String lValue = lDataChildTag.getDefChildAttribute(AppConstants.statusElement,
									lDefAttributeNameArr[i]);
							lData.append(getHtmlData(aCaseComponent, null, lDataChildTag, "attribute",
									lDefAttributeNameArr[i], lValue, aAllContentEditable, aIncludeHTMLStyle));
							lData.append("</div>");
						}
					}
				}

				lData.append(convertXMLDataToHtml(aCaseComponent, lDataChildTag.getChildTags(), aIncludeStreamingFolder,
						aIncludeHTMLStyle, aAllContentEditable, aIncludeAttributes));

				lData.append("</div>");
			} else {
				if (!lDataChildTag.getValue().equals("")) {
					String lDataType = lDataChildTag.getDefAttribute(AppConstants.defKeyType);
					// NOTE blobs are not editable
					boolean lIsBlob = lDataType.equals("blob") || lDataType.equals("picture");
					String lValue = "";
					// NOTE blobs are not editable
					boolean lEditable = !lIsBlob && (aAllContentEditable || lDataChildTag.getName().matches(
							"name|title|shorttitle|text|defaulttext|richtext|hovertext|description|correctanswer"));
					if (lIsBlob) {
						String lBlobtype = lDataChildTag.getAttribute(AppConstants.keyBlobtype);
						if (lBlobtype.equals("")) {
							lBlobtype = AppConstants.blobtypeDatabase;
						}
						String lBlobId = lDataChildTag.getValue();
						if (!lBlobId.equals("")) {
							IEBlob lBlob = getBlobManager().getBlob(Integer.parseInt(lBlobId));
							if (lBlobtype.equals(AppConstants.blobtypeDatabase) && lBlob.getUrl() != null
									&& !lBlob.getUrl().equals("")) {
								lBlobtype = AppConstants.blobtypeIntUrl;
							}
							if (lBlob != null && lBlob.getFilename() != null && !lBlob.getFilename().equals("")) {
								if (lBlobtype.equals(AppConstants.blobtypeDatabase)) {
									lValue = "<a href=\"" + lBlob.getBloId() + "/" + lBlob.getFilename()
											+ "\" target=\"_blank\">" + lBlob.getFilename() + "</a>";
								}
							}
							if (lBlob != null && lBlob.getUrl() != null && !lBlob.getUrl().equals("")) {
								if (lBlobtype.equals(AppConstants.blobtypeIntUrl)) {
									if (aIncludeStreamingFolder) {
										// NOTE blob file may exist in streaming sub folder for case or in streaming
										// folder itself
										if (new File(lStreamingSubPath + "\\" + lBlob.getUrl()).exists()) {
											// file in streaming sub folder
											lValue = "<a href=\"" + lUrlSubPath + lBlob.getUrl()
													+ "\" target=\"_blank\">" + lBlob.getUrl() + "</a>";
										} else {
											if (new File(lStreamingPath + "\\" + lBlob.getUrl()).exists()) {
												// file in streaming folder
												lValue = "<a href=\"" + lUrlPath + lBlob.getUrl()
														+ "\" target=\"_blank\">" + lBlob.getUrl() + "</a>";
											} else {
												// file not found
												lValue = lBlob.getUrl();
											}
										}
									} else {
										// streaming file is not exported
										lValue = lBlob.getUrl();
									}
								} else if (lBlobtype.equals(AppConstants.blobtypeExtUrl)) {
									lValue = "<a href=\"" + lBlob.getUrl() + "\" target=\"_blank\">" + lBlob.getUrl()
											+ "</a>";
								}
							}
						}
					} else {
						lValue = lDataChildTag.getValue();
					}
					lData.append("<div");
					if (aIncludeHTMLStyle) {
						lData.append(" style=\"margin-left:20px;\"");
					}
					lData.append(">");
					lData.append(getHtmlData(aCaseComponent, null, lDataChildTag.getParentTag(), "child",
							lDataChildTag.getName(), lValue, lEditable, aIncludeHTMLStyle));
					lData.append("</div>");
				}
			}
		}
		return lData;
	}

	/**
	 * Converts XML data to HTML.
	 *
	 * @param aCaseComponent      the a case component
	 * @param aDataChildTags      the a data child tags
	 * @param aAllContentEditable the a include all content
	 * @param aIncludeAttributes  the a include attributes
	 * @param aLocaleStr          the a locale str
	 */
	protected List<String[]> convertXMLDataToExcel(IECaseComponent aCaseComponent, List<IXMLTag> aDataChildTags,
			boolean aAllContentEditable, boolean aIncludeAttributes, String aLocaleStr) {
		List<String[]> lStrings = new ArrayList<String[]>();
		boolean lUseLocale = !StringUtils.isEmpty(aLocaleStr);
		for (IXMLTag lDataChildTag : aDataChildTags) {
			String contentType = lDataChildTag.getDefAttribute(AppConstants.defKeyType);
			if (contentType.equals(AppConstants.defValueNode)) {
				if (aIncludeAttributes) {
					// loop through all setable attributes of node tag
					String lDefAttributeNames = "";
					IXMLTag lDefChildTag = lDataChildTag.getDefChild(AppConstants.initialstatusElement);
					if (lDefChildTag != null) {
						lDefAttributeNames = lDefChildTag.getAttribute(AppConstants.defKeyAttributes);
					}
					if (!lDefAttributeNames.equals("")) {
						String[] lDefAttributeNameArr = lDefAttributeNames.split(",");
						for (int i = 0; i < lDefAttributeNameArr.length; i++) {
							String lValue = "";
							String lStatus = "";
							if (lUseLocale) {
								// NOTE default export default value
								lStatus = caseComponentsContentExcelSheetStatusDefault;
								IXMLTag lStatusTag = lDataChildTag.getChild(AppConstants.statusElement);
								if (lStatusTag != null) {
									IXMLTag lLanguageTag = lStatusTag.getChild(aLocaleStr);
									if (lLanguageTag != null) {
										lValue = unescapeXML(lLanguageTag.getValue());
										// NOTE translated value found, don't use export value
										lStatus = "";
									}
								}
							}
							// NOTE if default language is exported or no translated value found
							if (!lUseLocale || lStatus.equals(caseComponentsContentExcelSheetStatusDefault)) {
								lValue = lDataChildTag.getDefChildAttribute(AppConstants.statusElement,
										lDefAttributeNameArr[i]);
							}
							lStrings.add(getExcelData(aCaseComponent, null, lDataChildTag, "attribute",
									lDefAttributeNameArr[i], lStatus, 1, "", lValue));
						}
					}
				}

				lStrings.addAll(convertXMLDataToExcel(aCaseComponent, lDataChildTag.getChildTags(), aAllContentEditable,
						aIncludeAttributes, aLocaleStr));
			} else {
				if (!lDataChildTag.getValue().equals("")) {
					String lDataType = lDataChildTag.getDefAttribute(AppConstants.defKeyType);
					// NOTE blobs are not editable
					boolean lIsBlob = lDataType.equals("blob") || lDataType.equals("picture");
					boolean lIsEditable = !lIsBlob && (aAllContentEditable
							|| lDataChildTag.getName().matches(AppConstants.nodeChildTagsWithTextToExport));

					if (lIsEditable) {
						String lValue = "";
						String lStatus = "";
						if (lUseLocale) {
							// NOTE default export default value
							lStatus = caseComponentsContentExcelSheetStatusDefault;
							IXMLTag lLanguageTag = lDataChildTag.getChild(aLocaleStr);
							if (lLanguageTag != null) {
								lValue = unescapeXML(lLanguageTag.getValue());
								// NOTE translated value found, don't use export value
								lStatus = "";
							}
						}
						// NOTE if default language is exported or no translated value found
						if (!lUseLocale || lStatus.equals(caseComponentsContentExcelSheetStatusDefault)) {
							lValue = lDataChildTag.getValue();
						}
						boolean lIsRichtext = lDataType.equals("simplerichtext") || lDataType.equals("richtext");
						if (lIsRichtext) {
							// NOTE HTML codes needs to be preserved so split HTML codes and texts and store
							// separately in Excel file
							List<Object> result = xmlHelper.splitHtmlCodesAndTexts(lValue);
							String htmlCodes = (String) result.get(0);
							List<String> htmlTexts = (List<String>) result.get(1);
							int index = 1;
							for (String htmlText : htmlTexts) {
								// NOTE if there are multiple texts within HTML codes index indicates the number
								// of the text. The HTML codes are only stored in the Excel file for index = 1
								lStrings.add(getExcelData(aCaseComponent, null, lDataChildTag.getParentTag(), "child",
										lDataChildTag.getName(), lStatus, index, index == 1 ? htmlCodes : "",
										htmlText));
								index++;
							}
						} else {
							lStrings.add(getExcelData(aCaseComponent, null, lDataChildTag.getParentTag(), "child",
									lDataChildTag.getName(), lStatus, 1, "", lValue));
						}
					}
				}
			}
		}
		return lStrings;
	}

	/**
	 * Create case component roles xml file.
	 *
	 * @param aCacIds     the a cac ids
	 * @param aCarIds     the a car ids
	 * @param aToZipList  the a to zip list, is updated within this method
	 * @param aRemoveList the a remove list, is updated within this method
	 */
	public void createCaseComponentRolesXmlFile(Hashtable<String, String> aCacIds, Hashtable<String, String> aCarIds,
			List<String> aToZipList, List<String> aRemoveList) {
		// create xml file for casecomponentroles
		ICaseComponentRoleManager caseComponentRoleManager = (ICaseComponentRoleManager) getBean(
				"caseComponentRoleManager");
		String lXml = getSpring().getEmptyXml();
		IXMLTag lRootTag = getXmlManager().getXmlTree(null, lXml, AppConstants.other_tag);
		IXMLTag lComponentTag = lRootTag.getChild(AppConstants.componentElement);
		IXMLTag lDbTag = addChildTag(lComponentTag, "db_casecomponentroles", "");
		for (Enumeration<String> keys = aCacIds.keys(); keys.hasMoreElements();) {
			String lCacId = keys.nextElement();
			List<IECaseComponentRole> lCaseComponentRoles = caseComponentRoleManager
					.getAllCaseComponentRolesByCacId(Integer.parseInt(lCacId));
			for (IECaseComponentRole lItem : lCaseComponentRoles) {
				String lCarId = "" + lItem.getCarCarId();
				IXMLTag lTag = addChildTag(lDbTag, "item", "");
				// maybe use world wide unique ids
				String lUUID = UUIDGenerator.getInstance().generateTimeBasedUUID().toString();
				addChildTag(lTag, "ccrid", lUUID);
				addChildTag(lTag, "originalccrid", "" + lItem.getCcrId());
				addChildTag(lTag, "caccacid", "" + aCacIds.get(lCacId));
				addChildTag(lTag, "carcarid", "" + aCarIds.get(lCarId));
				addChildTag(lTag, "name", escapeXML("" + lItem.getName()));
			}
		}
		String lDbXmlData = getXmlManager().xmlTreeToDoc(lRootTag, "UTF-8");
		String lTempPath = getTempPath();
		saveXMLAsFile(lDbXmlData, lTempPath, "casecomponentroles.xml");
		aToZipList.add(lTempPath + "casecomponentroles.xml");
		aRemoveList.add(lTempPath + "casecomponentroles.xml");
	}

	/**
	 * Create components xml file.
	 *
	 * @param aComIds     the a com ids
	 * @param aToZipList  the a to zip list, is updated within this method
	 * @param aRemoveList the a remove list, is updated within this method
	 */
	public void createComponentsXmlFile(Hashtable<String, String> aComIds, List<String> aToZipList,
			List<String> aRemoveList) {
		// create xml file for components
		IComponentManager componentManager = (IComponentManager) getBean("componentManager");
		String lXml = getSpring().getEmptyXml();
		IXMLTag lRootTag = getXmlManager().getXmlTree(null, lXml, AppConstants.other_tag);
		IXMLTag lComponentTag = lRootTag.getChild(AppConstants.componentElement);
		IXMLTag lDbTag = addChildTag(lComponentTag, "db_components", "");
		for (Enumeration<String> keys = aComIds.keys(); keys.hasMoreElements();) {
			String lComId = keys.nextElement();
			IEComponent lItem = componentManager.getComponent(Integer.parseInt(lComId));
			IXMLTag lTag = addChildTag(lDbTag, "item", "");
			// maybe use world wide unique ids
			addChildTag(lTag, "comid", lItem.getUuid());
			addChildTag(lTag, "originalcomid", "" + lItem.getComId());
			String lComComId = "";
			String lOriginalComComId = "";
			IEComponent lParent = componentManager.getComponent(lItem.getComComId());
			if (lParent != null) {
				lComComId = lParent.getUuid();
				lOriginalComComId = "" + lParent.getComId();
			}
			addChildTag(lTag, "comcomid", lComComId);
			addChildTag(lTag, "originalcomcomid", lOriginalComComId);
			addChildTag(lTag, "code", escapeXML("" + lItem.getCode()));
			addChildTag(lTag, "type", "" + lItem.getType());
			addChildTag(lTag, "version", "" + lItem.getVersion());
			addChildTag(lTag, "active", "" + lItem.getActive());
			addChildTag(lTag, "multiple", "" + lItem.getMultiple());
			IXMLTag lXmlDefinitionTag = addChildTag(lTag, "xmldefinition", "");
			String lXmldefinition = lItem.getXmldefinition();
			if ((lXmldefinition == null) || (lXmldefinition.equals("")))
				lXmldefinition = getSpring().getEmptyXml();
			IXMLTag lDataRootTag = getXmlManager().getXmlTree(null, lXmldefinition, AppConstants.other_tag);
			addChildTag(lXmlDefinitionTag, lDataRootTag);
			addChildTag(lTag, "creationdate", "" + lItem.getCreationdate().getTime());
			addChildTag(lTag, "lastupdatedate", "" + lItem.getLastupdatedate().getTime());
		}
		String lDbXmlData = getXmlManager().xmlTreeToDoc(lRootTag, "UTF-8");
		String lTempPath = getTempPath();
		saveXMLAsFile(lDbXmlData, lTempPath, "components.xml");
		aToZipList.add(lTempPath + "components.xml");
		aRemoveList.add(lTempPath + "components.xml");
	}

	/**
	 * Create blobs xml file and blob files.
	 *
	 * @param aBlobIds                the a blob ids
	 * @param aUUIDBlobIds            the a UUID blob ids
	 * @param aToZipList              the a to zip list, is updated within this
	 *                                method
	 * @param aRemoveList             the a remove list, is updated within this
	 *                                method
	 * @param aIncludeStreamingFolder the a include streaming folder
	 * @param aStreamingFolderCasId   the a streaming folder cas id
	 */
	public void createBlobsXmlFileAndBlobFiles(List<String> aBlobIds, Hashtable<String, String> aUUIDBlobIds,
			List<String> aToZipList, List<String> aRemoveList, boolean aIncludeStreamingFolder,
			int aStreamingFolderCasId) {
		// create xml file for blobs
		IBlobManager blobManager = (IBlobManager) getBean("blobManager");
		List<Integer> lBloIds = new ArrayList<Integer>();
		for (String lBlobId : aBlobIds) {
			lBloIds.add(Integer.parseInt(lBlobId));
		}
		List<IEBlob> lBlobs = blobManager.getAllBlobsByBloIds(lBloIds);
		String lXml = getSpring().getEmptyXml();
		IXMLTag lRootTag = getXmlManager().getXmlTree(null, lXml, AppConstants.other_tag);
		IXMLTag lComponentTag = lRootTag.getChild(AppConstants.componentElement);
		IXMLTag lDbTag = addChildTag(lComponentTag, "db_blobs", "");
		List<String> lBlobFiles = new ArrayList<String>();
		String lBlobPath = getAppManager().getAbsoluteBlobPath();
		String lStreamingPath = getAppManager().getAbsoluteStreamingPath();
		String lStreamingSubPath = lStreamingPath + aStreamingFolderCasId + "\\";
		for (IEBlob lItem : lBlobs) {
			IXMLTag lTag = addChildTag(lDbTag, "item", "");
			addChildTag(lTag, "bloid", "" + aUUIDBlobIds.get("" + lItem.getBloId()));
			addChildTag(lTag, "originalbloid", "" + lItem.getBloId());
			addChildTag(lTag, "contenttype", "" + lItem.getContenttype());
			addChildTag(lTag, "filename", escapeXML(lItem.getFilename()));
			addChildTag(lTag, "format", "" + lItem.getFormat());
			addChildTag(lTag, "name", escapeXML("" + lItem.getName()));
			addChildTag(lTag, "url", escapeXML("" + lItem.getUrl()));
			String lFilename = "";
			if (lItem.getFilename() != null && !lItem.getFilename().equals("")) {
				lFilename = lBlobPath + lItem.getBloId() + "\\" + lItem.getFilename();
				if (new File(lFilename).exists()) {
					lBlobFiles.add(lFilename);
				}
			} else if (aIncludeStreamingFolder && lItem.getUrl() != null && !lItem.getUrl().equals("")) {
				// NOTE blob file may exist in streaming sub folder for case or in streaming
				// folder itself
				lFilename = lStreamingSubPath + lItem.getUrl();
				if (new File(lFilename).exists()) {
					lBlobFiles.add(lFilename);
				} else {
					lFilename = lStreamingPath + lItem.getUrl();
					if (new File(lFilename).exists()) {
						lBlobFiles.add(lFilename);
					}
				}
			}
		}
		String lDbXmlData = getXmlManager().xmlTreeToDoc(lRootTag, "UTF-8");
		String lTempPath = getTempPath();
		saveXMLAsFile(lDbXmlData, lTempPath, "blobs.xml");
		aToZipList.add(lTempPath + "blobs.xml");
		aToZipList.addAll(lBlobFiles);
		aRemoveList.add(lTempPath + "blobs.xml");
	}

	/**
	 * Remove file from disk.
	 *
	 * @param aFileName the file name
	 */
	public void removeFile(String aFileName) {
		if (!aFileName.equals("")) {
			File lFile = new File(aFileName);
			if (new File(lFile.getAbsolutePath()).exists())
				lFile.getAbsoluteFile().delete();
		}
	}

	/**
	 * Remove the created export file.
	 */
	public void removeExportFile() {
		removeFile(exportFileName);
		exportFileName = "";
	}

	/**
	 * Read xml from file into XML tags.
	 *
	 * @param aTempDir  the a temp dir
	 * @param aFileName the a file name
	 *
	 * @return XML tags
	 */
	private List<IXMLTag> readXMLTagsFromFile(String aTempDir, String aFileName, int aTagType, String aTagName) {
		try {
			File lFile = new File((Paths.get(aTempDir, aFileName)).toString());
			if (!lFile.exists())
				return null;
			FileInputStream fis = new FileInputStream(lFile);
			InputSource inputSource = new InputSource(fis);
			List<IXMLTag> lXMLTags = xmlTree.getXMLListIS(null, inputSource, aTagName, aTagType);
			fis.close();
			return lXMLTags;
		} catch (FileNotFoundException e) {
			return null;
		} catch (IOException e) {
			return null;
		}
	}

	/**
	 * Read xml from file into XML tag tree.
	 *
	 * @param aTempDir  the a temp dir
	 * @param aFileName the a file name
	 *
	 * @return XML tag
	 */
	private IXMLTag readXMLFromFile(String aTempDir, String aFileName, int aTagType) {
		// NOTE return 'component' tag
		List<IXMLTag> lXMLTags = readXMLTagsFromFile(aTempDir, aFileName, aTagType, AppConstants.componentElement);
		if (lXMLTags == null || lXMLTags.size() == 0) {
			return null;
		}
		return lXMLTags.get(0);
	}

	/**
	 * Parses html from file and returns map of ids and values.
	 *
	 * @param aTempDir  the a temp dir
	 * @param aFileName the a file name
	 *
	 * @return map of ids and values
	 */
	private Map<String, String> readContentFromHTMLFile(String aTempDir, String aFileName) {

		// TODO get aLocaleStr

		File lFile = new File(aTempDir + aFileName);
		String lLine;
		StringBuffer lHtmlBuffer = new StringBuffer();
		try {
			BufferedReader lIn = new BufferedReader(new InputStreamReader(new FileInputStream(lFile), "UTF8"));
			while ((lLine = lIn.readLine()) != null) {
				lHtmlBuffer.append(lLine);
				lHtmlBuffer.append("\n");
			}
			lIn.close();
		} catch (UnsupportedEncodingException e) {
			return null;
		} catch (FileNotFoundException e) {
			return null;
		} catch (IOException e) {
			return null;
		}

		String lDivStart = "<div id=\"";
		String lIdEnd = "\"";
		String lDivStartEnd = ">";
		String lDivEnd = "</div>";

		int lDivStartLength = lDivStart.length();
		int lIdEndLength = lIdEnd.length();
		int lDivStartEndLength = lDivStartEnd.length();
		int lDivEndLength = lDivEnd.length();

		int lFromIndex = 0;
		int lDivStartIndex = lHtmlBuffer.indexOf(lDivStart, lFromIndex);
		int lIdEndIndex = -1;
		int lDivStartEndIndex = -1;
		int lDivEndIndex = -1;

		Map<String, String> lIdsAndValues = new HashMap<String, String>();
		String lId = "";
		String lValue = "";
		while (lDivStartIndex >= 0) {
			lFromIndex = lDivStartIndex + lDivStartLength;
			lIdEndIndex = lHtmlBuffer.indexOf(lIdEnd, lFromIndex);
			if (lIdEndIndex >= 0) {
				lId = lHtmlBuffer.substring(lFromIndex, lIdEndIndex);
				lFromIndex = lIdEndIndex + lIdEndLength;
				lDivStartEndIndex = lHtmlBuffer.indexOf(lDivStartEnd, lFromIndex);
				if (lDivStartEndIndex >= 0) {
					lFromIndex = lDivStartEndIndex + lDivStartEndLength;
					lDivEndIndex = lHtmlBuffer.indexOf(lDivEnd, lFromIndex);
					if (lDivEndIndex >= 0) {
						// NOTE escape for XML because values are stored in XML within EMERGO
						lValue = escapeXML(lHtmlBuffer.substring(lFromIndex, lDivEndIndex));
						// NOTE formatting in text editor might add tabs so remove them
						lValue = lValue.replace("\t", "");
						// NOTE formatting in text editor might also add new line before and after value
						// so remove them
						while (lValue.startsWith("\n")) {
							lValue = lValue.substring(1);
						}
						while (lValue.endsWith("\n")) {
							lValue = lValue.substring(0, lValue.length() - 1);
						}
						lIdsAndValues.put(lId, lValue);
						lFromIndex = lDivEndIndex + lDivEndLength;
					}
				}
			}
			lDivStartIndex = lHtmlBuffer.indexOf(lDivStart, lFromIndex);
		}
		return lIdsAndValues;
	}

	/**
	 * Read excel file and returns list of ids and values.
	 *
	 * @param aTempDir  the a temp dir
	 * @param aFileName the a file name
	 *
	 * @return list of ids and values
	 */
	private List<String[]> readContentFromExcelFile(String aTempDir, String aFileName) {
		List<String[]> lIdsAndValues = new ArrayList<String[]>();

		ExcelHelper excelHelper = new ExcelHelper();

		Workbook workbook = excelHelper.createOrLoadWorkbook(false, false, aTempDir, aFileName);
		Sheet sheet = workbook.getSheet(caseComponentsContentExcelSheetName);
		if (sheet == null) {
			return lIdsAndValues;
		}
		workbook.setActiveSheet(workbook.getSheetIndex(caseComponentsContentExcelSheetName));

		int rowNum = 1;
		int colNum = 0;
		int maxContentCols = caseComponentsContentExcelSheetContentHeaders.length;
		int maxInfoCols = caseComponentsContentExcelSheetInfoHeaders.length;

		// NOTE the a info row in Excel contains the following values:
		// localeStr, value
		String[] lIdsAndValue = new String[maxInfoCols + 1];
		lIdsAndValue[0] = caseComponentsContentExcelSheetLocaleStrId;
		lIdsAndValue[1] = "";
		Row row = sheet.getRow(rowNum);
		if (row != null) {
			lIdsAndValue[1] = excelHelper.getXLSXCellValueAsString(row, 0);
		}
		lIdsAndValues.add(lIdsAndValue);

		rowNum = 4;

		// NOTE a content row in Excel contains the following values:
		// cacId, carId, tagId, childTagName, status, index, html, value
		row = sheet.getRow(rowNum);
		while (row != null) {
			lIdsAndValue = new String[maxContentCols];
			for (int i = colNum; i < (colNum + maxContentCols); i++) {
				lIdsAndValue[i - colNum] = excelHelper.getXLSXCellValueAsString(row, i);
				if (i == (colNum + maxContentCols - 2)) {
					// NOTE this cell contains HTML or is empty
					// NOTE escape it for XML because values are stored in XML within EMERGO
					lIdsAndValue[i - colNum] = escapeXML(lIdsAndValue[i - colNum]);
				} else if (i == (colNum + maxContentCols - 1)) {
					// NOTE this cell contains a value
					// NOTE first escape it to html4 so all special characters will be replaced by
					// HTML interpretable codes
					// NOTE next escape it for XML because values are stored in XML within EMERGO
					lIdsAndValue[i - colNum] = escapeXML(StringEscapeUtils.escapeHtml4(lIdsAndValue[i - colNum]));
					// NOTE replace all &# by &amp;# because & is also stored escaped within EMERGO
					lIdsAndValue[i - colNum] = lIdsAndValue[i - colNum].replaceAll("&#", "&amp;#");
				}
			}
			lIdsAndValues.add(lIdsAndValue);
			rowNum++;
			row = sheet.getRow(rowNum);
		}

		excelHelper.closeWorkbook(workbook);

		return lIdsAndValues;
	}

	/**
	 * Sets the blob ids within aRootTag by replacing world wide unique ids by blob
	 * ids.
	 *
	 * @param aRootTag     the a root tag
	 * @param aUUIDBlobIds the a blob ids, keys contain world wide unique ids,
	 *                     values contain blob ids
	 */
	private void setBlobIds(IXMLTag aRootTag, Hashtable<String, String> aUUIDBlobIds) {
		if (aRootTag == null) {
			return;
		}
		List<IXMLTag> lNodeTags = CDesktopComponents.cScript().getNodeTags(aRootTag);
		// NOTE component and content tag might have blob childs too, so add them
		lNodeTags.add(aRootTag.getChild(AppConstants.componentElement));
		lNodeTags.add(aRootTag.getChild(AppConstants.contentElement));
		for (IXMLTag lTag : lNodeTags) {
			List<IXMLTag> lBlobTags = new ArrayList<IXMLTag>();
			for (String childTagName : AppConstants.nodeChildTagsWithBlob) {
				lBlobTags.addAll(lTag.getChilds(childTagName));
			}
			for (IXMLTag lBlobTag : lBlobTags) {
				String lBlobId = lBlobTag.getValue();
				if (!lBlobId.equals("")) {
					if (aUUIDBlobIds.containsKey(lBlobId))
						lBlobId = aUUIDBlobIds.get(lBlobId);
					else
						lBlobId = "";
				}
				lBlobTag.setValue(lBlobId);
			}
		}
	}

	/**
	 * Replaces all references to world wide unique ids within aRootTag by car ids,
	 * com ids and cac ids.
	 *
	 * @param lRootTag the l root tag
	 * @param aCarIds  the a car ids, keys contain world wide unique ids, values
	 *                 contain car ids
	 * @param aComIds  the a com ids, keys contain world wide unique ids, values
	 *                 contain com ids
	 * @param aCacIds  the a cac ids, keys contain world wide unique ids, values
	 *                 contain cac ids
	 */
	private void replaceUUIDSByIds(IXMLTag lRootTag, Hashtable<String, String> aCarIds,
			Hashtable<String, String> aComIds, Hashtable<String, String> aCacIds) {
		List<IXMLTag> lNodeTags = CDesktopComponents.cScript().getAllTags(lRootTag);
		if (lNodeTags == null)
			return;
		for (IXMLTag lTag : lNodeTags) {
			if (lTag.getName().equals("car") || lTag.getName().equals("refcar") || lTag.getName().equals("carid")) {
				String lValue = "" + aCarIds.get(lTag.getValue());
				if (lValue.equals("null"))
					lValue = "0";
				lTag.setValue(lValue);
			}
			if (lTag.getName().equals("com") || lTag.getName().equals("refcom") || lTag.getName().equals("comid")) {
				String lValue = "" + aComIds.get(lTag.getValue());
				if (lValue.equals("null")) {
					lValue = "" + getComponentManager().getComponent(lTag.getValue()).getComId();
					aComIds.put(lTag.getValue(), lValue);
				}
				lTag.setValue(lValue);
			}
			if (lTag.getName().equals("cac") || lTag.getName().equals("refcac") || lTag.getName().equals("cacid")) {
				String lValue = "" + aCacIds.get(lTag.getValue());
				if (lValue.equals("null"))
					lValue = "0";
				lTag.setValue(lValue);
			}
		}
	}

	/**
	 * Import case. Creates a temporary zip file on the server, which is deleted
	 * after importing. Unzips this file on server. All temporary files are deleted
	 * after importing. Imports and creates case, case roles, case components, case
	 * component roles and blobs out of unzipped xml files and moves unzipped blob
	 * files to correct path on server. World wide unique ids are replaced by new db
	 * ids, such as cas id, car ids, cac ids, ccr ids an blo ids and references to
	 * these world wide unique ids within the xml data of the case components are
	 * replaced by these ids.
	 *
	 * @param aAccount  the account
	 * @param aCaseName the name the case should get
	 * @param aFileName the file name, name of the temporary file to be created on
	 *                  the server
	 * @param aBytes    the bytes array, content of th temporary file to be created
	 *                  on the server
	 *
	 * @return the imported case
	 */
	public IECase importCase(IEAccount aAccount, String aCaseName, String aFileName, byte[] aBytes) {
		IFileManager fileManager = (IFileManager) getBean("fileManager");
		String lTempPath = getTempPath();
		String lFileName = fileManager.createFile(lTempPath, aFileName, aBytes);
		if (lFileName.equals(""))
			return null;

		if (!unpackageZipToFiles(lTempPath, new File(lTempPath + lFileName)))
			return null;

		Hashtable<String, String> lCarIds = new Hashtable<>(0);
		Hashtable<String, IECaseRole> lHCaseRoles = new Hashtable<>(0);
		Hashtable<String, String> lHBloIds = new Hashtable<>(0);
		Hashtable<String, IEComponent> lHComponents = new Hashtable<>(0);
		Hashtable<String, IECaseComponent> lHCaseComponents = new Hashtable<>(0);

		IECase lCase = createCaseFromXmlFile(aCaseName, aAccount);
		if (lCase == null)
			return null;
		createCaseRolesFromXmlFile(lCase, lCarIds, lHCaseRoles);
		createBlobsAndBlobFilesFromXmlFile(lHBloIds);
		createOrUpdateComponentsFromXmlFile(aAccount, lHComponents, false);
		createCaseComponentsFromXmlFile(lCase, aAccount, lCarIds, lHBloIds, lHComponents, lHCaseComponents);
		createCaseComponentRolesFromXmlFile(lHCaseRoles, lHCaseComponents);

		removeFile(lTempPath + lFileName);

		return lCase;
	}

	/**
	 * Import case component. Creates a temporary zip file on the server, which is
	 * deleted after importing. Unzips this file on server. All temporary files are
	 * deleted after importing. Imports and creates case component and blobs out of
	 * unzipped xml files and moves unzipped blob files to correct path on server.
	 * World wide unique ids are replaced by new db ids, such as cac ids and blo ids
	 * and references to these world wide unique ids within the xml data of the case
	 * components are replaced by these ids.
	 *
	 * @param aAccount  the account
	 * @param aCase     the case
	 * @param aFileName the file name, name of the temporary file to be created on
	 *                  the server
	 * @param aBytes    the bytes array, content of th temporary file to be created
	 *                  on the server
	 *
	 * @return the imported case components
	 */
	public List<IECaseComponent> importCaseComponents(IEAccount aAccount, IECase aCase, String aFileName,
			byte[] aBytes) {
		List<IECaseComponent> lCaseComponents = new ArrayList<IECaseComponent>();
		IFileManager fileManager = (IFileManager) getBean("fileManager");
		String lTempPath = getTempPath();
		String lFileName = fileManager.createFile(lTempPath, aFileName, aBytes);
		if (lFileName.equals(""))
			return lCaseComponents;

		if (!unpackageZipToFiles(lTempPath, new File(lTempPath + lFileName)))
			return lCaseComponents;

		Hashtable<String, String> lCarIds = new Hashtable<String, String>(0);
		Hashtable<String, String> lHBloIds = new Hashtable<String, String>(0);
		Hashtable<String, IEComponent> lHComponents = new Hashtable<String, IEComponent>(0);
		Hashtable<String, IECaseComponent> lHCaseComponents = new Hashtable<String, IECaseComponent>(0);

		createBlobsAndBlobFilesFromXmlFile(lHBloIds);
		// NOTE component must have admin as owner
		// get all admin accounts
		List<IEAccount> adminAccounts = getAccountManager().getAllAccounts(true, AppConstants.c_role_adm);
		IEAccount lOwner = null;
		if (adminAccounts.size() > 0) {
			// set first admin as owner
			lOwner = adminAccounts.get(0);
		} else {
			// no admin, set aAccount as owner
			lOwner = aAccount;
		}
		createOrUpdateComponentsFromXmlFile(lOwner, lHComponents, false);
		createCaseComponentsFromXmlFile(aCase, aAccount, lCarIds, lHBloIds, lHComponents, lHCaseComponents);

		removeFile(lTempPath + lFileName);

		for (Enumeration<String> lKeys = lHCaseComponents.keys(); lKeys.hasMoreElements();) {
			String lKey = lKeys.nextElement();
			lCaseComponents.add(lHCaseComponents.get(lKey));
		}

		return lCaseComponents;
	}

	/**
	 * Imports case component content. Creates a temporary zip file on the server,
	 * which is deleted after importing. Unzips this file on server. All temporary
	 * files are deleted after importing.
	 *
	 * @param aAccount               the account
	 * @param aCase                  the current case
	 * @param aFileName              the file name, name of the temporary file to be
	 *                               created on the server
	 * @param aBytes                 the bytes array, content of the temporary file
	 *                               to be created on the server
	 * @param aUpdatedCaseComponents the array with updated case components, to be
	 *                               filled within this method
	 * @param aErrors                the a errors
	 * @param aWarnings              the a warnings
	 *
	 * @return if successfull
	 */
	public boolean importCaseComponentContent(IEAccount aAccount, IECase aCase, String aFileName, byte[] aBytes,
			List<IECaseComponent> aUpdatedCaseComponents, List<String[]> aErrors, List<String[]> aWarnings) {
		IFileManager fileManager = (IFileManager) getBean("fileManager");
		String lTempPath = getTempPath();
		String lFileName = fileManager.createFile(lTempPath, aFileName, aBytes);
		if (lFileName.equals("")) {
			getAppManager().addError(aErrors, "content", "import.file_not_found");
			return false;
		}
		String lFileExtension = CDesktopComponents.vView().getFileExtension(lFileName);

		boolean lSuccess = false;
		if (lFileExtension.equals("zip")) {
			if (!unpackageZipToFiles(lTempPath, new File(lTempPath + lFileName))) {
				getAppManager().addError(aErrors, "content", "import.unpacking");
				return false;
			}
		}
		lSuccess = updateCaseComponentContentFromFile(lTempPath, lFileName, lFileExtension, aAccount, aCase,
				aUpdatedCaseComponents, aErrors, aWarnings);
		removeFile(lTempPath + lFileName);
		return lSuccess;
	}

	/**
	 * Import components. Creates a temporary zip file on the server, which is
	 * deleted after importing. Unzips this file on server. All temporary files are
	 * deleted after importing. Imports and creates components out of unzipped xml
	 * files. World wide unique ids are replaced by new db ids, such as com ids.
	 *
	 * @param aAccount  the account
	 * @param aFileName the file name, name of the temporary file to be created on
	 *                  the server
	 * @param aBytes    the bytes array, content of the temporary file to be created
	 *                  on the server
	 *
	 * @return if successfull
	 */
	public boolean importComponents(IEAccount aAccount, String aFileName, byte[] aBytes) {
		IFileManager fileManager = (IFileManager) getBean("fileManager");
		String lTempPath = getTempPath();
		String lFileName = fileManager.createFile(lTempPath, aFileName, aBytes);
		if (lFileName.equals(""))
			return false;

		if (!unpackageZipToFiles(lTempPath, new File(lTempPath + lFileName)))
			return false;

		Hashtable<String, IEComponent> lHComponents = new Hashtable<String, IEComponent>(0);

		createOrUpdateComponentsFromXmlFile(aAccount, lHComponents, true);
		
		removeFile(lTempPath + lFileName);

		if (lHComponents.size() == 0)
			return false;

		return true;
	}

	/**
	 * Import components. Unzips zip file on server. All temporary files are deleted
	 * after importing. Imports and creates components out of unzipped xml files.
	 * World wide unique ids are replaced by new db ids, such as com ids.
	 *
	 * @param aAccount          the account
	 * @param aPathPlusFileName the path plus file name
	 *
	 * @return if successfull
	 */
	public boolean importComponents(IEAccount aAccount, String aPathPlusFileName) {
		String lTempPath = getTempPath();
		if (!unpackageZipToFiles(lTempPath, new File(aPathPlusFileName)))
			return false;
		Hashtable<String, IEComponent> lHComponents = new Hashtable<String, IEComponent>(0);
		createOrUpdateComponentsFromXmlFile(aAccount, lHComponents, true);
		return true;
	}

	/**
	 * Create case from xml file.
	 *
	 * @param aCaseName the name the case should get
	 * @param aAccount  the current account
	 *
	 * @return the generated case
	 */
	public IECase createCaseFromXmlFile(String aCaseName, IEAccount aAccount) {
		// create case
		String lTempPath = getTempPath();
		IXMLTag lComponentTag = readXMLFromFile(lTempPath, "case.xml", AppConstants.other_tag);
		removeFile(lTempPath + "case.xml");
		if (lComponentTag == null)
			return null;
		IXMLTag lDbTag = lComponentTag.getChild("db_case");
		IXMLTag lTag = lDbTag.getChild("item");
//		String lUCasId = lTag.getChild("casid").getValue();
		ICaseManager caseManager = (ICaseManager) getBean("caseManager");
		IECase lCase = caseManager.getNewCase();
		lCase.setCode(replaceAllIsoEntitiesBySpecialCharacters(unescapeXML(lTag.getChildValue("code"))));
		lCase.setName(aCaseName);
		lCase.setStatus(Integer.parseInt(lTag.getChildValue("status")));
		lCase.setVersion(Integer.parseInt(lTag.getChildValue("version")));
		lCase.setActive(lTag.getChildValue("active").equals("true"));
		lCase.setMultilingual(lTag.getChildValue("multilingual").equals("true"));
		if (lTag.getChild("skin") == null) {
			// NOTE old packages don't have an entry for skin
			lCase.setSkin("classic");
		} else {
			lCase.setSkin(lTag.getChildValue("skin"));
		}
		lCase.setEAccount(aAccount);
		lCase.setCreationdate(new Date());
		lCase.setLastupdatedate(new Date());
		caseManager.saveCase(lCase);
		return lCase;
	}

	/**
	 * Create case roles from xml file.
	 *
	 * @param aCase       the case
	 * @param aCarIds     the a car ids, is updated within this method
	 * @param aHCaseRoles the a h case roles, is updated within this method
	 */
	public void createCaseRolesFromXmlFile(IECase aCase, Hashtable<String, String> aCarIds,
			Hashtable<String, IECaseRole> aHCaseRoles) {
		// create caseroles
		String lTempPath = getTempPath();
		IXMLTag lComponentTag = readXMLFromFile(lTempPath, "caseroles.xml", AppConstants.other_tag);
		removeFile(lTempPath + "caseroles.xml");
		IXMLTag lDbTag = lComponentTag.getChild("db_caseroles");
		List<IXMLTag> lItems = lDbTag.getChilds("item");
		ICaseRoleManager caseRoleManager = (ICaseRoleManager) getBean("caseRoleManager");
		for (IXMLTag lXmlTag : lItems) {
			String lUCarId = lXmlTag.getChild("carid").getValue();
			IECaseRole lCaseRole = caseRoleManager.getNewCaseRole();
			lCaseRole.setName(replaceAllIsoEntitiesBySpecialCharacters(unescapeXML(lXmlTag.getChildValue("name"))));
			lCaseRole.setNpc(lXmlTag.getChildValue("npc").equals("true"));
			lCaseRole.setECase(aCase);
			lCaseRole.setCreationdate(new Date());
			lCaseRole.setLastupdatedate(new Date());
			caseRoleManager.saveCaseRole(lCaseRole);
			String lCarId = "" + lCaseRole.getCarId();
			aCarIds.put(lUCarId, lCarId);
			aHCaseRoles.put(lUCarId, lCaseRole);
		}
	}

	/**
	 * Create blobs and blob files from xml file.
	 *
	 * @param aHBloIds the a h blo ids, is updated within this method
	 */
	public void createBlobsAndBlobFilesFromXmlFile(Hashtable<String, String> aHBloIds) {
		// create blobs
		IFileManager fileManager = (IFileManager) getBean("fileManager");
		String lTempPath = getTempPath();
		IXMLTag lComponentTag = readXMLFromFile(lTempPath, "blobs.xml", AppConstants.other_tag);
		removeFile(lTempPath + "blobs.xml");
		IXMLTag lDbTag = lComponentTag.getChild("db_blobs");
		List<IXMLTag> lItems = lDbTag.getChilds("item");
		IBlobManager blobManager = (IBlobManager) getBean("blobManager");
		String lBlobPath = getAppManager().getAbsoluteBlobPath();
		for (IXMLTag lXmlTag : lItems) {
			String lUBloId = lXmlTag.getChildValue("bloid");
			String lOriginalBloId = lXmlTag.getChildValue("originalbloid");
			String lBlobFileName = replaceAllIsoEntitiesBySpecialCharacters(
					unescapeXML(lXmlTag.getChildValue("filename")));
			IEBlob lBlob = blobManager.getNewBlob();
			lBlob.setContenttype(lXmlTag.getChildValue("contenttype"));
			lBlob.setFilename(lBlobFileName);
			lBlob.setFormat(lXmlTag.getChildValue("format"));
			lBlob.setName(replaceAllIsoEntitiesBySpecialCharacters(unescapeXML(lXmlTag.getChildValue("name"))));
			lBlob.setUrl(replaceAllIsoEntitiesBySpecialCharacters(unescapeXML(lXmlTag.getChildValue("url"))));
			lBlob.setCreationdate(new Date());
			lBlob.setLastupdatedate(new Date());
			blobManager.saveBlob(lBlob);
			String lBloId = "" + lBlob.getBloId();
			aHBloIds.put(lUBloId, lBloId);
			if (!StringUtils.isBlank(lBlobFileName)) {
				// copy file from temp dir to blob dir
				String lFromBlobPath = lTempPath + lOriginalBloId + "/";
				String lFromFile = lFromBlobPath + lBlobFileName;
				String lToBlobPath = lBlobPath + lBloId + "/";
				String lToFile = lToBlobPath + lBlobFileName;
				fileManager.createDir(lToBlobPath);
				fileManager.copyFile(lFromFile, lToFile);
				// remove file in temp dir
				removeFile(lFromFile);
				fileManager.deleteDir(lFromBlobPath);
			}
		}
	}

	/**
	 * Create components from xml file if they don't exist yet.
	 *
	 * @param aAccount     the current account
	 * @param aHComponents the a h components, is updated within this method
	 */
	public void createOrUpdateComponentsFromXmlFile(IEAccount aAccount, Hashtable<String, IEComponent> aHComponents,
			boolean aUpdate) {
		// create components if they don't exist or update existing ones
		String lTempPath = getTempPath();
		IComponentManager componentManager = (IComponentManager) getBean("componentManager");
		Hashtable<String, IEComponent> lHNewComponents = new Hashtable<String, IEComponent>(0);
		IXMLTag lComponentTag = readXMLFromFile(lTempPath, "components.xml", AppConstants.definition_tag);
		removeFile((Paths.get(lTempPath, "components.xml")).toString());
		List<IXMLTag> lItems = new ArrayList<IXMLTag>();
		// old IMS packages don't contain components
		if (lComponentTag != null) {
			IXMLTag lDbTag = lComponentTag.getChild("db_components");
			lItems = lDbTag.getChilds("item");
		}
		// create new components or update existing ones
		for (IXMLTag lXmlTag : lItems) {
			String lUComId = lXmlTag.getChildValue("comid");
			IEComponent lComponent = componentManager.getComponent(lUComId);
			boolean lExists = (lComponent != null);
			if (!lExists) {
				// create component
				IEComponent lNewComponent = componentManager.getNewComponent();
				lNewComponent.setUuid(lXmlTag.getChildValue("comid"));
				lNewComponent
						.setCode(replaceAllIsoEntitiesBySpecialCharacters(unescapeXML(lXmlTag.getChildValue("code"))));
				// Owner is set to case developer! So administrator doesn't see this component.
				// He should not be administrator of all imported components.
				// Case developer, who imports case, doesn't see the components either.
				// So there are more or less hidden and can only be seen be a db adminstrator.
				lNewComponent.setEAccount(aAccount);
				// Set comcomid to 0. It will be set in the next loop, when all components are
				// imported.
				lNewComponent.setComComId(0);
				lNewComponent.setType(Integer.parseInt(lXmlTag.getChildValue("type")));
				lNewComponent.setVersion(Integer.parseInt(lXmlTag.getChildValue("version")));
				lNewComponent.setActive(lXmlTag.getChildValue("active").equals("true"));
				lNewComponent.setMultiple(lXmlTag.getChildValue("multiple").equals("true"));
				String lXml = getSpring().getEmptyXml();
				IXMLTag lRootTag = getXmlManager().getXmlTree(null, lXml, AppConstants.other_tag);
				lRootTag.setChildTags(
						lXmlTag.getChild("xmldefinition").getChild(AppConstants.rootElement).getChildTags());
				String lXmlDefinition = getXmlManager().xmlTreeToDoc(lRootTag, "UTF-8");
				if ((lXmlDefinition == null) || (lXmlDefinition.equals("")))
					lXmlDefinition = getSpring().getEmptyXml();
				lXmlDefinition = replaceAllIsoEntitiesBySpecialCharacters(lXmlDefinition);
				lNewComponent.setXmldefinition(lXmlDefinition);
				Date lDate = null;
				String lDateStr = lXmlTag.getChildValue("creationdate");
				if (lDateStr.equals("")) {
					// support older ims packages
					lDate = new Date();
				} else {
					lDate = new Date(Long.parseLong(lDateStr));
				}
				lNewComponent.setCreationdate(lDate);
				lDateStr = lXmlTag.getChildValue("lastupdatedate");
				if (lDateStr.equals("")) {
					// support older ims packages
					lDate = new Date();
				} else {
					lDate = new Date(Long.parseLong(lDateStr));
				}
				lNewComponent.setLastupdatedate(lDate);
				componentManager.saveComponent(lNewComponent);
				lHNewComponents.put(lUComId, lNewComponent);
			} else if (aUpdate) {
				boolean lUpdated = false;
				String lCode = lXmlTag.getChildValue("code");
				if (!lCode.equals(lComponent.getCode())) {
					lComponent.setCode(lCode);
					lUpdated = true;
				}
				int lType = Integer.parseInt(lXmlTag.getChildValue("type"));
				if (lType != lComponent.getType()) {
					lComponent.setType(lType);
					lUpdated = true;
				}
				int lVersion = Integer.parseInt(lXmlTag.getChildValue("version"));
				if (lVersion != lComponent.getVersion()) {
					lComponent.setVersion(lVersion);
					lUpdated = true;
				}
				boolean lActive = lXmlTag.getChildValue("active").equals("true");
				if (lActive != lComponent.getActive()) {
					lComponent.setActive(lActive);
					lUpdated = true;
				}
				boolean lMultiple = lXmlTag.getChildValue("multiple").equals("true");
				if (lMultiple != lComponent.getMultiple()) {
					lComponent.setMultiple(lMultiple);
					lUpdated = true;
				}
				String lXml = getSpring().getEmptyXml();
				IXMLTag lRootTag = getXmlManager().getXmlTree(null, lXml, AppConstants.other_tag);
				lRootTag.setChildTags(
						lXmlTag.getChild("xmldefinition").getChild(AppConstants.rootElement).getChildTags());
				String lXmlDefinition = getXmlManager().xmlTreeToDoc(lRootTag, "UTF-8");
				if ((lXmlDefinition == null) || (lXmlDefinition.equals("")))
					lXmlDefinition = getSpring().getEmptyXml();
				lXmlDefinition = replaceAllIsoEntitiesBySpecialCharacters(lXmlDefinition);
				if (!lXmlDefinition.equals(lComponent.getXmldefinition())) {
					lComponent.setXmldefinition(lXmlDefinition);
					lUpdated = true;
				}
				if (lUpdated) {
					Date lDate = null;
					String lDateStr = lXmlTag.getChildValue("lastupdatedate");
					if (lDateStr.equals("")) {
						// support older ims packages
						lDate = new Date();
					} else {
						lDate = new Date(Long.parseLong(lDateStr));
					}
					lComponent.setLastupdatedate(lDate);
					componentManager.saveComponent(lComponent);
				}
			}
		}
		// set parents of newly created components
		for (IXMLTag lXmlTag : lItems) {
			String lUComId = lXmlTag.getChildValue("comid");
			IEComponent lComponent = componentManager.getComponent(lUComId);
			if (lComponent != null) {
				IEComponent lNewComponent = lHNewComponents.get("" + lComponent.getUuid());
				boolean lNew = (lNewComponent != null);
				if (lNew) {
					IEComponent lParent = componentManager.getComponent(lXmlTag.getChildValue("comcomid"));
					if (lParent != null)
						lComponent.setComComId(lParent.getComId());
					componentManager.saveComponent(lComponent);
				}
				aHComponents.put(lUComId, lComponent);
			}
		}
	}

	/**
	 * Create case components from xml file.
	 *
	 * @param aCase            the case
	 * @param aAccount         the current account
	 * @param aCarIds          the a car ids
	 * @param aHBloIds         the a h blo ids
	 * @param aHComponents     the a h components
	 * @param aHCaseComponents the a h components, is updated within this method
	 */
	public void createCaseComponentsFromXmlFile(IECase aCase, IEAccount aAccount, Hashtable<String, String> aCarIds,
			Hashtable<String, String> aHBloIds, Hashtable<String, IEComponent> aHComponents,
			Hashtable<String, IECaseComponent> aHCaseComponents) {
		// create casecomponents
		String lTempPath = getTempPath();
		IComponentManager componentManager = (IComponentManager) getBean("componentManager");
		IXMLTag lComponentTag = readXMLFromFile(lTempPath, "casecomponents.xml", AppConstants.other_tag);
		removeFile(lTempPath + "casecomponents.xml");
		IXMLTag lDbTag = lComponentTag.getChild("db_casecomponents");
		List<IXMLTag> lItems = lDbTag.getChilds("item");
		Hashtable<String, String> lComIds = new Hashtable<String, String>(0);
		Hashtable<String, String> lCacIds = new Hashtable<String, String>(0);
		ICaseComponentManager caseComponentManager = (ICaseComponentManager) getBean("caseComponentManager");
		List<IECaseComponent> lCaseComponents = new ArrayList<IECaseComponent>();
		for (IXMLTag lXmlTag : lItems) {
			String lUCacId = lXmlTag.getChildValue("cacid");
			String lUComId = lXmlTag.getChildValue("comcomuuid");
			IECaseComponent lCaseComponent = caseComponentManager.getNewCaseComponent();
			lCaseComponent
					.setName(replaceAllIsoEntitiesBySpecialCharacters(unescapeXML(lXmlTag.getChildValue("name"))));
			lCaseComponent.setECase(aCase);
			lCaseComponent.setEAccount(aAccount);
			IEComponent lComponent = aHComponents.get(lUComId);
			if (lComponent == null) {
				// if not found than old IMS package, so assume component is already present
				lComponent = componentManager.getComponent(Integer.parseInt(lXmlTag.getChildValue("comcomid")));
			}
			String lComId = "0";
			if (lComponent != null) {
				lComId = "" + lComponent.getComId();
			}
			lCaseComponent.setEComponent(lComponent);
			String lXml = getSpring().getEmptyXml();
			IXMLTag lRootTag = getXmlManager().getXmlTree(null, lXml, AppConstants.other_tag);
			lRootTag.setChildTags(lXmlTag.getChild("xmldata").getChild(AppConstants.rootElement).getChildTags());
			String lXmlData = getXmlManager().xmlTreeToDoc(lRootTag, "UTF-8");
			if ((lXmlData == null) || (lXmlData.equals("")))
				lXmlData = getSpring().getEmptyXml();
			lXmlData = replaceAllIsoEntitiesBySpecialCharacters(lXmlData);
			lCaseComponent.setXmldata(lXmlData);
			lCaseComponent.setCreationdate(new Date());
			lCaseComponent.setLastupdatedate(new Date());
			caseComponentManager.saveCaseComponent(lCaseComponent);
			String lCacId = "" + lCaseComponent.getCacId();
			lComIds.put(lUComId, lComId);
			lCacIds.put(lUCacId, lCacId);
			lCaseComponents.add(lCaseComponent);
			aHCaseComponents.put(lUCacId, lCaseComponent);
		}
		// replace UUIDS of carids, cacids and bloids by ids in xml data
		for (IECaseComponent lItem : lCaseComponents) {
			String lXmlData = lItem.getXmldata();
			if ((lXmlData == null) || (lXmlData.equals("")))
				lXmlData = getSpring().getEmptyXml();
			IXMLTag lDataRootTag = getXmlManager().getXmlTree(null, lXmlData, AppConstants.other_tag);
			// replace all references to UUIDS to carids, comids or cacids within xml
			replaceUUIDSByIds(lDataRootTag, aCarIds, lComIds, lCacIds);
			// replace all references to UUIDS to bloids within xml
			setBlobIds(lDataRootTag, aHBloIds);
			lXmlData = getXmlManager().xmlTreeToDoc(lDataRootTag, "UTF-8");
			if ((lXmlData == null) || (lXmlData.equals("")))
				lXmlData = getSpring().getEmptyXml();
			lItem.setXmldata(lXmlData);
			caseComponentManager.saveCaseComponent(lItem);
		}
	}

	/**
	 * Create case component roles from xml file.
	 *
	 * @param aHCaseRoles      the a h roles
	 * @param aHCaseComponents the a h components
	 */
	public void createCaseComponentRolesFromXmlFile(Hashtable<String, IECaseRole> aHCaseRoles,
			Hashtable<String, IECaseComponent> aHCaseComponents) {
		// create casecomponentroles
		String lTempPath = getTempPath();
		IXMLTag lComponentTag = readXMLFromFile(lTempPath, "casecomponentroles.xml", AppConstants.other_tag);
		removeFile(lTempPath + "casecomponentroles.xml");
		IXMLTag lDbTag = lComponentTag.getChild("db_casecomponentroles");
		List<IXMLTag> lItems = lDbTag.getChilds("item");
		ICaseComponentRoleManager caseComponentRoleManager = (ICaseComponentRoleManager) getBean(
				"caseComponentRoleManager");
		for (IXMLTag lXmlTag : lItems) {
			String lUCacId = lXmlTag.getChildValue("caccacid");
			String lUCarId = lXmlTag.getChildValue("carcarid");
			IECaseComponentRole lCaseComponentRole = caseComponentRoleManager.getNewCaseComponentRole();
			lCaseComponentRole
					.setName(replaceAllIsoEntitiesBySpecialCharacters(unescapeXML(lXmlTag.getChildValue("name"))));
			lCaseComponentRole.setCacCacId(aHCaseComponents.get(lUCacId).getCacId());
			lCaseComponentRole.setCarCarId(aHCaseRoles.get(lUCarId).getCarId());
			lCaseComponentRole.setCreationdate(new Date());
			lCaseComponentRole.setLastupdatedate(new Date());
			caseComponentRoleManager.saveCaseComponentRole(lCaseComponentRole);
		}
	}

	/**
	 * Update case component content from file.
	 *
	 * @param aPath                  the a path
	 * @param aFileName              the a file name
	 * @param aFileExtension         file extension
	 * @param aAccount               the current account
	 * @param aCase                  the current case
	 * @param aUpdatedCaseComponents the a updated components
	 * @param aErrors                the a errors
	 * @param aWarnings              the a warnings
	 *
	 * @return if successful
	 */
	public boolean updateCaseComponentContentFromFile(String aPath, String aFileName, String aFileExtension,
			IEAccount aAccount, IECase aCase, List<IECaseComponent> aUpdatedCaseComponents, List<String[]> aErrors,
			List<String[]> aWarnings) {
		boolean lAccountIsAdmin = CDesktopComponents.sSpring().hasRole(AppConstants.c_role_adm);

		Hashtable<String, IECaseComponent> lCaseComponentsPerCacId = new Hashtable<String, IECaseComponent>();
		Hashtable<String, IXMLTag> lRootTagsPerCacId = new Hashtable<String, IXMLTag>();
		Hashtable<String, Hashtable<String, IXMLTag>> lNodeTagsPerCacIdAndTagId = new Hashtable<String, Hashtable<String, IXMLTag>>();
		Hashtable<String, IXMLTag> lUpdatedRootTagsPerCacId = new Hashtable<String, IXMLTag>();

		ICaseComponentRoleManager lCaseComponentRoleManager = (ICaseComponentRoleManager) getBean(
				"caseComponentRoleManager");

		List<String[]> lContentIdsAndValuesList = null;

		String lLocaleStr = "";
		if (aFileExtension.equals("zip")) {
			lContentIdsAndValuesList = getContentIdsAndValuesFromHTMLFile(aPath, "casecomponentscontent.html", aErrors,
					aWarnings);
		} else if (aFileExtension.equals("xlsx")) {
			lContentIdsAndValuesList = getContentIdsAndValuesFromExcelFile(aPath, aFileName, aErrors, aWarnings);
			// NOTE first get locale Str, it is within the first entry of
			// lContentIdsAndValuesList
			if (lContentIdsAndValuesList.size() > 0) {
				if (lContentIdsAndValuesList.get(0).length > 0) {
					lLocaleStr = lContentIdsAndValuesList.get(0)[0];
				}
				lContentIdsAndValuesList.remove(0);
			}
		} else {
			return false;
		}
		if (aErrors.size() > 0) {
			return false;
		}

		IECaseComponent lCaseComponent = null;
		boolean lWarningNoAuthorAdded = false;
		for (String[] lContentIdsAndValue : lContentIdsAndValuesList) {
			lCaseComponent = getCaseComponent(aAccount, aCase, lAccountIsAdmin, lCaseComponentsPerCacId,
					lContentIdsAndValue[0], lWarningNoAuthorAdded, aErrors, aWarnings);
			if (aErrors.size() > 0) {
				return false;
			}
			updateCaseComponentContent(aUpdatedCaseComponents, lCaseComponent, lCaseComponentRoleManager, lLocaleStr,
					lContentIdsAndValue[1], lContentIdsAndValue[2], lContentIdsAndValue[3], lContentIdsAndValue[4],
					lContentIdsAndValue[5], lContentIdsAndValue[6], lRootTagsPerCacId, lNodeTagsPerCacIdAndTagId,
					lUpdatedRootTagsPerCacId, aErrors, aWarnings);
		}

		ICaseComponentManager caseComponentManager = (ICaseComponentManager) getBean("caseComponentManager");
		for (IECaseComponent lUpdatedCaseComponent : aUpdatedCaseComponents) {
			String lUpdatedCacId = "" + lUpdatedCaseComponent.getCacId();
			if (lUpdatedRootTagsPerCacId.containsKey(lUpdatedCacId)) {
				String lXmlData = getXmlManager().xmlTreeToDoc(lUpdatedRootTagsPerCacId.get(lUpdatedCacId));
				lUpdatedCaseComponent.setXmldata(lXmlData);
				caseComponentManager.updateCaseComponent(lUpdatedCaseComponent);
			}
		}

		return true;
	}

	/**
	 * Get content ids and values from html file.
	 *
	 * @param aPath     the a path
	 * @param aFileName the a file name
	 * @param aErrors   the a errors
	 * @param aWarnings the a warnings
	 *
	 * @return if successful
	 */
	public List<String[]> getContentIdsAndValuesFromHTMLFile(String aPath, String aFileName, List<String[]> aErrors,
			List<String[]> aWarnings) {

		// TODO get aLocaleStr

		List<String[]> lContentIdsAndValuesList = new ArrayList<String[]>();

		Map<String, String> lIdsAndValues = readContentFromHTMLFile(aPath, aFileName);
		removeFile(aPath + aFileName);
		if (lIdsAndValues == null) {
			getAppManager().addError(aErrors, "content", "import.reading");
			return lContentIdsAndValuesList;
		}

		String lId = "";
		String lValue = "";
		String[] lIds = null;
		String lCacId = "";
		String lCarId = "";
		String lTagId = "";
		String lValueType = "";
		String lChildTagName = "";
		String lAttributeName = "";
		String[] lKeyValue = null;
		String lStatus = "";
		for (Map.Entry<String, String> lIdAndValue : lIdsAndValues.entrySet()) {
			lId = lIdAndValue.getKey();
			lValue = lIdAndValue.getValue();
			lIds = lId.split("_");
			lCacId = "";
			lCarId = "";
			lTagId = "";
			lChildTagName = "";
			lAttributeName = "";
			// derive ids and child tag name
			if (lIds != null && lIds.length == 4) {
				// NOTE example of structure of id
				// emergo_cacid=19814_tagid=2_name or emergo_cacid=23249_carid=2217
				lKeyValue = lIds[1].split("=");
				if (lKeyValue.length == 2) {
					lCacId = lKeyValue[1];
				}
				// lValue is value of child tag of node tag with tagid
				// or value of attribute of node tag
				// or case component name for certain case role
				lKeyValue = lIds[2].split("=");
				if (lKeyValue.length == 2) {
					if (lKeyValue[0].equals("tagid")) {
						lTagId = lKeyValue[1];
					} else if (lKeyValue[0].equals("carid")) {
						lCarId = lKeyValue[1];
					}
				}
				lKeyValue = lIds[3].split("=");
				if (lKeyValue.length == 2) {
					lValueType = lKeyValue[0];
					if (lValueType.equals("child")) {
						lChildTagName = lKeyValue[1];
					} else if (lValueType.equals("attribute")) {
						lAttributeName = lKeyValue[1];
					}
				}
			}
			if (lCacId.length() > 0 && (lCarId.length() > 0 || lTagId.length() > 0)) {
				String[] lContentIdsAndValue = new String[] { lCacId, // 0
						lCarId, // 1
						lTagId, // 2
						lChildTagName, // 3
						lAttributeName, // 4
						lValue, // 5
						lStatus // 6
				};
				lContentIdsAndValuesList.add(lContentIdsAndValue);
			}
		}
		return lContentIdsAndValuesList;
	}

	/**
	 * Get content ids and values from html file.
	 *
	 * @param aPath     the a path
	 * @param aFileName the a file name
	 * @param aErrors   the a errors
	 * @param aWarnings the a warnings
	 *
	 * @return if successful
	 */
	public List<String[]> getContentIdsAndValuesFromExcelFile(String aPath, String aFileName, List<String[]> aErrors,
			List<String[]> aWarnings) {
		List<String[]> lContentIdsAndValuesList = new ArrayList<String[]>();

		List<String[]> lIdsAndValues = readContentFromExcelFile(aPath, aFileName);
		if (lIdsAndValues == null) {
			getAppManager().addError(aErrors, "content", "import.reading");
			return lContentIdsAndValuesList;
		}

		// NOTE first get locale Str, it is within the first entry of lIdsAndValues
		String lLocaleStr = "";
		if (lIdsAndValues.size() > 0 && lIdsAndValues.get(0)[0].equals(caseComponentsContentExcelSheetLocaleStrId)) {
			lLocaleStr = lIdsAndValues.get(0)[1];
			lIdsAndValues.remove(0);
		}
		lContentIdsAndValuesList.add(new String[] { lLocaleStr });

		String lCacId = "";
		String lCarId = "";
		String lTagId = "";
		String lType = "";
		String lKey = "";
		String lStatus = "";
		int lIndex = 0;
		String lHtml = "";
		String lValue = "";
		int lContentIdsAndValuesListIndex = lContentIdsAndValuesList.size() - 1;

		for (String[] lIdsAndValue : lIdsAndValues) {
			// NOTE a String[] within lIdsAndValues containts the following values:
			// cacId, carId, tagId, childTagName, status, index, html, value
			lCacId = lIdsAndValue[0];
			lCarId = lIdsAndValue[1];
			lTagId = lIdsAndValue[2];
			lType = lIdsAndValue[3];
			lKey = lIdsAndValue[4];
			lStatus = lIdsAndValue[5];
			lIndex = Integer.parseInt(lIdsAndValue[6]);
			lHtml = lIdsAndValue[7];
			lValue = lIdsAndValue[8];

			if (lCacId.length() > 0 && (lCarId.length() > 0 || lTagId.length() > 0)) {
				if (lIndex == 1) {
					if (lHtml.length() > 0) {
						lValue = lHtml.replace(
								AppConstants.labelVarPrefix + "%" + lIndex + AppConstants.labelVarPostfix, lValue);
					}
					lContentIdsAndValuesList.add(new String[] { lCacId, // 0
							lCarId, // 1
							lTagId, // 2
							lType.equals("child") ? lKey : "", // 3
							lType.equals("attribute") ? lKey : "", // 4
							lValue, // 5
							lStatus // 6
					});
					lContentIdsAndValuesListIndex++;
				} else {
					// NOTE get previously stored value. It needs to be adjusted by replacing string
					// (AppConstants.labelVarPrefix + "%" + lIndex + AppConstants.labelVarPostfix)
					// by value
					lContentIdsAndValuesList.get(lContentIdsAndValuesListIndex)[5] = lContentIdsAndValuesList
							.get(lContentIdsAndValuesListIndex)[5]
							.replace(AppConstants.labelVarPrefix + "%" + lIndex + AppConstants.labelVarPostfix, lValue);
				}
			}
		}

		return lContentIdsAndValuesList;
	}

	/**
	 * Get case component.
	 *
	 * @param aAccount                the current account
	 * @param aCase                   the current case
	 * @param aAccountIsAdmin         is account admin?
	 * @param aCaseComponentsPerCacId the case components per cac id
	 * @param aCacId                  the cac id
	 * @param aErrors                 the errors
	 * @param aWarnings               the warnings
	 *
	 * @return case component or null if unsuccessful
	 */
	protected IECaseComponent getCaseComponent(IEAccount aAccount, IECase aCase, boolean aAccountIsAdmin,
			Hashtable<String, IECaseComponent> aCaseComponentsPerCacId, String aCacId, boolean aWarningNoAuthorAdded,
			List<String[]> aErrors, List<String[]> aWarnings) {
		if (aCacId.equals("")) {
			return null;
		}
		if (aCaseComponentsPerCacId.containsKey(aCacId)) {
			return aCaseComponentsPerCacId.get(aCacId);
		}
		IECaseComponent lCaseComponent = getSpring().getCaseComponent(aCacId);
		if (lCaseComponent == null) {
			return null;
		}
		// NOTE only author of case component for current case may change case component
		if (lCaseComponent.getEAccount().getAccId() == aAccount.getAccId()
				&& lCaseComponent.getECase().getCasId() == aCase.getCasId()) {
			if (!aCaseComponentsPerCacId.contains(aCacId)) {
				aCaseComponentsPerCacId.put(aCacId, lCaseComponent);
			}
		} else {
			if (lCaseComponent.getECase().getCasId() != aCase.getCasId()) {
				// error
				getAppManager().addError(aErrors, "content", "import.other_case");
				return null;
			} else if (!aAccountIsAdmin && lCaseComponent.getEAccount().getAccId() != aAccount.getAccId()) {
				// warning
				if (!aWarningNoAuthorAdded) {
					// only add warning once
					aWarningNoAuthorAdded = true;
					getAppManager().addWarning(aWarnings, "content", "import.no_author");
				}
			}
		}
		return lCaseComponent;
	}

	/**
	 * Update case component content.
	 * 
	 * @return if successful
	 */
	protected boolean updateCaseComponentContent(List<IECaseComponent> aUpdatedCaseComponents,
			IECaseComponent aCaseComponent, ICaseComponentRoleManager aCaseComponentRoleManager, String aLocaleStr,
			String aCarId, String aTagId, String aChildTagName, String aAttributeName, String aValue, String aStatus,
			Hashtable<String, IXMLTag> aRootTagsPerCacId,
			Hashtable<String, Hashtable<String, IXMLTag>> aNodeTagsPerCacIdAndTagId,
			Hashtable<String, IXMLTag> aUpdatedRootTagsPerCacId, List<String[]> aErrors, List<String[]> aWarnings) {
		if (aCaseComponent == null) {
			return false;
		}

		IXMLTag lRootTag = null;
		String lCacId = "" + aCaseComponent.getCacId();
		if (aRootTagsPerCacId.containsKey(lCacId)) {
			lRootTag = aRootTagsPerCacId.get(lCacId);
		} else {
			lRootTag = getSpring().getXmlDataTree(aCaseComponent);
			if (lRootTag != null && !aRootTagsPerCacId.contains(lCacId)) {
				aRootTagsPerCacId.put(lCacId, lRootTag);
			}
		}

		if (lRootTag == null) {
			return false;
		}

		List<IXMLTag> lNodeTags = null;
		if (!aNodeTagsPerCacIdAndTagId.containsKey(lCacId)) {
			Hashtable<String, IXMLTag> lNodeTagsPerTagId = new Hashtable<String, IXMLTag>();
			lNodeTags = CDesktopComponents.cScript().getNodeTags(lRootTag);
			if (lNodeTags != null) {
				for (IXMLTag lNodeTag : lNodeTags) {
					lNodeTagsPerTagId.put(lNodeTag.getAttribute(AppConstants.keyId), lNodeTag);
				}
			}
			aNodeTagsPerCacIdAndTagId.put(lCacId, lNodeTagsPerTagId);
		}

		if (!aCarId.equals("")) {
			boolean lUpdated = updateCaseComponentName(aCaseComponent, aCaseComponentRoleManager, aCarId, lRootTag,
					aValue, aStatus, aLocaleStr);
			if (lUpdated) {
				if (!aUpdatedCaseComponents.contains(aCaseComponent)) {
					aUpdatedCaseComponents.add(aCaseComponent);
				}
			}
		} else if (!aTagId.equals("") && !aChildTagName.equals("blob")) {
			// NOTE blob values may not be changed
			// check if value of child tag of node tag has to be changed
			IXMLTag lXMLTag = aNodeTagsPerCacIdAndTagId.get(lCacId).get(aTagId);
			if (lXMLTag != null) {
				boolean lUpdated = false;
				if (!aChildTagName.equals("")) {
					lUpdated = updateChildTagContent(aCaseComponent, lXMLTag, aChildTagName, aValue, aStatus,
							aLocaleStr);
				} else if (!aAttributeName.equals("")) {
					lUpdated = updateAttributeValue(aCaseComponent, lXMLTag, aAttributeName, aValue, aStatus,
							aLocaleStr);
				}
				if (lUpdated) {
					if (!aUpdatedCaseComponents.contains(aCaseComponent)) {
						aUpdatedCaseComponents.add(aCaseComponent);
					}
					if (!aUpdatedRootTagsPerCacId.contains(lCacId)) {
						aUpdatedRootTagsPerCacId.put(lCacId, lRootTag);
					}
				}
			}
		}
		return true;
	}

	/**
	 * Handle case component name
	 * 
	 * @return case component name
	 */
	protected String handleCaseComponentName(String aValue) {
		while (aValue.contains("&amp;")) {
			aValue = aValue.replaceAll("&amp;", "&");
		}
		// NOTE replace special characters, for instance &eacute;
		return xmlHelper.replaceSpecialCharacters(aValue);
	}

	/**
	 * Update case component name.
	 * 
	 * @return if updated
	 */
	protected boolean updateCaseComponentName(IECaseComponent aCaseComponent,
			ICaseComponentRoleManager aCaseComponentRoleManager, String aCarId, IXMLTag aRootTag, String aValue,
			String aStatus, String aLocaleStr) {
		boolean lUpdated = false;
		IECaseComponentRole lCaseComponentRole = aCaseComponentRoleManager
				.getCaseComponentRole(aCaseComponent.getCacId(), Integer.parseInt(aCarId));
		if (lCaseComponentRole == null) {
			return lUpdated;
		}

		boolean lUseLocale = !StringUtils.isEmpty(aLocaleStr);
		if (lUseLocale && aStatus.trim().equals(caseComponentsContentExcelSheetStatusDefault)) {
			// NOTE do nothing, imported value is equal to value in default language
			return lUpdated;
		}

		String lOldName = lCaseComponentRole.getName();
		String lNewName = "";
		String lOldValue = "";
		String lLocaleForDebug = "LOCALE=";
		if (lUseLocale) {
			lNewName = lCaseComponentRole.getName();
			String lStartLocale = AppConstants.labelVarPrefix + aLocaleStr + AppConstants.labelVarPostfix;
			String lEndLocale = AppConstants.labelVarPrefix + "/" + aLocaleStr + AppConstants.labelVarPostfix;
			int lStart = lOldName.indexOf(lStartLocale);
			int lEnd = lOldName.indexOf(lEndLocale);
			if (lStart >= 0 && lEnd >= (lStart + lStartLocale.length())) {
				lOldValue = lOldName.substring(lStart + lStartLocale.length(), lEnd);
			}
			if (lOldValue.length() == 0 && !StringUtils.isEmpty(aValue)) {
				// NOTE append at the end
				lNewName = lOldName + lStartLocale + handleCaseComponentName(aValue) + lEndLocale;
			} else {
				if (aStatus.trim().equals(caseComponentsContentExcelSheetStatusDelete)) {
					lNewName = lOldName.replace(lStartLocale + lOldValue + lEndLocale, "");
					lCaseComponentRole.setName(lNewName);
					aCaseComponentRoleManager.updateCaseComponentRole(lCaseComponentRole);
					return true;
				} else {
					if (!aValue.equals(lOldValue)) {
						aValue = handleCaseComponentName(aValue);
						if (!aValue.equals(lOldValue)) {
							lNewName = lOldName.replace(lStartLocale + lOldValue + lEndLocale,
									lStartLocale + aValue + lEndLocale);
						}
					}
				}
			}
			lLocaleForDebug += aLocaleStr + ";";
		} else {
			lNewName = lCaseComponentRole.getName();
			String lStartLocale = AppConstants.labelVarPrefix;
			int lStart = lOldName.indexOf(lStartLocale);
			if (lStart < 0) {
				lOldValue = lOldName;
				if (!aValue.equals(lOldValue)) {
					aValue = handleCaseComponentName(aValue);
					if (!aValue.equals(lOldValue)) {
						lNewName = aValue;
					}
				}
			} else {
				lOldValue = lOldName.substring(0, lStart);
				if (!aValue.equals(lOldValue)) {
					aValue = handleCaseComponentName(aValue);
					if (!aValue.equals(lOldValue)) {
						lNewName = lOldName.replace(lOldValue, aValue);
					}
				}
			}
			lLocaleForDebug += "default;";
		}

		if (!lNewName.equals(lOldName)) {
			lCaseComponentRole.setName(lNewName);
			aCaseComponentRoleManager.updateCaseComponentRole(lCaseComponentRole);
			lUpdated = true;
			if (_log.isDebugEnabled()) {
				_log.debug(lLocaleForDebug + "CACID=" + lCaseComponentRole.getCacCacId() + ";CARID="
						+ lCaseComponentRole.getCarCarId() + " OLD");
				_log.debug(lOldValue);
				_log.debug(lLocaleForDebug + "CACID=" + lCaseComponentRole.getCacCacId() + ";CARID="
						+ lCaseComponentRole.getCarCarId() + " NEW");
				_log.debug(aValue);
			}
		}

		return lUpdated;
	}

	/**
	 * Get child tag.
	 * 
	 * @return child tag
	 */
	private IXMLTag getNewChildTag(IXMLTag aParentTag, String aTagName) {
		if (aParentTag == null || StringUtils.isEmpty(aTagName)) {
			return null;
		}
		IXMLTag lChildTag = getXmlManager().newXMLTag(aTagName, "");
		lChildTag.setParentTag(aParentTag);
		aParentTag.getChildTags().add(lChildTag);
		return lChildTag;
	}

	/**
	 * Update child tag content.
	 * 
	 * @return if updated
	 */
	protected boolean updateChildTagContent(IECaseComponent aCaseComponent, IXMLTag aNodeTag, String aChildTagName,
			String aValue, String aStatus, String aLocaleStr) {
		boolean lUpdated = false;

		boolean lUseLocale = !StringUtils.isEmpty(aLocaleStr);
		if (lUseLocale && aStatus.trim().equals(caseComponentsContentExcelSheetStatusDefault)) {
			// NOTE do nothing, imported value is equal to value in default language
			return lUpdated;
		}

		IXMLTag lChildTag = aNodeTag.getChild(aChildTagName);
		if (lChildTag == null) {
			return false;
		}

		String lLocaleForDebug = "LOCALE=";
		if (lUseLocale) {
			// NOTE update depending on aLocaleStr

			IXMLTag lLocaleChildTag = lChildTag.getChild(aLocaleStr);
			if (lLocaleChildTag == null && !StringUtils.isEmpty(aValue)) {
				lLocaleChildTag = getNewChildTag(lChildTag, aLocaleStr);
				if (lLocaleChildTag == null) {
					return false;
				}
			} else {
				if (aStatus.trim().equals(caseComponentsContentExcelSheetStatusDelete)) {
					lChildTag.getChildTags().remove(lLocaleChildTag);
					lLocaleChildTag.setParentTag(null);
					return true;
				}
			}
			if (lLocaleChildTag == null) {
				return false;
			}
			lChildTag = lLocaleChildTag;
			lLocaleForDebug += aLocaleStr + ";";
		} else {
			lLocaleForDebug += "default;";
		}

		String lOldValue = lChildTag.getValue();
		if (!aValue.equals(lOldValue)) {
			// NOTE & may be escaped to &amp; so replace all occurences
			while (aValue.contains("&amp;")) {
				aValue = aValue.replaceAll("&amp;", "&");
			}
			if (!aValue.equals(lOldValue)) {
				if (aValue.contains("&")) {
					// NOTE replace special characters, for instance &eacute;
					aValue = xmlHelper.replaceSpecialCharacters(aValue);
				}
				if (!aValue.equals(lOldValue)) {
					// NOTE the value is stored escaped for XML within EMERGO
					aValue = escapeXML(aValue);
					if (!aValue.equals(lOldValue)) {
						// NOTE HTML parser in method splitHtmlCodesAndTexts within CXmlHelper (that is
						// used to export case components content),
						// delivers <br /> codes (with space). However rich text editor in EMERGO stores
						// <br/>. So replace.
						aValue = aValue.replace("&lt;br /&gt;", "&lt;br/&gt;");
						if (!aValue.equals(lOldValue)) {
							lChildTag.setValue(aValue);
							lUpdated = true;
							if (_log.isDebugEnabled()) {
								_log.debug(lLocaleForDebug + "CACID=" + aCaseComponent.getCacId() + ";TAGID="
										+ aNodeTag.getAttribute(AppConstants.keyId) + ";CHILDNAME="
										+ lChildTag.getName() + " OLD");
								_log.debug(lOldValue);
								_log.debug(lLocaleForDebug + "CACID=" + aCaseComponent.getCacId() + ";TAGID="
										+ aNodeTag.getAttribute(AppConstants.keyId) + ";CHILDNAME="
										+ lChildTag.getName() + " NEW");
								_log.debug(aValue);
							}
						}
					}
				}
			}
		}

		return lUpdated;
	}

	/**
	 * Update attribute value.
	 * 
	 * @return if updated
	 */
	protected boolean updateAttributeValue(IECaseComponent aCaseComponent, IXMLTag aNodeTag, String aAttributeName,
			String aValue, String aStatus, String aLocaleStr) {
		boolean lUpdated = false;

		boolean lUseLocale = !StringUtils.isEmpty(aLocaleStr);
		if (lUseLocale && aStatus.trim().equals(caseComponentsContentExcelSheetStatusDefault)) {
			// NOTE do nothing, imported value is equal to value in default language
			return lUpdated;
		}

		IXMLTag lDefStatusChildTag = aNodeTag.getDefTag().getChild(AppConstants.statusElement);
		if (lDefStatusChildTag == null || !lDefStatusChildTag.isAttribute(aAttributeName)) {
			return false;
		}

		IXMLTag lChildTag = aNodeTag.getChild(AppConstants.statusElement);
		if (lChildTag == null) {
			lChildTag = getNewChildTag(aNodeTag, AppConstants.statusElement);
			if (lChildTag == null) {
				return false;
			}
		}

		String lOldValue = "";
		String lLocaleForDebug = "LOCALE=";
		if (lUseLocale) {
			// NOTE update depending on aLocaleStr
			IXMLTag lLocaleChildTag = lChildTag.getChild(aLocaleStr);
			if (lLocaleChildTag == null && !StringUtils.isEmpty(aValue)) {
				lLocaleChildTag = getNewChildTag(lChildTag, aLocaleStr);
				if (lLocaleChildTag == null) {
					return false;
				}
				lOldValue = lChildTag.getDefAttribute(aAttributeName);
			} else {
				if (aStatus.trim().equals(caseComponentsContentExcelSheetStatusDelete)) {
					lLocaleChildTag.getAttributes().remove(aAttributeName);
					if (lLocaleChildTag.getAttributes().size() == 0) {
						lChildTag.getChildTags().remove(lLocaleChildTag);
						lLocaleChildTag.setParentTag(null);
					}
					return true;
				} else {
					lOldValue = lChildTag.getAttribute(aAttributeName);
				}
			}
			lChildTag = lLocaleChildTag;
			lLocaleForDebug += aLocaleStr + ";";
		} else {
			lOldValue = lChildTag.getDefAttribute(aAttributeName);
			lLocaleForDebug += "default;";
		}

		if (!aValue.equals(lOldValue)) {
			lChildTag.setAttribute(aAttributeName, aValue);
			lUpdated = true;
			if (_log.isDebugEnabled()) {
				_log.debug(lLocaleForDebug + "CACID=" + aCaseComponent.getCacId() + ";TAGID="
						+ aNodeTag.getAttribute(AppConstants.keyId) + ";ATTRIBUTENAME=" + aAttributeName + " OLD");
				_log.debug(lOldValue);
				_log.debug(lLocaleForDebug + "CACID=" + aCaseComponent.getCacId() + ";TAGID="
						+ aNodeTag.getAttribute(AppConstants.keyId) + ";ATTRIBUTENAME=" + aAttributeName + " NEW");
				_log.debug(aValue);
			}
		}

		return lUpdated;
	}

	private String replaceAllIsoEntitiesBySpecialCharactersNew(String aStr) {
		int size = aStr.length();
		StringBuffer result = new StringBuffer(size);

		for (int i = 0; i < size; ++i) {
			char c = aStr.charAt(i);

			Boolean lAppendEscape = false;

			if (c == '&') {
				StringBuffer escapedSequence = new StringBuffer();

				++i;
				while (i < size) {
					c = aStr.charAt(i);
					if (c == ';' && !escapedSequence.toString().equals("amp"))
						break;
					escapedSequence.append(c);
					++i;
				}

				if (escapedSequence.length() >= 2 && escapedSequence.charAt(0) == '#') {
					int j = 0;

					try {
						if (escapedSequence.charAt(1) == 'x')

							// create an integer value based on the hexidecimal
							// representation
							j = Integer.parseInt(escapedSequence.substring(2), 16);
						else

							// create an integer value based on the decimal
							// representation
							j = Integer.parseInt(escapedSequence.substring(1));
					} catch (NumberFormatException e) {
					}

					if (j < 0 || j > Character.MAX_VALUE) {
					}
					// create character based on the numeric value
					c = (char) j;
				} else {
					String escapedToken = escapedSequence.toString();
					if (escapedToken.matches(
							"amp;AElig;|amp;Aacute;|amp;Acirc;|amp;Agrave;|amp;Aring;|amp;Atilde;|amp;Auml;|amp;Ccedil;|amp;ETH;"
									+ "|amp;Eacute;|amp;Ecirc;|amp;Egrave;|amp;Euml;|amp;Iacute;|amp;Icirc;|amp;Igrave;|amp;Iuml;|amp;Ntilde;"
									+ "|amp;Oacute;|amp;Ocirc;|amp;Ograve;|amp;Oslash;|amp;Otilde;|amp;Ouml;|amp;THORN;|amp;Uacute;|amp;Ucirc;"
									+ "|amp;Ugrave;|amp;Uuml;|amp;Yacute;|amp;aacute;|amp;acirc;|amp;aelig;|amp;agrave;|amp;aring;|amp;atilde;|amp;auml;"
									+ "|amp;ccedil;|amp;eacute;|amp;ecirc;|amp;egrave;|amp;eth;|amp;euml;|amp;iacute;|amp;icirc;|amp;igrave;|amp;iuml;"
									+ "|amp;ntilde;|amp;oacute;|amp;ocirc;|amp;ograve;|amp;oslash;|amp;otilde;|amp;ouml;|amp;szlig;|amp;thorn;|amp;uacute;"
									+ "|amp;ucirc;|amp;ugrave;|amp;uuml;|amp;yacute;|amp;yuml;|amp;cent;"))
						lAppendEscape = true;

					/*
					 * if (escapedToken.equals("amp;AElig;")) {c = '�';} else if
					 * (escapedToken.equals("amp;Aacute;")) {c = '�';} else if
					 * (escapedToken.equals("amp;Acirc;")) {c = '�';} else if
					 * (escapedToken.equals("amp;Agrave;")) {c = '�';} else if
					 * (escapedToken.equals("amp;Aring;")) {c = '�';} else if
					 * (escapedToken.equals("amp;Atilde;")) {c = '�';} else if
					 * (escapedToken.equals("amp;Auml;")) {c = '�';} else if
					 * (escapedToken.equals("amp;Ccedil;")) {c = '�';} else if
					 * (escapedToken.equals("amp;ETH;")) {c = '�';} else if
					 * (escapedToken.equals("amp;Eacute;")) {c = '�';} else if
					 * (escapedToken.equals("amp;Ecirc;")) {c = '�';} else if
					 * (escapedToken.equals("amp;Egrave;")) {c = '�';} else if
					 * (escapedToken.equals("amp;Euml;")) {c = '�';} else if
					 * (escapedToken.equals("amp;Iacute;")) {c = '�';} else if
					 * (escapedToken.equals("amp;Icirc;")) {c = '�';} else if
					 * (escapedToken.equals("amp;Igrave;")) {c = '�';} else if
					 * (escapedToken.equals("amp;Iuml;")) {c = '�';} else if
					 * (escapedToken.equals("amp;Ntilde;")) {c = '�';} else if
					 * (escapedToken.equals("amp;Oacute;")) {c = '�';} else if
					 * (escapedToken.equals("amp;Ocirc;")) {c = '�';} else if
					 * (escapedToken.equals("amp;Ograve;")) {c = '�';} else if
					 * (escapedToken.equals("amp;Oslash;")) {c = '�';} else if
					 * (escapedToken.equals("amp;Otilde;")) {c = '�';} else if
					 * (escapedToken.equals("amp;Ouml;")) {c = '�';} else if
					 * (escapedToken.equals("amp;THORN;")) {c = '�';} else if
					 * (escapedToken.equals("amp;Uacute;")) {c = '�';} else if
					 * (escapedToken.equals("amp;Ucirc;")) {c = '�';} else if
					 * (escapedToken.equals("amp;Ugrave;")) {c = '�';} else if
					 * (escapedToken.equals("amp;Uuml;")) {c = '�';} else if
					 * (escapedToken.equals("amp;Yacute;")) {c = '�';} else if
					 * (escapedToken.equals("amp;aacute;")) {c = '�';} else if
					 * (escapedToken.equals("amp;acirc;")) {c = '�';} else if
					 * (escapedToken.equals("amp;aelig;")) {c = '�';} else if
					 * (escapedToken.equals("amp;agrave;")) {c = '�';} else if
					 * (escapedToken.equals("amp;aring;")) {c = '�';} else if
					 * (escapedToken.equals("amp;atilde;")) {c = '�';} else if
					 * (escapedToken.equals("amp;auml;")) {c = '�';} else if
					 * (escapedToken.equals("amp;ccedil;")) {c = '�';} else if
					 * (escapedToken.equals("amp;eacute;")) {c = '�';} else if
					 * (escapedToken.equals("amp;ecirc;")) {c = '�';} else if
					 * (escapedToken.equals("amp;egrave;")) {c = '�';} else if
					 * (escapedToken.equals("amp;eth;")) {c = '�';} else if
					 * (escapedToken.equals("amp;euml;")) {c = '�';} else if
					 * (escapedToken.equals("amp;iacute;")) {c = '�';} else if
					 * (escapedToken.equals("amp;icirc;")) {c = '�';} else if
					 * (escapedToken.equals("amp;igrave;")) {c = '�';} else if
					 * (escapedToken.equals("amp;iuml;")) {c = '�';} else if
					 * (escapedToken.equals("amp;ntilde;")) {c = '�';} else if
					 * (escapedToken.equals("amp;oacute;")) {c = '�';} else if
					 * (escapedToken.equals("amp;ocirc;")) {c = '�';} else if
					 * (escapedToken.equals("amp;ograve;")) {c = '�';} else if
					 * (escapedToken.equals("amp;oslash;")) {c = '�';} else if
					 * (escapedToken.equals("amp;otilde;")) {c = '�';} else if
					 * (escapedToken.equals("amp;ouml;")) {c = '�';} else if
					 * (escapedToken.equals("amp;szlig;")) {c = '�';} else if
					 * (escapedToken.equals("amp;thorn;")) {c = '�';} else if
					 * (escapedToken.equals("amp;uacute;")) {c = '�';} else if
					 * (escapedToken.equals("amp;ucirc;")) {c = '�';} else if
					 * (escapedToken.equals("amp;ugrave;")) {c = '�';} else if
					 * (escapedToken.equals("amp;uuml;")) {c = '�';} else if
					 * (escapedToken.equals("amp;yacute;")) {c = '�';} else if
					 * (escapedToken.equals("amp;yuml;")) {c = '�';} else if
					 * (escapedToken.equals("amp;cent;")) {c = '�';}
					 */

				}
			}
			// character "�" is UTF-8 code "\uFFFD"

			if (lAppendEscape)
				result.append("\uFFFD");
			else
				result.append(c);
		}

		return result.toString();
	}

	/**
	 * Replaces all iso entities by special characters.
	 *
	 * @param aString the a string
	 *
	 * @return the converted string
	 */
	private String replaceAllIsoEntitiesBySpecialCharacters(String aString) {
		aString = aString.replaceAll("&euml;", "�");
		aString = aString.replaceAll("&eacute;", "�");
		aString = aString.replaceAll("&ecirc;", "�");
		aString = aString.replaceAll("&egrave;", "�");
		aString = aString.replaceAll("&iuml;", "�");
		aString = aString.replaceAll("&ouml;", "�");
		aString = aString.replaceAll("&uuml;", "�");
		// NOTE Commented out following lines. Special characters in rich text were not
		// imported properly.
		// And these lines don't seem to have any purpose. Exporting and importing a
		// case without them is ok.
		/*
		 * aString = aString.replaceAll("&amp;euml;", "�"); aString =
		 * aString.replaceAll("&amp;eacute;", "�"); aString =
		 * aString.replaceAll("&amp;ecirc;", "�"); aString =
		 * aString.replaceAll("&amp;egrave;", "�"); aString =
		 * aString.replaceAll("&amp;iuml;", "�"); aString =
		 * aString.replaceAll("&amp;ouml;", "�"); aString =
		 * aString.replaceAll("&amp;uuml;", "�");
		 */
		return aString;
	}

	/**
	 * Package files to zip file.
	 *
	 * @param files        the files
	 * @param baseDir      the base dir
	 * @param blobDir      the blob dir
	 * @param streamingDir the base dir
	 * @param zipFile      the zip file
	 *
	 * @return true, if successful
	 */
	public boolean packageFilesToZip(List<String> files, String baseDir, String blobDir, String streamingDir,
			File zipFile) {
		return new ZipHelper().packageFilesToZip(files, baseDir, blobDir, streamingDir, zipFile);
	}

	/**
	 * Unpackage zip file to files within baseDir.
	 *
	 * @param baseDir the base dir
	 * @param zipFile the zip file
	 *
	 * @return true, if successful
	 */
	public boolean unpackageZipToFiles(String baseDir, File zipFile) {
		return new ZipHelper().unpackageZipToFiles(baseDir, zipFile);
	}

	/**
	 * Get file names within zip file.
	 *
	 * @param zipFile the zip file
	 *
	 * @return file names
	 */
	public List<String> getFileNamesInZip(File zipFile) {
		return new ZipHelper().getFileNamesInZip(zipFile);
	}

}
