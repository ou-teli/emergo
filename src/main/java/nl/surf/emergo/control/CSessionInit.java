/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.util.SessionInit;

import nl.surf.emergo.security.spring.zk.CustomSessionCControl;

/**
 * The Class CSessionInit.
 * A listener which is called by ZK if the session is opened.
 */
public class CSessionInit implements SessionInit {

	/* (non-Javadoc)
	 * @see org.zkoss.zk.ui.util.SessionCleanup#init(org.zkoss.zk.ui.Session, Object)
	 */
	public void init(Session zksession, Object object) throws Exception {
		//CControl is used to get and set app attributes per session.
		CControl cControl = new CustomSessionCControl(zksession);
		//NOTE because session cleanup is not sent 'session' keys in memory are removed on session init of the same or another user
		cControl.removeTimedOutAppAttributesPerSessionIds();
	}
	
}
