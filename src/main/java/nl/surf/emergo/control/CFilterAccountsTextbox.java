/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zul.Include;

import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.def.CDefTextbox;

/**
 * The Class CFilterAccountsTextbox.
 */
public class CFilterAccountsTextbox extends CDefTextbox {

	private static final long serialVersionUID = 6533738842360171880L;

	public CFilterAccountsTextbox() {
		super();
		setAttribute("width", "colWidth2");
	}

	/**
	 * On create fill with filter value.
	 */
	public void onCreate(CreateEvent aEvent) {
		Map<String, String> keysAndValueParts = (Map<String, String>)CDesktopComponents.cControl().getAccSessAttr(AppConstants.account_filter_map);
		if (keysAndValueParts != null && keysAndValueParts.containsKey(getId())) {
			setValue(keysAndValueParts.get(getId()));
		} else {
			keysAndValueParts = (Map<String, String>)CDesktopComponents.cControl().getAccSessAttr(AppConstants.run_filter_map);
			if (keysAndValueParts != null && keysAndValueParts.containsKey(getId())) {
				setValue(keysAndValueParts.get(getId()));
			} else {
				keysAndValueParts = (Map<String, String>)CDesktopComponents.cControl().getAccSessAttr(AppConstants.case_filter_map);
				if (keysAndValueParts != null && keysAndValueParts.containsKey(getId())) {
					setValue(keysAndValueParts.get(getId()));
				}
			}
		}
		boolean lPartOfRunsFilter = "true".equals(getAttribute("runFilter"));
		if (lPartOfRunsFilter) {
			Component lInclude = getParent();
			while (lInclude != null && !(lInclude instanceof Include))
				lInclude = lInclude.getParent();
			if (lInclude != null) {
				boolean lIncludeRunsFilter = "true".equals(lInclude.getAttribute("includeRunFilter"));
				if (lIncludeRunsFilter)
					getParent().setVisible(true);
			}
		}
	}

}
