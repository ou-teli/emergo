/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.zkoss.web.Attributes;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.def.CDefWindow;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IERole;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpring;

public class CEmergoAlternativeLandingpageWnd extends CDefWindow {

	private static final long serialVersionUID = 6077579223606887351L;

	/** The spring. */
	protected SSpring sSpring = new SSpring();

	/** The control */
	protected CControl cControl = new CControl();

	/** The view. */
	protected VView vView = new VView();

	public void onCreate() {
		//NOTE if used on emergo.studienet.ou.nl, an alternative landing page may be used as default instead of e.g. account_roles.zul.
		
		String emergoAlternativeLandingPageState = (String)cControl.getAccSessAttr("emergo-alternative-landing-page-state");
		boolean handleEmergoAlternativeLandingPage = emergoAlternativeLandingPageState != null && emergoAlternativeLandingPageState.equals("login-required");
		if (!handleEmergoAlternativeLandingPage) {
			detach();
			return;
		}
		
		//START copied from CEmergoWnd and adjusted

		// if no login screen then login not in breadcrumb
		cControl.setAccSessAttr("no_login", "true");

		// get Language from IDM
		Locale locale = new Locale("nl");

		locale = (Locale)cControl.getAccSessAttr("idm_locale");
		//Note clear session attribute to reduce the number of session attributes
		cControl.setAccSessAttr("idm_locale", null);

		// set Language
		cControl.setSessAttr(Attributes.PREFERRED_LOCALE, locale);

		// get User id from IDM
		String lUserId = "admin";

		lUserId = (String)cControl.getAccSessAttr("idm_userid");
		//Note clear session attribute to reduce the number of session attributes
		cControl.setAccSessAttr("idm_userid", null);

		// get Account
		IAccountManager lAccountManager = (IAccountManager) sSpring.getBean("accountManager");
		IEAccount lAccount = lAccountManager.getAccount(lUserId);
		if (lAccount == null || !lAccount.getActive() || lAccount.getERoles().size() == 0) {
			cControl.clearSession();
			doOverlapped();
			setPosition("top,center");
			setVisible(true);

			//NOTE set alternative landing page state
			cControl.setAccSessAttr("emergo-alternative-landing-page-state", "no-entrance");
		}
		else {
			//START copied from CHandleLogin and adjusted
			cControl.setAccSessAttr(AppConstants.sessKeyContentFiles, null);
			cControl.setAccSessAttr(AppConstants.sessKeyContentFileBlobIds, null);

			// set session variables to be used on other app pages
			cControl.setAccId(lAccount.getAccId());
			// get roles in correct order, this order is given by getAllRoles
			List<IERole> lERoles = new ArrayList<IERole>(0);
			for (IERole lRole : lAccountManager.getAllRoles()) {
				for (IERole lARole : lAccount.getERoles()) {
					if (lARole.getRolId() == lRole.getRolId())
						lERoles.add(lRole);
				}
			}
			// set for each role if account has this role
			String lRoleCode = "";
			for (IERole lRole : lAccount.getERoles()) {
				lRoleCode = lRole.getCode();
			}
			if (lAccount.getERoles().size() != 1) {
				cControl.setAccSessAttr("multi_role", "true");
			}
			else {
				cControl.setAccSessAttr("role", lRoleCode);
				cControl.setAccSessAttr("multi_role", "false");
			}
			//END copied from CHandleLogin and adjusted

			//NOTE set alternative landing page state
			cControl.setAccSessAttr("emergo-alternative-landing-page-state", "ok");

			//NOTE redirect to the alternative landing page
			vView.redirectToView((String)cControl.getAccSessAttr("emergo-alternative-landing-page-url"));
		}
		//END copied from CEmergoWnd and adjusted
	}
	
}
