/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import org.apache.commons.lang3.StringUtils;

import nl.surf.emergo.view.VView;

/**
 * The Class CLoginLabel.
 * 
 * Used to handle on login breadcrumb.
 */
public class CLoginLabel extends CBreadcrumbNoLoginLabel {

	private static final long serialVersionUID = 609587266514895408L;

	/**
	 * On click clear session.
	 */
	public void onClick() {
    	//other institutions may use an institution prefix as request parameter. It is stored in the session after successful login.
		String requestParams = "";
    	String external_institution_prefix = (String)CDesktopComponents.cControl().getAccSessAttr("login_external_institution_prefix");
    	if (!StringUtils.isEmpty(external_institution_prefix)) {
    		requestParams = "?institution_prefix=" + external_institution_prefix;
    	}
		
		CDesktopComponents.vView().clearSession();
		CDesktopComponents.vView().redirectToView(CDesktopComponents.vView().getAbsoluteUrl(VView.v_login + requestParams));
	}

}
