/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;

import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.util.DesktopInit;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.IRunAccountManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.crm.CCrmRunGroupAccountsHelper;
import nl.surf.emergo.control.run.CRunWnd;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunAccount;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpring;

/**
 * The Class CDesktopInit.
 * A listener which is called by ZK if the desktop, the browser instance, is started.
 * If the desktop is started this class checks against unauthorized access.
 */
public class CDesktopInit implements DesktopInit {

	/** The c control. */
	protected CControl cControl = new CControl();

	/** The s spring. */
	protected SSpring sSpring = new SSpring();

	/** The v view. */
	protected VView vView = new VView();

	/** The run wnd. */
	protected CRunWnd runWnd = null;

	public CDesktopInit () {
		Object object = cControl.getRunSessAttr(CControl.runWnd);
		if (object != null && object instanceof CRunWnd) {
			runWnd = (CRunWnd)object;
		}
	}


	/* (non-Javadoc)
	 * @see org.zkoss.zk.ui.util.DesktopInit#init(org.zkoss.zk.ui.Desktop)
	 */
	public void init(Desktop desktop, Object object) throws Exception {
		boolean lRedirectToLogin = false;
		Path lPath = Paths.get(desktop.getRequestPath());
		String lRootPath = lPath.getName(0).toString();
		String lFileName = lPath.getFileName().toString();
		if (lRootPath.equals(lFileName)) {
			lRootPath = "";
		}
		String lRunStatus = VView.getReqPar("runstatus");
		if (lRunStatus != null && !lRunStatus.equals("")) {
			lRedirectToLogin = !isRunWindowAllowed(lRunStatus, lRootPath, lFileName);
		} else {
			// deal with different file separators for different OS
			lRedirectToLogin = !isDesktopAllowed(lRootPath, lFileName);
		}
		if (lRedirectToLogin) {
			vView.forbidden();
		}
	}

	/**
	 * Checks if it is allowed to start the player window.
	 *
	 * @param aRunStatus the status of the run
	 * @param aRootPath the root path of the starting url
	 * @param aFileName the name of the file in the starting url
	 *
	 * @return true if run is allowed
	 */
	private boolean isRunWindowAllowed(String aRunStatus, String aRootPath, String aFileName) {
		if (!(aFileName.equalsIgnoreCase(VView.v_run) || aFileName.equalsIgnoreCase(VView.v_rununity)))
			return false;
		String lSkinPath = Paths.get(VView.getInitParameter("emergo.skins.path")).getName(0).toString();
		if (!aRootPath.equalsIgnoreCase(lSkinPath))
			return false;
		String lRgaId = VView.getReqPar("rgaId");
		String lAccId = VView.getReqPar("accId");
		String lRunId = VView.getReqPar("runId");
		String lCarId = VView.getReqPar("carId");
		IERunGroupAccount lRga = null;
		if (!((lRgaId == null) || (lRgaId.equals("")) || (lRgaId.equals("0"))))
			lRga = sSpring.getRunGroupAccount(Integer.parseInt(lRgaId));
		int lAccInt = 0;
		if ((lAccId != null) && (!lAccId.equals("")) && (!lAccId.equals("0")))
			lAccInt = Integer.parseInt(lAccId);
		int lCarInt = 0;
		if ((lCarId != null) && (!lCarId.equals("")) && (!lCarId.equals("0")))
			lCarInt = Integer.parseInt(lCarId);
		int lRunInt = 0;
		if ((lRunId != null) && (!lRunId.equals("")) && (!lRunId.equals("0")))
			lRunInt = Integer.parseInt(lRunId);
		IERun lRun = null;
		IERunGroup lRug = null;
		if (lRga != null) {
			lRug = lRga.getERunGroup();
			if (lRug == null)
				return false;
			if (lRunInt > 0) {
				if (lRug.getERun().getRunId() != lRunInt)
					return false;
				lRun = sSpring.getRun(lRunInt);
			} else {
				lRun = lRug.getERun();
				lRunInt = lRun.getRunId();
			}
		} else {
			if (lRunInt == 0)  //runId is empty AND rgaId is empty
				return false;
			lRun = sSpring.getRun(lRunInt);
		}
		if (lRun == null)
			return false;
		// readonly test runs may be played by everyone
		if (aRunStatus.equals(AppConstants.runStatusPreviewReadOnly) && (lRun.getStatus() == AppConstants.run_status_test) && (lRga != null))
			return true;
		int lUserInt = 0;
		IEAccount lAcc = CDesktopComponents.sSpring().getAccount();
		if (lAcc != null)
			lUserInt = lAcc.getAccId();
		IAccountManager lAccountManager = (IAccountManager)sSpring.getBean("accountManager");
		String lRole = "";
		String lSessRole = (String)cControl.getAccSessAttr("role");
		if ((lSessRole != null) && !lSessRole.equals(""))
			lRole = lSessRole;
		IRunAccountManager lRunAccountManager = (IRunAccountManager)sSpring.getBean("runAccountManager");
		switch (lRole) {
		case AppConstants.c_role_stu:
			if (lUserInt == 0)
				return false;
			if (!lAcc.getActive())
				return false;
			if (!aRunStatus.equals(AppConstants.runStatusRun))
				return false;
			if (!isRunning(lRun))
				return false;
			if (lRga == null) {
				// open run; create new rungroup and rungroupaccount if not already present
				// (could have been created in earlier run in same session)
				if (lAccInt == 0)
					return false;
				if (!lRun.getOpenaccess())
					return false;
				CCrmRunGroupAccountsHelper cHelper = new CCrmRunGroupAccountsHelper();
				lRug = cHelper.addRungroupIfNotExists(lAccInt, lRunInt, lCarInt, true);
				if (lRug == null)
					return false;
				lRga = cHelper.addRungroupaccountIfNotExists(lAccInt, lRug.getRugId());
			} else {
				if (!lRug.getActive())
					return false;
				IERunAccount lRuacc = lRunAccountManager.getRunAccountByRunIdAccId(lRunInt, lRga.getEAccount().getAccId());
				if ((lRuacc == null) || !lRuacc.getStuactive())
					return false;
			}
			if (lUserInt != lRga.getEAccount().getAccId())
				return false;
			if ((lRun.getOpenaccess()) && (!lRga.getEAccount().getOpenaccess()))
				return false;
			return true;
		case AppConstants.c_role_tut:
			if (lUserInt == 0)
				return false;
			if (!lAcc.getActive())
				return false;
			if (!aRunStatus.matches(AppConstants.runStatusPreviewReadOnly + "|" + AppConstants.runStatusTutorRun))
				return false;
			if (!isRunningForTutor(lRun))
				return false;
			if (lRga == null)
				return false;
			// tutor must be active for this run
			IERunAccount lRuacc = lRunAccountManager.getRunAccountByRunIdAccId(lRunInt, lUserInt);
			if ((lRuacc == null) || !lRuacc.getTutactive())
				return false;
			// we already checked if player has rungroup for this run
			return true;
		case AppConstants.c_role_cde:
			if (lUserInt == 0)
				return false;
			if (!lAcc.getActive())
				return false;
			if (!lAccountManager.hasRole(lAcc, lRole))
				return false;
			if (!aRunStatus.matches(AppConstants.runStatusPreview + "|" + AppConstants.runStatusPreviewReadOnly + "|" + AppConstants.runStatusTutorRun))
				return false;
			if (lRun.getStatus() != AppConstants.run_status_test)
				return false;
			if (lRga == null)
				return false;
			if (lUserInt != lRga.getEAccount().getAccId())
				return false;
			return true;
		case AppConstants.c_role_adm:
			if (lUserInt == 0)
				return false;
			if (!lAcc.getActive())
				return false;
			if (!lAccountManager.hasRole(lAcc, lRole))
				return false;
			if (!aRunStatus.equals(AppConstants.runStatusPreviewReadOnly))
				return false;
			if (lRun.getStatus() != AppConstants.run_status_runnable)
				return false;
			if (lRga == null)
				return false;
			return true;
		default:
			return false;
		}
	}

	/**
	 * Checks if aRun is running.
	 *
	 * @param aRun the a run
	 *
	 * @return true, if is running
	 */
	private boolean isRunning(IERun aRun) {
		Date lCurrentDate = new Date();
		if (!aRun.getActive())
			return false;
		if (aRun.getStatus() != AppConstants.run_status_runnable)
			return false;
		Date lStartDate = aRun.getStartdate();
//		Date lEndDate = aRun.getEnddate();
		if ((lStartDate != null) && (lStartDate.after(lCurrentDate)))
			return false;
//		if ((lEndDate != null) && (lEndDate.before(lCurrentDate)))
//			return false;
		return true;
	}

	/**
	 * Checks if aRun is runnable for tutor.
	 * 
	 * @param aRun the a run
	 * 
	 * @return true, if is running
	 */
	private boolean isRunningForTutor(IERun aRun) {
		// tutor can always access runnable run
//		if (!aRun.getActive())
//			return false;
		if (aRun.getStatus() != AppConstants.run_status_runnable)
			return false;
		return true;
	}

	/**
	 * Checks if it is allowed to start new desktop.
	 *
	 * @param aRootPath the root path of the starting url
	 * @param aFileName the name of the file in the starting url
	 *
	 * @return true if run is allowed
	 */
	private boolean isDesktopAllowed(String aRootPath, String aFileName) {
		if (!(aRootPath.equals("")) && (aRootPath.matches(VView.getInitParameter("emergo.specials.paths"))))
			return true; //allowed to start zul file for special cases: logging of experiment runs, testing code, testing webservices  
		if (aFileName.equalsIgnoreCase(VView.v_login))
			return true;
		if (aFileName.equalsIgnoreCase(VView.v_emergo))
			return true;
		if (aFileName.equalsIgnoreCase(VView.v_Forbidden))
			return true;
		IEAccount lAcc = CDesktopComponents.sSpring().getAccount();
		String lRole = "";
		String lSessRole = (String)cControl.getAccSessAttr("role");
		if ((lSessRole != null) && !lSessRole.equals(""))
			lRole = lSessRole;
		String lSkinsPath = Paths.get(VView.getInitParameter("emergo.skins.path")).getName(0).toString();
		if (aRootPath.equalsIgnoreCase(lSkinsPath)) {
			if (aFileName.equalsIgnoreCase(VView.v_run))
				return false; // starting player
			// allow only request parameters in runwindow startup file
			String lRgaId = VView.getReqPar("rgaId");
			if (!((lRgaId == null) || (lRgaId.equals("")) || (lRgaId.equals("0"))))
				return false;
			String lAccId = VView.getReqPar("accId");
			if ((lAccId != null) && (!lAccId.equals("")) && (!lAccId.equals("0")))
				return false;
			String lRunId = VView.getReqPar("runId");
			if ((lRunId != null) && (!lRunId.equals("")) && (!lRunId.equals("0")))
				return false;
			String lCarId = VView.getReqPar("carId");
			if ((lCarId != null) && (!lCarId.equals("")) && (!lCarId.equals("0")))
				return false;
			IERun lRun = null;
			if (runWnd != null)
				lRun = runWnd.sSpring.getRun();
			if (!(lRole.matches(AppConstants.c_role_cde + "|" + AppConstants.c_role_tut))) {
				// run window must exist
				if (lRun == null)
					return false;
				if ((lAcc == null) || (lRole.equals("")) || (lRole.equals(AppConstants.c_role_crm))) {
					if (lRun.getStatus() != AppConstants.run_status_test)
						return false;  // must be logged in as adm, tut or stu to be in real run, crm can only start test run
				}
			} else {
				// cde can preview item and tut can inspect recorded interventions without starting run
				if ((lRun != null) && (lRole.equals(AppConstants.c_role_cde))) {
					// cde can only start test run
					//TODO switching from eg. tutor role, having started student status view (so run exists, status run_status_run), to cde, trying to preview item
					if (lRun.getStatus() != AppConstants.run_status_test) {
						return false;
					}
				}
			}
			String lUrl = VView.getReqPar("url");
			if (!((lUrl == null) || (lUrl.equals("")))) {
				String lSessMediaInd = VView.getReqPar("sessmediaind");
				int lMediaInd = -1;
				if ((lSessMediaInd == null) || (lSessMediaInd.equals("")))
					return false;
				try {
					lMediaInd = Integer.parseInt(lSessMediaInd);
				} catch (NumberFormatException e) {
					return false;
				}
				ArrayList<String> lMediaFiles = (ArrayList<String>)cControl.getRunSessAttr(AppConstants.sessKeyRunMediaRef);
				if ((lMediaFiles == null) || (lMediaInd < 0) || (lMediaInd >= lMediaFiles.size()))
					return false;
				String lTest = lMediaFiles.get(lMediaInd);
				if (!lUrl.equalsIgnoreCase(lTest))
					return false;  // file not allowed
				ArrayList<String> lMediaViewers = (ArrayList<String>)cControl.getRunSessAttr(AppConstants.sessKeyRunMediaViewer);
				lTest = lMediaViewers.get(lMediaInd);
				if (!aFileName.equalsIgnoreCase(lTest))
					return false;  // wrong player
			}
			return true;
		}
		if (aRootPath.equals("") || aRootPath.matches(VView.getInitParameter("emergo.root.subpaths"))) {
			if (lAcc == null)
				return false;  // must be logged in
			if (aFileName.equalsIgnoreCase(VView.v_account_roles))
				return true;  // choose role; allowed
			switch (lRole) {
			case AppConstants.c_role_adm:
				if (aFileName.startsWith(AppConstants.c_role_adm))
					return true;
				return false;
			case AppConstants.c_role_cde:
				if (aFileName.startsWith(AppConstants.c_role_cde))
					return true;
				return false;
			case AppConstants.c_role_crm:
				if (aFileName.startsWith(AppConstants.c_role_crm))
					return true;
				return false;
			case AppConstants.c_role_tut:
				if (aFileName.startsWith(AppConstants.c_role_tut))
					return true;
				return false;
			case AppConstants.c_role_stu:
				if (aFileName.startsWith(AppConstants.c_role_stu))
					return true;
				return false;
			default:
				return false;
			}
		}
		return false;
	}

}
