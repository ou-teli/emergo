/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import org.zkoss.zk.ui.event.Event;

import nl.surf.emergo.control.def.CDefTreecell;

/**
 * The Class CTreecell.
 *
 * Used to create a clickable treecell.
 * Action of treecell can be set to open a new browser instance with a given url.
 * If action is not set or empty ZK calls onClick method.
 */
public class CTreecell extends CDefTreecell {

	private static final long serialVersionUID = 3194799420580955947L;

	/**
	 * Instantiates a new treecell.
	 */
	public CTreecell() {
		super();
	}

	/**
	 * On click, no parent tree a treeitem was clicked.
	 *
	 * @param aEvent the click event
	 */
	public void onClick(Event aEvent) {
		CTree lTree = (CTree) getTree();
		if (lTree != null)
			//perhaps already detached by previous action and rerendering
			lTree.contentItemClicked(aEvent.getTarget());
	}
}
