/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.Executions;

/**
 * The Class CObserverManager.
 * 
 * Is used to implement Observer design pattern.
 * Implements CIObserved interface. Is used by all other implementations of the CIObserved interface.
 * Every implementation has to use one instance of CObserverManager, to implement methods
 * registerObserver, removeObserver and notifyObservers, so code is limited.
 */
public class CObserverManager implements CIObserved {

	/** The object being observed. */
	protected CIObserved observed = null;

	/** Holds all observer ids that should be notified. */
	protected List<String> observerIds = new ArrayList<String>(0);

	/**
	 * Instantiates a new observer manager.
	 * 
	 * @param aObserved the object being observed
	 */
	public CObserverManager(CIObserved aObserved) {
		observed = aObserved;
	}

	/**
	 * Sets the observed object.
	 * 
	 * @param aObserved the observed object
	 */
	public void setObserved(CIObserved aObserved) {
		observed = aObserved;
	}

	/**
	 * Checks if object identified by aObserverId is an observer.
	 * 
	 * @param aObserverId the observer id
	 * 
	 * @return true, if is observer
	 */
	private boolean isObserver(String aObserverId) {
		int i = observerIds.indexOf(aObserverId);
		return (i >= 0);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.def.CIObserved#registerObserver(java.lang.String)
	 */
	public void registerObserver(String aObserverId) {
		if (!isObserver(aObserverId))
			observerIds.add(aObserverId);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.def.CIObserved#removeObserver(java.lang.String)
	 */
	public void removeObserver(String aObserverId) {
		int i = observerIds.indexOf(aObserverId);
		// remember aObserverId is unique within application page
		if (i >= 0)
			observerIds.remove(i);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.def.CIObserved#notifyObservers(java.lang.String, java.lang.Object)
	 */
	public void notifyObservers(String aAction, Object aStatus) {
		if (observed == null)
			return;
		// get ZK desktop, the current app page
		Desktop lThisDesktop = Executions.getCurrent().getDesktop();
		// loop through observers
		for (int i = 0; i < observerIds.size(); i++) {
			// get observer by observer id
			CIObserver lObserver = (CIObserver) CDesktopComponents.vView().getComponent((String) observerIds.get(i));
			if (lObserver != null) {
				// if desktop of observer is current desktop
				boolean lOk = (((Component)lObserver).getDesktop() == lThisDesktop);
				if (lOk)
					// notify observers on the current desktop
					lObserver.observedNotify(observed, aAction, aStatus);
				else {
					// ZK prevents you from notifying observers on another desktop
					// So store the notifications in a session variable, so the other
					// desktop can get them using a timer.
					// A run session variable is used, because this code is only needed by the app player.
					List<Object> lObservedNotifies = (List)CDesktopComponents.cControl().getRunSessAttr("observedNotifies");
					if (lObservedNotifies == null)
						lObservedNotifies = new ArrayList<Object>(0);
					List<Object> lObservedNotify = new ArrayList<Object>(0);
					lObservedNotify.add(lObserver);
					lObservedNotify.add(observed);
					lObservedNotify.add(aAction);
					lObservedNotify.add(aStatus);
					lObservedNotifies.add(lObservedNotify);
					CDesktopComponents.cControl().setRunSessAttr("observedNotifies", lObservedNotifies);
				}
			}
		}
	}
}
