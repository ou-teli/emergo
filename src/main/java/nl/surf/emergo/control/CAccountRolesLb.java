/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.def.CDefListbox;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IERole;

/**
 * The Class CAccountRolesLb.
 * 
 * Is used to show a list of account roles.
 */
public class CAccountRolesLb extends CDefListbox {

	private static final long serialVersionUID = 5531745748161110606L;

	/**
	 * On create, create list items.
	 */
	public void onCreate(CreateEvent aEvent) {
		IEAccount account = CDesktopComponents.sSpring().getAccount();
		if (account == null) {
			return;
		}
		IAccountManager accountManager = (IAccountManager) CDesktopComponents.sSpring().getBean("accountManager");
		// get roles in correct order, this order is given by getAllRoles
		for (IERole lRole : accountManager.getAllRoles()) {
			for (IERole lARole : account.getERoles()) {
				if (lARole.getRolId() == lRole.getRolId()) {
					Listitem lListitem = new Listitem();
					appendChild(lListitem);
					lListitem.setValue(lRole.getCode());
					Listcell lListcell = new Listcell();
					lListitem.appendChild(lListcell);
					Hbox lHbox = new Hbox();
					lListcell.appendChild(lHbox);
					Div lDiv = new Div();
					lHbox.appendChild(lDiv);
					lDiv.setWidth("490px");
					CDesktopComponents.vView().setHelpTextByLabelKey(lDiv, lRole.getCode() + ".help");
					Label lLabel = new Label();
					lDiv.appendChild(lLabel);
					lLabel.setValue(CDesktopComponents.vView().getLabel(lRole.getCode()));
				}
			}
		}
	}
	
	/**
	 * On select show correct start page per account role.
	 */
	@Override
	public void onSelect() {
		Listitem lListitem = getSelectedItem();
		if (lListitem != null) {
			CControl cControl = CDesktopComponents.cControl();
			// save account role in session variable for use on following pages
			cControl.setAccSessAttr("role",lListitem.getValue());
			// reinitialize session variables that refer to content files that can pass the content files filter servlet
			cControl.setAccSessAttr(AppConstants.sessKeyContentFiles, null);
			cControl.setAccSessAttr(AppConstants.sessKeyContentFileBlobIds, null);
			// redirect to correct landing page per account role
			CAccountsHelper helper = new CAccountsHelper();
			CDesktopComponents.vView().redirectToView(helper.getLandingpage(cControl.getAccId(), (String)lListitem.getValue()));
		}
	}
}
