/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.zkoss.zul.Textbox;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.IAccountRequestManager;
import nl.surf.emergo.business.impl.AccountLockManager;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.utilities.PropsValues;
import nl.surf.emergo.utilities.pwned.PwndHelper;
import nl.surf.emergo.view.VView;

/**
 * The Class CLoginBtn.
 * 
 * Used to handle login on login page.
 */
public class CLoginBtn extends CInputBtn {

	private static final long serialVersionUID = -6889371144066537615L;
	private static final Logger _log = LogManager.getLogger(CLoginBtn.class);
	private PwndHelper pwndHelper = new PwndHelper();

	/**
	 * On click check if account has access to app. If no access, clear session
	 * variables and show no access popup. If access, set session variables to use
	 * within app and redirect to either a page to choose a role (if more than one
	 * role) or to the start page for the role (if one role).
	 */
	public void onClick() {
		IAccountRequestManager accountRequestManager = (IAccountRequestManager) CDesktopComponents.sSpring()
				.getBean("accountRequestManager");

		// determine if user has access to app
		String lUserId = ((Textbox) getFellowIfAny("userid")).getValue();
		
		IEAccount lAccount;
		boolean accountLocked = false;

		// check if account is locked
		ApplicationContext context = new AnnotationConfigApplicationContext(AccountLockManager.class);
		AccountLockManager mgr = context.getBean(AccountLockManager.class);
		if (mgr.isAccountLocked(lUserId)) {
			accountLocked = true;
			lAccount = null;
		} else {
			String lPassword = ((Textbox) getFellowIfAny("password")).getValue();
			lAccount = handlePassword(lUserId, lPassword);

			if (lAccount == null) {
				mgr.addFailedAttempt(lUserId);

				if (mgr.isAccountLocked(lUserId)) {
					accountLocked = true;
				} else {
					// check for a not yet processed account request linked to the entered data
					// if so, add request as a new student account
					Boolean lProcessed = false;
					lAccount = accountRequestManager.ratifyAccountRequest(lUserId, lPassword, lProcessed);
				}
			} else {
				mgr.unlock(lUserId);

				if (pwndHelper.isPwned(lPassword)) {
					_log.warn("User " + lUserId + " has a pwned password");
					showModalPopup(VView.v_s_password_pwned);
				}
			}
		}

		((ConfigurableApplicationContext) context).close();

		CHandleLogin lHandleLogin = new CHandleLogin();
		// NOTE handleLogin clears session if lAccount is null
		lHandleLogin.handleLogin(lAccount, null);

		if (accountLocked) {
			showAccountErrorPopup(VView.v_s_account_locked);
		} else if (lAccount == null) {
			showAccountErrorPopup(VView.v_s_no_valid_account);
		}

	}

	private void showAccountErrorPopup(final String viewId) {
		// NOTE if no valid account session is cleared, including value for no_login
		// which is set in class IDMHeaderTest, so set value for no_login again.
		CDesktopComponents.cControl().setAccSessAttr("no_login", "false");
		showPopup(viewId);
	}

	private IEAccount handlePassword(String lUserId, String lPassword) {
		IAccountManager accountManager = (IAccountManager) CDesktopComponents.sSpring().getBean("accountManager");
		// account must be active
		Boolean lActive = true;
		IEAccount lAccount = null;

		if (PropsValues.PASSWORD_ALLOW_FOR_UNENCODED) {
			// NOTE allow unencoded passwords when logging in. Entered password may be equal
			// to db password.
			lAccount = accountManager.getAccount(lUserId, lPassword, lActive);
		}

		if (lAccount == null && PropsValues.ENCODE_PASSWORD) {
			// if account not found and password might be encoded search for account with
			// encoded password
			lAccount = accountManager.getAccount(lUserId);
			if (lAccount != null && !(lAccount.getActive()
					&& accountManager.matchesEncodedPassword(lPassword, lAccount.getPassword()))) {
				lAccount = null;
			}
		}

		return lAccount;
	}

}
