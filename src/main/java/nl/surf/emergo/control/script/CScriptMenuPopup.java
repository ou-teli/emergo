/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.script;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.OpenEvent;
import org.zkoss.zul.Menu;
import org.zkoss.zul.Menupopup;

import nl.surf.emergo.control.CContentHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefMenu;
import nl.surf.emergo.control.def.CDefMenupopup;

/**
 * The Class CScriptMenuPopup.
 * 
 * Used to show menu popup when building script condition or action.
 */
public class CScriptMenuPopup extends CDefMenupopup {

	private static final long serialVersionUID = 8477129716908603638L;

	/**
	 * On open set correct menu items depending on condition or action and on 
	 * kind of sub element type.
	 * 
	 * @param aEvent the a event
	 */
	public void onOpen(OpenEvent aEvent) {
		if (!aEvent.isOpen())
			// don't react on closing menu
			return;
		// can be used multiple times so clear children
		getChildren().clear();
		// reference is label right clicked on!
		Component lTarget = aEvent.getReference();
		String lScriptTagType = (String) lTarget.getAttribute("scripttagtype");
		String lScriptTagSubtype = (String) lTarget.getAttribute("scripttagsubtype");
		String lType = (String) lTarget.getAttribute("type");
		if (lType.equals("")) {
			return;
		}

		Menupopup lSubMenupopup = new CDefMenupopup();
		if (lScriptTagType.equals("condition")) {
			// if building condition
			if (lScriptTagSubtype.equals("itemfeedback")) {
				// if condition within component 'items', add tag evaluation menu item
				lSubMenupopup.appendChild(new CScriptMi(lTarget, "evalrungrouptagstatus", lScriptTagSubtype, "cde_s_script.evalitemalternativestatus", "cde_s_script.evalitemalternativestatus.help"));
			}
			else {
				// if condition within script component
				// add evaluate tag status menu item
				String[] lTypes = new String[]{"evalrungrouptagstatus", "evalrungroupcomponentstatus", "evalrungrouptagtemplate", "evalrungroupcomponenttemplate"};
				for (int i=0;i<lTypes.length;i++) {
					lSubMenupopup.appendChild(new CScriptMi(lTarget, lTypes[i], lScriptTagSubtype, "cde_s_script." + lTypes[i], "cde_s_script." + lTypes[i] + ".help"));
				}
			}
			// add menu items for logical operators
			String[] lTypes = new String[]{"parenthesisopen", "parenthesisclose", "and", "or", "not"};
			for (int i=0;i<lTypes.length;i++) {
				lSubMenupopup.appendChild(new CScriptMi(lTarget, lTypes[i], lScriptTagSubtype, "cde_s_script." + lTypes[i], "cde_s_script." + lTypes[i] + ".help"));
			}
		}
		else if (lScriptTagType.equals("action") && lType.equals("empty")) {
			// if building action
			// actions are not built using logical operators, so their will be only one action part and the empty script label, total 2 parts
			boolean lAlreadyActionDefined = (lTarget.getParent().getParent().getChildren().size() > 1);
			if (!lAlreadyActionDefined) {
				// no action part defined
				// add set tag status menu item
				String[] lTypes = new String[]{"setrungrouptagstatus", "setrungroupcomponentstatus", "setrungrouptagtemplate", "setrungroupcomponenttemplate"};
				for (int i=0;i<lTypes.length;i++) {
					lSubMenupopup.appendChild(new CScriptMi(lTarget, lTypes[i], lScriptTagSubtype, "cde_s_script." + lTypes[i], "cde_s_script." + lTypes[i] + ".help"));
				}
			}
		}
		
		if (lSubMenupopup.getChildren().size() > 0) {
			Menu lSubMenu = new CDefMenu();
			appendChild(lSubMenu);
			lSubMenu.setLabel(CDesktopComponents.vView().getLabel("new"));
			lSubMenu.appendChild(lSubMenupopup);
		}
		
		if (CContentHelper.isMethod(lType) || CContentHelper.isTemplateMethod(lType)) {
			// only this parts can be edited
			appendChild(new CScriptMi(lTarget, "edit", lScriptTagSubtype, "edit", ""));
		}
		if (!lType.equals("empty")) {
			// empty scriptlabel can not be deleted, because is starting point for build
			appendChild(new CScriptMi(lTarget, "delete", lScriptTagSubtype, "delete", ""));
		}
	}
}
