/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.script;

import nl.surf.emergo.control.CCombo;
import nl.surf.emergo.domain.IECaseComponent;

/**
 * The Class CScriptCacCombo.
 *
 * Used as combobox for case components, used within the CScriptMethodHbox class.
 */
public class CScriptCacCombo extends CCombo {

	private static final long serialVersionUID = -5808057866197766278L;

	/**
	 * Instantiates a new script case component combo.
	 * aComponentCode is used for references to other casecomponents.
	 * It defines the type of components to which can be referenced and
	 * can be empty.
	 * aComponentCodes possibly is not used anymore.
	 *
	 * @param aId the id
	 * @param aComponentCodes the component codes
	 */
	public CScriptCacCombo(String aId, String aComponentCodes) {
		super(aId, "componentcodes", aComponentCodes, "casecomponent.");
	}

	/**
	 * Gets the item id, the db cacid of the case component.
	 *
	 * @param aItem the item
	 *
	 * @return the item id
	 */
	@Override
	protected String getItemId(Object aItem) {
		return "" + ((IECaseComponent)aItem).getCacId();
	}

	/**
	 * Gets the item name, the name of the case component.
	 *
	 * @param aItem the item
	 *
	 * @return the item name
	 */
	@Override
	protected String getItemName(Object aItem) {
		return ((IECaseComponent)aItem).getName();
	}

	/**
	 * Gets the item help.
	 *
	 * @param aItem the item
	 *
	 * @return the item help
	 */
	@Override
	protected String getItemHelp(Object aItem) {
		return "";
	}

}
