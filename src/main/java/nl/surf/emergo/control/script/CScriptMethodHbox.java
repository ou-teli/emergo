/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.script;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.zkoss.zhtml.Hr;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CContentHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.cde.CCdeComponentItemWnd;
import nl.surf.emergo.control.def.CDefHbox;
import nl.surf.emergo.control.def.CDefTextbox;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.domain.IEComponent;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpring;

/**
 * The Class CScriptMethodHbox.
 * 
 * Is used to show a hbox containing input elements to create/edit a script method.
 * A method can be either a part of a script condition or a script action.
 * The input elements are not created within this class, but within CContentHelper.
 * Only the content and visibility of the input elements are set within this class.
 */
public class CScriptMethodHbox extends CDefHbox {

	private static final long serialVersionUID = 5016361402932810132L;

	protected CScript cScript = CDesktopComponents.cScript();

	/** The chosen cac id, initially empty. */
	protected String chosenCacId = "0";

	/** The chosen cac, initially empty. */
	protected IECaseComponent chosenCac = null;

	/** The chosen com id, initially empty. */
	protected String chosenComId = "0";

	/** The chosen com, initially empty. */
	protected IEComponent chosenCom = null;

	/** The chosen tag name, initially empty. */
	protected String chosenTagName = "0";

	/** The chosen status id, initially empty. */
	protected String chosenStatusId = "0";

	/** The chosen function id, initially empty. */
	protected String chosenFunctionId = "0";

	/**
	 * Gets currently selected case component.
	 * 
	 * @return the case component
	 */
	public IECaseComponent getCaseComponent() {
		return chosenCac;
	}

	/**
	 * Gets currently selected component.
	 * 
	 * @return the component
	 */
	public IEComponent getComponent() {
		return chosenCom;
	}

	/**
	 * Gets name of currently selected tag.
	 * 
	 * @return the tag name
	 */
	public String getChosenTagName() {
		return chosenTagName;
	}

	/**
	 * On action, set content and/or visibility of input elements.
	 * 
	 * @param aSender the sender, the input element that is changed, either a combobox or a listbox, both are of type Listbox within ZK
	 * @param aAction the action
	 * @param aActionStatus the action status
	 */
	public void onAction(Listbox aSender, String aAction, Object aActionStatus) {
		String lNodeName = (String)getAttribute("nodename");
		boolean isNewItem = false;
		if (aAction.equals("comboboxSelectItem") || aAction.equals("listboxSelectItem")) {
			lNodeName = (String)getAttribute("nodename");
			isNewItem = lNodeName != null && lNodeName.equals("reftag");
			if (lNodeName == null || lNodeName.equals("")) {
				// node name of edited condition or action part
				CCdeComponentItemWnd lWindow = (CCdeComponentItemWnd) getRoot();
				if (lWindow != null) {
					lNodeName = lWindow.getNodename();
					isNewItem = lWindow.getNewitem();
				}
			}
		}

		if (aAction.equals("comboboxSelectItem") && aActionStatus instanceof String) {
			// a combobox item is selected
			
			String lActionStatus = (String)aActionStatus;
			
			boolean lIsTemplate = CContentHelper.isTemplateMethod(lNodeName);
			boolean lNoTagsUsed = CDesktopComponents.vView().getComponent(lNodeName+"scriptTagName") == null;
			boolean lNoStatusUsed = CDesktopComponents.vView().getComponent(lNodeName+"scriptStatusId") == null;

			String lId = aSender.getId();
			String lRef = (String) getAttribute("ref");
			// is CScriptMethodHbox used in a reference tag, so not in script tag
			boolean lRefIsTrue = ((lRef != null) && (lRef.equals("true")));
			// case component is chosen in corresponding combobox
			boolean lCacChosen = (lId.equals(lNodeName+"scriptCacId"));
			// component id chosen in corresponding combobox
			boolean lComChosen = (lId.equals(lNodeName+"scriptComId"));
			// tag name is chosen in corresponding combobox
			boolean lTagNameChosen = (lId.equals(lNodeName+"scriptTagName"));
			// status is chosen in corresponding combobox
			boolean lStatusIdChosen = (lId.equals(lNodeName+"scriptStatusId"));
			// function is chosen in corresponding combobox
			boolean lFunctionIdChosen = (lId.equals(lNodeName+"scriptFunctionId"));
			String lPreselect = (String) getAttribute("preselectIfOneItem");
			boolean lPreselectIfOneItem = ((lPreselect == null) || (lPreselect.equals("true")));

			if ((lId.equals(lNodeName+"scriptCacId")) ||
				(lId.equals(lNodeName+"scriptComId")) ||	
				(lId.equals(lNodeName+"scriptTagName")) ||
				(lId.equals(lNodeName+"scriptStatusId")) ||
				(lId.equals(lNodeName+"scriptFunctionId"))) {
				// if selection within combobox 
				if (lId.equals(lNodeName+"scriptCacId")) {
					// if case component is chosen
					chosenCacId = lActionStatus;
					chosenCac = CDesktopComponents.sSpring().getCaseComponent(chosenCacId);
					chosenTagName = "0";
					chosenStatusId = "0";
					chosenFunctionId = "0";
				}
				if (lId.equals(lNodeName+"scriptComId")) {
					// if case component is chosen
					chosenComId = lActionStatus;
					chosenCom = CDesktopComponents.sSpring().getComponent(chosenComId);
					chosenTagName = "0";
					chosenStatusId = "0";
					chosenFunctionId = "0";
				}
				if (lId.equals(lNodeName+"scriptTagName")) {
					// if tag name is chosen
					chosenTagName = lActionStatus;
					chosenStatusId = "0";
					chosenFunctionId = "0";
				}
				if (lId.equals(lNodeName+"scriptStatusId")) {
					// if statuskey is chosen
					chosenStatusId = lActionStatus;
					chosenFunctionId = "0";
				}
				if (lId.equals(lNodeName+"scriptFunctionId")) {
					// if functionkey is chosen
					chosenFunctionId = lActionStatus;
				}

				boolean lShow = !(chosenCacId.equals("0") && chosenComId.equals("0"));
				// only update content if casecomponent is chosen
				boolean lUpdate = lCacChosen || lComChosen;
				if (!lIsTemplate) {
					//handle case role list box
					handleCaseRoles(lNodeName, lShow, lUpdate, lRefIsTrue, lPreselectIfOneItem);
				}
				else {
					//handle script component templates text box
					handleComponentTemplates(lNodeName, lShow, lUpdate);
				}
				//handle tag names combo box
				handleTagNames(lNodeName, lShow, lUpdate, isNewItem);

				lShow = lShow && (lNoTagsUsed || !chosenTagName.equals("0"));
				lUpdate = (lCacChosen || lComChosen || lTagNameChosen); 
				if (!lIsTemplate) {
					//either tag list box or tag combo box is used
					//handle tags list box
					handleTags(lNodeName, lShow, lUpdate, lPreselectIfOneItem);
					//handle tag combo box
					handleTag(lNodeName, lShow, lUpdate, isNewItem);
					//handle tag content name combo
					handleTagContentName(lNodeName, lShow, lUpdate, isNewItem);
				}
				//handle script tag templates text box
				handleTagTemplates(lNodeName, lShow, lUpdate);
				//handle script tag status combo box
				handleTagStatus(lNodeName, lShow, lUpdate, isNewItem);

				lShow = ((lShow) && (lNoStatusUsed || !chosenStatusId.equals("0")));
				lUpdate = (lCacChosen || lComChosen || lTagNameChosen || lStatusIdChosen);
				//handle script function combo box
				handleFunction(lNodeName, lShow, lUpdate, isNewItem);

				lUpdate = (lCacChosen || lComChosen || lTagNameChosen || lStatusIdChosen || lFunctionIdChosen); 
				//handle script value text box
				boolean lIsFunctionCount = !chosenFunctionId.equals("") && Integer.parseInt(chosenFunctionId) == AppConstants.functionCountIndex;
				boolean lBooleanValueInput = 
						(cScript.getTagOperatorValueType(chosenCacId, chosenTagName, chosenStatusId, "0").equals("boolean"));
				handleValue(lNodeName, lShow, lUpdate, lIsFunctionCount, lBooleanValueInput);
				//handle script operator combo box
				handleOperator(lNodeName, lShow, lUpdate, isNewItem);
				
				boolean lBooleanOperatorValueInput = 
						(cScript.getTagOperatorValueType(chosenCacId, chosenTagName, chosenStatusId, chosenFunctionId).equals("boolean"));
				//handle script operator value combo box
				handleOperatorValue(lNodeName, lShow, lBooleanOperatorValueInput);
				//handle script operator value text box
				handleOperatorValues(lNodeName, lShow, lUpdate, lBooleanOperatorValueInput, lIsFunctionCount);
			}
		}
		if (aAction.equals("listboxSelectItem") && aActionStatus instanceof List<?>) {
			// a listbox item is selected
			if (CDesktopComponents.sSpring().getRunGroupAccount() != null) {
				//NOTE if current status need te be shown
				boolean lIsMethodUsedInAction = CContentHelper.isMethodUsedInAction(lNodeName) || CContentHelper.isMethodUsedInRunAction(lNodeName);
				//NOTE if used to set value 
				if (lIsMethodUsedInAction) {
					CScriptOperatorValueCombo lScriptOperatorValue = (CScriptOperatorValueCombo) CDesktopComponents.vView().getComponent(lNodeName+"scriptOperatorValue");
					if (lScriptOperatorValue != null && lScriptOperatorValue.getParent().isVisible()) {
						//must exist and parent visible
						CDesktopComponents.vView().setTooltip(lScriptOperatorValue, getRunGroupAccountStatusValues(lNodeName));
					}
					CDefTextbox lScriptOperatorValues = (CDefTextbox) CDesktopComponents.vView().getComponent(lNodeName+"scriptOperatorValues");
					if (lScriptOperatorValues != null && lScriptOperatorValues.getParent().isVisible()) {
						//must exist and parent visible
						CDesktopComponents.vView().setTooltip(lScriptOperatorValues, getRunGroupAccountStatusValues(lNodeName));
					}
				}
			}
		}
	}

	/**
	 * Handle case roles.
	 * 
	 * @param aNodeName the a node name
	 * @param aShow the a show
	 * @param aUpdate the a update
	 * @param aRefIsTrue the a ref is true
	 * @param aPreselectIfOneItem the a preselect if one item
	 */
	protected void handleCaseRoles(String aNodeName, boolean aShow, boolean aUpdate, boolean aRefIsTrue, boolean aPreselectIfOneItem) {
		// show case roles within list box
		CScriptCarListbox lCarListbox = (CScriptCarListbox) CDesktopComponents.vView().getComponent(aNodeName+"scriptCarIds");
		if (lCarListbox != null) {
			if (aUpdate) {
				// only update content  if
				List lCars = null;
				// get current case component edited
				String lCurrentCacId = (String) CDesktopComponents.cControl().getAccSessAttr("cacId");
				if (aRefIsTrue)
					// if reference condition, get caseroles for combo of current cac and chosen cac
					lCars = cScript.getCaseRolesByMasterSlaveCacIds(lCurrentCacId, chosenCacId);
				else {
					// if script condition or action
					if (lCurrentCacId != null)
						// get caseroles for current script casecomponent
						lCars = cScript.getCaseRolesByCacId(lCurrentCacId);
					else {
						// if no current casecomponent,
						// caserole can be set as desktop attribute to filter on
						IECaseRole lCaseRole = (IECaseRole)getDesktop().getAttribute("desktop_caserole");
						// get caseroles for chosen casecomponent
						lCars = cScript.getCaseRolesByCacId(chosenCacId);
						if (lCaseRole != null) {
							// 
							if (lCars == null) {
								// add caserole
								lCars = new ArrayList(0);
								lCars.add(lCaseRole);
							}
							else {
								// remove all caseroles unequal to caserole
								for (int i = (lCars.size() - 1); i >= 0; i--) {
									IECaseRole lItem = (IECaseRole) lCars.get(i);
									if (lItem.getCarId() != lCaseRole.getCarId())
										lCars.remove(i);
								}
							}
						}
					}
				}
				// default show all caseroles selected
				String[] lSelectedIds = null;
				if ((lCars != null) && (lCars.size() > 0)) {
					lSelectedIds = new String[lCars.size()];
					for (int i = 0; i < lCars.size(); i++) {
						IECaseRole lItem = (IECaseRole) lCars.get(i);
						lSelectedIds[i] = "" + lItem.getCarId();
					}
				} else
					lSelectedIds = new String[0];
				lCarListbox.showItems(lCars, lSelectedIds, aPreselectIfOneItem, true);
			}
			// input element is grouped with header, so hide/show parent
			lCarListbox.getParent().setVisible(aShow && lCarListbox.getItems().size() > 0);
		}
	}

	/**
	 * Handle component templates.
	 * 
	 * @param aNodeName the a node name
	 * @param aShow the a show
	 * @param aUpdate the a update
	 */
	protected void handleComponentTemplates(String aNodeName, boolean aShow, boolean aUpdate) {
		// show component templates input area 
		handleTemplates(aNodeName+"scriptComponentTemplates", aShow, aUpdate);
	}

	/**
	 * Handle tag names.
	 * 
	 * @param aNodeName the a node name
	 * @param aShow the a show
	 * @param aUpdate the a update
	 * @param aIsNewItem the a is new item
	 */
	protected void handleTagNames(String aNodeName, boolean aShow, boolean aUpdate, boolean aIsNewItem) {
		// show tagnames of casecomponent within combobox
		CScriptTagNameCombo lTagNameCombo = (CScriptTagNameCombo) CDesktopComponents.vView().getComponent(aNodeName+"scriptTagName");
		if (lTagNameCombo != null) {
			if (aUpdate) {
				List lTagNames = null;
				boolean lCondition = ((String)lTagNameCombo.getAttribute("tagname")).equals("condition");
				boolean lReftag = ((String)lTagNameCombo.getAttribute("tagname")).equals("reftag");

				String lStatusElement = "";
				if (lCondition || lReftag)
					lStatusElement = AppConstants.getstatusElement;
				else
					lStatusElement = AppConstants.setstatusElement;
				if (!chosenCacId.equals("0"))
					lTagNames = cScript.getNodeTagNames(chosenCacId, lStatusElement);
				else if (!chosenComId.equals("0"))
					lTagNames = cScript.getDefNodeTagNames(chosenComId, lStatusElement);
				lTagNameCombo.showItems(lTagNames, chosenTagName, aIsNewItem, true, true);
				if (!aIsNewItem && chosenTagName.equals("0") && (lTagNames.size() == 1))
					// if only 1, tagname automatically selected
					chosenTagName = (String) lTagNames.get(0);
			}
			// input element is grouped with header, so hide/show parent
			lTagNameCombo.getParent().setVisible(aShow && lTagNameCombo.getItems().size() > 0);
		}
	}

	/**
	 * Handle tags.
	 * 
	 * @param aNodeName the a node name
	 * @param aShow the a show
	 * @param aUpdate the a update
	 * @param aPreselectIfOneItem the a preselect if one item
	 */
	protected void handleTags(String aNodeName, boolean aShow, boolean aUpdate, boolean aPreselectIfOneItem) {
		CScriptTagListbox lTagListbox = (CScriptTagListbox) CDesktopComponents.vView().getComponent(aNodeName+"scriptTagIds");
		if (lTagListbox != null) {
			if (aUpdate) {
				String lTagName = chosenTagName;
				if ((lTagName.equals("")) || (lTagName.equals("0"))) {
					// attribute can be set on creation of listbox
					lTagName = (String) lTagListbox.getAttribute("tagname");
				}
				List lTags = cScript.getNodeTags(chosenCacId, lTagName);
				lTagListbox.showItems(lTags, new String[0], aPreselectIfOneItem, false);
			}
			// input element is grouped with header, so hide/show parent
			lTagListbox.getParent().setVisible(aShow && lTagListbox.getItems().size() > 0);
		}
	}

	/**
	 * Handle tag.
	 * 
	 * @param aNodeName the a node name
	 * @param aShow the a show
	 * @param aUpdate the a update
	 * @param aIsNewItem the a is new item
	 */
	protected void handleTag(String aNodeName, boolean aShow, boolean aUpdate, boolean aIsNewItem) {
		CScriptTagCombo lTagCombo = (CScriptTagCombo) CDesktopComponents.vView().getComponent(aNodeName+"scriptTagId");
		if (lTagCombo != null) {
			if (aUpdate) {
				String lTagName = chosenTagName;
				if ((lTagName.equals("")) || (lTagName.equals("0"))) {
					lTagName = (String) lTagCombo.getAttribute("tagname");
				}
				List lTags = null;
				if (lTagName.equals(AppConstants.selectAll)) {
					lTags = cScript.getNodeTags(chosenCacId);
				}
				else {
					lTags = cScript.getNodeTags(chosenCacId, lTagName);
				}
				lTagCombo.showItems(lTags, new String(), aIsNewItem, true, false);
			}
			// input element is grouped with header, so hide/show parent
			lTagCombo.getParent().setVisible(aShow && lTagCombo.getItems().size() > 0);
		}
	}

	/**
	 * Handle tag content name.
	 * 
	 * @param aNodeName the a node name
	 * @param aShow the a show
	 * @param aUpdate the a update
	 * @param aIsNewItem the a is new item
	 */
	protected void handleTagContentName(String aNodeName, boolean aShow, boolean aUpdate, boolean aIsNewItem) {
		CScriptTagContentCombo lTagContentCombo = (CScriptTagContentCombo) CDesktopComponents.vView().getComponent(aNodeName+"scriptTagContentName");
		if (lTagContentCombo != null) {
			if (aUpdate) {
				String lTagName = chosenTagName;
				if (lTagName.equals("") || lTagName.equals("0")) {
					lTagName = (String) lTagContentCombo.getAttribute("tagname");
					if (lTagName == null) {
						lTagName = "";
					}
				}
				List lTagContentNames = cScript.getNodeTagContentNames(chosenCacId, lTagName);
				lTagContentCombo.showItems(lTagContentNames, new String(), aIsNewItem, true, true);
			}
			// input element is grouped with header, so hide/show parent
			lTagContentCombo.getParent().setVisible(aShow && lTagContentCombo.getItems().size() > 0);
		}
	}

	/**
	 * Handle tag templates.
	 * 
	 * @param aNodeName the a node name
	 * @param aShow the a show
	 * @param aUpdate the a update
	 */
	protected void handleTagTemplates(String aNodeName, boolean aShow, boolean aUpdate) {
		// show tag templates input area 
		handleTemplates(aNodeName+"scriptTagTemplates", aShow, aUpdate);
	}

	/**
	 * Handle templates.
	 * 
	 * @param aNodeName the a node name
	 * @param aShow the a show
	 * @param aUpdate the a update
	 */
	protected void handleTemplates(String aId, boolean aShow, boolean aUpdate) {
		// show tag templates input area 
		CDefTextbox lScriptTagTemplates = (CDefTextbox) CDesktopComponents.vView().getComponent(aId);
		if (lScriptTagTemplates != null) {
			if (aUpdate)
				lScriptTagTemplates.setValue("");
			// input element is grouped with header, so hide/show parent
			lScriptTagTemplates.getParent().setVisible(aShow);
		}
	}

	/**
	 * Handle tag status.
	 * 
	 * @param aNodeName the a node name
	 * @param aShow the a show
	 * @param aUpdate the a update
	 * @param aIsNewItem the a is new item
	 */
	protected void handleTagStatus(String aNodeName, boolean aShow, boolean aUpdate, boolean aIsNewItem) {
		CScriptStatusCombo lScriptStatusCombo = (CScriptStatusCombo) CDesktopComponents.vView().getComponent(aNodeName+"scriptStatusId");
		if (lScriptStatusCombo != null) {
			if (aUpdate) {
				List lTagStatusKeyIds = null;
				boolean lCondition = ((String)lScriptStatusCombo.getAttribute("tagname")).equals("condition");

				String lStatusElement = "";
				// if administrator or case run manager, then this box is used to alter all student status
				String lSessRole = (String)CDesktopComponents.cControl().getAccSessAttr("role");
				if (!(lSessRole.equals(AppConstants.c_role_adm) || lSessRole.equals(AppConstants.c_role_crm))) {
					if (lCondition) {
						lStatusElement = AppConstants.getstatusElement;
					}
					else {
						lStatusElement = AppConstants.setstatusElement;
					}
				}
				if (!chosenCacId.equals("0")) {
					lTagStatusKeyIds = cScript.getTagStatusKeyIds(chosenCacId, chosenTagName, lStatusElement);
				}
				else if (!chosenComId.equals("0")) { 
					lTagStatusKeyIds = cScript.getDefTagStatusKeyIds(chosenComId, chosenTagName, lStatusElement);
				}
				lScriptStatusCombo.showItems(lTagStatusKeyIds, new String(), aIsNewItem, true, true);
				if (!aIsNewItem && chosenStatusId.equals("0") && (lTagStatusKeyIds != null) && (lTagStatusKeyIds.size() == 1)) {
					// if only 1, tagname automatically selected
					chosenStatusId = (String) lTagStatusKeyIds.get(0);
				}
			}
			// input element is grouped with header, so hide/show parent
			boolean lShow = aShow && lScriptStatusCombo.getItems().size() > 0;
			lScriptStatusCombo.getParent().setVisible(lShow);
			// show or hide separator line between content input fields and status key input fields
			Hr lHr = (Hr)CDesktopComponents.vView().getComponent("scriptHr");
			if (lHr != null) {
				lHr.setVisible(lShow);
			}
		}
	}

	/**
	 * Handle function.
	 * 
	 * @param aNodeName the a node name
	 * @param aShow the a show
	 * @param aUpdate the a update
	 * @param aIsNewItem the a is new item
	 */
	protected void handleFunction(String aNodeName, boolean aShow, boolean aUpdate, boolean aIsNewItem) {
		CScriptFunctionCombo lScriptFunctionCombo = (CScriptFunctionCombo) CDesktopComponents.vView().getComponent(aNodeName+"scriptFunctionId");
		if (lScriptFunctionCombo != null) {
			List lTagFunctionKeyIds = null;
			boolean lCondition = ((String)lScriptFunctionCombo.getAttribute("condition")).equals("true");
			if (lCondition)
				lTagFunctionKeyIds = cScript.getTagConditionFunctionKeyIds(chosenStatusId);
			else
				lTagFunctionKeyIds = cScript.getTagActionFunctionKeyIds(chosenStatusId);
			// choosing function isn't always possible
			boolean lFunctionsToChose = ((lTagFunctionKeyIds != null) && (lTagFunctionKeyIds.size() > 0));
			if (aUpdate) {
				lScriptFunctionCombo.showItems(lTagFunctionKeyIds, new String(), aIsNewItem, false, true);
			}
			// only show if function can be chosen
			// input element is grouped with header, so hide/show parent
			lScriptFunctionCombo.getParent().setVisible(aShow && lFunctionsToChose && lScriptFunctionCombo.getItems().size() > 0);
		}
	}

	/**
	 * Handle value.
	 * 
	 * @param aNodeName the a node name
	 * @param aShow the a show
	 * @param aUpdate the a update
	 * @param aIsFunctionCount the a is function count
	 * @param aBoolInput the a bool input
	 */
	protected void handleValue(String aNodeName, boolean aShow, boolean aUpdate, boolean aIsFunctionCount, boolean aBoolInput) {
		//NOTE method is used to get status value
		CDefTextbox lScriptValueTextbox = (CDefTextbox) CDesktopComponents.vView().getComponent(aNodeName+"scriptValueTextbox");
		if (lScriptValueTextbox != null) {
			// input element is grouped with header, so hide/show parent
			lScriptValueTextbox.getParent().setVisible(aShow && aIsFunctionCount && !aBoolInput);
		}
		CScriptOperatorValueCombo lScriptValueCombo = (CScriptOperatorValueCombo) CDesktopComponents.vView().getComponent(aNodeName+"scriptValueCombo");
		if (lScriptValueCombo != null) {
			//NOTE To keep supporting old situation where count was a count of value true, set default value to true.
			if (aUpdate) {
				// second element is true
				if (lScriptValueCombo.getChildren().size() > 1) {
					lScriptValueCombo.setSelectedIndex(1);
				}
			}
			// input element is grouped with header, so hide/show parent
			lScriptValueCombo.getParent().setVisible(aShow && aIsFunctionCount && aBoolInput && lScriptValueCombo.getItems().size() > 0);
		}
	}

	/**
	 * Handle operator.
	 * 
	 * @param aNodeName the a node name
	 * @param aShow the a show
	 * @param aUpdate the a update
	 * @param aIsNewItem the a is new item
	 */
	protected void handleOperator(String aNodeName, boolean aShow, boolean aUpdate, boolean aIsNewItem) {
		CScriptOperatorCombo lScriptOperatorCombo = (CScriptOperatorCombo) CDesktopComponents.vView().getComponent(aNodeName+"scriptOperatorId");
		if (lScriptOperatorCombo != null) {
			if (aUpdate) {
				List lTagOperatorKeyIds = cScript.getTagOperatorKeyIds(chosenStatusId,chosenFunctionId);
				lScriptOperatorCombo.showItems(lTagOperatorKeyIds, new String(), aIsNewItem, true, false);
			}
			// input element is grouped with header, so hide/show parent
			lScriptOperatorCombo.getParent().setVisible(aShow && lScriptOperatorCombo.getItems().size() > 0);
		}
	}

	/**
	 * Handle operator value.
	 * 
	 * @param aNodeName the a node name
	 * @param aShow the a show
	 * @param aBoolInput the a bool input
	 */
	protected void handleOperatorValue(String aNodeName, boolean aShow, boolean aBoolInput) {
		//NOTE method is used to set status value
		CScriptOperatorValueCombo lScriptOperatorValue = (CScriptOperatorValueCombo) CDesktopComponents.vView().getComponent(aNodeName+"scriptOperatorValue");
		if (lScriptOperatorValue != null) {
			// input element is grouped with header, so hide/show parent
			boolean lVisible = aShow && aBoolInput && lScriptOperatorValue.getItems().size() > 0;
			lScriptOperatorValue.getParent().setVisible(lVisible);
			//NOTE if used to set value for one rungroupaccount and visible, show current value(s) 
			if (CDesktopComponents.sSpring().getRunGroupAccount() != null && lVisible) {
				boolean lIsMethodUsedInAction = CContentHelper.isMethodUsedInAction(aNodeName) || CContentHelper.isMethodUsedInRunAction(aNodeName);
				//NOTE if used to set value 
				if (lIsMethodUsedInAction) {
					CDesktopComponents.vView().setTooltip(lScriptOperatorValue, getRunGroupAccountStatusValues(aNodeName));
				}
			}
		}
	}

	/**
	 * Handle operator values.
	 * 
	 * @param aNodeName the a node name
	 * @param aShow the a show
	 * @param aUpdate the a update
	 * @param aBoolInput the a bool input
	 * @param aIsFunctionCount the a is function count
	 */
	protected void handleOperatorValues(String aNodeName, boolean aShow, boolean aUpdate, boolean aBoolInput, boolean aIsFunctionCount) {
		//NOTE method is used to set status value
		CDefTextbox lScriptOperatorValues = (CDefTextbox) CDesktopComponents.vView().getComponent(aNodeName+"scriptOperatorValues");
		if (lScriptOperatorValues != null) {
			if (aUpdate)
				lScriptOperatorValues.setValue("");
			// input element is grouped with header, so hide/show parent
			boolean lVisible = aShow && !aBoolInput;
			lScriptOperatorValues.getParent().setVisible(lVisible);
			//NOTE if used to set value for one rungroupaccount and visible, show current value(s) 
			if (CDesktopComponents.sSpring().getRunGroupAccount() != null && lVisible) {
				boolean lIsMethodUsedInAction = CContentHelper.isMethodUsedInAction(aNodeName) || CContentHelper.isMethodUsedInRunAction(aNodeName);
				//NOTE if used to set value 
				if (lIsMethodUsedInAction) {
					CDesktopComponents.vView().setTooltip(lScriptOperatorValues, getRunGroupAccountStatusValues(aNodeName));
				}
			}
			// change label value of parent child
			Label lLabel = null;
			for (Component lComponent : lScriptOperatorValues.getParent().getChildren()) {
				if (lComponent instanceof Label) {
					lLabel = (Label)lComponent;
				}
			}
			if (lLabel != null) {
				if (aIsFunctionCount) {
					lLabel.setValue(CDesktopComponents.vView().getLabel("cde_s_script.operatorvalue.count.header"));
				}
				else {
					lLabel.setValue(CDesktopComponents.vView().getLabel("cde_s_script.give_operatorvalues.header"));
				}
			}
		}
	}

	/**
	 * Gets case component or tag status values as HTML text for current rungroupaccount, for current selection.
	 * 
	 * @param aNodeName the a node name
	 * 
	 * @return the rungroupaccount status values as HTML text
	 */
	protected String getRunGroupAccountStatusValues(String aNodeName) {
		StringBuffer lStatusValues = new StringBuffer();
		if (CDesktopComponents.sSpring().getRunGroupAccount() == null || chosenStatusId.equals("0")) {
			return lStatusValues.toString();
		}
		VView vView = CDesktopComponents.vView();
		SSpring sSpring = CDesktopComponents.sSpring();

		int lKeyIndex = 0;
		try {
			lKeyIndex = Integer.parseInt(chosenStatusId);
		} catch (NumberFormatException e) {
		}
		String lStatusKey = "";
		if (lKeyIndex == 0) {
			return lStatusValues.toString();
		}
		else {
			lStatusKey = sSpring.getAppManager().getStatusKey(lKeyIndex);
		}
		if (lStatusKey.length() == 0) {
			return lStatusValues.toString();
		}

		boolean lIsMethod = CContentHelper.isMethod(aNodeName);
		boolean lAreTagsUsed = vView.getComponent(aNodeName+"scriptTagName") != null;

		//determine involved case components
		List<IECaseComponent> lInvolvedCaseComponents = new ArrayList<IECaseComponent>();
		if (lIsMethod) {
			//normal method, only one case component is chosen
			IECaseComponent lCaseComponent = sSpring.getCaseComponent(chosenCacId);
			if (lCaseComponent != null) {
				lInvolvedCaseComponents.add(lCaseComponent);
			}
		}
		else {
			//template method, multiple case components may be involved
			//get content of corresponding template field
			CDefTextbox lScriptTemplates = (CDefTextbox) vView.getComponent(aNodeName+"scriptComponentTemplates");
			if (lScriptTemplates != null && lScriptTemplates.getValue() != null && lScriptTemplates.getValue().length() > 0) {
				String lTemplate = lScriptTemplates.getValue();
				//get chosen component
				IEComponent lComponent = sSpring.getComponent(chosenComId);
				if (lComponent != null) {
					//get case components for chosen component
					List<IECaseComponent> lCaseComponents = sSpring.getCaseComponents(sSpring.getCase(), lComponent.getCode());
					for (IECaseComponent lCaseComponent : lCaseComponents) {
						if (lCaseComponent.getName().matches(lTemplate)) {
							lInvolvedCaseComponents.add(lCaseComponent);
						}
					}
				}
			}
		}

		//determine involved case tags per case component
		Hashtable<IECaseComponent,List<IXMLTag>> lInvolvedTagsPerCaseComponent = new Hashtable<IECaseComponent,List<IXMLTag>>();
		if (lAreTagsUsed) {
			//for all involved case components
			for (IECaseComponent lCaseComponent : lInvolvedCaseComponents) {
				List<IXMLTag> lTags = cScript.getRunGroupNodeTags(lCaseComponent, chosenTagName);
				if (lTags != null && lTags.size() > 0) {
					List<IXMLTag> lInvolvedTags = new ArrayList<IXMLTag>();
					//multiple tags may be chosen in listbox
					Listbox lListbox = (Listbox) vView.getComponent(aNodeName+"scriptTagIds");
					if (lListbox == null) {
						//one tag may be chosen in combobox
						lListbox = (Listbox) vView.getComponent(aNodeName+"scriptTagId");
					}
					if (lListbox != null) {
						for (Listitem lListitem : lListbox.getSelectedItems()) {
							String lTagId = (String)lListitem.getValue();
							for (IXMLTag lTag : lTags) {
								if (lTag.getAttribute(AppConstants.keyId).equals(lTagId)) {
									lInvolvedTags.add(lTag);
									//only one tag with lTagId so break
									break;
								}
							}
						}
					}
					//tags may be determined by template so get content of corresponding template field
					CDefTextbox lScriptTemplates = (CDefTextbox) vView.getComponent(aNodeName+"scriptTagTemplates");
					if (lScriptTemplates != null && lScriptTemplates.getValue() != null && lScriptTemplates.getValue().length() > 0) {
						String lTemplate = lScriptTemplates.getValue();
						//get correspondig tags for template
						for (IXMLTag lTag : lTags) {
							if (sSpring.getTagName(lTag).matches(lTemplate)) {
								lInvolvedTags.add(lTag);
							}
						}
					}
					if (lInvolvedTags.size() > 0) {
						lInvolvedTagsPerCaseComponent.put(lCaseComponent, lInvolvedTags);
					}
				}
			}
		}

		lStatusValues = getRunGroupAccountStatusValuesAsHTML(lInvolvedCaseComponents, lInvolvedTagsPerCaseComponent, lAreTagsUsed, lStatusKey);
		
		if (lStatusValues.length() > 0) {
			lStatusValues.insert(0, vView.getLabel("adm_s_rungroupaccountstatus.statusvalues") + "<br/>");
		}
		return lStatusValues.toString();
	}

	/**
	 * Gets case component or tag status values as HTML text.
	 * 
	 * @param aCaseComponents the a case components
	 * @param aTagsPerCaseComponent the a tags per case component
	 * @param aAreTagsUsed the are tags used
	 * @param aStatusKey the a status key
	 * 
	 * @return the status values as HTML text
	 */
	protected StringBuffer getRunGroupAccountStatusValuesAsHTML(List<IECaseComponent> aCaseComponents, Hashtable<IECaseComponent,
			List<IXMLTag>> aTagsPerCaseComponent, boolean aAreTagsUsed, String aStatusKey) {
		StringBuffer lStatusValues = new StringBuffer();
		VView vView = CDesktopComponents.vView();
		SSpring sSpring = CDesktopComponents.sSpring();
		boolean lShowTagHeader = false;
		if (aAreTagsUsed) {
			for (IECaseComponent lCaseComponent : aCaseComponents) {
				if (aTagsPerCaseComponent.containsKey(lCaseComponent)) {
					for (IXMLTag lTag : aTagsPerCaseComponent.get(lCaseComponent)) {
						lStatusValues.append("<tr>");
						if (aCaseComponents.size() > 1) {
							lStatusValues.append("<td>" + lCaseComponent.getName() + "</td>");
						}
						if (aCaseComponents.size() > 1 || aTagsPerCaseComponent.get(lCaseComponent).size() > 1) {
							lStatusValues.append("<td>" + sSpring.getTagName(lTag) + "</td>");
							lShowTagHeader = true;
						}
						lStatusValues.append("<td>" + lTag.getCurrentStatusAttribute(aStatusKey) + "</td>");
						lStatusValues.append("</tr>");
					}
				}
			}
		}
		else {
			for (IECaseComponent lCaseComponent : aCaseComponents) {
				IXMLTag lTag = sSpring.getComponentDataStatusTag(lCaseComponent, AppConstants.statusTypeRunGroup);
				if (lTag != null) {
					lStatusValues.append("<tr>");
					if (aCaseComponents.size() > 1) {
						lStatusValues.append("<td>" + lCaseComponent.getName() + "</td>");
					}
					lStatusValues.append("<td>" + lTag.getCurrentStatusAttribute(aStatusKey) + "</td>");
					lStatusValues.append("</tr>");
				}
			}
		}

		if (lStatusValues.length() > 0) {
			String lTablePrefix = "<table style=\"border-spacing:5px;\">";
			lTablePrefix += "<tr>";
			if (aCaseComponents.size() > 1) {
				lTablePrefix += "<td><b>" + vView.getLabel("casecomponent") + "</b></td>";
			}
			if (lShowTagHeader) {
				lTablePrefix += "<td><b>" + vView.getLabel("nodetag") + "</b></td>";
			}
			lTablePrefix += "<td><b>" + vView.getLabel("attributevalue") + "</b></td>";
			lTablePrefix += "</tr>";
			lStatusValues.insert(0, lTablePrefix);
			lStatusValues.append("</table>");
		}

		return lStatusValues;
	}

}
