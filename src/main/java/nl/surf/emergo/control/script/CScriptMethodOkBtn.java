/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.script;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Textbox;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.IXmlManager;
import nl.surf.emergo.control.CContentHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.cde.CCdeScriptMethodWnd;
import nl.surf.emergo.control.def.CDefButton;
import nl.surf.emergo.control.def.CDefCheckbox;
import nl.surf.emergo.control.def.CDefTextbox;

/**
 * The Class CScriptMethodOkBtn.
 * 
 * If script method ok, save (new) values of input elements within CScriptMethodHbox
 * within method, either a condition or action.
 */
public class CScriptMethodOkBtn extends CDefButton {

	private static final long serialVersionUID = -8383064391132683362L;

	public void onCreate() {
		setVisible(((String)CDesktopComponents.cControl().getAccSessAttr("is_author")).equals("true"));
	}

	/**
	 * Sets xml child tags by adding child tags to aParentTag, with name aNodeName
	 * and values given by selected items within listbox aListbox (single or multi
	 * select).
	 * 
	 * @param aParentTag the parent tag
	 * @param aListbox the listbox
	 * @param aNodeName the node name
	 * 
	 * @return the newly added child tags
	 */
	private List<IXMLTag> setXMLChildTags(IXMLTag aParentTag, Listbox aListbox, String aNodeName) {
		List<IXMLTag> lNewTags = new ArrayList<IXMLTag>(0);
		if ((aParentTag == null) || (aListbox == null))
			return lNewTags;
		List<IXMLTag> lTags = aParentTag.getChildTags();
		Set<Listitem> lSelectedItems = aListbox.getSelectedItems();
		IXmlManager	xmlManager = CDesktopComponents.sSpring().getXmlManager();
		for (Listitem lItem : lSelectedItems) {
			String lId = (String) lItem.getValue();
			IXMLTag lTag = xmlManager.newXMLTag(aNodeName, lId);
			lNewTags.add(lTag);
			lTags.add(lTag);
		}
		return lNewTags;
	}

	/**
	 * Sets xml child tags by adding child tags to aParentTag, with name aNodeName
	 * and values given by content of aTextbox, a comma separated list.
	 * 
	 * @param aParentTag the parent tag
	 * @param aTextbox the textbox
	 * @param aNodeName the node name
	 * 
	 * @return the newly added child tags
	 */
	private List<IXMLTag> setXMLChildTags(IXMLTag aParentTag, Textbox aTextbox, String aNodeName, boolean aAllowEmpty) {
		List<IXMLTag> lNewTags = new ArrayList<IXMLTag>(0);
		if ((aParentTag == null) || (aTextbox == null))
			return lNewTags;
		String lValue = aTextbox.getText().trim();
		if ((lValue == null) || (lValue.equals(""))) {
			if (aAllowEmpty)
				lValue = "";
			else
				return lNewTags;
		}
		if ((lValue == null) || (lValue.equals("")))
			return lNewTags;
		List<IXMLTag> lTags = aParentTag.getChildTags();
		IXmlManager	xmlManager = CDesktopComponents.sSpring().getXmlManager();
		if (aNodeName.contains("value")) {
			IXMLTag lTag = xmlManager.newXMLTag(aNodeName, lValue);
			lNewTags.add(lTag);
			lTags.add(lTag);
			return lNewTags;
		}
		// consider operatorvalue values as 1 string
		if (aNodeName.contains("operatorvalue")) {
			IXMLTag lTag = xmlManager.newXMLTag(aNodeName, lValue);
			lNewTags.add(lTag);
			lTags.add(lTag);
			return lNewTags;
		}
		// values are separated by comma
		String[] lOperatorValueArr = lValue.split(",");
		for (int i = 0; i < lOperatorValueArr.length; i++) {
			String lId = lOperatorValueArr[i];
			IXMLTag lTag = xmlManager.newXMLTag(aNodeName, lId);
			lNewTags.add(lTag);
			lTags.add(lTag);
		}
		return lNewTags;
	}

	/**
	 * Sets xml child tag by adding child tag to aParentTag, with name aNodeName
	 * and value given by aCheckbox.
	 * 
	 * @param aParentTag the parent tag
	 * @param aCheckbox the checkbox
	 * @param aNodeName the node name
	 * 
	 * @return the child tag
	 */
	private IXMLTag setXMLChildTag(IXMLTag aParentTag, Checkbox aCheckbox, String aNodeName) {
		if (aParentTag == null || aCheckbox == null)
			return null;
		IXmlManager	xmlManager = CDesktopComponents.sSpring().getXmlManager();
		boolean lIsChecked = aCheckbox.isChecked();
		IXMLTag lTag = aParentTag.getChild(aNodeName);
		if (lTag == null) {
			lTag = xmlManager.newXMLTag(aNodeName, "" + lIsChecked);
			aParentTag.getChildTags().add(lTag);
		}
		else {
			lTag.setValue("" + lIsChecked);
		}
		return lTag;
	}

	/**
	 * On click, save (new) values of input elements within CScriptMethodHbox within method, either a condition or action.
	 */
	public void onClick() {
		CCdeScriptMethodWnd lWindow = (CCdeScriptMethodWnd) getRoot();
		// node name of edited condition or action part
		String lNodeName = lWindow.getNodename();

		// all possible input elements, not all are existing simultaneously 
		CScriptCacCombo lScriptCacCombo = (CScriptCacCombo) CDesktopComponents.vView().getComponent(lNodeName+"scriptCacId");
		CScriptComCombo lScriptComCombo = (CScriptComCombo) CDesktopComponents.vView().getComponent(lNodeName+"scriptComId");
		CScriptCarListbox lScriptCarListbox = (CScriptCarListbox) CDesktopComponents.vView().getComponent(lNodeName+"scriptCarIds");
		CDefTextbox lScriptComponentTemplates = (CDefTextbox) CDesktopComponents.vView().getComponent(lNodeName+"scriptComponentTemplates");
		CDefCheckbox lScriptComponentTemplatesCheckbox = (CDefCheckbox) CDesktopComponents.vView().getComponent(lNodeName+"scriptComponentTemplatesCheckbox");
		CScriptTagNameCombo lScriptTagNameCombo = (CScriptTagNameCombo) CDesktopComponents.vView().getComponent(lNodeName+"scriptTagName");
		CScriptTagListbox lScriptTagListbox = (CScriptTagListbox) CDesktopComponents.vView().getComponent(lNodeName+"scriptTagIds");
		CScriptTagCombo lScriptTagCombo = (CScriptTagCombo) CDesktopComponents.vView().getComponent(lNodeName+"scriptTagId");
		CDefTextbox lScriptTagTemplates = (CDefTextbox) CDesktopComponents.vView().getComponent(lNodeName+"scriptTagTemplates");
		CDefCheckbox lScriptTagTemplatesCheckbox = (CDefCheckbox) CDesktopComponents.vView().getComponent(lNodeName+"scriptTagTemplatesCheckbox");
		CScriptStatusCombo lScriptStatusCombo = (CScriptStatusCombo) CDesktopComponents.vView().getComponent(lNodeName+"scriptStatusId");
		CScriptFunctionCombo lScriptFunctionCombo = (CScriptFunctionCombo) CDesktopComponents.vView().getComponent(lNodeName+"scriptFunctionId");
		CDefTextbox lScriptValueTextbox = (CDefTextbox) CDesktopComponents.vView().getComponent(lNodeName+"scriptValueTextbox");
		CScriptOperatorValueCombo lScriptValueCombo = (CScriptOperatorValueCombo) CDesktopComponents.vView().getComponent(lNodeName+"scriptValueCombo");
		CScriptOperatorCombo lScriptOperatorCombo = (CScriptOperatorCombo) CDesktopComponents.vView().getComponent(lNodeName+"scriptOperatorId");
		CScriptOperatorValueCombo lScriptOperatorValue = (CScriptOperatorValueCombo) CDesktopComponents.vView().getComponent(lNodeName+"scriptOperatorValue");
		CDefTextbox lScriptOperatorValues = (CDefTextbox) CDesktopComponents.vView().getComponent(lNodeName+"scriptOperatorValues");

		IXmlManager	xmlManager = CDesktopComponents.sSpring().getXmlManager();
		IXMLTag lMethod = null;
		boolean lTemplate = CContentHelper.isTemplateMethod(lNodeName);
		boolean lTagStatus = CContentHelper.isMethodUsingTags(lNodeName); 
		boolean lComponentStatus = !lTagStatus;
		boolean lCondition = CContentHelper.isMethodUsedInCondition(lNodeName);
		boolean lAction = CContentHelper.isMethodUsedInAction(lNodeName) || CContentHelper.isMethodUsedInRunAction(lNodeName);
		if (lTagStatus) {
			lMethod = xmlManager.newXMLTag(lNodeName, "");
			if (!lTemplate) {
				// save chosen case component
				// cac combo so only one cac selected
				List<IXMLTag> lCacId = setXMLChildTags(lMethod, lScriptCacCombo, "cacid");
				if (lCacId.size() > 0) {
					IXMLTag lCac = lCacId.get(0);
					if (lScriptTagListbox != null) {
						// save chosen tag ids
						setXMLChildTags(lCac, lScriptTagListbox, "tagid");
					}
					if (lScriptTagCombo != null) {
						// save chosen tag id
						setXMLChildTags(lCac, lScriptTagCombo, "tagid");
					}
					if (lScriptTagTemplates != null) {
						// save entered tag id templates
						setXMLChildTags(lCac, lScriptTagTemplates, "tagtemplate", false);
					}
					if (lScriptTagTemplatesCheckbox != null) {
						// save entered or operation
						setXMLChildTag(lCac, lScriptTagTemplatesCheckbox, "tagtemplateoroperation");
					}
				}
			}
			else {
				// save chosen component
				// com combo so only one com selected
				List<IXMLTag> lComId = setXMLChildTags(lMethod, lScriptComCombo, "comid");
				if (lComId.size() > 0) {
					IXMLTag lCom = lComId.get(0);
					if (lScriptComponentTemplates != null) {
						// save entered component id templates
						setXMLChildTags(lCom, lScriptComponentTemplates, "componenttemplate", false);
					}
					if (lScriptComponentTemplatesCheckbox != null) {
						// save entered or operation
						setXMLChildTag(lCom, lScriptComponentTemplatesCheckbox, "componenttemplateoroperation");
					}
					if (lScriptTagTemplates != null) {
						// save entered tag id templates
						setXMLChildTags(lCom, lScriptTagTemplates, "tagtemplate", false);
					}
					if (lScriptTagTemplatesCheckbox != null) {
						// save entered or operation
						setXMLChildTag(lCom, lScriptTagTemplatesCheckbox, "tagtemplateoroperation");
					}
				}
			}
		}
		if (lComponentStatus) {
			lMethod = xmlManager.newXMLTag(lNodeName, "");
			if (!lTemplate) {
				// save chosen case component
				setXMLChildTags(lMethod, lScriptCacCombo, "cacid");
			}
			else {
				// save chosen component
				List<IXMLTag> lComId = setXMLChildTags(lMethod, lScriptComCombo, "comid");
				if (lComId.size() > 0) {
					IXMLTag lCom = lComId.get(0);
					if (lScriptComponentTemplates != null) {
						// save entered component id templates
						setXMLChildTags(lCom, lScriptComponentTemplates, "componenttemplate", false);
					}
					if (lScriptComponentTemplatesCheckbox != null) {
						// save entered or operation
						setXMLChildTag(lCom, lScriptComponentTemplatesCheckbox, "componenttemplateoroperation");
					}
				}
			}
		}
		if (lMethod != null && lScriptCarListbox != null) {
			// save chosen case roles
			setXMLChildTags(lMethod, lScriptCarListbox, "carid");
		}
		if (lMethod != null && lScriptTagNameCombo != null) {
			// save chosen tag name
			setXMLChildTags(lMethod, lScriptTagNameCombo, "tagname");
		}
		if (lMethod != null && lScriptStatusCombo != null) {
			// save chosen status id
			setXMLChildTags(lMethod, lScriptStatusCombo, "statusid");
		}
		if ((lCondition || lAction) && lMethod != null && lScriptFunctionCombo != null) {
			// save chosen function id
			setXMLChildTags(lMethod, lScriptFunctionCombo, "functionid");
		}

		boolean lBoolInput = 
			(CDesktopComponents.sSpring().getAppManager().getTagOperatorValueType(
					lMethod.getChildValue("comid"), 
					lMethod.getChildValue("cacid"), 
					lMethod.getChildValue("tagname"), 
					lMethod.getChildValue("statusid"), 
					lMethod.getChildValue("functionid"))
					.equals("boolean"));

		if (lCondition && lMethod != null) {
			// save chosen value
			// NOTE only one of the two will be visible. Visibility is managed by setting parent's visibility!
			if (lScriptValueTextbox != null && lScriptValueTextbox.getParent().isVisible()) {
				setXMLChildTags(lMethod, lScriptValueTextbox, "value", true);
			}
			if (lScriptValueCombo != null && lScriptValueCombo.getParent().isVisible()) {
				setXMLChildTags(lMethod, lScriptValueCombo, "value");
			}
		}
		if (lCondition && lMethod != null && lScriptOperatorCombo != null) {
			// save chosen operator id
			List<IXMLTag> lOperatorId = setXMLChildTags(lMethod, lScriptOperatorCombo, "operatorid");
			if (lOperatorId.size() > 0) {
//				only one operator
				IXMLTag lOperator = lOperatorId.get(0);
				if (lScriptOperatorValue != null && lBoolInput) {
					// save chosen operator value
					setXMLChildTags(lOperator, lScriptOperatorValue, "operatorvalue");
				}
				if (lScriptOperatorValues != null && !lBoolInput) {
					// save chosen operator value
					setXMLChildTags(lOperator, lScriptOperatorValues, "operatorvalue", true);
				}
			}
		}
		if (lAction && lMethod != null && lScriptOperatorValue != null) {
			if (lScriptOperatorValue != null && lBoolInput) {
				// save chosen operator value
				setXMLChildTags(lMethod, lScriptOperatorValue, "operatorvalue");
			}
			if (lScriptOperatorValues != null && !lBoolInput) {
				// save chosen operator value
				setXMLChildTags(lMethod, lScriptOperatorValues,	"operatorvalue", true);
			}
		}

		// return modified method to calling window
		lWindow.setAttribute("item", lMethod);
		lWindow.detach();
	}
}
