/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.script;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import org.zkoss.lang.Strings;

import nl.surf.emergo.business.IAppManager;
import nl.surf.emergo.business.IXMLAttributeValueTime;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.domain.IEComponent;
import nl.surf.emergo.zkspring.SSpring;

/**
 * The Class CScript.
 * 
 * Is used for script case component, as well in edit and run environment.
 * There is some overlap with XmlManager class, so maybe this code should be moved to this class.
 * Maybe this class should be integrated within the SSpring class.
 */
public class CScript extends Object {

	/** The SSpring. */
	private SSpring sSpring = null;
	
	/**
	 * Instantiates a new cscript.
	 */
	public CScript() {
		sSpring = CDesktopComponents.sSpring();
	}

	/**
	 * Instantiates a new cscript.
	 *
	 * @param aSpring the a spring
	 */
	public CScript(SSpring aSpring) {
		sSpring = aSpring;
	}

	/**
	 * Gets the app manager.
	 * 
	 * @return the app manager
	 */
	public IAppManager getAppManager() {
		return sSpring.getAppManager();
	}

	/**
	 * Gets a list of case roles applicable for case component given by cac id.
	 * 
	 * @param aCacId the db cac id
	 * 
	 * @return the case roles by cac id
	 */
	public List<IECaseRole> getCaseRolesByCacId(String aCacId) {
		return sSpring.getSCaseRoleHelper().getCaseRolesByCacId(aCacId);
	}

	/**
	 * Gets a list of case roles by master slave cac ids.
	 * See corresponding method in SSpring class
	 * 
	 * @param aMCacId the master db cac id
	 * @param aSCacId the slave db cac id
	 * 
	 * @return the case roles by master slave cac ids
	 */
	public List<IECaseRole> getCaseRolesByMasterSlaveCacIds(String aMCacId, String aSCacId) {
		return sSpring.getSCaseRoleHelper().getCaseRolesByMasterSlaveCacIds(aMCacId, aSCacId);
	}

	/**
	 * Gets the node tags of a case component given by aCacId.
	 * 
	 * @param aCacId the db cac id
	 * 
	 * @return list of node tags
	 */
	public List<IXMLTag> getNodeTags(String aCacId) {
		IXMLTag lRootTag = sSpring.getXmlDataTree(aCacId);
		return getNodeTags(lRootTag);
	}

	/**
	 * Gets the node tags of a case component.
	 * 
	 * @param aCaseComponent the a case component
	 * 
	 * @return list of node tags
	 */
	public List<IXMLTag> getNodeTags(IECaseComponent aCaseComponent) {
		IXMLTag lRootTag = sSpring.getXmlDataTree(aCaseComponent);
		return getNodeTags(lRootTag);
	}

	/**
	 * Gets a list of run group node tags, that is data tags enriched with status tags
	 * for case component given by aCacId and the current case role within the player.
	 * Is only used within the player.
	 * 
	 * @param aCacId the cac id
	 * 
	 * @return the rungrouptags
	 */
	public List<IXMLTag> getRunGroupNodeTags(String aCacId) {
		IXMLTag lRootTag = sSpring.getXmlDataPlusRunStatusTree(aCacId, AppConstants.statusTypeRunGroup);
		return getNodeTags(lRootTag);
	}

	/**
	 * Gets a list of run group node tags, that is data tags enriched with status tags
	 * for case component given by aCacId and the current case role within the player.
	 * Is only used within the player.
	 * 
	 * @param aCaseComponent the a case component
	 * 
	 * @return the rungrouptags
	 */
	public List<IXMLTag> getRunGroupNodeTags(IECaseComponent aCaseComponent) {
		IXMLTag lRootTag = sSpring.getXmlDataPlusRunStatusTree(aCaseComponent, AppConstants.statusTypeRunGroup);
		return getNodeTags(lRootTag);
	}

	/**
	 * Gets a list of nodetags.
	 * 
	 * @param aRootTag the root tag
	 * 
	 * @return the nodetags
	 */
	public List<IXMLTag> getNodeTags(IXMLTag aRootTag) {
		if (aRootTag == null)
			return null;
		IXMLTag lContentTag = aRootTag.getChild(AppConstants.contentElement);
		if (lContentTag == null)
			return null;
		List<IXMLTag> lTags = new ArrayList<IXMLTag>(0);
		getNewNodeTagList(lContentTag.getChildTags(AppConstants.defValueNode), lTags);
		return lTags;
	}

	/**
	 * Gets a list of nodetags.
	 * 
	 * @param aParentTag the parent tag
	 * 
	 * @return the nodetags
	 */
	public List<IXMLTag> getChildNodeTags(IXMLTag aParentTag) {
		if (aParentTag == null)
			return null;
		List<IXMLTag> lTags = new ArrayList<IXMLTag>(0);
		getNewNodeTagList(aParentTag.getChildTags(AppConstants.defValueNode), lTags);
		return lTags;
	}

	/**
	 * Gets a list of all tags.
	 * 
	 * @param aRootTag the root tag
	 * 
	 * @return all tags
	 */
	public List<IXMLTag> getAllTags(IXMLTag aRootTag) {
		if (aRootTag == null)
			return null;
		IXMLTag lContentTag = aRootTag.getChild(AppConstants.contentElement);
		if (lContentTag == null)
			return null;
		List<IXMLTag> lTags = new ArrayList<IXMLTag>(0);
		getNewAllTagList(lContentTag.getChildTags(), lTags);
		return lTags;
	}

	/**
	 * Gets a list of node tags with name given by aTagNames and case component given by aCacId.
	 * 
	 * @param aCacId the cac id
	 * @param aTagNames the tag names comma separated
	 * 
	 * @return the node tags
	 */
	public List<IXMLTag> getNodeTags(String aCacId, String aTagNames) {
		List<IXMLTag> lTags = getNodeTags(aCacId);
		return getTags(lTags,aTagNames);
	}

	/**
	 * Gets a list of node tags with name given by aTagNames and case component.
	 * 
	 * @param aCaseComponent the a case component
	 * @param aTagNames the tag names comma separated
	 * 
	 * @return the node tags
	 */
	public List<IXMLTag> getNodeTags(IECaseComponent aCaseComponent, String aTagNames) {
		List<IXMLTag> lTags = getNodeTags(aCaseComponent);
		return getTags(lTags,aTagNames);
	}

	/**
	 * Gets a list of node tags with name given by aTagNames and a parent tag.
	 * 
	 * @param aParentTag the a parent tag
	 * @param aTagNames the tag names comma separated
	 * 
	 * @return the node tags
	 */
	public List<IXMLTag> getChildNodeTags(IXMLTag aParentTag, String aTagNames) {
		List<IXMLTag> lTags = getChildNodeTags(aParentTag);
		return getTags(lTags,aTagNames);
	}

	/**
	 * Gets a list of run group node tags with name aTagNames, that is data tags enriched with
	 * status tags for case component given by aCacId and the current case role within the player.
	 * Is only used within the player.
	 * 
	 * @param aCacId the cac id
	 * @param aTagNames the tag names comma separated
	 * 
	 * @return the rungrouptags
	 */
	public List<IXMLTag> getRunGroupNodeTags(String aCacId, String aTagNames) {
		List<IXMLTag> lTags = getRunGroupNodeTags(aCacId);
		return getTags(lTags,aTagNames);
	}

	/**
	 * Gets a list of run group node tags with name aTagNames, that is data tags enriched with
	 * status tags for case component given by aCacId and the current case role within the player.
	 * Is only used within the player.
	 * 
	 * @param aCaseComponent the case component
	 * @param aTagNames the tag names comma separated
	 * 
	 * @return the rungrouptags
	 */
	public List<IXMLTag> getRunGroupNodeTags(IECaseComponent aCaseComponent, String aTagNames) {
		List<IXMLTag> lTags = getRunGroupNodeTags(aCaseComponent);
		return getTags(lTags,aTagNames);
	}

	/**
	 * Gets a list of tags with name aTagNames out of list of tags, aTags.
	 * 
	 * @param aTags the tags
	 * @param aTagNames the tag names comma separated
	 * 
	 * @return the tags
	 */
	public List<IXMLTag> getTags(List<IXMLTag> aTags, String aTagNames) {
		if (aTagNames.equals(""))
			return aTags;
		String[] lTagNames = aTagNames.split(",");
		if (aTags == null)
			return aTags;
		for (int i = (aTags.size() - 1); i >= 0; i--) {
			IXMLTag lTag = aTags.get(i);
			boolean lFound = false;
			for (int j=0;j<lTagNames.length;j++) {
				if (lTag.getName().equals(lTagNames[j]))
					lFound = true;
			}
			if (!lFound)
				aTags.remove(i);
		}
		return aTags;
	}

	/**
	 * Gets a new node tag list.
	 * Loops through list aTags and makes a new list with the same tags.
	 * 
	 * @param aTags the tag list
	 * @param aNewTags used to return the new tag list
	 */
	public void getNewNodeTagList(List<IXMLTag> aTags, List<IXMLTag> aNewTags) {
		if (aTags == null)
			return;
		for (IXMLTag lTag : aTags) {
			aNewTags.add(lTag);
			getNewNodeTagList(lTag.getChildTags(AppConstants.defValueNode), aNewTags);
		}
	}

	/**
	 * Gets a new all tag list.
	 * Loops through list aTags and makes a new list with the same tags.
	 * 
	 * @param aTags the tag list
	 * @param aNewTags used to return the new tag list
	 */
	public void getNewAllTagList(List<IXMLTag> aTags, List<IXMLTag> aNewTags) {
		if (aTags == null)
			return;
		for (IXMLTag lTag : aTags) {
			aNewTags.add(lTag);
			getNewAllTagList(lTag.getChildTags(), aNewTags);
		}
	}

	/**
	 * Gets a String list of all node tag names, within the xml data of
	 * case component given by aCacId.
	 * 
	 * @param aCacId the cac id
	 * @param aDefTagChildName the def tag child name
	 * 
	 * @return the node tag names
	 */
	public List<String> getNodeTagNames(String aCacId, String aDefTagChildName) {
		return getNodeTagNames(sSpring.getCaseComponent(aCacId), aDefTagChildName);
	}

	/**
	 * Gets a String list of all node tag names, within the xml data of
	 * case component given by aCacId.
	 * 
	 * @param aCaseComponent the case component
	 * @param aDefTagChildName the def tag child name
	 * 
	 * @return the node tag names
	 */
	public List<String> getNodeTagNames(IECaseComponent aCaseComponent, String aDefTagChildName) {
		if (aCaseComponent == null)
			return null;
		IXMLTag lRootTag = sSpring.getXmlDataTree(aCaseComponent);
		if (lRootTag == null)
			return null;
		IXMLTag lContentTag = lRootTag.getChild(AppConstants.contentElement);
		if (lContentTag == null)
			return null;
		List<IXMLTag> lTags = new ArrayList<IXMLTag>(0);
		getNewNodeTagList(lContentTag.getChildTags(AppConstants.defValueNode), lTags);
		// use hashtable to get unique names 
		Hashtable<String,String> hAllTagNames = new Hashtable<String,String>(0);
		Hashtable<String,String> hTagNames = new Hashtable<String,String>(0);
		for (IXMLTag lTag : lTags) {
			if (!hAllTagNames.containsKey(lTag.getName())) {
				hAllTagNames.put(lTag.getName(), "");
				Hashtable<String,List<IXMLAttributeValueTime>> lAttributes = getDefTagStatusAttributes(aCaseComponent.getEComponent(), lTag.getName(), aDefTagChildName);
				if (lAttributes != null && lAttributes.size() > 0) {
					hTagNames.put(lTag.getName(), "");
				}
			}
		}
		List<String> lTagNames = new ArrayList<String>(0);
		for (Enumeration<String> keys = hTagNames.keys(); keys.hasMoreElements();) {
			lTagNames.add(keys.nextElement());
		}
		return lTagNames;
	}

	/**
	 * Gets a String list of all node tag names, within the xml data of
	 * component given by aComId.
	 * 
	 * @param aComId the com id
	 * @param aDefTagChildName the def tag child name
	 * 
	 * @return the node tag names
	 */
	public List<String> getDefNodeTagNames(String aComId, String aDefTagChildName) {
		IEComponent lComponent = sSpring.getComponent(aComId);
		if (lComponent == null)
			return null;
		IXMLTag lRootTag = sSpring.getXmlDefTree(lComponent);
		if (lRootTag == null)
			return null;
		IXMLTag lContentTag = lRootTag.getChild(AppConstants.contentElement);
		if (lContentTag == null)
			return null;
		List<IXMLTag> lTags = new ArrayList<IXMLTag>(0);
		getNewNodeTagList(lContentTag.getChildTags(AppConstants.defValueNode), lTags);
		// use hashtable to get unique names 
		Hashtable<String,String> hAllTagNames = new Hashtable<String,String>(0);
		Hashtable<String,String> hTagNames = new Hashtable<String,String>(0);
		for (IXMLTag lTag : lTags) {
			if (!hAllTagNames.containsKey(lTag.getName())) {
				hAllTagNames.put(lTag.getName(), "");
				Hashtable<String,List<IXMLAttributeValueTime>> lAttributes = getDefTagStatusAttributes(aComId, lTag.getName(), aDefTagChildName);
				if ((lAttributes != null) && (lAttributes.size() > 0)) {
					hTagNames.put(lTag.getName(), "");
				}
		}
		}
		List<String> lTagNames = new ArrayList<String>(0);
		for (Enumeration<String> keys = hTagNames.keys(); keys.hasMoreElements();) {
			lTagNames.add(keys.nextElement());
		}
		return lTagNames;
	}

	/**
	 * Gets a String list of all node tag content names, within the xml data of
	 * case component given by aCacId and tag given by aTagName.
	 * 
	 * @param aCacId the cac id
	 * @param aTagName the tag name
	 * 
	 * @return the node tag content names
	 */
	public List<String> getNodeTagContentNames(String aCacId, String aTagName) {
		IECaseComponent lCaseComponent = sSpring.getCaseComponent(aCacId);
		if (lCaseComponent == null)
			return null;
		IXMLTag lRootTag = sSpring.getXmlDefTree(lCaseComponent.getEComponent());
		if (lRootTag == null)
			return null;
		IXMLTag lContentTag = lRootTag.getChild(AppConstants.contentElement);
		if (lContentTag == null)
			return null;
		List<IXMLTag> lTags = lContentTag.getChilds(aTagName);
		List<String> lTagContentNames = new ArrayList<String>(0);
		for (IXMLTag lTag : lTags) {
			for (IXMLTag lChildTag : lTag.getChildTags()) {
				//NOTE comment out following line, private child tags are also allowed
				//It is the responsibility of the case developer to handle these input with care
//				if (!lChildTag.getFirstAttribute(AppConstants.defKeyPrivate).equals(AppConstants.statusValueTrue)) {
					//no private tags
					String lName = lChildTag.getName();
					boolean lOk = false;
					for (String lChildTagName : AppConstants.nodeChildTagsWithText) {
						if (lChildTagName.equals(lName)) {
							lOk = true;
							break;
						}
					}
					if (lOk) {
						lTagContentNames.add(lChildTag.getName());
					}
//				}
			}
		}
		return lTagContentNames;
	}

	/**
	 * Gets the definition tag status attributes of case component given by aCacId,
	 * for tag given by aTagName. This info is found in the content part of
	 * the xml definition.
	 * If aTagName is empty, tag status attributes of component part of
	 * xml definition are given.
	 * 
	 * @param aCacId the db cac id
	 * @param aTagName the tag name
	 * @param aDefTagChildName the def tag child name
	 * 
	 * @return a hashtable of definition tag status attributes
	 */
	public Hashtable<String,List<IXMLAttributeValueTime>> getTagStatusAttributes(String aCacId, String aTagName, String aDefTagChildName) {
		IECaseComponent lCaseComponent = sSpring.getCaseComponent(aCacId);
		if (lCaseComponent == null)
			return null;
		return getDefTagStatusAttributes(lCaseComponent.getEComponent(), aTagName, aDefTagChildName);
	}

	/**
	 * Gets the definition tag status attributes of component given by aComId,
	 * for tag given by aTagName. This info is found in the content part of
	 * the xml definition.
	 * If aTagName is empty, tag status attributes of component part of
	 * xml definition are given.
	 * 
	 * @param aComId the db com id
	 * @param aTagName the tag name
	 * @param aDefTagChildName the def tag child name
	 * 
	 * @return a hashtable of definition tag status attributes
	 */
	public Hashtable<String,List<IXMLAttributeValueTime>> getDefTagStatusAttributes(String aComId, String aTagName, String aDefTagChildName) {
		return getDefTagStatusAttributes(sSpring.getComponent(aComId), aTagName, aDefTagChildName);
	}

	/**
	 * Gets the definition tag status attributes of component given by aComponent,
	 * for tag given by aTagName. This info is found in the content part of
	 * the xml definition.
	 * If aTagName is empty, tag status attributes of component part of
	 * xml definition are given.
	 * 
	 * @param aComponent the a component
	 * @param aTagName the tag name
	 * @param aDefTagChildName the def tag child name
	 * 
	 * @return a hashtable of definition tag status attributes
	 */
	public Hashtable<String,List<IXMLAttributeValueTime>> getDefTagStatusAttributes(IEComponent aComponent, String aTagName, String aDefTagChildName) {
		if (aComponent == null)
			return null;
		// get xml definition root tag
		IXMLTag lDefRootTag = sSpring.getXmlDefTree(aComponent);
		if (lDefRootTag == null)
			return null;
		IXMLTag lDefTag = null;
		if ((aTagName == null) || (aTagName.equals("")) || (aTagName.equals("0")))
			// if tagname empty, use component part
			lDefTag = lDefRootTag.getChild(AppConstants.componentElement);
		else {
			// if tagname not empty, use content part
			lDefTag = lDefRootTag.getChild(AppConstants.contentElement);
			if (lDefTag != null)
				// and get definition tag with name aTagName
				// all definition tags are child of the content root
				lDefTag = lDefTag.getChild(aTagName);
		}
		if (lDefTag == null)
			return null;
		// attributes are found in status child
		IXMLTag lDefStatusTag = lDefTag.getChild(AppConstants.statusElement);
		String lAttributesStr = null;
		if (!Strings.isEmpty(aDefTagChildName)) {
			IXMLTag lDefTagChild = lDefTag.getChild(aDefTagChildName);
			if (lDefTagChild != null) {
				lAttributesStr = lDefTagChild.getAttribute(AppConstants.defKeyAttributes);
			}
		}
		if (lDefStatusTag == null)
			return null;
		Hashtable<String,List<IXMLAttributeValueTime>> lAttributes = lDefStatusTag.getAttributes();
		if (lAttributesStr != null) {
			if (!lAttributesStr.equals("")) {
				lAttributesStr = "," + lAttributesStr + ",";
			}
			Hashtable<String,List<IXMLAttributeValueTime>> lFilteredAttributes = new Hashtable<String,List<IXMLAttributeValueTime>>();
			for (Enumeration<String> keys = lAttributes.keys(); keys.hasMoreElements();) {
				String lKey = keys.nextElement();
				if (lAttributesStr.contains("," + lKey + ",")) {
					lFilteredAttributes.put(lKey, lAttributes.get(lKey));
				}
			}
			lAttributes = lFilteredAttributes;
		}
		if ((lAttributes == null) || (lAttributes.size() == 0))
			return null;
		return lAttributes;
	}

	/**
	 * Gets the tag status key ids.
	 * 
	 * Gets the tag status attributes of case component given by aCacId,
	 * for tag given by aTagName.
	 * If aTagName is empty, attributes of component are given
	 * 
	 * @param aCacId the db cac id
	 * @param aTagName the tag name
	 * @param aDefTagChildName the def tag child name
	 * 
	 * @return the tag status key ids
	 */
	public List<String> getTagStatusKeyIds(String aCacId, String aTagName, String aDefTagChildName) {
		Hashtable<String,List<IXMLAttributeValueTime>> lAttributes = getTagStatusAttributes(aCacId, aTagName, aDefTagChildName);
		if ((lAttributes == null) || (lAttributes.size() == 0))
			return null;
		List<String> lTagStatusIds = new ArrayList<String>(0);
		for (Enumeration<String> keys = lAttributes.keys(); keys.hasMoreElements();) {
			String lKey = keys.nextElement();
			if (!lKey.equals("status")) {
				// don't give back the status attribute, it is not used
				int lStatusKeyIndex = getAppManager().getStatusKeyIndex(lKey);
				if (lStatusKeyIndex != -1)
					lTagStatusIds.add("" + lStatusKeyIndex);
				else
					lTagStatusIds.add("" + lKey);
			}
		}
		return lTagStatusIds;
	}

	/**
	 * Gets the tag status key ids.
	 * 
	 * Gets the tag status attributes of component given by aComId,
	 * for tag given by aTagName.
	 * If aTagName is empty, attributes of component are given
	 * 
	 * @param aComId the db com id
	 * @param aTagName the tag name
	 * @param aDefTagChildName the def tag child name
	 * 
	 * @return the tag status key ids
	 */
	public List<String> getDefTagStatusKeyIds(String aComId, String aTagName, String aDefTagChildName) {
		Hashtable<String,List<IXMLAttributeValueTime>> lAttributes = getDefTagStatusAttributes(aComId, aTagName, aDefTagChildName);
		if ((lAttributes == null) || (lAttributes.size() == 0))
			return null;
		List<String> lTagStatusIds = new ArrayList<String>(0);
		for (Enumeration<String> keys = lAttributes.keys(); keys.hasMoreElements();) {
			String lKey = keys.nextElement();
			if (!lKey.equals("status")) {
				// don't give back the status attribute, it is not used
				int lStatusKeyIndex = getAppManager().getStatusKeyIndex(lKey);
				if (lStatusKeyIndex != -1)
					lTagStatusIds.add("" + lStatusKeyIndex);
				else
					lTagStatusIds.add("" + lKey);
			}
		}
		return lTagStatusIds;
	}

	/**
	 * Gets the tag condition function key ids.
	 * See corresponding method in AppManager class
	 * 
	 * @param aStatusKeyId the status key id
	 * 
	 * @return the tag condition function key ids
	 */
	public List<String> getTagConditionFunctionKeyIds(String aStatusKeyId) {
		return sSpring.getAppManager().getTagConditionFunctionKeyIds(aStatusKeyId);
	}

	/**
	 * Gets the tag action function key ids.
	 * See corresponding method in AppManager class
	 * 
	 * @param aStatusKeyId the status key id
	 * 
	 * @return the tag action function key ids
	 */
	public List<String> getTagActionFunctionKeyIds(String aStatusKeyId) {
		return sSpring.getAppManager().getTagActionFunctionKeyIds(aStatusKeyId);
	}

	/**
	 * Gets the tag operator key ids.
	 * See corresponding method in AppManager class
	 * 
	 * @param aStatusKeyId the status key id
	 * @param aStatusFunctionId the status function id
	 * 
	 * @return the tag operator key ids
	 */
	public List<String> getTagOperatorKeyIds(String aStatusKeyId, String aStatusFunctionId) {
		return sSpring.getAppManager().getTagOperatorKeyIds(aStatusKeyId, aStatusFunctionId);
	}

	/**
	 * Gets the tag operator value ids.
	 * See corresponding method in AppManager class
	 * 
	 * @return the tag operator value ids
	 */
	public List<String> getTagOperatorValueIds() {
		return sSpring.getAppManager().getTagOperatorValueIds();
	}

	/**
	 * Gets the tag operator value type.
	 * See corresponding method in AppManager class
	 * 
	 * @param aCacId the db cac id
	 * @param aTagName the tag name
	 * @param aStatusKeyId the status key id
	 * @param aStatusFunctionId the status function id
	 * 
	 * @return the tag operator value type
	 */
	public String getTagOperatorValueType(String aCacId, String aTagName, String aStatusKeyId, String aStatusFunctionId) {
		return sSpring.getAppManager().getTagOperatorValueType("", aCacId, aTagName, aStatusKeyId, aStatusFunctionId);
	}

}
