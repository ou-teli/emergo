/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.script;

import nl.surf.emergo.control.CCombo;
import nl.surf.emergo.control.CContentHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.view.VView;

/**
 * The Class CScriptStatusCombo.
 * 
 * Used as combobox for status keys, used within the CScriptMethodHbox class.
 */
public class CScriptStatusCombo extends CCombo {

	private static final long serialVersionUID = -7517416933763867870L;

	/**
	 * Instantiates a new c script status combo.
	 * 
	 * @param aId the a id
	 * @param aTagName the a tag name
	 */
	public CScriptStatusCombo(String aId, String aTagName) {
		super(aId, "tagname", aTagName, VView.statuskeyLabelKeyPrefix);
	}

	/**
	 * Gets the item id, the serial number of the status key.
	 * 
	 * @param aId the a id
	 * 
	 * @return the item id
	 */
	@Override
	protected String getItemLabel(int aId) {
		return CDesktopComponents.vView().getLabel(labelPrefix + aId);
	}

	/**
	 * Gets the item name, the name of the status key.
	 * 
	 * @param aItem the item
	 * 
	 * @return the item name
	 */
	@Override
	protected String getItemId(Object aItem) {
		return ((String)aItem);
	}

	/* (non-Javadoc)
	 * @see nl.surf.emergo.control.CCombo#getItemName(java.lang.Object)
	 */
	@Override
	protected String getItemName(Object aItem) {
		String lKeyId = (String) aItem;
		String lTagName = (String)getAttribute("tagname");
		String lLabel = "";
		//NOTE action label may be different from condition label
		/*
		if (lTagName != null && lTagName.equals("action")) {
			lLabel = CDesktopComponents.vView().getLabel(labelPrefix + "action." + lKeyId);
		}
		if (lLabel.equals("")) {
			lLabel = CDesktopComponents.vView().getLabel(labelPrefix + lKeyId);
		}
		*/
		
		if (lTagName != null && lTagName.equals("action")) {
			lLabel = CContentHelper.getLabelText(getComponentCode(), getChosenTagName(), lKeyId, labelPrefix + "action.");
		}
		if (lLabel.equals("")) {
			lLabel = CContentHelper.getLabelText(getComponentCode(), getChosenTagName(), lKeyId, labelPrefix);
		}
		
		return lLabel;
	}

	/**
	 * Gets the item help.
	 *
	 * @param aItem the item
	 *
	 * @return the item help
	 */
	@Override
	protected String getItemHelp(Object aItem) {
		String lKeyId = (String) aItem;
		String lTagName = (String)getAttribute("tagname");
		String lLabel = "";
		/*
		if (lTagName != null && lTagName.equals("action")) {
			lLabel = CDesktopComponents.vView().getLabel(labelPrefix + "action." + lKeyId + ".help");
		}
		if (lLabel.equals("")) {
			lLabel = CDesktopComponents.vView().getLabel(labelPrefix + lKeyId + ".help");
		}
		*/
		
		if (lTagName != null && lTagName.equals("action")) {
			lLabel = CContentHelper.getHelpText(getComponentCode(), getChosenTagName(), lKeyId, labelPrefix + "action.");
		}
		if (lLabel.equals("")) {
			lLabel = CContentHelper.getHelpText(getComponentCode(), getChosenTagName(), lKeyId, labelPrefix);
		}
		
		//NOTE help text is show as tooltiptext so replace HTML breaks
		return lLabel.replaceAll("<br/>", " ");
	}

}
