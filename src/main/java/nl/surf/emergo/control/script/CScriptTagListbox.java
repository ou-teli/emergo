/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.script;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CListbox;
import nl.surf.emergo.view.VView;


/**
 * The Class CScriptTagListbox.
 * 
 * Used as listbox for tags, used within the CScriptMethodHbox class.
 */
public class CScriptTagListbox extends CListbox {

	private static final long serialVersionUID = 6005066669769605925L;

	/**
	 * Instantiates a new script tag listbox.
	 * 
	 * @param aId the id
	 * @param aTagName the tag name
	 */
	public CScriptTagListbox(String aId, String aTagName) {
		super(aId, "tagname", aTagName,  VView.nodetagLabelKeyPrefix);
	}

	/**
	 * Gets the item id, the id of the tag.
	 * 
	 * @param aItem the item
	 * 
	 * @return the item id
	 */
	@Override
	protected String getItemId(Object aItem) {
		return ((IXMLTag)aItem).getAttribute(AppConstants.keyId);
	}

	/**
	 * Gets the item name, the key value of the tag.
	 * 
	 * @param aItem the item
	 * 
	 * @return the item name
	 */
	@Override
	protected String getItemName(Object aItem) {
		IXMLTag lTag = (IXMLTag)aItem;
		return getXmlManager().getTagKeyValues(lTag, lTag.getDefAttribute(AppConstants.defKeyKey));
	}

	/**
	 * Gets the item help.
	 *
	 * @param aItem the item
	 *
	 * @return the item help
	 */
	@Override
	protected String getItemHelp(Object aItem) {
		return "";
	}

}
