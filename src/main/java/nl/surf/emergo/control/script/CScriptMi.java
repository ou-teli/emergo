/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.script;

import java.util.HashMap;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Window;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.IXmlManager;
import nl.surf.emergo.control.CContentHelper;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefMenuitem;
import nl.surf.emergo.view.VView;

/**
 * The Class CScriptMi.
 *
 * Used to show script menu item and to handle clicking on it.
 */
public class CScriptMi extends CDefMenuitem {

	private static final long serialVersionUID = 157383210874528993L;
	
	protected CScriptLabel component = null;

	protected String type;
	
	protected String subtype;
	

	protected Component selectedComponent = null;
	
	protected String newnodetype = "";

	protected IXMLTag methodTag = null;

	protected String methodTagSubtype = "";

	protected Map<String,Object> params = new HashMap<String,Object>();

	/**
	 * Instantiates a new script menu item.
	 *
	 * @param aTarget the target right clicked
	 * @param aType the type of menu item
	 * @param aScriptTagSubtype the subtype of script tag
	 * @param aLabelKey the menu label key
	 * @param aHelpLabelKey the menu help label key
	 */
	public CScriptMi(Component aTarget, String aType, String aScriptTagSubtype, String aLabelKey, String aHelpLabelKey) {
		init(aTarget, aType, aScriptTagSubtype, CDesktopComponents.vView().getLabel(aLabelKey), CDesktopComponents.vView().getLabel(aHelpLabelKey));
	}

	/**
	 * Inits a new script menu item.
	 *
	 * @param aTarget the target right clicked
	 * @param aType the type of menu item
	 * @param aScriptTagSubtype the subtype of script tag
	 * @param aLabel the menu label
	 * @param aTooltiptext the a tool tip text
	 */
	private void init(Component aTarget, String aType, String aScriptTagSubtype, String aLabel, String aTooltiptext) {
		component = (CScriptLabel)aTarget;
		type = aType;
		subtype = aScriptTagSubtype;
		setLabel(aLabel);
		//NOTE setTooltip results in flickering, so use tool tip text. And replace possible <br/> by space. 
		setTooltiptext(aTooltiptext.replace("<br/>", " "));
	}

	/**
	 * On click, show appropriate dialog depending on type of the menu item,
	 * set within CScriptMenuPopup class. Either 'edit', 'delete' or equal to
	 * subtype of scripttag.
	 */
	public void onClick() {
		if (component == null)
			return;
		selectedComponent = component.getParent();
		newnodetype = (String)component.getAttribute("newnodetype");
		methodTag = (IXMLTag)component.getAttribute("tag");
		String lMethodTagType = (String)component.getAttribute("type");
		methodTagSubtype = (String)component.getAttribute("scripttagsubtype");
		String lMiType = type;
		boolean lIsMainCondition = (Boolean)component.getAttribute("ismaincondition");
		
		CScriptLabel lLabel = (CScriptLabel)component;
		lLabel.selectedComponent = selectedComponent;
		lLabel.newnodetype = newnodetype;
		lLabel.methodTag = methodTag;
		lLabel.methodTagSubtype = methodTagSubtype;
		lLabel.isMainCondition = lIsMainCondition;

		// use parent of lTarget, because target is within group
		if (lMiType.equals("edit")) {
			lLabel.typeOfAction = lMiType;
			doEditLabel(selectedComponent, newnodetype, methodTag, lMethodTagType, methodTagSubtype, lMiType, lIsMainCondition);
		}
		else {
			if (lMiType.equals("delete")) {
				lLabel.typeOfAction = lMiType;
				doDeleteLabel(selectedComponent, methodTag, lMethodTagType, methodTagSubtype, lMiType);
			}
			else {
				lLabel.typeOfAction = "new";
				doNewLabel(selectedComponent, newnodetype, methodTag, lMethodTagType, methodTagSubtype, lMiType, lIsMainCondition);
			}
		}
	}

	/**
	 * Handles creation of new label, method or logical operator, thereby modifying
	 * content of aSelectedTag.
	 *
	 * @param aSelectedComponent the selected component
	 * @param aNewnodetype the a new node type
	 * @param aMethodTag the method tag, empty
	 * @param aMethodTagType the method tag type
	 * @param aMethodTagSubtype the method tag sub type
	 * @param aMiType the mi type
	 * @param aIsMainCondition the is main condition
	 */
	public void doNewLabel(Component aSelectedComponent, String aNewnodetype, IXMLTag aMethodTag,
			String aMethodTagType, String aMethodTagSubtype, String aMiType, boolean aIsMainCondition) {
		CContentHelper cComponent = new CContentHelper(null);
		if (CContentHelper.isMethod(aMiType) || CContentHelper.isTemplateMethod(aMiType)) {
			// if method part within condition or action, show dialog to create new method
			params.put("item", null);
			params.put("new", "true");
			params.put("nodename", aMiType);
			params.put("subtype", aMethodTagSubtype);
			params.put("ismaincondition", aIsMainCondition);
			showPopup(VView.v_cde_s_script_method);
		} else {
			// if logical operator, create it immediately
			IXmlManager xmlManager = CDesktopComponents.sSpring().getXmlManager();
			IXMLTag lItem = xmlManager.newXMLTag(aMiType, "");
			cComponent.renderScriptLabelsVbox(aMethodTagSubtype, aSelectedComponent.getParent(),
					aSelectedComponent, lItem, aNewnodetype, false);
		}
	}

	/**
	 * Handles editing of existing method label thereby modifying content of aSelectedTag.
	 *
	 * @param aSelectedComponent the selected component
	 * @param aNewnodetype the a new node type
	 * @param aMethodTag the method tag, empty
	 * @param aMethodTagType the method tag type
	 * @param aMethodTagSubtype the method tag sub type
	 * @param aMiType the mi type
	 * @param aIsMainCondition the is main condition
	 */
	public void doEditLabel(Component aSelectedComponent, String aNewnodetype, IXMLTag aMethodTag,
			String aMethodTagType, String aMethodTagSubtype, String aMiType, boolean aIsMainCondition) {
		params.put("item", aMethodTag);
		params.put("new", "false");
		params.put("nodename", aMethodTagType);
		params.put("subtype", aMethodTagSubtype);
		params.put("ismaincondition", aIsMainCondition);
		showPopup(VView.v_cde_s_script_method);
	}

	/**
	 * Handles deleting of existing label, method or logical operator, thereby
	 * modifying content of aSelectedTag.
	 *
	 * @param aSelectedComponent the selected component
	 * @param aMethodTag the method tag, empty
	 * @param aMethodTagType the method tag type
	 * @param aMethodTagSubtype the method tag sub type
	 * @param aMiType the mi type
	 */
	public void doDeleteLabel(Component aSelectedComponent, IXMLTag aMethodTag,
			String aMethodTagType, String aMethodTagSubtype, String aMiType) {
		params.put("itemtype", CDesktopComponents.vView().getLabel("cde_s_script." + aMethodTagType));
		showPopup(VView.v_delete_item);
	}

	public Window showPopup(String aView) {
/*
 		Window lWindow = CDesktopComponents.vView().modalPopup(aView, null, params, "center");
		if (lWindow.getAttribute("item") != null) {
			component.handleItem(lWindow.getAttribute("item"));
		}
		else if (lWindow.getAttribute("items") != null) {
			component.handleItem(lWindow.getAttribute("item"));
		}
		else if (lWindow.getAttribute("ok") != null) {
 			component.deleteItem();
		}
		else if (lWindow.hasAttribute("item") || lWindow.hasAttribute("items") || lWindow.hasAttribute("ok")) {
			component.cancelItem();
		}
*/
 		Window lWindow = CDesktopComponents.vView().quasiModalPopup(aView, getRoot(), params, "center");
 		//NOTE set notifyComponent to component that is equal to the CScriptLabel instance.
 		//Menu items don't exist anymore if the quasi popup is shown.
 		lWindow.setAttribute("notifyComponent", component);
		return lWindow;
	}
	
}
