/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.script;

import java.util.Enumeration;
import java.util.Hashtable;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zk.ui.Component;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CContentHelper;
import nl.surf.emergo.control.CIObserved;
import nl.surf.emergo.control.def.CDefLabel;

/**
 * The Class CScriptLabel.
 *
 * Used to render condition or action as readable string on the screen.
 */
public class CScriptLabel extends CDefLabel {

	private static final long serialVersionUID = -5263260578587073154L;

	public String typeOfAction = "";
	
	public Component selectedComponent = null;
	
	public String newnodetype = "";

	public IXMLTag methodTag = null;

	public String methodTagSubtype = "";
	
	public boolean isMainCondition;

	/**
	 * Instantiates a new script label.
	 *
	 * @param aAttributes the attributes
	 * @param aLabelValue the label value
	 */
	public CScriptLabel(Hashtable<String,Object> aAttributes, String aLabelValue) {
		init(aAttributes, aLabelValue);
	}

	/**
	 * Inits script label.
	 * Saves four parameters as attributes.
	 * Set value to aLabelValue and enable context menu if appropriate.
	 *
	 * @param aAttributes the attributes
	 * @param aLabelValue the label value
	 */
	private void init(Hashtable<String,Object> aAttributes, String aLabelValue) {
		for (Enumeration<String> keys = aAttributes.keys(); keys.hasMoreElements();) {
			String key = keys.nextElement();
			setAttribute(key, aAttributes.get(key));
		}
		setAttribute("scripttagtype", aAttributes.get("scripttagtype"));
		setAttribute("scripttagsubtype", aAttributes.get("scripttagsubtype"));
		setAttribute("tag", aAttributes.get("tag"));
		setAttribute("type", aAttributes.get("type"));
		setValue(aLabelValue);
		setStyle("cursor:pointer;");
		if (!StringUtils.isEmpty((String)getAttribute("type"))) {
			// enable context menu
			setContext("scriptMenuPopup");
		}
	}
	
	/**
	 * On click show edit menu.
	 */
	public void onClick() {
		String lLabelType = (String)getAttribute("type");
		if (lLabelType == null || lLabelType.equals("")) {
			return;
		}
		CScriptMenuPopup lMenu = (CScriptMenuPopup)getFellowIfAny("scriptMenuPopup");
		if (lMenu != null) {
			lMenu.clearMenuitems();
			lMenu.open(this);
		}
	}
	
	@Override
	public void observedNotify(CIObserved aObserved, String aAction, Object aStatus) {
		if (aAction.equals("item")) {
			handleItem(aStatus);
		}
		else if (aAction.equals("items")) {
			handleItems(aStatus);
		}
		else if (aAction.equals("ok")) {
			deleteItem();
		}
		else if (aAction.equals("cancel")) {
			cancelItem();
		}
	}

	protected void handleItem(Object aStatus) {
		CContentHelper contentHelper = new CContentHelper(null);
		if (typeOfAction.equals("new")) {
			//render new method as label
			contentHelper.renderScriptLabelsVbox(methodTagSubtype, selectedComponent.getParent(),
					selectedComponent, (IXMLTag)aStatus, newnodetype, isMainCondition);
		}
		else if (typeOfAction.equals("edit")) {
			// Inserting item and deleting old item is more easy than trying to update one item.
			// render new method as label
			contentHelper.renderScriptLabelsVbox(methodTagSubtype, selectedComponent.getParent(),
					selectedComponent, (IXMLTag)aStatus, newnodetype, isMainCondition);
			selectedComponent.detach();
		}
	}

	protected void handleItems(Object aStatus) {
	}

	protected void deleteItem() {
		CContentHelper contentHelper = new CContentHelper(null);
		contentHelper.deleteScriptLabel(selectedComponent.getParent(), selectedComponent, methodTag);
		selectedComponent.detach();
	}

	protected void cancelItem() {
	}

}
