/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.script;

import nl.surf.emergo.control.CCombo;
import nl.surf.emergo.domain.IECaseRole;

/**
 * The Class CScriptCarCombo.
 * 
 * Used as combobox for case roles, used within the CScriptMethodHbox class.
 */
public class CScriptCarCombo extends CCombo {

	private static final long serialVersionUID = 7114402752589570194L;

	/**
	 * Instantiates a new script case role combobox.
	 * 
	 * @param aId the id
	 */
	public CScriptCarCombo(String aId) {
		super(aId, "caserole.");
	}

	/**
	 * Gets the item id, the db carid of the case role.
	 * 
	 * @param aItem the item
	 * 
	 * @return the item id
	 */
	@Override
	protected String getItemId(Object aItem) {
		return "" + ((IECaseRole)aItem).getCarId();
	}

	/**
	 * Gets the item name, the name of the case role.
	 * 
	 * @param aItem the item
	 * 
	 * @return the item name
	 */
	@Override
	protected String getItemName(Object aItem) {
		return ((IECaseRole)aItem).getName();
	}

	/**
	 * Gets the item help.
	 *
	 * @param aItem the item
	 *
	 * @return the item help
	 */
	@Override
	protected String getItemHelp(Object aItem) {
		return "";
	}

}
