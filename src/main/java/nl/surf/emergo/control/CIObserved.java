/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

/**
 * The Interface CIObserved.
 * 
 * Is used to implement Observer design pattern.
 * CIObserved is the class being observed. It must know its observers to notify them
 * when something happens.
 * 
 * Within ZK framework every object on a application page gets an unique id. You can
 * also set this id yourself, but it must stay unique.
 * This id should be used within methods registerObserver and
 * removeObserver. We use id's instead of objects themselfs, because an class can have
 * multiple instances (still waiting for garbadge collection) but ZK only knows one of
 * them by id. That's the one we need.
 */
public interface CIObserved {
	
	/**
	 * Registers an observer by id string.
	 * 
	 * @param aObserverId the observer id
	 */
	public void registerObserver(String aObserverId);

	/**
	 * Removes an observer by id string.
	 * 
	 * @param aObserverId the observer id
	 */
	public void removeObserver(String aObserverId);

	/**
	 * Notifies observers about some action with possibly some action status.
	 * 
	 * @param aAction the action
	 * @param aStatus the status object, can be String or other class
	 */
	public void notifyObservers(String aAction, Object aStatus);
}
