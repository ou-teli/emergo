/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zk.ui.AbstractComponent;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Treechildren;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.Window;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.def.CDefTree;
import nl.surf.emergo.control.def.CDefTreechildren;

/**
 * The Class CTree.
 *
 * Ancestor of all tree controls within app, author as well as player environment.
 */
public class CTree extends CDefTree {

	private static final long serialVersionUID = 2812300174671791348L;
	
	public String typeOfAction = "";
	
	public Component parent;
	
	public Component contentItem;
	
	public Object selectedItem;
	
	public String newNodeType;

	//NOTE use properties that have to be both available within onDrop and observedNotify methods  
	protected Component dragged;

	protected Component dropped;

	protected IXMLTag draggedTag;

	protected IXMLTag droppedTag;
	
	protected boolean copy;
	
	protected List<IXMLTag> copiedTags;
	
	protected List<String> dropTypes;
	
	protected CContentHelper contentHelper = new CContentHelper(this);
	
	protected boolean saveInDb;
	
	protected String dropType;
	
	protected Map<String,Object> params = new HashMap<String,Object>();

	/**
	 * On open. Is called by CTreeitem when contentitem is opened, so onOpen is managed by tree just like onSelect.
	 * Added for polymorphism.
	 *
	 * @param aContentItem the contentitem
	 * @param aOpen the open state
	 */
	public void onOpen(Component aContentItem, boolean aOpen) {
	}

	/**
	 * On select.
	 * Added for polymorphism.
	 */
	public void onSelect() {
	}

	/**
	 * On drop, move or copy a contentitem within the tree, and move or copy the corresponding
	 * xml tag within the xml tag tree.
	 * On error or if dropping not possible do nothing
	 *
	 * @param aDragged the dragged contentitem
	 * @param aDropped the contentitem dropped on
	 * @param aCopy the copy state
	 */
	public void onDrop(Component aDragged, Component aDropped, boolean aCopy) {
		dragged = aDragged;
		dropped = aDropped;
		copy = aCopy;
		draggedTag = (IXMLTag) aDragged.getAttribute("item");
		droppedTag = (IXMLTag) aDropped.getAttribute("item");
		copiedTags = new ArrayList<IXMLTag>(0);
		dropTypes = new ArrayList<String>(0);
		saveInDb = true;
		
		typeOfAction = "drop";
		// do move or copy xml tag within xml tag tree
		if (handleDrop()) {
			renderDrop();
		}
	}

	/**
	 * Drops (copies or replaces) xml node tag within xml data of current case component.
	 * If copy is true copiedTags is filled with the copied tags (until now only 1)
	 * and dropTypes with the drop types of the copied tags. So the calling method can
	 * render these new tags on screen too.
	 *
	 * @return boolean
	 */
	public boolean handleDrop() {
		dropType = CDesktopComponents.sSpring().getXmlManager().getDropType(draggedTag, droppedTag, new ArrayList<String[]>(0));
		if (dropType.equals("" + AppConstants.siblingOrChildDrop)) {
			//let user choose drop type
			showPopup(contentHelper.getDropItemAsSiblingOrChildView());
			//NOTE because modal windows are not allowed anymore drop handling is postponed within method handleItem.
			return false;
		}
		else {
			//drop type is obvious, either sibling or child
			dropType = "";
		}
		return saveDrop();
	}

	/**
	 * Saves dropped xml node tag within xml data of current case component.
	 * If copy is true copiedTags is filled with the copied tags (until now only 1)
	 * and dropTypes with the drop types of the copied tags.
	 *
	 * @return boolean
	 */
	protected boolean saveDrop() {
		IXMLTag lRootTag = contentHelper.getXmlDataTree();
		List<String[]> lErrors = new ArrayList<String[]>(0);
		CDesktopComponents.sSpring().getXmlManager().dropNode(lRootTag, draggedTag, droppedTag, copy, dropType, copiedTags, dropTypes, lErrors);
		String lXmlData = CDesktopComponents.sSpring().getXmlManager().xmlTreeToDoc(lRootTag);
		if (!CDesktopComponents.sSpring().getXmlManager().isXMLValid(lXmlData)) {
			contentHelper.appManager.addError(lErrors, "tag", "drop_invalid_xml");
		}
		if (lErrors.size() == 0) {
			if (saveInDb) {
				contentHelper.setXmlData(lXmlData);
			}
			return true;
		}
		else {
			// if errors show them on target aDropped
			CDesktopComponents.cControl().showErrors((AbstractComponent)dropped, lErrors);
			return false;
		}
	}

	protected void renderDrop() {
		String lDropType = "" + AppConstants.noDrop;
		if (dropTypes.size() == 1) {
			lDropType = (String) dropTypes.get(0);
		}
		if (lDropType.equals("" + AppConstants.noDrop))
			// no dropping possible or allowed
			return;

		if (copy) {
			// copy aDragged
			List<Component> lDraggedContentItems = contentHelper.xmlChildsToContentItems(copiedTags, dropped.getParent().getParent(), dropped);
			dragged = (Component) lDraggedContentItems.get(0);
		}
		else {
			// move aDragged by changing its parent
			dropped.getParent().insertBefore(dragged, dropped);
		}
		if (lDropType.equals("" + AppConstants.childDrop)) {
			// if dropped as child set parent of aDragged
			Treechildren lTreechildren = ((Treeitem)dropped).getTreechildren();
			if (lTreechildren == null) {
				lTreechildren = new CDefTreechildren();
				dropped.appendChild(lTreechildren);
			}
			dragged.setParent(lTreechildren);
		}
		boolean lSingleopen = (draggedTag.getDefAttribute(AppConstants.defKeySingleopen).equals(AppConstants.statusValueTrue));
		if (!copy && lSingleopen) {
			// if 'cut/paste' and single open tag, re-render dragged tree item, because value of opened can be affected
			IXMLTag lRootTag = contentHelper.getXmlDataTree();
			// get dragged tag again by id, because status of lDraggedTag itself is not affected by code above
			draggedTag = CDesktopComponents.sSpring().getXmlManager().getTagById(lRootTag, draggedTag.getAttribute(AppConstants.keyId));
			contentHelper.reRenderTreeitem(dragged, draggedTag);
		}
	}

	public Window showPopup(String aView) {
/*
 		Window lWindow = CDesktopComponents.vView().modalPopup(aView, null, params, "center");
		if (lWindow.getAttribute("item") != null) {
			handleItem(lWindow.getAttribute("item"));
		}
		else if (lWindow.getAttribute("items") != null) {
			handleItem(lWindow.getAttribute("item"));
		}
		else if (lWindow.getAttribute("ok") != null) {
 			deleteItem();
		}
		else if (lWindow.hasAttribute("item") || lWindow.hasAttribute("items") || lWindow.hasAttribute("ok")) {
			cancelItem();
		}
*/
 		Window lWindow = CDesktopComponents.vView().quasiModalPopup(aView, getRoot(), params, "center");
 		lWindow.setAttribute("notifyComponent", this);
		return lWindow;
	}

	@Override
	public void observedNotify(CIObserved aObserved, String aAction, Object aStatus) {
		if (aAction.equals("item")) {
			handleItem(aStatus);
		}
		else if (aAction.equals("items")) {
			handleItems(aStatus);
		}
		else if (aAction.equals("ok")) {
			deleteItem();
		}
		else if (aAction.equals("cancel")) {
			cancelItem();
		}
	}

	protected void handleItem(Object aStatus) {
		if (typeOfAction.equals("new")) {
			contentHelper.newContentItem(parent, contentItem, (IXMLTag)selectedItem, (IXMLTag)aStatus, newNodeType);
		}
		else if (typeOfAction.equals("edit")) {
//			Inserting item and deleting old item is more easy than trying to update one item.
			if (contentItem.getParent() == null) {
				contentItem = getContentItem(((IXMLTag)aStatus).getAttribute(AppConstants.keyId));
			}
			if (contentItem != null) {
				contentHelper.editContentItem(contentItem.getParent(), contentItem, (IXMLTag)aStatus);
			}
		}
		else if (typeOfAction.equals("drop")) {
			String lItem = (String)aStatus;
			if (StringUtils.isEmpty(lItem)) {
				return;
			}
			if (lItem.equals("sibling")) {
				dropType = "" + AppConstants.siblingDrop;
			}
			else if (lItem.equals("child")) {
				dropType = "" + AppConstants.childDrop;
			}
			else {
				dropType = "";
			}
			if (dropType.length() > 0 && saveDrop()) {
				renderDrop();
			}
		}
	}

	protected void handleItems(Object aStatus) {
	}

	protected void deleteItem() {
		//NOTE comment next line and uncomment lines after that if temporarily multiple tree items have to be deleted.
		contentHelper.deleteContentItem(contentItem, true);
/*			Set<Treeitem> lTreeitemSet = getSelectedItems();
		Object[] lTreeitems = lTreeitemSet.toArray(); 
		for (int i=(lTreeitems.length - 1);i>=0;i--) {
			contentHelper.deleteContentItem((Component)lTreeitems[i], true);
		} */
	}

	protected void cancelItem() {
	}

	/**
	 * Called when contentitem is clicked.
	 * Added for polymorphism.
	 *
	 * @param aClickedComponent the clicked component within the contentitem
	 *
	 * @return the component
	 */
	public Component contentItemClicked(Component aClickedComponent) {
		Treeitem lTreeitem = getContentItemClicked(aClickedComponent);
		CContentItemMenuPopup lMenu = (CContentItemMenuPopup)getFellowIfAny("menuPopup");
		if (lTreeitem != null && lMenu != null) {
			lMenu.clearMenuitems();
			lMenu.open(lTreeitem);
		}
		return null;
	}

	/**
	 * Called when contentitem must get state finished.
	 * Added for polymorphism.
	 *
	 * @param aContentItem the contentitem
	 * @param aClickedComponent the clicked component within the contentitem
	 * @param aValue the value needed for the finish action
	 */
	public void contentItemFinished(Component aContentItem, Component aClickedComponent, String aValue) {
	}

	/**
	 * Gets the contentitem clicked.
	 *
	 * @param aClickedTreeComponent the clicked component within the contentitem
	 *
	 * @return the contentitem clicked
	 */
	public Treeitem getContentItemClicked(Component aClickedTreeComponent) {
		Component lParent = aClickedTreeComponent.getParent();
		while (!(lParent instanceof Treeitem))
			lParent = lParent.getParent();
		return ((Treeitem) lParent);
	}

	/**
	 * Gets the xml tag referenced within the contentitem.
	 *
	 * @param aContentItem the contentitem
	 *
	 * @return the contentitem xml tag
	 */
	public IXMLTag getContentItemTag(Component aContentItem) {
		if (aContentItem == null)
			return null;
		return ((IXMLTag) aContentItem.getAttribute("item"));
	}

	/**
	 * Gets the contentitem with xml tag given by aTagId.
	 *
	 * @param aTagId the tag id
	 *
	 * @return the contentitem
	 */
	public Treeitem getContentItem(String aTagId) {
		return (Treeitem)getContentItem(this,aTagId);
	}

	/**
	 * Gets the contentitem with xml tag given by aTagId and parent component by aParent. Nested.
	 *
	 * @param aParent the parent component
	 * @param aTagId the tag id
	 *
	 * @return the contentitem
	 */
	protected Component getContentItem(Component aParent,String aTagId) {
		for (Component lComponent : aParent.getChildren()) {
			if (lComponent instanceof Treeitem) {
				IXMLTag lTag = getContentItemTag((Component)lComponent);
				if ((lTag != null) && (lTag.getAttribute(AppConstants.keyId).equals(aTagId)))
					return (Component)lComponent;
			}
			// nesting
			Component lContentItem = getContentItem(lComponent,aTagId);
			if (lContentItem != null)
				return lContentItem;
		}
		return null;
	}

}
