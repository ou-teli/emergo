/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.stu;

import java.util.List;

import org.zkoss.zul.Listitem;

import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefListbox;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.domain.IERunTeam;

/**
 * The Class CStuRunGroupAccountCaseRoleComboOpen.
 */
public class CStuRunGroupAccountCaseRoleComboOpen extends CDefListbox {

	private static final long serialVersionUID = -8916574421325919350L;

	/** The run id. */
	protected int runId = 0;

	/** The s spring. */
	protected List<IERunGroupAccount> rungroupaccounts = null;
	
	/**
	 * Instantiates a new case role combo for an open access run.
	 * Show possible case roles.
	 * 
	 * @param aId the a id
	 * @param aRunId the a run id
	 * @param aCaseRoles the case roles for this run
	 * @param aRunGroupAccounts the run group accounts for the current account
	 */
	public CStuRunGroupAccountCaseRoleComboOpen(String aId,int aRunId,List<IECaseRole> aCaseRoles, List<IERunGroupAccount> aRunGroupAccounts) {
		setRows(1);
		setMold("select");
		if (!aId.equals(""))
			setId(aId);
		runId = aRunId;
		rungroupaccounts = aRunGroupAccounts;
		Listitem lListitem = super.insertListitem(null,CDesktopComponents.vView().getLabel("stu_runs.combobox.item.rgaId.0"),null);
		setSelectedItem(lListitem);
		for (IECaseRole lObject : aCaseRoles) {
			lListitem = super.insertListitem(lObject,lObject.getName(),null);
		}
	}

	/**
	 * On select update run team combobox and redirect button, with selected
	 * case role and run team.
	 */
	public void onSelect() {
		IECaseRole lCaseRole = (IECaseRole)getSelectedItem().getValue();
		boolean lCaseRoleSelected = (lCaseRole != null);
		CStuRunGroupAccountRunTeamCombo lRunTeamCombo = (CStuRunGroupAccountRunTeamCombo)getFellowIfAny("runsLbRunTeamCombo"+runId);
		IERunTeam lRunTeam = null;
		IERunGroupAccount lRuAc = null;
		if (lCaseRoleSelected) {
			int lCarId = lCaseRole.getCarId();
			for (IERunGroupAccount lObject : rungroupaccounts) {
				IERunGroup lRug = lObject.getERunGroup();
				if (lRug.getECaseRole().getCarId() == lCarId)
					lRuAc = lObject;
			}
		}
		boolean lRunTeamSelected = true;
		if (lRunTeamCombo != null) {
			// if caserole selected enable runteam combo
			// if caserole not selected disable runteam combo
			lRunTeamCombo.setDisabled(!lCaseRoleSelected);
			lRunTeamCombo.setRunGroupAccount(lRuAc);
			if (lRunTeamCombo.getSelectedItem() != null)
				lRunTeam = (IERunTeam)lRunTeamCombo.getSelectedItem().getValue();
			lRunTeamSelected = lRunTeamCombo.getSelectedState();
		}
		CStuRunGroupAccountRedirectBtn lRunRedirectButton = (CStuRunGroupAccountRedirectBtn)getFellowIfAny("runsLbRedirectButton"+runId);
		lRunRedirectButton.setDisabled((!lCaseRoleSelected) || (!lRunTeamSelected));
		if (lCaseRole != null) {
			lRunRedirectButton.setCarId(lCaseRole.getCarId());
		}
		// first time starting open run: account not yet linked to run group - will be done when opening case, so lRuAc and lRunTeam will be null
		if (lRuAc != null)
			lRunRedirectButton.setRgaId(lRuAc.getRgaId());
		if (lRunTeam != null)
			lRunRedirectButton.setRutId(lRunTeam.getRutId());
	}
}
