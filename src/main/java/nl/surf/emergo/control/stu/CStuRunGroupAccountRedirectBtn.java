/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.stu;

import org.zkoss.zk.ui.event.Event;

import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.COpenWebPageUsingJavascriptBtn;
import nl.surf.emergo.view.VView;

/**
 * The Class CStuRunGroupAccountRedirectBtn. When clicked it opens the Emergo player
 * in a new browser instance for a particular run group account, possibly within in a particular run team.
 */
public class CStuRunGroupAccountRedirectBtn extends COpenWebPageUsingJavascriptBtn {

	private static final long serialVersionUID = 6504979904455595062L;

	/** The run id. */
	protected int runId = 0;

	/** The case skin path. */
	protected String caseSkinPath = "";

	/** The acc id. */
	protected int accId = 0;

	/** The rga id. */
	protected int rgaId = 0;

	/** The rut id. */
	protected int rutId = 0;

	/** The car id. */
	protected int carId = 0;

	/** The should be in team. */
	protected boolean shouldBeInTeam = false;

	protected String urlToRedirectTo = "";

	/**
	 * Instantiates a new c stu run group account redirect btn.
	 *
	 * @param aId the a id
	 * @param aRunId the a run id
	 * @param aCaseSkinPath the a case skin path
	 */
	public CStuRunGroupAccountRedirectBtn(String aId,int aRunId,String aCaseSkinPath) {
		if (!aId.equals(""))
			setId(aId);
		runId = aRunId;
		caseSkinPath = aCaseSkinPath;
	}

	/**
	 * Sets the rga id and creates appropriate client action.
	 *
	 * @param aRgaId the rga id
	 */
	public void setRgaId(int aRgaId) {
		rgaId = aRgaId;
		createAction();
	}

	/**
	 * Sets the rut id and creates appropriate client action.
	 *
	 * @param aRutId the rut id
	 */
	public void setRutId(int aRutId) {
		rutId = aRutId;
		createAction();
	}

	/**
	 * Sets the should be in team and creates appropriate client action.
	 *
	 * @param aShouldBeInTeam the new should be in team
	 */
	public void setShouldBeInTeam(boolean aShouldBeInTeam) {
		shouldBeInTeam = aShouldBeInTeam;
		createAction();
	}

	/**
	 * Sets the acc id and creates appropriate client action.
	 *
	 * @param aAccId the acc id
	 */
	public void setAccId(int aAccId) {
		accId = aAccId;
		createAction();
	}

	/**
	 * Sets the car id and creates appropriate client action.
	 *
	 * @param aCarId the car id
	 */
	public void setCarId(int aCarId) {
		carId = aCarId;
		createAction();
	}

	/**
	 * Creates client action to open Emergo player in new browser instance for
	 * a run group account, possibly within a run team.
	 */
	private void createAction() {
		String lParams =
			"&cacId=0"+
			"&tagId=0"+
			"&runstatus="+AppConstants.runStatusRun+
			"&rgaId="+rgaId+
			"&rutId="+rutId+
			"&runId="+runId+
			"&accId="+accId+
			"&carId="+carId+
			CDesktopComponents.cControl().getReqLangParams();

		if (shouldBeInTeam && rutId == 0) {
			javascriptOnClickAction = "alert('" + CDesktopComponents.vView().getLabel("alert.notinteam") + "');";
		}
		else {
			String lUrl = CDesktopComponents.vView().uniqueView(caseSkinPath + VView.v_run);
			urlToRedirectTo = CDesktopComponents.vView().getAbsoluteUrl(lUrl + lParams);
		}
	}
	
	public void onClick(Event aEvent) {
		if (!isDoubleClick()) {
			CDesktopComponents.vView().redirectToView(urlToRedirectTo);
		}
	}

}
