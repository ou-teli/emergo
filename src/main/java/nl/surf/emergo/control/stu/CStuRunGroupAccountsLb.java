/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.stu;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import org.zkoss.zul.Hbox;

import nl.surf.emergo.business.IRunAccountManager;
import nl.surf.emergo.business.IRunGroupManager;
import nl.surf.emergo.business.IRunManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefListbox;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunAccount;
import nl.surf.emergo.domain.IERunGroup;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.domain.IERunTeam;

/**
 * The Class CStuRunGroupAccountsLb.
 */
public class CStuRunGroupAccountsLb extends CDefListbox {

	private static final long serialVersionUID = 287844948174655550L;

	/** The free accessible runs list. */
	protected List<IERun> openRuns = null;

	/** The free accessible run ids list. */
	protected List<Integer> openRunIds = new ArrayList<Integer>();

	protected class HashtableSortByRunName implements Comparator<Hashtable<String,Object>>{
		public int compare(Hashtable<String,Object> o1, Hashtable<String,Object> o2) {
			return ((IERun)o1.get("item")).getName().compareTo(((IERun)o2.get("item")).getName());
		}
	}
	
	/**
	 * On create render listitems.
	 */
	public void onCreate() {
		getItems().clear();
		CStuRunGroupAccountsVbox vbox = (CStuRunGroupAccountsVbox)getParent();
		if (vbox == null)
			return;
		List<IECase> lCases = new ArrayList<IECase>();
		List<Integer> lRunIds = new ArrayList<Integer>();
		List<Hashtable<String,Object>> lStuItems = new ArrayList<Hashtable<String,Object>>(0);
		int lAccId = vbox.getAccount().getAccId();
		IRunAccountManager lRunAccManager = (IRunAccountManager)CDesktopComponents.sSpring().getBean("runAccountManager");
		for (IERunGroupAccount lRunGroupAccount : vbox.getRgas()) {
			IERun lRun = lRunGroupAccount.getERunGroup().getERun();
			int lRunId = lRun.getRunId();
			if (checkRun (lRunId) && !lRunIds.contains(lRunId)) {
				lRunIds.add(lRunId);
				if ((isRunning(lRun)) && (lRun.getStatus() == AppConstants.run_status_runnable)) {
					List<IERunGroupAccount> lRunGroupAccounts = new ArrayList<IERunGroupAccount>(0);
					IERunAccount lRuAcc = lRunAccManager.getRunAccountByRunIdAccId(lRunId, lAccId);
					// runaccount exists for this run, must be active as student 
					if ((lRuAcc != null) && lRuAcc.getStuactive()) {
						if (lRun.getOpenaccess()) {
							if (vbox.getAccount().getOpenaccess()) {
								// acccount is allowed to view open runs
								lRunGroupAccounts = getOpenRunGroupAccounts(lRunId,vbox.getRgas());
							}
						} else {
							lRunGroupAccounts = getRunGroupAccounts(lRunId,vbox.getRgas());
						}
					}
					if (lRunGroupAccounts.size() > 0) {
						if (!lCases.contains(lRun.getECase()))
							lCases.add(lRun.getECase());
						Hashtable<String,Object> lStuItem = new Hashtable<String,Object>(0);
						lStuItem.put("item",lRun);
						lStuItem.put("account",vbox.getAccount());
						lStuItem.put("rungroupaccounts",lRunGroupAccounts);
						lStuItem.put("runteams",vbox.getRgaHelper().getRunTeams(lRunGroupAccounts));
						lStuItems.add(lStuItem);
					}
				}
			}
		}
		if (vbox.getAccount().getOpenaccess()) {
			List<IERun> lOpenRuns = getOpenRunsIfApplicable();
			for (IERun lOpenRun : lOpenRuns) {
				int lRunId = lOpenRun.getRunId();
				if (!lRunIds.contains(lRunId)) {
					if (isRunning(lOpenRun)) {
						// runaccount doesn't exist for this run or is active as tutor; in both cases open access run can be started 
						if (!lCases.contains(lOpenRun.getECase()))
							lCases.add(lOpenRun.getECase());
						Hashtable<String,Object> lStuItem = new Hashtable<String,Object>(0);
						lStuItem.put("item",lOpenRun);
						lStuItem.put("account",vbox.getAccount());
						lStuItem.put("rungroupaccounts",new ArrayList<IERunGroupAccount>(0));
						lStuItem.put("runteams",new ArrayList<IERunTeam>(0));
						lStuItems.add(lStuItem);
					}
				}
			}
		}
		Collections.sort(lStuItems, new HashtableSortByRunName());
		
		vbox.getRgaHelper().setCases(lCases);
		for (Hashtable<String,Object> lStuItem : lStuItems) {
			vbox.getRgaHelper().renderItem(this, null, lStuItem);
		}
		boolean lVisible = !((lStuItems == null) || lStuItems.isEmpty());
		setVisible (lVisible);
		Hbox lLabel = (Hbox)getFellowIfAny(getLabelBoxId());
		if (lLabel != null)
			lLabel.setVisible(lVisible);

		restoreSettings();
	}

	/**
	 * Checks if aRun is running.
	 *
	 * @param aRun the a run
	 *
	 * @return true, if is running
	 */
	private boolean isRunning(IERun aRun) {
		//student can only access active runs
		if (!aRun.getActive())
			return false;
		boolean lRunning = true;
		Date lCurrentDate = new Date();
		Date lStartDate = aRun.getStartdate();
		//NOTE end date is not checked, it is only an indication. Students should be able to open the case after the end date.
		if ((lStartDate != null) && (lStartDate.after(lCurrentDate)))
			lRunning = false;
		return lRunning;
	}

	/**
	 * Gets the active run group accounts out of aRunGroupAccounts for aRunId.
	 *
	 * @param aRunId the a run id
	 * @param aRunGroupAccounts the a run group accounts
	 *
	 * @return the run group accounts
	 */
	private List<IERunGroupAccount> getRunGroupAccounts(int aRunId, List<IERunGroupAccount> aRunGroupAccounts) {
		List<IERunGroupAccount> lRunGroupAccounts = new ArrayList<IERunGroupAccount>(0);
		for (IERunGroupAccount lRunGroupAccount : aRunGroupAccounts) {
			IERunGroup lRunGroup = (IERunGroup)lRunGroupAccount.getERunGroup();
			if ((lRunGroup.getActive()) && (lRunGroup.getERun().getRunId() == aRunId)) {
				lRunGroupAccounts.add(lRunGroupAccount);
			}
		}
		return lRunGroupAccounts;
	}

	/**
	 * Gets the run group accounts out of aRunGroupAccounts for open run with id aRunId.
	 * If run group is not active, activate run group (for it is an open run)
	 *
	 * @param aRunId the a run id
	 * @param aRunGroupAccounts the a run group accounts
	 *
	 * @return the run group accounts
	 */
	private List<IERunGroupAccount> getOpenRunGroupAccounts(int aRunId,List<IERunGroupAccount> aRunGroupAccounts) {
		List<IERunGroupAccount> lRunGroupAccounts = new ArrayList<IERunGroupAccount>(0);
		for (IERunGroupAccount lRunGroupAccount : aRunGroupAccounts) {
			IERunGroup lRunGroup = (IERunGroup)lRunGroupAccount.getERunGroup();
			if (lRunGroup.getERun().getRunId() == aRunId) {
				if (!lRunGroup.getActive()) {
					lRunGroup.setActive(true);
					((IRunGroupManager)CDesktopComponents.sSpring().getBean("runGroupManager")).updateRunGroup(lRunGroup);
				}
				lRunGroupAccounts.add(lRunGroupAccount);
			}
		}
		return lRunGroupAccounts;
	}

	/**
	 * Checks if run is applicable for this listbox.
	 *
	 * @param aRunId the run id
	 *
	 * @return true if applicable, false if not
	 */
	protected boolean checkRun (int aRunId) {
		return true;
	}

	/**
	 * Returns all free accessible runs.
	 *
	 * @return the run list
	 */
	protected List<IERun> getOpenRuns() {
		if (openRuns == null) {
			openRuns = ((IRunManager)CDesktopComponents.sSpring().getBean("runManager")).getAllRunnableOpenAccessRuns();
		}
		return openRuns;
	}

	/**
	 * Returns all free accessible runs if applicable.
	 *
	 * @return the run list
	 */
	protected List<IERun> getOpenRunsIfApplicable () {
		return getOpenRuns();
	}

	/**
	 * Returns all free accessible run ids.
	 *
	 * @return the run ids list
	 */
	protected List<Integer> getOpenRunIds () {
		if ((openRunIds.isEmpty()) && (!getOpenRuns().isEmpty())) {
			for (IERun lOpenRun : getOpenRuns()) {
				int lRunId = lOpenRun.getRunId();
				openRunIds.add(lRunId);
			}
		}
		return openRunIds;
	}

	/**
	 * Returns id of label textbox.
	 *
	 * @return the label id
	 */
	protected String getLabelBoxId () {
		return "";
	}

}
