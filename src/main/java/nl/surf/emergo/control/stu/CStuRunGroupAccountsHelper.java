/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.stu;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import org.zkoss.zul.Html;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;

import nl.surf.emergo.business.ICaseRoleManager;
import nl.surf.emergo.business.IRunTeamManager;
import nl.surf.emergo.business.IRunTeamRunGroupManager;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefHelper;
import nl.surf.emergo.control.def.CDefListcell;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IECaseRole;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.domain.IERunGroupAccount;
import nl.surf.emergo.domain.IERunTeam;
import nl.surf.emergo.domain.IERunTeamRunGroup;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpring;

/**
 * The Class CStuRunGroupAccountsHelper.
 */
public class CStuRunGroupAccountsHelper extends CDefHelper {

	/** The cases. */
	protected List<IECase> cases = null;

	/** The casecomponents. */
	protected List<IECaseComponent> casecomponents = null;

	/**
	 * Instantiates a new CAdmRunGroupAccountsHelper.
	 */
	public CStuRunGroupAccountsHelper(VView aView, SSpring aSpring) {
	}

	/**
	 * Sets the cases.
	 *
	 * @param aCases the new cases
	 */
	public void setCases(List<IECase> aCases) {
		cases = aCases;
	}

	/**
	 * Renders one account and buttons and combo boxes to act on the account.
	 *
	 * @param aListbox the a listbox
	 * @param aInsertBefore the a insert before
	 * @param aItem the a item
	 */
	public void renderItem(Listbox aListbox,Listitem aInsertBefore,Hashtable<String,Object> aItem) {
		Listitem lListitem = super.newListitem();
		List<IERunGroupAccount> lRunGroupAccounts = (List)aItem.get("rungroupaccounts");
		List<IERunTeam> lRunTeams = (List)aItem.get("runteams");
		IERun lItem = (IERun)aItem.get("item");
		lListitem.setValue(lItem);
		super.appendListcell(lListitem,lItem.getName());
		String lName = lItem.getECase().getName();
		super.appendListcell(lListitem,lName);
		super.appendListcell(lListitem,getDateStrAsYMD(lItem.getStartdate()));
		super.appendListcell(lListitem,getDateStrAsYMD(lItem.getEnddate()));
		Listcell lListcell = new CDefListcell();
		boolean lShowCaseRoleCombo = false;
		boolean lOpenRun = lItem.getOpenaccess();
		List<IECaseRole> lCaseRoles = new ArrayList<IECaseRole>();
		if (lOpenRun) {
			ICaseRoleManager lCaseRoleManager = (ICaseRoleManager)CDesktopComponents.sSpring().getBean("caseRoleManager");
			lCaseRoles = lCaseRoleManager.getAllCaseRolesByCasId(lItem.getECase().getCasId(), false);
			lShowCaseRoleCombo = (lCaseRoles.size() > 1);
			if (lShowCaseRoleCombo) {
				CStuRunGroupAccountCaseRoleComboOpen lCaseRoleCombo = new CStuRunGroupAccountCaseRoleComboOpen("runsLbCaseRoleCombo"+lItem.getRunId(),lItem.getRunId(),lCaseRoles,lRunGroupAccounts);
				lListcell.appendChild(lCaseRoleCombo);
				lListcell.appendChild(new Html("<br/>"));
			}
		} else {
			lShowCaseRoleCombo = (lRunGroupAccounts.size() > 1);
			if (lShowCaseRoleCombo) {
				CStuRunGroupAccountCaseRoleCombo lCaseRoleCombo = new CStuRunGroupAccountCaseRoleCombo("runsLbCaseRoleCombo"+lItem.getRunId(),lItem.getRunId(),lRunGroupAccounts);
				lListcell.appendChild(lCaseRoleCombo);
				lListcell.appendChild(new Html("<br/>"));
			}
		}

		boolean lShowRunTeamCombo = ((lShowCaseRoleCombo && (lRunTeams.size() == 1)) || (lRunTeams.size() > 1));
		if (lShowRunTeamCombo) {
			CStuRunGroupAccountRunTeamCombo lRunTeamCombo = new CStuRunGroupAccountRunTeamCombo("runsLbRunTeamCombo"+lItem.getRunId(),lItem,lRunTeams,this);
			lRunTeamCombo.setDisabled(lShowCaseRoleCombo);
			if (lRunGroupAccounts.size() == 1)
				lRunTeamCombo.setRunGroupAccount((IERunGroupAccount)lRunGroupAccounts.get(0));
			lListcell.appendChild(lRunTeamCombo);
			lListcell.appendChild(new Html("<br/>"));
		}

		List<IECase> lCases = cases;
		if (lCases == null) {
			lCases = new ArrayList<IECase>();
			lCases.add(lItem.getECase());
		}
		if (casecomponents == null)
			casecomponents = CDesktopComponents.sSpring().getCaseComponentsByStatusType(lCases, AppConstants.statusTypeRunTeam);
		List<IECaseComponent> lCaseComponents = new ArrayList<IECaseComponent>();
		for (IECaseComponent lCaseComponent : casecomponents) {
			if (lCaseComponent.getECase().getCasId() == lItem.getECase().getCasId())
				lCaseComponents.add(lCaseComponent);
		}
		boolean lShouldBeInTeam = (lCaseComponents.size() > 0);

		CStuRunGroupAccountRedirectBtn lRedirect = new CStuRunGroupAccountRedirectBtn("runsLbRedirectButton"+lItem.getRunId(),lItem.getRunId(), CDesktopComponents.sSpring().getSCaseSkinHelper().getCaseSkinPath(lItem));
		lRedirect.setLabel(CDesktopComponents.vView().getLabel("open_case"));
		lRedirect.setDisabled(lShowCaseRoleCombo || lShowRunTeamCombo);
		lRedirect.setShouldBeInTeam(lShouldBeInTeam);
		if (lOpenRun) {
			lRedirect.setAccId(((IEAccount)aItem.get("account")).getAccId());
			if (lCaseRoles.size() == 1)
				lRedirect.setCarId(((IECaseRole)lCaseRoles.get(0)).getCarId());
		}
		if ((lRunGroupAccounts.size() == 1) && !lShowCaseRoleCombo)
			// open access run with more case roles could have only 1 rungroupaccount yet
			lRedirect.setRgaId(((IERunGroupAccount)lRunGroupAccounts.get(0)).getRgaId());
		if (lRunTeams.size() == 1)
			lRedirect.setRutId(((IERunTeam)lRunTeams.get(0)).getRutId());
		lListcell.appendChild(lRedirect);

		lListitem.appendChild(lListcell);
		super.insertListitem(aListbox,lListitem,aInsertBefore);
	}

	/**
	 * Gets the run teams for aRunGroupAccounts.
	 *
	 * @param aRunGroupAccounts the a run group accounts
	 *
	 * @return the run teams
	 */
	public List<IERunTeam> getRunTeams(List<IERunGroupAccount> aRunGroupAccounts) {
		List<IERunTeam> lRunTeams = new ArrayList<IERunTeam>(0);
		Hashtable<String,IERunTeam> lRunTeamsHash = new Hashtable<String,IERunTeam>(0);
		List<Integer> lRugIds = new ArrayList<Integer>(0);
		for (IERunGroupAccount lRunGroupAccount : aRunGroupAccounts) {
			lRugIds.add(lRunGroupAccount.getERunGroup().getRugId());
		}
		List<IERunTeam> runTeams = ((IRunTeamManager)CDesktopComponents.sSpring().getBean("runTeamManager")).getAllRunTeams();
		List<IERunTeamRunGroup> lRunTeamGroups = ((IRunTeamRunGroupManager)CDesktopComponents.sSpring().getBean("runTeamRunGroupManager")).getAllRunTeamRunGroupsByRugIds(lRugIds);
		for (IERunGroupAccount lRunGroupAccount : aRunGroupAccounts) {
			for (IERunTeamRunGroup lRunTeamRunGroup : lRunTeamGroups) {
				if (lRunTeamRunGroup.getRugRugId() == lRunGroupAccount.getERunGroup().getRugId()) {
					IERunTeam lRunTeam = null;
					for (IERunTeam lRut : runTeams) {
						if (lRut.getRutId() == lRunTeamRunGroup.getRutRutId())
							lRunTeam = lRut;
					}
					if (lRunTeam != null) {
						lRunTeamsHash.put(""+lRunTeam.getRutId(), lRunTeam);
					}
				}
			}
		}
		for (Enumeration<String> lKeys = lRunTeamsHash.keys(); lKeys.hasMoreElements();) {
			lRunTeams.add(lRunTeamsHash.get(lKeys.nextElement()));
		}
		return lRunTeams;
	}

}