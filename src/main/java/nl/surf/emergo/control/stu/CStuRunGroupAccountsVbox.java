/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.surf.emergo.control.stu;

import java.util.List;

import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.IRunGroupAccountManager;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefVbox;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IERunGroupAccount;

/**
 * The Class CStuRunGroupAccountsVbox.
 *
 * Is used by student.
 * Is used to show the cases for student, private and public cases.
 */
public class CStuRunGroupAccountsVbox extends CDefVbox {

	private static final long serialVersionUID = -3669718229104216732L;

	/** The account id. */
	protected int accId = 0;

	/** The account. */
	protected IEAccount account = null;

	/** The run group accounts. */
	protected List<IERunGroupAccount> rgas = null;

	/** The stu run group accounts helper. */
	protected CStuRunGroupAccountsHelper rgaHelper = new CStuRunGroupAccountsHelper(CDesktopComponents.vView(), CDesktopComponents.sSpring());

	public CStuRunGroupAccountsVbox() {
		super();
		accId = CDesktopComponents.cControl().getAccId();
		if (accId == 0) {
			return;
		}
		account = ((IAccountManager)CDesktopComponents.sSpring().getBean("accountManager")).getAccount(accId);
		rgas = ((IRunGroupAccountManager)CDesktopComponents.sSpring().getBean("runGroupAccountManager")).getAllRunGroupAccountsByAccId(accId);
	}

	/**
	 * Get Account.
	 *
	 * @return account
	 */
	public IEAccount getAccount() {
		return account;
	}

	/**
	 * Get run group accounts.
	 *
	 * @return run group accounts
	 */
	public List<IERunGroupAccount> getRgas() {
		return rgas;
	}

	/**
	 * Get run group accounts helper.
	 *
	 * @return run group accounts helper
	 */
	public CStuRunGroupAccountsHelper getRgaHelper() {
		return rgaHelper;
	}

}
