/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.openu.stoer.idm.autologin;

import java.rmi.RemoteException;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.web.Attributes;

import nl.openu.stoer.idm.utils.Constants;
import nl.openu.stoer.idm.utils.Cookies;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.utilities.PropsUtil;
import nl.surf.emergo.utilities.PropsValues;
import nl.surf.emergo.view.VView;

/**
 * This class handles the IDM login attempts.
 * 
 * @author Aad Slootmaker
 */
public class IDMAutoLogin {
	private static final Logger LOG = LogManager.getLogger(IDMAutoLogin.class);
	private static final String[] NO_CREDENTIALS = null;
	private VView vView = new VView();
	private CControl cControl = new CControl();
	private String idmUserId = "";
	private Locale idmLocale = null;

	public String[] login(final HttpServletRequest req, final HttpServletResponse res) throws Exception {

		// Force an Emergo login dialogue when emergo is running behind IDM sso
		if (PropsValues.PROJECT_SSO_MODE) {
			return NO_CREDENTIALS;
		}

		if (!isAuthenticated(req)) {
			return NO_CREDENTIALS;
		}

		try {
			String[] lUser = getIdmUser(req);
			idmUserId = lUser[0];
			String lUserType = lUser[1];
			if (idmUserId.equals("")) {
				if (LOG.isDebugEnabled()) {
					LOG.debug("Invalid IDM user " + idmUserId);
				}
				return NO_CREDENTIALS;
			}
			LOG.info("IDM user logged in: " + idmUserId + ": Free memory: " + Runtime.getRuntime().freeMemory() + ": "
					+ new Date());
			LOG.info("IDM user type: " + lUserType + ": Free memory: " + Runtime.getRuntime().freeMemory() + ": "
					+ new Date());
			cControl.setAccSessAttr("idm_userid", idmUserId);
			idmLocale = getIdmLocale(req);
			cControl.setAccSessAttr("idm_locale", idmLocale);
			cControl.setAccSessAttr("idm_usertype", lUserType);
			vView.redirectToView(VView.v_emergo);
			return lUser;

		} catch (Exception e) {
			LOG.error(e);
			return NO_CREDENTIALS;
		}
	}

	public String getIdmUserId() {
		return idmUserId;
	}

	public Locale getIdmLocale() {
		return idmLocale;
	}

	private String[] getIdmUser(final HttpServletRequest req) throws Exception, RemoteException {
		// Get the user id from idm
		String lUserType = req.getHeader("ouusertype");
		LOG.info(
				"ouusertype: " + lUserType + ": Free memory: " + Runtime.getRuntime().freeMemory() + ": " + new Date());
		return (lUserType.equalsIgnoreCase("employee")) ? new String[] { req.getHeader("ouuser"), "employee" }
				: new String[] { req.getHeader("ouidm"), "student" };
	}

	private Locale getIdmLocale(final HttpServletRequest req) throws Exception, RemoteException {
		// Get the language from idm
		Cookie langCookie = Cookies.getCookie(req, "ounllang");
		Locale loc = null;

		if (langCookie != null) {
			loc = new Locale(langCookie.getValue());
		}

		if (loc == null) {
			loc = (Locale) cControl.getSessAttr(Attributes.PREFERRED_LOCALE);
			if (loc == null) {
				// ensure that there always is a locale
				loc = new Locale("nl");
			}
		}

		return loc;
	}

	private boolean isAuthenticated(final HttpServletRequest req) {
		return req.getHeader("ouuser") != null;
	}

	private String getLoginUrl() {
		return getProperty(Constants.IDM_LOGOUT_URL);
	}

	private String getServiceUrl() {
		return getProperty(Constants.IDM_SELFSERVICE_URL);
	}

	private String getProperty(final String key) {
		try {
			String result = PropsUtil.get(key);

			if (result == null) {
				LOG.error(String.format("Required property '%s' was not set", key));
			} else if (LOG.isDebugEnabled()) {
				LOG.debug(String.format("'%s' has value: '%s'", key, result));
			}
			return result;

		} catch (Exception e) {
			LOG.error(String.format("Unable to retrieve property '%s'", key));
			return null;
		}
	}
}
