/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.openu.stoer.idm.autologin;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.impl.RequestInfoImpl;

import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.def.CDefWindow;
import nl.surf.emergo.view.VView;

/**
 * The Class IDMHeaderTest.
 * 
 * Used to test if header contains idm user info.
 */
public class IDMHeaderTest extends CDefWindow {

	private static final long serialVersionUID = 4882925088737584434L;

	/** The view. */
	private VView vView = CDesktopComponents.vView();

	/** The control. */
	private CControl cControl = CDesktopComponents.cControl();

	/**
	 * On create check idm user.
	 */
	public void onCreate() {
		RequestInfoImpl lReqInfoImpl = new RequestInfoImpl(Executions.getCurrent(),null);
    	HttpServletRequest lServletReq = (HttpServletRequest)lReqInfoImpl.getNativeRequest();
    	if (lServletReq != null) {
    		IDMAutoLogin lAuLogin = new IDMAutoLogin();
    		try {
    			String[] lUser = (String[])lAuLogin.login(lServletReq,null);
    			if (lUser == null){
           			setVisible (true);
        			if (vView.getIncludeThatRunsEmergo() == null) {
        				doOverlapped();
        				setPosition("center,center");
        			}
    			}
    		} catch (Exception e) {
    		}
    	}
    	
    	cControl.handleLanguage(CDesktopComponents.vView().getCurrentUrl());
    	
		//user gets login.zul if statement above on line 69 does not redirect to emergo.zul (in case user is linked to EMERGO using IDM)
    	cControl.setAccSessAttr("no_login", "false");

    	//other institutions may use a student id as request parameter. It is stored in the EMERGO database after successful login.
    	String external_student_id = VView.getReqPar("student_id");
    	if (!StringUtils.isEmpty(external_student_id)) {
        	cControl.setAccSessAttr("login_external_student_id", external_student_id);
    		CDesktopComponents.sSpring().getSLogHelper().logAction("EXTERNAL-STUDENT-LOGIN sess_login_external_student_id=" + cControl.getAccSessAttr("login_external_student_id"));
    	}

    	//other institutions may use a institution prefix as request parameter. It is stored in the session after successful login.
    	String external_institution_prefix = VView.getReqPar("institution_prefix");
    	if (!StringUtils.isEmpty(external_institution_prefix)) {
        	cControl.setAccSessAttr("login_external_institution_prefix", external_institution_prefix);
    		CDesktopComponents.sSpring().getSLogHelper().logAction("EXTERNAL-INSTITUTION-PREFIX sess_login_external_institution_prefix=" + cControl.getAccSessAttr("login_external_institution_prefix"));
    	}
	}

}
