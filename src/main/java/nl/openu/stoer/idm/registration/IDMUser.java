/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.openu.stoer.idm.registration;

import java.util.Locale;

import org.apache.commons.lang3.StringUtils;

/**
 * Enter the class documentation here.
 * 
 * @author Harrie Martens
 * @author Hubert Vogten
 * @version $Revision$, $Date$
 */
public class IDMUser {
	private String screenName;
	private String firstName;
	private String middleName;
	private String lastName;
	private String email;
	private Locale locale;
	private String idmRole;
	private long companyId;

	/**
	 * Instantiates a new IDM user.
	 */
	public IDMUser() {
	}

	/**
	 * Instantiates a new iDM user.
	 * 
	 * @param screenName
	 *            the screen name
	 * @param firstName
	 *            the first name
	 * @param middleName
	 *            the middle name
	 * @param lastName
	 *            the last name
	 * @param email
	 *            the email
	 * @param idmRole
	 *            the IDM role
	 * @param locale
	 *            the locale
	 */
	// CHECKSTYLE:OFF too many parameters
	public IDMUser(final String screenName, final String firstName, final String middleName, final String lastName,
			final String email, final String idmRole, final Locale locale, final long companyId) {
		// CHECKSTYLE:ON
		this.screenName = screenName;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.email = email;
		this.idmRole = idmRole;
		this.locale = locale;
		this.companyId = companyId;
	}

	/**
	 * Gets the screen name.
	 * 
	 * @return the screen name
	 */
	public String getScreenName() {
		return screenName;
	}

	/**
	 * Sets the screen name.
	 * 
	 * @param screenName
	 *            the new screen name
	 */
	public void setScreenName(final String screenName) {
		this.screenName = screenName;
	}

	/**
	 * Gets the first name.
	 * 
	 * @return the first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the first name.
	 * 
	 * @param firstName
	 *            the new first name
	 */
	public void setFirstName(final String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Gets the middle name.
	 * 
	 * @return the middle name
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * Sets the middle name.
	 * 
	 * @param middleName
	 *            the new middle name
	 */
	public void setMiddleName(final String middleName) {
		this.middleName = middleName;
	}

	/**
	 * Gets the last name.
	 * 
	 * @return the last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the last name.
	 * 
	 * @param lastName
	 *            the new last name
	 */
	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets the email.
	 * 
	 * @return the email
	 */
	public String getEmailAddress() {
		return email;
	}

	/**
	 * Sets the email.
	 * 
	 * @param email
	 *            the new email
	 */
	public void setEmailAddress(final String email) {
		this.email = email;
	}

	/**
	 * Gets the IDM role.
	 * 
	 * @return the IDM role
	 */
	public String getIDMRole() {
		return idmRole;
	}

	/**
	 * Sets the IDM role.
	 * 
	 * @param idmRole
	 *            the new IDM role
	 */
	public void setIDMRole(final String idmRole) {
		this.idmRole = idmRole;
	}

	/**
	 * Gets the locale.
	 * 
	 * @return the locale
	 */
	public Locale getLocale() {
		return locale;
	}

	/**
	 * Sets the locale.
	 * 
	 * @param locale
	 *            the new locale
	 */
	public void setLocale(final Locale locale) {
		this.locale = locale;
	}

	/**
	 * Gets the company id.
	 * 
	 * @return the company id
	 */
	public long getCompanyId() {
		return companyId;
	}

	/**
	 * Sets the company id.
	 * 
	 * @param companyId
	 *            the new company id
	 */
	public void setCompanyId(final long companyId) {
		this.companyId = companyId;
	}

	@Override
	public String toString() {
		return String.format(
				"screenName=%s, firstName=%s, middleName=%s, lastName=%s, email=%s, locale=%s, idmRole=%s, "
						+ "companyId=%d", screenName, firstName, middleName, lastName, email, locale, idmRole,
				companyId);
	}

	/**
	 * Validates the idm user by checking if the required fields are provided.
	 * <p>
	 * These fields are the firstname, lastname, screeenname and email.
	 * 
	 * @return true, if the user is valid, false otherwise
	 */
	public boolean isValid() {
		return StringUtils.isNotBlank(firstName) && StringUtils.isNotBlank(lastName)
				&& StringUtils.isNotBlank(screenName) && StringUtils.isNotBlank(email) && (locale != null);
	}

}
