/*
 * Emergo, a framework for developing online multi-user serious games
 * Copyright (C) 2006-2013 Aad Slootmaker and Hub Kurvers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program (/license.txt); if not,
 * write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *     Contact information:
 *     Open University of the Netherlands
 *     Valkenburgerweg 177 Heerlen
 *     PO Box 2960 6401 DL Heerlen
 *     e-mail: aad.slootmaker@ou.nl or
 *             hub.kurvers@ou.nl
 *
 *
 * Open Universiteit Nederland, hereby disclaims all copyright interest
 * in the program Emergo written by
 * Aad Slootmaker and Hub Kurvers
 *
 */
package nl.openu.stoer.idm.utils;

/**
 * This utility class defines all constants for this project.
 * 
 * @author Jules Alberts
 * @author Hubert Vogten
 * @version $Revision$, $Date$
 */
public final class Constants {

	/**
	 * This constructor prevents the creation of instances for this utility
	 * class.
	 */
	private Constants() {
	}

	/*************************************************************************
	 * iemand kan student en bama student zijn, maar niet student en prospect.
	 * studiemail en all users kunnen genegeerd worden
	 *************************************************************************/

	/** The Constant IDM_ROL_FORMER_STUDENT. */
	public static final String IDM_ROL_FORMER_STUDENT = "Former student";

	/** The Constant IDM_ROL_STUDENT. */
	public static final String IDM_ROL_STUDENT = "Student";

	/** The Constant IDM_ROL_BAMA_STUDENT. */
	public static final String IDM_ROL_BAMA_STUDENT = "Bama Student";

	/** The Constant IDM_ROL_ALUMNUS. */
	public static final String IDM_ROL_ALUMNUS = "Alumnus";

	/** The Constant IDM_ROL_DISABLED. */
	public static final String IDM_ROL_DISABLED = "Disabled";

	/** The Constant IDM_ROL_PROSPECT. */
	public static final String IDM_ROL_PROSPECT = "Prospect";

	/** The Constant IDM_ROL_STUDIEMAIL. */
	public static final String IDM_ROL_STUDIEMAIL = "Studiemail";

	/** The Constant IDM_ROL_ALL_USERS. */
	public static final String IDM_ROL_ALL_USERS = "All users";

	/** The Constant STOER_ROL_GRIJS. */
	public static final String STOER_ROL_GRIJS = "idm_grijs";

	/** The Constant STOER_ROL_STUDENT. */
	public static final String STOER_ROL_STUDENT = "idm_student";

	/** The Constant OU_MEDEWERKER. */
	public static final String OU_MEDEWERKER = "idm_medewerker";

	/** The Constant OU_MEDEWERKER_CELSTEC. */
	public static final String OU_MEDEWERKER_CELSTEC = "idm_celstec";

	/** The Constant OU_MEDEWERKER_INF. */
	public static final String OU_MEDEWERKER_INF = "idm_informatica";

	/*************************************************************************
	 * put the values for these properties in portal-ext.properties
	 *************************************************************************/

	/** The property key for the idm web service url. */
	public static final String IDM_WEBSERVICE = "idm.webservice.url";
	
	/** The property key for the idm web service user name. */
	public static final String IDM_WEBSERVICE_USERNAME = "idm.webservice.username";

	/** The property key for the idm web service password. */
	public static final String IDM_WEBSERVICE_PASSWORD = "idm.webservice.password";

	/** The property key for the portal's login url. */
	public static final String IDM_LOGOUT_URL = "idm.logout.url";

	/** The property key for idm self service page url. */
	public static final String IDM_SELFSERVICE_URL = "idm.selfservice.url";
}
