function getElement(elementName) {
	var lObj = null;
	if ((navigator.appName.indexOf("Microsoft") != -1) || (navigator.appName.indexOf("Netscape") != -1)) {
		lObj = window[elementName];
		if (lObj == null) {
			lObj = document[elementName];
		}
	} else {
		lObj = document[elementName];
		if (lObj == null) {
			lObj = window[elementName];
		}
	}
	return lObj;
};

function eventToServer(event, data) {
	externalEventToServer('webcam', rtcComponentId, event, data);
};

function showMessage(message) {
	$('#' + gRtcMessageFieldId).html(message);
	/*
	var lField = getElement(gRtcMessageFieldId);
	if (lField) {
		lField.html(message);
		//$('#message').html(message);
	}
	*/
};

function setText(pField, pStatus, pAdd) {
	console.log(pAdd);
	if (!(pStatus == "")) {
		pStatus += "\n";
	}
	pStatus += pAdd;
	var lField = getElement(pField);
	if (lField) {
		lField.value = pStatus;
		lField.scrollTop = 99999;
	} 
	return pStatus;
};

function webcamStatus(pStatusInt, pStatusText) {
	if (pStatusInt == 4) {
		//connected
		//also, if request to accept webcam was visible, it is now closed
		st_ServerConnected = true;
		showMessage('');
		eventToServer('onShowRecord','webcam');
		if (st_RecordRequest) {
			startRec(movieName, webcamVisible + "");
			st_RecordRequest = false;
		}
	} else if (pStatusInt == 5) {
		//recording started
		eventToServer('onRecordingStarted','webcam');
	} else if (pStatusInt == 6) {
		//recording paused
		eventToServer('onRecordingPaused','webcam');
	} else if (pStatusInt == 7) {
		//recording ressumed
		eventToServer('onRecordingResumed','webcam');
	} else if (pStatusInt == 8) {
		//recording finished
		var newFileName = movieName;
		newFileName = newFileName + streamExtension;
		eventToServer('onRecordComplete','webcam,'+newFileName);
	} else if (pStatusInt == 9) {
		//recording finished because of error
		//nothing to do; error already sent
	} else if (pStatusInt == 14) {
		//request to accept webcam is shown
		eventToServer('onShowWebcamSettings','webcam');
	}
	gStatusText = setText(gRtcStatusFieldId, gStatusText, pStatusInt + ": " + pStatusText);
};

function webcamError(pErrorInt, pErrorText, pErrorId) {
	var lNwError = pErrorInt + ": " + pErrorId + ": " + pErrorText;
	gErrorText = setText(gRtcErrorFieldId, gErrorText, lNwError);
	eventToServer('onError','webcam,' + pErrorId);
};

function getRTCWebCam() {
	return getElement(rtcElementName);
};

var rtcComponentId;
var gRecordRTC = false;
var gRTCError = false;
var gRTCLoaded = false;
var gWaitForExec = [];

var rtcElementName;

var rtcLocalVideo;
var rtcPeerConnection = null;
var rtcPeerConnectionConfig = {'iceServers': []};
var rtcLocalStream = null;
var rtcWsURL;
var rtcWsConnection = null;
var rtcStreamInfo;
var rtcUserData = {param1:"value1"};
var rtcVideoBitrate;
var rtcAudioBitrate;
var rtcVideoFrameRate;
var rtcVideoChoice;
var rtcAudioChoice;
var rtcVideoIndex = -1;
var rtcAudioIndex = -1;
var rtcUserAgent = null;
var rtcNewAPI = false;
var rtcSDPOutput = new Object();
var rtcGotUserMedia = false;
var streamExtension;

var rtcConstraints =
    {
		video: true,
		audio: true
    };

var movieName;
var webcamVisible = false;
var st_ServerConnected = false;
var st_RecordRequest = false;
var st_ConnectError = false;

var gRtcStatusFieldId;
var gRtcErrorFieldId;
var gStatusText;
var gErrorText;
var gRtcMessageFieldId;

var rtcLogStr;

function webcamInit (aInitValues) {

	var lComponentId = "";
	if (aInitValues.a_componentId) {
		lComponentId = aInitValues.a_componentId;
	}
	rtcComponentId = lComponentId;

	var lAppName = "";
	if (aInitValues.a_appName) {
		lAppName = aInitValues.a_appName;
	}
	var lStreamName = "";
	var lMovieName = "";
	if (aInitValues.a_streamName) {
		lStreamName = aInitValues.a_streamName;
		lMovieName = lStreamName;
	}
	rtcStreamInfo = {applicationName:lAppName, streamName:lStreamName, sessionId:"[empty]"};
	movieName = lMovieName;

	var lRtcWsURL = "";
	if (aInitValues.a_rtcWsURL) {
		lRtcWsURL = aInitValues.a_rtcWsURL;
	}
	rtcWsURL = lRtcWsURL;

	var lRtcElementName = "";
	if (aInitValues.a_wcElementId) {
		lRtcElementName = aInitValues.a_wcElementId;
	}
	rtcElementName = lRtcElementName;

	if (aInitValues.a_wcVisible) {
		webcamVisible = (aInitValues.a_wcVisible === "true");
	}
	
	var lStreamExtension = "";
	if (aInitValues.a_streamExtension) {
		lStreamExtension = aInitValues.a_streamExtension;
	}
	streamExtension = lStreamExtension;

	gRtcStatusFieldId = "StatusText";
	if (aInitValues.a_statusFieldId) {
		gRtcStatusFieldId = aInitValues.a_statusFieldId;
	}
	gRtcErrorFieldId = "ErrorText";
	if (aInitValues.a_errorFieldId) {
		gRtcErrorFieldId = aInitValues.a_errorFieldId;
	}
	gStatusText = "";
	gErrorText = "";

	gRtcMessageFieldId = "message";
	if (aInitValues.a_messageFieldId) {
		gRtcMessageFieldId = aInitValues.a_messageFieldId;
	}

	rtcVideoBitrate = 1500;
	if (aInitValues.a_videoBitrate) {
		rtcVideoBitrate = aInitValues.a_videoBitrate;
	}
	rtcAudioBitrate = 128;
	if (aInitValues.a_audioBitrate) {
		rtcAudioBitrate = aInitValues.a_audioBitrate;
	}
	rtcVideoFrameRate = "29.97";
	if (aInitValues.a_videoFrameRate) {
		rtcVideoFrameRate = aInitValues.a_videoFrameRate;
	}
	rtcVideoChoice = "42e01f";
	if (aInitValues.a_videoChoice) {
		rtcVideoChoice = aInitValues.a_videoChoice;
	}
	rtcAudioChoice = "opus";
	if (aInitValues.a_audioChoice) {
		rtcAudioChoice = aInitValues.a_audioChoice;
	}

	gRecordRTC = false;
	gRTCError = false;
	gRTCLoaded = false;
	gWaitForExec = [];
	st_ServerConnected = false;
	st_RecordRequest = false;
	st_ConnectError = false;

	rtcLocalVideo = getRTCWebCam();

	if (rtcLocalVideo) {
		DetectRTC.load(function () {
			rtcLogStr = "";
			if (DetectRTC.isWebRTCSupported && DetectRTC.isGetUserMediaSupported) {
				rtcLogStr = "webRTC:true & browser:" + adapter.browserDetails.browser + " & browserversion:" +
							adapter.browserDetails.version;
				console.log("webrtc browser: " + adapter.browserDetails.browser + ", version: " + adapter.browserDetails.version);
				if (!DetectRTC.isVideoSupportsStreamCapturing) {
					console.log("webrtc: video stream capturing not supported");
					rtcLogStr += " & videostreamcapturing:false";
					webcamError(3, "video stream capturing not supported", "noVideoSupport");
					eventToServer('onLog','webcam, ' + rtcLogStr);
				} else {
					console.log("webrtc: video stream capturing supported");
					rtcLogStr += " & videostreamcapturing:true";
					if (!DetectRTC.isAudioContextSupported) {
						console.log("webrtc: audio context not supported");
						rtcLogStr += " & audiocontextsupport:false";
						webcamError(4, "audio context not supported", "noAudioSupport");
						eventToServer('onLog','webcam, ' + rtcLogStr);
					} else {
						console.log("webrtc: audio context supported");
						rtcLogStr += " & audiocontextsupport:true";
						if (!DetectRTC.isCreateMediaStreamSourceSupported) {
							console.log("webrtc: create mediastream source not supported");
							rtcLogStr += " & createmediastreamsourcesupport:false";
							webcamError(5, "create mediastream source not supported", "noMediaStream");
							eventToServer('onLog','webcam, ' + rtcLogStr);
						} else {
							console.log("webrtc: create mediastream source supported");
							rtcLogStr += " & createmediastreamsourcesupport:true; USE WEBRTC";
							gRecordRTC = true;
							if (DetectRTC.hasMicrophone) {
								console.log("webrtc: hasMicrophone");
								console.log("webrtc: number of mikes: " + DetectRTC.audioInputDevices.length);
				                DetectRTC.audioInputDevices.forEach(function(device, idx) {
				                    var lLabel = device.label || 'UnKnown mike';
				    				console.log("webrtc: mike: " + lLabel);
									rtcLogStr += " & microphone: " + lLabel;
								});
							} else {
								webcamError(1, "microphone not found", "noMicrophone");
								console.log("webrtc: microphone not detected");
								gRTCError = true;
								rtcLogStr += " & no microphone";
							}
							if (!gRTCError) {
								if (DetectRTC.hasWebcam) {
									console.log("webrtc: hasWebcam");
									console.log("webrtc: number of cameras: " + DetectRTC.videoInputDevices.length);
					                DetectRTC.videoInputDevices.forEach(function(device, idx) {
					                    var lLabel = device.label || 'UnKnown Camera';
					    				console.log("webrtc: camera: " + lLabel);
										rtcLogStr += " & camera: " + lLabel;
					                });
								} else {
									webcamError(0, "webcam not found", "noWebcam");
									console.log("webrtc: webcam not detected");
									gRTCError = true;
									rtcLogStr += " & no webcam";
								}
							}
							if (!gRTCError) {
							    if(navigator.mediaDevices.getUserMedia)
								{
									navigator.mediaDevices.getUserMedia(rtcConstraints).then(getUserMediaSuccess).catch(getUserMediaError);
									rtcNewAPI = true;
								}
							    else if (navigator.getUserMedia)
							    {
							        navigator.getUserMedia(rtcConstraints, getUserMediaSuccess, getUserMediaError);
							    } else {
							    	console.log("webrtc getUserMedia: unknown error");
									rtcLogStr += " & webRTC:false";
									webcamError(2, "webRTC not supported", "noWebRTC");
									eventToServer('onLog','webcam, ' + rtcLogStr);
									gRTCError = true;
							    }
							} else {
								eventToServer('onLog','webcam, ' + rtcLogStr);
							}
						}
					}
				}
			} else {
				webcamError(2, "webRTC not supported", "noWebRTC");
			}
		});
	} else {
		console.log("WEBCAM ERROR: no video element on page with id:  " + rtcElementName);
		gRTCError = true;
		webcamError(-850, "webcam error on webpage", "noVideoElement");
	}
}

//NOTE stopRec called from Java code
function stopRec(pSave) {
	if (gRecordRTC) {
		console.log('Stopping recording. Save?: ' + pSave);
		RTC_stopRec(pSave);
	}
};

//NOTE pauseRec called from Java code
function pauseRec() {
	if (gRecordRTC) {
		console.log('Pause recording.');
		RTC_pauseRec();
	}
};

//NOTE resumeRec called from Java code
function resumeRec() {
	if (gRecordRTC) {
		console.log('Resume recording.');
		RTC_resumeRec();
	}
};

function getUserMediaSuccess(stream)
{
	eventToServer('onLog','webcam,' + rtcLogStr);
	console.log("getUserMediaSuccess: "+stream);
    rtcLocalStream = stream;
	try{
		rtcLocalVideo.srcObject = stream;
	} catch (error){
		rtcLocalVideo.src = window.URL.createObjectURL(stream);
	}
	rtcGotUserMedia = true;
	webcamStatus(4, "webRTC: got usermedia");
	
	gRTCLoaded = true;
	
	console.log("starting recording, show webcam? " + webcamVisible); 
	RTC_startRec();
}

function wsConnect(url)
{
	
	rtcWsConnection = new WebSocket(url);
	rtcWsConnection.binaryType = 'arraybuffer';

	rtcWsConnection.onopen = function()
	{
		console.log("wsConnection.onopen -> url: " + url);

		rtcPeerConnection = new RTCPeerConnection(rtcPeerConnectionConfig);
		rtcPeerConnection.onicecandidate = gotIceCandidate;
		rtcPeerConnection.addEventListener('connectionstatechange', e => RTC_ConnectionStateChange(rtcPeerConnection, e));
		rtcPeerConnection.addEventListener('signalingstatechange', e => RTC_SignalingStateChange(rtcPeerConnection, e));

		if (rtcNewAPI)
		{
			var localTracks = rtcLocalStream.getTracks();
			for(localTrack in localTracks)
			{
				rtcPeerConnection.addTrack(localTracks[localTrack], rtcLocalStream);
			}
		}
		else
		{
			rtcPeerConnection.addStream(rtcLocalStream);
		}

		rtcPeerConnection.createOffer(gotDescription, errorHandler);
	}

 //var offerOptions = {
    // New spec states offerToReceiveAudio/Video are of type long (due to
    // having to tell how many "m" lines to generate).
    // http://w3c.github.io/webrtc-pc/#idl-def-RTCOfferAnswerOptions.
  //  offerToReceiveAudio: 1,
   // offerToReceiveVideo: 1,
	// codecPayloadType: 0x42E01F,
 // };

	rtcWsConnection.onmessage = function(evt)
	{
		console.log("wsConnection.onmessage: "+evt.data);

		var msgJSON = JSON.parse(evt.data);

		var msgStatus = Number(msgJSON['status']);
		var msgCommand = msgJSON['command'];

		if (msgStatus != 200)
		{
			RTC_stopRec(false);
			st_ConnectError = true;
			webcamError(-100, msgJSON['statusDescription'], "applicationError");
		} else
		{
			var sdpData = msgJSON['sdp'];
			if (sdpData !== undefined)
			{
				console.log('sdp: '+msgJSON['sdp']);

				rtcPeerConnection.setRemoteDescription(new RTCSessionDescription(sdpData), function() {
					//peerConnection.createAnswer(gotDescription, errorHandler);
				}, errorHandler);
			}

			var iceCandidates = msgJSON['iceCandidates'];
			if (iceCandidates !== undefined)
			{
				for(var index in iceCandidates)
				{
					console.log('iceCandidates: '+iceCandidates[index]);

					rtcPeerConnection.addIceCandidate(new RTCIceCandidate(iceCandidates[index]));
				}
			}
		}

		if (rtcWsConnection != null)
			rtcWsConnection.close();
		rtcWsConnection = null;
	}

	rtcWsConnection.onclose = function()
	{
		console.log("wsConnection.onclose");
	}

	rtcWsConnection.onerror = function(evt)
	{
		console.log("wsConnection.onerror: "+JSON.stringify(evt));

		RTC_stopRec(false);
		st_ConnectError = true;
		webcamError(-100, "wsConnection.onerror: "+JSON.stringify(evt), "connectionError");
	}
}

function gotIceCandidate(event)
{
    if(event.candidate != null)
    {
    	console.log('gotIceCandidate: '+JSON.stringify({'ice': event.candidate}));
    }
}

function gotDescription(description)
{
	var enhanceData = new Object();

	if (rtcAudioBitrate !== undefined)
		enhanceData.audioBitrate = Number(rtcAudioBitrate);
	if (rtcVideoBitrate !== undefined)
		enhanceData.videoBitrate = Number(rtcVideoBitrate);
	if (rtcVideoFrameRate !== undefined)
		enhanceData.videoFrameRate = Number(rtcVideoFrameRate);

	description.sdp = enhanceSDP(description.sdp, enhanceData);

	console.log('gotDescription: '+JSON.stringify({'sdp': description}));

	rtcPeerConnection.setLocalDescription(description, function () {
		rtcWsConnection.send('{"direction":"publish", "command":"sendOffer", "streamInfo":'
				+JSON.stringify(rtcStreamInfo)+', "sdp":'+JSON.stringify(description)+', "userData":'+JSON.stringify(rtcUserData)+'}');
    }, function() {console.log('set description error')});
}

function addAudio(sdpStr, audioLine)
{
	var sdpLines = sdpStr.split(/\r\n/);
	var sdpSection = 'header';
	var hitMID = false;
	var sdpStrRet = '';
	var done = false;

	for(var sdpIndex in sdpLines)
	{
		var sdpLine = sdpLines[sdpIndex];
		if (sdpLine.length <= 0)
			continue;

		sdpStrRet +=sdpLine;
		sdpStrRet += '\r\n';

		if ( 'a=rtcp-mux'.localeCompare(sdpLine) == 0 && done == false )
		{
			sdpStrRet +=audioLine;
			done = true;
		}
	}
	return sdpStrRet;
}

function addVideo(sdpStr, videoLine)
{
	var sdpLines = sdpStr.split(/\r\n/);
	var sdpSection = 'header';
	var hitMID = false;
	var sdpStrRet = '';
	var done = false;

	var rtcpSize = false;
	var rtcpMux = false;

	for(var sdpIndex in sdpLines)
	{
		var sdpLine = sdpLines[sdpIndex];

		if (sdpLine.length <= 0)
			continue;

		if ( sdpLine.includes("a=rtcp-rsize") )
		{
			rtcpSize = true;
		}

		if ( sdpLine.includes("a=rtcp-mux") )
		{
			rtcpMux = true;
		}

	}

	for(var sdpIndex in sdpLines)
	{
		var sdpLine = sdpLines[sdpIndex];

		sdpStrRet +=sdpLine;
		sdpStrRet += '\r\n';

		if ( ('a=rtcp-rsize'.localeCompare(sdpLine) == 0 ) && done == false && rtcpSize == true)
		{
			sdpStrRet +=videoLine;
			done = true;
		}

		if ( 'a=rtcp-mux'.localeCompare(sdpLine) == 0 && done == true && rtcpSize == false)
		{
			sdpStrRet +=videoLine;
			done = true;
		}

		if ( 'a=rtcp-mux'.localeCompare(sdpLine) == 0 && done == false && rtcpSize == false )
		{
			done = true;
		}

	}
	return sdpStrRet;
}

function enhanceSDP(sdpStr, enhanceData)
{
	var sdpLines = sdpStr.split(/\r\n/);
	var sdpSection = 'header';
	var hitMID = false;
	var sdpStrRet = '';

	//console.log("Original SDP: "+sdpStr);

	// Firefox provides a reasonable SDP, Chrome is just odd
	// so we have to do a little mundging to make it all work
	if ( !sdpStr.includes("THIS_IS_SDPARTA") || rtcVideoChoice.includes("VP9") )
	{
		for(var sdpIndex in sdpLines)
		{
			var sdpLine = sdpLines[sdpIndex];

			if (sdpLine.length <= 0)
				continue;

			var doneCheck = checkLine(sdpLine);
			if ( !doneCheck )
				continue;

			sdpStrRet +=sdpLine;
			sdpStrRet += '\r\n';

		}
		sdpStrRet =  addAudio(sdpStrRet, deliverCheckLine(rtcAudioChoice,"audio"));
		sdpStrRet =  addVideo(sdpStrRet, deliverCheckLine(rtcVideoChoice,"video"));
		sdpStr = sdpStrRet;
		sdpLines = sdpStr.split(/\r\n/);
		sdpStrRet = '';
	}

	for(var sdpIndex in sdpLines)
	{
		var sdpLine = sdpLines[sdpIndex];

		if (sdpLine.length <= 0)
			continue;

		if ( sdpLine.indexOf("m=audio") ==0 && rtcAudioIndex !=-1 )
		{
			var audioMLines = sdpLine.split(" ");
			sdpStrRet+=audioMLines[0]+" "+audioMLines[1]+" "+audioMLines[2]+" "+rtcAudioIndex+"\r\n";
			continue;
		}

		if ( sdpLine.indexOf("m=video") == 0 && rtcVideoIndex !=-1 )
		{
			var audioMLines = sdpLine.split(" ");
			sdpStrRet+=audioMLines[0]+" "+audioMLines[1]+" "+audioMLines[2]+" "+rtcVideoIndex+"\r\n";
			continue;
		}

		sdpStrRet += sdpLine;

		if (sdpLine.indexOf("m=audio") === 0)
		{
			sdpSection = 'audio';
			hitMID = false;
		}
		else if (sdpLine.indexOf("m=video") === 0)
		{
			sdpSection = 'video';
			hitMID = false;
		}
		else if (sdpLine.indexOf("a=rtpmap") == 0 )
		{
			sdpSection = 'bandwidth';
			hitMID = false;
		}

		if (sdpLine.indexOf("a=mid:") === 0 || sdpLine.indexOf("a=rtpmap") == 0 )
		{
			if (!hitMID)
			{
				if ('audio'.localeCompare(sdpSection) == 0)
				{
					if (enhanceData.audioBitrate !== undefined)
					{
						sdpStrRet += '\r\nb=CT:' + (enhanceData.audioBitrate);
						sdpStrRet += '\r\nb=AS:' + (enhanceData.audioBitrate);
					}
					hitMID = true;
				}
				else if ('video'.localeCompare(sdpSection) == 0)
				{
					if (enhanceData.videoBitrate !== undefined)
					{
						sdpStrRet += '\r\nb=CT:' + (enhanceData.videoBitrate);
						sdpStrRet += '\r\nb=AS:' + (enhanceData.videoBitrate);
						if ( enhanceData.videoFrameRate !== undefined )
							{
								sdpStrRet += '\r\na=framerate:'+enhanceData.videoFrameRate;
							}
					}
					hitMID = true;
				}
				else if ('bandwidth'.localeCompare(sdpSection) == 0 )
				{
					var rtpmapID;
					rtpmapID = getrtpMapID(sdpLine);
					if ( rtpmapID !== null  )
					{
						var match = rtpmapID[2].toLowerCase();
						if ( ('vp9'.localeCompare(match) == 0 ) ||  ('vp8'.localeCompare(match) == 0 ) || ('h264'.localeCompare(match) == 0 ) ||
							('red'.localeCompare(match) == 0 ) || ('ulpfec'.localeCompare(match) == 0 ) || ('rtx'.localeCompare(match) == 0 ) )
						{
							if (enhanceData.videoBitrate !== undefined)
								{
								sdpStrRet+='\r\na=fmtp:'+rtpmapID[1]+' x-google-min-bitrate='+(enhanceData.videoBitrate)+';x-google-max-bitrate='+(enhanceData.videoBitrate);
								}
						}

						if ( ('opus'.localeCompare(match) == 0 ) ||  ('isac'.localeCompare(match) == 0 ) || ('g722'.localeCompare(match) == 0 ) || ('pcmu'.localeCompare(match) == 0 ) ||
								('pcma'.localeCompare(match) == 0 ) || ('cn'.localeCompare(match) == 0 ))
						{
							if (enhanceData.audioBitrate !== undefined)
								{
								sdpStrRet+='\r\na=fmtp:'+rtpmapID[1]+' x-google-min-bitrate='+(enhanceData.audioBitrate)+';x-google-max-bitrate='+(enhanceData.audioBitrate);
								}
						}
					}
				}
			}
		}
		sdpStrRet += '\r\n';
	}
	console.log("Resuling SDP: "+sdpStrRet);
	return sdpStrRet;
}

function deliverCheckLine(profile,type)
{
	var outputString = "";
	for(var line in rtcSDPOutput)
	{
		var lineInUse = rtcSDPOutput[line];
		outputString+=line;
		if ( lineInUse.includes(profile) )
		{
			if ( profile.includes("VP9") || profile.includes("VP8"))
			{
				var output = "";
				var outputs = lineInUse.split(/\r\n/);
				for(var position in outputs)
				{
					var transport = outputs[position];
					if (transport.indexOf("transport-cc") !== -1 || transport.indexOf("goog-remb") !== -1 || transport.indexOf("nack") !== -1)
					{
						continue;
					}
					output+=transport;
					output+="\r\n";
				}

				if (type.includes("audio") )
				{
					rtcAudioIndex = line;
				}

				if (type.includes("video") )
				{
					rtcVideoIndex = line;
				}

				return output;
			}
			if (type.includes("audio") )
			{
				rtcAudioIndex = line;
			}

			if (type.includes("video") )
			{
				rtcVideoIndex = line;
			}
			return lineInUse;
		}
	}
	return outputString;
}

function checkLine(line)
{
	if ( line.startsWith("a=rtpmap") || line.startsWith("a=rtcp-fb") || line.startsWith("a=fmtp"))
	{
		var res = line.split(":");

		if ( res.length>1 )
		{
			var number = res[1].split(" ");
			if ( !isNaN(number[0]) )
			{
				if ( !number[1].startsWith("http") && !number[1].startsWith("ur") )
				{
					var currentString = rtcSDPOutput[number[0]];
					if (!currentString)
					{
						currentString = "";
					}
					currentString+=line+"\r\n";
					rtcSDPOutput[number[0]]=currentString;
					return false;
				}
			}
		}
	}

	return true;
}

function getrtpMapID(line)
{
	var findid = new RegExp('a=rtpmap:(\\d+) (\\w+)/(\\d+)');
	var found = line.match(findid);
	return (found && found.length >= 3) ? found: null;
}

function getUserMediaError(error)
{
	eventToServer('onLog','webcam,' + rtcLogStr);
	errorHandler(error);
}

function errorHandler(error)
{
	var lErrName = error.name;
	var lErrMess = error.message;
	var lErrId;
	var lErrNr;
	RTC_stopRec(false);
	switch (lErrName) {
		case 'NotReadableError':
			if (lErrMess.includes('video')) {
				lErrId = 'notReadableVideo';
				lErrNr = -101;
			} else {
				if (lErrMess.includes('audio')) {
					lErrId = 'notReadableAudio';
					lErrNr = -102;
				} else {
					lErrId = 'notReadableUnknown';
					lErrNr = -103;
				}
			}
			break;
		case 'AbortError':
			lErrId = 'abortError';
			lErrNr = -104;
			break;
		case 'NotAllowedError':
			lErrId = 'notAllowedError';
			lErrNr = -105;
			break;
		case 'NotFoundError':
			lErrId = 'notFoundError';
			lErrNr = -106;
			break;
		case 'OverconstrainedError':
			lErrId = 'overconstrainedError';
			lErrNr = -107;
			break;
		case 'SecurityError':
			lErrId = 'securityError';
			lErrNr = -108;
			break;
		case 'TypeError':
			lErrId = 'typeError';
			lErrNr = -109;
			break;
		default:
			lErrId = 'connectionError';
			lErrNr = -100;
	}
	webcamError(lErrNr, "error in connection to streaming server: " + lErrMess, lErrId);
}

function RTC_ConnectionStateChange(p_PC, event) {
	console.log("webrtc connection state change: " + p_PC.connectionState);
	switch(p_PC.connectionState) {
		case "new":
		case "checking":
			// Trying to connect
			break;
		case "connected":
			// The connection has become fully connected
			rtcStateChanged("recording");
			break;
		case "disconnected":
		case "failed":
			// One or more transports has terminated unexpectedly or in an error
			webcamError(-100, "error connecting to streaming server", "connectionError");
			console.log("webrtc: error in server connection");
			gRTCError = true;
			break;
		case "closed":
			// The connection has been closed
			// closed event not recieved in Chrome?
			rtcPeerConnection = null;
			if (rtcWsConnection != null)
				rtcWsConnection.close();
			rtcWsConnection = null;
			//rtcStateChanged("stopped");
			break;
	}
}

function RTC_SignalingStateChange(p_PC, event) {
	switch(p_PC.signalingState) {
		case "closed":
			// The connection has been closed
			// closed event not recieved in Chrome?
			rtcPeerConnection = null;
			if (rtcWsConnection != null)
				rtcWsConnection.close();
			rtcWsConnection = null;
			//rtcStateChanged("stopped");
			break;
	}
}

function RTC_startRec() {
	if (st_ConnectError)
		return;
	if (rtcGotUserMedia) {

		rtcStreamInfo.streamName = movieName;
		rtcUserAgent = navigator.userAgent.toLowerCase();

		console.log("startRecording: wsURL:"+rtcWsURL+" streamInfo:"+JSON.stringify(rtcStreamInfo));

		wsConnect(rtcWsURL);

	} else {
		st_RecordRequest = true;
	}
}

function RTC_stopRec(pSuccess) {
	if (rtcPeerConnection != null)
		rtcPeerConnection.close();

	rtcPeerConnection = null;

	if (rtcWsConnection != null)
		rtcWsConnection.close();
	rtcWsConnection = null;

    console.log("Stopping tracks");
    
	if (rtcLocalVideo) {
		if (rtcLocalVideo.srcObject) {
			rtcLocalVideo.srcObject.getTracks().forEach(track => {
		        console.log("Stop stream track");
				track.stop();
			});
		}
	}

	// here because onconnectionstatechange does not arrive in Chrome
	if (pSuccess) {
		rtcStateChanged("stopped");
	} else {
		rtcStateChanged("errorstop");
	}

	rtcLocalVideo.style.display = "none";
	rtcLocalVideo.removeAttribute('src');
};

function RTC_pauseRec() {
    console.log("Pausing tracks");
    
	if (rtcLocalVideo) {
		if (rtcLocalVideo.srcObject) {
			rtcLocalVideo.srcObject.getTracks().forEach(track => {
		        console.log("Pause stream track");
				track.enabled = false;
			});
		}
	}
	rtcStateChanged("paused");
};

function RTC_resumeRec() {
    console.log("Resuming tracks");
    
	if (rtcLocalVideo) {
		if (rtcLocalVideo.srcObject) {
			rtcLocalVideo.srcObject.getTracks().forEach(track => {
		        console.log("Resume stream track");
				track.enabled = true;
			});
		}
	}
	rtcStateChanged("resumed");
};

function rtcStateChanged (pState) {
	console.log("rtcStatus: " + pState);
	switch (pState) {
	case "recording":
		//NOTE timeout because Wowza audio stream starts delayed?
		//setTimeout(() => {
			if (webcamVisible) {
				console.log("SHOW WEBCAM");
				rtcLocalVideo.style.display = "block";
				rtcLocalVideo.play();
			}
			webcamStatus(5, "recording started");
		//}, 1000);
	
		break;
	case "paused":
		webcamStatus(6, "recording paused");
		break;
	case "resumed":
		webcamStatus(7, "recording resumed");
		break;
	case "stopped":
		webcamStatus(8, "recording stopped");
		break;
	case "inactive":
		var dummy;
		break;
	case "destroyed":
		var dummy;
		break;
	case "errorstop":
		webcamStatus(9, "recording stopped on error");
		break;
	}
}

