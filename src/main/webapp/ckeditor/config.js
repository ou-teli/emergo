CKEDITOR.editorConfig = function(config) {
	config.extraPlugins = 'uicolor';
	config.resize_enabled = false;
	config.toolbar = 'MyToolbar';
	config.toolbar_MyToolbar = [
   			[ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', 'SpecialChar', 'Font', 'FontSize', 'TextColor', 'BGColor', 'NumberedList', 'BulletedList', 'Link', 'Unlink', 'Image', 'UIColor', 'Source'] ];
};
