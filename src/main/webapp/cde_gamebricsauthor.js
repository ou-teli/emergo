let gCurrentElementParts = {};
let gModifiedElementParts = {};
let gDirtyElementParts = {};
let gCurrentGAElement;
let gGABusy = false;

let gSelectElement;
let gSelectElementNameId;
let gSelectElementElementsId;
let gSelectElementValue;

document.body.addEventListener('change', handleInputChange);
document.body.addEventListener('keyup', handleInputChange);
document.body.addEventListener('paste', handleInputChange);

let gGACheckDirty = true;

document.onreadystatechange = () => {

	if (document.readyState === 'complete') {
		const lLabelBrCrElements = document.querySelectorAll('.em_redirectlbl, .em_redirectbtn');
		lLabelBrCrElements.forEach(el => {
			el.addEventListener('click', async (e) => {
				if (gGACheckDirty) {
					gGACheckDirty = false;

					e.preventDefault();
					e.stopPropagation();
					await checkForDirtyPage();
	
					let clickEvent = new MouseEvent('click', {
						bubbles: true,
						cancelable: true,
						view: window
					});
					el.dispatchEvent(clickEvent);
					
				} else {
					gGACheckDirty = true;
				}
			});
		});
	}

}


async function checkForDirtyPage() {
	if (gCurrentGAElement) {
		if (isElementDirty()) {
			console.log('page dirty');
			await confirmSaveChanges(gCurrentGAElement);
		}
	}
}

function resetCurrentElelmentVars() {
	gCurrentElementParts = {};
	gModifiedElementParts = {};
	gDirtyElementParts = {};
}

function isElementDirty() {
	let lDirty = false;
	if (gCurrentGAElement) {
		Object.keys(gDirtyElementParts).forEach(function(lKey) {
			if (gDirtyElementParts[lKey]) {
				lDirty = true;
				console.log('element ' + lKey + ' is dirty');
			}
		})
	}
	return lDirty;
}
	
function getParentDiv(element) {
	let parentElement = null;
	while (element != null) {
		element = element.parentElement;
		if (element) {
			if (element.className?.includes('zkparentelement')) {
				parentElement = element;
				element = null;
			}
		}
	}
	return parentElement;
}
	
function navigateForward(element, toId) {
	let parentElement = getParentDiv(element);
	if (parentElement != null) {
	    let jsonObject = {
	    	"action":"navigate_forward",
	    	"toId":toId
    	};
		zAu.send(new zk.Event(zk.Widget.$(parentElement.id), 'onNotify', jsonObject));
	}
}
	
function navigateBackward(element, toId) {
	let parentElement = getParentDiv(element);
	if (parentElement != null) {
	    let jsonObject = {
	    	"action":"navigate_backward",
	    	"toId":toId
    	};
		zAu.send(new zk.Event(zk.Widget.$(parentElement.id), 'onNotify', jsonObject));
	}
}

function getElementnameElement(pElement) {
	let lAttr = pElement.getAttribute('elementName');
	while (pElement && !lAttr) {
		pElement = pElement.parentElement;
		if (pElement) {
			lAttr = pElement.getAttribute('elementName');
		}
	}
	return pElement;
}
	
function elementNameExists(textContent, myElementsId) {
	let myElements = document.getElementById(myElementsId);
	for (const element of myElements.childNodes) {
		if (element.nodeName == 'LI' && element.textContent == textContent) {
			return true;
		}
	}
	return false;
}
	
function getUniqueText(textContent, myElementsId) {
	let content = textContent;
	let nr = 1;
	while (elementNameExists(textContent, myElementsId)) {
		textContent = content + ' (kopie ' + nr + ')';
		nr++;
	}
	return textContent;
}

function sortJsonElementsByName(x,y) {
	return ((x.name == y.name) ? 0 : ((x.name > y.name) ? 1 : -1 ));
}

function parseJsonAndSortByName(elementsJson) {
	return JSON.parse(elementsJson).sort(sortJsonElementsByName);
}

function getJsonValue(element, key) {
	for (let tempKey in element) {
		if (tempKey == key) {
			return element[tempKey];
		}
	}
	return '';
}

function setJsonValue(element, key, value) {
	for (let tempKey in element) {
		if (tempKey == key) {
			element[tempKey] = value;
			return;
		}
	}
}

function showAlertPopup(text, okButtonLabel, okButtonFunction) {
	document.getElementById('gamebricsAuthorAlertPopupDiv').style.visibility = 'visible';
	document.getElementById('gamebricsAuthorAlertPopupTextDiv').innerHTML = text;
	document.getElementById('gamebricsAuthorAlertPopupOkButton').innerHTML = okButtonLabel;
	document.getElementById('gamebricsAuthorAlertPopupOkButton').onclick = okButtonFunction;
}

function hideAlertPopup() {
	document.getElementById('gamebricsAuthorAlertPopupDiv').style.visibility = 'hidden';
}

function showDefaultAlertPopup(text) {
	showAlertPopup(text, 'OK', function(){hideAlertPopup()});
}

function showConfirmPopup(text, okButtonLabel, cancelButtonLabel, okButtonFunction, cancelButtonFunction) {
	document.getElementById('gamebricsAuthorConfirmPopupDiv').style.visibility = 'visible';
	document.getElementById('gamebricsAuthorConfirmPopupTextDiv').innerHTML = text;
	document.getElementById('gamebricsAuthorConfirmPopupOkButton').innerHTML = okButtonLabel;
	document.getElementById('gamebricsAuthorConfirmPopupCancelButton').innerHTML = cancelButtonLabel;
	document.getElementById('gamebricsAuthorConfirmPopupOkButton').onclick = okButtonFunction;
	document.getElementById('gamebricsAuthorConfirmPopupCancelButton').onclick = cancelButtonFunction;
}

function showConfirmPopupModal(text, okButtonLabel, cancelButtonLabel, okButtonFunctions, cancelButtonFunctions) {
	document.getElementById('gamebricsAuthorConfirmPopupDiv').style.visibility = 'visible';
	document.getElementById('gamebricsAuthorConfirmPopupTextDiv').innerHTML = text;
	document.getElementById('gamebricsAuthorConfirmPopupOkButton').innerHTML = okButtonLabel;
	document.getElementById('gamebricsAuthorConfirmPopupCancelButton').innerHTML = cancelButtonLabel;
	return new Promise(function(resolve, reject) {
		document.getElementById('gamebricsAuthorConfirmPopupOkButton').onclick = function() {
			if (okButtonFunctions && Array.isArray(okButtonFunctions)) {
				okButtonFunctions.forEach(function(okButtonFunction) {
					if (typeof okButtonFunction === 'function') {
						okButtonFunction();
					}
				});
			}
			resolve();
		}
		document.getElementById('gamebricsAuthorConfirmPopupCancelButton').onclick = function() {
			if (cancelButtonFunctions && Array.isArray(cancelButtonFunctions)) {
				cancelButtonFunctions.forEach(function(cancelButtonFunction) {
					if (typeof cancelButtonFunction === 'function') {
						cancelButtonFunction();
					}
				});
			}
			resolve();
		}
	})
}

function hideConfirmPopup() {
	document.getElementById('gamebricsAuthorConfirmPopupDiv').style.visibility = 'hidden';
}

function showPromptPopup(text, inputText, okButtonLabel, cancelButtonLabel, okButtonFunction, cancelButtonFunction) {
	document.getElementById('gamebricsAuthorPromptPopupDiv').style.visibility = 'visible';
	document.getElementById('gamebricsAuthorPromptPopupTextDiv').innerHTML = text;
	document.getElementById('gamebricsAuthorPromptPopupTextInput').value = inputText;
	document.getElementById('gamebricsAuthorPromptPopupTextInput').focus();
	document.getElementById('gamebricsAuthorPromptPopupOkButton').innerHTML = okButtonLabel;
	document.getElementById('gamebricsAuthorPromptPopupCancelButton').innerHTML = cancelButtonLabel;
	document.getElementById('gamebricsAuthorPromptPopupOkButton').onclick = okButtonFunction;
	document.getElementById('gamebricsAuthorPromptPopupCancelButton').onclick = cancelButtonFunction;
}

function hidePromptPopup() {
	document.getElementById('gamebricsAuthorPromptPopupDiv').style.visibility = 'hidden';
}

function showPromptRichtextPopup(element, text, richtextFieldZkId, inputText, okButtonLabel, cancelButtonLabel, okButtonFunction, cancelButtonFunction) {
	document.getElementById('gamebricsAuthorPromptRichtextPopupTextDiv').innerHTML = text;
	document.getElementById('gamebricsAuthorPromptRichtextPopupOkButton').innerHTML = okButtonLabel;
	document.getElementById('gamebricsAuthorPromptRichtextPopupCancelButton').innerHTML = cancelButtonLabel;
	document.getElementById('gamebricsAuthorPromptRichtextPopupOkButton').onclick = okButtonFunction;
	document.getElementById('gamebricsAuthorPromptRichtextPopupCancelButton').onclick = cancelButtonFunction;
		
	let parentElement = getParentDiv(element);
    let jsonObject = {
    	"action":"show_richtext",
    	"richtextFieldZkId":richtextFieldZkId,
    	"inputText":inputText
   	};
	zAu.send(new zk.Event(zk.Widget.$(parentElement.id), 'onNotify', jsonObject));
}

function showPromptRichtextPopupDiv() {
	document.getElementById('gamebricsAuthorPromptRichtextPopupDiv').style.visibility = 'visible';
}

function hidePromptRichtextPopup() {
	document.getElementById('gamebricsAuthorPromptRichtextPopupDiv').style.visibility = 'hidden';
}

function initElements(myElementsJson, allElementsJson, elementNameId, myElementsId, allElementsId) {
	initElementsLength(myElementsJson, allElementsJson, elementNameId, myElementsId, allElementsId, -1)
}

function initElementsLength(myElementsJson, allElementsJson, elementNameId, myElementsId, allElementsId, pStringLength) {
	gSelectElement = null;

	console.log('initElements; selected: ' + gSelectElementValue);

	if (elementNameId && elementNameId.trim().length > 0) {
		let elementName = document.getElementById(elementNameId);
		elementName.value = '';
	}
	if (myElementsJson) {
		initMyElements(myElementsJson, elementNameId, myElementsId, pStringLength);
	}
	if (allElementsJson) {
		initAllElements(allElementsJson, elementNameId, myElementsId, allElementsId, pStringLength);
	}
	
	if (gSelectElement) {
		setTimeout(function() {selectElement(gSelectElement, gSelectElementNameId, gSelectElementElementsId); document.getElementById(gSelectElementElementsId).scrollTop = gSelectElement.offsetTop;}, 2000);
	}
	
	gSelectElementValue = null;
}

function initMyElements(myElementsJson, elementNameId, myElementsId, pStringLength) {
	removeListElements(myElementsId);
	let myElements = parseJsonAndSortByName(myElementsJson);
	for (let i=0;i<Object.keys(myElements).length;i++) {
  		let lNewElement = addMyElement(elementNameId, myElementsId, myElements[i], pStringLength);
  		if (!gSelectElement && ((gSelectElementValue == myElements[i].name) || (i == (Object.keys(myElements).length - 1)))) {
			gSelectElement = lNewElement;
			gSelectElementNameId = elementNameId;
			gSelectElementElementsId = myElementsId;
		}
	}
}

function initAllElements(allElementsJson, elementNameId, myElementsId, allElementsId, pStringLength) {
	removeListElements(allElementsId);
	let allElements = parseJsonAndSortByName(allElementsJson);
	for (let i=0;i<Object.keys(allElements).length;i++) {
		let lNewElement = addAllElement(elementNameId, myElementsId, allElementsId, allElements[i], pStringLength);
  		if (!gSelectElement && (i == (Object.keys(allElements).length - 1))) {
			gSelectElement = lNewElement;
			gSelectElementNameId = elementNameId;
			gSelectElementElementsId = allElementsId;
		}
	}
}

function addElements(myElementsJson, elementNameId, myElementsId) {
	addElementsLength(myElementsJson, elementNameId, myElementsId, -1)
}

function addElementsLength(myElementsJson, elementNameId, myElementsId, pStringLength) {
	let myElements = parseJsonAndSortByName(myElementsJson);
	for (let i=0;i<Object.keys(myElements).length;i++) {
		addMyElement(elementNameId, myElementsId, myElements[i], pStringLength);
  		if (i == (Object.keys(myElements).length - 1)) {
			gSelectElementValue = myElements[i].name;
		}
	}

	document.getElementById(myElementsId).scrollTop = 99999;
}

function initListConnectElementsLength(pElementsJson, pElementNameId, pElementsId, pStringLength) {
	gSelectElement = null;

	console.log('initListConnectElements; selected: ' + gSelectElementValue);

	if (pElementsJson) {
		removeListElements(pElementsId);
		let lElements = parseJsonAndSortByName(pElementsJson);
		for (let i=0;i<Object.keys(lElements).length;i++) {
	  		let lNewElement = addListConnectElement(pElementNameId, pElementsId, lElements[i], pStringLength);
	  		if (!gSelectElement && ((gSelectElementValue == lElements[i].name) || (i == (Object.keys(lElements).length - 1)))) {
				gSelectElement = lNewElement;
				gSelectElementNameId = pElementNameId;
				gSelectElementElementsId = pElementsId;
			}
		}
	}
	
	if (gSelectElement) {
		setTimeout(function() {selectElement(gSelectElement, gSelectElementNameId, gSelectElementElementsId); document.getElementById(gSelectElementElementsId).scrollTop = gSelectElement.offsetTop;}, 2000);
	}
	
	gSelectElementValue = null;
}

function getFunctionResult(pFunctionName) {
	let lFn = window[pFunctionName];
	if (typeof lFn === "function")
		return lFn();
	console.log('not a function: ' + pFunctionName);
}

function addElementParts(elementPartsJson) {
	resetCurrentElelmentVars();
	let elementParts = parseJsonAndSortByName(elementPartsJson);
	Object.keys(elementParts[0]).forEach(function(lKey) {
		let lValue = showElementPart(lKey, elementParts[0][lKey]);
		gCurrentElementParts[lKey] = lValue;
		gModifiedElementParts[lKey] = lValue;
		gDirtyElementParts[lKey] = false;
		console.log('key: ' + lKey + ' value: ' + lValue);
	})
	setElementDirty(false);
}

function showElementPart(pKey, pValue) {
	let lFieldId = getFieldId(pKey);
	if (lFieldId) {
		return fillField(lFieldId, pValue);
	} else {
		lFieldId = getCKEditorId(pKey);
		if (lFieldId)
			return fillCKEditor(lFieldId, pValue);
		else {
			lFieldId = getCompositeId(pKey);
			if (lFieldId) {
				if (typeof fillCompositeElement === "function")
					return fillCompositeElement(lFieldId, pValue);
				else
					console.log("function 'fillCompositeElement()' not present");
			}
		}
	}
	return pValue;
}

function getElementPartObjectId(pElementPartId, pFnElementIds, pFnObjectIds) {
	let lElementPartIds = getFunctionResult(pFnElementIds);
	if (lElementPartIds) {
		let lObjectIds = getFunctionResult(pFnObjectIds);
		if (lObjectIds) {
			let lInd = lElementPartIds.indexOf(pElementPartId);
			if (lInd > -1)
				return lObjectIds[lInd];
		}
	}
}

function getFieldId(pElementPartId) {
	return getElementPartObjectId(pElementPartId, "getElementPartIdsForFields", "getElementPartFieldIds");
}

function fillField(pFieldId, pValue) {
	let lField = document.getElementById(pFieldId);
	if (lField)
		lField.value = pValue;
	else
		fillZulField(pFieldId, pValue);
	return pValue;
}

function fillZulField(pFieldId, pValue) {
	let lZKField = zk.Widget.$('$' + pFieldId);
	if (lZKField)
		lZKField.setValue(pValue);
	else
		console.log("field not present: " + pFieldId);
}

function getCKEditorId(pElementPartId) {
	return getElementPartObjectId(pElementPartId, "getElementPartIdsCKEditor", "getElementPartCKEditorIds");
}

function fillCKEditor(pEditorId, pValue) {
	if (!pValue)
		//NOTE initial empty value added by CKEditor
		pValue = "<p><br></p>";
	let lCKEditor = zk.Widget.$('$' + pEditorId);
	if (lCKEditor) {
		lCKEditor.setValue(pValue);
		if (typeof initCKEditorValue === "function")
			initCKEditorValue(pEditorId, pValue);
	} else {
		console.log("ckeditor not present: " + pEditorId);
	}
	return pValue;
}

function getCompositeId(pElementPartId) {
	return getElementPartObjectId(pElementPartId, "getElementPartIdsComposite", "getElementPartCompositeIds");
}

function handleInputChange(event) {
	gGABusy = false;
	// Get the input element that triggered the event
	const lInputElement = event.target;
	const lElPartTagNames = ['input', 'textarea'];
	let lId = lInputElement.id;
	if (lElPartTagNames.includes(lInputElement.tagName.toLowerCase())) {
		let lElementPartId = getFieldElementPartId(lId);
		if (lElementPartId) {
			console.log('element attribute connected to "' + lId + '" changed to: ' + lInputElement.value);
			elementPartValueChanged(lElementPartId, lInputElement.value);
		}
	}
	console.log('Input "' + lId + '" changed to: ' + lInputElement.value);
}

function getFieldElementPartId(pFieldId) {
	return getElementPartObjectId(pFieldId, "getElementPartFieldIds", "getElementPartIdsForFields");
}

function getCKEditorElementPartId(pCKEditorId) {
	return getElementPartObjectId(pCKEditorId, "getElementPartCKEditorIds", "getElementPartIdsCKEditor");
}

function zulElementValueChanged(pElementPartObjectId, pValue, pCompositeAttr) {
	let lElementPartId = getFieldElementPartId(pElementPartObjectId);
	if (lElementPartId) {
		console.log('ZUL input ' + pElementPartObjectId + ' changed to: ' + pValue);
		elementPartValueChanged(lElementPartId, pValue);
	} else {
		lElementPartId = getCKEditorElementPartId(pElementPartObjectId);
		if (lElementPartId) {
			console.log('ZUL richtext input ' + pElementPartObjectId + ' changed to: ' + pValue);
			elementPartValueChanged(lElementPartId, pValue);
		} else if (pCompositeAttr) {
			console.log('element attribute connected to ' + pElementPartObjectId + ', in group ' + pCompositeAttr + ', changed to: ' + pValue);
			if (typeof compositeValueChanged === "function")
				compositeValueChanged(pElementPartObjectId, pCompositeAttr, pValue);
			else
				console.log("function 'compositeValueChanged()' not present");
		}
	}
}

function setElementDirty(pDirty) {
	if (typeof setPageElementDirty === "function")
		setPageElementDirty(pDirty);
}

function elementPartValueChanged(pElementPartId, pValue) {
	if (!gCurrentGAElement) {
		setElementDirty(false);
		return;
	}
	if (gModifiedElementParts[pElementPartId] == pValue)
		return;
	let lWasClean = (gCurrentElementParts[pElementPartId] == gModifiedElementParts[pElementPartId]);
	let lIsClean = false;
	gModifiedElementParts[pElementPartId] = pValue;
	if (gCurrentElementParts[pElementPartId] != pValue) {
		console.log('element ' + pElementPartId + ' changed to: ' + pValue + '\n element is dirty!');
		gDirtyElementParts[pElementPartId] = true;
	} else {
		gDirtyElementParts[pElementPartId] = false;
		lIsClean = true;
		console.log('element ' + pElementPartId + ' changed to: ' + pValue + '\n element is clean!');
	}
	if (lWasClean != lIsClean)
		setElementDirty(isElementDirty());
}

function saveChangesClicked(pButtonId) {
	console.log('button id: ' + pButtonId);
	if (typeof savePageChanges === "function")
		savePageChanges(pButtonId);
	else
		saveElementPartChanges();
}

function discardElementPartChanges() {
	if (!gCurrentGAElement) {
		return;
	}
	Object.keys(gDirtyElementParts).forEach(function(lKey) {
		if (gDirtyElementParts[lKey]) {
			gModifiedElementParts[lKey] = gCurrentElementParts[lKey]; 
			gDirtyElementParts[lKey] = false; 
		}
	})
	setElementDirty(false);
}

function saveElementPartChanges() {
	if (!gCurrentGAElement) {
		return;
	}
	if (!isElementDirty()) {
		return;
	}
	let lParentElement = getParentDiv(gCurrentGAElement);
    let lJsonObject = {
    	"action":"save_changes",
    	"elementType":gCurrentGAElement.getAttribute('elementType'),
    	"elementName":gCurrentGAElement.getAttribute('elementName'),
    	"cacId":gCurrentGAElement.getAttribute('cacId'),
    	"tagId":gCurrentGAElement.getAttribute('tagId'),
    	"tagName":gCurrentGAElement.getAttribute('tagName')
	};
	let lChildElements = {};
	Object.keys(gDirtyElementParts).forEach(function(lKey) {
		if (gDirtyElementParts[lKey]) {
			lChildElements[lKey] = gModifiedElementParts[lKey];
			gCurrentElementParts[lKey] = gModifiedElementParts[lKey]; 
			gDirtyElementParts[lKey] = false; 
		}
	})
	lJsonObject["changedChildren"] = lChildElements;
	zAu.send(new zk.Event(zk.Widget.$(lParentElement.id), 'onNotify', lJsonObject));
	setElementDirty(false);
}

function removeListElements(listId) {
	removeAllChildNodes(document.getElementById(listId));
}

function removeAllChildNodes(parent) {
	while (parent.firstChild) {
		parent.removeChild(parent.firstChild);
	}
}

async function deselectElement(myElementsId) {
	let element = getSelectedElement(myElementsId);
	if (element) {
		if (isElementDirty()) {
			await confirmSaveChanges(element);
		}
		element.setAttribute('selected', 'false');
		element.className = '';
	}
	return 'done';
}
	
async function confirmSaveChanges(element) {
	await showConfirmPopupModal("Wilt u de wijzigingen in " + element.getAttribute('elementType') + " '" + element.getAttribute('elementName') + "' bewaren?",
 			'Ja',
 			'Nee', 
 			[hideConfirmPopup,saveElementPartChanges],
 			[hideConfirmPopup,discardElementPartChanges]);
	return 'done';
}

async function selectElement(element, elementNameId, myElementsId) {
	if (gGABusy)
		return;
	if (elementNameId != null && elementNameId != '') {
		//NOTE first clear textbox with name of currently selected element, if present
  		let elementName = document.getElementById(elementNameId);
		elementName.value = '';
	}

	await deselectElement(myElementsId);

	element.setAttribute('selected', 'true');
	element.className = 'selected';
	gCurrentGAElement = element;

	let parentElement = getParentDiv(element);
    let jsonObject = {
    	"action":"select_element",
    	"elementType":element.getAttribute('elementType'),
    	"cacId":element.getAttribute('cacId'),
    	"tagId":element.getAttribute('tagId')
   	};
	console.log('After prompt Select element: ' + element.getAttribute('elementName'));
	zAu.send(new zk.Event(zk.Widget.$(parentElement.id), 'onNotify', jsonObject));
}
	
function getSelectedElement(elementsId) {
	if (elementsId) {
		let elements = document.getElementById(elementsId);
		for (const element of elements.childNodes) {
			if (element.nodeName == 'LI') {
				let selected = element.getAttribute('selected');
				if (selected != null && selected == 'true') {
					return element;
				}
			}
		}
	}
	return null;
}

function showFullText(evt, text) {
	let tooltips = document.getElementsByClassName('ga-tooltip');
	if (tooltips) {
		let tooltip = document.getElementsByClassName('ga-tooltip')[tooltips.length-1];
		tooltip.innerHTML = text;
		tooltip.style.display = "block";
		tooltip.style.left = evt.pageX + 10 + 'px';
		tooltip.style.top = evt.pageY + 10 + 'px';
	}
}

function hideFullText() {
	let tooltips = document.getElementsByClassName('ga-tooltip');
	if (tooltips) {
		let tooltip = document.getElementsByClassName('ga-tooltip')[tooltips.length-1];
		tooltip.style.display = "none";
	}
}	

function createTextNode(pParent, pTextStr, pStringLength) {
	let lString = "";
	let lAddEvent = false;
	if ((pStringLength > 0) && (pTextStr.length > pStringLength)) {
		lString = pTextStr.slice(0, pStringLength) + "...";
		lAddEvent = true;
	} else {
		lString = pTextStr;
	}
	pParent.appendChild(document.createTextNode(lString));
	if (lAddEvent) {
		pParent.addEventListener("mouseover", function(ev) {showFullText(ev, pTextStr);});
		pParent.addEventListener("mouseout", function() {hideFullText();});
	}
}

function addMyElement(elementNameId, myElementsId, myElement, pStringLength) {
	let lNewElement = createListElement(elementNameId, myElementsId, myElement);
	
	let lItemSelectRect = createElementSelectedRectangle(elementNameId, myElementsId);
	lNewElement.appendChild(lItemSelectRect);
	
	createTextNode(lItemSelectRect, myElement.name, pStringLength);
	lItemSelectRect.appendChild(createEditElementButton(elementNameId, myElementsId));
	lItemSelectRect.appendChild(createDeleteElementButton(elementNameId, myElementsId));

	return lNewElement;
}
	
function createListElement(elementNameId, elementsId, elementContent) {
	let elementsList = document.getElementById(elementsId);
	let newElement = document.createElement('li');
	elementsList.appendChild(newElement);
	newElement.setAttribute('elementType', elementContent.elementType);
	newElement.setAttribute('cacId', elementContent.cacId);
	newElement.setAttribute('tagId', elementContent.tagId);
	newElement.setAttribute('elementName', elementContent.name);
	newElement.onclick = function(){selectElement(this, elementNameId, elementsId)};
	newElement.style.cursor = 'pointer';
	
	return newElement;
}

function addListConnectElement(elementNameId, pElementsId, pElement, pStringLength) {
	let lNewElement = createListElement(elementNameId, pElementsId, pElement);
	
	let lItemSelectRect = createElementSelectedRectangle(elementNameId, pElementsId);
	lNewElement.appendChild(lItemSelectRect);
	
	createTextNode(lItemSelectRect, pElement.name, pStringLength);
	lItemSelectRect.appendChild(createCheckElementButton(elementNameId, pElementsId));

	return lNewElement;
}
	
function createEditElementButton(elementNameId, myElementsId) {
	let newSubElement = document.createElement('button');
	newSubElement.onclick = function(){promptEditElement(getElementnameElement(this), elementNameId, myElementsId)};
	newSubElement.className = 'ga-btn btn btn-secondary btn-edit btn-context';

	let newSubSubElement = document.createElement('i');
	newSubElement.appendChild(newSubSubElement);
	newSubSubElement.className = 'fa fa-pencil-alt';

	return newSubElement;
}
	
function createDeleteElementButton(elementNameId, myElementsId) {
	let newSubElement = document.createElement('button');
	newSubElement.onclick = function(){confirmDeleteElement(getElementnameElement(this), elementNameId, myElementsId)};
	newSubElement.className = 'ga-btn btn btn-secondary btn-delete btn-context';
	return newSubElement;
}
	
function createElementSelectedRectangle(elementNameId, myElementsId) {
	let newSubElement = document.createElement('div');
	newSubElement.className = 'ga-selectrect';
	return newSubElement;
}
	
function createCheckElementButton(elementNameId, myElementsId) {
	let newSubElement = document.createElement('button');
	newSubElement.className = 'ga-btn btn btn-secondary btn-check btn-context';
	return newSubElement;
}
	
async function promptEditElement(element, elementNameId, myElementsId) {
	gGABusy = true;
	let lWaited = false;
	console.log('Before prompt Edit element: ' + element.getAttribute('elementName'));
	if (gCurrentGAElement) {
		if (element.getAttribute('elementName') != gCurrentGAElement.getAttribute('elementName')) {
			lWaited = true;
			await deselectElement(myElementsId);
		}
	}
	console.log('After prompt Edit element: ' + element.getAttribute('elementName'));

	gGABusy = false;
	showPromptPopup('Pas naam aan',
 			element.getAttribute('elementName'), 
 			'OK',
 			'Annuleer',
 			function(){hidePromptPopup();editElement(element, elementNameId, myElementsId, document.getElementById('gamebricsAuthorPromptPopupTextInput').value)},
 			function(){hidePromptPopup()});
 	if (lWaited) {
		selectElement(element, elementNameId, myElementsId);
	}
}

function editElement(element, elementNameId, myElementsId, name) {
	//first save change

	if (name != '' && (name != getUniqueText(name, myElementsId))) {
		showAlertPopup('Deze naam komt al voor!', 
			'OK', 
			function(){hideAlertPopup()});
		return;
	}
		
	let parentElement = getParentDiv(element);
    let jsonObject = {
    	"action":"edit_element",
    	"elementType":element.getAttribute('elementType'),
    	"cacId":element.getAttribute('cacId'),
    	"tagId":element.getAttribute('tagId'),
    	"elementName":name
   	};
	zAu.send(new zk.Event(zk.Widget.$(parentElement.id), 'onNotify', jsonObject));

/*		
	//then update interface
	element.setAttribute('elementName', name);
	element.textContent = name;
	element.childNodes[0].nodeValue = name;
	
	//NOTE add buttons, because setting nodeValue results in removing buttons
	element.appendChild(createEditElementButton(elementNameId, myElementsId));
	element.appendChild(createDeleteElementButton(elementNameId, myElementsId));
	*/
}

async function confirmDeleteElement(element, elementNameId, myElementsId) {
	gGABusy = true;
	if (gCurrentGAElement) {
		if (element.getAttribute('elementName') != gCurrentGAElement.getAttribute('elementName')) {
			await deselectElement(myElementsId);
		}
	}

	gGABusy = false;
	showConfirmPopup("Weet u zeker dat u '" + element.getAttribute('elementName') + "' wilt verwijderen?",
 			'OK',
 			'Annuleren', 
 			function(){hideConfirmPopup();deleteElement(element)}, 
 			function(){hideConfirmPopup()});
}

function deleteElement(element) {
		//first save change
	let parentElement = getParentDiv(element);
    let jsonObject = {
    	"action":"delete_element",
    	"elementType":element.getAttribute('elementType'),
    	"cacId":element.getAttribute('cacId'),
    	"tagId":element.getAttribute('tagId')
   	};
	zAu.send(new zk.Event(zk.Widget.$(parentElement.id), 'onNotify', jsonObject));
	//then update interface
	element.parentElement.removeChild(element);
}

function addAllElement(elementNameId, myElementsId, allElementsId, allElement, pStringLength) {
	let allElements = document.getElementById(allElementsId);

	let newElement = document.createElement('li');
	allElements.appendChild(newElement);
	newElement.setAttribute('elementType', allElement.elementType);
	newElement.setAttribute('cacId', allElement.cacId);
	newElement.setAttribute('tagId', allElement.tagId);
	newElement.setAttribute('elementName', allElement.name);
	newElement.setAttribute('elementOwner', allElement.owner);
	newElement.style.cursor = 'pointer';
	createTextNode(newElement, allElement.name + ' [' + allElement.owner + ']', pStringLength);

	newElement.appendChild(createCopyElementButton(elementNameId, myElementsId));

	return newElement;
}

function createCopyElementButton(elementNameId, myElementsId) {
	let newSubElement = document.createElement('button');
	newSubElement.onclick = function(){copyElement(this.parentElement, elementNameId, myElementsId)};
	newSubElement.className = 'ga-btn btn btn-secondary btn-context';

	let newSubSubElement = document.createElement('i');
	newSubElement.appendChild(newSubSubElement);
	newSubSubElement.className = 'fas fa-copy';

	return newSubElement;
}
	
function copyElement(element, elementNameId, myElementsId) {
	if (elementNameId != null && elementNameId != '') {
  		let elementName = document.getElementById(elementNameId);
		elementName.value = '';
	}
	let name = getUniqueText(element.getAttribute('elementName'), myElementsId);

	let parentElement = getParentDiv(element);
    let jsonObject = {
    	"action":"copy_element",
    	"elementType":element.getAttribute('elementType'),
    	"cacId":element.getAttribute('cacId'),
    	"tagId":element.getAttribute('tagId'),
    	"elementName":name
   	};
	zAu.send(new zk.Event(zk.Widget.$(parentElement.id), 'onNotify', jsonObject));

	document.getElementById(myElementsId).scrollTop = 99999;
}

function showElement(clickedElement, elementType, elementNameId, myElementsId) {
 	let nameElement = document.getElementById(elementNameId);

	let name = nameElement.value;
	let selectedElement = getSelectedElement(myElementsId);
	let mode = 'new';
	let cacId = '0';
	let tagId = '0';
	if (selectedElement != null) {
		mode = 'update';
		cacId = selectedElement.getAttribute('cacId');
		tagId = selectedElement.getAttribute('tagId');
		name = selectedElement.textContent;
	}
		
	if (name != '' && selectedElement == null && name != getUniqueText(name, myElementsId)) {
		showAlertPopup('Deze naam komt al voor!', 
			'OK', 
			function(){hideAlertPopup()});
		return;
	}
		
	let parentElement = getParentDiv(clickedElement);
    let jsonObject = {
    	"action":"show_element",
    	"elementType":elementType,
    	"mode":mode,
    	"cacId":cacId,
    	"tagId":tagId,
    	"elementName":name
   	};
	zAu.send(new zk.Event(zk.Widget.$(parentElement.id), 'onNotify', jsonObject));
}

