public class Vignette {

	protected int cacId;
	
	protected int tagId;

	protected String name;

	protected String state;

	protected String doneDate;

	protected int numberOfRecordings;

	public int getCacId() {
		return cacId;
	}

	public void setCacId(int cacId) {
		this.cacId = cacId;
	}

	public int getTagId() {
		return tagId;
	}

	public void setTagId(int tagId) {
		this.tagId = tagId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDoneDate() {
		return doneDate;
	}

	public void setDoneDate(String doneDate) {
		this.doneDate = doneDate;
	}

	public int getNumberOfRecordings() {
		return numberOfRecordings;
	}

	public void setNumberOfRecordings(int numberOfRecordings) {
		this.numberOfRecordings = numberOfRecordings;
	}

}
