public class Vignette {

	protected int rgaId;
	
	protected String studentName;
	
	protected int cacId;
	
	protected int tagId;

	protected String vignetteName;

	protected String vType;

	protected String state;

	protected String doneDate;

	protected int numberOfRecordings;

	public int getRgaId() {
		return rgaId;
	}

	public void setRgaId(int rgaId) {
		this.rgaId = rgaId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public int getCacId() {
		return cacId;
	}

	public void setCacId(int cacId) {
		this.cacId = cacId;
	}

	public int getTagId() {
		return tagId;
	}

	public void setTagId(int tagId) {
		this.tagId = tagId;
	}

	public String getVignetteName() {
		return vignetteName;
	}

	public void setVignetteName(String vignetteName) {
		this.vignetteName = vignetteName;
	}

	public String getVType() {
		return vType;
	}

	public void setVType(String vType) {
		this.vType = vType;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDoneDate() {
		return doneDate;
	}

	public void setDoneDate(String doneDate) {
		this.doneDate = doneDate;
	}

	public int getNumberOfRecordings() {
		return numberOfRecordings;
	}

	public void setNumberOfRecordings(int numberOfRecordings) {
		this.numberOfRecordings = numberOfRecordings;
	}

}
