Folder can contain zul file plug-ins developed for several games.
To use a plug-in:
- zip its files as root files in a zip package
- add a plug-in element in the Navigation component
- in the dialogue upload the zip package
- in the dialogue enter the main file of the zip package, normally plugin.zul
- in the dialogue enter the position of the plug-in

A plugin does not have to be a bunch of zul files. It can be a single html file too for instance.
In this case it can not be multiple files because the path to other files will not be correct,
because the plugin is rendered using a zul incude component, not a zul iframe component.