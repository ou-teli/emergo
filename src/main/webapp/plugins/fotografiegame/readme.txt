For the Fotografie game eight plug-ins are developed.
All plug-ins are meant to be used full screen within a location in the game.
The plugin-ins use the SSpring class of the runWnd to get and set data.
See plugin_macro.zul for documentation on the working of a plug-in.
Locatie 1 till 3 only differ in plugin.zul and photo??.jpg files. Other files are identical.
Sandbox resembles location 1 but has less functionality and other photo's.
Fotoinventaris, fotoinventarissandbox and klaaropdracht also resemble one another.