function getSelItem(list) {
  var values = list.options[list.selectedIndex].value;
  return values;
}

function getSelItems(list) {
  var values = '';
  for (i=0;i<list.options.length;i++) {
    var option = list.options[i];
    if (option.selected) {
      var value = option.value;
      if (values != '')
        values = values + ';';
      values = values + value;
    }
  }
  return values;
}

function getText(field) {
  var value = field.value;
  return value;
}