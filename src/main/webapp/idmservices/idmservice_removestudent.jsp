<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="nl.surf.emergo.webservices.EmergoServicesHelper" %>
<%@ page import="nl.surf.emergo.webservices.idmclient.*" %>

<%@ include file="idmservices_settings_inc.jsp" %>

<%
EmergoServicesHelper helper = new EmergoServicesHelper();
String lUserId = request.getParameter("userid");
if (lUserId == null)
	lUserId = "";
  
List<String> result = null;
String[] lArgs = new String[2];
lArgs[0] = service_wsdl;
boolean lUseClient = helper.useClient(request.getParameter("environment"));
if (lUseClient) {
	IdmServices_Client bmss = new IdmServices_Client(lArgs);
	result = bmss.removeStudent(
		  lUserId
    );
}
else {
//  IdmServices bmss = new IdmServices();
	IdmServices_Client bmss = new IdmServices_Client(lArgs);
	result = bmss.removeStudent(
		  lUserId
    );
}

PrintWriter csv = response.getWriter();
csv.println("<html>");
csv.println("<head>");
csv.println("<title></title>");
csv.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">");
csv.println("</head>");
csv.println("<body>");
if (!lUseClient)
  csv.println("Use Server!<br>");
if ((result == null) || (result.size() == 0))
  csv.println("<b>Result is empty</b>");
else {
	  csv.println(result.get(0));
	  csv.println(result.get(1));
}
csv.println("</body>");
csv.println("</html>");
%>