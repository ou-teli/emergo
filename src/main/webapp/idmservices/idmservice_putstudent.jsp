<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="nl.surf.emergo.webservices.EmergoServicesHelper" %>
<%@ page import="nl.surf.emergo.webservices.idmclient.*" %>

<%@ include file="idmservices_settings_inc.jsp" %>

<%
EmergoServicesHelper helper = new EmergoServicesHelper();
String lUserId = request.getParameter("userid");
if (lUserId == null)
	lUserId = "";
String lStudentid = request.getParameter("studentid");
if (lStudentid == null)
	lStudentid = "";
String lTitle = request.getParameter("title");
if (lTitle == null)
	lTitle = "";
String lInitials = request.getParameter("initials");
if (lInitials == null)
	lInitials = "";
String lNameprefix = request.getParameter("nameprefix");
if (lNameprefix == null)
	lNameprefix = "";
String lLastname = request.getParameter("lastname");
if (lLastname == null)
	lLastname = "";
String lEmail = request.getParameter("email");
if (lEmail == null)
	lEmail = "";
String lCourseCodes = request.getParameter("coursecodes");
if (lCourseCodes == null)
	lCourseCodes = "";
List<String> lCourseCodeArr = Arrays.asList(lCourseCodes.split("\\s*,\\s*"));
String lActiveStr = request.getParameter("active");
Boolean lActive = false;
if (lActiveStr == null)
	lActive = false;
else
	lActive = (lActiveStr.equals("true"));
  
List<String> result = null;
String[] lArgs = new String[2];
lArgs[0] = service_wsdl;
boolean lUseClient = helper.useClient(request.getParameter("environment"));
if (lUseClient) {
	IdmServices_Client bmss = new IdmServices_Client(lArgs);
	result = bmss.putStudent(
		  lUserId,
		  lStudentid,
		  lTitle,
		  lInitials,
		  lNameprefix, 
		  lLastname, 
		  lEmail,
		  lCourseCodeArr,
		  lActive
    );
}
else {
//  IdmServices bmss = new IdmServices();
	IdmServices_Client bmss = new IdmServices_Client(lArgs);
	result = bmss.putStudent(
		  lUserId,
		  lStudentid,
		  lTitle,
		  lInitials, 
		  lNameprefix, 
		  lLastname, 
		  lEmail,
		  lCourseCodeArr,
		  lActive
    );
}

PrintWriter csv = response.getWriter();
csv.println("<html>");
csv.println("<head>");
csv.println("<title></title>");
csv.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">");
csv.println("</head>");
csv.println("<body>");
if (!lUseClient)
  csv.println("Use Server!<br>");
if ((result == null) || (result.size() == 0))
  csv.println("<b>Result is empty</b>");
else {
	  csv.println(result.get(0));
	  csv.println(result.get(1));
}
csv.println("</body>");
csv.println("</html>");
%>