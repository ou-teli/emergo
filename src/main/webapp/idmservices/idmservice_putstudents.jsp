<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="nl.surf.emergo.webservices.EmergoServicesHelper" %>
<%@ page import="nl.surf.emergo.webservices.idmclient.*" %>


<%@ include file="idmservices_settings_inc.jsp" %>

<%
EmergoServicesHelper helper = new EmergoServicesHelper();
String lCourseCodes = "";
for (int i=1;i<=10;i++) {
	if (!lCourseCodes.equals(""))
		lCourseCodes = lCourseCodes + ",";
	lCourseCodes = lCourseCodes + "C" + i;
}
List<String> lCourseCodeArr = Arrays.asList(lCourseCodes.split(","));

List<String> result = null;
boolean lUseClient = helper.useClient(request.getParameter("environment"));

PrintWriter csv = response.getWriter();
csv.println("<html>");
csv.println("<head>");
csv.println("<title></title>");
csv.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">");
csv.println("</head>");
csv.println("<body>");
if (!lUseClient)
	  csv.println("Use Server!<br>");

String[] lArgs = new String[2];
lArgs[0] = service_wsdl;
if (lUseClient) {
	IdmServices_Client bmss = new IdmServices_Client(lArgs);
	for (int i=1;i<=50;i++) {
  result = bmss.putStudent(
		  "idm_"+i,
		  "idm_"+i,
		  "idm_"+i,
		  "idm_"+i,
		  "idm_"+i,
		  "idm_"+i,
		  "idm_"+i,
		  lCourseCodeArr,
		  true
    );
  if ((result == null) || (result.size() == 0))
	  csv.print("<b>Result is empty</b><br/>");
	else {
		  csv.print(result.get(0));
		  csv.print(result.get(1) + "<br/>");
	}
	}
}
else {
//  IdmServices bmss = new IdmServices();
	IdmServices_Client bmss = new IdmServices_Client(lArgs);
	for (int i=1;i<=50;i++) {
  result = bmss.putStudent(
		  "idm_"+i,
		  "idm_"+i,
		  "idm_"+i,
		  "idm_"+i,
		  "idm_"+i,
		  "idm_"+i,
		  "idm_"+i,
		  lCourseCodeArr,
		  true
    );
  if ((result == null) || (result.size() == 0))
	  csv.print("<b>Result is empty</b><br/>");
	else {
		  csv.print(result.get(0) + ":");
		  csv.print(result.get(1) + "<br/>");
	}
	}
}

csv.println("</body>");
csv.println("</html>");
%>