// jQuery.noConflict();

// RUN

jQuery(document).ready(function(){

	setTimeout("initParallax();",100);
//	initParallax();

});
	
function initParallax() {
	var lBackwidth = parseInt (jQuery("#background_width").val());
	if (lBackwidth == 0) {
		lBackwidth = 2340;
	};
    jQuery(".parallax-layer").parallax({
    	mouseport: jQuery(".layers"),
        xparallax: true,
        yparallax: false,
        width: lBackwidth,
        frameDuration: 20,
        decay: 0.95
    },
    {width: lBackwidth},
    {width: lBackwidth},
    {width: lBackwidth + 100},
    {width: lBackwidth + 200},
    {width: lBackwidth + 300},
    {width: lBackwidth + 400},
    {width: lBackwidth + 500},
    {width: lBackwidth + 600});
    jQuery(".parallax-layer").trigger('mouseenter.parallax');

}
