<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="nl.surf.emergo.webservices.EmergoServicesHelper" %>
<%@ page import="nl.surf.emergo.webservices.client.*" %>

<%@ include file="webservices_settings_inc.jsp" %>

<%
EmergoServicesHelper helper = new EmergoServicesHelper();
String lRgaId = request.getParameter("rgaid");
if ((lRgaId == null) || (lRgaId.equals("")))
		lRgaId = "0";
String lKey = request.getParameter("key");
if (lKey == null)
		lKey = "";

WState result = null;
String[] lArgs = new String[2];
lArgs[0] = service_wsdl;
boolean lUseClient = helper.useClient(request.getParameter("environment"));
if (lUseClient) {
	EmergoServices_Client bmss = new EmergoServices_Client(lArgs);
	result = bmss.getStateByKey(
			  lRgaId,
			  lKey);
}
else {
//  EmergoServices bmss = new EmergoServices();
	EmergoServices_Client bmss = new EmergoServices_Client(lArgs);
	result = bmss.getStateByKey(
			  lRgaId,
			  lKey);
}

PrintWriter csv = response.getWriter();
csv.println("<html>");
csv.println("<head>");
csv.println("<title></title>");
csv.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">");
csv.println("</head>");
csv.println("<body>");
if (!lUseClient)
  csv.println("Use Server!<br>");
if (result == null)
  csv.println("<b>Result is empty</b>");
else {
csv.println("<table border=1>");
csv.println("<tr>");
csv.println("<td><b>"+"Key"+"</b></td>");
csv.println("<td><b>"+"Type"+"</b></td>");
csv.println("<td><b>"+"Value"+"</b></td>");
csv.println("</tr>");
csv.println("<tr>");
csv.println("<td>"+result.getKey()+"</td>");
csv.println("<td>"+result.getType()+"</td>");
csv.println("<td>"+result.getValue()+"</td>");
csv.println("</tr>");
csv.println("</table>");
}
csv.println("</body>");
csv.println("</html>");
%>