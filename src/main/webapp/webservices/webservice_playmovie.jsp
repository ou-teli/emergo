<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="nl.surf.emergo.webservices.EmergoServicesHelper" %>
<%@ page import="nl.surf.emergo.webservices.client.*" %>

<%@ include file="webservices_settings_inc.jsp" %>

<%
EmergoServicesHelper helper = new EmergoServicesHelper();
String lCasus = request.getParameter("casus");
if (lCasus == null)
	lCasus = "";
String lStudentid = request.getParameter("studentid");
if (lStudentid == null)
	lStudentid = "";
String lFilmpje = request.getParameter("filmpje");
if (lFilmpje == null)
	lFilmpje = "";

String result = "";
String[] lArgs = new String[2];
lArgs[0] = service_wsdl;
boolean lUseClient = helper.useClient(request.getParameter("environment"));
if (lUseClient) {
	EmergoServices_Client bmss = new EmergoServices_Client(lArgs);
	result = bmss.playMovie(
			  lCasus,
			  lStudentid,
			  lFilmpje);
}
else {
//  EmergoServices bmss = new EmergoServices();
	EmergoServices_Client bmss = new EmergoServices_Client(lArgs);
	result = bmss.playMovie(
			  lCasus,
			  lStudentid,
			  lFilmpje);
}

PrintWriter csv = response.getWriter();
csv.println("<html>");
csv.println("<head>");
csv.println("<title></title>");
csv.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">");
csv.println("</head>");
csv.println("<body>");
if (!lUseClient)
  csv.println("Use Server!<br>");
csv.println("<b>" + result + "</b>");
csv.println("</body>");
csv.println("</html>");
%>