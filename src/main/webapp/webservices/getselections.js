function getSelItem(list) {
  var values = list.options[list.selectedIndex].value;
  return values;
}

function getSelItems(list) {
  var values = '';
  for (i=0;i<list.options.length;i++) {
    var option = list.options[i];
    if (option.selected) {
      var value = option.value;
      if (values != '')
        values = values + ';';
      values = values + value;
    }
  }
  return values;
}

function getText(field) {
  var value = field.value;
  return value;
}

function getUserId() {
  return getText(document.test.userid);
}

function getPassword() {
  return getText(document.test.password);
}

function getRgaId() {
  return getText(document.test.rgaid);
}

function getSensorData() {
  return getText(document.test.sensordata);
}

function getCaseComponentName() {
  return getText(document.test.casecomponentname);
}

function getLatitude() {
  return getText(document.test.latitude);
}

function getLongitude() {
  return getText(document.test.longitude);
}

function getKey() {
  return getText(document.test.key);
}

function getId() {
  return getText(document.test.id);
}

function getValue() {
  return getText(document.test.value);
}



function getCasus() {
	  return getText(document.test.casus);
	}

function getStudentid() {
	  return getText(document.test.studentid);
	}

function getFilmpje() {
	  return getText(document.test.filmpje);
	}

