<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="nl.surf.emergo.webservices.EmergoServicesHelper" %>
<%@ page import="nl.surf.emergo.webservices.client.*" %>

<%@ include file="webservices_settings_inc.jsp" %>

<%
EmergoServicesHelper helper = new EmergoServicesHelper();
String lRgaId = request.getParameter("rgaid");
if ((lRgaId == null) || (lRgaId.equals("")))
		lRgaId = "0";
String lLatitude = request.getParameter("latitude");
if ((lLatitude == null) || (lLatitude.equals("")))
		lLatitude = "0";
String lLongitude = request.getParameter("longitude");
if ((lLongitude == null) || (lLongitude.equals("")))
		lLongitude = "0";
  
List<Url> result = null;
String[] lArgs = new String[2];
lArgs[0] = service_wsdl;
boolean lUseClient = helper.useClient(request.getParameter("environment"));
if (lUseClient) {
	EmergoServices_Client bmss = new EmergoServices_Client(lArgs);
	result = bmss.getUrlsByPosition(
			  lRgaId,
			  lLatitude,
			  lLongitude);
}
else {
//  EmergoServices bmss = new EmergoServices();
	EmergoServices_Client bmss = new EmergoServices_Client(lArgs);
	result = bmss.getUrlsByPosition(
			  lRgaId,
			  lLatitude,
			  lLongitude);
}

PrintWriter csv = response.getWriter();
csv.println("<html>");
csv.println("<head>");
csv.println("<title></title>");
csv.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">");
csv.println("</head>");
csv.println("<body>");
if (!lUseClient)
  csv.println("Use Server!<br>");
if ((result == null) || (result.size() == 0))
  csv.println("<b>Result is empty</b>");
else {
csv.println("<table border=1>");
csv.println("<tr>");
csv.println("<td><b>"+"Url"+"</b></td>");
csv.println("<td><b>"+"Description"+"</b></td>");
csv.println("<td><b>"+"Latitude"+"</b></td>");
csv.println("<td><b>"+"Longitude"+"</b></td>");
csv.println("</tr>");
for (int i=0;i<result.size();i++) {
	Url item = result.get(i);
	csv.println("<tr>");
	csv.println("<td>"+item.getUrl()+"</td>");
	csv.println("<td>"+item.getDescription()+"</td>");
	csv.println("<td>"+item.getLatitude()+"</td>");
	csv.println("<td>"+item.getLongitude()+"</td>");
	csv.println("</tr>");
}
csv.println("</table>");
}
csv.println("</body>");
csv.println("</html>");
%>