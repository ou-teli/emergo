<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="nl.surf.emergo.webservices.EmergoServicesHelper" %>
<%@ page import="nl.surf.emergo.webservices.client.*" %>

<%@ include file="webservices_settings_inc.jsp" %>

<%
EmergoServicesHelper helper = new EmergoServicesHelper();
String lUserId = request.getParameter("userid");
if ((lUserId == null) || (lUserId.equals("")))
	lUserId = "0";
String lPassword = request.getParameter("password");
if ((lPassword == null) || (lPassword.equals("")))
	lPassword = "0";

List<WRun> result = null;
String[] lArgs = new String[2];
lArgs[0] = service_wsdl;
boolean lUseClient = helper.useClient(request.getParameter("environment"));
if (lUseClient) {
	EmergoServices_Client bmss = new EmergoServices_Client(lArgs);
	result = bmss.getRunsForCRM(
			  lUserId,
			  lPassword);
}
else {
//  EmergoServices bmss = new EmergoServices();
	EmergoServices_Client bmss = new EmergoServices_Client(lArgs);
	result = bmss.getRunsForCRM(
			  lUserId,
			  lPassword);
}

PrintWriter csv = response.getWriter();
csv.println("<html>");
csv.println("<head>");
csv.println("<title></title>");
csv.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">");
csv.println("</head>");
csv.println("<body>");
if (!lUseClient)
  csv.println("Use Server!<br>");
if ((result == null) || (result.size() == 0))
  csv.println("<b>Result is empty</b>");
else {
csv.println("<table border=1>");
csv.println("<tr>");
csv.println("<td><b>"+"Id"+"</b></td>");
csv.println("<td><b>"+"Code"+"</b></td>");
csv.println("<td><b>"+"Name"+"</b></td>");
csv.println("<td><b>"+"Run Group Accounts"+"</b></td>");
csv.println("</tr>");
for (int i=0;i<result.size();i++) {
  WRun item = result.get(i);
  csv.println("<tr>");
  csv.println("<td>"+item.getId()+"</td>");
  csv.println("<td>"+item.getCode()+"</td>");
  csv.println("<td>"+item.getName()+"</td>");

  csv.println("<td>");
  csv.println("<table border=1>");
  csv.println("<tr>");
  csv.println("<td><b>"+"Id"+"</b></td>");
  csv.println("<td><b>"+"Name"+"</b></td>");
  csv.println("</tr>");
  for (int j=0;j<item.getRgas().size();j++) {
	  WRunGroupAccount item2 = item.getRgas().get(j);
	  csv.println("<tr>");
	  csv.println("<td>"+item2.getId()+"</td>");
	  csv.println("<td>"+item2.getName()+"</td>");
	  csv.println("</tr>");
  }
  csv.println("</table>");
  csv.println("</td>");

  csv.println("</tr>");
}
csv.println("</table>");
}
csv.println("</body>");
csv.println("</html>");
%>