import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.Collection;
import java.util.Iterator;

import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Treechildren;
import org.zkoss.zul.Treeitem;
import org.zkoss.util.media.Media;

import nl.surf.emergo.control.CControl;
import nl.surf.emergo.webservices.EmergoServicesHelper;
import nl.surf.emergo.webservices.client.*;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;

public class CWsHelper {

	protected EmergoServicesHelper helper = new EmergoServicesHelper();
	protected String emergo_url = "http://localhost/emergo/";
	protected String service_url = emergo_url + "services/EmergoServices";
	protected String service_namespace = emergo_url + "services";
	protected String service_wsdl = service_url + "?wsdl";
	String[] lArgs = new String[2];
	lArgs[0] = service_wsdl;
	protected EmergoServicesClient bmss = new EmergoServices_Client(lArgs);
	protected String rgaId = "116";
	protected Exlocation[] exlocations = bmss.getAllExlocations(rgaId);

	public byte[] mediaToByteArray(Media aMedia) {
		try {
			InputStream lStream = null;
			Reader lTest = null;
			if (aMedia.isBinary()) {
				lStream = aMedia.getStreamData();
				int lLength = lStream.available();
				byte[] lBytes = new byte[lLength];
				if (lStream.read(lBytes, 0, lLength) == lLength)
					return lBytes;
			} else {
				lTest = aMedia.getReaderData();
				int lMax = 0;
				int lCnt = 60000;
				char[] lChar = new char[lCnt];
				while (lCnt > -1) {
					lCnt = lTest.read(lChar, 0, lCnt);
					if (lCnt > -1) {
						lMax = lMax + lCnt;
					}
				}
				if (lMax > 0) {
					lTest.reset();
					char[] lChar2 = new char[lMax];
					lTest.read(lChar2);
					byte[] lBytes = new byte[lMax];
					for (int lInd = 0; lInd < lMax; lInd++) {
						lBytes[lInd] = (byte) lChar2[lInd];
					}
					return lBytes;
				}
			}

		} catch (FileNotFoundException fnfe) {
		} catch (IOException ioe) {
		}
		return null;
	}

	public Exlocation[] getExlocations() {
		return exlocations;
	}

	public Exlocation getStartExlocation() {
		if (exlocations == null)
			return null;
		for (int i=0;i<exlocations.length;i++) {
			if (exlocations[i].getName().equals("Heerlen"))
				return exlocations[i];
		}
		return null;
	}

	public Url[] getContextData(double aLatitude, double aLongitude) {
		return bmss.getContextData(rgaId, ""+aLatitude, ""+aLongitude);
	}

	public Url[] getAllUrls() {
		return bmss.getAllUrls(rgaId);
	}

	public void setUrl(Media aMedia, double aLatitude, double aLongitude) {
		Url lUrl = new Url();
		lUrl.setName(((Media) media).getName());
		lUrl.setUrl("");
		lUrl.setData(mediaToByteArray(aMedia));
		lUrl.setDescription("");
		lUrl.setLatitude(""+aLatitude);
		lUrl.setLongitude(""+aLongitude);
		bmss.setUrl(rgaId, lUrl);
	}

	public void deleteUrl(Url) {
		bmss.deleteUrl(rgaId, Url);
	}
}

CWsHelper cWsHelper = new CWsHelper();

public class CZkListcell extends Listcell {

	public void onClick(Event aEvent) {
//		getParent().setAttribute("listcellclicked",getId());
		Url url = (Url)getParent().getAttribute("url");
		if (getId().equals("name")) {
			cZkHelper.showUrl(url);
		}
		if ((getId().equals("latitude")) || (getId().equals("longitude"))) {
			cZkHelper.getComponent("mymap").panTo(Double.parseDouble(url.getLatitude()), Double.parseDouble(url.getLongitude()));
		}
	}
}

public class CZkHelper {

	public Object getComponent(String aId) {
		if (Executions.getCurrent() == null)
			return null;
		Collection lComponents = Executions.getCurrent().getDesktop()
				.getComponents();
		if (lComponents != null) {
			for (Iterator it = lComponents.iterator(); it.hasNext();) {
				Component lComponent = (Component) it.next();
				if (lComponent.getId().equals(aId))
					return lComponent;
			}
		}
		return null;
	}

	public void showUrls(Listbox aListbox, Url[] aUrls) {
		aListbox.setVisible(false);
		aListbox.getChildren().clear();
		if ((aUrls == null) || (aUrls.length == 0))
			return;
		for (int i=0;i<aUrls.length;i++) {
			Listitem lListitem = new Listitem();
			aListbox.appendChild(lListitem);
			lListitem.setAttribute("url",aUrls[i]);
			Listcell lListcell = new Listcell();
//			Listcell lListcell = new CZkListcell();
			lListitem.appendChild(lListcell);
//			lListcell.setId("name");
			lListcell.setLabel(aUrls[i].getName());
			lListcell = new Listcell();
//			lListcell = new CZkListcell();
			lListitem.appendChild(lListcell);
//			lListcell.setId("latitude");
			lListcell.setLabel(aUrls[i].getLatitude());
			lListcell = new Listcell();
//			lListcell = new CZkListcell();
			lListitem.appendChild(lListcell);
//			lListcell.setId("longitude");
			lListcell.setLabel(aUrls[i].getLongitude());
		}
		aListbox.setVisible(true);
	}
	
	public void showUrl(Url aUrl) {
  		Executions.getCurrent().sendRedirect(aUrl.getUrl(), "null");
	}
}

CZkHelper cZkHelper = new CZkHelper();

