<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="nl.surf.emergo.webservices.EmergoServicesHelper" %>
<%@ page import="nl.surf.emergo.webservices.client.*" %>

<%@ include file="webservices_settings_inc.jsp" %>

<%
EmergoServicesHelper helper = new EmergoServicesHelper();
String lRgaId = request.getParameter("rgaid");
if ((lRgaId == null) || (lRgaId.equals("")))
	lRgaId = "0";
String lId = request.getParameter("id");
String lName = request.getParameter("name");
lRgaId = "116";
lId = "3";
lName = "";
Url lUrl = new Url();
lUrl.setId(lId);
lUrl.setName(lName);

String result = "";
String[] lArgs = new String[2];
lArgs[0] = service_wsdl;
boolean lUseClient = helper.useClient(request.getParameter("environment"));
if (lUseClient) {
	EmergoServices_Client bmss = new EmergoServices_Client(lArgs);
	result = bmss.deleteUrl(lRgaId,lUrl);
}
else {
//  EmergoServices bmss = new EmergoServices();
	EmergoServices_Client bmss = new EmergoServices_Client(lArgs);
	result = bmss.deleteUrl(lRgaId,lUrl);
}

PrintWriter csv = response.getWriter();
csv.println("<html>");
csv.println("<head>");
csv.println("<title></title>");
csv.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">");
csv.println("</head>");
csv.println("<body>");
if (!lUseClient)
  csv.println("Use Server!<br>");
if ((result == null) || (result.length() == 0))
  csv.println("<b>Result is empty</b>");
else {
  csv.println(result);
}
csv.println("</body>");
csv.println("</html>");
%>