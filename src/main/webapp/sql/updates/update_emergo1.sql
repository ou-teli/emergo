<<<emergo,dropTable,courseaccountroles>>>DROP TABLE IF EXISTS `courseaccountroles`;
<<<emergo,dropTable,courseaccounts>>>DROP TABLE IF EXISTS `courseaccounts`;
<<<emergo,dropTable,coursecases>>>DROP TABLE IF EXISTS `coursecases`;
<<<emergo,dropTable,courseroles>>>DROP TABLE IF EXISTS `courseroles`;
<<<emergo,dropTable,courses>>>DROP TABLE IF EXISTS `courses`;
<<<emergo,dropTable,institutions>>>DROP TABLE IF EXISTS `institutions`;
<<<emergo,dropTable,languages>>>DROP TABLE IF EXISTS `languages`;
<<<emergo,dropTable,texts>>>DROP TABLE IF EXISTS `texts`;

<<<emergo,dropColumn,blobs,Content>>>ALTER TABLE `blobs` DROP COLUMN `Content`;
<<<emergo,dropColumn,casecomponentroles,Xmldata>>>ALTER TABLE `casecomponentroles` DROP COLUMN `Xmldata`, ROW_FORMAT = DYNAMIC;
<<<emergo,dropColumn,caseroles,Xmldescription>>>ALTER TABLE `caseroles` DROP COLUMN `Xmldescription`, ROW_FORMAT = DYNAMIC;
<<<emergo,dropColumn,cases,Xmldescription>>>ALTER TABLE `cases` DROP COLUMN `Xmldescription`, ROW_FORMAT = DYNAMIC;
<<<emergo,dropColumn,components,Name>>>ALTER TABLE `components` DROP COLUMN `Name`, ROW_FORMAT = DYNAMIC;
<<<emergo,dropColumn,components,Xmldescription>>>ALTER TABLE `components` DROP COLUMN `Xmldescription`, ROW_FORMAT = DYNAMIC;
<<<emergo,dropColumn,roles,Description>>>ALTER TABLE `roles` DROP COLUMN `Description`, ROW_FORMAT = DYNAMIC;
<<<emergo,dropColumn,runs,Running>>>ALTER TABLE `runs` DROP COLUMN `Running`, ROW_FORMAT = DYNAMIC;
