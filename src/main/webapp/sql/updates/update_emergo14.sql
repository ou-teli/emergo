<<<emergo,alterTable,accountcontexts>>>ALTER TABLE `accountcontexts` CHARACTER SET = utf8mb4;
<<<emergo,alterTable,accountcontexts>>>ALTER TABLE `accountcontexts` CONVERT TO CHARACTER SET utf8mb4;

<<<emergo,alterTable,accountrequests>>>ALTER TABLE `accountrequests` CHARACTER SET = utf8mb4;
<<<emergo,alterTable,accountrequests>>>ALTER TABLE `accountrequests` CONVERT TO CHARACTER SET utf8mb4;

<<<emergo,alterTable,accountroles>>>ALTER TABLE `accountroles` CHARACTER SET = utf8mb4;
<<<emergo,alterTable,accountroles>>>ALTER TABLE `accountroles` CONVERT TO CHARACTER SET utf8mb4;

<<<emergo,alterTable,accounts>>>ALTER TABLE `accounts` CHARACTER SET = utf8mb4;
<<<emergo,alterTable,accounts>>>ALTER TABLE `accounts` CONVERT TO CHARACTER SET utf8mb4;

<<<emergo,alterTable,blobs>>>ALTER TABLE `blobs` CHARACTER SET = utf8mb4;
<<<emergo,alterTable,blobs>>>ALTER TABLE `blobs` CONVERT TO CHARACTER SET utf8mb4;

<<<emergo,alterTable,casecomponentroles>>>ALTER TABLE `casecomponentroles` CHARACTER SET = utf8mb4;
<<<emergo,alterTable,casecomponentroles>>>ALTER TABLE `casecomponentroles` CONVERT TO CHARACTER SET utf8mb4;

<<<emergo,alterTable,casecomponents>>>ALTER TABLE `casecomponents` CHARACTER SET = utf8mb4;
<<<emergo,alterTable,casecomponents>>>ALTER TABLE `casecomponents` CONVERT TO CHARACTER SET utf8mb4;

<<<emergo,alterTable,caseroles>>>ALTER TABLE `caseroles` CHARACTER SET = utf8mb4;
<<<emergo,alterTable,caseroles>>>ALTER TABLE `caseroles` CONVERT TO CHARACTER SET utf8mb4;

<<<emergo,alterTable,cases>>>ALTER TABLE `cases` CHARACTER SET = utf8mb4;
<<<emergo,alterTable,cases>>>ALTER TABLE `cases` CONVERT TO CHARACTER SET utf8mb4;

<<<emergo,alterTable,components>>>ALTER TABLE `components` CHARACTER SET = utf8mb4;
<<<emergo,alterTable,components>>>ALTER TABLE `components` CONVERT TO CHARACTER SET utf8mb4;

<<<emergo,alterTable,contexts>>>ALTER TABLE `contexts` CHARACTER SET = utf8mb4;
<<<emergo,alterTable,contexts>>>ALTER TABLE `contexts` CONVERT TO CHARACTER SET utf8mb4;

<<<emergo,alterTable,mails>>>ALTER TABLE `mails` CHARACTER SET = utf8mb4;
<<<emergo,alterTable,mails>>>ALTER TABLE `mails` CONVERT TO CHARACTER SET utf8mb4;

<<<emergo,alterTable,roles>>>ALTER TABLE `roles` CHARACTER SET = utf8mb4;
<<<emergo,alterTable,roles>>>ALTER TABLE `roles` CONVERT TO CHARACTER SET utf8mb4;

<<<emergo,alterTable,runaccounts>>>ALTER TABLE `runaccounts` CHARACTER SET = utf8mb4;
<<<emergo,alterTable,runaccounts>>>ALTER TABLE `runaccounts` CONVERT TO CHARACTER SET utf8mb4;

<<<emergo,alterTable,runcasecomponents>>>ALTER TABLE `runcasecomponents` CHARACTER SET = utf8mb4;
<<<emergo,alterTable,runcasecomponents>>>ALTER TABLE `runcasecomponents` CONVERT TO CHARACTER SET utf8mb4;

<<<emergo,alterTable,runcontexts>>>ALTER TABLE `runcontexts` CHARACTER SET = utf8mb4;
<<<emergo,alterTable,runcontexts>>>ALTER TABLE `runcontexts` CONVERT TO CHARACTER SET utf8mb4;

<<<emergo,alterTable,rungroupaccounts>>>ALTER TABLE `rungroupaccounts` CHARACTER SET = utf8mb4;
<<<emergo,alterTable,rungroupaccounts>>>ALTER TABLE `rungroupaccounts` CONVERT TO CHARACTER SET utf8mb4;

<<<emergo,alterTable,rungroupcasecomponents>>>ALTER TABLE `rungroupcasecomponents` CHARACTER SET = utf8mb4;
<<<emergo,alterTable,rungroupcasecomponents>>>ALTER TABLE `rungroupcasecomponents` CONVERT TO CHARACTER SET utf8mb4;

<<<emergo,alterTable,rungroupcasecomponentupdates>>>ALTER TABLE `rungroupcasecomponentupdates` CHARACTER SET = utf8mb4;
<<<emergo,alterTable,rungroupcasecomponentupdates>>>ALTER TABLE `rungroupcasecomponentupdates` CONVERT TO CHARACTER SET utf8mb4;

<<<emergo,alterTable,rungroups>>>ALTER TABLE `rungroups` CHARACTER SET = utf8mb4;
<<<emergo,alterTable,rungroups>>>ALTER TABLE `rungroups` CONVERT TO CHARACTER SET utf8mb4;

<<<emergo,alterTable,runs>>>ALTER TABLE `runs` CHARACTER SET = utf8mb4;
<<<emergo,alterTable,runs>>>ALTER TABLE `runs` CONVERT TO CHARACTER SET utf8mb4;

<<<emergo,alterTable,runteamcasecomponents>>>ALTER TABLE `runteamcasecomponents` CHARACTER SET = utf8mb4;
<<<emergo,alterTable,runteamcasecomponents>>>ALTER TABLE `runteamcasecomponents` CONVERT TO CHARACTER SET utf8mb4;

<<<emergo,alterTable,runteamrungroups>>>ALTER TABLE `runteamrungroups` CHARACTER SET = utf8mb4;
<<<emergo,alterTable,runteamrungroups>>>ALTER TABLE `runteamrungroups` CONVERT TO CHARACTER SET utf8mb4;

<<<emergo,alterTable,runteams>>>ALTER TABLE `runteams` CHARACTER SET = utf8mb4;
<<<emergo,alterTable,runteams>>>ALTER TABLE `runteams` CONVERT TO CHARACTER SET utf8mb4;

<<<emergo,alterTable,sys>>>ALTER TABLE `sys` CHARACTER SET = utf8mb4;
<<<emergo,alterTable,sys>>>ALTER TABLE `sys` CONVERT TO CHARACTER SET utf8mb4;
