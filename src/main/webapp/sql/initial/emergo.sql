CREATE TABLE `blobs` (
  `BloId` int(11) NOT NULL auto_increment,
  `Name` text NOT NULL,
  `Filename` text NOT NULL,
  `Contenttype` text NOT NULL,
  `Format` text NOT NULL,
  `Url` text,
  `Creationdate` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `Lastupdatedate` timestamp NOT NULL default '1970-01-02 00:00:01',
  PRIMARY KEY  (`BloId`)
) ENGINE=MyISAM AUTO_INCREMENT=6501 DEFAULT CHARSET=latin1;

CREATE TABLE `mails` (
  `MaiId` int(11) NOT NULL auto_increment,
  `Code` text NOT NULL,
  `Description` text NOT NULL,
  `Subject` text NOT NULL,
  `Body` text NOT NULL,
  `Creationdate` timestamp NOT NULL default '1970-01-02 00:00:01',
  `Lastupdatedate` timestamp NOT NULL default '1970-01-02 00:00:01',
  PRIMARY KEY  (`MaiId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mails` (`MaiId`,`Code`,`Description`,`Subject`,`Body`,`Creationdate`,`Lastupdatedate`) VALUES
 (1,'outmailpredef_sent','An out mail is sent from within the Emergo player.','Emergo: E-bericht ''<subject>'' verzonden door ''<sender>''','\r\n''<sender>'' heeft binnen run ''<runname>'' een e-bericht met onderwerp ''<subject>'' verstuurd.\r\n\r\nDit is een automatisch gegenereerde email. U kunt op deze email niet reageren.','2008-02-01 00:00:00','2008-02-01 00:00:00'),
 (2,'outmailhelp_sent','A help out mail is sent from within the Emergo player.','Emergo: Help e-bericht ''<subject>'' verzonden door ''<sender>''','\r\n''<sender>'' heeft binnen run ''<runname>'' een e-bericht met onderwerp ''<subject>'' verstuurd.\r\n\r\nDit is een automatisch gegenereerde email. U kunt op deze email niet reageren.','2008-02-01 00:00:00','2008-02-01 00:00:00'),
 (3,'inmailpredef_sent','An in mail is sent from within the Emergo player.','Emergo: E-bericht ''<subject>'' verzonden door ''<sender>''','\r\n''<sender>'' heeft binnen run ''<runname>'' een e-bericht met onderwerp ''<subject>'' verstuurd.\r\n\r\nDit is een automatisch gegenereerde email. U kunt op deze email niet reageren.','2008-02-01 00:00:00','2008-02-01 00:00:00'),
 (4,'inmailhelp_sent','A help in mail is sent from within the Emergo player.','Emergo: Help e-bericht ''<subject>'' verzonden door ''<sender>''','\r\n''<sender>'' heeft binnen run ''<runname>'' een e-bericht met onderwerp ''<subject>'' verstuurd.\r\n\r\nDit is een automatisch gegenereerde email. U kunt op deze email niet reageren.','2008-02-01 00:00:00','2008-02-01 00:00:00');

CREATE TABLE `sys` (
  `SysId` int(11) NOT NULL auto_increment,
  `Syskey` text NOT NULL,
  `Sysvalue` text NOT NULL,
  `Creationdate` timestamp NOT NULL default '1970-01-02 00:00:01',
  `Lastupdatedate` timestamp NOT NULL default '1970-01-02 00:00:01',
  PRIMARY KEY  (`SysId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `sys` (`SysId`,`Syskey`,`Sysvalue`,`Creationdate`,`Lastupdatedate`) VALUES
 (1,'smtpserver','','2013-01-01 00:00:00','2013-01-01 00:00:00'),
 (2,'smtpnoreplysender','','2013-01-01 00:00:00','2013-01-01 00:00:00'),
 (3,'sqllastupdatenumber','8','2013-01-01 00:00:00','2013-01-01 00:00:00');

 
CREATE TABLE `accounts` (
  `AccId` int(11) NOT NULL auto_increment,
  `AccAccId` int(11) NOT NULL default '0',
  `Userid` varchar(50) NOT NULL default '',
  `Password` varchar(100) NOT NULL default '',
  `Title` text NOT NULL,
  `Initials` text NOT NULL,
  `Nameprefix` text NOT NULL,
  `Lastname` text NOT NULL,
  `Email` text NOT NULL,
  `Phonenumber` text NOT NULL,
  `Job` text NOT NULL,
  `Active` tinyint(1) NOT NULL default '1',
  `Openaccess` tinyint(1) NOT NULL default '1',
  `Creationdate` timestamp NOT NULL default '1970-01-02 00:00:01',
  `Lastupdatedate` timestamp NOT NULL default '1970-01-02 00:00:01',
  PRIMARY KEY  (`AccId`),
  KEY `AccAccId` (`AccAccId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `accounts` (`AccId`,`AccAccId`,`Userid`,`Password`,`Title`,`Initials`,`Nameprefix`,`Lastname`,`Email`,`Phonenumber`,`Job`,`Active`,`Creationdate`,`Lastupdatedate`) VALUES
 (1,1,'admin','$2a$10$xPLf.mle1UgfaZzyxR1ueeSwmV0jC0WlnDnp.cu8OfeehkDxKoWPC','','','','Administrator','','','',1,'2013-01-01 00:00:00','2013-01-01 00:00:00');

CREATE TABLE `accountrequests` (
  `ArqId` int(11) NOT NULL auto_increment,
  `Userid` varchar(50) NOT NULL default '',
  `Password` varchar(50) NOT NULL default '',
  `Title` text NOT NULL,
  `Initials` text NOT NULL,
  `Nameprefix` text NOT NULL,
  `Lastname` text NOT NULL,
  `Email` text NOT NULL,
  `Processed` tinyint(1) NOT NULL default '0',
  `Creationdate` timestamp NOT NULL default '1970-01-02 00:00:01',
  `Lastupdatedate` timestamp NOT NULL default '1970-01-02 00:00:01',
  PRIMARY KEY  (`ArqId`)
) ENGINE=InnoDB;

CREATE TABLE `cases` (
  `CasId` int(11) NOT NULL auto_increment,
  `AccAccId` int(11) NOT NULL default '0',
  `CasCasId` int(11) NOT NULL,
  `Code` text NOT NULL,
  `Name` text NOT NULL,
  `Version` int(11) NOT NULL default '1',
  `Status` int(11) NOT NULL default '0',
  `Active` tinyint(1) NOT NULL default '1',
  `Skin` text NOT NULL,
  `Creationdate` timestamp NOT NULL default '1970-01-02 00:00:01',
  `Lastupdatedate` timestamp NOT NULL default '1970-01-02 00:00:01',
  PRIMARY KEY  (`CasId`),
  KEY `AccAccId` (`AccAccId`),
  CONSTRAINT `FKCasAccAccId` FOREIGN KEY (`AccAccId`) REFERENCES `accounts` (`AccId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `components` (
  `ComId` int(11) NOT NULL auto_increment,
  `AccAccId` int(11) NOT NULL default '0',
  `ComComId` int(11) NOT NULL default '0',
  `Code` text NOT NULL,
  `Uuid` text NOT NULL,
  `Type` int(11) NOT NULL default '1',
  `Version` int(11) NOT NULL default '1',
  `Active` tinyint(1) NOT NULL default '1',
  `Creationdate` timestamp NOT NULL default '1970-01-02 00:00:01',
  `Lastupdatedate` timestamp NOT NULL default '1970-01-02 00:00:01',
  `Xmldefinition` mediumtext NOT NULL,
  `Multiple` tinyint(1) unsigned NOT NULL default '0',
  PRIMARY KEY  (`ComId`),
  KEY `AccAccId` (`AccAccId`),
  KEY `ComComId` (`ComComId`),
  CONSTRAINT `FKComAccAccId` FOREIGN KEY (`AccAccId`) REFERENCES `accounts` (`AccId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `contexts` (
  `ConId` INT(11) NOT NULL AUTO_INCREMENT,
  `Context` TEXT NOT NULL,
  `Active` tinyint(1) NOT NULL default '1',
  `Creationdate` TIMESTAMP NOT NULL DEFAULT '1970-01-02 00:00:01',
  `Lastupdatedate` TIMESTAMP NOT NULL DEFAULT '1970-01-02 00:00:01',
  PRIMARY KEY  USING BTREE(`ConId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `roles` (
  `RolId` int(11) NOT NULL auto_increment,
  `Code` text NOT NULL,
  `Creationdate` timestamp NOT NULL default '1970-01-02 00:00:01',
  `Lastupdatedate` timestamp NOT NULL default '1970-01-02 00:00:01',
  PRIMARY KEY  (`RolId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `roles` (`RolId`,`Code`,`Creationdate`,`Lastupdatedate`) VALUES
 (1,'adm','2013-01-01 00:00:00','2013-01-01 00:00:00'),
 (2,'cde','2013-01-01 00:00:00','2013-01-01 00:00:00'),
 (3,'crm','2013-01-01 00:00:00','2013-01-01 00:00:00'),
 (4,'tut','2013-01-01 00:00:00','2013-01-01 00:00:00'),
 (5,'stu','2013-01-01 00:00:00','2013-01-01 00:00:00');

CREATE TABLE `accountcontexts` (
  `AccId` int(11) NOT NULL auto_increment,
  `AccAccId` int(11) NOT NULL default '0',
  `ConConId` int(11) NOT NULL default '0',
  `Active` tinyint(1) NOT NULL default '1',
  `Creationdate` timestamp NOT NULL default '1970-01-02 00:00:01',
  `Lastupdatedate` timestamp NOT NULL default '1970-01-02 00:00:01',
  PRIMARY KEY  USING BTREE (`AccId`),
  KEY `AccAccId` (`AccAccId`),
  KEY `ConConId` (`ConConId`),
  CONSTRAINT `FKAccAccAccId` FOREIGN KEY (`AccAccId`) REFERENCES `accounts` (`AccId`) ON DELETE CASCADE,
  CONSTRAINT `FKConConId` FOREIGN KEY (`ConConId`) REFERENCES `contexts` (`ConId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

CREATE TABLE `accountroles` (
  `AcrId` int(11) NOT NULL auto_increment,
  `AccAccId` int(11) NOT NULL default '0',
  `RolRolId` int(11) NOT NULL default '0',
  `Creationdate` timestamp NOT NULL default '1970-01-02 00:00:01',
  `Lastupdatedate` timestamp NOT NULL default '1970-01-02 00:00:01',
  PRIMARY KEY  (`AcrId`),
  KEY `AccAccId` (`AccAccId`),
  KEY `RolRolId` (`RolRolId`),
  CONSTRAINT `FKAcrAccAccId` FOREIGN KEY (`AccAccId`) REFERENCES `accounts` (`AccId`) ON DELETE CASCADE,
  CONSTRAINT `FKAcrRolRolId` FOREIGN KEY (`RolRolId`) REFERENCES `roles` (`RolId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `accountroles` (`AcrId`,`AccAccId`,`RolRolId`,`Creationdate`,`Lastupdatedate`) VALUES
 (1,1,1,'2013-01-01 00:00:00','2013-01-01 00:00:00'),
 (2,1,2,'2013-01-01 00:00:00','2013-01-01 00:00:00'),
 (3,1,3,'2013-01-01 00:00:00','2013-01-01 00:00:00'),
 (4,1,4,'2013-01-01 00:00:00','2013-01-01 00:00:00'),
 (5,1,5,'2013-01-01 00:00:00','2013-01-01 00:00:00');

CREATE TABLE `casecomponents` (
  `CacId` int(11) NOT NULL auto_increment,
  `CasCasId` int(11) NOT NULL default '0',
  `ComComId` int(11) NOT NULL default '0',
  `AccAccId` int(11) NOT NULL default '0',
  `Name` text NOT NULL,
  `Xmldata` mediumtext NOT NULL,
  `Creationdate` timestamp NOT NULL default '1970-01-02 00:00:01',
  `Lastupdatedate` timestamp NOT NULL default '1970-01-02 00:00:01',
  PRIMARY KEY  (`CacId`),
  KEY `CasCasId` (`CasCasId`),
  KEY `ComComId` (`ComComId`),
  KEY `AccAccId` (`AccAccId`),
  CONSTRAINT `FKCacAccAccId` FOREIGN KEY (`AccAccId`) REFERENCES `accounts` (`AccId`) ON DELETE CASCADE,
  CONSTRAINT `FKCacCasCasId` FOREIGN KEY (`CasCasId`) REFERENCES `cases` (`CasId`) ON DELETE CASCADE,
  CONSTRAINT `FKCacComComId` FOREIGN KEY (`ComComId`) REFERENCES `components` (`ComId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `caseroles` (
  `CarId` int(11) NOT NULL auto_increment,
  `CasCasId` int(11) NOT NULL default '0',
  `Name` text NOT NULL,
  `Npc` tinyint(1) NOT NULL default '0',
  `Creationdate` timestamp NOT NULL default '1970-01-02 00:00:01',
  `Lastupdatedate` timestamp NOT NULL default '1970-01-02 00:00:01',
  PRIMARY KEY  (`CarId`),
  KEY `CasCasId` (`CasCasId`),
  CONSTRAINT `FKCarCasCasId` FOREIGN KEY (`CasCasId`) REFERENCES `cases` (`CasId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `casecomponentroles` (
  `CcrId` int(11) NOT NULL auto_increment,
  `CacCacId` int(11) NOT NULL default '0',
  `CarCarId` int(11) NOT NULL default '0',
  `Name` text NOT NULL,
  `Creationdate` timestamp NOT NULL default '1970-01-02 00:00:01',
  `Lastupdatedate` timestamp NOT NULL default '1970-01-02 00:00:01',
  PRIMARY KEY  USING BTREE (`CcrId`),
  KEY `CacCacId` (`CacCacId`),
  KEY `CarCarId` (`CarCarId`),
  CONSTRAINT `FKCcrCacCacId` FOREIGN KEY (`CacCacId`) REFERENCES `casecomponents` (`CacId`) ON DELETE CASCADE,
  CONSTRAINT `FKCcrCarCarId` FOREIGN KEY (`CarCarId`) REFERENCES `caseroles` (`CarId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `runs` (
  `RunId` int(11) NOT NULL auto_increment,
  `AccAccId` int(11) NOT NULL default '0',
  `CasCasId` int(11) NOT NULL default '0',
  `Code` text NOT NULL,
  `Name` text NOT NULL,
  `Startdate` timestamp NOT NULL default '1970-01-02 00:00:01',
  `Enddate` timestamp NOT NULL default '1970-01-02 00:00:01',
  `Status` int(11) NOT NULL default '0',
  `Active` tinyint(1) NOT NULL default '1',
  `Openaccess` tinyint(1) default '0',
  `Accessmailsubject` text NOT NULL,
  `Accessmailbody` text NOT NULL,
  `Creationdate` timestamp NOT NULL default '1970-01-02 00:00:01',
  `Lastupdatedate` timestamp NOT NULL default '1970-01-02 00:00:01',
  PRIMARY KEY  (`RunId`),
  KEY `AccAccId` (`AccAccId`),
  KEY `CasCasId` (`CasCasId`),
  CONSTRAINT `FKRunAccAccId` FOREIGN KEY (`AccAccId`) REFERENCES `accounts` (`AccId`) ON DELETE CASCADE,
  CONSTRAINT `FKRunCasCasId` FOREIGN KEY (`CasCasId`) REFERENCES `cases` (`CasId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `runcasecomponents` (
  `RccId` int(11) NOT NULL auto_increment,
  `RunRunId` int(11) NOT NULL default '0',
  `CacCacId` int(11) NOT NULL default '0',
  `Xmldata` mediumtext NOT NULL,
  `Creationdate` timestamp NOT NULL default '1970-01-02 00:00:01',
  `Lastupdatedate` timestamp NOT NULL default '1970-01-02 00:00:01',
  PRIMARY KEY  USING BTREE (`RccId`),
  KEY `RunRunId` (`RunRunId`),
  KEY `CacCacId` (`CacCacId`),
  CONSTRAINT `FKRccCacCacId` FOREIGN KEY (`CacCacId`) REFERENCES `casecomponents` (`CacId`) ON DELETE CASCADE,
  CONSTRAINT `FKRccRunRunId` FOREIGN KEY (`RunRunId`) REFERENCES `runs` (`RunId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `runcontexts` (
  `RucId` int(11) NOT NULL auto_increment,
  `RunRunId` int(11) NOT NULL default '0',
  `ConConId` int(11) NOT NULL default '0',
  `Creationdate` timestamp NOT NULL default '1970-01-02 00:00:01',
  `Lastupdatedate` timestamp NOT NULL default '1970-01-02 00:00:01',
  PRIMARY KEY  USING BTREE (`RucId`),
  KEY `RunRunId` USING BTREE (`RunRunId`),
  KEY `ConConId` USING BTREE (`ConConId`),
  CONSTRAINT `FKRucRunRunID` FOREIGN KEY (`RunRunId`) REFERENCES `runs` (`RunId`) ON DELETE CASCADE,
  CONSTRAINT `FKRucConConId` FOREIGN KEY (`ConConId`) REFERENCES `contexts` (`ConId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

CREATE TABLE `rungroups` (
  `RugId` int(11) NOT NULL auto_increment,
  `RunRunId` int(11) NOT NULL default '0',
  `CarCarId` int(11) NOT NULL default '0',
  `Name` text NOT NULL,
  `Composite` tinyint(1) NOT NULL default '1',
  `Active` tinyint(1) NOT NULL default '1',
  `Creationdate` timestamp NOT NULL default '1970-01-02 00:00:01',
  `Lastupdatedate` timestamp NOT NULL default '1970-01-02 00:00:01',
  PRIMARY KEY  (`RugId`),
  KEY `RunRunId` (`RunRunId`),
  KEY `CarCarId` (`CarCarId`),
  CONSTRAINT `FKRugCarCarId` FOREIGN KEY (`CarCarId`) REFERENCES `caseroles` (`CarId`) ON DELETE CASCADE,
  CONSTRAINT `FKRugRunRunId` FOREIGN KEY (`RunRunId`) REFERENCES `runs` (`RunId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `rungroupaccounts` (
  `RgaId` int(11) NOT NULL auto_increment,
  `AccAccId` int(11) NOT NULL default '0',
  `RugRugId` int(11) NOT NULL default '0',
  `Creationdate` timestamp NOT NULL default '1970-01-02 00:00:01',
  `Lastupdatedate` timestamp NOT NULL default '1970-01-02 00:00:01',
  PRIMARY KEY  (`RgaId`),
  KEY `AccAccId` (`AccAccId`),
  KEY `RugRugId` (`RugRugId`),
  CONSTRAINT `FKRgaAccAccId` FOREIGN KEY (`AccAccId`) REFERENCES `accounts` (`AccId`) ON DELETE CASCADE,
  CONSTRAINT `FKRgaRugRugId` FOREIGN KEY (`RugRugId`) REFERENCES `rungroups` (`RugId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `runaccounts` ( 
  `RuaId` INT(11) NOT NULL AUTO_INCREMENT,
  `RunRunId` INT(11) NOT NULL DEFAULT 0,
  `AccAccId` INT(11) NOT NULL DEFAULT 0,
  `StuActive` TINYINT(1) NOT NULL DEFAULT 0,
  `TutActive` TINYINT(1) NOT NULL DEFAULT 0,
  `Creationdate` timestamp NOT NULL default '1970-01-02 00:00:01',
  `Lastupdatedate` timestamp NOT NULL default '1970-01-02 00:00:01',
  PRIMARY KEY (`RuaId`),
  KEY `RunRunId` (`RunRunId` ASC), 
  KEY `AccAccId` (`AccAccId` ASC), 
  CONSTRAINT `FKRuaAccAccId` FOREIGN KEY (`AccAccId`) REFERENCES `accounts` (`AccId`) ON DELETE CASCADE,
  CONSTRAINT `FKRuaRunRunId` FOREIGN KEY (`RunRunId`) REFERENCES `runs` (`RunId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `rungroupcasecomponents` (
  `RgcId` int(11) NOT NULL auto_increment,
  `RugRugId` int(11) NOT NULL default '0',
  `CacCacId` int(11) NOT NULL default '0',
  `Xmldata` mediumtext NOT NULL,
  `Creationdate` timestamp NOT NULL default '1970-01-02 00:00:01',
  `Lastupdatedate` timestamp NOT NULL default '1970-01-02 00:00:01',
  PRIMARY KEY  USING BTREE (`RgcId`),
  KEY `RugRugId` (`RugRugId`),
  KEY `CacCacId` (`CacCacId`),
  CONSTRAINT `FKRgcCacCacId` FOREIGN KEY (`CacCacId`) REFERENCES `casecomponents` (`CacId`) ON DELETE CASCADE,
  CONSTRAINT `FKRgcRugRugId` FOREIGN KEY (`RugRugId`) REFERENCES `rungroups` (`RugId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `rungroupcasecomponentupdates` (
  `RguId` int(11) NOT NULL auto_increment,
  `RugRugId` int(11) NOT NULL default '0',
  `CacCacId` int(11) NOT NULL default '0',
  `RugRugFromId` int(11) NOT NULL default '0',
  `Xmldata` mediumtext NOT NULL,
  `Processed` tinyint(1) NOT NULL default '0',
  `Creationdate` timestamp NOT NULL default '1970-01-02 00:00:01',
  `Lastupdatedate` timestamp NOT NULL default '1970-01-02 00:00:01',
  PRIMARY KEY  USING BTREE (`RguId`),
  KEY `RugRugId` (`RugRugId`),
  KEY `CacCacId` (`CacCacId`),
  KEY `RugRugFromId` (`RugRugFromId`),
  CONSTRAINT `FKRguCacCacId` FOREIGN KEY (`CacCacId`) REFERENCES `casecomponents` (`CacId`) ON DELETE CASCADE,
  CONSTRAINT `FKRguRugRugFromId` FOREIGN KEY (`RugRugFromId`) REFERENCES `rungroups` (`RugId`) ON DELETE CASCADE,
  CONSTRAINT `FKRguRugRugId` FOREIGN KEY (`RugRugId`) REFERENCES `rungroups` (`RugId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `runteams` (
  `RutId` int(11) NOT NULL auto_increment,
  `RunRunId` int(11) NOT NULL default '0',
  `Name` text NOT NULL,
  `Active` tinyint(1) NOT NULL default '1',
  `Creationdate` timestamp NOT NULL default '1970-01-02 00:00:01',
  `Lastupdatedate` timestamp NOT NULL default '1970-01-02 00:00:01',
  PRIMARY KEY  (`RutId`),
  KEY `RunRunId` (`RunRunId`),
  CONSTRAINT `FKRutRunRunId` FOREIGN KEY (`RunRunId`) REFERENCES `runs` (`RunId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `runteamcasecomponents` (
  `RtcId` int(11) NOT NULL auto_increment,
  `RutRutId` int(11) NOT NULL default '0',
  `CacCacId` int(11) NOT NULL default '0',
  `Xmldata` mediumtext NOT NULL,
  `Creationdate` timestamp NOT NULL default '1970-01-02 00:00:01',
  `Lastupdatedate` timestamp NOT NULL default '1970-01-02 00:00:01',
  PRIMARY KEY  USING BTREE (`RtcId`),
  KEY `RutRutId` (`RutRutId`),
  KEY `CacCacId` (`CacCacId`),
  CONSTRAINT `FKRtcCacCacId` FOREIGN KEY (`CacCacId`) REFERENCES `casecomponents` (`CacId`) ON DELETE CASCADE,
  CONSTRAINT `FKRtcRutRutId` FOREIGN KEY (`RutRutId`) REFERENCES `runteams` (`RutId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `runteamrungroups` (
  `RtrId` int(11) NOT NULL auto_increment,
  `RutRutId` int(11) NOT NULL default '0',
  `RugRugId` int(11) NOT NULL default '0',
  `Creationdate` timestamp NOT NULL default '1970-01-02 00:00:01',
  `Lastupdatedate` timestamp NOT NULL default '1970-01-02 00:00:01',
  PRIMARY KEY  (`RtrId`),
  KEY `RutRutId` (`RutRutId`),
  KEY `RugRugId` (`RugRugId`),
  CONSTRAINT `FKRtrRugRugId` FOREIGN KEY (`RugRugId`) REFERENCES `rungroups` (`RugId`) ON DELETE CASCADE,
  CONSTRAINT `FKRtrRutRutId` FOREIGN KEY (`RutRutId`) REFERENCES `runteams` (`RutId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
