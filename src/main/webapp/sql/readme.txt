This folder contains sub folders 'initial' and 'updates'.
The sub folder 'initial' contains the initial emergo.sql: all database tables and some content.
If the Emergo logon screen is opened and the database is empty (database should be created before, see installation manual), this file is read and all database tables are automatically created and some content is added: roles, admin account, mail templates and some system variables. See method InitDatabase within SSpring class.
If the logon screen is opened on localhost emergo.sql is preserved, otherwise it is deleted after it is used.
The file is part of the Emergo installation package.
The file is maintained by an admin manually, for instance if table definitions change or above mentioned content.
For changes in the content of table components another update mechanism is used, see readme.txt in folder components.

The sub folder 'updates' contains sql update files. The updates mostly concern alterations of the database table definitions, but also statements adjusting the content of the database are possible. But not the content of the table components. Another update mechanism is used, see readme.txt in folder components.
Every update file name contains a number indicating the imposed order of the update. Update file number 1 is the first update after ZK framework was updated from version 3 to version 6. So for Emergo versions using ZK framework version 3, this is the first update file to apply.
Update files also contain non-sql parts indicated by <<<>>>. These are meant to prevent sql errors when applying an update file, for instance trying to create a table which already exists. The non-sql part then indicates, it must be checked if the table exists before applying sql statement. The non-sql parts are interpreted and stripped of by method InitDatabase in the SSpring class.
If the Emergo logon screen is opened the update files are applied on the database in order of there update number. The last update number is saved within the database in table sys, so it is clear which updates are applied and which not yet. Because the Emergo installation package contains all update files. See method InitDatabase within SSpring class.
If the logon screen is opened on localhost the update files are preserved, otherwise they are deleted after being used.
