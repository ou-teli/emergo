import nl.surf.emergo.control.CDesktopComponents;
Object vView = CDesktopComponents.vView();
Object cControl = CDesktopComponents.cControl();
Object sSpring = CDesktopComponents.sSpring();

public class MapValueComparator implements Comparator {

	private Map map;

	public MapValueComparator(Map map) {
		this.map = map;
	}

	public int compare(Object keyA, Object keyB) {
		if (map.get(keyA) != null && map.get(keyB) != null) {
			return ((String) map.get(keyA)).compareTo((String) map.get(keyB));
		}
		return 0;
	}
}
	
public class MapKeyComparator implements Comparator {

	private Map map;

	public MapKeyComparator(Map map) {
		this.map = map;
	}

	public int compare(Object keyA, Object keyB) {
		if (keyA != null && keyB != null) {
			return ((String)keyA).compareTo(((String)keyB));
		}
		return 0;
	}
}
	
public class Container{
	    
	private String _label;
	private String _value;
			
	public Container (String label, String value) {
		_label = label;
		_value = value;
	}
	public String getLabel() {
		return _label;
	}
	public String getValue() {
		return _value;
	}
	public void setLabel(String label) {
		_label = label;
	}
	public void setValue(String value) {
		_value = value;
	}
	public String toString () {
		return getLabel() + " (" + getValue() + ")";
	}
	public boolean equals (Object o) {
		return (o instanceof Container) && ((Container)o).toString().equals(toString());
	}
}

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.zkspring.SSpring;

import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;

public class ChosenboxModel{
	    
	private ListModelList _model;
 	private Set _selectedObjects = new HashSet();;
	public static SSpring sSpring = CDesktopComponents.sSpring();
 	
	public String getTagAttribute(IXMLTag tag, String key) {
		return sSpring.unescapeXML(tag.getAttribute(key));
	}

 	public ListModel getModel (Object pItems, String pKey, List pSelectedValues) {
 		if (_model == null) {
 			List lObjects = new ArrayList();
 			Set lSelection = new HashSet();
			if (pItems instanceof List) {
				//items is list of IXMLTag
				String labelKey = "";
				if (pKey.equals("studyCode")) {
					labelKey = "code";
				}
				List objects = (List)pItems;
				for (Object object : objects) {
					IXMLTag item = (IXMLTag)object;
					lObjects.add(new Container(getTagAttribute(item, "id"), getTagAttribute(item, labelKey)));
					if (pSelectedValues.contains(getTagAttribute(item, labelKey))) {
						lSelection.add(new Container(getTagAttribute(item, "id"), getTagAttribute(item, labelKey)));
					}
				}
			}
			else if (pItems instanceof Map) {
				//items is map of <String,String>
				Map objects = (Map)pItems;
				for (Object object : objects.entrySet()) {
					Map.Entry entry = (Map.Entry)object;
					lObjects.add(new Container((String)entry.getValue(), (String)entry.getKey()));
					if (pSelectedValues.contains(entry.getKey())) {
						lSelection.add(new Container((String)entry.getValue(), (String)entry.getKey()));
					}
				}
			}
 			_model = new ListModelList(lObjects);
 			_selectedObjects.addAll(lSelection);
 		}
 		return _model;
 	}
 	
	public Set getSelectedObjects () {
		return _selectedObjects;
	}

}
	



import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import nl.surf.emergo.business.IAccountContextManager;
import nl.surf.emergo.business.IAccountManager;
import nl.surf.emergo.business.IAppManager;
import nl.surf.emergo.business.ICaseManager;
import nl.surf.emergo.business.IContextManager;
import nl.surf.emergo.business.IRunManager;
import nl.surf.emergo.business.IXMLTag;
import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CControl;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.domain.IEAccount;
import nl.surf.emergo.domain.IEAccountContext;
import nl.surf.emergo.domain.IECase;
import nl.surf.emergo.domain.IECaseComponent;
import nl.surf.emergo.domain.IEContext;
import nl.surf.emergo.domain.IERun;
import nl.surf.emergo.view.VView;
import nl.surf.emergo.zkspring.SSpring;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkmax.zul.Chosenbox;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Include;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Tabpanel;
import org.zkoss.zul.Tabpanels;
import org.zkoss.zul.Tabs;
import org.zkoss.zul.Textbox;

public class StudyWnd extends nl.surf.emergo.control.CDecoratedInputWndTC {

	public static VView vView = CDesktopComponents.vView();
	public static CControl cControl = CDesktopComponents.cControl();
	public static SSpring sSpring = CDesktopComponents.sSpring();
	
	private Session session = Sessions.getCurrent();
	
	public Boolean debugCode = false;

	//only support Psychology, for the time being
	public static String studyCodePsychology = "P";
	public static String replyEmailPsychology = "psychologie@ou.nl";

	//one case contains all data entered by admin and tutor users. A case with the following name must exist.
	public String caseName = "Bijeenkomsten";

	//one run contains all data entered by student users. A run with the following name must exist.
	public static String runName = "Bijeenkomsten";

	//constants for rendering tab controls and tab panels
	public static String[] mainTabs = new String[]
		{
			"Admin",
			"Docent",
			"Student"
		};
	public static String[][] subTabs = new String[][]
		{
			{"Opleidingen", "Cursussen", "Plaatsen", "Bijeenkomsten per cursus", "Mail templates per cursus", "Mail settings", "Admin settings", "Bijeenkomsten"},
			{"Bijeenkomsten"},
			{"Aanmelden"}
		};
	public static String[][] includeSrc = new String[][]
		{
			{"adm_studies.zul", "adm_courses.zul", "adm_places.zul", "adm_courses-sessions.zul", "adm_courses-mailTemplates.zul", "adm_mailSettings.zul", "adm_adminSettings.zul", "adm_sessions.zul"},
			{"tut_sessions-students.zul"},
			{"stu_enroll.zul"}
		};

	//constants for rendering (input) elements
	public static final String mailTemplateTypeInitialResponseId = "1";
	public static final String mailTemplateTypeFollowUpResponseId = "2";
	public static final String mailTemplateTypeInitialWaitlistResponseId = "3";
	public static final String mailTemplateTypeInitialCanceledResponseId = "4";
	public static final String mailTemplateTypeInitialResponse = "initial response - enrolled";
	public static final String mailTemplateTypeFollowUpResponse = "follow up response - enrolled";
	public static final String mailTemplateTypeInitialWaitlistResponse = "initial response - waitlist";
	public static final String mailTemplateTypeInitialCanceledResponse = "initial response - canceled";
	public static final Map mailTemplateTypes = new TreeMap();

	public static final Map quartiles = new TreeMap();

	public static final String adminSettingAdminId = "1";
	public static final String adminSettingTeacherId = "2";
	public static final String adminSettingAdmin = "admins";
	public static final String adminSettingTeacher = "teachers";
	public static final Map adminSettings = new TreeMap();

	//case used for study meetings
	public IECase studiesCase = null;
	//NOTE use case case component to store studies
	public IECaseComponent studiesCaseComponent = null;
	//NOTE root tag of case case component
	public IXMLTag studiesRootTag = null;
	//NOTE storage tag for studies
	public IXMLTag studiesStorageTag = null;

	//will contain all study tags
	public List studyList = new ArrayList();
    
	//NOTE the different types of storage tags. Add one if needed.
	public static String[] storageTagNames = new String[]{"courses","courses-sessions","places","courses-mailTemplates","mailSettings","adminSettings","sessions"};
	//NOTE the different types of data to store. Add one if needed. Must correspond with string array above.
	public static String[] subStorageTagNames = new String[]{"course","course-session","place","course-mailTemplate","mailSetting","adminSetting","session"};

	//NOTE get case components to store other authored data
	//NOTE every study, like Psychology, has its own case component to store data. A case component with the name shown below must exist.
	//We misuse states components for storage. 

	public Hashtable studyCaseComponents = new Hashtable();
	public Hashtable studyRootTags = new Hashtable();
	public Hashtable studyStorageTags = new Hashtable();
	
	public Integer accId = "";
	public IEAccount account = null;
		
	//user type is set by idm
	public String userType = "";
	public boolean isIdmUsed = false;

	//roles may be set by Emergo
	public boolean hasAdmRole = false;
	public boolean hasTutRole = false;
	public boolean hasStuRole = false;

	public boolean[] hasRole = new boolean[]
		{
			false, //adm
			false, //tut
			false  //stu
		};
	//admUserids contains acronyms of users that should have adm rights.
	public List admUserids = new ArrayList();
	//tutUserids contains acronyms of users that should have tut rights.
	public List tutUserids = new ArrayList();
	//if no user show message 'no admission'
	public boolean isUser = false;

	//NOTE for role specific data
	//NOTE contexts are course codes
	public TreeMap contexts = new TreeMap();
	public TreeMap conIdsPerContext = new TreeMap();
	//NOTE contexts for student accounts
	public TreeMap accountContexts = new TreeMap();
	//NOTE teachers
	public TreeMap tutors = new TreeMap();
		
	public String idmEducationIndicator = "%";
	
	//for uniquely identifying (input) elements
	public String idSeparator = "_";
	public String componentIdPrefix = "study" + idSeparator + "meetings";

	//events used for updating screens of other users
	public String globalEventUpdateInputElement = "onGlobalUpdateInputElement";
	public String globalEventDeleteRow = "onGlobalDeleteRow";
	
	//event queue used for updating screens of other users
	public EventQueue eventQueueUpdateOtherUsers = EventQueues.lookup("eventQueueUpdateOtherUsers", EventQueues.APPLICATION, true);
	
	//events used for updating tabs of the current user
	public String eventUpdateInputElement = "onUpdateInputElement";
	public String eventDeleteRow = "onDeleteRow";


	//NOTE synchronization related functions

	public void subscribeToEventQueueUpdateOtherUsers() {
		eventQueueUpdateOtherUsers.subscribe(
			new EventListener() {
				public void onEvent(Event evt) {
					handleEvent(evt);
				}
			}
		);
	}
	
	protected void handleEvent(Event evt) {
		if (evt.getName().equals(globalEventUpdateInputElement)) {
			Events.postEvent(eventUpdateInputElement, this, evt.getData());
		}
		else if (evt.getName().equals(globalEventDeleteRow)) {
			Events.postEvent(eventDeleteRow, this, evt.getData());
		}
	}

	public void updateOtherUsers(String updateType, String role, Component inputElement, String value) {
		//TODO support tut role
		//For stu only updates have to be sent for other students in the same run (a run corresponds to a series of study meetings
		
		if (role.matches("adm|stu")) {
			if (updateType.equals("update")) {
				Object[] eventData = new Object[]{this, role, inputElement, value};
				this.eventQueueUpdateOtherUsers.publish(new Event(this.globalEventUpdateInputElement, null, eventData));
			}
			else if (updateType.equals("delete")) {
				Object[] eventData = new Object[]{this, role, inputElement, inputElement.getId()};
				this.eventQueueUpdateOtherUsers.publish(new Event(this.globalEventDeleteRow, null, eventData));
			}
			
		}
	}


	//NOTE get study meetings case (used for storage of admin and tutor data) and run (used for storage of student data)
	
	//NOTE get case case component and storage tags to store study data
	
	public IECase getStudiesCase() {
		for (IECase specificCase : ((ICaseManager) sSpring.getBean("caseManager")).getAllCases()) {
			if ((specificCase).getName().equals(caseName)) {
				return specificCase;
			}
		}
		return null;
	}
	
	public IECaseComponent getCaseComponent(String code, String name) {
		if (studiesCase == null) {
			return null;
		}
		return sSpring.getCaseComponent((IECase) studiesCase, code, name);
	}
	
	public IXMLTag getRootTag(IECaseComponent caseComponent) {
		if (caseComponent == null) {
			return null;
		}
		return sSpring.getXmlDataTree(caseComponent.getCacId() + "");
	}
	
	public IXMLTag getStorageTagByRootTag(IXMLTag rootTag, String tagName) {
		//NOTE for storage the status child tag of the component tag is used
		//Every type of data within this application gets/has its own child tag within the status child tag, with the name equal to the type of data, for instance 'courses'. 
		//The actual data is stored as child tag of the latter tag, for instance the 'courses' tag contains child tag with name 'course'.
		if (rootTag == null) {
			return null;
		}
		IXMLTag componentTag = rootTag.getChild(AppConstants.componentElement);
		if (componentTag == null) {
			return null;
		}
		IXMLTag statusTag = componentTag.getChild(AppConstants.statusElement);
		if (statusTag == null) {
			return null;
		}
		IXMLTag storageTag = statusTag.getChild(tagName);
		if (storageTag == null) {
			storageTag = addNewChildTag(statusTag, tagName);
		}
		return storageTag;
	}
	
	protected void fillConstants() {
		mailTemplateTypes.put(mailTemplateTypeInitialResponseId, mailTemplateTypeInitialResponse);
		mailTemplateTypes.put(mailTemplateTypeFollowUpResponseId, mailTemplateTypeFollowUpResponse);
		mailTemplateTypes.put(mailTemplateTypeInitialWaitlistResponseId, mailTemplateTypeInitialWaitlistResponse);
		mailTemplateTypes.put(mailTemplateTypeInitialCanceledResponseId, mailTemplateTypeInitialCanceledResponse);
		quartiles.put("1", "1");
		quartiles.put("2", "2");
		quartiles.put("3", "3");
		quartiles.put("4", "4");
		adminSettings.put(adminSettingAdminId, adminSettingAdmin);
		//adminSettings.put(adminSettingTeacherId, adminSettingTeacher);
	}
	
	protected void fillWithTestData() {
		//NOTE if storage has no children, do temporarily initialization
		if (studiesStorageTag != null && studiesStorageTag.getChildTags().size() == 0) {
			List studies = new ArrayList();
			studies.add(new String[]{"1", "B", "Bedrijfswetenschappen", "", "false"});
			studies.add(new String[]{"2", "C", "Cultuurwetenschappen", "", "false"});
			studies.add(new String[]{"3", "E", "Economie", "", "false"});
			studies.add(new String[]{"4", "I", "Informatica", "", "false"});
			studies.add(new String[]{"5", "M", "Management wetenschappen", "", "false"});
			studies.add(new String[]{"6", "N", "Natuurwetenschappen", "", "false"});
			studies.add(new String[]{"7", "P", "Psychologie", "P-bijeenkomsten", "true"});
			studies.add(new String[]{"8", "R", "Rechten", "", "false"});
		
			for (Object object : studies) {
				String[] study = (String[])object;
				IXMLTag childTag = addNewChildTag(studiesStorageTag, "study");
				setTagAttribute(childTag, "id", study[0]);
				setTagAttribute(childTag, "code", study[1]);
				setTagAttribute(childTag, "name", study[2]);
				setTagAttribute(childTag, "caseComponentName", study[3]);
				setTagAttribute(childTag, "used", study[4]);
			}
			storeStudyList();
		}
	}
	
	public void determineRoles() {
		//determine roles. If adm then automatically also tut and stu to test other roles. If tut then automatically also stu to test other role.

		accId = (Integer)cControl.getAccId();
		if (accId == null) {
			return;
		}
		account = ((IAccountManager) sSpring.getBean("accountManager")).getAccount(accId);
		
		//user type is set by idm
		userType = notNullString((String)cControl.getAccSessAttr("idm_usertype"));
		//Note clear session attribute to reduce the number of session attributes
		cControl.setAccSessAttr("idm_usertype", null);
		isIdmUsed = !userType.equals("");
	
		//Emergo users that are adm don't have to be added. They get automatically adm rights.
 		admUserids = stringToListOfStrings(getStudyDataValue(studyCodePsychology, "adminSetting", "key", adminSettingAdminId, "value").replaceAll(",", AppConstants.statusCommaReplace));
 		tutUserids = stringToListOfStrings(getStudyDataValue(studyCodePsychology, "adminSetting", "key", adminSettingTeacherId, "value").replaceAll(",", AppConstants.statusCommaReplace));

		//roles may be set by Emergo
		hasAdmRole = sSpring.hasRole(AppConstants.c_role_adm);
		hasTutRole = sSpring.hasRole(AppConstants.c_role_tut);
		hasStuRole = sSpring.hasRole(AppConstants.c_role_stu);

		hasRole[0] = (userType.equals("employee") && admUserids.contains(account.getUserid())) || hasAdmRole;
		//hasRole[1] = hasRole[0] || (userType.equals("employee") && tutUserids.contains(userid)) || (!isIdmUsed && hasTutRole);
		hasRole[1] = hasRole[0] || (userType.equals("employee")) || (!isIdmUsed && hasTutRole);
		hasRole[2] = hasRole[0] || (userType.equals("student")) || (!isIdmUsed && hasStuRole);
		//NOTE only adm may test system in stu role
		if (!hasRole[0] && hasRole[1]) {
			hasRole[2] = false;
		}

		//if no user show message 'no admission'
		isUser = (account != null) && account.getActive() && (hasRole[0] || hasRole[1] || hasRole[2]);
	}
	
	public void determineContextsAndTutors() {
		IContextManager contextManager = (IContextManager)sSpring.getBean("contextManager");
		List dbContexts = contextManager.getAllContexts(true);
		for (Object object : dbContexts) {
			IEContext dbContext = (IEContext)object;
			//NOTE strip % from course code
			String context = stripContext(dbContext.getContext());
			//NOTE temporarily get only Psychology codes
			if (context.startsWith(studyCodePsychology)) {
				if (!contexts.containsKey(context)) {
					contexts.put(context, context);
					conIdsPerContext.put(context, new ArrayList());
				}
				((List)conIdsPerContext.get(context)).add(dbContext.getConId());
			}
		}

		List dbAccountContexts = ((IAccountContextManager)sSpring.getBean("accountContextManager")).getAllAccountContextsByAccountIdActive(account.getAccId(), true);
		for (Object object : dbAccountContexts) {
			IEAccountContext dbAccountContext = (IEAccountContext)object;
			String accountContext = stripContext(contextManager.getContext(dbAccountContext.getConConId()).getContext());
			if (!accountContexts.containsKey(accountContext)) {
				accountContexts.put(accountContext, accountContext);
			}
		}

		List dbAccounts = ((IAccountManager)sSpring.getBean("accountManager")).getAllAccounts(true, "tut");
		for (Object object : dbAccounts) {
			IEAccount dbAccount = (IEAccount)object;
			String userid = dbAccount.getUserid().toLowerCase();
			//NOTE tutor acronyms has to be entered by admin
			//if (tutUserids.contains(userid) && !tutors.containsKey(userid)) {
			if (!tutors.containsKey(userid)) {
				String name = dbAccount.getLastname();
				if (!dbAccount.getNameprefix().equals("")) {
					name+= ", " + dbAccount.getNameprefix();
				}
				if (!dbAccount.getInitials().equals("")) {
					name+= " " + dbAccount.getInitials();
				}
				tutors.put(userid, name);
			}
		}
		//sort contexts and tutors
		contexts = sortTreeMapOnValue(contexts);
		tutors = sortTreeMapOnValue(tutors);
	}

	public StudyWnd() {
		fillConstants();
		determineRoles();
		if (isUser) {
			//fillWithTestData();

			studiesCase = getStudiesCase();
			studiesCaseComponent = getCaseComponent("case", "");
			studiesRootTag = getRootTag(studiesCaseComponent);
			studiesStorageTag = getStorageTagByRootTag(studiesRootTag, "studies");

			if (studiesStorageTag != null) {
				studyList = studiesStorageTag.getChildTags();
			}
		    
			//create study specific case components, study root tags and study storage tags
			for (Object study : studyList) {
				IECaseComponent studyCaseComponent = getCaseComponent("", ((IXMLTag) study).getAttribute("caseComponentName"));
				if (studyCaseComponent != null) {
					studyCaseComponents.put(((IXMLTag) study).getAttribute("code"), studyCaseComponent);
				}
				IXMLTag studyRootTag = getRootTag(studyCaseComponent);
				if (studyRootTag != null) {
					studyRootTags.put(((IXMLTag) study).getAttribute("code"), studyRootTag);
				}
				Hashtable subStudyStorageTags = new Hashtable();
				for (int i=0;i<storageTagNames.length;i++) {
					IXMLTag storageTag = getStorageTagByRootTag(studyRootTag, storageTagNames[i]);
					if (storageTag != null) {
						subStudyStorageTags.put(storageTagNames[i], storageTag);
					}
				}
				studyStorageTags.put(((IXMLTag) study).getAttribute("code"), subStudyStorageTags);
			}
			
			determineContextsAndTutors();
		}
	
	}
	

	//NOTE helper objects and functions
		
	public boolean getIsUser() {
		return isUser;
	}
	
	public String notNullString(String string) {
		if (string == null) {
			return "";
		}
		return string;
	}
	
	public List stringToListOfStrings(String stringOfStrings) {
		List list = new ArrayList();
		if (!stringOfStrings.equals("")) {
			String[] strings = stringOfStrings.split(AppConstants.statusCommaReplace);
			for (int i=0;i<strings.length;i++) {
				if (!strings[i].equals("")) {
					list.add(strings[i].trim());
				}
			}
		}
		return list;
	}
	
	public TreeMap sortTreeMapOnValue(TreeMap unsortedTreeMap) {
		TreeMap treeMap = new TreeMap(new MapValueComparator(unsortedTreeMap));
		treeMap.putAll(unsortedTreeMap);
		return treeMap;
	}
	
	public TreeMap sortTreeMapOnKey(TreeMap unsortedTreeMap) {
		TreeMap treeMap = new TreeMap(new MapKeyComparator(unsortedTreeMap));
		treeMap.putAll(unsortedTreeMap);
		return treeMap;
	}

	public String getDateAsDateTimeStr(Date date) {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
	}
	
	public String getStudentName(String pUserId) {
		IEAccount lAccount = ((IAccountManager)sSpring.getBean("accountManager")).getAccount(pUserId);
		if (lAccount == null) {
			return pUserId;
		}
		return getStudentName (lAccount);
	}
	
	public String getStudentName(IEAccount pAccount) {
		if (pAccount == null) {
			return "user not found";
		}
		List alteredUserInfo = stringToListOfStrings(pAccount.getJob());
		String name = pAccount.getInitials();
		if (alteredUserInfo.size() > 0) {
			//possible alternative
			name = (alteredUserInfo.get(0).equals("")) ? name : (String)alteredUserInfo.get(0);
		}
		String lastname = pAccount.getLastname();
		if (!pAccount.getNameprefix().equals("")) {
			lastname += ", " + pAccount.getNameprefix();
		}
		if (alteredUserInfo.size() > 0) {
			//possible alternative
			lastname = (alteredUserInfo.get(1).equals("")) ? lastname : (String)alteredUserInfo.get(1);
		} 
		if (name.equals("")) {
			name = lastname;
		}
		else if (!lastname.equals("")) {
			name += " " + lastname;
		}
		return name;
	}
	
	public String mailStudent(String userId, String body, Map params) {
		IEAccount account = ((IAccountManager)sSpring.getBean("accountManager")).getAccount(userId);
		if (account == null) {
			return "account with userid'" + userId + "'not found";
		}
		String from = replyEmailPsychology;
		String ccs = "";
		String bccs = replyEmailPsychology;
		bccs = "";
		String subject = "Info studie-bijeenkomsten";

		String name = getStudentName(account);
		params.put("Naam", name);
		String tos = account.getEmail();
		List alteredUserInfo = stringToListOfStrings(account.getJob());
		if (alteredUserInfo.size() > 0) {
			//possible alternative
			tos = (alteredUserInfo.get(2).equals("")) ? tos : (String)alteredUserInfo.get(2);
		} 
		
		for (Object param : params.entrySet()) {
			String searchStr = "${\"" + (String) ((Map.Entry)param).getKey() + "\"}";
			String replaceStr = (String) ((Map.Entry)param).getValue();
			while (body.indexOf(searchStr) >= 0) {
				body = body.replace(searchStr, replaceStr);
			}
		}
		
		List allMailSettingsData = getStudyData(studyCodePsychology, "mailSetting");
		for (Object mailSettingData : allMailSettingsData) {
			String searchStr = "${\"" + getTagAttribute((IXMLTag)mailSettingData, "key") + "\"}";
			String replaceStr = getTagAttribute((IXMLTag)mailSettingData, "value");
			while (body.indexOf(searchStr) >= 0) {
				body = body.replace(searchStr, replaceStr);
			}
		}
		
		return ((IAppManager)sSpring.getBean("appManager")).sendMail(from, tos, ccs, bccs, subject, body);
	} 
	
	
	//NOTE xml tag related functions

	public void setTagAttribute(IXMLTag tag, String key, String value) {
		if (!value.equals(getTagAttribute(tag,key))) {
			tag.setAttribute(key, sSpring.escapeXML(value));
		}
	}
	
	public String getTagAttribute(IXMLTag tag, String key) {
		return sSpring.unescapeXML(tag.getAttribute(key));
	}
	
	public int getMaxId(List tagList) {
		int maxId = 0;
		for (Object tag : tagList) {
			int id = Integer.parseInt(((IXMLTag) tag).getAttribute("id"));
			if (id > maxId) {
				maxId = id; 
			}
		}
		return maxId;
	}

	public IXMLTag addNewChildTag(IXMLTag tag, String name) {
		IXMLTag childTag = sSpring.getXmlManager().newXMLTag(name, "");
		childTag.setParentTag(tag);
		tag.getChildTags().add(childTag);
		return childTag;
	}

	
	//NOTE get and store study data

	public void storeStudyList() {
		if (studiesCaseComponent != null && studiesRootTag != null) {
			sSpring.setCaseComponentData("" + studiesCaseComponent.getCacId(), sSpring.getXmlManager().xmlTreeToDoc(studiesRootTag));
		}
	}


	public String getStorageTagName(String subStorageTagName) {
		for (int i=0;i<subStorageTagNames.length;i++) {
			if (subStorageTagNames[i].equals(subStorageTagName) && i < storageTagNames.length) {
				return storageTagNames[i];
			}
		}
		return "";
	}
	

	//NOTE get and store authored data

	public List getStudyData(String studyCode, String dataType) {
		if (dataType.equals("study")) {
			return studyList;
		}
		if (studyStorageTags.containsKey(studyCode) && ((Map) studyStorageTags.get(studyCode)).containsKey(getStorageTagName(dataType))) {
			return (List)((IXMLTag)((Map) studyStorageTags.get(studyCode)).get(getStorageTagName(dataType))).getChildTags();
		}
		return new ArrayList();
	}
	
	public String getStudyDataValue(String studyCode, String dataType, String attKey, String attValue, String attKeyToGet) {
		for (Object tag : getStudyData(studyCode, dataType)) {
			if (((IXMLTag) tag).getAttribute(attKey).equals(attValue)) {
				return ((IXMLTag) tag).getAttribute(attKeyToGet);
			}
		}
		return "";
	}
	
	public void storeStudyData(Rows rows) {
		String studyCode = notNullString((String) rows.getAttribute("studyCode"));
		String dataType = notNullString((String) rows.getAttribute("dataType"));
		if (dataType.equals("study")) {
			storeStudyList();
		} else if (studyCaseComponents.containsKey(studyCode) && studyRootTags.containsKey(studyCode)) {
			sSpring.setCaseComponentData("" + ((IECaseComponent) studyCaseComponents.get(studyCode)).getCacId(), sSpring.getXmlManager().xmlTreeToDoc((IXMLTag) studyRootTags.get(studyCode)));
		}
	}

	public IXMLTag getTagByNameAndId(List tags, String name, String id) {
		if (tags == null) {
			return null;
		}
		for (Object object : tags) {
			IXMLTag tag = (IXMLTag)object;
			if (tag.getName().equals(name) && tag.getAttribute("id").equals(id)) {
				return tag;
			}
		}
		return null;
	}

	public IXMLTag getLocalTag(IXMLTag remoteTag) {
		//NOTE gets the local tag corresponding with a remote tag (of another user). Names and ids of (parent)tags should be equal.
		//get local study root tag
		IXMLTag localStudyRootTag = null;
		if (studyRootTags.containsKey(studyCodePsychology)) {
			localStudyRootTag = (IXMLTag)studyRootTags.get(studyCodePsychology);
		}
		if (localStudyRootTag == null) {
			return null;
		}
		//determine (parent)names and (parent)ids for remoteTag
		IXMLTag tempTag = remoteTag;
		List tagNamesPlusIds = new ArrayList();
		while (tempTag != null) {
			tagNamesPlusIds.add(0, new String[]{tempTag.getName(), tempTag.getAttribute("id")});
			tempTag = tempTag.getParentTag();
		}
		//search local tag with these (parent)names and (parent)ids
		List tags = new ArrayList();
		tags.add(localStudyRootTag);
		IXMLTag localTag = null;
		for (Object object : tagNamesPlusIds) {
			String[] tagNamePlusId = (String[])object;
			localTag = getTagByNameAndId(tags, tagNamePlusId[0], tagNamePlusId[1]);
			if (localTag == null) {
				return null;
			}
			else {
				tags = localTag.getChildTags();
			}
		}
		return localTag;
	}
	
	
	public String stripContext(String context) {
		int pos = context.indexOf(idmEducationIndicator);
		if (pos > 0) {
			context = context.substring(0, pos);
		}
		return context.toUpperCase();
	}
	

	//NOTE general ZK component functions
		
	public String getRowsId(Component rows) {
		//NOTE every rows has a studyCode and dataType attribute, see other zul files.
		String studyCode = notNullString((String) rows.getAttribute("studyCode"));
		String dataType = notNullString((String) rows.getAttribute("dataType"));
		if (studyCode.equals("")) {
			return componentIdPrefix + idSeparator + dataType;
		}
		else {
			return componentIdPrefix + idSeparator + studyCode + idSeparator + dataType;
		}
	}

	public String getRowId(Component row, IXMLTag tag) {
		return getRowsId(row.getParent()) + idSeparator + getTagAttribute(tag, "id");
	}

	public String getSubRowId(Component row, IXMLTag tag, boolean pAddParentTag) {
		String lId = getRowId(row, tag);
		if (pAddParentTag) {
			lId += idSeparator + getTagAttribute(tag.getParentTag(), "id");
		}
		return lId;
	}

	public String getComponentId(Component row, IXMLTag tag, String key) {
		if (key.equals("")) {
			return "";
		}
		if ((boolean)row.getAttribute("subRow")) {
			return getSubRowId(row, tag, true) + idSeparator + key;
		}
		return getRowId(row, tag) + idSeparator + key;
	}

	public Component appendComponent(Component row, Component component, IXMLTag tag, String key, String dependencies) {
		//NOTE setting id probably is not needed anymore. It was to be able to notify input elements, but now another mechanish is used.
		component.setId(getComponentId(row, tag, key));
		row.appendChild(component);
		component.setAttribute("tag", tag);
		component.setAttribute("key", key);
		component.setAttribute("dependencies", dependencies);
		component.setVisible(true);
		return component;
	}

	public void setLabelValue(Label component, IXMLTag tag, String key) {
		component.setValue(getTagAttribute(tag, key));
	}

	public Label appendLabel(Row row, IXMLTag tag, String key, String dependencies) {
		Label component = (Label)vView.getComponent("templateLabel").clone();
		appendComponent(row, component, tag, key, dependencies);
		setLabelValue(component, tag, key);
		return component;
	}

	public void setTextboxValue(Textbox component, IXMLTag tag, String key) {
		component.setValue(getTagAttribute(tag, key).replace(AppConstants.statusCrLfReplace, "\n"));
	}

	public Textbox appendTextbox(Row row, IXMLTag tag, String key, int numberOfRows, String dependencies) {
		Textbox component = (Textbox)vView.getComponent("templateTextbox").clone();
		appendComponent(row, component, tag, key, dependencies);
		if (numberOfRows > 1) {
			component.setRows(numberOfRows);
			component.setInplace(false);
		}
		setTextboxValue(component, tag, key);
		return component;
	}

	public void setIntboxValue(Intbox component, IXMLTag tag, String key) {
		int lValue = 0;
		try {
			lValue = Integer.parseInt(getTagAttribute(tag, key));
		} catch (NumberFormatException e) {
		}
		component.setValue(lValue);
	}

	public Intbox appendIntbox(Row row, IXMLTag tag, String key, String dependencies) {
		Intbox component = (Intbox)vView.getComponent("templateIntbox").clone();
		appendComponent(row, component, tag, key, dependencies);
		setIntboxValue(component, tag, key);
		return component;
	}

	public void setCheckboxValue(Checkbox component, IXMLTag tag, String key) {
		component.setChecked(getTagAttribute(tag, key).equals("true"));
	}

	public Checkbox appendCheckbox(Row row, IXMLTag tag, String key, String dependencies) {
		Checkbox component = (Checkbox)vView.getComponent("templateCheckbox").clone();
		appendComponent(row, component, tag, key, dependencies);
		setCheckboxValue(component, tag, key);
		return component;
	}

	public Comboitem appendComboitem(Combobox component, String value, String label) {
		if (!value.equals("") && !label.equals("")) {
			Comboitem item = component.appendItem(label);
			item.setValue(value);
			return item;
		}
		return null;
	}

	public List setComboitems(Combobox component, IXMLTag tag, String key, Object items) {
		String selectedKey = "";
		if (component.getSelectedItem() != null) {
			selectedKey = component.getSelectedItem().getValue();
		}
		component.getChildren().clear();
		List comboitems = new ArrayList();
		if (items instanceof List) {
			//items is list of IXMLTag
			String labelKey = "";
			if (key.equals("studyCode")) {
				labelKey = "code";
			}
			List objects = (List)items;
			for (Object object : objects) {
				IXMLTag item = (IXMLTag)object;
				String tempKey = getTagAttribute(item, "id");
				String tempValue = getTagAttribute(item, labelKey);
				Comboitem comboitem = appendComboitem(component, tempKey, tempValue);
				if (comboitem != null) {
					comboitems.add(comboitem);
					if (tempKey.equals(selectedKey)) {
						component.setSelectedItem(comboitem);
					}
				}
			}
		}
		else if (items instanceof Map) {
			//items is map of <String,String>
			Map objects = (Map)items;
			for (Object object : objects.entrySet()) {
				Map.Entry entry = (Map.Entry)object;
				String tempKey = (String)entry.getKey();
				String tempValue = (String)entry.getValue();
				Comboitem comboitem = appendComboitem(component, tempKey, tempValue);
				if (comboitem != null) {
					comboitems.add(comboitem);
					if (tempKey.equals(selectedKey)) {
						component.setSelectedItem(comboitem);
					}
				}
			}
		}
		return comboitems;
	}

	public void setComboitemsValue(Combobox component, IXMLTag tag, String key) {
		String selectedValue = getTagAttribute(tag, key);
		for (Comboitem item : component.getItems()) {
			if (item.getValue().equals(selectedValue)) {
				component.setSelectedItem(item);
			}
		}
	}

	public Combobox appendCombobox(Row row, IXMLTag tag, String key, Object items, String dependencies) {
		Combobox component = (Combobox)vView.getComponent("templateCombobox").clone();
		appendComponent(row, component, tag, key, dependencies);
		setComboitems(component, tag, key, items);
		setComboitemsValue(component, tag, key);
		return component;
	}

	public List setChosenitems(Chosenbox component, IXMLTag tag, String key, Object items) {
		List selectedValueList = stringToListOfStrings(getTagAttribute(tag, key));
		ChosenboxModel lCBModel = new ChosenboxModel();
		component.setModel(lCBModel.getModel(items, key, selectedValueList));
		component.setSelectedObjects(lCBModel.getSelectedObjects());
		return new ArrayList();
	}

	public Component appendChosenbox(Row row, IXMLTag tag, String key, Object items, String dependencies, String pEmptyMsg, String pNotFoundMsg) {
		Chosenbox component = (Chosenbox)vView.getComponent("templateChosenbox").clone();
		component.setEmptyMessage(pEmptyMsg);
		component.setNoResultsText(pNotFoundMsg);
		appendComponent(row, component, tag, key, dependencies);
		component.setAttribute("items", items);
		setChosenitems(component, tag, key, items);
		return component;
	}
	
	public Component appendListitem(Listbox component, String value, String label) {
		if (!value.equals("") && !label.equals("")) {
			return component.appendItem(label, value);
		}
		return null;
	}

	public List setListitems(Listbox component, IXMLTag tag, String key, Object items) {
		component.getChildren().clear();
		List listitems = new ArrayList();
		if (items instanceof List) {
			//items is list of IXMLTag
			String labelKey = "";
			if (key.equals("studyCode")) {
				labelKey = "code";
			}
			List objects = (List)items;
			for (Object object : objects) {
				IXMLTag item = (IXMLTag)object;
				Component listitem = appendListitem(component, getTagAttribute(item, "id"), getTagAttribute(item, labelKey));
				if (listitem != null) {
					listitems.add(listitem);
				}
			}
		}
		else if (items instanceof Map) {
			//items is map of <String,String>
			Map objects = (Map)items;
			for (Object object : objects.entrySet()) {
				Map.Entry entry = (Map.Entry)object;
				Component listitem = appendListitem(component, (String)entry.getKey(), (String)entry.getValue());
				if (listitem != null) {
					listitems.add(listitem);
				}
			}
		}
		return listitems;
	}

	public void setListitemsValue(Listbox component, IXMLTag tag, String key) {
		component.clearSelection();
		List selectedValueList = stringToListOfStrings(getTagAttribute(tag, key));
		for (Listitem item : component.getItems()) {
			if (selectedValueList.contains(item.getValue())) {
				component.addItemToSelection(item);
			}
		}
	}

	public Listbox appendListbox(Row row, IXMLTag tag, String key, Object items, int numberOfRows, String dependencies) {
		Listbox component = (Listbox)vView.getComponent("templateListbox").clone();
		appendComponent(row, component, tag, key, dependencies);
		component.setMultiple(true);
		component.setCheckmark(true);
		if (numberOfRows >= 1) {
			component.setRows(numberOfRows);
		}
		setListitems(component, tag, key, items);
		setListitemsValue(component, tag, key);
		return component;
	}

	public Button appendDeleteButton(Row row, IXMLTag tag, String key, String dependencies) {
		Button component = (Button)vView.getComponent("templateDeleteButton").clone();
		appendComponent(row, component, tag, key, dependencies);
		return component;
	}

	public void showDeleteButtonOfLastRow(Rows rows) {
		//show delete button of last row
		for (Component row : rows.getChildren()) {
			for (Component component : row.getChildren()) {
				if (component.getAttribute("inputType").equals("DeleteButton")) {
					component.setVisible(true);
				}
			}
		}
	}
	
	public Component getRows(Component component) {
		while (component != null && !(component instanceof Rows)) {
			component = component.getParent();
		}
		return component;
	}
		
	public Component getRow(Component component) {
		while (component != null && !(component instanceof Row)) {
			component = component.getParent();
		}
		return component;
	}
		

	//NOTE general ZK component functions
	
	public void createRows(Rows rows) {
		rows.setId(getRowsId(rows));
		List tagList = getRowsTagList(rows);
		boolean isEmptyRow = false; 
		for (int i=0;i<tagList.size();i++) {
			if (i == (tagList.size() - 1)) {
				isEmptyRow = isTagEmpty(rows, (IXMLTag)tagList.get(i));
			}
			addRow(rows, (IXMLTag)tagList.get(i), isEmptyRow);
		}
		if (!isEmptyRow) {
			addEmptyRow(rows);
		}
	}

	public void createSubRows(Rows rows) {
		//rows.setId(getRowsId(rows));
		List tagList = getRowsSubTagsList(rows);
		// for now, empty rows not used in sub grid
		boolean isEmptyRow = false; 
		for (int i=0;i<tagList.size();i++) {
			addSubRow(rows, (IXMLTag)tagList.get(i), isEmptyRow, true);
		}
	}

	public boolean isTagEmpty(Component rows, IXMLTag tag) {
		//NOTE every rows has a notEmptyTagKeys and notEmptyTagKeyTypes attribute, see other zul files.
		String[] notEmptyTagKeys = ((String) rows.getAttribute("notEmptyTagKeys")).split(",");
		String[] notEmptyTagKeyTypes = ((String) rows.getAttribute("notEmptyTagKeyTypes")).split(",");
		boolean empty = true;
		for (int i=0;i<notEmptyTagKeys.length;i++) {
			if (notEmptyTagKeyTypes[i].equals("string")) {
				empty = empty && getTagAttribute(tag, notEmptyTagKeys[i]).equals("");
			}
			else if (notEmptyTagKeyTypes[i].equals("boolean")) {
				empty = empty && !getTagAttribute(tag, notEmptyTagKeys[i]).equals("true");
			}
		}
		return empty;
	}

	public void addRow(Rows rows, IXMLTag tag, boolean isEmpty) {
		Row row = addSubRow(rows, tag, isEmpty, false);
		//NOTE delete button is common for every row
		Component deleteButton = appendDeleteButton(row, tag, "delete" + idSeparator + "button", "");
		deleteButton.setVisible(!isEmpty);
	}

	public Row addSubRow(Rows rows, IXMLTag tag, boolean isEmpty, boolean pAddParentTagId) {
		Row row = new Row();
		rows.appendChild(row);
		row.setId(getSubRowId(row, tag, pAddParentTagId));
		if (pAddParentTagId)
			row.setAttribute("subRow", true);
		else
			row.setAttribute("subRow", false);
		row.setAttribute("id", tag.getAttribute("id"));
		row.setAttribute("isEmpty", isEmpty);
		//NOTE onFillRow will create the specific input elements for the row
		Events.sendEvent("onFillRow", rows , new Object[]{row, tag});
		return row;
	}

	public void addEmptyRow(Rows rows) {
		IXMLTag storageTag = getStorageTag(rows);
		if (storageTag == null) {
			return;
		}
		String dataType = notNullString((String) rows.getAttribute("dataType"));
		int maxId = getMaxId(getRowsTagList(rows)) + 1;
		IXMLTag tag = addNewChildTag(storageTag, dataType);
		//NOTE onFillEmptyTag will initialize specific tag attributes for tag
			Events.sendEvent("onFillEmptyTag", rows, new Object[]{tag, new Integer(maxId)});
		addRow(rows, tag, true);
	}

	public void deleteRow(Component inputElement, boolean store) {
		Component rows = getRows(inputElement);
		if (rows == null) {
			return;
		}
		String dataType = notNullString((String)rows.getAttribute("dataType"));
		IXMLTag tag = (IXMLTag)inputElement.getAttribute("tag");
		tag.setAttribute("deleted", "true");
		//NOTE remove tag from tag list
		List tagList = getRowsTagList(rows);
		String id = tag.getAttribute("id");
		for (int i=(tagList.size()-1);i>=0;i--) {
			if (id.equals(((IXMLTag)tagList.get(i)).getAttribute("id"))) {
				tagList.remove(i);
			}
		}
		
		if (store) {
			String role = (String)getRows(inputElement).getAttribute("role");
			updateOtherUsers("delete", role, inputElement, "");
		}
									
		//NOTE update other input elements that depend on inputElement
		updateDependentInputElements(inputElement);
		//NOTE remove corresponding row
		for (int i=(rows.getChildren().size() - 1);i>=0;i--) {
			if (id.equals(rows.getChildren().get(i).getAttribute("id"))) {
				rows.removeChild(rows.getChildren().get(i));
			}
		}
		if (store) {
			//NOTE store adjusted content
			storeData(rows);
		}
	}
			
	public void handleEmptyRow(Component component) {
		//NOTE if row is not empty anymore, add a new row.
		Component rows = getRows(component);
		Component row = getRow(component);
		if ((boolean) row.getAttribute("isEmpty")) {
			row.setAttribute("isEmpty", false);
			showDeleteButtonOfLastRow((Rows) rows);
			addEmptyRow((Rows) rows);
		}
	}
	

	//NOTE general ZK component functions
	
	public List getRowsTagList(Component rows) {
		String studyCode = notNullString((String) rows.getAttribute("studyCode"));
		String dataType = notNullString((String) rows.getAttribute("dataType"));
		return getStudyData(studyCode, dataType);
	}

	public IXMLTag getSubTag(IXMLTag pTag, String pSubTagLabel, String pAttr, String pValue) {
		if (pTag != null) {
			for (IXMLTag lTag : pTag.getChilds(pSubTagLabel)) {
				if (lTag.getAttribute(pAttr).equals(pValue)) {
					return lTag;
				}
			}
		}
		int lMaxId = getMaxId(pTag.getChilds(pSubTagLabel)) + 1;
		IXMLTag lSubTag = addNewChildTag(pTag, pSubTagLabel);
		lSubTag.setAttribute(pAttr, pValue);
		if (!pAttr.equals("id")) {
			lSubTag.setAttribute("id", "" + lMaxId);
		}
		return lSubTag;
	}
			
	public List getRowsSubTagsList(Component rows) {
		Map lItems = (Map)rows.getAttribute("subItems");
		String lSubTagLabel = (String)rows.getAttribute("dataType");
		String lSubTagIdKey = (String)rows.getAttribute("subTagIdKey");
		IXMLTag lTag = (IXMLTag)rows.getAttribute("parentTag");
		List lSubTags = new ArrayList();
		for (Object lObject : lItems.entrySet()) {
			Map.Entry lItem = (Map.Entry)lObject;
			IXMLTag lSubTag = getSubTag(lTag, lSubTagLabel, lSubTagIdKey, (String)lItem.getKey());
			lSubTags.add(lSubTag);
		}
		return lSubTags;
	}

	public void storeInput(Component inputElement, String value, boolean store) {
		value = value.replace("\n", AppConstants.statusCrLfReplace);
		if (value.equals(getTagAttribute((IXMLTag)inputElement.getAttribute("tag"), (String)inputElement.getAttribute("key")))) {
			return;
		}
		setTagAttribute((IXMLTag)inputElement.getAttribute("tag"), (String)inputElement.getAttribute("key"), value);
		if (store) {
			storeData(getRows(inputElement));
		}
		updateDependentInputElements(inputElement);
		if (store) {
			String role = (String)getRows(inputElement).getAttribute("role");
			updateOtherUsers("update", role, inputElement, value);
		}
	}

	public void updateDependentInputElements(Component inputElement) {
		for (Object dependentInputElement : getDependentInputElements(inputElement)) {
			//NOTE the dependent input element's content has to be updated, which is specifically handled by the parent rows element of the dependent input element 
			Events.postEvent("onUpdateComponentContent", getRows((Component)dependentInputElement), new Object[]{dependentInputElement, inputElement.getAttribute("tag")});
		}
	}

	public List getDependentInputElements(Component inputElement) {
		//NOTE get dependent input elemenst for inputElement
		//The 'dependencies' attribute of these input elements contains the data type of inputElement
		Collection components = Executions.getCurrent().getDesktop().getComponents();
		List dependentInputElements = new ArrayList();
		if (components == null) {
			return dependentInputElements;
		}
		String dataType = notNullString((String)getRows(inputElement).getAttribute("dataType"));
		for (Object object : components) {
			Component component = (Component)object;
			String dependencies = (String)component.getAttribute("dependencies");
			if (dependencies != null && !dependencies.equals("")) {
				String[] dependencyArr = dependencies.split(",");
				for (int i=0;i<dependencyArr.length;i++) {
					if (dependencyArr[i].equals(dataType)) {
						dependentInputElements.add(component);
					}
				}
			}
		}
		return dependentInputElements;
	}

	public void storeData(Component rows) {
		storeStudyData((Rows) rows);
	}

	public IXMLTag getStorageTag(Component rows) {
		String studyCode = notNullString((String) rows.getAttribute("studyCode"));
		String dataType = notNullString((String) rows.getAttribute("dataType"));
		if (dataType.equals("study")) {
			return studiesStorageTag;
		}
		else if (studyStorageTags.containsKey(studyCode) && ((Map)studyStorageTags.get(studyCode)).containsKey(getStorageTagName(dataType))) {
			return (IXMLTag)((Map)studyStorageTags.get(studyCode)).get(getStorageTagName(dataType));
		}
		return null;
	}
	

	//NOTE specific ZK component functions needed for specific input elements
	
	public TreeMap getCourseNames(Component rows) {
		String studyCode = notNullString((String)rows.getAttribute("studyCode"));
		List tags = getStudyData(studyCode, "course");
		TreeMap map = new TreeMap();
		for (Object object : tags) {
			IXMLTag tag = (IXMLTag)object;
			if (getTagAttribute(tag, "used").equals("true") && !isTagEmpty(rows, tag)) {
				map.put(getTagAttribute(tag, "id"), getTagAttribute(tag, "name"));
			}
		}
		return sortTreeMapOnValue(map);
	}

	
	public Map getPlaceNames(Component rows) {
		String studyCode = notNullString((String)rows.getAttribute("studyCode"));
		List tags = getStudyData(studyCode, "place");
		TreeMap map = new TreeMap();
		for (Object object : tags) {
			IXMLTag tag = (IXMLTag)object;
			if (getTagAttribute(tag, "used").equals("true") && !isTagEmpty(rows, tag)) {
				map.put(getTagAttribute(tag, "id"), getTagAttribute(tag, "name"));
			}
		}
		return sortTreeMapOnValue(map);
	}

	public String getPlaceName(Component rows, String pPlaceId) {
		String studyCode = notNullString((String)rows.getAttribute("studyCode"));
		List tags = getStudyData(studyCode, "place");
		for (Object object : tags) {
			IXMLTag tag = (IXMLTag)object;
			if (getTagAttribute(tag, "id").equals(pPlaceId)) {
				return getTagAttribute(tag, "name");
			}
		}
		return "";
	}

	public String getSMPlaceName(String pStudyCode, String pPlaceId) {
		List tags = getStudyData(pStudyCode, "place");
		for (Object object : tags) {
			IXMLTag tag = (IXMLTag)object;
			if (getTagAttribute(tag, "id").equals(pPlaceId)) {
				return getTagAttribute(tag, "name");
			}
		}
		return "";
	}

	public TreeMap getTeacherNames(Component rows, IXMLTag sessionPerCourseTag) {
		TreeMap map = new TreeMap();
		List courseIdsToFilterOn = stringToListOfStrings (sessionPerCourseTag.getAttribute("courses"));
		if ((courseIdsToFilterOn == null) || (courseIdsToFilterOn.size() == 0)) {
			return map;
		}
		String studyCode = notNullString((String)rows.getAttribute("studyCode"));
		List tags = getStudyData(studyCode, "course");
		for (Object object : tags) {
			IXMLTag tag = (IXMLTag)object;
	 		if (debugCode)
	 			Clients.alert("getTeacherNames");
			if (!isTagEmpty(rows, tag)) {
				if (courseIdsToFilterOn.contains(tag.getAttribute("id"))) {
					List teacherIds = stringToListOfStrings(tag.getAttribute("teachers"));
					for (Object object2 : teacherIds) {
						String teacherId = (String)object2;
						if (!teacherId.equals("") && tutors.containsKey(teacherId)) {
							for (Object object3 : tutors.entrySet()) {
								Map.Entry entry = (Map.Entry)object3;
								if (entry.getKey().equals(teacherId)) {
									map.put(entry.getKey(), entry.getValue());
								}
							}
						}
					}
				}
			}
		}
		return sortTreeMapOnValue(map);
	}
	
	public void createTabControls(Component parent) {
		Tabbox tabbox = new Tabbox();
		parent.appendChild(tabbox);
		tabbox.setWidth("100%");
		tabbox.setSclass("nav_tabs");
		Tabs tabs = new Tabs();
		tabbox.appendChild(tabs);
		for (int i=0;i<mainTabs.length;i++) {
			if (hasRole[i]) {
				Tab tab = new Tab(mainTabs[i]);
				tab.setId("mainTab_" + mainTabs[i]);
				tabs.appendChild(tab);
			}
		}
		Tabpanels tabpanels = new Tabpanels();
		tabbox.appendChild(tabpanels);
		for (int i=0;i<mainTabs.length;i++) {
			if (hasRole[i]) {
				Tabpanel tabpanel = new Tabpanel();
				tabpanels.appendChild(tabpanel);
				Tabbox tabbox2 = new Tabbox();
				tabpanel.appendChild(tabbox2);
				tabbox2.setWidth("100%");
				tabbox2.setSclass("nav_tabs");
				Tabs tabs2 = new Tabs();
				tabbox2.appendChild(tabs2);
				for (int j=0;j<subTabs[i].length;j++) {
					//NOTE temporarily don't show first admin tab 
					if (i != 0 || j != 0) {
						Tab tab2 = new Tab(subTabs[i][j]);
						tab2.setId("tab_" + i + "_" + j);
						tab2.setAttribute("i", i);
						tab2.setAttribute("j", j);
						tab2.addEventListener("onClick",
							new EventListener() {
								public void onEvent(Event event) {
									//NOTE if tab panel content not filled yet, fill it
									Include include = (Include)vView.getComponent("include_" + tab2.getAttribute("i") + "_" + tab2.getAttribute("j"));
									if (include != null) {
										String src = (String)include.getAttribute("src");
										if (src != null) {
											include.setSrc(src);
											include.setAttribute("src", null);
										}
									}
								}
							});
						tabs2.appendChild(tab2);
					}
				}
				Tabpanels tabpanels2 = new Tabpanels();
				tabbox2.appendChild(tabpanels2);
				for (int j=0;j<subTabs[i].length;j++) {
					if (i != 0 || j != 0) {
						//NOTE temporarily don't show first admin tabpanel for studies. Only needed if more studies get involved. 
						Tabpanel tabpanel2 = new Tabpanel();
						tabpanels2.appendChild(tabpanel2);
						Include include = new Include();
						include.setId("include_" + i + "_" + j);
						tabpanel2.appendChild(include);
						//TODO support multiple study codes
						//NOTE temporarily only support Psychology courses. 
						include.setAttribute("studyCode", studyCodePsychology);
						if ((i == 0 && j == 1) || (i > 0 && j == 0)) {
							//NOTE initially only fill first tab panel per role
							include.setSrc(includeSrc[i][j]);
						}
						else {
							//NOTE store src for later use
							include.setAttribute("src", includeSrc[i][j]);
						}
					}
				}
			}
		}
		
	}
	
	//NOTE sessions functions
		
	protected Map gAllCourseMailTemplates = new TreeMap();
	
	public IRunManager runManager = (IRunManager)sSpring.getBean("runManager");

	public List getAllCourseMailTemplates(String pStudyCode) {
		if (gAllCourseMailTemplates.get(pStudyCode) == null) {
			gAllCourseMailTemplates.put(pStudyCode, getStudyData(pStudyCode, "course-mailTemplate"));
		}
		return (List)gAllCourseMailTemplates.get(pStudyCode);
	}

	public String getMailTypeText(String courseId, String pMailType, String pStudyCode) {
		for (Object courseMailTemplateData : getAllCourseMailTemplates(pStudyCode)) {
			if (getTagAttribute((IXMLTag)courseMailTemplateData, "course").equals(courseId) && 
				getTagAttribute((IXMLTag)courseMailTemplateData, "type").equals(pMailType)) {
				return getTagAttribute((IXMLTag)courseMailTemplateData, "text").replace(AppConstants.statusCrLfReplace, "\n");
			}
		}
		return "";
	}

	public String getSMCourseCode(String courseId, String pStudyCode) {
		List allContextsData = getStudyData(pStudyCode, "course");
		for (Object contextData : allContextsData) {
			if (getTagAttribute((IXMLTag)contextData, "id").equals(courseId)) {
				return getTagAttribute((IXMLTag)contextData, "course");
			}
		}
		return "";
	}

	public String getSMCourseName(String courseId, String pStudyCode) {
		List allContextsData = getStudyData(pStudyCode, "course");
		for (Object contextData : allContextsData) {
			if (getTagAttribute((IXMLTag)contextData, "id").equals(courseId)) {
				return getTagAttribute((IXMLTag)contextData, "name");
			}
		}
		return "";
	}

	public List getAccountSessions(Map pContexts, String pCode) {
		List lCSTags = getStudyData(pCode, "course-session");
		List lCourseTags = getStudyData(pCode, "course");
		Map lCourseCodes = new TreeMap();
		for (Object lObject : lCourseTags) {
			IXMLTag lTag = (IXMLTag)lObject;
			if (lTag.getAttribute("used").equalsIgnoreCase("true")) {
				lCourseCodes.put(lTag.getAttribute("id"), lTag.getAttribute("course"));
			}
		}
		List lASessions = new ArrayList();
		for (Object lObject : lCSTags) {
			IXMLTag lTag = (IXMLTag)lObject;
			if (lTag.getAttribute("used").equalsIgnoreCase("true")) {
				List lCourseIds = stringToListOfStrings(lTag.getAttribute("courses"));
				boolean lFound = false;
				for (Object lObject2 : lCourseIds) {
					String lCourseId = (String)lObject2;
					if (!lFound) {
						if (lCourseCodes.containsKey(lCourseId)) {
							if (pContexts.containsKey(lCourseCodes.get(lCourseId))) {
								lFound = true;
								lASessions.add(lTag);
							}
						}
					}
				}
			}
		}
		return lASessions;
	}
		
	public IERun getSessionRun(String pFacCode, String pSessionId) {
		String lRunName = caseName + idSeparator + pFacCode + idSeparator + pSessionId;
		List lRuns = runManager.getAllRunsByCasId(studiesCase.getCasId());
		IERun lSRun = null;
		for (Object lObject : lRuns) {
			IERun lRun = (IERun)lObject;
			if (lRun.getName().equals(lRunName))
				lSRun = lRun;
		}
		if (lSRun == null) {
			List lAdmins = (List)((IAccountManager)sSpring.getBean("accountManager")).getAllAccounts(true, AppConstants.c_role_adm);
			IEAccount lAdm;
			if ((lAdmins != null) && (lAdmins.size() > 0)) {
				lAdm = (IEAccount)lAdmins.get(0);
				lSRun = runManager.getNewRun();
				lSRun.setEAccount(lAdm);
				lSRun.setECase(studiesCase);
				lSRun.setCode(lRunName);
				lSRun.setName(lRunName);
				lSRun.setAccessmailsubject("");
				lSRun.setAccessmailbody("");
				lSRun.setXmldata(AppConstants.emptyXml);
				lSRun.setStartdate(new Date());
				lSRun.setEnddate(new Date());
				lSRun.setActive(true);
				lSRun.setOpenaccess(false);
				runManager.newRun(lSRun);
				// default of newRun is under construction, so set status after newRun
				lSRun.setStatus(AppConstants.run_status_test);
				runManager.updateRun(lSRun);
				lSRun = runManager.getRun(lSRun.getRunId());
			}
		}
		return lSRun;
	}
		
	public IXMLTag getSessionRunStatusTag(IERun pRun) {
		String lXMLData = pRun.getXmldata();
		return sSpring.getXmlManager().getXmlTree(null, lXMLData, AppConstants.data_tag);
	}
		
	public List getSessionRunData(IERun pRun, String pDataType) {
		IXMLTag lSessionRunDataRootTag = getStorageTagByRootTag(getSessionRunStatusTag(pRun), pDataType);
		List lSessionRunData = lSessionRunDataRootTag.getChildTags();
		if (lSessionRunData.size() > 0)
			return lSessionRunData;
		return new ArrayList();
	}
	
	public Map getStudentPlacesInfo(IXMLTag pSessionTag, IERun pSessionRun, String pAccUserId, Rows pRows) {
		String lStudyCode = notNullString((String)pRows.getAttribute("studyCode"));
		List lPlacesTags = getStudyData(lStudyCode, "place");
		Map lAllValidPlaces = new TreeMap();
		for (Object lObject : lPlacesTags) {
			IXMLTag lTag = (IXMLTag)lObject;
			if (getTagAttribute(lTag, "used").equals("true") && !getTagAttribute(lTag, "name").equals("")) {
				lAllValidPlaces.put(getTagAttribute(lTag, "id"), getTagAttribute(lTag, "name"));
			}
		}
		Map lRunPlacesInfo = new TreeMap();
		List lRunPlacesTags = getSessionRunData(pSessionRun, "places");
		for (Object lObject : lRunPlacesTags) {
			IXMLTag lTag = (IXMLTag)lObject;
			if (!(lAllValidPlaces.get(getTagAttribute(lTag, "placeId")) == null)) {
				List lRPList = new ArrayList();
				List lStudentTags = lTag.getChildTags();
				IXMLTag lStudentTag = null;
				int lStuPlacePosition = 0;
				int lCount = 0;
				for (Object lObject2 : lStudentTags) {
					IXMLTag lSTag = (IXMLTag)lObject2;
					if (!getTagAttribute(lSTag, "canceled").equals("true")) {
						lCount++;
						if (getTagAttribute(lSTag, "userId").equals(pAccUserId)) {
							lStudentTag = lSTag;
							lStuPlacePosition = lCount;
						}
					}
				}
				lRPList.add(lCount); //number of signed up and not withdrawn accounts
				lRPList.add(lStuPlacePosition);
				lRPList.add(lStudentTag);
				lRunPlacesInfo.put(getTagAttribute(lTag, "placeId"), lRPList);
			}
		}
			
		Map lPlacesInfo = new TreeMap();
		List lSessionPlacesTags = pSessionTag.getChildTags();
		for (Object lObject : lSessionPlacesTags) {
			IXMLTag lTag = (IXMLTag)lObject;
			String lPlaceId = getTagAttribute(lTag, "place");
			if (!(lAllValidPlaces.get(lPlaceId) == null)) {
				if (getTagAttribute(lTag, "checked").equals("true")) {
					List lInfoList = new ArrayList();
					lInfoList.add(lTag);
					List lRPList = (List)lRunPlacesInfo.get(lPlaceId);
					lInfoList.add(lRPList);
					lPlacesInfo.put(lPlaceId, lInfoList);
				}
			}
		}
		return lPlacesInfo;
	}
		
	public IXMLTag getChildTagByAttributeValue(IXMLTag pParentTag, String pAttrId, String pAttrValue) {
		IXMLTag lReturnTag = null;
		List lChildTags = pParentTag.getChildTags();
		for (Object lObject : lChildTags) {
			IXMLTag lTag = (IXMLTag)lObject;
			if (getTagAttribute(lTag, pAttrId).equals(pAttrValue))
				return lTag;
		}
		return lReturnTag;
	}
		
	public void studentActionMail(IXMLTag pSessionTag, String pMailType, String pStudyCode, String pPlaceId, String pUserId) {
		//TODO 'courses' attribute possibly contains multiple course ids
		//		match student's course ids with 'courses' course id list; proceed with first matching course id 
		List lCourseIds = stringToListOfStrings(getTagAttribute(pSessionTag, "courses"));
		String lMailText = "";
		for (Object lCourseId : lCourseIds) {
			lMailText = !lMailText.equals("") ? lMailText : getMailTypeText((String)lCourseId, pMailType, pStudyCode);
		}
		lMailText = !lMailText.equals("") ? lMailText : getMailTypeText("-1", pMailType, pStudyCode);
		String lCourseId = (String)lCourseIds.get(0);
		Map lParams = new HashMap();
		lParams.put("Cursus-code", getSMCourseCode(lCourseId, pStudyCode));
		lParams.put("Cursus-naam", getSMCourseName(lCourseId, pStudyCode));
		String lPlace = getSMPlaceName(pStudyCode, pPlaceId);
		lParams.put("Plaats", lPlace);
		lParams.put("Kwartiel", getTagAttribute(pSessionTag, "quartile"));
		String lErrors = mailStudent(pUserId, lMailText, lParams);
		if (!lErrors.equals("")) {
			Messagebox.show("Bij het sturen van een mail naar student '" + getStudentName (pUserId) + "' voor sessie '" + lPlace + 
					"' zijn de volgende fouten opgetreden: '" + lErrors + "'!", "ERROR", Messagebox.OK, Messagebox.ERROR);
		}
	}
		
	public int getMaxNumberOfStudents(IXMLTag pSessionPlaceTag) {
		int lMaxNumber = 0;
		if (pSessionPlaceTag == null) {
			return lMaxNumber;
		}
		try {
			lMaxNumber = Integer.parseInt(getTagAttribute(pSessionPlaceTag, "maximumNumber"));
		} catch (NumberFormatException e) {
		}
		return lMaxNumber;
	}
	
	public int getNumberOfStudents(IXMLTag pPlaceTag) {
		int lNumber = 0;
		for (IXMLTag lStudentTag : pPlaceTag.getChildTags()) {
			if (!getTagAttribute(lStudentTag, "canceled").equals("true")) {
				lNumber++;
			}
		}
		return lNumber;
	}
	
	public int getStudentPosition(IXMLTag pPlaceTag, IXMLTag pStudentTag) {
		if (getTagAttribute(pStudentTag, "canceled").equals("true")) {
			return 0;
		}
		int lPosition = 0;
		for (IXMLTag lStudentTag : pPlaceTag.getChildTags()) {
			if (!getTagAttribute(lStudentTag, "canceled").equals("true")) {
				lPosition++;
			}
			if (lStudentTag == pStudentTag) {
				return lPosition;
			}
		}
		return 0;
	}
	
	public boolean addOrChangeStuTag(IXMLTag pRunStatusTag, String pUserId, Map.Entry pEntry, int pOrderNr, String pStudyCode, boolean pNotifyStudent) {
		String lPlaceId = (String)pEntry.getKey();
		IXMLTag lSessionRunDataRootTag = getStorageTagByRootTag(pRunStatusTag, "places");

		IXMLTag lPlaceTag = getChildTagByAttributeValue(lSessionRunDataRootTag, "placeId", lPlaceId);
		if (lPlaceTag == null) {
			lPlaceTag = addNewChildTag(lSessionRunDataRootTag, "place");
			setTagAttribute(lPlaceTag, "placeId", lPlaceId);
		}
		IXMLTag lStudentTag = getChildTagByAttributeValue(lPlaceTag, "userId", pUserId);
		return addOrChangeStudent(pRunStatusTag, pUserId, lStudentTag, lPlaceTag, (IXMLTag)((List)pEntry.getValue()).get(0),
			pOrderNr, pStudyCode, true, false, null, false, false);
	}

	public boolean addOrChangeStudent(
			IXMLTag pRunStatusTag,
			String pUserId,
			//NOTE pStudentTag is unequal to null if fake student is de-faked
			IXMLTag pStudentTag, 
			IXMLTag pPlaceTag, 
			IXMLTag pSessionPlaceTag,
			int pOrderNr,
			String pStudyCode, 
			boolean pNotifyStudent,
			boolean pIsFakeStudent,
			//NOTE pAccount may be fake account if pIsFakeStudent = true, but is real account if pDefakeStudent = true
			//It may also be unequal to null if pIsFakeStudent = false
			IEAccount pAccount,
			boolean pDefakeStudent,
			boolean showMessageboxes) {
		//get current values, before adjusting student tags
		int lMaxNumberOfStudents = getMaxNumberOfStudents(pSessionPlaceTag);
		int lNumberOfStudents = getNumberOfStudents(pPlaceTag);
		String lPlaceId = getTagAttribute(pPlaceTag, "placeId");

		String lPlaceName = "";
		List tags = getStudyData(pStudyCode, "place");
		for (Object object : tags) {
			IXMLTag tag = (IXMLTag)object;
			if (getTagAttribute(tag, "id").equals(lPlaceId)) {
				lPlaceName = getTagAttribute(tag, "name");
				break;
			}
		}

		String lStudentName = "";
		if (pAccount != null) {
			lStudentName = pAccount.getLastname();
			if (!pAccount.getNameprefix().equals("")) {
				lStudentName += ", " + pAccount.getNameprefix(); 
			}
			if (!pAccount.getInitials().equals("")) {
				lStudentName += ", " + pAccount.getInitials(); 
			}
		}
		
		String lCreationDate = "";
		boolean lAddNew = false;
		boolean lChanged = true;
		if (pStudentTag == null) {
			//NOTE new real or fake student
			lAddNew = true;
			lCreationDate = getDateAsDateTimeStr(new Date());
		} else {
			//NOTE updating existing real student or de-faking fake student
			if (getTagAttribute(pStudentTag, "canceled").equals("true")) {
				lAddNew = true;
				//put previously canceled location request at the end of the queue
				lCreationDate = getTagAttribute(pStudentTag, "creationDate");
				pPlaceTag.getChildTags().remove(pStudentTag);
			} else {
				if (!getTagAttribute(pStudentTag, "pending").equals("true") &&
					getTagAttribute(pStudentTag, "placePreference").equals("" + pOrderNr)) {
					lChanged = false;
				}
			}
		}
		
		if (showMessageboxes && lAddNew && !pIsFakeStudent && pAccount != null) {
			//NOTE if real student, check if student tag with pUserId already exists for pPlaceTag
			//If so than present error message
			List lStudentTags = pPlaceTag.getChilds("student");
			boolean lExists = false;;
			for (Object object : lStudentTags) {
				IXMLTag tag = (IXMLTag)object;
				if (getTagAttribute(tag, "userId").equals(pUserId) && !getTagAttribute(tag, "canceled").equals("true")) {
					lExists = true;
					break;
				}
			}
			if (lExists) {
				lChanged = false;
				Messagebox.show("Student '" + lStudentName + "' is al aangemeld voor sessie '" + lPlaceName + "'!", "ERROR", Messagebox.OK, Messagebox.ERROR);
				return lChanged;
			}
		}
		
		if (lAddNew) {
			pStudentTag = addNewChildTag(pPlaceTag, "student");
			setTagAttribute(pStudentTag, "userId", pUserId);
			setTagAttribute(pStudentTag, "creationDate", lCreationDate);
			if (pIsFakeStudent) {
				//for fake student store account details in student tag
				setTagAttribute(pStudentTag, "fake", "true");
				setTagAttribute(pStudentTag, "studentid", pAccount.getStudentid());
				setTagAttribute(pStudentTag, "initials", pAccount.getInitials());
				setTagAttribute(pStudentTag, "lastname", pAccount.getLastname());
				setTagAttribute(pStudentTag, "email", pAccount.getEmail());
			}
			if (pNotifyStudent) {
				//NOTE only send confirmation mail if location is added, not if order of location has changed
				String lMailType = mailTemplateTypeInitialResponseId;
				if (lMaxNumberOfStudents > 0) {
					if (lNumberOfStudents >= lMaxNumberOfStudents) {
						lMailType = mailTemplateTypeInitialWaitlistResponseId;
					}
				}
				studentActionMail(pSessionPlaceTag.getParentTag(), lMailType, pStudyCode, lPlaceId, pUserId);
			}
		}
		else {
			if (pDefakeStudent) {
				setTagAttribute(pStudentTag, "userId", pUserId);
				//NOTE if another place is chosen, studentTag has to be moved to the other place tag
				if (pStudentTag.getParentTag() != pPlaceTag) {
					pStudentTag.getParentTag().getChildTags().remove(pStudentTag);
					pStudentTag.setParentTag(pPlaceTag);
					pPlaceTag.getChildTags().add(pStudentTag);
				}
				//clear fake attributes
				setTagAttribute(pStudentTag, "fake", "");
				setTagAttribute(pStudentTag, "studentid", "");
				setTagAttribute(pStudentTag, "initials", "");
				setTagAttribute(pStudentTag, "lastname", "");
				setTagAttribute(pStudentTag, "email", "");
				String lMailType = mailTemplateTypeInitialResponseId;
				if (lMaxNumberOfStudents > 0) {
					if (lNumberOfStudents >= lMaxNumberOfStudents) {
						lMailType = mailTemplateTypeInitialWaitlistResponseId;
					}
				}
				studentActionMail(pSessionPlaceTag.getParentTag(), lMailType, pStudyCode, lPlaceId, pUserId);
				lChanged = true;
			}
		}
		if (lChanged) {
			setTagAttribute(pStudentTag, "placePreference", "" + pOrderNr);
			setTagAttribute(pStudentTag, "pending", "false");
			setTagAttribute(pStudentTag, "canceled", "false");
			setTagAttribute(pStudentTag, "lastUpdateDate", getDateAsDateTimeStr(new Date()));
		}
		if (showMessageboxes) {
			if (lAddNew) {
				Messagebox.show("Student '" + lStudentName + "' is aangemeld voor sessie '" + lPlaceName + "'.", "INFO", Messagebox.OK, Messagebox.INFORMATION);
			}
			if (pDefakeStudent) {
				String fakeStudentName = getTagAttribute(pStudentTag, "lastname");
				String initials = getTagAttribute(pStudentTag, "initials");
				if (!initials.equals("")) {
					fakeStudentName += ", " + initials; 
				}
				Messagebox.show("Fake student '" + fakeStudentName + "' is vervangen door student '" + lStudentName + "' voor sessie '" + lPlaceName + "'.", "INFO", Messagebox.OK, Messagebox.INFORMATION);
			}
		}
		return lChanged;
	}

	public boolean cancelStuTag(IXMLTag pRunStatusTag, String pUserId, Map.Entry pEntry, String pStudyCode) {
		String lPlaceId = (String)pEntry.getKey();
		IXMLTag lSessionRunDataRootTag = getStorageTagByRootTag(pRunStatusTag, "places");

		IXMLTag lPlaceTag = getChildTagByAttributeValue(lSessionRunDataRootTag, "placeId", lPlaceId);
		if (lPlaceTag == null) {
			return false;
		}
		IXMLTag lStudentTag = getChildTagByAttributeValue(lPlaceTag, "userId", pUserId);
		if (lStudentTag == null) {
			return false;
		}
		return cancelOrDeleteStudent(pRunStatusTag, lStudentTag, lPlaceTag, (IXMLTag)((List)pEntry.getValue()).get(0),
			pStudyCode, true, true, false);
	}
		
	public boolean cancelOrDeleteStudent(
			IXMLTag pRunStatusTag,
			IXMLTag pStudentTag, 
			IXMLTag pPlaceTag, 
			IXMLTag pSessionPlaceTag,
			String pStudyCode, 
			boolean pNotifyStudent, 
			boolean pNotifyNextStudent,
			boolean pDelete) {
		//get current values, before adjusting student tags
		int lMaxNumberOfStudents = getMaxNumberOfStudents(pSessionPlaceTag);
		int lNumberOfStudents = getNumberOfStudents(pPlaceTag);
		int lQueuePosition = getStudentPosition(pPlaceTag, pStudentTag);
		
		boolean lIsStudentAlreadyCanceled = getTagAttribute(pStudentTag, "canceled").equals("true");
		if (pDelete) {
			pStudentTag.getParentTag().getChildTags().remove(pStudentTag);
			pStudentTag.setParentTag(null);
			if (lIsStudentAlreadyCanceled) {
				//if already canceled do not notify student(s)
				return true;
			}
		}
		else {
			if (lIsStudentAlreadyCanceled) {
				//if already canceled do nothing
				return false;
			}
			setTagAttribute(pStudentTag, "canceled", "true");
			setTagAttribute(pStudentTag, "pending", "false");
			setTagAttribute(pStudentTag, "lastUpdateDate", getDateAsDateTimeStr(new Date()));
		}
		
		String lPlaceId = getTagAttribute(pPlaceTag, "place");
		if (pNotifyStudent) {
			studentActionMail (pSessionPlaceTag.getParentTag(), mailTemplateTypeInitialCanceledResponseId, pStudyCode, lPlaceId, getTagAttribute(pStudentTag, "userId"));
		}
		
		if (pNotifyNextStudent && lMaxNumberOfStudents > 0 && lMaxNumberOfStudents >= lQueuePosition && lNumberOfStudents > lMaxNumberOfStudents) {
			//NOTE enroll first student in waitlist if present 
			List lStudentTags = pPlaceTag.getChildTags();
			int lCount = 0;
			for (Object lObject : lStudentTags) {
				IXMLTag lSTag = (IXMLTag)lObject;
				if (!getTagAttribute(lSTag, "canceled").equals("true")) {
					lCount++;
					if (lCount == lMaxNumberOfStudents) {
						//NOTE probably not necessary to check for pending being true
 						if (!getTagAttribute(lSTag, "pending").equals("true")) {
 							//NOTE change status of first student on waitlist to 'pending' and send mail
 							setTagAttribute(lSTag, "pending", "true");
							studentActionMail (pSessionPlaceTag.getParentTag(), mailTemplateTypeFollowUpResponseId, pStudyCode, lPlaceId, getTagAttribute(lSTag, "userId"));
 						}
					}
				}
			}
		}
		return true;
	}
		
	public void checkForPendingStudents(IXMLTag pRunStatusTag, String pUserId, String pPlaceId, IXMLTag pPlacesTag) {
		
	}

	//NOTE student enroll functions

	public List getAccSessions (String pStuStudyCode) {
		if (hasRole[0]) {
			// admin can sign up as student for all meetings, to enable testing the procedure
			return getAccountSessions(contexts, pStuStudyCode);
		} else {
			return getAccountSessions(accountContexts, pStuStudyCode);
		}
	}
	
	public void selectSession (List pAccSessions, int pIndex, String pRowsId) {
		IXMLTag lSessionTag = (IXMLTag)pAccSessions.get(pIndex);
		Events.sendEvent("onSelectSession", vView.getComponent(pRowsId), new Object[]{lSessionTag});
	}
	
	public IERun getStuRun(String pStuStudyCode, IXMLTag pSessionTag) {
		//get run; create if doesn't exist
		return getSessionRun(pStuStudyCode, getTagAttribute(pSessionTag, "id"));
	}

	public Map getStuPlacesInfo(Rows rows, IERun pRun, String pStuStudyCode, IXMLTag pSessionTag) {
		return getStudentPlacesInfo(pSessionTag, pRun, account.getUserid(), rows);
	}

	public Object[] createStuRows(Rows rows, String pStuStudyCode, Map pPlacesInfo, Map pComboItems, Map pWorkStatus, int pMaxPreferenceNumber, String pDeleteButtonId) {
		//NOTE if this code is copied as zscript Map parameters are handled by value, not by reference!
		//Therefore pComboItems and pWorkStatus are returned as Object array
		
		rows.getChildren().clear();
		
		pComboItems.clear();
		pWorkStatus.clear();
		rows.setAttribute("canAddNewRow", true);
		Map lAddRowItems = new TreeMap();
		boolean lReload = false;
		for (Object lObject : pPlacesInfo.entrySet()) {
			Map.Entry lEntry = (Map.Entry)lObject;
			String lPlaceId = (String)lEntry.getKey();
			List lInfoList = (List)lEntry.getValue();
			IXMLTag lSessionPlaceTag = (IXMLTag)lInfoList.get(0);
			List lStuPlaceInfo = (List)lInfoList.get(1);
			if ((lStuPlaceInfo != null) && ((int)lStuPlaceInfo.get(1) != 0)) {
				int lPreferenceNumber = Integer.parseInt(getTagAttribute((IXMLTag)lStuPlaceInfo.get(2), "placePreference"));
				if (getTagAttribute((IXMLTag)lStuPlaceInfo.get(2), "pending").equals("true")) {
					Messagebox.Button lButtonPressed = Messagebox.show("Er is een plaats vrijgekomen in locatie " + getSMPlaceName(pStuStudyCode, lPlaceId) + 
						", waar u als eerste op de wachtlijst stond. Wilt u hiervan gebruik maken? Zo nee, dan wordt u verwijderd van de wachtlijst.", 
						"Locatie beschikbaar", new Messagebox.Button[] {Messagebox.Button.OK,  Messagebox.Button.CANCEL}, 
						new String[] {"Ja","Nee"}, Messagebox.QUESTION, Messagebox.Button.OK, null);
					if (lButtonPressed == Messagebox.Button.OK) {
						lAddRowItems.put(lPreferenceNumber, lInfoList);
					} else {
						pComboItems.put (Integer.parseInt(lPlaceId), getPlaceName(rows, lPlaceId));
					}
					lReload = true;
				} else
					lAddRowItems.put(lPreferenceNumber, lInfoList);
			} else {
				// make key of type Integer, so places will be ordered alphabetically (if they're entered in the correct order)
				pComboItems.put (Integer.parseInt(lPlaceId), getPlaceName(rows, lPlaceId));
			}
		}
		int lRowNumber = 1; 
		boolean lFirstEmptyFound = false;
		for (Object lObject : lAddRowItems.entrySet()) {
			Map.Entry lEntry = (Map.Entry)lObject;
			// automatically sorted on preferencenumber!
			int lPrefNr = (Integer)lEntry.getKey();
			for (int i = (lRowNumber); i < lPrefNr; i++) {
				boolean lFirstEmpty = false;
				if (!lFirstEmptyFound && (boolean)rows.getAttribute("canAddNewRow")) {
					lFirstEmpty = true;
					lFirstEmptyFound = true;
				}
				pWorkStatus.put(i, "0");
				addEmptyStuRow (rows, i, lFirstEmpty, pDeleteButtonId);
			}
			String lPlaceId = getTagAttribute((IXMLTag)((List)lEntry.getValue()).get(0), "place");
			if ((boolean)rows.getAttribute("canAddNewRow")) {
				pWorkStatus.put(lPrefNr, lPlaceId);
				addStuRow (rows, (List)lEntry.getValue(), lPrefNr, pDeleteButtonId);
			} else {
				//higher prefered location has place for student; remove lower prefered locations from list
				pComboItems.put (Integer.parseInt(lPlaceId), getPlaceName(rows, lPlaceId));
				pWorkStatus.put(lPrefNr, "0");
				addEmptyStuRow (rows, lPrefNr, false, pDeleteButtonId);
			}
			lRowNumber = lPrefNr + 1;
		}
		for (int i = (lRowNumber); i <= pMaxPreferenceNumber; i++) {
			boolean lFirstEmpty = false;
			if (!lFirstEmptyFound && (boolean)rows.getAttribute("canAddNewRow")) {
				lFirstEmpty = true;
				lFirstEmptyFound = true;
			}
			pWorkStatus.put(i, "0");
			addEmptyStuRow (rows, i, lFirstEmpty, pDeleteButtonId);
		}
		Object[] result = new Object[2];
		result[0] = pComboItems;
		result[1] = pWorkStatus;
		if (lReload) {
			//NOTE 'pending' status has changed; reload page but first we had to determine new value for gWorkStatus
			Events.sendEvent("onClick", vView.getComponent("stuChangeButton"), "");
			return result;
		}
		if (lAddRowItems.isEmpty()) {
			vView.getComponent("stuSendButton").setVisible(true);
			vView.getComponent("stuChangeButton").setVisible(false);
		} else {
			vView.getComponent("stuSendButton").setVisible(false);
			vView.getComponent("stuChangeButton").setVisible(true);
		}
		List lRows = rows.getChildren();
		for (int i = 0; i < lRows.size(); i++) {
			//recalculate combobox items
			Events.sendEvent("onFillCombo", rows, lRows.get(i));
		}
		return result;
	}

	public Row addStuRow(Rows pRows, List pStuInfo, int pRowNumber, String pDeleteButtonId) {
		Row lRow = new Row();
		pRows.appendChild(lRow);
		lRow.setId(getRowsId(pRows) + idSeparator + pRowNumber);
		lRow.setAttribute("isEmpty", false);
		lRow.setAttribute("subRow", false);
		Events.sendEvent("onFillRow", pRows , new Object[]{lRow, pStuInfo});
		//NOTE delete button is common for every row
		Component lDeleteButton = (Component)vView.getComponent("templateStuDeleteButton").clone();
		lDeleteButton.setId(lRow.getId() + idSeparator + pDeleteButtonId); 
		lRow.appendChild(lDeleteButton);
		lDeleteButton.setVisible(true);
		return lRow;
	}

	public Row addEmptyStuRow(Rows pRows, int pRowNumber, boolean pFirstEmpty, String pDeleteButtonId) {
		Row lRow = new Row();
		pRows.appendChild(lRow);
		lRow.setId(getRowsId(pRows) + idSeparator + pRowNumber);
		lRow.setAttribute("isEmpty", true);
		lRow.setAttribute("subRow", false);
		Events.sendEvent("onFillEmptyRow", pRows, new Object[]{lRow, pFirstEmpty});
		//NOTE delete button is common for every row
		Component lDeleteButton = (Component)vView.getComponent("templateStuDeleteButton").clone();
		lDeleteButton.setId(lRow.getId() + idSeparator + pDeleteButtonId);
		lRow.appendChild(lDeleteButton);
		lDeleteButton.setVisible(false);
		return lRow;
	}

	public Component appendStuCombobox(Row row, IXMLTag tag, String key, Object items, String dependencies) {
		Combobox component = (Combobox)vView.getComponent("templateStuCombobox").clone();
		appendComponent(row, component, tag, key, dependencies);
		setComboitems(component, tag, key, items);
		setComboitemsValue(component, tag, key);
		return component;
	}
		
	public void fillStuPlacesCombo(Combobox pCombo, Map pComboItems) {
		pCombo.getChildren().clear();
		for (Object object : pComboItems.entrySet()) {
			Map.Entry entry = (Map.Entry)object;
			if (!entry.getKey().equals("") && !entry.getValue().equals("")) {
				Comboitem item = pCombo.appendItem((String)entry.getValue());
				item.setValue(entry.getKey());
			}
		}
	}
	
	public List getLocationStatusLabel(String pPlaceId, int pPreferenceNr, Map pPlacesInfo) {
		List lLocationInfo = new ArrayList();
		String lLabelValue = "";
		if (pPlaceId.equals("0")) {
			lLocationInfo.add(lLabelValue);
			lLocationInfo.add(true);
			return lLocationInfo;
		}
		int lQueuePosition = 0;
		int lSignedUpNr = 0;
		if (pPlacesInfo.get(pPlaceId) != null && ((List)pPlacesInfo.get(pPlaceId)).get(1) != null) {
			lQueuePosition = (Integer)((List)((List)pPlacesInfo.get(pPlaceId)).get(1)).get(1);
			lSignedUpNr = (Integer)((List)((List)pPlacesInfo.get(pPlaceId)).get(1)).get(0);
		}
		if (lQueuePosition > 0) {
			IXMLTag lStuTag = (IXMLTag)((List)((List)pPlacesInfo.get(pPlaceId)).get(1)).get(2);
			if (!getTagAttribute(lStuTag, "placePreference").equals(pPreferenceNr + ""))
				//stored selected location has other preference 
				lQueuePosition = 0;
		}
		int lMaxNr = 0;
		try {
			lMaxNr = Integer.parseInt(getTagAttribute((IXMLTag)((List)pPlacesInfo.get(pPlaceId)).get(0), "maximumNumber"));
		} catch (NumberFormatException e) {
		}
		lLabelValue = lQueuePosition == 0 ? 
			((lMaxNr == 0) || (lMaxNr > lSignedUpNr) ? "te plaatsen" : "naar wachtlijst") :
			((lMaxNr == 0) || (lMaxNr >= lQueuePosition) ? "geplaatst" : "op wachtlijst");
		lLocationInfo.add(lLabelValue);
		//new preference location can be added if no vacancy left in current location, and not already placed 
		boolean lCanAddRow = lQueuePosition == 0 ? 
			((lMaxNr > 0) && (lMaxNr <= lSignedUpNr) ? true : false) :
			((lMaxNr > 0) && (lMaxNr < lQueuePosition) ? true : false);
		//lCanAddRow = true; //TEST!
		lLocationInfo.add(lCanAddRow);
		return lLocationInfo;
	}

}
