<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>

<%
List<List<String>> result = (List<List<String>>)request.getAttribute("jspResult");
if (result != null) {
PrintWriter csv = response.getWriter();
	csv.print("<table id=\"studentresults\" style=\"width:1200px\">");
	int rows = 1;
	for (List<String> item : result) {
		int length = item.size();
		int counter = 1;
		if (rows == 1) {
			csv.print("<thead><tr>");
			for (String string : item) {
				csv.print("<td>");
				csv.print(string);
				counter++;
				csv.print("</td>");
			}
			csv.print("</tr></thead>");
			csv.print("<tbody>");
		} else {
			csv.print("<tr>");
			for (String string : item) {
				csv.print("<td>");
				csv.print(string);
				counter++;
				csv.print("</td>");
			}
			csv.print("</tr>");
		}
		rows++;
	}
	csv.print("</tbody>");
	csv.print("</table>");
}
%>