<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>

<%
//NOTE is used to show data tab separated in a textarea

List<List<String>> result = (List<List<String>>)request.getAttribute("queryResult");
if (result != null) {
PrintWriter csv = response.getWriter();
	csv.print("<textarea style=\"width:1200px\" rows=\"10\">");
	for (List<String> item : result) {
		int length = item.size();
		int counter = 1;
		for (String string : item) {
			csv.print(string);
			if (counter == length) {
				csv.print("\n");
			}
			else {
				csv.print("\t");
			}
			counter++;
		}
	}
	csv.print("</textarea>");
}
%>