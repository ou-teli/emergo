
//try {
//	var controlbarReady = playerMeta;
//} catch (err){
//}

//var $j = jQuery.noConflict();
var $j = jQuery;
var cb_PlayerName;

function addjwcontrolbar (obj){
	cb_PlayerName = obj.id;
	var controlbarReady = JWplayerReady (obj);
}

JWplayerReady = function(obj) {
	try {
		var player = $j("#" + cb_PlayerName);
//		player.each(function (){
//			this.addModelListener("TIME","timeTracker");
//			this.addModelListener("STATE","playTracker");
//			this.addControllerListener("MUTE","muteTracker");
//			this.addControllerListener("VOLUME","volumeTracker");
//		});
		$j(player).addControlbar();
		console.log("playerready ok");
	} catch (err){
		alert ("fout: " + err.message);
		console.log("playerready error: " + err.message);
	}
//	try {
//		controlbarReady(obj);
//	} catch (err){
//	}
};

//playerMeta = function(obj) {
//	alert("test");
//	return;
//}

jQuery.fn.addControlbar = function (options){
	var settings = jQuery.extend({
		'width': this.width(),
		'prefix': 'jw_controlbar',
		'elements': {
			'play': {
			},
			'pause': {
			},
			'stop': {
			},
			'currenttime': {
			},
			'scrubber': {
			},
			'totaltime': {
			},
			'fullscreen': {
			},
			'mute': {
			},
			'unmute': {
			},
			'volume': {
			}
		}
	}, options);
	
	var cbdiv = $j("."+settings.prefix);
//	$j(cbdiv).append(buildControlbar(settings));
//	alert("ready");
//	var p2 = $j("#"+player['id']);
//	var player = $j(this[0]);
//	alert($j(jwplayer(player)).getVolume());
//	alert($j(p2).getVolume());
	var play = $j(cbdiv).children("."+settings.prefix+".play");
	var pause = $j(cbdiv).children("."+settings.prefix+".pause");
	var stop = $j(cbdiv).children("."+settings.prefix+".stop");
	var scrubber = $j(cbdiv).children("."+settings.prefix+".scrubber");
	var currenttime = $j(cbdiv).children("."+settings.prefix+".currenttime");
	var totaltime = $j(cbdiv).children("."+settings.prefix+".totaltime");
	var fullscreen = $j(cbdiv).children("."+settings.prefix+".fullscreen");
	var mute = $j(cbdiv).children("."+settings.prefix+".mute");
	var unmute = $j(cbdiv).children("."+settings.prefix+".unmute");
	var volume = $j(cbdiv).children("."+settings.prefix+".volume");

	play.click(function() {
		jwplayer(cb_PlayerName).play();
	});

	pause.click(function() {
		jwplayer(cb_PlayerName).pause();
	});

	stop.click(function() {
		jwplayer(cb_PlayerName).stop();
	});

	$j(currenttime).html("00:00");

	$j(scrubber).slider({
			range: "min",
			min: 0,
			max: 100000,
			value: 0,
			slide: function(event, ui) {
				var duration = jwplayer(cb_PlayerName).getDuration();
				var seekTime = Math.round(duration * ui.value / 100000);
				jwplayer(cb_PlayerName).seek(seekTime);
			}
	});

	$j(totaltime).html("00:00");

	$j(mute).click(function() {
		jwplayer(cb_PlayerName).setMute(true);
	});

	$j(unmute).click(function() {
		jwplayer(cb_PlayerName).setMute(false);
	});

	$j(volume).slider({
			range: "min",
			min: 0,
			max: 100,
			value: jwplayer(cb_PlayerName).getVolume(),
			slide: function(event, ui) {
				jwplayer(cb_PlayerName).setVolume(ui.value);
			}
		});

	fullscreen.click(function() {
		jwplayer(cb_PlayerName).setFullscreen(true);
	});

};

function buildControlbar(settings){
	var result = "";
//	result += "<div class='"+settings.prefix+"' style='width:"+settings.width+"px'>";
	for (element in settings.elements){
		var style = "";
		for (styleElement in settings.elements[element]){
			style += styleElement+":"+settings.elements[element][styleElement]+";";
		}
		result += "<div class='"+settings.prefix+" "+element+"' style='"+style+"'>&nbsp;</div>";
	}
//	result += "</div>";
	return result;
}

function pad(s,l) {
	return( l.substr(0, (l.length-s.length) )+s );
}

function formatTime(seconds){
	var result = "";
	var remaining = Math.floor(seconds);
	
	if (seconds > 3600){
		result += pad((Math.floor(remaining/3600)).toString(),"00")+":";
		remaining = remaining % 3600;
	}
	
	result += pad((Math.floor(remaining/60)).toString(),"00")+":";
	remaining = remaining % 60;

	result += pad(remaining.toString(),"00")+"";
	
	return result;
}

function play(player){
	jwplayer(cb_PlayerName).play();
	return false;
}

function stop(player){
	jwplayer(cb_PlayerName).stop();
	return false;
}

function seek(player){
	jwplayer(cb_PlayerName).stop();
	return false;
}

function fullscreen(player){
	jwplayer(cb_PlayerName).setFullscreen();
	return false;
}

function timeTracker(obj){
	var percentComplete = Math.round(100000 * obj.position / obj.duration);

	var cbdiv = $j(".jw_controlbar");
	$j(cbdiv).children(".jw_controlbar.scrubber").slider('option', 'value', percentComplete);
	$j(cbdiv).children(".jw_controlbar.currenttime").html(formatTime(obj.position));
	$j(cbdiv).children(".jw_controlbar.totaltime").html(formatTime(obj.duration));
}

function playTracker(obj){
	var cbdiv = $j(".jw_controlbar");
	if (obj){
		$j(cbdiv).children(".jw_controlbar.pause").css("display","block");
		$j(cbdiv).children(".jw_controlbar.play").css("display","none");
	} else {
		$j(cbdiv).children(".jw_controlbar.pause").css("display","none");
		$j(cbdiv).children(".jw_controlbar.play").css("display","block");
	}
}

function muteTracker(obj){
	var player = $j("#" + cb_PlayerName);

	var cbdiv = $j(".jw_controlbar");

	if (!(obj.mute)){
		$j(cbdiv).children(".jw_controlbar.mute").css("display","block");
		$j(cbdiv).children(".jw_controlbar.unmute").css("display","none");
		setVolume($j(cbdiv).children(".jw_controlbar.volume"), jwplayer(cb_PlayerName).getVolume());
	} else {
		$j(cbdiv).children(".jw_controlbar.mute").css("display","none");
		$j(cbdiv).children(".jw_controlbar.unmute").css("display","block");
		setVolume($j(cbdiv).children(".jw_controlbar.volume"), 0);
	}
}

function volumeTracker(obj){
	var cbdiv = $j(".jw_controlbar");
	setVolume($j(cbdiv).children(".jw_controlbar.volume"), obj.volume);
}

function setVolume(slider, value){
	$j(slider).slider('option', 'value', value);
}