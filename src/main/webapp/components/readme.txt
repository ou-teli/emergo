This folder contains all component table records as an IMS content package components.zip.
If the Emergo logon screen is opened, this file is read and components are automatically imported, if not existing yet, or updated. See method InitDatabase within SSpring class.
If logon screen is opened on localhost components.zip is preserved, otherwise it is deleted after it is imported.
The file is part of the Emergo installation package. The same installation package can be used for updating the platform too.
The file is created by an admin in the component overview by exporting the components. Default all active components are checked for export, but the admin can (un)check components.