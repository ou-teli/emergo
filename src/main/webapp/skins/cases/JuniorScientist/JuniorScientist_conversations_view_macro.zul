<?xml version="1.0" encoding="UTF-8"?>

<zk xmlns:h="http://www.w3.org/1999/xhtml">

	<style src="${arg.a_propertyMap.get('zulfilepath')}assets/html/css/reset.css"/>
	<style src="${arg.a_propertyMap.get('zulfilepath')}assets/html/css/style.css"/>

	<h:link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,200,900italic,900,700italic,700,600italic,600,400italic,300italic,300,200italic' rel='stylesheet' type='text/css'/>

	<h:script src="https://kit.fontawesome.com/d0c839778d.js" crossorigin="anonymous"></h:script>

	<div id="${arg.a_propertyMap.get('macroBoxUuid')}_div" width="1200px" height="650px" class="emergo-cutout">
		<custom-attributes fragmentTag="${arg.a_propertyMap.get('fragmentTag')}"/>
		<custom-attributes conversationStyle="${arg.a_propertyMap.get('conversationStyle')}"/>
		<custom-attributes personsStyle="${arg.a_propertyMap.get('personsStyle')}"/>
		<custom-attributes personsSlideIn="${arg.a_propertyMap.get('personsSlideIn')}"/>
		<attribute name="onCreate">
			<![CDATA[
				Events.sendEvent("onSetFragmentTag", self, self.getAttribute("fragmentTag"));
				Events.sendEvent("onSetConversationStyle", self, self.getAttribute("conversationStyle"));
				Events.sendEvent("onSetPersonsStyle", self, self.getAttribute("personsStyle"));
				Events.sendEvent("onSetPersonsSlideIn", self, self.getAttribute("personsSlideIn"));
			]]>
		</attribute>
		<attribute name="onSetFragmentTag">
			<![CDATA[
				self.setAttribute("fragmentTag", event.getData());
				self.setVisible(self.getAttribute("fragmentTag") != null);
			]]>
		</attribute>
		<attribute name="onSetConversationStyle">
			<![CDATA[
				System.out.println(event.getData());
				self.setAttribute("conversationStyle", event.getData());
				String conversationStyle = self.getAttribute("conversationStyle");
				Object fellow = self.getFellow(self.getId() + "_conversation");
				fellow.setVisible(!conversationStyle.equals(""));
				String style = conversationStyle.equals("") ? "" : "position:absolute;left:0px;top:0px;width:100%;height:100%;" + conversationStyle;
				if (fellow.getStyle() != null && fellow.getStyle().equals(style)) {
					return;
				}
				fellow.setStyle(style);
			]]>
		</attribute>
		<attribute name="onSetPersonsStyle">
			<![CDATA[
				self.setAttribute("personsStyle", event.getData());
				String personsStyle = self.getAttribute("personsStyle");
				String style = personsStyle.equals("") ? "" : "position:absolute;left:0px;top:0px;width:100%;height:100%;" + self.getAttribute("personsStyle");
				Object fellow = self.getFellow(self.getId() + "_persons");
				if (fellow.getStyle() != null && fellow.getStyle().equals(style)) {
					return;
				}
				fellow.setStyle(style);
			]]>
		</attribute>
		<attribute name="onSetPersonsSlideIn">
			<![CDATA[
				self.setAttribute("personsSlideIn", event.getData());
				Object fellow = self.getFellow(self.getId() + "_persons");
				fellow.setClass("");
				if (!self.getAttribute("personsStyle").equals("")) {
					if (self.getAttribute("personsSlideIn")) {
						fellow.setClass("js-character char-slidein-right");
						fellow = self.getFellow(self.getId() + "_timer");
						fellow.start();
					}
				}
			]]>
		</attribute>
		
		<timer id="${arg.a_propertyMap.get('macroBoxUuid')}_div_timer" running="false" repeats="false" delay="1000">
			<attribute name="onTimer">
				<![CDATA[
					String id = self.getId().replace("_timer", "_persons");
					Object fellow = self.getFellow(id);
					fellow.setClass("");
				]]>
			</attribute>
		</timer>

		<div id="${arg.a_propertyMap.get('macroBoxUuid')}_div_conversation" visible="false"/>

		<div id="${arg.a_propertyMap.get('macroBoxUuid')}_video" visible="${arg.a_propertyMap.get('fragmentUrl') != ''}" style="display:flex;">
			<custom-attributes runComponentId="${arg.a_propertyMap.get('macroBoxUuid')}"/>
			<custom-attributes url="${arg.a_propertyMap.get('fragmentUrl')}"/>
			<attribute name="onCreate">
				<![CDATA[
					Events.sendEvent("onSetUrl", self, self.getAttribute("url"));
				]]>
			</attribute>
			<attribute name="onSetUrl">
				<![CDATA[
					import nl.surf.emergo.view.VView;
					self.getChildren().clear();
					self.setAttribute("url", event.getData());
					self.setVisible(!self.getAttribute("url").equals(""));
					if (self.getAttribute("url").equals("")) {
						return;
					}
					Include include = new Include();
					self.appendChild(include);
					include.setAttribute("runComponentId", self.getAttribute("runComponentId"));
					include.setAttribute("url", self.getAttribute("url"));
					include.setAttribute("playerName", self.getId() + "_video");
					include.setAttribute("showControls", "true");
					include.setAttribute("playerAutostart", "true");
//					include.setAttribute("playerStretching", "fit");
					include.setAttribute("video_size_width", "1200px");
					include.setAttribute("video_size_height", "650px");
					include.setSrc(VView.v_run_flash_fr);
				]]>
			</attribute>
		</div>

		<div id="${arg.a_propertyMap.get('macroBoxUuid')}_div_persons"/>

		<div id="${arg.a_propertyMap.get('macroBoxUuid')}_buttons" class="js-answer" style="display:flex;flex-direction:row;flex-wrap:nowrap;justify-content:center;">
			<custom-attributes macroBoxUuid="${arg.a_propertyMap.get('macroBoxUuid')}"/>
			<custom-attributes buttons="${arg.a_propertyMap.get('buttons')}"/>
			<attribute name="onCreate">
				<![CDATA[
					Events.sendEvent("onSetButtons", self, self.getAttribute("buttons"));
				]]>
			</attribute>
			<attribute name="onSetButtons">
				<![CDATA[
					self.setAttribute("buttons", event.getData());
					self.getChildren().clear();
					if (self.getAttribute("buttons") == null) {
						return;
					}
					for (Map button : buttons) {
						Div div = new Div();
						self.appendChild(div);
						div.setAttribute("macroBoxUuid", self.getAttribute("macroBoxUuid"));
						div.setAttribute("buttonTag", button.get("buttonTag"));
						org.zkoss.zhtml.Button htmlButton = new org.zkoss.zhtml.Button();
						div.appendChild(htmlButton);
						htmlButton.setSclass("js-btn primary");
						Html html = new Html();
						htmlButton.appendChild(html);
						html.setContent(button.get("buttonText"));
						div.addEventListener("onClick", new EventListener() {
							public void onEvent(Event event) {
								Events.echoEvent("onButtonClick", nl.surf.emergo.control.CDesktopComponents.vView().getComponent(div.getAttribute("macroBoxUuid")), div.getAttribute("buttonTag"));
							}
						});
					}
				]]>
			</attribute>
		</div>

		<div id="${arg.a_propertyMap.get('macroBoxUuid')}_dialogue" style="top:610px;">
			<custom-attributes dialogueIsVisible="${arg.a_propertyMap.get('dialogueIsVisible')}"/>
			<custom-attributes personIsPc="${arg.a_propertyMap.get('personIsPc')}"/>
			<custom-attributes url="${arg.a_propertyMap.get('fragmentUrl')}"/>
			<attribute name="onCreate">
				<![CDATA[
					Events.sendEvent("onSetDialogueIsVisible", self, self.getAttribute("dialogueIsVisible"));
					Events.sendEvent("onSetPersonIsPc", self, self.getAttribute("personIsPc"));
					Events.sendEvent("onSetUrl", self, self.getAttribute("url"));
				]]>
			</attribute>
			<attribute name="onSetDialogueIsVisible">
				<![CDATA[
					if (event.getData() != null) {
						self.setAttribute("dialogueIsVisible", event.getData());
						self.setVisible(self.getAttribute("dialogueIsVisible"));
					} else {
						//NOTE not sure about this... (possibly occurs after starting a new session):
						self.setVisible(true);
					}
				]]>
			</attribute>
			<attribute name="onSetPersonIsPc">
				<![CDATA[
					self.setAttribute("personIsPc", event.getData());
					String classStr = "js-dialogue" + (self.getAttribute("personIsPc") ? " inner-dialogue" : "");
					if (self.getClass() != null && self.getClass().equals(classStr)) {
						return;
					}
					self.setClass(classStr);
				]]>
			</attribute>
			<attribute name="onSetUrl">
				<![CDATA[
					self.setAttribute("url", event.getData());
					String style = "";
					if (!self.getAttribute("url").equals("")) {
						style = "top:520px;width:1120px;height:50px;box-shadow:none;background-color:transparent";
					}
					self.setStyle(style);
				]]>
			</attribute>
			<div class="js-dialogue__inner">
				<div id="${arg.a_propertyMap.get('macroBoxUuid')}_close_button">
					<custom-attributes macroBoxUuid="${arg.a_propertyMap.get('macroBoxUuid')}"/>
					<custom-attributes buttonTag="${arg.a_propertyMap.get('closeButtonTag')}"/>
					<custom-attributes btnTooltip="${arg.a_propertyMap.get('btnCloseTooltip')}"/>
					<attribute name="onCreate">
						<![CDATA[
							Events.sendEvent("onSetButtonTag", self, self.getAttribute("buttonTag"));
							Events.sendEvent("onSetButtonTooltip", self, self.getAttribute("btnTooltip"));
						]]>
					</attribute>
					<attribute name="onSetButtonTag">
						<![CDATA[
							self.setAttribute("buttonTag", event.getData());
							self.setVisible(self.getAttribute("buttonTag") != null);
						]]>
					</attribute>
					<attribute name="onSetButtonTooltip">
						<![CDATA[
							self.setAttribute("btnTooltip", event.getData());
							String content = self.getAttribute("btnTooltip");
							Object fellowDiv = self.getFellow(self.getId() + "_tooltipdiv");
							Object fellowHtml = self.getFellow(self.getId() + "_tooltiphtml");
							fellowDiv.setVisible(content != null && !content.equals(""));
							if (fellowHtml.getContent() == content) {
								return;
							}
							fellowHtml.setContent(content);
						]]>
					</attribute>
					<attribute name="onClick">
						<![CDATA[
							Events.echoEvent("onButtonClick", nl.surf.emergo.control.CDesktopComponents.vView().getComponent(self.getAttribute("macroBoxUuid")), self.getAttribute("buttonTag"));
						]]>
					</attribute>
					<h:button id="${arg.a_propertyMap.get('macroBoxUuid')}_close_button_button" class="js-btn round primary js-dialog-close small">
						<h:i class="fas fa-times"/>
						<h:div id="${arg.a_propertyMap.get('macroBoxUuid')}_close_button_tooltipdiv" class="js-tooltip" style="left: -242px;">
							<html id="${arg.a_propertyMap.get('macroBoxUuid')}_close_button_tooltiphtml" />
						</h:div>
					</h:button>
				</div>

				<div id="${arg.a_propertyMap.get('macroBoxUuid')}_left_button" class="js-dialogue__button left">
					<custom-attributes macroBoxUuid="${arg.a_propertyMap.get('macroBoxUuid')}"/>
					<custom-attributes buttonTag="${arg.a_propertyMap.get('leftButtonTag')}"/>
					<custom-attributes btnTooltip="${arg.a_propertyMap.get('btnLeftTooltip')}"/>
					<attribute name="onCreate">
						<![CDATA[
							Events.sendEvent("onSetButtonTag", self, self.getAttribute("buttonTag"));
							Events.sendEvent("onSetButtonTooltip", self, self.getAttribute("btnTooltip"));
						]]>
					</attribute>
					<attribute name="onSetButtonTag">
						<![CDATA[
							self.setAttribute("buttonTag", event.getData());
							self.setVisible(self.getAttribute("buttonTag") != null);
						]]>
					</attribute>
					<attribute name="onSetButtonTooltip">
						<![CDATA[
							self.setAttribute("btnTooltip", event.getData());
							String content = self.getAttribute("btnTooltip");
							Object fellowDiv = self.getFellow(self.getId() + "_tooltipdiv");
							Object fellowHtml = self.getFellow(self.getId() + "_tooltiphtml");
							fellowDiv.setVisible(content != null && !content.equals(""));
							if (fellowHtml.getContent() == content) {
								return;
							}
							fellowHtml.setContent(content);
						]]>
					</attribute>
					<attribute name="onClick">
						<![CDATA[
							Events.echoEvent("onButtonClick", nl.surf.emergo.control.CDesktopComponents.vView().getComponent(self.getAttribute("macroBoxUuid")), self.getAttribute("buttonTag"));
						]]>
					</attribute>
					<h:button id="${arg.a_propertyMap.get('macroBoxUuid')}_left_button_button" class="js-btn round primary">
						<h:i class="fas fa-chevron-left"/>
						<h:div id="${arg.a_propertyMap.get('macroBoxUuid')}_left_button_tooltipdiv" class="js-tooltip" style="left: -242px;">
							<html id="${arg.a_propertyMap.get('macroBoxUuid')}_left_button_tooltiphtml" />
						</h:div>
					</h:button>
				</div>
				<div class="js-dialogue-portrait">
					<div id="${arg.a_propertyMap.get('macroBoxUuid')}_portrait">
						<custom-attributes portraitClass="${arg.a_propertyMap.get('personPortraitClass')}"/>
						<custom-attributes portraitUrl="${arg.a_propertyMap.get('personPortraitUrl')}"/>
						<custom-attributes portraitName="${arg.a_propertyMap.get('personPortraitName')}"/>
						<attribute name="onCreate">
							<![CDATA[
								Events.sendEvent("onSetPortraitClass", self, self.getAttribute("portraitClass"));
								Events.sendEvent("onSetPortraitUrl", self, self.getAttribute("portraitUrl"));
								Events.sendEvent("onSetPortraitName", self, self.getAttribute("portraitName"));
							]]>
						</attribute>
						<attribute name="onSetPortraitClass">
							<![CDATA[
								self.setAttribute("portraitClass", event.getData());
								String classStr = "js-avatar char-" + self.getAttribute("portraitClass");
								if (self.getClass() != null && self.getClass().equals(classStr)) {
									return;
								}
								self.setClass(classStr);
							]]>
						</attribute>
						<attribute name="onSetPortraitUrl">
							<![CDATA[
								self.setAttribute("portraitUrl", event.getData());
								String urlStr = self.getAttribute("portraitUrl");
								String styleStr = "";
								if (!urlStr.equals("")) {
									styleStr = "background-size:100%;background-repeat:no-repeat;background-image: url('" + urlStr + "');";
								}
								if (self.getStyle() != null && self.getStyle().equals(styleStr)) {
									return;
								}
								self.setStyle(styleStr);
							]]>
						</attribute>
						<attribute name="onSetPortraitName">
							<![CDATA[
								self.setAttribute("portraitName", event.getData());
								String content = self.getAttribute("portraitName");
								Object fellow = self.getFellow(self.getId() + "_namehtml");
								fellow.setVisible(!content.equals(""));
								if (fellow.getContent() == content) {
									return;
								}
								fellow.setContent(content);
							]]>
						</attribute>
						<h:div class="js-portrait"></h:div>
						<html id="${arg.a_propertyMap.get('macroBoxUuid')}_portrait_namehtml" class="js-portrait-name" />
					</div>
				</div>
				<div id="${arg.a_propertyMap.get('macroBoxUuid')}_text" visible="${arg.a_propertyMap.get('fragmentText') != ''}" class="js-dialogue-txt">
					<custom-attributes text="${arg.a_propertyMap.get('fragmentText')}"/>
					<attribute name="onCreate">
						<![CDATA[
							Events.sendEvent("onSetText", self, self.getAttribute("text"));
						]]>
					</attribute>
					<attribute name="onSetText">
						<![CDATA[
							self.setAttribute("text", event.getData());
							self.setVisible(!self.getAttribute("text").equals(""));
							Object fellow = self.getFellow(self.getId() + "_text");
							fellow.setContent(self.getAttribute("text"));
						]]>
					</attribute>
					<html id="${arg.a_propertyMap.get('macroBoxUuid')}_text_text" />
				</div>
				<div id="${arg.a_propertyMap.get('macroBoxUuid')}_right_button" class="js-dialogue__button right">
					<custom-attributes macroBoxUuid="${arg.a_propertyMap.get('macroBoxUuid')}"/>
					<custom-attributes buttonTag="${arg.a_propertyMap.get('rightButtonTag')}"/>
					<custom-attributes btnTooltip="${arg.a_propertyMap.get('btnRightTooltip')}"/>
					<attribute name="onCreate">
						<![CDATA[
							Events.sendEvent("onSetButtonTag", self, self.getAttribute("buttonTag"));
							Events.sendEvent("onSetButtonTooltip", self, self.getAttribute("btnTooltip"));
						]]>
					</attribute>
					<attribute name="onSetButtonTag">
						<![CDATA[
							self.setAttribute("buttonTag", event.getData());
							self.setVisible(self.getAttribute("buttonTag") != null);
						]]>
					</attribute>
					<attribute name="onSetButtonTooltip">
						<![CDATA[
							self.setAttribute("btnTooltip", event.getData());
							String content = self.getAttribute("btnTooltip");
							Object fellowDiv = self.getFellow(self.getId() + "_tooltipdiv");
							Object fellowHtml = self.getFellow(self.getId() + "_tooltiphtml");
							fellowDiv.setVisible(content != null && !content.equals(""));
							if (fellowHtml.getContent() == content) {
								return;
							}
							fellowHtml.setContent(content);
						]]>
					</attribute>
					<attribute name="onClick">
						<![CDATA[
							Events.echoEvent("onButtonClick", nl.surf.emergo.control.CDesktopComponents.vView().getComponent(self.getAttribute("macroBoxUuid")), self.getAttribute("buttonTag"));
						]]>
					</attribute>
					<h:button id="${arg.a_propertyMap.get('macroBoxUuid')}_right_button_button" class="js-btn round primary">
						<h:i class="fas fa-chevron-right"/>
						<h:div id="${arg.a_propertyMap.get('macroBoxUuid')}_right_button_tooltipdiv" class="js-tooltip" style="left: -242px;">
							<html id="${arg.a_propertyMap.get('macroBoxUuid')}_right_button_tooltiphtml" />
						</h:div>
					</h:button>
				</div>
			</div>
		</div>
		
		<div id="${arg.a_propertyMap.get('macroBoxUuid')}_jump_button" class="js-answer" style="position:absolute;left:20px;top:645px;">
			<custom-attributes macroBoxUuid="${arg.a_propertyMap.get('macroBoxUuid')}"/>
			<custom-attributes buttonTag="${arg.a_propertyMap.get('jumpButtonTag')}"/>
			<custom-attributes buttonText="${arg.a_propertyMap.get('jumpButtonText')}"/>
			<custom-attributes buttonIconClass="${arg.a_propertyMap.get('jumpButtonIconClass')}"/>
			<attribute name="onCreate">
				<![CDATA[
					Events.sendEvent("onSetButtonTag", self, self.getAttribute("buttonTag"));
					Events.sendEvent("onSetButtonText", self, self.getAttribute("buttonText"));
					Events.sendEvent("onSetButtonIconClass", self, self.getAttribute("buttonIconClass"));
				]]>
			</attribute>
			<attribute name="onSetButtonTag">
				<![CDATA[
					self.setAttribute("buttonTag", event.getData());
					self.setVisible(self.getAttribute("buttonTag") != null);
				]]>
			</attribute>
			<attribute name="onSetButtonText">
				<![CDATA[
					self.setAttribute("buttonText", event.getData());
					Object fellow = self.getFellow(self.getId() + "_text");
					fellow.setContent(self.getAttribute("buttonText"));
				]]>
			</attribute>
			<attribute name="onSetButtonIconClass">
				<![CDATA[
					self.setAttribute("buttonIconClass", event.getData());
					Object fellow = self.getFellow(self.getId() + "_icon");
					fellow.setSclass(self.getAttribute("buttonIconClass"));
				]]>
			</attribute>
			<attribute name="onClick">
				<![CDATA[
					Events.echoEvent("onButtonClick", nl.surf.emergo.control.CDesktopComponents.vView().getComponent(self.getAttribute("macroBoxUuid")), self.getAttribute("buttonTag"));
				]]>
			</attribute>
			<h:button class="js-btn c2a primary icon-right">
				<html id="${arg.a_propertyMap.get('macroBoxUuid')}_jump_button_text" />
				<h:i id="${arg.a_propertyMap.get('macroBoxUuid')}_jump_button_icon" />
			</h:button>
		</div>

	</div>

</zk>