<?xml version="1.0" encoding="UTF-8"?>
<zk xmlns:h="http://www.w3.org/1999/xhtml">

<style>

div.mainMenu
{
	position					: absolute;
	left							: 0px; 
	top								: 0px;
	width							: 290px;
	height						: 650px;
	z-index						: 3;
}

div.generalMenuOptions
{
	position					: absolute;
	left							: 0px; 
	top								: 0px;
	width							: 290px;
	height						: 460px;
	background-color	: #42495a;
	overflow					: auto;
}

div.stepMenuOptions
{
	position					: absolute;
	left							: 0px; 
	top								: 460px;
	width							: 290px;
	height						: 190px;
	background-color	: #5d6474;
	overflow					: auto;
}

img.closeMainMenuButton
{
	position					: absolute;
	left							: 240px; 
	top								: 20px;
	width							: 25px;
	height						: 25px;
	cursor						: pointer;
}

div.dashboardButton,
div.progressButton,
div.notificationsButton,
div.rubricButton,
div.methodButton,
div.myGoalsButton,
div.myFeedbackButton,
div.myRecordingsButton,
div.superTeacherButton,
div.prepareButton,
div.practiceButton,
div.feedbackButton,
div.viewFeedbackButton,
div.defineGoalsButton
{
	position					: relative;
	left							: 30px;
	top								: 20px;
	width							: 260px;
	height						: 40px;
	cursor						: pointer;
}

img.generalMenuOptionsButtonIcon,
img.stepMenuOptionsButtonIcon
{
	position					: absolute;
	left							: 0px; 
	top								: 3px;
	width							: 16px;
	height						: 16px;
}

span.generalMenuOptionsButtonLabel,
span.stepMenuOptionsButtonLabel
{
	position					: absolute;
	left							: 25px; 
	top								: 0px;
	width							: 200px;
	height						: 25px;
	color							: #ffffff;
	font-size					: 16px;
}

span.stepMenuOptionsHeaderLabel
{
	position					: absolute;
	left							: 30px; 
	top								: 65px;
	width							: 200px;
	height						: 25px;
	color							: #c1c1c1;
	font-size					: 16px;
}

</style>

<div id="mainMenu" class="mainMenu" visible="${preferred_panel == ''}">
	<div id="generalMenuOptions" class="generalMenuOptions">
		<div if="${arg.a_propertyMap.get('isStudent') || arg.a_propertyMap.get('isTeacher') || arg.a_propertyMap.get('isSuperTeacher')}" id="dashboardButton" use="nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitMainMenuOptionButton" class="dashboardButton">
			<custom-attributes if="${arg.a_propertyMap.get('isStudent')}" panel="dashboard"/>
			<custom-attributes if="${arg.a_propertyMap.get('isTeacher')}" panel="teacherDashboard"/>
			<custom-attributes if="${!arg.a_propertyMap.get('isStudent') and !arg.a_propertyMap.get('isTeacher') and arg.a_propertyMap.get('isSuperTeacher')}" panel="teacherDashboard"/>
			<image id="dashboardButtonIcon" src="${arg.a_propertyMap.get('zulfilepath')}tachometer-alt-fast-white.svg" class="generalMenuOptionsButtonIcon"/>
			<label id="dashboardButtonLabel" use="nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel" class="font generalMenuOptionsButtonLabel">
				<custom-attributes labelKey="PV-toolkit.mainmenuoption.dashboard"/>
			</label>
		</div>
		<div if="${arg.a_propertyMap.get('isPractitioner')}" id="progressButton" use="nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitMainMenuOptionButton" class="progressButton">
			<custom-attributes panel="progress"/>
			<image id="progressButtonIcon" src="${arg.a_propertyMap.get('zulfilepath')}dot-circle.svg" class="generalMenuOptionsButtonIcon"/>
			<label id="progressButtonLabel" use="nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel" class="font generalMenuOptionsButtonLabel">
				<custom-attributes labelKey="PV-toolkit.mainmenuoption.progress"/>
			</label>
		</div>
		<div if="${arg.a_propertyMap.get('isTeacher') || arg.a_propertyMap.get('isSuperTeacher')}" id="groupsProgressButton" use="nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitMainMenuOptionButton" class="progressButton">
			<custom-attributes panel="teacherGroupsProgress"/>
			<image id="groupsProgressButtonIcon" src="${arg.a_propertyMap.get('zulfilepath')}chart-line-white.svg" class="generalMenuOptionsButtonIcon"/>
			<label id="groupsProgressButtonLabel" use="nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel" class="font generalMenuOptionsButtonLabel">
				<custom-attributes labelKey="PV-toolkit.mainmenuoption.groupsProgress"/>
			</label>
		</div>
		<div if="${arg.a_propertyMap.get('isTeacher') || arg.a_propertyMap.get('isSuperTeacher')}" id="studentsProgressButton" use="nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitMainMenuOptionButton" class="progressButton">
			<custom-attributes panel="teacherStudentsProgress"/>
			<image id="studentsProgressButtonIcon" src="${arg.a_propertyMap.get('zulfilepath')}user-friends-white.svg" class="generalMenuOptionsButtonIcon"/>
			<label id="studentsProgressButtonLabel" use="nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel" class="font generalMenuOptionsButtonLabel">
				<custom-attributes labelKey="PV-toolkit.mainmenuoption.studentsProgress"/>
			</label>
		</div>
		<div if="${arg.a_propertyMap.get('isStudent') || arg.a_propertyMap.get('isTeacher')}" id="notificationsButton" use="nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitMainMenuOptionButton" class="notificationsButton">
			<custom-attributes panel="notifications"/>
			<image id="notificationsButtonIcon" src="${arg.a_propertyMap.get('zulfilepath')}envelope-white.svg" class="generalMenuOptionsButtonIcon"/>
			<label id="notificationsButtonLabel" use="nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel" class="font generalMenuOptionsButtonLabel">
				<custom-attributes labelKey="PV-toolkit.mainmenuoption.notifications"/>
			</label>
		</div>
		<div id="rubricButton" use="nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitMainMenuOptionButton" class="rubricButton">
			<custom-attributes panel="rubric"/>
			<image id="rubricButtonIcon" src="${arg.a_propertyMap.get('zulfilepath')}star-white.svg" class="generalMenuOptionsButtonIcon"/>
			<label id="rubricButtonLabel" use="nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel" class="font generalMenuOptionsButtonLabel">
				<custom-attributes labelKey="PV-toolkit.mainmenuoption.rubric"/>
			</label>
		</div>
		<div id="methodButton" use="nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitMainMenuOptionButton" class="methodButton">
			<custom-attributes panel="method"/>
			<image id="methodButtonIcon" src="${arg.a_propertyMap.get('zulfilepath')}ballot-white.svg" class="generalMenuOptionsButtonIcon"/>
			<label id="methodButtonLabel" use="nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel" class="font generalMenuOptionsButtonLabel">
				<custom-attributes labelKey="PV-toolkit.mainmenuoption.method"/>
			</label>
		</div>
		<div if="${arg.a_propertyMap.get('isPractitioner')}" id="myGoalsButton" use="nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitMainMenuOptionButton" class="myGoalsButton">
			<custom-attributes panel="myGoals"/>
			<image id="myGoalsButtonIcon" src="${arg.a_propertyMap.get('zulfilepath')}bullseye-arrow-white.svg" class="generalMenuOptionsButtonIcon"/>
			<label id="myGoalsButtonLabel" use="nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel" class="font generalMenuOptionsButtonLabel">
				<custom-attributes labelKey="PV-toolkit.mainmenuoption.myGoals"/>
			</label>
		</div>
		<div if="${arg.a_propertyMap.get('isFeedbackGiver')}" id="myFeedbackButton" use="nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitMainMenuOptionButton" class="myFeedbackButton">
			<custom-attributes panel="myFeedback"/>
			<image id="myFeedbackButtonIcon" src="${arg.a_propertyMap.get('zulfilepath')}comment-alt-check.svg" class="generalMenuOptionsButtonIcon"/>
			<label id="myFeedbackButtonLabel" use="nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel" class="font generalMenuOptionsButtonLabel">
				<custom-attributes labelKey="PV-toolkit.mainmenuoption.myFeedback"/>
			</label>
		</div>
		<div if="${arg.a_propertyMap.get('isPractitioner')}" id="myRecordingsButton" use="nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitMainMenuOptionButton" class="myRecordingsButton">
			<custom-attributes panel="myRecordings"/>
			<image id="myRecordingsButtonIcon" src="${arg.a_propertyMap.get('zulfilepath')}video-white.svg" class="generalMenuOptionsButtonIcon"/>
			<label id="myRecordingsButtonLabel" use="nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel" class="font generalMenuOptionsButtonLabel">
				<custom-attributes labelKey="PV-toolkit.mainmenuoption.myRecordings"/>
			</label>
		</div>
		<div if="${arg.a_propertyMap.get('isSuperTeacher')}" id="superTeacherButton" use="nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitMainMenuOptionButton" class="superTeacherButton">
			<custom-attributes panel="superTeacher"/>
			<image id="superTeacherButtonIcon" src="${arg.a_propertyMap.get('zulfilepath')}atom.svg" class="generalMenuOptionsButtonIcon"/>
			<label id="superTeacherButtonLabel" use="nl.surf.emergo.control.run.cases.PVtoolkit.def.CRunPVToolkitDefLabel" class="font generalMenuOptionsButtonLabel">
				<custom-attributes labelKey="PV-toolkit.mainmenuoption.superTeacher"/>
			</label>
		</div>
		<image id="closeMainMenuButton" use="nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitCloseMainMenuImage" src="${arg.a_propertyMap.get('zulfilepath')}arrow-alt-left-white.svg" class="closeMainMenuButton"/>
	</div>
	<div id="stepMenuOptions" use="nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkitMainMenuStepOptionButtonsDiv" class="stepMenuOptions"/>
</div>

</zk>
