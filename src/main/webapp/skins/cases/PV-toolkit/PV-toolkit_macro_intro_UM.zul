<?xml version="1.0" encoding="UTF-8"?>

<zk xmlns="http://www.zkoss.org/2005/zul"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.zkoss.org/2005/zul http://www.zkoss.org/2005/zul/zul.xsd">

<style>
div.introDiv
{
	position					: absolute;
	left							: 0px;
	top								:	0px;
	width							: 1200px;
	height						: 650px;
	background-color	: #EEEEEE;color:#000000
}

div.introSubDiv
{
	position					: absolute;
	left							: 20px;
	top								:	20px;
	width							: 1160px;
}

span.introFont
{
	font-family				: 'Open Sans', Arial, sans-serif;
	font-weight				: 600;
	font-style				: normal;
}

div.introCenter
{
	width							: 1160px;
	text-align				: center;
}

span.introHeader
{
	font-family				: 'Open Sans', Arial, sans-serif;
	font-weight				: 600;
	font-style				: normal;
	font-size					: 18px;
}

button.continuButton
{
	position					: absolute;
	left							: 525px;
	top								: 580px;
	width							: 150px;
	height						: 30px;
  border						: none;
  border-radius			: 12px;
  color							: white;
	background-color	: #74ba0c;
	font-size					: 16px;
	font-weight				: 600;
 	cursor						: pointer;
}

</style>

<zscript><![CDATA[

import nl.surf.emergo.business.impl.AppConstants;
import nl.surf.emergo.control.CDesktopComponents;
import nl.surf.emergo.control.run.cases.PVtoolkit.CRunPVToolkit;

List errorMessages = new ArrayList();
boolean informed_consent = CDesktopComponents.sSpring().getCurrentRunTagStatus(
	CRunPVToolkit.gCaseComponentNameStatesExperiment,
	"state",
	"informed_consent",
	AppConstants.statusKeyValue,
	AppConstants.statusTypeRunGroup,
	errorMessages).equals(AppConstants.statusValueTrue);

]]></zscript>

<div zclass="introDiv">
	<div id="introInformedConsent1" zclass="introSubDiv" visible="${!informed_consent}"> 
		<div zclass="introCenter">
			<label zclass="introHeader" value="Informatie bij het onderdeel Juridisch Presenteren en flankerend onderzoek Pe(e)rfect Vaardig via een survey-interview"/>
		</div>
		<html zclass="introFont">
<br/>		
Dit onderdeel 'Juridisch presenteren' doorloopt u op een bepaalde manier. We spreken van een leer &amp; studeer-traject. Deze manier van doorlopen is niet voor iedereen precies hetzelfde, maar iedereen heeft even veel tijd nodig, krijgt even veel tijd, krijgt even veel hulp, en heeft dus een kwalitatief gelijkwaardig onderdeel. We zullen in dit traject regelmatig uw reactie vragen zodat wij uw manier van doorlopen van dit onderdeel goed op waarde kunnen schatten en u beter kunnen helpen. Hierdoor kunnen wij uw mening meenemen bij de uitwerking van de leer &amp; studeer-trajecten in dit onderdeel. Deze trajecten kunnen we door uw doorlopen van dit onderdeel tevens automatisch onderzoeken in het onderzoek 'Pe(e)rfect Vaardig'.<br/> 
<br/>
U hoeft hier dus niets extra's voor te doen en het kost u geen extra tijd.<br/> 
<br/>
Alle gegevens worden voor dit onderzoek volgens AVG-richtlijnen opgeslagen en anoniem verwerkt voor de verdere uitwerking van deze trajecten (zie <a href="https://www.ou.nl/privacy" target="_blank">www.ou.nl/privacy</a>). Onder deze gegevens vallen de opnames van uw juridische presentaties die u heeft gedeeld met anderen (namelijk de peers in uw peerfeedbackgroepje en uw docent), uw reacties op andermans juridische presentaties uit uw peerfeedbackgroepje, en uw e-mailadres. Deze opnames worden veilig bewaard in DANS-EASY (KNAW) met alleen toegang voor de primaire onderzoeker. De opnames die u niet heeft gedeeld worden niet ingezien noch door ons bewaard.<br/>
De hierboven vermelde informatie heeft u al van ons gekregen op het moment dat u met dit onderdeel van start ging. U bent nu op een punt in dit onderdeel aangekomen dat we u tevens vragen of u wilt deelnemen aan het survey-interview. Deelname aan het survey-interview duurt circa 1 uur. Ná deelname kunnen we u een bescheiden, maar wel memorabele attentie aanbieden. Door uw deelname aan het survey-interview kunnen we preciezer nagaan wat uw ervaringen waren met de PV-tool in functie van uw juridische presentaties.<br/> 
Dit survey-interview bestaat uit een survey-deel en een interview-deel. De survey is een online vragenlijst waarvan de beantwoording circa 20 minuten duurt. Het interview wordt door een onderzoeker geleid en wordt online (waarschijnlijk in Teams) afgenomen in een klein groepje met medestudenten in de week van 12 april-16 april (week 15). Het interview duurt circa 40 minuten. Het interview is dus COVID-proof. Mede uw antwoorden op de online vragenlijst uit de survey gelden als input voor dit interview. Bij de verwerking van de gegevens uit het survey-interview is vanzelfsprekend anonimiteit gewaarborgd en geldt dat de gegevens veilig bewaard worden op de wijze zoals ook hierboven is vermeld. U krijgt in week 13 de mogelijkheid om als antwoord in de aan u geadresseerde mail aan te geven op welk moment het interview voor u het meest passend is. U krijgt in die week de online vragenlijst toegestuurd als u nu instemt met deelname aan de survey-interview.<br/>  
<br/>
Mocht u hierover nog vragen hebben, dan kunt u contact opnemen met de primaire onderzoeker bij dit onderdeel.<br/>  
Dr Rob Nadolski; e-mail: rob.nadolski@ou.nl of tel: 045-5762 389
		</html>
		<button zclass="continuButton introFont" label="Verder">
			<attribute name="onClick">
				<![CDATA[
					introInformedConsent1.setVisible(false);
					introInformedConsent2.setVisible(true);
				]]>
			</attribute>
		</button>
	</div>

	<div id="introInformedConsent2" zclass="introSubDiv" visible="false"> 
		<html>
			<br/>
		</html>
		<div zclass="introCenter">
			<label zclass="introHeader" value="Informed consent t.b.v. PV-tool"/>
		</div>
		<html zclass="introFont">
			<ul>
			<li>Ik geef toestemming om de gegevens die verzameld zijn tijdens dit onderdeel te gebruiken voor wetenschappelijk onderzoek.</li>
			<li>Ik heb de informatie ontvangen die bij dit onderdeel en het onderzoek hoorde en ik heb de gelegenheid gehad om vragen te stellen aan de onderzoeker en/of docent als er bepaalde dingen niet duidelijk waren.</li>
			<li>Ik begrijp dat de informatie die ik ten behoeve van dit onderzoek in dit onderdeel geef anoniem worden verzameld en niet tot mij terug te leiden zijn.</li>
			<li>Ik begrijp dat ik op elk moment kan stoppen met het onderzoek in dit onderdeel, ik hoef hiervoor geen reden op te geven. Deelname aan het onderzoek is geheel vrijwillig en heeft geen impact op de studieresultaten.</li>
			<li>Indien u de bovenstaande punten heeft gelezen en akkoord gaat met deelname aan het onderzoek in dit onderdeel, gelieve hieronder dan het toestemmingsformulier digitaal te tekenen.</li>
			</ul>
		</html>
		<div zclass="introCenter">
			<radiogroup id="consent1">
				<radio zclass="introFont" style="padding-right:10px;" label="Akkoord"/>
				<radio zclass="introFont" label="Niet akkoord"/>
			</radiogroup>
		</div>
		<html>
			<br/>
			<br/>
		</html>
		<div zclass="introCenter">
			<label zclass="introHeader" value="Informed consent t.b.v. survey-interview"/>
		</div>
		<html zclass="introFont">
			<ul>
			<li>Ik geef toestemming om de gegevens die verzameld worden tijdens de survey-interview te gebruiken voor wetenschappelijk onderzoek.</li>
			<li>Ik heb de informatie ontvangen die bij dit survey-interview hoorde en ik heb de gelegenheid gehad om vragen te stellen aan de onderzoeker als er bepaalde dingen niet duidelijk waren.</li>
			<li>Ik begrijp dat al de informatie die ik ten behoeve van dit survey-interview over dit onderdeel geef anoniem worden verzameld en niet tot mij terug te leiden zijn.</li>
			<li>Ik begrijp dat ik op elk moment kan stoppen met het survey-interview bij dit onderdeel, ik hoef hiervoor geen reden op te geven. Deelname aan de survey-interview is geheel vrijwillig en heeft geen impact op de studieresultaten.</li>
			<li>Indien u de bovenstaande punten heeft gelezen en akkoord gaat met deelname aan het survey-interview, gelieve hieronder dan het toestemmingsformulier digitaal te tekenen.</li>
			</ul>
		</html>
		<div zclass="introCenter">
			<radiogroup id="consent2">
				<radio zclass="introFont" style="padding-right:10px;" label="Akkoord"/>
				<radio zclass="introFont" label="Niet akkoord"/>
			</radiogroup>
		</div>
		<button zclass="continuButton introFont" label="Verder">
			<attribute name="onClick">
				<![CDATA[
					if (consent1.getSelectedIndex() == -1 || consent2.getSelectedIndex() == -1) {
						Messagebox.show("Maakt a.u.b. twee maal een keuze.", "Informatie", Messagebox.OK, Messagebox.INFORMATION);
					}
					else {
					  CDesktopComponents.sSpring().setCurrentRunTagStatus(
							CRunPVToolkit.gCaseComponentNameStatesExperiment,
							"state",
							"informed_consent1",
							AppConstants.statusKeyValue,
							"" + (consent1.getSelectedIndex() == 0),
							AppConstants.statusTypeRunGroup,
							true,
							errorMessages);
					  CDesktopComponents.sSpring().setCurrentRunTagStatus(
							CRunPVToolkit.gCaseComponentNameStatesExperiment,
							"state",
							"informed_consent2",
							AppConstants.statusKeyValue,
							"" + (consent2.getSelectedIndex() == 0),
							AppConstants.statusTypeRunGroup,
							true,
							errorMessages);
						introInformedConsent2.setVisible(false);
						introMethode.setVisible(true);
					}
				]]>
			</attribute>
		</button>
	</div>

	<div id="introMethode" zclass="introSubDiv" visible="${informed_consent}"> 
		<div zclass="introCenter">
			<label zclass="introHeader" value="PV-Methode"/>
		</div>
		<html>
			<br/>
		</html>
		<html zclass="introFont">
De PV-Methode wordt ondersteund met de PV-tool. PV staat voor Pe(e)rfect Vaardig. Peers (medestudenten) zijn cruciaal bij het gebruik van de PV-Methode en de PV-tool. De PV-tool is de App waarmee u de PV-Methode doorloopt.<br/>
De PV-methode bestaat uit vijf stappen die u in deze fase van de cursus vier keer doorloopt. Het doorlopen van de vijf stappen vormt samen een presentatie-ronde. Per oefencasus gelden twee presentatie-rondes. Omdat er twee oefencasus zijn, bestaat de Oefencasus-fase dus uit vier presentatie-rondes.<br/>
De stappen in de PV-Methode zijn:<br/> 
Stap 1: Voorbereiden<br/>
Stap 2: Oefenen (waaronder zelffeedback geven en feedback vragen)<br/>
Stap 3: Peerfeedback geven<br/>
Stap 4: Feedback bekijken<br/>
Stap 5: Doelen stellen<br/>
In de PV-tool krijgt u bij elke stap informatie over uw acties binnen die stap. U gaat in de PV-Methode altijd een stap vooruit, u kunt nooit een stap terug. Maar, wat u in een stap doet blijft wel altijd ‘opvraagbaar’ bij andere stappen, en dit geldt ook over presentatie-rondes heen. Op deze manier ontwikkelt u zich op de vaardigheid waarvoor de PV-Methode wordt gebruikt. Voor u is dit de vaardigheid juridisch presenteren. Omdat u de PV-Methode doorloopt met medestudenten uit uw peerfeedbackgroepje moet uw tempo op elkaar zijn afgestemd. In de PV-tool verneemt u welk tempo in uw peerfeedbackgroepje nodig is. Dit tempo staat ook in het studiemateriaal. Daarom kunt u soms nog niet naar de volgende stap. Dit is omdat een medestudent (of uzelf) nog acties moet doen of omdat uw docent dit heeft bepaald. Elke stap heeft een basisscherm.<br/> 
NB Volg alle acht vermelde deadlines nauwgezet. Er zijn twee deadlines per presentatie-ronde.<br/> 
<br/>
Bij de stapsgewijze aanpak van de PV-Methode staat een Rubriek centraal. Een Rubriek toont hoe de vaardigheid is opgesplitst in deelvaardigheden en beheersingsniveaus. De beheersingsniveaus zijn voor elke deelvaardigheid nader omschreven. Deze Rubriek kunt u op elk moment raadplegen.<br/> 
De Rubriek bevat zowel een tekstuele uitleg alsook een verheldering via video. De Rubriek helpt u bij: (i) het krijgen van een completer en beter beeld op de vaardigheid, (ii) het oefenen van de vaardigheid en (iii) het geven van feedback. De Rubriek helpt u te bepalen hoe u het doet op de beheersing van de vaardigheid.<br/>
<br/>
Overigens, de hierboven staande beschrijving van de PV-Methode is altijd oproepbaar via het Menu (links op het scherm). Het basisscherm van de Stap bereikt u altijd via ‘huidige activiteit’ in het Menu.<br/> 
Kies nu ‘Verder’ om naar Stap 1 van ronde 1 te gaan.
		</html>
		<button zclass="continuButton introFont" label="Verder">
			<attribute name="onClick">
				<![CDATA[
				  CDesktopComponents.sSpring().setCurrentRunTagStatus(
						CRunPVToolkit.gCaseComponentNameStatesExperiment,
						"state",
						"beam_to_PV_tool",
						AppConstants.statusKeyValue,
						AppConstants.statusValueTrue,
						AppConstants.statusTypeRunGroup,
						true,
						errorMessages);
				]]>
			</attribute>
		</button>
	</div>
</div>

</zk>